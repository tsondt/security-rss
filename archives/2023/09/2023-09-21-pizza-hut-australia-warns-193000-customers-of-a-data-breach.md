Title: Pizza Hut Australia warns 193,000 customers of a data breach
Date: 2023-09-21T11:50:45-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-21-pizza-hut-australia-warns-193000-customers-of-a-data-breach

[Source](https://www.bleepingcomputer.com/news/security/pizza-hut-australia-warns-193-000-customers-of-a-data-breach/){:target="_blank" rel="noopener"}

> Pizza Hut Australia is sending data breach notifications to customers, warning that a cyberattack allowed hackers to access their personal information. [...]
