Title: Russian allegedly smuggled US weapons electronics to Moscow
Date: 2023-09-19T19:55:51+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-19-russian-allegedly-smuggled-us-weapons-electronics-to-moscow

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/19/russian_electronics_smuggler_charged/){:target="_blank" rel="noopener"}

> Feds claim sniper scope displays sold in sanctions-busting move A Russian national helped smuggle, via shell companies in Hong Kong, more than $1.6 million in microelectronics to Moscow potentially to support its war against Ukraine, it is claimed.... [...]
