Title: 'Redfly' hackers infiltrated power supplier's network for 6 months
Date: 2023-09-12T06:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-12-redfly-hackers-infiltrated-power-suppliers-network-for-6-months

[Source](https://www.bleepingcomputer.com/news/security/redfly-hackers-infiltrated-power-suppliers-network-for-6-months/){:target="_blank" rel="noopener"}

> An espionage threat group tracked as 'Redfly' hacked a national electricity grid organization in Asia and quietly maintained access to the breached network for six months. [...]
