Title: Fake Signal and Telegram Apps in the Google Play Store
Date: 2023-09-14T11:05:51+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberespionage;espionage;open source;Signal;spyware;Telegram
Slug: 2023-09-14-fake-signal-and-telegram-apps-in-the-google-play-store

[Source](https://www.schneier.com/blog/archives/2023/09/fake-signal-and-telegram-apps-in-the-google-play-store.html){:target="_blank" rel="noopener"}

> Google removed fake Signal and Telegram apps from its Play store. An app with the name Signal Plus Messenger was available on Play for nine months and had been downloaded from Play roughly 100 times before Google took it down last April after being tipped off by security firm ESET. It was also available in the Samsung app store and on signalplus[.]org, a dedicated website mimicking the official Signal.org. An app calling itself FlyGram, meanwhile, was created by the same threat actor and was available through the same three channels. Google removed it from Play in 2021. Both apps remain [...]
