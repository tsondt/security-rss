Title: ‘Snatch’ Ransom Group Exposes Visitor IP Addresses
Date: 2023-09-27T11:48:37+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Breadcrumbs;Data Breaches;Ne'er-Do-Well News;Ransomware;8Base Ransomware;@htmalgae;AtomicStealer;DomainTools.com;Google.com;Malwarebytes;Microsoft Teams;Mihail Kolesnikov;Rilide;Trustwave Spiderlabs
Slug: 2023-09-27-snatch-ransom-group-exposes-visitor-ip-addresses

[Source](https://krebsonsecurity.com/2023/09/snatch-ransom-group-exposes-visitor-ip-addresses/){:target="_blank" rel="noopener"}

> The victim shaming site operated by the Snatch ransomware group is leaking data about its true online location and internal operations, as well as the Internet addresses of its visitors, KrebsOnSecurity has found. The leaked data suggest that Snatch is one of several ransomware groups using paid ads on Google.com to trick people into installing malware disguised as popular free software, such as Microsoft Teams, Adobe Reader, Mozilla Thunderbird, and Discord. First spotted in 2018, the Snatch ransomware group has published data stolen from hundreds of organizations that refused to pay a ransom demand. Snatch publishes its stolen data at [...]
