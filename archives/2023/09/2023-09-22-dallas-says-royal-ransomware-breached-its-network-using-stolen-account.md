Title: Dallas says Royal ransomware breached its network using stolen account
Date: 2023-09-22T16:59:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-09-22-dallas-says-royal-ransomware-breached-its-network-using-stolen-account

[Source](https://www.bleepingcomputer.com/news/security/dallas-says-royal-ransomware-breached-its-network-using-stolen-account/){:target="_blank" rel="noopener"}

> The City of Dallas, Texas, said this week that the Royal ransomware attack that forced it to shut down all IT systems in May started with a stolen account. [...]
