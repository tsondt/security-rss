Title: Hotel hackers redirect guests to fake Booking.com to steal cards
Date: 2023-09-22T07:41:05-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-09-22-hotel-hackers-redirect-guests-to-fake-bookingcom-to-steal-cards

[Source](https://www.bleepingcomputer.com/news/security/hotel-hackers-redirect-guests-to-fake-bookingcom-to-steal-cards/){:target="_blank" rel="noopener"}

> Security researchers discovered a multi-step information stealing campaign where hackers breach the systems of hotels, booking sites, and travel agencies and then use their access to go after financial data belonging to customers. [...]
