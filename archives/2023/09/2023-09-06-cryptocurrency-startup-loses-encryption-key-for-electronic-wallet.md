Title: Cryptocurrency Startup Loses Encryption Key for Electronic Wallet
Date: 2023-09-06T11:05:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptocurrency;encryption;keys
Slug: 2023-09-06-cryptocurrency-startup-loses-encryption-key-for-electronic-wallet

[Source](https://www.schneier.com/blog/archives/2023/09/cryptocurrency-startup-loses-encryption-key-for-electronic-wallet.html){:target="_blank" rel="noopener"}

> The cryptocurrency fintech startup Prime Trust lost the encryption key to its hardware wallet—and the recovery key—and therefore $38.9 million. It is now in bankruptcy. I can’t understand why anyone thinks these technologies are a good idea. [...]
