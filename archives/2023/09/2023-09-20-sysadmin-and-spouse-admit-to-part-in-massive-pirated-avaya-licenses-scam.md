Title: Sysadmin and spouse admit to part in 'massive' pirated Avaya licenses scam
Date: 2023-09-20T12:17:07+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-09-20-sysadmin-and-spouse-admit-to-part-in-massive-pirated-avaya-licenses-scam

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/20/avaya_guilty_pleas/){:target="_blank" rel="noopener"}

> Could spend 20 years in prison after selling $88M in ADI software keys A sysadmin and his partner pleaded guilty this week to being part of a "massive" international ring that sold software licenses worth $88 million for "significantly below the wholesale price."... [...]
