Title: Evasive Gelsemium hackers spotted in attack against Asian govt
Date: 2023-09-23T11:09:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-23-evasive-gelsemium-hackers-spotted-in-attack-against-asian-govt

[Source](https://www.bleepingcomputer.com/news/security/evasive-gelsemium-hackers-spotted-in-attack-against-asian-govt/){:target="_blank" rel="noopener"}

> A stealthy advanced persistent threat (APT) tracked as Gelsemium was observed in attacks targeting a Southeast Asian government that spanned six months between 2022 and 2023. [...]
