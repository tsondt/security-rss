Title: Dymocks Booksellers suffers data breach impacting 836k customers
Date: 2023-09-08T13:13:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-08-dymocks-booksellers-suffers-data-breach-impacting-836k-customers

[Source](https://www.bleepingcomputer.com/news/security/dymocks-booksellers-suffers-data-breach-impacting-836k-customers/){:target="_blank" rel="noopener"}

> Dymocks Booksellers is warning customers their personal information was exposed in a data breach after the company's database was shared on hacking forums. [...]
