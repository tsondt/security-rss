Title: Marvell disputes claim Cavium backdoored chips for Uncle Sam
Date: 2023-09-19T20:55:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-09-19-marvell-disputes-claim-cavium-backdoored-chips-for-uncle-sam

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/19/marvell_disputes_claim_that_cavium/){:target="_blank" rel="noopener"}

> Allegations date back a decade to leaked Snowden docs Cavium, a maker of semiconductors acquired in 2018 by Marvell, was allegedly identified in documents leaked in 2013 by Edward Snowden as a vendor of semiconductors backdoored for US intelligence. Marvell denies it or Cavium placed backdoors in products at the behest of the US government.... [...]
