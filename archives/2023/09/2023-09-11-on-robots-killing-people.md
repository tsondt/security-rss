Title: On Robots Killing People
Date: 2023-09-11T11:04:44+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;cars;regulation;robotics
Slug: 2023-09-11-on-robots-killing-people

[Source](https://www.schneier.com/blog/archives/2023/09/on-robots-killing-people.html){:target="_blank" rel="noopener"}

> The robot revolution began long ago, and so did the killing. One day in 1979, a robot at a Ford Motor Company casting plant malfunctioned—human workers determined that it was not going fast enough. And so twenty-five-year-old Robert Williams was asked to climb into a storage rack to help move things along. The one-ton robot continued to work silently, smashing into Williams’s head and instantly killing him. This was reportedly the first incident in which a robot killed a human; many more would follow. At Kawasaki Heavy Industries in 1981, Kenji Urada died in similar circumstances. A malfunctioning robot he [...]
