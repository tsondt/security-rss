Title: Cloud CISO Perspectives: Late September 2023
Date: 2023-09-29T16:00:00+00:00
Author: Eric Brewer
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2023-09-29-cloud-ciso-perspectives-late-september-2023

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-late-september-2023/){:target="_blank" rel="noopener"}

> Welcome to the second Cloud CISO Perspectives for September 2023. This month, I’m turning the mic over to my colleague Eric Brewer, Google Cloud’s vice president of infrastructure and Google Fellow, to explain the importance of this year’s Securing Open Source Software Summit and why securing open source code is one of the most crucial tasks we face. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. aside_block <ListValue: [StructValue([('title', 'Board of [...]
