Title: Cisco BroadWorks impacted by critical authentication bypass flaw
Date: 2023-09-07T16:10:37-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-07-cisco-broadworks-impacted-by-critical-authentication-bypass-flaw

[Source](https://www.bleepingcomputer.com/news/security/cisco-broadworks-impacted-by-critical-authentication-bypass-flaw/){:target="_blank" rel="noopener"}

> A critical vulnerability impacting the Cisco BroadWorks Application Delivery Platform and Cisco BroadWorks Xtended Services Platform could allow remote attackers to forge credentials and bypass authentication. [...]
