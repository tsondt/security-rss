Title: MGM casino's ESXi servers allegedly encrypted in ransomware attack
Date: 2023-09-14T18:52:04-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-09-14-mgm-casinos-esxi-servers-allegedly-encrypted-in-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/mgm-casinos-esxi-servers-allegedly-encrypted-in-ransomware-attack/){:target="_blank" rel="noopener"}

> An affiliate of the BlackCat ransomware group, also known as APLHV, is behind the attack that disrupted MGM Resorts' operations, forcing the company to shut down IT systems. [...]
