Title: Google extends security update support for Chromebooks to 10 years
Date: 2023-09-15T12:05:48-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2023-09-15-google-extends-security-update-support-for-chromebooks-to-10-years

[Source](https://www.bleepingcomputer.com/news/security/google-extends-security-update-support-for-chromebooks-to-10-years/){:target="_blank" rel="noopener"}

> Google has announced the Auto Update Expiration (AUE) date will be extended from 5 years to 10 for all Chromebooks, guaranteeing a decade of monthly security updates. [...]
