Title: ‘Our health data is about to flow more freely, like it or not’: big tech’s plans for the NHS
Date: 2023-09-14T04:00:19+00:00
Author: Cori Crider
Category: The Guardian
Tags: NHS;Technology;Data and computer security;Health;Health policy;Healthcare industry;Peter Thiel
Slug: 2023-09-14-our-health-data-is-about-to-flow-more-freely-like-it-or-not-big-techs-plans-for-the-nhs

[Source](https://www.theguardian.com/society/2023/sep/14/our-health-data-is-about-to-flow-more-freely-like-it-or-not-big-techs-plans-for-the-nhs){:target="_blank" rel="noopener"}

> The government is about to award a £480m contract to build a vast new database of patient data. But if people don’t trust it, they’ll opt out – I know, because I felt I had to Last December, I had an abortion. Most unwanted pregnancies set a panic-timer ticking, but I was almost counting down the seconds until I had to catch a flight from London, where I live, to Texas, where I grew up, and where the provision of abortion care was recently made a felony. You bleed for a while after most abortions, and I was still bleeding [...]
