Title: LLMs and Tool Use
Date: 2023-09-08T11:05:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;essays;LLM
Slug: 2023-09-08-llms-and-tool-use

[Source](https://www.schneier.com/blog/archives/2023/09/ai-tool-use.html){:target="_blank" rel="noopener"}

> Last March, just two weeks after GPT-4 was released, researchers at Microsoft quietly announced a plan to compile millions of APIs—tools that can do everything from ordering a pizza to solving physics equations to controlling the TV in your living room—into a compendium that would be made accessible to large language models (LLMs). This was just one milestone in the race across industry and academia to find the best ways to teach LLMs how to manipulate tools, which would supercharge the potential of AI more than any of the impressive advancements we’ve seen to date. The Microsoft project aims to [...]
