Title: W3LL phishing kit hijacks thousands of Microsoft 365 accounts, bypasses MFA
Date: 2023-09-06T06:33:54-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-09-06-w3ll-phishing-kit-hijacks-thousands-of-microsoft-365-accounts-bypasses-mfa

[Source](https://www.bleepingcomputer.com/news/security/w3ll-phishing-kit-hijacks-thousands-of-microsoft-365-accounts-bypasses-mfa/){:target="_blank" rel="noopener"}

> A threat actor known as W3LL developed a phishing kit that can bypass multi-factor authentication along with other tools that compromised more than 8,000 Microsoft 365 corporate accounts. [...]
