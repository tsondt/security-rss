Title: Can we fix the weaknesses in password-based authentication?
Date: 2023-09-26T10:01:02-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-09-26-can-we-fix-the-weaknesses-in-password-based-authentication

[Source](https://www.bleepingcomputer.com/news/security/can-we-fix-the-weaknesses-in-password-based-authentication/){:target="_blank" rel="noopener"}

> There are inherent weaknesses to password-based authentication. Learn more from Specops Software on measures we can enforce to minimize these weaknesses and prevent corporate breaches. [...]
