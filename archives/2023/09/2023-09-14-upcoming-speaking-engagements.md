Title: Upcoming Speaking Engagements
Date: 2023-09-14T16:01:13+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2023-09-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2023/09/upcoming-speaking-engagements-31.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking at swampUP 2023 in San Jose, California, on September 13, 2023 at 11:35 AM PT. The list is maintained on this page. [...]
