Title: Cisco Catalyst SD-WAN Manager flaw allows remote server access
Date: 2023-09-28T11:15:23-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-28-cisco-catalyst-sd-wan-manager-flaw-allows-remote-server-access

[Source](https://www.bleepingcomputer.com/news/security/cisco-catalyst-sd-wan-manager-flaw-allows-remote-server-access/){:target="_blank" rel="noopener"}

> Cisco is warning of five new Catalyst SD-WAN Manager products vulnerabilities with the most critical allowing unauthenticated remote access to the server. [...]
