Title: PhD student guilty of 3D-printing 'kamikaze' drone for Islamic State terrorists
Date: 2023-09-29T19:31:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-29-phd-student-guilty-of-3d-printing-kamikaze-drone-for-islamic-state-terrorists

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/29/3d_drone_student/){:target="_blank" rel="noopener"}

> 'Research purposes' excuse didn't fly A PhD student has been found guilty of building a potentially deadly drone for Islamic State terrorists, in part using his home 3D printer.... [...]
