Title: Notepad++ 8.5.7 released with fixes for four security vulnerabilities
Date: 2023-09-08T15:46:15-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2023-09-08-notepad-857-released-with-fixes-for-four-security-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/notepad-plus-plus-857-released-with-fixes-for-four-security-vulnerabilities/){:target="_blank" rel="noopener"}

> Notepad++ version 8.5.7 has been released with fixes for multiple buffer overflow zero-days, with one marked as potentially leading to code execution by tricking users into opening specially crafted files. [...]
