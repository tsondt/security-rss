Title: Adobe, Apple, Google & Microsoft Patch 0-Day Bugs
Date: 2023-09-12T22:36:01+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;adobe;apple;Automox;CVE-2023-26369;CVE-2023-36761;CVE-2023-36802;CVE-2023-38148;CVE-2023-41064;google;iOS 16.16.1;Lockdown Mode;microsoft;Microsoft Patch Tuesday September 2023;Microsoft Word;Tom Bowyer
Slug: 2023-09-12-adobe-apple-google-microsoft-patch-0-day-bugs

[Source](https://krebsonsecurity.com/2023/09/adobe-apple-google-microsoft-patch-0-day-bugs/){:target="_blank" rel="noopener"}

> Microsoft today issued software updates to fix at least five dozen security holes in Windows and supported software, including patches for two zero-day vulnerabilities that are already being exploited. Also, Adobe, Google Chrome and Apple iOS users may have their own zero-day patching to do. On Sept. 7, researchers at Citizen Lab warned they were seeing active exploitation of a “zero-click,” zero-day flaw to install spyware on iOS devices without any interaction from the victim. “The exploit chain was capable of compromising iPhones running the latest version of iOS (16.6) without any interaction from the victim,” the researchers wrote. According [...]
