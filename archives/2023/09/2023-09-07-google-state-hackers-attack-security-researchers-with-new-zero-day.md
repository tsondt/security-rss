Title: Google: State hackers attack security researchers with new zero-day
Date: 2023-09-07T12:48:03-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2023-09-07-google-state-hackers-attack-security-researchers-with-new-zero-day

[Source](https://www.bleepingcomputer.com/news/security/google-state-hackers-attack-security-researchers-with-new-zero-day/){:target="_blank" rel="noopener"}

> Google's Threat Analysis Group (TAG) says North Korean state hackers are again targeting security researchers in attacks using at least one zero-day in an undisclosed popular software. [...]
