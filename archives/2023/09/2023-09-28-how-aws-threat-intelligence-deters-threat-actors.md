Title: How AWS threat intelligence deters threat actors
Date: 2023-09-28T11:22:42+00:00
Author: Mark Ryland
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Technical How-to;Thought Leadership;Amazon GuardDuty;AWS Shield;AWS WAF;Security Blog
Slug: 2023-09-28-how-aws-threat-intelligence-deters-threat-actors

[Source](https://aws.amazon.com/blogs/security/how-aws-threat-intelligence-deters-threat-actors/){:target="_blank" rel="noopener"}

> Every day across the Amazon Web Services (AWS) cloud infrastructure, we detect and successfully thwart hundreds of cyberattacks that might otherwise be disruptive and costly. These important but mostly unseen victories are achieved with a global network of sensors and an associated set of disruption tools. Using these capabilities, we make it more difficult and [...]
