Title: 4 Okta customers hit by campaign that gave attackers super admin control
Date: 2023-09-05T20:28:28+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;2fa;mfa;okta;social engineering
Slug: 2023-09-05-4-okta-customers-hit-by-campaign-that-gave-attackers-super-admin-control

[Source](https://arstechnica.com/?p=1965548){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Authentication service Okta said four of its customers have been hit in a recent social-engineering campaign that allowed hackers to gain control of super administrator accounts and from there weaken or entirely remove two-factor authentication protecting accounts from unauthorized access. The Okta super administrator accounts are assigned to users with the highest permissions inside an organization using Okta’s service. In recent weeks, Okta customers’ IT desk personnel have received calls that follow a consistent pattern of social engineering, in which attackers pose as a company insider in an attempt to trick workers into divulging passwords or [...]
