Title: Facebook Messenger phishing wave targets 100K business accounts per week
Date: 2023-09-11T11:01:25-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-11-facebook-messenger-phishing-wave-targets-100k-business-accounts-per-week

[Source](https://www.bleepingcomputer.com/news/security/facebook-messenger-phishing-wave-targets-100k-business-accounts-per-week/){:target="_blank" rel="noopener"}

> Hackers use a massive network of fake and compromised Facebook accounts to send out millions of Messenger phishing messages to target Facebook business accounts with password-stealing malware. [...]
