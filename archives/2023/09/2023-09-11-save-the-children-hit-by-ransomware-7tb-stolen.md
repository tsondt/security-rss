Title: Save the Children hit by ransomware, 7TB stolen
Date: 2023-09-11T22:21:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-11-save-the-children-hit-by-ransomware-7tb-stolen

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/11/bianlian_save_the_children/){:target="_blank" rel="noopener"}

> A new low, even for these lowlifes Updated Cybercrime crew BianLian says it has broken into the IT systems of a top nonprofit and stolen a ton of files, including what the miscreants claim is financial, health, and medical data.... [...]
