Title: Windows cryptomining attacks target graphic designer's high-powered GPUs
Date: 2023-09-07T11:46:17-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-09-07-windows-cryptomining-attacks-target-graphic-designers-high-powered-gpus

[Source](https://www.bleepingcomputer.com/news/security/windows-cryptomining-attacks-target-graphic-designers-high-powered-gpus/){:target="_blank" rel="noopener"}

> Cybercriminals are leveraging a legitimate Windows tool called 'Advanced Installer' to infect the computers of graphic designers with cryptocurrency miners. [...]
