Title: SSH keys stolen by stream of malicious PyPI and npm packages
Date: 2023-09-27T17:48:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-27-ssh-keys-stolen-by-stream-of-malicious-pypi-and-npm-packages

[Source](https://www.bleepingcomputer.com/news/security/ssh-keys-stolen-by-stream-of-malicious-pypi-and-npm-packages/){:target="_blank" rel="noopener"}

> A stream of malicious npm and PyPi packages have been found stealing a wide range of sensitive data from software developers on the platforms. [...]
