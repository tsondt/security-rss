Title: North Korea-backed hackers target security researchers with 0-day
Date: 2023-09-07T22:05:27+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;North Korea;security researchers;unc2970;zinc
Slug: 2023-09-07-north-korea-backed-hackers-target-security-researchers-with-0-day

[Source](https://arstechnica.com/?p=1966395){:target="_blank" rel="noopener"}

> Enlarge (credit: Dmitry Nogaev | Getty Images) North Korea-backed hackers are once again targeting security researchers with a zero-day exploit and related malware in an attempt to infiltrate computers used to perform sensitive investigations involving cybersecurity. The presently unfixed zero-day—meaning a vulnerability that’s known to attackers before the hardware or software vendor has a security patch available—resides in a popular software package used by the targeted researchers, Google researchers said Thursday. They declined to identify the software or provide details about the vulnerability until the vendor, which they privately notified, releases a patch. The vulnerability was exploited using a malicious [...]
