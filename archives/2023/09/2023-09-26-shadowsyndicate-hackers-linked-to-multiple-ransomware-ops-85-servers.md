Title: ShadowSyndicate hackers linked to multiple ransomware ops, 85 servers
Date: 2023-09-26T05:11:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-26-shadowsyndicate-hackers-linked-to-multiple-ransomware-ops-85-servers

[Source](https://www.bleepingcomputer.com/news/security/shadowsyndicate-hackers-linked-to-multiple-ransomware-ops-85-servers/){:target="_blank" rel="noopener"}

> Security researchers have identified infrastructure belonging to a threat actor now tracked as ShadowSyndicate, who likely deployed seven different ransomware families in attacks over the past year. [...]
