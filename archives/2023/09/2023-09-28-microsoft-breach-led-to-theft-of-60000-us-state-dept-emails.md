Title: Microsoft breach led to theft of 60,000 US State Dept emails
Date: 2023-09-28T16:45:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Government
Slug: 2023-09-28-microsoft-breach-led-to-theft-of-60000-us-state-dept-emails

[Source](https://www.bleepingcomputer.com/news/security/microsoft-breach-led-to-theft-of-60-000-us-state-dept-emails/){:target="_blank" rel="noopener"}

> Chinese hackers stole tens of thousands of emails from U.S. State Department accounts after breaching Microsoft's cloud-based Exchange email platform in May. [...]
