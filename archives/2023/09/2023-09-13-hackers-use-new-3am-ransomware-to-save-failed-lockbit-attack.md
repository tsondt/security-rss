Title: Hackers use new 3AM ransomware to save failed LockBit attack
Date: 2023-09-13T08:29:40-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-09-13-hackers-use-new-3am-ransomware-to-save-failed-lockbit-attack

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-new-3am-ransomware-to-save-failed-lockbit-attack/){:target="_blank" rel="noopener"}

> A new ransomware strain called 3AM has been uncovered after a threat actor used it in an attack that failed to deploy LockBit ransomware on a target network. [...]
