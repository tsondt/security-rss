Title: The Clorox Company admits cyberattack causing 'widescale disruption'
Date: 2023-09-19T12:15:08+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-09-19-the-clorox-company-admits-cyberattack-causing-widescale-disruption

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/19/the_clorox_company_admits_cyber/){:target="_blank" rel="noopener"}

> Back to 'manual' order processing for $7B household cleaning biz, financial impact will be 'material' The Clorox Company, makers of bleach and other household cleaning products, doesn't expect operations to return to normal until near month end as it combs over "widescale disruption to operations" caused by cyber baddies.... [...]
