Title: Feds raise alarm over Snatch ransomware as extortion crew brags of Veterans Affairs hit
Date: 2023-09-20T22:32:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-20-feds-raise-alarm-over-snatch-ransomware-as-extortion-crew-brags-of-veterans-affairs-hit

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/20/feds_issue_snatch_ransomware_alert/){:target="_blank" rel="noopener"}

> Invasion of the data snatchers The Snatch ransomware crew has listed on its dark-web site the Florida Department of Veterans Affairs as one of its latest victims – as the Feds warn organizations to be on the lookout for indicators of compromise linked to the extortionist gang.... [...]
