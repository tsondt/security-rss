Title: A phone call to helpdesk was likely all it took to hack MGM
Date: 2023-09-14T13:11:13+00:00
Author: Financial Times
Category: Ars Technica
Tags: Biz & IT;Security;malware;MGM;ransomware;syndication
Slug: 2023-09-14-a-phone-call-to-helpdesk-was-likely-all-it-took-to-hack-mgm

[Source](https://arstechnica.com/?p=1968329){:target="_blank" rel="noopener"}

> Enlarge / Gamblers and hotel guests at MGM casinos on the Las Vegas Strip, including the Bellagio, were affected by the security breach. (credit: Ethan Miller/Getty Images) A cyber criminal gang proficient in impersonation and malware has been identified as the likely culprit for an attack that paralized networks at US casino operator MGM Resorts International. The group, which security researchers call “Scattered Spider,” uses fraudulent phone calls to employees and help desks to “phish” for login credentials. It has targeted MGM and dozens of other Western companies with the aim of extracting ransom payments, according to two people familiar [...]
