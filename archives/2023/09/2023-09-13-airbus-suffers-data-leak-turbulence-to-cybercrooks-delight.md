Title: Airbus suffers data leak turbulence to cybercrooks' delight
Date: 2023-09-13T17:45:13+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-09-13-airbus-suffers-data-leak-turbulence-to-cybercrooks-delight

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/13/airbus_data_leak/){:target="_blank" rel="noopener"}

> Ransomware group nicked info from employee of airline, say researchers Aerospace giant Airbus has fallen victim to a data breach, thanks in part to the inattention of a third party.... [...]
