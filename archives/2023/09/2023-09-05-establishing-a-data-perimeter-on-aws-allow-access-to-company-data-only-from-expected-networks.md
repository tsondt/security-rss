Title: Establishing a data perimeter on AWS: Allow access to company data only from expected networks
Date: 2023-09-05T13:34:00+00:00
Author: Laura Reith
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Data protection;Identity;Network security;Security Blog;service control policies
Slug: 2023-09-05-establishing-a-data-perimeter-on-aws-allow-access-to-company-data-only-from-expected-networks

[Source](https://aws.amazon.com/blogs/security/establishing-a-data-perimeter-on-aws-allow-access-to-company-data-only-from-expected-networks/){:target="_blank" rel="noopener"}

> A key part of protecting your organization’s non-public, sensitive data is to understand who can access it and from where. One of the common requirements is to restrict access to authorized users from known locations. To accomplish this, you should be familiar with the expected network access patterns and establish organization-wide guardrails to limit access [...]
