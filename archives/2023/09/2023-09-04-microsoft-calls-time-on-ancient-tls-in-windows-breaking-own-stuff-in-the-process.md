Title: Microsoft calls time on ancient TLS in Windows, breaking own stuff in the process
Date: 2023-09-04T14:15:14+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-09-04-microsoft-calls-time-on-ancient-tls-in-windows-breaking-own-stuff-in-the-process

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/04/tls_windows_deprecation/){:target="_blank" rel="noopener"}

> Hold onto your SQL Server, enterprise admins Microsoft has reminded users that TLS 1.0 and 1.1 will soon be disabled by default in Windows.... [...]
