Title: Signal adds quantum-resistant encryption to its E2EE messaging protocol
Date: 2023-09-20T09:29:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-20-signal-adds-quantum-resistant-encryption-to-its-e2ee-messaging-protocol

[Source](https://www.bleepingcomputer.com/news/security/signal-adds-quantum-resistant-encryption-to-its-e2ee-messaging-protocol/){:target="_blank" rel="noopener"}

> Signal has announced that it upgraded its end-to-end communication protocol to use quantum-resistant encryption keys to protect users from future attacks. [...]
