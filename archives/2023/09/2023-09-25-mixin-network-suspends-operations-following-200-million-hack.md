Title: Mixin Network suspends operations following $200 million hack
Date: 2023-09-25T09:23:16-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-09-25-mixin-network-suspends-operations-following-200-million-hack

[Source](https://www.bleepingcomputer.com/news/security/mixin-network-suspends-operations-following-200-million-hack/){:target="_blank" rel="noopener"}

> Mixin Network, an open-source, peer-to-peer transactional network for digital assets, has announced today on Twitter that deposits and withdrawals are suspended effective immediately due to a $200 million hack the platform suffered on Saturday. [...]
