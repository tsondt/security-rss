Title: Google's Chrome gets caught with its WebP down, offers hasty patch-up
Date: 2023-09-12T15:00:09+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-09-12-googles-chrome-gets-caught-with-its-webp-down-offers-hasty-patch-up

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/12/chrome_browser_webp_exploit/){:target="_blank" rel="noopener"}

> Exploit observed in the wild as Mountain View pushes out updates Google has rushed out a fix for a vulnerability in its Chrome browser, noting that an exploit already exists in the wild.... [...]
