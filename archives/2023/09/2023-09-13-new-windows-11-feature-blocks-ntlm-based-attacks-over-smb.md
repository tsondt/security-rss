Title: New Windows 11 feature blocks NTLM-based attacks over SMB
Date: 2023-09-13T14:27:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-09-13-new-windows-11-feature-blocks-ntlm-based-attacks-over-smb

[Source](https://www.bleepingcomputer.com/news/security/new-windows-11-feature-blocks-ntlm-based-attacks-over-smb/){:target="_blank" rel="noopener"}

> Microsoft added a new security feature to Windows 11 that lets admins block NTLM over SMB to prevent pass-the-hash, NTLM relay, or password-cracking attacks. [...]
