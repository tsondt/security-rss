Title: Microsoft: China stole secret key that unlocked US govt email from crash debug dump
Date: 2023-09-06T22:59:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-06-microsoft-china-stole-secret-key-that-unlocked-us-govt-email-from-crash-debug-dump

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/06/microsoft_stolen_key_analysis/){:target="_blank" rel="noopener"}

> Mistakes were made, lessons learned, stuff now fixed, says Windows maker Remember that internal super-secret Microsoft security key that China stole and used to break into US government email accounts back in July?... [...]
