Title: Golf gear giant Callaway data breach exposes info of 1.1 million
Date: 2023-09-01T08:43:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-01-golf-gear-giant-callaway-data-breach-exposes-info-of-11-million

[Source](https://www.bleepingcomputer.com/news/security/golf-gear-giant-callaway-data-breach-exposes-info-of-11-million/){:target="_blank" rel="noopener"}

> Topgolf Callaway (Callaway) suffered a data breach at the start of August, which exposed the sensitive personal and account data of more than a million customers. [...]
