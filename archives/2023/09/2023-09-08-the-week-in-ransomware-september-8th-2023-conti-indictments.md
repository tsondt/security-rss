Title: The Week in Ransomware - September 8th 2023 - Conti Indictments
Date: 2023-09-08T17:45:45-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-09-08-the-week-in-ransomware-september-8th-2023-conti-indictments

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-september-8th-2023-conti-indictments/){:target="_blank" rel="noopener"}

> It started as a slow ransomware news week but slowly picked up pace with the Department of Justice announcing indictments on TrickBot and Conti operations members. [...]
