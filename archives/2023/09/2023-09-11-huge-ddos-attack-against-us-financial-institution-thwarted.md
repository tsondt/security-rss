Title: Huge DDoS attack against US financial institution thwarted
Date: 2023-09-11T18:46:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-11-huge-ddos-attack-against-us-financial-institution-thwarted

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/11/ddos_attack_against_us_bank/){:target="_blank" rel="noopener"}

> Akamai reckons traffic flood peaked at 55.1 million packets per second Akamai says it thwarted a major distributed denial-of-service (DDoS) attack aimed at a US bank that peaked at 55.1 million packets per second earlier this month.... [...]
