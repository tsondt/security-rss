Title: Discord is investigating cause of ‘You have been blocked’ errors
Date: 2023-09-29T08:21:21-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-29-discord-is-investigating-cause-of-you-have-been-blocked-errors

[Source](https://www.bleepingcomputer.com/news/security/discord-is-investigating-cause-of-you-have-been-blocked-errors/){:target="_blank" rel="noopener"}

> Many Discord users attempting to access the popular instant messaging and VoIP social platform today have been met with a scary "Sorry, you have been blocked" message. [...]
