Title: University of Sydney data breach impacts recent applicants
Date: 2023-09-03T11:13:15-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Education
Slug: 2023-09-03-university-of-sydney-data-breach-impacts-recent-applicants

[Source](https://www.bleepingcomputer.com/news/security/university-of-sydney-data-breach-impacts-recent-applicants/){:target="_blank" rel="noopener"}

> The University of Sydney (USYD) has announced it has suffered a data breach through a third-party service provider, exposing the personal data of recently applied and enrolled international applicants. [...]
