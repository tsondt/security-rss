Title: Friday Squid Blogging: Protecting Cephalopods in Medical Research
Date: 2023-09-29T21:07:58+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-09-29-friday-squid-blogging-protecting-cephalopods-in-medical-research

[Source](https://www.schneier.com/blog/archives/2023/09/friday-squid-blogging-protecting-cephalopods-in-medical-research.html){:target="_blank" rel="noopener"}

> From Nature : Cephalopods such as octopuses and squid could soon receive the same legal protection as mice and monkeys do when they are used in research. On 7 September, the US National Institutes of Health (NIH) asked for feedback on proposed guidelines that, for the first time in the United States, would require research projects involving cephalopods to be approved by an ethics board before receiving federal funding. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
