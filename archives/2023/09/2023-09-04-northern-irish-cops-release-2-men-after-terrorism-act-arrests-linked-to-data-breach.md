Title: Northern Irish cops release 2 men after Terrorism Act arrests linked to data breach
Date: 2023-09-04T12:33:13+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-09-04-northern-irish-cops-release-2-men-after-terrorism-act-arrests-linked-to-data-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/04/northern_irish_terrorism_arrests/){:target="_blank" rel="noopener"}

> Came in wake of the force publishing their own people's data in botched FoI Nearly four weeks after the Police Service of Northern Ireland (PSNI) published data on 10,000 employees in a botched response to a Freedom of Information request, another two men, aged 21 and 22, have been released on bail after being arrested under the Terrorism Act.... [...]
