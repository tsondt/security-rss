Title: ShinyHunters member pleads guilty to $6 million in data theft damages
Date: 2023-09-29T10:59:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-09-29-shinyhunters-member-pleads-guilty-to-6-million-in-data-theft-damages

[Source](https://www.bleepingcomputer.com/news/security/shinyhunters-member-pleads-guilty-to-6-million-in-data-theft-damages/){:target="_blank" rel="noopener"}

> Sebastien Raoult, a 22-year-old from France, has pleaded guilty in the U.S. District Court of Seattle to conspiracy to commit wire fraud and aggravated identity theft as part of his activities in the ShinyHunters hacking group. [...]
