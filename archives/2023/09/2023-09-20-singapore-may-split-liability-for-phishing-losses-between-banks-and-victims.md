Title: Singapore may split liability for phishing losses between banks and victims
Date: 2023-09-20T05:45:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-09-20-singapore-may-split-liability-for-phishing-losses-between-banks-and-victims

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/20/singapore_phishing_split_fraud/){:target="_blank" rel="noopener"}

> Won't someone please think of the banks? Singapore officials announced on Monday that next month they will deliver a consultation paper detailing a split liability scheme that will mean both consumers and banks are on the hook for financial losses flowing from scams.... [...]
