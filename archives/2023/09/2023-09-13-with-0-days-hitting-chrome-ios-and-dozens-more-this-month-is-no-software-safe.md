Title: With 0-days hitting Chrome, iOS, and dozens more this month, is no software safe?
Date: 2023-09-13T22:11:21+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;chrome;exploit;firefox;iOS;MacOS;vulnerability;zero-day
Slug: 2023-09-13-with-0-days-hitting-chrome-ios-and-dozens-more-this-month-is-no-software-safe

[Source](https://arstechnica.com/?p=1968268){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) End users, admins, and researchers better brace yourselves: The number of apps being patched for zero-day vulnerabilities has skyrocketed this month and is likely to get worse in the following weeks. People have worked overtime in recent weeks to patch a raft of vulnerabilities actively exploited in the wild, with offerings from Apple, Microsoft, Google, Mozilla, Adobe, and Cisco all being affected since the beginning of the month. The total number of zero-days in September so far is 10, compared with a total of 60 from January through August, according to security firm Mandiant. The [...]
