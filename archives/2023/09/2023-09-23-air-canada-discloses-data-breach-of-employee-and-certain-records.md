Title: Air Canada discloses data breach of employee and 'certain records'
Date: 2023-09-23T07:16:35-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-09-23-air-canada-discloses-data-breach-of-employee-and-certain-records

[Source](https://www.bleepingcomputer.com/news/security/air-canada-discloses-data-breach-of-employee-and-certain-records/){:target="_blank" rel="noopener"}

> Air Canada, the flag carrier and the largest airline of Canada, disclosed a cyber security incident this week in which hackers "briefly" obtained limited access to its internal systems. The incident resulted in the theft of a limited amount of personal information of some of its employees and "certain records." [...]
