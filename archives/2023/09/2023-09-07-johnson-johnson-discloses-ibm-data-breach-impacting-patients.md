Title: Johnson & Johnson discloses IBM data breach impacting patients
Date: 2023-09-07T11:02:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-09-07-johnson-johnson-discloses-ibm-data-breach-impacting-patients

[Source](https://www.bleepingcomputer.com/news/security/johnson-and-johnson-discloses-ibm-data-breach-impacting-patients/){:target="_blank" rel="noopener"}

> Johnson & Johnson Health Care Systems ("Janssen") has informed its CarePath customers that their sensitive information has been compromised in a third-party data breach involving IBM. [...]
