Title: Insurer fined $3M for exposing data of 650k clients for two years
Date: 2023-09-04T13:51:31-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-04-insurer-fined-3m-for-exposing-data-of-650k-clients-for-two-years

[Source](https://www.bleepingcomputer.com/news/security/insurer-fined-3m-for-exposing-data-of-650k-clients-for-two-years/){:target="_blank" rel="noopener"}

> The Swedish Authority for Privacy Protection (IMY) has fined Trygg-Hansa 35 million Swedish krona ($3,000,000) for exposing the sensitive data of hundreds of thousands of customers on its online portal. [...]
