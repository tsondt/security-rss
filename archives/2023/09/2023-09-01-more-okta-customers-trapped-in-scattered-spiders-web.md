Title: More Okta customers trapped in Scattered Spider's web
Date: 2023-09-01T19:15:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-01-more-okta-customers-trapped-in-scattered-spiders-web

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/01/okta_scattered_spider/){:target="_blank" rel="noopener"}

> Oktapus phishing campaign criminals are back in action Customers of cloudy identification vendor Okta are reporting social engineering attacks targeting their IT service desks in attempts to compromise user accounts with administrator permissions.... [...]
