Title: Iranian hackers breach defense orgs in password spray attacks
Date: 2023-09-14T12:30:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-09-14-iranian-hackers-breach-defense-orgs-in-password-spray-attacks

[Source](https://www.bleepingcomputer.com/news/security/iranian-hackers-breach-defense-orgs-in-password-spray-attacks/){:target="_blank" rel="noopener"}

> Microsoft says an Iranian-backed threat group has targeted thousands of organizations in the U.S. and worldwide in password spray attacks since February 2023. [...]
