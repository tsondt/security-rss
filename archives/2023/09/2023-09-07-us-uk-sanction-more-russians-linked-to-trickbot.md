Title: US, UK sanction more Russians linked to Trickbot
Date: 2023-09-07T22:44:43+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-07-us-uk-sanction-more-russians-linked-to-trickbot

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/07/trickbot_us_uk_sanctions/){:target="_blank" rel="noopener"}

> Top admin, HR managers, devs go on transatlantic deny-list The US and UK governments named and sanctioned 11 Russians said to be connected to the notorious Trickbot cybercrime crew this week.... [...]
