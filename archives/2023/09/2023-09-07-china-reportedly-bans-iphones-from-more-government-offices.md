Title: China reportedly bans iPhones from more government offices
Date: 2023-09-07T05:28:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-09-07-china-reportedly-bans-iphones-from-more-government-offices

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/07/china_government_reportedly_bans_iphones/){:target="_blank" rel="noopener"}

> So what? Smartphones are routinely restricted in, or excluded from, sensitive locations Analysis Chinese authorities have reportedly banned Apple's iPhones from some government offices.... [...]
