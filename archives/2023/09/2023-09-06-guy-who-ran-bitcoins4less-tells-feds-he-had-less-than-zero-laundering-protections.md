Title: Guy who ran Bitcoins4Less tells Feds he had less than zero laundering protections
Date: 2023-09-06T20:42:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-06-guy-who-ran-bitcoins4less-tells-feds-he-had-less-than-zero-laundering-protections

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/06/bitcoin_exchange_guilty/){:target="_blank" rel="noopener"}

> What? Yogurt Monster isn't really a legitimate customer's name?! A California man has admitted he failed to bake anti-money laundering protections into his cryptocurrency exchange, thus allowing scammers and drug traffickers to launder millions of dollars through the service.... [...]
