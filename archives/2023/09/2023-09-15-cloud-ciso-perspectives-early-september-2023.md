Title: Cloud CISO Perspectives: Early September 2023
Date: 2023-09-15T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2023-09-15-cloud-ciso-perspectives-early-september-2023

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-early-september-2023/){:target="_blank" rel="noopener"}

> Welcome to the first Cloud CISO Perspectives for September 2023. I’ll be talking about what I’m looking forward to at this year’s mWISE Conference, which opens its doors on Monday in Washington, D.C. It’s one of the most unique conferences a security professional or business leader can attend. You can register to attend the conference in-person or on our event livestream. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. aside_block [StructValue([(u'title', [...]
