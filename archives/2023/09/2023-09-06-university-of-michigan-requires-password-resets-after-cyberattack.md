Title: University of Michigan requires password resets after cyberattack
Date: 2023-09-06T16:43:14-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-09-06-university-of-michigan-requires-password-resets-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/university-of-michigan-requires-password-resets-after-cyberattack/){:target="_blank" rel="noopener"}

> The University of Michigan (UMICH) warned staff and students on Tuesday that they're required to reset their account passwords after a recent cyberattack. [...]
