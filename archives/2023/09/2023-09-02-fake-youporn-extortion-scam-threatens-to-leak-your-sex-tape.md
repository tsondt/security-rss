Title: Fake YouPorn extortion scam threatens to leak your sex tape
Date: 2023-09-02T10:12:06-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-09-02-fake-youporn-extortion-scam-threatens-to-leak-your-sex-tape

[Source](https://www.bleepingcomputer.com/news/security/fake-youporn-extortion-scam-threatens-to-leak-your-sex-tape/){:target="_blank" rel="noopener"}

> A new sextortion scam is making the rounds that pretends to be an email from the adult site YouPorn, warning that a sexually explicit video of you was uploaded to the site and suggesting you pay to have it taken down. [...]
