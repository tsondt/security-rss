Title: Google rolls out Privacy Sandbox to use Chrome browsing history for ads
Date: 2023-09-09T14:59:28-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Google;Security;Technology
Slug: 2023-09-09-google-rolls-out-privacy-sandbox-to-use-chrome-browsing-history-for-ads

[Source](https://www.bleepingcomputer.com/news/google/google-rolls-out-privacy-sandbox-to-use-chrome-browsing-history-for-ads/){:target="_blank" rel="noopener"}

> Google has started to roll out its new interest-based advertising platform called the Privacy Sandbox, shifting the tracking of user's interests from third-party cookies to the Chrome browser. [...]
