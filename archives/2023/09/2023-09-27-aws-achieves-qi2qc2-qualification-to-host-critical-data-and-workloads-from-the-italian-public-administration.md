Title: AWS achieves QI2/QC2 qualification to host critical data and workloads from the Italian Public Administration
Date: 2023-09-27T18:08:12+00:00
Author: Giuseppe Russo
Category: AWS Security
Tags: Announcements;Foundational (100);Public Sector;Security, Identity, & Compliance;AWS Digital Sovereignty Pledge;Security Blog
Slug: 2023-09-27-aws-achieves-qi2qc2-qualification-to-host-critical-data-and-workloads-from-the-italian-public-administration

[Source](https://aws.amazon.com/blogs/security/aws-achieves-qi2-qc2-qualification-to-host-critical-data-and-workloads-from-the-italian-public-administration/){:target="_blank" rel="noopener"}

> Amazon Web Service (AWS) is pleased to announce that it has achieved the QI2/QC2 qualification level, set out by the Italian National Cybersecurity Agency (ACN) in Determination No. 307/2022, for AWS cloud infrastructure and 130 AWS cloud services. The scope of this qualification level includes the management of Critical data and workloads for Italian public [...]
