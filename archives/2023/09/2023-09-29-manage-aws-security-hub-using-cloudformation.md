Title: Manage AWS Security Hub using CloudFormation
Date: 2023-09-29T16:35:57+00:00
Author: Priyank Ghedia
Category: AWS Security
Tags: Best Practices;Security, Identity, & Compliance;Technical How-to;AWS CloudFormation;AWS Security Hub;CloudFormation;Security Blog
Slug: 2023-09-29-manage-aws-security-hub-using-cloudformation

[Source](https://aws.amazon.com/blogs/security/manage-aws-security-hub-using-cloudformation/){:target="_blank" rel="noopener"}

> In this blog post, we show you how to enable and configure AWS Security Hub using the new Security Hub CloudFormation resources. Security Hub has expanded support for AWS CloudFormation by launching the updated Security Hub Hub resource and a new Standards resource for CloudFormation. The Hub resource can be used to enable Security Hub [...]
