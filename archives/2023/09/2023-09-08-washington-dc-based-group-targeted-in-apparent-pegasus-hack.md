Title: Washington DC-based group targeted in apparent Pegasus hack
Date: 2023-09-08T16:27:04+00:00
Author: Stephanie Kirchgaessner in Washington
Category: The Guardian
Tags: Washington DC;US news;Cybercrime;Surveillance;Malware;Data and computer security
Slug: 2023-09-08-washington-dc-based-group-targeted-in-apparent-pegasus-hack

[Source](https://www.theguardian.com/us-news/2023/sep/08/pegasus-hack-washington-dc-group-nso){:target="_blank" rel="noopener"}

> Citizen Lab discovers alleged attack using ‘zero-click exploit’ on individual employed by DC organization An individual employed by a Washington DC-based organization with international offices was targeted with powerful hacking software made by NSO Group, researchers have claimed, raising new concerns about the proliferation of spyware that can infect Apple devices. The alleged attack was discovered by researchers at the Citizen Lab at the Munk School at the University of Toronto while they were checking the individual’s device. Continue reading... [...]
