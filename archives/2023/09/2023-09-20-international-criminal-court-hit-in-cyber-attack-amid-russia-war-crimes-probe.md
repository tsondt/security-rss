Title: International Criminal Court hit in cyber-attack amid Russia war crimes probe
Date: 2023-09-20T19:46:03+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-20-international-criminal-court-hit-in-cyber-attack-amid-russia-war-crimes-probe

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/20/icc_hack/){:target="_blank" rel="noopener"}

> Right as judges issued warrants against Putin The International Criminal Court said crooks breached its IT systems last week, and that attack isn't over yet, with the ICC saying the "cybersecurity incident" is still ongoing.... [...]
