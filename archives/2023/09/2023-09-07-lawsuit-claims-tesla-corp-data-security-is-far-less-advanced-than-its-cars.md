Title: Lawsuit claims Tesla corp data security is far less advanced than its cars
Date: 2023-09-07T16:30:15+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-09-07-lawsuit-claims-tesla-corp-data-security-is-far-less-advanced-than-its-cars

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/07/tesla_leak_lawsuit/){:target="_blank" rel="noopener"}

> Sueball alleges company at fault after employee info leaked, including Musk's An ex-Tesla staffer has filed a proposed class action lawsuit that blames poor access control at the carmaker for a data leak, weeks after Tesla itself sued the alleged leakers, two former employees.... [...]
