Title: Backdoored firmware lets China state hackers control routers with “magic packets”
Date: 2023-09-27T19:04:36+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;backdoors;blacktech;firmware;routers
Slug: 2023-09-27-backdoored-firmware-lets-china-state-hackers-control-routers-with-magic-packets

[Source](https://arstechnica.com/?p=1971587){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Hackers backed by the Chinese government are planting malware into routers that provides long-lasting and undetectable backdoor access to the networks of multinational companies in the US and Japan, governments in both countries said Wednesday. The hacking group, tracked under names including BlackTech, Palmerworm, Temp.Overboard, Circuit Panda, and Radio Panda, has been operating since at least 2010, a joint advisory published by government entities in the US and Japan reported. The group has a history of targeting public organizations and private companies in the US and East Asia. The threat actor is somehow gaining administrator credentials [...]
