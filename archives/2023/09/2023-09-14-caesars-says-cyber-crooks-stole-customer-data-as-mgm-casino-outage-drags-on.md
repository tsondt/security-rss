Title: Caesars says cyber-crooks stole customer data as MGM casino outage drags on
Date: 2023-09-14T20:13:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-14-caesars-says-cyber-crooks-stole-customer-data-as-mgm-casino-outage-drags-on

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/14/caesars_mgm_hacks/){:target="_blank" rel="noopener"}

> Zero-days are so 2022. Why not just social engineer the help desk? Updated Casino giant Caesars Entertainment has confirmed miscreants stole a database containing customer info, including driver license and social security numbers for a "significant number" of its loyalty program members, in a social engineering attack earlier this month.... [...]
