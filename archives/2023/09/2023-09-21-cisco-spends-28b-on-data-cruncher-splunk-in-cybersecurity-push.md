Title: Cisco spends $28B on data cruncher Splunk in cybersecurity push
Date: 2023-09-21T14:55:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-09-21-cisco-spends-28b-on-data-cruncher-splunk-in-cybersecurity-push

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/21/cisco_acquires_data_crunching_firm/){:target="_blank" rel="noopener"}

> $157/share cash deal is the largest acquisition in networking titan's history Cisco is making its most expensive acquisition ever – by far - with an announcement it's buying data crunching software firm Splunk for $157 per share, or approximately $28 billion (£22.8b).... [...]
