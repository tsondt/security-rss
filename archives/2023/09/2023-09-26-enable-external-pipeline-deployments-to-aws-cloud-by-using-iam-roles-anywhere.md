Title: Enable external pipeline deployments to AWS Cloud by using IAM Roles Anywhere
Date: 2023-09-26T15:59:15+00:00
Author: Olivier Gaumond
Category: AWS Security
Tags: DevOps;Intermediate (200);Security, Identity, & Compliance;Technical How-to;AWS Identity and Access Management (IAM);Devops;IAM Roles Anywhere;Identity;Security;Security Blog;X.509 certificate
Slug: 2023-09-26-enable-external-pipeline-deployments-to-aws-cloud-by-using-iam-roles-anywhere

[Source](https://aws.amazon.com/blogs/security/enable-external-pipeline-deployments-to-aws-cloud-by-using-iam-roles-anywhere/){:target="_blank" rel="noopener"}

> Continuous integration and continuous delivery (CI/CD) services help customers automate deployments of infrastructure as code and software within the cloud. Common native Amazon Web Services (AWS) CI/CD services include AWS CodePipeline, AWS CodeBuild, and AWS CodeDeploy. You can also use third-party CI/CD services hosted outside the AWS Cloud, such as Jenkins, GitLab, and Azure DevOps, [...]
