Title: GitLab urges users to install security updates for critical pipeline flaw
Date: 2023-09-19T13:06:38-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-19-gitlab-urges-users-to-install-security-updates-for-critical-pipeline-flaw

[Source](https://www.bleepingcomputer.com/news/security/gitlab-urges-users-to-install-security-updates-for-critical-pipeline-flaw/){:target="_blank" rel="noopener"}

> GitLab has released security updates to address a critical severity vulnerability that allows attackers to run pipelines as other users via scheduled security scan policies. [...]
