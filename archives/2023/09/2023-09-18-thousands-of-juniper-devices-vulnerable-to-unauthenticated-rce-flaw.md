Title: Thousands of Juniper devices vulnerable to unauthenticated RCE flaw
Date: 2023-09-18T15:40:29-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-18-thousands-of-juniper-devices-vulnerable-to-unauthenticated-rce-flaw

[Source](https://www.bleepingcomputer.com/news/security/thousands-of-juniper-devices-vulnerable-to-unauthenticated-rce-flaw/){:target="_blank" rel="noopener"}

> An estimated 12,000 Juniper SRX firewalls and EX switches are vulnerable to a fileless remote code execution flaw that attackers can exploit without authentication. [...]
