Title: New ZeroFont phishing tricks Outlook into showing fake AV-scans
Date: 2023-09-26T17:32:25-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-26-new-zerofont-phishing-tricks-outlook-into-showing-fake-av-scans

[Source](https://www.bleepingcomputer.com/news/security/new-zerofont-phishing-tricks-outlook-into-showing-fake-av-scans/){:target="_blank" rel="noopener"}

> Hackers are utilizing a new trick of using zero-point fonts in emails to make malicious emails appear as safely scanned by security tools in Microsoft Outlook. [...]
