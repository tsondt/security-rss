Title: Feds' privacy panel backs renewing Feds' S. 702 spying powers — but with limits
Date: 2023-09-28T21:15:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-28-feds-privacy-panel-backs-renewing-feds-s-702-spying-powers-but-with-limits

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/28/section_702_spying/){:target="_blank" rel="noopener"}

> FBI agents ought to get spy court approval before reviewing US persons' chats, board reckons A privacy panel within the US government today narrowly recommended that Congress reauthorize the Feds' Section 702 spying powers — but with some stronger protections for US citizens only.... [...]
