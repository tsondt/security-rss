Title: Fake celebrity photo leak videos flood TikTok with Temu referral codes
Date: 2023-09-24T10:11:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-09-24-fake-celebrity-photo-leak-videos-flood-tiktok-with-temu-referral-codes

[Source](https://www.bleepingcomputer.com/news/security/fake-celebrity-photo-leak-videos-flood-tiktok-with-temu-referral-codes/){:target="_blank" rel="noopener"}

> TikTok is flooded with videos promoting fake nude celebrity photo leaks used to push referral rewards for the Temu online megastore. [...]
