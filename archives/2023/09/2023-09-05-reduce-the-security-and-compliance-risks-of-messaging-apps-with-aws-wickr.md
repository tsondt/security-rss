Title: Reduce the security and compliance risks of messaging apps with AWS Wickr
Date: 2023-09-05T16:35:37+00:00
Author: Anne Grahn
Category: AWS Security
Tags: Best Practices;Foundational (100);Security, Identity, & Compliance;AWS Wickr;Compliance;cryptography;Cybersecurity awareness;data privacy;Security;Security Blog
Slug: 2023-09-05-reduce-the-security-and-compliance-risks-of-messaging-apps-with-aws-wickr

[Source](https://aws.amazon.com/blogs/security/reduce-the-security-and-compliance-risks-of-messaging-apps-with-aws-wickr/){:target="_blank" rel="noopener"}

> Effective collaboration is central to business success, and employees today depend heavily on messaging tools. An estimated 3.09 billion mobile phone users access messaging applications (apps) to communicate, and this figure is projected to grow to 3.51 billion users in 2025. This post highlights the risks associated with messaging apps and describes how you can [...]
