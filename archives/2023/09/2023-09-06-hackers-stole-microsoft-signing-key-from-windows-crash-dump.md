Title: Hackers stole Microsoft signing key from Windows crash dump
Date: 2023-09-06T14:12:18-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-09-06-hackers-stole-microsoft-signing-key-from-windows-crash-dump

[Source](https://www.bleepingcomputer.com/news/microsoft/hackers-stole-microsoft-signing-key-from-windows-crash-dump/){:target="_blank" rel="noopener"}

> Microsoft says Storm-0558 Chinese hackers stole a signing key used to breach government email accounts from a Windows crash dump after compromising a Microsoft engineer's corporate account. [...]
