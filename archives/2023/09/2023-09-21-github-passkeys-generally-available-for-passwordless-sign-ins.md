Title: GitHub passkeys generally available for passwordless sign-ins
Date: 2023-09-21T14:59:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-09-21-github-passkeys-generally-available-for-passwordless-sign-ins

[Source](https://www.bleepingcomputer.com/news/security/github-passkeys-generally-available-for-passwordless-sign-ins/){:target="_blank" rel="noopener"}

> GitHub has made passkeys generally available across the platform today to secure accounts against phishing and allow passwordless logins for all users. [...]
