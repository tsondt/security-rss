Title: Attackers accessed UK military data through high-security fencing firm's Windows 7 rig
Date: 2023-09-04T15:25:09+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-09-04-attackers-accessed-uk-military-data-through-high-security-fencing-firms-windows-7-rig

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/04/zaun_breach_windows_7/){:target="_blank" rel="noopener"}

> Irony, not barbed wire, cuts the deepest The risk of running obsolete code and hardware was highlighted after attackers exfiltrated data from a UK supplier of high-security fencing for military bases. The initial entry point? A Windows 7 PC.... [...]
