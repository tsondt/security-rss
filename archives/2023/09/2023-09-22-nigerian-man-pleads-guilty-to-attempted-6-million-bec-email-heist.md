Title: Nigerian man pleads guilty to attempted $6 million BEC email heist
Date: 2023-09-22T15:24:53-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-09-22-nigerian-man-pleads-guilty-to-attempted-6-million-bec-email-heist

[Source](https://www.bleepingcomputer.com/news/security/nigerian-man-pleads-guilty-to-attempted-6-million-bec-email-heist/){:target="_blank" rel="noopener"}

> Kosi Goodness Simon-Ebo, a 29-year-old Nigerian national extradited from Canada to the United States last April, pleaded guilty to wire fraud and money laundering through business email compromise (BEC). [...]
