Title: Former CIO accuses Penn State of faking cybersecurity compliance
Date: 2023-09-18T20:15:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-09-18-former-cio-accuses-penn-state-of-faking-cybersecurity-compliance

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/18/cio_penn_state_security/){:target="_blank" rel="noopener"}

> Now-NASA boffin not impressed Last October, Pennsylvania State University (Penn State) was sued by a former chief information officer for allegedly falsifying government security compliance reports.... [...]
