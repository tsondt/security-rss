Title: T-Mobile US exposes some customer data – but don't call it a breach
Date: 2023-09-25T02:31:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-09-25-t-mobile-us-exposes-some-customer-data-but-dont-call-it-a-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/25/tmobile_exposes_some_customer_data/){:target="_blank" rel="noopener"}

> PLUS: Trojan hidden in PoC; cyber insurance surge; pig butchering's new cuts; and the week's critical vulns Infosec in brief T-Mobile US has had another bad week on the infosec front – this time stemming from a system glitch that exposed customer account data, followed by allegations of another breach the carrier denied.... [...]
