Title: Google Looker Studio abused in cryptocurrency phishing attacks
Date: 2023-09-07T15:07:27-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2023-09-07-google-looker-studio-abused-in-cryptocurrency-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/google-looker-studio-abused-in-cryptocurrency-phishing-attacks/){:target="_blank" rel="noopener"}

> Cybercriminals are abusing Google Looker Studio to create counterfeit cryptocurrency phishing websites that phish digital asset holders, leading to account takeovers and financial losses. [...]
