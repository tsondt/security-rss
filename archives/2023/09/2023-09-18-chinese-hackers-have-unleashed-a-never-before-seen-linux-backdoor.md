Title: Chinese hackers have unleashed a never-before-seen Linux backdoor
Date: 2023-09-18T23:25:04+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;advanced persistent threat;backdoor;Linux;malware
Slug: 2023-09-18-chinese-hackers-have-unleashed-a-never-before-seen-linux-backdoor

[Source](https://arstechnica.com/?p=1969201){:target="_blank" rel="noopener"}

> Enlarge Researchers have discovered a never-before-seen backdoor for Linux that’s being used by a threat actor linked to the Chinese government. The new backdoor originates from a Windows backdoor named Trochilus, which was first seen in 2015 by researchers from Arbor Networks, now known as Netscout. They said that Trochilus executed and ran only in memory, and the final payload never appeared on disks in most cases. That made the malware difficult to detect. Researchers from NHS Digital in the UK have said Trochilus was developed by APT10, an advanced persistent threat group linked to the Chinese government that also [...]
