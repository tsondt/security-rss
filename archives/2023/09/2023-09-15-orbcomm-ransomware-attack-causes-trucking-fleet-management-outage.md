Title: ORBCOMM ransomware attack causes trucking fleet management outage
Date: 2023-09-15T09:33:46-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-09-15-orbcomm-ransomware-attack-causes-trucking-fleet-management-outage

[Source](https://www.bleepingcomputer.com/news/security/orbcomm-ransomware-attack-causes-trucking-fleet-management-outage/){:target="_blank" rel="noopener"}

> Trucking and fleet management solutions provider ORBCOMM has confirmed that a ransomware attack is causing recent service outages that prevent trucking companies from managing their fleets. [...]
