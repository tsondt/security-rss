Title: Coffee Meets Bagel outage caused by cybercriminals deleting data and files
Date: 2023-09-06T16:01:07+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-09-06-coffee-meets-bagel-outage-caused-by-cybercriminals-deleting-data-and-files

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/06/coffee_meets_bagel_outage_caused/){:target="_blank" rel="noopener"}

> Did you potentially miss the love match of your life in week-long blackout? Nope, nobody could access it If you got snubbed by the object of your affections on dating app Coffee Meets Bagel (CMB) in late August, don't feel bad, the company says its systems were down due to cyber baddies.... [...]
