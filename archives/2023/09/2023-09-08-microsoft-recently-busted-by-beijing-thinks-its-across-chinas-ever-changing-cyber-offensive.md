Title: Microsoft, recently busted by Beijing, thinks it's across China's ever-changing cyber-offensive
Date: 2023-09-08T06:32:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-09-08-microsoft-recently-busted-by-beijing-thinks-its-across-chinas-ever-changing-cyber-offensive

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/08/microsoft_east_asia_infosec_report/){:target="_blank" rel="noopener"}

> Sometimes using AI to make hilariously wrong images that still drive social media engagement Microsoft, which earlier this week admitted not being able to detect a Chinese attack on its own infrastructure, has published a report [PDF] titled "Digital threats from East Asia increase in breadth and effectiveness." In the report, Redmond's Threat Intelligence group expounds on its fresh insight into evolving online aggressions from both China and North Korea.... [...]
