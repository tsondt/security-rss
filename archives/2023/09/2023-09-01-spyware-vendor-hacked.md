Title: Spyware Vendor Hacked
Date: 2023-09-01T11:07:01+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;activism;Brazil;hacking;spyware;vulnerabilities
Slug: 2023-09-01-spyware-vendor-hacked

[Source](https://www.schneier.com/blog/archives/2023/09/spyware-vendor-hacked.html){:target="_blank" rel="noopener"}

> A Brazilian spyware app vendor was hacked by activists: In an undated note seen by TechCrunch, the unnamed hackers described how they found and exploited several security vulnerabilities that allowed them to compromise WebDetetive’s servers and access its user databases. By exploiting other flaws in the spyware maker’s web dashboard—used by abusers to access the stolen phone data of their victims—the hackers said they enumerated and downloaded every dashboard record, including every customer’s email address. The hackers said that dashboard access also allowed them to delete victim devices from the spyware network altogether, effectively severing the connection at the server [...]
