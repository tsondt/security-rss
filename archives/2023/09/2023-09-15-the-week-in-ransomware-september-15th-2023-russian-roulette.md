Title: The Week in Ransomware - September 15th 2023 - Russian Roulette
Date: 2023-09-15T17:54:14-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-09-15-the-week-in-ransomware-september-15th-2023-russian-roulette

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-september-15th-2023-russian-roulette/){:target="_blank" rel="noopener"}

> This week's big news is the extortion attacks on the Caesars and MGM Las Vegas casino chains, with one having already paid the ransom and the other still facing operational disruptions. [...]
