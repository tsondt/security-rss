Title: Introducing the unified Chronicle Security Operations platform
Date: 2023-09-18T12:00:00+00:00
Author: Nimmy Reichenberg
Category: GCP Security
Tags: Security & Identity
Slug: 2023-09-18-introducing-the-unified-chronicle-security-operations-platform

[Source](https://cloud.google.com/blog/products/identity-security/introducing-the-unified-chronicle-security-operations-platform/){:target="_blank" rel="noopener"}

> At Google Cloud, our mission is to help organizations transform cybersecurity with frontline intelligence, expertise, and AI-powered innovation. Nowhere is this needed more than in security operations (SecOps), where understaffed and overwhelmed security teams struggle to defend against a threat landscape that is growing in volume and sophistication, often with tools that were designed in the pre-cloud era. We believe that successfully defending against modern threats requires modern thinking and modern solutions, which is why we’ve taken a fresh look at what threat detection, investigation, and response (TDIR) can be with Chronicle Security Operations. Following our Duet AI and threat-hunting [...]
