Title: The Hacker Tool to Get Personal Data from Credit Bureaus
Date: 2023-09-07T11:09:35+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybercrime;data collection;doxing;finance;law enforcement
Slug: 2023-09-07-the-hacker-tool-to-get-personal-data-from-credit-bureaus

[Source](https://www.schneier.com/blog/archives/2023/09/the-hacker-tool-to-get-personal-data-from-credit-bureaus.html){:target="_blank" rel="noopener"}

> The new site 404 Media has a good article on how hackers are cheaply getting personal information from credit bureaus: This is the result of a secret weapon criminals are selling access to online that appears to tap into an especially powerful set of data: the target’s credit header. This is personal information that the credit bureaus Experian, Equifax, and TransUnion have on most adults in America via their credit cards. Through a complex web of agreements and purchases, that data trickles down from the credit bureaus to other companies who offer it to debt collectors, insurance companies, and law [...]
