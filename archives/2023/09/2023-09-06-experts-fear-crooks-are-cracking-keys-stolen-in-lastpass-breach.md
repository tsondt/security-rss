Title: Experts Fear Crooks are Cracking Keys Stolen in LastPass Breach
Date: 2023-09-06T00:21:07+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;The Coming Storm;Web Fraud 2.0;1Password;AdBlock Plus;Ars Technica;Chainalysis;Karim Toubba;lastpass breach;MetaMask;Nicholas Weaver;Nick Bax;Plex;Taylor Monahan;Unciphered;Wladimir Palant
Slug: 2023-09-06-experts-fear-crooks-are-cracking-keys-stolen-in-lastpass-breach

[Source](https://krebsonsecurity.com/2023/09/experts-fear-crooks-are-cracking-keys-stolen-in-lastpass-breach/){:target="_blank" rel="noopener"}

> In November 2022, the password manager service LastPass disclosed a breach in which hackers stole password vaults containing both encrypted and plaintext data for more than 25 million users. Since then, a steady trickle of six-figure cryptocurrency heists targeting security-conscious people throughout the tech industry has led some security experts to conclude that crooks likely have succeeded at cracking open some of the stolen LastPass vaults. Taylor Monahan is founder and CEO of MetaMask, a popular software cryptocurrency wallet used to interact with the Ethereum blockchain. Since late December 2022, Monahan and other researchers have identified a highly reliable set [...]
