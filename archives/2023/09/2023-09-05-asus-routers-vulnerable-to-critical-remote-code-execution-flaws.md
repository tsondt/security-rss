Title: ASUS routers vulnerable to critical remote code execution flaws
Date: 2023-09-05T10:58:35-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-05-asus-routers-vulnerable-to-critical-remote-code-execution-flaws

[Source](https://www.bleepingcomputer.com/news/security/asus-routers-vulnerable-to-critical-remote-code-execution-flaws/){:target="_blank" rel="noopener"}

> Three critical-severity remote code execution vulnerabilities impact ASUS RT-AX55, RT-AX56U_V2, and RT-AC86U routers, potentially allowing threat actors to hijack devices if security updates are not installed. [...]
