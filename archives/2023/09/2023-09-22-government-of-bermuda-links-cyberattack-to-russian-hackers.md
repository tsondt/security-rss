Title: Government of Bermuda links cyberattack to Russian hackers
Date: 2023-09-22T13:30:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-09-22-government-of-bermuda-links-cyberattack-to-russian-hackers

[Source](https://www.bleepingcomputer.com/news/security/government-of-bermuda-links-cyberattack-to-russian-hackers/){:target="_blank" rel="noopener"}

> The Government of British overseas territory Bermuda has linked a cyberattack affecting all its departments' IT systems since Thursday to hackers based out of Russia. [...]
