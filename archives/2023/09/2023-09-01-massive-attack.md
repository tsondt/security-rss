Title: Massive attack
Date: 2023-09-01T13:34:15+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-09-01-massive-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/01/massive_attack/){:target="_blank" rel="noopener"}

> Defeating a DDoS swarm Webinar Any organization can lose service, revenue, and reputation as a result. If you are particularly unlucky, a DDoS attack can defenestrate your network defences. You may find yourself facing an cyber criminal who wants to take your business for everything it's got - not an attractive prospect in anybody's book.... [...]
