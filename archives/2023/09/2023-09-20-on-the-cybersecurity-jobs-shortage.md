Title: On the Cybersecurity Jobs Shortage
Date: 2023-09-20T11:06:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;employment
Slug: 2023-09-20-on-the-cybersecurity-jobs-shortage

[Source](https://www.schneier.com/blog/archives/2023/09/on-the-cybersecurity-jobs-shortage.html){:target="_blank" rel="noopener"}

> In April, Cybersecurity Ventures reported on extreme cybersecurity job shortage: Global cybersecurity job vacancies grew by 350 percent, from one million openings in 2013 to 3.5 million in 2021, according to Cybersecurity Ventures. The number of unfilled jobs leveled off in 2022, and remains at 3.5 million in 2023, with more than 750,000 of those positions in the U.S. Industry efforts to source new talent and tackle burnout continues, but we predict that the disparity between demand and supply will remain through at least 2025. The numbers never made sense to me, and Ben Rothke has dug in and explained [...]
