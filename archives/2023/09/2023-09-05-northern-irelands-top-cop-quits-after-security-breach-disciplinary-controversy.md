Title: Northern Ireland's top cop quits after security breach, disciplinary controversy
Date: 2023-09-05T11:45:56+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-09-05-northern-irelands-top-cop-quits-after-security-breach-disciplinary-controversy

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/05/northern_irish_police_chief_quits/){:target="_blank" rel="noopener"}

> Simon Byrne faced backlash over FoI blunder, plus claims officers were 'punished' to appease Sinn Féin Northern Ireland's police chief, Simon Byrne, resigned last night after an emergency meeting of the Policing Board amid discontent in the rank and file over a data breach that exposed serving officers' info, as well as news he was considering appealing a court ruling linked to the Troubles.... [...]
