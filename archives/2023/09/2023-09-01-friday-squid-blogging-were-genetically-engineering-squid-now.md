Title: Friday Squid Blogging: We’re Genetically Engineering Squid Now
Date: 2023-09-01T21:29:16+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-09-01-friday-squid-blogging-were-genetically-engineering-squid-now

[Source](https://www.schneier.com/blog/archives/2023/09/friday-squid-blogging-were-genetically-engineering-squid-now.html){:target="_blank" rel="noopener"}

> Is this a good idea? The transparent squid is a genetically altered version of the hummingbird bobtail squid, a species usually found in the tropical waters from Indonesia to China and Japan. It’s typically smaller than a thumb and shaped like a dumpling. And like other cephalopods, it has a relatively large and sophisticated brain. The see-through version is made possible by a gene editing technology called CRISPR, which became popular nearly a decade ago. Albertin and Rosenthal thought they might be able to use CRISPR to create a special squid for research. They focused on the hummingbird bobtail squid [...]
