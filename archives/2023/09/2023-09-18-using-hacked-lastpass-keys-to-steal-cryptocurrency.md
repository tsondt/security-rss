Title: Using Hacked LastPass Keys to Steal Cryptocurrency
Date: 2023-09-18T11:02:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptocurrency;operational security;Password Safe;passwords
Slug: 2023-09-18-using-hacked-lastpass-keys-to-steal-cryptocurrency

[Source](https://www.schneier.com/blog/archives/2023/09/using-hacked-lastpass-keys-to-steal-cryptocurrency.html){:target="_blank" rel="noopener"}

> Remember last November, when hackers broke into the network for LastPass—a password database—and stole password vaults with both encrypted and plaintext data for over 25 million users? Well, they’re now using that data break into crypto wallets and drain them: $35 million and counting, all going into a single wallet. That’s a really profitable hack. (It’s also bad opsec. The hackers need to move and launder all that money quickly.) Look, I know that online password databases are more convenient. But they’re also risky. This is why my Password Safe is local only. (I know this sounds like a commercial, [...]
