Title: Malice in the mail
Date: 2023-09-11T13:22:47+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-09-11-malice-in-the-mail

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/11/malice_in_the_mail/){:target="_blank" rel="noopener"}

> Defence against the dark arts of phishing Webinar Almost half of all losses to cybercrime come from Business Email Compromise (BEC), according to the FBI. It appears that even the most astute among us can fall foul of a cunningly crafted phishing email masquerading as a missive from a trusted source.... [...]
