Title: Manchester Police officers' data exposed in ransomware attack
Date: 2023-09-14T11:13:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-14-manchester-police-officers-data-exposed-in-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/manchester-police-officers-data-exposed-in-ransomware-attack/){:target="_blank" rel="noopener"}

> United Kingdom's Greater Manchester Police (GMP) said earlier today that some of its employees' personal information was impacted by a ransomware attack that hit a third-party supplier. [...]
