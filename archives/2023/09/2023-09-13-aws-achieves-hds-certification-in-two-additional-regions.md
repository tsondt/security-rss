Title: AWS achieves HDS certification in two additional Regions
Date: 2023-09-13T13:34:44+00:00
Author: Janice Leung
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Auditing;AWS security;HDS;HDS certification;Security;Security Blog
Slug: 2023-09-13-aws-achieves-hds-certification-in-two-additional-regions

[Source](https://aws.amazon.com/blogs/security/aws-achieves-hds-certification-in-two-additional-regions-2/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce that two additional AWS Regions—Middle East (UAE) and Europe (Zurich)—have been granted the Health Data Hosting (Hébergeur de Données de Santé, HDS) certification, increasing the scope to 20 global AWS Regions. The Agence Française de la Santé Numérique (ASIP Santé), the French governmental agency for health, introduced [...]
