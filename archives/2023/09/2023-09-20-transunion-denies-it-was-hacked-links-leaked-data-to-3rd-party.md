Title: TransUnion denies it was hacked, links leaked data to 3rd party
Date: 2023-09-20T15:34:05-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-09-20-transunion-denies-it-was-hacked-links-leaked-data-to-3rd-party

[Source](https://www.bleepingcomputer.com/news/security/transunion-denies-it-was-hacked-links-leaked-data-to-3rd-party/){:target="_blank" rel="noopener"}

> Credit reporting firm TransUnion has denied claims of a security breach after a threat actor known as USDoD leaked data allegedly stolen from the company's network. [...]
