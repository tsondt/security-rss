Title: Electoral Commission failed cybersecurity test in same year as hack
Date: 2023-09-05T09:57:21+00:00
Author: Dan Milmo
Category: The Guardian
Tags: Electoral Commission;Data protection;Data and computer security;Cybercrime;UK news;Politics
Slug: 2023-09-05-electoral-commission-failed-cybersecurity-test-in-same-year-as-hack

[Source](https://www.theguardian.com/politics/2023/sep/05/electoral-commission-failed-cybersecurity-test-in-same-year-as-hack){:target="_blank" rel="noopener"}

> UK election watchdog admits it did not pass assessment in 2021, when voter data security was breached The Electoral Commission has admitted it failed a cybersecurity test in the same year that hackers successfully attacked the organisation. The UK’s elections watchdog said it did not pass a Cyber Essentials test, a voluntary government-backed scheme that assesses an organisation’s readiness against cyber-attacks. Continue reading... [...]
