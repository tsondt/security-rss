Title: Russian infosec boss gets nine years for $100M insider-trading caper using stolen data
Date: 2023-09-08T00:57:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-09-08-russian-infosec-boss-gets-nine-years-for-100m-insider-trading-caper-using-stolen-data

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/08/russian_insider_training_prison/){:target="_blank" rel="noopener"}

> Confidential figures for Tesla, Snap, Roku, Avnet, others swiped and used to rack up millions in ill-gotten gains Vladislav Klyushin, the Russian owner of security penetration testing firm M-13, was jailed for nine years in the US on Thursday, for his involvement in a cyber-crime operation that stole top corporations' confidential financial information to make $93 million through insider trading.... [...]
