Title: New custom security posture controls and threat detections in Security Command Center
Date: 2023-09-27T16:53:47+00:00
Author: Tim Peacock
Category: GCP Security
Tags: Containers & Kubernetes;Security & Identity
Slug: 2023-09-27-new-custom-security-posture-controls-and-threat-detections-in-security-command-center

[Source](https://cloud-blog-transform.corp.google.com/blog/products/identity-security/new-custom-security-posture-controls-and-threat-detections-in-security-command-center/){:target="_blank" rel="noopener"}

> Security Command Center Premium, Google Cloud’s built-in security and risk management solution, provides out-of-the-box security controls for cloud posture management and threat detection. As our customers build more complex environments with different risk profiles, cloud security teams may need to monitor for specific conditions and threats not covered by Security Command Center’s default findings and detections. To help tailor detection and monitoring capabilities, Security Command Center now allows organizations to design their own customized security controls and threat detectors for their Google Cloud environment. For example, a security operator may need to detect if a key used to access a [...]
