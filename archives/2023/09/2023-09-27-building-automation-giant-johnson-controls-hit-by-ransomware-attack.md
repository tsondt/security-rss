Title: Building automation giant Johnson Controls hit by ransomware attack
Date: 2023-09-27T15:48:53-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-09-27-building-automation-giant-johnson-controls-hit-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/building-automation-giant-johnson-controls-hit-by-ransomware-attack/){:target="_blank" rel="noopener"}

> Johnson Controls International has suffered what is described as a massive ransomware attack that encrypted many of the company devices, including VMware ESXi servers, impacting the company's and its subsidiaries' operations. [...]
