Title: Google warns infoseccers: Beware of North Korean spies sliding into your DMs
Date: 2023-09-11T00:32:42+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-09-11-google-warns-infoseccers-beware-of-north-korean-spies-sliding-into-your-dms

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/11/infosec_roundup/){:target="_blank" rel="noopener"}

> ALSO: Verizon turns self in for reduced fine, malvertising comes to macOS, and this week's critical vulnerabilities In brief Watch out, cyber security researchers: Suspected North Korean-backed hackers are targeting members of the infosec community again, according to Google's Threat Analysis Group (TAG).... [...]
