Title: Associated Press warns that AP Stylebook data breach led to phishing attack
Date: 2023-09-10T13:22:02-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-09-10-associated-press-warns-that-ap-stylebook-data-breach-led-to-phishing-attack

[Source](https://www.bleepingcomputer.com/news/security/associated-press-warns-that-ap-stylebook-data-breach-led-to-phishing-attack/){:target="_blank" rel="noopener"}

> The Associated Press is warning of a data breach impacting AP Stylebook customers where the attackers used the stolen data to conduct targeted phishing attacks. [...]
