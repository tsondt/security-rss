Title: Claimants in Celsius crypto bankruptcy targeted in phishing attack
Date: 2023-09-19T19:38:43-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-09-19-claimants-in-celsius-crypto-bankruptcy-targeted-in-phishing-attack

[Source](https://www.bleepingcomputer.com/news/security/claimants-in-celsius-crypto-bankruptcy-targeted-in-phishing-attack/){:target="_blank" rel="noopener"}

> Scammers are impersonating the bankruptcy claim agent for crypto lender Celsius in phishing attacks that attempt to steal funds from cryptocurrency wallets. [...]
