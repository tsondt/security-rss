Title: GPUs from all major suppliers are vulnerable to new pixel-stealing attack
Date: 2023-09-26T17:40:53+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;GPUs;iframes;side channel
Slug: 2023-09-26-gpus-from-all-major-suppliers-are-vulnerable-to-new-pixel-stealing-attack

[Source](https://arstechnica.com/?p=1971213){:target="_blank" rel="noopener"}

> Enlarge GPUs from all six of the major suppliers are vulnerable to a newly discovered attack that allows malicious websites to read the usernames, passwords, and other sensitive visual data displayed by other websites, researchers have demonstrated in a paper published Tuesday. The cross-origin attack allows a malicious website from one domain—say, example.com—to effectively read the pixels displayed by a website from example.org, or another different domain. Attackers can then reconstruct them in a way that allows them to view the words or images displayed by the latter site. This leakage violates a critical security principle that forms one of [...]
