Title: Freecycle gives users the gift of a security breach notice
Date: 2023-09-05T14:24:13+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-09-05-freecycle-gives-users-the-gift-of-a-security-breach-notice

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/05/freecycle_becomes_the_latest_data/){:target="_blank" rel="noopener"}

> Change your passwords. And maybe give the recycling a miss this time Freecycle, the charity aimed at recycling detritus that would otherwise be headed for landfill, has become the latest organization to suffer at the hands of cyber attackers and admit to a breach.... [...]
