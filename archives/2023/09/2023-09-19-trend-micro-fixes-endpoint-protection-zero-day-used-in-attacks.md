Title: Trend Micro fixes endpoint protection zero-day used in attacks
Date: 2023-09-19T17:11:29-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-19-trend-micro-fixes-endpoint-protection-zero-day-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/trend-micro-fixes-endpoint-protection-zero-day-used-in-attacks/){:target="_blank" rel="noopener"}

> Trend Micro fixed a remote code execution zero-day vulnerability in the Trend Micro's Apex One endpoint protection solution that was actively exploited in attacks. [...]
