Title: Cisco security appliance 0-day is under attack by ransomware crooks
Date: 2023-09-08T19:50:26+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;cisco daptive Security Appliance Software;cisco Firepower Threat Defense;exploit;zero-day
Slug: 2023-09-08-cisco-security-appliance-0-day-is-under-attack-by-ransomware-crooks

[Source](https://arstechnica.com/?p=1966724){:target="_blank" rel="noopener"}

> Enlarge / Cisco Systems headquarters in San Jose, California, US, on Monday, Aug. 14, 2023. Cisco Systems Inc. is scheduled to release earnings figures on August 16. Photographer: David Paul Morris/Bloomberg via Getty Images Cisco on Thursday confirmed the existence of a currently unpatched zero-day vulnerability that hackers are exploiting to gain unauthorized access to two widely used security appliances it sells. The vulnerability resides in Cisco’s Adaptive Security Appliance Software and its Firepower Threat Defense, which are typically abbreviated as ASA and FTD. Cisco and researchers have known since last week that a ransomware crime syndicate called Akira was [...]
