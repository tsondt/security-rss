Title: How to implement cryptographic modules to secure private keys used with IAM Roles Anywhere
Date: 2023-09-20T19:55:17+00:00
Author: Edouard Kachelmann
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;authentication;authorization;AWS Identity and Access Management (IAM);Cryptographic library;Hardware security modules;IAM Roles Anywhere;Identity;PKCS#11;Security;Security Blog;X.509 certificate;YubiKey
Slug: 2023-09-20-how-to-implement-cryptographic-modules-to-secure-private-keys-used-with-iam-roles-anywhere

[Source](https://aws.amazon.com/blogs/security/how-to-implement-cryptographic-modules-to-secure-private-keys-used-with-iam-roles-anywhere/){:target="_blank" rel="noopener"}

> AWS Identity and Access Management (IAM) Roles Anywhere enables workloads that run outside of Amazon Web Services (AWS), such as servers, containers, and applications, to use X.509 digital certificates to obtain temporary AWS credentials and access AWS resources, the same way that you use IAM roles for workloads on AWS. Now, IAM Roles Anywhere allows [...]
