Title: Toyota says filled disk storage halted Japan-based factories
Date: 2023-09-06T09:47:59-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-06-toyota-says-filled-disk-storage-halted-japan-based-factories

[Source](https://www.bleepingcomputer.com/news/security/toyota-says-filled-disk-storage-halted-japan-based-factories/){:target="_blank" rel="noopener"}

> Toyota says a recent disruption of operations in Japan-based production plants was caused by its database servers running out of storage space. [...]
