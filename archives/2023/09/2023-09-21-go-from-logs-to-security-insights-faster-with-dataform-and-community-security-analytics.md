Title: Go from logs to security insights faster with Dataform and Community Security Analytics
Date: 2023-09-21T16:00:00+00:00
Author: Roy Arsan
Category: GCP Security
Tags: Security & Identity;Data Analytics
Slug: 2023-09-21-go-from-logs-to-security-insights-faster-with-dataform-and-community-security-analytics

[Source](https://cloud.google.com/blog/products/data-analytics/deploy-community-security-analytics-with-dataform/){:target="_blank" rel="noopener"}

> Making sense of security logs such as audit and network logs can be a challenge, given the volume, variety and velocity of valuable logs from your Google Cloud environment. To help accelerate the time to get security insights from your logs, the open-source Community Security Analytics (CSA) provides pre-built queries and reports you can use on top of Log Analytics powered by BigQuery. Customers and partners use CSA queries to help with data usage audits, threat detection and investigation, behavioral analytics and network forensics. It’s now easier than ever to deploy and operationalize CSA on BigQuery, with significant queries performance [...]
