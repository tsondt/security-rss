Title: Friday Squid Blogging: New Squid Species
Date: 2023-09-22T21:09:59+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-09-22-friday-squid-blogging-new-squid-species

[Source](https://www.schneier.com/blog/archives/2023/09/friday-squid-blogging-new-squid-species-2.html){:target="_blank" rel="noopener"}

> An ancient squid : New research on fossils has revealed that a vampire-like ancient squid haunted Earth’s oceans 165 million years ago. The study, published in June edition of the journal Papers in Palaeontology, says the creature had a bullet-shaped body with luminous organs, eight arms and sucker attachments. The discovery was made by scientists in France, who used modern imaging technique to analyse the previously discovered fossils. The ancient squid has been named Vampyrofugiens atramentum, which stands for the “fleeing vampire”. The researchers said that these features have never been recorded before. As usual, you can also use this [...]
