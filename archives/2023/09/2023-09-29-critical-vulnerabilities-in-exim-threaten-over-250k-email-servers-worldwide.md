Title: Critical vulnerabilities in Exim threaten over 250k email servers worldwide
Date: 2023-09-29T22:59:28+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;email;exim;exploits;rce;remote code execution;vulnerabilities
Slug: 2023-09-29-critical-vulnerabilities-in-exim-threaten-over-250k-email-servers-worldwide

[Source](https://arstechnica.com/?p=1972409){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Thousands of servers running the Exim mail transfer agent are vulnerable to potential attacks that exploit critical vulnerabilities, allowing remote execution of malicious code with little or no user interaction. The vulnerabilities were reported on Wednesday by Zero Day Initiative, but they largely escaped notice until Friday when they surfaced in a security mail list. Four of the six bugs allow for remote code execution and carry severity ratings of 7.5 to 9.8 out of a possible 10. Exim said it has made patches for three of the vulnerabilities available in a private repository. The status [...]
