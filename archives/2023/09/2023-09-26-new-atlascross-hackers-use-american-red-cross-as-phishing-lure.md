Title: New AtlasCross hackers use American Red Cross as phishing lure
Date: 2023-09-26T11:35:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-26-new-atlascross-hackers-use-american-red-cross-as-phishing-lure

[Source](https://www.bleepingcomputer.com/news/security/new-atlascross-hackers-use-american-red-cross-as-phishing-lure/){:target="_blank" rel="noopener"}

> A new APT hacking group named 'AtlasCross' targets organizations with phishing lures impersonating the American Red Cross to deliver backdoor malware. [...]
