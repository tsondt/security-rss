Title: The Signal Protocol used by 1+ billion people is getting a post-quantum makeover
Date: 2023-09-20T13:59:45+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Features;Security;encryption;post quantum cryptography;quantum computing;signal
Slug: 2023-09-20-the-signal-protocol-used-by-1-billion-people-is-getting-a-post-quantum-makeover

[Source](https://arstechnica.com/?p=1969529){:target="_blank" rel="noopener"}

> Enlarge (credit: Aurich Lawson | Getty Images) The Signal Foundation, maker of the Signal Protocol that encrypts messages sent by more than a billion people, has rolled out an update designed to prepare for a very real prospect that’s never far from the thoughts of just about every security engineer on the planet: the catastrophic fall of cryptographic protocols that secure some of the most sensitive secrets today. The Signal Protocol is a key ingredient in the Signal, Google RCS, and WhatsApp messengers, which collectively have more than 1 billion users. It’s the engine that provides end-to-end encryption, meaning messages [...]
