Title: Microsoft worker accidentally exposes 38TB of sensitive data in GitHub blunder
Date: 2023-09-18T18:03:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-18-microsoft-worker-accidentally-exposes-38tb-of-sensitive-data-in-github-blunder

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/18/more_microsoft_token_trouble/){:target="_blank" rel="noopener"}

> Included secrets, private keys, passwords, 30,000+ internal Teams messages A Microsoft employee accidentally exposed 38 terabytes of private data while publishing a bucket of open-source AI training data on GitHub, according to Wiz security researchers who spotted the leaky account and reported it to the Windows giant.... [...]
