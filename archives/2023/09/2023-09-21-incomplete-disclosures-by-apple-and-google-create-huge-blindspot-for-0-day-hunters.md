Title: Incomplete disclosures by Apple and Google create “huge blindspot” for 0-day hunters
Date: 2023-09-21T22:19:58+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;apple;cves;google;libwebp;WebP;zero-day
Slug: 2023-09-21-incomplete-disclosures-by-apple-and-google-create-huge-blindspot-for-0-day-hunters

[Source](https://arstechnica.com/?p=1970341){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Incomplete information included in recent disclosures by Apple and Google reporting critical zero-day vulnerabilities under active exploitation in their products has created a “huge blindspot” that’s causing a large number of offerings from other developers to go unpatched, researchers said Thursday. Two weeks ago, Apple reported that threat actors were actively exploiting a critical vulnerability in iOS so they could install espionage spyware known as Pegasus. The attacks used a zero-click method, meaning they required no interaction on the part of targets. Simply receiving a call or text on an iPhone was enough to become infected [...]
