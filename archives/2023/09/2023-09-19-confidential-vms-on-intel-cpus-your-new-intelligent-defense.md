Title: Confidential VMs on Intel CPUs: Your new intelligent defense
Date: 2023-09-19T16:00:00+00:00
Author: Sam Lugani
Category: GCP Security
Tags: Security & Identity
Slug: 2023-09-19-confidential-vms-on-intel-cpus-your-new-intelligent-defense

[Source](https://cloud.google.com/blog/products/identity-security/confidential-vms-on-intel-cpus-your-datas-new-intelligent-defense/){:target="_blank" rel="noopener"}

> For organizations who want to bring and process their most sensitive compute workloads in the cloud without any code changes, we offer Confidential virtual machines (VMs) that leverage the latest hardware-based security technology. Through our partnership with Intel, we are extending our Confidential VMs on the new C3 machines series that uses 4th Gen Intel Xeon Scalable CPUs and leverages Intel Trust Domain Extensions (Intel TDX) technology. Available now in private preview, these next-generation Confidential VMs expand Google’s Confidential Computing product portfolio to include different hardware vendors, giving customers more choice when it comes to cryptographically isolating their workloads from [...]
