Title: Hacker gains admin control of Sourcegraph and gives free access to the masses
Date: 2023-09-01T18:17:54+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;breaches;hacking;personal information;sourcegraph
Slug: 2023-09-01-hacker-gains-admin-control-of-sourcegraph-and-gives-free-access-to-the-masses

[Source](https://arstechnica.com/?p=1965211){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) An unknown hacker gained administrative control of Sourcegraph, an AI-driven service used by developers at Uber, Reddit, Dropbox, and other companies, and used it to provide free access to resources that normally would have required payment. In the process, the hacker(s) may have accessed personal information belonging to Sourcegraph users, Diego Comas, Sourcegraph’s head of security, said in a post on Wednesday. For paid users, the information exposed included license keys and the names and email addresses of license key holders. For non-paying users, it was limited to email addresses associated with their accounts. Private code, [...]
