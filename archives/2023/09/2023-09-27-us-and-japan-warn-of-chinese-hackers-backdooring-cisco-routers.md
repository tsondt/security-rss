Title: US and Japan warn of Chinese hackers backdooring Cisco routers
Date: 2023-09-27T11:51:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-27-us-and-japan-warn-of-chinese-hackers-backdooring-cisco-routers

[Source](https://www.bleepingcomputer.com/news/security/us-and-japan-warn-of-chinese-hackers-backdooring-cisco-routers/){:target="_blank" rel="noopener"}

> A joint cybersecurity advisory by the FBI, NSA, CISA, and the Japanese NISC (cybersecurity) and NPA (police) sheds light on the techniques the Chinese threat actors known as BlackTech use to attack Japanese and U.S. organizations. [...]
