Title: Tsunami watch
Date: 2023-09-04T14:08:40+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-09-04-tsunami-watch

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/04/tsunami_watch/){:target="_blank" rel="noopener"}

> Mitigating the threat of bot-driven DDoS attacks Webinar It's sometimes easy to be lulled into a sense of false security and imagine that your organization or business will not become a target of highly professional cybercriminals, hacktivists and even nation-state actors. But the threat posed by DDoS attacks is very much on the rise.... [...]
