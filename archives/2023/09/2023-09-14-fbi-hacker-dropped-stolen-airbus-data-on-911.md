Title: FBI Hacker Dropped Stolen Airbus Data on 9/11
Date: 2023-09-14T00:22:05+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;The Coming Storm;Airbus;BreachForums;fbi;Genesis Market;Hudson Rock;InfraGard;microsoft;RedLine;USDoD
Slug: 2023-09-14-fbi-hacker-dropped-stolen-airbus-data-on-911

[Source](https://krebsonsecurity.com/2023/09/fbi-hacker-dropped-stolen-airbus-data-on-9-11/){:target="_blank" rel="noopener"}

> In December 2022, KrebsOnSecurity broke the news that a cybercriminal using the handle “ USDoD ” had infiltrated the FBI ‘s vetted information sharing network InfraGard, and was selling the contact information for all 80,000 members. The FBI responded by reverifying all InfraGard members and by seizing the cybercrime forum where the data was being sold. But on Sept. 11, 2023, USDoD resurfaced after a lengthy absence to leak sensitive employee data stolen from the aerospace giant Airbus, while promising to visit the same treatment on top U.S. defense contractors. USDoD’s avatar used to be the seal of the U.S. [...]
