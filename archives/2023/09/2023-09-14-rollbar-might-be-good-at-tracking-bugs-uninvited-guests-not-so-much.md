Title: Rollbar might be good at tracking bugs, uninvited guests not so much
Date: 2023-09-14T15:00:15+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-09-14-rollbar-might-be-good-at-tracking-bugs-uninvited-guests-not-so-much

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/14/rollbar_breach/){:target="_blank" rel="noopener"}

> Company noticed data warehouse break-in via compromised account a month later Cloud-based bug tracking and monitoring platform Rollbar has warned users that attackers have rifled through their data.... [...]
