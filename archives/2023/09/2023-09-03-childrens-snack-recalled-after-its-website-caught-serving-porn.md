Title: Children's snack recalled after its website caught serving porn
Date: 2023-09-03T06:06:18-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-09-03-childrens-snack-recalled-after-its-website-caught-serving-porn

[Source](https://www.bleepingcomputer.com/news/security/childrens-snack-recalled-after-its-website-caught-serving-porn/){:target="_blank" rel="noopener"}

> Supermarket chain Lidl has been recalling four types of PAW Patrol-themed snacks across the UK. Except, the reason for the recall has got nothing to do with food contents, but the website listed on the snack's packaging serving porn. [...]
