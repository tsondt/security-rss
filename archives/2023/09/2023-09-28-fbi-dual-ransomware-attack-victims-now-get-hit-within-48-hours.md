Title: FBI: Dual ransomware attack victims now get hit within 48 hours
Date: 2023-09-28T14:14:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-09-28-fbi-dual-ransomware-attack-victims-now-get-hit-within-48-hours

[Source](https://www.bleepingcomputer.com/news/security/fbi-dual-ransomware-attack-victims-now-get-hit-within-48-hours/){:target="_blank" rel="noopener"}

> The FBI has warned about a new trend in ransomware attacks where multiple strains are deployed on victims' networks to encrypt systems in under two days. [...]
