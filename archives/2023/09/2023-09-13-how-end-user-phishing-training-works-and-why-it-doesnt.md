Title: How end-user phishing training works (and why it doesn’t)
Date: 2023-09-13T10:02:01-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-09-13-how-end-user-phishing-training-works-and-why-it-doesnt

[Source](https://www.bleepingcomputer.com/news/security/how-end-user-phishing-training-works-and-why-it-doesnt/){:target="_blank" rel="noopener"}

> Training end-users to spot phishing has its benefits, but it's clear to see organizations as a whole have failed to make a dent in phishing attacks. Learn more from Specops Software on how phishers use social engineering to exploit human psychology. [...]
