Title: How to enforce DNS name constraints in AWS Private CA
Date: 2023-09-06T13:40:25+00:00
Author: Isaiah Schisler
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;AWS Certificate Manager;AWS Private CA;AWS Private Certificate Authority;certificate management;certificates;Security Blog
Slug: 2023-09-06-how-to-enforce-dns-name-constraints-in-aws-private-ca

[Source](https://aws.amazon.com/blogs/security/how-to-enforce-dns-name-constraints-in-aws-private-ca/){:target="_blank" rel="noopener"}

> In March 2022, AWS announced support for custom certificate extensions, including name constraints, using AWS Certificate Manager (ACM) Private Certificate Authority (CA). Defining DNS name constraints with your subordinate CA can help establish guardrails to improve public key infrastructure (PKI) security and mitigate certificate misuse. For example, you can set a DNS name constraint that [...]
