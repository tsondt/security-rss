Title: Get the full benefits of IMDSv2 and disable IMDSv1 across your AWS infrastructure
Date: 2023-09-28T19:38:56+00:00
Author: Saju Sivaji
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;Amazon EC2;AWS Organizations;Data perimeters;EC2;IAM;IMDS;IMDSV1;IMDSV2;Instance profiles;Instance Role;Network Perimeter;Resource Policy;SCP;Security;Security Blog;service control policy;SSRF
Slug: 2023-09-28-get-the-full-benefits-of-imdsv2-and-disable-imdsv1-across-your-aws-infrastructure

[Source](https://aws.amazon.com/blogs/security/get-the-full-benefits-of-imdsv2-and-disable-imdsv1-across-your-aws-infrastructure/){:target="_blank" rel="noopener"}

> The Amazon Elastic Compute Cloud (Amazon EC2) Instance Metadata Service (IMDS) helps customers build secure and scalable applications. IMDS solves a security challenge for cloud users by providing access to temporary and frequently-rotated credentials, and by removing the need to hardcode or distribute sensitive credentials to instances manually or programmatically. The Instance Metadata Service Version 2 (IMDSv2) [...]
