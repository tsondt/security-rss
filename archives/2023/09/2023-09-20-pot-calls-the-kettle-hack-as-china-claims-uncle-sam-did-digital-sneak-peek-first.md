Title: Pot calls the kettle hack as China claims Uncle Sam did digital sneak peek first
Date: 2023-09-20T17:06:10+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2023-09-20-pot-calls-the-kettle-hack-as-china-claims-uncle-sam-did-digital-sneak-peek-first

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/20/huawei_china_claims/){:target="_blank" rel="noopener"}

> Beijing accuses US of breaking into Huawei servers in 2009 The ongoing face-off between Washington and Beijing over technology and security issues has taken a new twist, with China accusing the US of hacking into the servers of Huawei in 2009 and conducting other cyber-attacks to steal critical data.... [...]
