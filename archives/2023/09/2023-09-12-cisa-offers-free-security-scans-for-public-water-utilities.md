Title: CISA offers free security scans for public water utilities
Date: 2023-09-12T13:02:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-12-cisa-offers-free-security-scans-for-public-water-utilities

[Source](https://www.bleepingcomputer.com/news/security/cisa-offers-free-security-scans-for-public-water-utilities/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity & Infrastructure Security Agency (CISA) has announced it is offering free security scans for critical infrastructure facilities, such as water utilities, to help protect these crucial units from hacker attacks. [...]
