Title: Cloudflare DDoS protections ironically bypassed using Cloudflare
Date: 2023-09-30T10:16:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-30-cloudflare-ddos-protections-ironically-bypassed-using-cloudflare

[Source](https://www.bleepingcomputer.com/news/security/cloudflare-ddos-protections-ironically-bypassed-using-cloudflare/){:target="_blank" rel="noopener"}

> Cloudflare's Firewall and DDoS prevention can be bypassed through a specific attack process that leverages logic flaws in cross-tenant security controls. [...]
