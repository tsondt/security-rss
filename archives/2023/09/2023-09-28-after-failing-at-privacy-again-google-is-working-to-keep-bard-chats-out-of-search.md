Title: After failing at privacy, again, Google is working to keep Bard chats out of Search
Date: 2023-09-28T07:32:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-09-28-after-failing-at-privacy-again-google-is-working-to-keep-bard-chats-out-of-search

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/28/google_bard_chat/){:target="_blank" rel="noopener"}

> The URLs needed to share chat histories have been indexed. Of course Google's Bard chatbot is currently being re-educated to better understand privacy.... [...]
