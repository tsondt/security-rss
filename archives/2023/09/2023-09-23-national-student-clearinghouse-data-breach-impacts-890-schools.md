Title: National Student Clearinghouse data breach impacts 890 schools
Date: 2023-09-23T10:04:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Education
Slug: 2023-09-23-national-student-clearinghouse-data-breach-impacts-890-schools

[Source](https://www.bleepingcomputer.com/news/security/national-student-clearinghouse-data-breach-impacts-890-schools/){:target="_blank" rel="noopener"}

> U.S. educational nonprofit National Student Clearinghouse has disclosed a data breach affecting 890 schools using its services across the United States. [...]
