Title: BORN Ontario child registry data breach affects 3.4 million people
Date: 2023-09-25T13:31:41-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-09-25-born-ontario-child-registry-data-breach-affects-34-million-people

[Source](https://www.bleepingcomputer.com/news/security/born-ontario-child-registry-data-breach-affects-34-million-people/){:target="_blank" rel="noopener"}

> The Better Outcomes Registry & Network (BORN), a healthcare organization funded by the government of Ontario, has announced that it is among the victims of Clop ransomware's MOVEit hacking spree. [...]
