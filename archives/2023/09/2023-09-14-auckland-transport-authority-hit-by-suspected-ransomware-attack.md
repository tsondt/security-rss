Title: Auckland transport authority hit by suspected ransomware attack
Date: 2023-09-14T16:09:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-14-auckland-transport-authority-hit-by-suspected-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/auckland-transport-authority-hit-by-suspected-ransomware-attack/){:target="_blank" rel="noopener"}

> The Auckland Transport (AT) transportation authority in New Zealand is dealing with a widespread outage caused by a cyber incident, impacting a wide range of customer services. [...]
