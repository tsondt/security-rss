Title: Flipper Zero can be used to launch iOS Bluetooth spam attacks
Date: 2023-09-06T16:12:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple
Slug: 2023-09-06-flipper-zero-can-be-used-to-launch-ios-bluetooth-spam-attacks

[Source](https://www.bleepingcomputer.com/news/security/flipper-zero-can-be-used-to-launch-ios-bluetooth-spam-attacks/){:target="_blank" rel="noopener"}

> The Flipper Zero portable wireless pen-testing and hacking tool can be used to aggressively spam Bluetooth connection messages at Apple iOS devices, such as iPhones and iPads. [...]
