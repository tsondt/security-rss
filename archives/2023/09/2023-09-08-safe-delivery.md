Title: Safe delivery
Date: 2023-09-08T13:34:09+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-09-08-safe-delivery

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/08/safe_delivery/){:target="_blank" rel="noopener"}

> How to protect organizations from Business Email Compromise Webinar It is a stratospheric number of emails pinging around the globe and the sheer volume offers a seductively lucrative phishing opportunity to the legion of bad actors out there.... [...]
