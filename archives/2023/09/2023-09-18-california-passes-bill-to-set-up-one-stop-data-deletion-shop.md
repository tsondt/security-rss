Title: California passes bill to set up one-stop data deletion shop
Date: 2023-09-18T12:45:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-09-18-california-passes-bill-to-set-up-one-stop-data-deletion-shop

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/18/california_passes_bill_to_set/){:target="_blank" rel="noopener"}

> Also, LockBit gets a new second stringer, AirTag owners find yet another illicit use, and this week's critical vulns Infosec in brief Californians may be on their way to the nation's first "do not broker" list with the passage of a bill that would create a one-stop service for residents of the Golden State who want to opt out of being tracked by data brokers.... [...]
