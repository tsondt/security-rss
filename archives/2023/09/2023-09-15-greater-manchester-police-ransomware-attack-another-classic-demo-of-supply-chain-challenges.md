Title: Greater Manchester Police ransomware attack another classic demo of supply chain challenges
Date: 2023-09-15T09:45:28+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-09-15-greater-manchester-police-ransomware-attack-another-classic-demo-of-supply-chain-challenges

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/15/greater_manchester_police_breach_demonstrates/){:target="_blank" rel="noopener"}

> Are you the weakest link? The UK's Greater Manchester Police (GMP) has admitted that crooks have got their mitts on some of its data after a third-party supplier responsible for ID badges was attacked.... [...]
