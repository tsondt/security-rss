Title: Who’s Behind the 8Base Ransomware Website?
Date: 2023-09-19T02:12:53+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Ransomware;8Base Ransomware;@htmalgae;Andrei Kolev;Gitlab;JCube Group;VMware
Slug: 2023-09-19-whos-behind-the-8base-ransomware-website

[Source](https://krebsonsecurity.com/2023/09/whos-behind-the-8base-ransomware-website/){:target="_blank" rel="noopener"}

> The victim shaming website operated by the cybercriminals behind 8Base — currently one of the more active ransomware groups — was until earlier today leaking quite a bit of information that the crime group probably did not intend to be made public. The leaked data suggests that at least some of website’s code was written by a 36-year-old programmer residing in the capital city of Moldova. The 8Base ransomware group’s victim shaming website on the darknet. 8Base maintains a darknet website that is only reachable via Tor, a freely available global anonymity network. The site lists hundreds of victim organizations [...]
