Title: Cryptojackers spread their nets to capture more than just EC2
Date: 2023-09-18T11:15:12+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-09-18-cryptojackers-spread-their-nets-to-capture-more-than-just-ec2

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/18/cryptojackers_spread_their_nets_to/){:target="_blank" rel="noopener"}

> AMBERSQUID operation takes AWS's paths less travelled in search of compute As cloud native computing continues to gain popularity, so does the risk posed by criminals seeking to exploit the unwary. One newly spotted method targets services on the AWS platform, but not necessarily the ones you might think.... [...]
