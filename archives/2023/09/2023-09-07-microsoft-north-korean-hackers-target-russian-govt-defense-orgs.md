Title: Microsoft: North Korean hackers target Russian govt, defense orgs
Date: 2023-09-07T14:24:05-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Government;Microsoft
Slug: 2023-09-07-microsoft-north-korean-hackers-target-russian-govt-defense-orgs

[Source](https://www.bleepingcomputer.com/news/security/microsoft-north-korean-hackers-target-russian-govt-defense-orgs/){:target="_blank" rel="noopener"}

> Microsoft says North Korean hacking groups have breached multiple Russian government and defense targets since the start of the year. [...]
