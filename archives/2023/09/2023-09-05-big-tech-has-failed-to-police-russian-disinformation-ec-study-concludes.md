Title: Big Tech has failed to police Russian disinformation, EC study concludes
Date: 2023-09-05T17:45:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-09-05-big-tech-has-failed-to-police-russian-disinformation-ec-study-concludes

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/05/big_tech_vlops/){:target="_blank" rel="noopener"}

> In Putin's Russia, the planet hacks you The power of the EU's Digital Services Act (DSA) to actually police the world's very large online platforms (VLOPs) has been tested in a new study focused on Russian social media disinformation.... [...]
