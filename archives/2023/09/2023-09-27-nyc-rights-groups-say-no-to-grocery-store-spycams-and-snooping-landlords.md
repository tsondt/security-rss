Title: NYC rights groups say no to grocery store spycams and snooping landlords
Date: 2023-09-27T16:30:08+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-09-27-nyc-rights-groups-say-no-to-grocery-store-spycams-and-snooping-landlords

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/27/nyc_biometrics_ban/){:target="_blank" rel="noopener"}

> Letter to City Council supports measures to ban biometric tech from public spaces "New Yorkers should not be forced to accept biometric surveillance as part of simple activities like buying groceries or taking their kids to a baseball game," more than 30 civil and digital rights organizations said yesterday in a letter backing new privacy laws in the city.... [...]
