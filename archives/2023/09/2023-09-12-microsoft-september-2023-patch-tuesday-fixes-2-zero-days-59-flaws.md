Title: Microsoft September 2023 Patch Tuesday fixes 2 zero-days, 59 flaws
Date: 2023-09-12T14:11:24-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-09-12-microsoft-september-2023-patch-tuesday-fixes-2-zero-days-59-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-september-2023-patch-tuesday-fixes-2-zero-days-59-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's September 2023 Patch Tuesday, with security updates for 59 flaws, including two actively exploited zero-day vulnerabilities. [...]
