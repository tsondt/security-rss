Title: Automatically detect and block low-volume network floods
Date: 2023-09-07T19:01:17+00:00
Author: Bryan Van Hook
Category: AWS Security
Tags: Advanced (300);Best Practices;Networking & Content Delivery;Security, Identity, & Compliance;Technical How-to;Amazon VPC;DDoS;Networking;Security;Security Blog
Slug: 2023-09-07-automatically-detect-and-block-low-volume-network-floods

[Source](https://aws.amazon.com/blogs/security/automatically-detect-and-block-low-volume-network-floods/){:target="_blank" rel="noopener"}

> In this blog post, I show you how to deploy a solution that uses AWS Lambda to automatically manage the lifecycle of Amazon VPC Network Access Control List (ACL) rules to mitigate network floods detected using Amazon CloudWatch Logs Insights and Amazon Timestream. Application teams should consider the impact unexpected traffic floods can have on an application’s availability. Internet-facing applications can [...]
