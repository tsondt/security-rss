Title: Hackers steal $53 million worth of cryptocurrency from CoinEx
Date: 2023-09-13T10:11:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-09-13-hackers-steal-53-million-worth-of-cryptocurrency-from-coinex

[Source](https://www.bleepingcomputer.com/news/security/hackers-steal-53-million-worth-of-cryptocurrency-from-coinex/){:target="_blank" rel="noopener"}

> Global cryptocurrency exchange CoinEX announced that someone hacked its hot wallets and stole large amounts of digital assets that were used to support the platform's operations. [...]
