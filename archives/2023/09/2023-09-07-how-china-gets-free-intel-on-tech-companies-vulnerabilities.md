Title: How China gets free intel on tech companies’ vulnerabilities
Date: 2023-09-07T13:14:52+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Security;Uncategorized;china;hacking;phishing;security disclosures;syndication
Slug: 2023-09-07-how-china-gets-free-intel-on-tech-companies-vulnerabilities

[Source](https://arstechnica.com/?p=1966082){:target="_blank" rel="noopener"}

> Enlarge (credit: Wired staff; Getty Images) For state-sponsored hacking operations, unpatched vulnerabilities are valuable ammunition. Intelligence agencies and militaries seize on hackable bugs when they're revealed—exploiting them to carry out their campaigns of espionage or cyberwar—or spend millions to dig up new ones or to buy them in secret from the hacker gray market. But for the past two years, China has added another approach to obtaining information about those vulnerabilities: a law that simply demands that any network technology business operating in the country hand it over. When tech companies learn of a hackable flaw in their products, they’re [...]
