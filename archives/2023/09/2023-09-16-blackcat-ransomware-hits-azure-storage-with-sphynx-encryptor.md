Title: BlackCat ransomware hits Azure Storage with Sphynx encryptor
Date: 2023-09-16T10:11:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-09-16-blackcat-ransomware-hits-azure-storage-with-sphynx-encryptor

[Source](https://www.bleepingcomputer.com/news/security/blackcat-ransomware-hits-azure-storage-with-sphynx-encryptor/){:target="_blank" rel="noopener"}

> The BlackCat (ALPHV) ransomware gang now uses stolen Microsoft accounts and the recently spotted Sphynx encryptor to encrypt targets' Azure cloud storage. [...]
