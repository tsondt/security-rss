Title: SickKids impacted by BORN Ontario data breach that hit 3.4 million
Date: 2023-09-26T05:20:00-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-09-26-sickkids-impacted-by-born-ontario-data-breach-that-hit-34-million

[Source](https://www.bleepingcomputer.com/news/security/sickkids-impacted-by-born-ontario-data-breach-that-hit-34-million/){:target="_blank" rel="noopener"}

> The Hospital for Sick Children, more commonly known as SickKids, is among healthcare providers that were impacted by the recent breach at BORN Ontario. The top Canadian pediatric hospital disclosed that as a part of its operations, it shares personal health information with BORN Ontario "related to pregnancy, birth and newborn care." [...]
