Title: Crypto casino Stake.com loses $41 million to hot wallet hackers
Date: 2023-09-05T14:24:30-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-09-05-crypto-casino-stakecom-loses-41-million-to-hot-wallet-hackers

[Source](https://www.bleepingcomputer.com/news/security/crypto-casino-stakecom-loses-41-million-to-hot-wallet-hackers/){:target="_blank" rel="noopener"}

> Online cryptocurrency casino Stake.com announced that its ETH/BSC hot wallets had been compromised to perform unauthorized transactions, with over $40 million in crypto reportedly stolen. [...]
