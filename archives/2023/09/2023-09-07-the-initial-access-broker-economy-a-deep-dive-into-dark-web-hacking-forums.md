Title: The Initial Access Broker Economy: A Deep Dive into Dark Web Hacking Forums
Date: 2023-09-07T10:02:04-04:00
Author: Sponsored by Flare
Category: BleepingComputer
Tags: Security
Slug: 2023-09-07-the-initial-access-broker-economy-a-deep-dive-into-dark-web-hacking-forums

[Source](https://www.bleepingcomputer.com/news/security/the-initial-access-broker-economy-a-deep-dive-into-dark-web-hacking-forums/){:target="_blank" rel="noopener"}

> Initial access brokers (IAB) are cybercriminals that focus on gaining access to corporate environments, which they then auction off to other hackers. Learn more from Flare about the IAB economy and how it affects your business. [...]
