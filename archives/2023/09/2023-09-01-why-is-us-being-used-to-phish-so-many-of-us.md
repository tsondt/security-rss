Title: Why is .US Being Used to Phish So Many of Us?
Date: 2023-09-01T15:38:11+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;The Coming Storm;Web Fraud 2.0;Coalition for Online Accountability;Dean Marks;GoDaddy;Interisle Consulting Group;National Telecommunications and Information Administration;U.S. Department of Commerce
Slug: 2023-09-01-why-is-us-being-used-to-phish-so-many-of-us

[Source](https://krebsonsecurity.com/2023/09/why-is-us-being-used-to-phish-so-many-of-us/){:target="_blank" rel="noopener"}

> Domain names ending in “. US ” — the top-level domain for the United States — are among the most prevalent in phishing scams, new research shows. This is noteworthy because.US is overseen by the U.S. government, which is frequently the target of phishing domains ending in.US. Also,.US domains are only supposed to be available to U.S. citizens and to those who can demonstrate that they have a physical presence in the United States..US is the “country code top-level domain” or ccTLD of the United States. Most countries have their own ccTLDs:.MX for Mexico, for example, or.CA for Canada. But [...]
