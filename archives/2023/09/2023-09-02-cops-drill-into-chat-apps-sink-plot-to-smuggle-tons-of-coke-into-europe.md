Title: Cops drill into chat apps, sink plot to smuggle tons of coke into Europe
Date: 2023-09-02T07:55:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-02-cops-drill-into-chat-apps-sink-plot-to-smuggle-tons-of-coke-into-europe

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/02/europol_balkan_cartel/){:target="_blank" rel="noopener"}

> Big blow to blighters' blow-by-the-boatload blueprint Video Efforts by cops to seize and shut down encrypted messaging apps favored by criminals, and then mine their conversations for evidence, appear to have led to more arrests — plus the seizure of about 2.7 tonnes of cocaine.... [...]
