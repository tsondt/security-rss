Title: Ransomware access broker steals accounts via Microsoft Teams phishing
Date: 2023-09-12T15:14:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-09-12-ransomware-access-broker-steals-accounts-via-microsoft-teams-phishing

[Source](https://www.bleepingcomputer.com/news/security/ransomware-access-broker-steals-accounts-via-microsoft-teams-phishing/){:target="_blank" rel="noopener"}

> Microsoft says an initial access broker known for working with ransomware groups has recently switched to Microsoft Teams phishing attacks to breach corporate networks. [...]
