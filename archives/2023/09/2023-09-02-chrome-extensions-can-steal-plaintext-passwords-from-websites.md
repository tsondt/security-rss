Title: Chrome extensions can steal plaintext passwords from websites
Date: 2023-09-02T11:04:15-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-02-chrome-extensions-can-steal-plaintext-passwords-from-websites

[Source](https://www.bleepingcomputer.com/news/security/chrome-extensions-can-steal-plaintext-passwords-from-websites/){:target="_blank" rel="noopener"}

> A team of researchers from the University of Wisconsin-Madison has uploaded to the Chrome Web Store a proof-of-concept extension that can steal plaintext passwords from a website's source code. [...]
