Title: TikTok flooded by 'Elon Musk' cryptocurrency giveaway scams
Date: 2023-09-17T14:34:07-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-09-17-tiktok-flooded-by-elon-musk-cryptocurrency-giveaway-scams

[Source](https://www.bleepingcomputer.com/news/security/tiktok-flooded-by-elon-musk-cryptocurrency-giveaway-scams/){:target="_blank" rel="noopener"}

> TikTok is flooded by a surge of fake cryptocurrency giveaways posted to the video-sharing platform, with almost all of the videos pretending to be themes based on Elon Musk, Tesla, or SpaceX. [...]
