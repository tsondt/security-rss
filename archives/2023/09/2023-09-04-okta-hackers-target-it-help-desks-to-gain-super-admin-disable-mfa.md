Title: Okta: Hackers target IT help desks to gain Super Admin, disable MFA
Date: 2023-09-04T11:29:48-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-04-okta-hackers-target-it-help-desks-to-gain-super-admin-disable-mfa

[Source](https://www.bleepingcomputer.com/news/security/okta-hackers-target-it-help-desks-to-gain-super-admin-disable-mfa/){:target="_blank" rel="noopener"}

> Identity and access management company Okta released a warning about social engineering attacks targeting IT service desk agents at U.S.-based customers in an attempt to trick them into resetting multi-factor authentication (MFA) for high-privileged users. [...]
