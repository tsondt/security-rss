Title: Modern GPUs vulnerable to new GPU.zip side-channel attack
Date: 2023-09-27T10:06:58-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2023-09-27-modern-gpus-vulnerable-to-new-gpuzip-side-channel-attack

[Source](https://www.bleepingcomputer.com/news/security/modern-gpus-vulnerable-to-new-gpuzip-side-channel-attack/){:target="_blank" rel="noopener"}

> Researchers from four American universities have developed a new GPU side-channel attack that leverages data compression to leak sensitive visual data from modern graphics cards when visiting web pages. [...]
