Title: Signal Will Leave the UK Rather Than Add a Backdoor
Date: 2023-09-26T11:15:02+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;backdoors;encryption;privacy;Signal
Slug: 2023-09-26-signal-will-leave-the-uk-rather-than-add-a-backdoor

[Source](https://www.schneier.com/blog/archives/2023/09/signal-will-leave-the-uk-rather-than-add-a-backdoor.html){:target="_blank" rel="noopener"}

> Totally expected, but still good to hear : Onstage at TechCrunch Disrupt 2023, Meredith Whittaker, the president of the Signal Foundation, which maintains the nonprofit Signal messaging app, reaffirmed that Signal would leave the U.K. if the country’s recently passed Online Safety Bill forced Signal to build “backdoors” into its end-to-end encryption. “We would leave the U.K. or any jurisdiction if it came down to the choice between backdooring our encryption and betraying the people who count on us for privacy, or leaving,” Whittaker said. “And that’s never not true.” [...]
