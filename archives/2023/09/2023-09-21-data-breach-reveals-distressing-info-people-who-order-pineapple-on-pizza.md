Title: Data breach reveals distressing info: People who order pineapple on pizza
Date: 2023-09-21T06:27:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-09-21-data-breach-reveals-distressing-info-people-who-order-pineapple-on-pizza

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/21/pizza_hut_australia_data_breach/){:target="_blank" rel="noopener"}

> Pizza Hut Australia says 190,000 customers' info – including order history – has been accessed Pizza Hut's Australian outpost has suffered a data breach.... [...]
