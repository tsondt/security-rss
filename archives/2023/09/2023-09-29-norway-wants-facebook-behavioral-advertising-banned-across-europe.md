Title: Norway wants Facebook behavioral advertising banned across Europe
Date: 2023-09-29T13:45:11+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-09-29-norway-wants-facebook-behavioral-advertising-banned-across-europe

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/29/norway_facebook_behavioral_ads/){:target="_blank" rel="noopener"}

> But Meta was just about to start asking people for their permission! Norway has told the European Data Protection Board (EDPB) it believes a countrywide ban on Meta harvesting user data to serve up advertising on Facebook and Instagram should be made permanent and extended across Europe.... [...]
