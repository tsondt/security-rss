Title: 2023 H1 IRAP report is now available on AWS Artifact for Australian customers
Date: 2023-09-21T13:47:37+00:00
Author: Patrick Chang
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Auditing;Australia;AWS Artifact;AWS security;Compliance;IRAP;MEL Region;Security;Security Blog;SYD Region
Slug: 2023-09-21-2023-h1-irap-report-is-now-available-on-aws-artifact-for-australian-customers

[Source](https://aws.amazon.com/blogs/security/2023-h1-irap-report-is-now-available-on-aws-artifact-for-australian-customers/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is excited to announce that a new Information Security Registered Assessors Program (IRAP) report (2023 H1) is now available through AWS Artifact. An independent Australian Signals Directorate (ASD) certified IRAP assessor completed the IRAP assessment of AWS in August 2023. The new IRAP report includes an additional six AWS services, as well as the new AWS [...]
