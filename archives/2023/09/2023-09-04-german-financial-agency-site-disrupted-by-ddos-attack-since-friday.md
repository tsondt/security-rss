Title: German financial agency site disrupted by DDoS attack since Friday
Date: 2023-09-04T13:11:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-04-german-financial-agency-site-disrupted-by-ddos-attack-since-friday

[Source](https://www.bleepingcomputer.com/news/security/german-financial-agency-site-disrupted-by-ddos-attack-since-friday/){:target="_blank" rel="noopener"}

> The German Federal Financial Supervisory Authority (BaFin) announced today that an ongoing distributed denial-of-service (DDoS) attack has been impacting its website since Friday. [...]
