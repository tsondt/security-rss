Title: Cars Have Terrible Data Privacy
Date: 2023-09-12T11:20:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;data protection;privacy
Slug: 2023-09-12-cars-have-terrible-data-privacy

[Source](https://www.schneier.com/blog/archives/2023/09/cars-have-terrible-data-privacy.html){:target="_blank" rel="noopener"}

> A new Mozilla Foundation report concludes that cars, all of them, have terrible data privacy. All 25 car brands we researched earned our *Privacy Not Included warning label—making cars the official worst category of products for privacy that we have ever reviewed. There’s a lot of details in the report. They’re all bad. BoingBoing post. [...]
