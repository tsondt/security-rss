Title: CISA warns govt agencies to secure iPhones against spyware attacks
Date: 2023-09-11T12:21:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Apple
Slug: 2023-09-11-cisa-warns-govt-agencies-to-secure-iphones-against-spyware-attacks

[Source](https://www.bleepingcomputer.com/news/security/cisa-warns-govt-agencies-to-secure-iphones-against-spyware-attacks/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity and Infrastructure Security Agency (CISA) ordered federal agencies today to patch security vulnerabilities abused as part of a zero-click iMessage exploit chain to infect iPhones with NSO Group's Pegasus spyware. [...]
