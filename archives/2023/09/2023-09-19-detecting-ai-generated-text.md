Title: Detecting AI-Generated Text
Date: 2023-09-19T11:08:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;LLM
Slug: 2023-09-19-detecting-ai-generated-text

[Source](https://www.schneier.com/blog/archives/2023/09/detecting-ai-generated-text.html){:target="_blank" rel="noopener"}

> There are no reliable ways to distinguish text written by a human from text written by an large language model. OpenAI writes : Do AI detectors work? In short, no. While some (including OpenAI) have released tools that purport to detect AI-generated content, none of these have proven to reliably distinguish between AI-generated and human-generated content. Additionally, ChatGPT has no “knowledge” of what content could be AI-generated. It will sometimes make up responses to questions like “did you write this [essay]?” or “could this have been written by AI?” These responses are random and have no basis in fact. To [...]
