Title: Retool blames breach on Google Authenticator MFA cloud sync feature
Date: 2023-09-15T15:15:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency;Google
Slug: 2023-09-15-retool-blames-breach-on-google-authenticator-mfa-cloud-sync-feature

[Source](https://www.bleepingcomputer.com/news/security/retool-blames-breach-on-google-authenticator-mfa-cloud-sync-feature/){:target="_blank" rel="noopener"}

> Software company Retool says the accounts of 27 cloud customers were compromised following a targeted and multi-stage social engineering attack. [...]
