Title: Mirai variant infects low-cost Android TV boxes for DDoS attacks
Date: 2023-09-06T12:56:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-06-mirai-variant-infects-low-cost-android-tv-boxes-for-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/security/mirai-variant-infects-low-cost-android-tv-boxes-for-ddos-attacks/){:target="_blank" rel="noopener"}

> A new Mirai malware botnet variant has been spotted infecting inexpensive Android TV set-top boxes used by millions for media streaming. [...]
