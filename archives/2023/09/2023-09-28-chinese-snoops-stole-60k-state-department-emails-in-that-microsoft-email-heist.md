Title: Chinese snoops stole 60K State Department emails in that Microsoft email heist
Date: 2023-09-28T23:13:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-28-chinese-snoops-stole-60k-state-department-emails-in-that-microsoft-email-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/28/chinese_hackers_stole_60000_state/){:target="_blank" rel="noopener"}

> No classified systems involved apparently, but internal diplomatic notes, travel details, staff SSNs, etc Chinese snoops stole about 60,000 State Department emails when they broke into Microsoft-hosted Outlook and Exchange Online accounts belonging to US government officials over the summer.... [...]
