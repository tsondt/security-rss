Title: The Week in Ransomware - September 29th 2023 - Dark Angels
Date: 2023-09-29T17:50:41-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-09-29-the-week-in-ransomware-september-29th-2023-dark-angels

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-september-29th-2023-dark-angels/){:target="_blank" rel="noopener"}

> This week has been a busy ransomware week, with ransomware attacks having a massive impact on organizations and the fallout of the MOVEit breaches to be disclosed. [...]
