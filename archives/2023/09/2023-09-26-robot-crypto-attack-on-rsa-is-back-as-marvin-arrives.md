Title: ROBOT crypto attack on RSA is back as Marvin arrives
Date: 2023-09-26T17:00:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-09-26-robot-crypto-attack-on-rsa-is-back-as-marvin-arrives

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/26/robot_marvin_rsa/){:target="_blank" rel="noopener"}

> More precise timing tests find many implementations vulnerable An engineer has identified longstanding undetected flaws in a 25-year-old method for encrypting data using RSA public-key cryptography.... [...]
