Title: Apple backports BLASTPASS zero-day fix to older iPhones
Date: 2023-09-12T09:42:38-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple
Slug: 2023-09-12-apple-backports-blastpass-zero-day-fix-to-older-iphones

[Source](https://www.bleepingcomputer.com/news/security/apple-backports-blastpass-zero-day-fix-to-older-iphones/){:target="_blank" rel="noopener"}

> Apple released security updates for older iPhones to fix a zero-day vulnerability tracked as CVE-2023-41064 that was actively exploited to infect iOS devices with NSO's Pegasus spyware. [...]
