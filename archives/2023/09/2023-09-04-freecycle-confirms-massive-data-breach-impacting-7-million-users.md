Title: Freecycle confirms massive data breach impacting 7 million users
Date: 2023-09-04T14:09:38-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-09-04-freecycle-confirms-massive-data-breach-impacting-7-million-users

[Source](https://www.bleepingcomputer.com/news/security/freecycle-confirms-massive-data-breach-impacting-7-million-users/){:target="_blank" rel="noopener"}

> Freecycle, an online forum dedicated to exchanging used items rather than trashing them, confirmed a massive data breach that affected more than 7 million users. [...]
