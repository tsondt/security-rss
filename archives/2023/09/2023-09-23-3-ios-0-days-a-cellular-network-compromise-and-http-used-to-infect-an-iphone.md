Title: 3 iOS 0-days, a cellular network compromise, and HTTP used to infect an iPhone
Date: 2023-09-23T00:23:58+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;exploit;iOS;iphone;zero-day
Slug: 2023-09-23-3-ios-0-days-a-cellular-network-compromise-and-http-used-to-infect-an-iphone

[Source](https://arstechnica.com/?p=1970625){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Apple has patched a potent chain of iOS zero-days that were used to infect the iPhone of an Egyptian presidential candidate with sophisticated spyware developed by a commercial exploit seller, Google and researchers from Citizen Lab said Friday. The previously unknown vulnerabilities, which Apple patched on Thursday, were exploited in clickless attacks, meaning they didn’t require a target to take any steps other than to visit a website that used the HTTP protocol rather than the safer HTTPS alternative. A packet inspection device sitting on a cellular network in Egypt kept an eye out for connections [...]
