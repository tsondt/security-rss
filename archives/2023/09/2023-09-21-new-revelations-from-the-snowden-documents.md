Title: New Revelations from the Snowden Documents
Date: 2023-09-21T11:03:43+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;backdoors;cryptography;Edward Snowden;NSA;privacy;Schneier news;surveillance
Slug: 2023-09-21-new-revelations-from-the-snowden-documents

[Source](https://www.schneier.com/blog/archives/2023/09/new-revelations-from-the-snowden-documents.html){:target="_blank" rel="noopener"}

> Jake Appelbaum’s PhD thesis contains several new revelations from the classified NSA documents provided to journalists by Edward Snowden. Nothing major, but a few more tidbits. Kind of amazing that that all happened ten years ago. At this point, those documents are more historical than anything else. And it’s unclear who has those archives anymore. According to Appelbaum, The Intercept destroyed their copy. I recently published an essay about my experiences ten years ago. [...]
