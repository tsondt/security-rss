Title: Robocall scammers sentenced in US after netting $1.2M via India-based call centers
Date: 2023-09-20T13:29:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-09-20-robocall-scammers-sentenced-in-us-after-netting-12m-via-india-based-call-centers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/20/court_sentences_two_to_41/){:target="_blank" rel="noopener"}

> Part of network of crims who used 'trickery and threats' to target elderly, says US Attorney Two Indian nationals each received 41-month prison sentences for their involvement in $1.2 million worth of robocall scams targeting the elderly, according to the district of New Jersey’s attorney's office on Tuesday.... [...]
