Title: TissuPath hack: patient data possibly exposed in cyber-attack on Melbourne pathology clinic
Date: 2023-09-05T06:30:36+00:00
Author: Ariel Bogle
Category: The Guardian
Tags: Cybercrime;Australia news;Data and computer security;Hacking;Victoria
Slug: 2023-09-05-tissupath-hack-patient-data-possibly-exposed-in-cyber-attack-on-melbourne-pathology-clinic

[Source](https://www.theguardian.com/technology/2023/sep/05/tissupath-hack-patient-data-breach-cyber-attack-melbourne-pathology-clinic){:target="_blank" rel="noopener"}

> Company says it is investigating the potential exposure of referral letters, patient names, contact details and Medicare numbers Get our morning and afternoon news emails, free app or daily news podcast Ten years worth of pathology referral letters may have been exposed in a cybersecurity incident affecting the Victorian pathology clinic TissuPath. The government is aware of the data breach as well as potential incidents affecting real estate firm Barry Plant and owners corporation management company Strata Plan, national cybersecurity coordinator Darren Goldie said in a statement. Sign up for Guardian Australia’s free morning and afternoon email newsletters for your [...]
