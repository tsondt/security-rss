Title: Twitter chaos after Elon Musk takeover may have violated privacy order, DoJ alleges
Date: 2023-09-13T11:50:26+00:00
Author: Dan Milmo and agency
Category: The Guardian
Tags: X (formerly known as Twitter);Elon Musk;Data and computer security;Technology sector;Internet;Technology;Media;Social media;Business;US news;Law (US);World news;Mergers and acquisitions;Privacy;Law
Slug: 2023-09-13-twitter-chaos-after-elon-musk-takeover-may-have-violated-privacy-order-doj-alleges

[Source](https://www.theguardian.com/technology/2023/sep/13/twitter-elon-musk-takeover-ftc-order-data-security-privacy-doj-case){:target="_blank" rel="noopener"}

> US Department of Justice questions compliance with FTC order on data security and privacy practices Elon Musk’s takeover of Twitter created a “chaotic environment” at the social media platform that may have violated a government order requiring an overhaul of its data security and privacy practices, according to a court filing. The US Department of Justice (DoJ) alleged in a legal filing on Tuesday that depositions from former employees at Twitter, now rebranded X, raised “serious questions” about whether the company was complying with an order imposed by the consumer and competition watchdog, the Federal Trade Commission (FTC). Continue reading... [...]
