Title: China's national security minister rates fake news among most pressing cyber threats
Date: 2023-09-28T03:58:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-09-28-chinas-national-security-minister-rates-fake-news-among-most-pressing-cyber-threats

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/28/chen_yixin_china_digital_threats/){:target="_blank" rel="noopener"}

> He's also worried about alliances that freeze out Chinese tech Chinese minister for national security Chen Yixin has penned an article rating the digital risks his country faces and rated network security incidents as the most realistic source of harm to the Chinternet – both in terms of attacks and the dissemination of fake news.... [...]
