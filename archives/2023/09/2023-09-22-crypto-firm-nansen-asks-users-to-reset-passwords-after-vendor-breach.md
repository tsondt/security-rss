Title: Crypto firm Nansen asks users to reset passwords after vendor breach
Date: 2023-09-22T13:22:43-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-09-22-crypto-firm-nansen-asks-users-to-reset-passwords-after-vendor-breach

[Source](https://www.bleepingcomputer.com/news/security/crypto-firm-nansen-asks-users-to-reset-passwords-after-vendor-breach/){:target="_blank" rel="noopener"}

> Ethereum blockchain analytics firm Nansen asks a subset of its users to reset passwords following a recent data breach at its authentication provider. [...]
