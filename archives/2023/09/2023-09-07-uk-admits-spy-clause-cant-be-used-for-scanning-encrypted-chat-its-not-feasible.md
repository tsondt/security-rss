Title: UK admits 'spy clause' can't be used for scanning encrypted chat – it's not 'feasible'
Date: 2023-09-07T10:09:18+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-09-07-uk-admits-spy-clause-cant-be-used-for-scanning-encrypted-chat-its-not-feasible

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/07/uk_government_clause_online_safety_bill/){:target="_blank" rel="noopener"}

> But don't celebrate yet... it has simply kicked the online safety can down the road, Westminster style Comment Sanity appears to have prevailed in the debate over the UK's Online Safety Bill after the government agreed to ditch proposals – at least for the time being – to legislate the scanning of end-to-end encrypted messages.... [...]
