Title: Australia to build six 'cyber shields' to defend its shores
Date: 2023-09-19T03:44:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-09-19-australia-to-build-six-cyber-shields-to-defend-its-shores

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/19/australia_six_cyber_shields/){:target="_blank" rel="noopener"}

> Local corporate regulator warns boards that cyber is totally a directorial duty Australia will build "six cyber shields around our nation" declared home affairs minister Clare O'Neill yesterday, as part of a national cyber security strategy.... [...]
