Title: Google is retiring its Gmail Basic HTML view in January 2024
Date: 2023-09-25T12:08:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2023-09-25-google-is-retiring-its-gmail-basic-html-view-in-january-2024

[Source](https://www.bleepingcomputer.com/news/security/google-is-retiring-its-gmail-basic-html-view-in-january-2024/){:target="_blank" rel="noopener"}

> Google is notifying Gmail users that the webmail's Basic HTML view will be deprecated in January 2024, and users will require modern browsers to continue using the service. [...]
