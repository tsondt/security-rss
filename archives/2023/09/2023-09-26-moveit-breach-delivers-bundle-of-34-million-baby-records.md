Title: MOVEit breach delivers bundle of 3.4 million baby records
Date: 2023-09-26T14:30:12+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-09-26-moveit-breach-delivers-bundle-of-34-million-baby-records

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/26/ontario_born_moveit_breach/){:target="_blank" rel="noopener"}

> Progress Software vulnerability ID'd in enormous burglary at Ontario's BORN Canada's Better Outcomes Registry & Network (BORN) fears a MOVEit breach allowed cybercriminals to copy 3.4 million people's childcare health records dating back more than a decade.... [...]
