Title: Google throws California $93M to make location tracking lawsuit disappear
Date: 2023-09-15T17:15:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-09-15-google-throws-california-93m-to-make-location-tracking-lawsuit-disappear

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/15/google_location_tracking_settlement/){:target="_blank" rel="noopener"}

> Half a percent of last quarter's net income? That'll teach 'em Google has been hit with another lawsuit alleging it deceived users about its collection, storage, and use of their location data, this time from the state of California. Yet it's over before it really began.... [...]
