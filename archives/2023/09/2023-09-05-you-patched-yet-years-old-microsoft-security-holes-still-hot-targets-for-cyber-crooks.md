Title: You patched yet? Years-old Microsoft security holes still hot targets for cyber-crooks
Date: 2023-09-05T21:37:01+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-05-you-patched-yet-years-old-microsoft-security-holes-still-hot-targets-for-cyber-crooks

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/05/qualys_top_20_vulnerabilities/){:target="_blank" rel="noopener"}

> We're number one! We're number one! We're... It's generally accepted that security flaws in Microsoft's products are a top magnet for crooks and fraudsters: its sprawling empire of hardware and software is a target-rich ecosystem in that there is a wide range of bugs to exploit, and a huge number of vulnerable organizations and users.... [...]
