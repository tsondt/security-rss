Title: Accelerating JVM cryptography with Amazon Corretto Crypto Provider 2
Date: 2023-09-01T19:56:25+00:00
Author: Will Childs-Klein
Category: AWS Security
Tags: Announcements;Best Practices;Intermediate (200);Security, Identity, & Compliance;cryptography;Security Blog
Slug: 2023-09-01-accelerating-jvm-cryptography-with-amazon-corretto-crypto-provider-2

[Source](https://aws.amazon.com/blogs/security/accelerating-jvm-cryptography-with-amazon-corretto-crypto-provider-2/){:target="_blank" rel="noopener"}

> Earlier this year, Amazon Web Services (AWS) released Amazon Corretto Crypto Provider (ACCP) 2, a cryptography provider built by AWS for Java virtual machine (JVM) applications. ACCP 2 delivers comprehensive performance enhancements, with some algorithms (such as elliptic curve key generation) seeing a greater than 13-fold improvement over ACCP 1. The new release also brings [...]
