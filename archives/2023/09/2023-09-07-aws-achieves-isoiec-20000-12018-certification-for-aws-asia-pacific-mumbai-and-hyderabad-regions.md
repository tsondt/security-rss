Title: AWS achieves ISO/IEC 20000-1:2018 certification for AWS Asia Pacific (Mumbai) and (Hyderabad) Regions
Date: 2023-09-07T16:06:24+00:00
Author: Airish Mariano
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Certificate;Compliance;Compliance reports;India;ISO 20000;MeitY;Security Blog
Slug: 2023-09-07-aws-achieves-isoiec-20000-12018-certification-for-aws-asia-pacific-mumbai-and-hyderabad-regions

[Source](https://aws.amazon.com/blogs/security/aws-achieves-iso-iec-20000-12018-certification-for-aws-asia-pacific-mumbai-and-hyderabad-regions/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is proud to announce the successful completion of the ISO/IEC 20000-1:2018 certification for the AWS Asia Pacific (Mumbai) and (Hyderabad) Regions in India. The scope of the ISO/IEC 20000-1:2018 certification is limited to the IT Service Management System (ITSMS) of AWS India Data Center (DC) Operations that supports the delivery of [...]
