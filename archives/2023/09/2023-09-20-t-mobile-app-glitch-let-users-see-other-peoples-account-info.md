Title: T-Mobile app glitch let users see other people's account info
Date: 2023-09-20T17:11:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-09-20-t-mobile-app-glitch-let-users-see-other-peoples-account-info

[Source](https://www.bleepingcomputer.com/news/security/t-mobile-app-glitch-let-users-see-other-peoples-account-info/){:target="_blank" rel="noopener"}

> T-Mobile customers today were able to see other people's account and billing information after logging into the company's official mobile application. [...]
