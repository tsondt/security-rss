Title: ESA gets the job of building Europe's secure satcomms network
Date: 2023-09-22T05:31:15+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-09-22-esa-gets-the-job-of-building-europes-secure-satcomms-network

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/22/esa_wins_iris2_satellite_contract/){:target="_blank" rel="noopener"}

> IRIS 2 oversight deal signed as constellation’s schedule slips, and Ariane 6 hits another snag The European Space Agency has signed up to build and launch the European Union's Infrastructure for Resilience, Interconnectivity and Security by Satellite constellation.... [...]
