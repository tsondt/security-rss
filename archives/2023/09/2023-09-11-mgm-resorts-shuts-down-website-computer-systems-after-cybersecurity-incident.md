Title: MGM Resorts shuts down website, computer systems after 'cybersecurity incident'
Date: 2023-09-11T20:17:57+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-11-mgm-resorts-shuts-down-website-computer-systems-after-cybersecurity-incident

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/11/mgm_resorts_cybersecurity_incident/){:target="_blank" rel="noopener"}

> Ransomware? Some would be willing to bet on that MGM Resorts has shut down some of its IT systems following a "cybersecurity incident" that the casino-and-hotel giant says is currently under investigation.... [...]
