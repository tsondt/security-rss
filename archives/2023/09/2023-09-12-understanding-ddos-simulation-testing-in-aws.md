Title: Understanding DDoS simulation testing in AWS
Date: 2023-09-12T16:45:41+00:00
Author: Harith Gaddamanugu
Category: AWS Security
Tags: AWS Shield;Best Practices;Intermediate (200);Security, Identity, & Compliance;AWS Shield Advanced;Security Blog
Slug: 2023-09-12-understanding-ddos-simulation-testing-in-aws

[Source](https://aws.amazon.com/blogs/security/understanding-ddos-simulation-testing-at-aws/){:target="_blank" rel="noopener"}

> Distributed denial of service (DDoS) events occur when a threat actor sends traffic floods from multiple sources to disrupt the availability of a targeted application. DDoS simulation testing uses a controlled DDoS event to allow the owner of an application to assess the application’s resilience and practice event response. DDoS simulation testing is permitted on [...]
