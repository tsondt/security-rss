Title: Manage infrastructure with Workload Identity Federation and Terraform Cloud
Date: 2023-09-21T16:00:00+00:00
Author: Manjuram Perumalla
Category: GCP Security
Tags: Security & Identity;Developers & Practitioners;DevOps & SRE
Slug: 2023-09-21-manage-infrastructure-with-workload-identity-federation-and-terraform-cloud

[Source](https://cloud.google.com/blog/products/devops-sre/infrastructure-as-code-with-terraform-and-identity-federation/){:target="_blank" rel="noopener"}

> Introduction Terraform Cloud (TFC) can help manage infrastructure as code (IaC) development for large enterprises. As the number of Google Cloud projects grows, managing access controls for Terraform Cloud projects and workspaces can become complex. Don't worry, we have a solution that is designed to be more secure than using Google Cloud service account keys, and also scales well for hundreds or even thousands of Google Cloud projects, TFC workspaces, and TFC projects using Workload Identity Federation. An enterprise scenario Consider a fictitious financial services firm example.com that offers banking, lending, insurance and brokerage service to customers. Their Google Cloud [...]
