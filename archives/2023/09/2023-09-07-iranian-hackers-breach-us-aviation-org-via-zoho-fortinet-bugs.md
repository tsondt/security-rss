Title: Iranian hackers breach US aviation org via Zoho, Fortinet bugs
Date: 2023-09-07T17:32:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-09-07-iranian-hackers-breach-us-aviation-org-via-zoho-fortinet-bugs

[Source](https://www.bleepingcomputer.com/news/security/iranian-hackers-breach-us-aviation-org-via-zoho-fortinet-bugs/){:target="_blank" rel="noopener"}

> State-backed hacking groups have breached a U.S. aeronautical organization using exploits targeting critical Zoho and Fortinet vulnerabilities, a joint advisory published by CISA, the FBI, and the United States Cyber Command (USCYBERCOM) revealed on Thursday. [...]
