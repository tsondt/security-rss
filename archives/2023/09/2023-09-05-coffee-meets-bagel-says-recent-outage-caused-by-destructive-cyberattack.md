Title: Coffee Meets Bagel says recent outage caused by destructive cyberattack
Date: 2023-09-05T18:01:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-09-05-coffee-meets-bagel-says-recent-outage-caused-by-destructive-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/coffee-meets-bagel-says-recent-outage-caused-by-destructive-cyberattack/){:target="_blank" rel="noopener"}

> The Coffee Meets Bagel dating platform confirms last week's outage was caused by hackers breaching the company's systems and deleting company data. [...]
