Title: Friday Squid Blogging: Cleaning Squid
Date: 2023-09-15T21:08:38+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-09-15-friday-squid-blogging-cleaning-squid

[Source](https://www.schneier.com/blog/archives/2023/09/friday-squid-blogging-cleaning-squid.html){:target="_blank" rel="noopener"}

> Two links on how to properly clean squid. I learned a few years ago, in Spain, and got pretty good at it. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
