Title: A new Chrome 0-day is sending the Internet into a new chapter of Groundhog Day
Date: 2023-09-28T21:23:15+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;chrome;exploit;firefox;libvpx;libwebp;zero-day
Slug: 2023-09-28-a-new-chrome-0-day-is-sending-the-internet-into-a-new-chapter-of-groundhog-day

[Source](https://arstechnica.com/?p=1972043){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) A critical zero-day vulnerability Google reported on Wednesday in its Chrome browser is opening the Internet to a new chapter of Groundhog Day. Like a critical zero-day Google disclosed on September 11, the new exploited vulnerability doesn’t affect just Chrome. Already, Mozilla has said that its Firefox browser is vulnerable to the same bug, which is tracked as CVE-2023-5217. And just like CVE-2023-4863 from 17 days ago, the new one resides in a widely used code library for processing media files, specifically those in the VP8 format. Pages here and here list hundreds of packages for [...]
