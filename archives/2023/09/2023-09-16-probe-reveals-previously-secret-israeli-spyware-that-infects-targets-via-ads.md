Title: Probe reveals previously secret Israeli spyware that infects targets via ads
Date: 2023-09-16T09:05:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-16-probe-reveals-previously-secret-israeli-spyware-that-infects-targets-via-ads

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/16/insanet_spyware/){:target="_blank" rel="noopener"}

> Oh s#!t, Sherlock Israeli software maker Insanet has reportedly developed a commercial product called Sherlock that can infect devices via online adverts to snoop on targets and collect data about them for the biz's clients.... [...]
