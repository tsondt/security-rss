Title: Microsoft will block 3rd-party printer drivers in Windows Update
Date: 2023-09-11T15:22:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-09-11-microsoft-will-block-3rd-party-printer-drivers-in-windows-update

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-will-block-3rd-party-printer-drivers-in-windows-update/){:target="_blank" rel="noopener"}

> Microsoft will block third-party printer driver delivery in Windows Update as part of a substantial and gradual shift in its printer driver strategy over the next 4 years. [...]
