Title: Windows 11 22H2 adds a built-in passkey manager for Windows Hello
Date: 2023-09-26T13:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-09-26-windows-11-22h2-adds-a-built-in-passkey-manager-for-windows-hello

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-11-22h2-adds-a-built-in-passkey-manager-for-windows-hello/){:target="_blank" rel="noopener"}

> Today's Windows 11 update includes several security improvements, including a new passkeys management dashboard designed to help users go passwordless more easily and tools to reduce the attack surface. [...]
