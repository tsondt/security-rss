Title: The International Criminal Court will now prosecute cyberwar crimes
Date: 2023-09-08T17:23:40+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Policy;Security;Cyberattacks;cybercrime;hospital cyberattack;Invasion of Ukraine;prosecution;syndication
Slug: 2023-09-08-the-international-criminal-court-will-now-prosecute-cyberwar-crimes

[Source](https://arstechnica.com/?p=1966589){:target="_blank" rel="noopener"}

> Enlarge / Karim Khan speaks at Colombia's Special Jurisdiction for Peace during the visit of the Prosecutor of the International Criminal Court in Bogota, Colombia, on June 6, 2023. (credit: Long Visual Press/Getty ) For years, some cybersecurity defenders and advocates have called for a kind of Geneva Convention for cyberwar, new international laws that would create clear consequences for anyone hacking civilian critical infrastructure, like power grids, banks, and hospitals. Now the lead prosecutor of the International Criminal Court at the Hague has made it clear that he intends to enforce those consequences—no new Geneva Convention required. Instead, he [...]
