Title: Broaden your cyber security knowhow at CyberThreat 2023
Date: 2023-09-20T09:23:09+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2023-09-20-broaden-your-cyber-security-knowhow-at-cyberthreat-2023

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/20/broaden_your_cyber_security_knowhow/){:target="_blank" rel="noopener"}

> November’s two day conference sees experts from the cyber security community share their insight and knowledge Sponsored Post Cyber security remains a top three priority for most, if not all, organisations. The risks associated with failure to implement adequate defences were once again highlighted by the ransomware incident which impacted several hospital computer systems across the US last month.... [...]
