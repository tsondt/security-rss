Title: T-Mobile denies new data breach rumors, points to authorized retailer
Date: 2023-09-22T11:05:02-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-09-22-t-mobile-denies-new-data-breach-rumors-points-to-authorized-retailer

[Source](https://www.bleepingcomputer.com/news/security/t-mobile-denies-new-data-breach-rumors-points-to-authorized-retailer/){:target="_blank" rel="noopener"}

> T-Mobile has denied suffering another data breach following Thursday night reports that a threat actor leaked a large database allegedly containing T-Mobile employees' data. [...]
