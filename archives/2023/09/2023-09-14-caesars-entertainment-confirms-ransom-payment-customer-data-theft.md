Title: Caesars Entertainment confirms ransom payment, customer data theft
Date: 2023-09-14T12:58:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-09-14-caesars-entertainment-confirms-ransom-payment-customer-data-theft

[Source](https://www.bleepingcomputer.com/news/security/caesars-entertainment-confirms-ransom-payment-customer-data-theft/){:target="_blank" rel="noopener"}

> Caesars Entertainment, self-described as the largest U.S. casino chain with the most extensive loyalty program in the industry, says it paid a ransom to avoid the online leak of customer data stolen in a recent cyberattack. [...]
