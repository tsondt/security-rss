Title: TransUnion reckons big dump of stolen customer data came from someone else
Date: 2023-09-21T18:58:49+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-21-transunion-reckons-big-dump-of-stolen-customer-data-came-from-someone-else

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/21/transunion_data_dump/){:target="_blank" rel="noopener"}

> Prolific info-thief strikes again Days after a miscreant boasted leaking a 3GB-plus database from TransUnion containing financial information on 58,505 people, the credit-checking agency has claimed the info was actually swiped from a third party.... [...]
