Title: US govt IT help desk techie 'leaked top secrets' to foreign nation
Date: 2023-09-21T22:10:36+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-21-us-govt-it-help-desk-techie-leaked-top-secrets-to-foreign-nation

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/21/it_help_desk_guy_arrested/){:target="_blank" rel="noopener"}

> National defense files can earn you $55K... and espionage charges A US government worker has been arrested and charged with spying for Ethiopia, according to court documents unsealed Thursday.... [...]
