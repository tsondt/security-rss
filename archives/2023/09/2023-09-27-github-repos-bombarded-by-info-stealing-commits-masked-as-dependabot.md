Title: GitHub repos bombarded by info-stealing commits masked as Dependabot
Date: 2023-09-27T08:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-09-27-github-repos-bombarded-by-info-stealing-commits-masked-as-dependabot

[Source](https://www.bleepingcomputer.com/news/security/github-repos-bombarded-by-info-stealing-commits-masked-as-dependabot/){:target="_blank" rel="noopener"}

> Hackers are breaching GitHub accounts and inserting malicious code disguised as Dependabot contributions to steal authentication secrets and passwords from developers. [...]
