Title: US and UK sanction 11 TrickBot and Conti cybercrime gang members
Date: 2023-09-07T10:27:20-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-09-07-us-and-uk-sanction-11-trickbot-and-conti-cybercrime-gang-members

[Source](https://www.bleepingcomputer.com/news/security/us-and-uk-sanction-11-trickbot-and-conti-cybercrime-gang-members/){:target="_blank" rel="noopener"}

> The USA and the United Kingdom have sanctioned eleven Russian nationals associated with the TrickBot and Conti ransomware cybercrime operations. [...]
