Title: Menacing marketeers fined by ICO for 1.9M cold calls
Date: 2023-09-21T10:17:06+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-09-21-menacing-marketeers-fined-by-ico-for-19m-cold-calls

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/21/ico_fines_another_bad_bunch/){:target="_blank" rel="noopener"}

> Five businesses facing half a million in collective penalties for illegally phoning folk registered with TPS The UK data watchdog has penalized five businesses it says collectively made 1.9 million cold calls to members of the public, illegally, as those people had opted out of being menaced at home by marketeers.... [...]
