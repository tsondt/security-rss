Title: Google quietly corrects previously submitted disclosure for critical webp 0-day
Date: 2023-09-27T00:47:53+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;chrome;google;libwebp;WebP
Slug: 2023-09-27-google-quietly-corrects-previously-submitted-disclosure-for-critical-webp-0-day

[Source](https://arstechnica.com/?p=1971345){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Google has quietly resubmitted a disclosure of a critical code-execution vulnerability affecting thousands of individual apps and software frameworks after its previous submission left readers with the mistaken impression that the threat affected only the Chrome browser. The vulnerability originates in the libwebp code library, which Google created in 2010 for rendering images in webp, a then-new format that resulted in files that were up to 26 percent smaller than PNG images. Libwebp is incorporated into just about every app, operating system, or other code library that renders webp images, most notably the Electron framework used [...]
