Title: Rollbar discloses data breach after hackers stole access tokens
Date: 2023-09-13T15:57:05-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-09-13-rollbar-discloses-data-breach-after-hackers-stole-access-tokens

[Source](https://www.bleepingcomputer.com/news/security/rollbar-discloses-data-breach-after-hackers-stole-access-tokens/){:target="_blank" rel="noopener"}

> Software bug-tracking company Rollbar disclosed a data breach after unknown attackers hacked its systems in early August and gained access to customer access tokens. [...]
