Title: Ragnar Locker claims attack on Israel's Mayanei Hayeshua hospital
Date: 2023-09-08T15:02:43-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-09-08-ragnar-locker-claims-attack-on-israels-mayanei-hayeshua-hospital

[Source](https://www.bleepingcomputer.com/news/security/ragnar-locker-claims-attack-on-israels-mayanei-hayeshua-hospital/){:target="_blank" rel="noopener"}

> The Ragnar Locker ransomware gang has claimed responsibility for an attack on Israel's Mayanei Hayeshua hospital, threatening to leak 1 TB of data allegedly stolen during the cyberattack. [...]
