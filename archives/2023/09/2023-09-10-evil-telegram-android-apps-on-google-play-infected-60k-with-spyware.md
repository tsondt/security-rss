Title: 'Evil Telegram' Android apps on Google Play infected 60K with spyware
Date: 2023-09-10T10:39:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2023-09-10-evil-telegram-android-apps-on-google-play-infected-60k-with-spyware

[Source](https://www.bleepingcomputer.com/news/security/evil-telegram-android-apps-on-google-play-infected-60k-with-spyware/){:target="_blank" rel="noopener"}

> Several malicious Telegram clones for Android on Google Play were installed over 60,000 times, infecting people with spyware that steals user messages, contacts lists, and other data. [...]
