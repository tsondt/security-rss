Title: Apple opens annual applications for free hackable iPhones
Date: 2023-09-04T02:58:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-09-04-apple-opens-annual-applications-for-free-hackable-iphones

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/04/infosec_in_brief/){:target="_blank" rel="noopener"}

> ALSO: Brazilian stalkerware database ripped by the short hairs, a fast fashion breach, and this week's critical vulns Infosec in brief The latest round of Apple's Security Research Device (SRD) program is open, giving security researchers a chance to get their hands on an unlocked device – and Apple's blessing to attack it and test its security capabilities.... [...]
