Title: Uncle Sam warns deepfakes are coming for your brand and bank account
Date: 2023-09-13T18:30:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-09-13-uncle-sam-warns-deepfakes-are-coming-for-your-brand-and-bank-account

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/13/us_agencies_deepfake_threat/){:target="_blank" rel="noopener"}

> No, your CEO is not on Teams asking you to transfer money Deepfakes are coming for your brand, bank accounts, and corporate IP, according to a warning from US law enforcement and cyber agencies.... [...]
