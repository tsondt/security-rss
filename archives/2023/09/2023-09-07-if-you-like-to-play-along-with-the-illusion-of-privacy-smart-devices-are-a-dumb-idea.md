Title: If you like to play along with the illusion of privacy, smart devices are a dumb idea
Date: 2023-09-07T12:11:07+00:00
Author: Richard Currie
Category: The Register
Tags: 
Slug: 2023-09-07-if-you-like-to-play-along-with-the-illusion-of-privacy-smart-devices-are-a-dumb-idea

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/07/smart_devices_privacy/){:target="_blank" rel="noopener"}

> You're just giving manufacturers carte blanche to profit off personal data Updated Depressingly predictable research from Which? serves as another reminder, if one was needed, that furnishing your home with internet-connected "smart" devices could be a dumb idea if you'd rather try to preserve your privacy.... [...]
