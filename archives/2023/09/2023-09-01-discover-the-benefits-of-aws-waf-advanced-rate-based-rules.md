Title: Discover the benefits of AWS WAF advanced rate-based rules
Date: 2023-09-01T13:17:50+00:00
Author: Rodrigo Ferroni
Category: AWS Security
Tags: Announcements;AWS WAF;Best Practices;Intermediate (200);Security, Identity, & Compliance;Security Blog
Slug: 2023-09-01-discover-the-benefits-of-aws-waf-advanced-rate-based-rules

[Source](https://aws.amazon.com/blogs/security/discover-the-benefits-of-aws-waf-advanced-rate-based-rules/){:target="_blank" rel="noopener"}

> In 2017, AWS announced the release of Rate-based Rules for AWS WAF, a new rule type that helps protect websites and APIs from application-level threats such as distributed denial of service (DDoS) attacks, brute force log-in attempts, and bad bots. Rate-based rules track the rate of requests for each originating IP address and invokes a [...]
