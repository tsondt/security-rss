Title: DARPA takes its long-duration Manta undersea drone for a test-dip
Date: 2023-09-28T18:36:29+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-09-28-darpa-takes-its-long-duration-manta-undersea-drone-for-a-test-dip

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/28/darpa_underwater_drone/){:target="_blank" rel="noopener"}

> Autonomous sub should recharge and resupply in perfect stealth, hopefully DARPA's extended-duration unmanned undersea vehicle (UUV) is having its first aquatic excursion to test if this naval drone has wings, er, fins.... [...]
