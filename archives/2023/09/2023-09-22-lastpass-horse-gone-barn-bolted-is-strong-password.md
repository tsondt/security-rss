Title: LastPass: ‘Horse Gone Barn Bolted’ is Strong Password
Date: 2023-09-22T23:41:09+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;The Coming Storm;Web Fraud 2.0;International Computer Science Institute;Karim Toubba;lastpass breach;Nicholas Weaver;Wladimir Palant
Slug: 2023-09-22-lastpass-horse-gone-barn-bolted-is-strong-password

[Source](https://krebsonsecurity.com/2023/09/lastpass-horse-gone-barn-bolted-is-strong-password/){:target="_blank" rel="noopener"}

> The password manager service LastPass is now forcing some of its users to pick longer master passwords. LastPass says the changes are needed to ensure all customers are protected by their latest security improvements. But critics say the move is little more than a public relations stunt that will do nothing to help countless early adopters whose password vaults were exposed in a 2022 breach at LastPass. LastPass sent this notification to users earlier this week. LastPass told customers this week they would be forced to update their master password if it was less than 12 characters. LastPass officially instituted [...]
