Title: MGM Resorts shuts down IT systems after cyberattack
Date: 2023-09-11T14:54:41-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-09-11-mgm-resorts-shuts-down-it-systems-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/mgm-resorts-shuts-down-it-systems-after-cyberattack/){:target="_blank" rel="noopener"}

> MGM Resorts International disclosed today that it is dealing with a cybersecurity issue that impacted some of its systems, including its main website, online reservations, and in-casino services, like ATMs, slot machines, and credit card machines. [...]
