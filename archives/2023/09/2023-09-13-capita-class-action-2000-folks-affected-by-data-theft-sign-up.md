Title: Capita class action: 2,000 folks affected by data theft sign up
Date: 2023-09-13T10:02:19+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-09-13-capita-class-action-2000-folks-affected-by-data-theft-sign-up

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/13/capita_class_action_2000_claimants/){:target="_blank" rel="noopener"}

> Pensioners, employees and medical pros among those aiming to be compensated for data exposure The number of claimants signing up to a collective action against Capita over the infamous March cyber security break-in and subsequent data exposure keeps going up, according to the lawyer overseeing the case.... [...]
