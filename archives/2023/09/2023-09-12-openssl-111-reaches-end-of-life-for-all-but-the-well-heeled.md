Title: OpenSSL 1.1.1 reaches end of life for all but the well-heeled
Date: 2023-09-12T18:00:15+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-09-12-openssl-111-reaches-end-of-life-for-all-but-the-well-heeled

[Source](https://go.theregister.com/feed/www.theregister.com/2023/09/12/openssl_111_end_of_life/){:target="_blank" rel="noopener"}

> $50k to breathe new life into its corpse. The rest of us must move on to OpenSSL 3.0 OpenSSL 1.1.1 has reached the end of its life, making a move to a later version essential for all, bar those with extremely deep pockets.... [...]
