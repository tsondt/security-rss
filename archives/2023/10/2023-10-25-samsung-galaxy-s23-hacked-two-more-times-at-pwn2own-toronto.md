Title: Samsung Galaxy S23 hacked two more times at Pwn2Own Toronto
Date: 2023-10-25T18:46:03-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-25-samsung-galaxy-s23-hacked-two-more-times-at-pwn2own-toronto

[Source](https://www.bleepingcomputer.com/news/security/samsung-galaxy-s23-hacked-two-more-times-at-pwn2own-toronto/){:target="_blank" rel="noopener"}

> Security researchers hacked the Samsung Galaxy S23 smartphone two more times on the second day of the Pwn2Own 2023 hacking competition in Toronto, Canada. [...]
