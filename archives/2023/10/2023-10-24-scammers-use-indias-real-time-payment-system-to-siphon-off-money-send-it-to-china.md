Title: Scammers use India’s real-time payment system to siphon off money, send it to China
Date: 2023-10-24T03:30:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-10-24-scammers-use-indias-real-time-payment-system-to-siphon-off-money-send-it-to-china

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/24/scammers_use_indias_realtime_payment/){:target="_blank" rel="noopener"}

> Countries signed on for India’s stack might watch out China-based scammers are using a combination of fake loan apps and India's real-time mobile payment system, Unified Payments Interface (UPI), to separate victims from their cash, according to a report by threat intel firm CloudSEK.... [...]
