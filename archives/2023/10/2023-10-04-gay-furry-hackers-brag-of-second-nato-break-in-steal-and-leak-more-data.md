Title: 'Gay furry hackers' brag of second NATO break-in, steal and leak more data
Date: 2023-10-04T20:22:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-10-04-gay-furry-hackers-brag-of-second-nato-break-in-steal-and-leak-more-data

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/04/nato_data_attack/){:target="_blank" rel="noopener"}

> 'No impact on missions,' military powerhouse insists NATO is "actively addressing" multiple IT security incidents after a hacktivist group claimed it once again breached some of the military alliance's websites, this time stealing what's claimed to be more than 3,000 files and 9GB of data.... [...]
