Title: AWS FedRAMP Revision 5 baselines transition update
Date: 2023-10-25T13:22:37+00:00
Author: Kevin Donohue
Category: AWS Security
Tags: Announcements;AWS GovCloud (US);Federal;Foundational (100);Government;Public Sector;Security, Identity, & Compliance;AWS (US) GovCloud;AWS East/West;DoD;Federal government;FedRAMP;NIST;Security Blog;Shared Responsibility Model
Slug: 2023-10-25-aws-fedramp-revision-5-baselines-transition-update

[Source](https://aws.amazon.com/blogs/security/aws-fedramp-revision-5-transition-update/){:target="_blank" rel="noopener"}

> On May 20, 2023, the Federal Risk and Authorization Management Program (FedRAMP) released the FedRAMP Rev.5 baselines. The FedRAMP baselines were updated to correspond with the National Institute of Standards and Technology’s (NIST) Special Publication (SP) 800-53 Rev. 5 Catalog of Security and Privacy Controls for Information Systems and Organizations and SP 800-53B Control Baselines for Information Systems [...]
