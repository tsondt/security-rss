Title: Third Flagstar Bank data breach since 2021 affects 800,000 customers
Date: 2023-10-08T10:07:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-08-third-flagstar-bank-data-breach-since-2021-affects-800000-customers

[Source](https://www.bleepingcomputer.com/news/security/third-flagstar-bank-data-breach-since-2021-affects-800-000-customers/){:target="_blank" rel="noopener"}

> Flagstar Bank is warning that over 800,000 US customers had their personal information stolen by cybercriminals due to a breach at a third-party service provider. [...]
