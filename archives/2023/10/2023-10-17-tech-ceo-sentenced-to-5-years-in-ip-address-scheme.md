Title: Tech CEO Sentenced to 5 Years in IP Address Scheme
Date: 2023-10-17T16:23:25+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Web Fraud 2.0;American Registry for Internet Numbers;Amir Golestan;ARIN;Micfo LLC;spamhaus
Slug: 2023-10-17-tech-ceo-sentenced-to-5-years-in-ip-address-scheme

[Source](https://krebsonsecurity.com/2023/10/tech-ceo-sentenced-to-5-years-in-ip-address-scheme/){:target="_blank" rel="noopener"}

> Amir Golestan, the 40-year-old CEO of the Charleston, S.C. based technology company Micfo LLC, has been sentenced to five years in prison for wire fraud. Golestan’s sentencing comes nearly two years after he pleaded guilty to using an elaborate network of phony companies to secure more than 735,000 Internet Protocol (IP) addresses from the American Registry for Internet Numbers (ARIN), the nonprofit which oversees IP addresses assigned to entities in the U.S., Canada, and parts of the Caribbean. Amir Golestan, the former CEO of Micfo. In 2018, ARIN sued Golestan and Micfo, alleging they had obtained hundreds of thousands of [...]
