Title: Ragnar Locker ransomware’s dark web extortion sites seized by police
Date: 2023-10-19T10:39:39-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-10-19-ragnar-locker-ransomwares-dark-web-extortion-sites-seized-by-police

[Source](https://www.bleepingcomputer.com/news/security/ragnar-locker-ransomwares-dark-web-extortion-sites-seized-by-police/){:target="_blank" rel="noopener"}

> The Ragnar Locker ransomware operation's Tor negotiation and data leak sites were seized Thursday morning as part of an international law enforcement operation. [...]
