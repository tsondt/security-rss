Title: LockBit alleges it boarded Boeing, stole 'sensitive data'
Date: 2023-10-30T02:30:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-10-30-lockbit-alleges-it-boarded-boeing-stole-sensitive-data

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/30/security_in_brief/){:target="_blank" rel="noopener"}

> ALSO: CISA begs for a consistent budget, Las Vegas school breach; Nigeria arrests six cyber princes, the week's critical vulnerabilities Security In Brief Notorious ransomware gang LockBit has reportedly exfiltrated “a tremendous amount of sensitive data from aerospace outfit Boeing.... [...]
