Title: Massive cybercrime URL shortening service uncovered via DNS data
Date: 2023-10-31T11:23:22-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-10-31-massive-cybercrime-url-shortening-service-uncovered-via-dns-data

[Source](https://www.bleepingcomputer.com/news/security/massive-cybercrime-url-shortening-service-uncovered-via-dns-data/){:target="_blank" rel="noopener"}

> A threat actor that security researchers call Prolific Puma has been providing link shortening services to cybercriminals for at least four years while keeping a sufficiently low profile to operate undetected. [...]
