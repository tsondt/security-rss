Title: Cyberattack on health services provider impacts 5 Canadian hospitals
Date: 2023-10-24T10:18:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-10-24-cyberattack-on-health-services-provider-impacts-5-canadian-hospitals

[Source](https://www.bleepingcomputer.com/news/security/cyberattack-on-health-services-provider-impacts-5-canadian-hospitals/){:target="_blank" rel="noopener"}

> A cyberattack on shared service provider TransForm has impacted operations in five hospitals in Ontario, Canada, impacting patient care and causing appointments to be rescheduled. [...]
