Title: Regulator, insurers and customers all coming for Progress after MOVEit breach
Date: 2023-10-16T02:58:14+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-10-16-regulator-insurers-and-customers-all-coming-for-progress-after-moveit-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/16/infosec_in_brief/){:target="_blank" rel="noopener"}

> Also, CISA cataloging new ransomware data points, 17k WP sites hijacked by malware in Sept., and more critical vulns Infosec in brief The fallout from the exploitation of bugs in Progress Software's MOVEit file transfer software continues, with the US Securities and Exchange Commission (SEC) now investigating the matter, and lots of affected parties seeking compensation.... [...]
