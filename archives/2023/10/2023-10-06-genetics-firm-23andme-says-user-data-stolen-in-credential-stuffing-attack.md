Title: Genetics firm 23andMe says user data stolen in credential stuffing attack
Date: 2023-10-06T11:48:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-10-06-genetics-firm-23andme-says-user-data-stolen-in-credential-stuffing-attack

[Source](https://www.bleepingcomputer.com/news/security/genetics-firm-23andme-says-user-data-stolen-in-credential-stuffing-attack/){:target="_blank" rel="noopener"}

> 23andMe has confirmed to BleepingComputer that it is aware of user data from its platform circulating on hacker forums and attributes the leak to a credential-stuffing attack. [...]
