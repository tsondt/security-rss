Title: 23andMe hit with lawsuits after hacker leaks stolen genetics data
Date: 2023-10-13T16:12:56-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare;Legal
Slug: 2023-10-13-23andme-hit-with-lawsuits-after-hacker-leaks-stolen-genetics-data

[Source](https://www.bleepingcomputer.com/news/security/23andme-hit-with-lawsuits-after-hacker-leaks-stolen-genetics-data/){:target="_blank" rel="noopener"}

> Genetic testing provider 23andMe faces multiple class action lawsuits in the U.S. following a large-scale data breach that is believed to have impacted millions of its customers. [...]
