Title: Microsoft tests Windows 11 encrypted DNS server auto-discovery
Date: 2023-10-25T16:45:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-10-25-microsoft-tests-windows-11-encrypted-dns-server-auto-discovery

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-tests-windows-11-encrypted-dns-server-auto-discovery/){:target="_blank" rel="noopener"}

> Microsoft is testing support for the Discovery of Network-designated Resolvers (DNR) internet standard, which enables automated client-side discovery of encrypted DNS servers on local area networks. [...]
