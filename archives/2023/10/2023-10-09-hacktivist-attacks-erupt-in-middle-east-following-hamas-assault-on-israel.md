Title: Hacktivist attacks erupt in Middle East following Hamas assault on Israel
Date: 2023-10-09T13:00:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-09-hacktivist-attacks-erupt-in-middle-east-following-hamas-assault-on-israel

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/09/hacktivism_middle_east/){:target="_blank" rel="noopener"}

> Groups range from known collectives to new outfits eager to raise their profile Hacktivism efforts have proliferated rapidly in the Middle East following the official announcement of a war between Palestine and Israel.... [...]
