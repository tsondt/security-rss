Title: Casio discloses data breach impacting customers in 149 countries
Date: 2023-10-19T07:37:48-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-19-casio-discloses-data-breach-impacting-customers-in-149-countries

[Source](https://www.bleepingcomputer.com/news/security/casio-discloses-data-breach-impacting-customers-in-149-countries/){:target="_blank" rel="noopener"}

> Japanese electronics manufacturer Casio disclosed a data breach impacting customers from 149 countries after hackers gained to the servers of its ClassPad education platform. [...]
