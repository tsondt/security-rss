Title: Microsoft Exchange gets ‘better’ patch to mitigate critical bug
Date: 2023-10-10T16:03:23-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-10-10-microsoft-exchange-gets-better-patch-to-mitigate-critical-bug

[Source](https://www.bleepingcomputer.com/news/security/microsoft-exchange-gets-better-patch-to-mitigate-critical-bug/){:target="_blank" rel="noopener"}

> The Exchange Team asked admins to deploy a new and "better" patch for a critical Microsoft Exchange Server vulnerability initially addressed in August. [...]
