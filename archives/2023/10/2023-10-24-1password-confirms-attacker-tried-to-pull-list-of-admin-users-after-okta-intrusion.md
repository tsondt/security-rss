Title: 1Password confirms attacker tried to pull list of admin users after Okta intrusion
Date: 2023-10-24T15:15:23+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-24-1password-confirms-attacker-tried-to-pull-list-of-admin-users-after-okta-intrusion

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/24/1password_confirms_all_logins_are/){:target="_blank" rel="noopener"}

> Says logins are safe, as high-profile customers complain they knew about the breach before Okta 1Password is confirming it was attacked by cyber criminals after Okta was breached for the second time in as many years, but says customers' login details are safe.... [...]
