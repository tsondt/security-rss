Title: Cisco patches IOS XE zero-days used to hack over 50,000 devices
Date: 2023-10-23T10:08:05-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-10-23-cisco-patches-ios-xe-zero-days-used-to-hack-over-50000-devices

[Source](https://www.bleepingcomputer.com/news/security/cisco-patches-ios-xe-zero-days-used-to-hack-over-50-000-devices/){:target="_blank" rel="noopener"}

> Cisco has addressed the two vulnerabilities (CVE-2023-20198 and CVE-2023-20273) that hackers exploited to compromise tens of thousands of IOS XE devices over the past week. [...]
