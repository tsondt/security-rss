Title: Dozens of countries will pledge to stop paying ransomware gangs
Date: 2023-10-31T12:54:50-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Government
Slug: 2023-10-31-dozens-of-countries-will-pledge-to-stop-paying-ransomware-gangs

[Source](https://www.bleepingcomputer.com/news/security/dozens-of-countries-will-pledge-to-stop-paying-ransomware-gangs/){:target="_blank" rel="noopener"}

> An alliance of 40 countries will sign a pledge during the third annual International Counter-Ransomware Initiative summit in Washington, D.C., to stop paying ransoms demanded by cybercriminal groups. [...]
