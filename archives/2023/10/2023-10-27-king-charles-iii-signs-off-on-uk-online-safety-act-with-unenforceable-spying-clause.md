Title: King Charles III signs off on UK Online Safety Act, with unenforceable spying clause
Date: 2023-10-27T09:51:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-10-27-king-charles-iii-signs-off-on-uk-online-safety-act-with-unenforceable-spying-clause

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/27/online_safety_act_charles/){:target="_blank" rel="noopener"}

> It's now up to Ofcom to sort out this messy legislation With the assent of King Charles, the United Kingdom's Online Safety Act has become law, one that the British government says will "make the UK the safest place in the world to be online."... [...]
