Title: The Week in Ransomware - October 20th 2023 - Fighting Back
Date: 2023-10-21T11:05:10-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-10-21-the-week-in-ransomware-october-20th-2023-fighting-back

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-october-20th-2023-fighting-back/){:target="_blank" rel="noopener"}

> This was a bad week for ransomware, with the Trigona ransomware suffering a data breach and law enforcement disrupting the RagnarLocker ransomware operation. [...]
