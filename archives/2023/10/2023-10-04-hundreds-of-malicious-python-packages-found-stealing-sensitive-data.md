Title: Hundreds of malicious Python packages found stealing sensitive data
Date: 2023-10-04T17:31:38-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-04-hundreds-of-malicious-python-packages-found-stealing-sensitive-data

[Source](https://www.bleepingcomputer.com/news/security/hundreds-of-malicious-python-packages-found-stealing-sensitive-data/){:target="_blank" rel="noopener"}

> A malicious campaign that researchers observed growing more complex over the past half year, has been planting on open-source platforms hundreds of info-stealing packages that counted about 75,000 downloads. [...]
