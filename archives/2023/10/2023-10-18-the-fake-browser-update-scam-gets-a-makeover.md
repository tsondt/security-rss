Title: The Fake Browser Update Scam Gets a Makeover
Date: 2023-10-18T14:03:28+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Web Fraud 2.0;Binance Smart Chain;BNB Smart Chain;ClearFake;Dusty Miller;Guardio Labs;Nati Tal;Oleg Zaytsev;Randy McEoin
Slug: 2023-10-18-the-fake-browser-update-scam-gets-a-makeover

[Source](https://krebsonsecurity.com/2023/10/the-fake-browser-update-scam-gets-a-makeover/){:target="_blank" rel="noopener"}

> One of the oldest malware tricks in the book — hacked websites claiming visitors need to update their Web browser before they can view any content — has roared back to life in the past few months. New research shows the attackers behind one such scheme have developed an ingenious way of keeping their malware from being taken down by security experts or law enforcement: By hosting the malicious files on a decentralized, anonymous cryptocurrency blockchain. In August 2023, security researcher Randy McEoin blogged about a scam he dubbed ClearFake, which uses hacked WordPress sites to serve visitors with a [...]
