Title: Phishers Spoof USPS, 12 Other Natl’ Postal Services
Date: 2023-10-09T20:39:43+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Web Fraud 2.0;@chenlun;Alibaba;Correos.es;dnslytics.com;domaintools;Poste Italiane;Posti;PostNL;PostNord;UA-80133954-3;urlscan.io;USPS
Slug: 2023-10-09-phishers-spoof-usps-12-other-natl-postal-services

[Source](https://krebsonsecurity.com/2023/10/phishers-spoof-usps-12-other-natl-postal-services/){:target="_blank" rel="noopener"}

> The fake USPS phishing page. Recent weeks have seen a sizable uptick in the number of phishing scams targeting U.S. Postal Service (USPS) customers. Here’s a look at an extensive SMS phishing operation that tries to steal personal and financial data by spoofing the USPS, as well as postal services in at least a dozen other countries. KrebsOnSecurity recently heard from a reader who received an SMS purporting to have been sent by the USPS, saying there was a problem with a package destined for the reader’s address. Clicking the link in the text message brings one to the domain [...]
