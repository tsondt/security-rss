Title: BLOODALCHEMY provides backdoor to southeast Asian nations' secrets
Date: 2023-10-16T15:15:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-16-bloodalchemy-provides-backdoor-to-southeast-asian-nations-secrets

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/16/bloodalchemy_backdoor/){:target="_blank" rel="noopener"}

> Sophisticated malware devs believed to be behind latest addition to toolset of China-aligned attackers Security researchers have uncovered a backdoor used in attacks against governments and organizations in the Association of Southeast Asian Nations (ASEAN).... [...]
