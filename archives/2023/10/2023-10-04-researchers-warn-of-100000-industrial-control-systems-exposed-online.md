Title: Researchers warn of 100,000 industrial control systems exposed online
Date: 2023-10-04T13:35:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-04-researchers-warn-of-100000-industrial-control-systems-exposed-online

[Source](https://www.bleepingcomputer.com/news/security/researchers-warn-of-100-000-industrial-control-systems-exposed-online/){:target="_blank" rel="noopener"}

> About 100,000 industrial control systems (ICS) were found on the public web, exposed to attackers probing them for vulnerabilities and at risk of unauthorized access. Among them are power grids, traffic light systems, security and water systems. [...]
