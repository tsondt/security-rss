Title: Everest cybercriminals offer corporate insiders cold, hard cash for remote access
Date: 2023-10-12T12:42:43+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-12-everest-cybercriminals-offer-corporate-insiders-cold-hard-cash-for-remote-access

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/12/everest_courting_corporate_insiders/){:target="_blank" rel="noopener"}

> The ransomware gang changes identities more than Jason Bourne The Everest ransomware group is stepping up its efforts to purchase access to corporate networks directly from employees amid what researchers believe to be a major transition for the cybercriminals.... [...]
