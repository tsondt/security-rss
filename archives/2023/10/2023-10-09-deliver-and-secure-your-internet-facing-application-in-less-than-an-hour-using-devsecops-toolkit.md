Title: Deliver and secure your internet-facing application in less than an hour using Dev(Sec)Ops Toolkit
Date: 2023-10-09T18:08:38+00:00
Author: David Tu
Category: GCP Security
Tags: DevOps & SRE;Security & Identity;Networking
Slug: 2023-10-09-deliver-and-secure-your-internet-facing-application-in-less-than-an-hour-using-devsecops-toolkit

[Source](https://cloud.google.com/blog/products/networking/introducing-the-devsecops-toolkit/){:target="_blank" rel="noopener"}

> We are excited to announce the preview of the Dev(Sec)Ops toolkit for global front-end internet-facing applications, which can help you launch new apps on Google Cloud in less than an hour. This toolkit is part of the recently announced Cross-Cloud Network solution to help customers scale and secure their applications. The toolkit provides an out-of-the-box, expert-curated solution to accelerate the delivery of internet-facing applications. A sample application included in the toolkit demonstrates how customers can quickly integrate Cloud Load Balancing, Cloud Armor, and Cloud CDN according to the provided reference architecture. The toolkit supports deploying applications via Cloud Build or [...]
