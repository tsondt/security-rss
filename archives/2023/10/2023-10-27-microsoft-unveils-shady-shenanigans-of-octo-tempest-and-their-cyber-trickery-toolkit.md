Title: Microsoft unveils shady shenanigans of Octo Tempest and their cyber-trickery toolkit
Date: 2023-10-27T12:43:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-27-microsoft-unveils-shady-shenanigans-of-octo-tempest-and-their-cyber-trickery-toolkit

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/27/octo_tempest_microsoft/){:target="_blank" rel="noopener"}

> Gang thought to be behind attack on MGM Resorts has a skillset larger than most cybercrime groups in existence Microsoft's latest report on "one of the most dangerous financial criminal groups" operating offers security pros an abundance of threat intelligence to protect themselves from its myriad tactics.... [...]
