Title: The Future of Drone Warfare
Date: 2023-10-31T11:03:37+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;drones;military;war
Slug: 2023-10-31-the-future-of-drone-warfare

[Source](https://www.schneier.com/blog/archives/2023/10/the-future-of-drone-warfare.html){:target="_blank" rel="noopener"}

> Ukraine is using $400 drones to destroy tanks: Facing an enemy with superior numbers of troops and armor, the Ukrainian defenders are holding on with the help of tiny drones flown by operators like Firsov that, for a few hundred dollars, can deliver an explosive charge capable of destroying a Russian tank worth more than $2 million. [...] A typical FPV weighs up to one kilogram, has four small engines, a battery, a frame and a camera connected wirelessly to goggles worn by a pilot operating it remotely. It can carry up to 2.5 kilograms of explosives and strike a [...]
