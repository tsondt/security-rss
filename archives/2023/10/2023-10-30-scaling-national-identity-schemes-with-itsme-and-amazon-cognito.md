Title: Scaling national identity schemes with itsme and Amazon Cognito
Date: 2023-10-30T17:40:39+00:00
Author: Guillaume Neau
Category: AWS Security
Tags: Amazon Cognito;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2023-10-30-scaling-national-identity-schemes-with-itsme-and-amazon-cognito

[Source](https://aws.amazon.com/blogs/security/scaling-national-identity-schemes-with-itsme-and-amazon-cognito/){:target="_blank" rel="noopener"}

> In this post, we demonstrate how you can use identity federation and integration between the identity provider itsme® and Amazon Cognito to quickly consume and build digital services for citizens on Amazon Web Services (AWS) using available national digital identities. We also provide code examples and integration proofs of concept to get you started quickly. [...]
