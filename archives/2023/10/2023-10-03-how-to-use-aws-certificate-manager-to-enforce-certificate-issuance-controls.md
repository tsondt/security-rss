Title: How to use AWS Certificate Manager to enforce certificate issuance controls
Date: 2023-10-03T21:09:23+00:00
Author: Roger Park
Category: AWS Security
Tags: AWS Certificate Manager;AWS Identity and Access Management (IAM);Intermediate (200);Security, Identity, & Compliance;Technical How-to;AWS Identity and Access Management;Security Blog
Slug: 2023-10-03-how-to-use-aws-certificate-manager-to-enforce-certificate-issuance-controls

[Source](https://aws.amazon.com/blogs/security/how-to-use-aws-certificate-manager-to-enforce-certificate-issuance-controls/){:target="_blank" rel="noopener"}

> AWS Certificate Manager (ACM) lets you provision, manage, and deploy public and private Transport Layer Security (TLS) certificates for use with AWS services and your internal connected resources. You probably have many users, applications, or accounts that request and use TLS certificates as part of your public key infrastructure (PKI); which means you might also need [...]
