Title: Stanford schooled in cybersecurity after Akira claims ransomware attack
Date: 2023-10-30T14:45:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-30-stanford-schooled-in-cybersecurity-after-akira-claims-ransomware-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/30/stanford_university_confirms_investigation_into/){:target="_blank" rel="noopener"}

> This marks the third criminal intrusion at the institution in as many years Stanford University has confirmed it is "investigating a cybersecurity incident" after an attack last week by the Akira ransomware group.... [...]
