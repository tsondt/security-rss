Title: Cloud CISO Perspectives: How boards can help cyber-crisis communications
Date: 2023-10-16T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2023-10-16-cloud-ciso-perspectives-how-boards-can-help-cyber-crisis-communications

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-how-boards-can-help-cyber-crisis-communications/){:target="_blank" rel="noopener"}

> Welcome to the first Cloud CISO Perspectives for October 2023. This month, I’ll be discussing the increasingly-important (and often undervalued) organizational skill of crisis communications — and how boards can help prepare their organizations for the inevitable. Effective crisis communications was a central pillar of our third Perspectives on Security for the Board report, published last week. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. aside_block <ListValue: [StructValue([('title', 'Board of Directors [...]
