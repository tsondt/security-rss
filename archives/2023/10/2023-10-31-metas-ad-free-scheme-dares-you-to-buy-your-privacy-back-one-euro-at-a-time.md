Title: Meta's ad-free scheme dares you to buy your privacy back, one euro at a time
Date: 2023-10-31T09:30:07+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-10-31-metas-ad-free-scheme-dares-you-to-buy-your-privacy-back-one-euro-at-a-time

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/31/meta_ad_free_europe/){:target="_blank" rel="noopener"}

> If you're in the EU, EEA, or Switzerland From November, it will be possible to pay Meta to stop shoveling ads in your Instagram or Facebook feeds and slurping your data for marketing purposes so long as you live in the EU, EEA, or Switzerland.... [...]
