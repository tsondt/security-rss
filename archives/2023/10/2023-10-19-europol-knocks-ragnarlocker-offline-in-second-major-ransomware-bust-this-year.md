Title: Europol knocks RagnarLocker offline in second major ransomware bust this year
Date: 2023-10-19T16:30:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-19-europol-knocks-ragnarlocker-offline-in-second-major-ransomware-bust-this-year

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/19/europol_knocks_ragnarlocker_offline/){:target="_blank" rel="noopener"}

> Group will be remembered as staunch negotiator and a bullier of critical infrastructure orgs Law enforcement agencies have taken over RagnarLocker ransomware group's leak site in an internationally coordinated takedown.... [...]
