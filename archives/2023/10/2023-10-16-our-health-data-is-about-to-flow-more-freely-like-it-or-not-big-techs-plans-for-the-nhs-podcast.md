Title: ‘Our health data is about to flow more freely, like it or not’: big tech’s plans for the NHS – podcast
Date: 2023-10-16T04:00:30+00:00
Author: Written by Cori Crider, read by Kate Handford and produced by Nicola Alexandrou. The executive producer was Ellie Bury
Category: The Guardian
Tags: NHS;Data and computer security;Health;Health policy;Healthcare industry;Peter Thiel
Slug: 2023-10-16-our-health-data-is-about-to-flow-more-freely-like-it-or-not-big-techs-plans-for-the-nhs-podcast

[Source](https://www.theguardian.com/news/audio/2023/oct/16/our-health-data-is-about-to-flow-more-freely-like-it-or-not-big-techs-plans-for-the-nhs-podcast){:target="_blank" rel="noopener"}

> The government is about to award a £480m contract to build a vast new database of patient data. But if people don’t trust it, they’ll opt out – I know, because I felt I had to Continue reading... [...]
