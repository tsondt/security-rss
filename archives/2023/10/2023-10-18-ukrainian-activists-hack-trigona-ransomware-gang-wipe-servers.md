Title: Ukrainian activists hack Trigona ransomware gang, wipe servers
Date: 2023-10-18T19:17:43-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-10-18-ukrainian-activists-hack-trigona-ransomware-gang-wipe-servers

[Source](https://www.bleepingcomputer.com/news/security/ukrainian-activists-hack-trigona-ransomware-gang-wipe-servers/){:target="_blank" rel="noopener"}

> A group of cyber activists under the Ukrainian Cyber Alliance banner has hacked the servers of the Trigona ransomware gang and wiped them clean after copying all the information available. [...]
