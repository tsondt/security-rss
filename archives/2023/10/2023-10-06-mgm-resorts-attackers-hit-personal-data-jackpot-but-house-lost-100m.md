Title: MGM Resorts attackers hit personal data jackpot, but house lost $100M
Date: 2023-10-06T15:30:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-06-mgm-resorts-attackers-hit-personal-data-jackpot-but-house-lost-100m

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/06/mgm_resorts_cyberattack_cost/){:target="_blank" rel="noopener"}

> Racecars and cyber insurance will balance its books in no time, though MGM Resorts has admitted that the cyberattack it suffered in September will likely cost the company at least $100 million.... [...]
