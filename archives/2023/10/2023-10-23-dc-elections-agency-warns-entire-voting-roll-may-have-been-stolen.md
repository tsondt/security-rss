Title: DC elections agency warns entire voting roll may have been stolen
Date: 2023-10-23T19:15:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-10-23-dc-elections-agency-warns-entire-voting-roll-may-have-been-stolen

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/23/washington_elections_agency_breach/){:target="_blank" rel="noopener"}

> Home of the Republic seemingly hit by Sony/NTT Docomo ransomware crew The US Capitol's election agency says a ransomware crew might have stolen its entire voter roll, which includes the personal information of all registered voters in the District of Columbia.... [...]
