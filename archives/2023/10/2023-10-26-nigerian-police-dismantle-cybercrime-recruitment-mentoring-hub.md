Title: Nigerian Police dismantle cybercrime recruitment, mentoring hub
Date: 2023-10-26T13:52:41-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-10-26-nigerian-police-dismantle-cybercrime-recruitment-mentoring-hub

[Source](https://www.bleepingcomputer.com/news/security/nigerian-police-dismantle-cybercrime-recruitment-mentoring-hub/){:target="_blank" rel="noopener"}

> The Nigerian Police Form has arrested six suspects and dismantled a mentoring hub linked to cybercrime activities, including business email compromise, romance, and investment scams. [...]
