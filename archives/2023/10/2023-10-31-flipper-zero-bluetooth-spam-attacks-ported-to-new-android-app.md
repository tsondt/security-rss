Title: Flipper Zero Bluetooth spam attacks ported to new Android app
Date: 2023-10-31T16:06:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-10-31-flipper-zero-bluetooth-spam-attacks-ported-to-new-android-app

[Source](https://www.bleepingcomputer.com/news/security/flipper-zero-bluetooth-spam-attacks-ported-to-new-android-app/){:target="_blank" rel="noopener"}

> Recent Flipper Zero Bluetooth spam attacks have now been ported to an Android app, allowing a much larger number of devices to implement these annoying spam alerts. [...]
