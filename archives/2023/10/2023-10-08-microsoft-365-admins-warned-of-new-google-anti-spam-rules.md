Title: Microsoft 365 admins warned of new Google anti-spam rules
Date: 2023-10-08T11:09:08-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-08-microsoft-365-admins-warned-of-new-google-anti-spam-rules

[Source](https://www.bleepingcomputer.com/news/security/microsoft-365-admins-warned-of-new-google-anti-spam-rules/){:target="_blank" rel="noopener"}

> Microsoft urged Microsoft 365 email senders this week to authenticate outbound messages following new anti-spam rules for bulk senders announced earlier this week by Google. [...]
