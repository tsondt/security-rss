Title: Russian Sandworm hackers breached 11 Ukrainian telcos since May
Date: 2023-10-16T14:06:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-16-russian-sandworm-hackers-breached-11-ukrainian-telcos-since-may

[Source](https://www.bleepingcomputer.com/news/security/russian-sandworm-hackers-breached-11-ukrainian-telcos-since-may/){:target="_blank" rel="noopener"}

> The state-sponsored Russian hacking group tracked as 'Sandworm' has compromised eleven telecommunication service providers in Ukraine between May and September 2023. [...]
