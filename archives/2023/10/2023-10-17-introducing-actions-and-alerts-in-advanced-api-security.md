Title: Introducing Actions and Alerts in Advanced API Security
Date: 2023-10-17T16:00:00+00:00
Author: Varun Krovvidi
Category: GCP Security
Tags: API Management;Security & Identity
Slug: 2023-10-17-introducing-actions-and-alerts-in-advanced-api-security

[Source](https://cloud.google.com/blog/products/identity-security/introducing-actions-and-alerts-in-advanced-api-security/){:target="_blank" rel="noopener"}

> APIs provide direct access to application functionality and data, making them a powerful developer tool. Unfortunately, that also makes them a favorite target for threat actors. Proactively identifying API security threats is top of mind for 60% of IT leaders according to Google Cloud’s 2022 API Security Research Report. Most of the current approaches to securing APIs focus on detecting security vulnerabilities, but rapidly reacting and responding to API security issues once they are detected is just as important in maintaining a strong application security posture. This is where Advanced API Security for Apigee API Management can help. It’s an [...]
