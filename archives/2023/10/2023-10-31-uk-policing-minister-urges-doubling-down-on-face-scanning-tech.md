Title: UK policing minister urges doubling down on face-scanning tech
Date: 2023-10-31T12:30:12+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2023-10-31-uk-policing-minister-urges-doubling-down-on-face-scanning-tech

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/31/uk_police_minister_lfr/){:target="_blank" rel="noopener"}

> 'No question' it will solve more crimes, Tory MP claims A UK minister for policing has called for forces to double their use of algorithmic-assisted facial recognition in a bid to snare more criminals.... [...]
