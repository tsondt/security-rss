Title: Hackers use Binance Smart Chain contracts to store malicious scripts
Date: 2023-10-13T12:08:55-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-10-13-hackers-use-binance-smart-chain-contracts-to-store-malicious-scripts

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-binance-smart-chain-contracts-to-store-malicious-scripts/){:target="_blank" rel="noopener"}

> Cybercriminals are employing a novel code distribution technique dubbed 'EtherHiding,' which abuses Binance's Smart Chain (BSC) contracts to hide malicious scripts in the blockchain. [...]
