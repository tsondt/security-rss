Title: Ransomware attacks now target unpatched WS_FTP servers
Date: 2023-10-12T15:10:39-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-12-ransomware-attacks-now-target-unpatched-ws_ftp-servers

[Source](https://www.bleepingcomputer.com/news/security/ransomware-attacks-now-target-unpatched-ws-ftp-servers/){:target="_blank" rel="noopener"}

> Internet-exposed WS_FTP servers unpatched against a maximum severity vulnerability are now targeted in ransomware attacks. [...]
