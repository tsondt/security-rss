Title: Fighting off cyberattacks? Make sure user credentials aren’t compromised
Date: 2023-10-17T10:02:04-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-10-17-fighting-off-cyberattacks-make-sure-user-credentials-arent-compromised

[Source](https://www.bleepingcomputer.com/news/security/fighting-off-cyberattacks-make-sure-user-credentials-arent-compromised/){:target="_blank" rel="noopener"}

> Login credential theft presents one of the biggest and most enduring cybersecurity problems. This article by Specops SOftware looks at the motivations driving credential theft and the tactics bad actors are likely to use. [...]
