Title: US v Sam Bankman-Fried trial begins ... as imploded crypto-biz boss sues his insurer
Date: 2023-10-03T23:47:14+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-10-03-us-v-sam-bankman-fried-trial-begins-as-imploded-crypto-biz-boss-sues-his-insurer

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/03/sbfd_trial_insurance_lawsuit/){:target="_blank" rel="noopener"}

> After people's funds go up in smoke, ex-CEO seeks cash to foot legal bills The first of two US government prosecutions of former FTX CEO Sam Bankman-Fried commenced in New York on Monday, only a day after the cryptocurrency tycoon sued his own insurance company for failing to cover his legal costs.... [...]
