Title: ServiceNow quietly addresses unauthenticated data exposure flaw from 2015
Date: 2023-10-26T08:30:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-26-servicenow-quietly-addresses-unauthenticated-data-exposure-flaw-from-2015

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/26/servicenow_data_exposure_flaw/){:target="_blank" rel="noopener"}

> Researcher who publicized issue brands company’s communication 'appalling' ServiceNow is issuing a fix for a flaw that exposes data after a researcher published a method for unauthenticated attackers to steal an organization's sensitive files.... [...]
