Title: Google promises Germany to creep on users less after market power probe
Date: 2023-10-06T11:56:01+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-10-06-google-promises-germany-to-creep-on-users-less-after-market-power-probe

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/06/google_bundeskartellamt_data_processing/){:target="_blank" rel="noopener"}

> Regulation complements EU's Digital Markets Act to cover more services Google has committed to being a little less creepy with user data in response to proceedings from the German Federal Cartel Office (Bundeskartellamt).... [...]
