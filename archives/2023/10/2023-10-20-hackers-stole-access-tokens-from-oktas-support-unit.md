Title: Hackers Stole Access Tokens from Okta’s Support Unit
Date: 2023-10-20T18:39:23+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;BeyondTrust;Charlotte Wylie;Marc Maiffret;Okta;Okta breach
Slug: 2023-10-20-hackers-stole-access-tokens-from-oktas-support-unit

[Source](https://krebsonsecurity.com/2023/10/hackers-stole-access-tokens-from-oktas-support-unit/){:target="_blank" rel="noopener"}

> Okta, a company that provides identity tools like multi-factor authentication and single sign-on to thousands of businesses, has suffered a security breach involving a compromise of its customer support unit, KrebsOnSecurity has learned. Okta says the incident affected a “very small number” of customers, however it appears the hackers responsible had access to Okta’s support platform for at least two weeks before the company fully contained the intrusion. In an advisory sent to an undisclosed number of customers on Oct. 19, Okta said it “has identified adversarial activity that leveraged access to a stolen credential to access Okta’s support case [...]
