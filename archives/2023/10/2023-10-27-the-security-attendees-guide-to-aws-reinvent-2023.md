Title: The security attendee’s guide to AWS re:Invent 2023
Date: 2023-10-27T14:05:12+00:00
Author: Katie Collins
Category: AWS Security
Tags: Announcements;AWS re:Invent;Events;Foundational (100);Security, Identity, & Compliance;AWS Re:Invent;Live Events;re:Invent 2023;Security Blog
Slug: 2023-10-27-the-security-attendees-guide-to-aws-reinvent-2023

[Source](https://aws.amazon.com/blogs/security/the-security-attendees-guide-to-aws-reinvent-2023/){:target="_blank" rel="noopener"}

> AWS re:Invent 2023 is fast approaching, and we can’t wait to see you in Las Vegas in November. re:Invent offers you the chance to come together with cloud enthusiasts from around the world to hear the latest cloud industry innovations, meet with Amazon Web Services (AWS) experts, and build connections. This post will highlight key [...]
