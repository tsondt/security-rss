Title: Patch Tuesday, October 2023 Edition
Date: 2023-10-10T22:51:31+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Security Tools;Time to Patch;Adam Barnett;Amazon;apple;CloudFlare;CVE-2023-35349;CVE-2023-36563;CVE-2023-36778;CVE-2023-41763;CVE-2023-44487;Damian Menscher;google;Immersive Labs;iOS 17.0.3;iPadOS 17.0.3;libvpx;microsoft;Natalie Silva;Patch Tuesday October 2023;Rapid Reset Attack;Rapid7;Skype for Business;windows;Wordpad
Slug: 2023-10-10-patch-tuesday-october-2023-edition

[Source](https://krebsonsecurity.com/2023/10/patch-tuesday-october-2023-edition/){:target="_blank" rel="noopener"}

> Microsoft today issued security updates for more than 100 newly-discovered vulnerabilities in its Windows operating system and related software, including four flaws that are already being exploited. In addition, Apple recently released emergency updates to quash a pair of zero-day bugs in iOS. Apple last week shipped emergency updates in iOS 17.0.3 and iPadOS 17.0.3 in response to active attacks. The patch fixes CVE-2023-42724, which attackers have been using in targeted attacks to elevate their access on a local device. Apple said it also patched CVE-2023-5217, which is not listed as a zero-day bug. However, as Bleeping Computer pointed out, [...]
