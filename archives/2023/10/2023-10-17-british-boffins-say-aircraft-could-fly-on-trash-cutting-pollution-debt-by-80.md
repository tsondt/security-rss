Title: British boffins say aircraft could fly on trash, cutting pollution debt by 80%
Date: 2023-10-17T07:30:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-10-17-british-boffins-say-aircraft-could-fly-on-trash-cutting-pollution-debt-by-80

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/17/sustainable_jet_fuel/){:target="_blank" rel="noopener"}

> Domestic jets can use 'municipal solid waste' to fly the friendly skies Sustainable aviation fuels (SAFs) made from sources other than fossil fuels have the potential to reduce emissions by up to 80 percent, UK researchers have found.... [...]
