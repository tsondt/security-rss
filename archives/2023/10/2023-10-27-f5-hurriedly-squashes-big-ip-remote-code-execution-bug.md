Title: F5 hurriedly squashes BIG-IP remote code execution bug
Date: 2023-10-27T17:34:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-27-f5-hurriedly-squashes-big-ip-remote-code-execution-bug

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/27/f5_hurriedly_fixes_bigip_remote/){:target="_blank" rel="noopener"}

> Fixes came earlier than scheduled as vulnerability became known to outsiders F5 has issued a fix for a remote code execution (RCE) bug in its BIG-IP suite carrying a near-maximum severity score.... [...]
