Title: Friday Squid Blogging: Why There Are No Giant Squid in Aquariums
Date: 2023-10-20T21:03:46+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-10-20-friday-squid-blogging-why-there-are-no-giant-squid-in-aquariums

[Source](https://www.schneier.com/blog/archives/2023/10/friday-squid-blogging-why-there-are-no-giant-squid-in-aquariums.html){:target="_blank" rel="noopener"}

> They’re too big and we can’t recreate their habitat. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
