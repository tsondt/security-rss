Title: Hacking Scandinavian Alcohol Tax
Date: 2023-10-30T11:10:00+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;A Hacker's Mind;hacking;noncomputer hacks
Slug: 2023-10-30-hacking-scandinavian-alcohol-tax

[Source](https://www.schneier.com/blog/archives/2023/10/hacking-scandinavian-alcohol-tax.html){:target="_blank" rel="noopener"}

> The islands of Åland are an important tax hack : Although Åland is part of the Republic of Finland, it has its own autonomous parliament. In areas where Åland has its own legislation, the group of islands essentially operates as an independent nation. This allows Scandinavians to avoid the notoriously high alcohol taxes: Åland is a member of the EU and its currency is the euro, but Åland’s relationship with the EU is regulated by way of a special protocol. In order to maintain the important sale of duty-free goods on ferries operating between Finland and Sweden, Åland is not [...]
