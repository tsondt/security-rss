Title: HM Government has partnered with SANS to train cyber security experts
Date: 2023-10-12T08:42:14+00:00
Author: SANS
Category: The Register
Tags: 
Slug: 2023-10-12-hm-government-has-partnered-with-sans-to-train-cyber-security-experts

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/12/hm_government_has_partnered_with/){:target="_blank" rel="noopener"}

> Partner Content According to the Cyber Security Breaches Survey 26 percent of medium businesses, 37 percent of large businesses and 25 percent of high-income charities have experienced cyber crime in the last 12 months.... [...]
