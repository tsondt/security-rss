Title: US construction giant unearths concrete evidence of cyberattack
Date: 2023-10-12T10:55:10+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-10-12-us-construction-giant-unearths-concrete-evidence-of-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/12/simpson_manufacturing_security_incident/){:target="_blank" rel="noopener"}

> Simpson Manufacturing yanks systems offline, warns of ongoing disruption Simpson Manufacturing Company yanked some tech systems offline this week to contain a cyberattack it expects will "continue to cause disruption."... [...]
