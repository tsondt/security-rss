Title: Hackers update Cisco IOS XE backdoor to hide infected devices
Date: 2023-10-22T13:37:01-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-10-22-hackers-update-cisco-ios-xe-backdoor-to-hide-infected-devices

[Source](https://www.bleepingcomputer.com/news/security/hackers-update-cisco-ios-xe-backdoor-to-hide-infected-devices/){:target="_blank" rel="noopener"}

> The number of Cisco IOS XE devices detected with a malicious backdoor implant has plummeted from over 50,000 impacted devices to only a few hundred after the attackers updated the backdoor to hide infected systems from scans. [...]
