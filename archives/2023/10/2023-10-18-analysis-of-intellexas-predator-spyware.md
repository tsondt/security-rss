Title: Analysis of Intellexa’s Predator Spyware
Date: 2023-10-18T11:06:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberweapons;privacy;spyware;surveillance
Slug: 2023-10-18-analysis-of-intellexas-predator-spyware

[Source](https://www.schneier.com/blog/archives/2023/10/analysis-of-intellexas-predator-spyware.html){:target="_blank" rel="noopener"}

> Amnesty International has published a comprehensive analysis of the Predator government spyware products. These technologies used to be the exclusive purview of organizations like the NSA. Now they’re available to every country on the planet—democratic, nondemocratic, authoritarian, whatever—for a price. This is the legacy of not securing the Internet when we could have. [...]
