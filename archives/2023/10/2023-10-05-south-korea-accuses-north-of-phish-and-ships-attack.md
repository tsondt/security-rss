Title: South Korea accuses North of Phish and Ships attack
Date: 2023-10-05T05:29:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-10-05-south-korea-accuses-north-of-phish-and-ships-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/05/north_korea_phishing_attack_on_south/){:target="_blank" rel="noopener"}

> Kim Jong-un looks at industry's progress with green eyes, says South Korea's spy agency South Korea's National Intelligence Service (NIS) has warned North Korea is attacking its shipbuilding sector.... [...]
