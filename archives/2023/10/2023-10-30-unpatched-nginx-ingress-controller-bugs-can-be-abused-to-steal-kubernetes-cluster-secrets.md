Title: Unpatched NGINX ingress controller bugs can be abused to steal Kubernetes cluster secrets
Date: 2023-10-30T20:00:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-10-30-unpatched-nginx-ingress-controller-bugs-can-be-abused-to-steal-kubernetes-cluster-secrets

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/30/unpatched_nginx_ingress_controller_bugs/){:target="_blank" rel="noopener"}

> Just tricks, no treats with these 3 vulns Three unpatched high-severity bugs in the NGINX ingress controller can be abused by miscreants to steal credentials and other secrets from Kubernetes clusters.... [...]
