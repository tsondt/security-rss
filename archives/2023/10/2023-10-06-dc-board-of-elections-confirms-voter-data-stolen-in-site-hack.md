Title: D.C. Board of Elections confirms voter data stolen in site hack
Date: 2023-10-06T19:07:50-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-06-dc-board-of-elections-confirms-voter-data-stolen-in-site-hack

[Source](https://www.bleepingcomputer.com/news/security/dc-board-of-elections-confirms-voter-data-stolen-in-site-hack/){:target="_blank" rel="noopener"}

> The District of Columbia Board of Elections (DCBOE) is currently probing a data leak involving an unknown number of voter records following breach claims from a threat actor known as RansomedVC. [...]
