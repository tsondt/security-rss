Title: “Cisco buried the lede.” >10,000 network devices backdoored through unpatched 0-day
Date: 2023-10-17T18:40:47+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security
Slug: 2023-10-17-cisco-buried-the-lede-10000-network-devices-backdoored-through-unpatched-0-day

[Source](https://arstechnica.com/?p=1976348){:target="_blank" rel="noopener"}

> Enlarge / Cables run into a Cisco data switch. (credit: Getty Images) On Monday, Cisco reported that a critical zero-day vulnerability in devices running IOS XE software was being exploited by an unknown threat actor who was using it to backdoor vulnerable networks. Company researchers described the infections as a "cluster of activity." On Tuesday, researchers from security firm VulnCheck said that at last count, that cluster comprised more than 10,000 switches, routers, and other Cisco devices. All of them, VulnCheck said, have been infected by an implant that allows the threat actor to remotely execute commands that run at [...]
