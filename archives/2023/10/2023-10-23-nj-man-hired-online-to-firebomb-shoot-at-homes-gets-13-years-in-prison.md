Title: NJ Man Hired Online to Firebomb, Shoot at Homes Gets 13 Years in Prison
Date: 2023-10-23T13:08:27+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Patrick McGovern-Allen;violence-as-a-service
Slug: 2023-10-23-nj-man-hired-online-to-firebomb-shoot-at-homes-gets-13-years-in-prison

[Source](https://krebsonsecurity.com/2023/10/nj-man-hired-online-to-firebomb-shoot-at-homes-gets-13-years-in-prison/){:target="_blank" rel="noopener"}

> A 22-year-old New Jersey man has been sentenced to more than 13 years in prison for participating in a firebombing and a shooting at homes in Pennsylvania last year. Patrick McGovern-Allen was the subject of a Sept. 4, 2022 story here about the emergence of “violence-as-a-service” offerings, where random people from the Internet hire themselves out to perform a variety of local, physical attacks, including firebombing a home, “bricking” windows, slashing tires, or performing a drive-by shooting at someone’s residence. McGovern-Allen, of Egg Harbor Township, N.J., was arrested Aug. 12, 2022 on an FBI warrant, which showed he was part [...]
