Title: Irish cops data debacle exposes half a million motorist records
Date: 2023-10-24T10:02:29+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-24-irish-cops-data-debacle-exposes-half-a-million-motorist-records

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/24/irish_national_police_leak/){:target="_blank" rel="noopener"}

> Details of civilians and Garda officers were included, as well as high-res scans of identity documents A third-party contractor running a database without password protection exposed more than 500,000 records related to vehicle seizures by the Irish National Police (An Garda Síochána, "Garda").... [...]
