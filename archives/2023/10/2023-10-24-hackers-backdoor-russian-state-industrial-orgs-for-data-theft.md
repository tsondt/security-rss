Title: Hackers backdoor Russian state, industrial orgs for data theft
Date: 2023-10-24T15:48:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-24-hackers-backdoor-russian-state-industrial-orgs-for-data-theft

[Source](https://www.bleepingcomputer.com/news/security/hackers-backdoor-russian-state-industrial-orgs-for-data-theft/){:target="_blank" rel="noopener"}

> Several state and key industrial organizations in Russia were attacked with a custom Go-based backdoor that performs data theft, likely aiding espionage operations. [...]
