Title: Yes, Singapore immigration plans to scan your face instead of your passport
Date: 2023-10-02T01:00:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-10-02-yes-singapore-immigration-plans-to-scan-your-face-instead-of-your-passport

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/02/singapore_face_scan_passports/){:target="_blank" rel="noopener"}

> No, that does not mean you can leave it at home just yet Last week the internet was abuzz with talk that Singapore's commercial Changi airport was no longer going to require passports for clearance at immigration. Although it is true the paper documentation will be replaced by biometric measures, it's not quite time to pack the document away.... [...]
