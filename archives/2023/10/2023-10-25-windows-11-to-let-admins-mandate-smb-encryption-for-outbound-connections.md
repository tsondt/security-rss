Title: Windows 11 to let admins mandate SMB encryption for outbound connections
Date: 2023-10-25T14:34:39-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-10-25-windows-11-to-let-admins-mandate-smb-encryption-for-outbound-connections

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-11-to-let-admins-mandate-smb-encryption-for-outbound-connections/){:target="_blank" rel="noopener"}

> Windows 11 will let admins mandate SMB client encryption for all outbound connections, starting with today's Windows 11 Insider Preview Build 25982 rolling out to Insiders in the Canary Channel. [...]
