Title: Co-founder of collapsed crypto biz Three Arrows cuffed at airport
Date: 2023-10-03T01:30:15+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-10-03-co-founder-of-collapsed-crypto-biz-three-arrows-cuffed-at-airport

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/03/asia_in_brief/){:target="_blank" rel="noopener"}

> Plus: Philippine state health insurance knocked offline by ransomware, China relaxes data export laws, and more Asia in brief Zhu Su, co-founder of fallen crypto business Three Arrows Capital (3AC), was arrested last Friday at Changi Airport in Singapore as he attempted to leave the country.... [...]
