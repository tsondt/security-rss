Title: Air Europa data breach: Customers warned to cancel credit cards
Date: 2023-10-10T13:45:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-10-air-europa-data-breach-customers-warned-to-cancel-credit-cards

[Source](https://www.bleepingcomputer.com/news/security/air-europa-data-breach-customers-warned-to-cancel-credit-cards/){:target="_blank" rel="noopener"}

> Spanish airline Air Europa, the country's third-largest airline and a member of the SkyTeam alliance, warned customers on Monday to cancel their credit cards after attackers accessed their card information in a recent data breach. [...]
