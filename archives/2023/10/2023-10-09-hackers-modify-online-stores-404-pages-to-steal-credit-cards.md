Title: Hackers modify online stores’ 404 pages to steal credit cards
Date: 2023-10-09T13:59:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-09-hackers-modify-online-stores-404-pages-to-steal-credit-cards

[Source](https://www.bleepingcomputer.com/news/security/hackers-modify-online-stores-404-pages-to-steal-credit-cards/){:target="_blank" rel="noopener"}

> A new Magecart card skimming campaign hijacks the 404 error pages of online retailer's websites, hiding malicious code to steal customers' credit card information. [...]
