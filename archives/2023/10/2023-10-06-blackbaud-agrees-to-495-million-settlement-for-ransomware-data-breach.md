Title: Blackbaud agrees to $49.5 million settlement for ransomware data breach
Date: 2023-10-06T14:43:05-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-06-blackbaud-agrees-to-495-million-settlement-for-ransomware-data-breach

[Source](https://www.bleepingcomputer.com/news/security/blackbaud-agrees-to-495-million-settlement-for-ransomware-data-breach/){:target="_blank" rel="noopener"}

> Cloud computing provider Blackbaud reached a $49.5 million agreement with attorneys general from 49 U.S. states to settle a multi-state investigation of a May 2020 ransomware attack and the resulting data breach. [...]
