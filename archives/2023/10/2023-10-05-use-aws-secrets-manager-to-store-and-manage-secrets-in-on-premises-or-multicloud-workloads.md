Title: Use AWS Secrets Manager to store and manage secrets in on-premises or multicloud workloads
Date: 2023-10-05T14:13:45+00:00
Author: Sreedar Radhakrishnan
Category: AWS Security
Tags: Advanced (300);AWS Secrets Manager;Best Practices;Security, Identity, & Compliance;Technical How-to;IAM Roles Anywhere;Security Blog
Slug: 2023-10-05-use-aws-secrets-manager-to-store-and-manage-secrets-in-on-premises-or-multicloud-workloads

[Source](https://aws.amazon.com/blogs/security/use-aws-secrets-manager-to-store-and-manage-secrets-in-on-premises-or-multicloud-workloads/){:target="_blank" rel="noopener"}

> AWS Secrets Manager helps you manage, retrieve, and rotate database credentials, API keys, and other secrets throughout their lifecycles. You might already use Secrets Manager to store and manage secrets in your applications built on Amazon Web Services (AWS), but what about secrets for applications that are hosted in your on-premises data center, or hosted [...]
