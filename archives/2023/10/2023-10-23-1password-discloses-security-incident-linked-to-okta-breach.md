Title: 1Password discloses security incident linked to Okta breach
Date: 2023-10-23T18:34:52-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-10-23-1password-discloses-security-incident-linked-to-okta-breach

[Source](https://www.bleepingcomputer.com/news/security/1password-discloses-security-incident-linked-to-okta-breach/){:target="_blank" rel="noopener"}

> 1Password, a popular password management platform used by over 100,000 businesses, suffered a security breach after hackers gained access to its Okta ID management tenant. [...]
