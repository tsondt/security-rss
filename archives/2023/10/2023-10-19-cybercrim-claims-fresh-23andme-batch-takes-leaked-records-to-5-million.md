Title: Cybercrim claims fresh 23andMe batch takes leaked records to 5 million
Date: 2023-10-19T16:00:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-19-cybercrim-claims-fresh-23andme-batch-takes-leaked-records-to-5-million

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/19/latest_23andme_data_leak_takes/){:target="_blank" rel="noopener"}

> Class action lawsuits abound after mega breach A cybercriminal claims they've uploaded a second batch of stolen profile data from biotech company 23andMe, posting it to the same cybercrime forum that hosted the first batch two weeks ago.... [...]
