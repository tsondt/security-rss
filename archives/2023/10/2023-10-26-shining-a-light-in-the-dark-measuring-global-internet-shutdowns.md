Title: Shining a light in the dark: Measuring global internet shutdowns
Date: 2023-10-26T16:00:00+00:00
Author: Max Saltonstall
Category: GCP Security
Tags: Data Analytics;Security & Identity
Slug: 2023-10-26-shining-a-light-in-the-dark-measuring-global-internet-shutdowns

[Source](https://cloud.google.com/blog/products/identity-security/shining-a-light-in-the-dark-measuring-global-internet-shutdowns/){:target="_blank" rel="noopener"}

> It’s hard to imagine (or for some of us, remember) life without the internet. From work, to family, to leisure, the internet has become interwoven in the fabric of our routines. But what if all of that got cut off, suddenly and without warning? For many people around the world, that's a daily reality. In 2022, 35 countries cut off internet access, across at least 187 instances, with each outage lasting hours, days, or weeks. Censored Planet Observatory, a team of researchers at the University of Michigan, has been working since 2010 to shine a spotlight on this problem. They [...]
