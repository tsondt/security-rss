Title: Secure by Design: AWS to enhance MFA requirements in 2024
Date: 2023-10-03T15:03:28+00:00
Author: Steve Schmidt
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS IAM;MFA;multi-factor authentication;Security Blog
Slug: 2023-10-03-secure-by-design-aws-to-enhance-mfa-requirements-in-2024

[Source](https://aws.amazon.com/blogs/security/security-by-design-aws-to-enhance-mfa-requirements-in-2024/){:target="_blank" rel="noopener"}

> Security is our top priority at Amazon Web Services (AWS). To that end, I’m excited to share that AWS is further strengthening the default security posture of our customers’ environments by requiring the use of multi-factor authentication (MFA), beginning with the most privileged users in their accounts. MFA is one of the simplest and most [...]
