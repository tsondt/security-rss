Title: AWS Security Profile: Liam Wadman, Senior Solutions Architect, AWS Identity
Date: 2023-10-20T16:56:33+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Thought Leadership;AWS Identity;AWS Re:Invent;AWS Security Profile;AWS Security Profiles;Security Blog
Slug: 2023-10-20-aws-security-profile-liam-wadman-senior-solutions-architect-aws-identity

[Source](https://aws.amazon.com/blogs/security/aws-security-profile-liam-wadman-sr-solutions-architect-aws-identity/){:target="_blank" rel="noopener"}

> In the AWS Security Profile series, I interview some of the humans who work in AWS Security and help keep our customers safe and secure. In this profile, I interviewed Liam Wadman, Senior Solutions Architect for AWS Identity. Pictured: Liam making quick informed decisions about risk and reward How long have you been at AWS [...]
