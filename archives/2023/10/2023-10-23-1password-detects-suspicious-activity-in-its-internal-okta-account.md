Title: 1Password detects “suspicious activity” in its internal Okta account
Date: 2023-10-23T20:56:49+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;1password;okta;unauthorized access
Slug: 2023-10-23-1password-detects-suspicious-activity-in-its-internal-okta-account

[Source](https://arstechnica.com/?p=1978094){:target="_blank" rel="noopener"}

> Enlarge (credit: 1Password) 1Password, a password manager used by millions of people and more than 100,000 businesses, said it detected suspicious activity on a company account provided by Okta, the identity and authentication service that disclosed a breach on Friday. “On September 29, we detected suspicious activity on our Okta instance that we use to manage our employee-facing apps,” 1Password CTO Pedro Canahuati wrote in an email. “We immediately terminated the activity, investigated, and found no compromise of user data or other sensitive systems, either employee-facing or user-facing.” Since then, Canahuati said, his company had been working with Okta to [...]
