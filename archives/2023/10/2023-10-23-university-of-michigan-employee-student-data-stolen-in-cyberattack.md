Title: University of Michigan employee, student data stolen in cyberattack
Date: 2023-10-23T15:34:41-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-10-23-university-of-michigan-employee-student-data-stolen-in-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/university-of-michigan-employee-student-data-stolen-in-cyberattack/){:target="_blank" rel="noopener"}

> The University of Michigan says in a statement today that they suffered a data breach after hackers broke into its network in August and accessed systems with information belonging to students, applicants, alumni, donors, employees, patients, and research study participants. [...]
