Title: Ransomware attacks register record speeds thanks to success of infosec industry
Date: 2023-10-10T08:30:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-10-ransomware-attacks-register-record-speeds-thanks-to-success-of-infosec-industry

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/10/ransomware_attacks_register_record_speeds/){:target="_blank" rel="noopener"}

> Dwell times drop to hours rather than days for the first time The time taken by cyber attackers between gaining an initial foothold in a victim's environment and deploying ransomware has fallen to 24 hours, according to a study.... [...]
