Title: QNAP takes down server behind widespread brute-force attacks
Date: 2023-10-23T08:02:03-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-23-qnap-takes-down-server-behind-widespread-brute-force-attacks

[Source](https://www.bleepingcomputer.com/news/security/qnap-takes-down-server-behind-widespread-brute-force-attacks/){:target="_blank" rel="noopener"}

> QNAP took down a malicious server used in widespread brute-force attacks targeting Internet-exposed NAS (network-attached storage) devices with weak passwords. [...]
