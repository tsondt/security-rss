Title: Trio of TorchServe flaws means PyTorch users need an urgent upgrade
Date: 2023-10-04T01:28:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-10-04-trio-of-torchserve-flaws-means-pytorch-users-need-an-urgent-upgrade

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/04/shelltorch_vulnerabilities/){:target="_blank" rel="noopener"}

> Meta, the project's maintainer, shrugs: We fixed it, let's move on A trio of now-patched security issues in TorchServe, an open-source tool for scaling PyTorch machine-learning models in production, could lead to server takeover and remote code execution (RCE), according to security researchers.... [...]
