Title: Fake 'RedAlert' rocket alert app for Israel installs Android spyware
Date: 2023-10-16T11:18:28-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-10-16-fake-redalert-rocket-alert-app-for-israel-installs-android-spyware

[Source](https://www.bleepingcomputer.com/news/security/fake-redalert-rocket-alert-app-for-israel-installs-android-spyware/){:target="_blank" rel="noopener"}

> Israeli Android users are targeted by a malicious version of the 'RedAlert - Rocket Alerts' app that, while it offers the promised functionality, acts as spyware in the background. [...]
