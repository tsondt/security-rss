Title: City of Philadelphia discloses data breach after five months
Date: 2023-10-23T05:25:58-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-23-city-of-philadelphia-discloses-data-breach-after-five-months

[Source](https://www.bleepingcomputer.com/news/security/city-of-philadelphia-discloses-data-breach-after-five-months/){:target="_blank" rel="noopener"}

> The City of Philadelphia is investigating a data breach after attackers "may have gained access" to City email accounts containing personal and protected health information five months ago, in May. [...]
