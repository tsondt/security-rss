Title: Forget the outside hacker, the bigger threat is inside by the coffee machine
Date: 2023-10-26T20:15:10+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2023-10-26-forget-the-outside-hacker-the-bigger-threat-is-inside-by-the-coffee-machine

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/26/register_kettle_insider_threat/){:target="_blank" rel="noopener"}

> After a week of incidents, Register vultures pick over the innards Kettle In this week's Kettle the topic is one that's been much in the news this week - the much-underrated insider threat issue.... [...]
