Title: Thwarted ransomware raid targeting WS_FTP servers demanded just 0.018 BTC
Date: 2023-10-13T18:15:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-13-thwarted-ransomware-raid-targeting-ws_ftp-servers-demanded-just-0018-btc

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/13/ws_ftp_bug_ransomware/){:target="_blank" rel="noopener"}

> Early attempt to exploit latest Progress Software bug spotted in the wild An early ransomware campaign against organizations by exploiting the vulnerability in Progress Software's WS_FTP Server was this week spotted by security researchers.... [...]
