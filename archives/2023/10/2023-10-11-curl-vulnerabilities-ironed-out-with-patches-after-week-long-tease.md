Title: curl vulnerabilities ironed out with patches after week-long tease
Date: 2023-10-11T10:05:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-11-curl-vulnerabilities-ironed-out-with-patches-after-week-long-tease

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/11/vulnerabilities_in_curl_receive_patches/){:target="_blank" rel="noopener"}

> The coordinated disclosure didn’t quite go to plan, though Updated After a week of rampant speculation about the nature of the security issues in curl, the latest version of the command line transfer tool was finally released today.... [...]
