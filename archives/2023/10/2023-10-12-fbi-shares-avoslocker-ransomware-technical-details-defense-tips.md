Title: FBI shares AvosLocker ransomware technical details, defense tips
Date: 2023-10-12T19:38:58-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-10-12-fbi-shares-avoslocker-ransomware-technical-details-defense-tips

[Source](https://www.bleepingcomputer.com/news/security/fbi-shares-avoslocker-ransomware-technical-details-defense-tips/){:target="_blank" rel="noopener"}

> The U.S. government has updated the list of tools AvosLocker ransomware affiliates use in attacks to include open-source utilities along with custom PowerShell, and batch scripts. [...]
