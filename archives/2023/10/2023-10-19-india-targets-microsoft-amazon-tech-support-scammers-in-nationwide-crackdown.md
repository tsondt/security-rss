Title: India targets Microsoft, Amazon tech support scammers in nationwide crackdown
Date: 2023-10-19T13:22:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-19-india-targets-microsoft-amazon-tech-support-scammers-in-nationwide-crackdown

[Source](https://www.bleepingcomputer.com/news/security/india-targets-microsoft-amazon-tech-support-scammers-in-nationwide-crackdown/){:target="_blank" rel="noopener"}

> India's Central Bureau of Investigation (CBI) raided 76 locations in a nationwide crackdown on cybercrime operations behind tech support scams and cryptocurrency fraud. [...]
