Title: How Sensitive Data Protection can help secure generative AI workloads
Date: 2023-10-04T16:00:02+00:00
Author: Assaf Namer
Category: GCP Security
Tags: AI & Machine Learning;Security & Identity
Slug: 2023-10-04-how-sensitive-data-protection-can-help-secure-generative-ai-workloads

[Source](https://cloud.google.com/blog/products/identity-security/how-sensitive-data-protection-can-help-secure-generative-ai-workloads/){:target="_blank" rel="noopener"}

> Generative AI models are a hot topic across nearly every industry. Enterprises are looking to leverage this technology to enrich their business services and engagement with customers, as well as streamline operations and speed up business processes. However, as with most AI/ML applications, generative AI models are fueled by data and data context. Understanding and protecting this sensitive, enterprise-specific data is critical to ensuring successful deployments and proper use. In recent surveys conducted by Google among attendees across our Modern Security events at the Google campus in Sunnyvale, respondents said that “data leakage” and “privacy” were two of the top [...]
