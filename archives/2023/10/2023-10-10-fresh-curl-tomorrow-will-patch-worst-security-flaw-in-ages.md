Title: Fresh curl tomorrow will patch 'worst' security flaw in ages
Date: 2023-10-10T14:30:08+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-10-10-fresh-curl-tomorrow-will-patch-worst-security-flaw-in-ages

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/10/curl_patch_in_update/){:target="_blank" rel="noopener"}

> It’s bad, folks. Pair of CVEs incoming on October 11 Start your patch engines – a new version of curl is due tomorrow that addresses a pair of flaws, one of which lead developer Daniel Stenberg describes as "probably the worst curl security flaw in a long time."... [...]
