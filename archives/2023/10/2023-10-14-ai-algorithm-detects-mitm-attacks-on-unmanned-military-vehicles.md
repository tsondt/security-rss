Title: AI algorithm detects MitM attacks on unmanned military vehicles
Date: 2023-10-14T11:14:07-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-14-ai-algorithm-detects-mitm-attacks-on-unmanned-military-vehicles

[Source](https://www.bleepingcomputer.com/news/security/ai-algorithm-detects-mitm-attacks-on-unmanned-military-vehicles/){:target="_blank" rel="noopener"}

> Professors at the University of South Australia and Charles Sturt University have developed an algorithm to detect and intercept man-in-the-middle (MitM) attacks on unmanned military robots. [...]
