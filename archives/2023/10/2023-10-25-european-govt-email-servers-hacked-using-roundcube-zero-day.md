Title: European govt email servers hacked using Roundcube zero-day
Date: 2023-10-25T07:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-25-european-govt-email-servers-hacked-using-roundcube-zero-day

[Source](https://www.bleepingcomputer.com/news/security/european-govt-email-servers-hacked-using-roundcube-zero-day/){:target="_blank" rel="noopener"}

> The Winter Vivern Russian hacking group has been exploiting a Roundcube Webmail zero-day since at least October 11 to attack European government entities and think tanks. [...]
