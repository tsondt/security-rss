Title: Microsoft Defender now auto-isolates compromised accounts
Date: 2023-10-11T14:37:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-10-11-microsoft-defender-now-auto-isolates-compromised-accounts

[Source](https://www.bleepingcomputer.com/news/security/microsoft-defender-now-auto-isolates-compromised-accounts/){:target="_blank" rel="noopener"}

> Microsoft Defender for Endpoint now uses automatic attack disruption to isolate compromised user accounts and block lateral movement in hands-on-keyboard attacks with the help of a new 'contain user' capability in public preview. [...]
