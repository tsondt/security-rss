Title: Casio keyed up after data loss hits customers in 149 countries
Date: 2023-10-19T19:45:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-10-19-casio-keyed-up-after-data-loss-hits-customers-in-149-countries

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/19/casio_data_theft/){:target="_blank" rel="noopener"}

> Crooks broke into the ClassPad server and swiped online learning database Japanese electronics giant Casio said miscreants broke into its ClassPad server and stole a database with personal information belonging to customers in 149 countries.... [...]
