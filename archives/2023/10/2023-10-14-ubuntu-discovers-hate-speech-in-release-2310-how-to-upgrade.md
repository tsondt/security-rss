Title: Ubuntu discovers 'hate speech' in release 23.10 — how to upgrade?
Date: 2023-10-14T06:00:00-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-10-14-ubuntu-discovers-hate-speech-in-release-2310-how-to-upgrade

[Source](https://www.bleepingcomputer.com/news/security/ubuntu-discovers-hate-speech-in-release-2310-how-to-upgrade/){:target="_blank" rel="noopener"}

> Ubuntu, the most popular Linux distribution, has pulled its Desktop release 23.10 after its Ukrainian translations were discovered to contain hate speech. According to the Ubuntu project, a malicious contributor is behind anti-Semitic, homophobic, and xenophobic slurs that were injected into the distro via a "third party tool." [...]
