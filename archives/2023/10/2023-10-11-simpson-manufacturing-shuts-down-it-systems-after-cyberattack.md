Title: Simpson Manufacturing shuts down IT systems after cyberattack
Date: 2023-10-11T09:55:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-11-simpson-manufacturing-shuts-down-it-systems-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/simpson-manufacturing-shuts-down-it-systems-after-cyberattack/){:target="_blank" rel="noopener"}

> Simpson Manufacturing disclosed via a SEC 8-K filing a cybersecurity incident that has caused disruptions in its operations, which are expected to continue. [...]
