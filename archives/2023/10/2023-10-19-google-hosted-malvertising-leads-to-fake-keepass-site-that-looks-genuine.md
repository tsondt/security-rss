Title: Google-hosted malvertising leads to fake Keepass site that looks genuine
Date: 2023-10-19T04:50:35+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;google;malvertising;malware;punycode
Slug: 2023-10-19-google-hosted-malvertising-leads-to-fake-keepass-site-that-looks-genuine

[Source](https://arstechnica.com/?p=1977141){:target="_blank" rel="noopener"}

> Enlarge (credit: Miragec/Getty Images) Google has been caught hosting a malicious ad so convincing that there’s a decent chance it has managed to trick some of the more security-savvy users who encountered it. Screenshot of the malicious ad hosted on Google. (credit: Malwarebytes) Looking at the ad, which masquerades as a pitch for the open source password manager Keepass, there’s no way to know that it’s fake. It’s on Google, after all, which claims to vet the ads it carries. Making the ruse all the more convincing, clicking on it leads to ķeepass[.]info, which, when viewed in an address bar, [...]
