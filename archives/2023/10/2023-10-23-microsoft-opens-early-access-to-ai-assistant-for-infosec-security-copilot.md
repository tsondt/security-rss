Title: Microsoft opens early access to AI assistant for infosec, Security Copilot
Date: 2023-10-23T13:00:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-23-microsoft-opens-early-access-to-ai-assistant-for-infosec-security-copilot

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/23/microsoft_security_copilots_early_access/){:target="_blank" rel="noopener"}

> Copilotization of all things continues... as helper offers incident reports to share with the boss and more Microsoft is opening up the early access program for its flagship cybersecurity AI product, which marks the inevitable folding in of Copilot into its infosec suite.... [...]
