Title: Messaging Service Wiretap Discovered through Expired TLS Cert
Date: 2023-10-27T11:01:00+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;certificates;man-in-the-middle attacks;privacy;surveillance;TLS
Slug: 2023-10-27-messaging-service-wiretap-discovered-through-expired-tls-cert

[Source](https://www.schneier.com/blog/archives/2023/10/messaging-service-wiretap-discovered-through-expired-tls-cert.html){:target="_blank" rel="noopener"}

> Fascinating story of a covert wiretap that was discovered because of an expired TLS certificate: The suspected man-in-the-middle attack was identified when the administrator of jabber.ru, the largest Russian XMPP service, received a notification that one of the servers’ certificates had expired. However, jabber.ru found no expired certificates on the server, ­ as explained in a blog post by ValdikSS, a pseudonymous anti-censorship researcher based in Russia who collaborated on the investigation. The expired certificate was instead discovered on a single port being used by the service to establish an encrypted Transport Layer Security (TLS) connection with users. Before it [...]
