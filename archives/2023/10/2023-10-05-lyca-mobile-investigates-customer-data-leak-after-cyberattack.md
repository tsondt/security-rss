Title: Lyca Mobile investigates customer data leak after cyberattack
Date: 2023-10-05T11:01:39-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-05-lyca-mobile-investigates-customer-data-leak-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/lyca-mobile-investigates-customer-data-leak-after-cyberattack/){:target="_blank" rel="noopener"}

> Lyca Mobile has released a statement about an unexpected disruption on its network caused by a cyberattack that may have also compromised customer data. [...]
