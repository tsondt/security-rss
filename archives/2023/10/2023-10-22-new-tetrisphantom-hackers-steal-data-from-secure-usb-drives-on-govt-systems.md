Title: New TetrisPhantom hackers steal data from secure USB drives on govt systems
Date: 2023-10-22T11:18:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-22-new-tetrisphantom-hackers-steal-data-from-secure-usb-drives-on-govt-systems

[Source](https://www.bleepingcomputer.com/news/security/new-tetrisphantom-hackers-steal-data-from-secure-usb-drives-on-govt-systems/){:target="_blank" rel="noopener"}

> A new sophisticated threat tracked as 'TetrisPhantom' has been using compromised secure USB drives to target government systems in the Asia-Pacific region. [...]
