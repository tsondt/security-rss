Title: BianLian extortion group claims recent Air Canada breach
Date: 2023-10-11T17:07:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-11-bianlian-extortion-group-claims-recent-air-canada-breach

[Source](https://www.bleepingcomputer.com/news/security/bianlian-extortion-group-claims-recent-air-canada-breach/){:target="_blank" rel="noopener"}

> The BianLian extortion group claims to have stolen 210GB of data after breaching the network of Air Canada, the country's largest airline and a founding member of Star Alliance. [...]
