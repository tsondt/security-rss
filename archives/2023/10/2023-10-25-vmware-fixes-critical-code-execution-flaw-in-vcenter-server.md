Title: VMware fixes critical code execution flaw in vCenter Server
Date: 2023-10-25T05:00:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-25-vmware-fixes-critical-code-execution-flaw-in-vcenter-server

[Source](https://www.bleepingcomputer.com/news/security/vmware-fixes-critical-code-execution-flaw-in-vcenter-server/){:target="_blank" rel="noopener"}

> VMware issued security updates to fix a critical vCenter Server vulnerability that can be exploited to gain remote code execution attacks on vulnerable servers. [...]
