Title: Iranian hackers lurked in Middle Eastern govt network for 8 months
Date: 2023-10-19T12:40:42-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-19-iranian-hackers-lurked-in-middle-eastern-govt-network-for-8-months

[Source](https://www.bleepingcomputer.com/news/security/iranian-hackers-lurked-in-middle-eastern-govt-network-for-8-months/){:target="_blank" rel="noopener"}

> The Iranian hacking group tracked as OilRig (APT34) breached at least twelve computers belonging to a Middle Eastern government network and maintained access for eight months between February and September 2023. [...]
