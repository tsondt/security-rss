Title: Cloudflare sees surge in hyper-volumetric HTTP DDoS attacks
Date: 2023-10-26T09:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-26-cloudflare-sees-surge-in-hyper-volumetric-http-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/security/cloudflare-sees-surge-in-hyper-volumetric-http-ddos-attacks/){:target="_blank" rel="noopener"}

> The number of hyper-volumetric HTTP DDoS (distributed denial of service) attacks recorded in the third quarter of 2023 surpasses every precedent, indicating that the field has entered a new chapter. [...]
