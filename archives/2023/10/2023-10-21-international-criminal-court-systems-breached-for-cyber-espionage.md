Title: International Criminal Court systems breached for cyber espionage
Date: 2023-10-21T10:01:10-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-10-21-international-criminal-court-systems-breached-for-cyber-espionage

[Source](https://www.bleepingcomputer.com/news/security/international-criminal-court-systems-breached-for-cyber-espionage/){:target="_blank" rel="noopener"}

> The International Criminal Court provided additional information about the cyberattack five weeks ago, saying that it was a targeted operation for espionage purposes. [...]
