Title: Apple emergency update fixes new zero-day used to hack iPhones
Date: 2023-10-04T14:19:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple;Security
Slug: 2023-10-04-apple-emergency-update-fixes-new-zero-day-used-to-hack-iphones

[Source](https://www.bleepingcomputer.com/news/apple/apple-emergency-update-fixes-new-zero-day-used-to-hack-iphones/){:target="_blank" rel="noopener"}

> Apple released emergency security updates to patch a new zero-day security flaw exploited in attacks targeting iPhone and iPad users. [...]
