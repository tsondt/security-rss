Title: D-Link clears up 'exaggerations' around data breach
Date: 2023-10-18T14:45:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-18-d-link-clears-up-exaggerations-around-data-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/18/dlink_debunks_overblown_data_breach/){:target="_blank" rel="noopener"}

> Who knew 3 million actually means 700 in cybercrime forum lingo? D-Link has confirmed suspicions that it was successfully targeted by cyber criminals, but is talking down the scale of the impact.... [...]
