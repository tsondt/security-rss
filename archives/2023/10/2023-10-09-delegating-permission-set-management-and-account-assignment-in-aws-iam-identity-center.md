Title: Delegating permission set management and account assignment in AWS IAM Identity Center
Date: 2023-10-09T20:39:53+00:00
Author: Jake Barker
Category: AWS Security
Tags: AWS IAM Identity Center;Best Practices;Expert (400);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2023-10-09-delegating-permission-set-management-and-account-assignment-in-aws-iam-identity-center

[Source](https://aws.amazon.com/blogs/security/delegating-permission-set-management-and-account-assignment-in-aws-iam-identity-center/){:target="_blank" rel="noopener"}

> In this blog post, we look at how you can use AWS IAM Identity Center (successor to AWS Single Sign-On) to delegate the management of permission sets and account assignments. Delegating the day-to-day administration of user identities and entitlements allows teams to move faster and reduces the burden on your central identity administrators. IAM Identity [...]
