Title: Deepfake Election Interference in Slovakia
Date: 2023-10-06T07:04:28+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;deep fake;voting
Slug: 2023-10-06-deepfake-election-interference-in-slovakia

[Source](https://www.schneier.com/blog/archives/2023/10/deepfake-election-interference-in-slovakia.html){:target="_blank" rel="noopener"}

> Well designed and well timed deepfake or two Slovakian politicians discussing how to rig the election: Šimečka and Denník N immediately denounced the audio as fake. The fact-checking department of news agency AFP said the audio showed signs of being manipulated using AI. But the recording was posted during a 48-hour moratorium ahead of the polls opening, during which media outlets and politicians are supposed to stay silent. That meant, under Slovakia’s election rules, the post was difficult to widely debunk. And, because the post was audio, it exploited a loophole in Meta’s manipulated-media policy, which dictates only faked videos—­where [...]
