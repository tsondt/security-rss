Title: Over 10,000 Cisco devices hacked in IOS XE zero-day attacks
Date: 2023-10-17T09:15:05-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-17-over-10000-cisco-devices-hacked-in-ios-xe-zero-day-attacks

[Source](https://www.bleepingcomputer.com/news/security/over-10-000-cisco-devices-hacked-in-ios-xe-zero-day-attacks/){:target="_blank" rel="noopener"}

> Attackers have exploited a recently disclosed critical zero-day bug to compromise and infect more than 10,000 Cisco IOS XE devices with malicious implants. [...]
