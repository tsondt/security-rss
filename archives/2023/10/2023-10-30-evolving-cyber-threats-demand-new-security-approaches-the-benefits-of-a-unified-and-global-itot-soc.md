Title: Evolving cyber threats demand new security approaches – The benefits of a unified and global IT/OT SOC
Date: 2023-10-30T13:35:44+00:00
Author: Stuart Gregg
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;architect;centralization;decentralization;IoT;operations;SecOps;Security;Security Blog;Securityoperations;SOC;tools
Slug: 2023-10-30-evolving-cyber-threats-demand-new-security-approaches-the-benefits-of-a-unified-and-global-itot-soc

[Source](https://aws.amazon.com/blogs/security/evolving-cyber-threats-demand-new-security-approaches-the-benefits-of-a-unified-and-global-it-ot-soc/){:target="_blank" rel="noopener"}

> In this blog post, we discuss some of the benefits and considerations organizations should think through when looking at a unified and global information technology and operational technology (IT/OT) security operations center (SOC). Although this post focuses on the IT/OT convergence within the SOC, you can use the concepts and ideas discussed here when thinking [...]
