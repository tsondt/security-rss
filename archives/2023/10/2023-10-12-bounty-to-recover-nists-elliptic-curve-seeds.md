Title: Bounty to Recover NIST’s Elliptic Curve Seeds
Date: 2023-10-12T11:09:38+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;backdoors;contests;cryptography;NIST;random numbers
Slug: 2023-10-12-bounty-to-recover-nists-elliptic-curve-seeds

[Source](https://www.schneier.com/blog/archives/2023/10/bounty-to-recover-nists-elliptic-curve-seeds.html){:target="_blank" rel="noopener"}

> This is a fun challenge: The NIST elliptic curves that power much of modern cryptography were generated in the late ’90s by hashing seeds provided by the NSA. How were the seeds generated? Rumor has it that they are in turn hashes of English sentences, but the person who picked them, Dr. Jerry Solinas, passed away in early 2023 leaving behind a cryptographic mystery, some conspiracy theories, and an historical password cracking challenge. So there’s a $12K prize to recover the hash seeds. Some backstory : Some of the backstory here (it’s the funniest fucking backstory ever): it’s lately been [...]
