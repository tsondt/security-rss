Title: E-Root admin faces 20 years for selling stolen RDP, SSH accounts
Date: 2023-10-19T18:42:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-10-19-e-root-admin-faces-20-years-for-selling-stolen-rdp-ssh-accounts

[Source](https://www.bleepingcomputer.com/news/security/e-root-admin-faces-20-years-for-selling-stolen-rdp-ssh-accounts/){:target="_blank" rel="noopener"}

> Sandu Diaconu, the operator of the E-Root marketplace, has been extradited to the U.S. to face a maximum imprisonment penalty of 20 years for selling access to compromised computers. [...]
