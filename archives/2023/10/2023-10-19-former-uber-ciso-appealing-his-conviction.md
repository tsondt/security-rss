Title: Former Uber CISO Appealing His Conviction
Date: 2023-10-19T11:08:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;breaches;data breaches;FTC;regulation;Uber
Slug: 2023-10-19-former-uber-ciso-appealing-his-conviction

[Source](https://www.schneier.com/blog/archives/2023/10/former-uber-ciso-appealing-his-conviction.html){:target="_blank" rel="noopener"}

> Joe Sullivan, Uber’s CEO during their 2016 data breach, is appealing his conviction. Prosecutors charged Sullivan, whom Uber hired as CISO after the 2014 breach, of withholding information about the 2016 incident from the FTC even as its investigators were scrutinizing the company’s data security and privacy practices. The government argued that Sullivan should have informed the FTC of the 2016 incident, but instead went out of his way to conceal it from them. Prosecutors also accused Sullivan of attempting to conceal the breach itself by paying $100,000 to buy the silence of the two hackers behind the compromise. Sullivan [...]
