Title: New Marvin attack revives 25-year-old decryption flaw in RSA
Date: 2023-10-01T10:16:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-01-new-marvin-attack-revives-25-year-old-decryption-flaw-in-rsa

[Source](https://www.bleepingcomputer.com/news/security/new-marvin-attack-revives-25-year-old-decryption-flaw-in-rsa/){:target="_blank" rel="noopener"}

> A flaw related to the PKCS #1 v1.5 padding in SSL servers discovered in 1998 and believed to have been resolved still impacts several widely-used projects today. [...]
