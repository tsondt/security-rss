Title: New Microsoft bug bounty program focuses on AI-powered Bing
Date: 2023-10-12T13:29:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-10-12-new-microsoft-bug-bounty-program-focuses-on-ai-powered-bing

[Source](https://www.bleepingcomputer.com/news/security/new-microsoft-bug-bounty-program-focuses-on-ai-powered-bing/){:target="_blank" rel="noopener"}

> Microsoft announced a new AI bounty program focused on the AI-driven Bing experience, with rewards reaching $15,000. [...]
