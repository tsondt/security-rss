Title: Ragnar Locker ransomware developer arrested in France
Date: 2023-10-20T11:58:08-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-20-ragnar-locker-ransomware-developer-arrested-in-france

[Source](https://www.bleepingcomputer.com/news/security/ragnar-locker-ransomware-developer-arrested-in-france/){:target="_blank" rel="noopener"}

> Law enforcement agencies arrested a malware developer linked with the Ragnar Locker ransomware gang and seized the group's dark web sites in a joint international operation. [...]
