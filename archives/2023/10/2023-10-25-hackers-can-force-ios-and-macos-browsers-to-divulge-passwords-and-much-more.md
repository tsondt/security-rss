Title: Hackers can force iOS and macOS browsers to divulge passwords and much more
Date: 2023-10-25T17:00:39+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;Uncategorized;apple;iOS;MacOS;Safari;side channels
Slug: 2023-10-25-hackers-can-force-ios-and-macos-browsers-to-divulge-passwords-and-much-more

[Source](https://arstechnica.com/?p=1978389){:target="_blank" rel="noopener"}

> Enlarge (credit: Kim et al.) Researchers have devised an attack that forces Apple’s Safari browser to divulge passwords, Gmail message content, and other secrets by exploiting a side channel vulnerability in the A- and M-series CPUs running modern iOS and macOS devices. iLeakage, as the academic researchers have named the attack, is practical and requires minimal resources to carry out. It does, however, require extensive reverse-engineering of Apple hardware and significant expertise in exploiting a class of vulnerability known as a side channel, which leaks secrets based on clues left in electromagnetic emanations, data caches, or other manifestations of a [...]
