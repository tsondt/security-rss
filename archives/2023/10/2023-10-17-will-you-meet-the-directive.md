Title: Will you meet the directive?
Date: 2023-10-17T03:06:15+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2023-10-17-will-you-meet-the-directive

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/17/will_you_meet_the_directive/){:target="_blank" rel="noopener"}

> Your guide to SEC, DoD 8140.3 and NIS2 changes with the SANS Cyber Compliance Countdown Sponsored Post Imminent changes to cyber security regulations in the US and Europe demand that public and private sector organizations on both side of the Atlantic keep a close eye on their compliance.... [...]
