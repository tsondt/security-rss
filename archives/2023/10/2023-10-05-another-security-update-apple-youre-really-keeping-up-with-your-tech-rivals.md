Title: Another security update, Apple? You're really keeping up with your tech rivals
Date: 2023-10-05T18:16:07+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-10-05-another-security-update-apple-youre-really-keeping-up-with-your-tech-rivals

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/05/once_again_apple_issues_security/){:target="_blank" rel="noopener"}

> Zero day? More like every day, amirite? Apple has demonstrated that it can more than hold its own among the tech giants, at least in terms of finding itself on the wrong end of zero-day vulnerabilities.... [...]
