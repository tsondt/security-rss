Title: New educational lab for Security Command Center can help address security talent gap
Date: 2023-10-30T16:00:00+00:00
Author: Connor Hammersmith
Category: GCP Security
Tags: Training and Certifications;Security & Identity
Slug: 2023-10-30-new-educational-lab-for-security-command-center-can-help-address-security-talent-gap

[Source](https://cloud.google.com/blog/products/identity-security/new-educational-lab-for-security-command-center-can-help-address-security-talent-gap/){:target="_blank" rel="noopener"}

> To address the chronic shortage of security talent, Google Cloud has introduced a new virtual, lab-based training for Security Command Center, our flagship cloud security solution. The new lab, Mitigate Threats and Vulnerabilities with Security Command Center, has no security knowledge prerequisites and can be completed in just six hours. Non-security IT professionals can gain the skills to discover security vulnerabilities, identify potential threats to cloud resources, and respond to security issues across a Google Cloud environment. The lab can also help reduce toil for security professionals by providing a non-production environment in which they can experiment and hone their [...]
