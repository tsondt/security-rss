Title: How AWS protects customers from DDoS events
Date: 2023-10-10T12:02:57+00:00
Author: Tom Scholl
Category: AWS Security
Tags: Amazon CloudFront;Amazon Route 53;AWS Shield;AWS WAF;Foundational (100);Security, Identity, & Compliance;Technical How-to;Thought Leadership;Security Blog
Slug: 2023-10-10-how-aws-protects-customers-from-ddos-events

[Source](https://aws.amazon.com/blogs/security/how-aws-protects-customers-from-ddos-events/){:target="_blank" rel="noopener"}

> At Amazon Web Services (AWS), security is our top priority. Security is deeply embedded into our culture, processes, and systems; it permeates everything we do. What does this mean for you? We believe customers can benefit from learning more about what AWS is doing to prevent and mitigate customer-impacting security events. Since late August 2023, [...]
