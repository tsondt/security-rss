Title: Android adware apps on Google Play amass two million installs
Date: 2023-10-26T15:01:30-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2023-10-26-android-adware-apps-on-google-play-amass-two-million-installs

[Source](https://www.bleepingcomputer.com/news/security/android-adware-apps-on-google-play-amass-two-million-installs/){:target="_blank" rel="noopener"}

> Several malicious Google Play Android apps installed over 2 million times push intrusive ads to users while concealing their presence on the infected devices. [...]
