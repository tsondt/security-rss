Title: MGM Resorts ransomware attack led to $100 million loss, data theft
Date: 2023-10-06T09:53:05-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-06-mgm-resorts-ransomware-attack-led-to-100-million-loss-data-theft

[Source](https://www.bleepingcomputer.com/news/security/mgm-resorts-ransomware-attack-led-to-100-million-loss-data-theft/){:target="_blank" rel="noopener"}

> MGM Resorts reveals that last month's cyberattack cost the company $100 million and allowed the hackers to steal customers' personal information. [...]
