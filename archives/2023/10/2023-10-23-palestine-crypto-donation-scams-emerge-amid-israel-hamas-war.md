Title: Palestine crypto donation scams emerge amid Israel-Hamas war
Date: 2023-10-23T15:07:28-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-10-23-palestine-crypto-donation-scams-emerge-amid-israel-hamas-war

[Source](https://www.bleepingcomputer.com/news/security/palestine-crypto-donation-scams-emerge-amid-israel-hamas-war/){:target="_blank" rel="noopener"}

> As thousands of civilians die amid the deadly Israel-Hamas war, scammers are capitalizing on the horrific events to collect donations by pretending to be legitimate charities. BleepingComputer has come across several posts on X (formerly Twitter), Telegram and Instagram where scammers list dubious cryptocurrency wallet addresses. [...]
