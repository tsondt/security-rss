Title: CISA reveals 'Admin123' as top security threat in cyber sloppiness chart
Date: 2023-10-06T18:42:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-06-cisa-reveals-admin123-as-top-security-threat-in-cyber-sloppiness-chart

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/06/cisa_top_10_misconfigurations/){:target="_blank" rel="noopener"}

> Calls for wider adoption of security-by-design principles continue to ring loudly from Uncle Sam The US Cybersecurity and Infrastructure Security Agency (CISA) and the National Security Agency (NSA) are blaming unchanged default credentials as the prime security misconfiguration that leads to cyberattacks.... [...]
