Title: Cryptojackers steal AWS credentials from GitHub in 5 minutes
Date: 2023-10-30T18:31:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-30-cryptojackers-steal-aws-credentials-from-github-in-5-minutes

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/30/cryptojackers_steal_aws_credentials_github/){:target="_blank" rel="noopener"}

> Researchers just scratching surface of their understanding of campaign dating back to 2020 Security researchers have uncovered a multi-year cryptojacking campaign they claim autonomously clones GitHub repositories and steals their exposed AWS credentials.... [...]
