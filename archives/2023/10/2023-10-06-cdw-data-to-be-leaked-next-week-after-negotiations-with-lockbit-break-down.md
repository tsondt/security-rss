Title: CDW data to be leaked next week after negotiations with LockBit break down
Date: 2023-10-06T13:21:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-06-cdw-data-to-be-leaked-next-week-after-negotiations-with-lockbit-break-down

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/06/cdw_lockbit_negotiations/){:target="_blank" rel="noopener"}

> Ransomware spokesperson scoffs at IT reseller's offer of payment CDW, one of the largest resellers on the planet, will have its data leaked by LockBit after negotiations over the ransom fee broke down, a spokesperson for the cybercrime gang says.... [...]
