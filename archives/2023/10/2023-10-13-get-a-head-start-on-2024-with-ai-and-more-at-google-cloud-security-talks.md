Title: Get a head start on 2024 with AI and more at Google Cloud Security Talks
Date: 2023-10-13T16:00:00+00:00
Author: Ruchika Mishra
Category: GCP Security
Tags: Security & Identity
Slug: 2023-10-13-get-a-head-start-on-2024-with-ai-and-more-at-google-cloud-security-talks

[Source](https://cloud.google.com/blog/products/identity-security/get-a-head-start-on-2024-with-ai-and-more-at-google-cloud-security-talks/){:target="_blank" rel="noopener"}

> October marks the 20th anniversary of Cybersecurity Awareness Month, and the perfect time to hold Google Cloud Security Talks, our quarterly digital event on all things security. On October 25, we’ll bring together experts from Google Cloud and the broader Google security community to share insights, best practices, and ways to help increase resilience against modern risks and threats. We’ll kick things off with a keynote session on the overall state of Google Security with Royal Hansen, vice president, Privacy, Safety and Security Engineering. Hansen will provide Google’s perspective and insight on two topics that are top of mind for [...]
