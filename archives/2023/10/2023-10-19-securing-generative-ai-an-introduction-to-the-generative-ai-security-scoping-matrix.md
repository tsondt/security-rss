Title: Securing generative AI: An introduction to the Generative AI Security Scoping Matrix
Date: 2023-10-19T13:32:32+00:00
Author: Matt Saner
Category: AWS Security
Tags: Amazon Bedrock;Artificial Intelligence;Best Practices;Generative AI;Intermediate (200);Security, Identity, & Compliance;artificial intelligence;Security Blog
Slug: 2023-10-19-securing-generative-ai-an-introduction-to-the-generative-ai-security-scoping-matrix

[Source](https://aws.amazon.com/blogs/security/securing-generative-ai-an-introduction-to-the-generative-ai-security-scoping-matrix/){:target="_blank" rel="noopener"}

> Generative artificial intelligence (generative AI) has captured the imagination of organizations and is transforming the customer experience in industries of every size across the globe. This leap in AI capability, fueled by multi-billion-parameter large language models (LLMs) and transformer neural networks, has opened the door to new productivity improvements, creative capabilities, and more. As organizations [...]
