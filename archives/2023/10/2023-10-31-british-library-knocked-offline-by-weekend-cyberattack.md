Title: British Library knocked offline by weekend cyberattack
Date: 2023-10-31T14:58:52-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-31-british-library-knocked-offline-by-weekend-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/british-library-knocked-offline-by-weekend-cyberattack/){:target="_blank" rel="noopener"}

> The British Library has been hit by a major IT outage affecting its website and many of its services following a "cyber incident" that impacted its systems on Saturday, October 28. [...]
