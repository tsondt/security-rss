Title: AI Risks
Date: 2023-10-09T11:03:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;essays;national security policy;regulation
Slug: 2023-10-09-ai-risks

[Source](https://www.schneier.com/blog/archives/2023/10/ai-risks.html){:target="_blank" rel="noopener"}

> There is no shortage of researchers and industry titans willing to warn us about the potential destructive power of artificial intelligence. Reading the headlines, one would hope that the rapid gains in AI technology have also brought forth a unifying realization of the risks—and the steps we need to take to mitigate them. The reality, unfortunately, is quite different. Beneath almost all of the testimony, the manifestoes, the blog posts, and the public declarations issued about AI are battles among deeply divided factions. Some are concerned about far-future risks that sound like science fiction. Some are genuinely alarmed by the [...]
