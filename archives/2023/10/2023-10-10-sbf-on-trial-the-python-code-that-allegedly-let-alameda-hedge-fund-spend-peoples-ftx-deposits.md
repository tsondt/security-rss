Title: SBF on trial: The Python code that allegedly let Alameda hedge fund spend people's FTX deposits
Date: 2023-10-10T21:21:04+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-10-10-sbf-on-trial-the-python-code-that-allegedly-let-alameda-hedge-fund-spend-peoples-ftx-deposits

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/10/ftx_python_code/){:target="_blank" rel="noopener"}

> And Caroline Ellison says she was told by Bankman-Fried to take $10B from customer accounts At the fraud trial of former FTX head Sam Bankman-Fried, prosecutors presented the jury with Python code for the FTX backend that allowed flagged client accounts to spend money they didn't have on the cryptocurrency exchange.... [...]
