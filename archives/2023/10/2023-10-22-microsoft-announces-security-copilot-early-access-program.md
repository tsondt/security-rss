Title: Microsoft announces Security Copilot early access program
Date: 2023-10-22T10:09:08-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-10-22-microsoft-announces-security-copilot-early-access-program

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-announces-security-copilot-early-access-program/){:target="_blank" rel="noopener"}

> Microsoft announced this week that its ChatGPT-like Security Copilot AI assistant is now available in early access for some customers. [...]
