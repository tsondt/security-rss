Title: There’s a new way to flip bits in DRAM, and it works against the latest defenses
Date: 2023-10-19T12:30:00+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;bitflips;bits;DDR;DRAM;memory;rowhammer;rowpress
Slug: 2023-10-19-theres-a-new-way-to-flip-bits-in-dram-and-it-works-against-the-latest-defenses

[Source](https://arstechnica.com/?p=1976765){:target="_blank" rel="noopener"}

> Enlarge In 2015, researchers reported a surprising discovery that stoked industry-wide security concerns—an attack called RowHammer that could corrupt, modify, or steal sensitive data when a simple user-level application repeatedly accessed certain regions of DDR memory chips. In the coming years, memory chipmakers scrambled to develop defenses that prevented the attack, mainly by limiting the number of times programs could open and close the targeted chip regions in a given time. Recently, researchers devised a new method for creating the same types of RowHammer-induced bitflips even on a newer generation of chips, known as DDR4, that have the RowHammer mitigations [...]
