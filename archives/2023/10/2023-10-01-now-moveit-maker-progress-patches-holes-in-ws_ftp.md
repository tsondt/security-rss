Title: Now MOVEit maker Progress patches holes in WS_FTP
Date: 2023-10-01T21:51:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-10-01-now-moveit-maker-progress-patches-holes-in-ws_ftp

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/01/in_brief_infosec/){:target="_blank" rel="noopener"}

> Plus: Johnson Controls hit by IT 'incident', Exim and Chrome security updates, and more Infosec in brief Progress Software, maker of the mass-exploited MOVEit document transfer tool, is back in the news with more must-apply security patches, this time for another file-handling product: WS_FTP.... [...]
