Title: Feel-good story of the week: Two ransomware gangs meet their demise
Date: 2023-10-20T23:09:07+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;ragnar locker;ransomware;trigona
Slug: 2023-10-20-feel-good-story-of-the-week-two-ransomware-gangs-meet-their-demise

[Source](https://arstechnica.com/?p=1977607){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) From the warm-and-fuzzy files comes this feel-good Friday post, chronicling this week’s takedown of two hated ransomware groups. One vanished on Tuesday, allegedly after being hacked by a group claiming allegiance to Ukraine. The other was taken out a day later thanks to an international police dragnet. The first group, calling itself Trigona, saw the content on its dark-web victim naming-and-shaming site pulled down and replaced with a banner proclaiming: “Trigona is gone! The servers of Trigona ransomware gang has been infiltrated and wiped out.” An outfit calling itself Ukrainian Cyber Alliance took credit and [...]
