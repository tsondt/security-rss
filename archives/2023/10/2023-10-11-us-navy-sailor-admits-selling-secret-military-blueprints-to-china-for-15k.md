Title: US Navy sailor admits selling secret military blueprints to China for $15K
Date: 2023-10-11T19:42:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-10-11-us-navy-sailor-admits-selling-secret-military-blueprints-to-china-for-15k

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/11/us_navy_china_spy/){:target="_blank" rel="noopener"}

> Worth it for 20 years behind bars? A US Navy service member pleaded guilty yesterday to receiving thousands of dollars in bribes from a Chinese spymaster in exchange for passing on American military secrets.... [...]
