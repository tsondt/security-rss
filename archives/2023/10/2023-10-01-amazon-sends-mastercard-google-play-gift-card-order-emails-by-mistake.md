Title: Amazon sends Mastercard, Google Play gift card order emails by mistake
Date: 2023-10-01T14:23:58-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Technology
Slug: 2023-10-01-amazon-sends-mastercard-google-play-gift-card-order-emails-by-mistake

[Source](https://www.bleepingcomputer.com/news/security/amazon-sends-mastercard-google-play-gift-card-order-emails-by-mistake/){:target="_blank" rel="noopener"}

> Amazon mistakenly sent out purchase confirmation emails for Hotels.com, Google Play, and Mastercard gift cards to customers, making many worried their accounts were compromised. [...]
