Title: Google makes passkeys the default sign-in for personal accounts
Date: 2023-10-10T08:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2023-10-10-google-makes-passkeys-the-default-sign-in-for-personal-accounts

[Source](https://www.bleepingcomputer.com/news/security/google-makes-passkeys-the-default-sign-in-for-personal-accounts/){:target="_blank" rel="noopener"}

> Google announced today that passkeys are now the default sign-in option across all personal Google Accounts across its services and platforms. [...]
