Title: Bounty offered for secret NSA seeds behind NIST elliptic curves algo
Date: 2023-10-07T10:12:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-07-bounty-offered-for-secret-nsa-seeds-behind-nist-elliptic-curves-algo

[Source](https://www.bleepingcomputer.com/news/security/bounty-offered-for-secret-nsa-seeds-behind-nist-elliptic-curves-algo/){:target="_blank" rel="noopener"}

> A bounty of $12,288 has been announced for the first person to crack the NIST elliptic curves seeds and discover the original phrases that were hashed to generate them. [...]
