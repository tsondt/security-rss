Title: From chaos to cadence: Celebrating two decades of Microsoft's Patch Tuesday
Date: 2023-10-11T13:01:45+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-10-11-from-chaos-to-cadence-celebrating-two-decades-of-microsofts-patch-tuesday

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/11/microsoft_patch_tuesday_turns_20/){:target="_blank" rel="noopener"}

> IT folks look back on 20 years of what is now infosec tradition Feature Twenty years ago this month, Microsoft did something pretty revolutionary at the time when it formalized the Windows software release schedule.... [...]
