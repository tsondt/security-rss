Title: Model Extraction Attack on Neural Networks
Date: 2023-10-10T11:09:49+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;cryptanalysis
Slug: 2023-10-10-model-extraction-attack-on-neural-networks

[Source](https://www.schneier.com/blog/archives/2023/10/model-extraction-attack-on-neural-networks.html){:target="_blank" rel="noopener"}

> Adi Shamir et al. have a new model extraction attack on neural networks: Polynomial Time Cryptanalytic Extraction of Neural Network Models Abstract: Billions of dollars and countless GPU hours are currently spent on training Deep Neural Networks (DNNs) for a variety of tasks. Thus, it is essential to determine the difficulty of extracting all the parameters of such neural networks when given access to their black-box implementations. Many versions of this problem have been studied over the last 30 years, and the best current attack on ReLU-based deep neural networks was presented at Crypto’20 by Carlini, Jagielski, and Mironov. It [...]
