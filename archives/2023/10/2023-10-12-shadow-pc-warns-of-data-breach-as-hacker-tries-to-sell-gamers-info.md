Title: Shadow PC warns of data breach as hacker tries to sell gamers' info
Date: 2023-10-12T08:13:11-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-12-shadow-pc-warns-of-data-breach-as-hacker-tries-to-sell-gamers-info

[Source](https://www.bleepingcomputer.com/news/security/shadow-pc-warns-of-data-breach-as-hacker-tries-to-sell-gamers-info/){:target="_blank" rel="noopener"}

> Shadow PC, a provider of high-end cloud computing services, is warning customers of a data breach that exposed customers' private information, as a threat actor claims to be selling the stolen data for over 500,000 customers. [...]
