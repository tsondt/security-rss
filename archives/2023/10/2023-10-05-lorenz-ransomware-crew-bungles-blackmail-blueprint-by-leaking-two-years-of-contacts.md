Title: Lorenz ransomware crew bungles blackmail blueprint by leaking two years of contacts
Date: 2023-10-05T10:00:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-05-lorenz-ransomware-crew-bungles-blackmail-blueprint-by-leaking-two-years-of-contacts

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/05/lorenz_ransomware_group_leaks_details/){:target="_blank" rel="noopener"}

> Data leakers become data leakees The Lorenz ransomware group leaked the details of every person who contacted it via its online contact form over the course of the last two years.... [...]
