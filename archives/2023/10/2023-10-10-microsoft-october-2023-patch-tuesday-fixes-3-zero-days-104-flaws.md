Title: Microsoft October 2023 Patch Tuesday fixes 3 zero-days, 104 flaws
Date: 2023-10-10T13:49:40-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-10-10-microsoft-october-2023-patch-tuesday-fixes-3-zero-days-104-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-october-2023-patch-tuesday-fixes-3-zero-days-104-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's October 2023 Patch Tuesday, with security updates for 104 flaws, including three actively exploited zero-day vulnerabilities. [...]
