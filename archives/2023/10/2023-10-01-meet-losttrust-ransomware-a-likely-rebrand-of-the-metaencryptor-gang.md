Title: Meet LostTrust ransomware — A likely rebrand of the MetaEncryptor gang
Date: 2023-10-01T11:17:34-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-10-01-meet-losttrust-ransomware-a-likely-rebrand-of-the-metaencryptor-gang

[Source](https://www.bleepingcomputer.com/news/security/meet-losttrust-ransomware-a-likely-rebrand-of-the-metaencryptor-gang/){:target="_blank" rel="noopener"}

> The LostTrust ransomware operation is believed to be a rebrand of MetaEncryptor, utilizing almost identical data leak sites and encryptors. [...]
