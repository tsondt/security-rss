Title: What’s new with Cloud Firewall Standard
Date: 2023-10-23T16:00:00+00:00
Author: Faye Feng
Category: GCP Security
Tags: Networking;Security & Identity
Slug: 2023-10-23-whats-new-with-cloud-firewall-standard

[Source](https://cloud.google.com/blog/products/identity-security/whats-new-with-cloud-firewall-standard/){:target="_blank" rel="noopener"}

> Google Cloud Firewall is a fully distributed, stateful inspection next-generation firewall that is built into our software-defined networking fabric and enforced for each workload. With Cloud Firewall, you can enable advanced network threat protection with operational simplicity at cloud scale. Today, we are excited to announce the general availability of the fully qualified domain name (FQDN) feature for Cloud Firewall. FQDN is generally available to customers as part of the Cloud Firewall Standard tier, which also includes Google Cloud Threat Intelligence integration and geolocation filtering. We have also extended Google Cloud Threat Intelligence support with new IP reputation lists and [...]
