Title: Hackers email stolen student data to parents of Nevada school district
Date: 2023-10-28T14:11:53-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-10-28-hackers-email-stolen-student-data-to-parents-of-nevada-school-district

[Source](https://www.bleepingcomputer.com/news/security/hackers-email-stolen-student-data-to-parents-of-nevada-school-district/){:target="_blank" rel="noopener"}

> The Clark County School District (CCSD) in Nevada is dealing with a potentially massive data breach, as hackers email parents their children's' data that was allegedly stolen during a recent cyberattack. [...]
