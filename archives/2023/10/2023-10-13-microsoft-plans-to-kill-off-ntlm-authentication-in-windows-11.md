Title: Microsoft plans to kill off NTLM authentication in Windows 11
Date: 2023-10-13T12:46:09-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-10-13-microsoft-plans-to-kill-off-ntlm-authentication-in-windows-11

[Source](https://www.bleepingcomputer.com/news/security/microsoft-plans-to-kill-off-ntlm-authentication-in-windows-11/){:target="_blank" rel="noopener"}

> Microsoft announced earlier this week that the NTLM authentication protocol will be killed off in Windows 11 in the future. [...]
