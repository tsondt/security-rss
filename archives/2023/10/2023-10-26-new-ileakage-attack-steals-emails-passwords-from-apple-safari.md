Title: New iLeakage attack steals emails, passwords from Apple Safari
Date: 2023-10-26T07:26:48-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple
Slug: 2023-10-26-new-ileakage-attack-steals-emails-passwords-from-apple-safari

[Source](https://www.bleepingcomputer.com/news/security/new-ileakage-attack-steals-emails-passwords-from-apple-safari/){:target="_blank" rel="noopener"}

> Academic researchers created a new speculative side-channel attack they named iLeakage that works on all recent Apple devices and can extract sensitive information from the Safari web browser. [...]
