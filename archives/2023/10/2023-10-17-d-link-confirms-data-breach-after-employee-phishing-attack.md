Title: D-Link confirms data breach after employee phishing attack
Date: 2023-10-17T14:48:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-17-d-link-confirms-data-breach-after-employee-phishing-attack

[Source](https://www.bleepingcomputer.com/news/security/d-link-confirms-data-breach-after-employee-phishing-attack/){:target="_blank" rel="noopener"}

> Taiwanese networking equipment manufacturer D-Link confirmed a data breach linked to information stolen from its network and put up for sale on BreachForums earlier this month. [...]
