Title: KwikTrip all but says IT outage was caused by a cyberattack
Date: 2023-10-17T17:13:42-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-10-17-kwiktrip-all-but-says-it-outage-was-caused-by-a-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/kwiktrip-all-but-says-it-outage-was-caused-by-a-cyberattack/){:target="_blank" rel="noopener"}

> Kwik Trip has released another statement on an ongoing outage, all but confirming it suffered a cyberattack that has led to IT system disruptions. [...]
