Title: Kwik Trip finally confirms cyberattack was behind ongoing outage
Date: 2023-10-20T09:44:46-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-20-kwik-trip-finally-confirms-cyberattack-was-behind-ongoing-outage

[Source](https://www.bleepingcomputer.com/news/security/kwik-trip-finally-confirms-cyberattack-was-behind-ongoing-outage/){:target="_blank" rel="noopener"}

> Two weeks into an ongoing IT outage, Kwik Trip finally confirmed that it's investigating a cyberattack impacting the convenience store chain's internal network since October 9. [...]
