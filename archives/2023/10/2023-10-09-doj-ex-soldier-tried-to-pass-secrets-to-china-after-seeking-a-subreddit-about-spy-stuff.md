Title: DoJ: Ex-soldier tried to pass secrets to China after seeking a 'subreddit about spy stuff'
Date: 2023-10-09T15:15:15+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-10-09-doj-ex-soldier-tried-to-pass-secrets-to-china-after-seeking-a-subreddit-about-spy-stuff

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/09/doj_soldier_secrets_china/){:target="_blank" rel="noopener"}

> FBI agent claims sergeant with top clearance offered access to DoD tech systems A former US Army Sergeant with Top Secret US military clearance created a Word document entitled "Important Information to Share with Chinese Government," according to an FBI agent's sworn declaration.... [...]
