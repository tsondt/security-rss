Title: Don’t Let Zombie Zoom Links Drag You Down
Date: 2023-10-02T15:43:34+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;The Coming Storm;Charan Akiri;LinkedIn;Zoom;Zoom Personal Meeting ID
Slug: 2023-10-02-dont-let-zombie-zoom-links-drag-you-down

[Source](https://krebsonsecurity.com/2023/10/dont-let-zombie-zoom-links-drag-you-down/){:target="_blank" rel="noopener"}

> Many organizations — including quite a few Fortune 500 firms — have exposed web links that allow anyone to initiate a Zoom video conference meeting as a valid employee. These company-specific Zoom links, which include a permanent user ID number and an embedded passcode, can work indefinitely and expose an organization’s employees, customers or partners to phishing and other social engineering attacks. Image: @Pressmaster on Shutterstock. At issue is the Zoom Personal Meeting ID (PMI), which is a permanent identification number linked to your Zoom account and serves as your personal meeting room available around the clock. The PMI portion [...]
