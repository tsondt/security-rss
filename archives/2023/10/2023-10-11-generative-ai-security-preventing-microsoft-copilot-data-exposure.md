Title: Generative AI Security: Preventing Microsoft Copilot Data Exposure
Date: 2023-10-11T10:01:02-04:00
Author: Sponsored by Varonis
Category: BleepingComputer
Tags: Security
Slug: 2023-10-11-generative-ai-security-preventing-microsoft-copilot-data-exposure

[Source](https://www.bleepingcomputer.com/news/security/generative-ai-security-preventing-microsoft-copilot-data-exposure/){:target="_blank" rel="noopener"}

> Microsoft Copilot introduces potential privacy risks as it can have full access to your organization's documents, email, contacts, chats, and calendar. Learn more from Varonis about Microsoft Copilot's security model works and the privacy risks associated with using it. [...]
