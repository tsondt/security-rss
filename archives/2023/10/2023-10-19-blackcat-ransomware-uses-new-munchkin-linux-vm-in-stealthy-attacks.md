Title: BlackCat ransomware uses new ‘Munchkin’ Linux VM in stealthy attacks
Date: 2023-10-19T17:40:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-19-blackcat-ransomware-uses-new-munchkin-linux-vm-in-stealthy-attacks

[Source](https://www.bleepingcomputer.com/news/security/blackcat-ransomware-uses-new-munchkin-linux-vm-in-stealthy-attacks/){:target="_blank" rel="noopener"}

> The BlackCat/ALPHV ransomware operation has begun to use a new tool named 'Munchkin' that utilizes virtual machines to deploy encryptors on network devices stealthily. [...]
