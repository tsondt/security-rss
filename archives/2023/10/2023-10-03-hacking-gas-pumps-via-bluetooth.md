Title: Hacking Gas Pumps via Bluetooth
Date: 2023-10-03T11:01:05+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Bluetooth;cars;hacking;infrastructure
Slug: 2023-10-03-hacking-gas-pumps-via-bluetooth

[Source](https://www.schneier.com/blog/archives/2023/10/hacking-gas-pumps-via-bluetooth.html){:target="_blank" rel="noopener"}

> Turns out pumps at gas stations are controlled via Bluetooth, and that the connections are insecure. No details in the article, but it seems that it’s easy to take control of the pump and have it dispense gas without requiring payment. It’s a complicated crime to monetize, though. You need to sell access to the gas pump to others. [...]
