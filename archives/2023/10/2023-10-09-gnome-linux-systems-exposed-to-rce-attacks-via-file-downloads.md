Title: GNOME Linux systems exposed to RCE attacks via file downloads
Date: 2023-10-09T16:24:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Linux
Slug: 2023-10-09-gnome-linux-systems-exposed-to-rce-attacks-via-file-downloads

[Source](https://www.bleepingcomputer.com/news/security/gnome-linux-systems-exposed-to-rce-attacks-via-file-downloads/){:target="_blank" rel="noopener"}

> A memory corruption vulnerability in the open-source libcue library can let attackers execute arbitrary code on GNOME Linux systems. [...]
