Title: Over 40,000 Cisco IOS XE devices infected with backdoor using zero-day
Date: 2023-10-19T21:08:47-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-10-19-over-40000-cisco-ios-xe-devices-infected-with-backdoor-using-zero-day

[Source](https://www.bleepingcomputer.com/news/security/over-40-000-cisco-ios-xe-devices-infected-with-backdoor-using-zero-day/){:target="_blank" rel="noopener"}

> More than 40,000 Cisco devices running the IOS XE operating system have been compromised after hackers exploited a recently disclosed maximum severity vulnerability tracked as CVE-2023-20198. [...]
