Title: HelloKitty ransomware source code leaked on hacking forum
Date: 2023-10-09T08:25:50-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-10-09-hellokitty-ransomware-source-code-leaked-on-hacking-forum

[Source](https://www.bleepingcomputer.com/news/security/hellokitty-ransomware-source-code-leaked-on-hacking-forum/){:target="_blank" rel="noopener"}

> A threat actor has leaked the complete source code for the first version of the HelloKitty ransomware on a Russian-speaking hacking forum, claiming to be developing a new, more powerful encryptor. [...]
