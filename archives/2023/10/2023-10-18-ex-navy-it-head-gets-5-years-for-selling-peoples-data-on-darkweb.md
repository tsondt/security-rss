Title: Ex-Navy IT head gets 5 years for selling people’s data on darkweb
Date: 2023-10-18T16:47:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government;Legal
Slug: 2023-10-18-ex-navy-it-head-gets-5-years-for-selling-peoples-data-on-darkweb

[Source](https://www.bleepingcomputer.com/news/security/ex-navy-it-head-gets-5-years-for-selling-peoples-data-on-darkweb/){:target="_blank" rel="noopener"}

> Marquis Hooper, a former U.S. Navy IT manager, has received a sentence of five years and five months in prison for illegally obtaining US citizens' personally identifiable information (PII) and selling it on the dark web. [...]
