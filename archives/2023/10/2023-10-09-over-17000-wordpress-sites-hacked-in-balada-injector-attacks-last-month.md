Title: Over 17,000 WordPress sites hacked in Balada Injector attacks last month
Date: 2023-10-09T15:23:11-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-09-over-17000-wordpress-sites-hacked-in-balada-injector-attacks-last-month

[Source](https://www.bleepingcomputer.com/news/security/over-17-000-wordpress-sites-hacked-in-balada-injector-attacks-last-month/){:target="_blank" rel="noopener"}

> Multiple Balada Injector campaigns have compromised and infected over 17,000 WordPress sites using known flaws in premium theme plugins. [...]
