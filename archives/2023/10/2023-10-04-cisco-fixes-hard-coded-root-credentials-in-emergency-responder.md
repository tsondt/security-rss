Title: Cisco fixes hard-coded root credentials in Emergency Responder
Date: 2023-10-04T12:43:46-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-04-cisco-fixes-hard-coded-root-credentials-in-emergency-responder

[Source](https://www.bleepingcomputer.com/news/security/cisco-fixes-hard-coded-root-credentials-in-emergency-responder/){:target="_blank" rel="noopener"}

> Cisco released security updates to fix a Cisco Emergency Responder (CER) vulnerability that let attackers log into unpatched systems using hard-coded credentials. [...]
