Title: Over 40,000 admin portal accounts use 'admin' as a password
Date: 2023-10-17T17:47:32-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-10-17-over-40000-admin-portal-accounts-use-admin-as-a-password

[Source](https://www.bleepingcomputer.com/news/security/over-40-000-admin-portal-accounts-use-admin-as-a-password/){:target="_blank" rel="noopener"}

> Security researchers found that IT administrators are using tens of thousands of weak passwords to protect access to portals, leaving the door open to cyberattacks on enterprise networks. [...]
