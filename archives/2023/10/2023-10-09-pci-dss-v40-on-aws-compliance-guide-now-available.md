Title: PCI DSS v4.0 on AWS Compliance Guide now available
Date: 2023-10-09T16:51:14+00:00
Author: Ted Tanner
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS Compliance;Compliance;PCI DSS;Security Blog;Segmentation;Whitepaper
Slug: 2023-10-09-pci-dss-v40-on-aws-compliance-guide-now-available

[Source](https://aws.amazon.com/blogs/security/pci-dss-v4-0-on-aws-compliance-guide-now-available/){:target="_blank" rel="noopener"}

> Our mission at AWS Security Assurance Services is to ease Payment Card Industry Data Security Standard (PCI DSS) compliance for Amazon Web Services (AWS) customers. We work closely with AWS customers to answer their questions about understanding compliance on the AWS Cloud, finding and implementing solutions, and optimizing their controls and assessments. The most frequent [...]
