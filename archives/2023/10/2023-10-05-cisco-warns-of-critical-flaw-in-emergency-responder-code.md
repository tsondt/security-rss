Title: Cisco warns of critical flaw in Emergency Responder code
Date: 2023-10-05T19:45:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-10-05-cisco-warns-of-critical-flaw-in-emergency-responder-code

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/05/cisco_icritical_emergency/){:target="_blank" rel="noopener"}

> Hard-coded credentials strike again Cisco has issued a security advisory about a vulnerability in its Emergency Responder software that would allow an unauthenticated remote attacker to log in to an affected device using the root account.... [...]
