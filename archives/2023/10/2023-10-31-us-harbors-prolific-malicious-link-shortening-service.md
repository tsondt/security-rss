Title: .US Harbors Prolific Malicious Link Shortening Service
Date: 2023-10-31T13:26:55+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Web Fraud 2.0;Black Pumas;blackpumaoct33@ukr.net;GoDaddy;Infoblox;Josef Bakhovsky;Kristaps Ronka;Leila Puma;malicious link shortener;Namecheap;NameSilo;National Telecommunications and Information Administration;Prolific Puma
Slug: 2023-10-31-us-harbors-prolific-malicious-link-shortening-service

[Source](https://krebsonsecurity.com/2023/10/us-harbors-prolific-malicious-link-shortening-service/){:target="_blank" rel="noopener"}

> The top-level domain for the United States —.US — is home to thousands of newly-registered domains tied to a malicious link shortening service that facilitates malware and phishing scams, new research suggests. The findings come close on the heels of a report that identified.US domains as among the most prevalent in phishing attacks over the past year. Researchers at Infoblox say they’ve been tracking what appears to be a three-year-old link shortening service that is catering to phishers and malware purveyors. Infoblox found the domains involved are typically three to seven characters long, and hosted on bulletproof hosting providers that [...]
