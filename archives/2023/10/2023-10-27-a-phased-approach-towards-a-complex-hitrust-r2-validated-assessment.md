Title: A phased approach towards a complex HITRUST r2 validated assessment
Date: 2023-10-27T18:06:50+00:00
Author: Abdul Javid
Category: AWS Security
Tags: Advanced (300);Best Practices;Security, Identity, & Compliance;Thought Leadership;AWS HITRUST;HITRUST CSF certification;Security Blog
Slug: 2023-10-27-a-phased-approach-towards-a-complex-hitrust-r2-validated-assessment

[Source](https://aws.amazon.com/blogs/security/a-phased-approach-towards-a-complex-hitrust-r2-validated-assessment/){:target="_blank" rel="noopener"}

> Health Information Trust Alliance (HITRUST) offers healthcare organizations a comprehensive and standardized approach to information security, privacy, and compliance. HITRUST Common Security Framework (HITRUST CSF) can be used by organizations to establish a robust security program, ensure patient data privacy, and assist with compliance with industry regulations. HITRUST CSF enhances security, streamlines compliance efforts, reduces [...]
