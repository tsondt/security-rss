Title: How it works: The novel HTTP/2 ‘Rapid Reset’ DDoS attack
Date: 2023-10-10T12:57:31+00:00
Author: Daniele Iamartino
Category: GCP Security
Tags: Networking;Google Cloud;Security & Identity
Slug: 2023-10-10-how-it-works-the-novel-http2-rapid-reset-ddos-attack

[Source](https://cloud.google.com/blog/products/identity-security/how-it-works-the-novel-http2-rapid-reset-ddos-attack/){:target="_blank" rel="noopener"}

> A number of Google services and Cloud customers have been targeted with a novel HTTP/2-based DDoS attack which peaked in August. These attacks were significantly larger than any previously-reported Layer 7 attacks, with the largest attack surpassing 398 million requests per second. The attacks were largely stopped at the edge of our network by Google's global load balancing infrastructure and did not lead to any outages. While the impact was minimal, Google's DDoS Response Team reviewed the attacks and added additional protections to further mitigate similar attacks. In addition to Google's internal response, we helped lead a coordinated disclosure process [...]
