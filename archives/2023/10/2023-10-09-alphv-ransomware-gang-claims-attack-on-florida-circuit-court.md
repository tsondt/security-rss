Title: ALPHV ransomware gang claims attack on Florida circuit court
Date: 2023-10-09T17:32:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-09-alphv-ransomware-gang-claims-attack-on-florida-circuit-court

[Source](https://www.bleepingcomputer.com/news/security/alphv-ransomware-gang-claims-attack-on-florida-circuit-court/){:target="_blank" rel="noopener"}

> The ALPHV (BlackCat) ransomware gang has claimed an attack that affected state courts across Northwest Florida (part of the First Judicial Circuit) last week. [...]
