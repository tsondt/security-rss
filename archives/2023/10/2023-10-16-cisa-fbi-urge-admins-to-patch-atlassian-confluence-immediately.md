Title: CISA, FBI urge admins to patch Atlassian Confluence immediately
Date: 2023-10-16T11:05:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-16-cisa-fbi-urge-admins-to-patch-atlassian-confluence-immediately

[Source](https://www.bleepingcomputer.com/news/security/cisa-fbi-urge-admins-to-patch-atlassian-confluence-immediately/){:target="_blank" rel="noopener"}

> CISA, FBI, and MS-ISAC warned network admins today to immediately patch their Atlassian Confluence servers against a maximum severity flaw actively exploited in attacks. [...]
