Title: Malicious Solana, Kucoin packages infect NuGet devs with SeroXen RAT
Date: 2023-10-12T13:40:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-12-malicious-solana-kucoin-packages-infect-nuget-devs-with-seroxen-rat

[Source](https://www.bleepingcomputer.com/news/security/malicious-solana-kucoin-packages-infect-nuget-devs-with-seroxen-rat/){:target="_blank" rel="noopener"}

> Malicious NuGet packages appearing to have over 2 million downloads impersonate crypto wallets, crypto exchange, and Discord libraries to infect developers with the SeroXen remote access trojan. [...]
