Title: Single Sign On and the Cybercrime Ecosystem
Date: 2023-10-18T10:02:04-04:00
Author: Sponsored by Flare
Category: BleepingComputer
Tags: Security
Slug: 2023-10-18-single-sign-on-and-the-cybercrime-ecosystem

[Source](https://www.bleepingcomputer.com/news/security/single-sign-on-and-the-cybercrime-ecosystem/){:target="_blank" rel="noopener"}

> One of the trends driving an increase is the compromise of enterprise single sign on (SSO) applications are info-stealer malware attacks. Learn more from Flare about this cybercrime ecosystem. [...]
