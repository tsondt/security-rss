Title: Microsoft: Hackers target Azure cloud VMs via breached SQL servers
Date: 2023-10-04T10:53:25-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud;Microsoft
Slug: 2023-10-04-microsoft-hackers-target-azure-cloud-vms-via-breached-sql-servers

[Source](https://www.bleepingcomputer.com/news/security/microsoft-hackers-target-azure-cloud-vms-via-breached-sql-servers/){:target="_blank" rel="noopener"}

> Hackers have been observed trying to breach cloud environments through Microsoft SQL Servers vulnerable to SQL injection. [...]
