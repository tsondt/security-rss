Title: GoldDigger Android trojan targets Vietnamese banking apps, code contains hints of wider targets
Date: 2023-10-06T01:06:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-10-06-golddigger-android-trojan-targets-vietnamese-banking-apps-code-contains-hints-of-wider-targets

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/06/golddigger_android_trojan_vietnam_attacks/){:target="_blank" rel="noopener"}

> More malware scum using acessibility features to steal personal info Singapore-based infosec outfit Group-IB on Thursday released details of a new Android trojan that exploits the operating system's accessibility features to steal info that enables theft of personal information.... [...]
