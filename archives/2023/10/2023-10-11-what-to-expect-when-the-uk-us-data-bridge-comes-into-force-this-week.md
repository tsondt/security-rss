Title: What to expect when the UK-US Data Bridge comes into force this week
Date: 2023-10-11T09:15:11+00:00
Author: James Castro-Edwards, data privacy lawyer at Arnold and Porter
Category: The Register
Tags: 
Slug: 2023-10-11-what-to-expect-when-the-uk-us-data-bridge-comes-into-force-this-week

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/11/uk_us_data_bridge/){:target="_blank" rel="noopener"}

> Britain's privacy watchdog still not happy that agreement 'appropriately' protects sensitive data Opinion The UK Extension to the EU-US Data Privacy Framework (aka Data Bridge) will enter into force on October 12, allowing certifying entities to easily transfer personal data from the UK to the US.... [...]
