Title: CISA barred from coordinating with social media sites to police misinformation
Date: 2023-10-04T18:15:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-10-04-cisa-barred-from-coordinating-with-social-media-sites-to-police-misinformation

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/04/cisa_barred_from_coordinating_with/){:target="_blank" rel="noopener"}

> The 5th Circuit's re-ruling adds CISA to a list of alleged first-amendment violators. Next stop: Supreme Court The US Fifth Circuit Court of Appeals has modified a ruling from last month to add the Cybersecurity and Infrastructure Security Agency (CISA) to a list of US government entities prohibited from working with social media outfits to curtail the spread of misinformation.... [...]
