Title: Building core strength: New technical papers on infrastructure security
Date: 2023-10-27T16:00:00+00:00
Author: Kevin Plybon
Category: GCP Security
Tags: Security & Identity
Slug: 2023-10-27-building-core-strength-new-technical-papers-on-infrastructure-security

[Source](https://cloud.google.com/blog/products/identity-security/building-core-strength-new-technical-papers-on-infrastructure-security/){:target="_blank" rel="noopener"}

> Google’s infrastructure security teams continue to advance the state of the art in securing distributed systems. As the scale, capabilities, and geographical locations of our data centers and compute platforms grow, we continue to evolve the systems, controls, and technology used to secure them against external threats and insider risk. Building on the principles laid out in Building Secure and Reliable Systems, we are excited to announce a new series of technical whitepapers on infrastructure security. The series begins with two papers: Protecting the physical-to-logical space in a data center Enforcing boot integrity on production machines These papers are technical, [...]
