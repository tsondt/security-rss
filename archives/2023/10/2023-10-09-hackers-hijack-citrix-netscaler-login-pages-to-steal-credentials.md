Title: Hackers hijack Citrix NetScaler login pages to steal credentials
Date: 2023-10-09T10:45:01-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-09-hackers-hijack-citrix-netscaler-login-pages-to-steal-credentials

[Source](https://www.bleepingcomputer.com/news/security/hackers-hijack-citrix-netscaler-login-pages-to-steal-credentials/){:target="_blank" rel="noopener"}

> Hackers are conducting a large-scale campaign to exploit the recent CVE-2023-3519 flaw in Citrix NetScaler Gateways to steal user credentials. [...]
