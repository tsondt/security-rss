Title: Helping you bridge the cloud security gap
Date: 2023-10-24T08:15:10+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2023-10-24-helping-you-bridge-the-cloud-security-gap

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/24/helping_you_bridge_the_cloud/){:target="_blank" rel="noopener"}

> Learn how to implement effective identity and access management with Entra ID and SANS Sponsored Post The job of the cyber security professional is never easy, and it gets progressively harder with the movement of sensitive data and applications across the multiple different on and off premise systems that make up modern hybrid cloud environments.... [...]
