Title: Vulnerabilities in Supermicro BMCs could allow for unkillable server rootkits
Date: 2023-10-04T22:21:10+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;baseboard management controllers;bmcs;firmware;rootkits;vulnerabilities
Slug: 2023-10-04-vulnerabilities-in-supermicro-bmcs-could-allow-for-unkillable-server-rootkits

[Source](https://arstechnica.com/?p=1973415){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) If your organization uses servers that are equipped with baseboard management controllers from Supermicro, it’s time, once again, to patch seven high-severity vulnerabilities that attackers could exploit to gain control of them. And sorry, but the fixes must be installed manually. Typically abbreviated as BMCs, baseboard management controllers are small chips that are soldered onto the motherboard of servers inside data centers. Administrators rely on these powerful controllers for various remote management capabilities, including installing updates, monitoring temperatures and setting fan speeds accordingly, and reflashing the UEFI system firmware that allows servers to load their operating [...]
