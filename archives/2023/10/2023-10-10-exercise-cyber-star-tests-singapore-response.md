Title: Exercise Cyber Star tests Singapore response
Date: 2023-10-10T02:31:12+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2023-10-10-exercise-cyber-star-tests-singapore-response

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/10/exercise_cyber_star_tests_singapore/){:target="_blank" rel="noopener"}

> How SANS is helping boost the island’s defenses against whole-of-nation cyber attacks Sponsored The cyber attack which culminated in the personal details of 1.5m patients being compromised after hackers broke into the databases of SingHealth in 2018 provides a stark illustration of why organizations in Singapore need to remain vigilant and well protected against further incidents.... [...]
