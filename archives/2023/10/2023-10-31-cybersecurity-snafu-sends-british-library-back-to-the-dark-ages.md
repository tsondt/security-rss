Title: Cybersecurity snafu sends British Library back to the Dark Ages
Date: 2023-10-31T14:16:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-31-cybersecurity-snafu-sends-british-library-back-to-the-dark-ages

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/31/british_library_it_outage/){:target="_blank" rel="noopener"}

> Internet, phone lines, websites, and more went down on Saturday morning The British Library has confirmed to The Register that a "cyber incident" is the cause of a "major" multi-day IT outage.... [...]
