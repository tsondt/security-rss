Title: Now Russians accused of pwning JFK taxi system to sell top spots to cabbies
Date: 2023-10-31T19:16:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-10-31-now-russians-accused-of-pwning-jfk-taxi-system-to-sell-top-spots-to-cabbies

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/31/russians_nyc_jfk_taxi_hacking/){:target="_blank" rel="noopener"}

> Big Apple unlikely to get a bite out of them at this rate, though For a period of two years between September 2019 and September 2021, two Americans and two Russians allegedly compromising the taxi dispatch system at John F. Kennedy International Airport in New York to sell cabbies a place at the front of the dispatch line.... [...]
