Title: LinkedIn Smart Links attacks return to target Microsoft accounts
Date: 2023-10-11T09:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-11-linkedin-smart-links-attacks-return-to-target-microsoft-accounts

[Source](https://www.bleepingcomputer.com/news/security/linkedin-smart-links-attacks-return-to-target-microsoft-accounts/){:target="_blank" rel="noopener"}

> Hackers are once again abusing LinkedIn Smart Links in phishing attacks to bypass protection measures and evade detection in attempts to steal Microsoft account credentials. [...]
