Title: FBI warns of extortion groups targeting plastic surgery offices
Date: 2023-10-18T02:55:34-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-18-fbi-warns-of-extortion-groups-targeting-plastic-surgery-offices

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-extortion-groups-targeting-plastic-surgery-offices/){:target="_blank" rel="noopener"}

> The FBI warned of cybercriminals using spoofed emails and phone numbers to target plastic surgery offices across the United States for extortion in phishing attacks that spread malware. [...]
