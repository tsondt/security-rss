Title: Exim patches three of six zero-day bugs disclosed last week
Date: 2023-10-02T17:50:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-02-exim-patches-three-of-six-zero-day-bugs-disclosed-last-week

[Source](https://www.bleepingcomputer.com/news/security/exim-patches-three-of-six-zero-day-bugs-disclosed-last-week/){:target="_blank" rel="noopener"}

> Exim developers have released patches for three of the zero-days disclosed last week through Trend Micro's Zero Day Initiative (ZDI), one of them allowing unauthenticated attackers to gain remote code execution. [...]
