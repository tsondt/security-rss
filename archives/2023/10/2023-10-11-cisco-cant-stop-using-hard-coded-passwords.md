Title: Cisco Can’t Stop Using Hard-Coded Passwords
Date: 2023-10-11T11:04:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Cisco;passwords;vulnerabilities
Slug: 2023-10-11-cisco-cant-stop-using-hard-coded-passwords

[Source](https://www.schneier.com/blog/archives/2023/10/cisco-cant-stop-using-hard-coded-passwords.html){:target="_blank" rel="noopener"}

> There’s a new Cisco vulnerability in its Emergency Responder product: This vulnerability is due to the presence of static user credentials for the root account that are typically reserved for use during development. An attacker could exploit this vulnerability by using the account to log in to an affected system. A successful exploit could allow the attacker to log in to the affected system and execute arbitrary commands as the root user. This is not the first time Cisco products have had hard-coded passwords made public. You’d think it would learn. [...]
