Title: AWS announces Cloud Companion Guide for the CSA Cyber Trust mark
Date: 2023-10-16T20:25:48+00:00
Author: Kimberly Dickson
Category: AWS Security
Tags: Announcements;Intermediate (200);Security, Identity, & Compliance;AWS Compliance;Compliance;Security Blog;Singapore;Whitepaper
Slug: 2023-10-16-aws-announces-cloud-companion-guide-for-the-csa-cyber-trust-mark

[Source](https://aws.amazon.com/blogs/security/aws-announces-cloud-companion-guide-for-the-csa-cyber-trust-mark/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is excited to announce the release of a new Cloud Companion Guide to help customers prepare for the Cyber Trust mark developed by the Cyber Security Agency of Singapore (CSA). The Cloud Companion Guide to the CSA’s Cyber Trust mark provides guidance and a mapping of AWS services and features to [...]
