Title: Apple Private Wi-Fi hasn't worked for the past three years
Date: 2023-10-27T22:30:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-10-27-apple-private-wi-fi-hasnt-worked-for-the-past-three-years

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/27/apple_private_wifi_fixed/){:target="_blank" rel="noopener"}

> Not exactly the MAC daddy Three years after Apple introduced a menu setting called Private Wi-Fi Address, a way to spoof network identifiers called MAC addresses, the privacy protection may finally work as advertised, thanks to a software fix.... [...]
