Title: reCAPTCHA Enterprise and the importance of GDPR compliance
Date: 2023-10-12T16:00:00+00:00
Author: David Lenehan
Category: GCP Security
Tags: Security & Identity
Slug: 2023-10-12-recaptcha-enterprise-and-the-importance-of-gdpr-compliance

[Source](https://cloud.google.com/blog/products/identity-security/recaptcha-enterprise-and-the-importance-of-gdpr-compliance/){:target="_blank" rel="noopener"}

> The General Data Protection Regulation (GDPR) sets out specific requirements for businesses and organisations who are established in Europe or who serve users in Europe. It regulates how businesses can collect, use, and store personal data. At Google Cloud, we prioritise the security and privacy of your data, and we want you, as a reCAPTCHA Enterprise customer, to feel confident using our services in light of GDPR requirements. As a reCAPTCHA Enterprise customer, we support your GDPR compliance efforts by: Committing in our contracts to process your customer personal data in reCAPTCHA Enterprise only as you instruct us, and to [...]
