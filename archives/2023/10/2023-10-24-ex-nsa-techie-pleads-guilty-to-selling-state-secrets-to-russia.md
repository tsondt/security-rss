Title: Ex-NSA techie pleads guilty to selling state secrets to Russia
Date: 2023-10-24T16:45:15+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-10-24-ex-nsa-techie-pleads-guilty-to-selling-state-secrets-to-russia

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/24/nsa_techie_russia_espionage/){:target="_blank" rel="noopener"}

> Wannabe spy undone by system logs, among other lapses in judgement A former US National Security Agency techie has plead guilty to six counts of violating the Espionage Act after being caught handing classified information to FBI agents he thought were Russian spies.... [...]
