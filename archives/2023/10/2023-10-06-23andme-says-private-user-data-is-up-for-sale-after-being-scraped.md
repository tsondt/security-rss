Title: 23andMe says private user data is up for sale after being scraped
Date: 2023-10-06T23:58:11+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;23;23andme;data breach;privacy;scraping
Slug: 2023-10-06-23andme-says-private-user-data-is-up-for-sale-after-being-scraped

[Source](https://arstechnica.com/?p=1974265){:target="_blank" rel="noopener"}

> Enlarge / The 23andMe logo displayed on a smartphone screen. Genetic profiling service 23andMe has commenced an investigation after private user data was been scraped off its website Friday’s confirmation comes five days after an unknown entity took to an online crime forum to advertise the sale of private information for millions of 23andMe users. The forum posts claimed that the stolen data included origin estimation, phenotype, health information, photos, and identification data. The posts claimed that 23andMe’s CEO was aware the company had been “hacked” two months earlier and never revealed the incident. In a statement emailed after this [...]
