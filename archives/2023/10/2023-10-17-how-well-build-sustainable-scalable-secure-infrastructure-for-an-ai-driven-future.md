Title: How we’ll build sustainable, scalable, secure infrastructure for an AI-driven future
Date: 2023-10-17T15:30:00+00:00
Author: Amin Vahdat
Category: GCP Security
Tags: Sustainability;Networking;Security & Identity;Systems
Slug: 2023-10-17-how-well-build-sustainable-scalable-secure-infrastructure-for-an-ai-driven-future

[Source](https://cloud.google.com/blog/topics/systems/google-systems-innovations-at-ocp-global-summit/){:target="_blank" rel="noopener"}

> Editor’s note: Today, we hear from Parthasarathy Ranganathan, Google VP and Technical Fellow and Amin Vahdat, VP/GM. Partha delivered a keynote address today at the OCP Global Summit, an annual conference for leaders, researchers, and pioneers in the open hardware industry. Partha served on the OCP Board of Directors from 2020 to earlier this year, when he was succeeded by Amber Huffman as Google’s representative. Read on to hear about the macro trends driving systems design today, and an overview of all of our activities in the community. At Google, we build planet-scale computing for services that power billions of [...]
