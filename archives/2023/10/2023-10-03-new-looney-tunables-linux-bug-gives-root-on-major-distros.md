Title: New 'Looney Tunables' Linux bug gives root on major distros
Date: 2023-10-03T16:36:45-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Linux
Slug: 2023-10-03-new-looney-tunables-linux-bug-gives-root-on-major-distros

[Source](https://www.bleepingcomputer.com/news/security/new-looney-tunables-linux-bug-gives-root-on-major-distros/){:target="_blank" rel="noopener"}

> A new Linux vulnerability known as 'Looney Tunables' enables local attackers to gain root privileges by exploiting a buffer overflow weakness in the GNU C Library's ld.so dynamic loader. [...]
