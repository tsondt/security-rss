Title: Coin Flips Are Biased
Date: 2023-10-16T11:06:00+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;gambling;random numbers
Slug: 2023-10-16-coin-flips-are-biased

[Source](https://www.schneier.com/blog/archives/2023/10/coin-flips-are-biased.html){:target="_blank" rel="noopener"}

> Experimental result : Many people have flipped coins but few have stopped to ponder the statistical and physical intricacies of the process. In a preregistered study we collected 350,757 coin flips to test the counterintuitive prediction from a physics model of human coin tossing developed by Persi Diaconis. The model asserts that when people flip an ordinary coin, it tends to land on the same side it started—Diaconis estimated the probability of a same-side outcome to be about 51%. And the final paragraph: Could future coin tossers use the same-side bias to their advantage? The magnitude of the observed bias [...]
