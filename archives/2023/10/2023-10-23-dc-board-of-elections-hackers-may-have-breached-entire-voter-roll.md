Title: D.C. Board of Elections: Hackers may have breached entire voter roll
Date: 2023-10-23T04:32:31-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-23-dc-board-of-elections-hackers-may-have-breached-entire-voter-roll

[Source](https://www.bleepingcomputer.com/news/security/dc-board-of-elections-hackers-may-have-breached-entire-voter-roll/){:target="_blank" rel="noopener"}

> The District of Columbia Board of Elections (DCBOE) says that a threat actor who breached a web server operated by the DataNet Systems hosting provider in early October may have obtained access to the personal information of all registered voters. [...]
