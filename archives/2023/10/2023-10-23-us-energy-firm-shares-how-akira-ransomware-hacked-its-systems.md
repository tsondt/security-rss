Title: US energy firm shares how Akira ransomware hacked its systems
Date: 2023-10-23T12:35:43-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-23-us-energy-firm-shares-how-akira-ransomware-hacked-its-systems

[Source](https://www.bleepingcomputer.com/news/security/us-energy-firm-shares-how-akira-ransomware-hacked-its-systems/){:target="_blank" rel="noopener"}

> In a rare display of transparency, US energy services firm BHI Energy details how the Akira ransomware operation breached their networks and stole the data during the attack. [...]
