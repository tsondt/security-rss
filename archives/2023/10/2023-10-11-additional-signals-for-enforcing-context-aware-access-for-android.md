Title: Additional signals for enforcing Context Aware Access for Android
Date: 2023-10-11T16:00:00+00:00
Author: Abhishek Swaroop
Category: GCP Security
Tags: Security & Identity
Slug: 2023-10-11-additional-signals-for-enforcing-context-aware-access-for-android

[Source](https://cloud.google.com/blog/products/identity-security/additional-signals-for-enforcing-context-aware-access-for-android/){:target="_blank" rel="noopener"}

> A Zero Trust approach to security can help you safeguard your users, devices, and apps as well as protect your data against unauthorized access or exfiltration. As part of Google Cloud’s efforts to help organizations adopt Zero Trust, we designed our BeyondCorp Enterprise (BCE) solution to be an extensible platform enabling customers to use a variety of signals from Chrome, desktop operating systems, and mobile devices. BeyondCorp Enterprise, Workspace CAA, and Cloud Identity can now receive critical Android device security signals for both advanced managed devices and, for the first time, basic managed devices. For example, a customer can now [...]
