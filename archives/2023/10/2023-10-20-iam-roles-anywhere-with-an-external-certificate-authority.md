Title: IAM Roles Anywhere with an external certificate authority
Date: 2023-10-20T19:59:50+00:00
Author: Cody Penta
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2023-10-20-iam-roles-anywhere-with-an-external-certificate-authority

[Source](https://aws.amazon.com/blogs/security/iam-roles-anywhere-with-an-external-certificate-authority/){:target="_blank" rel="noopener"}

> AWS Identity and Access Management Roles Anywhere allows you to use temporary Amazon Web Services (AWS) credentials outside of AWS by using X.509 Certificates issued by your certificate authority (CA). Faraz Angabini goes deep into using IAM Roles Anywhere in his blog post Extend AWS IAM roles to workloads outside of AWS with IAM Roles [...]
