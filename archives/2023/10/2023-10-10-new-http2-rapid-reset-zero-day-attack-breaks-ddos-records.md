Title: New 'HTTP/2 Rapid Reset' zero-day attack breaks DDoS records
Date: 2023-10-10T10:12:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-10-new-http2-rapid-reset-zero-day-attack-breaks-ddos-records

[Source](https://www.bleepingcomputer.com/news/security/new-http-2-rapid-reset-zero-day-attack-breaks-ddos-records/){:target="_blank" rel="noopener"}

> A new DDoS (distributed denial of service) technique named 'HTTP/2 Rapid Reset' has been actively exploited as a zero-day since August, breaking all previous records in magnitude. [...]
