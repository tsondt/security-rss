Title: Chilean telecom giant GTD hit by the Rorschach ransomware gang
Date: 2023-10-25T18:05:37-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-10-25-chilean-telecom-giant-gtd-hit-by-the-rorschach-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/chilean-telecom-giant-gtd-hit-by-the-rorschach-ransomware-gang/){:target="_blank" rel="noopener"}

> Chile's Grupo GTD warns that a cyberattack has impacted its Infrastructure as a Service (IaaS) platform, disrupting online services. [...]
