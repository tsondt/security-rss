Title: New whitepaper available: Charting a path to stronger security with Zero Trust
Date: 2023-10-11T15:13:07+00:00
Author: Quint Van Deman
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Security;Security Blog;Whitepaper;Zero Trust
Slug: 2023-10-11-new-whitepaper-available-charting-a-path-to-stronger-security-with-zero-trust

[Source](https://aws.amazon.com/blogs/security/new-whitepaper-available-charting-a-path-to-stronger-security-with-zero-trust/){:target="_blank" rel="noopener"}

> Security is a top priority for organizations looking to keep pace with a changing threat landscape and build customer trust. However, the traditional approach of defined security perimeters that separate trusted from untrusted network zones has proven to be inadequate as hybrid work models accelerate digital transformation. Today’s distributed enterprise requires a new approach to [...]
