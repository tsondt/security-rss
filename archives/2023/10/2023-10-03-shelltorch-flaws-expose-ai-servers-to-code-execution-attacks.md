Title: ShellTorch flaws expose AI servers to code execution attacks
Date: 2023-10-03T12:37:17-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-03-shelltorch-flaws-expose-ai-servers-to-code-execution-attacks

[Source](https://www.bleepingcomputer.com/news/security/shelltorch-flaws-expose-ai-servers-to-code-execution-attacks/){:target="_blank" rel="noopener"}

> A set of critical vulnerabilities dubbed 'ShellTorch' in the open-source TorchServe AI model-serving tool impact tens of thousands of internet-exposed servers, some of which belong to large organizations. [...]
