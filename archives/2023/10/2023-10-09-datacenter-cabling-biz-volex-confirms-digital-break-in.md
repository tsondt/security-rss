Title: Datacenter cabling biz Volex confirms digital break-in
Date: 2023-10-09T11:30:07+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-10-09-datacenter-cabling-biz-volex-confirms-digital-break-in

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/09/volex_confirms_cyber_attack/){:target="_blank" rel="noopener"}

> All sites operational, no 'material' financial impact expected but stock markets still worried Volex, the British integrated maker of critical power and data transmission cables, confirmed this morning that intruders accessed data after breaking into its tech infrastructure.... [...]
