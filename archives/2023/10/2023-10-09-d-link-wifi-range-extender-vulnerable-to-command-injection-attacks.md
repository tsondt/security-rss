Title: D-Link WiFi range extender vulnerable to command injection attacks
Date: 2023-10-09T17:53:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2023-10-09-d-link-wifi-range-extender-vulnerable-to-command-injection-attacks

[Source](https://www.bleepingcomputer.com/news/security/d-link-wifi-range-extender-vulnerable-to-command-injection-attacks/){:target="_blank" rel="noopener"}

> The popular D-Link DAP-X1860 WiFi 6 range extender is susceptible to a vulnerability allowing DoS (denial of service) attacks and remote command injection. [...]
