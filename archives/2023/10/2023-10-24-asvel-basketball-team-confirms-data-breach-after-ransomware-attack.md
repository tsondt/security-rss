Title: ASVEL basketball team confirms data breach after ransomware attack
Date: 2023-10-24T11:07:21-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-24-asvel-basketball-team-confirms-data-breach-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/asvel-basketball-team-confirms-data-breach-after-ransomware-attack/){:target="_blank" rel="noopener"}

> French professional basketball team LDLC ASVEL (ASVEL) has confirmed that data was stolen after the NoEscape ransomware gang claimed to have attacked the club. [...]
