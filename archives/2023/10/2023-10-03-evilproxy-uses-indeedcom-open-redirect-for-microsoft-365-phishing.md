Title: EvilProxy uses indeed.com open redirect for Microsoft 365 phishing
Date: 2023-10-03T09:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-03-evilproxy-uses-indeedcom-open-redirect-for-microsoft-365-phishing

[Source](https://www.bleepingcomputer.com/news/security/evilproxy-uses-indeedcom-open-redirect-for-microsoft-365-phishing/){:target="_blank" rel="noopener"}

> A recently uncovered phishing campaign is targeting Microsoft 365 accounts of key executives in U.S.-based organizations by abusing open redirects from the Indeed employment website for job listings. [...]
