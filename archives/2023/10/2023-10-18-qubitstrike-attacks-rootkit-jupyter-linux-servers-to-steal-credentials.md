Title: Qubitstrike attacks rootkit Jupyter Linux servers to steal credentials
Date: 2023-10-18T06:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud;Linux
Slug: 2023-10-18-qubitstrike-attacks-rootkit-jupyter-linux-servers-to-steal-credentials

[Source](https://www.bleepingcomputer.com/news/security/qubitstrike-attacks-rootkit-jupyter-linux-servers-to-steal-credentials/){:target="_blank" rel="noopener"}

> Hackers are scanning for internet-exposed Jupyter Notebooks to breach servers and deploy a cocktail of malware consisting of a Linux rootkit, crypto miners, and password-stealing scripts. [...]
