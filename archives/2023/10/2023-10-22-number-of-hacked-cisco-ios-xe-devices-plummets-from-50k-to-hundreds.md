Title: Number of hacked Cisco IOS XE devices plummets from 50K to hundreds
Date: 2023-10-22T13:37:01-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-10-22-number-of-hacked-cisco-ios-xe-devices-plummets-from-50k-to-hundreds

[Source](https://www.bleepingcomputer.com/news/security/number-of-hacked-cisco-ios-xe-devices-plummets-from-50k-to-hundreds/){:target="_blank" rel="noopener"}

> The number of Cisco IOS XE devices hacked with a malicious backdoor implant has mysteriously plummeted from over 50,000 impacted devices to only a few hundred, with researchers unsure what is causing the sharp decline. [...]
