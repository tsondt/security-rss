Title: Prepare your AWS workloads for the “Operational risks and resilience – banks” FINMA Circular
Date: 2023-10-31T14:48:47+00:00
Author: Margo Cronin
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;AWS Compliance;AWS FINMA;AWS security;Certification;cybersecurity;Financial Services;Outsourcing Guidelines;Security Blog;Swiss banking regulations;Switzerland
Slug: 2023-10-31-prepare-your-aws-workloads-for-the-operational-risks-and-resilience-banks-finma-circular

[Source](https://aws.amazon.com/blogs/security/prepare-your-aws-workloads-for-the-operational-risks-and-resilience-banks-finma-circular/){:target="_blank" rel="noopener"}

> In December 2022, FINMA, the Swiss Financial Market Supervisory Authority, announced a fully revised circular called Operational risks and resilience – banks that will take effect on January 1, 2024. The circular will replace the Swiss Bankers Association’s Recommendations for Business Continuity Management (BCM), which is currently recognized as a minimum standard. The new circular [...]
