Title: US officials close to persuading allies to not pay off ransomware crooks
Date: 2023-10-31T22:49:01+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-10-31-us-officials-close-to-persuading-allies-to-not-pay-off-ransomware-crooks

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/31/us_ransomware_payment_ban/){:target="_blank" rel="noopener"}

> 'We're still in the final throes of getting every last member to sign' Top White House officials are working to secure an agreement between almost 50 countries to not pay ransom demands to cybercriminals as the international Counter Ransomware Initiative (CRI) summit gets underway in Washington DC Tuesday.... [...]
