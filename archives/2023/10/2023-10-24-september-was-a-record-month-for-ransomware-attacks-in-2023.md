Title: September was a record month for ransomware attacks in 2023
Date: 2023-10-24T03:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-24-september-was-a-record-month-for-ransomware-attacks-in-2023

[Source](https://www.bleepingcomputer.com/news/security/september-was-a-record-month-for-ransomware-attacks-in-2023/){:target="_blank" rel="noopener"}

> Ransomware activity in September reached unprecedented levels following a relative lull in August that was still way above regular standards for summer months. [...]
