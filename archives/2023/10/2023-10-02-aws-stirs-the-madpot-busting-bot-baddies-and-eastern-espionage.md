Title: AWS stirs the MadPot – busting bot baddies and eastern espionage
Date: 2023-10-02T10:45:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-10-02-aws-stirs-the-madpot-busting-bot-baddies-and-eastern-espionage

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/02/aws_security_madpot/){:target="_blank" rel="noopener"}

> Security exec Mark Ryland spills the tea on hush-hush threat intel tool Interview AWS has unveiled MadPot, its previously secret threat-intelligence tool that one of the cloud giant's security execs tells us has thwarted Chinese and Russian spies – and millions of bots.... [...]
