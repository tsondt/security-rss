Title: Ace holed: Hardware store empire felled by cyberattack
Date: 2023-10-31T17:33:06+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-10-31-ace-holed-hardware-store-empire-felled-by-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/31/ace_hardware_cyberattack/){:target="_blank" rel="noopener"}

> US outfit scrambles to repair operations, restore processing of online orders Ace Hardware appears to have been the latest organization to succumb to a cyberattack, judging by its website and a message from CEO John Venhuizen.... [...]
