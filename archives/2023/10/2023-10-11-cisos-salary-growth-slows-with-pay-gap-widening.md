Title: CISOs' salary growth slows – with pay gap widening
Date: 2023-10-11T14:36:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-11-cisos-salary-growth-slows-with-pay-gap-widening

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/11/cisos_salary_growth_slows/){:target="_blank" rel="noopener"}

> We still doubt any infosec leaders will be going without heating this winter The gap between the top and bottom-earning CISOs is growing wider, with the highest-paid execs having their salaries increased at three times the rate of those at the lower echelons.... [...]
