Title: Updated Essential Eight guidance for Australian customers
Date: 2023-10-23T20:32:49+00:00
Author: James Kingsmill
Category: AWS Security
Tags: Announcements;AWS Artifact;Foundational (100);Security, Identity, & Compliance;Auditing;Australia;AWS security;Compliance;IRAP;MEL Region;Security Blog;SYD Region
Slug: 2023-10-23-updated-essential-eight-guidance-for-australian-customers

[Source](https://aws.amazon.com/blogs/security/updated-essential-eight-guidance-for-australian-customers/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is excited to announce the release of AWS Prescriptive Guidance on Reaching Essential Eight Maturity on AWS. We designed this guidance to help customers streamline and accelerate their security compliance obligations under the Essential Eight framework of the Australian Cyber Security Centre (ACSC). What is the Essential Eight? The Essential Eight is [...]
