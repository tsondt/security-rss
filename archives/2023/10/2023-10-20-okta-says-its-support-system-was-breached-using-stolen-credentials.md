Title: Okta says its support system was breached using stolen credentials
Date: 2023-10-20T14:41:40-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-20-okta-says-its-support-system-was-breached-using-stolen-credentials

[Source](https://www.bleepingcomputer.com/news/security/okta-says-its-support-system-was-breached-using-stolen-credentials/){:target="_blank" rel="noopener"}

> ​Okta says attackers accessed files containing cookies and session tokens uploaded by customers to its support management system after breaching it using stolen credentials. [...]
