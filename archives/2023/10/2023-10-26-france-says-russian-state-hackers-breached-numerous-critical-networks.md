Title: France says Russian state hackers breached numerous critical networks
Date: 2023-10-26T12:40:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-26-france-says-russian-state-hackers-breached-numerous-critical-networks

[Source](https://www.bleepingcomputer.com/news/security/france-says-russian-state-hackers-breached-numerous-critical-networks/){:target="_blank" rel="noopener"}

> The Russian APT28 hacking group (aka 'Strontium' or 'Fancy Bear') has been targeting government entities, businesses, universities, research institutes, and think tanks in France since the second half of 2021. [...]
