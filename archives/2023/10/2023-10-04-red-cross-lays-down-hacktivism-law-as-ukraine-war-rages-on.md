Title: Red Cross lays down hacktivism law as Ukraine war rages on
Date: 2023-10-04T19:03:00+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-04-red-cross-lays-down-hacktivism-law-as-ukraine-war-rages-on

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/04/red_cross_hacktivist_rules/){:target="_blank" rel="noopener"}

> Rules apply to cyber vigilantes and their home nations, but experts cast doubt over potential benefits New guidelines have been codified to govern the rules of engagement concerning hacktivists involved in ongoing cyber warfare.... [...]
