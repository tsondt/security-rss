Title: Google mitigated the largest DDoS attack to date, peaking above 398 million rps
Date: 2023-10-10T12:00:01+00:00
Author: Tim April
Category: GCP Security
Tags: Networking;Google Cloud;Security & Identity
Slug: 2023-10-10-google-mitigated-the-largest-ddos-attack-to-date-peaking-above-398-million-rps

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-mitigated-largest-ddos-attack-peaking-above-398-million-rps/){:target="_blank" rel="noopener"}

> Over the last few years, Google's DDoS Response Team has observed the trend that distributed denial-of-service (DDoS) attacks are increasing exponentially in size. Last year, we blocked the largest DDoS attack recorded at the time. This August, we stopped an even larger DDoS attack — 71⁄2 times larger — that also used new techniques to try to disrupt websites and Internet services. This new series of DDoS attacks reached a peak of 398 million requests per second (rps), and relied on a novel HTTP/2 “Rapid Reset” technique based on stream multiplexing that has affected multiple Internet infrastructure companies. By contrast, [...]
