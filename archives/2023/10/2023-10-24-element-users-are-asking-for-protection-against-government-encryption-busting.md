Title: Element users are asking for protection against government encryption busting
Date: 2023-10-24T14:30:14+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-10-24-element-users-are-asking-for-protection-against-government-encryption-busting

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/24/element_spy_clause_protection/){:target="_blank" rel="noopener"}

> NATO, United Nations, US DoD, and French government among its customer base Element, one of the companies behind decentralized comms platform Matrix, says customers are asking it to insert a protective clause from the encryption-busting element of UK government's Online Safety Bill (OSB).... [...]
