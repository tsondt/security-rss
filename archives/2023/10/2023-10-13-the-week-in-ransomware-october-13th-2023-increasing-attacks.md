Title: The Week in Ransomware - October 13th 2023 - Increasing Attacks
Date: 2023-10-13T18:26:21-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-10-13-the-week-in-ransomware-october-13th-2023-increasing-attacks

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-october-13th-2023-increasing-attacks/){:target="_blank" rel="noopener"}

> Ransomware gangs continue to pummel the enterprise, with attacks causing disruption in business operations and resulting in data breaches if a ransom is not paid. [...]
