Title: Pro-Russia hackers target inboxes with 0-day in webmail app used by millions
Date: 2023-10-25T22:21:49+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;roundcube;winter vivern;xss;zero-day
Slug: 2023-10-25-pro-russia-hackers-target-inboxes-with-0-day-in-webmail-app-used-by-millions

[Source](https://arstechnica.com/?p=1978806){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) A relentless team of pro-Russia hackers has been exploiting a zero-day vulnerability in widely used webmail software in attacks targeting governmental entities and a think tank, all in Europe, researchers from security firm ESET said on Wednesday. The previously unknown vulnerability resulted from a critical cross-site scripting error in Roundcube, a server application used by more than 1,000 webmail services and millions of their end users. Members of a pro-Russia and Belarus hacking group tracked as Winter Vivern used the XSS bug to inject JavaScript into the Roundcube server application. The injection was triggered simply by [...]
