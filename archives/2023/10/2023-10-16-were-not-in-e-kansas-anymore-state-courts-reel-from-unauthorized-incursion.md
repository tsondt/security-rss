Title: We're not in e-Kansas anymore: State courts reel from 'unauthorized incursion'
Date: 2023-10-16T17:32:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-10-16-were-not-in-e-kansas-anymore-state-courts-reel-from-unauthorized-incursion

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/16/kansas_courts_security_incident/){:target="_blank" rel="noopener"}

> Fax, post, and human messengers can still be used for filing vital evidence An unspecified security incident is forcing many state courts across Kansas to rely on paper filings, and it may have continue to do so for weeks, a state judge has warned.... [...]
