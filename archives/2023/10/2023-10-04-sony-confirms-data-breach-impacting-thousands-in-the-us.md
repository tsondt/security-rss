Title: Sony confirms data breach impacting thousands in the U.S.
Date: 2023-10-04T08:04:49-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-04-sony-confirms-data-breach-impacting-thousands-in-the-us

[Source](https://www.bleepingcomputer.com/news/security/sony-confirms-data-breach-impacting-thousands-in-the-us/){:target="_blank" rel="noopener"}

> Sony Interactive Entertainment (Sony) has notified current and former employees and their family members about a cybersecurity breach that exposed personal information. [...]
