Title: Microsoft extends Purview Audit log retention after July breach
Date: 2023-10-19T16:21:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-10-19-microsoft-extends-purview-audit-log-retention-after-july-breach

[Source](https://www.bleepingcomputer.com/news/security/microsoft-extends-purview-audit-log-retention-after-july-breach/){:target="_blank" rel="noopener"}

> Microsoft is extending Purview Audit log retention as promised after the Chinese Storm-0558 hacking group breached dozens of Exchange and Microsoft 365 corporate and government accounts in July. [...]
