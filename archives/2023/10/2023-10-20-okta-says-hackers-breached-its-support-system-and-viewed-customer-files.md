Title: Okta says hackers breached its support system and viewed customer files
Date: 2023-10-20T22:45:53+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;hacking;okta;security breaches
Slug: 2023-10-20-okta-says-hackers-breached-its-support-system-and-viewed-customer-files

[Source](https://arstechnica.com/?p=1977688){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) Identity and authentication management provider Okta said hackers managed to view private customer information after gaining access to credentials to its customer support management system. “The threat actor was able to view files uploaded by certain Okta customers as part of recent support cases,” Okta Chief Security Officer David Bradbury said Friday. He suggested those files comprised HTTP archive, or HAR, files, which company support personnel use to replicate customer browser activity during troubleshooting sessions. “HAR files can also contain sensitive data, including cookies and session tokens, that malicious actors can use to impersonate valid [...]
