Title: Microsoft Edge, Teams get fixes for zero-days in open-source libraries
Date: 2023-10-03T10:54:18-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-10-03-microsoft-edge-teams-get-fixes-for-zero-days-in-open-source-libraries

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-edge-teams-get-fixes-for-zero-days-in-open-source-libraries/){:target="_blank" rel="noopener"}

> Microsoft released emergency security updates for Edge, Teams, and Skype to patch two zero-day vulnerabilities in open-source libraries used by the three products. [...]
