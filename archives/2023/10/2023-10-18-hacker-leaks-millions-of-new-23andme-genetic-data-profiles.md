Title: Hacker leaks millions of new 23andMe genetic data profiles
Date: 2023-10-18T14:04:06-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-10-18-hacker-leaks-millions-of-new-23andme-genetic-data-profiles

[Source](https://www.bleepingcomputer.com/news/security/hacker-leaks-millions-of-new-23andme-genetic-data-profiles/){:target="_blank" rel="noopener"}

> A hacker has leaked an additional 4.1 million stolen 23andMe genetic data profiles for people in Great Britain and Germany on a hacking forum. [...]
