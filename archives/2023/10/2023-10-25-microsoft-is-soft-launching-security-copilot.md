Title: Microsoft is Soft-Launching Security Copilot
Date: 2023-10-25T11:07:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;computer security;incident response;LLM;Microsoft;security analysis
Slug: 2023-10-25-microsoft-is-soft-launching-security-copilot

[Source](https://www.schneier.com/blog/archives/2023/10/microsoft-is-soft-launching-security-copilot.html){:target="_blank" rel="noopener"}

> Microsoft has announced an early access program for its LLM-based security chatbot assistant: Security Copilot. I am curious whether this thing is actually useful. [...]
