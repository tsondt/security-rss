Title: Squid games: 35 security holes still unpatched in proxy after 2 years, now public
Date: 2023-10-13T00:21:34+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-10-13-squid-games-35-security-holes-still-unpatched-in-proxy-after-2-years-now-public

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/13/squid_proxy_bugs_remain_unfixed/){:target="_blank" rel="noopener"}

> We'd like to say don't panic... but maybe? 35 vulnerabilities in the Squid caching proxy remain unfixed more than two years after being found and disclosed to the open source project's maintainers, according to the person who reported them.... [...]
