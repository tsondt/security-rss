Title: Ex-Navy IT manager gets 5 years in slammer for 2018 database heist
Date: 2023-10-19T14:01:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-19-ex-navy-it-manager-gets-5-years-in-slammer-for-2018-database-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/19/navy_it_manager_prison/){:target="_blank" rel="noopener"}

> Seafaring cybercrim's wife faces similar sentence next month A former IT manager for the US Navy is facing a five-and-a-half year prison sentence for selling thousands of people's personal records on the dark web.... [...]
