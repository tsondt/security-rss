Title: Google Chrome's new "IP Protection" will hide users' IP addresses
Date: 2023-10-22T18:00:10-04:00
Author: Mayank Parmar
Category: BleepingComputer
Tags: Google;Security
Slug: 2023-10-22-google-chromes-new-ip-protection-will-hide-users-ip-addresses

[Source](https://www.bleepingcomputer.com/news/google/google-chromes-new-ip-protection-will-hide-users-ip-addresses/){:target="_blank" rel="noopener"}

> Google is getting ready to test a new "IP Protection" feature for the Chrome browser that enhances users' privacy by masking their IP addresses using proxy servers. [...]
