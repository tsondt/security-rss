Title: Use SAML with Amazon Cognito to support a multi-tenant application with a single user pool
Date: 2023-10-10T17:52:03+00:00
Author: Neela Kulkarni
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;ADFS federation;Amazon Cognito;Cognito attribute mapping;IdP attribute mapping;LDAP;RBAC;SAML attribute mapping;Security Blog;Serverless
Slug: 2023-10-10-use-saml-with-amazon-cognito-to-support-a-multi-tenant-application-with-a-single-user-pool

[Source](https://aws.amazon.com/blogs/security/use-saml-with-amazon-cognito-to-support-a-multi-tenant-application-with-a-single-user-pool/){:target="_blank" rel="noopener"}

> Amazon Cognito is a customer identity and access management solution that scales to millions of users. With Cognito, you have four ways to secure multi-tenant applications: user pools, application clients, groups, or custom attributes. In an earlier blog post titled Role-based access control using Amazon Cognito and an external identity provider, you learned how to [...]
