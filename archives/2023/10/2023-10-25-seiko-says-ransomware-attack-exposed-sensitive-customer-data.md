Title: Seiko says ransomware attack exposed sensitive customer data
Date: 2023-10-25T12:40:43-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-25-seiko-says-ransomware-attack-exposed-sensitive-customer-data

[Source](https://www.bleepingcomputer.com/news/security/seiko-says-ransomware-attack-exposed-sensitive-customer-data/){:target="_blank" rel="noopener"}

> Japanese watchmaker Seiko has confirmed it suffered a Black Cat ransomware attack earlier this year, warning that the incident has led to a data breach, exposing sensitive customer, partner, and personnel information. [...]
