Title: AWS Digital Sovereignty Pledge: Announcing a new, independent sovereign cloud in Europe
Date: 2023-10-25T05:00:25+00:00
Author: Matt Garman
Category: AWS Security
Tags: Announcements;Foundational (100);Security;Security, Identity, & Compliance;Thought Leadership;AWS Digital Sovereignty Pledge;Digital Sovereignty;Security Blog;Sovereign-by-design
Slug: 2023-10-25-aws-digital-sovereignty-pledge-announcing-a-new-independent-sovereign-cloud-in-europe

[Source](https://aws.amazon.com/blogs/security/aws-digital-sovereignty-pledge-announcing-a-new-independent-sovereign-cloud-in-europe/){:target="_blank" rel="noopener"}

> French | German | Italian | Spanish From day one, Amazon Web Services (AWS) has always believed it is essential that customers have control over their data, and choices for how they secure and manage that data in the cloud. Last year, we introduced the AWS Digital Sovereignty Pledge, our commitment to offering AWS customers [...]
