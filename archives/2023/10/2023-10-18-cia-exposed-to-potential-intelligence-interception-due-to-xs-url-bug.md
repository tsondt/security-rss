Title: CIA exposed to potential intelligence interception due to X's URL bug
Date: 2023-10-18T13:00:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-18-cia-exposed-to-potential-intelligence-interception-due-to-xs-url-bug

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/18/cia_x_url_bug/){:target="_blank" rel="noopener"}

> Musk's mega-app-in-waiting goes from chopping headlines to profile URLs An ethical hacker has exploited a bug in the way X truncates URLs to take over a CIA Telegram channel used to receive intelligence.... [...]
