Title: Political Disinformation and AI
Date: 2023-10-05T11:12:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;ChatGPT;disinformation;essays;propaganda;social media;voting
Slug: 2023-10-05-political-disinformation-and-ai

[Source](https://www.schneier.com/blog/archives/2023/10/political-disinformation-and-ai.html){:target="_blank" rel="noopener"}

> Elections around the world are facing an evolving threat from foreign actors, one that involves artificial intelligence. Countries trying to influence each other’s elections entered a new era in 2016, when the Russians launched a series of social media disinformation campaigns targeting the US presidential election. Over the next seven years, a number of countries—most prominently China and Iran—used social media to influence foreign elections, both in the US and elsewhere in the world. There’s no reason to expect 2023 and 2024 to be any different. But there is a new element: generative AI and large language models. These have [...]
