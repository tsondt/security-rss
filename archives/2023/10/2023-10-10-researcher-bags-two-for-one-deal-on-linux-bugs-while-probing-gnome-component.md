Title: Researcher bags two-for-one deal on Linux bugs while probing GNOME component
Date: 2023-10-10T16:01:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-10-researcher-bags-two-for-one-deal-on-linux-bugs-while-probing-gnome-component

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/10/linux_gnome_libcue_exploit/){:target="_blank" rel="noopener"}

> One-click exploit could potentially affect most major distros Researchers discovered a high-severity remote code execution (RCE) vulnerability in an inherent component of GNOME-based Linux distros, potentially impacting a huge number of users.... [...]
