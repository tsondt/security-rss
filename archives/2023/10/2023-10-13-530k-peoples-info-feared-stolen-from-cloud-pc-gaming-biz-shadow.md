Title: 530K people's info feared stolen from cloud PC gaming biz Shadow
Date: 2023-10-13T18:57:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-10-13-530k-peoples-info-feared-stolen-from-cloud-pc-gaming-biz-shadow

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/13/shadow_data_theft/){:target="_blank" rel="noopener"}

> Will players press start to continue with this outfit? Shadow, which hosts Windows PC gaming in the cloud among other services, has confirmed criminals stole a database containing customer data following a social-engineering attack against one of its employees.... [...]
