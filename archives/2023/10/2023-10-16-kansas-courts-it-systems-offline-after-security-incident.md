Title: Kansas courts IT systems offline after ‘security incident’
Date: 2023-10-16T16:08:41-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-16-kansas-courts-it-systems-offline-after-security-incident

[Source](https://www.bleepingcomputer.com/news/security/kansas-courts-it-systems-offline-after-security-incident/){:target="_blank" rel="noopener"}

> Information systems of state courts across Kansas are still offline after they've been disrupted in what the Kansas judicial branch described last Thursday as a "security incident." [...]
