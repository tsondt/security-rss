Title: Friday Squid Blogging: On Squid Intelligence
Date: 2023-10-13T21:09:31+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-10-13-friday-squid-blogging-on-squid-intelligence

[Source](https://www.schneier.com/blog/archives/2023/10/friday-squid-blogging-on-squid-intelligence.html){:target="_blank" rel="noopener"}

> Article about squid intelligence. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
