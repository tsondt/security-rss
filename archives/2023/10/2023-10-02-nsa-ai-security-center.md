Title: NSA AI Security Center
Date: 2023-10-02T16:40:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;cybersecurity;national security policy;NSA
Slug: 2023-10-02-nsa-ai-security-center

[Source](https://www.schneier.com/blog/archives/2023/10/nsa-ai-security-center.html){:target="_blank" rel="noopener"}

> The NSA is starting a new artificial intelligence security center: The AI security center’s establishment follows an NSA study that identified securing AI models from theft and sabotage as a major national security challenge, especially as generative AI technologies emerge with immense transformative potential for both good and evil. Nakasone said it would become “NSA’s focal point for leveraging foreign intelligence insights, contributing to the development of best practices guidelines, principles, evaluation, methodology and risk frameworks” for both AI security and the goal of promoting the secure development and adoption of AI within “our national security systems and our defense [...]
