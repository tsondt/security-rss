Title: New critical Citrix NetScaler flaw exposes 'sensitive' data
Date: 2023-10-10T11:53:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-10-new-critical-citrix-netscaler-flaw-exposes-sensitive-data

[Source](https://www.bleepingcomputer.com/news/security/new-critical-citrix-netscaler-flaw-exposes-sensitive-data/){:target="_blank" rel="noopener"}

> Citrix NetScaler ADC and NetScaler Gateway are impacted by a critical severity flaw that allows the disclosure of sensitive information from vulnerable appliances. [...]
