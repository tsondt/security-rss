Title: FTC warns of ‘staggering’ losses to social media scams since 2021
Date: 2023-10-06T13:07:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-06-ftc-warns-of-staggering-losses-to-social-media-scams-since-2021

[Source](https://www.bleepingcomputer.com/news/security/ftc-warns-of-staggering-losses-to-social-media-scams-since-2021/){:target="_blank" rel="noopener"}

> The Federal Trade Commission says Americans have lost at least $2.7 billion to social media scams since 2021, with the real number likely many times larger due to unreported incidents. [...]
