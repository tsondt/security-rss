Title: FBI warns of surge in 'phantom hacker' scams impacting elderly
Date: 2023-10-02T11:01:06-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-02-fbi-warns-of-surge-in-phantom-hacker-scams-impacting-elderly

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-surge-in-phantom-hacker-scams-impacting-elderly/){:target="_blank" rel="noopener"}

> The FBI issued a public service announcement warning of a significant increase in 'phantom hacker' scams targeting senior citizens across the United States. [...]
