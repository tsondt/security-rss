Title: October Cybersecurity Awareness Month to target internal security risks
Date: 2023-10-19T12:35:07+00:00
Author: Jack Kirkstall
Category: The Register
Tags: 
Slug: 2023-10-19-october-cybersecurity-awareness-month-to-target-internal-security-risks

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/19/october_cybersecurity_awareness_month_to/){:target="_blank" rel="noopener"}

> SANS offers cyber security pros a valuable toolkit of resources to mitigate the potentially serious cybersecurity risks faced by internal staff Sponsored Post Organisations that fail to adequately address the potential vulnerabilities that internal employees sometimes encounter when developing an IT security strategy are exposing themselves to potentially catastrophic dangers, infosec experts have warned.... [...]
