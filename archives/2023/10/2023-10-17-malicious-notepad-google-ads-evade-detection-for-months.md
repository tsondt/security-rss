Title: Malicious Notepad++ Google ads evade detection for months
Date: 2023-10-17T15:48:10-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2023-10-17-malicious-notepad-google-ads-evade-detection-for-months

[Source](https://www.bleepingcomputer.com/news/security/malicious-notepad-plus-plus-google-ads-evade-detection-for-months/){:target="_blank" rel="noopener"}

> A new Google Search malvertizing campaign targets users looking to download the popular Notepad++ text editor, employing advanced techniques to evade detection and analysis. [...]
