Title: Atlassian warns of critical Confluence flaw leading to data loss
Date: 2023-10-31T14:04:46-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-31-atlassian-warns-of-critical-confluence-flaw-leading-to-data-loss

[Source](https://www.bleepingcomputer.com/news/security/atlassian-warns-of-critical-confluence-flaw-leading-to-data-loss/){:target="_blank" rel="noopener"}

> Australian software company Atlassian warned admins to immediately patch Internet-exposed Confluence instances against a critical security flaw that could lead to data loss following successful exploitation. [...]
