Title: Mask and redact sensitive data published to Amazon SNS using managed and custom data identifiers
Date: 2023-10-25T19:01:23+00:00
Author: Otavio Ferreira
Category: AWS Security
Tags: Amazon Simple Notification Service (SNS);Best Practices;Intermediate (200);Security, Identity, & Compliance;Amazon SNS;Compliance;Data protection;messaging;Security;Security Blog;Serverless
Slug: 2023-10-25-mask-and-redact-sensitive-data-published-to-amazon-sns-using-managed-and-custom-data-identifiers

[Source](https://aws.amazon.com/blogs/security/mask-and-redact-sensitive-data-published-to-amazon-sns-using-managed-and-custom-data-identifiers/){:target="_blank" rel="noopener"}

> Today, we’re announcing a new capability for Amazon Simple Notification Service (Amazon SNS) message data protection. In this post, we show you how you can use this new capability to create custom data identifiers to detect and protect domain-specific sensitive data, such as your company’s employee IDs. Previously, you could only use managed data identifiers [...]
