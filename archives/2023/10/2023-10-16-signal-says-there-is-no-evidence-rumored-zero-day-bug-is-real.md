Title: Signal says there is no evidence rumored zero-day bug is real
Date: 2023-10-16T02:04:15-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Technology
Slug: 2023-10-16-signal-says-there-is-no-evidence-rumored-zero-day-bug-is-real

[Source](https://www.bleepingcomputer.com/news/security/signal-says-there-is-no-evidence-rumored-zero-day-bug-is-real/){:target="_blank" rel="noopener"}

> Signal messenger has investigated rumors spreading online over the weekend of a zero-day security vulnerability related to the 'Generate Link Previews' feature, stating that there is no evidence this vulnerability is real. [...]
