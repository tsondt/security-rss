Title: CISA shares vulnerabilities, misconfigs used by ransomware gangs
Date: 2023-10-13T10:55:14-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-13-cisa-shares-vulnerabilities-misconfigs-used-by-ransomware-gangs

[Source](https://www.bleepingcomputer.com/news/security/cisa-shares-vulnerabilities-misconfigs-used-by-ransomware-gangs/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity and Infrastructure Security Agency (CISA) has unveiled additional details regarding misconfigurations and security vulnerabilities exploited by ransomware gangs, aiming to help critical infrastructure organizations thwart their attacks. [...]
