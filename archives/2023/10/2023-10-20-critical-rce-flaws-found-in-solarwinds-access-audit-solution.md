Title: Critical RCE flaws found in SolarWinds access audit solution
Date: 2023-10-20T10:59:25-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-20-critical-rce-flaws-found-in-solarwinds-access-audit-solution

[Source](https://www.bleepingcomputer.com/news/security/critical-rce-flaws-found-in-solarwinds-access-audit-solution/){:target="_blank" rel="noopener"}

> Security researchers found three critical remote code execution vulnerabilities in the SolarWinds Access Rights Manager (ARM) product that remote attackers could use to run code with SYSTEM privileges. [...]
