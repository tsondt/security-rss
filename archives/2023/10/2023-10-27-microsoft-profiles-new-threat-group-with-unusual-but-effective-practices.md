Title: Microsoft profiles new threat group with unusual but effective practices
Date: 2023-10-27T23:20:36+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;hacking;microsoft;octo tempest;ransomware
Slug: 2023-10-27-microsoft-profiles-new-threat-group-with-unusual-but-effective-practices

[Source](https://arstechnica.com/?p=1979474){:target="_blank" rel="noopener"}

> Enlarge / This is not what a hacker looks like. Except on hacker cosplay night. (credit: Getty Images | Bill Hinton ) Microsoft has been tracking a threat group that stands out for its ability to cash in from data theft hacks that use broad social engineering attacks, painstaking research, and occasional physical threats. Unlike many ransomware attack groups, Octo Tempest, as Microsoft has named the group, doesn’t encrypt data after gaining illegal access to it. Instead, the threat actor threatens to share the data publicly unless the victim pays a hefty ransom. To defeat targets’ defenses, the group resorts [...]
