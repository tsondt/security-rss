Title: Make-me-root 'Looney Tunables' security hole on Linux needs your attention
Date: 2023-10-04T21:27:27+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-10-04-make-me-root-looney-tunables-security-hole-on-linux-needs-your-attention

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/04/linux_looney_tunables_bug/){:target="_blank" rel="noopener"}

> What's up, Doc? Try elevated permissions Grab security updates for your Linux distributions: there's a security hole that can be fairly easily exploited by rogue users, intruders, and malicious software to gain root access and take over the box.... [...]
