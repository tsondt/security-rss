Title: Friday Squid Blogging: On the Ugliness of Squid Fishing
Date: 2023-10-27T21:13:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-10-27-friday-squid-blogging-on-the-ugliness-of-squid-fishing

[Source](https://www.schneier.com/blog/archives/2023/10/friday-squid-blogging-on-the-ugliness-of-squid-fishing.html){:target="_blank" rel="noopener"}

> And seafood in general : A squid ship is a bustling, bright, messy place. The scene on deck looks like a mechanic’s garage where an oil change has gone terribly wrong. Scores of fishing lines extend into the water, each bearing specialized hooks operated by automated reels. When they pull a squid on board, it squirts warm, viscous ink, which coats the walls and floors. Deep-sea squid have high levels of ammonia, which they use for buoyancy, and a smell hangs in the air. The hardest labor generally happens at night, from 5 P.M. until 7 A.M. Hundreds of bowling-ball-size [...]
