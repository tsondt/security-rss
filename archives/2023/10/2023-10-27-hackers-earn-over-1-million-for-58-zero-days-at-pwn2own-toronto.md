Title: Hackers earn over $1 million for 58 zero-days at Pwn2Own Toronto
Date: 2023-10-27T15:00:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-27-hackers-earn-over-1-million-for-58-zero-days-at-pwn2own-toronto

[Source](https://www.bleepingcomputer.com/news/security/hackers-earn-over-1-million-for-58-zero-days-at-pwn2own-toronto/){:target="_blank" rel="noopener"}

> The Pwn2Own Toronto 2023 hacking competition has ended with security researchers earning $1,038,500 for 58 zero-day exploits (and multiple bug collisions) targeting consumer products between October 24 and October 27. [...]
