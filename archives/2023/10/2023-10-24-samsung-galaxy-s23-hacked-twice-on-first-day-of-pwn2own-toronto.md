Title: Samsung Galaxy S23 hacked twice on first day of Pwn2Own Toronto
Date: 2023-10-24T19:48:31-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-24-samsung-galaxy-s23-hacked-twice-on-first-day-of-pwn2own-toronto

[Source](https://www.bleepingcomputer.com/news/security/samsung-galaxy-s23-hacked-twice-on-first-day-of-pwn2own-toronto/){:target="_blank" rel="noopener"}

> Security researchers hacked the Samsung Galaxy S23 twice during the first day of the consumer-focused Pwn2Own 2023 hacking contest in Toronto, Canada. [...]
