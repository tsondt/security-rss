Title: Malicious Ads in Bing Chat
Date: 2023-10-04T11:08:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;malware;marketing
Slug: 2023-10-04-malicious-ads-in-bing-chat

[Source](https://www.schneier.com/blog/archives/2023/10/malicious-ads-in-bing-chat.html){:target="_blank" rel="noopener"}

> Malicious ads are creeping into chatbots. [...]
