Title: Florida man jailed after draining $1M from victims in crypto SIM swap attacks
Date: 2023-10-30T22:53:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-10-30-florida-man-jailed-after-draining-1m-from-victims-in-crypto-sim-swap-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/30/sim_swapping_crypto_crook_jailed/){:target="_blank" rel="noopener"}

> Not old enough to legally rent a car, old enough for a 30-month term A 20-year-old Florida man has been sentenced to 30 months behind bars for his role in a SIM-swapping ring that stole nearly $1 million in cryptocurrency from dozens of victims.... [...]
