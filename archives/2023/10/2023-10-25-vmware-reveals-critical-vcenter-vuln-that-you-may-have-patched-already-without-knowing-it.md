Title: VMware reveals critical vCenter vuln that you may have patched already without knowing it
Date: 2023-10-25T04:30:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-10-25-vmware-reveals-critical-vcenter-vuln-that-you-may-have-patched-already-without-knowing-it

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/25/vmware_vcenter_critical_flaw/){:target="_blank" rel="noopener"}

> Takes rare step of issuing patches for end-of-life versions, as some staff report end-of-career letters VMware has disclosed a critical vulnerability in its vCenter Server – and that it issued an update to fix it weeks ago, along with patches for unsupported versions of the software.... [...]
