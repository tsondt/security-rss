Title: Hot fuzz: Cascade finds dozens of RISC-V chip bugs using random data storm
Date: 2023-10-24T21:41:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-10-24-hot-fuzz-cascade-finds-dozens-of-risc-v-chip-bugs-using-random-data-storm

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/24/cascade_fuzzer_zurich/){:target="_blank" rel="noopener"}

> ETH Zurich boffins say they've devised a better CPU fuzzer to find flaws Video Boffins from ETH Zurich have devised a novel fuzzer for finding bugs in RISC-V chips and have used it to find more than three dozen.... [...]
