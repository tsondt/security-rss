Title: AWS-LC is now FIPS 140-3 certified
Date: 2023-10-06T17:55:44+00:00
Author: Nevine Ebeid
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Compliance;cryptography;Federal Information Processing Standard;FIPS;FIPS 140;FIPS 140-2;NIST;Open source;Security Blog
Slug: 2023-10-06-aws-lc-is-now-fips-140-3-certified

[Source](https://aws.amazon.com/blogs/security/aws-lc-is-now-fips-140-3-certified/){:target="_blank" rel="noopener"}

> AWS Cryptography is pleased to announce that today, the National Institute for Standards and Technology (NIST) awarded AWS-LC its validation certificate as a Federal Information Processing Standards (FIPS) 140-3, level 1, cryptographic module. This important milestone enables AWS customers that require FIPS-validated cryptography to leverage AWS-LC as a fully owned AWS implementation. AWS-LC is an [...]
