Title: American Family Insurance confirms cyberattack is behind IT outages
Date: 2023-10-21T16:10:14-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-10-21-american-family-insurance-confirms-cyberattack-is-behind-it-outages

[Source](https://www.bleepingcomputer.com/news/security/american-family-insurance-confirms-cyberattack-is-behind-it-outages/){:target="_blank" rel="noopener"}

> Insurance giant American Family Insurance has confirmed it suffered a cyberattack and shut down portions of its IT systems after customers reported website outages all week. [...]
