Title: China-linked cyberspies backdoor semiconductor firms with Cobalt Strike
Date: 2023-10-05T14:57:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-05-china-linked-cyberspies-backdoor-semiconductor-firms-with-cobalt-strike

[Source](https://www.bleepingcomputer.com/news/security/china-linked-cyberspies-backdoor-semiconductor-firms-with-cobalt-strike/){:target="_blank" rel="noopener"}

> Hackers engaging in cyber espionage have targeted Chinese-speaking semiconductor companies with TSMC-themed lures that infect them with Cobalt Strike beacons. [...]
