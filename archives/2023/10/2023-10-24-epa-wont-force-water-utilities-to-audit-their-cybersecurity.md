Title: EPA Won’t Force Water Utilities to Audit Their Cybersecurity
Date: 2023-10-24T11:02:03+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;infrastructure;national security policy;utilities
Slug: 2023-10-24-epa-wont-force-water-utilities-to-audit-their-cybersecurity

[Source](https://www.schneier.com/blog/archives/2023/10/epa-wont-force-water-utilities-to-audit-their-cybersecurity.html){:target="_blank" rel="noopener"}

> The industry pushed back : Despite the EPA’s willingness to provide training and technical support to help states and public water system organizations implement cybersecurity surveys, the move garnered opposition from both GOP state attorneys and trade groups. Republican state attorneys that were against the new proposed policies said that the call for new inspections could overwhelm state regulators. The attorney generals of Arkansas, Iowa and Missouri all sued the EPA—claiming the agency had no authority to set these requirements. This led to the EPA’s proposal being temporarily blocked back in June. So now we have a piece of our [...]
