Title: Enable Security Hub partner integrations across your organization
Date: 2023-10-04T20:57:07+00:00
Author: Joaquin Manuel Rinaudo
Category: AWS Security
Tags: AWS Security Hub;Best Practices;Intermediate (200);Security, Identity, & Compliance;Compliance;Identity;Security;Security Blog
Slug: 2023-10-04-enable-security-hub-partner-integrations-across-your-organization

[Source](https://aws.amazon.com/blogs/security/enable-security-hub-partner-integrations-across-your-organization/){:target="_blank" rel="noopener"}

> AWS Security Hub offers over 75 third-party partner product integrations, such as Palo Alto Networks Prisma, Prowler, Qualys, Wiz, and more, that you can use to send, receive, or update findings in Security Hub. We recommend that you enable your corresponding Security Hub third-party partner product integrations when you use these partner solutions. By centralizing [...]
