Title: New WordPress backdoor creates rogue admin to hijack websites
Date: 2023-10-11T17:23:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-11-new-wordpress-backdoor-creates-rogue-admin-to-hijack-websites

[Source](https://www.bleepingcomputer.com/news/security/new-wordpress-backdoor-creates-rogue-admin-to-hijack-websites/){:target="_blank" rel="noopener"}

> A new malware has been posing as a legitimate caching plugin to target WordPress sites, allowing threat actors to create an administrator account and control the site's activity. [...]
