Title: IT networks under attack via critical Confluence zero-day. Patch now
Date: 2023-10-04T22:19:46+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-10-04-it-networks-under-attack-via-critical-confluence-zero-day-patch-now

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/04/critical_confluence_privilege_escalation_bug/){:target="_blank" rel="noopener"}

> 'Handful' of customers hit so far, public-facing instances at risk Atlassian today said miscreants have exploited a critical bug in on-premises instances of Confluence Server and Confluence Data Center to create and abuse admin accounts within the enterprise colab software.... [...]
