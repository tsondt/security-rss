Title: Amazon adds passkey support as new passwordless login option
Date: 2023-10-17T15:09:01-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Business;Technology
Slug: 2023-10-17-amazon-adds-passkey-support-as-new-passwordless-login-option

[Source](https://www.bleepingcomputer.com/news/security/amazon-adds-passkey-support-as-new-passwordless-login-option/){:target="_blank" rel="noopener"}

> Amazon has quietly added passkey support as a new passwordless login option for customers, offering better protection from information-stealing malware and phishing attacks. [...]
