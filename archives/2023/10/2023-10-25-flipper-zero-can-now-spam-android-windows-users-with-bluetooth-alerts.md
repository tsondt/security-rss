Title: Flipper Zero can now spam Android, Windows users with Bluetooth alerts
Date: 2023-10-25T14:54:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-25-flipper-zero-can-now-spam-android-windows-users-with-bluetooth-alerts

[Source](https://www.bleepingcomputer.com/news/security/flipper-zero-can-now-spam-android-windows-users-with-bluetooth-alerts/){:target="_blank" rel="noopener"}

> A custom Flipper Zero firmware called 'Xtreme' has added a new feature to perform Bluetooth spam attacks on Android and Windows devices. [...]
