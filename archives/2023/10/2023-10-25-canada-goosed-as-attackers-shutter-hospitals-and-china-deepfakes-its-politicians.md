Title: Canada goosed as attackers shutter hospitals and China deepfakes its politicians
Date: 2023-10-25T19:45:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-10-25-canada-goosed-as-attackers-shutter-hospitals-and-china-deepfakes-its-politicians

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/25/canadian_hospitals_spamoflague/){:target="_blank" rel="noopener"}

> Eh? Canucks cracked by cyber crims Cybercriminals have Canada in the crosshairs, with five Ontario hospitals and a fresh Spamoflague disinformation campaign targeting "dozens" of Canadian government officials, including the PM.... [...]
