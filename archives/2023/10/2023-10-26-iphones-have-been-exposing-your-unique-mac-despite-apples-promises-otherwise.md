Title: iPhones have been exposing your unique MAC despite Apple’s promises otherwise
Date: 2023-10-26T21:48:04+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;iphone;MAC address;privacy;wi-fi
Slug: 2023-10-26-iphones-have-been-exposing-your-unique-mac-despite-apples-promises-otherwise

[Source](https://arstechnica.com/?p=1979099){:target="_blank" rel="noopener"}

> Enlarge / Private Wi-Fi address setting on an iPhone. (credit: Apple) Three years ago, Apple introduced a privacy-enhancing feature that hid the Wi-Fi address of iPhones and iPads when they joined a network. On Wednesday, the world learned that the feature has never worked as advertised. Despite promises that this never-changing address would be hidden and replaced with a private one that was unique to each SSID, Apple devices have continued to display the real one, which in turn got broadcast to every other connected device on the network. The problem is that a Wi-Fi media access control address—typically called [...]
