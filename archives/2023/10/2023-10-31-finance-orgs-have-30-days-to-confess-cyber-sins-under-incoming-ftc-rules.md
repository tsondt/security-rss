Title: Finance orgs have 30 days to confess cyber sins under incoming FTC rules
Date: 2023-10-31T16:13:05+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-31-finance-orgs-have-30-days-to-confess-cyber-sins-under-incoming-ftc-rules

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/31/ftc_30_day_breach_disclosure/){:target="_blank" rel="noopener"}

> Follows similar efforts from the SEC and DHS in recent months The US has approved mandatory data breach reporting requirements that impose a 30-day deadline for non-banking financial organizations to report incidents.... [...]
