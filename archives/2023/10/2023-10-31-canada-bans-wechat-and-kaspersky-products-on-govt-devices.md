Title: Canada bans WeChat and Kaspersky products on govt devices
Date: 2023-10-31T11:06:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government;Software
Slug: 2023-10-31-canada-bans-wechat-and-kaspersky-products-on-govt-devices

[Source](https://www.bleepingcomputer.com/news/security/canada-bans-wechat-and-kaspersky-products-on-govt-devices/){:target="_blank" rel="noopener"}

> Canada has banned the use of Kaspersky security products and Tencent's WeChat app on mobile devices used by government employees, citing network and national security concerns. [...]
