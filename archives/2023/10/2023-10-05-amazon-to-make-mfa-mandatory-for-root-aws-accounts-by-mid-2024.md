Title: Amazon to make MFA mandatory for 'root' AWS accounts by mid-2024
Date: 2023-10-05T13:06:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud;Technology
Slug: 2023-10-05-amazon-to-make-mfa-mandatory-for-root-aws-accounts-by-mid-2024

[Source](https://www.bleepingcomputer.com/news/security/amazon-to-make-mfa-mandatory-for-root-aws-accounts-by-mid-2024/){:target="_blank" rel="noopener"}

> Amazon will require all privileged AWS (Amazon Web Services) accounts to use multi-factor authentication (MFA) for stronger protection against account hijacks leading to data breaches, starting in mid-2024. [...]
