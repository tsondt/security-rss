Title: Hacking the High School Grading System
Date: 2023-10-13T11:12:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;A Hacker's Mind;hacking;schools
Slug: 2023-10-13-hacking-the-high-school-grading-system

[Source](https://www.schneier.com/blog/archives/2023/10/hacking-the-high-school-grading-system.html){:target="_blank" rel="noopener"}

> Interesting New York Times article about high-school students hacking the grading system. What’s not helping? The policies many school districts are adopting that make it nearly impossible for low-performing students to fail—they have a grading floor under them, they know it, and that allows them to game the system. Several teachers whom I spoke with or who responded to my questionnaire mentioned policies stating that students cannot get lower than a 50 percent on any assignment, even if the work was never done, in some cases. A teacher from Chapel Hill, N.C., who filled in the questionnaire’s “name” field with [...]
