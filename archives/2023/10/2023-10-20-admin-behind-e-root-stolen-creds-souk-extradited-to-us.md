Title: Admin behind E-Root stolen creds souk extradited to US
Date: 2023-10-20T19:45:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-10-20-admin-behind-e-root-stolen-creds-souk-extradited-to-us

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/20/eroot_admin_extradited/){:target="_blank" rel="noopener"}

> There was a young man from Moldova, who the Feds just want to roll over, but with 20 inside, and nowhere to hide, he just wants it all to be over A Moldovan who allegedly ran the compromised-credential marketplace E-Root has been extradited from the UK to America to stand trial.... [...]
