Title: Decentralized Matrix messaging network says it now has 115M users
Date: 2023-10-24T12:44:07-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2023-10-24-decentralized-matrix-messaging-network-says-it-now-has-115m-users

[Source](https://www.bleepingcomputer.com/news/security/decentralized-matrix-messaging-network-says-it-now-has-115m-users/){:target="_blank" rel="noopener"}

> The team behind the Matrix open standard and real-time communication protocol has announced the release of its second major version, bringing end-to-end encryption to group VoIP, faster loading times, and more. [...]
