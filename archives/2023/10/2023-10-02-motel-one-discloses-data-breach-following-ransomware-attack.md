Title: Motel One discloses data breach following ransomware attack
Date: 2023-10-02T11:10:35-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-02-motel-one-discloses-data-breach-following-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/motel-one-discloses-data-breach-following-ransomware-attack/){:target="_blank" rel="noopener"}

> The Motel One Group has announced that it has been targeted by ransomware actors who managed to steal some customer data, including the details of 150 credit cards. [...]
