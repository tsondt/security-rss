Title: Hunters International leaks pre-op plastic surgery pics in negotiation no-no
Date: 2023-10-25T08:30:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-25-hunters-international-leaks-pre-op-plastic-surgery-pics-in-negotiation-no-no

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/25/rebuilt_hive_ransomware_gang_stings/){:target="_blank" rel="noopener"}

> No honor among thieves as group denies Hive ransomware links A newly emerged ransomware gang claims to have successfully gained access to the systems of a US plastic surgeon's clinic, leaking patients' pre-operation pictures in an attempt to hurry a ransom payment.... [...]
