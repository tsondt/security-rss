Title: New NSA Information from (and About) Snowden
Date: 2023-10-26T11:00:59+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Edward Snowden;Guardian;New York Times;NSA;privacy;surveillance
Slug: 2023-10-26-new-nsa-information-from-and-about-snowden

[Source](https://www.schneier.com/blog/archives/2023/10/new-nsa-information-from-and-about-snowden.html){:target="_blank" rel="noopener"}

> Interesting article about the Snowden documents, including comments from former Guardian editor Ewen MacAskill MacAskill, who shared the Pulitzer Prize for Public Service with Glenn Greenwald and Laura Poitras for their journalistic work on the Snowden files, retired from The Guardian in 2018. He told Computer Weekly that: As far as he knows, a copy of the documents is still locked in the New York Times office. Although the files are in the New York Times office, The Guardian retains responsibility for them. As to why the New York Times has not published them in a decade, MacAskill maintains “this [...]
