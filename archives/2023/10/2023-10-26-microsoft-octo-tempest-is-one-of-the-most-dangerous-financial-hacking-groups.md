Title: Microsoft: Octo Tempest is one of the most dangerous financial hacking groups
Date: 2023-10-26T18:55:18-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-10-26-microsoft-octo-tempest-is-one-of-the-most-dangerous-financial-hacking-groups

[Source](https://www.bleepingcomputer.com/news/security/microsoft-octo-tempest-is-one-of-the-most-dangerous-financial-hacking-groups/){:target="_blank" rel="noopener"}

> Microsoft has published a detailed profile of a native English-speaking threat actor with advanced social engineering capabilities it tracks as Octo Tempest, that targets companies in data extortion and ransomware attacks. [...]
