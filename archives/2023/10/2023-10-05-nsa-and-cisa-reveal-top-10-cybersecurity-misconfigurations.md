Title: NSA and CISA reveal top 10 cybersecurity misconfigurations
Date: 2023-10-05T14:08:02-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-10-05-nsa-and-cisa-reveal-top-10-cybersecurity-misconfigurations

[Source](https://www.bleepingcomputer.com/news/security/nsa-and-cisa-reveal-top-10-cybersecurity-misconfigurations/){:target="_blank" rel="noopener"}

> The National Security Agency (NSA) and the Cybersecurity and Infrastructure Security Agency (CISA) revealed today the top ten most common cybersecurity misconfigurations discovered by their red and blue teams in the networks of large organizations. [...]
