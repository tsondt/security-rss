Title: Stop what you’re doing and patch this critical Confluence flaw, warns Atlassian
Date: 2023-10-31T05:05:59+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-10-31-stop-what-youre-doing-and-patch-this-critical-confluence-flaw-warns-atlassian

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/31/critical_atlassian_confluence_flaw/){:target="_blank" rel="noopener"}

> Risk of ‘significant data loss’ for on-prem customers Atlassian has told customers they “must take immediate action” to address a newly discovered flaw in its Confluence collaboration tool.... [...]
