Title: HackerOne paid ethical hackers over $300 million in bug bounties
Date: 2023-10-28T11:17:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-28-hackerone-paid-ethical-hackers-over-300-million-in-bug-bounties

[Source](https://www.bleepingcomputer.com/news/security/hackerone-paid-ethical-hackers-over-300-million-in-bug-bounties/){:target="_blank" rel="noopener"}

> HackerOne has announced that its bug bounty programs have awarded over $300 million in rewards to ethical hackers and vulnerability researchers since the platform's inception. [...]
