Title: US cybercops urge admins to patch amid ongoing Confluence chaos
Date: 2023-10-17T13:02:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-10-17-us-cybercops-urge-admins-to-patch-amid-ongoing-confluence-chaos

[Source](https://go.theregister.com/feed/www.theregister.com/2023/10/17/confluence_zero_day_advisory/){:target="_blank" rel="noopener"}

> Do it now, no ifs or buts, says advisory US authorities have issued an urgent plea to network admins to patch the critical vulnerability in Atlassian Confluence Data Center and Server amid ongoing nation-state exploitation.... [...]
