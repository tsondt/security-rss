Title: Spain arrests 34 cybercriminals who stole data of 4 million people
Date: 2023-10-23T11:01:07-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-10-23-spain-arrests-34-cybercriminals-who-stole-data-of-4-million-people

[Source](https://www.bleepingcomputer.com/news/security/spain-arrests-34-cybercriminals-who-stole-data-of-4-million-people/){:target="_blank" rel="noopener"}

> The Spanish National Police have dismantled a cybercriminal organization that carried out a variety of computer scams to steal and monetize the data of over four million people. [...]
