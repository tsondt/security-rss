Title: Former Ubiquiti dev pleads guilty in data theft and extortion case
Date: 2023-02-03T01:30:11+00:00
Author: Tobias Mann
Category: The Register
Tags: 
Slug: 2023-02-03-former-ubiquiti-dev-pleads-guilty-in-data-theft-and-extortion-case

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/03/ubiquiti_dev_guilty/){:target="_blank" rel="noopener"}

> Nickolas Sharp now faces up to 35 years in prison A former Ubiquiti Networks employee accused of hatching an elaborate plot to first steal nearly $2 million from his employer, extort more, then later orchestrating a smear campaign against the company pleaded guilty to multiple felony charges Thursday.... [...]
