Title: Drug distributor AmerisourceBergen confirms security breach
Date: 2023-02-08T09:59:22-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-08-drug-distributor-amerisourcebergen-confirms-security-breach

[Source](https://www.bleepingcomputer.com/news/security/drug-distributor-amerisourcebergen-confirms-security-breach/){:target="_blank" rel="noopener"}

> Pharmaceutical distributor AmerisourceBergen confirmed that hackers compromised the IT system of one of its subsidiaries after threat actors began leaking allegedly stolen data. [...]
