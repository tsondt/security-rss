Title: CISA releases recovery script for ESXiArgs ransomware victims
Date: 2023-02-07T20:55:12-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-02-07-cisa-releases-recovery-script-for-esxiargs-ransomware-victims

[Source](https://www.bleepingcomputer.com/news/security/cisa-releases-recovery-script-for-esxiargs-ransomware-victims/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity and Infrastructure Security Agency (CISA) has released a script to recover VMware ESXi servers encrypted by the recent widespread ESXiArgs ransomware attacks. [...]
