Title: Side-Channel Attack against CRYSTALS-Kyber
Date: 2023-02-28T12:19:15+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;cryptography;encryption;machine learning;quantum computing;quantum cryptography;side-channel attacks
Slug: 2023-02-28-side-channel-attack-against-crystals-kyber

[Source](https://www.schneier.com/blog/archives/2023/02/side-channel-attack-against-crystals-kyber.html){:target="_blank" rel="noopener"}

> CRYSTALS-Kyber is one of the public-key algorithms currently recommended by NIST as part of its post-quantum cryptography standardization process. Researchers have just published a side-channel attack—using power consumption—against an implementation of the algorithm that was supposed to be resistant against that sort of attack. The algorithm is not “broken” or “cracked”—despite headlines to the contrary—this is just a side-channel attack. What makes this work really interesting is that the researchers used a machine-learning model to train the system to exploit the side channel. [...]
