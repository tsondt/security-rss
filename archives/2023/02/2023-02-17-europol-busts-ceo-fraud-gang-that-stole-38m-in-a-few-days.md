Title: Europol busts ‘CEO fraud’ gang that stole €38M in a few days
Date: 2023-02-17T13:18:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-17-europol-busts-ceo-fraud-gang-that-stole-38m-in-a-few-days

[Source](https://www.bleepingcomputer.com/news/security/europol-busts-ceo-fraud-gang-that-stole-38m-in-a-few-days/){:target="_blank" rel="noopener"}

> Europol has dismantled a Franco-Israeli 'CEO fraud' group that employed business email compromise (BEC) attacks to divert payments from organizations to bank accounts under the threat actor's control. [...]
