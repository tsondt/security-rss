Title: Microsoft delivers 75-count box of patches for Valentine's Day
Date: 2023-02-14T22:25:14+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-02-14-microsoft-delivers-75-count-box-of-patches-for-valentines-day

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/14/microsoft_adobe_patch_tuesday/){:target="_blank" rel="noopener"}

> Adobe, SAP, Intel, AMD, Android also show up with bouquet of fixes Patch Tuesday Happy Patch Tuesday for February, 2023, which falls on Valentine's Day.... [...]
