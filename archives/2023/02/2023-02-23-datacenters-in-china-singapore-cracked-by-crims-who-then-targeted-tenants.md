Title: Datacenters in China, Singapore cracked by crims who then targeted tenants
Date: 2023-02-23T05:45:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-02-23-datacenters-in-china-singapore-cracked-by-crims-who-then-targeted-tenants

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/23/datacenter_operators_in_china_singapore/){:target="_blank" rel="noopener"}

> Infiltrators tried to create fake remote hands tasks, alter visitor lists Criminals have targeted datacenter operators in Singapore and China, tapping into their CCTV cameras, accessing their tenant lists and then attacking those customers.... [...]
