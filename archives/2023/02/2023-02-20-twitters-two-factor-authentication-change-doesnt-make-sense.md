Title: Twitter’s two-factor authentication change “doesn’t make sense”
Date: 2023-02-20T14:55:18+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;2fa;Elon Musk;Identity theft;Twitter;two-factor authentication
Slug: 2023-02-20-twitters-two-factor-authentication-change-doesnt-make-sense

[Source](https://arstechnica.com/?p=1918817){:target="_blank" rel="noopener"}

> Enlarge (credit: Bloomberg via Getty Images ) Twitter announced Friday that as of March 20, it will only allow its users to secure their accounts with SMS-based two-factor authentication if they pay for a Twitter Blue subscription. Two-factor authentication, or 2FA, requires users to log in with a username and password and then an additional “factor” such as a numeric code. Security experts have long advised that people use a generator app to get these codes. But receiving them in SMS text messages is a popular alternative, so removing that option for unpaid users has left security experts scratching their [...]
