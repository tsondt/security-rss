Title: Sensitive DoD emails exposed by unsecured Azure server
Date: 2023-02-23T19:30:13+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-02-23-sensitive-dod-emails-exposed-by-unsecured-azure-server

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/23/azure_dod_emails_exposed/){:target="_blank" rel="noopener"}

> AWS, Google and Oracle may benefit as Microsoft blames the Pentagon and the Pentagon blames Microsoft A hole in a US military email server operated by Microsoft left more than a terabyte of sensitive data exposed to the internet less than a month after Office 365 was awarded a higher level of government security accreditation.... [...]
