Title: Linux version of Royal Ransomware targets VMware ESXi servers
Date: 2023-02-05T10:15:32-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-05-linux-version-of-royal-ransomware-targets-vmware-esxi-servers

[Source](https://www.bleepingcomputer.com/news/security/linux-version-of-royal-ransomware-targets-vmware-esxi-servers/){:target="_blank" rel="noopener"}

> Royal Ransomware is the latest ransomware operation to add support for encrypting Linux devices to its most recent malware variants, specifically targeting VMware ESXi virtual machines. [...]
