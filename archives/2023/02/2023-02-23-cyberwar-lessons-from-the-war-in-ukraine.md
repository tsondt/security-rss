Title: Cyberwar Lessons from the War in Ukraine
Date: 2023-02-23T12:27:20+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;cyberwar;defense;infrastructure;reports;Russia;Ukraine
Slug: 2023-02-23-cyberwar-lessons-from-the-war-in-ukraine

[Source](https://www.schneier.com/blog/archives/2023/02/cyberwar-lessons-from-the-war-in-ukraine.html){:target="_blank" rel="noopener"}

> The Aspen Institute has published a good analysis of the successes, failures, and absences of cyberattacks as part of the current war in Ukraine: “ The Cyber Defense Assistance Imperative ­ Lessons from Ukraine.” Its conclusion: Cyber defense assistance in Ukraine is working. The Ukrainian government and Ukrainian critical infrastructure organizations have better defended themselves and achieved higher levels of resiliency due to the efforts of CDAC and many others. But this is not the end of the road—the ability to provide cyber defense assistance will be important in the future. As a result, it is timely to assess how [...]
