Title: A Hacker’s Mind News
Date: 2023-02-03T20:03:10+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;A Hacker's Mind;books;interviews;Schneier news
Slug: 2023-02-03-a-hackers-mind-news

[Source](https://www.schneier.com/blog/archives/2023/02/a-hackers-mind-news.html){:target="_blank" rel="noopener"}

> A Hacker’s Mind will be published on Tuesday. I have done a written interview and a podcast interview about the book. It’s been chosen as a “ February 2023 Must-Read Book ” by the Next Big Idea Club. And an “Editor’s Pick”—whatever that means—on Amazon. There have been three reviews so far. I am hoping for more. And maybe even a published excerpt or two. Amazon and others will start shipping the book on Tuesday. If you ordered a signed copy from me, it is already in the mail. If you can leave a review somewhere, I would appreciate it. [...]
