Title: FBI is investigating a cybersecurity incident on its network
Date: 2023-02-17T09:23:28-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-17-fbi-is-investigating-a-cybersecurity-incident-on-its-network

[Source](https://www.bleepingcomputer.com/news/security/fbi-is-investigating-a-cybersecurity-incident-on-its-network/){:target="_blank" rel="noopener"}

> The U.S. Federal Bureau of Investigation (FBI) is reportedly investigating malicious cyber activity on the agency's network. [...]
