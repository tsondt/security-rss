Title: What Mary, Queen of Scots, can teach today’s cybersec royalty
Date: 2023-02-20T09:30:07+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2023-02-20-what-mary-queen-of-scots-can-teach-todays-cybersec-royalty

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/20/opinion_column_mary_queen_of_scots/){:target="_blank" rel="noopener"}

> Tech has changed in 400 years. The rules haven’t Opinion Mary, Queen of Scots, was a hapless CEO, even by the standards of 1600s Europe. Mother of the first Stuart King of England, James I (and VI of Scotland; let's not go into that), she was herself the first Stuart monarch to lose both throne and head. She wasn't the last. The family had issues.... [...]
