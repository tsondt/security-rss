Title: Atlassian data leak caused by stolen employee credentials
Date: 2023-02-16T12:41:16-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-02-16-atlassian-data-leak-caused-by-stolen-employee-credentials

[Source](https://www.bleepingcomputer.com/news/security/atlassian-data-leak-caused-by-stolen-employee-credentials/){:target="_blank" rel="noopener"}

> Atlassian has confirmed that a breach at a third-party vendor caused a recent leak of company data and that their network and customer information is secure. [...]
