Title: Healthcare giant CHS reports first data breach in GoAnywhere hacks
Date: 2023-02-14T11:26:54-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-14-healthcare-giant-chs-reports-first-data-breach-in-goanywhere-hacks

[Source](https://www.bleepingcomputer.com/news/security/healthcare-giant-chs-reports-first-data-breach-in-goanywhere-hacks/){:target="_blank" rel="noopener"}

> Community Health Systems (CHS) says it was impacted by a recent wave of attacks targeting a zero-day vulnerability in Fortra's GoAnywhere MFT secure file transfer platform. [...]
