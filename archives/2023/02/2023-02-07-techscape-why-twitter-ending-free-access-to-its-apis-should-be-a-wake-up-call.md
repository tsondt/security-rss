Title: TechScape: Why Twitter ending free access to its APIs should be a ‘wake-up call’
Date: 2023-02-07T11:45:45+00:00
Author: Chris Stokel-Walker
Category: The Guardian
Tags: Technology;Twitter;Elon Musk;Digital media;Blogging;Internet;Cybercrime;Data and computer security
Slug: 2023-02-07-techscape-why-twitter-ending-free-access-to-its-apis-should-be-a-wake-up-call

[Source](https://www.theguardian.com/technology/2023/feb/07/techscape-elon-musk-twitter-api){:target="_blank" rel="noopener"}

> In this week’s newsletter: The social media network is putting its APIs – the under-praised tool that keeps the internet as we know it going – behind a paywall. And the ramifications are huge Don’t get TechScape delivered to your inbox? Sign up here APIs may not seem like the sexiest thing to write about in a tech newsletter, but bear with me. Because APIs – or application programming interfaces – are important. They’re the synapses of our digital world: without them, our current ways of living wouldn’t work. For example, when you visit a website that requires you to [...]
