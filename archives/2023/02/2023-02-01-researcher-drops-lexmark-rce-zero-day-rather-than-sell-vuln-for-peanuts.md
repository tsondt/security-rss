Title: Researcher drops Lexmark RCE zero-day rather than sell vuln ‘for peanuts’
Date: 2023-02-01T12:18:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-02-01-researcher-drops-lexmark-rce-zero-day-rather-than-sell-vuln-for-peanuts

[Source](https://portswigger.net/daily-swig/researcher-drops-lexmark-rce-zero-day-rather-than-sell-vuln-for-peanuts){:target="_blank" rel="noopener"}

> Printer exploit chain could be weaponized to fully compromise more than 100 models [...]
