Title: Application security with Cloud SQL IAM database authentication
Date: 2023-02-07T17:00:00+00:00
Author: Davide Malagoli
Category: GCP Security
Tags: Application Modernization;Data Analytics;Identity & Security;Databases
Slug: 2023-02-07-application-security-with-cloud-sql-iam-database-authentication

[Source](https://cloud.google.com/blog/products/databases/application-security-with-cloud-sql-iam-database-authentication/){:target="_blank" rel="noopener"}

> Hardening a complex application is a challenge, more so for applications that include multiple layers with different authentication schemes. One common question is “how to integrate Cloud SQL for PostgreSQL or MySQL within your authentication flow?” Cloud SQL has always supported password-based authentication. There are, however, many questions that come with this approach: Where should you store the password? How do you manage different passwords for different environments? Who audits password complexity? Ideally, it would be preferable to not have to worry about passwords at all. Using username and password authentication also breaks the identity chain. Whoever knows the password [...]
