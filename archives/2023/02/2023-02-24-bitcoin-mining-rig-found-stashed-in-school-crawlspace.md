Title: Bitcoin mining rig found stashed in school crawlspace
Date: 2023-02-24T23:30:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-02-24-bitcoin-mining-rig-found-stashed-in-school-crawlspace

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/24/cryptocurrency_mining_school/){:target="_blank" rel="noopener"}

> Don't blame the kids! Ex-city employee charged with $17k power theft Pics A Massachusetts man accused of using his job as a city's assistant facilities director to hide a cryptocurrency mining operation in the crawlspace of a school has surrendered himself to authorities on Friday morning after skipping his Thursday arraignment.... [...]
