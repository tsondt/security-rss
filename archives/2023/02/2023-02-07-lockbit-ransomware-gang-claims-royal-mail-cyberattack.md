Title: LockBit ransomware gang claims Royal Mail cyberattack
Date: 2023-02-07T04:22:07-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-07-lockbit-ransomware-gang-claims-royal-mail-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-gang-claims-royal-mail-cyberattack/){:target="_blank" rel="noopener"}

> The LockBit ransomware operation has claimed the cyberattack on UK's leading mail delivery service Royal Mail that forced the company to halt its international shipping services due to "severe service disruption." [...]
