Title: Google paid $12 million in bug bounties to security researchers
Date: 2023-02-22T16:17:06-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-02-22-google-paid-12-million-in-bug-bounties-to-security-researchers

[Source](https://www.bleepingcomputer.com/news/security/google-paid-12-million-in-bug-bounties-to-security-researchers/){:target="_blank" rel="noopener"}

> Google last year paid its highest bug bounty ever through the Vulnerability Reward Program for a critical exploit chain report that the company valued at $605,000. [...]
