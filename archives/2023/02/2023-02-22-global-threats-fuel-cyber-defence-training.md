Title: Global threats fuel cyber defence training
Date: 2023-02-22T09:13:27+00:00
Author: Rob Jaques
Category: The Register
Tags: 
Slug: 2023-02-22-global-threats-fuel-cyber-defence-training

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/22/global_threats_fuel_cyber_defence/){:target="_blank" rel="noopener"}

> SANS Institute ramps up delivery of new security training courses to help keep info sec pros ahead of cyber criminals Sponsored Post The global impact of cyber threats on businesses, governments, organisations and individuals around the world is ramping up exponentially, with experts warning that danger is set to dramatically worsen in coming months and years.... [...]
