Title: Google's big security cert log overhaul broke Android apps. Now it's hit undo
Date: 2023-02-16T22:26:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-02-16-googles-big-security-cert-log-overhaul-broke-android-apps-now-its-hit-undo

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/16/google_delays_certificate_transparency_log/){:target="_blank" rel="noopener"}

> Devs missed warnings plus tons of code relies again on lone open source maintainer Google this week reversed an overhaul of one of its security-related file formats after the transition broke Android apps.... [...]
