Title: Researcher breaches Toyota supplier portal with info on 14,000 partners
Date: 2023-02-07T10:58:21-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-07-researcher-breaches-toyota-supplier-portal-with-info-on-14000-partners

[Source](https://www.bleepingcomputer.com/news/security/researcher-breaches-toyota-supplier-portal-with-info-on-14-000-partners/){:target="_blank" rel="noopener"}

> Toyota's Global Supplier Preparation Information Management System (GSPIMS) was breached by a security researcher who responsibly reported the issue to the company. [...]
