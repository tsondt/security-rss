Title: AIs as Computer Hackers
Date: 2023-02-02T11:59:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;China;DARPA;essays;hacking;robotics
Slug: 2023-02-02-ais-as-computer-hackers

[Source](https://www.schneier.com/blog/archives/2023/02/ais-as-computer-hackers.html){:target="_blank" rel="noopener"}

> Hacker “Capture the Flag” has been a mainstay at hacker gatherings since the mid-1990s. It’s like the outdoor game, but played on computer networks. Teams of hackers defend their own computers while attacking other teams’. It’s a controlled setting for what computer hackers do in real life: finding and fixing vulnerabilities in their own systems and exploiting them in others’. It’s the software vulnerability lifecycle. These days, dozens of teams from around the world compete in weekend-long marathon events held all over the world. People train for months. Winning is a big deal. If you’re into this sort of thing, [...]
