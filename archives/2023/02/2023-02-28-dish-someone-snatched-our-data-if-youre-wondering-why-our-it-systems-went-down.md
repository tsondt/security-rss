Title: Dish: Someone snatched our data, if you're wondering why our IT systems went down
Date: 2023-02-28T21:06:49+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-02-28-dish-someone-snatched-our-data-if-youre-wondering-why-our-it-systems-went-down

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/28/dish_outage_data_attack/){:target="_blank" rel="noopener"}

> Outage-hit telco still won't confirm ransomware infection, or if it's paying up Dish has confirmed what everyone was suspecting, given the ongoing downtime experienced by some of its systems, that the US telco was hit by criminal hackers.... [...]
