Title: Banning TikTok
Date: 2023-02-27T12:06:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;censorship;essays;national security policy;social media
Slug: 2023-02-27-banning-tiktok

[Source](https://www.schneier.com/blog/archives/2023/02/banning-tiktok.html){:target="_blank" rel="noopener"}

> Congress is currently debating bills that would ban TikTok in the United States. We are here as technologists to tell you that this is a terrible idea and the side effects would be intolerable. Details matter. There are several ways Congress might ban TikTok, each with different efficacies and side effects. In the end, all the effective ones would destroy the free Internet as we know it. There’s no doubt that TikTok and ByteDance, the company that owns it, are shady. They, like most large corporations in China, operate at the pleasure of the Chinese government. They collect extreme levels [...]
