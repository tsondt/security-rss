Title: Florida hospital takes IT systems offline after cyberattack
Date: 2023-02-03T12:37:16-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-03-florida-hospital-takes-it-systems-offline-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/florida-hospital-takes-it-systems-offline-after-cyberattack/){:target="_blank" rel="noopener"}

> Tallahassee Memorial HealthCare (TMH) has taken its IT systems offline and suspended non-emergency procedures following a late Thursday cyberattack. [...]
