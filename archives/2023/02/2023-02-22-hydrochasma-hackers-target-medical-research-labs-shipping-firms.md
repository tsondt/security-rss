Title: Hydrochasma hackers target medical research labs, shipping firms
Date: 2023-02-22T10:47:24-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-22-hydrochasma-hackers-target-medical-research-labs-shipping-firms

[Source](https://www.bleepingcomputer.com/news/security/hydrochasma-hackers-target-medical-research-labs-shipping-firms/){:target="_blank" rel="noopener"}

> A previously unknown threat actor named Hydrochasma has been targeting shipping and medical laboratories involved in COVID-19 vaccine development and treatments. [...]
