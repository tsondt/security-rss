Title: Crypto scam apps infiltrate Apple App Store and Google Play
Date: 2023-02-01T07:30:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-02-01-crypto-scam-apps-infiltrate-apple-app-store-and-google-play

[Source](https://www.bleepingcomputer.com/news/security/crypto-scam-apps-infiltrate-apple-app-store-and-google-play/){:target="_blank" rel="noopener"}

> Operators of high-yielding investment scams known as "pig butchering" have found a way to bypass the defenses in Google Play and Apple's App Store, the official repositories for Android and iOS apps. [...]
