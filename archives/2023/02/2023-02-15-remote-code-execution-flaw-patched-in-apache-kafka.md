Title: Remote code execution flaw patched in Apache Kafka
Date: 2023-02-15T14:01:58+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-02-15-remote-code-execution-flaw-patched-in-apache-kafka

[Source](https://portswigger.net/daily-swig/remote-code-execution-flaw-patched-in-apache-kafka){:target="_blank" rel="noopener"}

> Possible RCE and denial-of-service issue discovered in Kafka Connect [...]
