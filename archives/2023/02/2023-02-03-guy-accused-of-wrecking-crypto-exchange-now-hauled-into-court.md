Title: Guy accused of wrecking crypto exchange now hauled into court
Date: 2023-02-03T19:30:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-02-03-guy-accused-of-wrecking-crypto-exchange-now-hauled-into-court

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/03/mango_markets_fraud_trial/){:target="_blank" rel="noopener"}

> Mango Markets still offline for now... but v4 comeback release looms The man accused of bringing down decentralized crypto exchange Mango Markets through market manipulation has made his first appearance in court in connection with the theft of millions in cryptocurrency.... [...]
