Title: Largest Canadian bookstore Indigo shuts down site after cyberattack
Date: 2023-02-09T15:28:52-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-02-09-largest-canadian-bookstore-indigo-shuts-down-site-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/largest-canadian-bookstore-indigo-shuts-down-site-after-cyberattack/){:target="_blank" rel="noopener"}

> Indigo Books & Music, the largest bookstore chain in Canada, has been struck by a cyberattack yesterday, causing the company to make the website unavailable to customers and to only accept cash payments. [...]
