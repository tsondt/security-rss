Title: NY attorney general forces spyware vendor to alert victims
Date: 2023-02-04T11:23:17-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-04-ny-attorney-general-forces-spyware-vendor-to-alert-victims

[Source](https://www.bleepingcomputer.com/news/security/ny-attorney-general-forces-spyware-vendor-to-alert-victims/){:target="_blank" rel="noopener"}

> The New York attorney general's office has announced a $410,000 fine against a stalkerware developer who used 16 companies to promote surveillance tools illegally. [...]
