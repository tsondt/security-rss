Title: Attackers abuse Microsoft’s 'verified publisher' status to steal data
Date: 2023-02-01T06:30:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-02-01-attackers-abuse-microsofts-verified-publisher-status-to-steal-data

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/01/microsoft_oauth_attack_proofpoint/){:target="_blank" rel="noopener"}

> Malicious OAuth apps were the tickets into victims' systems Miscreants using malicious OAuth applications abused Microsoft's "verified publisher" status to gain access to organizations' cloud environments, then steal data and pry into to users' mailboxes, calendars, and meetings.... [...]
