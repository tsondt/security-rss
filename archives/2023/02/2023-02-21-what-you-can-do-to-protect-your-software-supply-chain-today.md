Title: What you can do to protect your software supply chain today
Date: 2023-02-21T17:00:00+00:00
Author: Ning Ge
Category: GCP Security
Tags: Identity & Security;Application Development
Slug: 2023-02-21-what-you-can-do-to-protect-your-software-supply-chain-today

[Source](https://cloud.google.com/blog/products/application-development/what-you-can-do-to-protect-your-software-supply-chain-today/){:target="_blank" rel="noopener"}

> “What are things we can do today to protect our software supply chains?” This is one of the top questions our team often gets when talking to IT leaders and practitioners about protecting their software supply chains – the code, people, systems, and processes that contribute to development and delivery of software. In the past few years, and frankly speaking, even today, this notion of “software supply chain” and the security risks involved are still a bit foreign to some organizations. However, we found that a brief educational session or a quick case study would easily help them understand the [...]
