Title: Microsoft sweeps up after breaking .NET with December security updates
Date: 2023-02-01T18:59:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-02-01-microsoft-sweeps-up-after-breaking-net-with-december-security-updates

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/01/microsoft_fix_dotnet_xps/){:target="_blank" rel="noopener"}

> XPS doc display issues fixed – until the next patch, at least Microsoft this week rolled out fixes to issues caused by security updates released in December 2022 that botched how XPS documents are displayed in various versions of.NET and.NET Framework.... [...]
