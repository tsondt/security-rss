Title: Twitter gets rid of SMS 2FA for non-Blue members — What you need to do
Date: 2023-02-19T21:38:02-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Technology
Slug: 2023-02-19-twitter-gets-rid-of-sms-2fa-for-non-blue-members-what-you-need-to-do

[Source](https://www.bleepingcomputer.com/news/security/twitter-gets-rid-of-sms-2fa-for-non-blue-members-what-you-need-to-do/){:target="_blank" rel="noopener"}

> Twitter has announced that it will no longer support SMS two-factor authentication unless you pay for a Twitter Blue subscription. However, there are more secure options for multi-factor authentication, which we describe below. [...]
