Title: The Week in Ransomware - February 3rd 2023 - Ending with a mess
Date: 2023-02-03T19:47:27-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-02-03-the-week-in-ransomware-february-3rd-2023-ending-with-a-mess

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-february-3rd-2023-ending-with-a-mess/){:target="_blank" rel="noopener"}

> While the week started slowly, it turned into a big ransomware mess, with attacks striking a big blow at businesses running VMware ESXi servers. [...]
