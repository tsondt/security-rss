Title: VMware warns admins to patch ESXi servers, disable OpenSLP service
Date: 2023-02-06T16:44:18-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-06-vmware-warns-admins-to-patch-esxi-servers-disable-openslp-service

[Source](https://www.bleepingcomputer.com/news/security/vmware-warns-admins-to-patch-esxi-servers-disable-openslp-service/){:target="_blank" rel="noopener"}

> VMware warned customers today to install the latest security updates and disable the OpenSLP service targeted in a large-scale campaign of ransomware attacks against Internet-exposed and vulnerable ESXi servers. [...]
