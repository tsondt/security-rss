Title: Scandinavian Airlines says cyberattack caused passenger data leak
Date: 2023-02-16T15:32:09-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-16-scandinavian-airlines-says-cyberattack-caused-passenger-data-leak

[Source](https://www.bleepingcomputer.com/news/security/scandinavian-airlines-says-cyberattack-caused-passenger-data-leak/){:target="_blank" rel="noopener"}

> Scandinavian Airlines (SAS) has posted a notice warning passengers that a recent multi-hour outage of its website and mobile app was caused by a cyberattack that also exposed customer data. [...]
