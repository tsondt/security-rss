Title: KrebsOnSecurity in Upcoming Hulu Series on Ashley Madison Breach
Date: 2023-02-07T22:16:16+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ashley Madison breach;ABC News Studio;ashley madison breach;hulu;The Ashley Madison Affair;The Wrap;Wall to Wall Media
Slug: 2023-02-07-krebsonsecurity-in-upcoming-hulu-series-on-ashley-madison-breach

[Source](https://krebsonsecurity.com/2023/02/krebsonsecurity-in-upcoming-hulu-series-on-ashley-madison-breach/){:target="_blank" rel="noopener"}

> KrebsOnSecurity will likely have a decent amount of screen time in an upcoming Hulu documentary series about the 2015 megabreach at marital infidelity site Ashley Madison. While I can’t predict what the producers will do with the video interviews we shot, it’s fair to say the series will explore compelling new clues as to who may have been responsible for the attack. The new docuseries produced by ABC News Studios and Wall to Wall Media is tentatively titled, “ The Ashley Madison Affair,” and is slated for release on Hulu in late Spring 2023. Wall to Wall Media is part [...]
