Title: PayPal and Twitter abused in Turkey relief donation scams
Date: 2023-02-09T06:00:00-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-02-09-paypal-and-twitter-abused-in-turkey-relief-donation-scams

[Source](https://www.bleepingcomputer.com/news/security/paypal-and-twitter-abused-in-turkey-relief-donation-scams/){:target="_blank" rel="noopener"}

> Scammers are now exploiting the ongoing humanitarian crisis in Turkey and Syria: this time stealing donations by abusing legitimate platforms like PayPal and Twitter. [...]
