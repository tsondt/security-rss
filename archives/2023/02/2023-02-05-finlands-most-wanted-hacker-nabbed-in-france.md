Title: Finland’s Most-Wanted Hacker Nabbed in France
Date: 2023-02-05T16:14:13+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Antti Kurittu;hack the planet;htp;Julius Kivimäki;Lizard Squad;Ryan Cleary;Vastaamo Psychotherapy Center;Zeekill
Slug: 2023-02-05-finlands-most-wanted-hacker-nabbed-in-france

[Source](https://krebsonsecurity.com/2023/02/finlands-most-wanted-hacker-nabbed-in-france/){:target="_blank" rel="noopener"}

> Julius “Zeekill” Kivimäki, a 25-year-old Finnish man charged with extorting a local online psychotherapy practice and leaking therapy notes for more than 22,000 patients online, was arrested this week in France. A notorious hacker convicted of perpetrating tens of thousands of cybercrimes, Kivimäki had been in hiding since October 2022, when he failed to show up in court and Finland issued an international warrant for his arrest. In late October 2022, Kivimäki was charged (and “arrested in absentia,” according to the Finns) with attempting to extort money from the Vastaamo Psychotherapy Center. In that breach, which occurred in October 2020, [...]
