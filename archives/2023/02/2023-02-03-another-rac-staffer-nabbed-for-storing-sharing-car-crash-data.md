Title: Another RAC staffer nabbed for storing, sharing car crash data
Date: 2023-02-03T11:30:07+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-02-03-another-rac-staffer-nabbed-for-storing-sharing-car-crash-data

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/03/roadside_recovery/){:target="_blank" rel="noopener"}

> Once is an accident. Twice is coincidence. Surely there won't be a third for roadside assistance biz A former employee of RAC, one of Britain's major roadside recovery service operators, has pleaded guilty to data theft after he stored traffic accident information on his personal device that was passed onto claims companies.... [...]
