Title: Ransomware scum launch wave of attacks on critical, but old, VMWare ESXi vuln
Date: 2023-02-06T06:30:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-02-06-ransomware-scum-launch-wave-of-attacks-on-critical-but-old-vmware-esxi-vuln

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/06/esxi_ransomware_campaign/){:target="_blank" rel="noopener"}

> You’ve had almost two years to patch and some of the software is EOL, now attackers déployer un rançongiciel France's Computer Emergency Response Team has issued a Bulletin D'Alerte regarding a campaign to infect VMware’s ESXI hypervisor with ransomware.... [...]
