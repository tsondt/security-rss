Title: Have we learnt nothing from SolarWinds supply chain attacks? Not yet it appears
Date: 2023-02-05T12:00:11+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-02-05-have-we-learnt-nothing-from-solarwinds-supply-chain-attacks-not-yet-it-appears

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/05/supply_chain_security_efforts/){:target="_blank" rel="noopener"}

> From frameworks to new federal offices it's time to get busy The hack of SolarWinds' software more than two years ago pushed the threat of software supply chain attacks to the front of security conversations, but is anything being done?.... [...]
