Title: Cloud CISO Perspectives: February 2023
Date: 2023-02-28T17:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Identity & Security
Slug: 2023-02-28-cloud-ciso-perspectives-february-2023

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-february-2023/){:target="_blank" rel="noopener"}

> Welcome to February’s Cloud CISO Perspectives. This month, we’re going to take a look at one of the most important issues our industry faces right now: securing the software supply chain. At Google Cloud, we’ve been heavily invested in creating a layered approach to software supply chain security, which I talk about in my column below. But first, I want to acknowledge that it has been one year since Russia invaded Ukraine. In addition to the immeasurable impact on the lives across the region, this also marks the first time that cyber operations have played such a prominent role in [...]
