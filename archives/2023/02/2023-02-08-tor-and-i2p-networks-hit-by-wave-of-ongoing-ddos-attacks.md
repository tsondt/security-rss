Title: Tor and I2P networks hit by wave of ongoing DDoS attacks
Date: 2023-02-08T15:42:14-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-08-tor-and-i2p-networks-hit-by-wave-of-ongoing-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/security/tor-and-i2p-networks-hit-by-wave-of-ongoing-ddos-attacks/){:target="_blank" rel="noopener"}

> If you've been experiencing Tor network connectivity and performance issues lately, you're not the only one since many others have had issues with onion sites loading slower or not loading at all. [...]
