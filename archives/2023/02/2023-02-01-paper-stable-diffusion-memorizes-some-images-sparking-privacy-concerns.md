Title: Paper: Stable Diffusion “memorizes” some images, sparking privacy concerns
Date: 2023-02-01T18:37:40+00:00
Author: Benj Edwards
Category: Ars Technica
Tags: Biz & IT;adversarial AI;AI;AI ethics;Google Imagen;image synthesis;machine learning;privacy;Stable Diffusion
Slug: 2023-02-01-paper-stable-diffusion-memorizes-some-images-sparking-privacy-concerns

[Source](https://arstechnica.com/?p=1913780){:target="_blank" rel="noopener"}

> Enlarge / An image from Stable Diffusion’s training set compared (left) to a similar Stable Diffusion generation (right) when prompted with "Ann Graham Lotz." (credit: Carlini et al., 2023) On Monday, a group of AI researchers from Google, DeepMind, UC Berkeley, Princeton, and ETH Zurich released a paper outlining an adversarial attack that can extract a small percentage of training images from latent diffusion AI image synthesis models like Stable Diffusion. It challenges views that image synthesis models do not memorize their training data and that training data might remain private if not disclosed. Recently, AI image synthesis models have [...]
