Title: FTX fiasco founder SBF faces further fraud charges
Date: 2023-02-23T20:30:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-02-23-ftx-fiasco-founder-sbf-faces-further-fraud-charges

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/23/sbf_additional_charges_ftx/){:target="_blank" rel="noopener"}

> Fake donors allegedly padded politicians' pockets, both Republican and Democrat FTX founder Sam Bankman-Fried's eight-count indictment related to the collapse of his crypto empire has been superseded by a new 12-count indictment unsealed in New York which provide graphic details about the extent the defunct biz paid off politicians.... [...]
