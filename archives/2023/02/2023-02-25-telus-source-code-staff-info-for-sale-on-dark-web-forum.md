Title: Telus source code, staff info for sale on dark web forum
Date: 2023-02-25T00:30:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-25-telus-source-code-staff-info-for-sale-on-dark-web-forum

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/25/telus_source_code_github_repos/){:target="_blank" rel="noopener"}

> $50k buys you '1,000 unique repositories' that may or may not be legit Canadian communications giant Telus is investigating whether crooks have stolen employee data and its source code, all of which is being offered for sale on a criminal forum.... [...]
