Title: Russian hackers using new Graphiron information stealer in Ukraine
Date: 2023-02-08T06:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-08-russian-hackers-using-new-graphiron-information-stealer-in-ukraine

[Source](https://www.bleepingcomputer.com/news/security/russian-hackers-using-new-graphiron-information-stealer-in-ukraine/){:target="_blank" rel="noopener"}

> The Russian hacking group known as 'Nodaria' (UAC-0056) is using a new information-stealing malware called 'Graphiron' to steal data from Ukrainian organizations. [...]
