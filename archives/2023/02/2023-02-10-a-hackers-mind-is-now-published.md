Title: A Hacker’s Mind Is Now Published
Date: 2023-02-10T20:03:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;A Hacker's Mind;books;Schneier news
Slug: 2023-02-10-a-hackers-mind-is-now-published

[Source](https://www.schneier.com/blog/archives/2023/02/a-hackers-mind-is-now-published.html){:target="_blank" rel="noopener"}

> Tuesday was the official publication date of A Hacker’s Mind: How the Powerful Bend Society’s Rules, and How to Bend them Back. It broke into the 2000s on the Amazon best-seller list. Reviews in the New York Times, Cory Doctorow’s blog, Science, and the Associated Press. I wrote essays related to the book for CNN and John Scalzi’s blog. Two podcast interviews: Keen On and Lawfare. And a written interview for the Ash Center at the Harvard Kennedy School. Lots more coming, I believe. Get your copy here. And—last request—right now there’s one Amazon review, and it’s not a good [...]
