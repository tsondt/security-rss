Title: Suspect in Finnish psychotherapy center blackmail hack arrested
Date: 2023-02-08T06:30:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-08-suspect-in-finnish-psychotherapy-center-blackmail-hack-arrested

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/08/vastaamo_hack_arrest_finland/){:target="_blank" rel="noopener"}

> Suomi sentence expected for shrink records theft French police have arrested a 25-year-old Finnish man accused of hacking a psychotherapy clinic, stealing more than 22,000 patients' therapy notes, demanding ransom payments from them and also leaking this very private info on a Tor website.... [...]
