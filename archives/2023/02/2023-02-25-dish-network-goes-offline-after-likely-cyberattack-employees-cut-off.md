Title: Dish Network goes offline after likely cyberattack, employees cut off
Date: 2023-02-25T00:43:12-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-02-25-dish-network-goes-offline-after-likely-cyberattack-employees-cut-off

[Source](https://www.bleepingcomputer.com/news/security/dish-network-goes-offline-after-likely-cyberattack-employees-cut-off/){:target="_blank" rel="noopener"}

> American TV giant and satellite broadcast provider, Dish Network has mysteriously gone offline with its websites and apps ceasing to function over the past 24 hours. [...]
