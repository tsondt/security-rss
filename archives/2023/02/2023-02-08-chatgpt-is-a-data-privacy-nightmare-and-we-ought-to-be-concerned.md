Title: ChatGPT is a data privacy nightmare, and we ought to be concerned
Date: 2023-02-08T14:12:27+00:00
Author: The Conversation
Category: Ars Technica
Tags: Biz & IT;Policy;AI;Artificial Intelligence;ChatGPT;ChatGPT Plus;privacy;syndication
Slug: 2023-02-08-chatgpt-is-a-data-privacy-nightmare-and-we-ought-to-be-concerned

[Source](https://arstechnica.com/?p=1915949){:target="_blank" rel="noopener"}

> Enlarge ChatGPT has taken the world by storm. Within two months of its release it reached 100 million active users, making it the fastest-growing consumer application ever launched. Users are attracted to the tool’s advanced capabilities —and concerned by its potential to cause disruption in various sectors. A much less discussed implication is the privacy risks ChatGPT poses to each and every one of us. Just yesterday, Google unveiled its own conversational AI called Bard, and others will surely follow. Technology companies working on AI have well and truly entered an arms race. The problem is, it’s fueled by our [...]
