Title: Eurocops shut down Exclu encrypted messaging app, arrest dozens
Date: 2023-02-07T07:30:05+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-02-07-eurocops-shut-down-exclu-encrypted-messaging-app-arrest-dozens

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/07/police_exclu_encrypted/){:target="_blank" rel="noopener"}

> German and Dutch authorities say the app was a favorite of organized criminals and drug smugglers An encrypted messaging service that has been on law enforcement's radar since a 2019 raid on an old NATO bunker has been shut down after a sweeping series of raids across Europe last week.... [...]
