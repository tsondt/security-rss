Title: How to use AWS Private Certificate Authority short-lived certificate mode
Date: 2023-02-20T22:26:45+00:00
Author: Zachary Miller
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Compliance;Identity;Security;Security Blog
Slug: 2023-02-20-how-to-use-aws-private-certificate-authority-short-lived-certificate-mode

[Source](https://aws.amazon.com/blogs/security/how-to-use-aws-private-certificate-authority-short-lived-certificate-mode/){:target="_blank" rel="noopener"}

> AWS Private Certificate Authority (AWS Private CA) is a highly available, fully managed private certificate authority (CA) service that you can use to create CA hierarchies and issue private X.509 certificates. You can use these private certificates to establish endpoints for TLS encryption, cryptographically sign code, authenticate users, and more. Based on customer feedback for [...]
