Title: Atlassian warns of critical Jira Service Management auth flaw
Date: 2023-02-03T09:31:44-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-03-atlassian-warns-of-critical-jira-service-management-auth-flaw

[Source](https://www.bleepingcomputer.com/news/security/atlassian-warns-of-critical-jira-service-management-auth-flaw/){:target="_blank" rel="noopener"}

> A critical vulnerability in Atlassian's Jira Service Management Server and Data Center could allow an unauthenticated attacker to impersonate other users and gain remote access to the systems. [...]
