Title: When Low-Tech Hacks Cause High-Impact Breaches
Date: 2023-02-27T04:15:15+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Web Fraud 2.0;escrow.com;GoDaddy breach December 2022;GoDaddy breach March 2020;GoDaddy breach November 2020;GoDaddy Breach November 2021;vishing;voice phishing
Slug: 2023-02-27-when-low-tech-hacks-cause-high-impact-breaches

[Source](https://krebsonsecurity.com/2023/02/when-low-tech-hacks-cause-high-impact-breaches/){:target="_blank" rel="noopener"}

> Web hosting giant GoDaddy made headlines this month when it disclosed that a multi-year breach allowed intruders to steal company source code, siphon customer and employee login credentials, and foist malware on customer websites. Media coverage understandably focused on GoDaddy’s admission that it suffered three different cyberattacks over as many years at the hands of the same hacking group. But it’s worth revisiting how this group typically got in to targeted companies: By calling employees and tricking them into navigating to a phishing website. In a filing with the U.S. Securities and Exchange Commission (SEC), GoDaddy said it determined that [...]
