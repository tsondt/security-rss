Title: AWS now licensed by DESC to operate as a Tier 1 cloud service provider in the Middle East (UAE) Region
Date: 2023-02-15T21:03:11+00:00
Author: Ioana Mecu
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Abu Dhabi;Auditing;AWS security;Cloud Service Provider;Compliance;CSA STAR;CSP;DESC;Dubai Electronic Security Centre;Emirate of Dubai;Government;IAR;Information Security Regulation;ISO;Licence;Middle East;Public Sector;Security;Security Blog;UAE
Slug: 2023-02-15-aws-now-licensed-by-desc-to-operate-as-a-tier-1-cloud-service-provider-in-the-middle-east-uae-region

[Source](https://aws.amazon.com/blogs/security/aws-now-licensed-by-desc-to-operate-as-a-tier-1-cloud-service-provider-in-the-middle-east-uae-region/){:target="_blank" rel="noopener"}

> We continue to expand the scope of our assurance programs at Amazon Web Services (AWS) and are pleased to announce that our Middle East (UAE) Region is now certified by the Dubai Electronic Security Centre (DESC) to operate as a Tier 1 cloud service provider (CSP). This alignment with DESC requirements demonstrates our continuous commitment [...]
