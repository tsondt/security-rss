Title: Microsoft Patch Tuesday, February 2023 Edition
Date: 2023-02-14T21:01:41+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;Security Tools;Time to Patch;CVE-2023-21529;CVE-2023-21706;CVE-2023-21707;CVE-2023-21715;CVE-2023-21716;CVE-2023-21823;CVE-2023-23376;Dustin Childs;Immersive Labs;Internet Explorer 11;Johannes Ullrich;Kevin Breen;Mandiant;Microsoft Office;Microsoft Patch Tuesday February 2023;sans internet storm center;Trend Micro Zero Day Initiative
Slug: 2023-02-14-microsoft-patch-tuesday-february-2023-edition

[Source](https://krebsonsecurity.com/2023/02/microsoft-patch-tuesday-february-2023-edition/){:target="_blank" rel="noopener"}

> Microsoft is sending the world a whole bunch of love today, in the form of patches to plug dozens of security holes in its Windows operating systems and other software. This year’s special Valentine’s Day Patch Tuesday includes fixes for a whopping three different “zero-day” vulnerabilities that are already being used in active attacks. Microsoft’s security advisories are somewhat sparse with details about the zero-day bugs. Redmond flags CVE-2023-23376 as an “Important” elevation of privilege vulnerability in the Windows Common Log File System Driver, which is present in Windows 10 and 11 systems, as well as many server versions of [...]
