Title: Record-breaking number of record-breaking DDoS attacks confirmed
Date: 2023-02-14T20:15:14+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-02-14-record-breaking-number-of-record-breaking-ddos-attacks-confirmed

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/14/cloudflare_record_ddos_attack/){:target="_blank" rel="noopener"}

> And growing abuse of cloud – because using hijacked Brazilian cable modems to down sites is so 2013 Dozens of companies over the weekend were hit by distributed denial-of-service (DDoS) attacks, including the largest one yet recorded, or so Cloudflare says.... [...]
