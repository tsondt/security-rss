Title: My Password Manager was Hacked! How to Prevent a Catastrophe
Date: 2023-02-15T10:06:12-05:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-02-15-my-password-manager-was-hacked-how-to-prevent-a-catastrophe

[Source](https://www.bleepingcomputer.com/news/security/my-password-manager-was-hacked-how-to-prevent-a-catastrophe/){:target="_blank" rel="noopener"}

> A recent password manager breach sent a shockwave through the security community. No service is perfect, and that goes for password managers, so what can you do to protect yourself? [...]
