Title: Spain, U.S. dismantle phishing gang that stole $5 million in a year
Date: 2023-02-13T12:44:34-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-13-spain-us-dismantle-phishing-gang-that-stole-5-million-in-a-year

[Source](https://www.bleepingcomputer.com/news/security/spain-us-dismantle-phishing-gang-that-stole-5-million-in-a-year/){:target="_blank" rel="noopener"}

> Spain's National Police and the U.S. Secret Service have dismantled a Madrid-based international cybercrime ring comprised of nine members who stole over €5,000,000 from individuals and North American companies. [...]
