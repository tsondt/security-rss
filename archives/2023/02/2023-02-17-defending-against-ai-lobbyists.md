Title: Defending against AI Lobbyists
Date: 2023-02-17T12:33:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;ChatGPT;essays;impersonation;laws
Slug: 2023-02-17-defending-against-ai-lobbyists

[Source](https://www.schneier.com/blog/archives/2023/02/defending-against-ai-lobbyists.html){:target="_blank" rel="noopener"}

> When is it time to start worrying about artificial intelligence interfering in our democracy? Maybe when an AI writes a letter to The New York Times opposing the regulation of its own technology. That happened last month. And because the letter was responding to an essay we wrote, we’re starting to get worried. And while the technology can be regulated, the real solution lies in recognizing that the problem is human actors—and those we can do something about. Our essay argued that the much heralded launch of the AI chatbot ChatGPT, a system that can generate text realistic enough to [...]
