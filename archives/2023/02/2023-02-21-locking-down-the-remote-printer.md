Title: Locking down the remote printer
Date: 2023-02-21T07:21:13+00:00
Author: John E Dunn
Category: The Register
Tags: 
Slug: 2023-02-21-locking-down-the-remote-printer

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/21/locking_down_the_remote_printer/){:target="_blank" rel="noopener"}

> No longer a blind spot, printer security is now a grown up conversation says Brother Sponsored Feature As businesses journey deeper into an era of restless digital change, it's surprising how inventions from past decades still define the office environment.... [...]
