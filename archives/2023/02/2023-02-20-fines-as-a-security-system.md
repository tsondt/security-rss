Title: Fines as a Security System
Date: 2023-02-20T12:09:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;courts;law enforcement;stalking;theft;tracking
Slug: 2023-02-20-fines-as-a-security-system

[Source](https://www.schneier.com/blog/archives/2023/02/fines-as-a-security-system.html){:target="_blank" rel="noopener"}

> Tile has an interesting security solution to make its tracking tags harder to use for stalking: The Anti-Theft Mode feature will make the devices invisible to Scan and Secure, the company’s in-app feature that lets you know if any nearby Tiles are following you. But to activate the new Anti-Theft Mode, the Tile owner will have to verify their real identity with a government-issued ID, submit a biometric scan that helps root out fake IDs, agree to let Tile share their information with law enforcement and agree to be subject to a $1 million penalty if convicted in a court [...]
