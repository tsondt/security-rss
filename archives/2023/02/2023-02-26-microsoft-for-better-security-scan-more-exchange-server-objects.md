Title: Microsoft: For better security, scan more Exchange server objects
Date: 2023-02-26T09:00:10+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-02-26-microsoft-for-better-security-scan-more-exchange-server-objects

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/26/microsoft_exchange_server_exclusion/){:target="_blank" rel="noopener"}

> Software giant takes some files and processes off the exclusion list Microsoft is recommending that Exchange server users scan certain objects for viruses and other threats that until now had been excluded.... [...]
