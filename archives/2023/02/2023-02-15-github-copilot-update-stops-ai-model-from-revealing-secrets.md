Title: GitHub Copilot update stops AI model from revealing secrets
Date: 2023-02-15T16:03:12-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-15-github-copilot-update-stops-ai-model-from-revealing-secrets

[Source](https://www.bleepingcomputer.com/news/security/github-copilot-update-stops-ai-model-from-revealing-secrets/){:target="_blank" rel="noopener"}

> GitHub has updated the AI model of Copilot, a programming assistant that generates real-time source code and function recommendations in Visual Studio, and says it's now safer and more powerful. [...]
