Title: Kremlin claims Ukraine hackers behind fake missile strike alerts
Date: 2023-02-23T06:30:11+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-02-23-kremlin-claims-ukraine-hackers-behind-fake-missile-strike-alerts

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/23/russia_fake_missile_alerts/){:target="_blank" rel="noopener"}

> Ten cities panic after emergency systems start Putin out warnings of an impending attack Millions of Russians in almost a dozen cities throughout the country were greeted Wednesday morning by radio alerts, text messages, and sirens warning of an air raid or missile strikes that never occurred. The warnings were later blamed on hackers.... [...]
