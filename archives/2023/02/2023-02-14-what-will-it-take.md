Title: What Will It Take?
Date: 2023-02-14T12:06:06+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;economics of security;essays;infrastructure;laws
Slug: 2023-02-14-what-will-it-take

[Source](https://www.schneier.com/blog/archives/2023/02/what-will-it-take.html){:target="_blank" rel="noopener"}

> What will it take for policy makers to take cybersecurity seriously? Not minimal-change seriously. Not here-and-there seriously. But really seriously. What will it take for policy makers to take cybersecurity seriously enough to enact substantive legislative changes that would address the problems? It’s not enough for the average person to be afraid of cyberattacks. They need to know that there are engineering fixes—and that’s something we can provide. For decades, I have been waiting for the “big enough” incident that would finally do it. In 2015, Chinese military hackers hacked the Office of Personal Management and made off with the [...]
