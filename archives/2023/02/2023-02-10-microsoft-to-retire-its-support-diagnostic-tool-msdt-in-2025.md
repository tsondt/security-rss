Title: Microsoft to retire its Support Diagnostic Tool (MSDT) in 2025
Date: 2023-02-10T13:05:34-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-02-10-microsoft-to-retire-its-support-diagnostic-tool-msdt-in-2025

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-to-retire-its-support-diagnostic-tool-msdt-in-2025/){:target="_blank" rel="noopener"}

> Microsoft announced that it will retire Microsoft Support Diagnostic Tool (MSDT) troubleshooters in future versions of Windows, with MSDT ultimately being removed in 2025. [...]
