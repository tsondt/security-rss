Title: Radio silence from DMS vendor quartet over XSS zero-days
Date: 2023-02-10T11:55:43+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-02-10-radio-silence-from-dms-vendor-quartet-over-xss-zero-days

[Source](https://portswigger.net/daily-swig/radio-silence-from-dms-vendor-quartet-over-xss-zero-days){:target="_blank" rel="noopener"}

> No response or patch yet forthcoming from providers of vulnerable document management systems [...]
