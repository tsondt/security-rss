Title: Keeping unstructured data safe and sound
Date: 2023-02-06T13:00:12+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-02-06-keeping-unstructured-data-safe-and-sound

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/06/keeping_unstructured_data_safe_and/){:target="_blank" rel="noopener"}

> How Dell PowerScale helps defend against information breaches Webinar There was a time when data was stored in cardboard files inside metal filing cabinets. The drawers were locked with a little key in the corner of the cabinet, which generally meant there was no getting in unless you had that key or at least some time to spare with a crowbar.... [...]
