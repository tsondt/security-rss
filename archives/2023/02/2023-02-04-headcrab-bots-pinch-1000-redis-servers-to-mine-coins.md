Title: HeadCrab bots pinch 1,000+ Redis servers to mine coins
Date: 2023-02-04T00:27:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-04-headcrab-bots-pinch-1000-redis-servers-to-mine-coins

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/04/headcrab_botnet_aqua/){:target="_blank" rel="noopener"}

> We devoting full time to floating under /etc A sneaky botnet dubbed HeadCrab that uses bespoke malware to mine for Monero has infected at least 1,200 Redis servers in the last 18 months.... [...]
