Title: New Dingo crypto token found charging a 99% transaction fee
Date: 2023-02-05T11:12:24-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-02-05-new-dingo-crypto-token-found-charging-a-99-transaction-fee

[Source](https://www.bleepingcomputer.com/news/security/new-dingo-crypto-token-found-charging-a-99-percent-transaction-fee/){:target="_blank" rel="noopener"}

> Researchers at IT security company Check Point security have flagged Dingo Token as a potential scam after finding a function that allows the project's owner to manipulate trading fees up to 99% of the transaction value. [...]
