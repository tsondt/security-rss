Title: Samsung adds zero-click attack protection to Galaxy devices
Date: 2023-02-20T08:16:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-02-20-samsung-adds-zero-click-attack-protection-to-galaxy-devices

[Source](https://www.bleepingcomputer.com/news/security/samsung-adds-zero-click-attack-protection-to-galaxy-devices/){:target="_blank" rel="noopener"}

> Samsung has developed a new security system called Samsung Message Guard to help Galaxy smartphone users keep safe from the so-called "zero-click" exploits that use malicious image files. [...]
