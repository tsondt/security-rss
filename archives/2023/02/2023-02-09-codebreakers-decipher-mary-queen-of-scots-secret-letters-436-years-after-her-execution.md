Title: Codebreakers decipher Mary, Queen of Scots' secret letters 436 years after her execution
Date: 2023-02-09T08:30:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-09-codebreakers-decipher-mary-queen-of-scots-secret-letters-436-years-after-her-execution

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/09/codebreakers_mary_queen_of_scots/){:target="_blank" rel="noopener"}

> Digital sleuths chop through crypto challenge in 'surreal' search A team of codebreakers discovered – and then cracked – more than 50 secret letters written by Mary Stuart, Queen of Scots while she was imprisoned in England by her cousin, Queen Elizabeth I.... [...]
