Title: DNA testing biz vows to improve infosec after criminals break into database it forgot it had
Date: 2023-02-20T20:30:11+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-02-20-dna-testing-biz-vows-to-improve-infosec-after-criminals-break-into-database-it-forgot-it-had

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/20/dna_testing_firm_pays_200k/){:target="_blank" rel="noopener"}

> Settles lawsuit with two states after wider leak that affected millions A DNA diagnostics company will pay $400,000 and tighten its security in the wake of a 2021 attack where criminals broke into its network and swiped personal data on over two million people from a nine-year-old "legacy" database the company forgot it had.... [...]
