Title: Over 12% of analyzed online stores expose private data, backups
Date: 2023-02-07T13:45:22-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-07-over-12-of-analyzed-online-stores-expose-private-data-backups

[Source](https://www.bleepingcomputer.com/news/security/over-12-percent-of-analyzed-online-stores-expose-private-data-backups/){:target="_blank" rel="noopener"}

> Many online stores are exposing private backups in public folders, including internal account passwords, which can be leveraged to take over the e-commerce sites and extort owners. [...]
