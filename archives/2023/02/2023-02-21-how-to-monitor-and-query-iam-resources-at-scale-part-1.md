Title: How to monitor and query IAM resources at scale – Part 1
Date: 2023-02-21T20:14:33+00:00
Author: Michael Chan
Category: AWS Security
Tags: AWS Identity and Access Management (IAM);Intermediate (200);Security, Identity, & Compliance;Technical How-to;authorization;control plane;data plane;eventual consistency;IAM;rate limiting;Security;Security Blog;throttling
Slug: 2023-02-21-how-to-monitor-and-query-iam-resources-at-scale-part-1

[Source](https://aws.amazon.com/blogs/security/how-to-monitor-and-query-iam-resources-at-scale-part-1/){:target="_blank" rel="noopener"}

> In this two-part blog post, we’ll provide recommendations for using AWS Identity and Access Management (IAM) APIs, and we’ll share useful details on how IAM works so that you can use it more effectively. For example, you might be creating new IAM resources such as roles and policies through automation and notice a delay for resource propagations. [...]
