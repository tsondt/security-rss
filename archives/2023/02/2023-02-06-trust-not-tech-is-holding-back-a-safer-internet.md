Title: Trust, not tech, is holding back a safer internet
Date: 2023-02-06T09:30:14+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2023-02-06-trust-not-tech-is-holding-back-a-safer-internet

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/06/opinion_security_trust/){:target="_blank" rel="noopener"}

> Excuse me, citizen, did you packet this data yourself? Opinion The tech sector is failing at cybersecurity. Global spending on the stuff is at $190 billion a year, a quarter of the US defense budget. That hasn't stemmed an estimated $7 trillion in annual cybercriminal damages. People are fond of saying that the Wild West days of the internet are over, but on those numbers an 1875 Dodge City bank vault looks like Fort Knox.... [...]
