Title: Confidential GKE Nodes are now available on Compute Optimized C2D VMs
Date: 2023-02-16T17:00:00+00:00
Author: Joanna Young
Category: GCP Security
Tags: Identity & Security
Slug: 2023-02-16-confidential-gke-nodes-are-now-available-on-compute-optimized-c2d-vms

[Source](https://cloud.google.com/blog/products/identity-security/confidential-gke-nodes-are-now-available-on-compute-optimized-c2d-vms/){:target="_blank" rel="noopener"}

> Today, we are happy to announce that Confidential GKE Nodes are available on compute optimized C2D VMs. Many companies have adopted Google Kubernetes Engine (GKE) as a key component in their application infrastructure. In some cases, the advantages of using containers and Kubernetes can surpass those of traditional architectures, but migrating to and operating apps in the cloud often requires strategic planning to reduce risk and prevent data breaches. This is where Confidential GKE Nodes can be utilized to enhance the security of your GKE clusters or node pools. aside_block [StructValue([(u'title', u"We'd love your feedback!"), (u'body', <wagtail.wagtailcore.rich_text.RichText object at 0x3ed4501958d0>), [...]
