Title: Manipulating Weights in Face-Recognition AI Systems
Date: 2023-02-03T12:07:04+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;backdoors;face recognition
Slug: 2023-02-03-manipulating-weights-in-face-recognition-ai-systems

[Source](https://www.schneier.com/blog/archives/2023/02/manipulating-weights-in-face-recognition-ai-systems.html){:target="_blank" rel="noopener"}

> Interesting research: “ Facial Misrecognition Systems: Simple Weight Manipulations Force DNNs to Err Only on Specific Persons “: Abstract: In this paper we describe how to plant novel types of backdoors in any facial recognition model based on the popular architecture of deep Siamese neural networks, by mathematically changing a small fraction of its weights (i.e., without using any additional training or optimization). These backdoors force the system to err only on specific persons which are preselected by the attacker. For example, we show how such a backdoored system can take any two images of a particular person and decide [...]
