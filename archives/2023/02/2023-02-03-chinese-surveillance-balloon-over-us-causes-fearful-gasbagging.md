Title: Chinese 'surveillance balloon' over US causes fearful gasbagging
Date: 2023-02-03T05:32:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-02-03-chinese-surveillance-balloon-over-us-causes-fearful-gasbagging

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/03/chinese_surveillance_balloon_over_us/){:target="_blank" rel="noopener"}

> Floats over missile silos, shooting it down ruled more dangerous than whatever it's up to Updated A Chinese high-altitude potential spy balloon, spotted drifting over America, has caused concern about national security – though the US Department of Defense says it will not be shot down by F22s at this time.... [...]
