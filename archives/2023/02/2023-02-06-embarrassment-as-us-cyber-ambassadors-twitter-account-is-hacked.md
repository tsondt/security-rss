Title: Embarrassment as US cyber ambassador's Twitter account is hacked
Date: 2023-02-06T23:59:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-06-embarrassment-as-us-cyber-ambassadors-twitter-account-is-hacked

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/06/us_cyber_ambassador_hacked/){:target="_blank" rel="noopener"}

> 'Perils of the job' we're told A top US cyber diplomat said his Twitter account was compromised over the weekend.... [...]
