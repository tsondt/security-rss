Title: SCARLETEEL hackers use advanced cloud skills to steal source code, data
Date: 2023-02-28T11:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud
Slug: 2023-02-28-scarleteel-hackers-use-advanced-cloud-skills-to-steal-source-code-data

[Source](https://www.bleepingcomputer.com/news/security/scarleteel-hackers-use-advanced-cloud-skills-to-steal-source-code-data/){:target="_blank" rel="noopener"}

> An advanced hacking operation dubbed 'SCARLETEEL' targets public-facing web apps running in containers to infiltrate cloud services and steal sensitive data. [...]
