Title: City of Oakland declares state of emergency after ransomware attack
Date: 2023-02-15T10:47:25-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-15-city-of-oakland-declares-state-of-emergency-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/city-of-oakland-declares-state-of-emergency-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Oakland has declared a local state of emergency because of the impact of a ransomware attack that forced the City to take all its IT systems offline on February 8th. [...]
