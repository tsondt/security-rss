Title: AWS Melbourne Region has achieved HCF Strategic Certification
Date: 2023-02-27T19:02:03+00:00
Author: Lori Klaassen
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Australia Regions;Australian government;AWS security;Cloud security;Compliance;Digital Transformation Agency;DTA;HCF;Hosting Certification Framework;MEL Region;Security;Security Blog;SYD Region
Slug: 2023-02-27-aws-melbourne-region-has-achieved-hcf-strategic-certification

[Source](https://aws.amazon.com/blogs/security/aws-melbourne-region-has-achieved-hcf-strategic-certification/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is delighted to confirm that our new AWS Melbourne Region has achieved Strategic Certification for the Australian Government’s Hosting Certification Framework (HCF). We know that maintaining security and resiliency to keep critical data and infrastructure safe is a top priority for the Australian Government and all our customers in Australia. The [...]
