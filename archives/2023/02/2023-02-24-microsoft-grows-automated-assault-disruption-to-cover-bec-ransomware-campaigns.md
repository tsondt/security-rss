Title: Microsoft grows automated assault disruption to cover BEC, ransomware campaigns
Date: 2023-02-24T06:30:14+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-02-24-microsoft-grows-automated-assault-disruption-to-cover-bec-ransomware-campaigns

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/24/microsoft_365_disrupt_attacks/){:target="_blank" rel="noopener"}

> There’s no HumOR in cyberattacks At last year's Ignite show, Microsoft talked up a capability in its 365 Defender that automatically detects and disrupts a cyberattack while still in progress, hopefully stopping or reducing any resulting damage. Now it's extending that to include additional criminal areas.... [...]
