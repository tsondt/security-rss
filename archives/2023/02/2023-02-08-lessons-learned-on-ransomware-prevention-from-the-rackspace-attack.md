Title: Lessons Learned on Ransomware Prevention from the Rackspace Attack
Date: 2023-02-08T10:04:08-05:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-02-08-lessons-learned-on-ransomware-prevention-from-the-rackspace-attack

[Source](https://www.bleepingcomputer.com/news/security/lessons-learned-on-ransomware-prevention-from-the-rackspace-attack/){:target="_blank" rel="noopener"}

> The ransomware attack on Rackspace has taught us the importance of good cybersecurity habits. Let's see what we can learn from the attack and how organizations can protect themselves. [...]
