Title: Passwords Are Terrible (Surprising No One)
Date: 2023-02-01T12:08:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cracking;national security policy;passwords
Slug: 2023-02-01-passwords-are-terrible-surprising-no-one

[Source](https://www.schneier.com/blog/archives/2023/02/passwords-are-terrible-surprising-no-one.html){:target="_blank" rel="noopener"}

> This is the result of a security audit: More than a fifth of the passwords protecting network accounts at the US Department of the Interior—including Password1234, Password1234!, and ChangeItN0w!—were weak enough to be cracked using standard methods, a recently published security audit of the agency found. [...] The results weren’t encouraging. In all, the auditors cracked 18,174—or 21 percent—­of the 85,944 cryptographic hashes they tested; 288 of the affected accounts had elevated privileges, and 362 of them belonged to senior government employees. In the first 90 minutes of testing, auditors cracked the hashes for 16 percent of the department’s user [...]
