Title: Health-ISAC and Google Cloud partner to build more resilient healthcare, one threat indicator at a time
Date: 2023-02-09T17:00:00+00:00
Author: Adam Licata
Category: GCP Security
Tags: Identity & Security
Slug: 2023-02-09-health-isac-and-google-cloud-partner-to-build-more-resilient-healthcare-one-threat-indicator-at-a-time

[Source](https://cloud.google.com/blog/products/identity-security/h-isac-and-google-cloud-partner-to-build-more-resilient-healthcare/){:target="_blank" rel="noopener"}

> Google Cloud is committed to helping healthcare and life sciences organizations defend themselves from threats that disrupt their ability to care for patients. The pandemic showed how vulnerable this industry is to attacks, and the real-life impacts that shutting down hospital systems and affecting drug makers can have on patients and families. In July 2022, we announced our agreement to join Health Information Sharing and Analysis Center (Health-ISAC) as an Ambassador partner and bring Google’s cybersecurity resources to help this community of more than 750 global organizations fight back. Today, we’re announcing the general availability of our next investment in [...]
