Title: How to visualize IAM Access Analyzer policy validation findings with QuickSight
Date: 2023-02-13T20:11:31+00:00
Author: Mostefa Brougui
Category: AWS Security
Tags: Advanced (300);AWS IAM Access Analyzer;Security, Identity, & Compliance;Technical How-to;Amazon Athena;Amazon QuickSight;analytics;AWS IAM;IAM Access Analyzer;Security;Security Blog
Slug: 2023-02-13-how-to-visualize-iam-access-analyzer-policy-validation-findings-with-quicksight

[Source](https://aws.amazon.com/blogs/security/how-to-visualize-iam-access-analyzer-policy-validation-findings-with-quicksight/){:target="_blank" rel="noopener"}

> In this blog post, we show you how to create an Amazon QuickSight dashboard to visualize the policy validation findings from AWS Identity and Access Management (IAM) Access Analyzer. You can use this dashboard to better understand your policies and how to achieve least privilege by periodically validating your IAM roles against IAM best practices. [...]
