Title: Australian government gives made-in-China CCTV cams the boot
Date: 2023-02-10T04:28:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-02-10-australian-government-gives-made-in-china-cctv-cams-the-boot

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/10/australian_government_removes_chinese_cameras/){:target="_blank" rel="noopener"}

> The usual suspects - Hikvision and Dahua - named as a risk to national security, prompting the usual denials Australia's Defence Department removed all Chinese manufactured surveillance cameras after an audit detailed the number of Hikvision and Dahua devices installed in various government facilities.... [...]
