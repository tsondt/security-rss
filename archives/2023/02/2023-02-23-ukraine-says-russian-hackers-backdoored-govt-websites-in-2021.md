Title: Ukraine says Russian hackers backdoored govt websites in 2021
Date: 2023-02-23T14:50:12-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-23-ukraine-says-russian-hackers-backdoored-govt-websites-in-2021

[Source](https://www.bleepingcomputer.com/news/security/ukraine-says-russian-hackers-backdoored-govt-websites-in-2021/){:target="_blank" rel="noopener"}

> The Computer Emergency Response Team of Ukraine (CERT-UA) says Russian state hackers have breached multiple government websites this week using backdoors planted as far back as December 2021. [...]
