Title: Hacker leaks alleged Activision employee data on cybercrime forum
Date: 2023-02-27T12:08:44-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2023-02-27-hacker-leaks-alleged-activision-employee-data-on-cybercrime-forum

[Source](https://www.bleepingcomputer.com/news/security/hacker-leaks-alleged-activision-employee-data-on-cybercrime-forum/){:target="_blank" rel="noopener"}

> A threat actor has posted data the alleged data stolen from American game publisher Activision in December 2022 on a hacking forum, highlighting the data's value for phishing operations. [...]
