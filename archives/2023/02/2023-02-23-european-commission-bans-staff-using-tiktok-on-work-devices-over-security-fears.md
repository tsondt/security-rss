Title: European Commission bans staff using TikTok on work devices over security fears
Date: 2023-02-23T12:58:45+00:00
Author: Mark Sweney
Category: The Guardian
Tags: TikTok;European Union;Data and computer security;Europe;Technology;World news;Business;China
Slug: 2023-02-23-european-commission-bans-staff-using-tiktok-on-work-devices-over-security-fears

[Source](https://www.theguardian.com/technology/2023/feb/23/european-commission-bans-staff-from-using-tiktok-on-work-devices){:target="_blank" rel="noopener"}

> Parent company, ByteDance, says action is ‘misguided’ and has contacted commission to ‘set the record straight’ The EU’s executive body has banned its thousands of staff from using TikTok over cybersecurity concerns, a decision the Chinese-owned social video app has criticised as “misguided” and based on “fundamental misconceptions”. The European Commission sent an email to employees ordering them to delete the app from all work phones and devices, and any personally owned ones that use the commission’s apps and email. Employees have until 15 March to comply. Continue reading... [...]
