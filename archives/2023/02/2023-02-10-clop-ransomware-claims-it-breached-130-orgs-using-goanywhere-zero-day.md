Title: Clop ransomware claims it breached 130 orgs using GoAnywhere zero-day
Date: 2023-02-10T15:15:39-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-10-clop-ransomware-claims-it-breached-130-orgs-using-goanywhere-zero-day

[Source](https://www.bleepingcomputer.com/news/security/clop-ransomware-claims-it-breached-130-orgs-using-goanywhere-zero-day/){:target="_blank" rel="noopener"}

> The Clop ransomware gang claims to be behind recent attacks that exploited a zero-day vulnerability in the GoAnywhere MFT secure file transfer tool, saying they stole data from over 130 organizations. [...]
