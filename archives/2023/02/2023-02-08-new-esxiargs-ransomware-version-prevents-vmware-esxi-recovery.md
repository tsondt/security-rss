Title: New ESXiArgs ransomware version prevents VMware ESXi recovery
Date: 2023-02-08T22:45:54-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-02-08-new-esxiargs-ransomware-version-prevents-vmware-esxi-recovery

[Source](https://www.bleepingcomputer.com/news/security/new-esxiargs-ransomware-version-prevents-vmware-esxi-recovery/){:target="_blank" rel="noopener"}

> New ESXiArgs ransomware attacks are now encrypting more extensive amounts of data, making it much harder, if not impossible, to recover encrypted VMware ESXi virtual machines. [...]
