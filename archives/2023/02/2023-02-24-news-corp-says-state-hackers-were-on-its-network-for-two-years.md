Title: News Corp says state hackers were on its network for two years
Date: 2023-02-24T13:44:45-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-24-news-corp-says-state-hackers-were-on-its-network-for-two-years

[Source](https://www.bleepingcomputer.com/news/security/news-corp-says-state-hackers-were-on-its-network-for-two-years/){:target="_blank" rel="noopener"}

> Mass media and publishing giant News Corporation (News Corp) says that attackers behind a breach disclosed in 2022 first gained access to its systems two years before, in February 2020. [...]
