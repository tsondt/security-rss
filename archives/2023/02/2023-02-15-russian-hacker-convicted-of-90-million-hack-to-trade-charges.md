Title: Russian hacker convicted of $90 million hack-to-trade charges
Date: 2023-02-15T15:39:15-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-02-15-russian-hacker-convicted-of-90-million-hack-to-trade-charges

[Source](https://www.bleepingcomputer.com/news/security/russian-hacker-convicted-of-90-million-hack-to-trade-charges/){:target="_blank" rel="noopener"}

> Russian national Vladislav Klyushin was found guilty of participating in a global scheme that involved hacking into U.S. computer networks to steal confidential earnings reports, which helped the criminals net $90,000,000 in illegal profits. [...]
