Title: A Device to Turn Traffic Lights Green
Date: 2023-02-22T12:30:20+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;impersonation;police;radio
Slug: 2023-02-22-a-device-to-turn-traffic-lights-green

[Source](https://www.schneier.com/blog/archives/2023/02/a-device-to-turn-traffic-lights-green.html){:target="_blank" rel="noopener"}

> Here’s a story about a hacker who reprogrammed a device called “Flipper Zero” to mimic Opticom transmitters—to turn traffic lights in his path green. As mentioned earlier, the Flipper Zero has a built-in sub-GHz radio that lets the device receive data (or transmit it, with the right firmware in approved regions) on the same wireless frequencies as keyfobs and other devices. Most traffic preemption devices intended for emergency traffic redirection don’t actually transmit signals over RF. Instead, they use optical technology to beam infrared light from vehicles to static receivers mounted on traffic light poles. Perhaps the most well-known branding [...]
