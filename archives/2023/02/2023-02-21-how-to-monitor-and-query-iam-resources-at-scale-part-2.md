Title: How to monitor and query IAM resources at scale – Part 2
Date: 2023-02-21T20:15:45+00:00
Author: Michael Chan
Category: AWS Security
Tags: AWS Identity and Access Management (IAM);Intermediate (200);Security, Identity, & Compliance;Technical How-to;authorization;control plane;data plane;eventual consistency;IAM;rate limiting;Security;Security Blog;throttling
Slug: 2023-02-21-how-to-monitor-and-query-iam-resources-at-scale-part-2

[Source](https://aws.amazon.com/blogs/security/how-to-monitor-and-query-iam-resources-at-scale-part-2/){:target="_blank" rel="noopener"}

> In this post, we continue with our recommendations for using AWS Identity and Access Management (IAM) APIs. In part 1 of this two-part series, we described how you could create IAM resources and use them soon after for authorization decisions. We also described options for monitoring and responding to IAM resource changes for entire accounts. [...]
