Title: Improve security of Amazon RDS master database credentials using AWS Secrets Manager
Date: 2023-02-09T20:32:44+00:00
Author: Vinod Santhanam
Category: AWS Security
Tags: Amazon RDS;AWS Secrets Manager;Intermediate (200);Security, Identity, & Compliance;Technical How-to;cryptography;Security Blog
Slug: 2023-02-09-improve-security-of-amazon-rds-master-database-credentials-using-aws-secrets-manager

[Source](https://aws.amazon.com/blogs/security/improve-security-of-amazon-rds-master-database-credentials-using-secrets-manager/){:target="_blank" rel="noopener"}

> Amazon Relational Database Service (Amazon RDS) makes it simpler to set up, operate, and scale a relational database in the AWS Cloud. AWS Secrets Manager helps you manage, retrieve, and rotate database credentials, API keys, and other secrets. Amazon RDS now offers integration with Secrets Manager to manage master database credentials. You no longer have to manage master database credentials, such as [...]
