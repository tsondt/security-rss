Title: The anatomy of ransomware event targeting data residing in Amazon S3
Date: 2023-02-06T18:07:51+00:00
Author: Megan O'Neil
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;Amazon S3;AWS Backup;AWS security;Cloud security;IAM;KMS;No more ransom;ransomware;RBAC;s3 intelligent tiering;s3 security;Security;Security Blog;SSE-KMS
Slug: 2023-02-06-the-anatomy-of-ransomware-event-targeting-data-residing-in-amazon-s3

[Source](https://aws.amazon.com/blogs/security/anatomy-of-a-ransomware-event-targeting-data-in-amazon-s3/){:target="_blank" rel="noopener"}

> Ransomware events have significantly increased over the past several years and captured worldwide attention. Traditional ransomware events affect mostly infrastructure resources like servers, databases, and connected file systems. However, there are also non-traditional events that you may not be as familiar with, such as ransomware events that target data stored in Amazon Simple Storage Service [...]
