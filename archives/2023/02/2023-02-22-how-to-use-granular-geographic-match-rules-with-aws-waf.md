Title: How to use granular geographic match rules with AWS WAF
Date: 2023-02-22T19:44:39+00:00
Author: Mohit Mysore
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Application security;AWS WAF;Compliance;Identity;Security;Security Blog
Slug: 2023-02-22-how-to-use-granular-geographic-match-rules-with-aws-waf

[Source](https://aws.amazon.com/blogs/security/how-to-use-granular-geographic-match-rules-with-aws-waf/){:target="_blank" rel="noopener"}

> In November 2022, AWS introduced support for granular geographic (geo) match conditions in AWS WAF. This blog post demonstrates how you can use this new feature to customize your AWS WAF implementation and improve the security posture of your protected application. AWS WAF provides inline inspection of inbound traffic at the application layer. You can [...]
