Title: Storage security toughen-up for compliance and cyberwar in 2023
Date: 2023-02-15T12:23:11+00:00
Author: James Hayes
Category: The Register
Tags: 
Slug: 2023-02-15-storage-security-toughen-up-for-compliance-and-cyberwar-in-2023

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/15/storage_security_toughenup_for_compliance/){:target="_blank" rel="noopener"}

> Giving storage platforms enhanced built-in security features will be a significant step toward counteracting the impacts of cybercrime in 2023, Dell experts predict Sponsored Feature Cybercriminals tend not to discriminate when it comes to the type of data they steal. Structured or unstructured, both formats contain valuable information that will bring them a profit. From a cybersecurity practitioner's perspective, however, structural state presents specific challenges when it comes to storing and moving sensitive data assets around.... [...]
