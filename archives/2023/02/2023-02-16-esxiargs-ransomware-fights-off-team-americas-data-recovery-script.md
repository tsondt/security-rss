Title: ESXiArgs ransomware fights off Team America's data recovery script
Date: 2023-02-16T01:30:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-02-16-esxiargs-ransomware-fights-off-team-americas-data-recovery-script

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/16/esxiargs_ransomware_variant_cisa/){:target="_blank" rel="noopener"}

> Want a clue to what you’re dealing with? Check the ransom note That didn't take long.... [...]
