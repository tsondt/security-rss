Title: LockBit brags it pumped ION full of ransomware
Date: 2023-02-03T07:30:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-03-lockbit-brags-it-pumped-ion-full-of-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/03/ion_ransomware_attack/){:target="_blank" rel="noopener"}

> Crims put a February 4 deadline for software slinger to pay up UK regulators are investigating a cyberattack against financial technology firm ION, while the LockBit ransomware gang has threatened to publish the stolen data on February 4 if the software provider doesn't pay up.... [...]
