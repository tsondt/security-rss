Title: Cloudflare blocks record-breaking 71 million RPS DDoS attack
Date: 2023-02-13T14:50:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-13-cloudflare-blocks-record-breaking-71-million-rps-ddos-attack

[Source](https://www.bleepingcomputer.com/news/security/cloudflare-blocks-record-breaking-71-million-rps-ddos-attack/){:target="_blank" rel="noopener"}

> This weekend, Cloudflare blocked what it describes as the largest volumetric distributed denial-of-service (DDoS) attack to date. [...]
