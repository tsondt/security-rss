Title: Google destroyed evidence for antitrust battle, Feds complain
Date: 2023-02-24T22:30:14+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-02-24-google-destroyed-evidence-for-antitrust-battle-feds-complain

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/24/feds_google_antitrust_communication/){:target="_blank" rel="noopener"}

> rm -rf'ing staff chat logs can't go unpunished, says Uncle Sam The US Department of Justice (DoJ) asked the judge hearing its antitrust case against Google to sanction the search advertising giant for destruction of evidence.... [...]
