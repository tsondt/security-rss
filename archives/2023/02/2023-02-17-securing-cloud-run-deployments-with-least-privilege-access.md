Title: Securing Cloud Run Deployments with Least Privilege Access
Date: 2023-02-17T17:00:00+00:00
Author: Victor Dantas
Category: GCP Security
Tags: Serverless;Developers & Practitioners;Identity & Security
Slug: 2023-02-17-securing-cloud-run-deployments-with-least-privilege-access

[Source](https://cloud.google.com/blog/products/identity-security/securing-cloud-run-deployments-with-least-privilege-access/){:target="_blank" rel="noopener"}

> With Cloud Run, developers can quickly deploy production web applications and APIs on a serverless environment that runs on top of Google’s scalable infrastructure. While development teams can leverage Cloud Run to improve development agility and iterate quickly, many overlook their infrastructure’s security posture. In particular, one aspect of security that does not get enough attention is access management and the principle of least privilege. The principle of least privilege states that a resource should only have access to the exact resources it needs in order to function. This principle was developed to address the risk of compromised identities granting [...]
