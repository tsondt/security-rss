Title: Google: Gmail client-side encryption now publicly available
Date: 2023-02-28T11:20:18-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google;Security
Slug: 2023-02-28-google-gmail-client-side-encryption-now-publicly-available

[Source](https://www.bleepingcomputer.com/news/security/google-gmail-client-side-encryption-now-publicly-available/){:target="_blank" rel="noopener"}

> Gmail client-side encryption (CSE) is now generally available for Google Workspace Enterprise Plus, Education Plus, and Education Standard customers. [...]
