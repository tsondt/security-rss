Title: Hyundai, Kia patch bug allowing car thefts with a USB cable
Date: 2023-02-15T13:11:25-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Technology
Slug: 2023-02-15-hyundai-kia-patch-bug-allowing-car-thefts-with-a-usb-cable

[Source](https://www.bleepingcomputer.com/news/security/hyundai-kia-patch-bug-allowing-car-thefts-with-a-usb-cable/){:target="_blank" rel="noopener"}

> Automakers Hyundai and KIA are rolling out an emergency software update on several of their car models impacted by an easy hack that makes it possible to steal them. [...]
