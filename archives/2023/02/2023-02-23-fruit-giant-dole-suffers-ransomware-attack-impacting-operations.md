Title: Fruit giant Dole suffers ransomware attack impacting operations
Date: 2023-02-23T10:00:53-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-23-fruit-giant-dole-suffers-ransomware-attack-impacting-operations

[Source](https://www.bleepingcomputer.com/news/security/fruit-giant-dole-suffers-ransomware-attack-impacting-operations/){:target="_blank" rel="noopener"}

> Dole Food Company, one of the world's largest producers and distributors of fresh fruit and vegetables, has announced that it is dealing with a ransomware attack that impacted its operations. [...]
