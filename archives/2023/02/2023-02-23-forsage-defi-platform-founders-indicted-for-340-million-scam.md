Title: Forsage DeFi platform founders indicted for $340 million scam
Date: 2023-02-23T15:14:22-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-02-23-forsage-defi-platform-founders-indicted-for-340-million-scam

[Source](https://www.bleepingcomputer.com/news/security/forsage-defi-platform-founders-indicted-for-340-million-scam/){:target="_blank" rel="noopener"}

> A Federal grand jury in the District of Oregon has indicted four Russian nationals founders of Forsage decentralized finance (DeFi) cryptocurrency investment platform for allegedly running a global Ponzi and pyramid scheme that raised $340 million. [...]
