Title: Norwegian police recover $5.8M crypto from massive Axie Infinity hack
Date: 2023-02-17T12:19:21-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-02-17-norwegian-police-recover-58m-crypto-from-massive-axie-infinity-hack

[Source](https://www.bleepingcomputer.com/news/security/norwegian-police-recover-58m-crypto-from-massive-axie-infinity-hack/){:target="_blank" rel="noopener"}

> Norwegian police (Økokrim) have seized 60 million kroner ($5,800,000) worth of cryptocurrency stolen by the North Korean Lazarus hacking group last year from Axie Infinity's Ronin Bridge. [...]
