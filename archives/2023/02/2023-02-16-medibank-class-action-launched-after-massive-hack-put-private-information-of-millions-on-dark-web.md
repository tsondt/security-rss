Title: Medibank class action launched after massive hack put private information of millions on dark web
Date: 2023-02-16T01:42:36+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Medibank;Cybercrime;Privacy;Data and computer security;Australia news;Law (Australia)
Slug: 2023-02-16-medibank-class-action-launched-after-massive-hack-put-private-information-of-millions-on-dark-web

[Source](https://www.theguardian.com/australia-news/2023/feb/16/medibank-class-action-launched-data-breach-private-information-dark-web){:target="_blank" rel="noopener"}

> Law firm Baker McKenzie says company failed to protect privacy of customers in Australia and overseas Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast The law firm Baker McKenzie has launched a class action lawsuit against Medibank over the health insurer’s massive cyber attack last year that resulted in the personal details of up to 10 million customers being posted on the dark web. In what became the largest breach of its kind to date in Australia, the hack on Medibank resulted in the personal [...]
