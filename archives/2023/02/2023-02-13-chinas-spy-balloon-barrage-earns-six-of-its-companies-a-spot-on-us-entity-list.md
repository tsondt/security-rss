Title: China's spy balloon barrage earns six of its companies a spot on US entity list
Date: 2023-02-13T06:28:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-02-13-chinas-spy-balloon-barrage-earns-six-of-its-companies-a-spot-on-us-entity-list

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/13/chinese_balloon_tech_companies_banned/){:target="_blank" rel="noopener"}

> US Commerce Department can't just let red balloons go by The US Department of Commerce added six more entities to its blacklist on Friday on grounds of national security after an errant Chinese surveillance balloon was shot down over the US last week.... [...]
