Title: Fortinet fixes critical RCE flaws in FortiNAC and FortiWeb
Date: 2023-02-17T09:13:14-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-17-fortinet-fixes-critical-rce-flaws-in-fortinac-and-fortiweb

[Source](https://www.bleepingcomputer.com/news/security/fortinet-fixes-critical-rce-flaws-in-fortinac-and-fortiweb/){:target="_blank" rel="noopener"}

> Cybersecurity solutions company Fortinet has released security updates for its FortiNAC and FortiWeb products, addressing two critical-severity vulnerabilities that may allow unauthenticated attackers to perform arbitrary code or command execution. [...]
