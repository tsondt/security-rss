Title: News Corp outfoxed by IT intruders for years
Date: 2023-02-28T08:31:14+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-02-28-news-corp-outfoxed-by-it-intruders-for-years

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/28/news_corp_dwell_time_breach/){:target="_blank" rel="noopener"}

> All the news that's fit to pwn The miscreants who infiltrated News Corporation's corporate IT network spent two years in the media monolith's system before being detected early last year.... [...]
