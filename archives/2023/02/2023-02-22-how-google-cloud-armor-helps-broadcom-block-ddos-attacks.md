Title: How Google Cloud Armor helps Broadcom block DDoS Attacks
Date: 2023-02-22T17:00:00+00:00
Author: Ariel Peretz
Category: GCP Security
Tags: Identity & Security
Slug: 2023-02-22-how-google-cloud-armor-helps-broadcom-block-ddos-attacks

[Source](https://cloud.google.com/blog/products/identity-security/how-google-cloud-armor-helps-broadcom-block-ddos-attacks/){:target="_blank" rel="noopener"}

> Technology leader Broadcom is a worldwide provider of enterprise security solutions that leverages its expertise in hardware and software to offer a broad portfolio of embedded security solutions, including integrated Symantec cybersecurity software. In 2021, Broadcom received the Google Cloud DevOps Award. As Broadcom migrated its enterprise security solution infrastructure from Amazon Web Services to Google Cloud, defending the environment’s network security infrastructure remained a top priority. In addition to keeping customer environments secure, Broadcom required a robust web application firewall with anti-DDoS capabilities to help meet compliance requirements − such as FedRAMP for their public sector customers. Google Cloud [...]
