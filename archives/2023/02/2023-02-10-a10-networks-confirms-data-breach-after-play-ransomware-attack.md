Title: A10 Networks confirms data breach after Play ransomware attack
Date: 2023-02-10T15:30:15-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-10-a10-networks-confirms-data-breach-after-play-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/a10-networks-confirms-data-breach-after-play-ransomware-attack/){:target="_blank" rel="noopener"}

> The California-based networking hardware manufacturer 'A10 Networks' has confirmed to BleepingComputer that the Play ransomware gang briefly gained access to its IT infrastructure and compromised data. [...]
