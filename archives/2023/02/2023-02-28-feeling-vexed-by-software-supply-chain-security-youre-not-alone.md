Title: Feeling VEXed by software supply chain security? You’re not alone
Date: 2023-02-28T01:01:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-28-feeling-vexed-by-software-supply-chain-security-youre-not-alone

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/28/supply_chain_security_chainguard/){:target="_blank" rel="noopener"}

> Chainguard CEO explains how to secure code given crims know to poison it at the source SCSW The vast majority of off-the-shelf software is composed of imported components, whether that's open source libraries or proprietary code. And that spells a security danger: if someone can subvert one of those components, they can infiltrate every installation of applications using those dependencies.... [...]
