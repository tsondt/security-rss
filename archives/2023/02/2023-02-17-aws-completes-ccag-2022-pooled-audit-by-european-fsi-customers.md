Title: AWS completes CCAG 2022 pooled audit by European FSI customers
Date: 2023-02-17T18:44:16+00:00
Author: Manuel Mazarredo
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;audit;CCAG;Security;Security Blog
Slug: 2023-02-17-aws-completes-ccag-2022-pooled-audit-by-european-fsi-customers

[Source](https://aws.amazon.com/blogs/security/aws-completes-ccag-2022-pooled-audit-by-european-fsi-customers/){:target="_blank" rel="noopener"}

> We are excited to announce that Amazon Web Services (AWS) has completed its annual Collaborative Cloud Audit Group (CCAG) Cloud Community audit with European financial service institutions (FSIs). Security at AWS is the highest priority. As customers embrace the scalability and flexibility of AWS, we are helping them evolve security, identity, and compliance into key [...]
