Title: Apple splats zero-day bug, other gremlins in macOS, iOS
Date: 2023-02-15T05:27:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-02-15-apple-splats-zero-day-bug-other-gremlins-in-macos-ios

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/15/apple_patches_zeroday_vulnerability/){:target="_blank" rel="noopener"}

> WebKit flaw 'may have been exploited' – just like Tim Cook 'may have' made a million bucks this week Apple this week released bug-splatting updates to its operating systems and Safari browser, to fix a zero-day vulnerability in its WebKit browser engine that's reported to have been actively exploited.... [...]
