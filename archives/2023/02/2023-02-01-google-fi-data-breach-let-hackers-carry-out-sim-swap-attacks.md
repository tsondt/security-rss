Title: Google Fi data breach let hackers carry out SIM swap attacks
Date: 2023-02-01T15:43:12-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2023-02-01-google-fi-data-breach-let-hackers-carry-out-sim-swap-attacks

[Source](https://www.bleepingcomputer.com/news/security/google-fi-data-breach-let-hackers-carry-out-sim-swap-attacks/){:target="_blank" rel="noopener"}

> Google Fi, Google's U.S.-only telecommunications and mobile internet service, has informed customers that personal data was exposed by a data breach at one of its primary network providers, with some customers warned that it allowed SIM swapping attacks. [...]
