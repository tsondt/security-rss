Title: Over 1,800 Android phishing forms for sale on cybercrime market
Date: 2023-02-01T17:30:45-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-02-01-over-1800-android-phishing-forms-for-sale-on-cybercrime-market

[Source](https://www.bleepingcomputer.com/news/security/over-1-800-android-phishing-forms-for-sale-on-cybercrime-market/){:target="_blank" rel="noopener"}

> A threat actor named InTheBox is promoting on Russian cybercrime forums an inventory of 1,894 web injects (overlays of phishing windows) for stealing credentials and sensitive data from banking, cryptocurrency exchange, and e-commerce apps [...]
