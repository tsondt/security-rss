Title: Friday Squid Blogging: Squid Processing Facility
Date: 2023-02-24T22:02:38+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid;video
Slug: 2023-02-24-friday-squid-blogging-squid-processing-facility

[Source](https://www.schneier.com/blog/archives/2023/02/friday-squid-blogging-squid-processing-facility.html){:target="_blank" rel="noopener"}

> This video of a modern large squid processing ship is a bit gory, but also interesting. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
