Title: Cloud CISO Perspectives: January 2023
Date: 2023-02-02T17:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Cloud CISO;Identity & Security
Slug: 2023-02-02-cloud-ciso-perspectives-january-2023

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-january-2023/){:target="_blank" rel="noopener"}

> Welcome to January’s Cloud CISO Perspectives. This month, we’re going to catch up with a few of the cloud security megatrends that I described a year ago, and see how they and the cloud security landscape has evolved. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. Checking in on two megatrends In January 2022, I described eight security “megatrends” that drive technological innovation and improve the overall security posture of cloud [...]
