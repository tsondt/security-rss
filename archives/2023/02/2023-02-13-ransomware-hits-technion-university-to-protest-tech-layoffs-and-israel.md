Title: Ransomware hits Technion university to protest tech layoffs and Israel
Date: 2023-02-13T00:06:25-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-02-13-ransomware-hits-technion-university-to-protest-tech-layoffs-and-israel

[Source](https://www.bleepingcomputer.com/news/security/ransomware-hits-technion-university-to-protest-tech-layoffs-and-israel/){:target="_blank" rel="noopener"}

> A new ransomware group going by the name 'DarkBit' has hit Technion - Israel Institute of Technology, one of Israel's leading research universities. The ransom note posted by DarkBit is littered with messaging protesting tech layoffs and promoting anti-Israel rhetoric, as well as the group demanding a $1.7 million payment. [...]
