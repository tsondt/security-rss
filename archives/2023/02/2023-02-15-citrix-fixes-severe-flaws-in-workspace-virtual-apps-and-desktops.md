Title: Citrix fixes severe flaws in Workspace, Virtual Apps and Desktops
Date: 2023-02-15T13:38:20-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-15-citrix-fixes-severe-flaws-in-workspace-virtual-apps-and-desktops

[Source](https://www.bleepingcomputer.com/news/security/citrix-fixes-severe-flaws-in-workspace-virtual-apps-and-desktops/){:target="_blank" rel="noopener"}

> Citrix Systems has released security updates for vulnerabilities in its Virtual Apps and Desktops, and Workspace Apps products. [...]
