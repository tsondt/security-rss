Title: City of Oakland systems offline after ransomware attack
Date: 2023-02-10T17:04:52-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-10-city-of-oakland-systems-offline-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/city-of-oakland-systems-offline-after-ransomware-attack/){:target="_blank" rel="noopener"}

> The City of Oakland was hit by a ransomware attack on Wednesday night that forced it to take all systems offline until the network is secured and affected services are brought back online. [...]
