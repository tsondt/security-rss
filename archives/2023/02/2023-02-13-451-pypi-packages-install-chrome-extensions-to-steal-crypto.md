Title: 451 PyPI packages install Chrome extensions to steal crypto
Date: 2023-02-13T14:46:46-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-02-13-451-pypi-packages-install-chrome-extensions-to-steal-crypto

[Source](https://www.bleepingcomputer.com/news/security/451-pypi-packages-install-chrome-extensions-to-steal-crypto/){:target="_blank" rel="noopener"}

> Over 450 malicious PyPI python packages were found installing malicious Chromium browser extensions to hijack cryptocurrency transactions made through browser-based crypto wallets and websites. [...]
