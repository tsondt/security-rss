Title: US teases more China tech sanctions, this time to deflate balloon-makers
Date: 2023-02-10T06:31:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-02-10-us-teases-more-china-tech-sanctions-this-time-to-deflate-balloon-makers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/10/china_surveillance_balloon_sanctions/){:target="_blank" rel="noopener"}

> State Dept already has one target, FBI is identifying sources of floating surveillance platform's components The Chinese surveillance balloon that drifted across the US last week looks set to spark a new round of sanctions against Middle Kingdom tech firms.... [...]
