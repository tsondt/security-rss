Title: Attacking Machine Learning Systems
Date: 2023-02-06T11:02:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptography;cyberattack;cybersecurity;essays;machine learning;vulnerabilities
Slug: 2023-02-06-attacking-machine-learning-systems

[Source](https://www.schneier.com/blog/archives/2023/02/attacking-machine-learning-systems.html){:target="_blank" rel="noopener"}

> The field of machine learning (ML) security—and corresponding adversarial ML—is rapidly advancing as researchers develop sophisticated techniques to perturb, disrupt, or steal the ML model or data. It’s a heady time; because we know so little about the security of these systems, there are many opportunities for new researchers to publish in this field. In many ways, this circumstance reminds me of the cryptanalysis field in the 1990. And there is a lesson in that similarity: the complex mathematical attacks make for good academic papers, but we mustn’t lose sight of the fact that insecure software will be the likely [...]
