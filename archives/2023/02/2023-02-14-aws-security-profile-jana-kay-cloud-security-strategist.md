Title: AWS Security Profile: Jana Kay, Cloud Security Strategist
Date: 2023-02-14T21:33:41+00:00
Author: Roger Park
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Thought Leadership;Security Blog
Slug: 2023-02-14-aws-security-profile-jana-kay-cloud-security-strategist

[Source](https://aws.amazon.com/blogs/security/aws-security-profile-jana-kay-cloud-security-strategist/){:target="_blank" rel="noopener"}

> In the AWS Security Profile series, we interview Amazon Web Services (AWS) thought leaders who help keep our customers safe and secure. This interview features Jana Kay, Cloud Security Strategist. Jana shares her unique career journey, insights on the Security and Resiliency of the Cloud Tabletop Exercise (TTX) program, thoughts on the data protection and [...]
