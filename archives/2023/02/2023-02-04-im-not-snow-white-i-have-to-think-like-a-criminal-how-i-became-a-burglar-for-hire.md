Title: ‘I’m not Snow White. I have to think like a criminal’: how I became a burglar for hire
Date: 2023-02-04T11:00:30+00:00
Author: Sam Wollaston
Category: The Guardian
Tags: Autobiography and memoir;Business;London;Life and style;Work & careers;Books;Data and computer security;Hacking;Crime;Culture;UK security and counter-terrorism;UK news;Commercial property
Slug: 2023-02-04-im-not-snow-white-i-have-to-think-like-a-criminal-how-i-became-a-burglar-for-hire

[Source](https://www.theguardian.com/books/2023/feb/04/im-not-snow-white-i-have-to-think-like-a-criminal-how-i-became-a-burglar-for-hire){:target="_blank" rel="noopener"}

> Jenny Radcliffe is a professional ‘people hacker’ – someone who claims she can get past anyone and get in anywhere. No building is secure. How does she do it? Plus, an extract from her memoir ‘Do I look like someone to mess with?” says Jenny Radcliffe, folding her arms in a really-don’t-mess-with-me kind of way. Her tattoos seem to be making the point, too. On her left forearm is a Latin phrase – facta non verba, actions not words – with a pair of devil’s horns; on her right, a feather, from the wings of an angel. Which is she, [...]
