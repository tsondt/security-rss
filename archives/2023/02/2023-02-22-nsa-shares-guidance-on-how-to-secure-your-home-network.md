Title: NSA shares guidance on how to secure your home network
Date: 2023-02-22T16:40:23-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-22-nsa-shares-guidance-on-how-to-secure-your-home-network

[Source](https://www.bleepingcomputer.com/news/security/nsa-shares-guidance-on-how-to-secure-your-home-network/){:target="_blank" rel="noopener"}

> The U.S. National Security Agency (NSA) has issued guidance to help remote workers secure their home networks and defend their devices from attacks. [...]
