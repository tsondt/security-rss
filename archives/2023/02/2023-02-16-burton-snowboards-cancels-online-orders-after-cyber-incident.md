Title: Burton Snowboards cancels online orders after 'cyber incident'
Date: 2023-02-16T12:57:19-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-16-burton-snowboards-cancels-online-orders-after-cyber-incident

[Source](https://www.bleepingcomputer.com/news/security/burton-snowboards-cancels-online-orders-after-cyber-incident/){:target="_blank" rel="noopener"}

> Burton Snowboards, a leading snowboard manufacturing company, has canceled all online orders today following what it describes as a "cyber incident." [...]
