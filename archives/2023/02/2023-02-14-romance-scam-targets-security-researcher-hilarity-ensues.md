Title: Romance scam targets security researcher, hilarity ensues
Date: 2023-02-14T02:30:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-14-romance-scam-targets-security-researcher-hilarity-ensues

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/14/romance_scam_sophos/){:target="_blank" rel="noopener"}

> Happy Valentine's Day! Now don't get fooled It sounds like the plot of a somewhat far-fetched romcom-slash-thriller Netflix series, maybe billed as You meets Your Place or Mine, dropping just in time for Valentine's Day.... [...]
