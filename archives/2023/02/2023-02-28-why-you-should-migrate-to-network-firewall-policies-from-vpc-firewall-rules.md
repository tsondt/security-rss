Title: Why you should migrate to network firewall policies from VPC Firewall rules
Date: 2023-02-28T17:00:00+00:00
Author: Faye Feng
Category: GCP Security
Tags: Application Development;Identity & Security
Slug: 2023-02-28-why-you-should-migrate-to-network-firewall-policies-from-vpc-firewall-rules

[Source](https://cloud.google.com/blog/products/identity-security/best-practices-for-migrating-to-network-firewall-policies/){:target="_blank" rel="noopener"}

> In the fall of 2022, we announced new policy constructs for Google Cloud Firewall, a scalable, cloud-first firewall service that helps secure traffic flow to and from workloads in Google Cloud, and whose distributed architecture enables simplified, granular control including micro-segmentation. Whereas legacy VPC firewall rules included network tags that were not governed by our IAM infrastructure, Cloud Firewall’s new network firewall policies with IAM-governed tags conform to our hierarchical control model and can help improve security operations. We recommend that customers migrate from VPC firewall rules to the newly introduced network firewall policies. To assist with the migration, we [...]
