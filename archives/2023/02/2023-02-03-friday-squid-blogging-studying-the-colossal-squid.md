Title: Friday Squid Blogging: Studying the Colossal Squid
Date: 2023-02-03T22:02:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-02-03-friday-squid-blogging-studying-the-colossal-squid

[Source](https://www.schneier.com/blog/archives/2023/02/friday-squid-blogging-studying-the-colossal-squid.html){:target="_blank" rel="noopener"}

> A survey of giant squid science. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
