Title: U.S., U.K. Sanction 7 Men Tied to Trickbot Hacking Group
Date: 2023-02-09T20:23:58+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Ransomware;Adam Meyers;Christina Svechinskaya;CrowdStrike;Dyre;Group-IB;Ilya Sachkov;Kaspersky Lab;Ruslan Stoyanov;Sergey Mikhaylov;trickbot;U.S. Department of Treasury;Vitaly "Bentley" Kovalev;Wizard Spider
Slug: 2023-02-09-us-uk-sanction-7-men-tied-to-trickbot-hacking-group

[Source](https://krebsonsecurity.com/2023/02/u-s-u-k-sanction-7-men-tied-to-trickbot-hacking-group/){:target="_blank" rel="noopener"}

> Authorities in the United States and United Kingdom today levied financial sanctions against seven men accused of operating “ Trickbot,” a cybercrime-as-a-service platform based in Russia that has enabled countless ransomware attacks and bank account takeovers since its debut in 2016. The U.S. Department of the Treasury says the Trickbot group is associated with Russian intelligence services, and that this alliance led to the targeting of many U.S. companies and government entities. Initially a stealthy trojan horse program delivered via email and used to steal passwords, Trickbot evolved into “a highly modular malware suite that provides the Trickbot Group with [...]
