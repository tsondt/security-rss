Title: North Korean hackers stole research data in two-month-long breach
Date: 2023-02-02T12:56:58-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-02-north-korean-hackers-stole-research-data-in-two-month-long-breach

[Source](https://www.bleepingcomputer.com/news/security/north-korean-hackers-stole-research-data-in-two-month-long-breach/){:target="_blank" rel="noopener"}

> A new cyber espionage campaign dubbed 'No Pineapple!' has been attributed to the North Korean Lazarus hacking group, allowing the threat actors to stealthily steal 100GB of data from the victim without causing any destruction. [...]
