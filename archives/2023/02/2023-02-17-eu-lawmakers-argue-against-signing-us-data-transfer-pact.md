Title: EU lawmakers argue against signing US data-transfer pact
Date: 2023-02-17T09:30:07+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-02-17-eu-lawmakers-argue-against-signing-us-data-transfer-pact

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/17/adequacy_decision_us_data_transfer/){:target="_blank" rel="noopener"}

> Committee: Something about complaints process being dealt with in total secrecy doesn't sit right Lawmakers in the European Parliament have urged the European Commission not to issue the "adequacy decision" needed for the EU-US Data Privacy Framework (DPF) to officially become the pipeline for data to freely flow from the EU to the States.... [...]
