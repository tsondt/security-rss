Title: Dish Network confirms ransomware attack behind multi-day outage
Date: 2023-02-28T12:24:55-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-28-dish-network-confirms-ransomware-attack-behind-multi-day-outage

[Source](https://www.bleepingcomputer.com/news/security/dish-network-confirms-ransomware-attack-behind-multi-day-outage/){:target="_blank" rel="noopener"}

> Satellite broadcast provider and TV giant Dish Network has finally confirmed that a ransomware attack was the cause of a multi-day network and service outage that started on Friday. [...]
