Title: GoDaddy joins the dots and realizes it's been under attack for three years
Date: 2023-02-20T02:27:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-02-20-godaddy-joins-the-dots-and-realizes-its-been-under-attack-for-three-years

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/20/in_brief_security/){:target="_blank" rel="noopener"}

> Also: Russia may legalize hacking; Oakland declares ransomware emergency; the CVEs you should know about this week In brief Web hosting and domain name concern GoDaddy has disclosed a fresh attack on its infrastructure, and concluded that it is one of a series of linked incidents dating back to 2020.... [...]
