Title: China makes it even harder for data to leave its shores
Date: 2023-02-27T13:30:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-02-27-china-makes-it-even-harder-for-data-to-leave-its-shores

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/27/china_data_regulatory_intervention/){:target="_blank" rel="noopener"}

> Many foreign companies had already given up – now there's more red tape Starting in June, companies operating in China must undergo a regulatory intervention when sending data abroad, thanks to the Cyberspace Administration of China (CAC).... [...]
