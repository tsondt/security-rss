Title: Google Cloud to join Catena-X and help build a sovereign data ecosystem in the automotive industry
Date: 2023-02-06T17:00:00+00:00
Author: Matthias Breunig
Category: GCP Security
Tags: Partners;Identity & Security;Manufacturing
Slug: 2023-02-06-google-cloud-to-join-catena-x-and-help-build-a-sovereign-data-ecosystem-in-the-automotive-industry

[Source](https://cloud.google.com/blog/topics/manufacturing/google-cloud-joins-catena-x-to-build-sovereign-data-ecosystem/){:target="_blank" rel="noopener"}

> The automotive industry has a long history of being at the forefront of automation and digitalization in many areas, be it digital design or digital shop floor. In parallel, carmakers have grown into broad partnerships within their ecosystem, with a special focus on their suppliers in the value chain. Digitizing and automating the way partners across the value chain work with each other and establishing a more advanced way of exchanging data based on digital sovereignty and open standards is an obvious evolution for this industry. This is the pioneering effort driven by Catena-X. Catena-X is building a trustworthy, collaborative, [...]
