Title: Top 2022 AWS data protection service and cryptography tool launches
Date: 2023-02-21T17:57:31+00:00
Author: Marta Taggart
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;2022;ACM;Amazon Macie;AWS Certificate Manager;AWS Clean Rooms;AWS Key Management System;AWS KMS;AWS Nitro System;AWS Private CA;AWS Private Certificate Authority;AWS Secrets Manager;Encryption;Open source;Security Blog;Service Launches
Slug: 2023-02-21-top-2022-aws-data-protection-service-and-cryptography-tool-launches

[Source](https://aws.amazon.com/blogs/security/top-2022-aws-data-protection-service-and-cryptography-tool-launches/){:target="_blank" rel="noopener"}

> Given the pace of Amazon Web Services (AWS) innovation, it can be challenging to stay up to date on the latest AWS service and feature launches. AWS provides services and tools to help you protect your data, accounts, and workloads from unauthorized access. AWS data protection services provide encryption capabilities, key management, and sensitive data [...]
