Title: Stanford University discloses data breach affecting PhD applicants
Date: 2023-02-24T11:27:59-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-24-stanford-university-discloses-data-breach-affecting-phd-applicants

[Source](https://www.bleepingcomputer.com/news/security/stanford-university-discloses-data-breach-affecting-phd-applicants/){:target="_blank" rel="noopener"}

> Stanford University disclosed a data breach after files containing Economics Ph.D. program admission information were downloaded from its website between December 2022 and January 2023. [...]
