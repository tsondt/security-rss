Title: Iran crew stole Charlie Hebdo database, says Microsoft
Date: 2023-02-04T08:45:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-04-iran-crew-stole-charlie-hebdo-database-says-microsoft

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/04/microsoft_iran_charlie_hebdo_hack/){:target="_blank" rel="noopener"}

> Same gang pestered US voters during 2020 presidential election Microsoft believes the gang who boasted it had stolen and leaked more than 200,000 Charlie Hebdo subscribers' personal information is none other than a Tehran-backed criminal group.... [...]
