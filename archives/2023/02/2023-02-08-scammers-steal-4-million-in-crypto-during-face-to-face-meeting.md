Title: Scammers steal $4 million in crypto during face-to-face meeting
Date: 2023-02-08T13:30:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-02-08-scammers-steal-4-million-in-crypto-during-face-to-face-meeting

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/08/webaverse_crypto_stolen/){:target="_blank" rel="noopener"}

> Demand to display wallet full of coin facilitated mystery heist Ahad Shams, the co-founder of Web3 metaverse gaming engine startup Webaverse, discovered in late November 2022 that someone had stolen $4 million of his cryptocurrency – during a real world interaction.... [...]
