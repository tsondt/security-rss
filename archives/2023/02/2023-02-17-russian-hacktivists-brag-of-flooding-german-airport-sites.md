Title: 'Russian hacktivists' brag of flooding German airport sites
Date: 2023-02-17T18:30:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-17-russian-hacktivists-brag-of-flooding-german-airport-sites

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/17/german_airport_websites_ddos/){:target="_blank" rel="noopener"}

> In other words, script kiddies up to shenanigans again A series of distributed denial-of-service (DDoS) attacks shut down seven German airports' websites on Thursday, a day after a major IT glitch at Lufthansa grounded flights.... [...]
