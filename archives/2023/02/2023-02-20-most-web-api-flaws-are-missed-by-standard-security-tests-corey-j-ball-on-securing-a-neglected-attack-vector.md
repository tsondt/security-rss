Title: ‘Most web API flaws are missed by standard security tests’ – Corey J Ball on securing a neglected attack vector
Date: 2023-02-20T13:58:59+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-02-20-most-web-api-flaws-are-missed-by-standard-security-tests-corey-j-ball-on-securing-a-neglected-attack-vector

[Source](https://portswigger.net/daily-swig/most-web-api-flaws-are-missed-by-standard-security-tests-corey-j-ball-on-securing-a-neglected-attack-vector){:target="_blank" rel="noopener"}

> API security is a ‘great gateway’ into a pen testing career, advises specialist in the field [...]
