Title: Rishi Sunak faces calls to ban TikTok use by government officials
Date: 2023-02-24T17:40:05+00:00
Author: Aletha Adu Political correspondent
Category: The Guardian
Tags: TikTok;Rishi Sunak;Conservatives;Data protection;Data and computer security;Technology;Social media;Internet;Politics;UK news
Slug: 2023-02-24-rishi-sunak-faces-calls-to-ban-tiktok-use-by-government-officials

[Source](https://www.theguardian.com/technology/2023/feb/24/rishi-sunak-tiktok-cybersecurity-uk-government-officials-china){:target="_blank" rel="noopener"}

> PM under pressure to follow EU and US in taking step over fears Chinese-owned app poses cybersecurity risk Rishi Sunak has been urged to ban government officials from using TikTok in line with moves by the EU and US, amid growing cybersecurity fears over China. Officials in Europe and the US have been told to limit the use of the Chinese-owned social video app over concerns that data can be accessed by Beijing. Continue reading... [...]
