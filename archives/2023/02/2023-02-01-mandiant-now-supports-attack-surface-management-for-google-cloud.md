Title: Mandiant now supports Attack Surface Management for Google Cloud
Date: 2023-02-01T17:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Identity & Security
Slug: 2023-02-01-mandiant-now-supports-attack-surface-management-for-google-cloud

[Source](https://cloud.google.com/blog/products/identity-security/mandiants-attack-surface-management-now-works-with-google-cloud/){:target="_blank" rel="noopener"}

> When we closed the Mandiant acquisition in September 2022, we set the expectation that we’d be investing heavily in cybersecurity offerings that can help customers mitigate risk. In the short time since our two companies came together, we’ve aimed to do just that. Today, we are announcing Mandiant Attack Surface Management for Google Cloud, which can enable customers to centralize visibility into cloud-hosted external assets. Attack Surface Management can deliver an adversary’s view of your organization’s attack surface, and can help discover external assets, identify business relationships, and actively check for exploitable weaknesses. Customers can automatically pull external-facing assets hosted [...]
