Title: Money Lover for Android & iOS leaked email addresses, transactions
Date: 2023-02-08T10:57:19-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-02-08-money-lover-for-android-ios-leaked-email-addresses-transactions

[Source](https://www.bleepingcomputer.com/news/security/money-lover-for-android-and-ios-leaked-email-addresses-transactions/){:target="_blank" rel="noopener"}

> A flaw in the Money Lover financial app for Android, iOS, and Windows allowed any logged-in member to see the email addresses and live transaction metadata for other users' shared wallets. [...]
