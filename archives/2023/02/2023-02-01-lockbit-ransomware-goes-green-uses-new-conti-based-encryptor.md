Title: LockBit ransomware goes 'Green,' uses new Conti-based encryptor
Date: 2023-02-01T17:48:11-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-02-01-lockbit-ransomware-goes-green-uses-new-conti-based-encryptor

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-goes-green-uses-new-conti-based-encryptor/){:target="_blank" rel="noopener"}

> The LockBit ransomware gang has again started using encryptors based on other operations, this time switching to one based on the leaked source code for the Conti ransomware. [...]
