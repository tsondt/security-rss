Title: Hacking the Tax Code
Date: 2023-02-10T11:24:37+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;economics of security;hacking;laws;loopholes
Slug: 2023-02-10-hacking-the-tax-code

[Source](https://www.schneier.com/blog/archives/2023/02/hacking-the-tax-code.html){:target="_blank" rel="noopener"}

> The tax code isn’t software. It doesn’t run on a computer. But it’s still code. It’s a series of algorithms that takes an input—financial information for the year—and produces an output: the amount of tax owed. It’s incredibly complex code; there are a bazillion details and exceptions and special cases. It consists of government laws, rulings from the tax authorities, judicial decisions, and legal opinions. Like computer code, the tax code has bugs. They might be mistakes in how the tax laws were written. They might be mistakes in how the tax code is interpreted, oversights in how parts of [...]
