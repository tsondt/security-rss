Title: Ransomware attack on ION Group impacts derivatives trading market
Date: 2023-02-02T09:13:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-02-ransomware-attack-on-ion-group-impacts-derivatives-trading-market

[Source](https://www.bleepingcomputer.com/news/security/ransomware-attack-on-ion-group-impacts-derivatives-trading-market/){:target="_blank" rel="noopener"}

> The LockBit ransomware gang has claimed responsibility for the cyberattack on ION Group, a UK-based software company whose products are used by financial institutions, banks, and corporations for trading, investment management, and market analytics. [...]
