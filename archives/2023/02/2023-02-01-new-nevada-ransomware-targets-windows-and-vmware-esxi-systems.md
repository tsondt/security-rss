Title: New Nevada Ransomware targets Windows and VMware ESXi systems
Date: 2023-02-01T14:26:36-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-01-new-nevada-ransomware-targets-windows-and-vmware-esxi-systems

[Source](https://www.bleepingcomputer.com/news/security/new-nevada-ransomware-targets-windows-and-vmware-esxi-systems/){:target="_blank" rel="noopener"}

> A relatively new ransomware operation known as Nevada seems to grow its capabilities quickly as security researchers noticed improved functionality for the locker targeting Windows and VMware ESXi systems. [...]
