Title: SonicWall warns web content filtering is broken on Windows 11 22H2
Date: 2023-02-08T17:57:11-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-08-sonicwall-warns-web-content-filtering-is-broken-on-windows-11-22h2

[Source](https://www.bleepingcomputer.com/news/security/sonicwall-warns-web-content-filtering-is-broken-on-windows-11-22h2/){:target="_blank" rel="noopener"}

> Security hardware manufacturer SonicWall warned customers today of what it describes as a "limitation" of the web content filtering (WCF) feature on Windows 11, version 22H2 systems. [...]
