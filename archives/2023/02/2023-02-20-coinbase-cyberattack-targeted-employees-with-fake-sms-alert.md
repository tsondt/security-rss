Title: Coinbase cyberattack targeted employees with fake SMS alert
Date: 2023-02-20T07:52:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-20-coinbase-cyberattack-targeted-employees-with-fake-sms-alert

[Source](https://www.bleepingcomputer.com/news/security/coinbase-cyberattack-targeted-employees-with-fake-sms-alert/){:target="_blank" rel="noopener"}

> Coinbase cryptocurrency exchange platform has disclosed that an unknown threat actor stole the login credentials of one of its employees in an attempt to gain remote access to the company's systems. [...]
