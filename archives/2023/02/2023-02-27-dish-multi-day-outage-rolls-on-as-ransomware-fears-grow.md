Title: Dish multi-day outage rolls on as ransomware fears grow
Date: 2023-02-27T20:30:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-27-dish-multi-day-outage-rolls-on-as-ransomware-fears-grow

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/27/dish_outage_ransomware/){:target="_blank" rel="noopener"}

> Techies 'hard at work' and all of that US telco Dish said it is investigating a multi-day network "issue" that knocked some of its systems offline, leaving customers stranded from the web.... [...]
