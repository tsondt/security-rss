Title: US NIST unveils winning encryption algorithm for IoT data protection
Date: 2023-02-08T14:45:10-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-08-us-nist-unveils-winning-encryption-algorithm-for-iot-data-protection

[Source](https://www.bleepingcomputer.com/news/security/us-nist-unveils-winning-encryption-algorithm-for-iot-data-protection/){:target="_blank" rel="noopener"}

> The National Institute of Standards and Technology (NIST) announced that ASCON is the winning bid for the "lightweight cryptography" program to find the best algorithm to protect small IoT (Internet of Things) devices with limited hardware resources. [...]
