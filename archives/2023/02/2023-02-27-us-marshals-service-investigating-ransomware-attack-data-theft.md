Title: U.S. Marshals Service investigating ransomware attack, data theft
Date: 2023-02-27T19:48:28-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-27-us-marshals-service-investigating-ransomware-attack-data-theft

[Source](https://www.bleepingcomputer.com/news/security/us-marshals-service-investigating-ransomware-attack-data-theft/){:target="_blank" rel="noopener"}

> The U.S. Marshals Service (USMS) is investigating the theft of sensitive law enforcement information following a ransomware attack that has impacted what it describes as "a stand-alone USMS system." [...]
