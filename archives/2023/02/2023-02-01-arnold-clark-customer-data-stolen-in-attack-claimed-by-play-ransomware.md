Title: Arnold Clark customer data stolen in attack claimed by Play ransomware
Date: 2023-02-01T13:38:40-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-01-arnold-clark-customer-data-stolen-in-attack-claimed-by-play-ransomware

[Source](https://www.bleepingcomputer.com/news/security/arnold-clark-customer-data-stolen-in-attack-claimed-by-play-ransomware/){:target="_blank" rel="noopener"}

> Arnold Clark, self-described as Europe's largest independent car retailer, is notifying some customers that their personal information has been stolen in a December 23 cyberattack claimed by the Play ransomware group. [...]
