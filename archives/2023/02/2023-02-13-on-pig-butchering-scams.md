Title: On Pig Butchering Scams
Date: 2023-02-13T19:54:04+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;fraud;scams
Slug: 2023-02-13-on-pig-butchering-scams

[Source](https://www.schneier.com/blog/archives/2023/02/on-pig-butchering-scams.html){:target="_blank" rel="noopener"}

> “Pig butchering” is the colorful name given to online cons that trick the victim into giving money to the scammer, thinking it is an investment opportunity. It’s a rapidly growing area of fraud, and getting more sophisticated. [...]
