Title: HardBit ransomware wants insurance details to set the perfect price
Date: 2023-02-20T17:09:01-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-20-hardbit-ransomware-wants-insurance-details-to-set-the-perfect-price

[Source](https://www.bleepingcomputer.com/news/security/hardbit-ransomware-wants-insurance-details-to-set-the-perfect-price/){:target="_blank" rel="noopener"}

> A ransomware threat called HardBit has moved to version 2.0 and its operators are trying to negotiate a ransom payment that would be covered by the victim's insurance company. [...]
