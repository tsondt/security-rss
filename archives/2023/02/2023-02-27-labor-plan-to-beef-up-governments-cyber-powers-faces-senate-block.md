Title: Labor plan to beef up government’s cyber powers faces Senate block
Date: 2023-02-27T14:00:00+00:00
Author: Daniel Hurst
Category: The Guardian
Tags: Cybercrime;Australian politics;Australia news;Technology;Data and computer security;Australian Greens;Clare O'Neil;Business;Anthony Albanese
Slug: 2023-02-27-labor-plan-to-beef-up-governments-cyber-powers-faces-senate-block

[Source](https://www.theguardian.com/technology/2023/feb/27/labor-plan-to-beef-up-governments-cyber-powers-faces-senate-block){:target="_blank" rel="noopener"}

> A paper expanding on greater ability to intervene during hacks – especially on private companies – causes alarm among Coalition and Greens Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast Labor could face Senate difficulties if it tries to dramatically expand the government’s powers to directly intervene in companies’ IT systems during cyber-attacks. Under existing laws – which were controversial when introduced by the former Coalition government – the Australian Signals Directorate has the ability to “step in” as a “last resort” in some emergency [...]
