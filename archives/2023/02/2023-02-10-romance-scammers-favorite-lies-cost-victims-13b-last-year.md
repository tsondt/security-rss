Title: Romance scammers' favorite lies cost victims $1.3B last year
Date: 2023-02-10T03:28:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-10-romance-scammers-favorite-lies-cost-victims-13b-last-year

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/10/romance_scammers_cost_victims_13b/){:target="_blank" rel="noopener"}

> Don't trust your super-hot military boyfriend you've never met. He doesn't exist As Valentine's Day approaches, if your offshore oil rig worker "boyfriend" – who looks like Bradley Cooper in his online pics and has hinted at proposing to you for months, but you've never met in real life – suddenly needs money for "hospital bills"... Just. Don't. Do. It.... [...]
