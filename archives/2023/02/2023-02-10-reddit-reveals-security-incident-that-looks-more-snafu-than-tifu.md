Title: Reddit reveals security incident that looks more SNAFU than TIFU
Date: 2023-02-10T01:29:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-02-10-reddit-reveals-security-incident-that-looks-more-snafu-than-tifu

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/10/reddit_security_incident/){:target="_blank" rel="noopener"}

> Phishing hooked internal documents, code, and some non-critical systems, but users' personal info safe Colorful web forum Reddit has revealed it has suffered a security breach.... [...]
