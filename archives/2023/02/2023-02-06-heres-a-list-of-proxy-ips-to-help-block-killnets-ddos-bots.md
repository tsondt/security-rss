Title: Here's a list of proxy IPs to help block KillNet's DDoS bots
Date: 2023-02-06T21:00:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-06-heres-a-list-of-proxy-ips-to-help-block-killnets-ddos-bots

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/06/killnet_proxy_ip_list/){:target="_blank" rel="noopener"}

> Put pro-Putin bots on the do not call list A free tool aims is helping organizations defend against KillNet distributed-denial-of-service (DDoS) bots and comes as the US government issued a warning that the Russian cybercrime gang is stepping up its network flooding attacks against hospitals and health clinics.... [...]
