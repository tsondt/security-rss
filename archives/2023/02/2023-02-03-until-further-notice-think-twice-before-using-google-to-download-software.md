Title: Until further notice, think twice before using Google to download software
Date: 2023-02-03T13:29:28+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;malicious ads;malvertising;malware
Slug: 2023-02-03-until-further-notice-think-twice-before-using-google-to-download-software

[Source](https://arstechnica.com/?p=1914704){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Searching Google for downloads of popular software has always come with risks, but over the past few months, it has been downright dangerous, according to researchers and a pseudorandom collection of queries. “Threat researchers are used to seeing a moderate flow of malvertising via Google Ads,” volunteers at Spamhaus wrote on Thursday. “However, over the past few days, researchers have witnessed a massive spike affecting numerous famous brands, with multiple malware being utilized. This is not ‘the norm.’” One of many new threats: MalVirt The surge is coming from numerous malware families, including AuroraStealer, IcedID, Meta [...]
