Title: Among the thousands of ESXiArgs ransomware victims? FBI and CISA to the rescue
Date: 2023-02-08T21:30:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-08-among-the-thousands-of-esxiargs-ransomware-victims-fbi-and-cisa-to-the-rescue

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/08/esxiargs_ransomware_recovery_script/){:target="_blank" rel="noopener"}

> Evil code hits more than 3,800 servers globally, according to the Feds The US Cybersecurity and Infrastructure Security Agency (CISA) has released a recovery script to help companies whose servers were scrambled in the recent ESXiArgs ransomware outbreak.... [...]
