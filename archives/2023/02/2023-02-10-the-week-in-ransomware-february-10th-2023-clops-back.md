Title: The Week in Ransomware - February 10th 2023 - Clop's Back
Date: 2023-02-10T18:24:36-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-02-10-the-week-in-ransomware-february-10th-2023-clops-back

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-february-10th-2023-clops-back/){:target="_blank" rel="noopener"}

> From ongoing attacks targeting ESXi servers to sanctions on Conti/TrickBot members, it has been quite a busy week regarding ransomware. [...]
