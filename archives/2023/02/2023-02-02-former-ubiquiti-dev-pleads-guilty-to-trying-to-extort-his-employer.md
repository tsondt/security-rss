Title: Former Ubiquiti dev pleads guilty to trying to extort his employer
Date: 2023-02-02T14:01:50-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-02-02-former-ubiquiti-dev-pleads-guilty-to-trying-to-extort-his-employer

[Source](https://www.bleepingcomputer.com/news/security/former-ubiquiti-dev-pleads-guilty-to-trying-to-extort-his-employer/){:target="_blank" rel="noopener"}

> Nickolas Sharp, a former Ubiquiti employee who managed the networking device maker's cloud team, pled guilty today to stealing gigabytes worth of files from Ubiquiti's network and trying to extort his employer while posing as an anonymous hacker and a whistleblower. [...]
