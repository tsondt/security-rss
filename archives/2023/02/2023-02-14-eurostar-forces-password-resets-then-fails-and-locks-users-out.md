Title: Eurostar forces 'password resets' — then fails and locks users out
Date: 2023-02-14T01:34:11-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-02-14-eurostar-forces-password-resets-then-fails-and-locks-users-out

[Source](https://www.bleepingcomputer.com/news/security/eurostar-forces-password-resets-then-fails-and-locks-users-out/){:target="_blank" rel="noopener"}

> Eurostar is emailing its users this week, forcing them to reset their account passwords in a bid to "upgrade" security. But when users visit the password reset link, they are met with "technical problems," making it impossible for them to reset password or access their account. [...]
