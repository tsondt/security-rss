Title: Google will boost Android security through firmware hardening
Date: 2023-02-21T12:30:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-02-21-google-will-boost-android-security-through-firmware-hardening

[Source](https://www.bleepingcomputer.com/news/security/google-will-boost-android-security-through-firmware-hardening/){:target="_blank" rel="noopener"}

> Google has presented a plan to strengthen the firmware security on secondary Android SoCs (systems on a chip) by introducing mechanisms like control flow integrity, memory safety systems, and compiler-based sanitizers. [...]
