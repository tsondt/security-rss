Title: Valve “honeypot” used to ban 40,000 Dota 2 players using cheat
Date: 2023-02-23T14:45:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2023-02-23-valve-honeypot-used-to-ban-40000-dota-2-players-using-cheat

[Source](https://www.bleepingcomputer.com/news/security/valve-honeypot-used-to-ban-40-000-dota-2-players-using-cheat/){:target="_blank" rel="noopener"}

> Game developer Valve has announced that it permanently banned more than 40,000 accounts for using cheating software to gain an unfair advantage over other players in the Dota 2 game. [...]
