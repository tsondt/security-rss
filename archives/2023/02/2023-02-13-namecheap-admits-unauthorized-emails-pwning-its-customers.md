Title: Namecheap admits 'unauthorized emails' pwning its customers
Date: 2023-02-13T16:13:07+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-02-13-namecheap-admits-unauthorized-emails-pwning-its-customers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/13/namecheap/){:target="_blank" rel="noopener"}

> Blames 'third-party provider' as phishers drain Ethereum wallets Domain registrar Namecheap blamed a "third-party provider" that sends its newsletters after customers complained of receiving phishing emails from Namecheap's system.... [...]
