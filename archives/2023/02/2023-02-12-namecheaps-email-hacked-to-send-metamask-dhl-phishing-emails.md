Title: NameCheap's email hacked to send Metamask, DHL phishing emails
Date: 2023-02-12T18:07:29-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-02-12-namecheaps-email-hacked-to-send-metamask-dhl-phishing-emails

[Source](https://www.bleepingcomputer.com/news/security/namecheaps-email-hacked-to-send-metamask-dhl-phishing-emails/){:target="_blank" rel="noopener"}

> Domain registrar Namecheap had their email account breached Sunday night, causing a flood of MetaMask and DHL phishing emails that attempted to steal recipients' personal information and cryptocurrency wallets. [...]
