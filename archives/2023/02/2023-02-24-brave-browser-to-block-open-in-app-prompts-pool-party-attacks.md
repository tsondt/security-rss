Title: Brave browser to block “open in app” prompts, pool-party attacks
Date: 2023-02-24T04:38:49-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2023-02-24-brave-browser-to-block-open-in-app-prompts-pool-party-attacks

[Source](https://www.bleepingcomputer.com/news/security/brave-browser-to-block-open-in-app-prompts-pool-party-attacks/){:target="_blank" rel="noopener"}

> Brave Software, the developer of the privacy-focused web browser, has announced some plants for the upcoming version 1.49 that will block everyday browsing annoyances like "open in app" prompts and add better protections against pool-party attacks, [...]
