Title: Second UK Computer Misuse Act consultation reflects ‘very little progress’
Date: 2023-02-08T17:02:06+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-02-08-second-uk-computer-misuse-act-consultation-reflects-very-little-progress

[Source](https://portswigger.net/daily-swig/second-uk-computer-misuse-act-consultation-reflects-very-little-progress){:target="_blank" rel="noopener"}

> Campaigner bemoans glacial progress of review and urges government to set clear timetable [...]
