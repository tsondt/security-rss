Title: Weee! grocery service confirms data breach, 1.1 million affected
Date: 2023-02-08T16:21:24-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-02-08-weee-grocery-service-confirms-data-breach-11-million-affected

[Source](https://www.bleepingcomputer.com/news/security/weee-grocery-service-confirms-data-breach-11-million-affected/){:target="_blank" rel="noopener"}

> The Weee! Asian and Hispanic food delivery service suffered a data breach exposing the personal information of 1.1 million customers. [...]
