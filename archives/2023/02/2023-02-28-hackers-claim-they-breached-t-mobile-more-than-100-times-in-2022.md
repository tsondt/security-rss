Title: Hackers Claim They Breached T-Mobile More Than 100 Times in 2022
Date: 2023-02-28T16:14:57+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;SIM Swapping;Web Fraud 2.0;Allison Nixon;International Computer Science Institute;Minecraft;Nicholas Weaver;Roblox;Security Keys;SIM swapping;T-Mobile;tmo up;Unit 221B
Slug: 2023-02-28-hackers-claim-they-breached-t-mobile-more-than-100-times-in-2022

[Source](https://krebsonsecurity.com/2023/02/hackers-claim-they-breached-t-mobile-more-than-100-times-in-2022/){:target="_blank" rel="noopener"}

> Image: Shutterstock.com Three different cybercriminal groups claimed access to internal networks at communications giant T-Mobile in more than 100 separate incidents throughout 2022, new data suggests. In each case, the goal of the attackers was the same: Phish T-Mobile employees for access to internal company tools, and then convert that access into a cybercrime service that could be hired to divert any T-Mobile user’s text messages and phone calls to another device. The conclusions above are based on an extensive analysis of Telegram chat logs from three distinct cybercrime groups or actors that have been identified by security researchers as [...]
