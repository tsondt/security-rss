Title: North Korean ransomware attacks on healthcare fund govt operations
Date: 2023-02-10T09:35:49-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-10-north-korean-ransomware-attacks-on-healthcare-fund-govt-operations

[Source](https://www.bleepingcomputer.com/news/security/north-korean-ransomware-attacks-on-healthcare-fund-govt-operations/){:target="_blank" rel="noopener"}

> A new cybersecurity advisory from the U.S. Cybersecurity & Infrastructure Security Agency (CISA) describes recently observed tactics, techniques, and procedures (TTPs) observed with North Korean ransomware operations against public health and other critical infrastructure sectors. [...]
