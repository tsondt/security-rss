Title: Russian hacktivists DDoS hospitals, with pathetic results
Date: 2023-02-28T07:30:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-28-russian-hacktivists-ddos-hospitals-with-pathetic-results

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/28/anonymous_sudan_ddos_hospitals/){:target="_blank" rel="noopener"}

> Not that we're urging them to try harder or anything A series of distributed-denial-of-service (DDoS) attacks shut down nine Danish hospitals' websites for a few hours on Sunday, but did not have any life-threatening impact on the medical centers' operations or digital infrastructure.... [...]
