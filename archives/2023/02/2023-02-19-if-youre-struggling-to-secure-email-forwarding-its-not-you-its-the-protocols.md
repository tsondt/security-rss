Title: If you're struggling to secure email forwarding, it's not you, it's ... the protocols
Date: 2023-02-19T09:00:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-02-19-if-youre-struggling-to-secure-email-forwarding-its-not-you-its-the-protocols

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/19/forwarding_email_security/){:target="_blank" rel="noopener"}

> Eggheads prove they can mimic messages and bag bug bounty bucks Analysis Over the past two decades, efforts have been made to make email more secure. Alas, defensive protocols implemented during this period, such as SPF, DKIM, and DMARC, remain unable to deal with the complexity of email forwarding and differing standards, a study has concluded.... [...]
