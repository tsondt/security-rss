Title: NPM packages posing as speed testers install crypto miners instead
Date: 2023-02-14T12:25:11-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-14-npm-packages-posing-as-speed-testers-install-crypto-miners-instead

[Source](https://www.bleepingcomputer.com/news/security/npm-packages-posing-as-speed-testers-install-crypto-miners-instead/){:target="_blank" rel="noopener"}

> A new set of 16 malicious NPM packages are pretending to be internet speed testers but are, in reality, coinminers that hijack the compromised computer's resources to mine cryptocurrency for the threat actors. [...]
