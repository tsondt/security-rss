Title: Cisco fixes bug allowing backdoor persistence between reboots
Date: 2023-02-02T12:07:55-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-02-cisco-fixes-bug-allowing-backdoor-persistence-between-reboots

[Source](https://www.bleepingcomputer.com/news/security/cisco-fixes-bug-allowing-backdoor-persistence-between-reboots/){:target="_blank" rel="noopener"}

> Cisco has released security updates this week to address a high-severity vulnerability in the Cisco IOx application hosting environment that can be exploited in command injection attacks. [...]
