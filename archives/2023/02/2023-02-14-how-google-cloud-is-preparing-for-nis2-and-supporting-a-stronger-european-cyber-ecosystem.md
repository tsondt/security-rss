Title: How Google Cloud is preparing for NIS2 and supporting a stronger European cyber ecosystem
Date: 2023-02-14T14:00:00+00:00
Author: Lie Junius
Category: GCP Security
Tags: Identity & Security
Slug: 2023-02-14-how-google-cloud-is-preparing-for-nis2-and-supporting-a-stronger-european-cyber-ecosystem

[Source](https://cloud.google.com/blog/products/identity-security/how-google-cloud-is-preparing-for-nis2-and-protecting-europe-from-cyber-threats/){:target="_blank" rel="noopener"}

> Online data theft is a significant risk for organizations around the world and in Europe. European businesses stand to lose roughly 10 terabytes of data each month to cyber theft, according to a July 2022 report from the European Union Agency for Cybersecurity (ENISA). Meanwhile, cyberattacks cost European businesses and consumers an estimated €180 billion to €290 billion annually. To help combat this threat, the EU passed the Network and Information Security Directive 2.0 (NIS2), a signature policy response that took effect in January. NIS2 builds on the EU’s previous efforts to raise the baseline level of cybersecurity throughout the [...]
