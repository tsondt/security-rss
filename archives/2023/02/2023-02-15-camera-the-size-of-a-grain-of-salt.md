Title: Camera the Size of a Grain of Salt
Date: 2023-02-15T12:13:04+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cameras;privacy;surveillance
Slug: 2023-02-15-camera-the-size-of-a-grain-of-salt

[Source](https://www.schneier.com/blog/archives/2023/02/camera-the-size-of-a-grain-of-salt.html){:target="_blank" rel="noopener"}

> Cameras are getting smaller and smaller, changing the scale and scope of surveillance. [...]
