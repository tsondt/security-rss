Title: Microsoft urges Exchange admins to remove some antivirus exclusions
Date: 2023-02-23T16:59:07-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-02-23-microsoft-urges-exchange-admins-to-remove-some-antivirus-exclusions

[Source](https://www.bleepingcomputer.com/news/security/microsoft-urges-exchange-admins-to-remove-some-antivirus-exclusions/){:target="_blank" rel="noopener"}

> Microsoft says some antivirus exclusions previously recommended for Exchange servers should be removed to boost their security. [...]
