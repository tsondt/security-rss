Title: SolarWinds and Market Incentives
Date: 2023-02-08T11:46:43+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;breaches;data breaches;economics of security;essays
Slug: 2023-02-08-solarwinds-and-market-incentives

[Source](https://www.schneier.com/blog/archives/2023/02/solarwinds-and-market-incentives.html){:target="_blank" rel="noopener"}

> In early 2021, IEEE Security and Privacy asked a number of board members for brief perspectives on the SolarWinds incident while it was still breaking news. This was my response. The penetration of government and corporate networks worldwide is the result of inadequate cyberdefenses across the board. The lessons are many, but I want to focus on one important one we’ve learned: the software that’s managing our critical networks isn’t secure, and that’s because the market doesn’t reward that security. SolarWinds is a perfect example. The company was the initial infection vector for much of the operation. Its trusted position [...]
