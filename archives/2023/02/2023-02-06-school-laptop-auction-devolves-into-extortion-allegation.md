Title: School laptop auction devolves into extortion allegation
Date: 2023-02-06T07:32:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-02-06-school-laptop-auction-devolves-into-extortion-allegation

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/06/school_laptop_auction_devolves_into/){:target="_blank" rel="noopener"}

> Also: Atlassian says Jira has a 9.4 severity bug and the TSA issues milquetoast no-fly list security advisory When a Texas school district sold some old laptops at auction last year, it probably didn't expect to end up in a public legal fight with a local computer repair shop – but a debate over what to do with district data found on the liquidated machines has led to precisely that.... [...]
