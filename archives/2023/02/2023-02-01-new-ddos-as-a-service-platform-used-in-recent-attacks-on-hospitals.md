Title: New DDoS-as-a-Service platform used in recent attacks on hospitals
Date: 2023-02-01T12:58:41-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-01-new-ddos-as-a-service-platform-used-in-recent-attacks-on-hospitals

[Source](https://www.bleepingcomputer.com/news/security/new-ddos-as-a-service-platform-used-in-recent-attacks-on-hospitals/){:target="_blank" rel="noopener"}

> A new DDoS-as-a-Service (DDoSaaS) platform named 'Passion' was seen used in recent attacks by pro-Russian hacktivists against medical institutions in the United States and Europe. [...]
