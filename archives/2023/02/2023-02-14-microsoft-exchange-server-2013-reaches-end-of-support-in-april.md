Title: Microsoft: Exchange Server 2013 reaches end of support in April
Date: 2023-02-14T14:30:23-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-02-14-microsoft-exchange-server-2013-reaches-end-of-support-in-april

[Source](https://www.bleepingcomputer.com/news/security/microsoft-exchange-server-2013-reaches-end-of-support-in-april/){:target="_blank" rel="noopener"}

> Microsoft has reminded admins that Exchange Server 2013 is reaching its extended end-of-support (EOS) date in 60 days, on April 11, 2023. [...]
