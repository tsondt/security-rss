Title: LockBit's Royal Mail ransom deadline flies by. No data released
Date: 2023-02-13T12:38:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-02-13-lockbits-royal-mail-ransom-deadline-flies-by-no-data-released

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/13/lockbits_royal_mail_ransom_deadline/){:target="_blank" rel="noopener"}

> Also: Russian wiper malware authors turn to data theft, plus this week's critical vulns in brief The notorious LockBit ransomware gang has taken credit for an attack on the Royal Mail – but a deadline it gave for payment has come and gone with nothing exposed to the web except the group's claims.... [...]
