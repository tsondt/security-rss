Title: Mary Queen of Scots Letters Decrypted
Date: 2023-02-09T12:15:29+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptography;encryption;history of cryptography
Slug: 2023-02-09-mary-queen-of-scots-letters-decrypted

[Source](https://www.schneier.com/blog/archives/2023/02/mary-queen-of-scots-letters-decrypted.html){:target="_blank" rel="noopener"}

> This is a neat piece of historical research. The team of computer scientist George Lasry, pianist Norbert Biermann and astrophysicist Satoshi Tomokiyo—all keen cryptographers—initially thought the batch of encoded documents related to Italy, because that was how they were filed at the Bibliothèque Nationale de France. However, they quickly realised the letters were in French. Many verb and adjectival forms being feminine, regular mention of captivity, and recurring names—such as Walsingham—all put them on the trail of Mary. Sir Francis Walsingham was Queen Elizabeth’s spymaster. The code was a simple replacement system in which symbols stand either for letters, or [...]
