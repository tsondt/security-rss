Title: Uncle Sam wants to strip the IoS out of IoT with light crypto
Date: 2023-02-09T00:30:15+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-02-09-uncle-sam-wants-to-strip-the-ios-out-of-iot-with-light-crypto

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/09/nist_iot_hpc_algorithms/){:target="_blank" rel="noopener"}

> NIST weighs up algorithms for small devices – and an architecture for massive systems The US National Institute of Standards and Technology wants to protect all devices great and small, and is getting closer to settling on next-gen cryptographic algorithms suitable for systems at both ends of that spectrum – the very great and the very small.... [...]
