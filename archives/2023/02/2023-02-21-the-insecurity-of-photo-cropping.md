Title: The Insecurity of Photo Cropping
Date: 2023-02-21T12:14:28+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;forensics;metadata;photos;whistleblowers
Slug: 2023-02-21-the-insecurity-of-photo-cropping

[Source](https://www.schneier.com/blog/archives/2023/02/the-insecurity-of-photo-cropping.html){:target="_blank" rel="noopener"}

> The Intercept has a long article on the insecurity of photo cropping: One of the hazards lies in the fact that, for some of the programs, downstream crop reversals are possible for viewers or readers of the document, not just the file’s creators or editors. Official instruction manuals, help pages, and promotional materials may mention that cropping is reversible, but this documentation at times fails to note that these operations are reversible by any viewers of a given image or document. [...] Uncropped versions of images can be preserved not just in Office apps, but also in a file’s own [...]
