Title: Lazarus hackers use new mixer to hide $100 million in stolen crypto
Date: 2023-02-13T11:00:57-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-02-13-lazarus-hackers-use-new-mixer-to-hide-100-million-in-stolen-crypto

[Source](https://www.bleepingcomputer.com/news/security/lazarus-hackers-use-new-mixer-to-hide-100-million-in-stolen-crypto/){:target="_blank" rel="noopener"}

> North Korean hackers have found a way around U.S.-imposed sanctions to launder the cryptocurrency proceeds from their heists, according to evidence discovered by blockchain analysts. [...]
