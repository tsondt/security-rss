Title: Super Bock says 'cyber' nasty 'disrupting computer services'
Date: 2023-02-02T11:15:05+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-02-02-super-bock-says-cyber-nasty-disrupting-computer-services

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/02/super_bock_cyberattack/){:target="_blank" rel="noopener"}

> Portugal's biggest exporter of beer warns of restrictions to supply chain Super Bock Group, Portugal's largest beverage biz, is warning of potential interruption to supplies as it manages the fallout from cybercrooks attacking its tech infrastructure.... [...]
