Title: Lawyers join forces to fight common enemy: The SEC and its probes into cyber-victims
Date: 2023-02-23T02:00:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-23-lawyers-join-forces-to-fight-common-enemy-the-sec-and-its-probes-into-cyber-victims

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/23/covington_sec_amicus/){:target="_blank" rel="noopener"}

> Did the financial watchdog just do the impossible and herd cats? More than 80 law firms say they are "deeply troubled" by the US Securities and Exchange Commission's demand that Covington & Burling hand over names of its clients whose information was stolen by Chinese state-sponsored hackers.... [...]
