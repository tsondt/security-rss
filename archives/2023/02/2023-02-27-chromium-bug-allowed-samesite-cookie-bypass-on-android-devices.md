Title: Chromium bug allowed SameSite cookie bypass on Android devices
Date: 2023-02-27T11:50:28+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-02-27-chromium-bug-allowed-samesite-cookie-bypass-on-android-devices

[Source](https://portswigger.net/daily-swig/chromium-bug-allowed-samesite-cookie-bypass-on-android-devices){:target="_blank" rel="noopener"}

> Protections against cross-site request forgery could be bypassed [...]
