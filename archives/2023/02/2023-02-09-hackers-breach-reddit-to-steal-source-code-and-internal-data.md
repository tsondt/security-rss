Title: Hackers breach Reddit to steal source code and internal data
Date: 2023-02-09T17:04:42-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-02-09-hackers-breach-reddit-to-steal-source-code-and-internal-data

[Source](https://www.bleepingcomputer.com/news/security/hackers-breach-reddit-to-steal-source-code-and-internal-data/){:target="_blank" rel="noopener"}

> Reddit suffered a cyberattack Sunday evening, allowing hackers to access internal business systems and steal internal documents and source code. [...]
