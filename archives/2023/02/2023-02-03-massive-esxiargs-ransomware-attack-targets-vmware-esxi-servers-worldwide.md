Title: Massive ESXiArgs ransomware attack targets VMware ESXi servers worldwide
Date: 2023-02-03T14:20:48-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-03-massive-esxiargs-ransomware-attack-targets-vmware-esxi-servers-worldwide

[Source](https://www.bleepingcomputer.com/news/security/massive-esxiargs-ransomware-attack-targets-vmware-esxi-servers-worldwide/){:target="_blank" rel="noopener"}

> Admins, hosting providers, and the French Computer Emergency Response Team (CERT-FR) warn that attackers actively target VMware ESXi servers unpatched against a two-year-old remote code execution vulnerability to deploy ransomware. [...]
