Title: Dole production plants crippled by ransomware, stores run short
Date: 2023-02-23T21:30:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-23-dole-production-plants-crippled-by-ransomware-stores-run-short

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/23/dole_ransomware_attack/){:target="_blank" rel="noopener"}

> Yes, we have no bananas, and things aren't looking peachy on the salad front Irish agricultural megacorp Dole has confirmed that it has fallen victim to a ransomware infection that reportedly shut down some of its North American production plants.... [...]
