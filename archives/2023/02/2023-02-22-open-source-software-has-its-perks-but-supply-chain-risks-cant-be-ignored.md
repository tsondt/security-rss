Title: Open source software has its perks, but supply chain risks can't be ignored
Date: 2023-02-22T12:46:11+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-02-22-open-source-software-has-its-perks-but-supply-chain-risks-cant-be-ignored

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/22/open_software_supply_chain_risks/){:target="_blank" rel="noopener"}

> While app development is faster and easier, security is still a concern Analysis Open source components play an increasingly central role in the software development scene, proving to be a boon in a time of continuous integration and deployment, DevOps, and daily software updates.... [...]
