Title: FTC: $1.3 billion lost by 70,000 Americans to romance scams last year
Date: 2023-02-13T10:24:58-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-13-ftc-13-billion-lost-by-70000-americans-to-romance-scams-last-year

[Source](https://www.bleepingcomputer.com/news/security/ftc-13-billion-lost-by-70-000-americans-to-romance-scams-last-year/){:target="_blank" rel="noopener"}

> The U.S. Federal Trade Commission (FTC) says Americans once again reported record losses of $1.3 billion to romance scams in 2022, with median losses of $4,400. [...]
