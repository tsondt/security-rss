Title: US cybersecurity chief: Software makers shouldn't lawyer their way out of security responsibilities
Date: 2023-02-28T22:32:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-28-us-cybersecurity-chief-software-makers-shouldnt-lawyer-their-way-out-of-security-responsibilities

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/28/cisa_easterly_secure_software/){:target="_blank" rel="noopener"}

> Who apart from Microsoft is happy with the ship now, oh just fix it later approach? What's more dangerous than Chinese spy balloons? Unsafe software and other technology products, according to America's Cybersecurity and Infrastructure Agency (CISA) Director Jen Easterly.... [...]
