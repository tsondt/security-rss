Title: Norway finds a way to recover crypto North Korea pinched in Axie heist
Date: 2023-02-17T05:15:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-02-17-norway-finds-a-way-to-recover-crypto-north-korea-pinched-in-axie-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/17/norwegian_authorities_found_59_million/){:target="_blank" rel="noopener"}

> Meanwhile South Korea's Do Kwon is sought for fraud by US authorities Norwegian authorities announced on Thursday that they had recovered $5.9 million of cryptocurrency stolen in the Axie Infinity hack – an incident widely held to have been perpetrated by the Lazarus Group, which has links to North Korea.... [...]
