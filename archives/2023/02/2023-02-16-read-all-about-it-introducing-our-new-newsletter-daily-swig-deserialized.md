Title: Read all about it: Introducing our new newsletter, Daily Swig Deserialized
Date: 2023-02-16T15:19:44+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-02-16-read-all-about-it-introducing-our-new-newsletter-daily-swig-deserialized

[Source](https://portswigger.net/daily-swig/read-all-about-it-introducing-our-new-newsletter-daily-swig-deserialized){:target="_blank" rel="noopener"}

> Free fortnightly roundup and exclusive content for subscribers only [...]
