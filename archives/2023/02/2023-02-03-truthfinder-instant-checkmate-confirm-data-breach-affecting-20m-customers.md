Title: TruthFinder, Instant Checkmate confirm data breach affecting 20M customers
Date: 2023-02-03T18:24:05-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-02-03-truthfinder-instant-checkmate-confirm-data-breach-affecting-20m-customers

[Source](https://www.bleepingcomputer.com/news/security/truthfinder-instant-checkmate-confirm-data-breach-affecting-20m-customers/){:target="_blank" rel="noopener"}

> PeopleConnect, the owners of the TruthFinder and Instant Checkmate background check services, confirmed they suffered a data breach after hackers leaked a 2019 backup database containing the info of millions of customers. [...]
