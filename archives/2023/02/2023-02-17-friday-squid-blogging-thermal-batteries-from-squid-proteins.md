Title: Friday Squid Blogging: Thermal Batteries from Squid Proteins
Date: 2023-02-17T22:03:44+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-02-17-friday-squid-blogging-thermal-batteries-from-squid-proteins

[Source](https://www.schneier.com/blog/archives/2023/02/friday-squid-blogging-thermal-batteries-from-squid-proteins.html){:target="_blank" rel="noopener"}

> Researchers are making thermal batteries from “a synthetic material that’s derived from squid ring teeth protein.” As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
