Title: Microsoft WinGet package manager failing from expired SSL certificate
Date: 2023-02-11T23:37:50-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-02-11-microsoft-winget-package-manager-failing-from-expired-ssl-certificate

[Source](https://www.bleepingcomputer.com/news/security/microsoft-winget-package-manager-failing-from-expired-ssl-certificate/){:target="_blank" rel="noopener"}

> Microsoft's WinGet package manager is currently having problems installing or upgrading packages after WinGet CDN's SSL/TLS certificate expired. [...]
