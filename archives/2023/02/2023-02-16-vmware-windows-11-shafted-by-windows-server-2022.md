Title: VMware, Windows 11 shafted by Windows Server 2022
Date: 2023-02-16T20:30:11+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-02-16-vmware-windows-11-shafted-by-windows-server-2022

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/16/microsoft_windows_server_vmware/){:target="_blank" rel="noopener"}

> OS won't start on some systems with ESXi VMs, while Win11 updates may not make it to devices Microsoft is sorting through two issues with Windows Server 2022 that affect VMware virtual machines and updates not getting passed on to Windows 11 devices.... [...]
