Title: Police hacked Exclu 'secure' message platform to snoop on criminals
Date: 2023-02-06T12:06:56-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-06-police-hacked-exclu-secure-message-platform-to-snoop-on-criminals

[Source](https://www.bleepingcomputer.com/news/security/police-hacked-exclu-secure-message-platform-to-snoop-on-criminals/){:target="_blank" rel="noopener"}

> The Dutch police announced on Friday that they dismantled the Exclu encrypted communications platform after hacking into the service to monitor the activities of criminal organizations. [...]
