Title: FTC: Americans lost $8.8 billion to fraud in 2022 after 30% surge
Date: 2023-02-23T15:52:55-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-23-ftc-americans-lost-88-billion-to-fraud-in-2022-after-30-surge

[Source](https://www.bleepingcomputer.com/news/security/ftc-americans-lost-88-billion-to-fraud-in-2022-after-30-percent-surge/){:target="_blank" rel="noopener"}

> The U.S. Federal Trade Commission (FTC) revealed today that Americans lost almost $8.8 billion to various types of scams in 2022, following a significant surge of over 30% more lost to fraud compared to the previous year. [...]
