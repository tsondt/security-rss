Title: Russian man pleads guilty to laundering Ryuk ransomware money
Date: 2023-02-07T13:57:34-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-02-07-russian-man-pleads-guilty-to-laundering-ryuk-ransomware-money

[Source](https://www.bleepingcomputer.com/news/security/russian-man-pleads-guilty-to-laundering-ryuk-ransomware-money/){:target="_blank" rel="noopener"}

> Russian citizen Denis Mihaqlovic Dubnikov pleaded guilty on Tuesday to laundering money for the notorious Ryuk ransomware group for over three years. [...]
