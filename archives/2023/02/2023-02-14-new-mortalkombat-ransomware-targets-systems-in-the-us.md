Title: New ‘MortalKombat’ ransomware targets systems in the U.S.
Date: 2023-02-14T08:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-14-new-mortalkombat-ransomware-targets-systems-in-the-us

[Source](https://www.bleepingcomputer.com/news/security/new-mortalkombat-ransomware-targets-systems-in-the-us/){:target="_blank" rel="noopener"}

> Hackers conducting a new financially motivated campaign are using a variant of the Xortist commodity ransomware named 'MortalKombat,' together with the Laplas clipper in cyberattacks. [...]
