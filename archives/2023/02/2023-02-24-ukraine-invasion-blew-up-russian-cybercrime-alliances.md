Title: Ukraine invasion blew up Russian cybercrime alliances
Date: 2023-02-24T05:00:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-24-ukraine-invasion-blew-up-russian-cybercrime-alliances

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/24/russian_cybercrime_economy/){:target="_blank" rel="noopener"}

> Study: Old pacts ditched the moment Moscow moved in The so-called "brotherhood" or Russian-speaking cybercriminals is yet another casualty of the war in Ukraine, albeit one that few outside of Moscow are mourning.... [...]
