Title: Ransomware crooks steal 3m+ patients' medical records, personal info
Date: 2023-02-11T02:16:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-11-ransomware-crooks-steal-3m-patients-medical-records-personal-info

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/11/ransomware_regal_medical_group/){:target="_blank" rel="noopener"}

> All that data coming soon to a darkweb crime forum near you? Several California medical groups have sent security breach notification letters to more than three million patients alerting them that crooks may have stolen a ton of their sensitive health and personal information during a ransomware infection in December.... [...]
