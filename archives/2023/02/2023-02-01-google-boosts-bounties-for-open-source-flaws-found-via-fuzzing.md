Title: Google boosts bounties for open source flaws found via fuzzing
Date: 2023-02-01T23:01:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-01-google-boosts-bounties-for-open-source-flaws-found-via-fuzzing

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/01/google_fuzz_rewards/){:target="_blank" rel="noopener"}

> Max reward per project integration is now $30k Google sweetened the potential pot to $30,000 for bug hunters in its open source OSS-Fuzz code testing project.... [...]
