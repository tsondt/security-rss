Title: Learn the art of malicious compliance: doing exactly what you were asked, even when it's wrong
Date: 2023-02-13T08:28:06+00:00
Author: Matthew JC Powell
Category: The Register
Tags: 
Slug: 2023-02-13-learn-the-art-of-malicious-compliance-doing-exactly-what-you-were-asked-even-when-its-wrong

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/13/who_me/){:target="_blank" rel="noopener"}

> Smart-alec worker found a way to avoid nasty, boring jobs – by doing what he was told Who, Me? Ah, gentle reader, welcome back once again to the comfortable backwater of The Register we call Who, Me? in which readers' tales of not-quite-rightness are immortalized for the ages.... [...]
