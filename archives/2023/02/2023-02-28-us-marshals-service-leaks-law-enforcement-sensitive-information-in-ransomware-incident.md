Title: US Marshals Service leaks ‘law enforcement sensitive information’ in ransomware incident
Date: 2023-02-28T06:59:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-02-28-us-marshals-service-leaks-law-enforcement-sensitive-information-in-ransomware-incident

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/28/us_marshals_ransomware_data_exfiltration/){:target="_blank" rel="noopener"}

> It’s not just another data breach when the victim oversees witness protection programs The US Marshals Service, the enforcement branch of the nation’s federal courts, has admitted a “major” breach of its information security defenses led to a ransomware infection and exfiltration of “law-enforcement sensitive information."... [...]
