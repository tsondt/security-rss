Title: Hyundai and Kia issue software upgrades to thwart killer TikTok car theft hack
Date: 2023-02-15T07:29:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-02-15-hyundai-and-kia-issue-software-upgrades-to-thwart-killer-tiktok-car-theft-hack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/15/hyundai_kia_software_upgrades/){:target="_blank" rel="noopener"}

> Gone in 60 seconds using a USB-A plug and brute force instead of a key Korean car-makers Hyundai and Kia will issue software updates to some of their models after a method of stealing them circulated on TikTok, leading to many thefts and even some deaths.... [...]
