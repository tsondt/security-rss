Title: New MortalKombat ransomware decryptor recovers your files for free
Date: 2023-02-28T13:27:06-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2023-02-28-new-mortalkombat-ransomware-decryptor-recovers-your-files-for-free

[Source](https://www.bleepingcomputer.com/news/security/new-mortalkombat-ransomware-decryptor-recovers-your-files-for-free/){:target="_blank" rel="noopener"}

> Cybersecurity company Bitdefender has released a free MortalKombat ransomware decryptor that victims can use to restore their files without paying a ransom. [...]
