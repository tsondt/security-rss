Title: VMware warns admins of critical Carbon Black App Control flaw
Date: 2023-02-22T12:12:40-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-22-vmware-warns-admins-of-critical-carbon-black-app-control-flaw

[Source](https://www.bleepingcomputer.com/news/security/vmware-warns-admins-of-critical-carbon-black-app-control-flaw/){:target="_blank" rel="noopener"}

> VMware has released a critical security upgrade to address a critical injection vulnerability that impacts several versions of Carbon Black App Control for Windows. [...]
