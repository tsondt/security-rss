Title: Upcoming Speaking Engagements
Date: 2023-02-14T16:54:43+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2023-02-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2023/02/upcoming-speaking-engagements-27.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking at Mobile World Congress 2023 in Barcelona, Spain, on March 1, 2023 at 1:00 PM CET. I’m speaking on “ How to Reclaim Power in the Digital World ” at EPFL in Lausanne, Switzerland, on Thursday, March 16, 2023, at 5:30 PM. I’m speaking at IT-S Now 2023 in Vienna, Austria, on June 1-2, 2023. The list is maintained on this page. [...]
