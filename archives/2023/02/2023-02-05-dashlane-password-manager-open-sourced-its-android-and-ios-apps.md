Title: Dashlane password manager open-sourced its Android and iOS apps
Date: 2023-02-05T12:17:34-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-05-dashlane-password-manager-open-sourced-its-android-and-ios-apps

[Source](https://www.bleepingcomputer.com/news/security/dashlane-password-manager-open-sourced-its-android-and-ios-apps/){:target="_blank" rel="noopener"}

> Dashlane announced it had made the source code for its Android and iOS apps available on GitHub under the Creative Commons Attribution-NonCommercial 4.0 license. [...]
