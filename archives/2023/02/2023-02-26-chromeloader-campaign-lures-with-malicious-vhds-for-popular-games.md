Title: ChromeLoader campaign lures with malicious VHDs for popular games
Date: 2023-02-26T11:10:01-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-26-chromeloader-campaign-lures-with-malicious-vhds-for-popular-games

[Source](https://www.bleepingcomputer.com/news/security/chromeloader-campaign-lures-with-malicious-vhds-for-popular-games/){:target="_blank" rel="noopener"}

> Security researchers have noticed that the operators of the ChromeLoader browser hijacking and adware campaign are now using VHD files named after popular games. Previously, such campaigns relied on ISO-based distribution. [...]
