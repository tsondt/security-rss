Title: Dutch Police arrest three ransomware actors extorting €2.5 million
Date: 2023-02-24T03:32:37-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-02-24-dutch-police-arrest-three-ransomware-actors-extorting-25-million

[Source](https://www.bleepingcomputer.com/news/security/dutch-police-arrest-three-ransomware-actors-extorting-25-million/){:target="_blank" rel="noopener"}

> The Amsterdam cybercrime police team has arrested three men for ransomware activity that generated €2.5 million from extorting small and large organizations in multiple countries. [...]
