Title: TELUS investigating leak of stolen source code, employee data
Date: 2023-02-23T21:54:58-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-02-23-telus-investigating-leak-of-stolen-source-code-employee-data

[Source](https://www.bleepingcomputer.com/news/security/telus-investigating-leak-of-stolen-source-code-employee-data/){:target="_blank" rel="noopener"}

> Canada's second-largest telecom, TELUS is investigating a potential data breach after a threat actor shared samples online of what appears to be employee data. The threat actor subsequently shared screenshots apparently showing private source code repositories and payroll records held by the company. [...]
