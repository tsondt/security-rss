Title: Who’s Behind the Botnet-Based Service BHProxies?
Date: 2023-02-24T19:51:23+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Web Fraud 2.0;Abdala Tawfik;Abdalla Khafagy;BHProxies;BitSight;Constella Intelligence;hassan_isabad_subar;Intel 471;LewkLabs;Minerva Labs;Mylobot
Slug: 2023-02-24-whos-behind-the-botnet-based-service-bhproxies

[Source](https://krebsonsecurity.com/2023/02/whos-behind-the-botnet-based-service-bhproxies/){:target="_blank" rel="noopener"}

> A security firm has discovered that a six-year-old crafty botnet known as Mylobot appears to be powering a residential proxy service called BHProxies, which offers paying customers the ability to route their web traffic anonymously through compromised computers. Here’s a closer look at Mylobot, and a deep dive into who may be responsible for operating the BHProxies service. The BHProxies website. First identified in 2017 by the security firm Deep Instinct, Mylobot employs a number of fairly sophisticated methods to remain undetected on infected hosts, such as running exclusively in the computer’s temporary memory, and waiting 14 days before attempting [...]
