Title: New Protections for Food Benefits Stolen by Skimmers
Date: 2023-02-17T21:09:07+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;All About Skimmers;The Coming Storm;Consolidated Appropriations Act of 2023;Deborah Harris;EBT fraud;EBT skimming;Electronic Benefits Transfer;Homeless Persons Representation Project;Michelle Salomon Madaio;The Massachusetts Law Reform Institute;U.S. Department of Agriculture
Slug: 2023-02-17-new-protections-for-food-benefits-stolen-by-skimmers

[Source](https://krebsonsecurity.com/2023/02/new-protections-for-food-benefits-stolen-by-skimmers/){:target="_blank" rel="noopener"}

> Millions of Americans receiving food assistance benefits just earned a new right that they can’t yet enforce: The right to be reimbursed if funds on their Electronic Benefit Transfer (EBT) cards are stolen by card skimming devices secretly installed at cash machines and grocery store checkout lanes. On December 29, 2022, President Biden signed into law the Consolidated Appropriations Act of 2023, which — for the first time ever — includes provisions for the replacement of stolen EBT benefits. This is a big deal because in 2022, organized crime groups began massively targeting EBT accounts — often emptying affected accounts [...]
