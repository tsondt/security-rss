Title: 'Ethical hacker' among ransomware suspects cuffed by Dutch cops
Date: 2023-02-25T09:04:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-25-ethical-hacker-among-ransomware-suspects-cuffed-by-dutch-cops

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/25/dutch_ransomware_arrest_hacker/){:target="_blank" rel="noopener"}

> Beware the Dark Side Dutch police have arrested three men for their alleged involvement with a ransomware gang that stole sensitive data and extorted hundreds of thousands of euros from thousands of companies.... [...]
