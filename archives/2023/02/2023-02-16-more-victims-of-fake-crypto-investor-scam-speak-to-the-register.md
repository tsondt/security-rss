Title: More victims of fake crypto investor scam speak to The Register
Date: 2023-02-16T18:30:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-02-16-more-victims-of-fake-crypto-investor-scam-speak-to-the-register

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/16/fake_crypto_investor_scam/){:target="_blank" rel="noopener"}

> UK-based Coin Publishers were conned out of $206,000 after meeting in a Barcelona hotel Exclusive When Ahad Shams detailed on Twitter how his company was scammed out of $4 million in cryptocurrency after a face-to-face meeting, Chris Hunter immediately recognized what was going on.... [...]
