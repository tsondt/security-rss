Title: Accidental WhatsApp account takeovers? It's a thing
Date: 2023-02-21T11:00:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-21-accidental-whatsapp-account-takeovers-its-a-thing

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/21/accidental_whatsapp_account_takeover/){:target="_blank" rel="noopener"}

> Blame it on phone number recycling (yes, that's a thing, too) A stranger may be receiving your private WhatsApp messages, and also be able to send messages to all of your contacts – if you have changed your phone number and didn't delete the WhatsApp account linked to it.... [...]
