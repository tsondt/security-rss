Title: Medusa botnet returns as a Mirai-based variant with ransomware sting
Date: 2023-02-07T13:00:54-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-07-medusa-botnet-returns-as-a-mirai-based-variant-with-ransomware-sting

[Source](https://www.bleepingcomputer.com/news/security/medusa-botnet-returns-as-a-mirai-based-variant-with-ransomware-sting/){:target="_blank" rel="noopener"}

> A new version of the Medusa DDoS (distributed denial of service) botnet, based on Mirai code, has appeared in the wild, featuring a ransomware module and a Telnet brute-forcer. [...]
