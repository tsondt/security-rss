Title: LastPass: DevOps engineer hacked to steal password vault data in 2022 breach
Date: 2023-02-27T20:40:56-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-02-27-lastpass-devops-engineer-hacked-to-steal-password-vault-data-in-2022-breach

[Source](https://www.bleepingcomputer.com/news/security/lastpass-devops-engineer-hacked-to-steal-password-vault-data-in-2022-breach/){:target="_blank" rel="noopener"}

> LastPass revealed more information on a "coordinated second attack," where a threat actor accessed and stole data from the Amazon AWS cloud storage servers for over two months. [...]
