Title: OAuth ‘masterclass’ crowned top web hacking technique of 2022
Date: 2023-02-10T14:56:50+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-02-10-oauth-masterclass-crowned-top-web-hacking-technique-of-2022

[Source](https://portswigger.net/daily-swig/oauth-masterclass-crowned-top-web-hacking-technique-of-2022){:target="_blank" rel="noopener"}

> Single sign-on and request smuggling to the fore in another stellar year for web security research [...]
