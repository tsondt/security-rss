Title: European Commission bans TikTok from staff gadgets
Date: 2023-02-24T07:27:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-02-24-european-commission-bans-tiktok-from-staff-gadgets

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/24/european_commission_bans_on_tiktok/){:target="_blank" rel="noopener"}

> Cyber Europe cyber worried about cyber threats, doesn't cyber use the other C word (China) The European Commission on Thursday banned the use of the TikTok short video app on corporate devices and on the personal devices of employees enrolled in the commission's mobile device management service.... [...]
