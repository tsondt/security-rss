Title: Clop ransomware flaw allowed Linux victims to recover files for months
Date: 2023-02-07T06:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Linux
Slug: 2023-02-07-clop-ransomware-flaw-allowed-linux-victims-to-recover-files-for-months

[Source](https://www.bleepingcomputer.com/news/security/clop-ransomware-flaw-allowed-linux-victims-to-recover-files-for-months/){:target="_blank" rel="noopener"}

> The Clop ransomware gang is now also using a malware variant that explicitly targets Linux servers, but a flaw in the encryption scheme has allowed victims to quietly recover their files for free for months. [...]
