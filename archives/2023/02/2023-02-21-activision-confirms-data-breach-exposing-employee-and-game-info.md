Title: Activision confirms data breach exposing employee and game info
Date: 2023-02-21T14:14:40-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2023-02-21-activision-confirms-data-breach-exposing-employee-and-game-info

[Source](https://www.bleepingcomputer.com/news/security/activision-confirms-data-breach-exposing-employee-and-game-info/){:target="_blank" rel="noopener"}

> Activision has confirmed that it suffered a data breach in December 2022 after one of its employees fell victim to an SMS phishing attack, giving hackers access to its internal systems. [...]
