Title: Antivirus apps are there to protect you – Cisco's ClamAV has a heckuva flaw
Date: 2023-02-17T06:02:15+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-02-17-antivirus-apps-are-there-to-protect-you-ciscos-clamav-has-a-heckuva-flaw

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/17/cisco_clamav_critical_flaw/){:target="_blank" rel="noopener"}

> Switchzilla hardware and software need attention, unless you fancy arbitrary remote code execution Antivirus software is supposed to be an important part of an organization's defense against the endless tide of malware.... [...]
