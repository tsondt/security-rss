Title: Emsisoft says hackers are spoofing its certs to breach networks
Date: 2023-02-15T12:01:28-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-15-emsisoft-says-hackers-are-spoofing-its-certs-to-breach-networks

[Source](https://www.bleepingcomputer.com/news/security/emsisoft-says-hackers-are-spoofing-its-certs-to-breach-networks/){:target="_blank" rel="noopener"}

> ​A hacker is using fake code-signing certificates impersonating cybersecurity firm Emsisoft to target customers using its security products, hoping to bypass their defenses. [...]
