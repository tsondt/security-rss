Title: Google lets a few Android devices into its Privacy Sandbox
Date: 2023-02-14T17:00:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-02-14-google-lets-a-few-android-devices-into-its-privacy-sandbox

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/14/google_privacy_sandox_beta/){:target="_blank" rel="noopener"}

> Chocolate Factory's ad tech renovation is moving ahead, like it or not Google on Tuesday began rolling out a beta test of its Privacy Sandbox software for a small portion of Android 13 devices to learn how its purportedly privacy-protecting ad tech actually performs.... [...]
