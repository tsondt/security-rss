Title: California medical group data breach impacts 3.3 million patients
Date: 2023-02-10T12:36:22-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-02-10-california-medical-group-data-breach-impacts-33-million-patients

[Source](https://www.bleepingcomputer.com/news/security/california-medical-group-data-breach-impacts-33-million-patients/){:target="_blank" rel="noopener"}

> Multiple medical groups in the Heritage Provider Network in California have suffered a ransomware attack, exposing sensitive patient information to cybercriminals. [...]
