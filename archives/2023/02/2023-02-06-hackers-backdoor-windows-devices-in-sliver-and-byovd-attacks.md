Title: Hackers backdoor Windows devices in Sliver and BYOVD attacks
Date: 2023-02-06T16:00:04-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-06-hackers-backdoor-windows-devices-in-sliver-and-byovd-attacks

[Source](https://www.bleepingcomputer.com/news/security/hackers-backdoor-windows-devices-in-sliver-and-byovd-attacks/){:target="_blank" rel="noopener"}

> A new hacking campaign exploits Sunlogin flaws to deploy the Sliver post-exploitation toolkit and launch Windows Bring Your Own Vulnerable Driver (BYOVD) attacks to disable security software. [...]
