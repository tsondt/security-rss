Title: Intel patches up SGX best it can after another load of security holes found
Date: 2023-02-15T20:40:11+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2023-02-15-intel-patches-up-sgx-best-it-can-after-another-load-of-security-holes-found

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/15/intel_sgx_vulns/){:target="_blank" rel="noopener"}

> Plus bugs squashed in Server Platform Services and more Intel's Software Guard Extensions (SGX) are under the spotlight again after the chipmaker disclosed several newly discovered vulnerabilities affecting the tech, and recommended users update their firmware.... [...]
