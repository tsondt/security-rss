Title: Russian charged with smuggling US counterintel tech to Motherland
Date: 2023-02-27T11:30:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-02-27-russian-charged-with-smuggling-us-counterintel-tech-to-motherland

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/27/in_brief_security/){:target="_blank" rel="noopener"}

> Also, don't download that 'ChatGPT Windows client,' and this week's critical vulnerabilities to keep an eye on In brief A Russian national has been hit with a five-count indictment alleging he smuggled hardware and software used for counterintelligence operations out of the US to the Russian Federal Security Service (FSB) and North Korea.... [...]
