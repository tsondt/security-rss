Title: Malicious Google ads sneak AWS phishing sites into search results
Date: 2023-02-09T13:37:46-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-02-09-malicious-google-ads-sneak-aws-phishing-sites-into-search-results

[Source](https://www.bleepingcomputer.com/news/security/malicious-google-ads-sneak-aws-phishing-sites-into-search-results/){:target="_blank" rel="noopener"}

> A new phishing campaign targeting Amazon Web Services (AWS) logins is abusing Google ads to sneak phishing sites into Google Search to steal your login credentials. [...]
