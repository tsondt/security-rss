Title: Intruder alert: FBI tackles 'isolated' IT security breach
Date: 2023-02-17T22:30:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-02-17-intruder-alert-fbi-tackles-isolated-it-security-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2023/02/17/fbi_security_incident/){:target="_blank" rel="noopener"}

> Move along, totally nothing to see here The FBI claims it has dealt with a cybersecurity "incident" that reportedly involved computer systems being used to investigate child sexual exploitation.... [...]
