Title: U.S. and U.K. sanction TrickBot and Conti ransomware operation members
Date: 2023-02-09T10:21:02-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-02-09-us-and-uk-sanction-trickbot-and-conti-ransomware-operation-members

[Source](https://www.bleepingcomputer.com/news/security/us-and-uk-sanction-trickbot-and-conti-ransomware-operation-members/){:target="_blank" rel="noopener"}

> The United States and the United Kingdom have sanctioned seven Russian individuals for their involvement in the TrickBot cybercrime group, whose malware was used to support attacks by the Conti and Ryuk ransomware operation. [...]
