Title: Friday Squid Blogging: Squid Is a Blockchain Thingy
Date: 2023-02-10T22:11:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;blockchain;squid
Slug: 2023-02-10-friday-squid-blogging-squid-is-a-blockchain-thingy

[Source](https://www.schneier.com/blog/archives/2023/02/friday-squid-blogging-squid-is-a-blockchain-thingy.html){:target="_blank" rel="noopener"}

> I had no idea—until I read this incredibly jargon-filled article: Squid is a cross-chain liquidity and messaging router that swaps across multiple chains and their native DEXs via axlUSDC. So there. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
