Title: Updated ebook: Protecting your AWS environment from ransomware
Date: 2023-02-10T19:08:35+00:00
Author: Megan O'Neil
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS security;ransomware;security best practices;Security Blog;Top 10
Slug: 2023-02-10-updated-ebook-protecting-your-aws-environment-from-ransomware

[Source](https://aws.amazon.com/blogs/security/updated-ebook-protecting-your-aws-environment-from-ransomware/){:target="_blank" rel="noopener"}

> Amazon Web Services is excited to announce that we’ve updated the AWS ebook, Protecting your AWS environment from ransomware. The new ebook includes the top 10 best practices for ransomware protection and covers new services and features that have been released since the original published date in April 2020. We know that customers care about [...]
