Title: Microsoft support 'cracks' Windows for customer after activation fails
Date: 2023-03-16T08:14:42-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-03-16-microsoft-support-cracks-windows-for-customer-after-activation-fails

[Source](https://www.bleepingcomputer.com/news/security/microsoft-support-cracks-windows-for-customer-after-activation-fails/){:target="_blank" rel="noopener"}

> In an unexpected twist, a Microsoft support engineer resorted to running an unofficial 'crack' on a customer's Windows PC after a genuine copy of the operating system failed to activate normally. It seems, this isn't the first time either that a Microsoft support professional has employed such workarounds. [...]
