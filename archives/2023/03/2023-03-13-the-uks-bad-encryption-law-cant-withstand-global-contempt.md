Title: The UK's bad encryption law can't withstand global contempt
Date: 2023-03-13T10:32:14+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2023-03-13-the-uks-bad-encryption-law-cant-withstand-global-contempt

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/13/column/){:target="_blank" rel="noopener"}

> Any sufficiently stupid technology is indistinguishable from magical thinking Opinion Around the world, a vital technology is failing. Just as massive solar flares fry satellites and climate-change superstorms overwhelm flood defences, so a new surge of ridiculous IT-related events is burning out irony meters across the globe.... [...]
