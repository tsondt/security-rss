Title: GitHub publishes RSA SSH host keys by mistake, issues update
Date: 2023-03-24T13:34:27+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2023-03-24-github-publishes-rsa-ssh-host-keys-by-mistake-issues-update

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/24/github_changes_its_ssh_host/){:target="_blank" rel="noopener"}

> Getting connection failures? Don't panic. Get new keys GitHub has updated its SSH keys after accidentally publishing the private part to the world. Whoops.... [...]
