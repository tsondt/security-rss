Title: Windows 11 Snipping Tool privacy bug exposes cropped image content
Date: 2023-03-21T17:32:57-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-03-21-windows-11-snipping-tool-privacy-bug-exposes-cropped-image-content

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-11-snipping-tool-privacy-bug-exposes-cropped-image-content/){:target="_blank" rel="noopener"}

> A severe privacy flaw named 'acropalypse' has also been found to affect the Windows Snipping Tool, allowing people to partially recover content that was edited out of an image. [...]
