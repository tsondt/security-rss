Title: UK expected to ban TikTok from government mobile phones
Date: 2023-03-15T13:59:22+00:00
Author: Dan Sabbagh, Dan Milmo and Safi Bugel
Category: The Guardian
Tags: TikTok;Technology;China;Department for International Development (DfID);Defence policy;Politics;Asia Pacific;UK news;World news;Data and computer security
Slug: 2023-03-15-uk-expected-to-ban-tiktok-from-government-mobile-phones

[Source](https://www.theguardian.com/technology/2023/mar/15/uk-expected-to-ban-tiktok-from-government-mobile-phones){:target="_blank" rel="noopener"}

> Ban on Chinese owned video-sharing app marks U-turn from previous relaxed position Britain is expected to announce a ban on the Chinese owned video-sharing app TikTok on government mobile phones imminently, bringing the UK inline with the US and European Commission and reflecting deteriorating relations with Beijing. The decision marks a sharp reverse from the UK’s previously relaxed position, but some critics and experts said Britain should also extend the ban to cover personal phones used by ministers and officials – and even consider a complete ban. Continue reading... [...]
