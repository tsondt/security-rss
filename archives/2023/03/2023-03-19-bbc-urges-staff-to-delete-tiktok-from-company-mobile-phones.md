Title: BBC urges staff to delete TikTok from company mobile phones
Date: 2023-03-19T13:09:18+00:00
Author: Matthew Weaver and Dan Milmo
Category: The Guardian
Tags: TikTok;BBC;Technology;Social media;Digital media;UK news;China;Data and computer security
Slug: 2023-03-19-bbc-urges-staff-to-delete-tiktok-from-company-mobile-phones

[Source](https://www.theguardian.com/technology/2023/mar/19/bbc-urges-staff-to-delete-tiktok-from-company-mobile-phones){:target="_blank" rel="noopener"}

> Move comes after UK government bans app on government devices over fears of data being accessed by Chinese state The BBC has urged its staff to delete the Chinese-own social media app TikTok from corporate mobile phones. Guidance to BBC staff circulated on Sunday said: “We don’t recommend installing TikTok on a BBC corporate device unless there is a justified business reason. If you do not need TikTok for business reasons, TikTok should be deleted.” Continue reading... [...]
