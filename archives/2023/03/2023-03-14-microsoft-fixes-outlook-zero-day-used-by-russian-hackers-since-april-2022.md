Title: Microsoft fixes Outlook zero-day used by Russian hackers since April 2022
Date: 2023-03-14T15:11:34-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-03-14-microsoft-fixes-outlook-zero-day-used-by-russian-hackers-since-april-2022

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-fixes-outlook-zero-day-used-by-russian-hackers-since-april-2022/){:target="_blank" rel="noopener"}

> Microsoft has patched an Outlook zero-day vulnerability (CVE-2023-23397) exploited by a hacking group linked to Russia's military intelligence service GRU to target European organizations. [...]
