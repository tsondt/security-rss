Title: India floats idea of dedicated tribunal to handle online offences
Date: 2023-03-13T07:58:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-03-13-india-floats-idea-of-dedicated-tribunal-to-handle-online-offences

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/13/consultation_for_digital_india_act/){:target="_blank" rel="noopener"}

> Consultation for the long-awaited Digital India Act is finally under way although the draft law's still not been revealed India's government has started to consult some proposed details of its long-awaited Digital India Act, including a declaration that the bill needed a dedicated adjudicatory tool for offenses committed online.... [...]
