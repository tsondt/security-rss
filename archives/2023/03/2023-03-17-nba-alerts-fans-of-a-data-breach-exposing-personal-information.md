Title: NBA alerts fans of a data breach exposing personal information
Date: 2023-03-17T16:21:13-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-17-nba-alerts-fans-of-a-data-breach-exposing-personal-information

[Source](https://www.bleepingcomputer.com/news/security/nba-alerts-fans-of-a-data-breach-exposing-personal-information/){:target="_blank" rel="noopener"}

> The NBA (National Basketball Association) is notifying fans of a data breach after some of their personal information, "held" by a third-party newsletter service, was stolen. [...]
