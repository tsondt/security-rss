Title: NordVPN makes its Meshnet private tunnel free for everyone
Date: 2023-03-13T19:01:33-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software;Technology
Slug: 2023-03-13-nordvpn-makes-its-meshnet-private-tunnel-free-for-everyone

[Source](https://www.bleepingcomputer.com/news/security/nordvpn-makes-its-meshnet-private-tunnel-free-for-everyone/){:target="_blank" rel="noopener"}

> NordVPN's Meshnet private tunnel feature for Windows, macOS, and Linux is now free for everyone, even users who do not have a subscription to NordVPN. [...]
