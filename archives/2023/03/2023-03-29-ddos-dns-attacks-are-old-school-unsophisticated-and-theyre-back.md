Title: DDoS DNS attacks are old-school, unsophisticated … and they’re back
Date: 2023-03-29T08:34:14+00:00
Author: Joseph Martins
Category: The Register
Tags: 
Slug: 2023-03-29-ddos-dns-attacks-are-old-school-unsophisticated-and-theyre-back

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/29/ddos_dns_attacks_are_oldschool/){:target="_blank" rel="noopener"}

> So why would you handle them on your own? Sponsored Feature Ransomware may currently be the biggest bogeyman for cybersecurity pros, law enforcement, and governments, but it shouldn't divert us from more traditional, but still very disruptive threats.... [...]
