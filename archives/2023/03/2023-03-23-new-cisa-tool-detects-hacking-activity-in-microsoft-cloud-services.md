Title: New CISA tool detects hacking activity in Microsoft cloud services
Date: 2023-03-23T14:34:26-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-03-23-new-cisa-tool-detects-hacking-activity-in-microsoft-cloud-services

[Source](https://www.bleepingcomputer.com/news/security/new-cisa-tool-detects-hacking-activity-in-microsoft-cloud-services/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity & Infrastructure Security Agency (CISA) has released a new open-source incident response tool that helps detect signs of malicious activity in Microsoft cloud environments. [...]
