Title: Dumb Password Rules
Date: 2023-03-02T12:05:42+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;complexity;passwords
Slug: 2023-03-02-dumb-password-rules

[Source](https://www.schneier.com/blog/archives/2023/03/dumb-password-rules.html){:target="_blank" rel="noopener"}

> Examples of dumb password rules. There are some pretty bad disasters out there. My worst experiences are with sites that have artificial complexity requirements that cause my personal password-generation systems to fail. Some of the systems on the list are even worse: when they fail they don’t tell you why, so you just have to guess until you get it right. [...]
