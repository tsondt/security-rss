Title: Zoll Medical says intruders had 1M+ patient, staff records at their fingertips
Date: 2023-03-13T21:30:10+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-03-13-zoll-medical-says-intruders-had-1m-patient-staff-records-at-their-fingertips

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/13/zoll_medical_data_intrusion/){:target="_blank" rel="noopener"}

> Names, addresses, SSNs all up for grabs Medical device and software maker Zoll Medical says the personal and health information of more than a million people, including patients and employees, may have been stolen by crooks in January.... [...]
