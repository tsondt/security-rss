Title: PlugX RAT masquerades as legit Windows debugger to slip past security
Date: 2023-03-01T07:30:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-03-01-plugx-rat-masquerades-as-legit-windows-debugger-to-slip-past-security

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/01/plugx_dll_loading_malware/){:target="_blank" rel="noopener"}

> DLL side-loading does the trick, again Cybercriminals are disguising the PlugX remote access trojan as a legitimate open-source Windows debugging tool to evade detection and compromise systems.... [...]
