Title: The Week in Ransomware - March 24th 2023 - Clop overload
Date: 2023-03-24T17:32:28-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-03-24-the-week-in-ransomware-march-24th-2023-clop-overload

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-march-24th-2023-clop-overload/){:target="_blank" rel="noopener"}

> This week's news has been dominated by the Clop ransomware gang extorting companies whose GoAnywhere services were breached using a zero-day vulnerability. [...]
