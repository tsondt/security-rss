Title: Ukrainian cyberpolice busts fraud gang that stole $4.3 million
Date: 2023-03-30T16:29:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-30-ukrainian-cyberpolice-busts-fraud-gang-that-stole-43-million

[Source](https://www.bleepingcomputer.com/news/security/ukrainian-cyberpolice-busts-fraud-gang-that-stole-43-million/){:target="_blank" rel="noopener"}

> Ukraine's cyberpolice has arrested members of a fraud gang that stole roughly $4,300,000 from over a thousand victims across the EU. [...]
