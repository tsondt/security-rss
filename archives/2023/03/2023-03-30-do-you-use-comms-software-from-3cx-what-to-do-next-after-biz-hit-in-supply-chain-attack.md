Title: Do you use comms software from 3CX? What to do next after biz hit in supply chain attack
Date: 2023-03-30T16:25:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-03-30-do-you-use-comms-software-from-3cx-what-to-do-next-after-biz-hit-in-supply-chain-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/30/communications_software_vendor_3cx_hit/){:target="_blank" rel="noopener"}

> Miscreants hit downstream customers with infostealers Two security firms have found what they believe to be a supply chain attack on communications software maker 3CX – and the vendor's boss is advising users to switch to the progressive web app until the 3CX desktop client is updated.... [...]
