Title: Bing search results hijacked via misconfigured Microsoft app
Date: 2023-03-30T13:05:42-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-03-30-bing-search-results-hijacked-via-misconfigured-microsoft-app

[Source](https://www.bleepingcomputer.com/news/security/bing-search-results-hijacked-via-misconfigured-microsoft-app/){:target="_blank" rel="noopener"}

> A misconfigured Microsoft application allowed anyone to log in and modify Bing.com search results in real-time, as well as inject XSS attacks to potentially breach the accounts of Office 365 users. [...]
