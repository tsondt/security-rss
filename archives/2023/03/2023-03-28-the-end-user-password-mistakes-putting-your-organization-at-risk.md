Title: The End-User Password Mistakes Putting Your Organization at Risk
Date: 2023-03-28T10:07:14-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-03-28-the-end-user-password-mistakes-putting-your-organization-at-risk

[Source](https://www.bleepingcomputer.com/news/security/the-end-user-password-mistakes-putting-your-organization-at-risk/){:target="_blank" rel="noopener"}

> Though there are many ways to create passwords, not all are equally effective. It is important to consider the various ways a password-protected system can fail. [...]
