Title: How to use Amazon GuardDuty and AWS WAF v2 to automatically block suspicious hosts
Date: 2023-03-29T18:13:22+00:00
Author: Eucke Warren
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;Amazon CloudWatch;Amazon EventBridge;Amazon GuardDuty;AWS WAF;Security Blog
Slug: 2023-03-29-how-to-use-amazon-guardduty-and-aws-waf-v2-to-automatically-block-suspicious-hosts

[Source](https://aws.amazon.com/blogs/security/how-to-use-amazon-guardduty-and-aws-waf-v2-to-automatically-block-suspicious-hosts/){:target="_blank" rel="noopener"}

> In this post, we’ll share an automation pattern that you can use to automatically detect and block suspicious hosts that are attempting to access your Amazon Web Services (AWS) resources. The automation will rely on Amazon GuardDuty to generate findings about the suspicious hosts, and then you can respond to those findings by programmatically updating [...]
