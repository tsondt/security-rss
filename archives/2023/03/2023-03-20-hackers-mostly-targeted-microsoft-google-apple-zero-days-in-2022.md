Title: Hackers mostly targeted Microsoft, Google, Apple zero-days in 2022
Date: 2023-03-20T13:08:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-20-hackers-mostly-targeted-microsoft-google-apple-zero-days-in-2022

[Source](https://www.bleepingcomputer.com/news/security/hackers-mostly-targeted-microsoft-google-apple-zero-days-in-2022/){:target="_blank" rel="noopener"}

> Hackers continue to target zero-day vulnerabilities in malicious campaigns, with researchers reporting that 55 zero-days were actively exploited in 2022, most targeting Microsoft, Google, and Apple products. [...]
