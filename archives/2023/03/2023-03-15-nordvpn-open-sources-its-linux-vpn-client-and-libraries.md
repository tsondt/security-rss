Title: NordVPN open sources its Linux VPN client and libraries
Date: 2023-03-15T17:34:37-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Linux;Security;Software
Slug: 2023-03-15-nordvpn-open-sources-its-linux-vpn-client-and-libraries

[Source](https://www.bleepingcomputer.com/news/security/nordvpn-open-sources-its-linux-vpn-client-and-libraries/){:target="_blank" rel="noopener"}

> Nord Security (Nord) has released the source code of its Linux NordVPN client and associated networking libraries in the hopes of being more transparent and easing users' security and privacy concerns. [...]
