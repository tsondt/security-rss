Title: The Security Vulnerabilities of Message Interoperability
Date: 2023-03-29T11:03:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;authentication;economics of security;privacy;psychology of security
Slug: 2023-03-29-the-security-vulnerabilities-of-message-interoperability

[Source](https://www.schneier.com/blog/archives/2023/03/the-security-vulnerabilities-of-message-interoperability.html){:target="_blank" rel="noopener"}

> Jenny Blessing and Ross Anderson have evaluated the security of systems designed to allow the various Internet messaging platforms to interoperate with each other: The Digital Markets Act ruled that users on different platforms should be able to exchange messages with each other. This opens up a real Pandora’s box. How will the networks manage keys, authenticate users, and moderate content? How much metadata will have to be shared, and how? In our latest paper, One Protocol to Rule Them All? On Securing Interoperable Messaging, we explore the security tensions, the conflicts of interest, the usability traps, and the likely [...]
