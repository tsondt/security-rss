Title: Friday Squid Blogging: We’re Almost at Flying Squid Drones
Date: 2023-03-03T22:23:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;drones;squid
Slug: 2023-03-03-friday-squid-blogging-were-almost-at-flying-squid-drones

[Source](https://www.schneier.com/blog/archives/2023/03/friday-squid-blogging-were-almost-at-flying-squid-drones.html){:target="_blank" rel="noopener"}

> Researchers are prototyping multi-segment shapeshifter drones, which are “the precursors to flying squid-bots.” As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
