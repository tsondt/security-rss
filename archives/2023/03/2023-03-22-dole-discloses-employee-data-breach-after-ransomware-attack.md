Title: Dole discloses employee data breach after ransomware attack
Date: 2023-03-22T15:04:35-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-22-dole-discloses-employee-data-breach-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/dole-discloses-employee-data-breach-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Fresh produce giant Dole Food Company has confirmed that the information of an undisclosed number of employees was accessed during a February ransomware attack. [...]
