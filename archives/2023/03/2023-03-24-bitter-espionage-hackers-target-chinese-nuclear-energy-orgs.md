Title: 'Bitter' espionage hackers target Chinese nuclear energy orgs
Date: 2023-03-24T10:47:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-24-bitter-espionage-hackers-target-chinese-nuclear-energy-orgs

[Source](https://www.bleepingcomputer.com/news/security/bitter-espionage-hackers-target-chinese-nuclear-energy-orgs/){:target="_blank" rel="noopener"}

> A cyberespionage hacking group tracked as 'Bitter APT' was recently seen targeting the Chinese nuclear energy industry using phishing emails to infect devices with malware downloaders. [...]
