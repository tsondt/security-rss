Title: Mental health provider Cerebral alerts 3.1M people of data breach
Date: 2023-03-10T10:43:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-03-10-mental-health-provider-cerebral-alerts-31m-people-of-data-breach

[Source](https://www.bleepingcomputer.com/news/security/mental-health-provider-cerebral-alerts-31m-people-of-data-breach/){:target="_blank" rel="noopener"}

> Healthcare platform Cerebral is sending data breach notices to 3.18 million people who have interacted with its websites, applications, and telehealth services. [...]
