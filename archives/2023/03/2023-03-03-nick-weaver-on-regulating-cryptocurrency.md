Title: Nick Weaver on Regulating Cryptocurrency
Date: 2023-03-03T15:58:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2023-03-03-nick-weaver-on-regulating-cryptocurrency

[Source](https://www.schneier.com/blog/archives/2023/03/nick-weaver-on-regulating-cryptocurrency.html){:target="_blank" rel="noopener"}

> Nicholas Weaver wrote an excellent paper on the problems of cryptocurrencies and the need to regulate the space—with all existing regulations. His conclusion: Regulators, especially regulators in the United States, often fear accusations of stifling innovation. As such, the cryptocurrency space has grown over the past decade with very little regulatory oversight. But fortunately for regulators, there is no actual innovation to stifle. Cryptocurrencies cannot revolutionize payments or finance, as the basic nature of all cryptocurrencies render them fundamentally unsuitable to revolutionize our financial system—which, by the way, already has decades of successful experience with digital payments and electronic money. [...]
