Title: Friday Squid Blogging: Giant Squid vs. Blue Marlin
Date: 2023-03-31T21:08:24+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-03-31-friday-squid-blogging-giant-squid-vs-blue-marlin

[Source](https://www.schneier.com/blog/archives/2023/03/friday-squid-blogging-giant-squid-vs-blue-marlin.html){:target="_blank" rel="noopener"}

> Epic matchup. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
