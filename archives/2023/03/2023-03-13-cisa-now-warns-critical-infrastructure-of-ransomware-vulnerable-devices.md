Title: CISA now warns critical infrastructure of ransomware-vulnerable devices
Date: 2023-03-13T14:34:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-13-cisa-now-warns-critical-infrastructure-of-ransomware-vulnerable-devices

[Source](https://www.bleepingcomputer.com/news/security/cisa-now-warns-critical-infrastructure-of-ransomware-vulnerable-devices/){:target="_blank" rel="noopener"}

> Today, the U.S. Cybersecurity & Infrastructure Security Agency (CISA) announced a new pilot program designed to help critical infrastructure entities protect their information systems from ransomware attacks. [...]
