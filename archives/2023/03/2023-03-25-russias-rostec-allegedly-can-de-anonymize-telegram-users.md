Title: Russia’s Rostec allegedly can de-anonymize Telegram users
Date: 2023-03-25T10:07:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-25-russias-rostec-allegedly-can-de-anonymize-telegram-users

[Source](https://www.bleepingcomputer.com/news/security/russia-s-rostec-allegedly-can-de-anonymize-telegram-users/){:target="_blank" rel="noopener"}

> Russia's Rostec has reportedly bought a platform that allows it to uncover the identities of anonymous Telegram users, likely to be used to tamp down on unfavorable news out of the country. [...]
