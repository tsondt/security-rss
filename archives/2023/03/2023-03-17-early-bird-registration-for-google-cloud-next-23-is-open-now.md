Title: Early-bird registration for Google Cloud Next ‘23 is open now
Date: 2023-03-17T16:00:00+00:00
Author: Matt Kaufman
Category: GCP Security
Tags: Application Development;AI & Machine Learning;Security & Identity;Data Analytics;Google Cloud Next
Slug: 2023-03-17-early-bird-registration-for-google-cloud-next-23-is-open-now

[Source](https://cloud.google.com/blog/topics/google-cloud-next/early-bird-registration-open-for-google-cloud-next-2023/){:target="_blank" rel="noopener"}

> San Francisco, here we come. Starting today, you can now register at the Early Bird rate of $899 USD* for Google Cloud Next ‘23, taking place in person, August 29-31, 2023. This year’s Next conference comes at an exciting time. The emergence of generative AI is a transformational opportunity that some say may be as meaningful as the cloud itself. Beyond generative AI, there are breakthroughs in cybersecurity, better and smarter ways to gather and gain insights from data, advances in application development, and so much more. It’s clear that there has never been a better time to work in [...]
