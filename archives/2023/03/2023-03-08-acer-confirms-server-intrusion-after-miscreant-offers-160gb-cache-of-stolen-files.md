Title: Acer confirms server intrusion after miscreant offers 160GB cache of stolen files
Date: 2023-03-08T01:12:00+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-08-acer-confirms-server-intrusion-after-miscreant-offers-160gb-cache-of-stolen-files

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/08/acer_confirms_server_breach/){:target="_blank" rel="noopener"}

> Customer info safe, or so we're told Acer has confirmed someone broke into one of its servers after a miscreant put up for sale a 160GB database of what's claimed to be the Taiwanese PC maker's confidential information.... [...]
