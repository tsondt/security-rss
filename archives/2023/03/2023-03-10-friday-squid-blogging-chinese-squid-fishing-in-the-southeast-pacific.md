Title: Friday Squid Blogging: Chinese Squid Fishing in the Southeast Pacific
Date: 2023-03-10T22:05:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-03-10-friday-squid-blogging-chinese-squid-fishing-in-the-southeast-pacific

[Source](https://www.schneier.com/blog/archives/2023/03/friday-squid-blogging-chinese-squid-fishing-in-the-southeast-pacific.html){:target="_blank" rel="noopener"}

> Chinese squid fishing boats are overwhelming Ecuador and Peru. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
