Title: Aussie tech worker payroll scheme operators found guilty of tax fraud
Date: 2023-03-08T04:04:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-03-08-aussie-tech-worker-payroll-scheme-operators-found-guilty-of-tax-fraud

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/08/plutus_payroll_guilty_verdict/){:target="_blank" rel="noopener"}

> Contractors left hanging while principals splurged on luxury goods Three of the principals of an Australian scheme that offered free payroll services to tech contractors have been found guilty of conspiring to defraud the Commonwealth and conspiring to deal with the proceeds of crime.... [...]
