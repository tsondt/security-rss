Title: Uncle Sam reveals it sent cyber-soldiers to Albania to hunt for Iranian threats
Date: 2023-03-24T01:05:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-24-uncle-sam-reveals-it-sent-cyber-soldiers-to-albania-to-hunt-for-iranian-threats

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/24/us_hunt_forward_albania/){:target="_blank" rel="noopener"}

> 'Hunt forward' teams of this sort aid with defense and learn how attackers like Tehran operate US Cyber Command operators have confirmed they carried out an online defensive mission in Albania, in response to last year's cyber attacks against the local government.... [...]
