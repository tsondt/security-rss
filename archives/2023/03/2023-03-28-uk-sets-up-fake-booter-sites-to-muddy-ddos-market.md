Title: UK Sets Up Fake Booter Sites To Muddy DDoS Market
Date: 2023-03-28T17:26:07+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;DDoS-for-Hire;Web Fraud 2.0;booter;ddos-for-hire;National Crime Agency;Operation Power Off;stresser;U.S. Department of Justice
Slug: 2023-03-28-uk-sets-up-fake-booter-sites-to-muddy-ddos-market

[Source](https://krebsonsecurity.com/2023/03/uk-sets-up-fake-booter-sites-to-muddy-ddos-market/){:target="_blank" rel="noopener"}

> The United Kingdom’s National Crime Agency (NCA) has been busy setting up phony DDoS-for-hire websites that seek to collect information on users, remind them that launching DDoS attacks is illegal, and generally increase the level of paranoia for people looking to hire such services. The warning displayed to users on one of the NCA’s fake booter sites. Image: NCA. The NCA says all of its fake so-called “booter” or “stresser” sites — which have so far been accessed by several thousand people — have been created to look like they offer the tools and services that enable cyber criminals to [...]
