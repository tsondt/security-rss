Title: Hackers inject credit card stealers into payment processing modules
Date: 2023-03-22T15:55:58-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-22-hackers-inject-credit-card-stealers-into-payment-processing-modules

[Source](https://www.bleepingcomputer.com/news/security/hackers-inject-credit-card-stealers-into-payment-processing-modules/){:target="_blank" rel="noopener"}

> A new credit card stealing hacking campaign is doing things differently than we have seen in the past by hiding their malicious code inside the 'Authorize.net' payment gateway module for WooCommcerce, allowing the breach to evade detection by security scans. [...]
