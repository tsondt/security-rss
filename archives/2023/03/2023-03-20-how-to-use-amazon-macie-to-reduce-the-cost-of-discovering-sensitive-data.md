Title: How to use Amazon Macie to reduce the cost of discovering sensitive data
Date: 2023-03-20T17:30:10+00:00
Author: Nicholas Doropoulos
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Technical How-to;Amazon Macie;Amazon S3;Data security investigations;PCI;PHI;PII;Security Blog;Sensitive Data Discovery
Slug: 2023-03-20-how-to-use-amazon-macie-to-reduce-the-cost-of-discovering-sensitive-data

[Source](https://aws.amazon.com/blogs/security/how-to-use-amazon-macie-to-reduce-the-cost-of-discovering-sensitive-data/){:target="_blank" rel="noopener"}

> Amazon Macie is a fully managed data security service that uses machine learning and pattern matching to discover and help protect your sensitive data, such as personally identifiable information (PII), payment card data, and Amazon Web Services (AWS) credentials. Analyzing large volumes of data for the presence of sensitive information can be expensive, due to [...]
