Title: Convincing Twitter 'quote tweet' phone scam targets bank customers
Date: 2023-03-16T10:00:00-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-03-16-convincing-twitter-quote-tweet-phone-scam-targets-bank-customers

[Source](https://www.bleepingcomputer.com/news/security/convincing-twitter-quote-tweet-phone-scam-targets-bank-customers/){:target="_blank" rel="noopener"}

> A convincing Twitter scam is targeting bank customers by abusing the quote-tweets feature, as observed by BleepingComputer. The scam preys on customers tweeting to their banks—such as to raise a complaints. But these customers instead receive a reply from the scammer, via a quote-tweet, urging them to call the scammer's "helpline." [...]
