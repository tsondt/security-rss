Title: Alert: Crims hijack these DrayTek routers to attack biz
Date: 2023-03-08T00:01:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-03-08-alert-crims-hijack-these-draytek-routers-to-attack-biz

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/08/draytek_router_malware_hiatus/){:target="_blank" rel="noopener"}

> Workaround: Throw away kit? Hope there's a patch? If you're still using post-support DrayTek Vigor routers it may be time to junk them, see if they can be patched, or come up with some other workaround, as a malware variant is setting up shop in the kit.... [...]
