Title: Got Conti? Here's the ransomware cure to avoid paying up
Date: 2023-03-16T20:28:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-16-got-conti-heres-the-ransomware-cure-to-avoid-paying-up

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/16/kaspersky_conti_decryptor/){:target="_blank" rel="noopener"}

> Kaspersky cracks the code, so get busy before the next update comes Good news for ransomware victims: Kaspersky security researchers say they've cracked the Conti ransomware code and released a decryptor tool after uncovering leaked data belonging to the notorious Russian crime group.... [...]
