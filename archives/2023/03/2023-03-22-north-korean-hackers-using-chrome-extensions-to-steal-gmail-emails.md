Title: North Korean hackers using Chrome extensions to steal Gmail emails
Date: 2023-03-22T11:06:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-22-north-korean-hackers-using-chrome-extensions-to-steal-gmail-emails

[Source](https://www.bleepingcomputer.com/news/security/north-korean-hackers-using-chrome-extensions-to-steal-gmail-emails/){:target="_blank" rel="noopener"}

> A joint cybersecurity advisory from the German Federal Office for the Protection of the Constitution (BfV) and the National Intelligence Service of the Republic of Korea (NIS) warn about Kimsuky's use of Chrome extensions to steal target's Gmail emails. [...]
