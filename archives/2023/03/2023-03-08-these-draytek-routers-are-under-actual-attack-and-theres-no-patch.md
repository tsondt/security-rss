Title: These DrayTek routers are under actual attack – and there's no patch
Date: 2023-03-08T00:01:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-03-08-these-draytek-routers-are-under-actual-attack-and-theres-no-patch

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/08/draytek_router_malware_hiatus/){:target="_blank" rel="noopener"}

> Workaround: Throw away kit? If you're still using post-support DrayTek Vigor routers it may be time to junk them, or come up with some other workaround, as a cunning malware variant is setting up shop in the kit.... [...]
