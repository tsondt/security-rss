Title: A Hacker’s Mind News
Date: 2023-03-24T19:07:32+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;A Hacker's Mind;books;Schneier news
Slug: 2023-03-24-a-hackers-mind-news

[Source](https://www.schneier.com/blog/archives/2023/03/a-hackers-mind-news-2.html){:target="_blank" rel="noopener"}

> My latest book continues to sell well. Its ranking hovers between 1,500 and 2,000 on Amazon. It’s been spied in airports. Reviews are consistently good. I have been enjoying giving podcast interviews. It all feels pretty good right now. You can order a signed book from me here. For those of you in New York, I’m giving at book talk at the Ford Foundation on Thursday, April 6. Admission is free, but you have to register. [...]
