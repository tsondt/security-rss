Title: 15 million public-facing services vulnerable to CISA KEV flaws
Date: 2023-03-31T15:23:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-31-15-million-public-facing-services-vulnerable-to-cisa-kev-flaws

[Source](https://www.bleepingcomputer.com/news/security/15-million-public-facing-services-vulnerable-to-cisa-kev-flaws/){:target="_blank" rel="noopener"}

> Over 15 million publicly facing services are susceptible to at least one of the 896 vulnerabilities listed in CISA's KEV (known exploitable vulnerabilities) catalog. [...]
