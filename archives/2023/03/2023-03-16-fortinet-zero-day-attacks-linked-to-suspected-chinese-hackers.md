Title: Fortinet zero-day attacks linked to suspected Chinese hackers
Date: 2023-03-16T15:13:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-16-fortinet-zero-day-attacks-linked-to-suspected-chinese-hackers

[Source](https://www.bleepingcomputer.com/news/security/fortinet-zero-day-attacks-linked-to-suspected-chinese-hackers/){:target="_blank" rel="noopener"}

> A suspected Chinese hacking group has been linked to a series of attacks on government organizations exploiting a Fortinet zero-day vulnerability (CVE-2022-41328) to deploy malware. [...]
