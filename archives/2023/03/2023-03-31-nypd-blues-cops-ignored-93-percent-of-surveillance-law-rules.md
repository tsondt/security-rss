Title: NYPD blues: Cops ignored 93 percent of surveillance law rules
Date: 2023-03-31T20:06:35+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-03-31-nypd-blues-cops-ignored-93-percent-of-surveillance-law-rules

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/31/nypd_surveillance_rules/){:target="_blank" rel="noopener"}

> Who watches the watchmen? The Office of the Inspector General Back in July 2020, then New York City Mayor Bill de Blasio signed the Public Oversight of Surveillance Technology (POST) Act into law, which required the New York Police Department to reveal how it uses surveillance technology and to formulate surveillance policies.... [...]
