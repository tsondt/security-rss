Title: FBI and CISA warn of increasing Royal ransomware attack risks
Date: 2023-03-03T11:20:39-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-03-fbi-and-cisa-warn-of-increasing-royal-ransomware-attack-risks

[Source](https://www.bleepingcomputer.com/news/security/fbi-and-cisa-warn-of-increasing-royal-ransomware-attack-risks/){:target="_blank" rel="noopener"}

> CISA and the FBI have issued a joint advisory highlighting the increasing threat behind ongoing Royal ransomware attacks targeting many U.S. critical infrastructure sectors, including healthcare, communications, and education. [...]
