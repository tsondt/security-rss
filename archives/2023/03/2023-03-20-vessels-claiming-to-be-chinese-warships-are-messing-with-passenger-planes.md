Title: Vessels claiming to be Chinese warships are messing with passenger planes
Date: 2023-03-20T07:29:09+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-03-20-vessels-claiming-to-be-chinese-warships-are-messing-with-passenger-planes

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/20/vessels_claiming_to_be_chinese/){:target="_blank" rel="noopener"}

> Australian airline Qantas warns pilots to keep calm and carry on amid reports of satnav and altimeter jamming Australian airline Qantas issued standing orders to its pilots last week advising them that some of its fleet experienced interference on VHF stations from sources purporting to be the Chinese Military.... [...]
