Title: Fooling a Voice Authentication System with an AI-Generated Voice
Date: 2023-03-01T12:06:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;authentication;banking;biometrics;deep fake;fraud;identification;spoofing;voice recognition
Slug: 2023-03-01-fooling-a-voice-authentication-system-with-an-ai-generated-voice

[Source](https://www.schneier.com/blog/archives/2023/03/fooling-a-voice-authentication-system-with-an-ai-generated-voice.html){:target="_blank" rel="noopener"}

> A reporter used an AI synthesis of his own voice to fool the voice authentication system for Lloyd’s Bank. [...]
