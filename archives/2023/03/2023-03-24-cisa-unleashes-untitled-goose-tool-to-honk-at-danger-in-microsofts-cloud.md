Title: CISA unleashes Untitled Goose Tool to honk at danger in Microsoft's cloud
Date: 2023-03-24T19:16:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-03-24-cisa-unleashes-untitled-goose-tool-to-honk-at-danger-in-microsofts-cloud

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/24/cisa_microsoft_cloud_ransomware/){:target="_blank" rel="noopener"}

> Not a headline we expected to write today American cybersecurity officials have released an early-warning system to protect Microsoft cloud users.... [...]
