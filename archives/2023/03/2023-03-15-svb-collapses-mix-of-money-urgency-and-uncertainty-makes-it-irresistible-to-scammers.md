Title: SVB collapse's mix of money, urgency and uncertainty makes it irresistible to scammers
Date: 2023-03-15T05:46:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-03-15-svb-collapses-mix-of-money-urgency-and-uncertainty-makes-it-irresistible-to-scammers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/15/rise_in_svb_related_domain/){:target="_blank" rel="noopener"}

> Phishing, dodgy domain names, and sophisticated attacks already deployed The collapse of Silicon Valley Bank (SVB) late last week sent tremors through the global financial system, creating opportunities for short-sellers – and numerous species of scammer.... [...]
