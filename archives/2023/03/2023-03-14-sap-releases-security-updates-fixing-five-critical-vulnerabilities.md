Title: SAP releases security updates fixing five critical vulnerabilities
Date: 2023-03-14T17:08:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-14-sap-releases-security-updates-fixing-five-critical-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/sap-releases-security-updates-fixing-five-critical-vulnerabilities/){:target="_blank" rel="noopener"}

> Software vendor SAP has released security updates for 19 vulnerabilities, five rated as critical, meaning that administrators should apply them as soon as possible to mitigate the associated risks. [...]
