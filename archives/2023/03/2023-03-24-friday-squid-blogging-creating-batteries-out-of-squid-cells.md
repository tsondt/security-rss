Title: Friday Squid Blogging: Creating Batteries Out of Squid Cells
Date: 2023-03-24T21:06:29+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-03-24-friday-squid-blogging-creating-batteries-out-of-squid-cells

[Source](https://www.schneier.com/blog/archives/2023/03/friday-squid-blogging-creating-batteries-out-of-squid-cells.html){:target="_blank" rel="noopener"}

> This is fascinating : “When a squid ends up chipping what’s called its ring tooth, which is the nail underneath its tentacle, it needs to regrow that tooth very rapidly, otherwise it can’t claw its prey,” he explains. This was intriguing news ­ and it sparked an idea in Hopkins lab where he’d been trying to figure out how to store and transmit heat. “It diffuses in all directions. There’s no way to capture the heat and move it the way that you would electricity. It’s just not a fundamental law of physics.” [...] The tiny brown batteries he mentions [...]
