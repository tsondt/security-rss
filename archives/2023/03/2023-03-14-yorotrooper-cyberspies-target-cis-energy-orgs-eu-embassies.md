Title: YoroTrooper cyberspies target CIS energy orgs, EU embassies
Date: 2023-03-14T10:56:21-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-14-yorotrooper-cyberspies-target-cis-energy-orgs-eu-embassies

[Source](https://www.bleepingcomputer.com/news/security/yorotrooper-cyberspies-target-cis-energy-orgs-eu-embassies/){:target="_blank" rel="noopener"}

> A new threat actor named 'YoroTrooper' has been running cyber-espionage campaigns since at least June 2022, targeting government and energy organizations in Commonwealth of Independent States (CIS) countries. [...]
