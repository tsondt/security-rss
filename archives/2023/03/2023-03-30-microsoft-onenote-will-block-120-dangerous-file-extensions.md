Title: Microsoft OneNote will block 120 dangerous file extensions
Date: 2023-03-30T17:40:13-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-03-30-microsoft-onenote-will-block-120-dangerous-file-extensions

[Source](https://www.bleepingcomputer.com/news/security/microsoft-onenote-will-block-120-dangerous-file-extensions/){:target="_blank" rel="noopener"}

> Microsoft has shared more information on what types of malicious embedded files OneNote will soon block to defend users against ongoing phishing attacks pushing malware. [...]
