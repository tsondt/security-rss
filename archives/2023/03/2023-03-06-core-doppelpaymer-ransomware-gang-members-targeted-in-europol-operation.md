Title: Core DoppelPaymer ransomware gang members targeted in Europol operation
Date: 2023-03-06T09:00:49-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-06-core-doppelpaymer-ransomware-gang-members-targeted-in-europol-operation

[Source](https://www.bleepingcomputer.com/news/security/core-doppelpaymer-ransomware-gang-members-targeted-in-europol-operation/){:target="_blank" rel="noopener"}

> Europol has announced that law enforcement in Germany and Ukraine targeted two individuals believed to be core members of the DoppelPaymer ransomware group. [...]
