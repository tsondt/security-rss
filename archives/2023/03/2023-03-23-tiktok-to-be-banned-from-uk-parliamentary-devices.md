Title: TikTok to be banned from UK parliamentary devices
Date: 2023-03-23T14:38:57+00:00
Author: Alex Hern and agencies
Category: The Guardian
Tags: TikTok;House of Commons;House of Lords;Espionage;China;UK news;Social media;Data and computer security
Slug: 2023-03-23-tiktok-to-be-banned-from-uk-parliamentary-devices

[Source](https://www.theguardian.com/technology/2023/mar/23/tiktok-to-be-banned-from-uk-parliamentary-devices){:target="_blank" rel="noopener"}

> Move follows UK government’s decision to ban Chinese-owned video-sharing app Politics live - latest updates Parliament is to ban the Chinese-owned video-sharing app TikTok from “all parliamentary devices and the wider parliamentary network”, citing the need for cybersecurity. The move goes further than the ban last week of the app on government mobile phones and devices, covering the whole parliamentary network. That means that MPs and parliamentary staff who continue to have TikTok installed on personal devices will find the service blocked if they try to access it over parliamentary wifi. Continue reading... [...]
