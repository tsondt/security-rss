Title: OpenAI: ChatGPT payment data leak caused by open-source bug
Date: 2023-03-24T14:39:50-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Technology
Slug: 2023-03-24-openai-chatgpt-payment-data-leak-caused-by-open-source-bug

[Source](https://www.bleepingcomputer.com/news/security/openai-chatgpt-payment-data-leak-caused-by-open-source-bug/){:target="_blank" rel="noopener"}

> OpenAI says a Redis client open-source library bug was behind Monday's ChatGPT outage and data leak, where users saw other users' personal information and chat queries. [...]
