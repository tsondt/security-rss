Title: LockBit brags: We'll leak thousands of SpaceX blueprints stolen from supplier
Date: 2023-03-13T23:40:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-13-lockbit-brags-well-leak-thousands-of-spacex-blueprints-stolen-from-supplier

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/13/lockbit_spacex_ransomware/){:target="_blank" rel="noopener"}

> And also, Ring hit with ransomware, too? Ransomware gang Lockbit has boasted it broke into Maximum Industries, which makes parts for SpaceX, and stole 3,000 proprietary schematics developed by Elon Musk's rocketeers.... [...]
