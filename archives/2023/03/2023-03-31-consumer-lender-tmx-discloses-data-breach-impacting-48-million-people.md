Title: Consumer lender TMX discloses data breach impacting 4.8 million people
Date: 2023-03-31T10:18:32-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-31-consumer-lender-tmx-discloses-data-breach-impacting-48-million-people

[Source](https://www.bleepingcomputer.com/news/security/consumer-lender-tmx-discloses-data-breach-impacting-48-million-people/){:target="_blank" rel="noopener"}

> TMX Finance and its subsidiaries TitleMax, TitleBucks, and InstaLoan have collectively disclosed a data breach that exposed the personal data of 4,822,580 customers. [...]
