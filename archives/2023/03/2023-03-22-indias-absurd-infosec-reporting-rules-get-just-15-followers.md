Title: India's absurd infosec reporting rules get just 15 followers
Date: 2023-03-22T03:30:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-03-22-indias-absurd-infosec-reporting-rules-get-just-15-followers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/22/cert_in_cyber_reporting_ignored/){:target="_blank" rel="noopener"}

> CERT-In was told its six-hour notification requirement was a bad idea – now it knows just how bad India's rules requiring local organizations to report infosec incidents within six hours of detection have been observed by a mere 15 entities/... [...]
