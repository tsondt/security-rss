Title: UK bans TikTok from government mobile phones
Date: 2023-03-16T15:25:22+00:00
Author: Dan Sabbagh Defence and security editor
Category: The Guardian
Tags: TikTok;UK news;Politics;China;Technology;World news;Data and computer security
Slug: 2023-03-16-uk-bans-tiktok-from-government-mobile-phones

[Source](https://www.theguardian.com/technology/2023/mar/16/uk-bans-tiktok-from-government-mobile-phones){:target="_blank" rel="noopener"}

> Move brings Britain in line with US and Europe and reflects worsening relations with China Britain is to ban the Chinese-owned video-sharing app TikTok from ministers’ and civil servants’ mobile phones, bringing the UK in line with the US and the European Commission and reflecting deteriorating relations with Beijing. The decision marks a sharp U-turn from the UK’s previous position and came a few hours after TikTok said its owner, ByteDance, had been told by Washington to sell the app or face a possible ban in the country. Continue reading... [...]
