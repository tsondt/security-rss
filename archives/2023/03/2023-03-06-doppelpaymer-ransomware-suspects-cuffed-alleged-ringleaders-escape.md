Title: DoppelPaymer ransomware suspects cuffed, alleged ringleaders escape
Date: 2023-03-06T21:45:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-06-doppelpaymer-ransomware-suspects-cuffed-alleged-ringleaders-escape

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/06/doppelpaymer_ransomware_arrests/){:target="_blank" rel="noopener"}

> Millions extorted from victims, one attack left hospital patient dead German and Ukrainian cops have arrested suspected members of the DoppelPaymer ransomware crew and issued warrants for three other "masterminds" behind the global operation that extorted tens of millions of dollars and may have led to the death of a hospital patient.... [...]
