Title: FBI: Ransomware hit 860 critical infrastructure orgs in 2022
Date: 2023-03-15T16:23:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-15-fbi-ransomware-hit-860-critical-infrastructure-orgs-in-2022

[Source](https://www.bleepingcomputer.com/news/security/fbi-ransomware-hit-860-critical-infrastructure-orgs-in-2022/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) revealed in its 2022 Internet Crime Report that ransomware gangs breached the networks of at least 860 critical infrastructure organizations last year. [...]
