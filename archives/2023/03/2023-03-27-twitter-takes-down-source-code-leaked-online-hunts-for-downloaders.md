Title: Twitter takes down source code leaked online, hunts for downloaders
Date: 2023-03-27T10:55:25-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-27-twitter-takes-down-source-code-leaked-online-hunts-for-downloaders

[Source](https://www.bleepingcomputer.com/news/security/twitter-takes-down-source-code-leaked-online-hunts-for-downloaders/){:target="_blank" rel="noopener"}

> Twitter has taken down internal source code for its platform and tools that was leaked on GitHub for months. Now it's using a subpoena to search for those who leaked and downloaded its code. [...]
