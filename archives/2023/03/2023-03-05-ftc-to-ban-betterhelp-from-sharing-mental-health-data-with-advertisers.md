Title: FTC to ban BetterHelp from sharing mental health data with advertisers
Date: 2023-03-05T10:12:24-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-05-ftc-to-ban-betterhelp-from-sharing-mental-health-data-with-advertisers

[Source](https://www.bleepingcomputer.com/news/security/ftc-to-ban-betterhelp-from-sharing-mental-health-data-with-advertisers/){:target="_blank" rel="noopener"}

> The Federal Trade Commission (FTC) has proposed to ban the online counseling service BetterHelp from sharing its customers' sensitive mental health data with advertising networks and marketers. [...]
