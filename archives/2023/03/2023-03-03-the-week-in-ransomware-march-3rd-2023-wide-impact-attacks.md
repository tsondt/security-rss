Title: The Week in Ransomware - March 3rd 2023 - Wide impact attacks
Date: 2023-03-03T18:46:41-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-03-03-the-week-in-ransomware-march-3rd-2023-wide-impact-attacks

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-march-3rd-2023-wide-impact-attacks/){:target="_blank" rel="noopener"}

> This week was highlighted by a massive BlackBasta ransomware attack targeting DISH Network and taking down numerous subsidiaries, including SlingTV and Boost Mobile. [...]
