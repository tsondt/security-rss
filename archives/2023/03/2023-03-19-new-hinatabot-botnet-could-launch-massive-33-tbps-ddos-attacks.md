Title: New ‘HinataBot’ botnet could launch massive 3.3 Tbps DDoS attacks
Date: 2023-03-19T10:20:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-19-new-hinatabot-botnet-could-launch-massive-33-tbps-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/security/new-hinatabot-botnet-could-launch-massive-33-tbps-ddos-attacks/){:target="_blank" rel="noopener"}

> A new malware botnet was discovered targeting Realtek SDK, Huawei routers, and Hadoop YARN servers to recruit devices into DDoS (distributed denial of service) swarm with the potential for massive attacks. [...]
