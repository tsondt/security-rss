Title: China sought control of submarine cables to spy, says Micronesia
Date: 2023-03-15T03:29:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-03-15-china-sought-control-of-submarine-cables-to-spy-says-micronesia

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/15/bribery_espionage_and_hostile_takeovers/){:target="_blank" rel="noopener"}

> Outgoing president alleges Beijing is systematically bullying strategically located island paradise The outgoing president of the Federated States of Micronesia (FSM), David Panuelo, penned a lengthy letter last week accusing Beijing of rampant bribery, spying and other tactics – including an attempt to take control of the nation's submarine cables and telecoms infrastructure.... [...]
