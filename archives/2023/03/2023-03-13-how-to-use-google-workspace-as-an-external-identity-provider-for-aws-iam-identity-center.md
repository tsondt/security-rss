Title: How to use Google Workspace as an external identity provider for AWS IAM Identity Center
Date: 2023-03-13T19:59:44+00:00
Author: Yegor Tokmakov
Category: AWS Security
Tags: AWS IAM Identity Center;Foundational (100);Security, Identity, & Compliance;Technical How-to;AWS CLI;AWS IAM;AWS Organizations;AWS Single Sign-On;Federation;G Suite;GSuite;IAM;IAM Identity Center;SAML;Security Blog
Slug: 2023-03-13-how-to-use-google-workspace-as-an-external-identity-provider-for-aws-iam-identity-center

[Source](https://aws.amazon.com/blogs/security/how-to-use-g-suite-as-external-identity-provider-aws-sso/){:target="_blank" rel="noopener"}

> March 8, 2023: We updated the post to reflect some name changes (G Suite is now Google Workspace; AWS Single Sign-On is now AWS IAM Identity Center) and associated changes to the user interface and workflow when setting up Google Workspace as an external identity provider for IAM Identity Center. September 12, 2022: This blog [...]
