Title: Cyberwarfare leaks show Russian army is adopting mindset of secret police
Date: 2023-03-30T15:00:02+00:00
Author: Andrei Soldatov
Category: The Guardian
Tags: Cyberwar;Data and computer security;Internet safety;Hacking;Internet;Mobile phones;Technology;Telecoms;Social media;Digital media;Media;Espionage;World news;Russia;Europe
Slug: 2023-03-30-cyberwarfare-leaks-show-russian-army-is-adopting-mindset-of-secret-police

[Source](https://www.theguardian.com/technology/2023/mar/30/cyberwarfare-leaks-show-russian-army-is-adopting-mindset-of-secret-police){:target="_blank" rel="noopener"}

> Documents leaked from Vulkan cybersecurity firm also raise questions about role of IT engineers behind information-control project A consortium of media outlets have published a bombshell investigation about Russia’s cyber-capabilities, based on a rare leak of documents. The files come from NTC Vulkan, a cybersecurity firm in Moscow that doubles as a contractor to Russian military and intelligence agencies. They reveal how, for years, a group of top Russian IT engineers have been hired to work with Russian military intelligence and a research facility of the FSB, Vladimir Putin’s domestic spy agency. This might seem an unusual mix, and would [...]
