Title: How to improve your Kubernetes security posture with GKE Dataplane V2 network policies
Date: 2023-03-03T17:00:00+00:00
Author: Bon Sethi
Category: GCP Security
Tags: Identity & Security;Developers & Practitioners;Containers & Kubernetes
Slug: 2023-03-03-how-to-improve-your-kubernetes-security-posture-with-gke-dataplane-v2-network-policies

[Source](https://cloud.google.com/blog/products/containers-kubernetes/better-security-for-your-serverless-workflows-in-cloud/){:target="_blank" rel="noopener"}

> As more organizations adopt Kubernetes, they also embrace new paradigms for connecting and protecting their workloads. Relying on perimeter defense alone is no longer an effective strategy. With microservice architecture patterns continuing to evolve rapidly, it is imperative that organizations adopt a defense-in-depth strategy to keep their applications and data protected. To effectively manage a highly distributed and dynamic system, with an abundance of exposed ports and APIs, organizations need more than traditional network-perimeter firewalls. With a myriad of connections between microservices, a rogue actor could use a compromised container instance to move laterally through the network to attack others, [...]
