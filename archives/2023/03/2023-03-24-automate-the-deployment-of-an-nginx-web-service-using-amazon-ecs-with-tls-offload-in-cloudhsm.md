Title: Automate the deployment of an NGINX web service using Amazon ECS with TLS offload in CloudHSM
Date: 2023-03-24T16:33:15+00:00
Author: Nikolas Nikravesh
Category: AWS Security
Tags: Advanced (300);AWS CloudHSM;AWS Fargate;Best Practices;Security, Identity, & Compliance;Technical How-to;AWS CDK;CloudHSM;ECS;Fargate;OpenSSL;Security Blog;TLS
Slug: 2023-03-24-automate-the-deployment-of-an-nginx-web-service-using-amazon-ecs-with-tls-offload-in-cloudhsm

[Source](https://aws.amazon.com/blogs/security/automate-the-deployment-of-an-nginx-web-service-using-amazon-ecs-with-tls-offload-in-cloudhsm/){:target="_blank" rel="noopener"}

> Customers who require private keys for their TLS certificates to be stored in FIPS 140-2 Level 3 certified hardware security modules (HSMs) can use AWS CloudHSM to store their keys for websites hosted in the cloud. In this blog post, we will show you how to automate the deployment of a web application using NGINX [...]
