Title: QNAP warns customers to patch Linux Sudo flaw in NAS devices
Date: 2023-03-29T14:15:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Linux
Slug: 2023-03-29-qnap-warns-customers-to-patch-linux-sudo-flaw-in-nas-devices

[Source](https://www.bleepingcomputer.com/news/security/qnap-warns-customers-to-patch-linux-sudo-flaw-in-nas-devices/){:target="_blank" rel="noopener"}

> Taiwanese hardware vendor QNAP warns customers to secure their Linux-powered network-attached storage (NAS) devices against a high-severity Sudo privilege escalation vulnerability. [...]
