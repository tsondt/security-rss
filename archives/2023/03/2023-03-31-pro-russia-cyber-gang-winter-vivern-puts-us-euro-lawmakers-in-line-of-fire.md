Title: Pro-Russia cyber gang Winter Vivern puts US, Euro lawmakers in line of fire
Date: 2023-03-31T07:30:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-31-pro-russia-cyber-gang-winter-vivern-puts-us-euro-lawmakers-in-line-of-fire

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/31/winter_vivern_european_goverments/){:target="_blank" rel="noopener"}

> Winter is coming for NATO countries A cyber spy gang supporting Russia is targeting US elected officials and their staffers, in addition to European lawmakers, using unpatched Zimbra Collaboration software in two campaigns spotted by Proofpoint.... [...]
