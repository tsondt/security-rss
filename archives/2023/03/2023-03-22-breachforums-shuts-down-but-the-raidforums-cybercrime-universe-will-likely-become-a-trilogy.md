Title: BreachForums shuts down ... but the RaidForums cybercrime universe will likely become a trilogy
Date: 2023-03-22T00:45:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-22-breachforums-shuts-down-but-the-raidforums-cybercrime-universe-will-likely-become-a-trilogy

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/22/breachforums_shut_down/){:target="_blank" rel="noopener"}

> Admins decide reviving crime-mart is dangerous, hint at revival BreachForums has reportedly shut down for good, just days after US authorities arrested the online criminal marketplace's alleged chief administrator.... [...]
