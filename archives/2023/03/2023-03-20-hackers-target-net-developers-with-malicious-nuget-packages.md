Title: Hackers target .NET developers with malicious NuGet packages
Date: 2023-03-20T15:22:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-03-20-hackers-target-net-developers-with-malicious-nuget-packages

[Source](https://www.bleepingcomputer.com/news/security/hackers-target-net-developers-with-malicious-nuget-packages/){:target="_blank" rel="noopener"}

> Threat actors are targeting and infecting.NET developers with cryptocurrency stealers delivered through the NuGet repository and impersonating multiple legitimate packages via typosquatting. [...]
