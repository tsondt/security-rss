Title: Ransomware gang leaks data stolen from City of Oakland
Date: 2023-03-04T15:47:41-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-03-04-ransomware-gang-leaks-data-stolen-from-city-of-oakland

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-leaks-data-stolen-from-city-of-oakland/){:target="_blank" rel="noopener"}

> The Play ransomware gang has begun to leak data from the City of Oakland, California, that was stolen in a recent cyberattack. [...]
