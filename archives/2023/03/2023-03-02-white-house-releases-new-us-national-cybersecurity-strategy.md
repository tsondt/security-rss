Title: White House releases new U.S. national cybersecurity strategy
Date: 2023-03-02T11:49:45-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-02-white-house-releases-new-us-national-cybersecurity-strategy

[Source](https://www.bleepingcomputer.com/news/security/white-house-releases-new-us-national-cybersecurity-strategy/){:target="_blank" rel="noopener"}

> The Biden-Harris administration today released its national cybersecurity strategy that focuses on shifting the burden of defending the country's cyberspace towards software vendors and service providers. [...]
