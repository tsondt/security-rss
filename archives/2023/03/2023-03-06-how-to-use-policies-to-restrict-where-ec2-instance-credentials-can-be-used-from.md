Title: How to use policies to restrict where EC2 instance credentials can be used from
Date: 2023-03-06T17:32:00+00:00
Author: Liam Wadman
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;Data perimeters;EC2;EC2 roles;IAM;IMDS;IMDSV1;IMDSV2;Instance profiles;Instance Role;Invariant;Network Perimeter;Organizations;Resource Policy;SCP;Security;Security Blog;service control policy;SIGV4;SSRF;Workload Identity
Slug: 2023-03-06-how-to-use-policies-to-restrict-where-ec2-instance-credentials-can-be-used-from

[Source](https://aws.amazon.com/blogs/security/how-to-use-policies-to-restrict-where-ec2-instance-credentials-can-be-used-from/){:target="_blank" rel="noopener"}

> Today AWS launched two new global condition context keys that make it simpler for you to write policies in which Amazon Elastic Compute Cloud (Amazon EC2) instance credentials work only when used on the instance to which they are issued. These new condition keys are available today in all AWS Regions, as well as AWS [...]
