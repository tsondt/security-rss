Title: BianLian ransomware gang shifts focus to pure data extortion
Date: 2023-03-16T18:10:58-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-16-bianlian-ransomware-gang-shifts-focus-to-pure-data-extortion

[Source](https://www.bleepingcomputer.com/news/security/bianlian-ransomware-gang-shifts-focus-to-pure-data-extortion/){:target="_blank" rel="noopener"}

> The BianLian ransomware group has shifted its focus from encrypting its victims' files to only exfiltrating data found on compromised networks and using them for extortion. [...]
