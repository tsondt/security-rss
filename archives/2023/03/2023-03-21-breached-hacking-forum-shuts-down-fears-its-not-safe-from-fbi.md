Title: Breached hacking forum shuts down, fears it's not 'safe' from FBI
Date: 2023-03-21T12:20:37-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-21-breached-hacking-forum-shuts-down-fears-its-not-safe-from-fbi

[Source](https://www.bleepingcomputer.com/news/security/breached-hacking-forum-shuts-down-fears-its-not-safe-from-fbi/){:target="_blank" rel="noopener"}

> The notorious Breached hacking forum has shut down after the remaining administrator, Baphomet, disclosed that they believe law enforcement has access to the site's servers. [...]
