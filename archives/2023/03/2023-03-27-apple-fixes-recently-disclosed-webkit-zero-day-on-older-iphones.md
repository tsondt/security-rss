Title: Apple fixes recently disclosed WebKit zero-day on older iPhones
Date: 2023-03-27T15:40:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple;Security
Slug: 2023-03-27-apple-fixes-recently-disclosed-webkit-zero-day-on-older-iphones

[Source](https://www.bleepingcomputer.com/news/apple/apple-fixes-recently-disclosed-webkit-zero-day-on-older-iphones/){:target="_blank" rel="noopener"}

> Apple has released security updates to backport patches released last month, addressing an actively exploited zero-day bug for older iPhones and iPads. [...]
