Title: Newly exposed APT43 hacking group targeting US orgs since 2018
Date: 2023-03-28T11:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-28-newly-exposed-apt43-hacking-group-targeting-us-orgs-since-2018

[Source](https://www.bleepingcomputer.com/news/security/newly-exposed-apt43-hacking-group-targeting-us-orgs-since-2018/){:target="_blank" rel="noopener"}

> A new North Korean hacking group has been revealed to be targeting government organizations, academics, and think tanks in the United States, Europe, Japan, and South Korea for the past five years. [...]
