Title: ChipMixer platform seized for laundering ransomware payments, drug sales
Date: 2023-03-15T10:53:56-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-03-15-chipmixer-platform-seized-for-laundering-ransomware-payments-drug-sales

[Source](https://www.bleepingcomputer.com/news/security/chipmixer-platform-seized-for-laundering-ransomware-payments-drug-sales/){:target="_blank" rel="noopener"}

> An international law enforcement operation has seized the cryptocurrency mixing service 'ChipMixer' which is said to be used by hackers, ransomware gangs, and scammers to launder their proceeds. [...]
