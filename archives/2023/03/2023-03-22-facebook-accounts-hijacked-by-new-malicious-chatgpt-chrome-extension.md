Title: Facebook accounts hijacked by new malicious ChatGPT Chrome extension
Date: 2023-03-22T12:44:32-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-22-facebook-accounts-hijacked-by-new-malicious-chatgpt-chrome-extension

[Source](https://www.bleepingcomputer.com/news/security/facebook-accounts-hijacked-by-new-malicious-chatgpt-chrome-extension/){:target="_blank" rel="noopener"}

> A trojanized version of the legitimate ChatGPT extension for Chrome is gaining popularity on the Chrome Web Store, accumulating over 9,000 downloads while stealing Facebook accounts. [...]
