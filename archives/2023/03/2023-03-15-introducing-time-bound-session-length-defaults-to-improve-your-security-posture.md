Title: Introducing time-bound Session Length defaults to improve your security posture
Date: 2023-03-15T16:00:00+00:00
Author: Vincent Winstead
Category: GCP Security
Tags: Google Cloud;Identity & Security
Slug: 2023-03-15-introducing-time-bound-session-length-defaults-to-improve-your-security-posture

[Source](https://cloud.google.com/blog/products/identity-security/improve-security-posture-with-time-bound-session-length/){:target="_blank" rel="noopener"}

> Google Cloud provides many layers of security for protecting your users and data. Session length is a configuration parameter that administrators can set to control how long users can access Google Cloud without having to reauthenticate. Managing session length is foundational to cloud security and it ensures access to Google Cloud services is time-bound after a successful authentication. Google Cloud session management provides flexible options for setting up session controls based on your organization’s security policy needs. To further improve security for our customers, we are rolling out a recommended default 16-hour session length to existing Google Cloud customers. Many [...]
