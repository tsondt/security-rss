Title: Latitude Financial data breach now impacts 14 million customers
Date: 2023-03-28T09:50:17-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-28-latitude-financial-data-breach-now-impacts-14-million-customers

[Source](https://www.bleepingcomputer.com/news/security/latitude-financial-data-breach-now-impacts-14-million-customers/){:target="_blank" rel="noopener"}

> Australian loan giant Latitude Financial Services (Latitude) is warning customers that its data breach is much more significant than initially stated, taking the number of affected individuals from 328,000 to 14 million. [...]
