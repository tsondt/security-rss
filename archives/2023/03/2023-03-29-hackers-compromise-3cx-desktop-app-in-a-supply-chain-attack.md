Title: Hackers compromise 3CX desktop app in a supply chain attack
Date: 2023-03-29T18:46:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-29-hackers-compromise-3cx-desktop-app-in-a-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/hackers-compromise-3cx-desktop-app-in-a-supply-chain-attack/){:target="_blank" rel="noopener"}

> A digitally signed and trojanized version of the 3CX Voice Over Internet Protocol (VOIP) desktop client is reportedly being used to target the company's customers in an ongoing supply chain attack. [...]
