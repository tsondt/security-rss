Title: Brazil seizing Flipper Zero shipments to prevent use in crime
Date: 2023-03-11T10:23:51-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2023-03-11-brazil-seizing-flipper-zero-shipments-to-prevent-use-in-crime

[Source](https://www.bleepingcomputer.com/news/security/brazil-seizing-flipper-zero-shipments-to-prevent-use-in-crime/){:target="_blank" rel="noopener"}

> The Brazilian National Telecommunications Agency is seizing incoming Flipper Zero purchases due to its alleged use in criminal activity, with purchasers stating that the government agency has rejected all attempts to certify the equipment. [...]
