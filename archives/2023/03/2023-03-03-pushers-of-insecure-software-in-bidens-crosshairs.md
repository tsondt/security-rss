Title: Pushers of insecure software in Biden's crosshairs
Date: 2023-03-03T00:15:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-03-pushers-of-insecure-software-in-bidens-crosshairs

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/03/us_national_cybersecurity_strategy/){:target="_blank" rel="noopener"}

> Just-revealed US cybersecurity strategy 'has fangs' for catching crafty criminals and crummy coders Analysis Technology providers can expect more regulations, while cyber criminals can look for US law enforcement to step up their efforts to disrupt ransomware gangs and other illicit activities, under the Biden administration's computer security plan announced on Thursday.... [...]
