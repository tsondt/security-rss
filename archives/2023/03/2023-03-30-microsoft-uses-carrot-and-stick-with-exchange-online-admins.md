Title: Microsoft uses carrot and stick with Exchange Online admins
Date: 2023-03-30T14:27:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-03-30-microsoft-uses-carrot-and-stick-with-exchange-online-admins

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/30/microsoft_hardening_exchange_online/){:target="_blank" rel="noopener"}

> If you need extra time to dump RPS, OK, but email from unsupported Exchange servers is blocked till they’re up to date Some Exchange Online users who have the RPS feature turned off by Microsoft can now have it re-enabled – at least until September when the tool is retired.... [...]
