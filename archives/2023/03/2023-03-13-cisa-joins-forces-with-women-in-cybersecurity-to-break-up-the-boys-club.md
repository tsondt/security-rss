Title: CISA joins forces with Women in CyberSecurity to break up the boy's club
Date: 2023-03-13T12:32:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-03-13-cisa-joins-forces-with-women-in-cybersecurity-to-break-up-the-boys-club

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/13/cisa_joins_forces_with_women/){:target="_blank" rel="noopener"}

> Also, the FBI just admitted to bypassing warrants by buying cellphone location data, and this week's actionable items in brief Cybersecurity and Infrastructure Security Agency's director Jen Easterly has been outspoken in her drive to bring more women into the security industry, and this year for International Women's Day her agency formalized that pledge by announcing a partnership with nonprofit Women in CyberSecurity (WiCyS).... [...]
