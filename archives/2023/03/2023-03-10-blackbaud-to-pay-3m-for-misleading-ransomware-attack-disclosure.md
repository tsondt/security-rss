Title: Blackbaud to pay $3M for misleading ransomware attack disclosure
Date: 2023-03-10T11:30:18-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-10-blackbaud-to-pay-3m-for-misleading-ransomware-attack-disclosure

[Source](https://www.bleepingcomputer.com/news/security/blackbaud-to-pay-3m-for-misleading-ransomware-attack-disclosure/){:target="_blank" rel="noopener"}

> Cloud software provider Blackbaud has agreed to pay $3 million to settle charges brought by the Securities and Exchange Commission (SEC), alleging that it failed to disclose the full impact of a 2020 ransomware attack that affected more than 13,000 customers. [...]
