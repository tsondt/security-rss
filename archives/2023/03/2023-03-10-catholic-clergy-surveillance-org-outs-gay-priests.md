Title: Catholic clergy surveillance org 'outs gay priests'
Date: 2023-03-10T02:30:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-03-10-catholic-clergy-surveillance-org-outs-gay-priests

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/10/catholic_clergy_surveillance/){:target="_blank" rel="noopener"}

> Religious non-profit allegedly hoovered up location data from dating apps to ID clerics A Catholic clergy conformance organization has reportedly been buying up tracking data from mobile apps to identify gay priests, and providing that information to bishops around America.... [...]
