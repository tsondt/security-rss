Title: US Citizen Hacked by Spyware
Date: 2023-03-21T12:34:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberespionage;Greece;malware;Meta;spyware
Slug: 2023-03-21-us-citizen-hacked-by-spyware

[Source](https://www.schneier.com/blog/archives/2023/03/us-citizen-hacked-by-spyware.html){:target="_blank" rel="noopener"}

> The New York Times is reporting that a US citizen’s phone was hacked by the Predator spyware. A U.S. and Greek national who worked on Meta’s security and trust team while based in Greece was placed under a yearlong wiretap by the Greek national intelligence service and hacked with a powerful cyberespionage tool, according to documents obtained by The New York Times and officials with knowledge of the case. The disclosure is the first known case of an American citizen being targeted in a European Union country by the advanced snooping technology, the use of which has been the subject [...]
