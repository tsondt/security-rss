Title: Warning on SolarWinds-like supply-chain attacks: 'They're just getting bigger'
Date: 2023-03-03T11:33:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-03-warning-on-solarwinds-like-supply-chain-attacks-theyre-just-getting-bigger

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/03/solarwinds_supplychain_security/){:target="_blank" rel="noopener"}

> Industry hasn't 'improved much at all' SCSW Back in 2020, Eric Scales led the incident response team investigating a nation-state hack that compromised his company's servers along with those at federal agencies and tech giants including Microsoft and Intel.... [...]
