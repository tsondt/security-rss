Title: Microsoft Exchange Online outage blocks access to mailboxes worldwide
Date: 2023-03-01T14:02:18-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-01-microsoft-exchange-online-outage-blocks-access-to-mailboxes-worldwide

[Source](https://www.bleepingcomputer.com/news/security/microsoft-exchange-online-outage-blocks-access-to-mailboxes-worldwide/){:target="_blank" rel="noopener"}

> Microsoft is investigating an ongoing outage that is blocking Exchange Online customers worldwide from accessing their mailboxes via any connection method or sending/receiving emails. [...]
