Title: Microsoft pushes OOB security updates for Windows Snipping tool flaw
Date: 2023-03-25T13:54:50-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-03-25-microsoft-pushes-oob-security-updates-for-windows-snipping-tool-flaw

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-pushes-oob-security-updates-for-windows-snipping-tool-flaw/){:target="_blank" rel="noopener"}

> Microsoft released an emergency security update for the Windows 10 and Windows 11 Snipping tool to fix the Acropalypse privacy vulnerability. [...]
