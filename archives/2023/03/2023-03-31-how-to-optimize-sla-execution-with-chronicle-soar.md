Title: How to optimize SLA execution with Chronicle SOAR
Date: 2023-03-31T16:00:00+00:00
Author: Ahnna Schini
Category: GCP Security
Tags: Security & Identity
Slug: 2023-03-31-how-to-optimize-sla-execution-with-chronicle-soar

[Source](https://cloud.google.com/blog/products/identity-security/how-to-optimize-sla-execution-with-chronicle-soar/){:target="_blank" rel="noopener"}

> Measuring the effectiveness of security operations programs can be challenging. Since time is of the essence when it comes to effective threat detection and response, one metric that is commonly used by security operations teams is service level agreements (SLAs). SLAs define the desired amount of time it should take a security operations team to investigate and address a “case.” They are also becoming an increasingly important tool for leadership as they aim to: Track security tools and services impact on the organization Measure the amount of risk reduction being performed Identify gaps, reallocate resources and evolve existing processes These [...]
