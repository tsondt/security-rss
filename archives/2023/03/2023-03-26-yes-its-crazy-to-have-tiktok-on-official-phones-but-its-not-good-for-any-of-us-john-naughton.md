Title: Yes, it’s crazy to have TikTok on official phones. But it’s not good for any of us | John Naughton
Date: 2023-03-26T09:00:14+00:00
Author: John Naughton
Category: The Guardian
Tags: TikTok;Data and computer security;Technology;China;World news;US news;UK news;Europe
Slug: 2023-03-26-yes-its-crazy-to-have-tiktok-on-official-phones-but-its-not-good-for-any-of-us-john-naughton

[Source](https://www.theguardian.com/commentisfree/2023/mar/26/crazy-tiktok-official-phones-not-good-any-of-us){:target="_blank" rel="noopener"}

> Fears for data security lie behind recent government bans on the Chinese-owned app, but zombie scrolling has health dangers too As of this moment, government officials in 11 countries are forbidden to run TikTok on their government-issued phones. The countries include the US, Canada, Denmark, Belgium, the UK, New Zealand, Norway, France, the Netherlands and Poland. In addition, European Commission and European parliament staff were required to delete the app. This raises two questions. First, why were politicians and senior officials in democracies scrolling like zombies through dance crazes, daft pet videos, feeling “bonita” and things you can do with [...]
