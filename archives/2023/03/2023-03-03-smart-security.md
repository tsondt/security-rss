Title: Smart security
Date: 2023-03-03T10:15:11+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-03-03-smart-security

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/03/smart_security/){:target="_blank" rel="noopener"}

> Outlawing cybersecurity hype Webinar Trying to keep on top of all the hype and complexity in cybersecurity can be more than an just an uphill struggle and more like a veritable mountain to climb every morning.... [...]
