Title: Announcing Google Cloud’s new Digital Sovereignty Explorer
Date: 2023-03-23T07:00:00+00:00
Author: Robert Sadowski
Category: GCP Security
Tags: Security & Identity
Slug: 2023-03-23-announcing-google-clouds-new-digital-sovereignty-explorer

[Source](https://cloud.google.com/blog/products/identity-security/announcing-google-clouds-new-digital-sovereignty-explorer/){:target="_blank" rel="noopener"}

> Digital sovereignty continues to be a top priority for organizations working to advance or begin their digital transformation efforts. Over the past few years, Google Cloud has worked extensively with customers, partners, policy makers and governments around the world to understand evolving sovereignty requirements. In Europe, this has resulted in our “ Cloud. On Europe’s Terms ” initiative and a broad portfolio of Sovereign Solutions we have already brought to market to help support customers’ current and emerging needs as they bring more workloads to the cloud. One thing remains clear from our ongoing discussions in the market: Designing a [...]
