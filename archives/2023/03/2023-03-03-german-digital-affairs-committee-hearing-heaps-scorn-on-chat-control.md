Title: German Digital Affairs Committee hearing heaps scorn on Chat Control
Date: 2023-03-03T10:34:38+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-03-03-german-digital-affairs-committee-hearing-heaps-scorn-on-chat-control

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/03/german_digital_committee_hearing_heaps/){:target="_blank" rel="noopener"}

> Proposal to break encryption to scan messages for abuse material challenged as illegal and unworkable Europe's proposed "Chat Control" legislation to automatically scan chat, email, and instant message communications for child sexual exploitation material (CSEM) ran up against broad resistance at a meeting of the German Parliament's (Bundestag) Digital Affairs Committee on Wednesday.... [...]
