Title: LockBit ransomware gang now also claims City of Oakland breach
Date: 2023-03-21T12:57:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-21-lockbit-ransomware-gang-now-also-claims-city-of-oakland-breach

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-gang-now-also-claims-city-of-oakland-breach/){:target="_blank" rel="noopener"}

> Another ransomware operation, the LockBit gang, now threatens to leak what it describes as files stolen from the City of Oakland's systems. [...]
