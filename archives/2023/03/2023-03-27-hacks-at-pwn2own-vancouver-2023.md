Title: Hacks at Pwn2Own Vancouver 2023
Date: 2023-03-27T11:03:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Adobe;cars;hacking;Windows;zero-day
Slug: 2023-03-27-hacks-at-pwn2own-vancouver-2023

[Source](https://www.schneier.com/blog/archives/2023/03/hacks-at-pwn2own-vancouver-2023.html){:target="_blank" rel="noopener"}

> An impressive array of hacks were demonstrated at the first day of the Pwn2Own conference in Vancouver: On the first day of Pwn2Own Vancouver 2023, security researchers successfully demoed Tesla Model 3, Windows 11, and macOS zero-day exploits and exploit chains to win $375,000 and a Tesla Model 3. The first to fall was Adobe Reader in the enterprise applications category after Haboob SA’s Abdul Aziz Hariri ( @abdhariri ) used an exploit chain targeting a 6-bug logic chain abusing multiple failed patches which escaped the sandbox and bypassed a banned API list on macOS to earn $50,000. The STAR [...]
