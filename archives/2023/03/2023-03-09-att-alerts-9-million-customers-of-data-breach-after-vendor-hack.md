Title: AT&T alerts 9 million customers of data breach after vendor hack
Date: 2023-03-09T12:24:39-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-09-att-alerts-9-million-customers-of-data-breach-after-vendor-hack

[Source](https://www.bleepingcomputer.com/news/security/atandt-alerts-9-million-customers-of-data-breach-after-vendor-hack/){:target="_blank" rel="noopener"}

> AT&T is notifying roughly 9 million customers that some of their information has been exposed after one of its marketing vendors was hacked in January. [...]
