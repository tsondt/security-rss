Title: FBI investigates data breach impacting U.S. House members and staff
Date: 2023-03-08T17:48:41-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-08-fbi-investigates-data-breach-impacting-us-house-members-and-staff

[Source](https://www.bleepingcomputer.com/news/security/fbi-investigates-data-breach-impacting-us-house-members-and-staff/){:target="_blank" rel="noopener"}

> The FBI is investigating a data breach affecting U.S. House of Representatives members and staff after their account and personal information was stolen from DC Health Link's servers. [...]
