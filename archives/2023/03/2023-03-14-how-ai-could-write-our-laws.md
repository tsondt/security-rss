Title: How AI Could Write Our Laws
Date: 2023-03-14T16:01:43+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;essays;laws
Slug: 2023-03-14-how-ai-could-write-our-laws

[Source](https://www.schneier.com/blog/archives/2023/03/how-ai-could-write-our-laws.html){:target="_blank" rel="noopener"}

> By Nathan E. Sanders & Bruce Schneier Nearly 90% of the multibillion-dollar federal lobbying apparatus in the United States serves corporate interests. In some cases, the objective of that money is obvious. Google pours millions into lobbying on bills related to antitrust regulation. Big energy companies expect action whenever there is a move to end drilling leases for federal lands, in exchange for the tens of millions they contribute to congressional reelection campaigns. But lobbying strategies are not always so blunt, and the interests involved are not always so obvious. Consider, for example, a 2013 Massachusetts bill that tried to [...]
