Title: Microsoft enables LSA protection by default in Windows Canary build
Date: 2023-03-08T15:38:21-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-03-08-microsoft-enables-lsa-protection-by-default-in-windows-canary-build

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-enables-lsa-protection-by-default-in-windows-canary-build/){:target="_blank" rel="noopener"}

> Microsoft says the latest Windows 11 build that is rolling out to Insiders in the Canary channel will try to enable Local Security Authority (LSA) protection by default. [...]
