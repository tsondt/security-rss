Title: FBI: Business email compromise tactics used to defraud U.S. vendors
Date: 2023-03-26T12:03:06-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-03-26-fbi-business-email-compromise-tactics-used-to-defraud-us-vendors

[Source](https://www.bleepingcomputer.com/news/security/fbi-business-email-compromise-tactics-used-to-defraud-us-vendors/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation is warning companies in the U.S. of threat actors using tactics similar to business email compromise that allow less technical actors to steal various goods from vendors. [...]
