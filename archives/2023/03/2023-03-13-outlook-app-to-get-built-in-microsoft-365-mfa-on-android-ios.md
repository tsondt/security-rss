Title: Outlook app to get built-in Microsoft 365 MFA on Android, iOS
Date: 2023-03-13T13:07:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-03-13-outlook-app-to-get-built-in-microsoft-365-mfa-on-android-ios

[Source](https://www.bleepingcomputer.com/news/microsoft/outlook-app-to-get-built-in-microsoft-365-mfa-on-android-ios/){:target="_blank" rel="noopener"}

> Microsoft will soon fast-track multi-factor authentication (MFA) adoption for its Microsoft 365 cloud productivity platform by adding MFA capabilities to the Outlook email client. [...]
