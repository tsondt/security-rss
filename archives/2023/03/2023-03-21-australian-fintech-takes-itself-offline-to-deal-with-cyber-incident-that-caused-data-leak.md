Title: Australian FinTech takes itself offline to deal with cyber incident that caused data leak
Date: 2023-03-21T03:58:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-03-21-australian-fintech-takes-itself-offline-to-deal-with-cyber-incident-that-caused-data-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/21/latitude_financial_cyber_attack_leak/){:target="_blank" rel="noopener"}

> Latitude blames a 'major vendor' for its woes. Is that a vendor? A cloud? Whoever they are, they're in trouble Latitude Financial has blamed a supplier for leaking creds that caused vast PII leak Australian outfit Latitude Financial has taken itself offline, and even stopped serving customers, while it tries to clean up an attack on its systems.... [...]
