Title: How Project Shield helped protect U.S. midterm elections from DDoS attacks
Date: 2023-03-22T16:00:00+00:00
Author: Emil Kiner
Category: GCP Security
Tags: Networking;Security & Identity
Slug: 2023-03-22-how-project-shield-helped-protect-us-midterm-elections-from-ddos-attacks

[Source](https://cloud.google.com/blog/products/identity-security/ddos-attack-trends-during-us-midterm-elections/){:target="_blank" rel="noopener"}

> Modern elections rely on public access to a vast array of online information, including political candidate stances, elections monitoring, and directions to polling sites. Public websites can be taken offline by an attacker with no special access, through the use of a Distributed Denial of Service (DDoS) attack. These DDoS attacks are often used to suppress information, damage organizations or individuals, or sway the outcome of geopolitical events. Project Shield is an offering from Google, provided at no charge, that keeps news, human rights, and elections organizations websites safe from DDoS attacks using the power of Google Cloud. In the [...]
