Title: Acer confirms breach after 160GB of data for sale on hacking forum
Date: 2023-03-07T10:38:43-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-07-acer-confirms-breach-after-160gb-of-data-for-sale-on-hacking-forum

[Source](https://www.bleepingcomputer.com/news/security/acer-confirms-breach-after-160gb-of-data-for-sale-on-hacking-forum/){:target="_blank" rel="noopener"}

> Taiwanese computer giant Acer confirmed that it suffered a data breach after threat actors hacked a server hosting private documents used by repair technicians. [...]
