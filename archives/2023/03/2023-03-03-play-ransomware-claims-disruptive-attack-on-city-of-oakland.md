Title: Play ransomware claims disruptive attack on City of Oakland
Date: 2023-03-03T10:42:21-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-03-play-ransomware-claims-disruptive-attack-on-city-of-oakland

[Source](https://www.bleepingcomputer.com/news/security/play-ransomware-claims-disruptive-attack-on-city-of-oakland/){:target="_blank" rel="noopener"}

> The Play ransomware gang has taken responsibility for a cyberattack on the City of Oakland that has disrupted IT systems since mid-February. [...]
