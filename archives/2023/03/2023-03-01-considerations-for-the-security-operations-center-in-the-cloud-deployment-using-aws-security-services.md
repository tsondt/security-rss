Title: Considerations for the security operations center in the cloud: deployment using AWS security services
Date: 2023-03-01T17:35:43+00:00
Author: Stuart Gregg
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;architect;centralization;decentralization;operations;SecOps;Security;Security Blog;Securityoperations;tools
Slug: 2023-03-01-considerations-for-the-security-operations-center-in-the-cloud-deployment-using-aws-security-services

[Source](https://aws.amazon.com/blogs/security/considerations-for-the-security-operations-center-in-the-cloud-deployment-using-aws-security-services/){:target="_blank" rel="noopener"}

> Welcome back. If you’re joining this series for the first time, we recommend that you read the first blog post in this series, Considerations for security operations in the cloud, for some context on what we will discuss and deploy in this blog post. In the earlier post, we talked through the different operating models [...]
