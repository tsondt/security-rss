Title: Be threat ready for 2023: Join us for Google Cloud Security Talks in March
Date: 2023-03-15T17:00:00+00:00
Author: Apurva Dave
Category: GCP Security
Tags: Identity & Security
Slug: 2023-03-15-be-threat-ready-for-2023-join-us-for-google-cloud-security-talks-in-march

[Source](https://cloud.google.com/blog/products/identity-security/join-us-for-google-cloud-security-talks-in-march/){:target="_blank" rel="noopener"}

> At Google Cloud, our north star for security success today is to help customers apply cloud-scale, modern security everywhere they operate. As part of our mission to help customers achieve these objectives, we host a quarterly digital discussion event, Google Cloud Security Talks, where we bring together experts from the Google Cloud family and across the industry at large. We’ve designed these sessions to share insights, best practices, and ways to leverage new capabilities to help increase resilience against modern risks and threats. The first installment of Google Cloud Security Talks 2023 on March 22 will focus on transforming security [...]
