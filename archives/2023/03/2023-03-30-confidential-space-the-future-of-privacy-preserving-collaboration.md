Title: Confidential Space: The future of privacy-preserving collaboration
Date: 2023-03-30T16:00:00+00:00
Author: Nelly Porter
Category: GCP Security
Tags: Security & Identity
Slug: 2023-03-30-confidential-space-the-future-of-privacy-preserving-collaboration

[Source](https://cloud.google.com/blog/products/identity-security/confidential-space-is-ga/){:target="_blank" rel="noopener"}

> Today, we are happy to announce that Confidential Space is Generally Available. Confidential Space builds on our Confidential Computing portfolio. It provides a secure enclave, also known as a Trusted Execution Environment (TEE), that our Google Cloud customers can leverage for privacy-focused use cases such as joint data analysis and machine learning (ML) model training. Importantly, Confidential Space is designed to protect data from all parties involved — including hardened protection against cloud service provider access. Back in October 2022, we previewed secure multi-party collaboration with Confidential Space. We have since worked diligently to enhance functionality working with multiple customers [...]
