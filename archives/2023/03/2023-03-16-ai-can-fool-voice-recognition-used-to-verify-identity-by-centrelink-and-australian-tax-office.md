Title: AI can fool voice recognition used to verify identity by Centrelink and Australian tax office
Date: 2023-03-16T14:00:15+00:00
Author: Nick Evershed and Josh Taylor
Category: The Guardian
Tags: Artificial intelligence (AI);Australia news;Data and computer security;Data protection;Technology;Tax;Welfare
Slug: 2023-03-16-ai-can-fool-voice-recognition-used-to-verify-identity-by-centrelink-and-australian-tax-office

[Source](https://www.theguardian.com/technology/2023/mar/16/voice-system-used-to-verify-identity-by-centrelink-can-be-fooled-by-ai){:target="_blank" rel="noopener"}

> Exclusive: Voiceprint program used by millions of Australians to access data held by government agencies shown to have a serious security flaw Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast A voice identification system used by the Australian government for millions of people has a serious security flaw, a Guardian Australia investigation has found. Centrelink and the Australian Taxation Office (ATO) both give people the option of using a “voiceprint”, along with other information, to verify their identity over the phone, allowing them to then [...]
