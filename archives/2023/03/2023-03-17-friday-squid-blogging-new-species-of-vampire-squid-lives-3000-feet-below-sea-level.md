Title: Friday Squid Blogging: New Species of Vampire Squid Lives 3,000 Feet below Sea Level
Date: 2023-03-17T21:19:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-03-17-friday-squid-blogging-new-species-of-vampire-squid-lives-3000-feet-below-sea-level

[Source](https://www.schneier.com/blog/archives/2023/03/friday-squid-blogging-new-species-of-vampire-squid-lives-3000-feet-below-sea-level.html){:target="_blank" rel="noopener"}

> At least, it seems to be a new species. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
