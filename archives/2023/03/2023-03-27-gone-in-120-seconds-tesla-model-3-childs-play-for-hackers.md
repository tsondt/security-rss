Title: Gone in 120 seconds: Tesla Model 3 child's play for hackers
Date: 2023-03-27T11:32:11+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2023-03-27-gone-in-120-seconds-tesla-model-3-childs-play-for-hackers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/27/in_brief_security/){:target="_blank" rel="noopener"}

> Plus OIG finds Uncle Sam fibbed over Login.gov In brief A team of hackers from French security shop Synacktiv have won $100,000 and a Tesla Model 3 after subverting the Muskmobile's entertainment system, and from there opening up the car's core management systems.... [...]
