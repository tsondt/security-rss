Title: Ferrari discloses data breach after receiving ransom demand
Date: 2023-03-20T19:20:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-20-ferrari-discloses-data-breach-after-receiving-ransom-demand

[Source](https://www.bleepingcomputer.com/news/security/ferrari-discloses-data-breach-after-receiving-ransom-demand/){:target="_blank" rel="noopener"}

> Ferrari has disclosed a data breach following a ransom demand received after attackers gained access to some of the company's IT systems. [...]
