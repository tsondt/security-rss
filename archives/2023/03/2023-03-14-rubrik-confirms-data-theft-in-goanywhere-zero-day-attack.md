Title: Rubrik confirms data theft in GoAnywhere zero-day attack
Date: 2023-03-14T16:43:43-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-03-14-rubrik-confirms-data-theft-in-goanywhere-zero-day-attack

[Source](https://www.bleepingcomputer.com/news/security/rubrik-confirms-data-theft-in-goanywhere-zero-day-attack/){:target="_blank" rel="noopener"}

> Cybersecurity company Rubrik has confirmed that its data was stolen using a zero-day vulnerability in the Fortra GoAnywhere secure file transfer platform. [...]
