Title: Critical infrastructure gear is full of flaws, but hey, at least it's certified
Date: 2023-03-23T21:59:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-03-23-critical-infrastructure-gear-is-full-of-flaws-but-hey-at-least-its-certified

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/23/critical_infrastructure_hardware_flaws/){:target="_blank" rel="noopener"}

> Security researchers find bugs, big and small, in every industrial box probed Devices used in critical infrastructure are riddled with vulnerabilities that can cause denial of service, allow configuration manipulation, and achieve remote code execution, according to security researchers.... [...]
