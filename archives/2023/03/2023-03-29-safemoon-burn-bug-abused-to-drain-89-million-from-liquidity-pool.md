Title: SafeMoon ‘burn’ bug abused to drain $8.9 million from liquidity pool
Date: 2023-03-29T14:48:56-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: CryptoCurrency;Security
Slug: 2023-03-29-safemoon-burn-bug-abused-to-drain-89-million-from-liquidity-pool

[Source](https://www.bleepingcomputer.com/news/cryptocurrency/safemoon-burn-bug-abused-to-drain-89-million-from-liquidity-pool/){:target="_blank" rel="noopener"}

> The SafeMoon token liquidity pool lost $8.9 million after a hacker exploited a newly created 'burn' smart contract function that artificially inflated the price, allowing the actors to sell SafeMoon at a much higher price. [...]
