Title: CloudPanel installations use the same SSL certificate private key
Date: 2023-03-23T11:56:28-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-23-cloudpanel-installations-use-the-same-ssl-certificate-private-key

[Source](https://www.bleepingcomputer.com/news/security/cloudpanel-installations-use-the-same-ssl-certificate-private-key/){:target="_blank" rel="noopener"}

> Self-hosted web administration solution CloudPanel was found to have several security issues, including using the same SSL certificate private key across all installations and unintentional overwriting of firewall rules to default to weaker settings. [...]
