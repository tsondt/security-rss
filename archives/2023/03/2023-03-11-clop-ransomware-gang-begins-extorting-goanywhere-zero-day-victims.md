Title: Clop ransomware gang begins extorting GoAnywhere zero-day victims
Date: 2023-03-11T14:36:12-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-03-11-clop-ransomware-gang-begins-extorting-goanywhere-zero-day-victims

[Source](https://www.bleepingcomputer.com/news/security/clop-ransomware-gang-begins-extorting-goanywhere-zero-day-victims/){:target="_blank" rel="noopener"}

> The Clop ransomware gang has begun extorting companies whose data was stolen using a zero-day vulnerability in the Fortra GoAnywhere MFT secure file-sharing solution. [...]
