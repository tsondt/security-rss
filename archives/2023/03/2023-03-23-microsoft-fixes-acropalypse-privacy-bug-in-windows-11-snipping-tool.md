Title: Microsoft fixes Acropalypse privacy bug in Windows 11 Snipping Tool
Date: 2023-03-23T13:23:47-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-03-23-microsoft-fixes-acropalypse-privacy-bug-in-windows-11-snipping-tool

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-fixes-acropalypse-privacy-bug-in-windows-11-snipping-tool/){:target="_blank" rel="noopener"}

> Microsoft is testing an updated version of the Windows 11 Snipping Tool that fixes a recently disclosed 'Acropalypse' privacy flaw that allows the partial restoration of cropped images. [...]
