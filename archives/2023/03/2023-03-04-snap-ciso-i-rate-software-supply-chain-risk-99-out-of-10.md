Title: Snap CISO: I rate software supply chain risk 9.9 out of 10
Date: 2023-03-04T00:01:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-04-snap-ciso-i-rate-software-supply-chain-risk-99-out-of-10

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/04/snap_ciso_supply_chain_security/){:target="_blank" rel="noopener"}

> 'Understanding your inventory is absolutely No. 1' he tells The Reg SCSW On a scale of 1 to 10, 10 being the highest risk, Snap Chief Information Security Officer Jim Higgins rates software supply chain risk "about 9.9"... [...]
