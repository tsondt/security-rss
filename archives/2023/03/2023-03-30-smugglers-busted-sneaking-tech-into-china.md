Title: Smugglers busted sneaking tech into China
Date: 2023-03-30T03:02:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-03-30-smugglers-busted-sneaking-tech-into-china

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/30/smugglers_busted_sneaking_tech_into/){:target="_blank" rel="noopener"}

> 'Intel inside' a suspiciously baggy t-shirt gave the game away – as did a truckload of parts International Talk Like a Pirate Day is still months away – circle September 19 on your calendar, me hearties! – but The Register has found news of technology smuggling in China that suggests a buccaneering approach to imports.... [...]
