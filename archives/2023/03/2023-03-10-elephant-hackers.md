Title: Elephant Hackers
Date: 2023-03-10T20:05:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;hacking
Slug: 2023-03-10-elephant-hackers

[Source](https://www.schneier.com/blog/archives/2023/03/elephant-hackers.html){:target="_blank" rel="noopener"}

> An elephant uses its right-of-way privileges to stop sugar-cane trucks and grab food. [...]
