Title: Mass Ransomware Attack
Date: 2023-03-23T11:05:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;hacking;ransomware;vulnerabilities
Slug: 2023-03-23-mass-ransomware-attack

[Source](https://www.schneier.com/blog/archives/2023/03/mass-ransomware-attack.html){:target="_blank" rel="noopener"}

> A vulnerability in a popular data transfer tool has resulted in a mass ransomware attack : TechCrunch has learned of dozens of organizations that used the affected GoAnywhere file transfer software at the time of the ransomware attack, suggesting more victims are likely to come forward. However, while the number of victims of the mass-hack is widening, the known impact is murky at best. Since the attack in late January or early February—the exact date is not known—Clop has disclosed less than half of the 130 organizations it claimed to have compromised via GoAnywhere, a system that can be hosted [...]
