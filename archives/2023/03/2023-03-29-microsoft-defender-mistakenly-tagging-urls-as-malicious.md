Title: Microsoft Defender mistakenly tagging URLs as malicious
Date: 2023-03-29T11:38:39-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-03-29-microsoft-defender-mistakenly-tagging-urls-as-malicious

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-mistakenly-tagging-urls-as-malicious/){:target="_blank" rel="noopener"}

> Microsoft Defender is mistakenly flagging legitimate links as malicious, with some customers having already received dozens of alert emails since the issues began over five hours ago. [...]
