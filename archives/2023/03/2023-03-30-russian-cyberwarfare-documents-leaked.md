Title: Russian Cyberwarfare Documents Leaked
Date: 2023-03-30T22:00:03+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberespionage;cyberwar;espionage;hacking;leaks;Russia;whistleblowers
Slug: 2023-03-30-russian-cyberwarfare-documents-leaked

[Source](https://www.schneier.com/blog/archives/2023/03/russian-cyberwarfare-documents-leaked.html){:target="_blank" rel="noopener"}

> Now this is interesting: Thousands of pages of secret documents reveal how Vulkan’s engineers have worked for Russian military and intelligence agencies to support hacking operations, train operatives before attacks on national infrastructure, spread disinformation and control sections of the internet. The company’s work is linked to the federal security service or FSB, the domestic spy agency; the operational and intelligence divisions of the armed forces, known as the GOU and GRU; and the SVR, Russia’s foreign intelligence organisation. Lots more at the link. The documents are in Russian, so it will be a while before we get translations. [...]
