Title: Google Pixel flaw allowed recovery of redacted, cropped images
Date: 2023-03-20T10:54:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2023-03-20-google-pixel-flaw-allowed-recovery-of-redacted-cropped-images

[Source](https://www.bleepingcomputer.com/news/security/google-pixel-flaw-allowed-recovery-of-redacted-cropped-images/){:target="_blank" rel="noopener"}

> An 'Acropalypse' flaw in Google Pixel's Markup tool made it possible to partially recover edited or redacted screenshots and images, including those that have been cropped or had their contents masked, for the past five years. [...]
