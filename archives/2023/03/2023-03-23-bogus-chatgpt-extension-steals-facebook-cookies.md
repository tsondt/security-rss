Title: Bogus ChatGPT extension steals Facebook cookies
Date: 2023-03-23T07:29:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-23-bogus-chatgpt-extension-steals-facebook-cookies

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/23/chatgpt_fake_chrome_extension/){:target="_blank" rel="noopener"}

> All aboard the chatbot hype train! Next stop: Fraud Google has removed a ChatGPT extension from the Chrome store that steals Facebook session cookies – but not before more than 9,000 users installed the account-compromising bot.... [...]
