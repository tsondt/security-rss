Title: Hackers steal $197 million in crypto in Euler Finance attack
Date: 2023-03-13T12:58:50-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-03-13-hackers-steal-197-million-in-crypto-in-euler-finance-attack

[Source](https://www.bleepingcomputer.com/news/security/hackers-steal-197-million-in-crypto-in-euler-finance-attack/){:target="_blank" rel="noopener"}

> Lending protocol Euler Finance was hit by a cryptocurrency flash loan attack on Sunday, with the threat actor stealing $197 million in multiple digital assets. [...]
