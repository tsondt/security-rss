Title: Google finds more Android, iOS zero-days used to install spyware
Date: 2023-03-29T08:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2023-03-29-google-finds-more-android-ios-zero-days-used-to-install-spyware

[Source](https://www.bleepingcomputer.com/news/security/google-finds-more-android-ios-zero-days-used-to-install-spyware/){:target="_blank" rel="noopener"}

> Google's Threat Analysis Group (TAG) discovered several exploit chains using Android, iOS, and Chrome zero-day and n-day vulnerabilities to install commercial spyware and malicious apps on targets' devices. [...]
