Title: GitHub.com rotates its exposed private SSH key
Date: 2023-03-24T04:33:19-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-03-24-githubcom-rotates-its-exposed-private-ssh-key

[Source](https://www.bleepingcomputer.com/news/security/githubcom-rotates-its-exposed-private-ssh-key/){:target="_blank" rel="noopener"}

> GitHub has rotated its private SSH key for GitHub.com after the secret was was accidentally published in a public GitHub repository. The software development and version control service says, the private RSA key was only "briefly" exposed, but that it took action out of "an abundance of caution." [...]
