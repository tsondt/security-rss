Title: Upcoming Speaking Engagements
Date: 2023-03-14T19:08:15+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2023-03-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2023/03/upcoming-speaking-engagements-28.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking on “ How to Reclaim Power in the Digital World ” at EPFL in Lausanne, Switzerland, on Thursday, March 16, 2023, at 5:30 PM CET. I’ll be discussing my new book A Hacker’s Mind: How the Powerful Bend Society’s Rules at Harvard Science Center in Cambridge, Massachusetts, USA, on Friday, March 31, 2023 at 6:00 PM EDT. I’ll be discussing my book A Hacker’s Mind with Julia Angwin at the Ford Foundation Center for Social Justice in New York City, on Thursday, April 6, [...]
