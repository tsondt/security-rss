Title: German political parties accused of microtargeting voters on Facebook
Date: 2023-03-22T12:31:09+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-03-22-german-political-parties-accused-of-microtargeting-voters-on-facebook

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/22/germany_complaints_noyb/){:target="_blank" rel="noopener"}

> Country's super strong data rights under magnifying glass after half a dozen complaints filed Remember the Who Targets Me browser extension from privacy activists at Noyb? The group yesterday filed explosive complaints based on log records from the extension that claim six of Germany's political parties broke European data law when they targeted voters on Facebook's adtech platform.... [...]
