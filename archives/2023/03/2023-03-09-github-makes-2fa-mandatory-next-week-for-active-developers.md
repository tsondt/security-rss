Title: GitHub makes 2FA mandatory next week for active developers
Date: 2023-03-09T12:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-09-github-makes-2fa-mandatory-next-week-for-active-developers

[Source](https://www.bleepingcomputer.com/news/security/github-makes-2fa-mandatory-next-week-for-active-developers/){:target="_blank" rel="noopener"}

> GitHub will start requiring active developers to enable two-factor authentication (2FA) on their accounts beginning next week, on March 13. [...]
