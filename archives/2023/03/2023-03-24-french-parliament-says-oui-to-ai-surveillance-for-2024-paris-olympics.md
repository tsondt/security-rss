Title: French parliament says <em>oui</em> to AI surveillance for 2024 Paris Olympics
Date: 2023-03-24T06:24:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-03-24-french-parliament-says-oui-to-ai-surveillance-for-2024-paris-olympics

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/24/al_surveillance_french/){:target="_blank" rel="noopener"}

> Liberté, égalité, reconnaissance faciale for all Despite the opposition of 38 civil society groups, the French National Assembly has approved the use of algorithmic video surveillance during the 2024 Paris Olympics.... [...]
