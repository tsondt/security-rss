Title: WiFi protocol flaw allows attackers to hijack network traffic
Date: 2023-03-28T15:05:30-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-28-wifi-protocol-flaw-allows-attackers-to-hijack-network-traffic

[Source](https://www.bleepingcomputer.com/news/security/wifi-protocol-flaw-allows-attackers-to-hijack-network-traffic/){:target="_blank" rel="noopener"}

> Cybersecurity researchers have discovered a fundamental security flaw in the design of the IEEE 802.11 WiFi protocol standard, allowing attackers to trick access points into leaking network frames in plaintext form. [...]
