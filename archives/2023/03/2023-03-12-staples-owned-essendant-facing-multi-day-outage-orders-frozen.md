Title: Staples-owned Essendant facing multi-day "outage," orders frozen
Date: 2023-03-12T13:15:08-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-03-12-staples-owned-essendant-facing-multi-day-outage-orders-frozen

[Source](https://www.bleepingcomputer.com/news/security/staples-owned-essendant-facing-multi-day-outage-orders-frozen/){:target="_blank" rel="noopener"}

> Staples-owned Essendant, a wholesale distributor of stationary and office supplies, is experiencing a multi-day systems "outage" preventing customers and suppliers from placing and fulfilling online orders. [...]
