Title: Microsoft Defender shoots down legit URLs as malicious
Date: 2023-03-29T18:31:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-03-29-microsoft-defender-shoots-down-legit-urls-as-malicious

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/29/microsoft_defender_url_alerts/){:target="_blank" rel="noopener"}

> Those hoping to use nefarious websites like, er, Zoom are overrun by alerts. Redmond 'investigating' Updated Microsoft's at-times-glitchy Defender service is again causing headaches for IT admins by flagging legitimate URLs as malicious.... [...]
