Title: UK creates fake DDoS-for-hire sites to identify cybercriminals
Date: 2023-03-24T12:35:07-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-03-24-uk-creates-fake-ddos-for-hire-sites-to-identify-cybercriminals

[Source](https://www.bleepingcomputer.com/news/security/uk-creates-fake-ddos-for-hire-sites-to-identify-cybercriminals/){:target="_blank" rel="noopener"}

> The U.K.'s National Crime Agency (NCA) revealed today that they created multiple fake DDoS-for-hire service websites to identify cybercriminals who utilize these platforms to attack organizations. [...]
