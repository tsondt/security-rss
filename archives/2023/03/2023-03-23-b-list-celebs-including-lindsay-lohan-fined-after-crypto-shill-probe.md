Title: B-List celebs including Lindsay Lohan fined after crypto shill probe
Date: 2023-03-23T06:30:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-03-23-b-list-celebs-including-lindsay-lohan-fined-after-crypto-shill-probe

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/23/lindsay_lohan_crypto/){:target="_blank" rel="noopener"}

> Didn't disclose payments as mastermind pumped up value of tokens with fake trades Eight very B-list celebrities have agreed to cough up fines after being accused of shilling a cryptocurrency without disclosing they were paid to do so, while the chap who apparently paid them has been charged with fraud.... [...]
