Title: BlackLotus bootkit bypasses UEFI Secure Boot on patched Windows 11
Date: 2023-03-02T18:20:53-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-03-02-blacklotus-bootkit-bypasses-uefi-secure-boot-on-patched-windows-11

[Source](https://www.bleepingcomputer.com/news/security/blacklotus-bootkit-bypasses-uefi-secure-boot-on-patched-windows-11/){:target="_blank" rel="noopener"}

> The developers of the BlackLotus UEFI bootkit have improved the malware with Secure Boot bypass capabilities that allow it to infected even fully patched Windows 11 systems. [...]
