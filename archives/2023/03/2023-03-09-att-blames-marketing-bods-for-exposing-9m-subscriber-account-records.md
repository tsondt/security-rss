Title: AT&amp;T blames marketing bods for exposing 9M subscriber account records
Date: 2023-03-09T22:30:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-09-att-blames-marketing-bods-for-exposing-9m-subscriber-account-records

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/09/att_wireless_breach/){:target="_blank" rel="noopener"}

> Tells folks not to worry, it was very old and boring data AT&T has confirmed that miscreants had access to nine million of its wireless customers' account details after one of its vendor's networks suffered a security failure in January.... [...]
