Title: Protecting collocated servers from DDoS attacks using GRE tunnels
Date: 2023-03-09T10:10:00-05:00
Author: Sponsored by Gcore
Category: BleepingComputer
Tags: Security
Slug: 2023-03-09-protecting-collocated-servers-from-ddos-attacks-using-gre-tunnels

[Source](https://www.bleepingcomputer.com/news/security/protecting-collocated-servers-from-ddos-attacks-using-gre-tunnels/){:target="_blank" rel="noopener"}

> You can get DDoS protection remotely for your collocated server using a generic routing encapsulation (GRE) tunnel. We will explain how GRE tunnels combined with Gcore scrubbing centers can help keep your data safe. [...]
