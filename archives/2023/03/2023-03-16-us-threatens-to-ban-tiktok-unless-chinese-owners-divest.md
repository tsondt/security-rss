Title: US threatens to ban TikTok unless Chinese owners divest
Date: 2023-03-16T15:35:59+00:00
Author: Sam Levin and agencies
Category: The Guardian
Tags: TikTok;Biden administration;US news;Technology;Data and computer security
Slug: 2023-03-16-us-threatens-to-ban-tiktok-unless-chinese-owners-divest

[Source](https://www.theguardian.com/technology/2023/mar/15/us-joe-biden-tiktok-ban-chinese-owners-divest){:target="_blank" rel="noopener"}

> Move is latest escalation by lawmakers over fears user data could be passed on to China’s government The Biden administration has threatened to ban TikTok in the US unless the social media company’s Chinese owners divest their stakes in it, according to news reports on Wednesday. The move, first reported by the Wall Street Journal, is the most dramatic in a series of escalations by US officials and legislators, driven by fears that US user data held by the company could be passed on to China’s government. It also comes amid a global backlash to the popular video-based app over [...]
