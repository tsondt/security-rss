Title: You've been pwned, how much will each stolen customer SSN cost you? How about $7.5k?
Date: 2023-03-18T14:02:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-18-youve-been-pwned-how-much-will-each-stolen-customer-ssn-cost-you-how-about-75k

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/18/orlando_family_physicians_settlement/){:target="_blank" rel="noopener"}

> At the very least, with other costs on top A Florida healthcare group has settled a class-action lawsuit after thieves stole more than 447,000 patients' names, Social Security numbers, and sensitive medical information, from its servers.... [...]
