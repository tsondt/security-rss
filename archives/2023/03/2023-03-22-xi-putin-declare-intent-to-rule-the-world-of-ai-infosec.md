Title: Xi, Putin declare intent to rule the world of AI, infosec
Date: 2023-03-22T01:58:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-03-22-xi-putin-declare-intent-to-rule-the-world-of-ai-infosec

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/22/russia_china_joint_statement/){:target="_blank" rel="noopener"}

> 'Technological sovereignty is the key to sustainability' states Russian despot Russian president Vladimir Putin and his Chinese counterpart Xi Jinping have set themselves the goal of dominating the world of information technology.... [...]
