Title: Alleged BreachForums owner ‘Pompompurin’ arrested on cybercrime charges
Date: 2023-03-17T19:32:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-17-alleged-breachforums-owner-pompompurin-arrested-on-cybercrime-charges

[Source](https://www.bleepingcomputer.com/news/security/alleged-breachforums-owner-pompompurin-arrested-on-cybercrime-charges/){:target="_blank" rel="noopener"}

> U.S. law enforcement arrested on Wednesday a New York man believed to be Pompompurin, the owner of the BreachForums hacking forum. [...]
