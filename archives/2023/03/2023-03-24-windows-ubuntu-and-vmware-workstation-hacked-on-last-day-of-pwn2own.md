Title: Windows, Ubuntu, and VMWare Workstation hacked on last day of Pwn2Own
Date: 2023-03-24T18:54:31-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-24-windows-ubuntu-and-vmware-workstation-hacked-on-last-day-of-pwn2own

[Source](https://www.bleepingcomputer.com/news/security/windows-ubuntu-and-vmware-workstation-hacked-on-last-day-of-pwn2own/){:target="_blank" rel="noopener"}

> On the third day of the Pwn2Own hacking contest, security researchers were awarded $185,000 after demonstrating 5 zero-day exploits targeting Windows 11, Ubuntu Desktop, and the VMware Workstation virtualization software. [...]
