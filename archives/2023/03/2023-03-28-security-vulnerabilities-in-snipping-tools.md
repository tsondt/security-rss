Title: Security Vulnerabilities in Snipping Tools
Date: 2023-03-28T11:13:32+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;privacy;vulnerabilities
Slug: 2023-03-28-security-vulnerabilities-in-snipping-tools

[Source](https://www.schneier.com/blog/archives/2023/03/security-vulnerabilities-in-snipping-tools.html){:target="_blank" rel="noopener"}

> Both Google’s Pixel’s Markup Tool and the Windows Snipping Tool have vulnerabilities that allow people to partially recover content that was edited out of images. [...]
