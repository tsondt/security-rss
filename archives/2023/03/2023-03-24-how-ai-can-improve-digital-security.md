Title: How AI can improve digital security
Date: 2023-03-24T16:00:00+00:00
Author: Royal Hansen
Category: GCP Security
Tags: AI & Machine Learning;Security & Identity
Slug: 2023-03-24-how-ai-can-improve-digital-security

[Source](https://cloud.google.com/blog/products/identity-security/how-ai-can-improve-digital-security/){:target="_blank" rel="noopener"}

> AI is having a transformative moment and causing profound shifts in what’s possible with technology. It has the power to unlock the potential of communities, companies, and countries around the world, bringing meaningful and positive change that could improve billions of peoples’ lives. Similarly, as these technologies advance, they have the potential to vastly improve how we identify, address, and reduce security risks. We’re at a key moment in our AI journey Breakthroughs in generative AI are fundamentally changing how people interact with technology. At Google Cloud, we’re committed to helping developers and organizations stay on top of these developments. [...]
