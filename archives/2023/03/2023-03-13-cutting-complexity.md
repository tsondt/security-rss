Title: Cutting complexity
Date: 2023-03-13T08:52:09+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-03-13-cutting-complexity

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/13/cutting_complexity/){:target="_blank" rel="noopener"}

> Ensuring cybersecurity defences do more with less Webinar It's like living in a fever dream out there in the world of cybersecurity. More and more sophisticated attacks, a tsunami of solutions offering a gilt-edged escape from the need to constantly reconfigure your defences, and relentless pressure to always stay one step ahead of the hackers.... [...]
