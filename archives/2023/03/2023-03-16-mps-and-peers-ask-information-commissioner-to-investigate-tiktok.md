Title: MPs and peers ask information commissioner to investigate TikTok
Date: 2023-03-16T20:27:09+00:00
Author: Dan Sabbagh and Dan Milmo
Category: The Guardian
Tags: TikTok;Technology;Politics;China;UK news;Apps;Oliver Dowden;Asia Pacific;World news;Data and computer security
Slug: 2023-03-16-mps-and-peers-ask-information-commissioner-to-investigate-tiktok

[Source](https://www.theguardian.com/technology/2023/mar/16/mps-and-peers-ask-information-commissioner-to-investigate-tiktok){:target="_blank" rel="noopener"}

> Letter argues that Chinese-owned video-sharing app could be in breach of UK law A cross-party group of MPs and peers have asked the information commissioner to investigate whether the Chinese-owned TikTok’s handling of personal information is in breach of UK law. The letter from the Inter-Parliamentary Alliance on China (IPAC) argues that TikTok cannot be compliant with data protection rules – and comes just hours after the UK announced a ban on the popular video-sharing app appearing on ministers’ and officials’ government-owned phones. Continue reading... [...]
