Title: STALKER 2 game developer hacked by Russian hacktivists, data stolen
Date: 2023-03-13T11:09:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2023-03-13-stalker-2-game-developer-hacked-by-russian-hacktivists-data-stolen

[Source](https://www.bleepingcomputer.com/news/security/stalker-2-game-developer-hacked-by-russian-hacktivists-data-stolen/){:target="_blank" rel="noopener"}

> GSC Game World, the developer of the highly-anticipated 'STALKER 2: Heart of Chornobyl' game, warned their systems were breached, allowing threat actors to steal game assets during the attack. [...]
