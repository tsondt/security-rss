Title: Google finds 18 zero-day vulnerabilities in Samsung Exynos chipsets
Date: 2023-03-16T16:33:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-16-google-finds-18-zero-day-vulnerabilities-in-samsung-exynos-chipsets

[Source](https://www.bleepingcomputer.com/news/security/google-finds-18-zero-day-vulnerabilities-in-samsung-exynos-chipsets/){:target="_blank" rel="noopener"}

> Project Zero, Google's zero-day bug-hunting team, discovered and reported 18 zero-day vulnerabilities in Samsung's Exynos chipsets used in mobile devices, wearables, and cars. [...]
