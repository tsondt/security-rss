Title: Join us at RSA Conference to transform cybersecurity with frontline intelligence and cloud innovation
Date: 2023-03-17T19:30:00+00:00
Author: Lorenz Jakober
Category: GCP Security
Tags: Security & Identity
Slug: 2023-03-17-join-us-at-rsa-conference-to-transform-cybersecurity-with-frontline-intelligence-and-cloud-innovation

[Source](https://cloud.google.com/blog/products/identity-security/join-google-cloud-and-mandiant-at-rsa-conference-2023/){:target="_blank" rel="noopener"}

> The promise of digital transformation is being challenged by the increasingly disruptive threat landscape. More sophisticated and capable adversaries have proliferated as nation-states pivot from cyber-espionage to compromise of private industry for financial gain. Their tactics shift and evolve rapidly as workloads and workforces become distributed and enterprises’ attack surface grows. And the security talent needed to help remains scarce and stubbornly grounded in ineffective, toil-based legacy approaches and tooling. aside_block [StructValue([(u'title', u'Hear monthly from our Cloud CISO in your inbox'), (u'body', <wagtail.wagtailcore.rich_text.RichText object at 0x3ec174b3b990>), (u'btn_text', u'Subscribe today'), (u'href', u'https://go.chronicle.security/cloudciso-newsletter-signup?utm_source=cgc-blog&utm_medium=blog&utm_campaign=FY23-Cloud-CISO-Perspectives-newsletter-blog-embed-CTA&utm_content=-&utm_term=-'), (u'image', <GAEImage: gcat small.jpg>)])] Join Mandiant and Google Cloud [...]
