Title: TikTok cannot be considered a private company, says Australian report
Date: 2023-03-19T23:30:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-03-19-tiktok-cannot-be-considered-a-private-company-says-australian-report

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/19/asia_tech_news_roundup/){:target="_blank" rel="noopener"}

> ALSO: Japan ends chip supply crimp on South Korea, APAC infosec spending surges; Philippines SIM registration stalls Asia In Brief ByteDance, the Chinese developer of TikTok, "can no longer be accurately described as a private enterprise" and is instead intertwined with China's government, according to a report [PDF] submitted to Australia's Select Committee on Foreign Interference through Social Media.... [...]
