Title: Prompt Injection Attacks on Large Language Models
Date: 2023-03-07T12:13:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;ChatGPT;cyberattack
Slug: 2023-03-07-prompt-injection-attacks-on-large-language-models

[Source](https://www.schneier.com/blog/archives/2023/03/prompt-injection-attacks-on-large-language-models.html){:target="_blank" rel="noopener"}

> This is a good survey on prompt injection attacks on large language models (like ChatGPT). Abstract: We are currently witnessing dramatic advances in the capabilities of Large Language Models (LLMs). They are already being adopted in practice and integrated into many systems, including integrated development environments (IDEs) and search engines. The functionalities of current LLMs can be modulated via natural language prompts, while their exact internal functionality remains implicit and unassessable. This property, which makes them adaptable to even unseen tasks, might also make them susceptible to targeted adversarial prompting. Recently, several ways to misalign LLMs using Prompt Injection (PI) [...]
