Title: Cloud CISO Perspectives: March 2023
Date: 2023-03-31T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Security & Identity
Slug: 2023-03-31-cloud-ciso-perspectives-march-2023

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-march-2023/){:target="_blank" rel="noopener"}

> Welcome to Cloud CISO Perspectives for March 2023. The Biden-Harris Administration released its National Cybersecurity Strategy on March 2, so this month I’d like to discuss how the strategy aligns with our approach to security at Google Cloud. While the strategy is intended to guide American cybersecurity efforts, it could have global implications for how the security industry interacts with governments around the world — and encourage more collaboration between policy makers and private enterprise on cybersecurity matters. aside_block [StructValue([(u'title', u'Hear monthly from our Cloud CISO in your inbox'), (u'body', <wagtail.wagtailcore.rich_text.RichText object at 0x3eea28808a10>), (u'btn_text', u'Subscribe today'), (u'href', u'https://go.chronicle.security/cloudciso-newsletter-signup?utm_source=cgc-blog&utm_medium=blog&utm_campaign=FY23-Cloud-CISO-Perspectives-newsletter-blog-embed-CTA&utm_content=-&utm_term=-'), (u'image', [...]
