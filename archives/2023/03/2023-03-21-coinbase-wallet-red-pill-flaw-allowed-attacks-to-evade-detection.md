Title: Coinbase Wallet 'Red Pill' flaw allowed attacks to evade detection
Date: 2023-03-21T10:45:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-21-coinbase-wallet-red-pill-flaw-allowed-attacks-to-evade-detection

[Source](https://www.bleepingcomputer.com/news/security/coinbase-wallet-red-pill-flaw-allowed-attacks-to-evade-detection/){:target="_blank" rel="noopener"}

> Coinbase wallet and other decentralized crypto apps (dapps) were found to be vulnerable to "red pill attacks," a method that can be used to hide malicious smart contract behavior from security features. [...]
