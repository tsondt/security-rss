Title: Lawyers cough up $200k after health data stolen in Microsoft Exchange pillaging
Date: 2023-03-27T22:45:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-27-lawyers-cough-up-200k-after-health-data-stolen-in-microsoft-exchange-pillaging

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/27/nyc_lawyers_security_data/){:target="_blank" rel="noopener"}

> In addition to $100k given to LockBit New York law firm Heidell, Pittoni, Murphy and Bach (HPMB) has agreed to pay $200,000 to settle a data-breach lawsuit related to the now-notorious Hafnium Microsoft Exchange attacks that siphoned sensitive data from victims around the world.... [...]
