Title: Multiple Internet to Baseband Remote Code Execution Vulnerabilities in Exynos Modems
Date: 2023-03-16T11:07:00.011000-07:00
Author: Google Project Zero (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2023-03-16-multiple-internet-to-baseband-remote-code-execution-vulnerabilities-in-exynos-modems

[Source](https://googleprojectzero.blogspot.com/2023/03/multiple-internet-to-baseband-remote-rce.html){:target="_blank" rel="noopener"}

> Posted by Tim Willis, Project Zero Note: Until security updates are available, users who wish to protect themselves from the baseband remote code execution vulnerabilities in Samsung’s Exynos chipsets can turn off Wi-Fi calling and Voice-over-LTE (VoLTE) in their device settings. Turning off these settings will remove the exploitation risk of these vulnerabilities. In late 2022 and early 2023, Project Zero reported eighteen 0-day vulnerabilities in Exynos Modems produced by Samsung Semiconductor. The four most severe of these eighteen vulnerabilities (CVE-2023-24033 and three other vulnerabilities that have yet to be assigned CVE-IDs) allowed for Internet-to-baseband remote code execution. Tests conducted [...]
