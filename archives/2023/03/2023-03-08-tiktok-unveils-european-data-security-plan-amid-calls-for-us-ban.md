Title: TikTok unveils European data security plan amid calls for US ban
Date: 2023-03-08T17:29:51+00:00
Author: Dan Milmo Global technology editor
Category: The Guardian
Tags: TikTok;US national security;Technology;Europe;World news;US news;China;Social media;Asia Pacific;Digital media;Media;Data and computer security
Slug: 2023-03-08-tiktok-unveils-european-data-security-plan-amid-calls-for-us-ban

[Source](https://www.theguardian.com/technology/2023/mar/08/tiktok-european-data-security-regime-us-ban-social-video-app){:target="_blank" rel="noopener"}

> Move comes as White House backs bill that could give it power to ban Chinese-owned app nationwide TikTok has announced a data security regime for protecting user information across Europe, as political pressure increases in the US to ban the social video app. The plan, known as Project Clover, involves user data being stored on servers in Ireland and Norway at an annual cost of €1.2bn (£1.1bn), while any data transfers outside Europe will be vetted by a third-party IT company. Continue reading... [...]
