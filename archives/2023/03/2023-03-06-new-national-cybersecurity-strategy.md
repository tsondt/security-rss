Title: New National Cybersecurity Strategy
Date: 2023-03-06T12:06:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;national security policy;software liability
Slug: 2023-03-06-new-national-cybersecurity-strategy

[Source](https://www.schneier.com/blog/archives/2023/03/new-national-cybersecurity-strategy.html){:target="_blank" rel="noopener"}

> Last week, the Biden Administration released a new National Cybersecurity Strategy (summary here ). There is lots of good commentary out there. It’s basically a smart strategy, but the hard parts are always the implementation details. It’s one thing to say that we need to secure our cloud infrastructure, and another to detail what the means technically, who pays for it, and who verifies that it’s been done. One of the provisions getting the most attention is a move to shift liability to software vendors, something I’ve been advocating for since at least 2003. Slashdot thread. [...]
