Title: ChatGPT may be a bigger cybersecurity risk than an actual benefit
Date: 2023-03-15T10:07:14-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-03-15-chatgpt-may-be-a-bigger-cybersecurity-risk-than-an-actual-benefit

[Source](https://www.bleepingcomputer.com/news/security/chatgpt-may-be-a-bigger-cybersecurity-risk-than-an-actual-benefit/){:target="_blank" rel="noopener"}

> ChatGPT made a splash with its believable AI-generated responses. However, it can help threat actors create convincing personas to steal credentials in phishing attacks. [...]
