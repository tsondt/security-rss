Title: China crisis is a TikToking time bomb
Date: 2023-03-27T09:30:10+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2023-03-27-china-crisis-is-a-tiktoking-time-bomb

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/27/china_crisis_is_a_tiktoking/){:target="_blank" rel="noopener"}

> ByteDance with the devil if you dare Opinion As country after country bans TikTok from official systems, it’s fair to ask what’s so dodgy about a social network filled with dance crazes, makeup advice and cats.... [...]
