Title: Windows 11, Tesla, Ubuntu, and macOS hacked at Pwn2Own 2023
Date: 2023-03-22T19:53:18-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-22-windows-11-tesla-ubuntu-and-macos-hacked-at-pwn2own-2023

[Source](https://www.bleepingcomputer.com/news/security/windows-11-tesla-ubuntu-and-macos-hacked-at-pwn2own-2023/){:target="_blank" rel="noopener"}

> On the first day of Pwn2Own Vancouver 2023, security researchers successfully demoed Tesla Model 3, Windows 11, and macOS zero-day exploits and exploit chains to win $375,000 and a Tesla Model 3. [...]
