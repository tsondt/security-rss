Title: Ransomware gang posts video of data stolen from Minneapolis schools
Date: 2023-03-08T12:37:04-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-08-ransomware-gang-posts-video-of-data-stolen-from-minneapolis-schools

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-posts-video-of-data-stolen-from-minneapolis-schools/){:target="_blank" rel="noopener"}

> The Medusa ransomware gang is demanding a $1,000,000 ransom from the Minneapolis Public Schools (MPS) district to delete data allegedly stolen in a ransomware attack. [...]
