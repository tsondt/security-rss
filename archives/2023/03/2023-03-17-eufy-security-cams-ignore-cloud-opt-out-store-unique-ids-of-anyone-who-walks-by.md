Title: Eufy security cams 'ignore cloud opt-out, store unique IDs' of anyone who walks by
Date: 2023-03-17T19:30:13+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-03-17-eufy-security-cams-ignore-cloud-opt-out-store-unique-ids-of-anyone-who-walks-by

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/17/eufy_lawsuit/){:target="_blank" rel="noopener"}

> Gadget maker accused of 'corporate voyeurism' by gathering up footage against your wishes A lawsuit filed against eufy security cam maker Anker Tech claims the biz assigns "unique identifiers" to the faces of any person who walks in front of its devices – and then stores that data in the cloud, "essentially logging the locations of unsuspecting individuals" when they stroll past.... [...]
