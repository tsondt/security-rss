Title: FTX cryptovillain Sam Bankman-Fried charged with bribing Chinese officials
Date: 2023-03-29T10:24:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-03-29-ftx-cryptovillain-sam-bankman-fried-charged-with-bribing-chinese-officials

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/29/us_charges_ftxs_sam_bankman/){:target="_blank" rel="noopener"}

> Court gives him new rules: Use one laptop, while living with the 'rents. US authorities have charged FTX co-founder Sam Bankman-Fried (aka SBF) with attempting to bribe Chinese officials with $40 million worth of cryptocurrency in exchange for unfreezing trading accounts.... [...]
