Title: Apple patches all the iThings, including iOS 15 hole under attack right now
Date: 2023-03-28T22:16:25+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-03-28-apple-patches-all-the-ithings-including-ios-15-hole-under-attack-right-now

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/28/apple_patches_iphone/){:target="_blank" rel="noopener"}

> Issue identified in February but owners of older kit weren't warned Happy belated Patch Tuesday from Cupertino: Apple has issued security updates for almost every piece of code it slings - including a fix for a vulnerability in older iOS devices the iGiant believes is under attack right now.... [...]
