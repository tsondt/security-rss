Title: Microsoft Patch Tuesday, March 2023 Edition
Date: 2023-03-15T15:19:32+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Security Tools;The Coming Storm;Time to Patch;CVE-2023-23397;CVE-2023-24800;Dustin Childs;Immersive Labs;Kevin Breen;Microsoft 365 Apps for Enterprise;Microsoft Patch Tuesday March 2023;Rapid7;Windows SmartScreen;Zero Day Initiative
Slug: 2023-03-15-microsoft-patch-tuesday-march-2023-edition

[Source](https://krebsonsecurity.com/2023/03/microsoft-patch-tuesday-march-2023-edition/){:target="_blank" rel="noopener"}

> Microsoft on Tuesday released updates to quash at least 74 security bugs in its Windows operating systems and software. Two of those flaws are already being actively attacked, including an especially severe weakness in Microsoft Outlook that can be exploited without any user interaction. The Outlook vulnerability ( CVE-2023-23397 ) affects all versions of Microsoft Outlook from 2013 to the newest. Microsoft said it has seen evidence that attackers are exploiting this flaw, which can be done without any user interaction by sending a booby-trapped email that triggers automatically when retrieved by the email server — before the email is [...]
