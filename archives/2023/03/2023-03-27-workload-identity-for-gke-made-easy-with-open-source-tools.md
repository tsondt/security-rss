Title: Workload Identity for GKE made easy with open source tools
Date: 2023-03-27T16:00:00+00:00
Author: Glen Yu
Category: GCP Security
Tags: Developers & Practitioners;Security & Identity;Containers & Kubernetes
Slug: 2023-03-27-workload-identity-for-gke-made-easy-with-open-source-tools

[Source](https://cloud.google.com/blog/products/containers-kubernetes/open-source-tools-can-help-gke-work-with-cloud-apis/){:target="_blank" rel="noopener"}

> Google Cloud offers a clever way of allowing Google Kubernetes Engine (GKE) workloads to safely and securely authenticate to Google APIs with minimal credentials exposure. I will illustrate this method using a tool called kaniko. What is kaniko? kaniko is an open source tool that allows you to build and push container images from Kubernetes pods when a Docker daemon is not easily accessible and you have no root access to the underlying machine. kaniko executes the build commands entirely in the userspace and has no dependency on the Docker daemon. This makes it a popular tool in continuous integration [...]
