Title: South Korea fines McDonald's for data leak from raw SMB share
Date: 2023-03-23T02:29:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-03-23-south-korea-fines-mcdonalds-for-data-leak-from-raw-smb-share

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/23/south_korea_privacy_fines_mcdonalds/){:target="_blank" rel="noopener"}

> British American Tobacco, Samsung, also burgered up their infosec South Korea's Personal Information Protection Commission has fined McDonald's, British American Tobacco, and Samsung for privacy breaches.... [...]
