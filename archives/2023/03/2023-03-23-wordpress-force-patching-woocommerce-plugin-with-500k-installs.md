Title: WordPress force patching WooCommerce plugin with 500K installs
Date: 2023-03-23T17:39:14-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-23-wordpress-force-patching-woocommerce-plugin-with-500k-installs

[Source](https://www.bleepingcomputer.com/news/security/wordpress-force-patching-woocommerce-plugin-with-500k-installs/){:target="_blank" rel="noopener"}

> Automattic, the company behind the WordPress content management system, is force installing a security update on hundreds of thousands of websites running the highly popular WooCommerce Payments for online stores. [...]
