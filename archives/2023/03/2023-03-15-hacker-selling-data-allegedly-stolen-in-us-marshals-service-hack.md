Title: Hacker selling data allegedly stolen in US Marshals Service hack
Date: 2023-03-15T14:06:14-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-15-hacker-selling-data-allegedly-stolen-in-us-marshals-service-hack

[Source](https://www.bleepingcomputer.com/news/security/hacker-selling-data-allegedly-stolen-in-us-marshals-service-hack/){:target="_blank" rel="noopener"}

> A threat actor is selling on a Russian-speaking hacking forum what they claim to be hundreds of gigabytes of data allegedly stolen from U.S. Marshals Service (USMS) servers. [...]
