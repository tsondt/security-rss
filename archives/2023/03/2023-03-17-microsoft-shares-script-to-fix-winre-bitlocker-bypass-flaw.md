Title: Microsoft shares script to fix WinRE BitLocker bypass flaw
Date: 2023-03-17T02:03:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-03-17-microsoft-shares-script-to-fix-winre-bitlocker-bypass-flaw

[Source](https://www.bleepingcomputer.com/news/security/microsoft-shares-script-to-fix-winre-bitlocker-bypass-flaw/){:target="_blank" rel="noopener"}

> Microsoft has released a script to make it easier to patch a BitLocker bypass security vulnerability in the Windows Recovery Environment (WinRE). [...]
