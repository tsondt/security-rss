Title: The Week in Ransomware - March 10th 2023 - Police Take Action
Date: 2023-03-10T17:34:39-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-03-10-the-week-in-ransomware-march-10th-2023-police-take-action

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-march-10th-2023-police-take-action/){:target="_blank" rel="noopener"}

> This week's biggest news was the coordinated, international law enforcement operation between Europol, the FBI, the Netherlands, Germany, and Ukraine that targeted the DoppelPaymer operation. [...]
