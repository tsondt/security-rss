Title: NetWire Remote Access Trojan Maker Arrested
Date: 2023-03-14T11:23:01+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybercrime;FBI;hacking;spyware
Slug: 2023-03-14-netwire-remote-access-trojan-maker-arrested

[Source](https://www.schneier.com/blog/archives/2023/03/netwire-remote-access-trojan-maker-arrested.html){:target="_blank" rel="noopener"}

> From Brian Krebs : A Croatian national has been arrested for allegedly operating NetWire, a Remote Access Trojan (RAT) marketed on cybercrime forums since 2012 as a stealthy way to spy on infected systems and siphon passwords. The arrest coincided with a seizure of the NetWire sales website by the U.S. Federal Bureau of Investigation (FBI). While the defendant in this case hasn’t yet been named publicly, the NetWire website has been leaking information about the likely true identity and location of its owner for the past 11 years. The article details the mistakes that led to the person’s address. [...]
