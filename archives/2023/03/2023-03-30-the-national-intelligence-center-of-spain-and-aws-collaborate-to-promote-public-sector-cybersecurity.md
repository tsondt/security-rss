Title: The National Intelligence Center of Spain and AWS collaborate to promote public sector cybersecurity
Date: 2023-03-30T06:16:48+00:00
Author: Borja Larrumbide
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;CCN;cybersecurity;Public Sector;Security Blog;Spain
Slug: 2023-03-30-the-national-intelligence-center-of-spain-and-aws-collaborate-to-promote-public-sector-cybersecurity

[Source](https://aws.amazon.com/blogs/security/the-national-intelligence-center-of-spain-and-aws-collaborate-to-promote-public-sector-cybersecurity/){:target="_blank" rel="noopener"}

> Spanish version » The National Intelligence Center and National Cryptological Center (CNI-CCN)—attached to the Spanish Ministry of Defense—and Amazon Web Services (AWS) have signed a strategic collaboration agreement to jointly promote cybersecurity and innovation in the public sector through AWS Cloud technology. Under the umbrella of this alliance, the CNI-CCN will benefit from the help [...]
