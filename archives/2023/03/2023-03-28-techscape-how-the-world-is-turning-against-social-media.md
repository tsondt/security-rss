Title: TechScape: How the world is turning against social media
Date: 2023-03-28T10:53:33+00:00
Author: Alex Hern
Category: The Guardian
Tags: Technology;TikTok;China;Social media;France;Cybercrime;Asia Pacific;Digital media;Europe;Internet;Espionage;Data and computer security;Microsoft;Computing;Artificial intelligence (AI)
Slug: 2023-03-28-techscape-how-the-world-is-turning-against-social-media

[Source](https://www.theguardian.com/technology/2023/mar/28/techscape-france-tiktok-ban-facebook){:target="_blank" rel="noopener"}

> France has banned not only TikTok from government phones, but Facebook and Twitter, too. Could this be a tipping point for big tech? Plus, AI-generated pictures of the pope signal a new type of viral image Don’t get TechScape delivered to your inbox? Sign up for the full article here Government workers in the UK, US, Canada and European Union (the list will have grown by the time you read this) are banned from installing TikTok on their phones. On Friday, France joined that list, preventing its civil servants from installing TikTok – and everything else. From the government’s press [...]
