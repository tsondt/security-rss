Title: Ex-Meta security staffer accuses Greece of spying on her phone
Date: 2023-03-21T08:31:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-21-ex-meta-security-staffer-accuses-greece-of-spying-on-her-phone

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/21/meta_employee_spyware/){:target="_blank" rel="noopener"}

> Beware of Greeks bearing GIFs Meta's former security policy manager, who split her time between the US and Greece, is reportedly suing the Hellenic national intelligence service for hacking her phone.... [...]
