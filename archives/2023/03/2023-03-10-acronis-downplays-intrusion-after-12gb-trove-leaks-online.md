Title: Acronis downplays intrusion after 12GB trove leaks online
Date: 2023-03-10T03:45:46+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-03-10-acronis-downplays-intrusion-after-12gb-trove-leaks-online

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/10/acronis_data_breach_details/){:target="_blank" rel="noopener"}

> Cyber-thief said goal was to 'humiliate' data-protection biz The CISO of Acronis has downplayed what appeared to be an intrusion into its systems, insisting only one customer was affected, using stolen credentials, and that all other data remains safe.... [...]
