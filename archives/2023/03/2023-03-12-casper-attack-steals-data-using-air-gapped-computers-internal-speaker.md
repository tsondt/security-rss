Title: CASPER attack steals data using air-gapped computer's internal speaker
Date: 2023-03-12T10:18:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-12-casper-attack-steals-data-using-air-gapped-computers-internal-speaker

[Source](https://www.bleepingcomputer.com/news/security/casper-attack-steals-data-using-air-gapped-computers-internal-speaker/){:target="_blank" rel="noopener"}

> Researchers at the School of Cyber Security at Korea University, Seoul, have presented a new covert channel attack named CASPER can leak data from air-gapped computers to a nearby smartphone at a rate of 20bits/sec. [...]
