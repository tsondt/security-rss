Title: Secure mail
Date: 2023-03-23T09:48:08+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-03-23-secure-mail

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/23/secure_mail/){:target="_blank" rel="noopener"}

> Protection from business email compromise Webinar In the distant past, a master forger with a quill could fake a signature on the end of a letter but at least then you had time to consider the potential for fraud before any damage could be done. In the digital age of email, it's increasingly hard to spot a scam's threat to your security and react in time.... [...]
