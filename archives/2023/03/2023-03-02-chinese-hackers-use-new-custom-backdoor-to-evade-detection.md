Title: Chinese hackers use new custom backdoor to evade detection
Date: 2023-03-02T15:09:01-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-02-chinese-hackers-use-new-custom-backdoor-to-evade-detection

[Source](https://www.bleepingcomputer.com/news/security/chinese-hackers-use-new-custom-backdoor-to-evade-detection/){:target="_blank" rel="noopener"}

> The Chinese cyber espionage hacking group Mustang Panda was seen deploying a new custom backdoor named 'MQsTTang' in attacks starting this year. [...]
