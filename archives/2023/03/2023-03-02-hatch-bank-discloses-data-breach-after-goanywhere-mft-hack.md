Title: Hatch Bank discloses data breach after GoAnywhere MFT hack
Date: 2023-03-02T14:33:21-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-03-02-hatch-bank-discloses-data-breach-after-goanywhere-mft-hack

[Source](https://www.bleepingcomputer.com/news/security/hatch-bank-discloses-data-breach-after-goanywhere-mft-hack/){:target="_blank" rel="noopener"}

> Fintech banking platform Hatch Bank has reported a data breach after hackers stole the personal information of almost 140,000 customers from the company's Fortra GoAnywhere MFT secure file-sharing platform. [...]
