Title: CI/CD: Necessary for modern software development, yet it carries a lot of risk
Date: 2023-03-02T23:10:08+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-03-02-cicd-necessary-for-modern-software-development-yet-it-carries-a-lot-of-risk

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/02/cicd_supply_chain_security/){:target="_blank" rel="noopener"}

> With great speed comes great insecurity SCSW CI/CD over the past decade has become the cornerstone of modern software development.... [...]
