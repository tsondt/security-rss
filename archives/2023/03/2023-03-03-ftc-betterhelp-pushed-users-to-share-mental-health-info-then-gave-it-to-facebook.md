Title: FTC: BetterHelp pushed users to share mental health info then gave it to Facebook
Date: 2023-03-03T21:30:14+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-03-03-ftc-betterhelp-pushed-users-to-share-mental-health-info-then-gave-it-to-facebook

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/03/ftc_online_counseling_betterhelp/){:target="_blank" rel="noopener"}

> Feds propose $7.8M payment and ban on revealing 'sensitive' data to settle complaint Even if you don't know anyone who has used BetterHelp's services, podcast fans will recognize it from its annoying adverts for its online therapists. American regulators, however, allege the company's relationship with the advertising industry is more perverse than a mere irritating jingle, claiming it betrayed loyalties that should lie with customers by passing on their mental health info to Facebook, Snapchat and others.... [...]
