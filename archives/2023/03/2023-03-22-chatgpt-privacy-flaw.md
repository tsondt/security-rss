Title: ChatGPT Privacy Flaw
Date: 2023-03-22T11:14:06+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;ChatGPT;cybersecurity;privacy
Slug: 2023-03-22-chatgpt-privacy-flaw

[Source](https://www.schneier.com/blog/archives/2023/03/chatgpt-privacy-flaw.html){:target="_blank" rel="noopener"}

> OpenAI has disabled ChatGPT’s privacy history, almost certainly because they had a security flaw where users were seeing each others’ histories. [...]
