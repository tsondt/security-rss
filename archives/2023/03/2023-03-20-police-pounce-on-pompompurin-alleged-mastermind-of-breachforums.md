Title: Police pounce on 'pompompurin' – alleged mastermind of BreachForums
Date: 2023-03-20T06:02:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-03-20-police-pounce-on-pompompurin-alleged-mastermind-of-breachforums

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/20/in_brief_security/){:target="_blank" rel="noopener"}

> Crypto laundering service gets cleaned up by police and SVB mess draws in more criminals In Brief A man accused of being the head of one of the biggest criminal online souks, BreachForums, has been arrested in Peekskill, New York.... [...]
