Title: Google Cloud and FS-ISAC team up to advance financial services security
Date: 2023-03-16T16:00:00+00:00
Author: Etienne De Burgh
Category: GCP Security
Tags: Financial Services;Identity & Security
Slug: 2023-03-16-google-cloud-and-fs-isac-team-up-to-advance-financial-services-security

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-fs-isac-advance-security-in-financial-services/){:target="_blank" rel="noopener"}

> Google Cloud is committed to strengthening the security and resiliency of financial services organizations and making the Internet a safer place for all organizations to conduct transactions and business. While building a secure and resilient ecosystem is a joint responsibility, we want to ensure that we’re working together to build a community of trust. To advance this mission and strengthen our commitment to the financial sector, Google Cloud is announcing today that we have joined the Financial Services Information Security and Analysis Center ’s Critical Providers Program. While Google Cloud has been a long-standing supporter of FS-ISAC, as have our [...]
