Title: US House reps, staff health data swiped in cyber-heist
Date: 2023-03-09T21:27:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-03-09-us-house-reps-staff-health-data-swiped-in-cyber-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/09/data_breach_us_house/){:target="_blank" rel="noopener"}

> Data for sale via dark web, Senate in line of fire, too Health data and other personal information of members of Congress and staff were stolen during a breach of servers run by DC Health Care Link and are now up for sale on the dark web.... [...]
