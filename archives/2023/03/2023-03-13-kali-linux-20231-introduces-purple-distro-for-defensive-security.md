Title: Kali Linux 2023.1 introduces 'Purple' distro for defensive security
Date: 2023-03-13T15:10:56-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-03-13-kali-linux-20231-introduces-purple-distro-for-defensive-security

[Source](https://www.bleepingcomputer.com/news/security/kali-linux-20231-introduces-purple-distro-for-defensive-security/){:target="_blank" rel="noopener"}

> ​Offensive Security has released ​Kali Linux 2023.1, the first version of 2023 and the project's 10th anniversary, with a new distro called 'Kali Purple,' aimed at Blue and Purple teamers for defensive security. [...]
