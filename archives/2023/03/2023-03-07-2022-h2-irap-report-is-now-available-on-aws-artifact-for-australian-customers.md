Title: 2022 H2 IRAP report is now available on AWS Artifact for Australian customers
Date: 2023-03-07T21:42:07+00:00
Author: Patrick Chang
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Auditing;Australia;AWS Artifact;AWS security;Compliance;IRAP;Security;Security Blog
Slug: 2023-03-07-2022-h2-irap-report-is-now-available-on-aws-artifact-for-australian-customers

[Source](https://aws.amazon.com/blogs/security/2022-h2-irap-report-is-now-available-on-aws-artifact-for-australian-customers/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is excited to announce that a new Information Security Registered Assessors Program (IRAP) report (2022 H2) is now available through AWS Artifact. An independent Australian Signals Directorate (ASD) certified IRAP assessor completed the IRAP assessment of AWS in December 2022. The new IRAP report includes an additional six AWS services, as well as the [...]
