Title: GitHub’s secret scanning alerts now available for all public repos
Date: 2023-03-01T12:33:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-01-githubs-secret-scanning-alerts-now-available-for-all-public-repos

[Source](https://www.bleepingcomputer.com/news/security/github-s-secret-scanning-alerts-now-available-for-all-public-repos/){:target="_blank" rel="noopener"}

> GitHub has announced that its secret scanning alerts service is now generally available to all public repositories and can be enabled to detect leaked secrets across an entire publishing history. [...]
