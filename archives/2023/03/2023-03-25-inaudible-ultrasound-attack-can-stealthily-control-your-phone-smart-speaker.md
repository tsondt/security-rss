Title: Inaudible ultrasound attack can stealthily control your phone, smart speaker
Date: 2023-03-25T11:14:07-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-25-inaudible-ultrasound-attack-can-stealthily-control-your-phone-smart-speaker

[Source](https://www.bleepingcomputer.com/news/security/inaudible-ultrasound-attack-can-stealthily-control-your-phone-smart-speaker/){:target="_blank" rel="noopener"}

> American university researchers have developed a novel attack which they named "Near-Ultrasound Inaudible Trojan" (NUIT) that can launch silent attacks against devices powered by voice assistants, like smartphones, smart speakers, and other IoTs. [...]
