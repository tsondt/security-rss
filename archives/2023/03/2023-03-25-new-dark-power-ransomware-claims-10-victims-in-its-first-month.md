Title: New Dark Power ransomware claims 10 victims in its first month
Date: 2023-03-25T12:29:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-25-new-dark-power-ransomware-claims-10-victims-in-its-first-month

[Source](https://www.bleepingcomputer.com/news/security/new-dark-power-ransomware-claims-10-victims-in-its-first-month/){:target="_blank" rel="noopener"}

> A new ransomware operation named 'Dark Power' has appeared, and it has already listed its first victims on a dark web data leak site, threatening to publish the data if a ransom is not paid. [...]
