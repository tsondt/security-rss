Title: Microsoft brings GPT-4-powered Security Copilot to incident response
Date: 2023-03-28T13:11:37-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-03-28-microsoft-brings-gpt-4-powered-security-copilot-to-incident-response

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-brings-gpt-4-powered-security-copilot-to-incident-response/){:target="_blank" rel="noopener"}

> Microsoft today announced Security Copilot, a new ChatGPT-like assistant powered by artificial intelligence that takes advantage of Microsoft's threat intelligence footprint to make faster decisions during incident response and to help with threat hunting and security reporting. [...]
