Title: Microsoft Excel now blocking untrusted XLL add-ins by default
Date: 2023-03-07T14:54:58-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-03-07-microsoft-excel-now-blocking-untrusted-xll-add-ins-by-default

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-excel-now-blocking-untrusted-xll-add-ins-by-default/){:target="_blank" rel="noopener"}

> Microsoft says the Excel spreadsheet software is now blocking untrusted XLL add-ins by default in Microsoft 365 tenants worldwide. [...]
