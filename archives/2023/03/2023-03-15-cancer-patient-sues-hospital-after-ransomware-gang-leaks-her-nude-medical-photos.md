Title: Cancer patient sues hospital after ransomware gang leaks her nude medical photos
Date: 2023-03-15T20:05:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-15-cancer-patient-sues-hospital-after-ransomware-gang-leaks-her-nude-medical-photos

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/15/cancer_lvhn_sues_hospital/){:target="_blank" rel="noopener"}

> Victim offered two years of credit monitoring after highly sensitive records dumped online A cancer patient whose nude medical photos and records were posted online after they were stolen by a ransomware gang, has sued her healthcare provider for allowing the "preventable" and "seriously damaging" leak.... [...]
