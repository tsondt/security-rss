Title: CISA releases free ‘Decider’ tool to help with MITRE ATT&CK mapping
Date: 2023-03-02T09:10:20-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2023-03-02-cisa-releases-free-decider-tool-to-help-with-mitre-attck-mapping

[Source](https://www.bleepingcomputer.com/news/security/cisa-releases-free-decider-tool-to-help-with-mitre-attandck-mapping/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity & Infrastructure Security Agency (CISA) has released 'Decider,' an open-source tool that helps defenders and security analysts quickly generate MITRE ATT&CK mapping reports. [...]
