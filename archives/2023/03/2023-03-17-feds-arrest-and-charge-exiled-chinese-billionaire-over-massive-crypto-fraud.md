Title: Feds arrest and charge exiled Chinese billionaire over massive crypto fraud
Date: 2023-03-17T02:59:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-03-17-feds-arrest-and-charge-exiled-chinese-billionaire-over-massive-crypto-fraud

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/17/miles_guo_charged/){:target="_blank" rel="noopener"}

> This one has it all: Donald Trump’s inner circle, a Beijing bot backlash, conspiracy theories, and more Meet the newest member of the crypto rogues' gallery: Ho Wan Kwok, aka Guo Wengui, aka Miles Guo, whom the US Department of Justice on Wednesday arrested over what investigators have described as a "sprawling and complex scheme... to solicit investments in various entities and programs through false statements and representations to hundreds of thousands of Kwok's online followers."... [...]
