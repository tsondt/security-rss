Title: Why (and how) Google Cloud is adding attack path simulation to Security Command Center
Date: 2023-03-24T16:00:00+00:00
Author: Robert Lagerström
Category: GCP Security
Tags: Security & Identity
Slug: 2023-03-24-why-and-how-google-cloud-is-adding-attack-path-simulation-to-security-command-center

[Source](https://cloud.google.com/blog/products/identity-security/adding-attack-path-simulation-technology-to-security-command-center/){:target="_blank" rel="noopener"}

> As cloud environments scale and evolve based on changing business priorities, security teams may struggle to understand where their biggest risks are and where to focus their security controls. Some cloud security products have begun to incorporate attack path analysis to address this prioritization problem. Attack path analysis is a technique of discovering possible pathways that adversaries can take to access and compromise IT assets. A common approach in implementing attack path analysis is to produce a graph of all assets, and then query the map to discover possible exploit paths. While this may produce impressive-looking graphs, it requires the [...]
