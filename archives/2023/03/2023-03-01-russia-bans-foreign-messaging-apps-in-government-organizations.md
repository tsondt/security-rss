Title: Russia bans foreign messaging apps in government organizations
Date: 2023-03-01T11:27:43-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government
Slug: 2023-03-01-russia-bans-foreign-messaging-apps-in-government-organizations

[Source](https://www.bleepingcomputer.com/news/security/russia-bans-foreign-messaging-apps-in-government-organizations/){:target="_blank" rel="noopener"}

> Russia's internet watchdog agency Roskomnadzor is warning that today is the first day that laws banning the use of many foreign private messaging applications in the country come into force. [...]
