Title: UK refreshes national security plan to stop more of China's secret-stealing cyber-tricks
Date: 2023-03-14T07:40:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-14-uk-refreshes-national-security-plan-to-stop-more-of-chinas-secret-stealing-cyber-tricks

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/14/uk_integrated_review_refresh/){:target="_blank" rel="noopener"}

> A threat that needs two orgs to tackle it: the 'Integrated Security Fund' and the 'National Protective Security Agency' Britain's domestic intelligence service MI5 will oversee a new agency tasked with helping local organizations combat Chinese cyber-spies and other threats]... [...]
