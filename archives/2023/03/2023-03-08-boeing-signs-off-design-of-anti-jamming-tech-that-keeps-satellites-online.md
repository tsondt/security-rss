Title: Boeing signs off design of anti-jamming tech that keeps satellites online
Date: 2023-03-08T06:27:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-03-08-boeing-signs-off-design-of-anti-jamming-tech-that-keeps-satellites-online

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/08/boeing_antijamming_software_tech_validated/){:target="_blank" rel="noopener"}

> China and Russia won't be jammin' US sats no more Boeing said on Tuesday its anti-jam ground-based satellite communications system had passed the necessary tests to validate its design for use in the U.S. Space Force’s Pathfinder program.... [...]
