Title: Cisco patches critical Web UI RCE flaw in multiple IP phones
Date: 2023-03-01T13:28:26-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-01-cisco-patches-critical-web-ui-rce-flaw-in-multiple-ip-phones

[Source](https://www.bleepingcomputer.com/news/security/cisco-patches-critical-web-ui-rce-flaw-in-multiple-ip-phones/){:target="_blank" rel="noopener"}

> Cisco has addressed a critical security vulnerability found in the Web UI of multiple IP Phone models that unauthenticated and remote attackers can exploit in remote code execution (RCE) attacks. [...]
