Title: FBI warns of cryptocurrency theft via “play-to-earn” games
Date: 2023-03-09T14:24:42-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-03-09-fbi-warns-of-cryptocurrency-theft-via-play-to-earn-games

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-cryptocurrency-theft-via-play-to-earn-games/){:target="_blank" rel="noopener"}

> Cybercriminals are now using fake rewards in so-called "play-to-earn" mobile and online games to steal millions worth of cryptocurrency, according to an FBI warning issued on Thursday. [...]
