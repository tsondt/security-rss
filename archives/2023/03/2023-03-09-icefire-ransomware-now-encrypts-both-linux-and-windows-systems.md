Title: IceFire ransomware now encrypts both Linux and Windows systems
Date: 2023-03-09T09:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-09-icefire-ransomware-now-encrypts-both-linux-and-windows-systems

[Source](https://www.bleepingcomputer.com/news/security/icefire-ransomware-now-encrypts-both-linux-and-windows-systems/){:target="_blank" rel="noopener"}

> Threat actors linked to the IceFire ransomware operation are now actively targeting Linux systems worldwide with a new dedicated encryptor. [...]
