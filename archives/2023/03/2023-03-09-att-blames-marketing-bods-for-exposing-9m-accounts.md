Title: AT&amp;T blames marketing bods for exposing 9M accounts
Date: 2023-03-09T22:30:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-09-att-blames-marketing-bods-for-exposing-9m-accounts

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/09/att_wireless_breach/){:target="_blank" rel="noopener"}

> Says it was old and boring data, so that's OK, then... AT&T has confirmed that miscreants had access to nine million of its wireless customers' account details after a vendor's network was broken into in January.... [...]
