Title: Google euthanizes Chrome Cleanup Tool because it no longer has a purpose
Date: 2023-03-11T00:28:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-03-11-google-euthanizes-chrome-cleanup-tool-because-it-no-longer-has-a-purpose

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/11/googe_chrome_cleanup_windows/){:target="_blank" rel="noopener"}

> Times have changed and unwanted software on Windows is a rarity (unless you count Windows itself) Google is bidding adieu to an application that enabled Chrome users on Windows systems to get rid of unwanted software.... [...]
