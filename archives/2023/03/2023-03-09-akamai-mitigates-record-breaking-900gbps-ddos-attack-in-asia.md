Title: Akamai mitigates record-breaking 900Gbps DDoS attack in Asia
Date: 2023-03-09T14:54:28-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-09-akamai-mitigates-record-breaking-900gbps-ddos-attack-in-asia

[Source](https://www.bleepingcomputer.com/news/security/akamai-mitigates-record-breaking-900gbps-ddos-attack-in-asia/){:target="_blank" rel="noopener"}

> Akamai reports having mitigated the largest DDoS (distributed denial of service) attack ever launched against a customer based in the Asia-Pacific region. [...]
