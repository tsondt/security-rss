Title: Simplify web app authentication: A guide to AD FS federation with Amazon Cognito user pools
Date: 2023-03-31T21:33:02+00:00
Author: Leo Drakopoulos
Category: AWS Security
Tags: Advanced (300);Amazon Cognito;Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2023-03-31-simplify-web-app-authentication-a-guide-to-ad-fs-federation-with-amazon-cognito-user-pools

[Source](https://aws.amazon.com/blogs/security/simplify-web-app-authentication-a-guide-to-ad-fs-federation-with-amazon-cognito-user-pools/){:target="_blank" rel="noopener"}

> August 13, 2018: Date this post was first published, on the Front-End Web and Mobile Blog. We updated the CloudFormation template, provided additional clarification on implementation steps, and revised to account for the new Amazon Cognito UI. User authentication and authorization can be challenging when you’re building web and mobile apps. The challenges include handling [...]
