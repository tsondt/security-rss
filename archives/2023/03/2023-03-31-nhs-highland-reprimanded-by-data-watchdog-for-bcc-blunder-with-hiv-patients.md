Title: NHS Highland 'reprimanded' by data watchdog for BCC blunder with HIV patients
Date: 2023-03-31T09:35:07+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-03-31-nhs-highland-reprimanded-by-data-watchdog-for-bcc-blunder-with-hiv-patients

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/31/nhs_highland_reprimanded_by_data/){:target="_blank" rel="noopener"}

> 'Serious breach of trust' says ICO, 'stakes too high' for mistakes in cases like this In a classic email snafu NHS Highland sent messages to 37 patients infected with HIV and inadvertently used carbon copy (CC) instead of Blind Carbon Copy meaning the recipients could see each other’s email addresses.... [...]
