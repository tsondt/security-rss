Title: Microsoft: Defender update behind Windows LSA protection warnings
Date: 2023-03-21T18:02:14-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-03-21-microsoft-defender-update-behind-windows-lsa-protection-warnings

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-update-behind-windows-lsa-protection-warnings/){:target="_blank" rel="noopener"}

> Microsoft says the KB5007651 Microsoft Defender Antivirus update triggers Windows Security warnings on Windows 11 systems saying that Local Security Authority (LSA) Protection is off. [...]
