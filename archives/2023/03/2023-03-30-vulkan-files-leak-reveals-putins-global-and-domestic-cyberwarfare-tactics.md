Title: ‘Vulkan files’ leak reveals Putin’s global and domestic cyberwarfare tactics
Date: 2023-03-30T15:00:00+00:00
Author: Luke Harding, Stiliyana Simeonova, Manisha Ganguly and Dan Sabbagh
Category: The Guardian
Tags: Cyberwar;Hacking;Espionage;Internet;Technology;World news;Russia;Vladimir Putin;Data and computer security;Internet safety;Privacy;Mobile phones;Telecoms;Social media;Digital media;Media;Europe
Slug: 2023-03-30-vulkan-files-leak-reveals-putins-global-and-domestic-cyberwarfare-tactics

[Source](https://www.theguardian.com/technology/2023/mar/30/vulkan-files-leak-reveals-putins-global-and-domestic-cyberwarfare-tactics){:target="_blank" rel="noopener"}

> • Documents leaked by whistleblower angry over Ukraine war • Private Moscow consultancy bolstering Russian cyberwarfare • Tools support hacking operations and attacks on infrastructure • Documents linked to notorious Russian hacking group Sandworm • Russian program aims to control internet and spread disinformation The inconspicuous office is in Moscow’s north-eastern suburbs. A sign reads: “Business centre”. Nearby are modern residential blocks and a rambling old cemetery, home to ivy-covered war memorials. The area is where Peter the Great once trained his mighty army. Inside the six-storey building, a new generation is helping Russian military operations. Its weapons are more [...]
