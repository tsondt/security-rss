Title: BlackGuard stealer now targets 57 crypto wallets, extensions
Date: 2023-03-23T18:08:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-23-blackguard-stealer-now-targets-57-crypto-wallets-extensions

[Source](https://www.bleepingcomputer.com/news/security/blackguard-stealer-now-targets-57-crypto-wallets-extensions/){:target="_blank" rel="noopener"}

> A new variant of the BlackGuard stealer has been spotted in the wild, featuring new capabilities like USB propagation, persistence mechanisms, loading additional payloads in memory, and targeting additional crypto wallets. [...]
