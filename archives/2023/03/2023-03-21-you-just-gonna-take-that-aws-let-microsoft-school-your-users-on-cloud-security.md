Title: You just gonna take that AWS? Let Microsoft school your users on cloud security?
Date: 2023-03-21T20:43:33+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-03-21-you-just-gonna-take-that-aws-let-microsoft-school-your-users-on-cloud-security

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/21/microsoft_aws_cloud_benchmarks/){:target="_blank" rel="noopener"}

> And Google Cloud is next Microsoft has torn the wraps off its multi-cloud security benchmark (MCSB), which replaces the four-year-old Azure Security Benchmark. Crucially, as the name suggests, it now has usage and configuration guidance that reaches into rival environments.... [...]
