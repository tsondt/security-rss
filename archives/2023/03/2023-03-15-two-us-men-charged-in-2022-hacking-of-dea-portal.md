Title: Two U.S. Men Charged in 2022 Hacking of DEA Portal
Date: 2023-03-15T01:25:20+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Ne'er-Do-Well News;Web Fraud 2.0;Convict;Doxbin;emergency data request;KT;Nicholas Ceraolo;Ominus;Phobia;Ryan Stevenson;Sagar Steven Singh;U.S. Drug Enforcement Agency;ViLE;Weep
Slug: 2023-03-15-two-us-men-charged-in-2022-hacking-of-dea-portal

[Source](https://krebsonsecurity.com/2023/03/two-us-men-charged-in-2022-hacking-of-dea-portal/){:target="_blank" rel="noopener"}

> Two U.S. men have been charged with hacking into a U.S. Drug Enforcement Agency (DEA) online portal that taps into 16 different federal law enforcement databases. Both are alleged to be part of a larger criminal organization that specializes in using fake emergency data requests from compromised police and government email accounts to publicly threaten and extort their victims. Prosecutors for the Eastern District of New York today unsealed criminal complaints against Sagar Steven Singh — a.k.a “ Weep ” — a 19-year-old from Pawtucket, Rhode Island; and Nicholas Ceraolo, 25, of Queens, NY, who allegedly went by the handles [...]
