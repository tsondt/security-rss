Title: New TPM 2.0 flaws could let hackers steal cryptographic keys
Date: 2023-03-04T10:11:22-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-04-new-tpm-20-flaws-could-let-hackers-steal-cryptographic-keys

[Source](https://www.bleepingcomputer.com/news/security/new-tpm-20-flaws-could-let-hackers-steal-cryptographic-keys/){:target="_blank" rel="noopener"}

> The Trusted Platform Module (TPM) 2.0 specification is affected by two buffer overflow vulnerabilities that could allow attackers to access or overwrite sensitive data, such as cryptographic keys. [...]
