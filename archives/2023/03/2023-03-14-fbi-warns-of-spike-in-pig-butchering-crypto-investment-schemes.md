Title: FBI warns of spike in ‘pig butchering’ crypto investment schemes
Date: 2023-03-14T13:38:50-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-03-14-fbi-warns-of-spike-in-pig-butchering-crypto-investment-schemes

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-spike-in-pig-butchering-crypto-investment-schemes/){:target="_blank" rel="noopener"}

> Americans are increasingly targeted in 'pig butchering' cryptocurrency investment schemes, according to a public service announcement issued today by the Federal Bureau of Investigation (FBI). [...]
