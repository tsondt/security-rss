Title: Feds Charge NY Man as BreachForums Boss “Pompompurin”
Date: 2023-03-17T23:39:22+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;BreachForums;Conor Brian Fitzpatrick;DC Health Link;fbi;John Langmire;pompompurin
Slug: 2023-03-17-feds-charge-ny-man-as-breachforums-boss-pompompurin

[Source](https://krebsonsecurity.com/2023/03/feds-charge-ny-man-as-breachforums-boss-pompompurin/){:target="_blank" rel="noopener"}

> The U.S. Federal Bureau of Investigation (FBI) this week arrested a New York man on suspicion of running BreachForums, a popular English-language cybercrime forum where some of the world biggest hacked databases routinely show up for sale. The forum’s administrator “ Pompompurin ” has been a thorn in the side of the FBI for years, and BreachForums is widely considered a reincarnation of RaidForums, a remarkably similar crime forum that the FBI infiltrated and dismantled in 2022. FBI agents carting items out of Fitzpatrick’s home on March 15. Image: News 12 Westchester. In an affidavit filed with the District Court [...]
