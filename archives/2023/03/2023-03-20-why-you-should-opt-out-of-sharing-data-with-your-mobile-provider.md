Title: Why You Should Opt Out of Sharing Data With Your Mobile Provider
Date: 2023-03-20T14:47:56+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;AT&T;CCPA;CPNI;Custom Experience Plus;customer proprietary network information;Electronic Privacy Information Center;Enhanced Relevant Advertising Program;FCC;Federal Communications Commission;Gavin Wright;MAIDs;mashable;mobile advertising IDs;T-Mobile;TechTarget;Verizon;Verizon Custom Experience Plus;Verizon Selects;Verizon Wireless
Slug: 2023-03-20-why-you-should-opt-out-of-sharing-data-with-your-mobile-provider

[Source](https://krebsonsecurity.com/2023/03/why-you-should-opt-out-of-sharing-data-with-your-mobile-provider/){:target="_blank" rel="noopener"}

> A new breach involving data from nine million AT&T customers is a fresh reminder that your mobile provider likely collects and shares a great deal of information about where you go and what you do with your mobile device — unless and until you affirmatively opt out of this data collection. Here’s a primer on why you might want to do that, and how. Image: Shutterstock Telecommunications giant AT&T disclosed this month that a breach at a marketing vendor exposed certain account information for nine million customers. AT&T said the data exposed did not include sensitive information, such as credit [...]
