Title: President Biden kind of mostly bans commercial spyware from US govt
Date: 2023-03-28T02:45:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-03-28-president-biden-kind-of-mostly-bans-commercial-spyware-from-us-govt

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/28/biden_spyware_executive_order/){:target="_blank" rel="noopener"}

> Executive order has loopholes for Uncle Sam's snoop tools and American-made code US president Joe Biden on Monday issued an executive order on the "prohibition on use by the United States government of commercial spyware that poses risks to national security" – a title that is not quite as simple it seems.... [...]
