Title: Secret Service, ICE break the law over and over with fake cell tower spying
Date: 2023-03-04T01:00:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-03-04-secret-service-ice-break-the-law-over-and-over-with-fake-cell-tower-spying

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/04/dhs_secret_service_ice_stingray/){:target="_blank" rel="noopener"}

> Investigations 'at risk' from sloppy surveillance uncovered by audit probe The US Secret Service and Immigration and Customs Enforcement (ICE) agencies have failed to follow the law and official policy regarding the use of cell-site simulators, according to a government audit.... [...]
