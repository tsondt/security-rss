Title: Psst! Infosec bigwigs: Wanna be head of security at HM Treasury for £50k?
Date: 2023-03-31T11:40:06+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2023-03-31-psst-infosec-bigwigs-wanna-be-head-of-security-at-hm-treasury-for-50k

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/31/job_ad_hm_treasury/){:target="_blank" rel="noopener"}

> Juicy private sector job vs... money off a season travel ticket Given the importance of the Treasury department's function to Britain, Reg readers might expect the Head of Cyber Security vacancy currently being advertised would come with a salary that reflects its criticality.... [...]
