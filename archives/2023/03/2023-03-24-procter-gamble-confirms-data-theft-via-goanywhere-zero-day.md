Title: Procter & Gamble confirms data theft via GoAnywhere zero-day
Date: 2023-03-24T13:54:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-24-procter-gamble-confirms-data-theft-via-goanywhere-zero-day

[Source](https://www.bleepingcomputer.com/news/security/procter-and-gamble-confirms-data-theft-via-goanywhere-zero-day/){:target="_blank" rel="noopener"}

> Consumer goods giant Procter & Gamble has confirmed a data breach affecting an undisclosed number of employees after its GoAnywhere MFT secure file-sharing platform was compromised in early February. [...]
