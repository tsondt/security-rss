Title: Pro-Russian hackers target elected US officials supporting Ukraine
Date: 2023-03-30T12:19:17+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;espionage;hacking;russia;Ukraine
Slug: 2023-03-30-pro-russian-hackers-target-elected-us-officials-supporting-ukraine

[Source](https://arstechnica.com/?p=1927817){:target="_blank" rel="noopener"}

> Enlarge / Locked out. (credit: Sean Gladwell / Getty Images ) Threat actors aligned with Russia and Belarus are targeting elected US officials supporting Ukraine, using attacks that attempt to compromise their email accounts, researchers from security firm Proofpoint said. The campaign, which also targets officials of European nations, uses malicious JavaScript that’s customized for individual webmail portals belonging to various NATO-aligned organizations, a report Proofpoint published Thursday said. The threat actor—which Proofpoint has tracked since 2021 under the name TA473—employs sustained reconnaissance and painstaking research to ensure the scripts steal targets’ usernames, passwords, and other sensitive login credentials as [...]
