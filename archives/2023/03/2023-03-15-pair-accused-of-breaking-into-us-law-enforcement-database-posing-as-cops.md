Title: Pair accused of breaking into US law enforcement database, posing as cops
Date: 2023-03-15T14:10:29+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-03-15-pair-accused-of-breaking-into-us-law-enforcement-database-posing-as-cops

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/15/vile_crime_group_suspect_arrested/){:target="_blank" rel="noopener"}

> Teen arrested yesterday while another man suspected of being a ViLE crime group member still 'at large' A 19-year-old suspected of belonging to the "ViLE" crime group told a man authorities think is in the same gang that he "jacked into a police officer's account" and "the portal had some fucking potent tools" according to a complaint unsealed today in the Eastern District of New York.... [...]
