Title: Mozilla Firefox gets built-in Firefox Relay controls
Date: 2023-03-15T17:50:38-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2023-03-15-mozilla-firefox-gets-built-in-firefox-relay-controls

[Source](https://www.bleepingcomputer.com/news/security/mozilla-firefox-gets-built-in-firefox-relay-controls/){:target="_blank" rel="noopener"}

> Mozilla has announced the integration of Firefox Relay, an email protection system that helps users evade trackers and spammers, directly into the Firefox browser. [...]
