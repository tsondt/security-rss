Title: German Police Raid DDoS-Friendly Host ‘FlyHosting’
Date: 2023-03-31T18:35:15+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: DDoS-for-Hire;Ne'er-Do-Well News;ddos-for-hire;FlyHosting;German Federal Criminal Police Office
Slug: 2023-03-31-german-police-raid-ddos-friendly-host-flyhosting

[Source](https://krebsonsecurity.com/2023/03/german-police-raid-ddos-friendly-host-flyhosting/){:target="_blank" rel="noopener"}

> Authorities in Germany this week seized Internet servers that powered FlyHosting, a dark web offering that catered to cybercriminals operating DDoS-for-hire services, KrebsOnSecurity has learned. FlyHosting first advertised on cybercrime forums in November 2022, saying it was a Germany-based hosting firm that was open for business to anyone looking for a reliable place to host malware, botnet controllers, or DDoS-for-hire infrastructure. A seizure notice left on the FlyHosting domains. A statement released today by the German Federal Criminal Police Office says they served eight search warrants on March 30, and identified five individuals aged 16-24 suspected of operating “an internet [...]
