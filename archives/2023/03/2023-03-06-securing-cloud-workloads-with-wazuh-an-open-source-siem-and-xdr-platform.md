Title: Securing cloud workloads with Wazuh - an open source, SIEM and XDR platform
Date: 2023-03-06T10:05:10-05:00
Author: Sponsored by Wazuh
Category: BleepingComputer
Tags: Security
Slug: 2023-03-06-securing-cloud-workloads-with-wazuh-an-open-source-siem-and-xdr-platform

[Source](https://www.bleepingcomputer.com/news/security/securing-cloud-workloads-with-wazuh-an-open-source-siem-and-xdr-platform/){:target="_blank" rel="noopener"}

> Wazuh is a free, open source security platform that offers Unified XDR and SIEM capabilities. Learn how Wazuh detect and defend against security threats targeting cloud environments. [...]
