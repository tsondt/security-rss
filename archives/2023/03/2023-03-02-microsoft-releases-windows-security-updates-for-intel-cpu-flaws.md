Title: Microsoft releases Windows security updates for Intel CPU flaws
Date: 2023-03-02T20:02:22-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-03-02-microsoft-releases-windows-security-updates-for-intel-cpu-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-releases-windows-security-updates-for-intel-cpu-flaws/){:target="_blank" rel="noopener"}

> Microsoft has released out-of-band security updates for 'Memory Mapped I/O Stale Data (MMIO)' information disclosure vulnerabilities in Intel CPUs. [...]
