Title: BianLian ransomware crew goes 100% extortion after free decryptor lands
Date: 2023-03-19T13:37:11+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-03-19-bianlian-ransomware-crew-goes-100-extortion-after-free-decryptor-lands

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/19/bianlian_ransomware_extortion/){:target="_blank" rel="noopener"}

> No good deed goes unpunished, or something like that The BianLian gang is ditching the encrypting-files-and-demanding-ransom route and instead is going for full-on extortion.... [...]
