Title: Exploding USB Sticks
Date: 2023-03-24T11:04:43+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;bombs;terrorism;USB
Slug: 2023-03-24-exploding-usb-sticks

[Source](https://www.schneier.com/blog/archives/2023/03/exploding-usb-sticks.html){:target="_blank" rel="noopener"}

> In case you don’t have enough to worry about, people are hiding explosives —actual ones—in USB sticks: In the port city of Guayaquil, journalist Lenin Artieda of the Ecuavisa private TV station received an envelope containing a pen drive which exploded when he inserted it into a computer, his employer said. Artieda sustained slight injuries to one hand and his face, said police official Xavier Chango. No one else was hurt. Chango said the USB drive sent to Artieda could have been loaded with RDX, a military-type explosive. More : According to police official Xavier Chango, the flash drive that [...]
