Title: Establishing a data perimeter on AWS: Allow only trusted resources from my organization
Date: 2023-03-09T16:24:35+00:00
Author: Laura Reith
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Data protection;Identity;Network security;Security Blog;service control policies
Slug: 2023-03-09-establishing-a-data-perimeter-on-aws-allow-only-trusted-resources-from-my-organization

[Source](https://aws.amazon.com/blogs/security/establishing-a-data-perimeter-on-aws-allow-only-trusted-resources-from-my-organization/){:target="_blank" rel="noopener"}

> Companies that store and process data on Amazon Web Services (AWS) want to prevent transfers of that data to or from locations outside of their company’s control. This is to support security strategies, such as data loss prevention, or to comply with the terms and conditions set forth by various regulatory and privacy agreements. On [...]
