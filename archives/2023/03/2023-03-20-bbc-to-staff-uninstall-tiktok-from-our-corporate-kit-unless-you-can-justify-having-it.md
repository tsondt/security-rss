Title: BBC to staff: Uninstall TikTok from our corporate kit unless you can 'justify' having it
Date: 2023-03-20T12:34:56+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-03-20-bbc-to-staff-uninstall-tiktok-from-our-corporate-kit-unless-you-can-justify-having-it

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/20/british_broadcasting_corporation_softbans_tiktok/){:target="_blank" rel="noopener"}

> Those with 'sensitive' work-related information told to contact Beeb's security team The world's oldest national broadcaster, the venerable British Broadcasting Corporation, has told staff they shouldn't keep the TikTok app on a BBC corporate device unless there is a "justified business reason."... [...]
