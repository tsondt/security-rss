Title: US federal agency hacked using old Telerik bug to steal data
Date: 2023-03-15T12:39:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-15-us-federal-agency-hacked-using-old-telerik-bug-to-steal-data

[Source](https://www.bleepingcomputer.com/news/security/us-federal-agency-hacked-using-old-telerik-bug-to-steal-data/){:target="_blank" rel="noopener"}

> Last year, a U.S. federal agency's Microsoft Internet Information Services (IIS) web server was hacked by exploiting a critical.NET deserialization vulnerability in the Progress Telerik UI for ASP.NET AJAX component. [...]
