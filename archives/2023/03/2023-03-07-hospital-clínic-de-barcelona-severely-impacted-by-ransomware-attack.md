Title: Hospital Clínic de Barcelona severely impacted by ransomware attack
Date: 2023-03-07T13:49:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-07-hospital-clínic-de-barcelona-severely-impacted-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/hospital-cl-nic-de-barcelona-severely-impacted-by-ransomware-attack/){:target="_blank" rel="noopener"}

> The Hospital Clínic de Barcelona suffered a ransomware attack on Sunday morning, severely disrupting its healthcare services after the institution's virtual machines were targeted by the attacks. [...]
