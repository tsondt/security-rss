Title: The Week in Ransomware - March 17th 2023 - Shifting to data extortion
Date: 2023-03-17T19:01:27-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-03-17-the-week-in-ransomware-march-17th-2023-shifting-to-data-extortion

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-march-17th-2023-shifting-to-data-extortion/){:target="_blank" rel="noopener"}

> The fallout from the Clop ransomware attacks on GoAnywhere platforms has become apparent this week, with the threat actors starting to extort victims on their data leak site and companies confirming breaches. [...]
