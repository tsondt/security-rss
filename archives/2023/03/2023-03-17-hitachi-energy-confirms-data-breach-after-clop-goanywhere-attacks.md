Title: Hitachi Energy confirms data breach after Clop GoAnywhere attacks
Date: 2023-03-17T12:20:58-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-17-hitachi-energy-confirms-data-breach-after-clop-goanywhere-attacks

[Source](https://www.bleepingcomputer.com/news/security/hitachi-energy-confirms-data-breach-after-clop-goanywhere-attacks/){:target="_blank" rel="noopener"}

> Hitachi Energy confirmed it suffered a data breach after the Clop ransomware gang stole data using a zero-day GoAnyway zero-day vulnerability. [...]
