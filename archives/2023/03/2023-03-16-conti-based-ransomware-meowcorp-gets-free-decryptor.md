Title: Conti-based ransomware ‘MeowCorp’ gets free decryptor
Date: 2023-03-16T14:08:24-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-03-16-conti-based-ransomware-meowcorp-gets-free-decryptor

[Source](https://www.bleepingcomputer.com/news/security/conti-based-ransomware-meowcorp-gets-free-decryptor/){:target="_blank" rel="noopener"}

> A decryption tool for a modified version of the Conti ransomware could help hundreds of victims recover their files for free. [...]
