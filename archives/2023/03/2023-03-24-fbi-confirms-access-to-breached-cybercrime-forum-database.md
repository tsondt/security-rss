Title: FBI confirms access to Breached cybercrime forum database
Date: 2023-03-24T17:59:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-24-fbi-confirms-access-to-breached-cybercrime-forum-database

[Source](https://www.bleepingcomputer.com/news/security/fbi-confirms-access-to-breached-cybercrime-forum-database/){:target="_blank" rel="noopener"}

> Today, the FBI confirmed they have access to the database of the notorious BreachForums (aka Breached) hacking forum after the U.S. Justice Department also officially announced the arrest of its owner [...]
