Title: Simplify management of Network Firewall rule groups with VPC managed prefix lists
Date: 2023-03-21T18:09:27+00:00
Author: Mojgan Toth
Category: AWS Security
Tags: Advanced (300);Best Practices;Security, Identity, & Compliance;Technical How-to;firewall;Security;Security Blog
Slug: 2023-03-21-simplify-management-of-network-firewall-rule-groups-with-vpc-managed-prefix-lists

[Source](https://aws.amazon.com/blogs/security/simplify-management-of-network-firewall-rule-groups-with-vpc-managed-prefix-lists/){:target="_blank" rel="noopener"}

> In this blog post, we will show you how to use managed prefix lists to simplify management of your AWS Network Firewall rules and policies across your Amazon Virtual Private Cloud (Amazon VPC) in the same AWS Region. AWS Network Firewall is a stateful, managed, network firewall and intrusion detection and prevention service for your [...]
