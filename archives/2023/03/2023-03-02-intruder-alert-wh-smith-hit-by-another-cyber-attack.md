Title: Intruder alert: WH Smith hit by another cyber attack
Date: 2023-03-02T13:27:00+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-03-02-intruder-alert-wh-smith-hit-by-another-cyber-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/02/wh_smith_breach/){:target="_blank" rel="noopener"}

> Less than a year after Funky Pigeon leaked data of greetings cards biz Less than a year after its online greetings card subsidiary Funky Pigeon was attacked, WH Smith has admitted someone broke into its systems.... [...]
