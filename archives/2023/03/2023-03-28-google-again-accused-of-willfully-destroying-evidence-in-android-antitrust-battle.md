Title: Google again accused of willfully destroying evidence in Android antitrust battle
Date: 2023-03-28T20:09:25+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-03-28-google-again-accused-of-willfully-destroying-evidence-in-android-antitrust-battle

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/28/google_destroying_evidence_claim/){:target="_blank" rel="noopener"}

> Starting to see a pattern here? Judge seems to think so Updated Google Chat histories handed over by the web giant in ongoing Android antitrust litigation reveal the biz has been systematically destroying evidence, according to those suing the big G.... [...]
