Title: US government sets a 30-day deadline for wiping TikTok from feds' phones
Date: 2023-03-01T00:30:05+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2023-03-01-us-government-sets-a-30-day-deadline-for-wiping-tiktok-from-feds-phones

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/01/government_tiktok_ban/){:target="_blank" rel="noopener"}

> Last chance to film yourself doing a ByteDance, in the US and abroad The White House has ordered all federal government employees to delete TikTok from work devices, over fears the video-sharing app could be used to spy on Americans.... [...]
