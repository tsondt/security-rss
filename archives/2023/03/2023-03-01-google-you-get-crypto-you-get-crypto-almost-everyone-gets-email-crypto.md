Title: Google: You get crypto, you get crypto, almost everyone gets email crypto!
Date: 2023-03-01T01:38:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-01-google-you-get-crypto-you-get-crypto-almost-everyone-gets-email-crypto

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/01/google_client_side_encryption/){:target="_blank" rel="noopener"}

> Personal Gmail users still out of luck Google continued its client-side encryption rollout, the feature generally available to some Gmail and Calendar users who can now send and receive encrypted messages and meeting invites.... [...]
