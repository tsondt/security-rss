Title: Cisco kindly reveals proof of concept attacks for flaws in rival Netgear's kit
Date: 2023-03-22T22:57:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-22-cisco-kindly-reveals-proof-of-concept-attacks-for-flaws-in-rival-netgears-kit

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/22/netgear_router_poc_exploits/){:target="_blank" rel="noopener"}

> Maybe this is deserved given the problem's in a hidden telnet service Public proof-of-concept exploits have landed for bugs in Netgear Orbi routers – including one critical command execution vulnerability.... [...]
