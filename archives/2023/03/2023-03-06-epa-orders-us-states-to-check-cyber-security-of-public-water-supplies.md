Title: EPA orders US states to check cyber security of public water supplies
Date: 2023-03-06T22:45:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-03-06-epa-orders-us-states-to-check-cyber-security-of-public-water-supplies

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/06/epa_security_public_water/){:target="_blank" rel="noopener"}

> Don’t let miscreants poison the wells The US government is requiring states to assess the cyber security capabilities of their drinking water systems, part of the White House's broader efforts to protect the nation's critical infrastructure from attacks by nation-states and other cyber threats.... [...]
