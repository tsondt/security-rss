Title: LA housing authority discloses data breach after ransomware attack
Date: 2023-03-13T15:51:33-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-13-la-housing-authority-discloses-data-breach-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/la-housing-authority-discloses-data-breach-after-ransomware-attack/){:target="_blank" rel="noopener"}

> The Housing Authority of the City of Los Angeles (HACLA) is warning of a "data security event" after the LockBit ransomware gang targeted the organization and leaked data stolen in the attack. [...]
