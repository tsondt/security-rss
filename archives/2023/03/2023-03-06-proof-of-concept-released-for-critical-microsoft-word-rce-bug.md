Title: Proof-of-Concept released for critical Microsoft Word RCE bug
Date: 2023-03-06T15:55:26-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-03-06-proof-of-concept-released-for-critical-microsoft-word-rce-bug

[Source](https://www.bleepingcomputer.com/news/security/proof-of-concept-released-for-critical-microsoft-word-rce-bug/){:target="_blank" rel="noopener"}

> A proof-of-concept for CVE-2023-21716, a critical vulnerability in Microsoft Word that allows remote code execution, has been published over the weekend. [...]
