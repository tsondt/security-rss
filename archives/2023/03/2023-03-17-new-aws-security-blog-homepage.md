Title: New AWS Security Blog homepage
Date: 2023-03-17T16:43:26+00:00
Author: Anna Brinkmann
Category: AWS Security
Tags: Announcements;Foundational (100);Security;Security, Identity, & Compliance;Uncategorized;Security Blog
Slug: 2023-03-17-new-aws-security-blog-homepage

[Source](https://aws.amazon.com/blogs/security/new-aws-security-blog-homepage/){:target="_blank" rel="noopener"}

> We’ve launched a new AWS Security Blog homepage! While we currently have no plans to deprecate our existing list-view homepage, we have recently launched a new, security-centered homepage to provide readers with more blog info and easy access to the rest of AWS Security. Please bookmark the new page, and let us know what you [...]
