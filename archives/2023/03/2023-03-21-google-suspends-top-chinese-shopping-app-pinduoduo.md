Title: Google suspends top Chinese shopping app Pinduoduo
Date: 2023-03-21T05:58:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-03-21-google-suspends-top-chinese-shopping-app-pinduoduo

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/21/google_suspends_pinduoduo/){:target="_blank" rel="noopener"}

> Alleges it’s infected with malware – but not the version in its own digital tat bazaar Google has suspended Chinese shopping app Pinduoduo from its Play store because versions of the software found elsewhere have included malware.... [...]
