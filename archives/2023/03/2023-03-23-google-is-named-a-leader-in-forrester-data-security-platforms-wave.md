Title: Google is named a Leader in Forrester Data Security Platforms Wave
Date: 2023-03-23T19:00:00+00:00
Author: Sunil Potti
Category: GCP Security
Tags: Productivity & Collaboration;Security & Identity
Slug: 2023-03-23-google-is-named-a-leader-in-forrester-data-security-platforms-wave

[Source](https://cloud.google.com/blog/products/identity-security/google-named-leader-in-forrester-data-security-platforms-wave/){:target="_blank" rel="noopener"}

> To help organizations confidently move their sensitive data to the cloud, Google Cloud works diligently to earn and maintain customer trust. Our Trusted Cloud is committed to giving you a secure foundation that you can verify and independently control. Discovery and protection of sensitive data are integral parts of Google Cloud’s strategy to safeguard customer data. With this as our longstanding approach to protecting customers, we are happy to share that Forrester Research has ranked Google Cloud a Leader in The Forrester WaveTM Data Security Platforms Q1 2023. Relentless security innovation leveraging Google’s core strengths Google keeps more people safe [...]
