Title: Crown Resorts confirms ransom demand after GoAnywhere breach
Date: 2023-03-28T12:26:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-28-crown-resorts-confirms-ransom-demand-after-goanywhere-breach

[Source](https://www.bleepingcomputer.com/news/security/crown-resorts-confirms-ransom-demand-after-goanywhere-breach/){:target="_blank" rel="noopener"}

> Crown Resorts, Australia's largest gambling and entertainment company, has confirmed that it suffered a data breach after its GoAnywhere secure file-sharing server was breached using a zero-day vulnerability. [...]
