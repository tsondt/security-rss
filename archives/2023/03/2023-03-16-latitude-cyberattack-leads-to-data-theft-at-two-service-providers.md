Title: Latitude cyberattack leads to data theft at two service providers
Date: 2023-03-16T13:32:05-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-16-latitude-cyberattack-leads-to-data-theft-at-two-service-providers

[Source](https://www.bleepingcomputer.com/news/security/latitude-cyberattack-leads-to-data-theft-at-two-service-providers/){:target="_blank" rel="noopener"}

> Latitude Financial Services (Latitude) has published a notice on its website today informing that it has suffered a ransomware attack that resulted in the theft of some customer data. [...]
