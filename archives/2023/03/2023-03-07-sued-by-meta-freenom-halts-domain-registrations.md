Title: Sued by Meta, Freenom Halts Domain Registrations
Date: 2023-03-07T23:19:26+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Web Fraud 2.0;Facebook;Freenom;ICANN;Instagram;Meta;WhatsApp
Slug: 2023-03-07-sued-by-meta-freenom-halts-domain-registrations

[Source](https://krebsonsecurity.com/2023/03/sued-by-meta-freenom-halts-domain-registrations/){:target="_blank" rel="noopener"}

> The domain name registrar Freenom, whose free domain names have long been a draw for spammers and phishers, has stopped allowing new domain name registrations. The move comes just days after the Dutch registrar was sued by Meta, which alleges the company ignores abuse complaints about phishing websites while monetizing traffic to those abusive domains. Freenom’s website features a message saying it is not currently allowing new registrations. Freenom is the domain name registry service provider for five so-called “country code top level domains” (ccTLDs), including.cf for the Central African Republic;.ga for Gabon;.gq for Equatorial Guinea;.ml for Mali; and.tk for [...]
