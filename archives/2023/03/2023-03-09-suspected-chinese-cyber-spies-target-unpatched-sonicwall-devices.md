Title: Suspected Chinese cyber spies target unpatched SonicWall devices
Date: 2023-03-09T02:26:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-09-suspected-chinese-cyber-spies-target-unpatched-sonicwall-devices

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/09/suspected_chinese_cyberspies_target_uppatched/){:target="_blank" rel="noopener"}

> They've been lurking in networks since at least 2021 Suspected Chinese cyber criminals have zeroed in on unpatched SonicWall gateways and are infecting the devices with credential-stealing malware that persists through firmware upgrades, according to Mandiant.... [...]
