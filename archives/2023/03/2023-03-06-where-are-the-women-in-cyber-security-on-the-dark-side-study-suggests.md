Title: Where are the women in cyber security? On the dark side, study suggests
Date: 2023-03-06T03:01:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-03-06-where-are-the-women-in-cyber-security-on-the-dark-side-study-suggests

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/06/in_brief_security/){:target="_blank" rel="noopener"}

> Also, Royal ransomware metastasizes to other critical sectors, and this week's critical vulnerabilities In Brief If you can't join them, then you may as well try to beat them – at least if you're a talented security engineer looking for a job and you happen to be a woman.... [...]
