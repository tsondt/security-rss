Title: Medusa ransomware gang picks up steam as it targets companies worldwide
Date: 2023-03-12T11:12:06-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-03-12-medusa-ransomware-gang-picks-up-steam-as-it-targets-companies-worldwide

[Source](https://www.bleepingcomputer.com/news/security/medusa-ransomware-gang-picks-up-steam-as-it-targets-companies-worldwide/){:target="_blank" rel="noopener"}

> A ransomware operation known as Medusa has begun to pick up steam in 2023, targeting corporate victims worldwide with million-dollar ransom demands. [...]
