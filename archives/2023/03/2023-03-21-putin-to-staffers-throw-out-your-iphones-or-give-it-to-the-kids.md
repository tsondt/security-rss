Title: Putin to staffers: Throw out your iPhones, or 'give it to the kids'
Date: 2023-03-21T06:30:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-21-putin-to-staffers-throw-out-your-iphones-or-give-it-to-the-kids

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/21/kremlin_iphone_ban/){:target="_blank" rel="noopener"}

> April Fools should use Russian or Chinese tech instead, Kremlin advises Advisors and staff to Russia's maximum leader have been told to ditch their iPhones by the end of the month. Or, for those who don't want to throw their Apple devices in the bin, the other option is to "give it to the kids," according to a local Kommersant report.... [...]
