Title: Who’s Behind the NetWire Remote Access Trojan?
Date: 2023-03-09T18:52:25+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Ne'er-Do-Well News;Constella Intelligence;domaintools;Dugidox;fbi;Mario Zanko;NetWire RAT
Slug: 2023-03-09-whos-behind-the-netwire-remote-access-trojan

[Source](https://krebsonsecurity.com/2023/03/whos-behind-the-netwire-remote-access-trojan/){:target="_blank" rel="noopener"}

> A Croatian national has been arrested for allegedly operating NetWire, a Remote Access Trojan (RAT) marketed on cybercrime forums since 2012 as a stealthy way to spy on infected systems and siphon passwords. The arrest coincided with a seizure of the NetWire sales website by the U.S. Federal Bureau of Investigation (FBI). While the defendant in this case hasn’t yet been named publicly, the NetWire website has been leaking information about the likely true identity and location of its owner for the past 11 years. Typically installed by booby-trapped Microsoft Office documents and distributed via email, NetWire is a multi-platform [...]
