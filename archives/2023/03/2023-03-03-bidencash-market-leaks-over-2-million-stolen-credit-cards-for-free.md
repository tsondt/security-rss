Title: BidenCash market leaks over 2 million stolen credit cards for free
Date: 2023-03-03T15:16:26-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-03-bidencash-market-leaks-over-2-million-stolen-credit-cards-for-free

[Source](https://www.bleepingcomputer.com/news/security/bidencash-market-leaks-over-2-million-stolen-credit-cards-for-free/){:target="_blank" rel="noopener"}

> A carding marketplace known as BidenCash has leaked online a free database of 2,165,700 debit and credit cards in celebration of its first anniversary. [...]
