Title: British retail chain WH Smith says data stolen in cyberattack
Date: 2023-03-02T09:59:05-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-02-british-retail-chain-wh-smith-says-data-stolen-in-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/british-retail-chain-wh-smith-says-data-stolen-in-cyberattack/){:target="_blank" rel="noopener"}

> British retailer WH Smith has suffered a data breach that exposed information belonging to current and former employees. [...]
