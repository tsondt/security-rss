Title: Australian police arrest four BEC actors who stole $1.7 million
Date: 2023-03-24T15:49:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-24-australian-police-arrest-four-bec-actors-who-stole-17-million

[Source](https://www.bleepingcomputer.com/news/security/australian-police-arrest-four-bec-actors-who-stole-17-million/){:target="_blank" rel="noopener"}

> The Australian Federal Police (AFP) has arrested four members of a cybercriminal syndicate that has laundered $1.7 million stolen from at least 15 victims between January 2020 and March 2023. [...]
