Title: Trezor warns of massive crypto wallet phishing campaign
Date: 2023-03-01T18:14:47-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-03-01-trezor-warns-of-massive-crypto-wallet-phishing-campaign

[Source](https://www.bleepingcomputer.com/news/security/trezor-warns-of-massive-crypto-wallet-phishing-campaign/){:target="_blank" rel="noopener"}

> An ongoing phishing campaign is pretending to be Trezor data breach notifications attempting to steal a target's cryptocurrency wallet and its assets. [...]
