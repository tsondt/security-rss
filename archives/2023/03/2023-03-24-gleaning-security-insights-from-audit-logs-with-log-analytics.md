Title: Gleaning security insights from audit logs with Log Analytics
Date: 2023-03-24T16:00:00+00:00
Author: Charles Baer
Category: GCP Security
Tags: Management Tools;Developers & Practitioners;Security & Identity
Slug: 2023-03-24-gleaning-security-insights-from-audit-logs-with-log-analytics

[Source](https://cloud.google.com/blog/products/identity-security/gleaning-security-insights-from-audit-logs-with-log-analytics/){:target="_blank" rel="noopener"}

> Cloud Audit logs serve a vital purpose in Google Cloud by helping customers meet their compliance and security requirements. Log Analytics, a recent feature addition to Cloud Logging, brings new capabilities to search, aggregate and transform logs at query time using the power of SQL. Together with predefined queries in Community Security Analytics, Log Analytics makes it easier than ever to get actionable insights from your Cloud Audit logs. From logs to insights Getting insights from audit logs can be a challenge as it involves numerous steps requiring different highly skilled teams and tooling. That challenge is compounded by the [...]
