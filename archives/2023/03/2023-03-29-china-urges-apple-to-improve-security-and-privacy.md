Title: China urges Apple to improve security and privacy
Date: 2023-03-29T01:27:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-03-29-china-urges-apple-to-improve-security-and-privacy

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/29/china_asks_apple_improve_security/){:target="_blank" rel="noopener"}

> It's a juicy market that welcomes foreign investment, National development boss reminds Tim Cook Senior Chinese government officials have urged Apple CEO Tim Cook to improve the security and privacy features of his company's products.... [...]
