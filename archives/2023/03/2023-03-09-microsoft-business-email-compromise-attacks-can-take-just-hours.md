Title: Microsoft: Business email compromise attacks can take just hours
Date: 2023-03-09T14:13:49-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-03-09-microsoft-business-email-compromise-attacks-can-take-just-hours

[Source](https://www.bleepingcomputer.com/news/security/microsoft-business-email-compromise-attacks-can-take-just-hours/){:target="_blank" rel="noopener"}

> Microsoft's Security Intelligence team recently investigated a business email compromise (BEC) attack and found that attackers move rapidly, with some steps taking mere minutes. [...]
