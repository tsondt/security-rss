Title: First-known Dero cryptojacking operation seen targeting Kubernetes
Date: 2023-03-15T06:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-15-first-known-dero-cryptojacking-operation-seen-targeting-kubernetes

[Source](https://www.bleepingcomputer.com/news/security/first-known-dero-cryptojacking-operation-seen-targeting-kubernetes/){:target="_blank" rel="noopener"}

> The first known cryptojacking operation mining the Dero coin has been found targeting vulnerable Kubernetes container orchestrator infrastructure with exposed APIs. [...]
