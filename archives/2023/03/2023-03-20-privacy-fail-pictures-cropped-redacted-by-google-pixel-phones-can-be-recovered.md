Title: Privacy fail: Pictures cropped, redacted by Google Pixel phones can be recovered
Date: 2023-03-20T21:13:03+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-03-20-privacy-fail-pictures-cropped-redacted-by-google-pixel-phones-can-be-recovered

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/20/google_pixel_acropalypse/){:target="_blank" rel="noopener"}

> aCropalypse Now, starring any 2018-or-later device If you've owned a Google Pixel smartphone since the 3 series came out in 2018, bad news: any screenshot that you've cropped or redacted on your Pixel can be potentially restored without much fuss.... [...]
