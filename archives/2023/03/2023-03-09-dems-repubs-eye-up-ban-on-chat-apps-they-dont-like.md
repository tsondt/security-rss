Title: Dems, Repubs eye up ban on chat apps they don't like
Date: 2023-03-09T01:28:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-03-09-dems-repubs-eye-up-ban-on-chat-apps-they-dont-like

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/09/us_chat_apps_ban/){:target="_blank" rel="noopener"}

> Clock is ticking for TikTok and other foreign natter-ware On Tuesday a bipartisan group of a dozen US senators introduced a bill to authorize the Commerce Department to ban information and communications technology products and services deemed threats to national security.... [...]
