Title: Aruba Networks fixes six critical vulnerabilities in ArubaOS
Date: 2023-03-01T17:15:30-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-01-aruba-networks-fixes-six-critical-vulnerabilities-in-arubaos

[Source](https://www.bleepingcomputer.com/news/security/aruba-networks-fixes-six-critical-vulnerabilities-in-arubaos/){:target="_blank" rel="noopener"}

> Aruba Networks published a security advisory to inform customers about six critical-severity vulnerabilities impacting multiple versions of ArubaOS, its proprietary network operating system. [...]
