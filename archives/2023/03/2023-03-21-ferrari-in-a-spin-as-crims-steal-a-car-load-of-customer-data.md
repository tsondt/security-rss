Title: Ferrari in a spin as crims steal a car-load of customer data
Date: 2023-03-21T01:45:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-03-21-ferrari-in-a-spin-as-crims-steal-a-car-load-of-customer-data

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/21/ferrari_cyber_incident_data_theft/){:target="_blank" rel="noopener"}

> Speeds away from the very suggestion it would ever pay a ransom Italian automaker Ferrari has warned its well-heeled customers that their personal data may be at risk.... [...]
