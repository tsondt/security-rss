Title: Azure blunder left Bing results editable, MS 365 accounts potentially exposed
Date: 2023-03-30T23:30:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-30-azure-blunder-left-bing-results-editable-ms-365-accounts-potentially-exposed

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/30/wiz_bing_takeover/){:target="_blank" rel="noopener"}

> 'BingBang' boo-boo affected other internal Microsoft apps, too An Azure Active Directory (AAD) misconfiguration by Microsoft in one of its own cloud-hosted applications could have allowed miscreants to subvert the IT giant's Bing search engine – even changing search results.... [...]
