Title: Why is TikTok banned from government phones – and should the rest of us be worried?
Date: 2023-03-17T17:05:40+00:00
Author: Dan Milmo and Alex Hern
Category: The Guardian
Tags: TikTok;Technology;China;Privacy;Internet;UK news;Social media;Data and computer security
Slug: 2023-03-17-why-is-tiktok-banned-from-government-phones-and-should-the-rest-of-us-be-worried

[Source](https://www.theguardian.com/technology/2023/mar/17/why-is-tiktok-banned-from-government-phones-and-should-rest-of-us-be-worried){:target="_blank" rel="noopener"}

> UK has removed app over concerns data can be monitored by Chinese state, but public remain vulnerable TikTok is wildly popular, with more than 1 billion people consuming its short video posts around the world. But the app is less favoured by politicians in key markets such as the US and UK, where it has been banned from government-issued phones over security fears. We answer your questions about why TikTok has become a lightning rod for suspicion of Chinese state espionage – and whether nationwide bans are likely. Continue reading... [...]
