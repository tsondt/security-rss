Title: Warning: Your wireless networks may leak data thanks to Wi-Fi spec ambiguity
Date: 2023-03-30T06:29:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-03-30-warning-your-wireless-networks-may-leak-data-thanks-to-wi-fi-spec-ambiguity

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/30/wifi_spec_ambiguity_leak/){:target="_blank" rel="noopener"}

> How someone can nab buffered info, by hook or by kr00k Ambiguity in the Wi-Fi specification has left the wireless networking stacks in various operating systems vulnerable to several attacks that have the potential to expose network traffic.... [...]
