Title: Google: Turn off Wi-Fi calling, VoLTE to protect your Android from Samsung hijack bugs
Date: 2023-03-17T20:35:03+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-17-google-turn-off-wi-fi-calling-volte-to-protect-your-android-from-samsung-hijack-bugs

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/17/android_google_project_zero_samsung_modems/){:target="_blank" rel="noopener"}

> Four flaws open mobiles, cars to remote-control at baseband level with just a phone number Google security analysts have warned Android device users that several zero-day vulnerabilities in some Samsung chipsets could allow an attacker to completely hijack and remote-control their handsets knowing just the phone number.... [...]
