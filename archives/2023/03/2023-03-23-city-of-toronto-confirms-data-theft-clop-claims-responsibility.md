Title: City of Toronto confirms data theft, Clop claims responsibility
Date: 2023-03-23T17:05:46-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-03-23-city-of-toronto-confirms-data-theft-clop-claims-responsibility

[Source](https://www.bleepingcomputer.com/news/security/city-of-toronto-confirms-data-theft-clop-claims-responsibility/){:target="_blank" rel="noopener"}

> City of Toronto is among Clop ransomware gang's latest victims hit in the ongoing GoAnywhere hacking spree. Other victims listed alongside the Toronto city government include UK's Virgin Red and the statutory corporation, Pension Protection Fund. [...]
