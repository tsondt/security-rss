Title: General Bytes Bitcoin ATMs hacked using zero-day, $1.5M stolen
Date: 2023-03-20T17:36:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-03-20-general-bytes-bitcoin-atms-hacked-using-zero-day-15m-stolen

[Source](https://www.bleepingcomputer.com/news/security/general-bytes-bitcoin-atms-hacked-using-zero-day-15m-stolen/){:target="_blank" rel="noopener"}

> Leading Bitcoin ATM maker General Bytes disclosed that hackers stole cryptocurrency from the company and its customers using a zero-day vulnerability in its BATM management platform. [...]
