Title: FBI and international cops catch a NetWire RAT
Date: 2023-03-10T01:33:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-10-fbi-and-international-cops-catch-a-netwire-rat

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/10/fbi_netwire_seizure/){:target="_blank" rel="noopener"}

> Malware-seekers were diverted to the Feds, severing a Croatian connection International law enforcement agencies have claimed another victory over cyber criminals, after seizing the website, and taking down the infrastructure operated by crims linked to the NetWire remote access trojan (RAT).... [...]
