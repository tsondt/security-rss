Title: Chick-fil-A confirms accounts hacked in months-long "automated" attack
Date: 2023-03-02T16:00:10-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-03-02-chick-fil-a-confirms-accounts-hacked-in-months-long-automated-attack

[Source](https://www.bleepingcomputer.com/news/security/chick-fil-a-confirms-accounts-hacked-in-months-long-automated-attack/){:target="_blank" rel="noopener"}

> American fast food chain Chick-fil-A has confirmed that customers' accounts were breached in a months-long credential stuffing attack, allowing threat actors to use stored rewards balances and access personal information. [...]
