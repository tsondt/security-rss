Title: The most important email conversation you will ever have
Date: 2023-03-30T09:14:08+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-03-30-the-most-important-email-conversation-you-will-ever-have

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/30/the_most_important_email_conversation/){:target="_blank" rel="noopener"}

> Securing your business against BEC Webinar Business email compromise (BEC) is possibly the worst of cybercrimes because it abuses trust. It feeds on relationships carefully nurtured over decades and erodes a confidence which is foundational to cooperation, and progress.... [...]
