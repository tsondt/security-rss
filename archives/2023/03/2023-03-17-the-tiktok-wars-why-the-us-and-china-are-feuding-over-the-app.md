Title: The TikTok wars – why the US and China are feuding over the app
Date: 2023-03-17T00:19:47+00:00
Author: Staff and agencies
Category: The Guardian
Tags: TikTok;China;Technology;Privacy;US news;US politics;Biden administration;Internet;Foreign policy;Data and computer security
Slug: 2023-03-17-the-tiktok-wars-why-the-us-and-china-are-feuding-over-the-app

[Source](https://www.theguardian.com/technology/2023/mar/16/the-tiktok-wars-why-the-us-and-china-are-feuding-over-the-app){:target="_blank" rel="noopener"}

> The US says the extremely popular video-sharing app ‘screams’ of national security concerns and considers a countrywide ban TikTok is once again fending off claims that its Chinese parent company, ByteDance, would share user data from its popular video-sharing app with the Chinese government, or push propaganda and misinformation on its behalf. China’s foreign ministry on Wednesday accused the US itself of spreading disinformation about TikTok’s potential security risks following a report in the Wall Street Journal that the committee on foreign investment in the US – part of the treasury department – was threatening a US ban on the [...]
