Title: Attackers hit Bitcoin ATMs to steal $1.5 million in crypto cash
Date: 2023-03-23T09:02:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-03-23-attackers-hit-bitcoin-atms-to-steal-15-million-in-crypto-cash

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/23/general_bytes_crypto_atm/){:target="_blank" rel="noopener"}

> Terminal maker General Bytes shutters its cloud business after second breach in seven months Unidentified miscreants have siphoned cryptocurrency valued at more than $1.5 million from Bitcoin ATMs by exploiting an unknown flaw in digicash delivery systems.... [...]
