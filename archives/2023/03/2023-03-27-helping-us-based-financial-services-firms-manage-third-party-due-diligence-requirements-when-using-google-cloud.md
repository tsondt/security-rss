Title: Helping U.S.-based financial services firms manage third-party due diligence requirements when using Google Cloud
Date: 2023-03-27T16:00:00+00:00
Author: Marina Kaganovich
Category: GCP Security
Tags: Financial Services;Security & Identity
Slug: 2023-03-27-helping-us-based-financial-services-firms-manage-third-party-due-diligence-requirements-when-using-google-cloud

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-fsi-due-diligence-requirements/){:target="_blank" rel="noopener"}

> Financial services institutions increasingly rely on external service providers for a variety of technology-related services, including cloud computing. This trend materialized as firms recognized the value in focusing on their core competencies while using third party solutions to gain business, operational, security, resiliency, and other efficiencies. As the financial services sector is one of the most heavily regulated, firms need to carefully consider which third parties they engage and for what types of services, as they remain ultimately accountable for the performance of such services in the eyes of both their customers and regulators. In the United States, financial services [...]
