Title: What happens if you 'cover up' a ransomware infection? For Blackbaud, a $3m charge
Date: 2023-03-10T22:05:18+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-10-what-happens-if-you-cover-up-a-ransomware-infection-for-blackbaud-a-3m-charge

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/10/sec_blackbaud_3m_penalty/){:target="_blank" rel="noopener"}

> File under cost of doing business Blackbaud has agreed to pay $3 million to settle charges that it made misleading disclosures about a 2020 ransomware infection in which crooks stole more than a million files on around 13,000 of the cloud software slinger's customers.... [...]
