Title: FTX inner circle helped itself to $3.2B, liquidators say
Date: 2023-03-16T22:04:47+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-03-16-ftx-inner-circle-helped-itself-to-32b-liquidators-say

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/16/ftx_sbf_liquidator_claims/){:target="_blank" rel="noopener"}

> SBF alone pocketed $2.2B, or so this bankruptcy paperwork goes In fresh filings in the FTX bankruptcy case, the cryptocurrency-exchange-slash-hedge-fund's liquidators say they've uncovered $3.2 billion (£2.6b) in payments and loans made to disgraced FTX founder Sam Bankman-Fried and his inner circle.... [...]
