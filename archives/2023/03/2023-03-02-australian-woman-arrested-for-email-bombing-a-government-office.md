Title: Australian woman arrested for email bombing a government office
Date: 2023-03-02T13:03:28-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government
Slug: 2023-03-02-australian-woman-arrested-for-email-bombing-a-government-office

[Source](https://www.bleepingcomputer.com/news/security/australian-woman-arrested-for-email-bombing-a-government-office/){:target="_blank" rel="noopener"}

> The Australian Federal Police arrested a woman in Werrington, Sydney, for allegedly email bombing the office of a Federal Member of Parliament. [...]
