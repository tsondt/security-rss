Title: Highlights from the New U.S. Cybersecurity Strategy
Date: 2023-03-03T01:33:06+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;The Coming Storm;Ballistic Ventures;Brian Fox;CHIPS Act;Emotet;Executive Order (EO) 13984;internet of things;Keith Alexander;National Cyber Investigative Joint Task Force;National Cybersecurity Strategy 2023;Solar Winds breach;Sonatype;Ted Schlein
Slug: 2023-03-03-highlights-from-the-new-us-cybersecurity-strategy

[Source](https://krebsonsecurity.com/2023/03/highlights-from-the-new-u-s-cybersecurity-strategy/){:target="_blank" rel="noopener"}

> The Biden administration today issued its vision for beefing up the nation’s collective cybersecurity posture, including calls for legislation establishing liability for software products and services that are sold with little regard for security. The White House’s new national cybersecurity strategy also envisions a more active role by cloud providers and the U.S. military in disrupting cybercriminal infrastructure, and it names China as the single biggest cyber threat to U.S. interests. The strategy says the White House will work with Congress and the private sector to develop legislation that would prevent companies from disavowing responsibility for the security of their [...]
