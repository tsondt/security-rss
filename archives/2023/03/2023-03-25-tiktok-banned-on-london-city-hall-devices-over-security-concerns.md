Title: TikTok banned on London City Hall devices over security concerns
Date: 2023-03-25T10:31:44+00:00
Author: PA Media
Category: The Guardian
Tags: TikTok;Social media;London;Politics;China;UK news;Technology;Media;Digital media;Data and computer security
Slug: 2023-03-25-tiktok-banned-on-london-city-hall-devices-over-security-concerns

[Source](https://www.theguardian.com/technology/2023/mar/25/tiktok-banned-on-london-city-hall-devices-over-security-concerns){:target="_blank" rel="noopener"}

> Move by Greater London authority comes after Chinese-owned app was blocked on UK parliamentary devices London City Hall staff will no longer have TikTok on their devices in the latest ban imposed on the Chinese-owned social media app over security concerns. The Greater London authority (GLA) said the rule was implemented as it takes information security “extremely seriously”. Continue reading... [...]
