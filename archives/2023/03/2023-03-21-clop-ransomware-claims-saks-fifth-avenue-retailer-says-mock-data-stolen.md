Title: Clop ransomware claims Saks Fifth Avenue, retailer says mock data stolen
Date: 2023-03-21T05:25:02-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-03-21-clop-ransomware-claims-saks-fifth-avenue-retailer-says-mock-data-stolen

[Source](https://www.bleepingcomputer.com/news/security/clop-ransomware-claims-saks-fifth-avenue-retailer-says-mock-data-stolen/){:target="_blank" rel="noopener"}

> The Clop ransomware gang claims to have attacked Saks Fifth Avenue on its dark web leak site. Saks admits the incident is linked to the ongoing GoAnywhere MFT software exploits but states that no real customer data was stolen. [...]
