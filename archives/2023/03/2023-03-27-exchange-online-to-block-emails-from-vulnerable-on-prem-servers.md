Title: Exchange Online to block emails from vulnerable on-prem servers
Date: 2023-03-27T17:43:51-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-03-27-exchange-online-to-block-emails-from-vulnerable-on-prem-servers

[Source](https://www.bleepingcomputer.com/news/security/exchange-online-to-block-emails-from-vulnerable-on-prem-servers/){:target="_blank" rel="noopener"}

> Microsoft is introducing a new Exchange Online security feature that will automatically start throttling and eventually block all emails sent from "persistently vulnerable Exchange servers" 90 days after the admins are pinged to secure them. [...]
