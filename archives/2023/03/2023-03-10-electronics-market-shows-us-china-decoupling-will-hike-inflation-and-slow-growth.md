Title: Electronics market shows US-China decoupling will hike inflation and slow growth
Date: 2023-03-10T18:00:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-03-10-electronics-market-shows-us-china-decoupling-will-hike-inflation-and-slow-growth

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/10/electronics_us_china_inflation/){:target="_blank" rel="noopener"}

> Singapore's central bank has a gloomy vision of the future According to the Monetary Authority of Singapore (MAS), trade barriers between US and China have resulted in geoeconomic fragmentation and will likely result in slower global growth and higher inflation.... [...]
