Title: Leaked IT contractor files detail Kremlin's stockpile of cyber-weapons
Date: 2023-03-31T01:24:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-03-31-leaked-it-contractor-files-detail-kremlins-stockpile-of-cyber-weapons

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/31/vulkan_files_russia/){:target="_blank" rel="noopener"}

> Snowden-esque 'Vulkan' dossier links Moscow firm to FSB, GRU, SRV An unidentified whistleblower has provided several media organizations with access to leaked documents from NTC Vulkan – a Moscow IT consultancy – that allegedly show how the firm supports Russia's military and intelligence agencies with cyber warfare tools.... [...]
