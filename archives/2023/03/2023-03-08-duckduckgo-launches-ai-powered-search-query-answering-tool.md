Title: DuckDuckGo launches AI-powered search query answering tool
Date: 2023-03-08T10:22:17-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Technology
Slug: 2023-03-08-duckduckgo-launches-ai-powered-search-query-answering-tool

[Source](https://www.bleepingcomputer.com/news/security/duckduckgo-launches-ai-powered-search-query-answering-tool/){:target="_blank" rel="noopener"}

> Privacy-focused search engine DuckDuckGo has launched the first beta version of DuckAssist, an AI-assisted feature that writes accurate summaries to answer users' search queries. [...]
