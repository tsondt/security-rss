Title: Expanding Cloud Armor DDoS protection to Network Load Balancing and VMs with Public IP addresses
Date: 2023-03-22T16:00:00+00:00
Author: Lihi Shadmi
Category: GCP Security
Tags: Security & Identity;Networking
Slug: 2023-03-22-expanding-cloud-armor-ddos-protection-to-network-load-balancing-and-vms-with-public-ip-addresses

[Source](https://cloud.google.com/blog/products/networking/introducing-advanced-ddos-protection-with-cloud-armor/){:target="_blank" rel="noopener"}

> Over the past few years, Google has observed that distributed denial-of-service (DDoS) attacks are increasing in frequency and growing exponentially in size. Google Cloud customers have been using Cloud Armor and leveraging the scale and capacity of Google’s network edge to protect their environment from some of the largest DDoS attacks ever seen. We are excited to announce the general availability of Cloud Armor advanced network DDoS protection, which expands Cloud Armor’s DDoS protection capabilities to workloads using external network load balancers, protocol forwarding, and VMs with Public IP addresses. These workloads are used by a diverse set of customers, [...]
