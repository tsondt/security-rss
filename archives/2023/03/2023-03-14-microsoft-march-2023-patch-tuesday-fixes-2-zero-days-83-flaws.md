Title: Microsoft March 2023 Patch Tuesday fixes 2 zero-days, 83 flaws
Date: 2023-03-14T13:29:30-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-03-14-microsoft-march-2023-patch-tuesday-fixes-2-zero-days-83-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-march-2023-patch-tuesday-fixes-2-zero-days-83-flaws/){:target="_blank" rel="noopener"}

> ​Today is Microsoft's March 2023 Patch Tuesday, and security updates fix two actively exploited zero-day vulnerabilities and a total of 83 flaws. [...]
