Title: Veeam fixes bug that lets hackers breach backup infrastructure
Date: 2023-03-08T13:13:30-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-08-veeam-fixes-bug-that-lets-hackers-breach-backup-infrastructure

[Source](https://www.bleepingcomputer.com/news/security/veeam-fixes-bug-that-lets-hackers-breach-backup-infrastructure/){:target="_blank" rel="noopener"}

> Veeam urged customers to patch a high-severity Backup Service security vulnerability impacting its Backup & Replication software. [...]
