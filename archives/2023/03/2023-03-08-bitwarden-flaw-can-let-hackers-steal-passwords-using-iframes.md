Title: Bitwarden flaw can let hackers steal passwords using iframes
Date: 2023-03-08T17:08:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2023-03-08-bitwarden-flaw-can-let-hackers-steal-passwords-using-iframes

[Source](https://www.bleepingcomputer.com/news/security/bitwarden-flaw-can-let-hackers-steal-passwords-using-iframes/){:target="_blank" rel="noopener"}

> Bitwarden's credentials autofill feature contains a risky behavior that could allow malicious iframes embedded in trusted websites to steal people's credentials and send them to an attacker. [...]
