Title: Use backups to recover from security incidents
Date: 2023-03-22T18:46:02+00:00
Author: Jason Hurst
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Incident response;Security Blog;Threat Detection & Incident Response
Slug: 2023-03-22-use-backups-to-recover-from-security-incidents

[Source](https://aws.amazon.com/blogs/security/use-backups-to-recover-from-security-incidents/){:target="_blank" rel="noopener"}

> Greetings from the AWS Customer Incident Response Team (CIRT)! AWS CIRT is dedicated to supporting customers during active security events on the customer side of the AWS Shared Responsibility Model. Over the past three years, AWS CIRT has supported customers with security events in their AWS accounts. These include the unauthorized use of AWS Identity [...]
