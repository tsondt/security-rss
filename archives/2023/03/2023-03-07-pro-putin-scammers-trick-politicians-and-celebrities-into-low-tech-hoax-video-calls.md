Title: Pro-Putin scammers trick politicians and celebrities into low-tech hoax video calls
Date: 2023-03-07T10:01:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-07-pro-putin-scammers-trick-politicians-and-celebrities-into-low-tech-hoax-video-calls

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/07/proputin_scammers_trick_politicians_and/){:target="_blank" rel="noopener"}

> Who needs deepfakes when you've got makeup and 'element of surprise'? Pro-Russian scammers using social engineering and impersonation to trick prominent western commentators into conducting recorded video calls have kicked these campaigns "into high gear" over the past 12 months, according to security researchers.... [...]
