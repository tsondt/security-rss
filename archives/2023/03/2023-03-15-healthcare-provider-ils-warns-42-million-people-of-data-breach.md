Title: Healthcare provider ILS warns 4.2 million people of data breach
Date: 2023-03-15T11:50:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-03-15-healthcare-provider-ils-warns-42-million-people-of-data-breach

[Source](https://www.bleepingcomputer.com/news/security/healthcare-provider-ils-warns-42-million-people-of-data-breach/){:target="_blank" rel="noopener"}

> Independent Living Systems (ILS), a Miami-based healthcare administration and managed care solutions provider, suffered a data breach that exposed the personal information of 4,226,508 individuals. [...]
