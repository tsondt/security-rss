Title: How to set up least privilege access to your encrypted Amazon SQS queue
Date: 2023-03-03T21:03:42+00:00
Author: Ahmed Bakry
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;Amazon SQS;KMS Policy;Least-privilage IAM policies;Resource-based policies;Security Blog;SQS;SQS Access Policy
Slug: 2023-03-03-how-to-set-up-least-privilege-access-to-your-encrypted-amazon-sqs-queue

[Source](https://aws.amazon.com/blogs/security/how-to-set-up-least-privilege-access-to-your-encrypted-amazon-sqs-queue/){:target="_blank" rel="noopener"}

> Amazon Simple Queue Service (Amazon SQS) is a fully-managed message queueing service that enables you to decouple and scale microservices, distributed systems, and serverless applications. Amazon SQS provides authentication mechanisms so that you can control who has access to the queue. It also provides encryption in transit with HTTP over SSL or TLS, and it [...]
