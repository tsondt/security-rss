Title: UK.gov bans TikTok from its devices as a 'precaution' over spying fears
Date: 2023-03-16T14:34:24+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2023-03-16-ukgov-bans-tiktok-from-its-devices-as-a-precaution-over-spying-fears

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/16/ukgov_bans_tiktok_from_work_devices/){:target="_blank" rel="noopener"}

> Gov staff using it on personal mobes just fine... it's not like ministers use WhatsApp etc for business... oh wait The United Kingdom government has banned use of Chinese social media platform TikTok among ministers and officials on their work devices as a “precautionary” measure over worries the app is used to snoop on Brits.... [...]
