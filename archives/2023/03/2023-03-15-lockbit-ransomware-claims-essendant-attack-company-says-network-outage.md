Title: LockBit ransomware claims Essendant attack, company says  “network outage”
Date: 2023-03-15T14:50:26-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-03-15-lockbit-ransomware-claims-essendant-attack-company-says-network-outage

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-claims-essendant-attack-company-says-network-outage-/){:target="_blank" rel="noopener"}

> LockBit ransomware has claimed a cyber attack on Essendant, a wholesale distributer of office products after a "significant" and ongoing outage knocked the company's operations offline. [...]
