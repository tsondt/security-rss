Title: Refreshed from its holiday, Emotet has gone phishing
Date: 2023-03-09T18:27:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-03-09-refreshed-from-its-holiday-emotet-has-gone-phishing

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/09/emotet_returns_after_break/){:target="_blank" rel="noopener"}

> Notorious botnet starts spamming again after a three-month pause Emotet is back. After another months-long lull since a spate of attacks in November 2022, the notorious malware operation that has already survived a law enforcement takedown and various periods of inactivity began sending out malicious emails on Tuesday morning.... [...]
