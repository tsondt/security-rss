Title: Fortinet: New FortiOS bug used as zero-day to attack govt networks
Date: 2023-03-13T18:38:03-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-03-13-fortinet-new-fortios-bug-used-as-zero-day-to-attack-govt-networks

[Source](https://www.bleepingcomputer.com/news/security/fortinet-new-fortios-bug-used-as-zero-day-to-attack-govt-networks/){:target="_blank" rel="noopener"}

> Unknown attackers used zero-day exploits to abuse a new FortiOS bug patched this month in attacks targeting government and large organizations that have led to OS and file corruption and data loss. [...]
