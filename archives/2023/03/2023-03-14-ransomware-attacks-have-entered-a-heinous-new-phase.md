Title: Ransomware attacks have entered a heinous new phase
Date: 2023-03-14T17:00:53+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;cybercriminals;ransomware
Slug: 2023-03-14-ransomware-attacks-have-entered-a-heinous-new-phase

[Source](https://arstechnica.com/?p=1923896){:target="_blank" rel="noopener"}

> Enlarge (credit: Don Farrall/Getty Images) In February, attackers from the Russia-based BlackCat ransomware group hit a physician practice in Lackawanna County, Pennsylvania, that's part of the Lehigh Valley Health Network (LVHN). At the time, LVHN said that the attack “involved” a patient photo system related to radiation oncology treatment. The health care group said that BlackCat had issued a ransom demand, “but LVHN refused to pay this criminal enterprise.” After a couple of weeks, BlackCat threatened to publish data stolen from the system. “Our blog is followed by a lot of world media, the case will be widely publicized and [...]
