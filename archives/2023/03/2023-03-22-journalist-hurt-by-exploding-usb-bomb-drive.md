Title: Journalist hurt by exploding USB bomb drive
Date: 2023-03-22T22:09:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-03-22-journalist-hurt-by-exploding-usb-bomb-drive

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/22/usb_bomb_journalist/){:target="_blank" rel="noopener"}

> Now that's a flash bang Police in Ecuador are investigating attacks on media organizations across the country after a journalist was injured by an exploding USB flash drive.... [...]
