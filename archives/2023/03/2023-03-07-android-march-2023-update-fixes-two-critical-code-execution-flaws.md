Title: Android March 2023 update fixes two critical code execution flaws
Date: 2023-03-07T09:48:21-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2023-03-07-android-march-2023-update-fixes-two-critical-code-execution-flaws

[Source](https://www.bleepingcomputer.com/news/security/android-march-2023-update-fixes-two-critical-code-execution-flaws/){:target="_blank" rel="noopener"}

> Google has released March 2023 security updates for Android, fixing a total of 60 flaws, and among them, two critical-severity remote code execution (RCE) vulnerabilities impacting Android Systems running versions 11, 12, and 13. [...]
