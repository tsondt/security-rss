Title: Forget ChatGPT, the most overhyped security tool is technology itself, Wiz warns
Date: 2023-03-02T08:30:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-03-02-forget-chatgpt-the-most-overhyped-security-tool-is-technology-itself-wiz-warns

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/02/wiz_security_failings_chatgpt/){:target="_blank" rel="noopener"}

> Infosec also needs to widen its talent pool or miss out Interview It's a tough economy to ask for a bigger security team or larger budget to buy technology to protect against cyberattacks.... [...]
