Title: EU mandated messaging platform love-in is easier said than done: Cambridge boffins
Date: 2023-03-29T14:28:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-03-29-eu-mandated-messaging-platform-love-in-is-easier-said-than-done-cambridge-boffins

[Source](https://go.theregister.com/feed/www.theregister.com/2023/03/29/eu_mandated_messaging_interop_paper/){:target="_blank" rel="noopener"}

> Digital Market Act interoperability requirement a social challenge as well as a technical one By March 2024, instant messaging and real-time media apps operated by large tech platforms in Europe will be required to communicate with other services, per the EU's Digital Markets Act (DMA).... [...]
