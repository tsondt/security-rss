Title: Spying through Push Notifications
Date: 2023-12-07T12:02:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;metadata;national security policy;privacy;spyware;surveillance;transparency
Slug: 2023-12-07-spying-through-push-notifications

[Source](https://www.schneier.com/blog/archives/2023/12/spying-through-push-notifications.html){:target="_blank" rel="noopener"}

> When you get a push notification on your Apple or Google phone, those notifications go through Apple and Google servers. Which means that those companies can spy on them—either for their own reasons or in response to government demands. Sen. Wyden is trying to get to the bottom of this : In a statement, Apple said that Wyden’s letter gave them the opening they needed to share more details with the public about how governments monitored push notifications. “In this case, the federal government prohibited us from sharing any information,” the company said in a statement. “Now that this method [...]
