Title: US and EU infosec authorities pen intel-sharing pact
Date: 2023-12-07T18:28:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-12-07-us-and-eu-infosec-authorities-pen-intel-sharing-pact

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/07/cisa_enisa_intel_sharing/){:target="_blank" rel="noopener"}

> As Cyber Solidarity Act edges closer to full adoption in Europe The US Cybersecurity and Infrastructure Security Agency (CISA) has signed a working arrangement with its EU counterparts to increase cross-border information sharing and more to tackle criminals.... [...]
