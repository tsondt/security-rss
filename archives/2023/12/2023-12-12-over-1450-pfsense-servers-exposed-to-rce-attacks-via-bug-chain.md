Title: Over 1,450 pfSense servers exposed to RCE attacks via bug chain
Date: 2023-12-12T09:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-12-over-1450-pfsense-servers-exposed-to-rce-attacks-via-bug-chain

[Source](https://www.bleepingcomputer.com/news/security/over-1-450-pfsense-servers-exposed-to-rce-attacks-via-bug-chain/){:target="_blank" rel="noopener"}

> Roughly 1,450 pfSense instances exposed online are vulnerable to command injection and cross-site scripting flaws that, if chained, could enable attackers to perform remote code execution on the appliance. [...]
