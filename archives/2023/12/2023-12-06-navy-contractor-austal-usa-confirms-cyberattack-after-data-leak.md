Title: Navy contractor Austal USA confirms cyberattack after data leak
Date: 2023-12-06T12:16:31-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-06-navy-contractor-austal-usa-confirms-cyberattack-after-data-leak

[Source](https://www.bleepingcomputer.com/news/security/navy-contractor-austal-usa-confirms-cyberattack-after-data-leak/){:target="_blank" rel="noopener"}

> Austal USA, a shipbuilding company and a contractor for the U.S. Department of Defense (DoD) and the Department of Homeland Security (DHS) confirmed that it suffered a cyberattack and is currently investigating the impact of the incident. [...]
