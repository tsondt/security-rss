Title: IAM Access Analyzer simplifies inspection of unused access in your organization
Date: 2023-12-04T20:24:46+00:00
Author: Achraf Moussadek-Kabdani
Category: AWS Security
Tags: Announcements;Intermediate (200);Security, Identity, & Compliance;Technical How-to;AWS IAM;AWS Identity and Access Management;AWS Identity and Access Management (IAM);IAM;IAM Access Analyzer;IAM policies;Identity and Access Management;Security Blog
Slug: 2023-12-04-iam-access-analyzer-simplifies-inspection-of-unused-access-in-your-organization

[Source](https://aws.amazon.com/blogs/security/iam-access-analyzer-simplifies-inspection-of-unused-access-in-your-organization/){:target="_blank" rel="noopener"}

> AWS Identity and Access Management (IAM) Access Analyzer offers tools that help you set, verify, and refine permissions. You can use IAM Access Analyzer external access findings to continuously monitor your AWS Organizations organization and Amazon Web Services (AWS) accounts for public and cross-account access to your resources, and verify that only intended external access [...]
