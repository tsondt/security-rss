Title: Introducing automated credential discovery to help secure your cloud environment
Date: 2023-12-19T17:00:00+00:00
Author: Tim Wingerter
Category: GCP Security
Tags: Security & Identity
Slug: 2023-12-19-introducing-automated-credential-discovery-to-help-secure-your-cloud-environment

[Source](https://cloud.google.com/blog/products/identity-security/introducing-automated-credential-discovery-to-help-secure-your-cloud-environment/){:target="_blank" rel="noopener"}

> Storing credentials in plaintext can make your organization less secure. Risks include exposing your credentials to unauthorized users, including threat actors. Improperly secured credentials can also be collected, propagated, and further exposed in various systems, such as logs or inventory systems. We recommend organizations protect their stored credentials with tools such as Secret Manager, which adds a layer of encryption and authorization to the use of secrets like passwords and API keys. However, like finding a needle buried under a ton of dirt, determining exactly which credentials have been stored and exposed in plaintext can be challenging. To help organizations [...]
