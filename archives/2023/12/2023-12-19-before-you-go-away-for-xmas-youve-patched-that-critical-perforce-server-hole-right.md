Title: Before you go away for Xmas: You've patched that critical Perforce Server hole, right?
Date: 2023-12-19T19:57:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-19-before-you-go-away-for-xmas-youve-patched-that-critical-perforce-server-hole-right

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/19/microsoft_warns_patch_critical_perforce/){:target="_blank" rel="noopener"}

> Microsoft bug hunters highlight weaknesses in source-wrangling suite Four vulnerabilities in Perforce Helix Core Server, including one critical remote code execution bug, should be patched "immediately," according to Microsoft, which spotted the flaws and disclosed them to the software vendor.... [...]
