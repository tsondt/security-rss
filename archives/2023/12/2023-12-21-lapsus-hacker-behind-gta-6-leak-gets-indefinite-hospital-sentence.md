Title: Lapsus$ hacker behind GTA 6 leak gets indefinite hospital sentence
Date: 2023-12-21T15:42:55-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-12-21-lapsus-hacker-behind-gta-6-leak-gets-indefinite-hospital-sentence

[Source](https://www.bleepingcomputer.com/news/security/lapsus-hacker-behind-gta-6-leak-gets-indefinite-hospital-sentence/){:target="_blank" rel="noopener"}

> Lapsus$ cybercrime and extortion group member, Arion Kurtaj has been sentenced indefinitely in a 'secure hospital' by a UK judge. Kurtaj who is 18 years of age and autistic is among the primary Lapsus$ threat actors, and was involved in the leak of assets associated with the video game, Grand Theft Auto VI. [...]
