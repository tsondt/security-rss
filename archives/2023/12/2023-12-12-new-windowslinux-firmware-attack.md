Title: New Windows/Linux Firmware Attack
Date: 2023-12-12T12:01:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;BIOS;exploits;firmware;Linux;vulnerabilities;Windows
Slug: 2023-12-12-new-windowslinux-firmware-attack

[Source](https://www.schneier.com/blog/archives/2023/12/new-windows-linux-firmware-attack.html){:target="_blank" rel="noopener"}

> Interesting attack based on malicious pre-OS logo images : LogoFAIL is a constellation of two dozen newly discovered vulnerabilities that have lurked for years, if not decades, in Unified Extensible Firmware Interfaces responsible for booting modern devices that run Windows or Linux.... The vulnerabilities are the subject of a coordinated mass disclosure released Wednesday. The participating companies comprise nearly the entirety of the x64 and ARM CPU ecosystem, starting with UEFI suppliers AMI, Insyde, and Phoenix (sometimes still called IBVs or independent BIOS vendors); device manufacturers such as Lenovo, Dell, and HP; and the makers of the CPUs that go [...]
