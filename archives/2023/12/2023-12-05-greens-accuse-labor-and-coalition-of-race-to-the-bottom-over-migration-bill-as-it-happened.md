Title: Greens accuse Labor and Coalition of ‘race to the bottom’ over migration bill – as it happened
Date: 2023-12-05T07:04:49+00:00
Author: Jordyn Beazley and Emily Wind (earlier)
Category: The Guardian
Tags: Australia news;Australian politics;Anthony Albanese;Banking;Australian economy;Interest rates;Westpac;Technology;Data and computer security;Environment;Business;Commonwealth Games
Slug: 2023-12-05-greens-accuse-labor-and-coalition-of-race-to-the-bottom-over-migration-bill-as-it-happened

[Source](https://www.theguardian.com/australia-news/live/2023/dec/05/australia-news-live-reserve-bank-interest-rates-michele-bullock-cost-of-living-inflation-immigration-detention-high-court-politics){:target="_blank" rel="noopener"}

> This blog is now closed. Third freed immigration detainee arrested after he allegedly breached bail conditions RBA interest rates: Australian mortgage holders spared pre-Christmas rise from Reserve Bank Get our morning and afternoon news emails, free app or daily news podcast Marles says Australians should express views on Israel-Hamas conflict ‘in a safe and a peaceful way’ The deputy prime minister, Richard Marles, is speaking to ABC RN. He is asked whether Australia is “a safe place for Jews right now”, amid a discussion of the Israel-Hamas war. [I understand] many of the Jewish community are finding this to be [...]
