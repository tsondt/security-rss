Title: CISA details twin attacks on federal servers via unpatched ColdFusion flaw
Date: 2023-12-05T17:40:21+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-12-05-cisa-details-twin-attacks-on-federal-servers-via-unpatched-coldfusion-flaw

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/05/cisa_coldfusion_government/){:target="_blank" rel="noopener"}

> Tardy IT admins likely to get a chilly reception over the lack of updates CISA has released details about a federal agency that recently had at least two public-facing servers compromised by attackers exploiting a critical Adobe ColdFusion vulnerability.... [...]
