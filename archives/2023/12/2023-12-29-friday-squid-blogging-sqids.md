Title: Friday Squid Blogging: Sqids
Date: 2023-12-29T22:08:31+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;open source;squid
Slug: 2023-12-29-friday-squid-blogging-sqids

[Source](https://www.schneier.com/blog/archives/2023/12/friday-squid-blogging-sqids.html){:target="_blank" rel="noopener"}

> They’re short unique strings : Sqids (pronounced “squids”) is an open-source library that lets you generate YouTube-looking IDs from numbers. These IDs are short, can be generated from a custom alphabet and are guaranteed to be collision-free. I haven’t dug into the details enough to know how they can be guaranteed to be collision-free. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
