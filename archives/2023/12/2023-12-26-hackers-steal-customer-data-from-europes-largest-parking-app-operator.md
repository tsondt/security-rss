Title: Hackers steal customer data from Europe’s largest parking app operator
Date: 2023-12-26T13:34:11+00:00
Author: Jasper Jolly
Category: The Guardian
Tags: Hacking;Cybercrime;Apps;Data and computer security;Information commissioner;Privacy;Europe;UK news;World news;Business;Technology
Slug: 2023-12-26-hackers-steal-customer-data-from-europes-largest-parking-app-operator

[Source](https://www.theguardian.com/technology/2023/dec/26/hackers-steal-customer-data-europe-parking-app-easypark-ringgo-parkmobile){:target="_blank" rel="noopener"}

> Owner of RingGo and ParkMobile says data including parts of credit card numbers taken in cyber-attack Europe’s largest parking app operator has reported itself to information regulators in the EU and UK after hackers stole customer data. EasyPark Group, the owner of brands including RingGo and ParkMobile, said customer names, phone numbers, addresses, email addresses and parts of credit card numbers had been taken but said parking data had not been compromised in the cyber-attack. Continue reading... [...]
