Title: Police Get Medical Records without a Warrant
Date: 2023-12-18T15:37:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;healthcare;medicine;police;privacy;surveillance
Slug: 2023-12-18-police-get-medical-records-without-a-warrant

[Source](https://www.schneier.com/blog/archives/2023/12/police-get-medical-records-without-a-warrant.html){:target="_blank" rel="noopener"}

> More unconstrained surveillance : Lawmakers noted the pharmacies’ policies for releasing medical records in a letter dated Tuesday to the Department of Health and Human Services (HHS) Secretary Xavier Becerra. The letter—signed by Sen. Ron Wyden (D-Ore.), Rep. Pramila Jayapal (D-Wash.), and Rep. Sara Jacobs (D-Calif.)—said their investigation pulled information from briefings with eight big prescription drug suppliers. They include the seven largest pharmacy chains in the country: CVS Health, Walgreens Boots Alliance, Cigna, Optum Rx, Walmart Stores, Inc., The Kroger Company, and Rite Aid Corporation. The lawmakers also spoke with Amazon Pharmacy. All eight of the pharmacies said they [...]
