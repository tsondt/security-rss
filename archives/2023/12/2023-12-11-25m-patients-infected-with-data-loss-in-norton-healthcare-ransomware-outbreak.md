Title: 2.5M patients infected with data loss in Norton Healthcare ransomware outbreak
Date: 2023-12-11T20:01:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-11-25m-patients-infected-with-data-loss-in-norton-healthcare-ransomware-outbreak

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/11/norton_healthcare_ransomware/){:target="_blank" rel="noopener"}

> AlphV lays claims to the intrusion Norton Healthcare, which runs eight hospitals and more than 30 clinics in Kentucky and Indiana, has admitted crooks may have stolen 2.5 million people's most sensitive data during a ransomware attack in May.... [...]
