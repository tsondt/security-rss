Title: WordPress fixes POP chain exposing websites to RCE attacks
Date: 2023-12-07T15:10:30-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-07-wordpress-fixes-pop-chain-exposing-websites-to-rce-attacks

[Source](https://www.bleepingcomputer.com/news/security/wordpress-fixes-pop-chain-exposing-websites-to-rce-attacks/){:target="_blank" rel="noopener"}

> WordPress has released version 6.4.2 that addresses a remote code execution (RCE) vulnerability that could be chained with another flaw to allow attackers run arbitrary PHP code on the target website. [...]
