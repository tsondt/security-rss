Title: Hollywood plays unwitting Cameo in Kremlin plot to discredit Zelensky
Date: 2023-12-09T11:28:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-12-09-hollywood-plays-unwitting-cameo-in-kremlin-plot-to-discredit-zelensky

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/09/russia_cameo_ukraine_propaganda/){:target="_blank" rel="noopener"}

> Microsoft spots surge in pro-Russia exploits of video platform to spread propaganda An unknown pro-Russia influence group spent time recruiting unwitting Hollywood actors to assist in smear campaigns against Ukraine and its president Volodymyr Zelensky.... [...]
