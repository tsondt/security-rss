Title: UniFi devices broadcasted private video to other users’ accounts
Date: 2023-12-14T21:34:03+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;privacy;ubiquiti;unifi;video
Slug: 2023-12-14-unifi-devices-broadcasted-private-video-to-other-users-accounts

[Source](https://arstechnica.com/?p=1991239){:target="_blank" rel="noopener"}

> Enlarge / An assortment of Ubiquiti cameras. (credit: Ubiquiti ) Users of UniFi, the popular line of wireless devices from manufacturer Ubiquiti, are reporting receiving private camera feeds from, and control over, devices belonging to other users, posts published to social media site Reddit over the past 24 hours show. “Recently, my wife received a notification from UniFi Protect, which included an image from a security camera,” one Reddit user reported. “However, here's the twist—this camera doesn't belong to us.” Stoking concern and anxiety The post included two images. The first showed a notification pushed to the person’s phone reporting [...]
