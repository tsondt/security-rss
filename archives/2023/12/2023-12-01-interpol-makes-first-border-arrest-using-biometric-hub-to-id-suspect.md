Title: Interpol makes first border arrest using Biometric Hub to ID suspect
Date: 2023-12-01T07:25:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-01-interpol-makes-first-border-arrest-using-biometric-hub-to-id-suspect

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/01/interpol_biohub_arrest/){:target="_blank" rel="noopener"}

> Global database of faces and fingerprints proves its worth European police have for the first time made an arrest after remotely checking Interpol's trove of biometric data to identify a suspected smuggler.... [...]
