Title: Microsoft seizes domains used to sell fraudulent Outlook accounts
Date: 2023-12-13T18:45:30-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-12-13-microsoft-seizes-domains-used-to-sell-fraudulent-outlook-accounts

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-seizes-domains-used-to-sell-fraudulent-outlook-accounts/){:target="_blank" rel="noopener"}

> Microsoft's Digital Crimes Unit seized multiple domains used by a Vietnam-based cybercrime group (Storm-1152) that registered over 750 million fraudulent accounts and raked in millions of dollars by selling them online to other cybercriminals. [...]
