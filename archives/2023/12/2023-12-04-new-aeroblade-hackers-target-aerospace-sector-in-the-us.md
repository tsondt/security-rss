Title: New AeroBlade hackers target aerospace sector in the U.S.
Date: 2023-12-04T09:56:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-04-new-aeroblade-hackers-target-aerospace-sector-in-the-us

[Source](https://www.bleepingcomputer.com/news/security/new-aeroblade-hackers-target-aerospace-sector-in-the-us/){:target="_blank" rel="noopener"}

> A previously unknown cyber espionage hacking group named 'AeroBlade' was discovered targeting organizations in the United States aerospace sector. [...]
