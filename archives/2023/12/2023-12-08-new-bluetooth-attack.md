Title: New Bluetooth Attack
Date: 2023-12-08T12:05:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;authentication;Bluetooth;cyberattack;man-in-the-middle attacks;secrecy;vulnerabilities
Slug: 2023-12-08-new-bluetooth-attack

[Source](https://www.schneier.com/blog/archives/2023/12/new-bluetooth-attack.html){:target="_blank" rel="noopener"}

> New attack breaks forward secrecy in Bluetooth. Three news articles : BLUFFS is a series of exploits targeting Bluetooth, aiming to break Bluetooth sessions’ forward and future secrecy, compromising the confidentiality of past and future communications between devices. This is achieved by exploiting four flaws in the session key derivation process, two of which are new, to force the derivation of a short, thus weak and predictable session key (SKC). Next, the attacker brute-forces the key, enabling them to decrypt past communication and decrypt or manipulate future communications. The vulnerability has been around for at least a decade. [...]
