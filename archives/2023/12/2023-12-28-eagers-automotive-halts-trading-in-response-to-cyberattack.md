Title: Eagers Automotive halts trading in response to cyberattack
Date: 2023-12-28T15:31:03-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-28-eagers-automotive-halts-trading-in-response-to-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/eagers-automotive-halts-trading-in-response-to-cyberattack/){:target="_blank" rel="noopener"}

> Eagers Automotive has announced it suffered a cyberattack and was forced to halt trading on the stock exchange as it evaluates the impact of the incident. [...]
