Title: BlackCat ransomware crims threaten to directly extort victim's customers
Date: 2023-12-05T12:30:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-12-05-blackcat-ransomware-crims-threaten-to-directly-extort-victims-customers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/05/alphvblackcat_shakes_up_tactics_again/){:target="_blank" rel="noopener"}

> Accounting software firm Tipalti says it’s investigating alleged break-in of its systems The AlphV/BlackCat ransomware group said it plans to "go direct" to the clients of a firm it allegedly attacked to extort them, claiming to have infiltrated the systems of accounting software vendor Tipalti.... [...]
