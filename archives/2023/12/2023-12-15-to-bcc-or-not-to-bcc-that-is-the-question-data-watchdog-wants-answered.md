Title: To BCC or not to BCC – that is the question data watchdog wants answered
Date: 2023-12-15T09:59:06+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-12-15-to-bcc-or-not-to-bcc-that-is-the-question-data-watchdog-wants-answered

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/15/to_bcc_or_not_bcc/){:target="_blank" rel="noopener"}

> The dos and don'ts of bulk emailing A data regulator has reminded companies they need to take care while writing emails to avoid unintentionally blurting out personal data.... [...]
