Title: Ransomware gang behind threats to Fred Hutch cancer patients
Date: 2023-12-15T11:50:11-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-12-15-ransomware-gang-behind-threats-to-fred-hutch-cancer-patients

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-behind-threats-to-fred-hutch-cancer-patients/){:target="_blank" rel="noopener"}

> The Hunters International ransomware gang claimed to be behind a cyberattack on the Fred Hutchinson Cancer Center (Fred Hutch) that resulted in patients receiving personalized extortion threats. [...]
