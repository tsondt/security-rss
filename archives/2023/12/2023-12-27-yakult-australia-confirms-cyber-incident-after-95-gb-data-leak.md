Title: Yakult Australia confirms 'cyber incident' after 95 GB data leak
Date: 2023-12-27T04:10:51-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-12-27-yakult-australia-confirms-cyber-incident-after-95-gb-data-leak

[Source](https://www.bleepingcomputer.com/news/security/yakult-australia-confirms-cyber-incident-after-95-gb-data-leak/){:target="_blank" rel="noopener"}

> Yakult Australia, manufacturer of a probiotic milk drink, has confirmed experiencing a "cyber incident" in a statement to BleepingComputer. Both the company's Australian and New Zealand IT systems have been affected. Cybercrime actor DragonForce which claimed responsibility for the attack has also leaked 95 GB of data. [...]
