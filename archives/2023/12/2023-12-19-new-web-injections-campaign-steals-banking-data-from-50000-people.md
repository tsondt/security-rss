Title: New Web injections campaign steals banking data from 50,000 people
Date: 2023-12-19T15:36:07-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-19-new-web-injections-campaign-steals-banking-data-from-50000-people

[Source](https://www.bleepingcomputer.com/news/security/new-web-injections-campaign-steals-banking-data-from-50-000-people/){:target="_blank" rel="noopener"}

> A new malware campaign that emerged in March 2023 used JavaScript web injections to try to steal the banking data of over 50,000 users of 40 banks in North America, South America, Europe, and Japan. [...]
