Title: Google Stops Collecting Location Data from Maps
Date: 2023-12-26T12:03:04+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;geolocation;Google;privacy
Slug: 2023-12-26-google-stops-collecting-location-data-from-maps

[Source](https://www.schneier.com/blog/archives/2023/12/google-stops-collecting-location-data-from-maps.html){:target="_blank" rel="noopener"}

> Google Maps now stores location data locally on your device, meaning that Google no longer has that data to turn over to the police. [...]
