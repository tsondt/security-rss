Title: Cloud engineer wreaks havoc on bank network after getting fired
Date: 2023-12-12T19:43:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-12-cloud-engineer-wreaks-havoc-on-bank-network-after-getting-fired

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/12/cloud_engineer_bank_prison/){:target="_blank" rel="noopener"}

> Now he's got two years behind bars to think about his bad choices An ex-First Republic Bank cloud engineer was sentenced to two years in prison for causing more than $220,000 in damage to his former employer's computer network after allegedly using his company-issued laptop to watch pornography.... [...]
