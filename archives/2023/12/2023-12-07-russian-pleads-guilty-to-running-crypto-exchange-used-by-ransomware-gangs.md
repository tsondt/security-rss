Title: Russian pleads guilty to running crypto-exchange used by ransomware gangs
Date: 2023-12-07T12:09:26-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-12-07-russian-pleads-guilty-to-running-crypto-exchange-used-by-ransomware-gangs

[Source](https://www.bleepingcomputer.com/news/security/russian-pleads-guilty-to-running-crypto-exchange-used-by-ransomware-gangs/){:target="_blank" rel="noopener"}

> Russian national Anatoly Legkodymov pleaded guilty to operating the Bitzlato cryptocurrency exchange that helped ransomware gangs and other cybercriminals launder over $700 million. [...]
