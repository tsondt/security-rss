Title: North Korea's state hackers stole $3 billion in crypto since 2017
Date: 2023-12-03T12:11:10-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-12-03-north-koreas-state-hackers-stole-3-billion-in-crypto-since-2017

[Source](https://www.bleepingcomputer.com/news/security/north-koreas-state-hackers-stole-3-billion-in-crypto-since-2017/){:target="_blank" rel="noopener"}

> North Korean-backed state hackers have stolen an estimated $3 billion in a long string of hacks targeting the cryptocurrency industry over the last six years since January 2017. [...]
