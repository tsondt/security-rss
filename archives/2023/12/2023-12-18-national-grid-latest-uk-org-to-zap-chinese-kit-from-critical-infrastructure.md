Title: National Grid latest UK org to zap Chinese kit from critical infrastructure
Date: 2023-12-18T12:36:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-12-18-national-grid-latest-uk-org-to-zap-chinese-kit-from-critical-infrastructure

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/18/national_grid_bans_china_equipment/){:target="_blank" rel="noopener"}

> Move reportedly made after consulting with National Cyber Security Centre The National Grid is reportedly the latest organization in the UK to begin pulling China-manufactured equipment from its network over cybersecurity fears.... [...]
