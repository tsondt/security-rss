Title: "Sierra:21" vulnerabilities impact critical infrastructure routers
Date: 2023-12-06T01:01:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2023-12-06-sierra21-vulnerabilities-impact-critical-infrastructure-routers

[Source](https://www.bleepingcomputer.com/news/security/sierra-21-vulnerabilities-impact-critical-infrastructure-routers/){:target="_blank" rel="noopener"}

> A set of 21 newly discovered vulnerabilities impact Sierra OT/IoT routers and threaten critical infrastructure with remote code execution, unauthorized access, cross-site scripting, authentication bypass, and denial of service attacks. [...]
