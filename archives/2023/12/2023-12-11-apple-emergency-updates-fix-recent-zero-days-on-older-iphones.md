Title: Apple emergency updates fix recent zero-days on older iPhones
Date: 2023-12-11T14:28:12-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple;Security
Slug: 2023-12-11-apple-emergency-updates-fix-recent-zero-days-on-older-iphones

[Source](https://www.bleepingcomputer.com/news/apple/apple-emergency-updates-fix-recent-zero-days-on-older-iphones/){:target="_blank" rel="noopener"}

> Apple has issued emergency security updates to backport patches for two actively exploited zero-day flaws to older iPhones and some Apple Watch and Apple TV models. [...]
