Title: Qakbot's backbot: FBI-led takedown keeps crims at bay for just 3 months
Date: 2023-12-19T09:26:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-12-19-qakbots-backbot-fbi-led-takedown-keeps-crims-at-bay-for-just-3-months

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/19/qakbot_returns/){:target="_blank" rel="noopener"}

> Experts say malware strain make take years to die off completely Multiple sources are confirming the resurgence of Qakbot malware mere months after the FBI and other law enforcement agencies shuttered the Windows botnet.... [...]
