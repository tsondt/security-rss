Title: Ledger dApp supply chain attack steals $600K from crypto wallets
Date: 2023-12-14T11:22:02-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-12-14-ledger-dapp-supply-chain-attack-steals-600k-from-crypto-wallets

[Source](https://www.bleepingcomputer.com/news/security/ledger-dapp-supply-chain-attack-steals-600k-from-crypto-wallets/){:target="_blank" rel="noopener"}

> Ledger is warnings users not to use web3 dApps after a supply chain attack on the 'Ledger dApp Connect Kit' library was found pushing a JavaScript wallet drainer that stole $600,000 in crypto and NFTs. [...]
