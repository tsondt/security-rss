Title: UEFI flaws allow bootkits to pwn potentially hundreds of devices using images
Date: 2023-12-01T20:12:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-12-01-uefi-flaws-allow-bootkits-to-pwn-potentially-hundreds-of-devices-using-images

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/01/uefi_image_parser_flaws/){:target="_blank" rel="noopener"}

> Exploits bypass most secure boot solutions from the biggest chip vendors Hundreds of consumer and enterprise devices are potentially vulnerable to bootkit exploits through unsecured BIOS image parsers.... [...]
