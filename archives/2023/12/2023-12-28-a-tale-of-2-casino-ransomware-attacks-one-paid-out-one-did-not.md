Title: A tale of 2 casino ransomware attacks: One paid out, one did not
Date: 2023-12-28T17:05:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-28-a-tale-of-2-casino-ransomware-attacks-one-paid-out-one-did-not

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/28/casino_ransomware_attacks/){:target="_blank" rel="noopener"}

> What can be learned from MGM's and Caesars' infosec moves Feature The same cybercrime crew broke into two high-profile Las Vegas casino networks over the summer, infected both with ransomware, and stole data belonging to tens of thousands of customers from the mega-resort chains.... [...]
