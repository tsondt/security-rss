Title: Discord in the ranks: Lone Airman behind top-secret info leak on chat platform
Date: 2023-12-12T18:00:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-12-12-discord-in-the-ranks-lone-airman-behind-top-secret-info-leak-on-chat-platform

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/12/us_air_force_discord_leaker/){:target="_blank" rel="noopener"}

> Poor cybersecurity hygiene in the military? Surely not! There was only one US Air National Guardsman behind the leak of top-secret US military documents on Discord, but his chain of command bears some responsibility for letting it happen on their watch.... [...]
