Title: Surveillance by the US Postal Service
Date: 2023-12-13T12:04:24+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;privacy;surveillance;theft;USPS
Slug: 2023-12-13-surveillance-by-the-us-postal-service

[Source](https://www.schneier.com/blog/archives/2023/12/surveillance-by-the-us-postal-service.html){:target="_blank" rel="noopener"}

> This is not about mass surveillance of mail, this is about the sorts of targeted surveillance the US Postal Inspection Service uses to catch mail thieves : To track down an alleged mail thief, a US postal inspector used license plate reader technology, GPS data collected by a rental car company, and, most damning of all, hid a camera inside one of the targeted blue post boxes which captured the suspect’s full face as they allegedly helped themselves to swathes of peoples’ mail. [...]
