Title: How worried should we be about the “AutoSpill” credential leak in Android password managers?
Date: 2023-12-13T15:21:27+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;android;credentials;password managers;passwords
Slug: 2023-12-13-how-worried-should-we-be-about-the-autospill-credential-leak-in-android-password-managers

[Source](https://arstechnica.com/?p=1990601){:target="_blank" rel="noopener"}

> Enlarge / Close up of hand holding smartphone and screen applications with unlocking mobile phones. Concept of technological safety. (credit: Getty Images) By now, you’ve probably heard about a vulnerability named AutoSpill, which can leak credentials from any of the seven leading password managers for Android. The threat it poses is real, but it’s also more limited and easier to contain than much of the coverage to date has recognized. This FAQ dives into the many nuances that make AutoSpill hard for most people (yours truly included) to understand. This post wouldn't have been possible without invaluable assistance from Alesandro [...]
