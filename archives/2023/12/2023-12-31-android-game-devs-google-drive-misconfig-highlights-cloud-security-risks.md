Title: Android game dev’s Google Drive misconfig highlights cloud security risks
Date: 2023-12-31T10:09:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-31-android-game-devs-google-drive-misconfig-highlights-cloud-security-risks

[Source](https://www.bleepingcomputer.com/news/security/android-game-devs-google-drive-misconfig-highlights-cloud-security-risks/){:target="_blank" rel="noopener"}

> Japanese game developer Ateam has proven that a simple Google Drive configuration mistake can result in the potential but unlikely exposure of sensitive information for nearly one million people over a period of six years and eight months. [...]
