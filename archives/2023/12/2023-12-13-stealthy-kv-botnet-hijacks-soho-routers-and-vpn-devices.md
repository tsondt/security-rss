Title: Stealthy KV-botnet hijacks SOHO routers and VPN devices
Date: 2023-12-13T17:47:20-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-13-stealthy-kv-botnet-hijacks-soho-routers-and-vpn-devices

[Source](https://www.bleepingcomputer.com/news/security/stealthy-kv-botnet-hijacks-soho-routers-and-vpn-devices/){:target="_blank" rel="noopener"}

> The Chinese state-sponsored APT hacking group known as Volt Typhoon (Bronze Silhouette) has been linked to a sophisticated botnet named 'KV-botnet' since at least 2022 to attack SOHO routers in high-value targets. [...]
