Title: Cyber-crooks slip into Vans, trample over operations
Date: 2023-12-18T19:45:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-18-cyber-crooks-slip-into-vans-trample-over-operations

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/18/vf_vans_cybersecurity_incident/){:target="_blank" rel="noopener"}

> IT systems encrypted, personal data pilfered from North Face parent, we're told A digital break-in has disrupted VF Corp's operations and its ability to fulfill orders, according to the apparel and footwear giant.... [...]
