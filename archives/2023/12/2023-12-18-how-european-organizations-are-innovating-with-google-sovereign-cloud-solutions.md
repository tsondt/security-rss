Title: How European organizations are innovating with Google Sovereign Cloud solutions
Date: 2023-12-18T17:00:00+00:00
Author: Laurence Lafont
Category: GCP Security
Tags: Security & Identity
Slug: 2023-12-18-how-european-organizations-are-innovating-with-google-sovereign-cloud-solutions

[Source](https://cloud.google.com/blog/products/identity-security/how-european-organizations-are-innovating-with-google-sovereign-cloud-solutions/){:target="_blank" rel="noopener"}

> European organizations continue to embrace cloud-based solutions to support their ongoing digital transformation. But the adoption of breakthrough technologies like generative AI and data analytics to drive new innovations will only accelerate if organizations are confident that when they use cloud services their data will remain secure, private, and under their control. This is why the ability to achieve and maintain digital sovereignty is so critical, and why Google Cloud has brought to market the industry’s broadest portfolio of sovereign cloud solutions as part of our “ Cloud. On Europe’s Terms ” initiative. We are excited to share some recent [...]
