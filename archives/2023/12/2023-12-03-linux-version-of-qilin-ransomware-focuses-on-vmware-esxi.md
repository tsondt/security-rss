Title: Linux version of Qilin ransomware focuses on VMware ESXi
Date: 2023-12-03T16:07:23-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Linux
Slug: 2023-12-03-linux-version-of-qilin-ransomware-focuses-on-vmware-esxi

[Source](https://www.bleepingcomputer.com/news/security/linux-version-of-qilin-ransomware-focuses-on-vmware-esxi/){:target="_blank" rel="noopener"}

> A sample of the Qilin ransomware gang's VMware ESXi encryptor has been found and it could be one of the most advanced and customizable Linux encryptors seen to date. [...]
