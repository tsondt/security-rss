Title: Microsoft unveils new, more secure Windows Protected Print Mode
Date: 2023-12-16T10:20:30-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-12-16-microsoft-unveils-new-more-secure-windows-protected-print-mode

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-unveils-new-more-secure-windows-protected-print-mode/){:target="_blank" rel="noopener"}

> Microsoft announced a new Windows Protected Print Mode (WPP), introducing significant security enhancements to the Windows print system. [...]
