Title: Interpol moves against human traffickers who enslave people to scam you online
Date: 2023-12-12T06:30:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-12-interpol-moves-against-human-traffickers-who-enslave-people-to-scam-you-online

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/12/interpol_cyberscam_trafficker_action/){:target="_blank" rel="noopener"}

> Scum lure folks with promises of good jobs in crypto and then won't let them leave Hundreds of suspected people smugglers have been arrested, and 163 potential victims rescued from servitude, as part of an Interpol-coordinated operation dubbed "Turquesa V" that targeted cyber criminals who lure workers into servitude to carry out their scams.... [...]
