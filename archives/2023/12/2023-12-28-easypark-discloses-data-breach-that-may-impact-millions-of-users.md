Title: EasyPark discloses data breach that may impact millions of users
Date: 2023-12-28T14:38:47-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-28-easypark-discloses-data-breach-that-may-impact-millions-of-users

[Source](https://www.bleepingcomputer.com/news/security/easypark-discloses-data-breach-that-may-impact-millions-of-users/){:target="_blank" rel="noopener"}

> Parking app developer EasyPark has published a notice on its website warning of a data breach it discovered on December 10, 2023, which impacts an unknown number of its millions of users. [...]
