Title: AWS successfully renews GSMA security certification for US East (Ohio) and Europe (Paris) Regions
Date: 2023-12-29T19:26:43+00:00
Author: Janice Leung
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;audit;AWS security;Compliance;GSM;Security;Security Blog;Telecom
Slug: 2023-12-29-aws-successfully-renews-gsma-security-certification-for-us-east-ohio-and-europe-paris-regions

[Source](https://aws.amazon.com/blogs/security/aws-successfully-renews-gsma-security-certification-for-us-east-ohio-and-europe-paris-regions-2/){:target="_blank" rel="noopener"}

> Amazon Web Services is pleased to announce that the AWS US East (Ohio) and Europe (Paris) Regions have been recertified through October 2024 by the GSM Association (GSMA) under its Security Accreditation Scheme for Subscription Management (SAS-SM) with scope Data Centre Operations and Management (DCOM). The US East (Ohio) Region first obtained GSMA certification in [...]
