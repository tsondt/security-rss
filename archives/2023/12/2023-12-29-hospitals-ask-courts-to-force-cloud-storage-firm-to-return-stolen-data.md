Title: Hospitals ask courts to force cloud storage firm to return stolen data
Date: 2023-12-29T15:20:27-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-12-29-hospitals-ask-courts-to-force-cloud-storage-firm-to-return-stolen-data

[Source](https://www.bleepingcomputer.com/news/security/hospitals-ask-courts-to-force-cloud-storage-firm-to-return-stolen-data/){:target="_blank" rel="noopener"}

> Two not-for-profit hospitals in New York are seeking a court order to retrieve data stolen in an August ransomware attack that's now stored on the servers of a Boston cloud storage company. [...]
