Title: CISA: Russian hackers target TeamCity servers since September
Date: 2023-12-13T13:02:28-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-13-cisa-russian-hackers-target-teamcity-servers-since-september

[Source](https://www.bleepingcomputer.com/news/security/cisa-russian-hackers-target-teamcity-servers-since-september/){:target="_blank" rel="noopener"}

> CISA and partner cybersecurity agencies and intelligence services warned that the APT29 hacking group linked to Russia's Foreign Intelligence Service (SVR) has been targeting unpatched TeamCity servers in widespread attacks since September 2023. [...]
