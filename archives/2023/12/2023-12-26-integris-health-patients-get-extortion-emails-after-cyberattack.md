Title: Integris Health patients get extortion emails after cyberattack
Date: 2023-12-26T15:03:49-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-12-26-integris-health-patients-get-extortion-emails-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/integris-health-patients-get-extortion-emails-after-cyberattack/){:target="_blank" rel="noopener"}

> Integris Health patients in Oklahoma are receiving blackmail emails stating that their data was stolen in a cyberattack on the healthcare network, and if they did not pay an extortion demand, the data would be sold to other threat actors. [...]
