Title: Philippines, South Korea, Interpol cuff 3,500 suspected cyber scammers, seize $300M
Date: 2023-12-20T00:32:54+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-12-20-philippines-south-korea-interpol-cuff-3500-suspected-cyber-scammers-seize-300m

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/20/interpol_haechi_iv/){:target="_blank" rel="noopener"}

> Alleged crims used AI to pose as friends, family, romantic partners – and sold dodgy NFTs A transnational police operation has resulted in the arrest of 3,500 alleged cybercriminals and the seizure of $300 million in cash and digital assets.... [...]
