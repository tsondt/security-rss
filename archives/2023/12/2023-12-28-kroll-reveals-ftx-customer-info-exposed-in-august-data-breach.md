Title: Kroll reveals FTX customer info exposed in August data breach
Date: 2023-12-28T13:06:39-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-12-28-kroll-reveals-ftx-customer-info-exposed-in-august-data-breach

[Source](https://www.bleepingcomputer.com/news/security/kroll-reveals-ftx-customer-info-exposed-in-august-data-breach/){:target="_blank" rel="noopener"}

> Risk and financial advisory company Kroll has released additional details regarding the August data breach, which exposed the personal information of FTX bankruptcy claimants. [...]
