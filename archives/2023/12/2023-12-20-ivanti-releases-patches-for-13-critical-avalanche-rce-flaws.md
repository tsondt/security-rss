Title: Ivanti releases patches for 13 critical Avalanche RCE flaws
Date: 2023-12-20T13:03:56-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-20-ivanti-releases-patches-for-13-critical-avalanche-rce-flaws

[Source](https://www.bleepingcomputer.com/news/security/ivanti-releases-patches-for-13-critical-avalanche-rce-flaws/){:target="_blank" rel="noopener"}

> ​Ivanti has released security updates to fix 13 critical security vulnerabilities in the company's Avalanche enterprise mobile device management (MDM) solution. [...]
