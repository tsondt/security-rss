Title: Ukrainian cellular and Internet still out, 1 day after suspected Russian cyberattack
Date: 2023-12-14T01:33:41+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;cellular;cyberattack;Internet;russia;Ukraine
Slug: 2023-12-14-ukrainian-cellular-and-internet-still-out-1-day-after-suspected-russian-cyberattack

[Source](https://arstechnica.com/?p=1991011){:target="_blank" rel="noopener"}

> Enlarge / A service center for "Kyivstar", a Ukrainian telecommunications company, that provides communication services and data transmission based on a broad range of fixed and mobile technologies. (credit: Getty Images) Ukrainian civilians on Wednesday grappled for a second day of widespread cellular phone and Internet outages after a cyberattack, purportedly carried out by Kremlin-supported hackers, hit the country’s biggest mobile phone and Internet provider a day earlier. Two separate hacking groups with ties to the Russian government took responsibility for Tuesday’s attack striking Kyivstar, which has said it serves 24.3 million mobile subscribers and more than 1.1 million home [...]
