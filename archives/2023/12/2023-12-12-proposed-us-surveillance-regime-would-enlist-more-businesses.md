Title: Proposed US surveillance regime would enlist more businesses
Date: 2023-12-12T01:45:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-12-12-proposed-us-surveillance-regime-would-enlist-more-businesses

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/12/setction_702_renewal/){:target="_blank" rel="noopener"}

> Expanded service provider definition could force cafes and hotels to spy for the feds Many US businesses may be required to assist in government-directed surveillance – depending upon which of two reform bills before Congress is approved.... [...]
