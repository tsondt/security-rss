Title: Sophos backports RCE fix after attacks on unsupported firewalls
Date: 2023-12-12T12:29:58-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-12-sophos-backports-rce-fix-after-attacks-on-unsupported-firewalls

[Source](https://www.bleepingcomputer.com/news/security/sophos-backports-rce-fix-after-attacks-on-unsupported-firewalls/){:target="_blank" rel="noopener"}

> Sophos was forced to backport a security update for CVE-2022-3236 for end-of-life (EOL) firewall firmware versions after discovering hackers actively exploiting the flaw in attacks. [...]
