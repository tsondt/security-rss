Title: MongoDB says customer data was exposed in a cyberattack
Date: 2023-12-16T17:37:19-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-12-16-mongodb-says-customer-data-was-exposed-in-a-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/mongodb-says-customer-data-was-exposed-in-a-cyberattack/){:target="_blank" rel="noopener"}

> MongoDB is warning that its corporate systems were breached and that customer data was exposed in a cyberattack that was detected by the company earlier this week. [...]
