Title: Interpol operation arrests 3,500 cybercriminals, seizes $300 million
Date: 2023-12-19T14:09:15-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-12-19-interpol-operation-arrests-3500-cybercriminals-seizes-300-million

[Source](https://www.bleepingcomputer.com/news/security/interpol-operation-arrests-3-500-cybercriminals-seizes-300-million/){:target="_blank" rel="noopener"}

> An international law enforcement operation codenamed 'Operation HAECHI IV' has led to the arrest of 3,500 suspects of various lower-tier cybercrimes and seized $300 million in illicit proceeds. [...]
