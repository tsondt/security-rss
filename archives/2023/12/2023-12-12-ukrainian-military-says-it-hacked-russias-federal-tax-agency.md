Title: Ukrainian military says it hacked Russia's federal tax agency
Date: 2023-12-12T15:39:31-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-12-ukrainian-military-says-it-hacked-russias-federal-tax-agency

[Source](https://www.bleepingcomputer.com/news/security/ukrainian-military-says-it-hacked-russias-federal-tax-agency/){:target="_blank" rel="noopener"}

> ​The Ukrainian government's military intelligence service says it hacked the Russian Federal Taxation Service (FNS), wiping the agency's database and backup copies. [...]
