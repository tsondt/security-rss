Title: A Robot the Size of the World
Date: 2023-12-15T12:01:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;ChatGPT;essays;Internet of Things;LLM;robotics
Slug: 2023-12-15-a-robot-the-size-of-the-world

[Source](https://www.schneier.com/blog/archives/2023/12/a-robot-the-size-of-the-world.html){:target="_blank" rel="noopener"}

> In 2016, I wrote about an Internet that affected the world in a direct, physical manner. It was connected to your smartphone. It had sensors like cameras and thermostats. It had actuators: Drones, autonomous cars. And it had smarts in the middle, using sensor data to figure out what to do and then actually do it. This was the Internet of Things (IoT). The classical definition of a robot is something that senses, thinks, and acts—that’s today’s Internet. We’ve been building a world-sized robot without even realizing it. In 2023, we upgraded the “thinking” part with large-language models (LLMs) like [...]
