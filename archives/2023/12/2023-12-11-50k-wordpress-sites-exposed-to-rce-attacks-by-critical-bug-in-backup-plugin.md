Title: 50K WordPress sites exposed to RCE attacks by critical bug in backup plugin
Date: 2023-12-11T17:46:23-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-11-50k-wordpress-sites-exposed-to-rce-attacks-by-critical-bug-in-backup-plugin

[Source](https://www.bleepingcomputer.com/news/security/50k-wordpress-sites-exposed-to-rce-attacks-by-critical-bug-in-backup-plugin/){:target="_blank" rel="noopener"}

> A critical severity vulnerability in a WordPress plugin with more than 90,000 installs can let attackers gain remote code execution to fully compromise vulnerable websites. [...]
