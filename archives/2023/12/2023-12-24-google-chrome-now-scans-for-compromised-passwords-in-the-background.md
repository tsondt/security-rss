Title: Google Chrome now scans for compromised passwords in the background
Date: 2023-12-24T10:11:12-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google;Security
Slug: 2023-12-24-google-chrome-now-scans-for-compromised-passwords-in-the-background

[Source](https://www.bleepingcomputer.com/news/google/google-chrome-now-scans-for-compromised-passwords-in-the-background/){:target="_blank" rel="noopener"}

> Google says the Chrome Safety Check feature will work in the background to check if passwords saved in the web browser have been compromised. [...]
