Title: Terrapin attacks can downgrade security of OpenSSH connections
Date: 2023-12-19T12:03:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-19-terrapin-attacks-can-downgrade-security-of-openssh-connections

[Source](https://www.bleepingcomputer.com/news/security/terrapin-attacks-can-downgrade-security-of-openssh-connections/){:target="_blank" rel="noopener"}

> Academic researchers developed a new attack called Terrapin that manipulates sequence numbers during the handshake process to breaks the SSH channel integrity when certain widely-used encryption modes are used. [...]
