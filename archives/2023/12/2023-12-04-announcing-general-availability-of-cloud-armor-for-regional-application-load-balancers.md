Title: Announcing general availability of Cloud Armor for regional application load balancers
Date: 2023-12-04T17:00:00+00:00
Author: Anil Nandigam
Category: GCP Security
Tags: Security & Identity
Slug: 2023-12-04-announcing-general-availability-of-cloud-armor-for-regional-application-load-balancers

[Source](https://cloud.google.com/blog/products/identity-security/announcing-general-availability-of-cloud-armor-for-regional-application-load-balancers/){:target="_blank" rel="noopener"}

> Google Cloud Armor provides our customers with advanced DDoS defense and Web Application Firewall (WAF) capabilities. Today, we’re excited to announce the general availability of Cloud Armor for Regional External Application Load Balancers, which can help create regionally-scoped Cloud Armor security policies. In these policies, rules are evaluated and enforced in a designated Google Cloud region to protect web and API workloads from DDoS attacks and other Layer 7 attacks. Cloud Armor recently helped mitigate the largest DDoS attack known to date, and we are continually expanding Cloud Armor’s capabilities to help our customers protect their environment from DDoS and [...]
