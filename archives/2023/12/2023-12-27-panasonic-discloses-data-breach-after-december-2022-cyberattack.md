Title: Panasonic discloses data breach after December 2022 cyberattack
Date: 2023-12-27T12:28:57-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-27-panasonic-discloses-data-breach-after-december-2022-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/panasonic-discloses-data-breach-after-december-2022-cyberattack/){:target="_blank" rel="noopener"}

> Panasonic Avionics Corporation, a leading supplier of in-flight communications and entertainment systems, disclosed a data breach affecting an undisclosed number of individuals after its corporate network was breached more than one year ago, in December 2022. [...]
