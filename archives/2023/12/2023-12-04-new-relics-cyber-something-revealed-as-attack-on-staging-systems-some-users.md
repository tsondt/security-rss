Title: New Relic's cyber-something revealed as attack on staging systems, some users
Date: 2023-12-04T04:27:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-12-04-new-relics-cyber-something-revealed-as-attack-on-staging-systems-some-users

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/04/new_relic_security_incident/){:target="_blank" rel="noopener"}

> Ongoing investigation found evidence of stolen employee creds and social engineering Nine days after issuing a vaguely worded warning about a possible cyber security incident, web tracking and analytics outfit New Relic has revealed a two-front attack.... [...]
