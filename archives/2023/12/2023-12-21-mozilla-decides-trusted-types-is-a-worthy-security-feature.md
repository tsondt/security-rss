Title: Mozilla decides Trusted Types is a worthy security feature
Date: 2023-12-21T11:03:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-12-21-mozilla-decides-trusted-types-is-a-worthy-security-feature

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/21/mozilla_decides_trusted_types_is/){:target="_blank" rel="noopener"}

> DOM-XSS attacks have become scarce on Google websites since TT debuted Mozilla last week revised its position on a web security technology called Trusted Types, which it has decided to implement in its Firefox browser.... [...]
