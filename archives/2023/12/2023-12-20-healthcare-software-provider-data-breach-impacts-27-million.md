Title: Healthcare software provider data breach impacts 2.7 million
Date: 2023-12-20T10:21:32-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-12-20-healthcare-software-provider-data-breach-impacts-27-million

[Source](https://www.bleepingcomputer.com/news/security/healthcare-software-provider-data-breach-impacts-27-million/){:target="_blank" rel="noopener"}

> ESO Solutions, a provider of software products for healthcare organizations and fire departments, disclosed that data belonging to 2.7 million patients has been compromised as a result of a ransomware attack. [...]
