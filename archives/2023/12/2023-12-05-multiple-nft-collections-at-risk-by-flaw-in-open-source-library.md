Title: Multiple NFT collections at risk by flaw in open-source library
Date: 2023-12-05T18:08:38-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-12-05-multiple-nft-collections-at-risk-by-flaw-in-open-source-library

[Source](https://www.bleepingcomputer.com/news/security/multiple-nft-collections-at-risk-by-flaw-in-open-source-library/){:target="_blank" rel="noopener"}

> A vulnerability in an open-source library that is common across the Web3 space impacts the security of pre-built smart contracts, affecting multiple NFT collections, including Coinbase. [...]
