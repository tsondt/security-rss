Title: Governance at scale: Enforce permissions and compliance by using policy as code
Date: 2023-12-13T17:25:46+00:00
Author: Roland Odorfer
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;governance;Security Blog
Slug: 2023-12-13-governance-at-scale-enforce-permissions-and-compliance-by-using-policy-as-code

[Source](https://aws.amazon.com/blogs/security/governance-at-scale-enforce-permissions-and-compliance-by-using-policy-as-code/){:target="_blank" rel="noopener"}

> AWS Identity and Access Management (IAM) policies are at the core of access control on AWS. They enable the bundling of permissions, helping to provide effective and modular access control for AWS services. Service control policies (SCPs) complement IAM policies by helping organizations enforce permission guardrails at scale across their AWS accounts. The use of access control [...]
