Title: ALPHV ransomware site outage rumored to be caused by law enforcement
Date: 2023-12-08T13:30:15-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-12-08-alphv-ransomware-site-outage-rumored-to-be-caused-by-law-enforcement

[Source](https://www.bleepingcomputer.com/news/security/alphv-ransomware-site-outage-rumored-to-be-caused-by-law-enforcement/){:target="_blank" rel="noopener"}

> A law enforcement operation is rumored to be behind an outage affecting ALPHV ransomware gang's websites over the last 30 hours. [...]
