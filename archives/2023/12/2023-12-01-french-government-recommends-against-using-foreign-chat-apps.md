Title: French government recommends against using foreign chat apps
Date: 2023-12-01T13:12:49-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-01-french-government-recommends-against-using-foreign-chat-apps

[Source](https://www.bleepingcomputer.com/news/security/french-government-recommends-against-using-foreign-chat-apps/){:target="_blank" rel="noopener"}

> Prime Minister of France Élisabeth Borne signed a circular last week requesting all government employees to uninstall foreign communication apps such as Signal, WhatsApp, and Telegram by December 8, 2023, in favor of a French messaging app named 'Olvid.' [...]
