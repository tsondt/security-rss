Title: FBI disrupts Blackcat ransomware operation, creates decryption tool
Date: 2023-12-19T09:16:00-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-12-19-fbi-disrupts-blackcat-ransomware-operation-creates-decryption-tool

[Source](https://www.bleepingcomputer.com/news/security/fbi-disrupts-blackcat-ransomware-operation-creates-decryption-tool/){:target="_blank" rel="noopener"}

> The Department of Justice announced today that the FBI successfully breached the ALPHV ransomware operation's servers to monitor their activities and obtain decryption keys. [...]
