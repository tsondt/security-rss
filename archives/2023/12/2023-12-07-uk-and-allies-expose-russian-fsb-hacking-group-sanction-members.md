Title: UK and allies expose Russian FSB hacking group, sanction members
Date: 2023-12-07T11:38:13-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-07-uk-and-allies-expose-russian-fsb-hacking-group-sanction-members

[Source](https://www.bleepingcomputer.com/news/security/uk-and-allies-expose-russian-fsb-hacking-group-sanction-members/){:target="_blank" rel="noopener"}

> The UK National Cyber Security Centre (NCSC) and Microsoft warn that the Russian state-backed actor "Callisto Group" (aka "Seaborgium" or "Star Blizzard") is targeting organizations worldwide with spear-phishing campaigns used to steal account credentials and data. [...]
