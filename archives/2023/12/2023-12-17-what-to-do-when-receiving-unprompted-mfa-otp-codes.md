Title: What to do when receiving unprompted MFA OTP codes
Date: 2023-12-17T11:06:12-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-12-17-what-to-do-when-receiving-unprompted-mfa-otp-codes

[Source](https://www.bleepingcomputer.com/news/security/what-to-do-when-receiving-unprompted-mfa-otp-codes/){:target="_blank" rel="noopener"}

> Receiving an unprompted one-time passcode (OTP) sent as an email or text should be a cause for concern as it likely means your credentials have been stolen. [...]
