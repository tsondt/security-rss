Title: Protect your Active Directory from these Password-based Vulnerabilities
Date: 2023-12-14T10:02:04-05:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-12-14-protect-your-active-directory-from-these-password-based-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/protect-your-active-directory-from-these-password-based-vulnerabilities/){:target="_blank" rel="noopener"}

> To safeguard against potential cyberattacks and outages, it is essential to be vigilant against common Active Directory attacks, Learn more from Specops Software about these attacks and how harden your defenses. [...]
