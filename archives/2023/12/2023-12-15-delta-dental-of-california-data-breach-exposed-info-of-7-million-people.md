Title: Delta Dental of California data breach exposed info of 7 million people
Date: 2023-12-15T09:53:04-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-15-delta-dental-of-california-data-breach-exposed-info-of-7-million-people

[Source](https://www.bleepingcomputer.com/news/security/delta-dental-of-california-data-breach-exposed-info-of-7-million-people/){:target="_blank" rel="noopener"}

> Delta Dental of California and its affiliates are warning almost seven million patients that they suffered a data breach after personal data was exposed in a MOVEit Transfer software breach. [...]
