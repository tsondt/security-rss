Title: Tipalti investigates claims of data stolen in ransomware attack
Date: 2023-12-04T14:22:36-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-12-04-tipalti-investigates-claims-of-data-stolen-in-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/tipalti-investigates-claims-of-data-stolen-in-ransomware-attack/){:target="_blank" rel="noopener"}

> Tipalti says they are investigating claims that the ALPHV ransomware gang breached its network and stole 256 GB of data, including data for Roblox and Twitch. [...]
