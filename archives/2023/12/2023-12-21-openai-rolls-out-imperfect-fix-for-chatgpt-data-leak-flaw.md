Title: OpenAI rolls out imperfect fix for ChatGPT data leak flaw
Date: 2023-12-21T11:44:57-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-21-openai-rolls-out-imperfect-fix-for-chatgpt-data-leak-flaw

[Source](https://www.bleepingcomputer.com/news/security/openai-rolls-out-imperfect-fix-for-chatgpt-data-leak-flaw/){:target="_blank" rel="noopener"}

> OpenAI has mitigated a data exfiltration bug in ChatGPT that could potentially leak conversation details to an external URL. [...]
