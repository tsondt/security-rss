Title: Lapsus$ teen sentenced to indefinite detention in hospital for Nvidia, GTA cyberattacks
Date: 2023-12-21T22:15:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-21-lapsus-teen-sentenced-to-indefinite-detention-in-hospital-for-nvidia-gta-cyberattacks

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/21/lapsus_teens_sentenced/){:target="_blank" rel="noopener"}

> Arion Kurtaj will remain hospitalized until a mental health tribunal says he can leave Two British teens who were members of the Lapsus$ gang have been sentenced for their roles in a cyber-crime spree that included compromising Uber, Nvidia, and fintech firm Revolut, and also blackmailing Grand Theft Auto maker Rockstar Games.... [...]
