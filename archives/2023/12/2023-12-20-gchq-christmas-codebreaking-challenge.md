Title: GCHQ Christmas Codebreaking Challenge
Date: 2023-12-20T12:05:24+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptanalysis;GCHQ;history of cryptography
Slug: 2023-12-20-gchq-christmas-codebreaking-challenge

[Source](https://www.schneier.com/blog/archives/2023/12/gchq-christmas-codebreaking-challenge.html){:target="_blank" rel="noopener"}

> Looks like fun. Details here. [...]
