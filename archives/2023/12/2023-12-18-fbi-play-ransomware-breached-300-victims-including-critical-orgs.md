Title: FBI: Play ransomware breached 300 victims, including critical orgs
Date: 2023-12-18T11:24:42-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-18-fbi-play-ransomware-breached-300-victims-including-critical-orgs

[Source](https://www.bleepingcomputer.com/news/security/fbi-play-ransomware-breached-300-victims-including-critical-orgs/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) says the Play ransomware gang has breached roughly 300 organizations worldwide between June 2022 and October 2023, some of them critical infrastructure entities. [...]
