Title: GitHub warns users to enable 2FA before upcoming deadline
Date: 2023-12-26T16:03:37-05:00
Author: Mayank Parmar
Category: BleepingComputer
Tags: Software;Security
Slug: 2023-12-26-github-warns-users-to-enable-2fa-before-upcoming-deadline

[Source](https://www.bleepingcomputer.com/news/software/github-warns-users-to-enable-2fa-before-upcoming-deadline/){:target="_blank" rel="noopener"}

> GitHub is warning users that they will soon have limited functionality on the site if they do not enable two-factor authentication (2FA) on their accounts. [...]
