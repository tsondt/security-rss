Title: Nissan is investigating cyberattack and potential data breach
Date: 2023-12-06T08:54:38-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-06-nissan-is-investigating-cyberattack-and-potential-data-breach

[Source](https://www.bleepingcomputer.com/news/security/nissan-is-investigating-cyberattack-and-potential-data-breach/){:target="_blank" rel="noopener"}

> Japanese car maker Nissan is investigating a cyberattack that targeted its systems in Australia and New Zealand, which may have let hackers access personal information. [...]
