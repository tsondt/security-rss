Title: New cybercrime market 'OLVX' gains popularity among hackers
Date: 2023-12-13T07:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-13-new-cybercrime-market-olvx-gains-popularity-among-hackers

[Source](https://www.bleepingcomputer.com/news/security/new-cybercrime-market-olvx-gains-popularity-among-hackers/){:target="_blank" rel="noopener"}

> A new cybercrime marketplace, OLVX, has emerged and is quickly gaining new customers looking to purchase tools to conduct online fraud and cyberattacks. [...]
