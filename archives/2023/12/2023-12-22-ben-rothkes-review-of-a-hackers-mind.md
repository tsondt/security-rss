Title: Ben Rothke’s Review of A Hacker’s Mind
Date: 2023-12-22T20:08:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;A Hacker's Mind;books;Schneier news
Slug: 2023-12-22-ben-rothkes-review-of-a-hackers-mind

[Source](https://www.schneier.com/blog/archives/2023/12/ben-rothkes-review-of-a-hackers-mind.html){:target="_blank" rel="noopener"}

> Ben Rothke chose A Hacker’s Mind as “the best information security book of 2023.” [...]
