Title: New phishing attack steals your Instagram backup codes to bypass 2FA
Date: 2023-12-20T14:35:15-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-20-new-phishing-attack-steals-your-instagram-backup-codes-to-bypass-2fa

[Source](https://www.bleepingcomputer.com/news/security/new-phishing-attack-steals-your-instagram-backup-codes-to-bypass-2fa/){:target="_blank" rel="noopener"}

> A new phishing campaign pretending to be a 'copyright infringement' email attempts to steal the backup codes of Instagram users, allowing hackers to bypass the two-factor authentication configured on the account. [...]
