Title: Lockbit ransomware disrupts emergency care at German hospitals
Date: 2023-12-27T16:05:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-12-27-lockbit-ransomware-disrupts-emergency-care-at-german-hospitals

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-disrupts-emergency-care-at-german-hospitals/){:target="_blank" rel="noopener"}

> German hospital network Katholische Hospitalvereinigung Ostwestfalen (KHO) has confirmed that recent service disruptions were caused by a Lockbit ransomware attack where the threat actors gained access to IT systems and encrypted devices on the network. [...]
