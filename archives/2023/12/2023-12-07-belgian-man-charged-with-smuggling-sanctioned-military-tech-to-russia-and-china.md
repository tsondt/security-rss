Title: Belgian man charged with smuggling sanctioned military tech to Russia and China
Date: 2023-12-07T07:30:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-07-belgian-man-charged-with-smuggling-sanctioned-military-tech-to-russia-and-china

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/07/belgian_russia_china_hardware/){:target="_blank" rel="noopener"}

> Indictments allege plot to shift FPGAs, accelerometers, and spycams A Belgian man has been arrested and charged for his role in a years-long smuggling scheme to export military-grade electronics from the US to Russia and China.... [...]
