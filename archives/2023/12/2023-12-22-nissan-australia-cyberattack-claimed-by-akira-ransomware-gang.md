Title: Nissan Australia cyberattack claimed by Akira ransomware gang
Date: 2023-12-22T11:38:37-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-22-nissan-australia-cyberattack-claimed-by-akira-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/nissan-australia-cyberattack-claimed-by-akira-ransomware-gang/){:target="_blank" rel="noopener"}

> Today, the Akira ransomware gang claimed that it breached the network of Nissan Australia, the Australian division of Japanese car maker Nissan. [...]
