Title: iPhone Triangulation attack abused undocumented hardware feature
Date: 2023-12-27T09:14:41-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple;Mobile
Slug: 2023-12-27-iphone-triangulation-attack-abused-undocumented-hardware-feature

[Source](https://www.bleepingcomputer.com/news/security/iphone-triangulation-attack-abused-undocumented-hardware-feature/){:target="_blank" rel="noopener"}

> The Operation Triangulation spyware attacks targeting iPhone devices since 2019 leveraged undocumented features in Apple chips to bypass hardware-based security protections. [...]
