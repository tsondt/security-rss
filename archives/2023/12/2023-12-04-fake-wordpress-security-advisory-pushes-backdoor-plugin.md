Title: Fake WordPress security advisory pushes backdoor plugin
Date: 2023-12-04T12:19:22-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-04-fake-wordpress-security-advisory-pushes-backdoor-plugin

[Source](https://www.bleepingcomputer.com/news/security/fake-wordpress-security-advisory-pushes-backdoor-plugin/){:target="_blank" rel="noopener"}

> WordPress administrators are being emailed fake WordPress security advisories for a fictitious vulnerability tracked as CVE-2023-45124 to infect sites with a malicious plugin. [...]
