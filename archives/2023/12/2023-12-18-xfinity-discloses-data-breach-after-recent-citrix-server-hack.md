Title: Xfinity discloses data breach after recent Citrix server hack
Date: 2023-12-18T19:03:30-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-18-xfinity-discloses-data-breach-after-recent-citrix-server-hack

[Source](https://www.bleepingcomputer.com/news/security/xfinity-discloses-data-breach-after-recent-citrix-server-hack/){:target="_blank" rel="noopener"}

> Comcast Cable Communications, doing business as Xfinity, disclosed on Monday that attackers who breached one of its Citrix servers in October also stole customer-sensitive information from its systems. [...]
