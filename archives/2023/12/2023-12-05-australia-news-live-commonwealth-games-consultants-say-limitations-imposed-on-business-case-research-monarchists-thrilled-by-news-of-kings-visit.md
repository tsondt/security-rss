Title: Australia news live: Commonwealth Games consultants say limitations imposed on business case research; monarchists ‘thrilled’ by news of King’s visit
Date: 2023-12-05T00:58:32+00:00
Author: Emily Wind
Category: The Guardian
Tags: Australia news;Australian politics;Anthony Albanese;Banking;Australian economy;Interest rates;Westpac;Technology;Data and computer security;Cop28;Environment;Business;Commonwealth Games
Slug: 2023-12-05-australia-news-live-commonwealth-games-consultants-say-limitations-imposed-on-business-case-research-monarchists-thrilled-by-news-of-kings-visit

[Source](https://www.theguardian.com/australia-news/live/2023/dec/05/australia-news-live-reserve-bank-interest-rates-michele-bullock-cost-of-living-inflation-immigration-detention-high-court-politics){:target="_blank" rel="noopener"}

> Meanwhile, ahead of the Reserve Bank’s decision on Tuesday afternoon, most economists expect the cash rate to stay steady at 4.35%. Follow the day’s news live Get our morning and afternoon news emails, free app or daily news podcast Marles says Australians should express views on Israel-Hamas conflict ‘in a safe and a peaceful way’ The deputy prime minister, Richard Marles, is speaking to ABC RN. He is asked whether Australia is “a safe place for Jews right now”, amid a discussion of the Israel-Hamas war. [I understand] many of the Jewish community are finding this to be a very [...]
