Title: AI Is Scarily Good at Guessing the Location of Random Photos
Date: 2023-12-29T12:03:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;data privacy;geolocation;privacy
Slug: 2023-12-29-ai-is-scarily-good-at-guessing-the-location-of-random-photos

[Source](https://www.schneier.com/blog/archives/2023/12/ai-is-scarily-good-at-guessing-the-location-of-random-photos.html){:target="_blank" rel="noopener"}

> Wow : To test PIGEON’s performance, I gave it five personal photos from a trip I took across America years ago, none of which have been published online. Some photos were snapped in cities, but a few were taken in places nowhere near roads or other easily recognizable landmarks. That didn’t seem to matter much. It guessed a campsite in Yellowstone to within around 35 miles of the actual location. The program placed another photo, taken on a street in San Francisco, to within a few city blocks. Not every photo was an easy match: The program mistakenly linked one [...]
