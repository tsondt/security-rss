Title: VMware fixes critical Cloud Director auth bypass unpatched for 2 weeks
Date: 2023-12-01T12:58:39-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-01-vmware-fixes-critical-cloud-director-auth-bypass-unpatched-for-2-weeks

[Source](https://www.bleepingcomputer.com/news/security/vmware-fixes-critical-cloud-director-auth-bypass-unpatched-for-2-weeks/){:target="_blank" rel="noopener"}

> VMware has fixed a critical authentication bypass vulnerability in Cloud Director appliance deployments, a bug that was left unpatched for over two weeks since it was disclosed on November 14th. [...]
