Title: EU lawmakers finalize cyber security rules that panicked open source devs
Date: 2023-12-04T06:01:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-12-04-eu-lawmakers-finalize-cyber-security-rules-that-panicked-open-source-devs

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/04/infosec_in_brief/){:target="_blank" rel="noopener"}

> PLUS: Montana TikTok ban ruled unconstitutional; Dollar Tree employee data stolen; critical vulnerabilities Infosec in brief The European Union’s Parliament and Council have reached an agreement on the Cyber Resilience Act (CRA), setting the long-awaited security regulation on a path to final approval and adoption, along with new rules exempting open source software.... [...]
