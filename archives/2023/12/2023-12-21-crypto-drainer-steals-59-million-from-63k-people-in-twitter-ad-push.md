Title: Crypto drainer steals $59 million from 63k people in Twitter ad push
Date: 2023-12-21T16:23:33-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-12-21-crypto-drainer-steals-59-million-from-63k-people-in-twitter-ad-push

[Source](https://www.bleepingcomputer.com/news/security/crypto-drainer-steals-59-million-from-63k-people-in-twitter-ad-push/){:target="_blank" rel="noopener"}

> Google and Twitter ads are promoting sites containing a cryptocurrency drainer named 'MS Drainer' that has already stolen $59 million from 63,210 victims over the past nine months. [...]
