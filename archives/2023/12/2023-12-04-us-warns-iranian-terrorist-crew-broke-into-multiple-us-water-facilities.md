Title: US warns Iranian terrorist crew broke into 'multiple' US water facilities
Date: 2023-12-04T23:30:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-04-us-warns-iranian-terrorist-crew-broke-into-multiple-us-water-facilities

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/04/iran_terrorist_us_water_attacks/){:target="_blank" rel="noopener"}

> There's a war on and critical infrastructure operators are still using default passwords Iran-linked cyber thugs have exploited Israeli-made programmable logic controllers (PLCs) used in "multiple" water systems and other operational technology environments at facilities across the US, according to multiple law enforcement agencies.... [...]
