Title: Regulator says stranger entered hospital, treated a patient, took a document ... then vanished
Date: 2023-12-01T10:15:14+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-12-01-regulator-says-stranger-entered-hospital-treated-a-patient-took-a-document-then-vanished

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/01/nhs_health_board_ticked_off/){:target="_blank" rel="noopener"}

> Scottish health group to tweak security checks, access authorization to avoid a repeat NHS Fife is on the wrong end of a stern ticking off by Britain's data regulator after it made a howling privacy error that aided an as yet unknown person who had entered a hospital ward only to walk off with data on 14 patients.... [...]
