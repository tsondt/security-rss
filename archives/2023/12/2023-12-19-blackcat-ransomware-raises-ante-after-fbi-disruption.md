Title: BlackCat Ransomware Raises Ante After FBI Disruption
Date: 2023-12-19T22:49:43+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Other;ALPHV ransomware;BlackCat ransomware;Deputy Attorney General Lisa O. Monaco;fbi;Lawrence Abrams;U.S. Department of Justice
Slug: 2023-12-19-blackcat-ransomware-raises-ante-after-fbi-disruption

[Source](https://krebsonsecurity.com/2023/12/blackcat-ransomware-raises-ante-after-fbi-disruption/){:target="_blank" rel="noopener"}

> The U.S. Federal Bureau of Investigation (FBI) disclosed today that it infiltrated the world’s second most prolific ransomware gang, a Russia-based criminal group known as ALPHV and BlackCat. The FBI said it seized the gang’s darknet website, and released a decryption tool that hundreds of victim companies can use to recover systems. Meanwhile, BlackCat responded by briefly “unseizing” its darknet site with a message promising 90 percent commissions for affiliates who continue to work with the crime group, and open season on everything from hospitals to nuclear power plants. A slightly modified version of the FBI seizure notice on the [...]
