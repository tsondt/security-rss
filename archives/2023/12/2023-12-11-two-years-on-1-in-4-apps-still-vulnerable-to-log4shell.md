Title: Two years on, 1 in 4 apps still vulnerable to Log4Shell
Date: 2023-12-11T15:01:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-12-11-two-years-on-1-in-4-apps-still-vulnerable-to-log4shell

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/11/log4j_vulnerabilities/){:target="_blank" rel="noopener"}

> Lack of awareness still blamed for patching apathy despite it being among most infamous bugs of all time Two years after the Log4Shell vulnerability in the open source Java-based Log4j logging utility was disclosed, circa one in four applications are dependent on outdated libraries, leaving them open to exploitation.... [...]
