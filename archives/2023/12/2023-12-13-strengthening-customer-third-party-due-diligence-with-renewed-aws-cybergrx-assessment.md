Title: Strengthening customer third-party due diligence with renewed AWS CyberGRX assessment
Date: 2023-12-13T14:30:06+00:00
Author: Naranjan Goklani
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;3P Risk;Auditing;AWS security;Compliance;Cyber Risk Management;CyberGRX;CyberGRX Assessment;Security;Security Blog;Third Party Risk;Third-Party Risk Management;TPRM
Slug: 2023-12-13-strengthening-customer-third-party-due-diligence-with-renewed-aws-cybergrx-assessment

[Source](https://aws.amazon.com/blogs/security/strengthening-customer-third-party-due-diligence-with-renewed-aws-cybergrx-assessment/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce the successful renewal of the AWS CyberGRX cyber risk assessment report. This third-party validated report helps customers perform effective cloud supplier due diligence on AWS and enhances customers’ third-party risk management process. With the increase in adoption of cloud products and services across multiple sectors and industries, AWS has become a critical [...]
