Title: AWS Security Profile: Arynn Crow, Sr. Manager for AWS User AuthN
Date: 2023-12-15T14:29:10+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Thought Leadership;AWS Security Profile;AWS Security Profiles;Security Blog
Slug: 2023-12-15-aws-security-profile-arynn-crow-sr-manager-for-aws-user-authn

[Source](https://aws.amazon.com/blogs/security/aws-security-profile-arynn-crow-sr-manager-for-aws-user-authn/){:target="_blank" rel="noopener"}

> AWS Security Profile series, I interview some of the humans who work in AWS Security and help keep our customers safe and secure. In this profile, I interviewed Arynn Crow, senior manager for AWS User AuthN in AWS Identity. How long have you been at AWS, and what do you do in your current role? [...]
