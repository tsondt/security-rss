Title: Cyber security isn’t simple, but it could be
Date: 2023-12-13T08:59:10+00:00
Author: John E Dunn
Category: The Register
Tags: 
Slug: 2023-12-13-cyber-security-isnt-simple-but-it-could-be

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/13/cyber_security_isnt_simple_but/){:target="_blank" rel="noopener"}

> The biggest problem is a tendency to ignore problems you can’t see or haven’t looked for, says SecurityHQ Sponsored Feature Most experts agree cybersecurity is now so complex that managing it has become a security problem in itself.... [...]
