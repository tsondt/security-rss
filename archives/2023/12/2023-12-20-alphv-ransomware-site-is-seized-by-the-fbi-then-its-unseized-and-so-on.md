Title: AlphV ransomware site is “seized” by the FBI. Then it’s “unseized.” And so on.
Date: 2023-12-20T21:08:54+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;alphv;fbi;ransomware;tor
Slug: 2023-12-20-alphv-ransomware-site-is-seized-by-the-fbi-then-its-unseized-and-so-on

[Source](https://arstechnica.com/?p=1992382){:target="_blank" rel="noopener"}

> Enlarge / Shortly after the FBI posted a notice saying it had seized the dark-web site of AlphV, the ransomware group posted this notice claiming otherwise. The FBI spent much of Tuesday locked in an online tug-of-war with one of the Internet’s most aggressive ransomware groups after taking control of infrastructure the group has used to generate more than $300 million in illicit payments to date. Early Tuesday morning, the dark-web site belonging to AlphV, a ransomware group that also goes by the name BlackCat, suddenly started displaying a banner that said it had been seized by the FBI as [...]
