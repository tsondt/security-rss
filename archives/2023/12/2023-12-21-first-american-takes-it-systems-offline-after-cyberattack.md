Title: First American takes IT systems offline after cyberattack
Date: 2023-12-21T14:06:26-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-21-first-american-takes-it-systems-offline-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/first-american-takes-it-systems-offline-after-cyberattack/){:target="_blank" rel="noopener"}

> First American Financial Corporation, the second-largest title insurance company in the United States, took some of its systems offline today to contain the impact of a cyberattack. [...]
