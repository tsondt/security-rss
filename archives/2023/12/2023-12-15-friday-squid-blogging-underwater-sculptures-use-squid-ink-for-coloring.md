Title: Friday Squid Blogging: Underwater Sculptures Use Squid Ink for Coloring
Date: 2023-12-15T22:06:30+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-12-15-friday-squid-blogging-underwater-sculptures-use-squid-ink-for-coloring

[Source](https://www.schneier.com/blog/archives/2023/12/friday-squid-blogging-underwater-sculptures-use-squid-ink-for-coloring.html){:target="_blank" rel="noopener"}

> The Molinière Underwater Sculpture Park has pieces that are colored in part with squid ink. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
