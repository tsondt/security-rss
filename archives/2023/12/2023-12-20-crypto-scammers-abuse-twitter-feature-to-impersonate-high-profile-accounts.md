Title: Crypto scammers abuse Twitter ‘feature’ to impersonate high-profile accounts
Date: 2023-12-20T15:17:10-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-12-20-crypto-scammers-abuse-twitter-feature-to-impersonate-high-profile-accounts

[Source](https://www.bleepingcomputer.com/news/security/crypto-scammers-abuse-twitter-feature-to-impersonate-high-profile-accounts/){:target="_blank" rel="noopener"}

> Cryptocurrency scammers are abusing a legitimate Twitter "feature" to promote scams, fake giveaways, and fraudulent Telegram channels used to steal your crypto and NFTs. [...]
