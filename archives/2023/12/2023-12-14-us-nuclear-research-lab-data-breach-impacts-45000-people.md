Title: U.S. nuclear research lab data breach impacts 45,000 people
Date: 2023-12-14T12:59:50-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-14-us-nuclear-research-lab-data-breach-impacts-45000-people

[Source](https://www.bleepingcomputer.com/news/security/us-nuclear-research-lab-data-breach-impacts-45-000-people/){:target="_blank" rel="noopener"}

> The Idaho National Laboratory (INL) confirmed that attackers stole the personal information of more than 45,000 individuals after breaching its cloud-based Oracle HCM HR management platform last month. [...]
