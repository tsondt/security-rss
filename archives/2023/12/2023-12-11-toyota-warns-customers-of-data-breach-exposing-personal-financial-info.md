Title: Toyota warns customers of data breach exposing personal, financial info
Date: 2023-12-11T10:32:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-11-toyota-warns-customers-of-data-breach-exposing-personal-financial-info

[Source](https://www.bleepingcomputer.com/news/security/toyota-warns-customers-of-data-breach-exposing-personal-financial-info/){:target="_blank" rel="noopener"}

> Toyota Financial Services (TFS) is warning customers it suffered a data breach, stating that sensitive personal and financial data was exposed in the attack. [...]
