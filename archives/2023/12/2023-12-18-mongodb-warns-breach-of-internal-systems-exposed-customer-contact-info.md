Title: MongoDB warns breach of internal systems exposed customer contact info
Date: 2023-12-18T02:25:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-12-18-mongodb-warns-breach-of-internal-systems-exposed-customer-contact-info

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/18/infosec_in_brief/){:target="_blank" rel="noopener"}

> PLUS: Cancer patients get ransom notes for Christmas, Delta Dental is the latest MOVEit victim, and critical vulns Infosec in brief MongoDB on Saturday issued an alert warning of "a security incident involving unauthorized access to certain MongoDB corporate systems, which includes exposure of customer account metadata and contact information."... [...]
