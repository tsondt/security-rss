Title: Amazon sues REKK fraud gang that stole millions in illicit refunds
Date: 2023-12-08T11:47:42-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-08-amazon-sues-rekk-fraud-gang-that-stole-millions-in-illicit-refunds

[Source](https://www.bleepingcomputer.com/news/security/amazon-sues-rekk-fraud-gang-that-stole-millions-in-illicit-refunds/){:target="_blank" rel="noopener"}

> Amazon's Customer Protection and Enforcement team has taken legal action against an underground store refund scheme that has resulted in the theft of millions of dollars worth of products from Amazon's online platforms. [...]
