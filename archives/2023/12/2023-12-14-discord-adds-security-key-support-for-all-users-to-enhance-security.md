Title: Discord adds Security Key support for all users to enhance security
Date: 2023-12-14T13:21:41-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2023-12-14-discord-adds-security-key-support-for-all-users-to-enhance-security

[Source](https://www.bleepingcomputer.com/news/security/discord-adds-security-key-support-for-all-users-to-enhance-security/){:target="_blank" rel="noopener"}

> Discord has made security key multi-factor authentication (MFA) available for all accounts on the platform, bringing significant security and anti-phishing benefits to its 500+ million registered users. [...]
