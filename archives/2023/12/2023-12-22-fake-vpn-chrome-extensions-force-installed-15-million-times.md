Title: Fake VPN Chrome extensions force-installed 1.5 million times
Date: 2023-12-22T08:30:15-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-22-fake-vpn-chrome-extensions-force-installed-15-million-times

[Source](https://www.bleepingcomputer.com/news/security/fake-vpn-chrome-extensions-force-installed-15-million-times/){:target="_blank" rel="noopener"}

> Three malicious Chrome extensions posing as VPN (Virtual Private Networks) infected were downloaded 1.5 million times, acting as browser hijackers, cashback hack tools, and data stealers. [...]
