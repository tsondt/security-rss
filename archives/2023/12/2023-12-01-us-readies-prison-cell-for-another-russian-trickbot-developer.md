Title: US readies prison cell for another Russian Trickbot developer
Date: 2023-12-01T15:08:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-12-01-us-readies-prison-cell-for-another-russian-trickbot-developer

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/01/trickbot_dev_guilty_plea/){:target="_blank" rel="noopener"}

> Hunt continues for the other elusive high-ranking members Another member of the Trickbot malware crew now faces a lengthy prison sentence amid US law enforcement's ongoing search for its leading members.... [...]
