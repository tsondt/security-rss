Title: Access AWS using a Google Cloud Platform native workload identity
Date: 2023-12-22T14:25:16+00:00
Author: Simran Singh
Category: AWS Security
Tags: Intermediate (200);Multicloud;Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2023-12-22-access-aws-using-a-google-cloud-platform-native-workload-identity

[Source](https://aws.amazon.com/blogs/security/access-aws-using-a-google-cloud-platform-native-workload-identity/){:target="_blank" rel="noopener"}

> Organizations undergoing cloud migrations and business transformations often find themselves managing IT operations in hybrid or multicloud environments. This can make it more complex to safeguard workloads, applications, and data, and to securely handle identities and permissions across Amazon Web Services (AWS), hybrid, and multicloud setups. In this post, we show you how to assume [...]
