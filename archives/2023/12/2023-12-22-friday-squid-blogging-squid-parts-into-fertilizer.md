Title: Friday Squid Blogging: Squid Parts into Fertilizer
Date: 2023-12-22T22:08:39+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-12-22-friday-squid-blogging-squid-parts-into-fertilizer

[Source](https://www.schneier.com/blog/archives/2023/12/friday-squid-blogging-squid-parts-into-fertilizer.html){:target="_blank" rel="noopener"}

> It’s squid parts from college dissections, so it’s not a volume operation. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
