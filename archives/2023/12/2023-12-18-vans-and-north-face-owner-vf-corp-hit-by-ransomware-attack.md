Title: Vans and North Face owner VF Corp hit by ransomware attack
Date: 2023-12-18T13:56:41-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2023-12-18-vans-and-north-face-owner-vf-corp-hit-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/vans-and-north-face-owner-vf-corp-hit-by-ransomware-attack/){:target="_blank" rel="noopener"}

> American global apparel and footwear giant VF Corporation, the owner of brands like Supreme, Vans, Timberland, and The North Face, has disclosed a security incident that caused operational disruptions. [...]
