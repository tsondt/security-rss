Title: Ten new Android banking trojans targeted 985 bank apps in 2023
Date: 2023-12-14T14:40:02-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-12-14-ten-new-android-banking-trojans-targeted-985-bank-apps-in-2023

[Source](https://www.bleepingcomputer.com/news/security/ten-new-android-banking-trojans-targeted-985-bank-apps-in-2023/){:target="_blank" rel="noopener"}

> This year has seen the emergence of ten new Android banking malware families, which collectively target 985 bank and fintech/trading apps from financial institutes across 61 countries. [...]
