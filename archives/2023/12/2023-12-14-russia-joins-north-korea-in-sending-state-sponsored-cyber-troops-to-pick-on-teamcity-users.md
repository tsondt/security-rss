Title: Russia joins North Korea in sending state-sponsored cyber troops to pick on TeamCity users
Date: 2023-12-14T14:12:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-12-14-russia-joins-north-korea-in-sending-state-sponsored-cyber-troops-to-pick-on-teamcity-users

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/14/russia_joins_north_korea_cybercity/){:target="_blank" rel="noopener"}

> National security and infosec authorities band together to help victims sniff out stealthy Russian baddies hiding in networks Updated The offensive cyber unit linked to Russia's Foreign Intelligence Service (SVR) is exploiting the critical vulnerability affecting the JetBrains TeamCity CI/CD server at scale, and has been since September, authorities warn.... [...]
