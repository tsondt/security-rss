Title: December Android updates fix critical zero-click RCE flaw
Date: 2023-12-04T14:37:38-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-04-december-android-updates-fix-critical-zero-click-rce-flaw

[Source](https://www.bleepingcomputer.com/news/security/december-android-updates-fix-critical-zero-click-rce-flaw/){:target="_blank" rel="noopener"}

> Google announced today that the December 2023 Android security updates tackle 85 vulnerabilities, including a critical severity zero-click remote code execution (RCE) bug. [...]
