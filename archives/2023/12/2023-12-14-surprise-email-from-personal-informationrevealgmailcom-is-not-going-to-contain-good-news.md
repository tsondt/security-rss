Title: Surprise! Email from personal.
<br />
information.reveal@gmail.com is not going to contain good news
Date: 2023-12-14T09:55:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-14-surprise-email-from-personal-informationrevealgmailcom-is-not-going-to-contain-good-news

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/14/karakurt_defense_advice/){:target="_blank" rel="noopener"}

> Internet plod highlight tactics used by cruel Karakurt crime gang Karakurt, a particularly nasty extortion gang that uses "extensive harassment" to pressure victims into handing over millions of dollars in ransom payments after compromising their IT infrastructure, pose a "significant challenge" for network defenders, we're told.... [...]
