Title: Just about every Windows and Linux device vulnerable to new LogoFAIL firmware attack
Date: 2023-12-06T15:02:41+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Features;Security;bootkits;firmware;logofail;malware;uefi;united extensible firmware interface
Slug: 2023-12-06-just-about-every-windows-and-linux-device-vulnerable-to-new-logofail-firmware-attack

[Source](https://arstechnica.com/?p=1988975){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Hundreds of Windows and Linux computer models from virtually all hardware makers are vulnerable to a new attack that executes malicious firmware early in the boot-up sequence, a feat that allows infections that are nearly impossible to detect or remove using current defense mechanisms. The attack—dubbed LogoFAIL by the researchers who devised it—is notable for the relative ease in carrying it out, the breadth of both consumer- and enterprise-grade models that are susceptible, and the high level of control it gains over them. In many cases, LogoFAIL can be remotely executed in post-exploit situations using techniques [...]
