Title: Fake F5 BIG-IP zero-day warning emails push data wipers
Date: 2023-12-20T16:52:42-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-12-20-fake-f5-big-ip-zero-day-warning-emails-push-data-wipers

[Source](https://www.bleepingcomputer.com/news/security/fake-f5-big-ip-zero-day-warning-emails-push-data-wipers/){:target="_blank" rel="noopener"}

> The Israel National Cyber Directorate warns of phishing emails pretending to be F5 BIG-IP zero-day security updates that deploy Windows and Linux data wipers. [...]
