Title: LockBit ransomware now poaching BlackCat, NoEscape affiliates
Date: 2023-12-13T13:22:58-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-12-13-lockbit-ransomware-now-poaching-blackcat-noescape-affiliates

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-now-poaching-blackcat-noescape-affiliates/){:target="_blank" rel="noopener"}

> The LockBit ransomware operation is now recruiting affiliates and developers from the BlackCat/ALPHV and NoEscape after recent disruptions and exit scams. [...]
