Title: The password attacks of 2023: Lessons learned and next steps
Date: 2023-12-20T10:02:04-05:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-12-20-the-password-attacks-of-2023-lessons-learned-and-next-steps

[Source](https://www.bleepingcomputer.com/news/security/the-password-attacks-of-2023-lessons-learned-and-next-steps/){:target="_blank" rel="noopener"}

> The password attacks of 2023 involved numerous high-profile brands, leading to the exposure of millions of users' data. Learn more from Specops Software on how to respond to these types of attacks. [...]
