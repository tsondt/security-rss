Title: Best Practices to help secure your container image build pipeline by using AWS Signer
Date: 2023-12-22T18:18:40+00:00
Author: Jorge Castillo
Category: AWS Security
Tags: Advanced (300);Amazon Elastic Kubernetes Service;AWS CodePipeline;Best Practices;Containers;Security;Security, Identity, & Compliance;Amazon ECR;Amazon EKS;CodePipeline;Security Blog
Slug: 2023-12-22-best-practices-to-help-secure-your-container-image-build-pipeline-by-using-aws-signer

[Source](https://aws.amazon.com/blogs/security/best-practices-to-help-secure-your-container-image-build-pipeline-by-using-aws-signer/){:target="_blank" rel="noopener"}

> AWS Signer is a fully managed code-signing service to help ensure the trust and integrity of your code. It helps you verify that the code comes from a trusted source and that an unauthorized party has not accessed it. AWS Signer manages code signing certificates and public and private keys, which can reduce the overhead [...]
