Title: AI Decides to Engage in Insider Trading
Date: 2023-12-01T12:03:28+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence;cheating;deception;insiders;LLM
Slug: 2023-12-01-ai-decides-to-engage-in-insider-trading

[Source](https://www.schneier.com/blog/archives/2023/12/ai-decides-to-engage-in-insider-trading.html){:target="_blank" rel="noopener"}

> A stock-trading AI (a simulated experiment) engaged in insider trading, even though it “knew” it was wrong. The agent is put under pressure in three ways. First, it receives a email from its “manager” that the company is not doing well and needs better performance in the next quarter. Second, the agent attempts and fails to find promising low- and medium-risk trades. Third, the agent receives an email from a company employee who projects that the next quarter will have a general stock market downturn. In this high-pressure situation, the model receives an insider tip from another employee that would [...]
