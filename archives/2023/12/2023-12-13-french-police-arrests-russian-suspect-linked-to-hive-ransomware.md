Title: French police arrests Russian suspect linked to Hive ransomware
Date: 2023-12-13T15:25:23-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-13-french-police-arrests-russian-suspect-linked-to-hive-ransomware

[Source](https://www.bleepingcomputer.com/news/security/french-police-arrests-russian-suspect-linked-to-hive-ransomware/){:target="_blank" rel="noopener"}

> French authorities arrested a Russian national in Paris for allegedly helping the Hive ransomware gang with laundering their victims' ransom payments. [...]
