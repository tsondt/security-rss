Title: Hacktivists boast: We shut down Iran's gas pumps today
Date: 2023-12-18T22:45:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-18-hacktivists-boast-we-shut-down-irans-gas-pumps-today

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/18/hacktivists_shut_down_irans_petrol/){:target="_blank" rel="noopener"}

> Predatory Sparrow previously knocked out railways and a steel plant Hacktivists reportedly disrupted services at about 70 percent of Iran's gas stations in a politically motivated cyberattack.... [...]
