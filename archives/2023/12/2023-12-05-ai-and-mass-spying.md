Title: AI and Mass Spying
Date: 2023-12-05T12:10:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;espionage;privacy;surveillance
Slug: 2023-12-05-ai-and-mass-spying

[Source](https://www.schneier.com/blog/archives/2023/12/ai-and-mass-spying.html){:target="_blank" rel="noopener"}

> Spying and surveillance are different but related things. If I hired a private detective to spy on you, that detective could hide a bug in your home or car, tap your phone, and listen to what you said. At the end, I would get a report of all the conversations you had and the contents of those conversations. If I hired that same private detective to put you under surveillance, I would get a different report: where you went, whom you talked to, what you purchased, what you did. Before the internet, putting someone under surveillance was expensive and time-consuming. [...]
