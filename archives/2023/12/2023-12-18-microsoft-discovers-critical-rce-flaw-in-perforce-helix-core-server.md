Title: Microsoft discovers critical RCE flaw in Perforce Helix Core Server
Date: 2023-12-18T15:49:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-18-microsoft-discovers-critical-rce-flaw-in-perforce-helix-core-server

[Source](https://www.bleepingcomputer.com/news/security/microsoft-discovers-critical-rce-flaw-in-perforce-helix-core-server/){:target="_blank" rel="noopener"}

> Four vulnerabilities, one of which is rated critical, have been discovered in the Perforce Helix Core Server, a source code management platform widely used by the gaming, government, military, and technology sectors. [...]
