Title: 23andMe updates user agreement to prevent data breach lawsuits
Date: 2023-12-07T15:40:20-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-12-07-23andme-updates-user-agreement-to-prevent-data-breach-lawsuits

[Source](https://www.bleepingcomputer.com/news/security/23andme-updates-user-agreement-to-prevent-data-breach-lawsuits/){:target="_blank" rel="noopener"}

> As Genetic testing provider 23andMe faces multiple lawsuits for an October credential stuffing attack that led to the theft of customer data, the company has modified its Terms of Use to make it harder to sue the company. [...]
