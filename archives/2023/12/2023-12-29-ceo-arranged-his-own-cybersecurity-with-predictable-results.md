Title: CEO arranged his own cybersecurity, with predictable results
Date: 2023-12-29T08:01:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-12-29-ceo-arranged-his-own-cybersecurity-with-predictable-results

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/29/on_call/){:target="_blank" rel="noopener"}

> Cleaning up after hackers is easy compared to surviving the politics of consultancy On Call It’s the last Friday of 2023, but because the need for tech support never goes away neither does On Call, The Register ’s Friday column in which readers share their tales of being asked to fix the unfeasible, in circumstances that are often indefensible.... [...]
