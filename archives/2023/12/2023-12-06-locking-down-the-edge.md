Title: Locking down the edge
Date: 2023-12-06T16:09:38+00:00
Author: Guy Matthews
Category: The Register
Tags: 
Slug: 2023-12-06-locking-down-the-edge

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/06/locking_down_the_edge/){:target="_blank" rel="noopener"}

> Watch this webinar to find out how Zero Trust fits into the edge security ecosystem Sponsored Post Edge security is a growing headache. The attack surface is expanding as more operational functions migrate out of centralized locations and into distributed sites and devices.... [...]
