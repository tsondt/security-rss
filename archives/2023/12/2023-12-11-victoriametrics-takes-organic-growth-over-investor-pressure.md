Title: VictoriaMetrics takes organic growth over investor pressure
Date: 2023-12-11T10:15:09+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-12-11-victoriametrics-takes-organic-growth-over-investor-pressure

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/11/victoriametrics_interview/){:target="_blank" rel="noopener"}

> Keeping the lights on with an enterprise product while staying true to your roots Interview Monitoring biz VictoriaMetrics is relatively unusual in its field. It is yet to accept external investment, preferring instead to try to grow organically rather than being forced to through a private equity meat grinder by committing to grow by X every year until the investor exits.... [...]
