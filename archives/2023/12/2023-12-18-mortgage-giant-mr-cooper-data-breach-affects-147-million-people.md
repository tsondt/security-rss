Title: Mortgage giant Mr. Cooper data breach affects 14.7 million people
Date: 2023-12-18T08:40:30-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-18-mortgage-giant-mr-cooper-data-breach-affects-147-million-people

[Source](https://www.bleepingcomputer.com/news/security/mortgage-giant-mr-cooper-data-breach-affects-147-million-people/){:target="_blank" rel="noopener"}

> Mr. Cooper is sending notices of a data breach to customers who were impacted by a cyberattack the firm suffered in November 2023. [...]
