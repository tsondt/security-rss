Title: BazarCall attacks abuse Google Forms to legitimize phishing emails
Date: 2023-12-13T15:34:36-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2023-12-13-bazarcall-attacks-abuse-google-forms-to-legitimize-phishing-emails

[Source](https://www.bleepingcomputer.com/news/security/bazarcall-attacks-abuse-google-forms-to-legitimize-phishing-emails/){:target="_blank" rel="noopener"}

> A new wave of BazarCall attacks uses Google Forms to generate and send payment receipts to victims, attempting to make the phishing attempt appear more legitimate. [...]
