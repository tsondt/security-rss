Title: Microsoft deprecates Defender Application Guard for some Edge users
Date: 2023-12-21T13:10:24-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-12-21-microsoft-deprecates-defender-application-guard-for-some-edge-users

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-deprecates-defender-application-guard-for-some-edge-users/){:target="_blank" rel="noopener"}

> Microsoft is deprecating Defender Application Guard (including the Windows Isolated App Launcher APIs) for Edge for Business users. [...]
