Title: ‘Wall of Flippers’ detects Flipper Zero Bluetooth spam attacks
Date: 2023-12-23T10:09:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-23-wall-of-flippers-detects-flipper-zero-bluetooth-spam-attacks

[Source](https://www.bleepingcomputer.com/news/security/wall-of-flippers-detects-flipper-zero-bluetooth-spam-attacks/){:target="_blank" rel="noopener"}

> A new Python project called 'Wall of Flippers' detects Bluetooth spam attacks launched by Flipper Zero and Android devices. [...]
