Title: Pro-China campaign targeted YouTube with AI avatars
Date: 2023-12-18T01:06:24+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-12-18-pro-china-campaign-targeted-youtube-with-ai-avatars

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/18/asia_in_brief/){:target="_blank" rel="noopener"}

> PLUS: Beijing wants ten-minute reporting of infosec incidents; Infosys CFO bails; TikTok's Indonesia comeback approved, for now Think tank Australian Strategic Policy Institute (ASPI) last week published details of a campaign that spreads English language pro-China and anti-US narratives on YouTube.... [...]
