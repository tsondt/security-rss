Title: BidenCash darkweb market gives 1.9 million credit cards for free
Date: 2023-12-21T10:40:07-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-21-bidencash-darkweb-market-gives-19-million-credit-cards-for-free

[Source](https://www.bleepingcomputer.com/news/security/bidencash-darkweb-market-gives-19-million-credit-cards-for-free/){:target="_blank" rel="noopener"}

> The BidenCash stolen credit card marketplace is giving away 1.9 million credit cards for free via its store to promote itself among cybercriminals. [...]
