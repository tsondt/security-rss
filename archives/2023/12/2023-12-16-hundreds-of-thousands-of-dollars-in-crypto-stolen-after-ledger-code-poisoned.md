Title: Hundreds of thousands of dollars in crypto stolen after Ledger code poisoned
Date: 2023-12-16T00:13:24+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-12-16-hundreds-of-thousands-of-dollars-in-crypto-stolen-after-ledger-code-poisoned

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/16/ledger_crypto_conect_kit/){:target="_blank" rel="noopener"}

> NPM repo hijacked after former worker phished Cryptocurrency wallet maker Ledger says someone slipped malicious code into one of its JavaScript libraries to steal more than half a million dollars from victims.... [...]
