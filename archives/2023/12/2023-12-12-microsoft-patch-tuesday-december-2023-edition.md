Title: Microsoft Patch Tuesday, December 2023 Edition
Date: 2023-12-12T22:21:00+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;The Coming Storm;Time to Patch;CVE-2023-35628;CVE-2023-35636;CVE-2023-35641;Immersive Labs;Kevin Breen;Microsoft Patch Tuesday December 2023;Satnam Narang;Tenable
Slug: 2023-12-12-microsoft-patch-tuesday-december-2023-edition

[Source](https://krebsonsecurity.com/2023/12/microsoft-patch-tuesday-december-2023-edition/){:target="_blank" rel="noopener"}

> The final Patch Tuesday of 2023 is upon us, with Microsoft Corp. today releasing fixes for a relatively small number of security holes in its Windows operating systems and other software. Even more unusual, there are no known “zero-day” threats targeting any of the vulnerabilities in December’s patch batch. Still, four of the updates pushed out today address “critical” vulnerabilities that Microsoft says can be exploited by malware or malcontents to seize complete control over a vulnerable Windows device with little or no help from users. Among the critical bugs quashed this month is CVE-2023-35628, a weakness present in Windows [...]
