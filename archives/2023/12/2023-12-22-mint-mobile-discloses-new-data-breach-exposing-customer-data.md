Title: Mint Mobile discloses new data breach exposing customer data
Date: 2023-12-22T20:35:18-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-12-22-mint-mobile-discloses-new-data-breach-exposing-customer-data

[Source](https://www.bleepingcomputer.com/news/security/mint-mobile-discloses-new-data-breach-exposing-customer-data/){:target="_blank" rel="noopener"}

> Mint Mobile has disclosed a new data breach that exposed the personal information of its customers, including data that can be used to perform SIM swap attacks. [...]
