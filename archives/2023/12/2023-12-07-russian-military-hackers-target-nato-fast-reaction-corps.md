Title: Russian military hackers target NATO fast reaction corps
Date: 2023-12-07T17:20:21-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-07-russian-military-hackers-target-nato-fast-reaction-corps

[Source](https://www.bleepingcomputer.com/news/security/russian-military-hackers-target-nato-fast-reaction-corps/){:target="_blank" rel="noopener"}

> Russian APT28 military hackers used Microsoft Outlook zero-day exploits to target multiple European NATO member countries, including a NATO Rapid Deployable Corps. [...]
