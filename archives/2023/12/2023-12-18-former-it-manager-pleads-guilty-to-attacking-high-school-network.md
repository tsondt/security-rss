Title: Former IT manager pleads guilty to attacking high school network
Date: 2023-12-18T10:00:06-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-12-18-former-it-manager-pleads-guilty-to-attacking-high-school-network

[Source](https://www.bleepingcomputer.com/news/security/former-it-manager-pleads-guilty-to-attacking-high-school-network/){:target="_blank" rel="noopener"}

> Conor LaHiff, a former IT manager for a New Jersey public high school, has admitted to committing a cyberattack against his former employer following the termination of his employment in June 2023. [...]
