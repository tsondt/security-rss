Title: US senator: Govts spy on Apple, Google users via mobile notifications
Date: 2023-12-06T14:58:06-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Apple;Google;Government
Slug: 2023-12-06-us-senator-govts-spy-on-apple-google-users-via-mobile-notifications

[Source](https://www.bleepingcomputer.com/news/security/us-senator-govts-spy-on-apple-google-users-via-mobile-notifications/){:target="_blank" rel="noopener"}

> A U.S. senator revealed today that government agencies worldwide demand mobile push notification records from Apple and Google users to spy on their customers. [...]
