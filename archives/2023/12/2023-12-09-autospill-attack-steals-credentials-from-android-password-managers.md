Title: AutoSpill attack steals credentials from Android password managers
Date: 2023-12-09T10:14:24-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2023-12-09-autospill-attack-steals-credentials-from-android-password-managers

[Source](https://www.bleepingcomputer.com/news/security/autospill-attack-steals-credentials-from-android-password-managers/){:target="_blank" rel="noopener"}

> Security researchers developed a new attack, which they named AutoSpill, to steal account credentials on Android during the autofill operation. [...]
