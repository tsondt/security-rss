Title: Apple slaps patch on WebKit holes in iPhones and Macs amid fears of active attacks
Date: 2023-12-01T21:31:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-01-apple-slaps-patch-on-webkit-holes-in-iphones-and-macs-amid-fears-of-active-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/01/iphones_macs_patch/){:target="_blank" rel="noopener"}

> Two CVEs can be abused to steal sensitive info or execute code Apple has issued emergency fixes to plug security flaws in iPhones, iPads, and Macs that may already be under attack.... [...]
