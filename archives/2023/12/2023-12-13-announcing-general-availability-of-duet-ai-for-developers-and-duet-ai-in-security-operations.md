Title: Announcing General Availability of Duet AI for Developers and Duet AI in Security Operations
Date: 2023-12-13T15:00:00+00:00
Author: Brad Calder
Category: GCP Security
Tags: Application Development;Security & Identity;AI & Machine Learning
Slug: 2023-12-13-announcing-general-availability-of-duet-ai-for-developers-and-duet-ai-in-security-operations

[Source](https://cloud.google.com/blog/products/ai-machine-learning/duet-ai-for-developers-and-in-security-operations-now-ga/){:target="_blank" rel="noopener"}

> At Google Cloud, we are infusing the power of AI throughout our products and solutions to build AI services that can help all of our users. Duet AI in Google Cloud provides you with AI-powered collaborators to boost your productivity, gain competitive insights, and improve your bottom line. Today, we are announcing that Duet AI for Developers and Duet AI in Security Operations are now generally available (GA). These offerings join Duet AI in Google Workspace, which has been generally available since August. All of our Duet AI services will be incorporating Gemini, our most capable model, over the next [...]
