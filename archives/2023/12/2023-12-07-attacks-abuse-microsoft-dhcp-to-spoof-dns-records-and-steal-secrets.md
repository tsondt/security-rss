Title: Attacks abuse Microsoft DHCP to spoof DNS records and steal secrets
Date: 2023-12-07T22:11:22+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-07-attacks-abuse-microsoft-dhcp-to-spoof-dns-records-and-steal-secrets

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/07/attacks_abuse_microsoft_dhcp/){:target="_blank" rel="noopener"}

> Akamai says it reported the flaws to Microsoft. Redmond shrugged A series of attacks against Microsoft Active Directory domains could allow miscreants to spoof DNS records, compromise Active Directory and steal all the secrets it stores, according to Akamai security researchers.... [...]
