Title: Meta rolls out default end-to-end encryption on Messenger, Facebook
Date: 2023-12-07T09:27:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-07-meta-rolls-out-default-end-to-end-encryption-on-messenger-facebook

[Source](https://www.bleepingcomputer.com/news/security/meta-rolls-out-default-end-to-end-encryption-on-messenger-facebook/){:target="_blank" rel="noopener"}

> Meta has announced that the immediate availability of end-to-end encryption for all chats and calls made through the Messenger app, as well as the Facebook social media platform. [...]
