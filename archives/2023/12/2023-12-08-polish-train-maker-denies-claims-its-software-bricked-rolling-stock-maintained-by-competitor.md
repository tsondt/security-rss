Title: Polish train maker denies claims its software bricked rolling stock maintained by competitor
Date: 2023-12-08T06:30:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-12-08-polish-train-maker-denies-claims-its-software-bricked-rolling-stock-maintained-by-competitor

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/08/polish_trains_geofenced_allegation/){:target="_blank" rel="noopener"}

> Says it was probably hacked, which isn't good news either A trio of Polish security researchers claim to have found that trains built by Newag SA contain software that sabotages them if the hardware is serviced by competitors.... [...]
