Title: ICANN Launches Service to Help With WHOIS Lookups
Date: 2023-12-06T15:51:58+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Security Tools;Catalin Cimpanu;GDPR;ICANN;Internet Corporation for Assigned Names and Numbers;RDRS;Registration Data Request Service
Slug: 2023-12-06-icann-launches-service-to-help-with-whois-lookups

[Source](https://krebsonsecurity.com/2023/12/icann-launches-service-to-help-with-whois-lookups/){:target="_blank" rel="noopener"}

> More than five years after domain name registrars started redacting personal data from all public domain registration records, the non-profit organization overseeing the domain industry has introduced a centralized online service designed to make it easier for researchers, law enforcement and others to request the information directly from registrars. In May 2018, the Internet Corporation for Assigned Names and Numbers (ICANN) — the nonprofit entity that manages the global domain name system — instructed all registrars to redact the customer’s name, address, phone number and email from WHOIS, the system for querying databases that store the registered users of domain [...]
