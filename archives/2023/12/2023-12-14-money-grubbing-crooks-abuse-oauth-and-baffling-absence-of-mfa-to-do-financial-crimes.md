Title: Money-grubbing crooks abuse OAuth – and baffling absence of MFA – to do financial crimes
Date: 2023-12-14T11:03:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-14-money-grubbing-crooks-abuse-oauth-and-baffling-absence-of-mfa-to-do-financial-crimes

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/14/moneygrubbing_crooks_abuse_oauth_apps/){:target="_blank" rel="noopener"}

> Business email compromise, illicit cryptomining, phishing... if it makes a dollar, this lot do it Multiple miscreants are misusing OAuth to automate financially motivated cyber crimes – such as business email compromise (BEC), phishing, large-scale spamming campaigns – and deploying virtual machines to illicitly mine for cryptocurrencies, according to Microsoft.... [...]
