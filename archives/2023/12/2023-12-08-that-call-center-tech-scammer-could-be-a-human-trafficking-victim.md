Title: That call center tech scammer could be a human trafficking victim
Date: 2023-12-08T15:25:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-12-08-that-call-center-tech-scammer-could-be-a-human-trafficking-victim

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/08/human_trafficking_for_cyber_scam/){:target="_blank" rel="noopener"}

> Interpol increasingly concerned as abject abuse of victims scales far beyond Asia origins Human trafficking for the purposes of populating cyber scam call centers is expanding beyond southeast Asia, where the crime was previously isolated.... [...]
