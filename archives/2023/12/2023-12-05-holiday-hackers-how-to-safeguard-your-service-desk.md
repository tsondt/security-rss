Title: Holiday Hackers: How to Safeguard Your Service Desk
Date: 2023-12-05T10:02:01-05:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-12-05-holiday-hackers-how-to-safeguard-your-service-desk

[Source](https://www.bleepingcomputer.com/news/security/holiday-hackers-how-to-safeguard-your-service-desk/){:target="_blank" rel="noopener"}

> Consumer traffic rises sharply during the holidays, as do the scope and severity of cyberattacks. Learn more from Specops Software on how to protect your service or help desk from social engineering attacks during the holiday season. [...]
