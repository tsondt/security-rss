Title: Ubisoft says it's investigating reports of a new security breach
Date: 2023-12-22T13:10:25-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-12-22-ubisoft-says-its-investigating-reports-of-a-new-security-breach

[Source](https://www.bleepingcomputer.com/news/security/ubisoft-says-its-investigating-reports-of-a-new-security-breach/){:target="_blank" rel="noopener"}

> Ubisoft is investigating whether it suffered a breach after images of the company's internal software and developer tools were leaked online. [...]
