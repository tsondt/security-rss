Title: Exposed Hugging Face API tokens offered full access to Meta's Llama 2
Date: 2023-12-04T14:00:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-12-04-exposed-hugging-face-api-tokens-offered-full-access-to-metas-llama-2

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/04/exposed_hugging_face_api_tokens/){:target="_blank" rel="noopener"}

> With more than 1,500 tokens exposed, research highlights importance of securing supply chains in AI and ML The API tokens of tech giants Meta, Microsoft, Google, VMware, and more have been found exposed on Hugging Face, opening them up to potential supply chain attacks.... [...]
