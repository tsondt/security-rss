Title: Norton Healthcare discloses data breach after May ransomware attack
Date: 2023-12-08T18:28:18-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-12-08-norton-healthcare-discloses-data-breach-after-may-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/norton-healthcare-discloses-data-breach-after-may-ransomware-attack/){:target="_blank" rel="noopener"}

> Kentucky health system Norton Healthcare has confirmed that a ransomware attack in May exposed personal information belonging to patients, employees, and dependents. [...]
