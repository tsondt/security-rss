Title: Xfinity discloses data breach affecting over 35 million people
Date: 2023-12-18T19:03:30-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-18-xfinity-discloses-data-breach-affecting-over-35-million-people

[Source](https://www.bleepingcomputer.com/news/security/xfinity-discloses-data-breach-affecting-over-35-million-people/){:target="_blank" rel="noopener"}

> Comcast Cable Communications, doing business as Xfinity, disclosed on Monday that attackers who breached one of its Citrix servers in October also stole customer-sensitive information from its systems. [...]
