Title: Ubiquiti users report having access to others’ UniFi routers, cameras
Date: 2023-12-14T15:38:21-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Technology
Slug: 2023-12-14-ubiquiti-users-report-having-access-to-others-unifi-routers-cameras

[Source](https://www.bleepingcomputer.com/news/security/ubiquiti-users-report-having-access-to-others-unifi-routers-cameras/){:target="_blank" rel="noopener"}

> Since yesterday, customers of Ubiquiti networking devices, ranging from routers to security cameras, have reported seeing other people's devices and notifications through the company's cloud services. [...]
