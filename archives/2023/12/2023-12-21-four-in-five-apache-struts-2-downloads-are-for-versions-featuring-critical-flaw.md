Title: Four in five Apache Struts 2 downloads are for versions featuring critical flaw
Date: 2023-12-21T14:13:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-12-21-four-in-five-apache-struts-2-downloads-are-for-versions-featuring-critical-flaw

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/21/apache_struts_vulnerable_downloads/){:target="_blank" rel="noopener"}

> Seriously, people - please check the stuff you fetch more carefully Security vendor Sonatype believes developers are failing to address the critical remote code execution (RCE) vulnerability in the Apache Struts 2 framework, based on recent downloads of the code.... [...]
