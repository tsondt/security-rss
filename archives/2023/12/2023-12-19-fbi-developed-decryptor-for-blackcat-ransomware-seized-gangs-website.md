Title: FBI developed decryptor for BlackCat ransomware, seized gang's website
Date: 2023-12-19T14:59:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-12-19-fbi-developed-decryptor-for-blackcat-ransomware-seized-gangs-website

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/19/blackcat_domain_seizure/){:target="_blank" rel="noopener"}

> Crims laugh it off and resume their activity The FBI created a decryption tool for the ransomware used by the gang known as BlackCat and/or AlphV, as part of a wider disruption campaign against the group.... [...]
