Title: CISA urges tech manufacturers to stop using default passwords
Date: 2023-12-15T14:01:45-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-15-cisa-urges-tech-manufacturers-to-stop-using-default-passwords

[Source](https://www.bleepingcomputer.com/news/security/cisa-urges-tech-manufacturers-to-stop-using-default-passwords/){:target="_blank" rel="noopener"}

> Today, the U.S. Cybersecurity and Infrastructure Security Agency (CISA) urged technology manufacturers to stop providing software and devices with default passwords. [...]
