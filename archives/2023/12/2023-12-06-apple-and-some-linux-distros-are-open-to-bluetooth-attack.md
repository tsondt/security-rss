Title: Apple and some Linux distros are open to Bluetooth attack
Date: 2023-12-06T20:47:32+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-06-apple-and-some-linux-distros-are-open-to-bluetooth-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/06/bluetooth_bug_apple_linux/){:target="_blank" rel="noopener"}

> Issue has been around since at least 2012 A years-old Bluetooth authentication bypass vulnerability allows miscreants to connect to Apple, Android and Linux devices and inject keystrokes to run arbitrary commands, according to a software engineer at drone technology firm SkySafe.... [...]
