Title: OpenAI Is Not Training on Your Dropbox Documents—Today
Date: 2023-12-19T12:09:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;data collection;national security policy;trust
Slug: 2023-12-19-openai-is-not-training-on-your-dropbox-documentstoday

[Source](https://www.schneier.com/blog/archives/2023/12/openai-is-not-training-on-your-dropbox-documents-today.html){:target="_blank" rel="noopener"}

> There’s a rumor flying around the Internet that OpenAI is training foundation models on your Dropbox documents. Here’s CNBC. Here’s Boing Boing. Some articles are more nuanced, but there’s still a lot of confusion. It seems not to be true. Dropbox isn’t sharing all of your documents with OpenAI. But here’s the problem: we don’t trust OpenAI. We don’t trust tech corporations. And—to be fair—corporations in general. We have no reason to. Simon Willison nails it in a tweet: “OpenAI are training on every piece of data they see, even when they say they aren’t” is the new “Facebook are [...]
