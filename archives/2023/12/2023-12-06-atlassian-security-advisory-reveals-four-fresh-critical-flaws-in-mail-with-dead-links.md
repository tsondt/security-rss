Title: Atlassian security advisory reveals four fresh critical flaws – in mail with dead links
Date: 2023-12-06T06:57:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-12-06-atlassian-security-advisory-reveals-four-fresh-critical-flaws-in-mail-with-dead-links

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/06/atlassian_four_rce_cves/){:target="_blank" rel="noopener"}

> Bitbucket, Confluence and Jira all in danger, again. Sigh Atlassian has emailed its customers to warn of four critical vulnerabilities, but the message had flaws of its own – the links it contained weren't live for all readers at the time of despatch.... [...]
