Title: New SLAM attack steals sensitive data from AMD, future Intel CPUs
Date: 2023-12-06T19:52:33-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-06-new-slam-attack-steals-sensitive-data-from-amd-future-intel-cpus

[Source](https://www.bleepingcomputer.com/news/security/new-slam-attack-steals-sensitive-data-from-amd-future-intel-cpus/){:target="_blank" rel="noopener"}

> Academic researchers developed a new side-channel attack called SLAM that exploits hardware features designed to improve security in upcoming CPUs from Intel, AMD, and Arm to obtain the root password hash from the kernel memory. [...]
