Title: Stealthy Linux rootkit found in the wild after going undetected for 2 years
Date: 2023-12-08T20:54:09+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;backdoor;Linux;malware;rootkits
Slug: 2023-12-08-stealthy-linux-rootkit-found-in-the-wild-after-going-undetected-for-2-years

[Source](https://arstechnica.com/?p=1989775){:target="_blank" rel="noopener"}

> Enlarge Stealthy and multifunctional Linux malware that has been infecting telecommunications companies went largely unnoticed for two years until being documented for the first time by researchers on Thursday. Researchers from security firm Group-IB have named the remote access trojan “Krasue,” after a nocturnal spirit depicted in Southeast Asian folklore “floating in mid-air, with no torso, just her intestines hanging from below her chin.” The researchers chose the name because evidence to date shows it almost exclusively targets victims in Thailand and “poses a severe risk to critical systems and sensitive data given that it is able to grant attackers [...]
