Title: Microsoft issues deadline for end of Windows 10 support – it's pay to play for security
Date: 2023-12-06T06:31:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-12-06-microsoft-issues-deadline-for-end-of-windows-10-support-its-pay-to-play-for-security

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/06/microsoft_windows_10_security/){:target="_blank" rel="noopener"}

> Limited options will be available into 2028, for an undisclosed price Microsoft on Tuesday warned that full security support for Windows 10 will end on October 14, 2025, but offered a lifeline for customers unable or unwilling to upgrade two years hence.... [...]
