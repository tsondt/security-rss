Title: Kali Linux 2023.4 released with GNOME 45 and 15 new tools
Date: 2023-12-05T14:11:37-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Linux
Slug: 2023-12-05-kali-linux-20234-released-with-gnome-45-and-15-new-tools

[Source](https://www.bleepingcomputer.com/news/security/kali-linux-20234-released-with-gnome-45-and-15-new-tools/){:target="_blank" rel="noopener"}

> Kali Linux 2023.4, the fourth and final version of 2023, is now available for download, with fifteen new tools and the GNOME 45 desktop environment. [...]
