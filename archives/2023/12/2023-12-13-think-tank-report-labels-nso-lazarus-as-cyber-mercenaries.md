Title: Think tank report labels NSO, Lazarus as 'cyber mercenaries'
Date: 2023-12-13T06:05:28+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-12-13-think-tank-report-labels-nso-lazarus-as-cyber-mercenaries

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/13/cyber_mercenary_orf_report/){:target="_blank" rel="noopener"}

> Sure, they do crimes. But the plausible deniability governments adore means they deserve a different label Cybercrime gangs like the notorious Lazarus group and spyware vendors like Israel's NSO should be considered cyber mercenaries – and become the subject of a concerted international response – according to a Monday report from Delhi-based think tank Observer Research Foundation (ORF).... [...]
