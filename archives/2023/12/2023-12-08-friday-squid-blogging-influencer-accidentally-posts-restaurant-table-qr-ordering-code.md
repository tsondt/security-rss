Title: Friday Squid Blogging: Influencer Accidentally Posts Restaurant Table QR Ordering Code
Date: 2023-12-08T22:03:06+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;operational security;restaurants;squid
Slug: 2023-12-08-friday-squid-blogging-influencer-accidentally-posts-restaurant-table-qr-ordering-code

[Source](https://www.schneier.com/blog/archives/2023/12/friday-squid-blogging-influencer-accidentally-posts-restaurant-table-qr-ordering-code.html){:target="_blank" rel="noopener"}

> Another rare security + squid story : The woman—who has only been identified by her surname, Wang—was having a meal with friends at a hotpot restaurant in Kunming, a city in southwest China. When everyone’s selections arrived at the table, she posted a photo of the spread on the Chinese social media platform WeChat. What she didn’t notice was that she’d included the QR code on her table, which the restaurant’s customers use to place their orders. Even though the photo was only shared with her WeChat friends list and not the entire social network, someone—or a lot of someones—used [...]
