Title: Hershey phishes! - Crooks snarf chocolate lovers' creds
Date: 2023-12-04T19:15:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-04-hershey-phishes-crooks-snarf-chocolate-lovers-creds

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/04/hershey_phishes_data_breach/){:target="_blank" rel="noopener"}

> Stealing Kit Kat maker's data?! Give me a break There's no sugarcoating this news: The Hershey Company has disclosed cyber crooks gobbled up 2,214 people's financial information following a phishing campaign that netted the chocolate maker's data.... [...]
