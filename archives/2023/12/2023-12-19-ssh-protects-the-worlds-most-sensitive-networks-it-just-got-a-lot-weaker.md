Title: SSH protects the world’s most sensitive networks. It just got a lot weaker
Date: 2023-12-19T17:35:09+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Features;Security;Uncategorized;authentication;encryption;prefix truncation attack;SSH
Slug: 2023-12-19-ssh-protects-the-worlds-most-sensitive-networks-it-just-got-a-lot-weaker

[Source](https://arstechnica.com/?p=1991880){:target="_blank" rel="noopener"}

> Enlarge / Terrapin is coming for your data. (credit: Aurich Lawson | Getty Images) Sometime around the start of 1995, an unknown person planted a password sniffer on the network backbone of Finland’s Helsinki University of Technology (now known as Aalto University). Once in place, this piece of dedicated hardware surreptitiously inhaled thousands of user names and passwords before it was finally discovered. Some of the credentials belonged to employees of a company run by Tatu Ylönen, who was also a database researcher at the university. The event proved to be seminal, not just for Ylönen's company but for the [...]
