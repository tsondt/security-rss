Title: Surveillance Cameras Disguised as Clothes Hooks
Date: 2023-12-14T16:23:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Amazon;cameras;courts;marketing;privacy;surveillance
Slug: 2023-12-14-surveillance-cameras-disguised-as-clothes-hooks

[Source](https://www.schneier.com/blog/archives/2023/12/surveillance-cameras-disguised-as-clothes-hooks.html){:target="_blank" rel="noopener"}

> This seems like a bad idea. And there are ongoing lawsuits against Amazon for selling them. [...]
