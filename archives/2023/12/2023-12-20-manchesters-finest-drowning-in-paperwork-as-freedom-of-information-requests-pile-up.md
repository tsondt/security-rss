Title: Manchester's finest drowning in paperwork as Freedom of Information requests pile up
Date: 2023-12-20T10:28:06+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-12-20-manchesters-finest-drowning-in-paperwork-as-freedom-of-information-requests-pile-up

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/20/greater_manchester_police_foi/){:target="_blank" rel="noopener"}

> Enforcement notice issued months after data regulator schooled police force Updated Greater Manchester Police (GMP) must clear the backlog of hundreds of Freedom of Information (FOI) Act requests – some years old – or find itself in contempt of court.... [...]
