Title: 23andMe responds to breach with new suit-limiting user terms
Date: 2023-12-11T11:46:05+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-12-11-23andme-responds-to-breach-with-new-suit-limiting-user-terms

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/11/in_brief_security/){:target="_blank" rel="noopener"}

> Also: 'well-known Bay Area tech' firm's laptops stolen and check out some critical vulns Security in brief The saga of 23andMe's mega data breach has reached something of a conclusion, with the company saying its probe has determined millions of leaked records originated from illicit break-ins into just 14,000 accounts.... [...]
