Title: Due to AI, “We are about to enter the era of mass spying,” says Bruce Schneier
Date: 2023-12-05T20:53:44+00:00
Author: Benj Edwards
Category: Ars Technica
Tags: AI;Biz & IT;AI ethics;AI safety;AI surveillance;bruce schneier;computer security;government;machine learning;spying;surveillance
Slug: 2023-12-05-due-to-ai-we-are-about-to-enter-the-era-of-mass-spying-says-bruce-schneier

[Source](https://arstechnica.com/?p=1988745){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images | Benj Edwards ) In an editorial for Slate published Monday, renowned security researcher Bruce Schneier warned that AI models may enable a new era of mass spying, allowing companies and governments to automate the process of analyzing and summarizing large volumes of conversation data, fundamentally lowering barriers to spying activities that currently require human labor. In the piece, Schneier notes that the existing landscape of electronic surveillance has already transformed the modern era, becoming the business model of the Internet, where our digital footprints are constantly tracked and analyzed for commercial reasons. Spying, by contrast, [...]
