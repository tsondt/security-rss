Title: Fall 2023 SOC reports now available in Spanish
Date: 2023-12-26T23:47:52+00:00
Author: Ryan Wilks
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS SOC Reports;Compliance;Security Blog;SOC
Slug: 2023-12-26-fall-2023-soc-reports-now-available-in-spanish

[Source](https://aws.amazon.com/blogs/security/fall-2023-soc-reports-now-available-in-spanish/){:target="_blank" rel="noopener"}

> Spanish version » We continue to listen to our customers, regulators, and stakeholders to understand their needs regarding audit, assurance, certification, and attestation programs at Amazon Web Services (AWS). We’re pleased to announce that the Fall 2023 System and Organization Controls (SOC) 1, SOC 2, and SOC 3 reports are now available in Spanish. These [...]
