Title: Kraft Heinz suggests we simmer down about Snatch ransomware attack claims
Date: 2023-12-15T19:59:55+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-15-kraft-heinz-suggests-we-simmer-down-about-snatch-ransomware-attack-claims

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/15/kraft_heinz_breach/){:target="_blank" rel="noopener"}

> Ah, beans The Kraft Heinz Company says its systems are all up and running as usual as it probes claims that some of its data was stolen by ransomware crooks.... [...]
