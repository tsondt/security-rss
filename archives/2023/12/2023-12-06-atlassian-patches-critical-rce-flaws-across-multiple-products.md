Title: Atlassian patches critical RCE flaws across multiple products
Date: 2023-12-06T10:49:36-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-06-atlassian-patches-critical-rce-flaws-across-multiple-products

[Source](https://www.bleepingcomputer.com/news/security/atlassian-patches-critical-rce-flaws-across-multiple-products/){:target="_blank" rel="noopener"}

> Atlassian has published security advisories for four critical remote code execution (RCE) vulnerabilities impacting Confluence, Jira, and Bitbucket servers, along with a companion app for macOS. [...]
