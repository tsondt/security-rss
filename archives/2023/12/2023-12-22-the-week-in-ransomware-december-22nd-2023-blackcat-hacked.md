Title: The Week in Ransomware - December 22nd 2023 - BlackCat hacked
Date: 2023-12-22T16:20:53-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-12-22-the-week-in-ransomware-december-22nd-2023-blackcat-hacked

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-december-22nd-2023-blackcat-hacked/){:target="_blank" rel="noopener"}

> Earlier this month, the BlackCat/ALPHV ransomware operation suffered a five-day disruption to their Tor data leak and negotiation sites, rumored to be caused by a law enforcement action. [...]
