Title: Microsoft seizes websites used to sell phony email accounts to Scattered Spider and other crims
Date: 2023-12-14T21:54:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-14-microsoft-seizes-websites-used-to-sell-phony-email-accounts-to-scattered-spider-and-other-crims

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/14/microsoft_seizes_storm_1152_websites/){:target="_blank" rel="noopener"}

> That should solve the global cybercrime problem, right? Microsoft has taken down US-based infrastructure and websites used by a cybercrime group to sell fraudulent online accounts to other crooks including Scattered Spider, the infamous social-engineering and extortion crew that hacked two Las Vegas casinos over the summer.... [...]
