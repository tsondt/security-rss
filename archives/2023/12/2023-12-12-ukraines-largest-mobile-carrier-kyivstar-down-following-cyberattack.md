Title: Ukraine's largest mobile carrier Kyivstar down following cyberattack
Date: 2023-12-12T10:46:39-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-12-12-ukraines-largest-mobile-carrier-kyivstar-down-following-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/ukraines-largest-mobile-carrier-kyivstar-down-following-cyberattack/){:target="_blank" rel="noopener"}

> Kyivstar, Ukraine's largest telecommunications service provider serving over 25 million mobile and home internet subscribers, has suffered a cyberattack impacting mobile and data services. [...]
