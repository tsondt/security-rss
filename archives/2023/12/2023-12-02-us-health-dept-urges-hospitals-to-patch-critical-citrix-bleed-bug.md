Title: US Health Dept urges hospitals to patch critical Citrix Bleed bug
Date: 2023-12-02T10:09:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-12-02-us-health-dept-urges-hospitals-to-patch-critical-citrix-bleed-bug

[Source](https://www.bleepingcomputer.com/news/security/us-health-dept-urges-hospitals-to-patch-critical-citrix-bleed-bug/){:target="_blank" rel="noopener"}

> The U.S. Department of Health and Human Services (HHS) warned hospitals this week to patch the critical 'Citrix Bleed' Netscaler vulnerability actively exploited in attacks. [...]
