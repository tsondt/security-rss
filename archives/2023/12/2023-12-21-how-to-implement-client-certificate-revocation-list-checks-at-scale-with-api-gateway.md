Title: How to implement client certificate revocation list checks at scale with API Gateway
Date: 2023-12-21T14:21:01+00:00
Author: Arthur Mnev
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;Amazon API Gateway;API Gateway;Certificates Revocation;CRL;OCSP;Security Blog;TLS
Slug: 2023-12-21-how-to-implement-client-certificate-revocation-list-checks-at-scale-with-api-gateway

[Source](https://aws.amazon.com/blogs/security/how-to-implement-client-certificate-revocation-list-checks-at-scale-with-api-gateway/){:target="_blank" rel="noopener"}

> As you design your Amazon API Gateway applications to rely on mutual certificate authentication (mTLS), you need to consider how your application will verify the revocation status of a client certificate. In your design, you should account for the performance and availability of your verification mechanism to make sure that your application endpoints perform reliably. [...]
