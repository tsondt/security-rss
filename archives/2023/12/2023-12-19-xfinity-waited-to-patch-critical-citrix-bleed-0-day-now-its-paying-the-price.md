Title: Xfinity waited to patch critical Citrix Bleed 0-day. Now it’s paying the price
Date: 2023-12-19T23:14:40+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;citrixbleed;Comcast;network breach;Xfinity
Slug: 2023-12-19-xfinity-waited-to-patch-critical-citrix-bleed-0-day-now-its-paying-the-price

[Source](https://arstechnica.com/?p=1992160){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images | Smith Collection/Gado ) Comcast waited as many as nine days to patch its network against a high-severity vulnerability, a lapse that allowed hackers to make off with password data and other sensitive information belonging to 36 million Xfinity customers. The breach, which was carried out by exploiting a vulnerability in network hardware sold by Citrix, gave hackers access to usernames and cryptographically hashed passwords for 35.9 million Xfinity customers, the cable TV and Internet provider said in a notification filed Monday with the Maine attorney general’s office. Citrix disclosed the vulnerability and issued a patch [...]
