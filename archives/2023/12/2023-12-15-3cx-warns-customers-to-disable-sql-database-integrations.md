Title: 3CX warns customers to disable SQL database integrations
Date: 2023-12-15T12:30:12-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-15-3cx-warns-customers-to-disable-sql-database-integrations

[Source](https://www.bleepingcomputer.com/news/security/3cx-warns-customers-to-disable-sql-database-integrations/){:target="_blank" rel="noopener"}

> VoIP communications company 3CX warned customers today to disable SQL Database integrations because of risks posed by what it describes as a potential vulnerability. [...]
