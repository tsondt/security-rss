Title: GTA 5 source code reportedly leaked online a year after RockStar hack
Date: 2023-12-25T13:27:56-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-12-25-gta-5-source-code-reportedly-leaked-online-a-year-after-rockstar-hack

[Source](https://www.bleepingcomputer.com/news/security/gta-5-source-code-reportedly-leaked-online-a-year-after-rockstar-hack/){:target="_blank" rel="noopener"}

> ​The source code for Grand Theft Auto 5 was reportedly leaked on Christmas Eve, a little over a year after the Lapsus$ threat actors hacked Rockstar games and stole corporate data. [...]
