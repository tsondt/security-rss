Title: Microsoft December 2023 Patch Tuesday fixes 34 flaws, 1 zero-day
Date: 2023-12-12T14:00:53-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-12-12-microsoft-december-2023-patch-tuesday-fixes-34-flaws-1-zero-day

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-december-2023-patch-tuesday-fixes-34-flaws-1-zero-day/){:target="_blank" rel="noopener"}

> Today is Microsoft's December 2023 Patch Tuesday, which includes security updates for a total of 34 flaws and one previously disclosed, unpatched vulnerability in AMD CPUs. [...]
