Title: Cloud engineer gets 2 years for wiping ex-employer’s code repos
Date: 2023-12-12T10:02:49-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-12-12-cloud-engineer-gets-2-years-for-wiping-ex-employers-code-repos

[Source](https://www.bleepingcomputer.com/news/security/cloud-engineer-gets-2-years-for-wiping-ex-employers-code-repos/){:target="_blank" rel="noopener"}

> Miklos Daniel Brody, a cloud engineer, was sentenced to two years in prison and a restitution of $529,000 for wiping the code repositories of his former employer in retaliation for being fired by the company. [...]
