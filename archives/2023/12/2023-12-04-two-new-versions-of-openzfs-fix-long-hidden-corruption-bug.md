Title: Two new versions of OpenZFS fix long-hidden corruption bug
Date: 2023-12-04T16:15:12+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2023-12-04-two-new-versions-of-openzfs-fix-long-hidden-corruption-bug

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/04/two_new_versions_of_openzfs/){:target="_blank" rel="noopener"}

> Version 2.2.2 and also 2.1.14, showing that this wasn't a new issue in the latest release The bug that was very occasionally corrupting data on file copies in OpenZFS 2.2.0 has been identified and fixed, and there's a fix for the previous OpenZFS release too.... [...]
