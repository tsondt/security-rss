Title: Microsoft: OAuth apps used to automate BEC and cryptomining attacks
Date: 2023-12-12T18:53:17-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-12-12-microsoft-oauth-apps-used-to-automate-bec-and-cryptomining-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-oauth-apps-used-to-automate-bec-and-cryptomining-attacks/){:target="_blank" rel="noopener"}

> Microsoft warns that financially-motivated threat actors are using OAuth applications to automate BEC and phishing attacks, push spam, and deploy VMs for cryptomining. [...]
