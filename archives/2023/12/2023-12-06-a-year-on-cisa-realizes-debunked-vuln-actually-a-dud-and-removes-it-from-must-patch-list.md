Title: A year on, CISA realizes debunked vuln actually a dud and removes it from must-patch list
Date: 2023-12-06T14:45:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-12-06-a-year-on-cisa-realizes-debunked-vuln-actually-a-dud-and-removes-it-from-must-patch-list

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/06/dud_cve_removed/){:target="_blank" rel="noopener"}

> Apparently no one thought to check if this D-Link router 'issue' was actually exploitable A security vulnerability previously added to CISA's Known Exploited Vulnerability catalog (KEV), which was recognized by CVE Numbering Authorities (CNA), and included in reputable threat reports is now being formally rejected by infosec organizations.... [...]
