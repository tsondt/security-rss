Title: Counter-Strike 2 HTML injection bug exposes players’ IP addresses
Date: 2023-12-11T15:05:29-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-12-11-counter-strike-2-html-injection-bug-exposes-players-ip-addresses

[Source](https://www.bleepingcomputer.com/news/security/counter-strike-2-html-injection-bug-exposes-players-ip-addresses/){:target="_blank" rel="noopener"}

> Valve has reportedly fixed an HTML injection flaw in CS2 that was heavily abused today to inject images into games and obtain other players' IP addresses. [...]
