Title: Fancy Bear goes phishing in US, European high-value networks
Date: 2023-12-06T00:15:15+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-06-fancy-bear-goes-phishing-in-us-european-high-value-networks

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/06/fancy_bear_phishing_microsoft/){:target="_blank" rel="noopener"}

> GRU-linked crew going after our code warns Microsoft - Outlook not good Fancy Bear, the Kremlin's cyber-spy crew, has been exploiting two previously patched bugs for large-scale phishing campaigns against high-value targets – like government, defense, and aerospace agencies in the US and Europe – since March, according to Microsoft.... [...]
