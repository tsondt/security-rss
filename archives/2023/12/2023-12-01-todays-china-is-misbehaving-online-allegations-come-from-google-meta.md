Title: Today's 'China is misbehaving online' allegations come from Google, Meta
Date: 2023-12-01T02:59:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-12-01-todays-china-is-misbehaving-online-allegations-come-from-google-meta

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/01/google_meta_allege_chiina_cyberattacks/){:target="_blank" rel="noopener"}

> Zuck boots propagandists, Big G finds surge of action directed at Taiwan Meta and Google have disclosed what they allege are offensive cyber ops conducted by China.... [...]
