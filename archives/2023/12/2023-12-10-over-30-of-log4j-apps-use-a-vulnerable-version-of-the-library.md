Title: Over 30% of Log4J apps use a vulnerable version of the library
Date: 2023-12-10T10:35:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-10-over-30-of-log4j-apps-use-a-vulnerable-version-of-the-library

[Source](https://www.bleepingcomputer.com/news/security/over-30-percent-of-log4j-apps-use-a-vulnerable-version-of-the-library/){:target="_blank" rel="noopener"}

> Roughly 38% of applications using the Apache Log4j library are using a version vulnerable to security issues, including Log4Shell, a critical vulnerability identified as CVE-2021-44228 that carries the maximum severity rating, despite patches being available for more than two years. [...]
