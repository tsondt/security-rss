Title: Cyber sleuths reveal how they infiltrate the biggest ransomware gangs
Date: 2023-12-22T15:55:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-12-22-cyber-sleuths-reveal-how-they-infiltrate-the-biggest-ransomware-gangs

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/22/how_to_infiltrate_ransomware_gangs/){:target="_blank" rel="noopener"}

> How do you break into the bad guys' ranks? Master the lingo and research, research, research Feature When AlphV/BlackCat's website went dark this month, it was like Chrimbo came early for cybersecurity defenders, some of whom seemingly believed law enforcement had busted one of the most menacing cyber criminal crews.... [...]
