Title: New iPhone Security Features to Protect Stolen Devices
Date: 2023-12-27T12:01:25+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;computer security;data protection;iPhone;theft
Slug: 2023-12-27-new-iphone-security-features-to-protect-stolen-devices

[Source](https://www.schneier.com/blog/archives/2023/12/new-iphone-security-features-to-protect-stolen-devices.html){:target="_blank" rel="noopener"}

> Apple is rolling out a new “Stolen Device Protection” feature that seems well thought out: When Stolen Device Protection is turned on, Face ID or Touch ID authentication is required for additional actions, including viewing passwords or passkeys stored in iCloud Keychain, applying for a new Apple Card, turning off Lost Mode, erasing all content and settings, using payment methods saved in Safari, and more. No passcode fallback is available in the event that the user is unable to complete Face ID or Touch ID authentication. For especially sensitive actions, including changing the password of the Apple ID account associated [...]
