Title: Ex-Amazon engineer pleads guilty to hacking crypto exchanges
Date: 2023-12-15T15:32:30-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-15-ex-amazon-engineer-pleads-guilty-to-hacking-crypto-exchanges

[Source](https://www.bleepingcomputer.com/news/security/ex-amazon-engineer-pleads-guilty-to-hacking-crypto-exchanges/){:target="_blank" rel="noopener"}

> Former Amazon security engineer Shakeeb Ahmed pleaded guilty this week to hacking and stealing over $12.3 million from two cryptocurrency exchanges in July 2022. [...]
