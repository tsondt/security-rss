Title: How the FBI seized BlackCat (ALPHV) ransomware’s servers
Date: 2023-12-19T12:27:04-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-12-19-how-the-fbi-seized-blackcat-alphv-ransomwares-servers

[Source](https://www.bleepingcomputer.com/news/security/how-the-fbi-seized-blackcat-alphv-ransomwares-servers/){:target="_blank" rel="noopener"}

> An unsealed FBI search warrant revealed how law enforcement hijacked the ALPHV/BlackCat ransomware operations websites and seized the associated URLs. [...]
