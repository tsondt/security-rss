Title: Mr Cooper cyberattack laid bare: 14.7M people's info stolen, costs hit $25M
Date: 2023-12-18T20:54:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-18-mr-cooper-cyberattack-laid-bare-147m-peoples-info-stolen-costs-hit-25m

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/18/mr_cooper_breach_disclosure/){:target="_blank" rel="noopener"}

> Mortgage lender says no evidence of identity theft (yet) after SSNs, DoBs, addresses, more swiped Mortgage lender Mr Cooper has now admitted almost 14.7 million people's private information, including addresses and bank account numbers, were stolen in an earlier IT security breach, which is expected to cost the business at least $25 million to clean up.... [...]
