Title: UK government denies China/Russia nuke plant hack claim
Date: 2023-12-05T06:30:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-12-05-uk-government-denies-chinarussia-nuke-plant-hack-claim

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/05/uk_government_denies_sellafield_hack_claim/){:target="_blank" rel="noopener"}

> Report suggests Sellafield compromised since 2015, response seems worryingly ignorant of Stuxnet The government of the United Kingdom has issued a strongly worded denial of a report that the Sellafield nuclear complex has been compromised by malware for years.... [...]
