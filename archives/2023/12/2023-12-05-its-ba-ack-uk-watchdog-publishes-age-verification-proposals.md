Title: It's ba-ack... UK watchdog publishes age verification proposals
Date: 2023-12-05T10:22:29+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-12-05-its-ba-ack-uk-watchdog-publishes-age-verification-proposals

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/05/uk_age_verifcation_proposals/){:target="_blank" rel="noopener"}

> Won't somebody think of the children? The UK's communications regulator has laid out guidance on how online services might perform age checks as part of the Online Safety Act.... [...]
