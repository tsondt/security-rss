Title: The SANS Holiday Hack Challenge is back!
Date: 2023-12-14T09:07:09+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2023-12-14-the-sans-holiday-hack-challenge-is-back

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/14/the_sans_holiday_hack_challenge/){:target="_blank" rel="noopener"}

> Skip the sleigh and sail with Santa in this year’s fun, hands-on SANS cybersecurity event Webinar Whether you are considering a career in cyber security or you already work in the industry, the 2023 SANS Holiday Hack Challenge is a great way of combining festive fun and learning. Who knows, the skills you acquire this holiday season might even help you foil a nefarious hacker at Yuletide next year.... [...]
