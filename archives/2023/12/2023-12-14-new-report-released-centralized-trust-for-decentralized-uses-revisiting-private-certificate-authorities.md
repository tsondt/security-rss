Title: New report released – Centralized Trust for Decentralized Uses: Revisiting Private Certificate Authorities
Date: 2023-12-14T20:42:36+00:00
Author: Katie Collins
Category: AWS Security
Tags: Announcements;AWS Private Certificate Authority;Foundational (100);Security, Identity, & Compliance;AWS Private CA;report;Security Blog
Slug: 2023-12-14-new-report-released-centralized-trust-for-decentralized-uses-revisiting-private-certificate-authorities

[Source](https://aws.amazon.com/blogs/security/new_report_released_centralized_trust_for_decentralized_uses/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is excited to announce the availability of a new sponsored report from S&P Global Market Intelligence 451 Research, Centralized Trust for Decentralized Uses: Revisiting Private Certificate Authorities. We heard from customers actively seeking centralized management solutions for multi-cloud environments and worked with 451 Research, a technology research solution that provides a [...]
