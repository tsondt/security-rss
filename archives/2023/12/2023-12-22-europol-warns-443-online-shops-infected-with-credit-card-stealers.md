Title: Europol warns 443 online shops infected with credit card stealers
Date: 2023-12-22T09:50:12-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-22-europol-warns-443-online-shops-infected-with-credit-card-stealers

[Source](https://www.bleepingcomputer.com/news/security/europol-warns-443-online-shops-infected-with-credit-card-stealers/){:target="_blank" rel="noopener"}

> Europol has notified over 400 websites that their online shops have been hacked with malicious scripts that steal debit and credit cards from customers making purchases. [...]
