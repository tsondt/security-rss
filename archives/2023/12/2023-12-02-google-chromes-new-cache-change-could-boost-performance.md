Title: Google Chrome's new cache change could boost performance
Date: 2023-12-02T11:09:18-05:00
Author: Mayank Parmar
Category: BleepingComputer
Tags: Google;Security
Slug: 2023-12-02-google-chromes-new-cache-change-could-boost-performance

[Source](https://www.bleepingcomputer.com/news/google/google-chromes-new-cache-change-could-boost-performance/){:target="_blank" rel="noopener"}

> Google is introducing a significant change to Chrome's Back/Forward Cache (BFCache) behavior, allowing web pages to be stored in the cache, even if a webmaster specifies not to store a page in the browser's cache. [...]
