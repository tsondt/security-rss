Title: Using Amazon GuardDuty ECS runtime monitoring with Fargate and Amazon EC2
Date: 2023-12-28T17:07:45+00:00
Author: Luke Notley
Category: AWS Security
Tags: Customer Solutions;Intermediate (200);Security, Identity, & Compliance;Amazon GuardDuty;AWS Fargate;Security Blog
Slug: 2023-12-28-using-amazon-guardduty-ecs-runtime-monitoring-with-fargate-and-amazon-ec2

[Source](https://aws.amazon.com/blogs/security/using-amazon-guardduty-ecs-runtime-monitoring-with-fargate-and-amazon-ec2/){:target="_blank" rel="noopener"}

> Containerization technologies such as Docker and orchestration solutions such as Amazon Elastic Container Service (Amazon ECS) are popular with customers due to their portability and scalability advantages. Container runtime monitoring is essential for customers to monitor the health, performance, and security of containers. AWS services such as Amazon GuardDuty, Amazon Inspector, and AWS Security Hub [...]
