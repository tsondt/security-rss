Title: Facebook Enables Messenger End-to-End Encryption by Default
Date: 2023-12-11T12:10:32+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;encryption;Facebook;Meta
Slug: 2023-12-11-facebook-enables-messenger-end-to-end-encryption-by-default

[Source](https://www.schneier.com/blog/archives/2023/12/facebook-enables-messenger-end-to-end-encryption-by-default.html){:target="_blank" rel="noopener"}

> It’s happened. Details here, and tech details here (for messages in transit) and here (for messages in storage) Rollout to everyone will take months, but it’s a good day for both privacy and security. Slashdot thread. [...]
