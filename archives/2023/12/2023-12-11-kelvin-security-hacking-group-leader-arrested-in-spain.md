Title: Kelvin Security hacking group leader arrested in Spain
Date: 2023-12-11T09:27:34-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-11-kelvin-security-hacking-group-leader-arrested-in-spain

[Source](https://www.bleepingcomputer.com/news/security/kelvin-security-hacking-group-leader-arrested-in-spain/){:target="_blank" rel="noopener"}

> The Spanish police have arrested one of the alleged leaders of the 'Kelvin Security' hacking group, which is believed to be responsible for 300 cyberattacks against organizations in 90 countries since 2020. [...]
