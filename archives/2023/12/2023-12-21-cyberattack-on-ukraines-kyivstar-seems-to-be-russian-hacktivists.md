Title: Cyberattack on Ukraine’s Kyivstar Seems to Be Russian Hacktivists
Date: 2023-12-21T12:10:10+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;cyberwar;hacking;Russia;telecom;Ukraine
Slug: 2023-12-21-cyberattack-on-ukraines-kyivstar-seems-to-be-russian-hacktivists

[Source](https://www.schneier.com/blog/archives/2023/12/cyberattack-on-ukraines-kyivstar-seems-to-be-russian-hacktivists.html){:target="_blank" rel="noopener"}

> The Solntsepek group has taken credit for the attack. They’re linked to the Russian military, so it’s unclear whether the attack was government directed or freelance. This is one of the most significant cyberattacks since Russia invaded in February 2022. [...]
