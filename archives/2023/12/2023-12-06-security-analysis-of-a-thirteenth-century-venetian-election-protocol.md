Title: Security Analysis of a Thirteenth-Century Venetian Election Protocol
Date: 2023-12-06T18:18:11+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;cybersecurity;history of security;Italy;protocols;voting
Slug: 2023-12-06-security-analysis-of-a-thirteenth-century-venetian-election-protocol

[Source](https://www.schneier.com/blog/archives/2023/12/security-analysis-of-a-thirteenth-century-venetian-election-protocol.html){:target="_blank" rel="noopener"}

> Interesting analysis : This paper discusses the protocol used for electing the Doge of Venice between 1268 and the end of the Republic in 1797. We will show that it has some useful properties that in addition to being interesting in themselves, also suggest that its fundamental design principle is worth investigating for application to leader election protocols in computer science. For example, it gives some opportunities to minorities while ensuring that more popular candidates are more likely to win, and offers some resistance to corruption of voters. The most obvious feature of this protocol is that it is complicated [...]
