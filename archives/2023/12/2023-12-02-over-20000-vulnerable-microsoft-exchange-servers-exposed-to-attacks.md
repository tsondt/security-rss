Title: Over 20,000 vulnerable Microsoft Exchange servers exposed to attacks
Date: 2023-12-02T13:54:46-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-12-02-over-20000-vulnerable-microsoft-exchange-servers-exposed-to-attacks

[Source](https://www.bleepingcomputer.com/news/security/over-20-000-vulnerable-microsoft-exchange-servers-exposed-to-attacks/){:target="_blank" rel="noopener"}

> Tens of thousands of Microsoft Exchange email servers in Europe, the U.S., and Asia exposed on the public internet are vulnerable to remote code execution flaws. [...]
