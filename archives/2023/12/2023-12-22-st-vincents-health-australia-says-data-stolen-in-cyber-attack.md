Title: St Vincent’s Health Australia says data stolen in cyber-attack
Date: 2023-12-22T02:26:57+00:00
Author: Benita Kolovos
Category: The Guardian
Tags: Health;Data and computer security;Cybercrime;Australia news
Slug: 2023-12-22-st-vincents-health-australia-says-data-stolen-in-cyber-attack

[Source](https://www.theguardian.com/australia-news/2023/dec/22/st-vincents-health-australia-hack-cyberattack-data-stolen-hospital-aged-care-what-to-do){:target="_blank" rel="noopener"}

> Hospital and aged care operator says cyber-attack was first detected on Tuesday and is investigating what data has been accessed Follow our Australia news live blog for latest updates St Vincent’s – Australia’s largest not-for-profit health and aged care provider – has confirmed it has fallen victim to a cyber-attack and hackers have stolen some of its data. In a statement, St Vincent’s Health Australia confirmed it began responding to a cybersecurity incident on Tuesday. It discovered late on Thursday that data had been stolen. Continue reading... [...]
