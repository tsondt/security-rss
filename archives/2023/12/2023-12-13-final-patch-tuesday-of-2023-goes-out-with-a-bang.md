Title: Final Patch Tuesday of 2023 goes out with a bang
Date: 2023-12-13T00:41:41+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-13-final-patch-tuesday-of-2023-goes-out-with-a-bang

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/13/december_2023_patch_tuesday/){:target="_blank" rel="noopener"}

> Microsoft fixed 36 flaws. Adobe addressed 212. Apple, Google, Cisco, VMware and Atlassian joined the party It's the last Patch Tuesday of 2023, which calls for celebration – just as soon as you update Windows, Adobe, Google, Cisco, FortiGuard, SAP, VMware, Atlassian and Apple products, of course.... [...]
