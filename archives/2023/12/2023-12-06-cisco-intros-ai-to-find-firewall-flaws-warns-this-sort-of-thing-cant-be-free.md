Title: Cisco intros AI to find firewall flaws, warns this sort of thing can't be free
Date: 2023-12-06T04:29:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-12-06-cisco-intros-ai-to-find-firewall-flaws-warns-this-sort-of-thing-cant-be-free

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/06/cisco_ai_security/){:target="_blank" rel="noopener"}

> Predicts cyber crims will find binary brainboxes harder to battle Cisco's executive veep for security Jeetu Patel has predicted that AI will change the infosec landscape, but that end users will eventually pay for the privilege of having a binary brainbox by their side when they go into battle.... [...]
