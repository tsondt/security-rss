Title: AI and Lossy Bottlenecks
Date: 2023-12-28T12:01:24+00:00
Author: B. Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;LLM;voting
Slug: 2023-12-28-ai-and-lossy-bottlenecks

[Source](https://www.schneier.com/blog/archives/2023/12/ai-and-lossy-bottlenecks.html){:target="_blank" rel="noopener"}

> Artificial intelligence is poised to upend much of society, removing human limitations inherent in many systems. One such limitation is information and logistical bottlenecks in decision-making. Traditionally, people have been forced to reduce complex choices to a small handful of options that don’t do justice to their true desires. Artificial intelligence has the potential to remove that limitation. And it has the potential to drastically change how democracy functions. AI researcher Tantum Collins and I, a public-interest technology scholar, call this AI overcoming “lossy bottlenecks.” Lossy is a term from information theory that refers to imperfect communications channels—that is, channels [...]
