Title: German police takes down Kingdom Market cybercrime marketplace
Date: 2023-12-20T09:38:50-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-20-german-police-takes-down-kingdom-market-cybercrime-marketplace

[Source](https://www.bleepingcomputer.com/news/security/german-police-takes-down-kingdom-market-cybercrime-marketplace/){:target="_blank" rel="noopener"}

> The Federal Criminal Police Office in Germany (BKA) and the internet-crime combating unit of Frankfurt (ZIT) have announced the seizure of Kingdom Market, a dark web marketplace for drugs, cybercrime tools, and fake government IDs. [...]
