Title: Northern Ireland cops count human cost of August data breach
Date: 2023-12-12T13:46:05+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-12-12-northern-ireland-cops-count-human-cost-of-august-data-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/12/psni_data_breach_forces_officers/){:target="_blank" rel="noopener"}

> Officers potentially targeted by dissidents can't afford to relocate for their safety, while others seek support to change their names An official review of the Police Service of Northern Ireland's (PSNI) August data breach has revealed the full extent of the impact on staff.... [...]
