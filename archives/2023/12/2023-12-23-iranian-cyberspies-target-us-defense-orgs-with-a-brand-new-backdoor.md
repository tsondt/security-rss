Title: Iranian cyberspies target US defense orgs with a brand new backdoor
Date: 2023-12-23T12:47:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-23-iranian-cyberspies-target-us-defense-orgs-with-a-brand-new-backdoor

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/23/iranian_cyberspies_target_us_defense/){:target="_blank" rel="noopener"}

> Also: International cops crackdown on credit card stealers and patch these critical vulns Infosec in brief Iranian cyberspies are targeting defense industrial base organizations with a new backdoor called FalseFont, according to Microsoft.... [...]
