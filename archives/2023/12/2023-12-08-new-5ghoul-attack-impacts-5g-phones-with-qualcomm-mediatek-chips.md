Title: New 5Ghoul attack impacts 5G phones with Qualcomm, MediaTek chips
Date: 2023-12-08T10:23:53-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-12-08-new-5ghoul-attack-impacts-5g-phones-with-qualcomm-mediatek-chips

[Source](https://www.bleepingcomputer.com/news/security/new-5ghoul-attack-impacts-5g-phones-with-qualcomm-mediatek-chips/){:target="_blank" rel="noopener"}

> A new set of vulnerabilities in 5G modems by Qualcomm and MediaTek, collectively called "5Ghoul," impact 710 5G smartphone models from Google partners (Android) and Apple, routers, and USB modems. [...]
