Title: Millions of Xfinity customers' info, hashed passwords feared stolen in cyberattack
Date: 2023-12-19T20:43:44+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-19-millions-of-xfinity-customers-info-hashed-passwords-feared-stolen-in-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/19/comcast_xfinity_hacked/){:target="_blank" rel="noopener"}

> 35M-plus Comcast user IDs accessed by intruder via Citrix Bleed Millions of Comcast Xfinity subscribers' personal data – including potentially their usernames, hashed passwords, contact details, and secret security question-answers – was likely stolen by one or more miscreants exploiting Citrix Bleed in October.... [...]
