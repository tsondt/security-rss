Title: Happy 14th Birthday, KrebsOnSecurity!
Date: 2023-12-29T22:16:27+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Other
Slug: 2023-12-29-happy-14th-birthday-krebsonsecurity

[Source](https://krebsonsecurity.com/2023/12/happy-14th-birthday-krebsonsecurity/){:target="_blank" rel="noopener"}

> KrebsOnSecurity celebrates its 14th year of existence today! I promised myself this post wouldn’t devolve into yet another Cybersecurity Year in Review. Nor do I wish to hold forth about whatever cyber horrors may await us in 2024. But I do want to thank you all for your continued readership, encouragement and support, without which I could not do what I do. As of this birthday, I’ve officially been an independent investigative journalist for longer than I was a reporter for The Washington Post (1995-2009). Of course, not if you count the many years I worked as a paperboy schlepping [...]
