Title: WordPress hosting service Kinsta targeted by Google phishing ads
Date: 2023-12-17T18:46:19-05:00
Author: Mayank Parmar
Category: BleepingComputer
Tags: Security;Cloud
Slug: 2023-12-17-wordpress-hosting-service-kinsta-targeted-by-google-phishing-ads

[Source](https://www.bleepingcomputer.com/news/security/wordpress-hosting-service-kinsta-targeted-by-google-phishing-ads/){:target="_blank" rel="noopener"}

> WordPress hosting provider Kinsta is warning customers that Google ads have been observed promoting phishing sites to steal hosting credentials. [...]
