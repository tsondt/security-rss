Title: The Week in Ransomware - December 1st 2023 - Police hits affiliates
Date: 2023-12-01T17:11:30-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-12-01-the-week-in-ransomware-december-1st-2023-police-hits-affiliates

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-december-1st-2023-police-hits-affiliates/){:target="_blank" rel="noopener"}

> An international law enforcement operation claims to have dismantled a ransomware affiliate operation in Ukraine, which was responsible for attacks on organizations in 71 countries. [...]
