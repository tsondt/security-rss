Title: 60 US credit unions offline after ransomware infects backend cloud outfit
Date: 2023-12-02T00:01:25+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-02-60-us-credit-unions-offline-after-ransomware-infects-backend-cloud-outfit

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/02/ransomware_infection_credit_unions/){:target="_blank" rel="noopener"}

> Supply chain attacks: The gift that keeps on giving A ransomware infection at a cloud IT provider has disrupted services for 60 or so credit unions across the US, all of which were relying on the attacked vendor.... [...]
