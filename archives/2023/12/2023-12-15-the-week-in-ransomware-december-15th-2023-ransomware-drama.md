Title: The Week in Ransomware - December 15th 2023 - Ransomware Drama
Date: 2023-12-15T16:18:41-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-12-15-the-week-in-ransomware-december-15th-2023-ransomware-drama

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-december-15th-2023-ransomware-drama/){:target="_blank" rel="noopener"}

> The big news over the past two weeks is the continued drama plaguing BlackCat/ALPHV after their infrastructure suddenly stopped working for almost five days. Multiple sources told BleepingComputer that this outage was related to a law enforcement operation, but BlackCat claims the outages were caused by a hardware/hosting issue. [...]
