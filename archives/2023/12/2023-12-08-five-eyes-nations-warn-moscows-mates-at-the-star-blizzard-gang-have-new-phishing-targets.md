Title: Five Eyes nations warn Moscow's mates at the Star Blizzard gang have new phishing targets
Date: 2023-12-08T01:31:58+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-08-five-eyes-nations-warn-moscows-mates-at-the-star-blizzard-gang-have-new-phishing-targets

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/08/five_eyes_star_blizzard_warning/){:target="_blank" rel="noopener"}

> The Russians are coming! Err, they've already infiltrated UK, US inboxes Russia-backed attackers have named new targets for their ongoing phishing campaigns, with defense-industrial firms and energy facilities now in their sights, according to agencies of the Five Eyes alliance.... [...]
