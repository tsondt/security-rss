Title: Dropbox spooks users with new AI features that send data to OpenAI when used
Date: 2023-12-13T19:41:35+00:00
Author: Benj Edwards
Category: Ars Technica
Tags: AI;Biz & IT;AI privacy;ChatGPT;chatgtp;data privacy;dropbox;large language models;machine learning;openai;privacy
Slug: 2023-12-13-dropbox-spooks-users-with-new-ai-features-that-send-data-to-openai-when-used

[Source](https://arstechnica.com/?p=1990819){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) On Wednesday, news quickly spread on social media about a new enabled-by-default Dropbox setting that shares Dropbox data with OpenAI for an experimental AI-powered search feature, but Dropbox says data is only shared if the feature is actively being used. Dropbox says that user data shared with third-party AI partners isn't used to train AI models and is deleted within 30 days. Even with assurances of data privacy laid out by Dropbox on an AI privacy FAQ page, the discovery that the setting had been enabled by default upset some Dropbox users. The setting was [...]
