Title: How Microsoft’s cybercrime unit has evolved to combat increased threats
Date: 2023-12-17T12:05:25+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Security;cybercrime;microsoft;syndication
Slug: 2023-12-17-how-microsofts-cybercrime-unit-has-evolved-to-combat-increased-threats

[Source](https://arstechnica.com/?p=1991622){:target="_blank" rel="noopener"}

> Microsoft's Cybercrime Center. (credit: Microsoft) Governments and the tech industry around the world have been scrambling in recent years to curb the rise of online scamming and cybercrime. Yet even with progress on digital defenses, enforcement, and deterrence, the ransomware attacks, business email compromises, and malware infections keep on coming. Over the past decade, Microsoft's Digital Crimes Unit (DCU) has forged its own strategies, both technical and legal, to investigate scams, take down criminal infrastructure, and block malicious traffic. The DCU is fueled, of course, by Microsoft's massive scale and the visibility across the Internet that comes from the reach [...]
