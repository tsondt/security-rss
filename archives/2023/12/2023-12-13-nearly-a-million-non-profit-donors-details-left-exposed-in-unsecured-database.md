Title: Nearly a million non-profit donors' details left exposed in unsecured database
Date: 2023-12-13T10:30:16+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-12-13-nearly-a-million-non-profit-donors-details-left-exposed-in-unsecured-database

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/13/donorview_database_breach/){:target="_blank" rel="noopener"}

> Trusted by major charities, DonorView publicly exposed children’s names and addresses, among other data Close to a million records containing personally identifiable information belonging to donors that sent money to non-profits were found exposed in an online database.... [...]
