Title: Australia building 'top secret' cloud to catch up and link with US, UK intel orgs
Date: 2023-12-07T04:33:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-12-07-australia-building-top-secret-cloud-to-catch-up-and-link-with-us-uk-intel-orgs

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/07/australia_top_secret_cloud/){:target="_blank" rel="noopener"}

> Plans to share 'vast amounts of data' – very carefully Australia is building a top-secret cloud to host intelligence data and share it with the US and UK, which have their own clouds built for the same purpose.... [...]
