Title: Mortgage firm LoanCare warns 1.3 million people of data breach
Date: 2023-12-27T12:44:15-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-12-27-mortgage-firm-loancare-warns-13-million-people-of-data-breach

[Source](https://www.bleepingcomputer.com/news/security/mortgage-firm-loancare-warns-13-million-people-of-data-breach/){:target="_blank" rel="noopener"}

> Mortgage servicing company LoanCare is warning 1,316,938 borrowers across the U.S. that their sensitive information was exposed in a data breach at its parent company, Fidelity National Financial. [...]
