Title: Something nasty injected login-stealing JavaScript into 50K online banking sessions
Date: 2023-12-20T23:45:34+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-20-something-nasty-injected-login-stealing-javascript-into-50k-online-banking-sessions

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/20/credentialstealing_malware_infects_50k_banking/){:target="_blank" rel="noopener"}

> Why keeping your PC secure and free of malware remains paramount IBM Security has dissected some JavaScript code that was injected into people's online banking pages to steal their login credentials, saying 50,000 user sessions with more than 40 banks worldwide were compromised by the malicious software in 2023.... [...]
