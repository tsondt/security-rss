Title: BlackBerry squashes plan to spin out its IoT biz
Date: 2023-12-12T08:23:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-12-12-blackberry-squashes-plan-to-spin-out-its-iot-biz

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/12/blackberry_cancels_split_plan/){:target="_blank" rel="noopener"}

> Board and incoming CEO decide reorganizing is better than splitting BlackBerry has decided its plan to split into two separate companies is not a good idea and will instead reorganize itself into two independent divisions.... [...]
