Title: AI and Trust
Date: 2023-12-04T12:05:33+00:00
Author: B. Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;essays;Liars and Outliers;regulation;trust
Slug: 2023-12-04-ai-and-trust

[Source](https://www.schneier.com/blog/archives/2023/12/ai-and-trust.html){:target="_blank" rel="noopener"}

> I trusted a lot today. I trusted my phone to wake me on time. I trusted Uber to arrange a taxi for me, and the driver to get me to the airport safely. I trusted thousands of other drivers on the road not to ram my car on the way. At the airport, I trusted ticket agents and maintenance engineers and everyone else who keeps airlines operating. And the pilot of the plane I flew in. And thousands of other people at the airport and on the plane, any of which could have attacked me. And all the people that [...]
