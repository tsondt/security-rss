Title: Friday Squid Blogging: Strawberry Squid in the Galápagos
Date: 2023-12-01T22:05:32+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-12-01-friday-squid-blogging-strawberry-squid-in-the-galápagos

[Source](https://www.schneier.com/blog/archives/2023/12/friday-squid-blogging-strawberry-squid-in-the-galapagos.html){:target="_blank" rel="noopener"}

> Scientists have found Strawberry Squid, “whose mismatched eyes help them simultaneously search for prey above and below them,” among the coral reefs in the Galápagos Islands. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
