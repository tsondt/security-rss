Title: NKabuse backdoor harnesses blockchain brawn to hit several architectures
Date: 2023-12-15T14:28:05+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-12-15-nkabuse-backdoor-harnesses-blockchain-brawn-to-hit-several-architectures

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/15/nkabuse_blockchain_backdoor_botnet/){:target="_blank" rel="noopener"}

> Novel malware adapts delivers DDoS attacks and provides RAT functionality Incident responders say they've found a new type of multi-platform malware abusing the New Kind of Network (NKN) protocol.... [...]
