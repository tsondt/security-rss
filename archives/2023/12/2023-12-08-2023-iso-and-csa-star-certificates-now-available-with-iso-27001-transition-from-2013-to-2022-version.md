Title: 2023 ISO and CSA STAR certificates now available with ISO 27001 transition from 2013 to 2022 version
Date: 2023-12-08T16:41:49+00:00
Author: Atulsing Patil
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS CSA STAR;AWS ISO;AWS ISO Certificates;AWS ISO20000;AWS ISO22301;AWS ISO27001;AWS ISO27017;AWS ISO27018;AWS ISO9001;certificates;CSA STAR;ISO;Security Blog
Slug: 2023-12-08-2023-iso-and-csa-star-certificates-now-available-with-iso-27001-transition-from-2013-to-2022-version

[Source](https://aws.amazon.com/blogs/security/2023-iso-and-csa-star-certificates-now-available-with-iso-27001-transition-from-2013-to-2022-version/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) successfully completed a surveillance audit for ISO 9001:2015, 27001:2022, 27017:2015, 27018:2019, 27701:2019, 20000-1:2018, and 22301:2019, and Cloud Security Alliance (CSA) STAR Cloud Controls Matrix (CCM) v4.0. Ernst and Young Certify Point auditors conducted the audit and reissued the certificates on Nov 22, 2023. The objective of the audit was to assess [...]
