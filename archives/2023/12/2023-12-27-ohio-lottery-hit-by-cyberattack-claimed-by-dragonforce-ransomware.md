Title: Ohio Lottery hit by cyberattack claimed by DragonForce ransomware
Date: 2023-12-27T17:11:53-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-27-ohio-lottery-hit-by-cyberattack-claimed-by-dragonforce-ransomware

[Source](https://www.bleepingcomputer.com/news/security/ohio-lottery-hit-by-cyberattack-claimed-by-dragonforce-ransomware/){:target="_blank" rel="noopener"}

> The Ohio Lottery was forced to shut down some key systems after a cyberattack affected an undisclosed number of internal applications on Christmas Eve. [...]
