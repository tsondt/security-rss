Title: Kraft Heinz investigates hack claims, says systems ‘operating normally’
Date: 2023-12-14T18:30:16-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-12-14-kraft-heinz-investigates-hack-claims-says-systems-operating-normally

[Source](https://www.bleepingcomputer.com/news/security/kraft-heinz-investigates-hack-claims-says-systems-operating-normally/){:target="_blank" rel="noopener"}

> Kraft Heinz has confirmed that their systems are operating normally and that there is no evidence they were breached after an extortion group listed them on a data leak site. [...]
