Title: Competing Section 702 surveillance bills on collision path for US House floor
Date: 2023-12-08T22:30:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-12-08-competing-section-702-surveillance-bills-on-collision-path-for-us-house-floor

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/08/competing_section_702_surveillance_bills/){:target="_blank" rel="noopener"}

> End-of-year deadline looms on US surveillance Two competing bills to reauthorize America's FISA Section 702 spying powers advanced in the House of Representatives committees this week, setting up Congress for a battle over warrantless surveillance before the law lapses in the New Year.... [...]
