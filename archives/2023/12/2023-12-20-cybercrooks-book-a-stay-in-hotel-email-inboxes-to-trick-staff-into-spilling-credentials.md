Title: Cybercrooks book a stay in hotel email inboxes to trick staff into spilling credentials
Date: 2023-12-20T21:30:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-12-20-cybercrooks-book-a-stay-in-hotel-email-inboxes-to-trick-staff-into-spilling-credentials

[Source](https://go.theregister.com/feed/www.theregister.com/2023/12/20/hotel_cybercrime_research/){:target="_blank" rel="noopener"}

> Research highlights how major attacks like those exploiting Booking.com are executed Cybercriminals are preying on the inherent helpfulness of hotel staff during the sector's busy holiday season.... [...]
