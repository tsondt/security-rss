Title: Ten Years Later, New Clues in the Target Breach
Date: 2023-12-14T17:51:39+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;74957809554;bothunter;chronopay;Ika;JSC Hot Spot;Lev Tolstoy;MegaPlan;Mikhail Borisovich Shefel;Mikhail Lenin;Mikhail Shefel;pavel vrublevsky;Pustota;r-fac1;target breach;v.zhabukin@freefrog-co-ru;Vasily Borisovich Zhabykin;Vera Vrublevsky;zaxvatmira@gmail.com
Slug: 2023-12-14-ten-years-later-new-clues-in-the-target-breach

[Source](https://krebsonsecurity.com/2023/12/ten-years-later-new-clues-in-the-target-breach/){:target="_blank" rel="noopener"}

> On Dec. 18, 2013, KrebsOnSecurity broke the news that U.S. retail giant Target was battling a wide-ranging computer intrusion that compromised more than 40 million customer payment cards over the previous month. The malware used in the Target breach included the text string “ Rescator,” which also was the handle chosen by the cybercriminal who was selling all of the cards stolen from Target customers. Ten years later, KrebsOnSecurity has uncovered new clues about the real-life identity of Rescator. Rescator, advertising a new batch of cards stolen in a 2014 breach at P.F. Chang’s. Shortly after breaking the Target story, [...]
