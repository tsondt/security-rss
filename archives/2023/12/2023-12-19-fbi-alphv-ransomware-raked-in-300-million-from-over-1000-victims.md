Title: FBI: ALPHV ransomware raked in $300 million from over 1,000 victims
Date: 2023-12-19T14:32:53-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-12-19-fbi-alphv-ransomware-raked-in-300-million-from-over-1000-victims

[Source](https://www.bleepingcomputer.com/news/security/fbi-alphv-ransomware-raked-in-300-million-from-over-1-000-victims/){:target="_blank" rel="noopener"}

> The ALPHV/BlackCat ransomware gang has made over $300 million in ransom payments from more than 1,000 victims worldwide as of September 2023, according to the Federal Bureau of Investigation (FBI). [...]
