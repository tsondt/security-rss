Title: HTC Global Services confirms cyberattack after data leaked online
Date: 2023-12-05T18:54:52-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-12-05-htc-global-services-confirms-cyberattack-after-data-leaked-online

[Source](https://www.bleepingcomputer.com/news/security/htc-global-services-confirms-cyberattack-after-data-leaked-online/){:target="_blank" rel="noopener"}

> IT services and business consulting company HTC Global Services has confirmed that they suffered a cyberattack after the ALPHV ransomware gang began leaking screenshots of stolen data. [...]
