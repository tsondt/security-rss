Title: The Week in Ransomware - December 29th 2023 - LockBit targets hospitals
Date: 2023-12-29T15:39:47-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-12-29-the-week-in-ransomware-december-29th-2023-lockbit-targets-hospitals

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-december-29th-2023-lockbit-targets-hospitals/){:target="_blank" rel="noopener"}

> It's been a quiet week, with even threat actors appearing to take some time off for the holidays. We did not see much research released on ransomware this week, with most of the news focusing on new attacks and LockBit affiliates increasingly targeting hospitals. [...]
