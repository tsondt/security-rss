Title: Police cracks down on DDoS-for-hire service active since 2013
Date: 2023-06-16T15:27:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-16-police-cracks-down-on-ddos-for-hire-service-active-since-2013

[Source](https://www.bleepingcomputer.com/news/security/police-cracks-down-on-ddos-for-hire-service-active-since-2013/){:target="_blank" rel="noopener"}

> Polish police officers part of the country's Central Cybercrime Bureau detained two suspects believed to have been involved in the operation of a long-running DDoS-for-hire service (aka booter or stresser) active since at least 2013. [...]
