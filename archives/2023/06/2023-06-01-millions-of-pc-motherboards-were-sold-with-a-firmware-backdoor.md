Title: Millions of PC motherboards were sold with a firmware backdoor
Date: 2023-06-01T13:04:17+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Security;Tech;firmware;Gigabyte;Motherboards;rootkits
Slug: 2023-06-01-millions-of-pc-motherboards-were-sold-with-a-firmware-backdoor

[Source](https://arstechnica.com/?p=1943487){:target="_blank" rel="noopener"}

> Enlarge (credit: BeeBright/Getty Images) Hiding malicious programs in a computer’s UEFI firmware, the deep-seated code that tells a PC how to load its operating system, has become an insidious trick in the toolkit of stealthy hackers. But when a motherboard manufacturer installs its own hidden backdoor in the firmware of millions of computers—and doesn’t even put a proper lock on that hidden back entrance—they’re practically doing hackers’ work for them. Researchers at firmware-focused cybersecurity company Eclypsium revealed today that they’ve discovered a hidden mechanism in the firmware of motherboards sold by the Taiwanese manufacturer Gigabyte, whose components are commonly used [...]
