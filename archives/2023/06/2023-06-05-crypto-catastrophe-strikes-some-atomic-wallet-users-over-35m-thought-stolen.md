Title: Crypto catastrophe strikes some Atomic Wallet users, over $35M thought stolen
Date: 2023-06-05T18:31:15+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-06-05-crypto-catastrophe-strikes-some-atomic-wallet-users-over-35m-thought-stolen

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/05/atomic_wallet_stolen_crypto/){:target="_blank" rel="noopener"}

> Victims nursing huge losses haven't the foggiest how heist happened, yet As much as $35 million worth of cryptocurrency may have been stolen in a large-scale attack on Atomic Wallet users, with one investigator claiming losses could potentially exceed $50 million.... [...]
