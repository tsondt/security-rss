Title: Linux version of Akira ransomware targets VMware ESXi servers
Date: 2023-06-28T14:51:17-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Linux
Slug: 2023-06-28-linux-version-of-akira-ransomware-targets-vmware-esxi-servers

[Source](https://www.bleepingcomputer.com/news/security/linux-version-of-akira-ransomware-targets-vmware-esxi-servers/){:target="_blank" rel="noopener"}

> The Akira ransomware operation uses a Linux encryptor to encrypt VMware ESXi virtual machines in double-extortion attacks against companies worldwide. [...]
