Title: Why cybersecurity needs a conference like mWISE
Date: 2023-06-08T10:04:08-04:00
Author: Sponsored by Mandiant
Category: BleepingComputer
Tags: Security
Slug: 2023-06-08-why-cybersecurity-needs-a-conference-like-mwise

[Source](https://www.bleepingcomputer.com/news/security/why-cybersecurity-needs-a-conference-like-mwise/){:target="_blank" rel="noopener"}

> Mandiant's mWISE #cybersecurity conference runs from Sept 18-20, 2023 in Washington, D.C. Organizers are asking the public for keynote topic and speaker ideas, and if you register now, you can save 45% off the standard price. [...]
