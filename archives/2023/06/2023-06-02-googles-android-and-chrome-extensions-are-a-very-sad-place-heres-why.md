Title: Google’s Android and Chrome extensions are a very sad place. Here’s why
Date: 2023-06-02T21:07:33+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Google;Security;apps;Chrome Web Store;extensions;google play
Slug: 2023-06-02-googles-android-and-chrome-extensions-are-a-very-sad-place-heres-why

[Source](https://arstechnica.com/?p=1944202){:target="_blank" rel="noopener"}

> Enlarge (credit: Photo Illustration by Miguel Candela/SOPA Images/LightRocket via Getty Images) No wonder Google is having trouble keeping up with policing its app store. Since Monday, researchers have reported that hundreds of Android apps and Chrome extensions with millions of installs from the company’s official marketplaces have included functions for snooping on user files, manipulating the contents of clipboards, and injecting deliberately unknown code into webpages. Google has removed many but not all of the malicious entries, the researchers said, but only after they were reported, and by then, they were on millions of devices—and possibly hundreds of millions. The [...]
