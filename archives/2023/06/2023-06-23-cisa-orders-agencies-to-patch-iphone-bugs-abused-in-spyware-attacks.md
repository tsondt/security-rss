Title: CISA orders agencies to patch iPhone bugs abused in spyware attacks
Date: 2023-06-23T14:06:38-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Apple
Slug: 2023-06-23-cisa-orders-agencies-to-patch-iphone-bugs-abused-in-spyware-attacks

[Source](https://www.bleepingcomputer.com/news/security/cisa-orders-agencies-to-patch-iphone-bugs-abused-in-spyware-attacks/){:target="_blank" rel="noopener"}

> Today, CISA ordered federal agencies to patch recently patched security vulnerabilities exploited as zero-days to deploy Triangulation spyware on iPhones via iMessage zero-click exploits. [...]
