Title: Clop ransomware crew sets June extortion deadline for MOVEit victims
Date: 2023-06-07T19:46:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-07-clop-ransomware-crew-sets-june-extortion-deadline-for-moveit-victims

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/07/clop_crew_sets_extortion_deadline/){:target="_blank" rel="noopener"}

> Plus: The Feds weigh in with advice, details Clop, the ransomware crew that has exploited the MOVEit vulnerability extensively to steal corporate data, has given victims a June 14 deadline to pay up or the purloined information will be leaked.... [...]
