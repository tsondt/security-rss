Title: Announcing Google Cloud’s first complete OSCAL package
Date: 2023-06-09T16:00:00+00:00
Author: Rachel Kim
Category: GCP Security
Tags: Public Sector;Security & Identity
Slug: 2023-06-09-announcing-google-clouds-first-complete-oscal-package

[Source](https://cloud.google.com/blog/products/identity-security/announcing-google-cloud-first-complete-oscal-package/){:target="_blank" rel="noopener"}

> Today, Google Cloud is proud to announce that we have successfully submitted the complete OSCAL package. This is a major milestone for us, as it represents our step forward supporting scalable compliance for Google Cloud and its customers. Open Security Control Assessment Language (OSCAL) OSCAL (Open Security Control Assessment Language) is an open, machine-readable language for representing security control assessments developed by NIST. It is designed to facilitate the exchange of information about security controls between organizations and systems, and enables the automation of security assessments. As organizations increasingly look to move from periodic audits to continuous controls monitoring, the [...]
