Title: Google bug bounties inch closer to Microsoft's payouts
Date: 2023-06-24T14:19:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-24-google-bug-bounties-inch-closer-to-microsofts-payouts

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/24/google_bug_bounties_2022/){:target="_blank" rel="noopener"}

> Chocolate Factory paid a record $12m in 2022 Bug hunters who found security holes in Google — and also responsibly disclosed details of those flaws to the Chocolate Factory — earned more than $12 million in bounty rewards in 2022, marking a record year for the corporation's Vulnerability Reward Programs (VRPs) in terms of payouts and number of vulnerabilities found and fixed.... [...]
