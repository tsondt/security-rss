Title: Friday Squid Blogging: Squid Chromolithographs
Date: 2023-06-02T21:13:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-06-02-friday-squid-blogging-squid-chromolithographs

[Source](https://www.schneier.com/blog/archives/2023/06/friday-squid-blogging-squid-chromolithographs.html){:target="_blank" rel="noopener"}

> Beautiful illustrations. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
