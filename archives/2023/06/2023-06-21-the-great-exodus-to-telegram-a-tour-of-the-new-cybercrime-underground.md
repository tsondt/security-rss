Title: The Great Exodus to Telegram: A Tour of the New Cybercrime Underground
Date: 2023-06-21T10:04:08-04:00
Author: Sponsored by Flare
Category: BleepingComputer
Tags: Security
Slug: 2023-06-21-the-great-exodus-to-telegram-a-tour-of-the-new-cybercrime-underground

[Source](https://www.bleepingcomputer.com/news/security/the-great-exodus-to-telegram-a-tour-of-the-new-cybercrime-underground/){:target="_blank" rel="noopener"}

> Threat actors are moving from the dark web to illicit Telegram channels specializing in cybercrime. This Flare article examines why threat actors are shifting from Tor and provides guidance on monitoring Telegram channels. [...]
