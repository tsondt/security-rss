Title: CISA orders federal agencies to secure Internet-exposed network devices
Date: 2023-06-13T13:33:11-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-13-cisa-orders-federal-agencies-to-secure-internet-exposed-network-devices

[Source](https://www.bleepingcomputer.com/news/security/cisa-orders-federal-agencies-to-secure-internet-exposed-network-devices/){:target="_blank" rel="noopener"}

> CISA issued this year's first binding operational directive (BOD) ordering federal civilian agencies to secure misconfigured or Internet-exposed networking equipment within 14 days of discovery. [...]
