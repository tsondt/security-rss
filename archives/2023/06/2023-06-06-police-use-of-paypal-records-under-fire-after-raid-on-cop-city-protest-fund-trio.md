Title: Police use of PayPal records under fire after raid on 'Cop City' protest fund trio
Date: 2023-06-06T23:03:28+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-06-police-use-of-paypal-records-under-fire-after-raid-on-cop-city-protest-fund-trio

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/06/cop_city_paypal/){:target="_blank" rel="noopener"}

> Nearly anything can look like money laundering if you squint hard enough Three supporters of activists against a $90 million police training facility dubbed Cop City were arrested after the cops used PayPal data to bring money-laundering charges against the trio.... [...]
