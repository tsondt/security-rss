Title: Western Digital boots outdated NAS devices off of My Cloud
Date: 2023-06-16T12:03:30-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud;Hardware
Slug: 2023-06-16-western-digital-boots-outdated-nas-devices-off-of-my-cloud

[Source](https://www.bleepingcomputer.com/news/security/western-digital-boots-outdated-nas-devices-off-of-my-cloud/){:target="_blank" rel="noopener"}

> Western Digital is warning owners of My Cloud series devices that can no longer connect to cloud services starting on June 15, 2023, if the devices are not upgraded to the latest firmware, version 5.26.202. [...]
