Title: American Airlines, Southwest Airlines disclose data breaches affecting pilots
Date: 2023-06-24T03:02:13-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-24-american-airlines-southwest-airlines-disclose-data-breaches-affecting-pilots

[Source](https://www.bleepingcomputer.com/news/security/american-airlines-southwest-airlines-disclose-data-breaches-affecting-pilots/){:target="_blank" rel="noopener"}

> American Airlines and Southwest Airlines, two of the largest airlines in the world, disclosed data breaches on Friday caused by the hack of Pilot Credentials, a third-party vendor that manages multiple airlines' pilot applications and recruitment portals. [...]
