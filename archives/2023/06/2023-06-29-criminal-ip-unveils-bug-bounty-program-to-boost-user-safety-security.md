Title: Criminal IP Unveils Bug Bounty Program to Boost User Safety, Security
Date: 2023-06-29T10:02:04-04:00
Author: Sponsored by Criminal IP
Category: BleepingComputer
Tags: Security
Slug: 2023-06-29-criminal-ip-unveils-bug-bounty-program-to-boost-user-safety-security

[Source](https://www.bleepingcomputer.com/news/security/criminal-ip-unveils-bug-bounty-program-to-boost-user-safety-security/){:target="_blank" rel="noopener"}

> OSINT-based CTI search engine Criminal IP has launched a bug bounty program aimed at strengthening the safety of its services and protecting its users. [...]
