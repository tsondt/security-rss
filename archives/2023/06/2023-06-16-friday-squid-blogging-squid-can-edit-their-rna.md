Title: Friday Squid Blogging: Squid Can Edit Their RNA
Date: 2023-06-16T21:13:49+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-06-16-friday-squid-blogging-squid-can-edit-their-rna

[Source](https://www.schneier.com/blog/archives/2023/06/friday-squid-blogging-squid-can-edit-their-rna.html){:target="_blank" rel="noopener"}

> This is just crazy : Scientists don’t yet know for sure why octopuses, and other shell-less cephalopods including squid and cuttlefish, are such prolific editors. Researchers are debating whether this form of genetic editing gave cephalopods an evolutionary leg (or tentacle) up or whether the editing is just a sometimes useful accident. Scientists are also probing what consequences the RNA alterations may have under various conditions. I sometimes think that cephalopods are aliens that crash-landed on this planet eons ago. Another article. As usual, you can also use this squid post to talk about the security stories in the news [...]
