Title: Capita wins £50M fraud reporting contract with City of London cops
Date: 2023-06-14T13:34:12+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-06-14-capita-wins-50m-fraud-reporting-contract-with-city-of-london-cops

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/14/capita_city_of_london_fraud_reporting_service_contract/){:target="_blank" rel="noopener"}

> No, the irony isn't lost on us either Capita, which is still dealing with a digital break-in that exposed customers' data to criminals, has scored a £50 million contract with the City of London police to run contact and engagement services for the force's fraud reporting service.... [...]
