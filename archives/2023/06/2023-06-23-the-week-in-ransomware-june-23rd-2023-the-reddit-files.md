Title: The Week in Ransomware - June 23rd 2023 - The Reddit Files
Date: 2023-06-23T18:49:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-06-23-the-week-in-ransomware-june-23rd-2023-the-reddit-files

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-june-23rd-2023-the-reddit-files/){:target="_blank" rel="noopener"}

> It was a relatively quiet week regarding ransomware news, with the BlackCat ransomware gang extorting Reddit and the ongoing MOVEit Transfer data breaches being the main focus. [...]
