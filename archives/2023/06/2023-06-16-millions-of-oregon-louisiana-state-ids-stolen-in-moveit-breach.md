Title: Millions of Oregon, Louisiana state IDs stolen in MOVEit breach
Date: 2023-06-16T10:28:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-16-millions-of-oregon-louisiana-state-ids-stolen-in-moveit-breach

[Source](https://www.bleepingcomputer.com/news/security/millions-of-oregon-louisiana-state-ids-stolen-in-moveit-breach/){:target="_blank" rel="noopener"}

> Louisiana and Oregon warn that millions of driver's licenses were exposed in a data breach after a ransomware gang hacked their MOVEit Transfer security file transfer systems to steal stored data. [...]
