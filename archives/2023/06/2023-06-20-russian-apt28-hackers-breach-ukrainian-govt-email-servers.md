Title: Russian APT28 hackers breach Ukrainian govt email servers
Date: 2023-06-20T09:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-20-russian-apt28-hackers-breach-ukrainian-govt-email-servers

[Source](https://www.bleepingcomputer.com/news/security/russian-apt28-hackers-breach-ukrainian-govt-email-servers/){:target="_blank" rel="noopener"}

> A threat group tracked as APT28 and linked to Russia's General Staff Main Intelligence Directorate (GRU) has breached Roundcube email servers belonging to multiple Ukrainian organizations, including government entities. [...]
