Title: US govt offers $10 million bounty for info on Clop ransomware
Date: 2023-06-17T16:06:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-06-17-us-govt-offers-10-million-bounty-for-info-on-clop-ransomware

[Source](https://www.bleepingcomputer.com/news/security/us-govt-offers-10-million-bounty-for-info-on-clop-ransomware/){:target="_blank" rel="noopener"}

> The U.S. State Department's Rewards for Justice program announced up to a $10 million bounty yesterday for information linking the Clop ransomware attacks to a foreign government. [...]
