Title: JP Morgan accidentally deletes evidence in multi-million record retention screwup
Date: 2023-06-26T09:30:13+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2023-06-26-jp-morgan-accidentally-deletes-evidence-in-multi-million-record-retention-screwup

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/26/jp_morgan_fined_for_deleting/){:target="_blank" rel="noopener"}

> Fined $4m for Who-Me-esque mess, for which it blames unnamed archiving vendor's retention settings JP Morgan has been fined $4 million by America's securities watchdog, the SEC, for deleting millions of email records dating from 2018 relating to its Chase Bank subsidiary.... [...]
