Title: Chinese balloon that US shot down was 'crammed' with American hardware
Date: 2023-06-29T17:03:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-06-29-chinese-balloon-that-us-shot-down-was-crammed-with-american-hardware

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/29/chinese_spy_balloon_crammed_with/){:target="_blank" rel="noopener"}

> Blasted from the sky in February, device never transmitted photos, videos, or radar data it collected, officials say It's been months since "spy balloon" fever gripped the United States, but the headline-grabbing flying object – alleged to have been deployed by China – is back in the news. Preliminary findings from the US inspection of its wreckage show a whole bunch of commercially available hardware made in the States.... [...]
