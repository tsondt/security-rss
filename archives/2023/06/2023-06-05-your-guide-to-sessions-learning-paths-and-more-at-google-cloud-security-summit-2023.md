Title: Your guide to sessions, learning paths, and more at Google Cloud Security Summit 2023
Date: 2023-06-05T16:00:00+00:00
Author: Anil Nandigam
Category: GCP Security
Tags: Security & Identity
Slug: 2023-06-05-your-guide-to-sessions-learning-paths-and-more-at-google-cloud-security-summit-2023

[Source](https://cloud.google.com/blog/products/identity-security/your-guide-to-google-cloud-security-summit-2023/){:target="_blank" rel="noopener"}

> Our annual Google Cloud Security Summit is coming next week, and we have an exciting agenda with keynotes, demos, and breakout sessions. This year, we’re featuring two tracks: Frontline Intelligence and Cloud Innovation. When you attend the summit, you will be the first to learn about the latest technologies and strategies that can help protect your business, your customers, and your cloud transformation from emerging threats. We have an exciting lineup of speakers this year including experts from Google and leaders from Citi, Broadcom, GoFundme, Nordnet AB, and our partners F5, Accenture, Deloitte, and Palo Alto Networks. Register here for [...]
