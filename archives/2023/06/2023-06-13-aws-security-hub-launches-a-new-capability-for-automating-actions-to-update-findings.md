Title: AWS Security Hub launches a new capability for automating actions to update findings
Date: 2023-06-13T17:57:49+00:00
Author: Stuart Gregg
Category: AWS Security
Tags: Announcements;Best Practices;Intermediate (200);Security, Identity, & Compliance;Automation;AWS Security Hub;SecOps;security automation;Security Blog;Securityoperations
Slug: 2023-06-13-aws-security-hub-launches-a-new-capability-for-automating-actions-to-update-findings

[Source](https://aws.amazon.com/blogs/security/aws-security-hub-launches-a-new-capability-for-automating-actions-to-update-findings/){:target="_blank" rel="noopener"}

> If you’ve had discussions with a security organization recently, there’s a high probability that the word automation has come up. As organizations scale and consume the benefits the cloud has to offer, it’s important to factor in and understand how the additional cloud footprint will affect operations. Automation is a key enabler for efficient operations [...]
