Title: Microsoft June 2023 Patch Tuesday fixes 78 flaws, 38 RCE bugs
Date: 2023-06-13T13:28:28-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-06-13-microsoft-june-2023-patch-tuesday-fixes-78-flaws-38-rce-bugs

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-june-2023-patch-tuesday-fixes-78-flaws-38-rce-bugs/){:target="_blank" rel="noopener"}

> Today is Microsoft's June 2023 Patch Tuesday, with security updates for 78 flaws, including 38 remote code execution vulnerabilities. [...]
