Title: AI as Sensemaking for Public Comments
Date: 2023-06-22T15:43:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;laws;LLM;public interest
Slug: 2023-06-22-ai-as-sensemaking-for-public-comments

[Source](https://www.schneier.com/blog/archives/2023/06/ai-as-sensemaking-for-public-comments.html){:target="_blank" rel="noopener"}

> It’s become fashionable to think of artificial intelligence as an inherently dehumanizing technology, a ruthless force of automation that has unleashed legions of virtual skilled laborers in faceless form. But what if AI turns out to be the one tool able to identify what makes your ideas special, recognizing your unique perspective and potential on the issues where it matters most? You’d be forgiven if you’re distraught about society’s ability to grapple with this new technology. So far, there’s no lack of prognostications about the democratic doom that AI may wreak on the US system of government. There are legitimate [...]
