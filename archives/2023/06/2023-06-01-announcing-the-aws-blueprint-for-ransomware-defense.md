Title: Announcing the AWS Blueprint for Ransomware Defense
Date: 2023-06-01T19:49:57+00:00
Author: Jeremy Ware
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS security;CIS;Cloud security;cybersecurity;ransomware;Security;security best practices;Security Blog;Security controls
Slug: 2023-06-01-announcing-the-aws-blueprint-for-ransomware-defense

[Source](https://aws.amazon.com/blogs/security/announcing-the-aws-blueprint-for-ransomware-defense/){:target="_blank" rel="noopener"}

> In this post, Amazon Web Services (AWS) introduces the AWS Blueprint for Ransomware Defense, a new resource that both enterprise and public sector organizations can use to implement preventative measures to protect data from ransomware events. The AWS Blueprint for Ransomware Defense provides a mapping of AWS services and features as they align to aspects [...]
