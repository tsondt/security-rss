Title: Quirky QWERTY killed a password in Paris
Date: 2023-06-30T07:27:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-06-30-quirky-qwerty-killed-a-password-in-paris

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/30/on_call/){:target="_blank" rel="noopener"}

> Quelle tragédie – techie had to visit the city of lights twice to sort this one out On Call Hard-coded into The Register 's week is that each Friday morning you’ll find a new instalment of On Call, our reader contributed tales of tech support troubles.... [...]
