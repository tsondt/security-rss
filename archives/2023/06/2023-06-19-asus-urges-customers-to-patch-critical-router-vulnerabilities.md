Title: ASUS urges customers to patch critical router vulnerabilities
Date: 2023-06-19T13:30:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-19-asus-urges-customers-to-patch-critical-router-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/asus-urges-customers-to-patch-critical-router-vulnerabilities/){:target="_blank" rel="noopener"}

> ASUS has released new firmware with cumulative security updates that address vulnerabilities in multiple router models, warning customers to immediately update their devices or restrict WAN access until they're secured. [...]
