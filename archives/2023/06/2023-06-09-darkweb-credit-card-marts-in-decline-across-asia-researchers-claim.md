Title: Darkweb credit card marts in decline across Asia, researchers claim
Date: 2023-06-09T03:31:09+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-06-09-darkweb-credit-card-marts-in-decline-across-asia-researchers-claim

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/09/group_ib_apac_incident_prevalence/){:target="_blank" rel="noopener"}

> India tops the charts for document theft The number of stolen Asian credit card numbers appearing on darkweb crime marts has fallen sharply, cyber security firm Group-IB told Singapore's ATxSG conference on Thursday.... [...]
