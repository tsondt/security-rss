Title: U.K. Cyber Thug “PlugwalkJoe” Gets 5 Years in Prison
Date: 2023-06-27T19:44:03+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;SIM Swapping;Graham Ivan Clarke;Joseph James O'Connor;PlugWalkJoe;SIM swapping;Twitter hack
Slug: 2023-06-27-uk-cyber-thug-plugwalkjoe-gets-5-years-in-prison

[Source](https://krebsonsecurity.com/2023/06/u-k-cyber-thug-plugwalkjoe-gets-5-years-in-prison/){:target="_blank" rel="noopener"}

> Joseph James “PlugwalkJoe” O’Connor, a 24-year-old from the United Kingdom who earned his 15 minutes of fame by participating in the July 2020 hack of Twitter, has been sentenced to five years in a U.S. prison. That may seem like harsh punishment for a brief and very public cyber joy ride. But O’Connor also pleaded guilty in a separate investigation involving a years-long spree of cyberstalking and cryptocurrency theft enabled by “ SIM swapping,” a crime wherein fraudsters trick a mobile provider into diverting a customer’s phone calls and text messages to a device they control. Joseph “PlugwalkJoe” O’Connor, in [...]
