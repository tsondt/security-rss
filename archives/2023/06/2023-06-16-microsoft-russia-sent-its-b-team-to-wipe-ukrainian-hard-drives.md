Title: Microsoft: Russia sent its B team to wipe Ukrainian hard drives
Date: 2023-06-16T06:31:08+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-06-16-microsoft-russia-sent-its-b-team-to-wipe-ukrainian-hard-drives

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/16/microsoft_cadet_blizzard_threat/){:target="_blank" rel="noopener"}

> WhisperGate-spreading Cadet Blizzard painted as haphazard but dangerous crew Here's a curious tale about a highly destructive yet flaky Kremlin-backed crew that was active during the early days of Russia's invasion of Ukraine, then went relatively quiet – until this year.... [...]
