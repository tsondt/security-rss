Title: Guess what happened to this US agency using outdated software?
Date: 2023-06-19T14:32:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-19-guess-what-happened-to-this-us-agency-using-outdated-software

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/19/old_telerik_bug_exploited/){:target="_blank" rel="noopener"}

> Also: Hackers target security researchers, MaaS model flourishing, and this week's vulnerabilities Infosec in brief Remember earlier this year, when we found out that a bunch of baddies including at least one nation-state group broke into a US federal government agency's Microsoft Internet Information Services (IIS) web server by exploiting a critical three-year-old Telerik bug to achieve remote code execution?... [...]
