Title: AWS Security Profile: Matthew Campagna, Senior Principal, Security Engineering, AWS Cryptography
Date: 2023-06-08T16:47:45+00:00
Author: Roger Park
Category: AWS Security
Tags: Events;Foundational (100);Security, Identity, & Compliance;Thought Leadership;AWS re:Inforce;AWS Security Profile;AWS Security Profiles;cryptography;Live Events;re:Inforce;re:Inforce 2023;Security Blog
Slug: 2023-06-08-aws-security-profile-matthew-campagna-senior-principal-security-engineering-aws-cryptography

[Source](https://aws.amazon.com/blogs/security/security-profile-matthew-campagna-aws-cryptography/){:target="_blank" rel="noopener"}

> In the AWS Security Profile series, we interview Amazon Web Services (AWS) thought leaders who help keep our customers safe and secure. This interview features Matt Campagna, Senior Principal, Security Engineering, AWS Cryptography, and re:Inforce 2023 session speaker, who shares thoughts on data protection, cloud security, post-quantum cryptography, and more. Matthew was first profiled on [...]
