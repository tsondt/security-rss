Title: Identity thieves can hunt us for 'rest of our lives,' claims suit after university data leak
Date: 2023-06-06T17:34:10+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-06-06-identity-thieves-can-hunt-us-for-rest-of-our-lives-claims-suit-after-university-data-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/06/mercer/){:target="_blank" rel="noopener"}

> Crooks steal Social Security numbers and post them on dark web, victims blame holes in Mercer's security An American university founded in 1833 is facing a bunch of class action lawsuits after the personal data of nearly 100,000 people was stolen from its tech infrastructure.... [...]
