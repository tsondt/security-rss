Title: What’s new in Assured Workloads: Region expansion, TLS version restrictions, new supported services
Date: 2023-06-22T16:00:00+00:00
Author: Collin Frierson
Category: GCP Security
Tags: Security & Identity
Slug: 2023-06-22-whats-new-in-assured-workloads-region-expansion-tls-version-restrictions-new-supported-services

[Source](https://cloud.google.com/blog/products/identity-security/new-to-assured-workloads-regions-tls-controls-and-supported-services/){:target="_blank" rel="noopener"}

> To maximize the benefits of digital transformation, organizations need to be comfortable bringing sensitive and regulated workloads to the cloud. Assured Workloads is a modern cloud solution that allows our customers to run regulated workloads in many of Google Cloud's global regions. Core to our strategy for Assured Workloads is to build security and compliance controls as software. Software allows us to scale globally and combine technologies to help our customers achieve specific compliance outcomes. This approach has enabled us to make Assured Workloads available in more countries, and expand the list of available services across multiple compliance frameworks. As [...]
