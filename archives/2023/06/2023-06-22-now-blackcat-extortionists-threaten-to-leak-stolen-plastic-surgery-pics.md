Title: Now BlackCat extortionists threaten to leak stolen plastic surgery pics
Date: 2023-06-22T17:57:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-22-now-blackcat-extortionists-threaten-to-leak-stolen-plastic-surgery-pics

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/22/blackcat_ransomware_plastic_surgery_clinic/){:target="_blank" rel="noopener"}

> Sharing a cancer patient's nude snaps earlier wasn't enough for these scumbags Ransomware gang BlackCat claims it infected a plastic surgery center, stole "lots" of highly sensitive medical records, and has vowed to leak patients' photos if the clinic doesn't pay up.... [...]
