Title: Simplify fine-grained authorization with Amazon Verified Permissions and Amazon Cognito
Date: 2023-06-14T13:36:51+00:00
Author: Phil Windley
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;Amazon Verified Permissions;AWS Identity;Security Blog
Slug: 2023-06-14-simplify-fine-grained-authorization-with-amazon-verified-permissions-and-amazon-cognito

[Source](https://aws.amazon.com/blogs/security/simplify-fine-grained-authorization-with-amazon-verified-permissions-and-amazon-cognito/){:target="_blank" rel="noopener"}

> AWS customers already use Amazon Cognito for simple, fast authentication. With the launch of Amazon Verified Permissions, many will also want to add simple, fast authorization to their applications by using the user attributes that they have in Amazon Cognito. In this post, I will show you how to use Amazon Cognito and Verified Permissions together to [...]
