Title: FTC accuses DNA testing company of lying about dumping samples
Date: 2023-06-21T19:30:15+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-06-21-ftc-accuses-dna-testing-company-of-lying-about-dumping-samples

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/21/dna_testing_company_ftc_complaint/){:target="_blank" rel="noopener"}

> 1Health must strengthen protections for genetic information as part of settlement The Federal Trade Commission has alleged that genetic testing firm 1Health.io, also known as Vitagene, deceived people when it said it would dispose of their physical DNA sample as well as their collected health data.... [...]
