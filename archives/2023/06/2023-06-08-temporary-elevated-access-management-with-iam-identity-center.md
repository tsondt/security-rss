Title: Temporary elevated access management with IAM Identity Center
Date: 2023-06-08T22:03:50+00:00
Author: Taiwo Awoyinfa
Category: AWS Security
Tags: Advanced (300);AWS Identity and Access Management (IAM);Security, Identity, & Compliance;Technical How-to;AWS CLI;AWS IAM;AWS IAM Identity Center;AWS Management Console;Privileged access;Security Blog;Temporary elevated access
Slug: 2023-06-08-temporary-elevated-access-management-with-iam-identity-center

[Source](https://aws.amazon.com/blogs/security/temporary-elevated-access-management-with-iam-identity-center/){:target="_blank" rel="noopener"}

> AWS recommends using automation where possible to keep people away from systems—yet not every action can be automated in practice, and some operations might require access by human users. Depending on their scope and potential impact, some human operations might require special treatment. One such treatment is temporary elevated access, also known as just-in-time access. [...]
