Title: Miscreants leak texts and info siphoned by Android stalkerware app LetMeSpy
Date: 2023-06-27T22:22:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-27-miscreants-leak-texts-and-info-siphoned-by-android-stalkerware-app-letmespy

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/27/letmespy_stalkerware_app_hacked/){:target="_blank" rel="noopener"}

> Just as America's Supremes set a high bar for cyberstalking It's bad enough there's some Android stalkerware out there with the not-at-all-creepy moniker LetMeSpy. Now someone's got hold of the information the app collects – such as victims' text messages and call logs – as well as the email addresses of those who sought out the software, and leaked it all.... [...]
