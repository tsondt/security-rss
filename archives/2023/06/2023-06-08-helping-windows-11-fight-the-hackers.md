Title: Helping Windows 11 fight the hackers
Date: 2023-06-08T09:07:14+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: 2023-06-08-helping-windows-11-fight-the-hackers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/08/helping_windows_11_fight_the/){:target="_blank" rel="noopener"}

> How Intel is using hardware-assisted security to beef up Microsoft OS protection Sponsored Feature When Windows 11 launched in October 2021, one of its big selling points was a new security architecture. Microsoft designed it from the ground up with zero-trust principles in mind, refusing to trust the legitimacy of any single system component. Instead, everything must prove that it has not been compromised.... [...]
