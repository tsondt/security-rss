Title: Barracuda says hacked ESG appliances must be replaced immediately
Date: 2023-06-07T16:57:36-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-07-barracuda-says-hacked-esg-appliances-must-be-replaced-immediately

[Source](https://www.bleepingcomputer.com/news/security/barracuda-says-hacked-esg-appliances-must-be-replaced-immediately/){:target="_blank" rel="noopener"}

> Email and network security company Barracuda warns customers they must replace Email Security Gateway (ESG) appliances hacked in attacks targeting a now-patched zero-day vulnerability. [...]
