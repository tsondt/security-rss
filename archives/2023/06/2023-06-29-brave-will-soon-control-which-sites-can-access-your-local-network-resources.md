Title: Brave will soon control which sites can access your local network resources
Date: 2023-06-29T00:15:32+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;brave browser
Slug: 2023-06-29-brave-will-soon-control-which-sites-can-access-your-local-network-resources

[Source](https://arstechnica.com/?p=1950882){:target="_blank" rel="noopener"}

> Enlarge The Brave browser will take action against websites that snoop on visitors by scanning their open Internet ports or accessing other network resources that can expose personal information. Starting in version 1.54, Brave will automatically block website port scanning, a practice that a surprisingly large number of sites were found engaging in a few years ago. According to this list compiled in 2021 by a researcher who goes by the handle G666g1e, 744 websites scanned visitors’ ports, most or all without providing notice or seeking permission in advance. eBay, Chick-fil-A, Best Buy, Kroger, and Macy's were among the offending [...]
