Title: Us, hacked by LockBit? No, says TSMC, that would be our IT supplier
Date: 2023-06-30T23:17:18+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-30-us-hacked-by-lockbit-no-says-tsmc-that-would-be-our-it-supplier

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/30/tsmc_supplier_lockbit_breach/){:target="_blank" rel="noopener"}

> So, uh, who's gonna pay that $70M ransom? Following claims by ransomware gang LockBit that it has stolen data belonging to TSMC, the chip-making giant has said it was in fact one of its equipment suppliers, Kinmax, that was compromised by the crew, and not TSMC itself.... [...]
