Title: Service Rents Email Addresses for Account Signups
Date: 2023-06-06T20:09:13+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Web Fraud 2.0;Impulse Scam Crypto Project;Kopeechka;Mastodon;Quotpw;Renaud Chaput;spam;trend micro
Slug: 2023-06-06-service-rents-email-addresses-for-account-signups

[Source](https://krebsonsecurity.com/2023/06/service-rents-email-addresses-for-account-signups/){:target="_blank" rel="noopener"}

> One of the most expensive aspects of any cybercriminal operation is the time and effort it takes to constantly create large numbers of new throwaway email accounts. Now a new service offers to help dramatically cut costs associated with large-scale spam and account creation campaigns, by paying people to sell their email account credentials and letting customers temporarily rent access to a vast pool of established accounts at major providers. The service in question — kopeechka[.]store — is perhaps best described as a kind of unidirectional email confirmation-as-a-service that promises to “save your time and money for successfully registering multiple [...]
