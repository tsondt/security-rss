Title: Lazarus hackers linked to the $35 million Atomic Wallet heist
Date: 2023-06-07T14:16:43-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-06-07-lazarus-hackers-linked-to-the-35-million-atomic-wallet-heist

[Source](https://www.bleepingcomputer.com/news/security/lazarus-hackers-linked-to-the-35-million-atomic-wallet-heist/){:target="_blank" rel="noopener"}

> The notorious North Korean hacking group known as Lazarus has been linked to the recent Atomic Wallet hack, resulting in the theft of over $35 million in crypto. [...]
