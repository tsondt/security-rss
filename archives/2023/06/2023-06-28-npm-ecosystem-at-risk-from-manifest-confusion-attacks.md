Title: NPM ecosystem at risk from “Manifest Confusion” attacks
Date: 2023-06-28T10:28:35-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-28-npm-ecosystem-at-risk-from-manifest-confusion-attacks

[Source](https://www.bleepingcomputer.com/news/security/npm-ecosystem-at-risk-from-manifest-confusion-attacks/){:target="_blank" rel="noopener"}

> The NPM (Node Package Manager) registry suffers from a security lapse called "manifest confusion," which undermines the trustworthiness of packages and makes it possible for attackers to hide malware in dependencies or perform malicious script execution during installation. [...]
