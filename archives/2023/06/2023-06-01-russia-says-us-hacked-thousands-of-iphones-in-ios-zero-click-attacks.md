Title: Russia says US hacked thousands of iPhones in iOS zero-click attacks
Date: 2023-06-01T12:11:28-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple;Mobile
Slug: 2023-06-01-russia-says-us-hacked-thousands-of-iphones-in-ios-zero-click-attacks

[Source](https://www.bleepingcomputer.com/news/security/russia-says-us-hacked-thousands-of-iphones-in-ios-zero-click-attacks/){:target="_blank" rel="noopener"}

> Russian cybersecurity firm Kaspersky says some iPhones on its network were hacked using an iOS vulnerability that installed malware via iMessage zero-click exploits. Russia blames these attacks on US intelligence agencies. [...]
