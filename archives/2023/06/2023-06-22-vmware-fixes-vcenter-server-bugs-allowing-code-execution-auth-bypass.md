Title: VMware fixes vCenter Server bugs allowing code execution, auth bypass
Date: 2023-06-22T12:07:59-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-22-vmware-fixes-vcenter-server-bugs-allowing-code-execution-auth-bypass

[Source](https://www.bleepingcomputer.com/news/security/vmware-fixes-vcenter-server-bugs-allowing-code-execution-auth-bypass/){:target="_blank" rel="noopener"}

> VMware has addressed multiple high-severity security flaws in vCenter Server, which can let attackers gain code execution and bypass authentication on unpatched systems. [...]
