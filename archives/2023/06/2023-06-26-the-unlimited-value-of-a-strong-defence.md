Title: The unlimited value of a strong defence
Date: 2023-06-26T02:16:15+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-06-26-the-unlimited-value-of-a-strong-defence

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/26/the_unlimited_value_of_a/){:target="_blank" rel="noopener"}

> How protective AI is a powerful weapon in the fight against cyber attackers using AI for malicious acts. Webinar In the new age of generative AI, it would be foolhardy to imagine that bad actors won't already be exploiting every opportunity to launch an attack with their own malicious AI generated war machines.... [...]
