Title: Microsoft cops $20M slap on the wrist for mishandling kids' Xbox data
Date: 2023-06-06T18:24:14+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-06-06-microsoft-cops-20m-slap-on-the-wrist-for-mishandling-kids-xbox-data

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/06/microsoft_fined_20m_for_collecting/){:target="_blank" rel="noopener"}

> Pocket change, in other words Microsoft is being fined $20 million by the US Federal Trade Commission for violating the Children's Online Privacy Protection Act (COPPA) by illegally gathering kids' personal information and retaining it without parental consent.... [...]
