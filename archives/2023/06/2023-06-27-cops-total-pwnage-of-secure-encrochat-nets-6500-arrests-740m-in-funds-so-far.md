Title: Cops' total pwnage of 'secure' EncroChat nets 6,500+ arrests, €740m in funds – so far
Date: 2023-06-27T21:23:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-27-cops-total-pwnage-of-secure-encrochat-nets-6500-arrests-740m-in-funds-so-far

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/27/encrochat_eu_arrests/){:target="_blank" rel="noopener"}

> Eurocop op cracking crims' chat app causes clink time and cash confiscation Police breaking into and snooping on the EncroChat encrypted messaging network has led to 6,558 arrests worldwide and nearly €740 million seized in criminal funds, according to cops in France and the Netherlands.... [...]
