Title: The Week in Ransomware - June 9th 2023 - It’s Clop... Again!
Date: 2023-06-09T18:44:02-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-06-09-the-week-in-ransomware-june-9th-2023-its-clop-again

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-june-9th-2023-its-clop-again/){:target="_blank" rel="noopener"}

> The week was dominated by fallout over the MOVEit Transfer data-theft attacks, with the Clop ransomware gang confirming that they were behind them. [...]
