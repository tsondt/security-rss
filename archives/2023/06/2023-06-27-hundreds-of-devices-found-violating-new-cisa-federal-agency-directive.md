Title: Hundreds of devices found violating new CISA federal agency directive
Date: 2023-06-27T14:06:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-27-hundreds-of-devices-found-violating-new-cisa-federal-agency-directive

[Source](https://www.bleepingcomputer.com/news/security/hundreds-of-devices-found-violating-new-cisa-federal-agency-directive/){:target="_blank" rel="noopener"}

> Censys researchers have discovered hundreds of Internet-exposed devices on the networks of U.S. federal agencies that have to be secured according to a recently issued CISA Binding Operational Directive. [...]
