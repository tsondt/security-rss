Title: 2023 ISO and CSA STAR certificates now available with 8 new services and 1 new Region
Date: 2023-06-08T13:16:16+00:00
Author: Atul Patil
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS CSA STAR;AWS ISO;AWS ISO Certificates;AWS ISO22301;AWS ISO27001;AWS ISO27017;AWS ISO27018;AWS ISO9001;Security Blog
Slug: 2023-06-08-2023-iso-and-csa-star-certificates-now-available-with-8-new-services-and-1-new-region

[Source](https://aws.amazon.com/blogs/security/2023-iso-and-csa-star-certificates-now-available-with-8-new-services-and-1-new-region/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) successfully completed a special onboarding audit with no findings for ISO 9001, 27001, 27017, 27018, 27701, and 22301, and Cloud Security Alliance (CSA) STAR CCM v4.0. Ernst and Young Certify Point auditors conducted the audit and reissued the certificates on May 23, 2023. The objective of the audit was to assess [...]
