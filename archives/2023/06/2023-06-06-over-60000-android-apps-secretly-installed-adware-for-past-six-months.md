Title: Over 60,000 Android apps secretly installed adware for past six months
Date: 2023-06-06T15:10:26-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2023-06-06-over-60000-android-apps-secretly-installed-adware-for-past-six-months

[Source](https://www.bleepingcomputer.com/news/security/over-60-000-android-apps-secretly-installed-adware-for-past-six-months/){:target="_blank" rel="noopener"}

> Over 60,000 Android apps disguised as legitimate applications have been quietly installing adware on mobile devices while remaining undetected for the past six months. [...]
