Title: NSA and FBI: Kimsuky hackers pose as journalists to steal intel
Date: 2023-06-02T14:07:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-02-nsa-and-fbi-kimsuky-hackers-pose-as-journalists-to-steal-intel

[Source](https://www.bleepingcomputer.com/news/security/nsa-and-fbi-kimsuky-hackers-pose-as-journalists-to-steal-intel/){:target="_blank" rel="noopener"}

> State-sponsored North Korean hacker group Kimsuky (a.ka. APT43) has been impersonating journalists and academics for spear-phishing campaigns to collect intelligence from think tanks, research centers, academic institutions, and various media organizations. [...]
