Title: Taking the art of email security to the next level
Date: 2023-06-06T08:28:11+00:00
Author: Guy Matthews
Category: The Register
Tags: 
Slug: 2023-06-06-taking-the-art-of-email-security-to-the-next-level

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/06/taking_the_art_of_email/){:target="_blank" rel="noopener"}

> AI is beefing up the cyber arsenals of both attackers and defenders Sponsored Feature Email is a popular target for cybercriminals, offering an easy way of launching an attack disguised as an innocent message. One moment of inattention on the part of the recipient and the door is open to malware, spam, phishing, perhaps even a dose of the dreaded ransomware. Entire organisations can suffer, not just individual victims.... [...]
