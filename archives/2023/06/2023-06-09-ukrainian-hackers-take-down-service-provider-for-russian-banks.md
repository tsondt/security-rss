Title: Ukrainian hackers take down service provider for Russian banks
Date: 2023-06-09T18:06:51-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-09-ukrainian-hackers-take-down-service-provider-for-russian-banks

[Source](https://www.bleepingcomputer.com/news/security/ukrainian-hackers-take-down-service-provider-for-russian-banks/){:target="_blank" rel="noopener"}

> A group of Ukrainian hackers known as the Cyber.Anarchy.Squad claimed an attack that took down Russian telecom provider Infotel JSC on Thursday evening. [...]
