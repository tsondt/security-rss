Title: Mirai botnet targets 22 flaws in D-Link, Zyxel, Netgear devices
Date: 2023-06-22T13:53:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-22-mirai-botnet-targets-22-flaws-in-d-link-zyxel-netgear-devices

[Source](https://www.bleepingcomputer.com/news/security/mirai-botnet-targets-22-flaws-in-d-link-zyxel-netgear-devices/){:target="_blank" rel="noopener"}

> A variant of the Mirai botnet is targeting almost two dozen vulnerabilities aiming to take control of D-Link, Arris, Zyxel, TP-Link, Tenda, Netgear, and MediaTek devices to use them for distributed denial-of-service (DDoS) attacks. [...]
