Title: Strava heatmap feature can be abused to find home addresses
Date: 2023-06-11T10:15:30-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-11-strava-heatmap-feature-can-be-abused-to-find-home-addresses

[Source](https://www.bleepingcomputer.com/news/security/strava-heatmap-feature-can-be-abused-to-find-home-addresses/){:target="_blank" rel="noopener"}

> Researchers at the North Carolina State University Raleigh have discovered a privacy risk in the Strava app's heatmap feature that could lead to identifying users' home addresses. [...]
