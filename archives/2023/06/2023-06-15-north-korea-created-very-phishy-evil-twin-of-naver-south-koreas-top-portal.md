Title: North Korea created very phishy evil twin of Naver, South Korea's top portal
Date: 2023-06-15T02:15:09+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-06-15-north-korea-created-very-phishy-evil-twin-of-naver-south-koreas-top-portal

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/15/north_korea_fake_naver_attack/){:target="_blank" rel="noopener"}

> Think of it as a fake Google tuned for credential capture and you'll understand why authorities want to kill it North Korea has created a fake version of South Korea's largest internet portal, Naver, in a large scale phishing attempt, Seoul's National Intelligence Service (NIS) said on Wednesday.... [...]
