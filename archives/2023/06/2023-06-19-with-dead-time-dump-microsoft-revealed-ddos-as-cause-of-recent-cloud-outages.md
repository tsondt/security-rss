Title: With dead-time dump, Microsoft revealed DDoS as cause of recent cloud outages
Date: 2023-06-19T00:32:15+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-06-19-with-dead-time-dump-microsoft-revealed-ddos-as-cause-of-recent-cloud-outages

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/19/microsoft_365_outage_ddos_cause/){:target="_blank" rel="noopener"}

> Previous claims its own software updates were the issue remain almost, kinda, plausible In the murky world of political and corporate spin, announcing bad news on Friday afternoon – a time when few media outlets are watching, and audiences are at a low ebb – is called "taking out the trash." And that’s what Microsoft appears to have done last Friday.... [...]
