Title: Anatsa Android trojan now steals banking info from users in US, UK
Date: 2023-06-26T13:21:29-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2023-06-26-anatsa-android-trojan-now-steals-banking-info-from-users-in-us-uk

[Source](https://www.bleepingcomputer.com/news/security/anatsa-android-trojan-now-steals-banking-info-from-users-in-us-uk/){:target="_blank" rel="noopener"}

> A new mobile malware campaign since March 2023 pushes the Android banking trojan 'Anatsa' to online banking customers in the U.S., the U.K., Germany, Austria, and Switzerland. [...]
