Title: Meet TeamT5, the Taiwanese infosec outfit taking on Beijing and defeating its smears
Date: 2023-06-05T02:33:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-06-05-meet-teamt5-the-taiwanese-infosec-outfit-taking-on-beijing-and-defeating-its-smears

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/05/teamt5_taiwan_threat_intelligence/){:target="_blank" rel="noopener"}

> Living in the eye of the geopolitical storm is not easy, but is good for business In late September 2021, staff at Taiwanese threat intelligence company TeamT5 noticed something very nasty: a fake news report accusing it of conducting phishing attacks against Japan's government and local tech companies.... [...]
