Title: Our commitment to shared cybersecurity goals
Date: 2023-06-07T17:45:16+00:00
Author: Mark Ryland
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Thought Leadership;cybersecurity;Cybersecurity awareness;Security Blog
Slug: 2023-06-07-our-commitment-to-shared-cybersecurity-goals

[Source](https://aws.amazon.com/blogs/security/our-commitment-to-shared-cybersecurity-goals/){:target="_blank" rel="noopener"}

> The United States Government recently launched its National Cybersecurity Strategy. The Strategy outlines the administration’s ambitious vision for building a more resilient future, both in the United States and around the world, and it affirms the key role cloud computing plays in realizing this vision. Amazon Web Services (AWS) is broadly committed to working with [...]
