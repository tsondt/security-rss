Title: Updated whitepaper available: Architecting for PCI DSS Segmentation and Scoping on AWS
Date: 2023-06-01T17:01:12+00:00
Author: Ted Tanner
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS;AWS Compliance;Compliance;PCI;PCI DSS;PII;Security architecture;Security Blog;Segmentation;Whitepaper
Slug: 2023-06-01-updated-whitepaper-available-architecting-for-pci-dss-segmentation-and-scoping-on-aws

[Source](https://aws.amazon.com/blogs/security/updated-whitepaper-available-architecting-for-pci-dss-segmentation-and-scoping-on-aws/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) has re-published the whitepaper Architecting for PCI DSS Scoping and Segmentation on AWS to provide guidance on how to properly define the scope of your Payment Card Industry (PCI) Data Security Standard (DSS) workloads that are running in the AWS Cloud. The whitepaper has been refreshed to include updated AWS best [...]
