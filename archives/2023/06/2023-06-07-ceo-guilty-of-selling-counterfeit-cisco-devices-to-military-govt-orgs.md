Title: CEO guilty of selling counterfeit Cisco devices to military, govt orgs
Date: 2023-06-07T10:19:17-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware;Legal
Slug: 2023-06-07-ceo-guilty-of-selling-counterfeit-cisco-devices-to-military-govt-orgs

[Source](https://www.bleepingcomputer.com/news/security/ceo-guilty-of-selling-counterfeit-cisco-devices-to-military-govt-orgs/){:target="_blank" rel="noopener"}

> A Florida man has pleaded guilty to importing and selling counterfeit Cisco networking equipment to various organizations, including education, government agencies, healthcare, and the military. [...]
