Title: Microsoft says share the wealth with cyber-info for business
Date: 2023-06-08T09:30:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-06-08-microsoft-says-share-the-wealth-with-cyber-info-for-business

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/08/microsoft_defender_security_posture/){:target="_blank" rel="noopener"}

> It's better to take action than wait for attacks The timeworn adage that "those who don't learn from history are doomed to repeat it" can certainly be applied to cyber security. Microsoft is hoping to spare enterprises that use its cloud services from repeating history by sharing what it has learned.... [...]
