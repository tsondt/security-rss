Title: CISA issues DDoS warning after attacks hit multiple US orgs
Date: 2023-06-30T12:24:37-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-30-cisa-issues-ddos-warning-after-attacks-hit-multiple-us-orgs

[Source](https://www.bleepingcomputer.com/news/security/cisa-issues-ddos-warning-after-attacks-hit-multiple-us-orgs/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity and Infrastructure Security Agency (CISA) warned today of ongoing distributed denial-of-service (DDoS) attacks after U.S. organizations across multiple industry sectors were hit. [...]
