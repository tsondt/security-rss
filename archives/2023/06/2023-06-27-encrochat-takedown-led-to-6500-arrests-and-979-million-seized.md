Title: EncroChat takedown led to 6,500 arrests and $979 million seized
Date: 2023-06-27T10:20:52-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-06-27-encrochat-takedown-led-to-6500-arrests-and-979-million-seized

[Source](https://www.bleepingcomputer.com/news/security/encrochat-takedown-led-to-6-500-arrests-and-979-million-seized/){:target="_blank" rel="noopener"}

> Europol announced today that the takedown of the EncroChat encrypted mobile communications platform has led to the arrest of over 6,600 people and the seizure of $979 million in illicit funds. [...]
