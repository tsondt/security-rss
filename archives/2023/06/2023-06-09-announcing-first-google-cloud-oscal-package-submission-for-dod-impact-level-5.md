Title: Announcing first Google Cloud OSCAL package submission for DoD Impact Level 5
Date: 2023-06-09T16:00:00+00:00
Author: Rachel Kim
Category: GCP Security
Tags: Public Sector;Security & Identity
Slug: 2023-06-09-announcing-first-google-cloud-oscal-package-submission-for-dod-impact-level-5

[Source](https://cloud.google.com/blog/products/identity-security/announcing-first-google-cloud-oscal-package-submission-for-dod-impact-level-5/){:target="_blank" rel="noopener"}

> Today, Google Cloud is proud to announce that we have successfully submitted the complete OSCAL package for Department of Defense (DoD) Impact Level 5 (IL5) to eMASS. This is a major milestone for us, as it represents our step forward supporting scalable compliance for Google Cloud and its customers. Open Security Control Assessment Language (OSCAL) OSCAL (Open Security Control Assessment Language) is an open, machine-readable language for representing security control assessments developed by NIST. It is designed to facilitate the exchange of information about security controls between organizations and systems, and enables the automation of security assessments. As organizations increasingly [...]
