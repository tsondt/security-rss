Title: Google Chrome password manager gets new safeguards for your credentials
Date: 2023-06-08T12:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2023-06-08-google-chrome-password-manager-gets-new-safeguards-for-your-credentials

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-password-manager-gets-new-safeguards-for-your-credentials/){:target="_blank" rel="noopener"}

> Google Chrome is getting new security-enhancing features for the built-in Password Manager, making it easier for users to manage their passwords and stay safe from account hijacking attacks. [...]
