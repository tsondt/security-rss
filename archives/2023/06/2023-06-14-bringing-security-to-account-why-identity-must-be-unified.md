Title: Bringing security to account: why identity must be unified
Date: 2023-06-14T10:35:08+00:00
Author: James Hayes
Category: The Register
Tags: 
Slug: 2023-06-14-bringing-security-to-account-why-identity-must-be-unified

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/14/bringing_security_to_account_why/){:target="_blank" rel="noopener"}

> As identity management becomes the new security perimeter, cyber risk underwriters want to see resilient IAM control ID sprawl Sponsored Feature Many organizations are suffering from an identity crisis. Not in the psychological sense, nor in respect to their branding or culture. But in how their IT systems enable employees to access the applications and data they need for work.... [...]
