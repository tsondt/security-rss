Title: Microsoft 365 users report Outlook, Teams won't start or freezes
Date: 2023-06-22T17:19:16-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-06-22-microsoft-365-users-report-outlook-teams-wont-start-or-freezes

[Source](https://www.bleepingcomputer.com/news/security/microsoft-365-users-report-outlook-teams-wont-start-or-freezes/){:target="_blank" rel="noopener"}

> Network and IT admins have been dealing with ongoing Microsoft 365 issues this week, reporting that some end users cannot use Microsoft Outlook or other Microsoft 365 apps. [...]
