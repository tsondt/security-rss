Title: DuckDuckGo browser for Windows available for everyone as public beta
Date: 2023-06-22T08:01:11-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2023-06-22-duckduckgo-browser-for-windows-available-for-everyone-as-public-beta

[Source](https://www.bleepingcomputer.com/news/security/duckduckgo-browser-for-windows-available-for-everyone-as-public-beta/){:target="_blank" rel="noopener"}

> DuckDuckGo has released its privacy-centric browser for Windows to the general public. It is a beta version available for download with no restrictions. [...]
