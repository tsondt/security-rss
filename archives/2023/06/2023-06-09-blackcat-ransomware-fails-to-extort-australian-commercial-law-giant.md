Title: BlackCat ransomware fails to extort Australian commercial law giant
Date: 2023-06-09T11:11:49-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-09-blackcat-ransomware-fails-to-extort-australian-commercial-law-giant

[Source](https://www.bleepingcomputer.com/news/security/blackcat-ransomware-fails-to-extort-australian-commercial-law-giant/){:target="_blank" rel="noopener"}

> Australian law firm HWL Ebsworth confirmed to local media outlets that its network was hacked after the ALPHV ransomware gang began leaking data they claim was stolen from the company. [...]
