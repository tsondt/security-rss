Title: Dark Pink cyber-spies add info stealers to their arsenal, notch up more victims
Date: 2023-06-01T01:24:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-01-dark-pink-cyber-spies-add-info-stealers-to-their-arsenal-notch-up-more-victims

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/01/dark_pink_cyber_spies/){:target="_blank" rel="noopener"}

> Not to be confused with K-Pop sensation BLACKPINK, gang pops military, govt and education orgs Dark Pink, a suspected nation-state-sponsored cyber-espionage group, has expanded its list of targeted organizations, both geographically and by sector, and has carried out at least two attacks since the beginning of the year.... [...]
