Title: Man charged in US for running 'Monopoly' darknet drug market
Date: 2023-06-26T09:54:05-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-26-man-charged-in-us-for-running-monopoly-darknet-drug-market

[Source](https://www.bleepingcomputer.com/news/security/man-charged-in-us-for-running-monopoly-darknet-drug-market/){:target="_blank" rel="noopener"}

> A 33-year-old man from Serbia has been extradited from Austria to the United States to face charges of running a criminal darknet narcotics marketplace called "Monopoly Market." [...]
