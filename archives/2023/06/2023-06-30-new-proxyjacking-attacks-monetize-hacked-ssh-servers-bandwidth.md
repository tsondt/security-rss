Title: New proxyjacking attacks monetize hacked SSH servers’ bandwidth
Date: 2023-06-30T14:47:52-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-30-new-proxyjacking-attacks-monetize-hacked-ssh-servers-bandwidth

[Source](https://www.bleepingcomputer.com/news/security/new-proxyjacking-attacks-monetize-hacked-ssh-servers-bandwidth/){:target="_blank" rel="noopener"}

> Attackers behind an ongoing series of proxyjacking attacks are hacking into vulnerable SSH servers exposed online to monetize them through services that pay for sharing unused Internet bandwidth. [...]
