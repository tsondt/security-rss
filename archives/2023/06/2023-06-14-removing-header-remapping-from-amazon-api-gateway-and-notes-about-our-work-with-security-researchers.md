Title: Removing header remapping from Amazon API Gateway, and notes about our work with security researchers
Date: 2023-06-14T17:59:49+00:00
Author: Mark Ryland
Category: AWS Security
Tags: Announcements;Intermediate (200);Security, Identity, & Compliance;Thought Leadership
Slug: 2023-06-14-removing-header-remapping-from-amazon-api-gateway-and-notes-about-our-work-with-security-researchers

[Source](https://aws.amazon.com/blogs/security/removing-header-remapping-from-amazon-api-gateway-and-notes-about-our-work-with-security-researchers/){:target="_blank" rel="noopener"}

> At Amazon Web Services (AWS), our APIs and service functionality are a promise to our customers, so we very rarely make breaking changes or remove functionality from production services. Customers use the AWS Cloud to build solutions for their customers, and when disruptive changes are made or functionality is removed, the downstream impacts can be [...]
