Title: Russians charged with hacking Mt. Gox crypto exchange, running BTC-e
Date: 2023-06-09T11:57:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-06-09-russians-charged-with-hacking-mt-gox-crypto-exchange-running-btc-e

[Source](https://www.bleepingcomputer.com/news/security/russians-charged-with-hacking-mt-gox-crypto-exchange-running-btc-e/){:target="_blank" rel="noopener"}

> Russian nationals Alexey Bilyuchenko and Aleksandr Verner have been charged with the 2011 hacking of the leading (at the time) cryptocurrency exchange Mt. Gox and the laundering of around 647,000 bitcoins they stole. [...]
