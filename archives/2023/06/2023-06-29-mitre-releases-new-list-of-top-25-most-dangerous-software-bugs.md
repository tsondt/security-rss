Title: MITRE releases new list of top 25 most dangerous software bugs
Date: 2023-06-29T12:28:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-29-mitre-releases-new-list-of-top-25-most-dangerous-software-bugs

[Source](https://www.bleepingcomputer.com/news/security/mitre-releases-new-list-of-top-25-most-dangerous-software-bugs/){:target="_blank" rel="noopener"}

> MITRE shared today this year's list of the top 25 most dangerous weaknesses plaguing software during the previous two years. [...]
