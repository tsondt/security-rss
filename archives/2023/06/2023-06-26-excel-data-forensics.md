Title: Excel Data Forensics
Date: 2023-06-26T15:36:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic;data mining;Microsoft;plagiarism
Slug: 2023-06-26-excel-data-forensics

[Source](https://www.schneier.com/blog/archives/2023/06/excel-data-forensics.html){:target="_blank" rel="noopener"}

> In this detailed article about academic plagiarism are some interesting details about how to do data forensics on Excel files. It really needs the graphics to understand, so see the description at the link. (And, yes, an author of a paper on dishonesty is being accused of dishonesty. There’s more evidence.) [...]
