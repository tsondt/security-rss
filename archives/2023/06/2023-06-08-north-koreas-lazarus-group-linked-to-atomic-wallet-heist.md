Title: North Korea's Lazarus Group linked to Atomic Wallet heist
Date: 2023-06-08T23:04:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-06-08-north-koreas-lazarus-group-linked-to-atomic-wallet-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/08/lazarus_link_atomic_wallet/){:target="_blank" rel="noopener"}

> Users' cryptocurrency wallets look unlikely to be refilled The North Korean criminal gang Lazarus Group has been blamed for last weekend's attack on Atomic Wallet that drained at least $35 million in cryptocurrency from private accounts.... [...]
