Title: Grafana warns of critical auth bypass due to Azure AD integration
Date: 2023-06-24T11:18:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-24-grafana-warns-of-critical-auth-bypass-due-to-azure-ad-integration

[Source](https://www.bleepingcomputer.com/news/security/grafana-warns-of-critical-auth-bypass-due-to-azure-ad-integration/){:target="_blank" rel="noopener"}

> Grafana has released security fixes for multiple versions of its application, addressing a vulnerability that enables attackers to bypass authentication and take over any Grafana account that uses Azure Active Directory for authentication. [...]
