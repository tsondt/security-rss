Title: Deepfakes being used in ‘sextortion’ scams, FBI warns
Date: 2023-06-08T00:45:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-06-08-deepfakes-being-used-in-sextortion-scams-fbi-warns

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/08/ai_deepfakes_sextortion_fbi/){:target="_blank" rel="noopener"}

> AI technology raises the bar in an already troubling crime Miscreants are using AI to create faked images of a sexual nature, then using them in sextortion schemes.... [...]
