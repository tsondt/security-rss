Title: Fortinet fixes critical FortiNAC remote command execution flaw
Date: 2023-06-23T08:42:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-23-fortinet-fixes-critical-fortinac-remote-command-execution-flaw

[Source](https://www.bleepingcomputer.com/news/security/fortinet-fixes-critical-fortinac-remote-command-execution-flaw/){:target="_blank" rel="noopener"}

> Cybersecurity solutions company Fortinet has updated its zero-trust access solution FortiNAC to address a critical-severity vulnerability that attackers could leverage to execute code and commands. [...]
