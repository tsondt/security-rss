Title: Microsoft confirms Azure, Outlook outages caused by DDoS attacks
Date: 2023-06-18T10:40:16-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-06-18-microsoft-confirms-azure-outlook-outages-caused-by-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-confirms-azure-outlook-outages-caused-by-ddos-attacks/){:target="_blank" rel="noopener"}

> Microsoft has confirmed that recent outages to Azure, Outlook, and OneDrive web portals resulted from Layer 7 DDoS attacks against the company's services. [...]
