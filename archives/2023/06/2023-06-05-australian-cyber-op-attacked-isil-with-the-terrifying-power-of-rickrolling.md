Title: Australian cyber-op attacked ISIL with the terrifying power of Rickrolling
Date: 2023-06-05T04:29:50+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-06-05-australian-cyber-op-attacked-isil-with-the-terrifying-power-of-rickrolling

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/05/australian_cyber_offense_techniques_revealed/){:target="_blank" rel="noopener"}

> Commanders in the field persuaded to give up, let their guard down, run around and desert their posts Australia's Signals Directorate, the signals intelligence organization, has revealed it employed zero-click attacks on devices used by fighters for Islamic State of Iraq and the Levant (ISIL) – then unleashed the terrifying power of Rick Astley.... [...]
