Title: CISA orders govt agencies to patch MOVEit bug used for data theft
Date: 2023-06-04T11:14:23-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-04-cisa-orders-govt-agencies-to-patch-moveit-bug-used-for-data-theft

[Source](https://www.bleepingcomputer.com/news/security/cisa-orders-govt-agencies-to-patch-moveit-bug-used-for-data-theft/){:target="_blank" rel="noopener"}

> CISA has added an actively exploited security bug in the Progress MOVEit Transfer managed file transfer (MFT) solution to its list of known exploited vulnerabilities, warning U.S. federal agencies to patch their systems by June 23. [...]
