Title: Microsoft stashes nearly half a billion in case LinkedIn data drama hits
Date: 2023-06-02T15:28:13+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-06-02-microsoft-stashes-nearly-half-a-billion-in-case-linkedin-data-drama-hits

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/02/microsoft_linkedin_fine_potential/){:target="_blank" rel="noopener"}

> Irish regulators sniffing around Facebook-for-suits subsidiary have threatened fine Microsoft has warned investors about a "non-public" draft decision by Irish regulators against LinkedIn for allegedly dodgy ad data practices, explaining it had set aside some cash to pay off any potential fine.... [...]
