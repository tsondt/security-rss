Title: WordPress Stripe payment plugin bug leaks customer order details
Date: 2023-06-13T12:02:58-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-13-wordpress-stripe-payment-plugin-bug-leaks-customer-order-details

[Source](https://www.bleepingcomputer.com/news/security/wordpress-stripe-payment-plugin-bug-leaks-customer-order-details/){:target="_blank" rel="noopener"}

> The WooCommerce Stripe Gateway plugin for WordPress was found to be vulnerable to a bug that allows any unauthenticated user to view order details placed through the plugin. [...]
