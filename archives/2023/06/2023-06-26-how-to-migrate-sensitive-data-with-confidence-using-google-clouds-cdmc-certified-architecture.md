Title: How to migrate sensitive data with confidence using Google Cloud’s CDMC-certified architecture
Date: 2023-06-26T16:00:00+00:00
Author: Mark Tomlinson
Category: GCP Security
Tags: Security & Identity
Slug: 2023-06-26-how-to-migrate-sensitive-data-with-confidence-using-google-clouds-cdmc-certified-architecture

[Source](https://cloud.google.com/blog/products/identity-security/how-to-migrate-sensitive-data-using-google-clouds-cdmc-certified-architecture/){:target="_blank" rel="noopener"}

> As enterprises accelerate adoption of cloud-based services and products, they face a common challenge: How can they effectively secure and govern rapidly expanding volumes of their most sensitive data in new environments? Today, Google Cloud released an architectural whitepaper and accompanying source code for a solution which successfully completed an assessment facilitated by KPMG LLP and has been accredited by the Enterprise Data Management Council (EDM Council) as a Cloud Data Management Capabilities (CDMC) Certified Cloud Solution. The architecture, which includes Google Cloud’s BigQuery and Dataplex Data Catalog, has been validated against CDMC’s control framework and can be used by [...]
