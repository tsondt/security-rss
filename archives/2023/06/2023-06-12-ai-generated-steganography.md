Title: AI-Generated Steganography
Date: 2023-06-12T11:18:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence;cryptography;encryption;steganography
Slug: 2023-06-12-ai-generated-steganography

[Source](https://www.schneier.com/blog/archives/2023/06/ai-generated-steganography.html){:target="_blank" rel="noopener"}

> New research suggests that AIs can produce perfectly secure steganographic images: Abstract: Steganography is the practice of encoding secret information into innocuous content in such a manner that an adversarial third party would not realize that there is hidden meaning. While this problem has classically been studied in security literature, recent advances in generative models have led to a shared interest among security and machine learning researchers in developing scalable steganography techniques. In this work, we show that a steganography procedure is perfectly secure under Cachin (1998)’s information theoretic-model of steganography if and only if it is induced by a [...]
