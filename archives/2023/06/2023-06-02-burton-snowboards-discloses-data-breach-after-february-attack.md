Title: Burton Snowboards discloses data breach after February attack
Date: 2023-06-02T12:19:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-02-burton-snowboards-discloses-data-breach-after-february-attack

[Source](https://www.bleepingcomputer.com/news/security/burton-snowboards-discloses-data-breach-after-february-attack/){:target="_blank" rel="noopener"}

> Leading snowboard maker Burton Snowboards confirmed notified customers of a data breach after some of their sensitive information was "potentially" accessed or stolen during what the company described in February as a "cyber incident." [...]
