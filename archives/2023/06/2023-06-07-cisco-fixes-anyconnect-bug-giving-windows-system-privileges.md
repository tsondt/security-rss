Title: Cisco fixes AnyConnect bug giving Windows SYSTEM privileges
Date: 2023-06-07T14:29:50-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-07-cisco-fixes-anyconnect-bug-giving-windows-system-privileges

[Source](https://www.bleepingcomputer.com/news/security/cisco-fixes-anyconnect-bug-giving-windows-system-privileges/){:target="_blank" rel="noopener"}

> Cisco has fixed a high-severity vulnerability found in Cisco Secure Client (formerly AnyConnect Secure Mobility Client) software that can let attackers escalate privileges to the SYSTEM account used by the operating system. [...]
