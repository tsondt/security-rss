Title: Microsoft: Hackers hijack Linux systems using trojanized OpenSSH version
Date: 2023-06-22T13:33:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-06-22-microsoft-hackers-hijack-linux-systems-using-trojanized-openssh-version

[Source](https://www.bleepingcomputer.com/news/security/microsoft-hackers-hijack-linux-systems-using-trojanized-openssh-version/){:target="_blank" rel="noopener"}

> Microsoft says Internet-exposed Linux and Internet of Things (IoT) devices are being hijacked in brute-force attacks as part of a recently observed cryptojacking campaign. [...]
