Title: Florida man insists he didn't violate the law by keeping Top Secret docs
Date: 2023-06-14T00:30:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-06-14-florida-man-insists-he-didnt-violate-the-law-by-keeping-top-secret-docs

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/14/florida_man_data/){:target="_blank" rel="noopener"}

> Populist politician pleads not guilty at Miami arraignment A Florida man and his valet appeared in a Miami federal courtroom on Tuesday to respond to criminal charges of document hoarding and related claims.... [...]
