Title: Chinese hackers used VMware ESXi zero-day to backdoor VMs
Date: 2023-06-13T12:48:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-13-chinese-hackers-used-vmware-esxi-zero-day-to-backdoor-vms

[Source](https://www.bleepingcomputer.com/news/security/chinese-hackers-used-vmware-esxi-zero-day-to-backdoor-vms/){:target="_blank" rel="noopener"}

> VMware patched today a VMware ESXi zero-day vulnerability exploited by a Chinese-sponsored hacking group to backdoor Windows and Linux virtual machines and steal data. [...]
