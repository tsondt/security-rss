Title: US government hit by Russia's Clop in MOVEit mass attack
Date: 2023-06-15T22:43:36+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-15-us-government-hit-by-russias-clop-in-moveit-mass-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/15/clop_broke_into_the_doe/){:target="_blank" rel="noopener"}

> CISA chief tells us exploitation 'largely opportunistic', not on same level of SolarWinds The US Department of Energy and other federal bodies are among a growing list of organizations hit by Russians exploiting the MOVEit file-transfer vulnerability.... [...]
