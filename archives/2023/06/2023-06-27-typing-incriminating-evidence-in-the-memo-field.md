Title: Typing Incriminating Evidence in the Memo Field
Date: 2023-06-27T20:36:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;crime;forensics
Slug: 2023-06-27-typing-incriminating-evidence-in-the-memo-field

[Source](https://www.schneier.com/blog/archives/2023/06/typing-incriminating-evidence-in-the-memo-field.html){:target="_blank" rel="noopener"}

> Don’t do it : Recently, the manager of the Harvard Med School morgue was accused of stealing and selling human body parts. Cedric Lodge and his wife Denise were among a half-dozen people arrested for some pretty grotesque crimes. This part is also at least a little bit funny though: Over a three-year period, Taylor appeared to pay Denise Lodge more than $37,000 for human remains. One payment, for $1,000 included the memo “head number 7.” Another, for $200, read “braiiiiiins.” It’s so easy to think that you won’t get caught. [...]
