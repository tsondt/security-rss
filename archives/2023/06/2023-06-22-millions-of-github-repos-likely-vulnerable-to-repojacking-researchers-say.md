Title: Millions of GitHub repos likely vulnerable to RepoJacking, researchers say
Date: 2023-06-22T11:45:30-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-22-millions-of-github-repos-likely-vulnerable-to-repojacking-researchers-say

[Source](https://www.bleepingcomputer.com/news/security/millions-of-github-repos-likely-vulnerable-to-repojacking-researchers-say/){:target="_blank" rel="noopener"}

> Millions of GitHub repositories may be vulnerable to dependency repository hijacking, also known as "RepoJacking," which could help attackers deploy supply chain attacks impacting a large number of users. [...]
