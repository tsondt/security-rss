Title: Clop ransomware gang starts extorting MOVEit data-theft victims
Date: 2023-06-15T11:39:03-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-06-15-clop-ransomware-gang-starts-extorting-moveit-data-theft-victims

[Source](https://www.bleepingcomputer.com/news/security/clop-ransomware-gang-starts-extorting-moveit-data-theft-victims/){:target="_blank" rel="noopener"}

> The Clop ransomware gang has started extorting companies impacted by the MOVEit data theft attacks by listing them on a data leak site, a common extortion tactic used as a precursor for the public leaking of stolen data. [...]
