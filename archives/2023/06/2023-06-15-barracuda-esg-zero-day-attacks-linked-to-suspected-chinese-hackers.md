Title: Barracuda ESG zero-day attacks linked to suspected Chinese hackers
Date: 2023-06-15T09:25:48-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-15-barracuda-esg-zero-day-attacks-linked-to-suspected-chinese-hackers

[Source](https://www.bleepingcomputer.com/news/security/barracuda-esg-zero-day-attacks-linked-to-suspected-chinese-hackers/){:target="_blank" rel="noopener"}

> A suspected pro-China hacker group tracked by Mandiant as UNC4841 has been linked to data-theft attacks on Barracuda ESG (Email Security Gateway) appliances using a now-patched zero-day vulnerability. [...]
