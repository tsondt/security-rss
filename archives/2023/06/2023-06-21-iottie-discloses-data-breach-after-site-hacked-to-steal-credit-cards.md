Title: iOttie discloses data breach after site hacked to steal credit cards
Date: 2023-06-21T18:01:32-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-06-21-iottie-discloses-data-breach-after-site-hacked-to-steal-credit-cards

[Source](https://www.bleepingcomputer.com/news/security/iottie-discloses-data-breach-after-site-hacked-to-steal-credit-cards/){:target="_blank" rel="noopener"}

> Car mount and mobile accessory maker iOttie warns that its site was compromised for almost two months to steal online shoppers' credit cards and personal information. [...]
