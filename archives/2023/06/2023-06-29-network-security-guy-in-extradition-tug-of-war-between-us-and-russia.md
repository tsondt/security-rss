Title: Network security guy in extradition tug of war between US and Russia
Date: 2023-06-29T00:58:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-29-network-security-guy-in-extradition-tug-of-war-between-us-and-russia

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/29/russian_facct_employee_extradiation/){:target="_blank" rel="noopener"}

> Group-IB spinout confirms Kislitsin is wanted by both Washington and Moscow A Russian network security specialist and former editor of Hacker magazine who is wanted by the US and Russia on cybercrime charges has been detained in Kazakhstan as the two governments seek his extradition.... [...]
