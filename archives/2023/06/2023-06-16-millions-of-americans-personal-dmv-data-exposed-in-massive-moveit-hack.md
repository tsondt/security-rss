Title: Millions of Americans’ personal DMV data exposed in massive MOVEit hack
Date: 2023-06-16T16:28:35+00:00
Author: Benj Edwards
Category: Ars Technica
Tags: Biz & IT;Security;hacks;infosec;IT
Slug: 2023-06-16-millions-of-americans-personal-dmv-data-exposed-in-massive-moveit-hack

[Source](https://arstechnica.com/?p=1948548){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) As part of a massive ongoing cyberattack that exploits flaws in MOVEit file transfer software, the personal data of millions of US citizens, including those residing in Louisiana and Oregon, have been exposed to criminal organizations, according to CNN. In the wider attack, hackers targeted government agencies as well as multiple global organizations, causing a breach that extends beyond US boundaries. While the effects of the MOVEit hack have been ongoing throughout the month of June, the most recent intrusion has hit over 3.5 million residents of Oregon and potentially over 3 million residents of Louisiana, [...]
