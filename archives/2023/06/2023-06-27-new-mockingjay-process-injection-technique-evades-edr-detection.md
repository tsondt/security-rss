Title: New Mockingjay process injection technique evades EDR detection
Date: 2023-06-27T09:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-27-new-mockingjay-process-injection-technique-evades-edr-detection

[Source](https://www.bleepingcomputer.com/news/security/new-mockingjay-process-injection-technique-evades-edr-detection/){:target="_blank" rel="noopener"}

> A new process injection technique named 'Mockingjay' could allow threat actors to bypass EDR (Endpoint Detection and Response) and other security products to stealthily execute malicious code on compromised systems. [...]
