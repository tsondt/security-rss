Title: Keep it schtum!
Date: 2023-06-23T08:53:07+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-06-23-keep-it-schtum

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/23/keep_it_schtum/){:target="_blank" rel="noopener"}

> Ensuring communications stay secure Webinar The explosion in remote working since the pandemic means the number of people doing their job from home has more than doubled in the UK.... [...]
