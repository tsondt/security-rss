Title: You might have been phished by the gang that stole North Korea’s lousy rocket tech
Date: 2023-06-02T05:15:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-06-02-you-might-have-been-phished-by-the-gang-that-stole-north-koreas-lousy-rocket-tech

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/02/us_south_korea_kimsuky_warning/){:target="_blank" rel="noopener"}

> US, South Korea, warn 'Kimsuky' is a very sophisticated social engineer The United States and the Republic of Korea have issued a joint cyber security advisory [PDF] about North Koreas "Kimsuky" cyber crime group.... [...]
