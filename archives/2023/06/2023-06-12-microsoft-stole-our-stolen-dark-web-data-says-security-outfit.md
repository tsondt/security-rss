Title: Microsoft stole our stolen dark web data, says security outfit
Date: 2023-06-12T19:15:33+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-06-12-microsoft-stole-our-stolen-dark-web-data-says-security-outfit

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/12/microsoft_hold_security_lawsuit/){:target="_blank" rel="noopener"}

> Suit claims Redmond took far more than allowed from Hold's 360M-credential database Microsoft stands accused by cyber intelligence firm Hold Security of violating an agreement between the pair by misusing Hold's database of more than 360 million sets of credentials culled from the dark web.... [...]
