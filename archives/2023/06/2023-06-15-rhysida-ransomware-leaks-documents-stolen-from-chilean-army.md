Title: Rhysida ransomware leaks documents stolen from Chilean Army
Date: 2023-06-15T18:42:34-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-15-rhysida-ransomware-leaks-documents-stolen-from-chilean-army

[Source](https://www.bleepingcomputer.com/news/security/rhysida-ransomware-leaks-documents-stolen-from-chilean-army/){:target="_blank" rel="noopener"}

> Threat actors behind a recently surfaced ransomware operation known as Rhysida have leaked online what they claim to be documents stolen from the network of the Chilean Army (Ejército de Chile). [...]
