Title: Zyxel shares tips on protecting firewalls from ongoing attacks
Date: 2023-06-03T10:06:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-03-zyxel-shares-tips-on-protecting-firewalls-from-ongoing-attacks

[Source](https://www.bleepingcomputer.com/news/security/zyxel-shares-tips-on-protecting-firewalls-from-ongoing-attacks/){:target="_blank" rel="noopener"}

> Zyxel has published a security advisory containing guidance on protecting firewall and VPN devices from ongoing attacks and detecting signs of exploitation. [...]
