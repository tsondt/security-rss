Title: New MOVEit Transfer critical flaws found after security audit, patch now
Date: 2023-06-09T14:49:06-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-09-new-moveit-transfer-critical-flaws-found-after-security-audit-patch-now

[Source](https://www.bleepingcomputer.com/news/security/new-moveit-transfer-critical-flaws-found-after-security-audit-patch-now/){:target="_blank" rel="noopener"}

> Progress Software warned customers today of newly found critical SQL injection vulnerabilities in its MOVEit Transfer managed file transfer (MFT) solution that can let attackers steal information from customers' databases. [...]
