Title: China's cyber now aimed at infrastructure, warns CISA boss
Date: 2023-06-13T04:45:56+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-06-13-chinas-cyber-now-aimed-at-infrastructure-warns-cisa-boss

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/13/china_cyber_threat_infrastructure/){:target="_blank" rel="noopener"}

> Resilience against threats needs a boost China's cyber-ops against the US have shifted from espionage activities to targeting infrastructure and societal disruption, the director of the Cybersecurity and Infrastructure Security Agency (CISA) Jen Easterly told an Aspen Institute event on Monday.... [...]
