Title: Stalkerware Vendor Hacked
Date: 2023-06-28T11:17:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;data breaches;hacking;stalking
Slug: 2023-06-28-stalkerware-vendor-hacked

[Source](https://www.schneier.com/blog/archives/2023/06/stalkerware-vendor-hacked.html){:target="_blank" rel="noopener"}

> The stalkerware company LetMeSpy has been hacked : TechCrunch reviewed the leaked data, which included years of victims’ call logs and text messages dating back to 2013. The database we reviewed contained current records on at least 13,000 compromised devices, though some of the devices shared little to no data with LetMeSpy. (LetMeSpy claims to delete data after two months of account inactivity.) [...] The database also contained over 13,400 location data points for several thousand victims. Most of the location data points are centered over population hotspots, suggesting the majority of victims are located in the United States, India [...]
