Title: Harvard Pilgrim Health Care ransomware attack hits 2.5 million people
Date: 2023-06-01T13:02:54-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-06-01-harvard-pilgrim-health-care-ransomware-attack-hits-25-million-people

[Source](https://www.bleepingcomputer.com/news/security/harvard-pilgrim-health-care-ransomware-attack-hits-25-million-people/){:target="_blank" rel="noopener"}

> Harvard Pilgrim Health Care (HPHC) has disclosed that a ransomware attack it suffered in April 2023 impacted 2,550,922 people, with the threat actors also stealing their sensitive data from compromised systems. [...]
