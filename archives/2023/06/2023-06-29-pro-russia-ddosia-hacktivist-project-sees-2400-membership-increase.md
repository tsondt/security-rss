Title: Pro-Russia DDoSia hacktivist project sees 2,400% membership increase
Date: 2023-06-29T11:40:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-29-pro-russia-ddosia-hacktivist-project-sees-2400-membership-increase

[Source](https://www.bleepingcomputer.com/news/security/pro-russia-ddosia-hacktivist-project-sees-2-400-percent-membership-increase/){:target="_blank" rel="noopener"}

> The pro-Russia crowdsourced DDoS (distributed denial of service) project, 'DDoSia,' has seen a massive 2,400% growth in less than a year, with over ten thousand people helping conduct attacks on Western organizations. [...]
