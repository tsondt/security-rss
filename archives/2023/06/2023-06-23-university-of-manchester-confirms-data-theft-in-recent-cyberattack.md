Title: University of Manchester confirms data theft in recent cyberattack
Date: 2023-06-23T15:21:27-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-23-university-of-manchester-confirms-data-theft-in-recent-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/university-of-manchester-confirms-data-theft-in-recent-cyberattack/){:target="_blank" rel="noopener"}

> The University of Manchester finally confirmed that attackers behind a cyberattack disclosed in early June had stolen data belonging to alumni and current students. [...]
