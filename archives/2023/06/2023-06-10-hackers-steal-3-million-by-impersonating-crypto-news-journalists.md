Title: Hackers steal $3 million by impersonating crypto news journalists
Date: 2023-06-10T10:09:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: CryptoCurrency;Security
Slug: 2023-06-10-hackers-steal-3-million-by-impersonating-crypto-news-journalists

[Source](https://www.bleepingcomputer.com/news/cryptocurrency/hackers-steal-3-million-by-impersonating-crypto-news-journalists/){:target="_blank" rel="noopener"}

> A hacking group tracked as 'Pink Drainer' is impersonating journalists in phishing attacks to compromise Discord and Twitter accounts for cryptocurrency-stealing attacks. [...]
