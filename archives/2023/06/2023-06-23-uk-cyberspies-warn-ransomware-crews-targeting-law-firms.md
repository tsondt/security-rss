Title: UK cyberspies warn ransomware crews targeting law firms
Date: 2023-06-23T12:09:09+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-06-23-uk-cyberspies-warn-ransomware-crews-targeting-law-firms

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/23/ransomware_law_firms/){:target="_blank" rel="noopener"}

> Nation states will use you to get to your friends, says NCSC British law practices of "all sizes and types" have been warned by GCHQ's cyberspy arm that their "widespread adoption of hybrid working" combined with the large sums of money they handle is making them a target.... [...]
