Title: Robot can rip the data out of RAM chips with chilling technology
Date: 2023-06-09T00:01:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-06-09-robot-can-rip-the-data-out-of-ram-chips-with-chilling-technology

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/09/cold_boot_ram_theft/){:target="_blank" rel="noopener"}

> 'The more important a thing is for the world, the less security it has' says inventor Cold boot attacks, in which memory chips can be chilled and data including encryption keys plundered, were demonstrated way back in 2008 – but they just got automated.... [...]
