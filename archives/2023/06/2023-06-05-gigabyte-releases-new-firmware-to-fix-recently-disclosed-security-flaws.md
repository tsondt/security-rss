Title: GIGABYTE releases new firmware to fix recently disclosed security flaws
Date: 2023-06-05T11:09:13-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-06-05-gigabyte-releases-new-firmware-to-fix-recently-disclosed-security-flaws

[Source](https://www.bleepingcomputer.com/news/security/gigabyte-releases-new-firmware-to-fix-recently-disclosed-security-flaws/){:target="_blank" rel="noopener"}

> GIGABYTE has released firmware updates to fix security vulnerabilities in over 270 motherboards that could be exploited to install malware. [...]
