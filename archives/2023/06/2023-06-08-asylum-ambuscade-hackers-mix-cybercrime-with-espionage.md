Title: Asylum Ambuscade hackers mix cybercrime with espionage
Date: 2023-06-08T15:21:33-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-08-asylum-ambuscade-hackers-mix-cybercrime-with-espionage

[Source](https://www.bleepingcomputer.com/news/security/asylum-ambuscade-hackers-mix-cybercrime-with-espionage/){:target="_blank" rel="noopener"}

> A hacking group tracked as 'Asylum Ambuscade' was observed in recent attacks targeting small to medium-sized companies worldwide, combining cyber espionage with cybercrime. [...]
