Title: Fujitsu admits it fluffed the fix for Japan’s flaky ID card scheme
Date: 2023-06-30T01:47:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-06-30-fujitsu-admits-it-fluffed-the-fix-for-japans-flaky-id-card-scheme

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/30/fujitsu_japan_micjet_id_card_pause/){:target="_blank" rel="noopener"}

> Yet another snafu for digital services push Fujitsu Japan is in the spotlight again for all the wrong reasons, after fumbling its attempt to fix the nation's troubled ID card scheme.... [...]
