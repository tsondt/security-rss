Title: Decision to hold women-in-cyber events in abortion-banning states sparks outcry
Date: 2023-06-14T23:48:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-14-decision-to-hold-women-in-cyber-events-in-abortion-banning-states-sparks-outcry

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/14/wicys_conference_backlash/){:target="_blank" rel="noopener"}

> 'Many factors were considered,' WyCiS boss tells The Reg as (ISC)2 suggests an end to 'girlfriend test' jargon Global nonprofit Women in Cybersecurity (WiCyS), despite months of controversy over the cities named to host its 2024 and 2025 conferences, says it will move forward as planned with the events in Nashville, Tennessee, and Dallas, Texas, respectively.... [...]
