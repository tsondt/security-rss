Title: Blink Copilot Brings Generative AI to Security Automation
Date: 2023-06-13T10:04:08-04:00
Author: Sponsored by Blink Ops
Category: BleepingComputer
Tags: Security
Slug: 2023-06-13-blink-copilot-brings-generative-ai-to-security-automation

[Source](https://www.bleepingcomputer.com/news/security/blink-copilot-brings-generative-ai-to-security-automation/){:target="_blank" rel="noopener"}

> Blink Copilot - a true no-code platform for automating security and IT operations workflows. It is now possible for any security professional to generate automated workflows by just typing a prompt. [...]
