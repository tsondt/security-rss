Title: The US Navy, NATO, and NASA are using a shady Chinese company’s encryption chips
Date: 2023-06-16T19:41:17+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Security;encryption;microprocessors;NASA;Nato;syndication;US Navy
Slug: 2023-06-16-the-us-navy-nato-and-nasa-are-using-a-shady-chinese-companys-encryption-chips

[Source](https://arstechnica.com/?p=1948695){:target="_blank" rel="noopener"}

> Enlarge (credit: Bet_Noire/Getty ) From TikTok to Huawei routers to DJI drones, rising tensions between China and the US have made Americans—and the US government—increasingly wary of Chinese-owned technologies. But thanks to the complexity of the hardware supply chain, encryption chips sold by the subsidiary of a company specifically flagged in warnings from the US Department of Commerce for its ties to the Chinese military have found their way into the storage hardware of military and intelligence networks across the West. In July of 2021, the Commerce Department's Bureau of Industry and Security added the Hangzhou, China-based encryption chip manufacturer [...]
