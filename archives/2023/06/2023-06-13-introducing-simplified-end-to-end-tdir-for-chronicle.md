Title: Introducing simplified end-to-end TDIR for Chronicle
Date: 2023-06-13T16:00:00+00:00
Author: Kristen Cooper
Category: GCP Security
Tags: Security & Identity
Slug: 2023-06-13-introducing-simplified-end-to-end-tdir-for-chronicle

[Source](https://cloud.google.com/blog/products/identity-security/introducing-simplified-end-to-end-tdir-for-chronicle/){:target="_blank" rel="noopener"}

> As cloud adoption continues to grow, so too does the number of cloud-born security threats. However, cloud environments can present significant opportunities to improve security with the right tools and processes in place. When it comes to effective threat detection, investigation and response (TDIR) in the cloud, modern solutions must ensure that the entire security operations workflow — from data analysis through detection to response — are working in tandem to deliver the insights, context, and processes needed for cyber defenders to respond to threats with speed and precision. At Google Cloud, we believe that modern security operations should rely [...]
