Title: Ethical Problems in Computer Security
Date: 2023-06-21T17:54:35+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;computer security;ethics
Slug: 2023-06-21-ethical-problems-in-computer-security

[Source](https://www.schneier.com/blog/archives/2023/06/ethical-problems-in-computer-security.html){:target="_blank" rel="noopener"}

> Tadayoshi Kohno, Yasemin Acar, and Wulf Loh wrote excellent paper on ethical thinking within the computer security community: “ Ethical Frameworks and Computer Security Trolley Problems: Foundations for Conversation “: Abstract: The computer security research community regularly tackles ethical questions. The field of ethics / moral philosophy has for centuries considered what it means to be “morally good” or at least “morally allowed / acceptable.” Among philosophy’s contributions are (1) frameworks for evaluating the morality of actions—including the well-established consequentialist and deontological frameworks—and (2) scenarios (like trolley problems) featuring moral dilemmas that can facilitate discussion about and intellectual inquiry into [...]
