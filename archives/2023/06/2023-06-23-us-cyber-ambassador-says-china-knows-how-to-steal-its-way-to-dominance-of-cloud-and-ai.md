Title: US cyber ambassador says China knows how to steal its way to dominance of cloud and AI
Date: 2023-06-23T03:31:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-06-23-us-cyber-ambassador-says-china-knows-how-to-steal-its-way-to-dominance-of-cloud-and-ai

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/23/nate_fick_china_playbook_fightback/){:target="_blank" rel="noopener"}

> Calls on governments to combat 'playbook' that propelled Huawei to prominence China has a playbook to use IP theft to seize leadership in cloud computing, and other nations should band together to stop that happening, according to Nathaniel C. Fick, the US ambassador-at-large for cyberspace and digital policy.... [...]
