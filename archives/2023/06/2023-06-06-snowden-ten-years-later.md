Title: Snowden Ten Years Later
Date: 2023-06-06T11:17:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Edward Snowden;essays;history of security;NSA;privacy;surveillance
Slug: 2023-06-06-snowden-ten-years-later

[Source](https://www.schneier.com/blog/archives/2023/06/snowden-ten-years-later.html){:target="_blank" rel="noopener"}

> In 2013 and 2014, I wrote extensively about new revelations regarding NSA surveillance based on the documents provided by Edward Snowden. But I had a more personal involvement as well. I wrote the essay below in September 2013. The New Yorker agreed to publish it, but the Guardian asked me not to. It was scared of UK law enforcement, and worried that this essay would reflect badly on it. And given that the UK police would raid its offices in July 2014, it had legitimate cause to be worried. Now, ten years later, I offer this as a time capsule [...]
