Title: The Week in Ransomware - June 2nd 2023 - Whodunit?
Date: 2023-06-02T17:47:03-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-06-02-the-week-in-ransomware-june-2nd-2023-whodunit

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-june-2nd-2023-whodunit/){:target="_blank" rel="noopener"}

> It has been a fairly quiet week regarding ransomware, with only a few reports released and no new significant attacks. However, we may have a rebrand in the making, and a ransomware operation is likely behind a new zero-day data-theft campaign, so we have some news to talk about. [...]
