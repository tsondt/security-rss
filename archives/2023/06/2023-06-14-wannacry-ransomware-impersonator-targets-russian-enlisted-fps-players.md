Title: WannaCry ransomware impersonator targets Russian "Enlisted" FPS players
Date: 2023-06-14T17:36:23-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2023-06-14-wannacry-ransomware-impersonator-targets-russian-enlisted-fps-players

[Source](https://www.bleepingcomputer.com/news/security/wannacry-ransomware-impersonator-targets-russian-enlisted-fps-players/){:target="_blank" rel="noopener"}

> A ransomware operation targets Russian players of the Enlisted multiplayer first-person shooter, using a fake website to spread trojanized versions of the game. [...]
