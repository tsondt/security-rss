Title: SMS delivery reports can be used to infer recipient's location
Date: 2023-06-17T10:07:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile;Technology
Slug: 2023-06-17-sms-delivery-reports-can-be-used-to-infer-recipients-location

[Source](https://www.bleepingcomputer.com/news/security/sms-delivery-reports-can-be-used-to-infer-recipients-location/){:target="_blank" rel="noopener"}

> A team of university researchers has devised a new side-channel attack named 'Freaky Leaky SMS,' which relies on the timing of SMS delivery reports to deduce a recipient's location. [...]
