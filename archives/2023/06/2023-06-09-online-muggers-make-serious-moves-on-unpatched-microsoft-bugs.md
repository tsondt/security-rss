Title: Online muggers make serious moves on unpatched Microsoft bugs
Date: 2023-06-09T23:47:21+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-06-09-online-muggers-make-serious-moves-on-unpatched-microsoft-bugs

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/09/microsoft_systems_flaws_patch/){:target="_blank" rel="noopener"}

> Win32k and Visual Studio flaws are under attack Two flaws in Microsoft software are under attack on systems that haven't been patched by admins.... [...]
