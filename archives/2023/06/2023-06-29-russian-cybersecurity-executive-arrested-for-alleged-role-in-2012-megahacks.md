Title: Russian Cybersecurity Executive Arrested for Alleged Role in 2012 Megahacks
Date: 2023-06-29T18:30:08+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Arkady Bukh;Dmitry Volkov;Dropbox;FACCT;Fight Against Cybercrime Technologies;Formspring;Group-IB;Ilya Sachkov;Lamarez;LinkedIn;Oleg Tolstikh;Oleksandr Vitalyevich Ieremenko;U.S. Secret Service;U.S. Securities & Exchange Commission;Yevgeniy Nikulin;Zl0m
Slug: 2023-06-29-russian-cybersecurity-executive-arrested-for-alleged-role-in-2012-megahacks

[Source](https://krebsonsecurity.com/2023/06/russian-cybersecurity-executive-arrested-for-alleged-role-in-2012-megahacks/){:target="_blank" rel="noopener"}

> Nikita Kislitsin, formerly the head of network security for one of Russia’s top cybersecurity firms, was arrested last week in Kazakhstan in response to 10-year-old hacking charges from the U.S. Department of Justice. Experts say Kislitsin’s prosecution could soon put the Kazakhstan government in a sticky diplomatic position, as the Kremlin is already signaling that it intends to block his extradition to the United States. Nikita Kislitsin, at a security conference in Russia. Kislitsin is accused of hacking into the now-defunct social networking site Formspring in 2012, and conspiring with another Russian man convicted of stealing tens of millions of [...]
