Title: MOVEIt breach impacts GenWorth, CalPERS as data for 3.2 million exposed
Date: 2023-06-23T11:06:33-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-23-moveit-breach-impacts-genworth-calpers-as-data-for-32-million-exposed

[Source](https://www.bleepingcomputer.com/news/security/moveit-breach-impacts-genworth-calpers-as-data-for-32-million-exposed/){:target="_blank" rel="noopener"}

> PBI Research Services (PBI) has suffered a data breach with three clients disclosing that the data for 4.75 million people was stolen in the recent MOVEit Transfer data-theft attacks. [...]
