Title: Data leak at major law firm sets Australia's government and elites scrambling
Date: 2023-06-20T05:04:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-06-20-data-leak-at-major-law-firm-sets-australias-government-and-elites-scrambling

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/20/hwl_ebsworth_cyber_incident/){:target="_blank" rel="noopener"}

> BlackCat attack sparks injunction preventing coverage of purloined docs An infosec incident at a major Australian law firm has sparked fear among the nation's governments, banks and businesses – and a free speech debate.... [...]
