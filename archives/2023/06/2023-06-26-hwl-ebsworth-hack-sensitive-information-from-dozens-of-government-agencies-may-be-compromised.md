Title: HWL Ebsworth hack: sensitive information from dozens of government agencies may be compromised
Date: 2023-06-26T06:30:44+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Australian politics;Cybercrime;Australia news;Internet;Technology;Data and computer security;Virgin Australia;National disability insurance scheme
Slug: 2023-06-26-hwl-ebsworth-hack-sensitive-information-from-dozens-of-government-agencies-may-be-compromised

[Source](https://www.theguardian.com/australia-news/2023/jun/26/hwl-ebsworth-hack-sensitive-information-from-dozens-of-government-agencies-may-be-compromised){:target="_blank" rel="noopener"}

> Hundreds of law firm’s clients waiting on confirmation of whether they are affected by data leaked in cyberattack Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast Hundreds of clients of law firm HWL Ebsworth, including dozens of government agencies, have been in discussions with the firm over whether highly sensitive legal information has been exposed. The Russian-linked ALPHV/Blackcat ransomware group hacked the law firm in April. Earlier this month, the group published 1.1TB of the data it claimed to have stolen, later established to be [...]
