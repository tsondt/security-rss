Title: Brit data watchdog fines sleazy sales ops £250K for 'bombarding' folk with calls
Date: 2023-06-09T11:30:11+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-06-09-brit-data-watchdog-fines-sleazy-sales-ops-250k-for-bombarding-folk-with-calls

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/09/ico_cold_call_fines/){:target="_blank" rel="noopener"}

> Crown Glazing and Maxen Power Supply fall foul of PECR Britain's data watchdog has slapped a financial penalty on two energy companies it claims were posing as third parties, including the National Grid and UK government, when making unsolicited marketing calls.... [...]
