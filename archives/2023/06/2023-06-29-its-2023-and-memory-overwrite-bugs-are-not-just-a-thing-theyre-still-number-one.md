Title: It's 2023 and memory overwrite bugs are not just a thing, they're still number one
Date: 2023-06-29T20:24:19+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-29-its-2023-and-memory-overwrite-bugs-are-not-just-a-thing-theyre-still-number-one

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/29/cwe_top_25_2023/){:target="_blank" rel="noopener"}

> Cough, cough, use Rust. Plus: Eight more exploited bugs added to CISA's must-patch list The most dangerous type of software bug is the out-of-bounds write, according to MITRE this week. This type of flaw is responsible for 70 CVE-tagged holes in the US government's list of known vulnerabilities that are under active attack and need to be patched, we note.... [...]
