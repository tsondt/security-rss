Title: Prevent account creation fraud with AWS WAF Fraud Control – Account Creation Fraud Prevention
Date: 2023-06-13T21:06:31+00:00
Author: David MacDonald
Category: AWS Security
Tags: Announcements;AWS WAF;Foundational (100);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2023-06-13-prevent-account-creation-fraud-with-aws-waf-fraud-control-account-creation-fraud-prevention

[Source](https://aws.amazon.com/blogs/security/prevent-account-creation-fraud-with-aws-waf-fraud-control-account-creation-fraud-prevention/){:target="_blank" rel="noopener"}

> Threat actors use sign-up pages and login pages to carry out account fraud, including taking unfair advantage of promotional and sign-up bonuses, publishing fake reviews, and spreading malware. In 2022, AWS released AWS WAF Fraud Control – Account Takeover Prevention (ATP) to help protect your application’s login page against credential stuffing attacks, brute force attempts, and [...]
