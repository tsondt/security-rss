Title: Malicious Chrome extensions with 75M installs removed from Web Store
Date: 2023-06-02T09:19:45-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2023-06-02-malicious-chrome-extensions-with-75m-installs-removed-from-web-store

[Source](https://www.bleepingcomputer.com/news/security/malicious-chrome-extensions-with-75m-installs-removed-from-web-store/){:target="_blank" rel="noopener"}

> Google has removed from the Chrome Web Store 32 malicious extensions that could alter search results and push spam or unwanted ads. Collectively, they come with a download count of 75 million. [...]
