Title: Clop ransomware claims responsibility for MOVEit extortion attacks
Date: 2023-06-05T17:27:12-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-06-05-clop-ransomware-claims-responsibility-for-moveit-extortion-attacks

[Source](https://www.bleepingcomputer.com/news/security/clop-ransomware-claims-responsibility-for-moveit-extortion-attacks/){:target="_blank" rel="noopener"}

> The Clop ransomware gang has told BleepingComputer they are behind the MOVEit Transfer data-theft attacks, where a zero-day vulnerability was exploited to breach multiple companies' servers and steal data. [...]
