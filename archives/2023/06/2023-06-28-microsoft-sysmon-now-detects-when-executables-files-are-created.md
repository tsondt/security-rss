Title: Microsoft Sysmon now detects when executables files are created
Date: 2023-06-28T17:28:25-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-06-28-microsoft-sysmon-now-detects-when-executables-files-are-created

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-sysmon-now-detects-when-executables-files-are-created/){:target="_blank" rel="noopener"}

> Microsoft has released Sysmon 15, converting it into a protected process and adding the new 'FileExecutableDetected' option to log when executable files are created. [...]
