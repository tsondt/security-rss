Title: FBI: FISA Section 702 'absolutely critical' to spy on, err, protect Americans
Date: 2023-06-09T20:30:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-09-fbi-fisa-section-702-absolutely-critical-to-spy-on-err-protect-americans

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/09/fbi_fisa_section_702_absolutely/){:target="_blank" rel="noopener"}

> No protection without surveillance? The FBI doesn't want to lose its favorite codified way to spy, Section 702 of the US Foreign Intelligence Surveillance Act. In its latest salvo, the agency's deputy director Paul Abbate called it "absolutely critical for the FBI to continue protecting the American people."... [...]
