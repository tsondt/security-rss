Title: FTC: Amazon trapped millions into hard-to-cancel Prime memberships
Date: 2023-06-21T11:35:24-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-21-ftc-amazon-trapped-millions-into-hard-to-cancel-prime-memberships

[Source](https://www.bleepingcomputer.com/news/security/ftc-amazon-trapped-millions-into-hard-to-cancel-prime-memberships/){:target="_blank" rel="noopener"}

> The Federal Trade Commission (FTC) says Amazon allegedly used dark patterns to trick millions of users into enrolling in its Prime program and trapping them by making it as difficult as possible to cancel the automatically-renewing subscriptions. [...]
