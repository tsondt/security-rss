Title: On the frontline of cyber threats
Date: 2023-06-08T13:00:13+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-06-08-on-the-frontline-of-cyber-threats

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/08/on_the_frontline_of_cyber/){:target="_blank" rel="noopener"}

> Watch it here: the unvarnished truth about the state of data security Webinar Rubrik Zero Lab's annual report on the state of data security is not a comfortable read. And as if to prepare you for what lies inside, the company has called it 'The Hard Truths.'... [...]
