Title: The Week in Ransomware - June 30th 2023 - Mistaken Identity
Date: 2023-06-30T17:33:18-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-06-30-the-week-in-ransomware-june-30th-2023-mistaken-identity

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-june-30th-2023-mistaken-identity/){:target="_blank" rel="noopener"}

> A case of mistaken identity and further MOVEit Transfer data breaches continue dominated the ransomware news cycle this week. [...]
