Title: Toyota admits to yet another cloud leak
Date: 2023-06-05T03:30:14+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-06-05-toyota-admits-to-yet-another-cloud-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/05/security_in_brief/){:target="_blank" rel="noopener"}

> Also, hackers publish RaidForum user data, Google's $180k Chrome bug bounty, and this week's vulnerabilities infosec in brief Japanese automaker Toyota is again apologizing for spilling customer records online due to a misconfigured cloud environment – the same explanation it gave when the same thing happened a couple of weeks ago. It's like a pattern.... [...]
