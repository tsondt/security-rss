Title: Clop ransomware likely testing MOVEit zero-day since 2021
Date: 2023-06-08T18:45:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-08-clop-ransomware-likely-testing-moveit-zero-day-since-2021

[Source](https://www.bleepingcomputer.com/news/security/clop-ransomware-likely-testing-moveit-zero-day-since-2021/){:target="_blank" rel="noopener"}

> The Clop ransomware gang has been looking for ways to exploit a now-patched zero-day in the MOVEit Transfer managed file transfer (MFT) solution since 2021, according to Kroll security experts. [...]
