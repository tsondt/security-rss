Title: Outlook.com hit by outages as hacktivists claim DDoS attacks
Date: 2023-06-06T12:31:03-04:00
Author: Mayank Parmar
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-06-06-outlookcom-hit-by-outages-as-hacktivists-claim-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/microsoft/outlookcom-hit-by-outages-as-hacktivists-claim-ddos-attacks/){:target="_blank" rel="noopener"}

> Outlook.com is suffering a series of outages today after being down multiple times yesterday, with hacktivists known as Anonymous Sudan claiming to perform DDoS attacks on the service. [...]
