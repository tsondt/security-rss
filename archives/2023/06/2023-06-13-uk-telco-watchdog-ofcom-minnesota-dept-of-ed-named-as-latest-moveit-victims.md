Title: UK telco watchdog Ofcom, Minnesota Dept of Ed named as latest MOVEit victims
Date: 2023-06-13T06:28:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-13-uk-telco-watchdog-ofcom-minnesota-dept-of-ed-named-as-latest-moveit-victims

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/13/ofcom_minnesota_moveit/){:target="_blank" rel="noopener"}

> As another CVE is assigned Two more organizations hit in the mass exploitation of the MOVEit file-transfer tool have been named – the Minnesota Department of Education in the US, and the UK's telco regulator Ofcom – just days after security researchers discovered additional flaws in Progress Software's buggy suite.... [...]
