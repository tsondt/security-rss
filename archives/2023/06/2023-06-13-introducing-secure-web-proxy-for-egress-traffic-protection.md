Title: Introducing Secure Web Proxy for egress traffic protection
Date: 2023-06-13T16:00:00+00:00
Author: Anil Nandigam
Category: GCP Security
Tags: Networking;Security & Identity
Slug: 2023-06-13-introducing-secure-web-proxy-for-egress-traffic-protection

[Source](https://cloud.google.com/blog/products/identity-security/introducing-secure-web-proxy-for-egress-traffic-protection/){:target="_blank" rel="noopener"}

> Google Cloud provides multiple layers of security to help customers stay ahead of evolving threats and keep their cloud workloads safe. Today at our annual Security Summit, we are excited to announce the general availability of Secure Web Proxy, a new cloud-first network security offering that provides web egress traffic inspection, protection, and control. Secure Web Proxy (Cloud SWP) can help networking and security teams implement Zero Trust networking principles, discover malicious activity, and support forensic investigations Introducing Cloud Secure Web Proxy How Secure Web Proxy works You configure workloads to use Secure Web Proxy as a gateway. Web requests [...]
