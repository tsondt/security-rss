Title: Customer Compliance Guides now available on AWS Artifact
Date: 2023-06-23T13:14:27+00:00
Author: Kevin Donohue
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS Artifact;Compliance;Department of Defense (DoD);DoD;FedRAMP;GovCloud;Government;HIPAA;ISO;NIST;Public Sector;Secret;Security;Security Blog;Shared Responsibility Model;SOC
Slug: 2023-06-23-customer-compliance-guides-now-available-on-aws-artifact

[Source](https://aws.amazon.com/blogs/security/customer-compliance-guides-now-available-on-aws-artifact/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) has released Customer Compliance Guides (CCGs) to support customers, partners, and auditors in their understanding of how compliance requirements from leading frameworks map to AWS service security recommendations. CCGs cover 100+ services and features offering security guidance mapped to 10 different compliance frameworks. Customers can select any of the available frameworks and services [...]
