Title: IAM: There and back again using resource hierarchies
Date: 2023-06-12T16:00:00+00:00
Author: Max Saltonstall
Category: GCP Security
Tags: Security & Identity
Slug: 2023-06-12-iam-there-and-back-again-using-resource-hierarchies

[Source](https://cloud.google.com/blog/products/identity-security/resource-hierarchies-make-your-iam-management-easier/){:target="_blank" rel="noopener"}

> Learning to use the public cloud for your first project can feel like hunting for treasure in deep, hidden dungeons, or battling fire-breathing dragons. There's jargon to learn, cryptic formulas to unravel, and countless ways to get yourself into trouble. Despite charting a course across an unfamiliar landscape, at some level it's also an exciting adventure into the unknown. You have ways to make sure that your journey is safe, and to keep all your resources secure, from thieves or (digital) sorcerers. As you explore, you’ll need strong Identity and Access Management (IAM) policies to protect your projects, and their [...]
