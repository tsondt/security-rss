Title: Lantum S3 bucket leak is prescription for chaos for thousands of UK doctors
Date: 2023-06-12T12:34:11+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2023-06-12-lantum-s3-bucket-leak-is-prescription-for-chaos-for-thousands-of-uk-doctors

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/12/lantum_s3_bucket_leak/){:target="_blank" rel="noopener"}

> Freelance agency exposed personal details that would be highly valuable in the wrong hands A UK agency for freelance doctors has potentially exposed personal details relating to 3,200 individuals via unsecured S3 buckets, which one expert said could be used to launch ID theft attacks or blackmail.... [...]
