Title: The Current State of Business Email Compromise Attacks
Date: 2023-06-28T10:01:02-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-06-28-the-current-state-of-business-email-compromise-attacks

[Source](https://www.bleepingcomputer.com/news/security/the-current-state-of-business-email-compromise-attacks/){:target="_blank" rel="noopener"}

> Business Email Compromise (BEC) poses a growing threat to businesses of all sizes. Learn more from Specops Software about the types of BEC attacks and how to avoid them. [...]
