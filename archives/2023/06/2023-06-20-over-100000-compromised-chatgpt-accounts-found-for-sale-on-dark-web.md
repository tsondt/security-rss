Title: Over 100,000 compromised ChatGPT accounts found for sale on dark web
Date: 2023-06-20T10:08:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-06-20-over-100000-compromised-chatgpt-accounts-found-for-sale-on-dark-web

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/20/stolen_chatgpt_accounts/){:target="_blank" rel="noopener"}

> Cybercrooks hoping users have whispered employer secrets to chatbot Singapore-based threat intelligence outfit Group-IB has found ChatGPT credentials in more than 100,000 stealer logs traded on the dark web in the past year.... [...]
