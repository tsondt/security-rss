Title: Chinese spies blamed for data-harvesting raids on Barracuda email gateways
Date: 2023-06-15T18:44:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-15-chinese-spies-blamed-for-data-harvesting-raids-on-barracuda-email-gateways

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/15/chinese_spies_behind_barracuda_esg/){:target="_blank" rel="noopener"}

> Snoops 'aggressively targeted' specific govt, academic accounts Chinese spies are behind the data-stealing malware injected into Barracuda's Email Security Gateway (ESG) devices globally as far back as October 2022, according to Mandiant.... [...]
