Title: Millions of Gigabyte PC motherboards backdoored? What's the actual score?
Date: 2023-06-02T02:07:20+00:00
Author: Tobias Mann
Category: The Register
Tags: 
Slug: 2023-06-02-millions-of-gigabyte-pc-motherboards-backdoored-whats-the-actual-score

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/02/gigabyte_uefi_backdoor/){:target="_blank" rel="noopener"}

> It's the 2020s and we're still running code automatically fetched over HTTP FAQ You may have seen some headlines about a supply-chain backdoor in millions of Gigabyte motherboards. Here's the lowdown.... [...]
