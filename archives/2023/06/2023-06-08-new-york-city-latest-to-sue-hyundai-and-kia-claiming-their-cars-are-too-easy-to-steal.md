Title: New York City latest to sue Hyundai and Kia claiming their cars are too easy to steal
Date: 2023-06-08T14:32:13+00:00
Author: Richard Currie
Category: The Register
Tags: 
Slug: 2023-06-08-new-york-city-latest-to-sue-hyundai-and-kia-claiming-their-cars-are-too-easy-to-steal

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/08/new_york_city_hyundai_kia/){:target="_blank" rel="noopener"}

> What started as a TikTok craze has become a 'public nuisance' Hyundai and Kia cars were stolen 977 times in New York City in the first four months of 2023, and authorities have had enough.... [...]
