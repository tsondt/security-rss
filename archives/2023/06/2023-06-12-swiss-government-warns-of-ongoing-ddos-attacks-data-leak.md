Title: Swiss government warns of ongoing DDoS attacks, data leak
Date: 2023-06-12T10:58:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-12-swiss-government-warns-of-ongoing-ddos-attacks-data-leak

[Source](https://www.bleepingcomputer.com/news/security/swiss-government-warns-of-ongoing-ddos-attacks-data-leak/){:target="_blank" rel="noopener"}

> The Swiss government has disclosed that a recent ransomware attack on an IT supplier might have impacted its data, while today, it warns that it is now targeted in DDoS attacks. [...]
