Title: Malaysia goes its own Huawei, won't ban Chinese vendor from 5G network
Date: 2023-06-02T18:33:08+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2023-06-02-malaysia-goes-its-own-huawei-wont-ban-chinese-vendor-from-5g-network

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/02/malaysia_5g_rollout_huawei/){:target="_blank" rel="noopener"}

> Country to have two networks as first buildout falls behind schedule Malaysia could be putting itself on a collision course with the EU and US as the country looks set to allow Chinese suppliers including Huawei a chance to play a part in its planned 5G network rollout.... [...]
