Title: BA, Boots and BBC cyber-attack: who is behind it and what happens next?
Date: 2023-06-07T13:42:53+00:00
Author: Dan Milmo and Alex Hern
Category: The Guardian
Tags: Cybercrime;Data and computer security;Internet;Technology;Russia;World news;Boots;British Airways;BBC;Business;Hacking
Slug: 2023-06-07-ba-boots-and-bbc-cyber-attack-who-is-behind-it-and-what-happens-next

[Source](https://www.theguardian.com/technology/2023/jun/07/ba-boots-bbc-cyber-attack-moveit-who-is-behind-it-and-what-happens-next){:target="_blank" rel="noopener"}

> A cybercrime group has exploited a flaw in MOVEit software and is demanding a ransom British Airways, Boots and the BBC have been hit with an ultimatum to begin ransom negotiations from a cybercrime group after employees’ personal data was stolen in a hacking attack. It emerged on Wednesday the gang behind a piece of ransomware known as Clop had posted the demand to its darkweb site, where stolen data is typically released if payments are not made by the victims. Continue reading... [...]
