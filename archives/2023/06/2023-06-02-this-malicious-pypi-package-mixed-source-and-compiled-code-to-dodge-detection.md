Title: This malicious PyPI package mixed source and compiled code to dodge detection
Date: 2023-06-02T06:24:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-06-02-this-malicious-pypi-package-mixed-source-and-compiled-code-to-dodge-detection

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/02/novel_pypi_attack_reversinglabs/){:target="_blank" rel="noopener"}

> Oh cool, something else to scan for Researchers recently uncovered the following novel attack on the Python Package Index (PyPI).... [...]
