Title: Barracuda Urges Replacing — Not Patching — Its Email Security Gateways
Date: 2023-06-08T20:17:06+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;Time to Patch;Barracuda Networks;Caitlin Condon;CVE-2023-2868;Email Security Gateway;International Computer Science Institute;Mandiant;Nicholas Weaver;Rapid7
Slug: 2023-06-08-barracuda-urges-replacing-not-patching-its-email-security-gateways

[Source](https://krebsonsecurity.com/2023/06/barracuda-urges-replacing-not-patching-its-email-security-gateways/){:target="_blank" rel="noopener"}

> It’s not often that a zero-day vulnerability causes a network security vendor to urge customers to physically remove and decommission an entire line of affected hardware — as opposed to just applying software updates. But experts say that is exactly what transpired this week with Barracuda Networks, as the company struggled to combat a sprawling malware threat which appears to have undermined its email security appliances in such a fundamental way that they can no longer be safely updated with software fixes. The Barracuda Email Security Gateway (ESG) 900 appliance. Campbell, Calif. based Barracuda said it hired incident response firm [...]
