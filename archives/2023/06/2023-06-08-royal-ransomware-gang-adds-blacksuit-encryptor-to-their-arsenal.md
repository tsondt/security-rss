Title: Royal ransomware gang adds BlackSuit encryptor to their arsenal
Date: 2023-06-08T03:12:24-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-06-08-royal-ransomware-gang-adds-blacksuit-encryptor-to-their-arsenal

[Source](https://www.bleepingcomputer.com/news/security/royal-ransomware-gang-adds-blacksuit-encryptor-to-their-arsenal/){:target="_blank" rel="noopener"}

> The Royal ransomware gang has begun testing a new encryptor called BlackSuit that shares many similarities with the operation's usual encryptor. [...]
