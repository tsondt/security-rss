Title: Massive phishing campaign uses 6,000 sites to impersonate 100 brands
Date: 2023-06-13T09:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-13-massive-phishing-campaign-uses-6000-sites-to-impersonate-100-brands

[Source](https://www.bleepingcomputer.com/news/security/massive-phishing-campaign-uses-6-000-sites-to-impersonate-100-brands/){:target="_blank" rel="noopener"}

> A widespread brand impersonation campaign targeting over a hundred popular apparel, footwear, and clothing brands has been underway since June 2022, tricking people into entering their account credentials and financial information on fake websites. [...]
