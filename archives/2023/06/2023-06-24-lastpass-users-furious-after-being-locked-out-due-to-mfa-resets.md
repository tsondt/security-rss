Title: LastPass users furious after being locked out due to MFA resets
Date: 2023-06-24T10:15:30-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-24-lastpass-users-furious-after-being-locked-out-due-to-mfa-resets

[Source](https://www.bleepingcomputer.com/news/security/lastpass-users-furious-after-being-locked-out-due-to-mfa-resets/){:target="_blank" rel="noopener"}

> LastPass password manager users have been experiencing significant login issues starting early May after being prompted to reset their authenticator apps. [...]
