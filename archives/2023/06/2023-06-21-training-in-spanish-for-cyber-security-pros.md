Title: Training in Spanish for cyber security pros
Date: 2023-06-21T13:25:08+00:00
Author: Rob Jaques
Category: The Register
Tags: 
Slug: 2023-06-21-training-in-spanish-for-cyber-security-pros

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/21/training_in_spanish_for_cyber/){:target="_blank" rel="noopener"}

> Sponsored Post Cybercrime is a global phenomenon, but the effectiveness of measures put in place to fight it varies considerably from one region to another.... [...]
