Title: A better way to stay ahead of attacks: Security Command Center adds attack path simulation
Date: 2023-06-13T16:00:00+00:00
Author: Joakim Nydrén
Category: GCP Security
Tags: Security & Identity
Slug: 2023-06-13-a-better-way-to-stay-ahead-of-attacks-security-command-center-adds-attack-path-simulation

[Source](https://cloud.google.com/blog/products/identity-security/security-command-center-adds-attack-path-simulation-to-stay-ahead-of-cyber-risks/){:target="_blank" rel="noopener"}

> To help secure increasingly complex and dynamic cloud environments, many security teams are turning to attack path analysis tools. These tools can enable them to better prioritize security findings and discover pathways that adversaries can exploit to access and compromise cloud assets such as virtual machines, databases, and storage buckets. Other attack path tools rely on static, point-in-time snapshots of an organization’s cloud footprint, which often contain sensitive data about their environment, how it is configured, and where the most sensitive data resides. At Google Cloud, we are taking a different approach. We are excited to announce today at the [...]
