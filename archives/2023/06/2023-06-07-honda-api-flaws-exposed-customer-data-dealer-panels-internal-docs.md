Title: Honda API flaws exposed customer data, dealer panels, internal docs
Date: 2023-06-07T16:10:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-07-honda-api-flaws-exposed-customer-data-dealer-panels-internal-docs

[Source](https://www.bleepingcomputer.com/news/security/honda-api-flaws-exposed-customer-data-dealer-panels-internal-docs/){:target="_blank" rel="noopener"}

> Honda's e-commerce platform for power equipment, marine, lawn & garden, was vulnerable to unauthorized access by anyone due to API flaws that allow password reset for any account. [...]
