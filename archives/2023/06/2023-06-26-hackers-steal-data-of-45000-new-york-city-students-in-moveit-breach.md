Title: Hackers steal data of 45,000 New York City students in MOVEit breach
Date: 2023-06-26T12:15:39-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Education
Slug: 2023-06-26-hackers-steal-data-of-45000-new-york-city-students-in-moveit-breach

[Source](https://www.bleepingcomputer.com/news/security/hackers-steal-data-of-45-000-new-york-city-students-in-moveit-breach/){:target="_blank" rel="noopener"}

> The New York City Department of Education (NYC DOE) says hackers stole documents containing the sensitive personal information of up to 45,000 students from its MOVEit Transfer server. [...]
