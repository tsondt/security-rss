Title: Use AWS Private Certificate Authority to issue device attestation certificates for Matter
Date: 2023-06-21T20:27:23+00:00
Author: Ramesh Nagappan
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;Security Blog
Slug: 2023-06-21-use-aws-private-certificate-authority-to-issue-device-attestation-certificates-for-matter

[Source](https://aws.amazon.com/blogs/security/use-aws-private-certificate-authority-to-issue-device-attestation-certificates-for-matter/){:target="_blank" rel="noopener"}

> In this blog post, we show you how to use AWS Private Certificate Authority (CA) to create Matter device attestation CAs to issue device attestation certificates (DAC). By using this solution, device makers can operate their own device attestation CAs, building on the solid security foundation provided by AWS Private CA. This post assumes that [...]
