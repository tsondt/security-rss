Title: Cops told: Er, no, you need a wiretap order if you want real-time Facebook snooping
Date: 2023-06-30T19:40:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-30-cops-told-er-no-you-need-a-wiretap-order-if-you-want-real-time-facebook-snooping

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/30/new_jersey_cops_facebook_wiretap/){:target="_blank" rel="noopener"}

> Privacy: It's a Jersey Thing New Jersey cops must apply for a wiretap order — not just a warrant — for near-continual snooping on suspects' Facebook accounts, according to a unanimous ruling by that US state's Supreme Court.... [...]
