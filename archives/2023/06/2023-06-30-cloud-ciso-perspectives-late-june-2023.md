Title: Cloud CISO Perspectives: Late June 2023
Date: 2023-06-30T16:00:00+00:00
Author: Sandra Joyce
Category: GCP Security
Tags: Security & Identity
Slug: 2023-06-30-cloud-ciso-perspectives-late-june-2023

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-late-june-2023/){:target="_blank" rel="noopener"}

> Welcome to the second Cloud CISO Perspectives for June 2023. In my previous newsletter, I discussed the growing, important role that accurate threat intelligence can play in helping keep organizations secure, and how we centered that at our Security Summit. Today’s cybersecurity threats can pose dangerous risks to organizations, and have a material impact on their business. It’s vital that leaders and boards need to stay abreast of accurate, timely threat intelligence, so I’m turning the mic over to Sandra Joyce, vice president of Mandiant Intelligence at Google Cloud, to talk more about the current threat landscape. As with all [...]
