Title: Ex-FBI employee jailed for taking classified material home
Date: 2023-06-26T12:04:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-06-26-ex-fbi-employee-jailed-for-taking-classified-material-home

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/26/infosec_in_brief/){:target="_blank" rel="noopener"}

> Also: a PII harvest at Dole's server farm, military members mailed mystery smartwatches, and this week's critical vulns Infosec in brief In a case startlingly similar to charges recently unsealed against one-term US president Donald Trump, a former FBI analyst has been jailed for taking sensitive classified material home with her.... [...]
