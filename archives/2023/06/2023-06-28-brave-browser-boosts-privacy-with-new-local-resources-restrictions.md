Title: Brave Browser boosts privacy with new local resources restrictions
Date: 2023-06-28T11:44:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2023-06-28-brave-browser-boosts-privacy-with-new-local-resources-restrictions

[Source](https://www.bleepingcomputer.com/news/security/brave-browser-boosts-privacy-with-new-local-resources-restrictions/){:target="_blank" rel="noopener"}

> The Brave team has announced that the privacy-centric browser will soon introduce new restriction controls allowing users to specify how long sites can access local network resources. [...]
