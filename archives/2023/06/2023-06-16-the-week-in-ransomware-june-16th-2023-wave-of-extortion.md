Title: The Week in Ransomware - June 16th 2023 - Wave of Extortion
Date: 2023-06-16T17:28:47-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-06-16-the-week-in-ransomware-june-16th-2023-wave-of-extortion

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-june-16th-2023-wave-of-extortion/){:target="_blank" rel="noopener"}

> The MOVEit Transfer extortion attacks continue to dominate the news cycle, with the Clop ransomware operation now extorting organizations breached in the attacks. [...]
