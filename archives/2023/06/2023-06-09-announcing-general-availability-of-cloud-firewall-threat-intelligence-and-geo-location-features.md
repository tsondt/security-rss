Title: Announcing general availability of Cloud Firewall threat intelligence and geo-location features
Date: 2023-06-09T16:00:00+00:00
Author: Faye Feng
Category: GCP Security
Tags: Security & Identity
Slug: 2023-06-09-announcing-general-availability-of-cloud-firewall-threat-intelligence-and-geo-location-features

[Source](https://cloud.google.com/blog/products/identity-security/announcing-general-availability-of-cloud-firewall-threat-intelligence-and-geo-location-features/){:target="_blank" rel="noopener"}

> Google Cloud Firewall is a fully distributed, stateful inspection firewall engine that is built into our software-defined networking fabric and enforced at each workload. With Cloud Firewall, you can enhance and simplify security posture, and implement Zero Trust networking for cloud workloads. Previously, we announced several enhancements and expansions to our Cloud Firewall offering. Today, we are excited to announce the general availability for the following Cloud Firewall features: Threat Intelligence for Cloud Firewall : curated threat intelligence lists built and maintained by Google Geo-location objects : filter traffic based on specific geographic locations Address groups : user-defined collection of [...]
