Title: Unsealed: Charges against Russians blamed for Mt Gox crypto-exchange collapse
Date: 2023-06-12T23:23:21+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-06-12-unsealed-charges-against-russians-blamed-for-mt-gox-crypto-exchange-collapse

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/12/mt_gox_crypto_indictments/){:target="_blank" rel="noopener"}

> What a blast from the past, the past being a year before the pandemic American prosecutors have unsealed an indictment against two Russians who allegedly had a hand in the ransacking and collapse of Mt Gox a decade ago, an implosion that cost the cryptocurrency exchange's thousands of customers most of their digital coins.... [...]
