Title: Torrent of image-based phishing emails are harder to detect and more convincing
Date: 2023-06-30T14:00:13+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;image;phishing;qr code
Slug: 2023-06-30-torrent-of-image-based-phishing-emails-are-harder-to-detect-and-more-convincing

[Source](https://arstechnica.com/?p=1951208){:target="_blank" rel="noopener"}

> Enlarge / Man hand holding a mobile phone with QR code. (credit: Getty Images) Phishing mongers have released a torrent of image-based junk emails that embed QR codes into their bodies to successfully bypass security protections and provide a level of customization to more easily fool recipients, researchers said. In many cases, the emails come from a compromised email address inside the organization the recipient works in, a tactic that provides a false sense of authenticity, researchers from security firm Inky said. The emails Inky detected instruct the employee to resolve security issues such as a missing two-factor authentication enrollment [...]
