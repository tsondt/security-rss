Title: Barracuda tells its ESG owners to 'immediately' junk buggy kit
Date: 2023-06-08T21:04:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-08-barracuda-tells-its-esg-owners-to-immediately-junk-buggy-kit

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/08/barracuda_immediately_replace_esg/){:target="_blank" rel="noopener"}

> That patch we issued? Yeah, it wasn't enough Barracuda has now told customers to "immediately" replace infected Email Security Gateway (ESG) appliances — even if they have received a patch to fix a critical bug under exploit.... [...]
