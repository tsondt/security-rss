Title: Microsoft links Clop ransomware gang to MOVEit data-theft attacks
Date: 2023-06-05T08:54:23-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-06-05-microsoft-links-clop-ransomware-gang-to-moveit-data-theft-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-links-clop-ransomware-gang-to-moveit-data-theft-attacks/){:target="_blank" rel="noopener"}

> Microsoft has linked the Clop ransomware gang to recent attacks exploiting a zero-day vulnerability in the MOVEit Transfer platform to steal data from organizations. [...]
