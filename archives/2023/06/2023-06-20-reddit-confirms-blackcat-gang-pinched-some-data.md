Title: Reddit confirms BlackCat gang pinched some data
Date: 2023-06-20T18:34:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-20-reddit-confirms-blackcat-gang-pinched-some-data

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/20/reddit_confirms_blackcat_extortion_attempt/){:target="_blank" rel="noopener"}

> Crooks demand $4.5m to keep '80GB' of corp info private – and no API price hikes Reddit this week confirmed ransomware gang BlackCat, aka AlphaV, broke into its corporate systems in February.... [...]
