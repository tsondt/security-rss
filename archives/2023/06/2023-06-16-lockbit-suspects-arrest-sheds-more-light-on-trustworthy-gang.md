Title: LockBit suspect's arrest sheds more light on 'trustworthy' gang
Date: 2023-06-16T19:01:11+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-06-16-lockbit-suspects-arrest-sheds-more-light-on-trustworthy-gang

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/16/lockbit_suspect_arrest/){:target="_blank" rel="noopener"}

> Plus: Accused is innocent until proven guilty, but is known to be an Apple fan FBI agents have arrested a Russian man suspected of being part of the Lockbit ransomware gang. An unsealed complaint alleges the 20-year-old was an Apple fanboy, an online gambler, and scored 80 percent of at least one ransom payment given to the criminals.... [...]
