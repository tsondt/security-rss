Title: New eBook: 5 Keys to Secure Enterprise Messaging
Date: 2023-06-02T16:33:54+00:00
Author: Anne Grahn
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS Wickr;cryptography;Cybersecurity awareness;data privacy;eBook;Encryption;Security;Security Blog
Slug: 2023-06-02-new-ebook-5-keys-to-secure-enterprise-messaging

[Source](https://aws.amazon.com/blogs/security/new-ebook-5-keys-to-secure-enterprise-messaging/){:target="_blank" rel="noopener"}

> AWS is excited to announce a new eBook, 5 Keys to Secure Enterprise Messaging. The new eBook includes best practices for addressing the security and compliance risks associated with messaging apps. An estimated 3.09 billion mobile phone users access messaging apps to communicate, and this figure is projected to grow to 3.51 billion users in [...]
