Title: Reddit hackers threaten to leak data stolen in February breach
Date: 2023-06-18T12:01:03-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-06-18-reddit-hackers-threaten-to-leak-data-stolen-in-february-breach

[Source](https://www.bleepingcomputer.com/news/security/reddit-hackers-threaten-to-leak-data-stolen-in-february-breach/){:target="_blank" rel="noopener"}

> The BlackCat (ALPHV) ransomware gang is behind a February cyberattack on Reddit, where the threat actors claim to have stolen 80GB of data from the company. [...]
