Title: Iowa’s largest school district confirms ransomware attack, data theft
Date: 2023-06-19T16:16:36-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Education
Slug: 2023-06-19-iowas-largest-school-district-confirms-ransomware-attack-data-theft

[Source](https://www.bleepingcomputer.com/news/security/iowas-largest-school-district-confirms-ransomware-attack-data-theft/){:target="_blank" rel="noopener"}

> Des Moines Public Schools, Iowa's largest school district, confirmed today that a ransomware attack was behind an incident that forced it to take all networked systems offline on January 9, 2023. [...]
