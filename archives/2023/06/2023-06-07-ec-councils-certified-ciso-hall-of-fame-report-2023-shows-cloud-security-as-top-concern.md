Title: EC-Council’s Certified CISO Hall of Fame Report 2023 shows Cloud Security as Top Concern
Date: 2023-06-07T10:02:04-04:00
Author: Sponsored by EC-Council
Category: BleepingComputer
Tags: Security;Cloud
Slug: 2023-06-07-ec-councils-certified-ciso-hall-of-fame-report-2023-shows-cloud-security-as-top-concern

[Source](https://www.bleepingcomputer.com/news/security/ec-councils-certified-ciso-hall-of-fame-report-2023-shows-cloud-security-as-top-concern/){:target="_blank" rel="noopener"}

> A survey of global cybersecurity leaders through the 2023 Certified CISO Hall of Fame Report commissioned by EC-Council identified 4 primary areas of grave concern: cloud security, data security, security governance, and lack of cybersecurity talent. [...]
