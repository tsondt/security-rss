Title: UPS discloses data breach after exposed customer info used in SMS phishing
Date: 2023-06-21T13:43:49-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-21-ups-discloses-data-breach-after-exposed-customer-info-used-in-sms-phishing

[Source](https://www.bleepingcomputer.com/news/security/ups-discloses-data-breach-after-exposed-customer-info-used-in-sms-phishing/){:target="_blank" rel="noopener"}

> Multinational shipping company UPS is alerting Canadian customers that some of their personal information might have been exposed via its online package look-up tools and abused in phishing attacks. [...]
