Title: Introducing new ways Security Command Center Premium protects identities
Date: 2023-06-01T16:00:00+00:00
Author: Timothy Peacock
Category: GCP Security
Tags: Security & Identity
Slug: 2023-06-01-introducing-new-ways-security-command-center-premium-protects-identities

[Source](https://cloud.google.com/blog/products/identity-security/introducing-new-ways-security-command-center-can-protect-identities/){:target="_blank" rel="noopener"}

> After decades of managing and securing identities in data centers, security and IT operations teams face new challenges when detecting identity compromise in their public cloud environments. Protecting cloud service accounts against leaked keys, privilege escalation in complex authorization systems, and insider threats are vital tasks when considering the threat landscape. Security Command Center Premium, our built-in security and risk management solution for Google Cloud, has released new capabilities to help detect compromised identities and protect against risks from external attackers and malicious insiders. Why identity security is complex In Google Cloud, there are three types of principals used to [...]
