Title: University of Manchester says hackers ‘likely’ stole data in cyberattack
Date: 2023-06-09T15:21:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Education
Slug: 2023-06-09-university-of-manchester-says-hackers-likely-stole-data-in-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/university-of-manchester-says-hackers-likely-stole-data-in-cyberattack/){:target="_blank" rel="noopener"}

> The University of Manchester warns staff and students that they suffered a cyberattack where threat actors likely stole data from the University's network. [...]
