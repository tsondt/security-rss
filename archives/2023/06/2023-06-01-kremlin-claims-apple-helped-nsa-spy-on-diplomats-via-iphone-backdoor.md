Title: Kremlin claims Apple helped NSA spy on diplomats via iPhone backdoor
Date: 2023-06-01T21:49:31+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-01-kremlin-claims-apple-helped-nsa-spy-on-diplomats-via-iphone-backdoor

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/01/fsb_apple_nsa_spyware_kaspersky/){:target="_blank" rel="noopener"}

> Did we just time warp back to 2013? Russian intelligence has accused American snoops and Apple of working together to backdoor iPhones to spy on "thousands" of diplomats worldwide.... [...]
