Title: Suncor Energy cyberattack impacts Petro-Canada gas stations
Date: 2023-06-26T09:27:48-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-26-suncor-energy-cyberattack-impacts-petro-canada-gas-stations

[Source](https://www.bleepingcomputer.com/news/security/suncor-energy-cyberattack-impacts-petro-canada-gas-stations/){:target="_blank" rel="noopener"}

> Petro-Canada gas stations across Canada are impacted by technical problems preventing customers from paying with credit card or rewards points as its parent company, Suncor Energy, discloses they suffered a cyberattack. [...]
