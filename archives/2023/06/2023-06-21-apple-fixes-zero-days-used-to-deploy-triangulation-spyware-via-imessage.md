Title: Apple fixes zero-days used to deploy Triangulation spyware via iMessage
Date: 2023-06-21T14:31:59-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple;Security
Slug: 2023-06-21-apple-fixes-zero-days-used-to-deploy-triangulation-spyware-via-imessage

[Source](https://www.bleepingcomputer.com/news/apple/apple-fixes-zero-days-used-to-deploy-triangulation-spyware-via-imessage/){:target="_blank" rel="noopener"}

> Apple addressed three new zero-day vulnerabilities exploited in attacks installing Triangulation spyware on iPhones via iMessage zero-click exploits. [...]
