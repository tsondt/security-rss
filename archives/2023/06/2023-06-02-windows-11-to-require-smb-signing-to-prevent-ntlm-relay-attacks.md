Title: Windows 11 to require SMB signing to prevent NTLM relay attacks
Date: 2023-06-02T14:22:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-06-02-windows-11-to-require-smb-signing-to-prevent-ntlm-relay-attacks

[Source](https://www.bleepingcomputer.com/news/security/windows-11-to-require-smb-signing-to-prevent-ntlm-relay-attacks/){:target="_blank" rel="noopener"}

> Microsoft says SMB signing (aka security signatures) will be required by default for all connections to defend against NTLM relay attacks, starting with today's Windows build (Enterprise edition) rolling out to Insiders in the Canary Channel. [...]
