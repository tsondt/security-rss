Title: CISA: LockBit ransomware extorted $91 million in 1,700 U.S. attacks
Date: 2023-06-14T11:38:35-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-14-cisa-lockbit-ransomware-extorted-91-million-in-1700-us-attacks

[Source](https://www.bleepingcomputer.com/news/security/cisa-lockbit-ransomware-extorted-91-million-in-1-700-us-attacks/){:target="_blank" rel="noopener"}

> U.S. and international cybersecurity authorities said in a joint LockBit ransomware advisory that the gang successfully extorted roughly $91 million following approximately 1,700 attacks against U.S. organizations since 2020. [...]
