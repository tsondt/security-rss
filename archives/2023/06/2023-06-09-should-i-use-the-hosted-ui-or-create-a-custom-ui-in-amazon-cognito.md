Title: Should I use the hosted UI or create a custom UI in Amazon Cognito?
Date: 2023-06-09T18:40:17+00:00
Author: Joshua Du Lac
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;Amazon Cognito;Identity;Security Blog
Slug: 2023-06-09-should-i-use-the-hosted-ui-or-create-a-custom-ui-in-amazon-cognito

[Source](https://aws.amazon.com/blogs/security/use-the-hosted-ui-or-create-a-custom-ui-in-amazon-cognito/){:target="_blank" rel="noopener"}

> Amazon Cognito is an authentication, authorization, and user management service for your web and mobile applications. Your users can sign in directly through many different authentication methods, such as native accounts within Amazon Cognito or through a social login, such as Facebook, Amazon, or Google. You can also configure federation through a third-party OpenID Connect [...]
