Title: US govt now bans TikTok from contractors' work gear
Date: 2023-06-06T19:25:14+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-06-06-us-govt-now-bans-tiktok-from-contractors-work-gear

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/06/us_contractors_tiktok_ban/){:target="_blank" rel="noopener"}

> BYODALAINGTI (as long as it's not got TikTok installed) The US federal government's ban on TikTok has been extended to include devices used by its many contractors - even those that are privately owned. The bottom line: if some electronics are used for government work, it better not have any ByteDance bits on it.... [...]
