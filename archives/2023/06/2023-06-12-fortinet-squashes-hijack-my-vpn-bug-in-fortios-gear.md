Title: Fortinet squashes hijack-my-VPN bug in FortiOS gear
Date: 2023-06-12T21:06:02+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-12-fortinet-squashes-hijack-my-vpn-bug-in-fortios-gear

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/12/fortinet_fixes_critical_rce_bug/){:target="_blank" rel="noopener"}

> And it's already being exploited in the wild, probably Fortinet has patched a critical bug in its FortiOS and FortiProxy SSL-VPN that can be exploited to hijack the equipment.... [...]
