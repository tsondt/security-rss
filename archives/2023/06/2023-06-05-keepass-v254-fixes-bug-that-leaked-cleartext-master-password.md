Title: KeePass v2.54 fixes bug that leaked cleartext master password
Date: 2023-06-05T10:15:30-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-05-keepass-v254-fixes-bug-that-leaked-cleartext-master-password

[Source](https://www.bleepingcomputer.com/news/security/keepass-v254-fixes-bug-that-leaked-cleartext-master-password/){:target="_blank" rel="noopener"}

> KeePass has released version 2.54, fixing the CVE-2023-32784 vulnerability that allows the extraction of the cleartext master password from the application's memory. [...]
