Title: Warning: JavaScript registry npm vulnerable to 'manifest confusion' abuse
Date: 2023-06-27T20:40:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-06-27-warning-javascript-registry-npm-vulnerable-to-manifest-confusion-abuse

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/27/javascript_registry_npm_vulnerable/){:target="_blank" rel="noopener"}

> Failure to match metadata with packaged files is perfect for supply chain attacks The npm Public Registry, a database of JavaScript packages, fails to compare npm package manifest data with the archive of files that data describes, creating an opportunity for the installation and execution of malicious files.... [...]
