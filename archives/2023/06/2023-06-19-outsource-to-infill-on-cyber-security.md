Title: Outsource to infill on cyber security
Date: 2023-06-19T08:35:12+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: 2023-06-19-outsource-to-infill-on-cyber-security

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/19/outsource_to_infill_on_cyber/){:target="_blank" rel="noopener"}

> Automating, simplifying, and calling in external help can increase the chances of blocking and mitigating attacks Sponsored Feature Life is tougher than ever for security pros facing a rising tide of cyberattacks. And adversaries are becoming more adept than ever at using diverse methods and technologies to scale up assaults on their selected targets.... [...]
