Title: Post-quantum hybrid SFTP file transfers using AWS Transfer Family
Date: 2023-06-13T12:59:54+00:00
Author: Panos Kampanakis
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;Technical How-to;cryptography;post-quantum cryptography;Security Blog
Slug: 2023-06-13-post-quantum-hybrid-sftp-file-transfers-using-aws-transfer-family

[Source](https://aws.amazon.com/blogs/security/post-quantum-hybrid-sftp-file-transfers-using-aws-transfer-family/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) prioritizes security, privacy, and performance. Encryption is a vital part of privacy. To help provide long-term protection of encrypted data, AWS has been introducing quantum-resistant key exchange in common transport protocols used by AWS customers. In this blog post, we introduce post-quantum hybrid key exchange with Kyber, the National Institute of Standards [...]
