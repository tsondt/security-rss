Title: Atomic Wallet hacks lead to over $35 million in crypto stolen
Date: 2023-06-04T15:04:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-06-04-atomic-wallet-hacks-lead-to-over-35-million-in-crypto-stolen

[Source](https://www.bleepingcomputer.com/news/security/atomic-wallet-hacks-lead-to-over-35-million-in-crypto-stolen/){:target="_blank" rel="noopener"}

> The developers of Atomic Wallet are investigating reports of large-scale theft of cryptocurrency from users' wallets, with over $35 million in crypto reportedly stolen. [...]
