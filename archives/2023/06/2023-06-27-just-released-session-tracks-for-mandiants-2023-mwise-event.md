Title: Just released: Session tracks for Mandiant’s 2023 mWISE event
Date: 2023-06-27T10:02:04-04:00
Author: Sponsored by Mandiant
Category: BleepingComputer
Tags: Security
Slug: 2023-06-27-just-released-session-tracks-for-mandiants-2023-mwise-event

[Source](https://www.bleepingcomputer.com/news/security/just-released-session-tracks-for-mandiants-2023-mwise-event/){:target="_blank" rel="noopener"}

> There are just a few days left to get the lowest price available for the mWISE cybersecurity conference. It runs from September 18 - 20, 2023 in Washington, DC. If you register now, you'll get 45% off the standard conference rate. [...]
