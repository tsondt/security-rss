Title: Posing as journalists, Pink Drainer pilfers $3.3M in crypto
Date: 2023-06-12T20:00:15+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-06-12-posing-as-journalists-pink-drainer-pilfers-33m-in-crypto

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/12/pink_drainer_crypto_scam/){:target="_blank" rel="noopener"}

> First the interview, then the phishing attack Miscreants targeting Discord and Twitter accounts have stolen more than $3.3 million in cryptocurrency from 2,300 victims so far in an ongoing campaign that started in April and saw the highest spike in activity earlier this month.... [...]
