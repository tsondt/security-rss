Title: Uncle Sam wants DEF CON hackers to pwn this Moonlighter satellite in space
Date: 2023-06-03T08:25:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-03-uncle-sam-wants-def-con-hackers-to-pwn-this-moonlighter-satellite-in-space

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/03/moonlighter_satellite_hacking/){:target="_blank" rel="noopener"}

> 'World's first and only' orbiting infosec playpen due to blast off Sunday Feature Assuming the weather and engineering gods cooperate, a US government-funded satellite dubbed Moonlighter will launch at 1212 EDT (1612 UTC) on Sunday, hitching a ride on a SpaceX rocket before being releasing into Earth's orbit.... [...]
