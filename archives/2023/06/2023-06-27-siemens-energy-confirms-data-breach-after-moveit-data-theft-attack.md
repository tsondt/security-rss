Title: Siemens Energy confirms data breach after MOVEit data-theft attack
Date: 2023-06-27T14:11:47-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-27-siemens-energy-confirms-data-breach-after-moveit-data-theft-attack

[Source](https://www.bleepingcomputer.com/news/security/siemens-energy-confirms-data-breach-after-moveit-data-theft-attack/){:target="_blank" rel="noopener"}

> Siemens Energy has confirmed that data was stolen during the recent Clop ransomware data-theft attacks using a zero-day vulnerability in the MOVEit Transfer platform. [...]
