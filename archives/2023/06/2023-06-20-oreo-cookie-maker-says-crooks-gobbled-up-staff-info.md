Title: Oreo cookie maker says crooks gobbled up staff info
Date: 2023-06-20T21:01:32+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-20-oreo-cookie-maker-says-crooks-gobbled-up-staff-info

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/20/mondelez_third_party_breach/){:target="_blank" rel="noopener"}

> 50K-plus employees' personal info swiped after law firm rolled Mondelez International has warned 51,000 of its past and present employees that their personal information has been stolen from a law firm hired by the Oreo and Ritz cracker giant.... [...]
