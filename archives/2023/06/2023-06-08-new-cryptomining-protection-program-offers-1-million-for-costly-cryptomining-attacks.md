Title: New Cryptomining Protection Program offers $1 million for costly cryptomining attacks
Date: 2023-06-08T15:00:00+00:00
Author: Tim Peacock
Category: GCP Security
Tags: Security & Identity
Slug: 2023-06-08-new-cryptomining-protection-program-offers-1-million-for-costly-cryptomining-attacks

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-offers-customers-financial-help-for-cryptomining-attacks/){:target="_blank" rel="noopener"}

> Cryptomining is a pervasive and costly threat to cloud environments. A single attack can result in unauthorized compute costs of hundreds of thousands of dollars in just days. Further, the Google Cybersecurity Action Team (GCAT) September 2022 Threat Horizons Report revealed that 65% of compromised cloud accounts experienced cryptocurrency mining. Stopping a cryptomining attack requires effective detection, which is why we have made it a focus of Security Command Center Premium, our built-in security and risk management solution for Google Cloud. To strengthen our customers’ confidence in their ability to quickly detect and stop cryptomining attacks, we are introducing a [...]
