Title: Ransomware scum hit Japanese pharma giant Eisai Group
Date: 2023-06-09T17:30:10+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-06-09-ransomware-scum-hit-japanese-pharma-giant-eisai-group

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/09/eisai_group_hit_by_ransomware/){:target="_blank" rel="noopener"}

> Some servers encrypted in weekend attack, but product supply not affected Japanese pharma giant Eisai today confirmed to The Register that "there is no imminent risk of stock shortage" after it was hit by ransomware at the weekend.... [...]
