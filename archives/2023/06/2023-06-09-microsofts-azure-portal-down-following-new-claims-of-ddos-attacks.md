Title: Microsoft’s Azure portal down following new claims of DDoS attacks
Date: 2023-06-09T11:52:43-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-06-09-microsofts-azure-portal-down-following-new-claims-of-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/microsoft/microsofts-azure-portal-down-following-new-claims-of-ddos-attacks/){:target="_blank" rel="noopener"}

> The Microsoft Azure Portal is down on the web as a threat actor known as Anonymous Suda claims to be targeting the site with a DDoS attack. [...]
