Title: New Horabot campaign takes over victim's Gmail, Outlook accounts
Date: 2023-06-01T16:54:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-01-new-horabot-campaign-takes-over-victims-gmail-outlook-accounts

[Source](https://www.bleepingcomputer.com/news/security/new-horabot-campaign-takes-over-victims-gmail-outlook-accounts/){:target="_blank" rel="noopener"}

> A previously unknown campaign involving the Hotabot botnet malware has targeted Spanish-speaking users in Latin America since at least November 2020, infecting them with a banking trojan and spam tool. [...]
