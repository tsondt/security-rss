Title: Security and Human Behavior (SHB) 2023
Date: 2023-06-16T19:07:58+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;conferences;cybersecurity;security conferences
Slug: 2023-06-16-security-and-human-behavior-shb-2023

[Source](https://www.schneier.com/blog/archives/2023/06/security-and-human-behavior-shb-2023.html){:target="_blank" rel="noopener"}

> I’m just back from the sixteenth Workshop on Security and Human Behavior, hosted by Alessandro Acquisti at Carnegie Mellon University in Pittsburgh. SHB is a small, annual, invitational workshop of people studying various aspects of the human side of security, organized each year by Alessandro Acquisti, Ross Anderson, and myself. The fifty or so attendees include psychologists, economists, computer security researchers, criminologists, sociologists, political scientists, designers, lawyers, philosophers, anthropologists, geographers, neuroscientists, business school professors, and a smattering of others. It’s not just an interdisciplinary event; most of the people here are individually interdisciplinary. Our goal is always to maximize discussion [...]
