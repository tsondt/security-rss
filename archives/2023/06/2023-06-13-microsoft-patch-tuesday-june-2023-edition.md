Title: Microsoft Patch Tuesday, June 2023 Edition
Date: 2023-06-13T20:44:28+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Security Tools;Time to Patch;Action1;CVE-2023-28310;CVE-2023-29357;CVE-2023-29363;CVE-2023-32014;CVE-2023-32015;CVE-2023-32031;Immersive Labs;Kevin Breen;Microsoft Patch Tuesday June 2023
Slug: 2023-06-13-microsoft-patch-tuesday-june-2023-edition

[Source](https://krebsonsecurity.com/2023/06/microsoft-patch-tuesday-june-2023-edition/){:target="_blank" rel="noopener"}

> Microsoft Corp. today released software updates to fix dozens of security vulnerabilities in its Windows operating systems and other software. This month’s relatively light patch load has another added bonus for system administrators everywhere: It appears to be the first Patch Tuesday since March 2022 that isn’t marred by the active exploitation of a zero-day vulnerability in Microsoft’s products. June’s Patch Tuesday features updates to plug at least 70 security holes, and while none of these are reported by Microsoft as exploited in-the-wild yet, Redmond has flagged several in particular as “more likely to be exploited.” Top of the list [...]
