Title: LockBit victims in the US alone paid over $90m in ransoms since 2020
Date: 2023-06-14T19:42:21+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-14-lockbit-victims-in-the-us-alone-paid-over-90m-in-ransoms-since-2020

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/14/lockbit_joint_advisory/){:target="_blank" rel="noopener"}

> As America, UK, Canada, Australia and friends share essential bible to detect and thwart infections Seven nations today issued an alert, plus protection tips, about LockBit, the prolific ransomware-as-a-service gang.... [...]
