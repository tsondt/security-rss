Title: Japan's digital ID card gets emergency review amid data leaks
Date: 2023-06-22T04:45:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-06-22-japans-digital-id-card-gets-emergency-review-amid-data-leaks

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/22/japan_my_number_security_review/){:target="_blank" rel="noopener"}

> PM wants response as urgent as that mustered for COVID-19 Japanese prime minister Fumio Kishida has ordered an emergency review of the nation's ID Cards, amid revelations of glitches and data leaks that threaten the government's digital services push.... [...]
