Title: Protect data from disasters using new Asynchronous Replication
Date: 2023-06-28T16:00:00+00:00
Author: Dengkui Xi
Category: GCP Security
Tags: Storage & Data Transfer;Security & Identity;Compute
Slug: 2023-06-28-protect-data-from-disasters-using-new-asynchronous-replication

[Source](https://cloud.google.com/blog/products/compute/introducing-persistent-disk-asynchronous-replication/){:target="_blank" rel="noopener"}

> In today's business landscape, data availability and integrity are paramount. Disasters, whether natural or man-made, can disrupt operations and pose a significant risk to critical information. To address this, today we are introducing Persistent Disk Asynchronous Replication, which enables disaster recovery for Compute Engine workloads by replicating data between Google Cloud regions, providing a sub-1 min Recovery Point Objective (RPO) and low Recovery Time Objective (RTO). Simplicity is central to the design of this solution. Replication is managed with a few API calls — there are no required VM agents, no dedicated replication VMs, no constraints on supported guest operating [...]
