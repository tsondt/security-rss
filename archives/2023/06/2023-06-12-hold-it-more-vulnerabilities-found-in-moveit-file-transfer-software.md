Title: Hold it – more vulnerabilities found in MOVEit file transfer software
Date: 2023-06-12T10:33:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-06-12-hold-it-more-vulnerabilities-found-in-moveit-file-transfer-software

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/12/security_in_brief/){:target="_blank" rel="noopener"}

> Also, the FBI's $180k investment in AN0M keeps paying off, and this week's critical vulnerabilities Infosec in brief Security firms helping Progress Software dissect the fallout from a ransomware attack against its MOVEit file transfer suite have discovered more issues that the company said could be used to stage additional exploits.... [...]
