Title: UK government to set deadline for removal of Chinese surveillance cams
Date: 2023-06-08T07:30:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-06-08-uk-government-to-set-deadline-for-removal-of-chinese-surveillance-cams

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/08/uk_government_china_cam_removal/){:target="_blank" rel="noopener"}

> And compile a list of vendors considered threats to national security The UK government will set a deadline for removing made-in-China surveillance cameras from "sensitive sites."... [...]
