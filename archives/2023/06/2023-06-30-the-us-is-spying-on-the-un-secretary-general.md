Title: The US Is Spying on the UN Secretary General
Date: 2023-06-30T11:02:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;espionage;FISA;leaks;NSA
Slug: 2023-06-30-the-us-is-spying-on-the-un-secretary-general

[Source](https://www.schneier.com/blog/archives/2023/06/the-us-is-spying-on-the-un-secretary-general.html){:target="_blank" rel="noopener"}

> The Washington Post is reporting that the US is spying on the UN Secretary General. The reports on Guterres appear to contain the secretary general’s personal conversations with aides regarding diplomatic encounters. They indicate that the United States relied on spying powers granted under the Foreign Intelligence Surveillance Act (FISA) to gather the intercepts. Lots of details about different conversations in the article, which are based on classified documents leaked on Discord by Jack Teixeira. There will probably a lot of faux outrage at this, but spying on foreign leaders is a perfectly legitimate use of the NSA’s capabilities and [...]
