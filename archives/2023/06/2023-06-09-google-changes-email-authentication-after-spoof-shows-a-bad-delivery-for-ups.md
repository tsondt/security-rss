Title: Google changes email authentication after spoof shows a bad delivery for UPS
Date: 2023-06-09T01:02:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-09-google-changes-email-authentication-after-spoof-shows-a-bad-delivery-for-ups

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/09/google_bimi_email_authentication/){:target="_blank" rel="noopener"}

> Google's blue tick proves untrustworthy Google says it has fixed a flaw that allowed a scammer to impersonate delivery service UPS on Gmail, after the data-hoarding web behemoth labeled the phony email as authentic.... [...]
