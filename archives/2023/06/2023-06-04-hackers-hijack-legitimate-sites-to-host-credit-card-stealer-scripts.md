Title: Hackers hijack legitimate sites to host credit card stealer scripts
Date: 2023-06-04T10:16:32-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-04-hackers-hijack-legitimate-sites-to-host-credit-card-stealer-scripts

[Source](https://www.bleepingcomputer.com/news/security/hackers-hijack-legitimate-sites-to-host-credit-card-stealer-scripts/){:target="_blank" rel="noopener"}

> A new Magecart credit card stealing campaign hijacks legitimate sites to act as "makeshift" command and control (C2) servers to inject and hide the skimmers on targeted eCommerce sites. [...]
