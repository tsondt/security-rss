Title: Introducing client authentication with Mutual TLS on Google Cloud Load Balancing
Date: 2023-06-16T16:00:00+00:00
Author: Joanna Solmon
Category: GCP Security
Tags: Security & Identity;Networking
Slug: 2023-06-16-introducing-client-authentication-with-mutual-tls-on-google-cloud-load-balancing

[Source](https://cloud.google.com/blog/products/networking/frontend-mtls-support-for-external-https-load-balancing/){:target="_blank" rel="noopener"}

> We are excited to announce the Preview of front-end mutual TLS (mTLS) support, allowing you to offload client certificate authentication using External HTTPS Load Balancing. With TLS offload the load balancer presents a certificate on behalf of the server that the client uses to verify the server’s identity. Now with frontend mTLS offload, the load-balancer can additionally request a certificate from the client and use that to verify the client’s identity. Use cases mTLS support can help customers meet compliance requirements for regulatory standards, such as OpenBanking, where applications need the load balancer to authenticate the identity of clients that [...]
