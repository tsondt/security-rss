Title: Have I Been Pwned warns of new Zacks data breach impacting 8 million
Date: 2023-06-12T13:59:35-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-12-have-i-been-pwned-warns-of-new-zacks-data-breach-impacting-8-million

[Source](https://www.bleepingcomputer.com/news/security/have-i-been-pwned-warns-of-new-zacks-data-breach-impacting-8-million/){:target="_blank" rel="noopener"}

> Zacks Investment Research (Zacks) has reportedly suffered an older, previously undisclosed data breach impacting 8.8 million customers, with the database now shared on a hacking forum. [...]
