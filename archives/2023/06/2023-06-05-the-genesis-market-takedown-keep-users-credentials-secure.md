Title: The Genesis Market Takedown – Keep Users Credentials Secure
Date: 2023-06-05T10:05:10-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-06-05-the-genesis-market-takedown-keep-users-credentials-secure

[Source](https://www.bleepingcomputer.com/news/security/the-genesis-market-takedown-keep-users-credentials-secure/){:target="_blank" rel="noopener"}

> Law enforcement arrested over 100 people in the takedown of the Genesis Market, notorious for selling stolen credentials. To prevent the loss of credentials, it's important to adopt a layered defense. [...]
