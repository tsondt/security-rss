Title: Free Akira ransomware decryptor helps recover your files
Date: 2023-06-30T12:45:31-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2023-06-30-free-akira-ransomware-decryptor-helps-recover-your-files

[Source](https://www.bleepingcomputer.com/news/security/free-akira-ransomware-decryptor-helps-recover-your-files/){:target="_blank" rel="noopener"}

> Cybersecurity firm Avast has released a free decryptor for the Akira ransomware that can help victims recover their data without paying the crooks any money. [...]
