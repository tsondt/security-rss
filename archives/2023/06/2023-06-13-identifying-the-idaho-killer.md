Title: Identifying the Idaho Killer
Date: 2023-06-13T11:03:38+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2023-06-13-identifying-the-idaho-killer

[Source](https://www.schneier.com/blog/archives/2023/06/identifying-the-idaho-killer.html){:target="_blank" rel="noopener"}

> The New York Times has a long article on the investigative techniques used to identify the person who stabbed and killed four University of Idaho students. Pay attention to the techniques: The case has shown the degree to which law enforcement investigators have come to rely on the digital footprints that ordinary Americans leave in nearly every facet of their lives. Online shopping, car sales, carrying a cellphone, drives along city streets and amateur genealogy all played roles in an investigation that was solved, in the end, as much through technology as traditional sleuthing. [...] At that point, investigators decided [...]
