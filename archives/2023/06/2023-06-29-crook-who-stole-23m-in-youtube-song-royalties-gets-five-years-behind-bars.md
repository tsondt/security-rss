Title: Crook who stole $23m+ in YouTube song royalties gets five years behind bars
Date: 2023-06-29T23:38:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-29-crook-who-stole-23m-in-youtube-song-royalties-gets-five-years-behind-bars

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/29/youtube_scammer_jailed/){:target="_blank" rel="noopener"}

> Claims he wants to stay in the music biz after time in a Sing Sing One of the two men who admitted stealing more than $23 million in royalty payments for songs played on YouTube has been sentenced to nearly six years behind bars for his role in what prosecutors called "one of the largest music-royalty frauds ever."... [...]
