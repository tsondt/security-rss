Title: Three ways to accelerate incident response in the cloud: insights from re:Inforce 2023
Date: 2023-06-30T19:46:58+00:00
Author: Anne Grahn
Category: AWS Security
Tags: Best Practices;Foundational (100);Security, Identity, & Compliance;Thought Leadership;Amazon Athena;Amazon Detective;Amazon EKS;Amazon EventBridge;Amazon GuardDuty;Amazon Inspector;Amazon Security Lake;AWS Organizations;AWS re:Inforce;AWS Security Hub;AWS Systems Manager Incident Manager;AWS Wickr;cryptography;Incident response;OpenSearch;re:Inforce 2023;Security;Security Blog;threat detection
Slug: 2023-06-30-three-ways-to-accelerate-incident-response-in-the-cloud-insights-from-reinforce-2023

[Source](https://aws.amazon.com/blogs/security/three-ways-to-accelerate-incident-response-in-the-cloud-insights-from-reinforce-2023/){:target="_blank" rel="noopener"}

> AWS re:Inforce took place in Anaheim, California, on June 13–14, 2023. AWS customers, partners, and industry peers participated in hundreds of technical and non-technical security-focused sessions across six tracks, an Expo featuring AWS experts and AWS Security Competency Partners, and keynote and leadership sessions. The threat detection and incident response track showcased how AWS customers [...]
