Title: Zyxel warns of critical command injection flaw in NAS devices
Date: 2023-06-20T10:26:31-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2023-06-20-zyxel-warns-of-critical-command-injection-flaw-in-nas-devices

[Source](https://www.bleepingcomputer.com/news/security/zyxel-warns-of-critical-command-injection-flaw-in-nas-devices/){:target="_blank" rel="noopener"}

> Zyxel is warning its NAS (Network Attached Storage) devices users to update their firmware to fix a critical severity command injection vulnerability. [...]
