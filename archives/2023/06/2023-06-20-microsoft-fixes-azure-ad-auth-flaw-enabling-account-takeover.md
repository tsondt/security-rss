Title: Microsoft fixes Azure AD auth flaw enabling account takeover
Date: 2023-06-20T12:38:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-06-20-microsoft-fixes-azure-ad-auth-flaw-enabling-account-takeover

[Source](https://www.bleepingcomputer.com/news/security/microsoft-fixes-azure-ad-auth-flaw-enabling-account-takeover/){:target="_blank" rel="noopener"}

> Microsoft has addressed an Azure Active Directory (Azure AD) authentication flaw that could allow threat actors to escalate privileges and potentially fully take over the target's account. [...]
