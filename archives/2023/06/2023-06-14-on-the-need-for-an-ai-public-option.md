Title: On the Need for an AI Public Option
Date: 2023-06-14T11:02:28+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;essays;LLM
Slug: 2023-06-14-on-the-need-for-an-ai-public-option

[Source](https://www.schneier.com/blog/archives/2023/06/on-the-need-for-an-ai-public-option.html){:target="_blank" rel="noopener"}

> Artificial intelligence will bring great benefits to all of humanity. But do we really want to entrust this revolutionary technology solely to a small group of US tech companies? Silicon Valley has produced no small number of moral disappointments. Google retired its “don’t be evil” pledge before firing its star ethicist. Self-proclaimed “ free speech absolutist ” Elon Musk bought Twitter in order to censor political speech, retaliate against journalists, and ease access to the platform for Russian and Chinese propagandists. Facebook lied about how it enabled Russian interference in the 2016 US presidential election and paid a public relations [...]
