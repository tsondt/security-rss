Title: Android spyware camouflaged as VPN, chat apps on Google Play
Date: 2023-06-19T11:22:50-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-06-19-android-spyware-camouflaged-as-vpn-chat-apps-on-google-play

[Source](https://www.bleepingcomputer.com/news/security/android-spyware-camouflaged-as-vpn-chat-apps-on-google-play/){:target="_blank" rel="noopener"}

> Three Android apps on Google Play were used by state-sponsored threat actors to collect intelligence from targeted devices, such as location data and contact lists. [...]
