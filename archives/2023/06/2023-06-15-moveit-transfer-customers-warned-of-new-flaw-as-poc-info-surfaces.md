Title: MOVEit Transfer customers warned of new flaw as PoC info surfaces
Date: 2023-06-15T16:58:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-15-moveit-transfer-customers-warned-of-new-flaw-as-poc-info-surfaces

[Source](https://www.bleepingcomputer.com/news/security/moveit-transfer-customers-warned-of-new-flaw-as-poc-info-surfaces/){:target="_blank" rel="noopener"}

> Progress warned MOVEit Transfer customers to restrict all HTTP access to their environments after info on a new SQL injection (SQLi) vulnerability was shared online today. [...]
