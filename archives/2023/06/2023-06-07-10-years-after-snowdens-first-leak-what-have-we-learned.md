Title: 10 years after Snowden's first leak, what have we learned?
Date: 2023-06-07T13:25:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-07-10-years-after-snowdens-first-leak-what-have-we-learned

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/07/10_years_after_snowden/){:target="_blank" rel="noopener"}

> Spies gonna spy Feature The world got a first glimpse into the US government's far-reaching surveillance of American citizens' communications – namely, their Verizon telephone calls – 10 years ago this week when Edward Snowden's initial leaks hit the press.... [...]
