Title: TSMC denies LockBit hack as ransomware gang demands $70 million
Date: 2023-06-30T09:45:30-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2023-06-30-tsmc-denies-lockbit-hack-as-ransomware-gang-demands-70-million

[Source](https://www.bleepingcomputer.com/news/security/tsmc-denies-lockbit-hack-as-ransomware-gang-demands-70-million/){:target="_blank" rel="noopener"}

> Chipmaking giant TSMC (Taiwan Semiconductor Manufacturing Company) denied being hacked after the LockBit ransomware gang demanded $70 million not to release stolen data. [...]
