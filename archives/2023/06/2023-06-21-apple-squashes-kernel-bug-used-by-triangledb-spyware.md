Title: Apple squashes kernel bug used by TriangleDB spyware
Date: 2023-06-21T20:26:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-21-apple-squashes-kernel-bug-used-by-triangledb-spyware

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/21/apple_patches_triangledb_spyware/){:target="_blank" rel="noopener"}

> Snoops may be targeting macOS in addition to iPhones, Kaspersky says Whoever is infecting people's iPhones with the TriangleDB spyware may be targeting macOS computers with similar malware, according to Kaspersky researchers.... [...]
