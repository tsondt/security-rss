Title: UPS Data Harvested for SMS Phishing Attacks
Date: 2023-06-23T14:55:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybercrime;phishing;phones;SMS;spam
Slug: 2023-06-23-ups-data-harvested-for-sms-phishing-attacks

[Source](https://www.schneier.com/blog/archives/2023/06/ups-data-harvested-for-sms-phishing-attacks.html){:target="_blank" rel="noopener"}

> I get UPS phishing spam on my phone all the time. I never click on it, because it’s so obviously spam. Turns out that hackers have been harvesting actual UPS delivery data from a Canadian tracking tool for its phishing SMSs. [...]
