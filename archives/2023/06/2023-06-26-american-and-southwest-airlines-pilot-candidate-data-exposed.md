Title: American and Southwest Airlines pilot candidate data exposed
Date: 2023-06-26T15:29:12+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-06-26-american-and-southwest-airlines-pilot-candidate-data-exposed

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/26/american_southwest_airline_breach/){:target="_blank" rel="noopener"}

> Time to start practicing identity protection A vendor that operates a pilot recruitment platform used by maor airlines exposed the personal files of more than 8,000 pilot and cadet applicants at American Airlines and Southwest Airlines.... [...]
