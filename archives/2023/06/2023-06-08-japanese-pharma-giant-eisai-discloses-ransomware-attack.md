Title: Japanese pharma giant Eisai discloses ransomware attack
Date: 2023-06-08T10:22:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-08-japanese-pharma-giant-eisai-discloses-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/japanese-pharma-giant-eisai-discloses-ransomware-attack/){:target="_blank" rel="noopener"}

> Pharmaceutical company Eisai has disclosed it suffered a ransomware incident that impacted its operations, admitting that attackers encrypted some of its servers. [...]
