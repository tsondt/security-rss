Title: Friday Squid Blogging: Giggling Squid
Date: 2023-06-23T21:06:57+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-06-23-friday-squid-blogging-giggling-squid

[Source](https://www.schneier.com/blog/archives/2023/06/friday-squid-blogging-giggling-squid.html){:target="_blank" rel="noopener"}

> Giggling Squid is a Thai chain in the UK. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
