Title: Fortinet fixes critical RCE flaw in Fortigate SSL-VPN devices, patch now
Date: 2023-06-11T11:43:48-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-06-11-fortinet-fixes-critical-rce-flaw-in-fortigate-ssl-vpn-devices-patch-now

[Source](https://www.bleepingcomputer.com/news/security/fortinet-fixes-critical-rce-flaw-in-fortigate-ssl-vpn-devices-patch-now/){:target="_blank" rel="noopener"}

> Fortinet has released new Fortigate firmware updates that fix an undisclosed, critical pre-authentication remote code execution vulnerability in SSL VPN devices. [...]
