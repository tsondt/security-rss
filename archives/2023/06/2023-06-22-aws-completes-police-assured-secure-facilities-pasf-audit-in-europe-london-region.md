Title: AWS completes Police-Assured Secure Facilities (PASF) audit in Europe (London) Region
Date: 2023-06-22T14:57:02+00:00
Author: Vishal Pabari
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS security;Compliance;Security Blog;United Kingdom
Slug: 2023-06-22-aws-completes-police-assured-secure-facilities-pasf-audit-in-europe-london-region

[Source](https://aws.amazon.com/blogs/security/aws-completes-police-assured-secure-facilities-pasf-audit-in-europe-london-region/){:target="_blank" rel="noopener"}

> We’re excited to announce that our Europe (London) Region has renewed our accreditation for United Kingdom (UK) Police-Assured Secure Facilities (PASF) for Official-Sensitive data. Since 2017, the Amazon Web Services (AWS) Europe (London) Region has been assured under the PASF program. This demonstrates our continuous commitment to adhere to the heightened expectations of customers with [...]
