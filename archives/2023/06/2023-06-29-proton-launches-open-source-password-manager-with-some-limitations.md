Title: Proton launches open-source password manager with some limitations
Date: 2023-06-29T10:56:35-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-29-proton-launches-open-source-password-manager-with-some-limitations

[Source](https://www.bleepingcomputer.com/news/security/proton-launches-open-source-password-manager-with-some-limitations/){:target="_blank" rel="noopener"}

> Proton AG has announced the global availability of Proton Pass, an open-source and free-to-use password manager available as a browser extension or mobile app on Android and iOS.manager. [...]
