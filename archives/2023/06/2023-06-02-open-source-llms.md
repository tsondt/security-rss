Title: Open-Source LLMs
Date: 2023-06-02T14:21:40+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;essays;Meta;open source
Slug: 2023-06-02-open-source-llms

[Source](https://www.schneier.com/blog/archives/2023/06/open-source-llms.html){:target="_blank" rel="noopener"}

> In February, Meta released its large language model: LLaMA. Unlike OpenAI and its ChatGPT, Meta didn’t just give the world a chat window to play with. Instead, it released the code into the open-source community, and shortly thereafter the model itself was leaked. Researchers and programmers immediately started modifying it, improving it, and getting it to do things no one else anticipated. And their results have been immediate, innovative, and an indication of how the future of this technology is going to play out. Training speeds have hugely increased, and the size of the models themselves has shrunk to the [...]
