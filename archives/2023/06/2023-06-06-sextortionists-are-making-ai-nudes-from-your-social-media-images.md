Title: Sextortionists are making AI nudes from your social media images
Date: 2023-06-06T16:43:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-06-sextortionists-are-making-ai-nudes-from-your-social-media-images

[Source](https://www.bleepingcomputer.com/news/security/sextortionists-are-making-ai-nudes-from-your-social-media-images/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) is warning of a rising trend of malicious actors creating deepfake nude content from social media images to perform sextortion attacks. [...]
