Title: Fears grow of deepfake ID scams following Progress hack
Date: 2023-06-28T13:38:58+00:00
Author: Financial Times
Category: Ars Technica
Tags: AI;Biz & IT;darknet;data theft;deepfakes;hacking;syndication
Slug: 2023-06-28-fears-grow-of-deepfake-id-scams-following-progress-hack

[Source](https://arstechnica.com/?p=1950562){:target="_blank" rel="noopener"}

> Enlarge / The number of deepfakes used in scams in just the first three months of 2023 outstripped all of 2022. (credit: FT Montage/Getty Images) When Progress Corp, the Massachusetts-based maker of business software, revealed its file transfer system had been compromised this month, the issue quickly gathered global significance. A Russian-speaking gang dubbed Cl0p had used the vulnerability to steal sensitive information from hundreds of companies including British Airways, Shell and PwC. It had been expected that the hackers would then attempt to extort affected organizations, threatening to release their data unless a ransom was paid. However, cyber security [...]
