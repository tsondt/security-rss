Title: AWS Security Profile – Cryptography Edition: Valerie Lambert, Senior Software Development Engineer
Date: 2023-06-07T20:26:02+00:00
Author: Roger Park
Category: AWS Security
Tags: Security, Identity, & Compliance;AWS re:Inforce;AWS Security Profile;AWS Security Profiles;cryptography;Live Events;re:Inforce;re:Inforce 2023;Security Blog
Slug: 2023-06-07-aws-security-profile-cryptography-edition-valerie-lambert-senior-software-development-engineer

[Source](https://aws.amazon.com/blogs/security/aws-security-profile-cryptography-edition-valerie-lambert-senior-software-development-engineer/){:target="_blank" rel="noopener"}

> In the AWS Security Profile series, we interview Amazon Web Services (AWS) experts who help keep our customers safe and secure. This interview features Valerie Lambert, Senior Software Development Engineer, Crypto Tools, and upcoming AWS re:Inforce 2023 speaker, who shares thoughts on data protection, cloud security, cryptography tools, and more. What do you do in [...]
