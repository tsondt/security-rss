Title: RDP honeypot targeted 3.5 million times in brute-force attacks
Date: 2023-06-13T03:38:14-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-06-13-rdp-honeypot-targeted-35-million-times-in-brute-force-attacks

[Source](https://www.bleepingcomputer.com/news/security/rdp-honeypot-targeted-35-million-times-in-brute-force-attacks/){:target="_blank" rel="noopener"}

> Remote desktop connections are so powerful a magnet for hackers that an exposed connection can average more than 37,000 times every day from various IP addresses. [...]
