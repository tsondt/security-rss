Title: Amazon Ring, Alexa accused of every nightmare IoT security fail you can imagine
Date: 2023-06-01T06:33:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-06-01-amazon-ring-alexa-accused-of-every-nightmare-iot-security-fail-you-can-imagine

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/01/ftc_alexa_ring_amazon_settlement/){:target="_blank" rel="noopener"}

> Staff able to watch customers in the bathroom? Tick! Obviously shabby infosec? Tick! Training AI as an excuse for data retention? Tick! America's Federal Trade Commission has made Amazon a case study for every cautionary tale about how sloppily designed internet-of-things devices and associated services represent a risk to privacy – and made the cost of those actions, as alleged, a mere $30.8 million.... [...]
