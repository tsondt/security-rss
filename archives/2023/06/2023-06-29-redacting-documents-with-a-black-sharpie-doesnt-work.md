Title: Redacting Documents with a Black Sharpie Doesn’t Work
Date: 2023-06-29T14:37:59+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Microsoft;redaction;Sony
Slug: 2023-06-29-redacting-documents-with-a-black-sharpie-doesnt-work

[Source](https://www.schneier.com/blog/archives/2023/06/redacting-documents-with-a-black-sharpie-doesnt-work.html){:target="_blank" rel="noopener"}

> We have learned this lesson again : As part of the FTC v. Microsoft hearing, Sony supplied a document from PlayStation chief Jim Ryan that includes redacted details on the margins Sony shares with publishers, its Call of Duty revenues, and even the cost of developing some of its games. It looks like someone redacted the documents with a black Sharpie ­ but when you scan them in, it’s easy to see some of the redactions. Oops. I don’t particularly care about the redacted information, but it’s there in the article. [...]
