Title: Updated AWS Ramp-Up Guide available for security, identity, and compliance
Date: 2023-06-06T16:06:46+00:00
Author: Anna McAbee
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS security training;Security;Security Blog
Slug: 2023-06-06-updated-aws-ramp-up-guide-available-for-security-identity-and-compliance

[Source](https://aws.amazon.com/blogs/security/updated-aws-ramp-up-guide-available-for-security-identity-and-compliance/){:target="_blank" rel="noopener"}

> To support our customers in securing their Amazon Web Services (AWS) environment, AWS offers digital training, whitepapers, blog posts, videos, workshops, and documentation to learn about security in the cloud. The AWS Ramp-Up Guide: Security is designed to help you quickly learn what is most important to you when it comes to security, identity, and [...]
