Title: Power LED Side-Channel Attack
Date: 2023-06-19T10:52:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2023-06-19-power-led-side-channel-attack

[Source](https://www.schneier.com/blog/archives/2023/06/power-led-side-channel-attack.html){:target="_blank" rel="noopener"}

> This is a clever new side-channel attack : The first attack uses an Internet-connected surveillance camera to take a high-speed video of the power LED on a smart card reader­or of an attached peripheral device­during cryptographic operations. This technique allowed the researchers to pull a 256-bit ECDSA key off the same government-approved smart card used in Minerva. The other allowed the researchers to recover the private SIKE key of a Samsung Galaxy S8 phone by training the camera of an iPhone 13 on the power LED of a USB speaker connected to the handset, in a similar way to how [...]
