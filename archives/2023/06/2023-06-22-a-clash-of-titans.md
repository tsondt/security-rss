Title: A clash of titans
Date: 2023-06-22T03:12:13+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-06-22-a-clash-of-titans

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/22/a_clash_of_titans/){:target="_blank" rel="noopener"}

> Shielding with protective AI from bad actors using AI for cyberattacks Webinar The one thing a cyber security team can rarely afford to do is relax its vigilance. But count the collective manhours spent on the frontline and the figure starts to look unsustainable, leaving many organizations with little choice but to engage with technology to help defend against malign intent.... [...]
