Title: Tackling the cyber skills gap with AI
Date: 2023-06-27T08:34:05+00:00
Author: John E Dunn
Category: The Register
Tags: 
Slug: 2023-06-27-tackling-the-cyber-skills-gap-with-ai

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/27/tackling_the_cyber_skills_gap/){:target="_blank" rel="noopener"}

> Why the future of cyber security could be fully autonomous where the AI works independently Sponsored Feature The cybersecurity sector, it is now routinely attested, is in the midst of a long-term skills crisis.... [...]
