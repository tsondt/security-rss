Title: Why Snap chose BeyondCorp Enterprise to build a durable Zero Trust framework
Date: 2023-06-14T16:00:00+00:00
Author: Rahul Ramachandran
Category: GCP Security
Tags: Customers;Security & Identity
Slug: 2023-06-14-why-snap-chose-beyondcorp-enterprise-to-build-a-durable-zero-trust-framework

[Source](https://cloud.google.com/blog/products/identity-security/why-snap-chose-beyondcorp-enterprise-to-build-a-durable-zero-trust-framework/){:target="_blank" rel="noopener"}

> Snap Inc., creators of Snapchat, is a technology company that believes “the camera presents the greatest opportunity to improve the way people live and communicate.” Born in the cloud, Snap delivers its modern social media framework using Google Cloud. In 2023, Google Cloud and Snap announced an expanded relationship that included Snap choosing Google Cloud for more workloads, including artificial intelligence, machine learning, and security services. One of the key aspects to securing these cloud workloads is a Zero Trust access framework, which became an essential part of Snap’s cloud journey. "Beyond Corp Enterprise (BCE) is a focal component of [...]
