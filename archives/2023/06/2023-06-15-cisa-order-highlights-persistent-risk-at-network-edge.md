Title: CISA Order Highlights Persistent Risk at Network Edge
Date: 2023-06-15T15:40:09+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;The Coming Storm;Time to Patch;Adam Boileau;Barracuda Networks;CISA;CVE-2023-27997;Cybersecurity and Infrastructure Security Agency;Fortinet;Fortra;GoAnywhere;Mandiant;MOVEit Transfer;Patrick Gray;Progress Software;Risky Business podcast
Slug: 2023-06-15-cisa-order-highlights-persistent-risk-at-network-edge

[Source](https://krebsonsecurity.com/2023/06/cisa-order-highlights-persistent-risk-at-network-edge/){:target="_blank" rel="noopener"}

> The U.S. government agency in charge of improving the nation’s cybersecurity posture is ordering all federal agencies to take new measures to restrict access to Internet-exposed networking equipment. The directive comes amid a surge in attacks targeting previously unknown vulnerabilities in widely used security and networking appliances. Under a new order from the Cybersecurity and Infrastructure Security Agency (CISA), federal agencies will have 14 days to respond to any reports from CISA about misconfigured or Internet-exposed networking equipment. The directive applies to any networking devices — such as firewalls, routers and load balancers — that allow remote authentication or administration. [...]
