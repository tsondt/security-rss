Title: SMS Phishers Harvested Phone Numbers, Shipment Data from UPS Tracking Tool
Date: 2023-06-22T19:11:33+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Web Fraud 2.0;Adidas;apple;Brian Hughes;Lego;smishing;SMS phishing;United Parcel Service;ups;UPS Canada Ltd.
Slug: 2023-06-22-sms-phishers-harvested-phone-numbers-shipment-data-from-ups-tracking-tool

[Source](https://krebsonsecurity.com/2023/06/sms-phishers-harvested-phone-numbers-shipment-data-from-ups-tracking-tool/){:target="_blank" rel="noopener"}

> The United Parcel Service (UPS) says fraudsters have been harvesting phone numbers and other information from its online shipment tracking tool in Canada to send highly targeted SMS phishing (a.k.a. “smishing”) messages that spoofed UPS and other top brands. The missives addressed recipients by name, included details about recent orders, and warned that those orders wouldn’t be shipped unless the customer paid an added delivery fee. In a snail mail letter sent this month to Canadian customers, UPS Canada Ltd. said it is aware that some package recipients have received fraudulent text messages demanding payment before a package can be [...]
