Title: VMware fixes critical vulnerabilities in vRealize network analytics tool
Date: 2023-06-07T11:09:39-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-07-vmware-fixes-critical-vulnerabilities-in-vrealize-network-analytics-tool

[Source](https://www.bleepingcomputer.com/news/security/vmware-fixes-critical-vulnerabilities-in-vrealize-network-analytics-tool/){:target="_blank" rel="noopener"}

> VMware issued multiple security patches today to address critical and high-severity vulnerabilities in VMware Aria Operations for Networks, allowing attackers to gain remote execution or access sensitive information. [...]
