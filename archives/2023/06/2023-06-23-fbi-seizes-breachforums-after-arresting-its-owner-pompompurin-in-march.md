Title: FBI seizes BreachForums after arresting its owner Pompompurin in March
Date: 2023-06-23T12:19:51-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-23-fbi-seizes-breachforums-after-arresting-its-owner-pompompurin-in-march

[Source](https://www.bleepingcomputer.com/news/security/fbi-seizes-breachforums-after-arresting-its-owner-pompompurin-in-march/){:target="_blank" rel="noopener"}

> U.S. law enforcement today seized the clear web domain of the notorious BreachForums (aka Breached) hacking forum three months after apprehending its owner Conor Fitzpatrick (aka Pompompurin), under cybercrime charges. [...]
