Title: Suspected LockBit ransomware affiliate arrested, charged in US
Date: 2023-06-15T13:33:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-06-15-suspected-lockbit-ransomware-affiliate-arrested-charged-in-us

[Source](https://www.bleepingcomputer.com/news/security/suspected-lockbit-ransomware-affiliate-arrested-charged-in-us/){:target="_blank" rel="noopener"}

> Russian national Ruslan Magomedovich Astamirov was arrested in Arizona and charged by the U.S. Justice Department for allegedly deploying LockBit ransomware on the networks of victims in the United States and abroad. [...]
