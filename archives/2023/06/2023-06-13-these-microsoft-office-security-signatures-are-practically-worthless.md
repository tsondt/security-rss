Title: These Microsoft Office security signatures are 'practically worthless'
Date: 2023-06-13T10:26:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-06-13-these-microsoft-office-security-signatures-are-practically-worthless

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/13/office_open_xml_signatures/){:target="_blank" rel="noopener"}

> Turns out it's easy to forge documents relying on OOXML Office Open XML (OOXML) Signatures, an Ecma/ISO standard used in Microsoft Office applications and open source OnlyOffice, have several security flaws and can be easily spoofed.... [...]
