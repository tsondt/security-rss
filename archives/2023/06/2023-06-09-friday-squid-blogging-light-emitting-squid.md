Title: Friday Squid Blogging: Light-Emitting Squid
Date: 2023-06-09T21:05:25+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-06-09-friday-squid-blogging-light-emitting-squid

[Source](https://www.schneier.com/blog/archives/2023/06/friday-squid-blogging-light-emitting-squid.html){:target="_blank" rel="noopener"}

> It’s a Taningia danae : Their arms are lined with two rows of sharp retractable hooks. And, like most deep-sea squid, they are adorned with light organs called photophores. They have some on the underside of their mantle. There are more facing upward, near one of their eyes. But it’s the photophores at the tip of two stubby arms that are truly unique. The size and shape of lemons­—each nestled within a retractable lid like an eyeball in a socket­—they are by far the largest photophores known to science. As usual, you can also use this squid post to talk [...]
