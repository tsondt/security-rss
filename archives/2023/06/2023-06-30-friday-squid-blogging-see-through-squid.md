Title: Friday Squid Blogging: See-Through Squid
Date: 2023-06-30T20:58:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-06-30-friday-squid-blogging-see-through-squid

[Source](https://www.schneier.com/blog/archives/2023/06/friday-squid-blogging-see-through-squid.html){:target="_blank" rel="noopener"}

> Doryteuthis opalescens is known as the market squid, and was critical in the recent squid RNA research. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
