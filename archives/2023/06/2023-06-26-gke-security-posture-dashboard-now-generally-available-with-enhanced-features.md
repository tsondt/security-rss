Title: GKE Security Posture dashboard now generally available with enhanced features
Date: 2023-06-26T16:00:00+00:00
Author: Daniel L'Hommedieu
Category: GCP Security
Tags: Containers & Kubernetes;Security & Identity
Slug: 2023-06-26-gke-security-posture-dashboard-now-generally-available-with-enhanced-features

[Source](https://cloud.google.com/blog/products/identity-security/gke-security-posture-now-generally-available-with-enhanced-features/){:target="_blank" rel="noopener"}

> We are excited to announce that the Google Kubernetes Engine (GKE) Security Posture dashboard is now generally available. The interface is designed to streamline the security management of your GKE clusters, and now includes a range of powerful features such as misconfiguration detection and vulnerability scanning to help ensure your applications remain safe and secure. As part of our goal to simplify security at scale by providing insights into larger and more complex clusters, we're introducing support for up to 1,000 GKE nodes to ensure that the Security Posture dashboard can be used with larger clusters. Support for this many [...]
