Title: Now Apple takes a bite out of encryption-bypassing 'spy clause' in UK internet law
Date: 2023-06-29T06:40:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-29-now-apple-takes-a-bite-out-of-encryption-bypassing-spy-clause-in-uk-internet-law

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/29/apple_online_safety_bill_opposition/){:target="_blank" rel="noopener"}

> Not the iPhone maker's first think-of-the-children rodeo Apple has joined the rapidly growing chorus of tech organizations calling on British lawmakers to revise the nation's Online Safety Bill – which for now is in the hands of the House of Lords – so that it safeguards strong end-to-end encryption.... [...]
