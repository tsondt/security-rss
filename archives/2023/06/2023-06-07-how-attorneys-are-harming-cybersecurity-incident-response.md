Title: How Attorneys Are Harming Cybersecurity Incident Response
Date: 2023-06-07T11:06:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;breaches;cybersecurity;incident response;insurance
Slug: 2023-06-07-how-attorneys-are-harming-cybersecurity-incident-response

[Source](https://www.schneier.com/blog/archives/2023/06/how-attorneys-are-harming-cybersecurity-incident-response.html){:target="_blank" rel="noopener"}

> New paper: “ Lessons Lost: Incident Response in the Age of Cyber Insurance and Breach Attorneys “: Abstract: Incident Response (IR) allows victim firms to detect, contain, and recover from security incidents. It should also help the wider community avoid similar attacks in the future. In pursuit of these goals, technical practitioners are increasingly influenced by stakeholders like cyber insurers and lawyers. This paper explores these impacts via a multi-stage, mixed methods research design that involved 69 expert interviews, data on commercial relationships, and an online validation workshop. The first stage of our study established 11 stylized facts that describe [...]
