Title: The Software-Defined Car
Date: 2023-06-05T11:14:49+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;cybersecurity;hacking
Slug: 2023-06-05-the-software-defined-car

[Source](https://www.schneier.com/blog/archives/2023/06/the-software-defined-car.html){:target="_blank" rel="noopener"}

> Developers are starting to talk about the software-defined car. For decades, features have accumulated like cruft in new vehicles: a box here to control the antilock brakes, a module there to run the cruise control radar, and so on. Now engineers and designers are rationalizing the way they go about building new models, taking advantage of much more powerful hardware to consolidate all those discrete functions into a small number of domain controllers. The behavior of new cars is increasingly defined by software, too. This is merely the progression of a trend that began at the end of the 1970s [...]
