Title: India probes medical info 'leak' to Telegram
Date: 2023-06-13T03:26:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-06-13-india-probes-medical-info-leak-to-telegram

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/13/asia_tech_news_roundup/){:target="_blank" rel="noopener"}

> PLUS: Vietnam's free domain names for youngsters; China's Cuba spy base; Hyundai and Samsung team for car chips; and more Asia In Brief India's government has denied its Co-WIN COVID-19 vaccination management platform has leaked data, but ordered an investigation into the program's security.... [...]
