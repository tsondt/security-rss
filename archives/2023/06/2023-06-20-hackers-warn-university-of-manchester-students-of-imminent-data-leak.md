Title: Hackers warn University of Manchester students’ of imminent data leak
Date: 2023-06-20T16:17:11-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-06-20-hackers-warn-university-of-manchester-students-of-imminent-data-leak

[Source](https://www.bleepingcomputer.com/news/security/hackers-warn-university-of-manchester-students-of-imminent-data-leak/){:target="_blank" rel="noopener"}

> The ransomware operation behind a cyberattack on the University of Manchester has begun to email students, warning that their data will soon be leaked after an extortion demand was not paid. [...]
