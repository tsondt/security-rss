Title: Microsoft links data wiping attacks to new Russian GRU hacking group
Date: 2023-06-14T13:27:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-06-14-microsoft-links-data-wiping-attacks-to-new-russian-gru-hacking-group

[Source](https://www.bleepingcomputer.com/news/security/microsoft-links-data-wiping-attacks-to-new-russian-gru-hacking-group/){:target="_blank" rel="noopener"}

> Microsoft has linked a threat group it tracks as Cadet Blizzard since April 2023 to Russia's Main Directorate of the General Staff of the Armed Forces (also known as GRU). [...]
