Title: The commonality of criminal intrusion
Date: 2023-06-13T15:07:05+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-06-13-the-commonality-of-criminal-intrusion

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/13/the_commonality_of_criminal_intrusion/){:target="_blank" rel="noopener"}

> Rubrik Zero Lab’s ‘The Hard Truths’ annual report into the state of data security Webinar It seems no longer possible to imagine whether it's just a case of if a security breach will occur within your organization, or if malicious actors will exploit a vulnerability to play havoc with your data. Rather, it's just a question of when.... [...]
