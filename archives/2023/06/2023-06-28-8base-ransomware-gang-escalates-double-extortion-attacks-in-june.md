Title: 8Base ransomware gang escalates double extortion attacks in June
Date: 2023-06-28T06:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-06-28-8base-ransomware-gang-escalates-double-extortion-attacks-in-june

[Source](https://www.bleepingcomputer.com/news/security/8base-ransomware-gang-escalates-double-extortion-attacks-in-june/){:target="_blank" rel="noopener"}

> ​A 8Base ransomware gang is targeting organizations worldwide in double-extortion attacks, with a steady stream of new victims since the beginning of June. [...]
