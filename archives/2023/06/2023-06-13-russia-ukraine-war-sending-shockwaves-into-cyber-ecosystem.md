Title: Russia-Ukraine war sending shockwaves into cyber-ecosystem
Date: 2023-06-13T08:31:25+00:00
Author: James Hayes
Category: The Register
Tags: 
Slug: 2023-06-13-russia-ukraine-war-sending-shockwaves-into-cyber-ecosystem

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/13/russiaukraine_war_sending_shockwaves_into/){:target="_blank" rel="noopener"}

> Conflict could be first shooting war to deploy armies of ‘citizen hackers’ that cause at-risk organisations to rethink their defensive strategies Sponsored Feature When military historians come to chronicle the first 15 months of the Russian invasion of Ukraine, they won't find any shortage of battlefront bulletins to inform their accounts.... [...]
