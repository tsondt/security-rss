Title: Expanding our Security AI ecosystem at Security Summit 2023
Date: 2023-06-13T16:01:00+00:00
Author: Sunil Potti
Category: GCP Security
Tags: Security & Identity
Slug: 2023-06-13-expanding-our-security-ai-ecosystem-at-security-summit-2023

[Source](https://cloud.google.com/blog/products/identity-security/expanding-our-security-ai-ecosystem-at-security-summit-2023/){:target="_blank" rel="noopener"}

> Organizations large and small are realizing that digital transformation requires a ground-up approach to modernize security. However, that digital transformation is being threatened by increasingly disruptive cyber risks and threats. At our annual Google Cloud Security Summit today, we’re sharing the latest insights into how the threat landscape is evolving and how innovations across our portfolio, including generative AI-driven capabilities, can help organizations around the world address their most pressing security challenges. We’ve recently announced the Google Cloud Security AI Workbench, an industry-first extensible platform powered by a specialized security large-language model (LLM), Sec-PaLM 2, as well as a partnership [...]
