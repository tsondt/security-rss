Title: EU boss Breton: there's no Huawei that Chinese comms kit is safe to use in Europe
Date: 2023-06-16T00:31:14+00:00
Author: Tobias Mann
Category: The Register
Tags: 
Slug: 2023-06-16-eu-boss-breton-theres-no-huawei-that-chinese-comms-kit-is-safe-to-use-in-europe

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/16/breton_calls_for_eu_huawei_bans/){:target="_blank" rel="noopener"}

> European Commission's own networks to toss Middle Kingdom boxes amid calls for total replacement European commissioner Thierry Breton wants Huawei and ZTE barred throughout the EU, and revealed plans to remove kit made by the Chinese telecom vendors from the Commission's internal networks.... [...]
