Title: Paragon Solutions Spyware: Graphite
Date: 2023-06-08T11:30:42+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Israel;national security policy;spyware
Slug: 2023-06-08-paragon-solutions-spyware-graphite

[Source](https://www.schneier.com/blog/archives/2023/06/paragon-solutions-spyware-graphite.html){:target="_blank" rel="noopener"}

> Paragon Solutions is yet another Israeli spyware company. Their product is called “Graphite,” and is a lot like NSO Group’s Pegasus. And Paragon is working with what seems to be US approval: American approval, even if indirect, has been at the heart of Paragon’s strategy. The company sought a list of allied nations that the US wouldn’t object to seeing deploy Graphite. People with knowledge of the matter suggested 35 countries are on that list, though the exact nations involved could not be determined. Most were in the EU and some in Asia, the people said. Remember when NSO Group [...]
