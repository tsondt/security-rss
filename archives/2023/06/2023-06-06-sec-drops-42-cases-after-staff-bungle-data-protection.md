Title: SEC drops 42 cases after staff bungle data protection
Date: 2023-06-06T04:02:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-06-sec-drops-42-cases-after-staff-bungle-data-protection

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/06/sec_dismissals_data_mishandling/){:target="_blank" rel="noopener"}

> Corporate watchdog fouled its info-separation regime, let the wrong people read sensitive docs The US Securities and Exchange Commission (SEC) has dismissed proceedings against 42 companies and individuals after admitting that its enforcement staff accessed documents that were supposed to be for judges' eyes only.... [...]
