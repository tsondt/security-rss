Title: Capita faces first legal Letter of Claim over mega breach
Date: 2023-06-16T13:04:59+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-06-16-capita-faces-first-legal-letter-of-claim-over-mega-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/16/capita_faces_first_legal_letter/){:target="_blank" rel="noopener"}

> Barings Law claims 250 people that 'suspect' data theft signed up to class action Capita is facing its first legal claim over the high profile digital burglary in late March that exposed some customer data to intruders and will cost the outsourcing biz around £20 million ($26 million) to clean up.... [...]
