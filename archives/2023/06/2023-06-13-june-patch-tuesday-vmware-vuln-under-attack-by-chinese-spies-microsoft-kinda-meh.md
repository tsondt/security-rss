Title: June Patch Tuesday: VMware vuln under attack by Chinese spies, Microsoft kinda meh
Date: 2023-06-13T20:32:39+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-13-june-patch-tuesday-vmware-vuln-under-attack-by-chinese-spies-microsoft-kinda-meh

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/13/june_patch_tuesday_vmware_vuln/){:target="_blank" rel="noopener"}

> Plus: Adobe, SAP and Android push updates Microsoft has released security updates for 78 flaws for June's Patch Tuesday, and luckily for admins, none of these are under exploit.... [...]
