Title: British Airways, Boots, BBC payroll data stolen in MOVEit supply-chain attack
Date: 2023-06-05T19:29:17+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-06-05-british-airways-boots-bbc-payroll-data-stolen-in-moveit-supply-chain-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/05/british_airways_boots_moveit/){:target="_blank" rel="noopener"}

> Microsoft blames Clop ransomware crew for theft of staff info British Airways, the BBC, and UK pharmacy chain Boots are among the companies whose data has been compromised after miscreants exploited a critical vulnerability in deployments of the MOVEit document-transfer app.... [...]
