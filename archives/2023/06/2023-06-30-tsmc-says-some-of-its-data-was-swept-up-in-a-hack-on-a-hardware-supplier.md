Title: TSMC says some of its data was swept up in a hack on a hardware supplier
Date: 2023-06-30T17:19:27+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;lockbit;ransomware;TSMC
Slug: 2023-06-30-tsmc-says-some-of-its-data-was-swept-up-in-a-hack-on-a-hardware-supplier

[Source](https://arstechnica.com/?p=1951336){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) Chipmaker TSMC said on Friday that one of its hardware suppliers experienced a “security incident” that allowed the attackers to obtain configurations and settings for some of the servers the company uses in its corporate network. The disclosure came a day after the LockBit ransomware crime syndicate listed TSMC on its extortion site and threatened to publish the data unless it received a payment of $70 million. The hardware supplier, Kinmax Technology, confirmed that one of its test environments had been attacked by an external group, which was then able to retrieve configuration files and [...]
