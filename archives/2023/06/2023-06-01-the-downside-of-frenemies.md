Title: The downside of frenemies
Date: 2023-06-01T16:43:10+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-06-01-the-downside-of-frenemies

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/01/the_downside_of_frenemies/){:target="_blank" rel="noopener"}

> Are DevOps Tools a potential risk to your software supply chain security? Webinar Popular DevOps tools are great when it comes to helping developers optimize digital infrastructure, but there's a potential downside – the hidden risks they can contain which may compromise your supply chain.... [...]
