Title: Ukraine war blurs lines between cyber-crims and state-sponsored attackers
Date: 2023-06-01T05:40:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-06-01-ukraine-war-blurs-lines-between-cyber-crims-and-state-sponsored-attackers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/01/ukraine_romcom_malware/){:target="_blank" rel="noopener"}

> This RomCom is no laughing matter A change in the deployment of the RomCom malware strain has illustrated the blurring distinction between cyberattacks motivated by money and those fueled by geopolitics, in this case Russia's illegal invasion of Ukraine, according to Trend Micro analysts.... [...]
