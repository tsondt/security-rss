Title: Life long cyber security learning
Date: 2023-06-30T09:01:59+00:00
Author: Rob Jaques
Category: The Register
Tags: 
Slug: 2023-06-30-life-long-cyber-security-learning

[Source](https://go.theregister.com/feed/www.theregister.com/2023/06/30/life_long_cyber_security_learning/){:target="_blank" rel="noopener"}

> SANS training courses are scheduled for multiple locations across the EMEA region this Autumn Sponsored Post Nobody here at is likely to argue with Albert Einstein's idea that "intellectual growth should commence at birth and cease only at death".... [...]
