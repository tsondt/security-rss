Title: How Confidential Computing can transform cloud security
Date: 2023-04-26T16:00:00+00:00
Author: Sam Lugani
Category: GCP Security
Tags: Security & Identity
Slug: 2023-04-26-how-confidential-computing-can-transform-cloud-security

[Source](https://cloud.google.com/blog/products/identity-security/rsa-confidential-computing-transforming-cloud-security/){:target="_blank" rel="noopener"}

> As one of the most trusted cloud platform providers, Google is committed to providing our clients secure and reliable environments for their workloads. Google believes the future of computing will increasingly shift to private, encrypted services where users can be confident that their data is not being exposed to cloud providers or their own insiders. Confidential Computing helps make this future possible by keeping data encrypted in memory, and elsewhere outside the CPU, while it is being processed. Since our first Confidential Computing offering in 2018, Google has been a pioneer in making the technology widely available through our cloud, [...]
