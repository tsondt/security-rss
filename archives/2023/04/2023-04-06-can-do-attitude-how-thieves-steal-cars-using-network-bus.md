Title: CAN do attitude: How thieves steal cars using network bus
Date: 2023-04-06T10:34:09+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2023-04-06-can-do-attitude-how-thieves-steal-cars-using-network-bus

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/06/can_injection_attack_car_theft/){:target="_blank" rel="noopener"}

> It starts with a headlamp and fake smart speaker, and ends in an injection attack and a vanished motor Automotive security experts say they have uncovered a method of car theft relying on direct access to the vehicle's system bus via a smart headlamp's wiring.... [...]
