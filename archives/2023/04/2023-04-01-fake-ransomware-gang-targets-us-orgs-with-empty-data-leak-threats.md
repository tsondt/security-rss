Title: Fake ransomware gang targets U.S. orgs with empty data leak threats
Date: 2023-04-01T11:59:04-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-04-01-fake-ransomware-gang-targets-us-orgs-with-empty-data-leak-threats

[Source](https://www.bleepingcomputer.com/news/security/fake-ransomware-gang-targets-us-orgs-with-empty-data-leak-threats/){:target="_blank" rel="noopener"}

> Fake extortionists are piggybacking on data breaches and ransomware incidents, threatening U.S. companies with publishing or selling allegedly stolen data unless they get paid. [...]
