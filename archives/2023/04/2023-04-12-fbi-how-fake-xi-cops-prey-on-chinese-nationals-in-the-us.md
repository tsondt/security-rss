Title: FBI: How fake Xi cops prey on Chinese nationals in the US
Date: 2023-04-12T23:26:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-04-12-fbi-how-fake-xi-cops-prey-on-chinese-nationals-in-the-us

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/12/crooks_prey_us_chinese/){:target="_blank" rel="noopener"}

> 你好 [insert name], 我在 Ministry of Public Security 工作 [insert shakedown] Criminals posing as law enforcement agents of the Chinese government are shaking down Chinese nationals living the United States by accusing them of financial crimes and threatening to arrest or hurt them if they don't pay, according to the FBI.... [...]
