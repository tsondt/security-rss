Title: Friday Squid Blogging: More Squid Camouflage Research
Date: 2023-04-28T21:07:37+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-04-28-friday-squid-blogging-more-squid-camouflage-research

[Source](https://www.schneier.com/blog/archives/2023/04/friday-squid-blogging-more-squid-camouflage-research.html){:target="_blank" rel="noopener"}

> Here’s a research group trying to replicate squid cell transparency in mammalian cells. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
