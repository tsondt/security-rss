Title: AWS Security Profile: Ryan Dsouza, Principal Solutions Architect
Date: 2023-04-14T20:11:53+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Thought Leadership;AWS Security Profile;AWS Security Profiles;Security Blog
Slug: 2023-04-14-aws-security-profile-ryan-dsouza-principal-solutions-architect

[Source](https://aws.amazon.com/blogs/security/aws-security-profile-ryan-dsouza-principal-solutions-architect/){:target="_blank" rel="noopener"}

> In the AWS Security Profile series, I interview some of the humans who work in Amazon Web Services Security and help keep our customers safe and secure. This interview is with Ryan Dsouza, Principal Solutions Architect for industrial internet of things (IIoT) security. How long have you been at AWS and what do you do [...]
