Title: KuCoin's Twitter account hacked to promote crypto scam
Date: 2023-04-24T12:17:17-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-04-24-kucoins-twitter-account-hacked-to-promote-crypto-scam

[Source](https://www.bleepingcomputer.com/news/security/kucoins-twitter-account-hacked-to-promote-crypto-scam/){:target="_blank" rel="noopener"}

> KuCoin's Twitter account was hacked, allowing attackers to promote a fake giveaway scam that led to the theft of over $22.6K in cryptocurrency. [...]
