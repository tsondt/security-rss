Title: Online Safety Bill age checks? We won't do 'em, says Wikipedia
Date: 2023-04-28T14:30:45+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-04-28-online-safety-bill-age-checks-we-wont-do-em-says-wikipedia

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/28/online_safety_bill_age_checks/){:target="_blank" rel="noopener"}

> World's encyclopedia warns draft law could boot it offline in UK Wikipedia won't be age-gating its services no matter what final form the UK's Online Safety Bill takes, two senior folks from nonprofit steward the Wikimedia Foundation said this morning.... [...]
