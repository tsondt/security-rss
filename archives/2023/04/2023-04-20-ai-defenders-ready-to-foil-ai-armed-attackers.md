Title: AI defenders ready to foil AI-armed attackers
Date: 2023-04-20T08:34:11+00:00
Author: James Hayes
Category: The Register
Tags: 
Slug: 2023-04-20-ai-defenders-ready-to-foil-ai-armed-attackers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/20/ai_defenders_ready_to_foil/){:target="_blank" rel="noopener"}

> Operational AI cybersecurity systems have been gaining valuable experience that will enable them to defend against AI-armed opponents. Sponsored Feature For some time now, alerts concerning the utilisation of AI by cybercriminals have been sounded in specialist and mainstream media alike – with the set-to between AI-armed attackers and AI-protected defenders envisaged in vivid gladiatorial terms.... [...]
