Title: The Week in Ransomware - April 14th 2023 - A Focus on Stolen Data
Date: 2023-04-14T18:35:57-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-04-14-the-week-in-ransomware-april-14th-2023-a-focus-on-stolen-data

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-april-14th-2023-a-focus-on-stolen-data/){:target="_blank" rel="noopener"}

> It has been mostly a quiet week regarding ransomware, with only a few bits of info released on older attacks and some reports released on existing organizations. [...]
