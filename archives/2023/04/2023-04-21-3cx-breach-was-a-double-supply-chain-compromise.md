Title: 3CX Breach Was a Double Supply Chain Compromise
Date: 2023-04-21T01:05:44+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Ne'er-Do-Well News;The Coming Storm;3CX;ClearSky Security;Diamond Sleet;double supply chain breach;Elastic Security;ESET;ICONICSTEALER;Kaspersky Lab;kim zetter;macOS;Mandiant;Marc-Etienne M.Leveille;microsoft;Peter Kalnai;supply chain;Trading Technologies;X_Trader;zero day;ZINC
Slug: 2023-04-21-3cx-breach-was-a-double-supply-chain-compromise

[Source](https://krebsonsecurity.com/2023/04/3cx-breach-was-a-double-supply-chain-compromise/){:target="_blank" rel="noopener"}

> We learned some remarkable new details this week about the recent supply-chain attack on VoIP software provider 3CX. The lengthy, complex intrusion has all the makings of a cyberpunk spy novel: North Korean hackers using legions of fake executive accounts on LinkedIn to lure people into opening malware disguised as a job offer; malware targeting Mac and Linux users working at defense and cryptocurrency firms; and software supply-chain attacks nested within earlier supply chain attacks. Researchers at ESET say this job offer from a phony HSBC recruiter on LinkedIn was North Korean malware masquerading as a PDF file. In late [...]
