Title: Hackers swap stealth for realistic checkout forms to steal credit cards
Date: 2023-04-28T12:41:52-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-28-hackers-swap-stealth-for-realistic-checkout-forms-to-steal-credit-cards

[Source](https://www.bleepingcomputer.com/news/security/hackers-swap-stealth-for-realistic-checkout-forms-to-steal-credit-cards/){:target="_blank" rel="noopener"}

> Hackers are hijacking online stores to display modern, realistic-looking fake payment forms to steal credit cards from unsuspecting customers. [...]
