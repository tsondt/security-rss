Title: Kubernetes RBAC abused to create persistent cluster backdoors
Date: 2023-04-21T11:35:11-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud
Slug: 2023-04-21-kubernetes-rbac-abused-to-create-persistent-cluster-backdoors

[Source](https://www.bleepingcomputer.com/news/security/kubernetes-rbac-abused-to-create-persistent-cluster-backdoors/){:target="_blank" rel="noopener"}

> Hackers use a novel method involving RBAC (Role-Based Access Control) to create persistent backdoor accounts on Kubernetes clusters and hijack their resources for Monero crypto-mining. [...]
