Title: Capita cyberattack disrupted access to its Microsoft Office 365 apps
Date: 2023-04-03T09:20:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-03-capita-cyberattack-disrupted-access-to-its-microsoft-office-365-apps

[Source](https://www.bleepingcomputer.com/news/security/capita-cyberattack-disrupted-access-to-its-microsoft-office-365-apps/){:target="_blank" rel="noopener"}

> British outsourcing services provider Capita announced today that a cyberattack on Friday prevented access to its internal Microsoft Office 365 applications. [...]
