Title: VMware fixes vRealize bug that let attackers run code as root
Date: 2023-04-20T13:22:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-04-20-vmware-fixes-vrealize-bug-that-let-attackers-run-code-as-root

[Source](https://www.bleepingcomputer.com/news/security/vmware-fixes-vrealize-bug-that-let-attackers-run-code-as-root/){:target="_blank" rel="noopener"}

> VMware addressed a critical vRealize Log Insight security vulnerability that allows remote attackers to gain remote execution on vulnerable appliances. [...]
