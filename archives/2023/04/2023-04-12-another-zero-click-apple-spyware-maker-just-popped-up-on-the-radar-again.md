Title: Another zero-click Apple spyware maker just popped up on the radar again
Date: 2023-04-12T00:42:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-04-12-another-zero-click-apple-spyware-maker-just-popped-up-on-the-radar-again

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/12/quadream_spyware_microsoft_citizenlab/){:target="_blank" rel="noopener"}

> Pegasus, pssh, you so 2000-and-late Malware reportedly developed by a little-known Israeli commercial spyware maker has been found on devices of journalists, politicians, and an NGO worker in multiple countries, say researchers.... [...]
