Title: Menaced by miscreants, critical infrastructure needs a good ETHOS. Ah, here's one
Date: 2023-04-25T20:10:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-04-25-menaced-by-miscreants-critical-infrastructure-needs-a-good-ethos-ah-heres-one

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/25/ot_ethos_critical_infrastructure/){:target="_blank" rel="noopener"}

> OT firms construct handy early-warning info-sharing system RSA Conference A group of some of the largest operational technology companies are using this year's RSA Conference as an opportunity to launch an open source early-threat-warning system designed for OT and industrial control systems (ICS) environments.... [...]
