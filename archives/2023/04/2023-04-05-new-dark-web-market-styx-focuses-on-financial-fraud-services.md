Title: New dark web market STYX focuses on financial fraud services
Date: 2023-04-05T17:29:55-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-05-new-dark-web-market-styx-focuses-on-financial-fraud-services

[Source](https://www.bleepingcomputer.com/news/security/new-dark-web-market-styx-focuses-on-financial-fraud-services/){:target="_blank" rel="noopener"}

> A new dark web marketplace called STYX launched earlier this year and appears to be on its way to becoming a thriving hub for buying and selling illegal services or stolen data. [...]
