Title: As the west tries to limit TikTok’s reach, what about China’s other apps?
Date: 2023-04-12T04:00:03+00:00
Author: Amy Hawkins and Helen Davidson
Category: The Guardian
Tags: TikTok;China;Technology;Data and computer security;US Congress;Apps;World news;Asia Pacific;US news;US politics
Slug: 2023-04-12-as-the-west-tries-to-limit-tiktoks-reach-what-about-chinas-other-apps

[Source](https://www.theguardian.com/technology/2023/apr/12/tiktok-china-apps-national-security-wechat-shein){:target="_blank" rel="noopener"}

> With government concerns over national security growing, Beijing’s influence over platforms such as WeChat and Shein could come under scrutiny As TikTok, the world’s most popular app, comes under increasing scrutiny in response to data privacy and security concerns, lawmakers in the west may soon set their sights on other Chinese platforms that have gone global. TikTok was built by ByteDance as a foreign version of its popular domestic video-sharing platform, Douyin. But it is far from being ByteDance’s only overseas moneymaker. The Chinese company owns dozens of apps that are available overseas, many of them English-language versions of Chinese [...]
