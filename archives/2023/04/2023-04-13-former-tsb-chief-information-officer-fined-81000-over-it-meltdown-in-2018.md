Title: Former TSB chief information officer fined £81,000 over IT meltdown in 2018
Date: 2023-04-13T13:50:27+00:00
Author: Kalyeena Makortoff Banking correspondent
Category: The Guardian
Tags: TSB;Banking;Financial sector;Prudential Regulation Authority (PRA);Regulators;Business;UK news;Data and computer security
Slug: 2023-04-13-former-tsb-chief-information-officer-fined-81000-over-it-meltdown-in-2018

[Source](https://www.theguardian.com/business/2023/apr/13/former-tsb-bank-chief-information-officer-fined-2018){:target="_blank" rel="noopener"}

> Regulator says Carlos Abarca ‘failed to take reasonable steps’ to ensure outsourcing firm was ready to migrate accounts en masse UK regulators have imposed an £81,000 fine on a former TSB information officer over the bank’s IT meltdown in 2018 that left millions of customers locked out of their accounts. The Prudential Regulation Authority (PRA) said Carlos Abarca, who was TSB’s chief information officer at the time of the meltdown, “failed to take reasonable steps” to ensure that an outsourcing firm owned by TSB’s parent company was ready to carry out the IT migration of customers en masse. Continue reading... [...]
