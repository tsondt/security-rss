Title: Hackers use Rilide browser extension to bypass 2FA, steal crypto
Date: 2023-04-06T15:02:16-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-04-06-hackers-use-rilide-browser-extension-to-bypass-2fa-steal-crypto

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-rilide-browser-extension-to-bypass-2fa-steal-crypto/){:target="_blank" rel="noopener"}

> A new malware strain called Rilide has been targeting Chromium-based web browsers like Google Chrome, Brave, Opera, and Microsoft Edge, to monitor user browsing history, snap screenshots, and inject scripts that can steal cryptocurrency. [...]
