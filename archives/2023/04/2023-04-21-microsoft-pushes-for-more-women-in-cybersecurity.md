Title: Microsoft pushes for more women in cybersecurity
Date: 2023-04-21T22:03:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-21-microsoft-pushes-for-more-women-in-cybersecurity

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/21/microsoft_women_cybersecurity/){:target="_blank" rel="noopener"}

> Redmond tops industry average, still got a way to go Microsoft has partnered with organizations around the globe to bring more women into infosec roles, though the devil is in the details.... [...]
