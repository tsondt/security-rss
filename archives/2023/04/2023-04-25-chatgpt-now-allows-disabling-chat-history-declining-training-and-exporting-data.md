Title: ChatGPT now allows disabling chat history, declining training, and exporting data
Date: 2023-04-25T21:01:17+00:00
Author: Benj Edwards
Category: Ars Technica
Tags: Biz & IT;AI;AI privacy;ChatGPT;ChatGPT Plus;large language models;machine learning;openai;privacy
Slug: 2023-04-25-chatgpt-now-allows-disabling-chat-history-declining-training-and-exporting-data

[Source](https://arstechnica.com/?p=1934271){:target="_blank" rel="noopener"}

> Enlarge (credit: OpenAI / Stable Diffusion) On Tuesday, OpenAI announced new controls for ChatGPT users that allow them to turn off chat history, simultaneously opting out of providing that conversation history as data for training AI models. Also, users can now export chat history for local storage. The new controls, which rolled out to all ChatGPT users today, can be found in ChatGPT settings. Conversations that begin with the chat history disabled won't be used to train and improve the ChatGPT model, nor will they appear in the history sidebar. OpenAI will retain the conversations internally for 30 days and [...]
