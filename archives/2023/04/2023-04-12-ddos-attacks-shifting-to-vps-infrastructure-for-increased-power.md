Title: DDoS attacks shifting to VPS infrastructure for increased power
Date: 2023-04-12T15:40:27-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-12-ddos-attacks-shifting-to-vps-infrastructure-for-increased-power

[Source](https://www.bleepingcomputer.com/news/security/ddos-attacks-shifting-to-vps-infrastructure-for-increased-power/){:target="_blank" rel="noopener"}

> Hyper-volumetric DDoS (distributed denial of service) attacks in the first quarter of 2023 have shifted from relying on compromised IoT devices to leveraging breached Virtual Private Servers (VPS). [...]
