Title: Windows 11 is getting a new 'Presence sensing' privacy setting
Date: 2023-04-13T14:19:53-04:00
Author: Mayank Parmar
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-04-13-windows-11-is-getting-a-new-presence-sensing-privacy-setting

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-11-is-getting-a-new-presence-sensing-privacy-setting/){:target="_blank" rel="noopener"}

> Windows 11 is getting a new privacy setting that allows users to control whether applications can detect when actively interacting with the device. [...]
