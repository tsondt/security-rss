Title: AWS Security Profile: Matt Luttrell, Principal Solutions Architect for AWS Identity
Date: 2023-04-12T19:23:43+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Thought Leadership;AWS Security Profile;Security Blog
Slug: 2023-04-12-aws-security-profile-matt-luttrell-principal-solutions-architect-for-aws-identity

[Source](https://aws.amazon.com/blogs/security/aws-security-profile-matt-luttrell-principal-solutions-architect-for-aws-identity/){:target="_blank" rel="noopener"}

> In the AWS Security Profile series, I interview some of the humans who work in Amazon Web Services Security and help keep our customers safe and secure. In this profile, I interviewed Matt Luttrell, Principal Solutions Architect for AWS Identity. How long have you been at AWS and what do you do in your current role? [...]
