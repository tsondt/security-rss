Title: Military helicopter crash blamed on failure to apply software patch
Date: 2023-04-18T03:30:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-04-18-military-helicopter-crash-blamed-on-failure-to-apply-software-patch

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/18/helicopter_crash_missing_software_patch/){:target="_blank" rel="noopener"}

> A rather nice beach in Australia now has a rather unusual and hopefully temporary feature An Australian military helicopter crash was reportedly caused by failure to apply a software patch, with a hefty side serving of pilot error.... [...]
