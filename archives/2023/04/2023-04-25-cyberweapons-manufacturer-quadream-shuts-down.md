Title: Cyberweapons Manufacturer QuaDream Shuts Down
Date: 2023-04-25T10:09:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;human rights;spyware
Slug: 2023-04-25-cyberweapons-manufacturer-quadream-shuts-down

[Source](https://www.schneier.com/blog/archives/2023/04/cyberweapons-manufacturer-quadream-shuts-down.html){:target="_blank" rel="noopener"}

> Following a report on its activities, the Israeli spyware company QuaDream has shut down. This was QuadDream: Key Findings Based on an analysis of samples shared with us by Microsoft Threat Intelligence, we developed indicators that enabled us to identify at least five civil society victims of QuaDream’s spyware and exploits in North America, Central Asia, Southeast Asia, Europe, and the Middle East. Victims include journalists, political opposition figures, and an NGO worker. We are not naming the victims at this time. We also identify traces of a suspected iOS 14 zero-click exploit used to deploy QuaDream’s spyware. The exploit [...]
