Title: Introducing AI-powered investigation in Chronicle Security Operations
Date: 2023-04-24T16:00:00+00:00
Author: Spencer Lichtenstein
Category: GCP Security
Tags: AI & Machine Learning;Security & Identity
Slug: 2023-04-24-introducing-ai-powered-investigation-in-chronicle-security-operations

[Source](https://cloud.google.com/blog/products/identity-security/rsa-introducing-ai-powered-investigation-chronicle-security-operations/){:target="_blank" rel="noopener"}

> Leveraging recent advances in artificial intelligence (AI) and large language models (LLM), Google Cloud Security AI Workbench sets out to address three of the biggest challenges in cybersecurity: threat overload, toilsome tools, and the talent gap. These could not be more relevant for security operations. At the RSA Conference 2023 in San Francisco today, we’re excited to announce AI-powered capabilities in Chronicle Security Operations that can transform threat detection, investigation and response for cyber defenders, simplifying complex data analysis and security engineering, and elevating the effectiveness of each defender. Thoughtful application of generative AI We know how difficult it is [...]
