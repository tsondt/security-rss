Title: Microsoft probes complaints of Edge leaking URLs to Bing
Date: 2023-04-26T21:08:37+00:00
Author: Tobias Mann
Category: The Register
Tags: 
Slug: 2023-04-26-microsoft-probes-complaints-of-edge-leaking-urls-to-bing

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/26/microsoft_edge_url/){:target="_blank" rel="noopener"}

> Remember next time Redmond begs you not to install another browser You might want to think twice before typing anything into Microsoft's Edge browser, as an apparent bug in a recent release of Redmond's Chromium clone appears to be funneling URLs you visit back to the Bing API.... [...]
