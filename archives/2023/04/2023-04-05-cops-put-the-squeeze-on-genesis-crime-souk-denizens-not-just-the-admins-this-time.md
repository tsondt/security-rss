Title: Cops put the squeeze on Genesis crime souk denizens, not just the admins this time
Date: 2023-04-05T21:45:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-05-cops-put-the-squeeze-on-genesis-crime-souk-denizens-not-just-the-admins-this-time

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/05/genesis_market_takedown/){:target="_blank" rel="noopener"}

> Feds managed to image entire backend server with full details The FBI today released additional information about its takedown of the Genesis Market, a major online shop for stolen account access credentials, revealing that they'd pwned the marketplace for at least two years.... [...]
