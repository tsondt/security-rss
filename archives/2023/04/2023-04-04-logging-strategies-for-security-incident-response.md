Title: Logging strategies for security incident response
Date: 2023-04-04T17:09:13+00:00
Author: Anna McAbee
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;AWS Incident Response;AWS security;Cloud security;Incident response;Logging;Monitoring;Security;Security Blog
Slug: 2023-04-04-logging-strategies-for-security-incident-response

[Source](https://aws.amazon.com/blogs/security/logging-strategies-for-security-incident-response/){:target="_blank" rel="noopener"}

> Effective security incident response depends on adequate logging, as described in the AWS Security Incident Response Guide. If you have the proper logs and the ability to query them, you can respond more rapidly and effectively to security events. If a security event occurs, you can use various log sources to validate what occurred and [...]
