Title: While Twitter wants to sell its verification, Microsoft will do it for free on LinkedIn
Date: 2023-04-14T10:14:11+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-04-14-while-twitter-wants-to-sell-its-verification-microsoft-will-do-it-for-free-on-linkedin

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/14/microsoft_linkedin_id_verify/){:target="_blank" rel="noopener"}

> Redmond expands a digital ID process for its platform as Musk seeks cash for blue check marks As Elon Musk tears at Twitter's credibility by demanding businesses and individuals pay for their blue verification checks, Microsoft is pushing ts own free digital ID technology to companies and their employees on LinkedIn.... [...]
