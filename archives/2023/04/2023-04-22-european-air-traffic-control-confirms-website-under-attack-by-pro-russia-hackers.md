Title: European air traffic control confirms website 'under attack' by pro-Russia hackers
Date: 2023-04-22T07:09:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-22-european-air-traffic-control-confirms-website-under-attack-by-pro-russia-hackers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/22/eurocontrol_russia_attack/){:target="_blank" rel="noopener"}

> Another cyber nuisance in support of Putin's war, nothing too serious Europe's air-traffic agency appears to be the latest target in pro-Russian miscreants' attempts to disrupt air travel.... [...]
