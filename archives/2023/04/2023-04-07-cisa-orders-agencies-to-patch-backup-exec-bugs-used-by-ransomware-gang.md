Title: CISA orders agencies to patch Backup Exec bugs used by ransomware gang
Date: 2023-04-07T17:07:52-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-04-07-cisa-orders-agencies-to-patch-backup-exec-bugs-used-by-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/cisa-orders-agencies-to-patch-backup-exec-bugs-used-by-ransomware-gang/){:target="_blank" rel="noopener"}

> On Friday, U.S. Cybersecurity and Infrastructure Security Agency (CISA) increased by five its list of security issues that threat actors have used in attacks, three of them in Veritas Backup Exec exploited to deploy ransomware. [...]
