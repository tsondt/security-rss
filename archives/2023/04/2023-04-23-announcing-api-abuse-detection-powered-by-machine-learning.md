Title: Announcing API abuse detection powered by machine learning
Date: 2023-04-23T16:00:00+00:00
Author: Shelly Hershkovitz
Category: GCP Security
Tags: Security & Identity
Slug: 2023-04-23-announcing-api-abuse-detection-powered-by-machine-learning

[Source](https://cloud.google.com/blog/products/identity-security/rsa-announcing-api-abuse-detection-machine-learning/){:target="_blank" rel="noopener"}

> API security incidents are increasingly common and disruptive. With the growth of API traffic, enterprises across the world are also experiencing an uptick in malicious API attacks, making API security a heightened priority. According to our latest API Security Research Report, 50% of organizations surveyed have experienced an API security incident in the past 12 months and of those, 77% delayed the rollout of a new service or application. At the RSA Conference 2023 today, we’re making it faster and easier to help detect API abuse incidents with the introduction of Advanced API Security Machine Learning powered abuse-detection dashboards. Our [...]
