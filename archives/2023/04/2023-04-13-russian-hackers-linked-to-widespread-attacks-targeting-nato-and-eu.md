Title: Russian hackers linked to widespread attacks targeting NATO and EU
Date: 2023-04-13T10:27:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-04-13-russian-hackers-linked-to-widespread-attacks-targeting-nato-and-eu

[Source](https://www.bleepingcomputer.com/news/security/russian-hackers-linked-to-widespread-attacks-targeting-nato-and-eu/){:target="_blank" rel="noopener"}

> Poland's Military Counterintelligence Service and its Computer Emergency Response Team have linked APT29 state-sponsored hackers, part of the Russian government's Foreign Intelligence Service (SVR), to widespread attacks targeting NATO and European Union countries. [...]
