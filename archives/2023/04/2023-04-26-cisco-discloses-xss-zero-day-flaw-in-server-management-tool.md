Title: Cisco discloses XSS zero-day flaw in server management tool
Date: 2023-04-26T14:51:50-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-04-26-cisco-discloses-xss-zero-day-flaw-in-server-management-tool

[Source](https://www.bleepingcomputer.com/news/security/cisco-discloses-xss-zero-day-flaw-in-server-management-tool/){:target="_blank" rel="noopener"}

> Cisco disclosed today a zero-day vulnerability in the company's Prime Collaboration Deployment (PCD) software that can be exploited for cross-site scripting attacks. [...]
