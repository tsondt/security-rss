Title: LLMs and Phishing
Date: 2023-04-10T11:23:02+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;ChatGPT;essays;machine learning;phishing;scams
Slug: 2023-04-10-llms-and-phishing

[Source](https://www.schneier.com/blog/archives/2023/04/llms-and-phishing.html){:target="_blank" rel="noopener"}

> Here’s an experiment being run by undergraduate computer science students everywhere: Ask ChatGPT to generate phishing emails, and test whether these are better at persuading victims to respond or click on the link than the usual spam. It’s an interesting experiment, and the results are likely to vary wildly based on the details of the experiment. But while it’s an easy experiment to run, it misses the real risk of large language models (LLMs) writing scam emails. Today’s human-run scams aren’t limited by the number of people who respond to the initial email contact. They’re limited by the labor-intensive process [...]
