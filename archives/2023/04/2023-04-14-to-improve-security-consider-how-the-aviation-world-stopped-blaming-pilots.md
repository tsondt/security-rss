Title: To improve security, consider how the aviation world stopped blaming pilots
Date: 2023-04-14T04:29:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-04-14-to-improve-security-consider-how-the-aviation-world-stopped-blaming-pilots

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/14/aviations_just_culture_improves_cybersecurity/){:target="_blank" rel="noopener"}

> When admitting to an error isn't seen as a failure, improvement easy to achieve, says pilot-turned-CISO To improve security, the cybersecurity industry needs to follow the aviation industry's shift from a blame culture to a "just" culture, according to ISACA director Serge Christiaans.... [...]
