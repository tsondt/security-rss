Title: Cloud CISO Perspectives: Late April 2023
Date: 2023-04-28T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Security & Identity
Slug: 2023-04-28-cloud-ciso-perspectives-late-april-2023

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-late-april-2023/){:target="_blank" rel="noopener"}

> Welcome to the second Cloud CISO Perspectives for April 2023. This is our first month moving to a twice-monthly cadence, with a guest column today from my friend and the CEO of Mandiant, Kevin Mandia. Mandiant, the company Kevin founded in 2004, was acquired by Google last year. At the RSA Conference 2023 in San Francisco this week, Mandiant and Google Cloud had a unified presence at cybersecurity's largest event for the first time. We’re excited to bring our joint capabilities, products, and expertise together to help our customers better defend their organizations against today’s rapidly-changing threat landscape. In his [...]
