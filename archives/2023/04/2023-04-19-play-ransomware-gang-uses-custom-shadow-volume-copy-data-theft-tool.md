Title: Play ransomware gang uses custom Shadow Volume Copy data-theft tool
Date: 2023-04-19T06:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-19-play-ransomware-gang-uses-custom-shadow-volume-copy-data-theft-tool

[Source](https://www.bleepingcomputer.com/news/security/play-ransomware-gang-uses-custom-shadow-volume-copy-data-theft-tool/){:target="_blank" rel="noopener"}

> The Play ransomware group has developed two custom tools in.NET, namely Grixba and VSS Copying Tool, which it uses to improve the effectiveness of its cyberattacks. [...]
