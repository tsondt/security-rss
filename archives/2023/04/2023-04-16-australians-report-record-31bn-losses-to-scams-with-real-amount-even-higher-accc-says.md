Title: Australians report record $3.1bn losses to scams, with real amount even higher, ACCC says
Date: 2023-04-16T20:00:19+00:00
Author: Cait Kelly
Category: The Guardian
Tags: Personal finance;Australian Competition and Consumer Commission (ACCC);Australia news;Cybercrime;Australian economy;Data and computer security;Crime - Australia
Slug: 2023-04-16-australians-report-record-31bn-losses-to-scams-with-real-amount-even-higher-accc-says

[Source](https://www.theguardian.com/australia-news/2023/apr/17/australians-report-record-31bn-losses-to-scams-with-real-amount-even-higher-accc-says){:target="_blank" rel="noopener"}

> Investment fraud amounts for biggest share at $1.5bn, followed by remote access and payment redirection rorts Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast Australians lost a record amount of more than $3.1bn to scams in 2022, up from the $2bn lost in 2021, a new report from the Australian Competition and Consumer Commission has revealed. The Targeting Scams report, which compiles data from Scamwatch, ReportCyber, major banks and money remitters, was based on an analysis of more than 500,000 reports. Sign up for Guardian [...]
