Title: Azure admins warned to disable shared key access as backdoor attack detailed
Date: 2023-04-11T13:00:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-11-azure-admins-warned-to-disable-shared-key-access-as-backdoor-attack-detailed

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/11/orca_azure_access_keys/){:target="_blank" rel="noopener"}

> The default is that sharing is caring as Redmond admits: 'These permissions could be abused' A design flaw in Microsoft Azure – that shared key authorization is enabled by default when creating storage accounts – could give attackers full access to your environment, according to Orca Security researchers.... [...]
