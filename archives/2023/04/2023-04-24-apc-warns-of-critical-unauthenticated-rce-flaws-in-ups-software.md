Title: APC warns of critical unauthenticated RCE flaws in UPS software
Date: 2023-04-24T11:14:52-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-24-apc-warns-of-critical-unauthenticated-rce-flaws-in-ups-software

[Source](https://www.bleepingcomputer.com/news/security/apc-warns-of-critical-unauthenticated-rce-flaws-in-ups-software/){:target="_blank" rel="noopener"}

> APC's Easy UPS Online Monitoring Software is vulnerable to unauthenticated arbitrary remote code execution, allowing hackers to take over devices and, in a worst-case scenario, disabling its functionality altogether. [...]
