Title: A Serial Tech Investment Scammer Takes Up Coding?
Date: 2023-04-03T16:13:42+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Ne'er-Do-Well News;Alan John Mykhailov;Blackstone Corporate Alliance Ltd;CodesToYou;Colette Davies;David Bruno;DomainTools.com;Drydennow.com;Igor Gubskyi;Igor Hubskyi;Iryna Davies;John Bernard;John Clifton Davies;Maria Yakovleva;Mariya Kulikova;MySolve;SafeSwiss Secure Communication AG;Secure Swiss Data;The Inside Knowledge GmbH
Slug: 2023-04-03-a-serial-tech-investment-scammer-takes-up-coding

[Source](https://krebsonsecurity.com/2023/04/a-serial-tech-investment-scammer-takes-up-coding/){:target="_blank" rel="noopener"}

> John Clifton Davies, a 60-year-old con man from the United Kingdom who fled the country in 2015 before being sentenced to 12 years in prison for fraud, has enjoyed a successful life abroad swindling technology startups by pretending to be a billionaire investor. Davies’ newest invention appears to be “ CodesToYou,” which purports to be a “full cycle software development company” based in the U.K. The scam artist John Bernard a.k.a. Alan John Mykailov (left) in a recent Zoom call, and a mugshot of John Clifton Davies from nearly a decade earlier. Several articles here have delved into the history [...]
