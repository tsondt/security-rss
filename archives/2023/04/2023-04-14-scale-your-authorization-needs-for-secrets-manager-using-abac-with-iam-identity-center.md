Title: Scale your authorization needs for Secrets Manager using ABAC with IAM Identity Center
Date: 2023-04-14T18:47:56+00:00
Author: Aravind Gopaluni
Category: AWS Security
Tags: AWS IAM Identity Center;AWS Secrets Manager;Best Practices;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Cloud security;Identity providers;Security;Security Blog
Slug: 2023-04-14-scale-your-authorization-needs-for-secrets-manager-using-abac-with-iam-identity-center

[Source](https://aws.amazon.com/blogs/security/scale-your-authorization-needs-for-secrets-manager-using-abac-with-iam-identity-center/){:target="_blank" rel="noopener"}

> With AWS Secrets Manager, you can securely store, manage, retrieve, and rotate the secrets required for your applications and services running on AWS. A secret can be a password, API key, OAuth token, or other type of credential used for authentication purposes. You can control access to secrets in Secrets Manager by using AWS Identity [...]
