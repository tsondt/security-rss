Title: Hyundai data breach exposes owner details in France and Italy
Date: 2023-04-12T10:55:52-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-12-hyundai-data-breach-exposes-owner-details-in-france-and-italy

[Source](https://www.bleepingcomputer.com/news/security/hyundai-data-breach-exposes-owner-details-in-france-and-italy/){:target="_blank" rel="noopener"}

> Hyundai has disclosed a data breach impacting Italian and French car owners and those who booked a test drive, warning that hackers gained access to personal data. [...]
