Title: Hackers use fake ‘Windows Update’ guides to target Ukrainian govt
Date: 2023-04-30T10:07:17-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government
Slug: 2023-04-30-hackers-use-fake-windows-update-guides-to-target-ukrainian-govt

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-fake-windows-update-guides-to-target-ukrainian-govt/){:target="_blank" rel="noopener"}

> The Computer Emergency Response Team of Ukraine (CERT-UA) says Russian hackers are targeting various government bodies in the country with malicious emails supposedly containing instructions on how to update Windows as a defense against cyber attacks. [...]
