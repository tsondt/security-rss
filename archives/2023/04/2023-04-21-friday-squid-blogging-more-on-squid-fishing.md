Title: Friday Squid Blogging: More on Squid Fishing
Date: 2023-04-21T21:04:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-04-21-friday-squid-blogging-more-on-squid-fishing

[Source](https://www.schneier.com/blog/archives/2023/04/friday-squid-blogging-more-on-squid-fishing.html){:target="_blank" rel="noopener"}

> The squid you eat most likely comes from unregulated waters. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
