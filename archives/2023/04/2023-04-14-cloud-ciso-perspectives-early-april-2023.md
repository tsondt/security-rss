Title: Cloud CISO Perspectives: Early April 2023
Date: 2023-04-14T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2023-04-14-cloud-ciso-perspectives-early-april-2023

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-early-april-2023/){:target="_blank" rel="noopener"}

> Welcome to the first Cloud CISO Perspectives for April 2023. This is our first month moving to a twice-monthly cadence, with a guest column at the end of April from my friend and colleague, Kevin Mandia. Today, I’d like to talk about the newest report from the Google Cybersecurity Action Team (GCAT), the first edition of our Perspectives on Security for the Board – a vital topic for cybersecurity and cyber risk. Before we get to the report, I’d like to encourage everyone to consider joining Google Cloud and Mandiant together for the first time at the RSA Conference in [...]
