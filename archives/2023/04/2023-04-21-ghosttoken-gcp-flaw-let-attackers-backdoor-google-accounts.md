Title: GhostToken GCP flaw let attackers backdoor Google accounts
Date: 2023-04-21T13:50:28-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-04-21-ghosttoken-gcp-flaw-let-attackers-backdoor-google-accounts

[Source](https://www.bleepingcomputer.com/news/security/ghosttoken-gcp-flaw-let-attackers-backdoor-google-accounts/){:target="_blank" rel="noopener"}

> Google has addressed a Cloud Platform (GCP) security vulnerability impacting all users and allowing attackers to backdoor their accounts using malicious OAuth applications installed from the Google Marketplace or third-party providers. [...]
