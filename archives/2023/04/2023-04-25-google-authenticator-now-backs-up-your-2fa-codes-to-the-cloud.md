Title: Google Authenticator now backs up your 2FA codes to the cloud
Date: 2023-04-25T10:39:17-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Google;Security
Slug: 2023-04-25-google-authenticator-now-backs-up-your-2fa-codes-to-the-cloud

[Source](https://www.bleepingcomputer.com/news/google/google-authenticator-now-backs-up-your-2fa-codes-to-the-cloud/){:target="_blank" rel="noopener"}

> The Google Authenticator app has received a critical update for Android and iOS that allows users to back up their two-factor authentication one-time passwords (OTPs) to their Google Accounts and have multi-device support. [...]
