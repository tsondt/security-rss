Title: Biometric Authentication Isn't Bulletproof —Here's How to Secure It
Date: 2023-04-05T10:05:15-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-04-05-biometric-authentication-isnt-bulletproof-heres-how-to-secure-it

[Source](https://www.bleepingcomputer.com/news/security/biometric-authentication-isnt-bulletproof-heres-how-to-secure-it/){:target="_blank" rel="noopener"}

> Biometric authentication is often thought of as nearly impossible to steal or fake. Not only are there ways around biometric authentication, but not all biometric methods are created equal. [...]
