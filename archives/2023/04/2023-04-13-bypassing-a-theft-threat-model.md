Title: Bypassing a Theft Threat Model
Date: 2023-04-13T11:22:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;alarms;Apple;iPhone;theft
Slug: 2023-04-13-bypassing-a-theft-threat-model

[Source](https://www.schneier.com/blog/archives/2023/04/bypassing-a-theft-threat-model.html){:target="_blank" rel="noopener"}

> Thieves cut through the wall of a coffee shop to get to an Apple store, bypassing the alarms in the process. I wrote about this kind of thing in 2000, in Secrets and Lies (page 318): My favorite example is a band of California art thieves that would break into people’s houses by cutting a hole in their walls with a chainsaw. The attacker completely bypassed the threat model of the defender. The countermeasures that the homeowner put in place were door and window alarms; they didn’t make a difference to this attack. The article says they took half a [...]
