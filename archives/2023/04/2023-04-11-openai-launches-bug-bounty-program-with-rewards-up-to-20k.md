Title: OpenAI launches bug bounty program with rewards up to $20K
Date: 2023-04-11T16:32:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-04-11-openai-launches-bug-bounty-program-with-rewards-up-to-20k

[Source](https://www.bleepingcomputer.com/news/security/openai-launches-bug-bounty-program-with-rewards-up-to-20k/){:target="_blank" rel="noopener"}

> AI research company OpenAI announced today the launch of a new bug bounty program to allow registered security researchers to discover vulnerabilities in its product line and get paid for reporting them via the Bugcrowd crowdsourced security platform. [...]
