Title: I Hate IAM: but I need it desperately
Date: 2023-04-17T15:58:00+00:00
Author: Max Saltonstall
Category: GCP Security
Tags: Developers & Practitioners;Security & Identity
Slug: 2023-04-17-i-hate-iam-but-i-need-it-desperately

[Source](https://cloud.google.com/blog/products/identity-security/i-hate-iam-i-need-it-desperately/){:target="_blank" rel="noopener"}

> While technology organizations seem to be in agreement regarding the necessity of well-structured Identity and Access Management (IAM) for securing their cloud deployments, they’re often ill-prepared for the level of effort required. Putting your house in order is hard: think of IAM as the blueprint for building a solid cloud house. You need a good structure, with room for all the necessary and desired elements, or the house will be unstable. Management of identities and their related access to resources and applications is foundational for minimizing risk in your cloud environment. The problem is how challenging identity and access control [...]
