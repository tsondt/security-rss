Title: The Attacks that can Target your Windows Active Directory
Date: 2023-04-18T10:07:14-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-04-18-the-attacks-that-can-target-your-windows-active-directory

[Source](https://www.bleepingcomputer.com/news/security/the-attacks-that-can-target-your-windows-active-directory/){:target="_blank" rel="noopener"}

> Hackers commonly target Active Directory with various attack techniques spanning many attack vectors. Let's consider a few of these attacks and what organizations can do to protect themselves. [...]
