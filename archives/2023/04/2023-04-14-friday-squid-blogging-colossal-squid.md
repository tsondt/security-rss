Title: Friday Squid Blogging: Colossal Squid
Date: 2023-04-14T21:14:26+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-04-14-friday-squid-blogging-colossal-squid

[Source](https://www.schneier.com/blog/archives/2023/04/friday-squid-blogging-colossal-squid.html){:target="_blank" rel="noopener"}

> Interesting article on the colossal squid, which is larger than the giant squid. The article answers a vexing question: So why do we always hear about the giant squid and not the colossal squid? Well, part of it has to do with the fact that the giant squid was discovered and studied long before the colossal squid. Scientists have been studying giant squid since the 1800s, while the colossal squid wasn’t even discovered until 1925. And its first discovery was just the head and arms found in a sperm whale’s stomach. It wasn’t until 1981 that the first whole animal [...]
