Title: New Rorschach ransomware is the fastest encryptor seen so far
Date: 2023-04-04T10:13:46-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-04-new-rorschach-ransomware-is-the-fastest-encryptor-seen-so-far

[Source](https://www.bleepingcomputer.com/news/security/new-rorschach-ransomware-is-the-fastest-encryptor-seen-so-far/){:target="_blank" rel="noopener"}

> Following a cyberattack on a U.S.-based company, malware researchers discovered what appears to be a new ransomware strain with "technically unique features," which they named Rorschach. [...]
