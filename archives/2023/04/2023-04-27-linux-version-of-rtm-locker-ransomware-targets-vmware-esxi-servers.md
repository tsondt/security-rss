Title: Linux version of RTM Locker ransomware targets VMware ESXi servers
Date: 2023-04-27T12:20:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Linux
Slug: 2023-04-27-linux-version-of-rtm-locker-ransomware-targets-vmware-esxi-servers

[Source](https://www.bleepingcomputer.com/news/security/linux-version-of-rtm-locker-ransomware-targets-vmware-esxi-servers/){:target="_blank" rel="noopener"}

> RTM Locker is the latest enterprise-targeting ransomware operation found to be deploying a Linux encryptor that targets virtual machines on VMware ESXi servers. [...]
