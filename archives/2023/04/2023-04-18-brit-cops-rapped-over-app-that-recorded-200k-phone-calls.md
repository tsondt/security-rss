Title: Brit cops rapped over app that recorded 200k phone calls
Date: 2023-04-18T13:38:08+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-04-18-brit-cops-rapped-over-app-that-recorded-200k-phone-calls

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/18/ico_surrey_sussex_police/){:target="_blank" rel="noopener"}

> Officers didn't know software was saving personal data and neither did people on other end Several police forces in Britain are being put on the naughty step by the UK's data watchdog for using a calling app that recorded hundreds of thousands of phone conversations and illegally retained that data.... [...]
