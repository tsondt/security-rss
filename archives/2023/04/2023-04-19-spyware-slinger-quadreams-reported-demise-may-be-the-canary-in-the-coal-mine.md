Title: Spyware slinger QuaDream’s reported demise may be the canary in the coal mine
Date: 2023-04-19T20:20:11+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-04-19-spyware-slinger-quadreams-reported-demise-may-be-the-canary-in-the-coal-mine

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/19/quadream_nso_spyware/){:target="_blank" rel="noopener"}

> NSO and others are still out there, but pariahs find it hard to do business Analysis Israeli spyware shop QuaDream is reportedly shutting down due to financial troubles.... [...]
