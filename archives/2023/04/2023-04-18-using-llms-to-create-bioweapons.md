Title: Using LLMs to Create Bioweapons
Date: 2023-04-18T11:19:03+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence;biological warfare;machine learning
Slug: 2023-04-18-using-llms-to-create-bioweapons

[Source](https://www.schneier.com/blog/archives/2023/04/using-llms-to-create-bioweapons.html){:target="_blank" rel="noopener"}

> I’m not sure there are good ways to build guardrails to prevent this sort of thing : There is growing concern regarding the potential misuse of molecular machine learning models for harmful purposes. Specifically, the dual-use application of models for predicting cytotoxicity18 to create new poisons or employing AlphaFold2 to develop novel bioweapons has raised alarm. Central to these concerns are the possible misuse of large language models and automated experimentation for dual-use purposes or otherwise. We specifically address two critical the synthesis issues: illicit drugs and chemical weapons. To evaluate these risks, we designed a test set comprising compounds [...]
