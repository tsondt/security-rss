Title: Can ChatGPT bash together some data-stealing code? With the right prompts, sure
Date: 2023-04-04T22:00:05+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-04-can-chatgpt-bash-together-some-data-stealing-code-with-the-right-prompts-sure

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/04/chatgpt_exfiltration_tool/){:target="_blank" rel="noopener"}

> But nothing a keen beginner couldn't do, anyway A Forcepoint staffer has blogged about how he used ChatGPT to craft some code that exfiltrates data from an infected machine. At first, it sounds bad, but in reality, it's nothing an intermediate or keen beginner programmer couldn't whack together themselves anyway.... [...]
