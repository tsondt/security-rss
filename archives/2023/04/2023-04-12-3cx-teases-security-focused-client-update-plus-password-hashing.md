Title: 3CX teases security-focused client update, plus password hashing
Date: 2023-04-12T04:35:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-04-12-3cx-teases-security-focused-client-update-plus-password-hashing

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/12/3cx_client_update_for_security/){:target="_blank" rel="noopener"}

> As Mandiant finds more evidence it was North Korea wot done it The CEO of VoIP software provider 3CX has teased the imminent release of a security-focused upgrade to the company’s progressive web application client.... [...]
