Title: LockBit crew cooks up half-baked Mac ransomware
Date: 2023-04-17T21:30:15+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-17-lockbit-crew-cooks-up-half-baked-mac-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/17/lockbit_ransomware_mac_devices/){:target="_blank" rel="noopener"}

> Please, no need to fix these problems LockBit has developed ransomware that can encrypt files on Arm-powered Macs, said to be a first for the prolific cybercrime crew.... [...]
