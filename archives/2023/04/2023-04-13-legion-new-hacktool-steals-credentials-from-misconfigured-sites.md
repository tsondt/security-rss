Title: Legion: New hacktool steals credentials from misconfigured sites
Date: 2023-04-13T06:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-13-legion-new-hacktool-steals-credentials-from-misconfigured-sites

[Source](https://www.bleepingcomputer.com/news/security/legion-new-hacktool-steals-credentials-from-misconfigured-sites/){:target="_blank" rel="noopener"}

> A new Python-based credential harvester and SMTP hijacking tool named 'Legion' is being sold on Telegram, allowing cybercriminals to automate attacks against online email services. [...]
