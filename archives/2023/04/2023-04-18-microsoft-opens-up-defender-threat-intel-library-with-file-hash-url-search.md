Title: Microsoft opens up Defender threat intel library with file hash, URL search
Date: 2023-04-18T19:30:10+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-04-18-microsoft-opens-up-defender-threat-intel-library-with-file-hash-url-search

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/18/microsoft_threat_intelligence_search/){:target="_blank" rel="noopener"}

> Surprised there's no ChatGPT angle and that it's not called MalwareTotal Security researchers and analysts can now search Microsoft's Threat Intelligence Defender database using file hashes and URLs when pulling together information for network intrusion investigations and whatnot.... [...]
