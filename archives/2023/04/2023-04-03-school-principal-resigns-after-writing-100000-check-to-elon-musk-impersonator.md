Title: School principal resigns after writing $100,000 check to Elon Musk impersonator
Date: 2023-04-03T01:58:13+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-04-03-school-principal-resigns-after-writing-100000-check-to-elon-musk-impersonator

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/03/infosec_in_brief/){:target="_blank" rel="noopener"}

> ALSO: DJI forgets the 'B' in 'BCC,' and this week's critical known exploits In Brief The principal of a Florida science and technology charter school has resigned after allegedly writing a $100,000 check to an Elon Musk impersonator using school funds.... [...]
