Title: Russia-pushed UN Cybercrime Treaty may rewrite global law. It's ... not great
Date: 2023-04-14T23:46:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-04-14-russia-pushed-un-cybercrime-treaty-may-rewrite-global-law-its-not-great

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/14/un_cybercrime_treaty/){:target="_blank" rel="noopener"}

> Let's go through all the proposed problematic powers, starting with surveillance and censorship Special report United Nations negotiators convened this week in Vienna, Austria, to formulate a draft cybercrime treaty, and civil society groups are worried.... [...]
