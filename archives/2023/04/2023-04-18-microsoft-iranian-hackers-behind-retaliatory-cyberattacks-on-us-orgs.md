Title: Microsoft: Iranian hackers behind retaliatory cyberattacks on US orgs
Date: 2023-04-18T16:03:42-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-04-18-microsoft-iranian-hackers-behind-retaliatory-cyberattacks-on-us-orgs

[Source](https://www.bleepingcomputer.com/news/security/microsoft-iranian-hackers-behind-retaliatory-cyberattacks-on-us-orgs/){:target="_blank" rel="noopener"}

> Microsoft has discovered that an Iranian hacking group known as 'Mint Sandstorm' is conducting cyberattacks on US critical infrastructure in what is believed to be retaliation for recent attacks on Iran's infrastructure. [...]
