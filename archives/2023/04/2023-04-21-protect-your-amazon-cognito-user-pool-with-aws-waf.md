Title: Protect your Amazon Cognito user pool with AWS WAF
Date: 2023-04-21T16:14:57+00:00
Author: Maitreya Ranganath
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Cognito;Edge;Security;Security Blog;WAF
Slug: 2023-04-21-protect-your-amazon-cognito-user-pool-with-aws-waf

[Source](https://aws.amazon.com/blogs/security/protect-your-amazon-cognito-user-pool-with-aws-waf/){:target="_blank" rel="noopener"}

> Many of our customers use Amazon Cognito user pools to add authentication, authorization, and user management capabilities to their web and mobile applications. You can enable the built-in advanced security in Amazon Cognito to detect and block the use of credentials that have been compromised elsewhere, and to detect unusual sign-in activity and then prompt users for [...]
