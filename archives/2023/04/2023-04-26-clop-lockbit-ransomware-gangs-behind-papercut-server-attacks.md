Title: Clop, LockBit ransomware gangs behind PaperCut server attacks
Date: 2023-04-26T19:28:17-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-04-26-clop-lockbit-ransomware-gangs-behind-papercut-server-attacks

[Source](https://www.bleepingcomputer.com/news/security/clop-lockbit-ransomware-gangs-behind-papercut-server-attacks/){:target="_blank" rel="noopener"}

> ​Microsoft has attributed recent attacks on PaperCut servers to the Clop and LockBit ransomware operations, which used the vulnerabilities to steal corporate data. [...]
