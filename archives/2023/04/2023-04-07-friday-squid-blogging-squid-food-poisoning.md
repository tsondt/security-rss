Title: Friday Squid Blogging: Squid Food Poisoning
Date: 2023-04-07T21:04:56+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-04-07-friday-squid-blogging-squid-food-poisoning

[Source](https://www.schneier.com/blog/archives/2023/04/friday-squid-blogging-squid-food-poisoning.html){:target="_blank" rel="noopener"}

> University of Connecticut basketball player Jordan Hawkins claims to have suffered food poisoning from calamari the night before his NCAA finals game. The restaurant disagrees : On Sunday, a Mastro’s employee politely cast doubt on the idea that the restaurant might have caused the illness, citing its intense safety protocols. The staffer, who spoke on condition of anonymity because he was not authorized to officially speak for Mastro’s, said restaurants in general were more likely to arouse suspicion when they had some rooting interest against the customer-athletes. As usual, you can also use this squid post to talk about the [...]
