Title: Microsoft Defender update causes Windows Hardware Stack Protection mess
Date: 2023-04-19T17:57:18-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-04-19-microsoft-defender-update-causes-windows-hardware-stack-protection-mess

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-update-causes-windows-hardware-stack-protection-mess/){:target="_blank" rel="noopener"}

> In a confusing mess, a recent Microsoft Defender update rolled out a new security feature called 'Kernel-mode Hardware-enforced Stack Protection,' while removing the LSA protection feature. Unfortunately, Microsoft has not provided any documentation on this change, leading to more questions than answers. [...]
