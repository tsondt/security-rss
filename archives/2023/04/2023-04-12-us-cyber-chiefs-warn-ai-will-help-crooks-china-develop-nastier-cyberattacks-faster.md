Title: US cyber chiefs warn AI will help crooks, China develop nastier cyberattacks faster
Date: 2023-04-12T01:50:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-12-us-cyber-chiefs-warn-ai-will-help-crooks-china-develop-nastier-cyberattacks-faster

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/12/us_chatgpt_threat/){:target="_blank" rel="noopener"}

> It's not all doom and gloom because ML also amplifies defensive efforts, probably Bots like ChatGPT may not be able to pull off the next big Microsoft server worm or Colonial Pipeline ransomware super-infection but they may help criminal gangs and nation-state hackers develop some attacks against IT, according to Rob Joyce, director of the NSA's Cybersecurity Directorate.... [...]
