Title: Kodi discloses data breach after forum database for sale online
Date: 2023-04-11T12:31:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-11-kodi-discloses-data-breach-after-forum-database-for-sale-online

[Source](https://www.bleepingcomputer.com/news/security/kodi-discloses-data-breach-after-forum-database-for-sale-online/){:target="_blank" rel="noopener"}

> The Kodi Foundation has disclosed a data breach after hackers stole the organization's MyBB forum database containing user data and private messages and attempted to sell it online. [...]
