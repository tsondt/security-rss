Title: Protect the Industrial Control Systems (ICS)
Date: 2023-04-20T08:08:11+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2023-04-20-protect-the-industrial-control-systems-ics

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/20/protect_the_industrial_control_systems/){:target="_blank" rel="noopener"}

> ICS security is fast becoming a frontline defense against hackers intent on causing mayhem Sponsored Post Some of the most famous cyber attacks in history have been directed against Industrial Control Systems (ICS).... [...]
