Title: Windows admins warned to patch critical MSMQ QueueJumper bug
Date: 2023-04-12T13:31:26-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-04-12-windows-admins-warned-to-patch-critical-msmq-queuejumper-bug

[Source](https://www.bleepingcomputer.com/news/security/windows-admins-warned-to-patch-critical-msmq-queuejumper-bug/){:target="_blank" rel="noopener"}

> Security researchers and experts warn of a critical vulnerability in the Windows Message Queuing (MSMQ) middleware service patched by Microsoft during this month's Patch Tuesday and exposing hundreds of thousands of systems to attacks. [...]
