Title: A '!password20231#' password may not be as complex as you think
Date: 2023-04-27T10:04:08-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-04-27-a-password20231-password-may-not-be-as-complex-as-you-think

[Source](https://www.bleepingcomputer.com/news/security/a-password20231-password-may-not-be-as-complex-as-you-think/){:target="_blank" rel="noopener"}

> In some ways, past best practices for password policies may have made password cracking easier. Let's examine the most common types of password attacks, and how to defend against them. [...]
