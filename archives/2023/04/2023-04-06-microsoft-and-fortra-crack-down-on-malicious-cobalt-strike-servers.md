Title: Microsoft and Fortra crack down on malicious Cobalt Strike servers
Date: 2023-04-06T13:04:55-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-04-06-microsoft-and-fortra-crack-down-on-malicious-cobalt-strike-servers

[Source](https://www.bleepingcomputer.com/news/security/microsoft-and-fortra-crack-down-on-malicious-cobalt-strike-servers/){:target="_blank" rel="noopener"}

> Microsoft, Fortra, and the Health Information Sharing and Analysis Center (Health-ISAC) have announced a broad legal crackdown against servers hosting cracked copies of Cobalt Strike, one of the primary hacking tools used by cybercriminals. [...]
