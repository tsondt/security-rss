Title: Attackers use abandoned WordPress plugin to backdoor websites
Date: 2023-04-20T16:02:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-20-attackers-use-abandoned-wordpress-plugin-to-backdoor-websites

[Source](https://www.bleepingcomputer.com/news/security/attackers-use-abandoned-wordpress-plugin-to-backdoor-websites/){:target="_blank" rel="noopener"}

> Attackers are using Eval PHP, an outdated legitimate WordPress plugin, to compromise websites by injecting stealthy backdoors. [...]
