Title: Upcoming Speaking Engagements
Date: 2023-04-14T20:04:59+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2023-04-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2023/04/upcoming-speaking-engagements-29.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking on “Cybersecurity Thinking to Reinvent Democracy” at RSA Conference 2023 in San Francisco, California, on Tuesday, April 25, 2023, at 9:40 AM PT. I’m speaking at IT-S Now 2023 in Vienna, Austria, on June 2, 2023 at 8:30 AM CEST. The list is maintained on this page. [...]
