Title: Microsoft delays Exchange Online CARs deprecation until 2024
Date: 2023-04-08T10:05:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-04-08-microsoft-delays-exchange-online-cars-deprecation-until-2024

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-delays-exchange-online-cars-deprecation-until-2024/){:target="_blank" rel="noopener"}

> Microsoft announced today that Client Access Rules (CARs) deprecation in Exchange Online will be delayed by one year until September 2024. [...]
