Title: Spain's most dangerous and elusive hacker now in police custody
Date: 2023-04-05T03:34:45-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-05-spains-most-dangerous-and-elusive-hacker-now-in-police-custody

[Source](https://www.bleepingcomputer.com/news/security/spains-most-dangerous-and-elusive-hacker-now-in-police-custody/){:target="_blank" rel="noopener"}

> The police in Spain have arrested José Luis Huertas (aka "Alcaseca", "Mango", "chimichuri"), a 19-year-old regarded as the most dangerous hackers in the country. [...]
