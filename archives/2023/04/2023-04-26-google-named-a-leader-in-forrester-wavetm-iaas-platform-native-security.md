Title: Google named a Leader in Forrester Wave™ IaaS Platform Native Security
Date: 2023-04-26T16:00:00+00:00
Author: Sunil Potti
Category: GCP Security
Tags: Security & Identity
Slug: 2023-04-26-google-named-a-leader-in-forrester-wavetm-iaas-platform-native-security

[Source](https://cloud.google.com/blog/products/identity-security/google-named-leader-in-forrester-wave-iaas-platform-native-security/){:target="_blank" rel="noopener"}

> As organizations migrate their applications and data to the cloud, cloud-first security products that are tightly integrated with cloud platforms make it easy for customers to start secure and stay secure. When done well, the use of these capabilities can help simplify security so it becomes almost invisible to users. Cloud services can reduce operational complexity, shift the balance of shared responsibility to a more mature, shared fate relationship between the customer and cloud provider, and decrease the need for highly-specialized security talent. Invisible security innovation leveraging Google’s core strengths At Google Cloud, we call this outcome invisible security, and [...]
