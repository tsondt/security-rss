Title: Microsoft tells admins to autoreview your Autopatch alerts or autolose the service
Date: 2023-04-05T11:15:10+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-04-05-microsoft-tells-admins-to-autoreview-your-autopatch-alerts-or-autolose-the-service

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/05/microsoft_autopatch_update/){:target="_blank" rel="noopener"}

> And you wouldn't want that... would you? Microsoft is updating a service introduced last year that shifts the responsibility of patching Windows devices from IT admins to the vendor itself.... [...]
