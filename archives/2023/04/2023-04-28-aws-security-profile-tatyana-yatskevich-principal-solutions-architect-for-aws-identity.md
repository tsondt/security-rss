Title: AWS Security Profile: Tatyana Yatskevich, Principal Solutions Architect for AWS Identity
Date: 2023-04-28T00:35:58+00:00
Author: Maddie Bacon
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Thought Leadership;AWS Security Profile;AWS Security Profiles;Security Blog
Slug: 2023-04-28-aws-security-profile-tatyana-yatskevich-principal-solutions-architect-for-aws-identity

[Source](https://aws.amazon.com/blogs/security/aws-security-profile-tatyana-yatskevich-principal-solutions-architect-for-aws-identity/){:target="_blank" rel="noopener"}

> In the AWS Security Profile series, I interview some of the humans who work in AWS Security and help keep our customers safe and secure. In this profile, I interviewed Tatyana Yatskevich, Principal Solutions Architect for AWS Identity. How long have you been at AWS and what do you do in your current role? I’ve been [...]
