Title: Google will add End-to-End encryption to Google Authenticator
Date: 2023-04-26T17:11:30-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Google;Security
Slug: 2023-04-26-google-will-add-end-to-end-encryption-to-google-authenticator

[Source](https://www.bleepingcomputer.com/news/google/google-will-add-end-to-end-encryption-to-google-authenticator/){:target="_blank" rel="noopener"}

> Google is bringing end-to-end encryption to Google Authenticator cloud backups after researchers warned users against synchronizing 2FA codes with their Google accounts. [...]
