Title: Security Risks of AI
Date: 2023-04-27T13:38:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;cybersecurity;machine learning;reports;risks
Slug: 2023-04-27-security-risks-of-ai

[Source](https://www.schneier.com/blog/archives/2023/04/security-risks-of-ai.html){:target="_blank" rel="noopener"}

> Stanford and Georgetown have a new report on the security risks of AI—particularly adversarial machine learning—based on a workshop they held on the topic. Jim Dempsey, one of the workshop organizers, wrote a blog post on the report: As a first step, our report recommends the inclusion of AI security concerns within the cybersecurity programs of developers and users. The understanding of how to secure AI systems, we concluded, lags far behind their widespread adoption. Many AI products are deployed without institutions fully understanding the security risks they pose. Organizations building or deploying AI models should incorporate AI concerns into [...]
