Title: Take control of your supply chain with Artifact Registry remote and virtual repositories
Date: 2023-04-17T17:07:00+00:00
Author: Michele Chubirka
Category: GCP Security
Tags: Security & Identity;Application Modernization;DevOps & SRE;Developers & Practitioners;Application Development
Slug: 2023-04-17-take-control-of-your-supply-chain-with-artifact-registry-remote-and-virtual-repositories

[Source](https://cloud.google.com/blog/products/identity-security/take-control-your-supply-chain-artifact-registry/){:target="_blank" rel="noopener"}

> Dev : "I need that library's functionality for the new feature!" Sec : "I can't approve it if I don't know that it's safe to deploy!" Dev : "And when will we know?" Sec : "My queue is 11 weeks long...." The most contentious conversations between security and development teams often involve the topic of open source. Developers just want to get the job done and that often means downloading and using open source packages or containers from public repositories. But security professionals despair over the lack of visibility into what third-party material is being used in their enterprise and [...]
