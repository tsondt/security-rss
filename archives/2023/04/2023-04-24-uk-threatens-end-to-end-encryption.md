Title: UK Threatens End-to-End Encryption
Date: 2023-04-24T10:39:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;encryption;laws;privacy;Signal;UK
Slug: 2023-04-24-uk-threatens-end-to-end-encryption

[Source](https://www.schneier.com/blog/archives/2023/04/uk-threatens-end-to-end-encryption.html){:target="_blank" rel="noopener"}

> In an open letter, seven secure messaging apps—including Signal and WhatsApp—point out that the UK’s Online Safety Bill could destroy end-to-end encryption: As currently drafted, the Bill could break end-to-end encryption,opening the door to routine, general and indiscriminate surveillance of personal messages of friends, family members, employees, executives, journalists, human rights activists and even politicians themselves, which would fundamentally undermine everyone’s ability to communicate securely. The Bill provides no explicit protection for encryption, and if implemented as written, could empower OFCOM to try to force the proactive scanning of private messages on end-to-end encrypted communication services – nullifying the purpose [...]
