Title: An earlier supply chain attack led to the 3CX supply chain attack, Mandiant says
Date: 2023-04-20T12:00:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-20-an-earlier-supply-chain-attack-led-to-the-3cx-supply-chain-attack-mandiant-says

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/20/3cx_supply_chain_attack/){:target="_blank" rel="noopener"}

> Threat hunters traced it back to malware-laced Trading Technologies' software The supply-chain attack against 3CX last month was caused by an earlier supply-chain compromise of a different software firm — Trading Technologies — according to Mandiant, whose consulting crew was hired by 3CX to help the VoIP biz investigate the intrusion.... [...]
