Title: Understanding Nasuni’s new ransomware protection service on Google Cloud
Date: 2023-04-17T16:00:00+00:00
Author: Bobby Silva
Category: GCP Security
Tags: Security & Identity;Partners;Storage & Data Transfer
Slug: 2023-04-17-understanding-nasunis-new-ransomware-protection-service-on-google-cloud

[Source](https://cloud.google.com/blog/products/storage-data-transfer/new-ransomware-protection-service-for-nasuni-on-google-cloud/){:target="_blank" rel="noopener"}

> Open ecosystems and collaborative partnerships matter, particularly with cloud infrastructure solutions. Google Cloud maintains a robust partner ecosystem that encourages companies like file-data-services provider Nasuni to create innovative solutions that run on its infrastructure. Partners can leverage what Google Cloud has built and advanced over many years to offer new, exciting capabilities for its customers. Nasuni's new Ransomware Protection add-on service to its Nasuni for Google Cloud offering is an example. At its core, the Nasuni File Data Platform can help organizations shift capacity off local file storage hardware and into cost-effective object storage. The technology caches files locally for [...]
