Title: How insecure is America's FirstNet emergency response system? No one's sure
Date: 2023-04-12T23:58:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-12-how-insecure-is-americas-firstnet-emergency-response-system-no-ones-sure

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/12/firstnet_cybersecurity_audit_wyden/){:target="_blank" rel="noopener"}

> Senator Wyden warns full probe needed AT&T is "concealing vital cybersecurity reporting" about its FirstNet phone network for first responders and the US military, according to US Senator Ron Wyden (D-OR), who said the network had been dubbed unsafe by CISA.... [...]
