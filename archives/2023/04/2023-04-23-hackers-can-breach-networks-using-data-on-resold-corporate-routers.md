Title: Hackers can breach networks using data on resold corporate routers
Date: 2023-04-23T12:32:57-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-04-23-hackers-can-breach-networks-using-data-on-resold-corporate-routers

[Source](https://www.bleepingcomputer.com/news/security/hackers-can-breach-networks-using-data-on-resold-corporate-routers/){:target="_blank" rel="noopener"}

> Enterprise-level network equipment on the secondary market hide sensitive data that hackers could use to breach corporate environments or to obtain customer information. [...]
