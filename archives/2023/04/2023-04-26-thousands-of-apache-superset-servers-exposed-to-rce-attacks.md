Title: Thousands of Apache Superset servers exposed to RCE attacks
Date: 2023-04-26T11:52:53-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-26-thousands-of-apache-superset-servers-exposed-to-rce-attacks

[Source](https://www.bleepingcomputer.com/news/security/thousands-of-apache-superset-servers-exposed-to-rce-attacks/){:target="_blank" rel="noopener"}

> Apache Superset is vulnerable to authentication bypass and remote code execution at default configurations, allowing attackers to potentially access and modify data, harvest credentials, and execute commands. [...]
