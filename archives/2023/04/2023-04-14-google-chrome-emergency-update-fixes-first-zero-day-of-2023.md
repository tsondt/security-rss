Title: Google Chrome emergency update fixes first zero-day of 2023
Date: 2023-04-14T14:00:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2023-04-14-google-chrome-emergency-update-fixes-first-zero-day-of-2023

[Source](https://www.bleepingcomputer.com/news/security/google-chrome-emergency-update-fixes-first-zero-day-of-2023/){:target="_blank" rel="noopener"}

> Google has released an emergency Chrome security update to address the first zero-day vulnerability exploited in attacks since the start of the year. [...]
