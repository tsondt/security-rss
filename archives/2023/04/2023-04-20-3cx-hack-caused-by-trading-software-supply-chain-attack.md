Title: 3CX hack caused by trading software supply chain attack
Date: 2023-04-20T08:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-04-20-3cx-hack-caused-by-trading-software-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/3cx-hack-caused-by-trading-software-supply-chain-attack/){:target="_blank" rel="noopener"}

> An investigation into last month's 3CX supply chain attack discovered that it was caused by another supply chain compromise where suspected North Korean attackers breached the site of stock trading automation company Trading Technologies to push trojanized software builds. [...]
