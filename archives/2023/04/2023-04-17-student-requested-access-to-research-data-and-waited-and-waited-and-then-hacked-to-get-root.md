Title: Student requested access to research data. And waited. And waited. And then hacked to get root
Date: 2023-04-17T07:29:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-04-17-student-requested-access-to-research-data-and-waited-and-waited-and-then-hacked-to-get-root

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/17/who_me/){:target="_blank" rel="noopener"}

> The punishment – Windows 98 administration chores – was far worse than the crime Who, Me? Welcome once more to Who Me? The Register ’s confessional column in which readers admit to being the source of SNAFUs.... [...]
