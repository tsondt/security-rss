Title: Hacking Pickleball
Date: 2023-04-21T18:11:58+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cheating;hacking;sports
Slug: 2023-04-21-hacking-pickleball

[Source](https://www.schneier.com/blog/archives/2023/04/hacking-pickleball.html){:target="_blank" rel="noopener"}

> My latest book, A Hacker’s Mind, has a lot of sports stories. Sports are filled with hacks, as players look for every possible advantage that doesn’t explicitly break the rules. Here’s an example from pickleball, which nicely explains the dilemma between hacking as a subversion and hacking as innovation: Some might consider these actions cheating, while the acting player would argue that there was no rule that said the action couldn’t be performed. So, how do we address these situations, and close those loopholes? We make new rules that specifically address the loophole action. And the rules book gets longer, [...]
