Title: Breached shutdown sparks migration to ARES data leak forums
Date: 2023-04-08T12:17:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-08-breached-shutdown-sparks-migration-to-ares-data-leak-forums

[Source](https://www.bleepingcomputer.com/news/security/breached-shutdown-sparks-migration-to-ares-data-leak-forums/){:target="_blank" rel="noopener"}

> A threat group called ARES is gaining notoriety on the cybercrime scene by selling and leaking databases stolen from corporations and public authorities. [...]
