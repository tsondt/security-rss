Title: That 3CX supply chain attack keeps getting worse: Other vendors hit
Date: 2023-04-24T03:27:05+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-04-24-that-3cx-supply-chain-attack-keeps-getting-worse-other-vendors-hit

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/24/in_brief_security/){:target="_blank" rel="noopener"}

> Also, Finland sentences CEO of breach company to prison (kind of), and this week's laundry list of critical vulns In Brief We thought it was probably the case when the news came out, but now it's been confirmed: The X_Trader supply chain attack behind the 3CX compromise last month wasn't confined to the telco developer.... [...]
