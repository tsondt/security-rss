Title: New Global AWS Data Processing Addendum
Date: 2023-04-11T19:04:14+00:00
Author: Chad Woolf
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS Compliance;Data Processing Agreement;Data protection;EU Data Protection;GDPR;General Data Protection Regulation;Security Blog;Terms of Service
Slug: 2023-04-11-new-global-aws-data-processing-addendum

[Source](https://aws.amazon.com/blogs/security/new-global-aws-data-processing-addendum/){:target="_blank" rel="noopener"}

> Navigating data protection laws around the world is no simple task. Today, I’m pleased to announce that AWS is expanding the scope of the AWS Data Processing Addendum (Global AWS DPA) so that it applies globally whenever customers use AWS services to process personal data, regardless of which data protection laws apply to that processing. [...]
