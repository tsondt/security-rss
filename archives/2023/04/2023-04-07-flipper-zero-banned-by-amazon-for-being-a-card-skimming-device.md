Title: Flipper Zero banned by Amazon for being a ‘card skimming device’
Date: 2023-04-07T05:01:02-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Technology;Security
Slug: 2023-04-07-flipper-zero-banned-by-amazon-for-being-a-card-skimming-device

[Source](https://www.bleepingcomputer.com/news/technology/flipper-zero-banned-by-amazon-for-being-a-card-skimming-device-/){:target="_blank" rel="noopener"}

> Amazon has banned the sale of the Flipper Zero portable multi-tool for pen-testers as it no longer allows its sale on the platform after tagging it as a card-skimming device. [...]
