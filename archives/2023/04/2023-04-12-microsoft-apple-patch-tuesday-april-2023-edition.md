Title: Microsoft (& Apple) Patch Tuesday, April 2023 Edition
Date: 2023-04-12T00:06:51+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Security Tools;The Coming Storm;Time to Patch;Bharat Jogi;CVE-2022-37969;CVE-2023-28219;CVE-2023-28220;CVE-2023-28252;DBAPPSecurity;Dustin Childs;iOS 15.5.7;iOS/iPadOS 16.4.1;macOS 12.6.5 and 11.7.6.;Mandiant;Nokoyawa ransomware;Qualys;Trend Micro Zero Day Initiative;Windows Common Log System File System
Slug: 2023-04-12-microsoft-apple-patch-tuesday-april-2023-edition

[Source](https://krebsonsecurity.com/2023/04/microsoft-apple-patch-tuesday-april-2023-edition/){:target="_blank" rel="noopener"}

> Microsoft today released software updates to plug 100 security holes in its Windows operating systems and other software, including a zero-day vulnerability that is already being used in active attacks. Not to be outdone, Apple has released a set of important updates addressing two zero-day vulnerabilities that are being used to attack iPhones, iPads and Macs. On April 7, Apple issued emergency security updates to fix two weaknesses that are being actively exploited, including CVE-2023-28206, which can be exploited by apps to seize control over a device. CVE-2023-28205 can be used by a malicious or hacked website to install code. [...]
