Title: How much to infect Android phones via Google Play store? How about $20k
Date: 2023-04-10T23:01:17+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-10-how-much-to-infect-android-phones-via-google-play-store-how-about-20k

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/10/kaspersky_google_play_malicious_apps/){:target="_blank" rel="noopener"}

> Or whatever you managed to haggle with these miscreants If you want to sneak malware onto people's Android devices via the official Google Play store, it may cost you about $20,000 to do so, Kaspersky suggests.... [...]
