Title: Microsoft shares guidance to detect BlackLotus UEFI bootkit attacks
Date: 2023-04-12T12:39:59-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-04-12-microsoft-shares-guidance-to-detect-blacklotus-uefi-bootkit-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-shares-guidance-to-detect-blacklotus-uefi-bootkit-attacks/){:target="_blank" rel="noopener"}

> Microsoft has shared guidance to help organizations check if hackers targeted or compromised machines with the BlackLotus UEFI bootkit by exploiting the CVE-2022-21894 vulnerability. [...]
