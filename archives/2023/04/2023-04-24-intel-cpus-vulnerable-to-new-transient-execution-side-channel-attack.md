Title: Intel CPUs vulnerable to new transient execution side-channel attack
Date: 2023-04-24T15:38:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2023-04-24-intel-cpus-vulnerable-to-new-transient-execution-side-channel-attack

[Source](https://www.bleepingcomputer.com/news/security/intel-cpus-vulnerable-to-new-transient-execution-side-channel-attack/){:target="_blank" rel="noopener"}

> A new side-channel attack impacting multiple generations of Intel CPUs has been discovered, allowing data to be leaked through the EFLAGS register. [...]
