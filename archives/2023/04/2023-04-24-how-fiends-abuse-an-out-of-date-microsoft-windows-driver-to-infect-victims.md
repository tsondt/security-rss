Title: How fiends abuse an out-of-date Microsoft Windows driver to infect victims
Date: 2023-04-24T11:30:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-04-24-how-fiends-abuse-an-out-of-date-microsoft-windows-driver-to-infect-victims

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/24/microsoft_windows_driver_aukill_ransomware/){:target="_blank" rel="noopener"}

> It's like those TV movies where a spy cuts a wire and the whole building's security goes out Ransomware spreaders have built a handy tool that abuses an out-of-date Microsoft Windows driver to disable security defenses before dropping malware into the targeted systems.... [...]
