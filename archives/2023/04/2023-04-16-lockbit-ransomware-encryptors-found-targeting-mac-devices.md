Title: LockBit ransomware encryptors found targeting Mac devices
Date: 2023-04-16T13:31:54-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Apple
Slug: 2023-04-16-lockbit-ransomware-encryptors-found-targeting-mac-devices

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-encryptors-found-targeting-mac-devices/){:target="_blank" rel="noopener"}

> The LockBit ransomware gang has created encryptors targeting Macs for the first time, likely becoming the first major ransomware operation to ever specifically target macOS. [...]
