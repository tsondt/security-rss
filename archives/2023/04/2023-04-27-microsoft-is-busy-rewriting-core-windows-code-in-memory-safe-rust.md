Title: Microsoft is busy rewriting core Windows code in memory-safe Rust
Date: 2023-04-27T20:45:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-04-27-microsoft-is-busy-rewriting-core-windows-code-in-memory-safe-rust

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/27/microsoft_windows_rust/){:target="_blank" rel="noopener"}

> Now that's a C change we can back Microsoft is rewriting core Windows libraries in the Rust programming language, and the more memory-safe code is already reaching developers.... [...]
