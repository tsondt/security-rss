Title: MSI confirms security breach following ransomware attack claims
Date: 2023-04-07T12:39:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-04-07-msi-confirms-security-breach-following-ransomware-attack-claims

[Source](https://www.bleepingcomputer.com/news/security/msi-confirms-security-breach-following-ransomware-attack-claims/){:target="_blank" rel="noopener"}

> Following reports of a ransomware attack, Taiwanese PC vendor MSI (short for Micro-Star International) confirmed today that its network was breached in a cyberattack. [...]
