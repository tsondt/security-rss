Title: The good, the bad and the generative AI
Date: 2023-04-26T08:32:14+00:00
Author: Martin Courtney
Category: The Register
Tags: 
Slug: 2023-04-26-the-good-the-bad-and-the-generative-ai

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/26/the_good_the_bad_and/){:target="_blank" rel="noopener"}

> ChatGPT is just the beginning: CISOs need to prepare for the next wave of AI-powered attacks Sponsored Feature Change in the tech industry is usually evolutionary, but perhaps more interesting are the exceptions to this rule – the microprocessor in 1968, the IBM PC in 1981, the web in 1989, the smartphone in 2007. These are the technologies whose appearance began new eras that completely reshaped the industry around them.... [...]
