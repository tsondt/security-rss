Title: Ransomware gangs abuse Process Explorer driver to kill security software
Date: 2023-04-19T13:46:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-04-19-ransomware-gangs-abuse-process-explorer-driver-to-kill-security-software

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gangs-abuse-process-explorer-driver-to-kill-security-software/){:target="_blank" rel="noopener"}

> Threat actors use a new hacking tool dubbed AuKill to disable Endpoint Detection & Response (EDR) Software on targets' systems before deploying backdoors and ransomware in Bring Your Own Vulnerable Driver (BYOVD) attacks. [...]
