Title: CISA orders govt agencies to update iPhones, Macs by May 1st
Date: 2023-04-10T12:24:43-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-04-10-cisa-orders-govt-agencies-to-update-iphones-macs-by-may-1st

[Source](https://www.bleepingcomputer.com/news/security/cisa-orders-govt-agencies-to-update-iphones-macs-by-may-1st/){:target="_blank" rel="noopener"}

> The Cybersecurity and Infrastructure Security Agency (CISA) ordered federal agencies to patch two security vulnerabilities actively exploited in the wild to hack iPhones, Macs, and iPads. [...]
