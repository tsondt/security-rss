Title: Ukrainian cops nab suspects accused of stealing $4.3m from victims across Europe
Date: 2023-04-01T07:25:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-01-ukrainian-cops-nab-suspects-accused-of-stealing-43m-from-victims-across-europe

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/01/ukrainian_cops_arrest/){:target="_blank" rel="noopener"}

> If the price looks too good to be true, it probably is Ukrainian cops have arrested two suspects and detained 10 others for their alleged roles in a cybercrime gang that used phishing scams and phony online marketplaces to steal more than $4.3 million from over 1,000 victims across Europe.... [...]
