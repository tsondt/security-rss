Title: 40% of IT security pros say they've been told not to report a data leak
Date: 2023-04-11T09:37:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-04-11-40-of-it-security-pros-say-theyve-been-told-not-to-report-a-data-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/11/in_brief_security/){:target="_blank" rel="noopener"}

> Plus: KFC, Pizza Hut owner spills more beans on ransomware hit... latest critical flaws... and more In Brief More than 40 percent of surveyed IT security professionals say they've been told to keep network breaches under wraps despite laws and common decency requiring disclosure.... [...]
