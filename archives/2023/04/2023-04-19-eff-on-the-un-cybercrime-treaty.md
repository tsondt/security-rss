Title: EFF on the UN Cybercrime Treaty
Date: 2023-04-19T10:07:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybercrime;laws;treaties;UN
Slug: 2023-04-19-eff-on-the-un-cybercrime-treaty

[Source](https://www.schneier.com/blog/archives/2023/04/eff-on-the-un-cybercrime-treaty.html){:target="_blank" rel="noopener"}

> EFF has a good explainer on the problems with the new UN Cybercrime Treaty, currently being negotiated in Vienna. The draft treaty has the potential to rewrite criminal laws around the world, possibly adding over 30 criminal offenses and new expansive police powers for both domestic and international criminal investigations. [...] While we don’t think the U.N. Cybercrime Treaty is necessary, we’ve been closely scrutinizing the process and providing constructive analysis. We’ve made clear that human rights must be baked into the proposed treaty so that it doesn’t become a tool to stifle freedom of expression, infringe on privacy and [...]
