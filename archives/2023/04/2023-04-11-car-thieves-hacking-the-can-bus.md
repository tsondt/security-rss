Title: Car Thieves Hacking the CAN Bus
Date: 2023-04-11T11:22:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;hacking;malware;theft
Slug: 2023-04-11-car-thieves-hacking-the-can-bus

[Source](https://www.schneier.com/blog/archives/2023/04/car-thieves-hacking-the-can-bus.html){:target="_blank" rel="noopener"}

> Car thieves are injecting malicious software into a car’s network through wires in the headlights (or taillights) that fool the car into believing that the electronic key is nearby. News articles. [...]
