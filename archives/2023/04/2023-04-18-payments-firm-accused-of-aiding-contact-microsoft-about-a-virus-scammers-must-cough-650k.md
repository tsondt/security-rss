Title: Payments firm accused of aiding 'contact Microsoft about a virus' scammers must cough $650k
Date: 2023-04-18T18:34:13+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-04-18-payments-firm-accused-of-aiding-contact-microsoft-about-a-virus-scammers-must-cough-650k

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/18/us_company_accused_of_helping/){:target="_blank" rel="noopener"}

> 'My computer locked up and a siren went off,' one mark tells Better Business Bureau Two execs and a multinational payment processing company must pay $650k to the US government, says the FTC, which accuses them of knowingly processing credit card payments for Microsoft-themed support scammers.... [...]
