Title: SAP releases security updates for two critical-severity flaws
Date: 2023-04-11T16:54:27-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2023-04-11-sap-releases-security-updates-for-two-critical-severity-flaws

[Source](https://www.bleepingcomputer.com/news/security/sap-releases-security-updates-for-two-critical-severity-flaws/){:target="_blank" rel="noopener"}

> Enterprise software vendor SAP has released its April 2023 security updates for several of its products, which includes fixes for two critical-severity vulnerabilities that impact the SAP Diagnostics Agent and the SAP BusinessObjects Business Intelligence Platform. [...]
