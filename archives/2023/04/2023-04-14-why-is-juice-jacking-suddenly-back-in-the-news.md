Title: Why is ‘Juice Jacking’ Suddenly Back in the News?
Date: 2023-04-14T20:27:56+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Security Tools;Aries Security;Brian Markus;defcon;fbi;FCC;juice jacking;OMG cable;Snopes
Slug: 2023-04-14-why-is-juice-jacking-suddenly-back-in-the-news

[Source](https://krebsonsecurity.com/2023/04/why-is-juice-jacking-suddenly-back-in-the-news/){:target="_blank" rel="noopener"}

> KrebsOnSecurity received a nice bump in traffic this week thanks to tweets from the Federal Bureau of Investigation (FBI) and the Federal Communications Commission (FCC) about “ juice jacking,” a term first coined here in 2011 to describe a potential threat of data theft when one plugs their mobile device into a public charging kiosk. It remains unclear what may have prompted the alerts, but the good news is that there are some fairly basic things you can do to avoid having to worry about juice jacking. On April 6, 2023, the FBI’s Denver office issued a warning about juice [...]
