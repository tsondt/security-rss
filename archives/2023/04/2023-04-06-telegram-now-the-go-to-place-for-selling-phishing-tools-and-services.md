Title: Telegram now the go-to place for selling phishing tools and services
Date: 2023-04-06T03:23:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-06-telegram-now-the-go-to-place-for-selling-phishing-tools-and-services

[Source](https://www.bleepingcomputer.com/news/security/telegram-now-the-go-to-place-for-selling-phishing-tools-and-services/){:target="_blank" rel="noopener"}

> Telegram has become the working ground for the creators of phishing bots and kits looking to market their products to a larger audience or to recruit unpaid helpers. [...]
