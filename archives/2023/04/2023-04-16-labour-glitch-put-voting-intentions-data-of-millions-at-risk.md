Title: Labour glitch put voting intentions data of millions at risk
Date: 2023-04-16T10:53:25+00:00
Author: Pippa Crerar Political editor
Category: The Guardian
Tags: Labour;Politics;UK news;Local elections;Local politics;Data and computer security
Slug: 2023-04-16-labour-glitch-put-voting-intentions-data-of-millions-at-risk

[Source](https://www.theguardian.com/politics/2023/apr/16/labour-glitch-put-voting-intentions-data-of-millions-at-risk){:target="_blank" rel="noopener"}

> Exclusive: experts say sensitive information could potentially have been harvested and used for targeted election interference The voting intentions of millions of Britons in local authority wards across the country could have been at risk of misuse as a result of a glitch in the Labour party’s main phone-banking system, the Guardian understands. Experts had warned that the sensitive data could potentially have been harvested via an automated program and used for targeted election interference by campaign groups or even hostile states. Continue reading... [...]
