Title: Google to kill Dropcam, Nest Secure hardware next year
Date: 2023-04-10T18:58:42+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-04-10-google-to-kill-dropcam-nest-secure-hardware-next-year

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/10/google_dropcam_nest_eol/){:target="_blank" rel="noopener"}

> Great, more company for Stadia, Duo and pals in the graveyard Owners of Dropcam security cameras and Nest Secure systems have been given an unwelcome deadline from Google: their smart home products will be shut off April 8 next year.... [...]
