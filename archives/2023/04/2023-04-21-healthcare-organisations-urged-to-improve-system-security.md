Title: Healthcare organisations urged to improve system security
Date: 2023-04-21T08:35:13+00:00
Author: Rob Jaques
Category: The Register
Tags: 
Slug: 2023-04-21-healthcare-organisations-urged-to-improve-system-security

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/21/healthcare_organisations_urged_to_improve/){:target="_blank" rel="noopener"}

> Patient data covering sensitive areas has long been a high-value target for cybercriminals Sponsored Post Digital patient medical records now cover a whole gamut of sensitive details such as clinical diagnoses/treatments, prescriptions, personal finances and insurance policies. Which makes keeping them safe more important than ever.... [...]
