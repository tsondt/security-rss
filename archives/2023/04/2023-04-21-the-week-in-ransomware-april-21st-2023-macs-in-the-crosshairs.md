Title: The Week in Ransomware - April 21st 2023 - Macs in the Crosshairs
Date: 2023-04-21T18:39:43-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-04-21-the-week-in-ransomware-april-21st-2023-macs-in-the-crosshairs

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-april-21st-2023-macs-in-the-crosshairs/){:target="_blank" rel="noopener"}

> A lot of news broke this week related to ransomware, with the discovery of LockBit testing macOS encryptors to an outage on NCR, causing massive headaches for restaurants. [...]
