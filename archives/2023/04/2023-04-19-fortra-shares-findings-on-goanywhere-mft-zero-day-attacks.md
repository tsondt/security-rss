Title: Fortra shares findings on GoAnywhere MFT zero-day attacks
Date: 2023-04-19T15:06:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-19-fortra-shares-findings-on-goanywhere-mft-zero-day-attacks

[Source](https://www.bleepingcomputer.com/news/security/fortra-shares-findings-on-goanywhere-mft-zero-day-attacks/){:target="_blank" rel="noopener"}

> Fortra has completed its investigation into the exploitation of CVE-2023-0669, a zero-day flaw in the GoAnywhere MFT solution that the Clop ransomware gang exploited to steal data from over a hundred companies. [...]
