Title: New Money Message ransomware demands million dollar ransoms
Date: 2023-04-02T13:36:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-02-new-money-message-ransomware-demands-million-dollar-ransoms

[Source](https://www.bleepingcomputer.com/news/security/new-money-message-ransomware-demands-million-dollar-ransoms/){:target="_blank" rel="noopener"}

> A new ransomware gang named 'Money Message' has appeared, targeting victims worldwide and demanding million-dollar ransoms not to leak data and release a decryptor. [...]
