Title: Money Message ransomware gang claims MSI breach, demands $4 million
Date: 2023-04-06T07:59:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-06-money-message-ransomware-gang-claims-msi-breach-demands-4-million

[Source](https://www.bleepingcomputer.com/news/security/money-message-ransomware-gang-claims-msi-breach-demands-4-million/){:target="_blank" rel="noopener"}

> Taiwanese PC parts maker MSI (Micro-Star International) has been listed on the extortion portal of a new ransomware gang known as "Money Message," which claims to have stolen source code from the company's network. [...]
