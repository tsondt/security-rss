Title: April brings tulips, taxes ... and phisherfolk scammers
Date: 2023-04-03T18:39:08+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-04-03-april-brings-tulips-taxes-and-phisherfolk-scammers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/03/tax_season_phishing_scam/){:target="_blank" rel="noopener"}

> Tactical#Octopus: Don't let users click on that zip file The last few days of America's tax season are stressful enough, dealing with deadlines and, increasingly, online scams. Now comes another one, a sophisticated and ongoing phishing campaign by a threat group dubbed "Tactical#Octopus" that is using tax-related lures to spread malware.... [...]
