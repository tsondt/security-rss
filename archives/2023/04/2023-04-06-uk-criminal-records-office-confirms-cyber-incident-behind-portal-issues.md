Title: UK criminal records office confirms cyber incident behind portal issues
Date: 2023-04-06T15:38:41-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-04-06-uk-criminal-records-office-confirms-cyber-incident-behind-portal-issues

[Source](https://www.bleepingcomputer.com/news/security/uk-criminal-records-office-confirms-cyber-incident-behind-portal-issues/){:target="_blank" rel="noopener"}

> The UK's Criminal Records Office (ACRO) has finally confirmed, after weeks of delaying issuing a statement, that online portal issues experienced since January 17 resulted from what it described as a "cyber security incident." [...]
