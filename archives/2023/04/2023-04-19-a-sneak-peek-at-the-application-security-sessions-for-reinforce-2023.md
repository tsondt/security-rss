Title: A sneak peek at the application security sessions for re:Inforce 2023
Date: 2023-04-19T19:11:00+00:00
Author: Paul Hawkins
Category: AWS Security
Tags: Announcements;AWS re:Inforce;Events;Foundational (100);Security, Identity, & Compliance;Application security;cloud security conference;Live Events;re:Inforce 2023;Security Blog
Slug: 2023-04-19-a-sneak-peek-at-the-application-security-sessions-for-reinforce-2023

[Source](https://aws.amazon.com/blogs/security/a-sneak-peek-at-the-application-security-sessions-for-reinforce-2023/){:target="_blank" rel="noopener"}

> A full conference pass is $1,099. Register today with the code secure150off to receive a limited time $150 discount, while supplies last. AWS re:Inforce is a security learning conference where you can gain skills and confidence in cloud security, compliance, identity, and privacy. As a re:Inforce attendee, you have access to hundreds of technical and non-technical sessions, an Expo [...]
