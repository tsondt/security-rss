Title: Microsoft SQL servers hacked to deploy Trigona ransomware
Date: 2023-04-19T15:26:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-04-19-microsoft-sql-servers-hacked-to-deploy-trigona-ransomware

[Source](https://www.bleepingcomputer.com/news/security/microsoft-sql-servers-hacked-to-deploy-trigona-ransomware/){:target="_blank" rel="noopener"}

> Attackers are hacking into poorly secured and Interned-exposed Microsoft SQL (MS-SQL) servers to deploy Trigona ransomware payloads and encrypt all files. [...]
