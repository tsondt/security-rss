Title: Wrong time to weaken encryption, UK IT chartered institute tells government
Date: 2023-04-18T11:27:10+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2023-04-18-wrong-time-to-weaken-encryption-uk-it-chartered-institute-tells-government

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/18/wrong_time_to_weaken_encryption/){:target="_blank" rel="noopener"}

> Plus: Signal, WhatsApp, and Viber also write online protest over Online Safety Bill back door The UK’s chartered institute for IT has slammed proposed legislation that could see the government open a “back door” to encrypted messaging.... [...]
