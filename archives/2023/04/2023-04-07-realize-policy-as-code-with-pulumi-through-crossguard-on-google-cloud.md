Title: Realize policy-as-code with Pulumi through CrossGuard on Google Cloud
Date: 2023-04-07T16:00:00+00:00
Author: Xuejiao Zhang
Category: GCP Security
Tags: Security & Identity;DevOps & SRE
Slug: 2023-04-07-realize-policy-as-code-with-pulumi-through-crossguard-on-google-cloud

[Source](https://cloud.google.com/blog/products/devops-sre/using-pulumi-crossguard-for-policy-as-code-on-google-cloud/){:target="_blank" rel="noopener"}

> When it comes to creating and deploying cloud infrastructure on Google Cloud, more organizations are using CrossGuard from Pulumi. This policy-as-code offering lets you set guardrails to enforce compliance for resources, so you can provision your own infrastructure while sticking to best practices and baseline your organization’s security compliance. Pulumi lets you write code in your favorite language (such as Python) and automatically provisions and manages Google Cloud resources using an infrastructure-as-code approach. Policy-as-Code is the use of code to define and manage rules and conditions, where a policy contains specific logic you would like to enforce. For example, you [...]
