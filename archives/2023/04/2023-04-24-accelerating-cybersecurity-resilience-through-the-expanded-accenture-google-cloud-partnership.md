Title: Accelerating cybersecurity resilience through the expanded Accenture-Google Cloud partnership
Date: 2023-04-24T16:00:00+00:00
Author: Paolo Dal Cin
Category: GCP Security
Tags: Security & Identity
Slug: 2023-04-24-accelerating-cybersecurity-resilience-through-the-expanded-accenture-google-cloud-partnership

[Source](https://cloud.google.com/blog/products/identity-security/rsa-accelerating-cybersecurity-resilience-accenture-google-cloud-partnership/){:target="_blank" rel="noopener"}

> Accenture and Google Cloud have partnered since 2018 to help the world’s largest organizations digitally transform their enterprises by harnessing our data analytics, AI/ML, and services expertise to build stronger digital cores. Today at the RSA Conference 2023 in San Francisco, we’re excited to announce that the next phase in our growing partnership will focus on enhancing Accenture Security’s Managed Extended Detection and Response (MxDR) service by integrating Chronicle Security Operations, Mandiant Threat Intelligence, and Security AI Workbench. With these enhancements to MxDR, we will be able to better help businesses on any cloud platform transform their security programs, protect [...]
