Title: Google Public Sector achieves CJIS compliance in Florida
Date: 2023-04-10T16:00:00+00:00
Author: Brent Mitchell
Category: GCP Security
Tags: Security & Identity;Public Sector
Slug: 2023-04-10-google-public-sector-achieves-cjis-compliance-in-florida

[Source](https://cloud.google.com/blog/topics/public-sector/how-google-public-sector-supports-the-cjis-standard-across-the-us/){:target="_blank" rel="noopener"}

> Google Public Sector has completed the process with Florida Department of Law Enforcement (FDLE) to ensure Google Cloud supports the requirements necessary to store, process, and support criminal justice information (CJI). As part of this process, Google Public Sector worked with FDLE to conduct physical audits of facilities nationally and technical audits of our National Institute of Standards and Technology Special Publication 800-53 ( NIST 800-53 ) security controls to ensure the highest level of protection for Criminal Justice Information Services (CJIS) workloads are supported by Google Cloud. Google Cloud compliance commitments extend to Google support services and FDLE was [...]
