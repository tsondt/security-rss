Title: Feds seize $112m in cryptocurrency linked to 'pig-butchering' finance scams
Date: 2023-04-04T23:00:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-04-feds-seize-112m-in-cryptocurrency-linked-to-pig-butchering-finance-scams

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/04/fbi_pig_butchering_cryptocurrency/){:target="_blank" rel="noopener"}

> Thieves go nose-to-tail stripping cash from victims The US Department of Justice has seized cryptocurrency worth about $112 million from accounts linked to so-called pig butchering investment scams.... [...]
