Title: US extradites Nigerian charged in $6m email fraud scam
Date: 2023-04-14T21:20:37+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-14-us-extradites-nigerian-charged-in-6m-email-fraud-scam

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/14/nigerian_bec_scam/){:target="_blank" rel="noopener"}

> Maybe our prince has come at last A suspected Nigerian fraudster is scheduled to appear in court Friday for his alleged role in a $6 million plot to scam businesses via email.... [...]
