Title: Research on AI in Adversarial Settings
Date: 2023-04-06T10:59:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence
Slug: 2023-04-06-research-on-ai-in-adversarial-settings

[Source](https://www.schneier.com/blog/archives/2023/04/research-on-ai-in-adversarial-settings.html){:target="_blank" rel="noopener"}

> New research: “ Achilles Heels for AGI/ASI via Decision Theoretic Adversaries “: As progress in AI continues to advance, it is important to know how advanced systems will make choices and in what ways they may fail. Machines can already outsmart humans in some domains, and understanding how to safely build ones which may have capabilities at or above the human level is of particular concern. One might suspect that artificially generally intelligent (AGI) and artificially superintelligent (ASI) will be systems that humans cannot reliably outsmart. As a challenge to this assumption, this paper presents the Achilles Heel hypothesis which [...]
