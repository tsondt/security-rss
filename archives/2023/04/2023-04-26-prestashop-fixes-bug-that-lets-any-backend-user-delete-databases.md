Title: PrestaShop fixes bug that lets any backend user delete databases
Date: 2023-04-26T15:30:05-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-26-prestashop-fixes-bug-that-lets-any-backend-user-delete-databases

[Source](https://www.bleepingcomputer.com/news/security/prestashop-fixes-bug-that-lets-any-backend-user-delete-databases/){:target="_blank" rel="noopener"}

> The open-source e-commerce platform PrestaShop has released a new version that addresses a critical-severity vulnerability allowing any back-office user to write, update, or delete SQL databases regardless of their permissions. [...]
