Title: Medusa ransomware claims attack on Open University of Cyprus
Date: 2023-04-06T12:11:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Education
Slug: 2023-04-06-medusa-ransomware-claims-attack-on-open-university-of-cyprus

[Source](https://www.bleepingcomputer.com/news/security/medusa-ransomware-claims-attack-on-open-university-of-cyprus/){:target="_blank" rel="noopener"}

> The Medusa ransomware gang has claimed a cyberattack on the Open University of Cyprus (OUC), which caused severe disruptions of the organization's operations. [...]
