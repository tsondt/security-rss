Title: Cold storage giant Americold outage caused by network breach
Date: 2023-04-28T15:53:38-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-04-28-cold-storage-giant-americold-outage-caused-by-network-breach

[Source](https://www.bleepingcomputer.com/news/security/cold-storage-giant-americold-outage-caused-by-network-breach/){:target="_blank" rel="noopener"}

> Americold, a leading cold storage and logistics company, has been facing IT issues since its network was breached on Tuesday night. [...]
