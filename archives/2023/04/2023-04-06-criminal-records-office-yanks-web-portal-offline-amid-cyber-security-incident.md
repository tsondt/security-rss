Title: Criminal records office yanks web portal offline amid 'cyber security incident'
Date: 2023-04-06T08:30:07+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-04-06-criminal-records-office-yanks-web-portal-offline-amid-cyber-security-incident

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/06/acro_security_incident/){:target="_blank" rel="noopener"}

> ACRO says payment data safe, other info may have been snaffled ACRO, the UK's criminal records office, is combing over a "cyber security incident" that forced it to pull its customer portal offline.... [...]
