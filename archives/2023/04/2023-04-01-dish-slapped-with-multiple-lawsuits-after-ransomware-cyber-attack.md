Title: DISH slapped with multiple lawsuits after ransomware cyber attack
Date: 2023-04-01T06:39:00-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-04-01-dish-slapped-with-multiple-lawsuits-after-ransomware-cyber-attack

[Source](https://www.bleepingcomputer.com/news/security/dish-slapped-with-multiple-lawsuits-after-ransomware-cyber-attack/){:target="_blank" rel="noopener"}

> Dish Network has been slapped with multiple class action lawsuits after it suffered a ransomware incident that was behind the company's multi-day "network outage." The legal actions aim to recover losses faced by DISH investors who were adversely affected by what has been dubbed a "securities fraud." [...]
