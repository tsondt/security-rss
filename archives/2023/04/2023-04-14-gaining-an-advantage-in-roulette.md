Title: Gaining an Advantage in Roulette
Date: 2023-04-14T11:02:30+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cheating;gambling
Slug: 2023-04-14-gaining-an-advantage-in-roulette

[Source](https://www.schneier.com/blog/archives/2023/04/gaining-an-advantage-in-roulette.html){:target="_blank" rel="noopener"}

> You can beat the game without a computer : On a perfect [roulette] wheel, the ball would always fall in a random way. But over time, wheels develop flaws, which turn into patterns. A wheel that’s even marginally tilted could develop what Barnett called a ‘drop zone.’ When the tilt forces the ball to climb a slope, the ball decelerates and falls from the outer rim at the same spot on almost every spin. A similar thing can happen on equipment worn from repeated use, or if a croupier’s hand lotion has left residue, or for a dizzying number of [...]
