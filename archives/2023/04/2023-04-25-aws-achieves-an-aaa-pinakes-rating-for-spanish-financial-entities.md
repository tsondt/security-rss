Title: AWS achieves an AAA Pinakes rating for Spanish financial entities
Date: 2023-04-25T01:54:22+00:00
Author: Daniel Fuertes
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;3P Risk;AWS security;Compliance;Cyber Risk Management;financial cybersecurity;Security Blog;Spain;Third Party Risk;Third-Party Risk Management
Slug: 2023-04-25-aws-achieves-an-aaa-pinakes-rating-for-spanish-financial-entities

[Source](https://aws.amazon.com/blogs/security/aws-achieves-an-aaa-pinakes-rating-for-spanish-financial-entities/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce that we have achieved an AAA rating from Pinakes. The scope of this qualification covers 166 services in 25 global AWS Regions. The Spanish banking association Centro de Cooperación Interbancaria (CCI) developed Pinakes, a rating framework intended to manage and monitor the cybersecurity controls of service providers [...]
