Title: DoJ, Treasury accuses 3 men of laundering crypto for North Korea
Date: 2023-04-26T18:44:25+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-04-26-doj-treasury-accuses-3-men-of-laundering-crypto-for-north-korea

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/26/doj_treasury_sanctions_north_korea/){:target="_blank" rel="noopener"}

> If the DPRK is named, you know it somehow involves Lazarus Group The US government is aggressively pursuing three men accused of wide-ranging and complex conspiracies of laundering stolen and illicit cryptocurrency that the North Korean regime used to finance its massive weapons programs.... [...]
