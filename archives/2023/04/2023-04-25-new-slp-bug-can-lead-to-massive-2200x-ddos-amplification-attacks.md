Title: New SLP bug can lead to massive 2,200x DDoS amplification attacks
Date: 2023-04-25T11:26:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2023-04-25-new-slp-bug-can-lead-to-massive-2200x-ddos-amplification-attacks

[Source](https://www.bleepingcomputer.com/news/security/new-slp-bug-can-lead-to-massive-2-200x-ddos-amplification-attacks/){:target="_blank" rel="noopener"}

> A new reflective Denial-of-Service (DoS) amplification vulnerability in the Service Location Protocol (SLP) allows threat actors to launch massive denial-of-service attacks with 2,200X amplification. [...]
