Title: Massive Balada Injector campaign attacking WordPress sites since 2017
Date: 2023-04-07T12:24:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-07-massive-balada-injector-campaign-attacking-wordpress-sites-since-2017

[Source](https://www.bleepingcomputer.com/news/security/massive-balada-injector-campaign-attacking-wordpress-sites-since-2017/){:target="_blank" rel="noopener"}

> An estimated one million WordPress websites have been compromised during a long-lasting campaign that exploits "all known and recently discovered theme and plugin vulnerabilities" to inject a Linux backdoor that researchers named Balad Injector. [...]
