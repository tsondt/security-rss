Title: Reduce triage time for security investigations with Amazon Detective visualizations and export data
Date: 2023-04-05T20:51:16+00:00
Author: Alex Waddell
Category: AWS Security
Tags: Amazon Detective;Amazon GuardDuty;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2023-04-05-reduce-triage-time-for-security-investigations-with-amazon-detective-visualizations-and-export-data

[Source](https://aws.amazon.com/blogs/security/reduce-triage-time-for-security-investigations-with-detective-visualizations-and-export-data/){:target="_blank" rel="noopener"}

> To respond to emerging threats, you will often need to sort through large datasets rapidly to prioritize security findings. Amazon Detective recently released two new features to help you do this. New visualizations in Detective show the connections between entities related to multiple Amazon GuardDuty findings, and a new export data feature helps you use [...]
