Title: How Google and Intel make Confidential Computing more secure
Date: 2023-04-24T16:00:00+00:00
Author: Andrés Lagar-Cavilla
Category: GCP Security
Tags: Security & Identity
Slug: 2023-04-24-how-google-and-intel-make-confidential-computing-more-secure

[Source](https://cloud.google.com/blog/products/identity-security/rsa-google-intel-confidential-computing-more-secure/){:target="_blank" rel="noopener"}

> Confidential Computing has quickly emerged as a critical technology to ensure confidentiality and security of sensitive data while it’s being processed. It performs computation in a hardware isolated environment that is encrypted with keys managed by the processor and unavailable to the operator. These isolated environments help prevent unauthorized access or modification of applications and data while in use, thereby increasing the security assurances for organizations that manage sensitive and regulated data in public cloud infrastructure. Raising the bar for Confidential Computing Google is committed to ensuring Confidential Computing technology is as secure as possible before releasing products to customers. [...]
