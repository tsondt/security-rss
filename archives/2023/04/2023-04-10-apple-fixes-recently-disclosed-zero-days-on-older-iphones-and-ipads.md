Title: Apple fixes recently disclosed zero-days on older iPhones and iPads
Date: 2023-04-10T16:16:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple;Security
Slug: 2023-04-10-apple-fixes-recently-disclosed-zero-days-on-older-iphones-and-ipads

[Source](https://www.bleepingcomputer.com/news/apple/apple-fixes-recently-disclosed-zero-days-on-older-iphones-and-ipads/){:target="_blank" rel="noopener"}

> Apple has released emergency updates to backport security patches released on Friday, addressing two actively exploited zero-day flaws also affecting older iPhones, iPads, and Macs. [...]
