Title: Russian snoops just love invading unpatched Cisco gear, America and UK warn
Date: 2023-04-18T20:45:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-18-russian-snoops-just-love-invading-unpatched-cisco-gear-america-and-uk-warn

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/18/uk_us_apt28_cisco_routers/){:target="_blank" rel="noopener"}

> Spying on foreign targets? That's our job! The UK and US governments have sounded the alarm on Russian intelligence targeting unpatched Cisco routers to deploy malware and carry out surveillance.... [...]
