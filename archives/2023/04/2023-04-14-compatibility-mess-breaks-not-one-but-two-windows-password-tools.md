Title: Compatibility mess breaks not one but two Windows password tools
Date: 2023-04-14T17:50:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-04-14-compatibility-mess-breaks-not-one-but-two-windows-password-tools

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/14/microsoft_laps_compatibility/){:target="_blank" rel="noopener"}

> Windows LAPS and legacy LAPS don't play nicely under certain conditions, Microsoft says Integrating the Local Administrator Password Solution (LAPS) into Windows and Windows Server that came with updates earlier this week is causing interoperability problems with what's called legacy LAPS, Microsoft says.... [...]
