Title: Android Minecraft clones with 35M downloads infect users with adware
Date: 2023-04-27T15:42:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2023-04-27-android-minecraft-clones-with-35m-downloads-infect-users-with-adware

[Source](https://www.bleepingcomputer.com/news/security/android-minecraft-clones-with-35m-downloads-infect-users-with-adware/){:target="_blank" rel="noopener"}

> A set of 38 Minecraft copycat games on Google Play infected devices with the Android adware 'HiddenAds' to stealthily load ads in the background to generate revenue for its operators. [...]
