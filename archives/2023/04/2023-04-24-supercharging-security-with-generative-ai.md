Title: Supercharging security with generative AI
Date: 2023-04-24T16:00:00+00:00
Author: Sunil Potti
Category: GCP Security
Tags: AI & Machine Learning;Security & Identity
Slug: 2023-04-24-supercharging-security-with-generative-ai

[Source](https://cloud.google.com/blog/products/identity-security/rsa-google-cloud-security-ai-workbench-generative-ai/){:target="_blank" rel="noopener"}

> At Google Cloud, we continue to invest in key technologies to progress towards our true north star on invisible security : making strong security pervasive and simple for everyone. Our investments are based on insights from our world-class threat intelligence teams and experience helping customers respond to the most sophisticated cyberattacks. Customers can tap into these capabilities to gain perspective and visibility on the most dangerous threat actors that no one else has. Recent advances in artificial intelligence (AI), particularly large language models (LLMs), accelerate our ability to help the people who are responsible for keeping their organizations safe. These [...]
