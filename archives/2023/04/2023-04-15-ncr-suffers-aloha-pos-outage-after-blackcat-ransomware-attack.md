Title: NCR suffers Aloha POS outage after BlackCat ransomware attack
Date: 2023-04-15T14:26:51-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Technology
Slug: 2023-04-15-ncr-suffers-aloha-pos-outage-after-blackcat-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/ncr-suffers-aloha-pos-outage-after-blackcat-ransomware-attack/){:target="_blank" rel="noopener"}

> NCR is suffering an outage on its Aloha point of sale platform after being hit by an ransomware attack claimed by the BlackCat/ALPHV gang. [...]
