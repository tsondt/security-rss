Title: FBI Seizes Bot Shop ‘Genesis Market’ Amid Arrests Targeting Operators, Suppliers
Date: 2023-04-04T21:04:11+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;fbi;Flashpoint;Genesis Market;Genesis Security;Intel 471;U.S. District Court for the Eastern District of Wisconsin
Slug: 2023-04-04-fbi-seizes-bot-shop-genesis-market-amid-arrests-targeting-operators-suppliers

[Source](https://krebsonsecurity.com/2023/04/fbi-seizes-bot-shop-genesis-market-amid-arrests-targeting-operators-suppliers/){:target="_blank" rel="noopener"}

> Several domain names tied to Genesis Market, a bustling cybercrime store that sold access to passwords and other data stolen from millions of computers infected with malicious software, were seized by the Federal Bureau of Investigation (FBI) today. Sources tell KrebsOnsecurity the domain seizures coincided with “dozens” of arrests in the United States and abroad targeting those who allegedly operated the service, as well as suppliers who continuously fed Genesis Market with freshly-stolen data. Several websites tied to the cybercrime store Genesis Market had their homepages changed today to this seizure notice. Active since 2018, Genesis Market’s slogan was, “Our [...]
