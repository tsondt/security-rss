Title: Appeals court spares Google from $20m patent payout over Chrome
Date: 2023-04-19T22:28:05+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-04-19-appeals-court-spares-google-from-20m-patent-payout-over-chrome

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/19/google_20m_patent_appeal/){:target="_blank" rel="noopener"}

> Chocolate Factory can afford some staples now, or? Six years after a jury decided otherwise, Google has convinced an appeals court to reverse a $20 million judgment against the web giant after Chrome infringed some patents.... [...]
