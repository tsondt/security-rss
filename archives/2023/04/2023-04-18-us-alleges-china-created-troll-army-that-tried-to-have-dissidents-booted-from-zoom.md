Title: US alleges China created troll army that tried to have dissidents booted from Zoom
Date: 2023-04-18T04:37:21+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-04-18-us-alleges-china-created-troll-army-that-tried-to-have-dissidents-booted-from-zoom

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/18/us_vs_yunpeng_bai_julien_li/){:target="_blank" rel="noopener"}

> Charges laid against 44, including officers of China’s Cyberspace Administration The United States Department of Justice has charged 44 people over schemes prosecutors allege were run by China’s National Police to silence opponents of the Communist Party of China.... [...]
