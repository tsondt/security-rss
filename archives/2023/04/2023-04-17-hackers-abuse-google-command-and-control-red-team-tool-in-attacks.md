Title: Hackers abuse Google Command and Control red team tool in attacks
Date: 2023-04-17T13:05:25-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-04-17-hackers-abuse-google-command-and-control-red-team-tool-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/hackers-abuse-google-command-and-control-red-team-tool-in-attacks/){:target="_blank" rel="noopener"}

> The Chinese state-sponsored hacking group APT41 was found abusing the GC2 (Google Command and Control) red teaming tool in data theft attacks against a Taiwanese media and an Italian job search company. [...]
