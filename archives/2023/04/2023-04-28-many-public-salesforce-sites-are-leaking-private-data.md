Title: Many Public Salesforce Sites are Leaking Private Data
Date: 2023-04-28T02:09:56+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;The Coming Storm;Time to Patch;Charan Akiri;DC Health;DC Health Link;Huntington Bank;Matthew Jennings;Mike Rupert;Salesforce Community websites;Scott Carbee;TCF Bank;Vermont
Slug: 2023-04-28-many-public-salesforce-sites-are-leaking-private-data

[Source](https://krebsonsecurity.com/2023/04/many-public-salesforce-sites-are-leaking-private-data/){:target="_blank" rel="noopener"}

> A shocking number of organizations — including banks and healthcare providers — are leaking private and sensitive information from their public Salesforce Community websites, KrebsOnSecurity has learned. The data exposures all stem from a misconfiguration in Salesforce Community that allows an unauthenticated user to access records that should only be available after logging in. A researcher found DC Health had five Salesforce Community sites exposing data. Salesforce Community is a widely-used cloud-based software product that makes it easy for organizations to quickly create websites. Customers can access a Salesforce Community website in two ways: Authenticated access (requiring login), and guest [...]
