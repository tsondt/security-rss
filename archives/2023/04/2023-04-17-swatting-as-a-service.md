Title: Swatting as a Service
Date: 2023-04-17T11:15:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;cyberattack;impersonation;police
Slug: 2023-04-17-swatting-as-a-service

[Source](https://www.schneier.com/blog/archives/2023/04/swatting-as-a-service.html){:target="_blank" rel="noopener"}

> Motherboard is reporting on AI-generated voices being used for “swatting”: In fact, Motherboard has found, this synthesized call and another against Hempstead High School were just one small part of a months-long, nationwide campaign of dozens, and potentially hundreds, of threats made by one swatter in particular who has weaponized computer generated voices. Known as “Torswats” on the messaging app Telegram, the swatter has been calling in bomb and mass shooting threats against highschools and other locations across the country. Torswat’s connection to these wide ranging swatting incidents has not been previously reported. The further automation of swatting techniques threatens [...]
