Title: It's this easy to seize control of someone's Nexx 'smart' home plugs, garage doors
Date: 2023-04-07T11:00:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-07-its-this-easy-to-seize-control-of-someones-nexx-smart-home-plugs-garage-doors

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/07/cisa_nexx_iot_flaws/){:target="_blank" rel="noopener"}

> Netizens urged to disconnect kit after 40,000-plus devices found riddled with dumb bugs A handful of bugs in Nexx's smart home devices can be exploited by crooks to, among other things, open doors, power off appliances, and disable alarms. More than 40,000 of these gadgets in residential and commercial properties are said to be vulnerable after the manufacturer failed to act.... [...]
