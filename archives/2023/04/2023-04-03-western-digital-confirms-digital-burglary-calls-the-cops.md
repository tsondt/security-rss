Title: Western Digital confirms digital burglary, calls the cops
Date: 2023-04-03T11:58:22+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2023-04-03-western-digital-confirms-digital-burglary-calls-the-cops

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/03/western_digital_confirms_security_incident/){:target="_blank" rel="noopener"}

> Thinks info from internal systems 'obtained' by miscreant, unsure of nature or scope data Western Digital is today dealing with a "network security incident" after detecting a break-in into its internal systems by an unauthorized third party.... [...]
