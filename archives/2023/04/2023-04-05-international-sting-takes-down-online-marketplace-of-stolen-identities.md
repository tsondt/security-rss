Title: International sting takes down online marketplace of stolen identities
Date: 2023-04-05T12:00:04+00:00
Author: Jamie Grierson
Category: The Guardian
Tags: Data and computer security;Internet;Identity fraud;UK news;World news;US news
Slug: 2023-04-05-international-sting-takes-down-online-marketplace-of-stolen-identities

[Source](https://www.theguardian.com/technology/2023/apr/05/international-sting-takes-down-online-marketplace-of-stolen-identities){:target="_blank" rel="noopener"}

> Operation led by FBI and Dutch police with involvement of UK National Crime Agency takes Genesis Market offline A criminal online marketplace selling millions of stolen identities for as little as 56p has been taken down in an international crackdown. The sting, led by the FBI and Dutch police with the involvement of law enforcement agencies across 18 countries, including the UK’s National Crime Agency (NCA), took Genesis Market offline on Tuesday evening. Continue reading... [...]
