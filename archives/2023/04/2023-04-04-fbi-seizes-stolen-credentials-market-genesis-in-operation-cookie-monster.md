Title: FBI seizes stolen credentials market Genesis in Operation Cookie Monster
Date: 2023-04-04T16:18:43-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-04-04-fbi-seizes-stolen-credentials-market-genesis-in-operation-cookie-monster

[Source](https://www.bleepingcomputer.com/news/security/fbi-seizes-stolen-credentials-market-genesis-in-operation-cookie-monster/){:target="_blank" rel="noopener"}

> The domains for Genesis Market, one of the most popular marketplaces for stolen credentials of all types, were seized by law enforcement earlier this week as part of Operation Cookie Monster. [...]
