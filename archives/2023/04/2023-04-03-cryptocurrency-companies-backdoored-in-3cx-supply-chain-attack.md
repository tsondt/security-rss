Title: Cryptocurrency companies backdoored in 3CX supply chain attack
Date: 2023-04-03T13:22:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-04-03-cryptocurrency-companies-backdoored-in-3cx-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/cryptocurrency-companies-backdoored-in-3cx-supply-chain-attack/){:target="_blank" rel="noopener"}

> Some of the victims affected by the 3CX supply chain attack have also had their systems backdoored with Gopuram malware, with the threat actors specifically targeting cryptocurrency companies with this additional malicious payload. [...]
