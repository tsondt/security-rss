Title: Hacking Suicide
Date: 2023-04-14T19:06:51+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Denmark;hacking;laws
Slug: 2023-04-14-hacking-suicide

[Source](https://www.schneier.com/blog/archives/2023/04/hacking-suicide.html){:target="_blank" rel="noopener"}

> Here’s a religious hack : You want to commit suicide, but it’s a mortal sin: your soul goes straight to hell, forever. So what you do is murder someone. That will get you executed, but if you confess your sins to a priest beforehand you avoid hell. Problem solved. This was actually a problem in the 17th and 18th centuries in Northern Europe, particularly Denmark. And it remained a problem until capital punishment was abolished for murder. It’s a clever hack. I didn’t learn about it in time to put it in my book, A Hacker’s Mind, but I have [...]
