Title: 3CX thought supply chain attack was a false positive
Date: 2023-04-03T07:32:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-04-03-3cx-thought-supply-chain-attack-was-a-false-positive

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/03/3cx_false_positive_supply_chain_attack/){:target="_blank" rel="noopener"}

> 'It's not unusual for VoIP apps' says CEO The CEO of VoIP software provider 3CX said his team tested its products in response to alerts notifying it of a supply chain attack, and assessed reports that its client code was infested with malware were a false positive.... [...]
