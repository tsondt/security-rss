Title: Use IAM roles to connect GitHub Actions to actions in AWS
Date: 2023-04-20T19:28:28+00:00
Author: David Rowe
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;Technical How-to;AWS IAM;AWS Identity and Access Management (IAM);IAM;Security Blog
Slug: 2023-04-20-use-iam-roles-to-connect-github-actions-to-actions-in-aws

[Source](https://aws.amazon.com/blogs/security/use-iam-roles-to-connect-github-actions-to-actions-in-aws/){:target="_blank" rel="noopener"}

> Have you ever wanted to initiate change in an Amazon Web Services (AWS) account after you update a GitHub repository, or deploy updates in an AWS application after you merge a commit, without the use of AWS Identity and Access Management (IAM) user access keys? If you configure an OpenID Connect (OIDC) identity provider (IdP) [...]
