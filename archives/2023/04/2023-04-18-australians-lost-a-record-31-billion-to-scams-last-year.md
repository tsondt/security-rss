Title: Australians lost a record $3.1 billion to scams last year
Date: 2023-04-18T12:00:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-18-australians-lost-a-record-31-billion-to-scams-last-year

[Source](https://www.bleepingcomputer.com/news/security/australians-lost-a-record-31-billion-to-scams-last-year/){:target="_blank" rel="noopener"}

> The Australian Competition & Consumer Commission (ACCC) says Australians lost a record $3.1 billion to scams in 2022, an 80% increase over the total losses recorded in 2021. [...]
