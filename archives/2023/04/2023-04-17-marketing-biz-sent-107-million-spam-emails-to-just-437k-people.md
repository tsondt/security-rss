Title: Marketing biz sent 107 million spam emails... to just 437k people
Date: 2023-04-17T12:45:13+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-04-17-marketing-biz-sent-107-million-spam-emails-to-just-437k-people

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/17/ico_spam_email_fine/){:target="_blank" rel="noopener"}

> Recruitment company fined £130,000 by data regulator for breaking PECR A recruitment business that sent out an eye watering 107 million spam emails is now nursing a £130,000 ($161,000) fine from Britain’s data watchdog.... [...]
