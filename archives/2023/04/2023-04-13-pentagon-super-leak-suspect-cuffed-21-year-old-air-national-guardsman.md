Title: Pentagon super-leak suspect cuffed: 21-year-old Air National Guardsman
Date: 2023-04-13T19:52:28+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-13-pentagon-super-leak-suspect-cuffed-21-year-old-air-national-guardsman

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/13/alleged_pentagon_leaker_arrested/){:target="_blank" rel="noopener"}

> When bragging about your job on Discord gets just a little out of hand? The FBI has detained a 21-year-old Air National Guardsman suspected of leaking a trove of classified Pentagon documents on Discord.... [...]
