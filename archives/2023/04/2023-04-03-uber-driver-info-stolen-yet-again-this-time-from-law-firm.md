Title: Uber driver info stolen yet again: This time from law firm
Date: 2023-04-03T20:27:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-03-uber-driver-info-stolen-yet-again-this-time-from-law-firm

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/03/uber_drivers_info_stolen/){:target="_blank" rel="noopener"}

> Never mind software supply chain attacks, lawyers are the new soft target? Uber has had more of its internal data stolen from a third party that suffered a security breach. This time, the personal info of the app's drivers was swiped by miscreants from the IT systems of law firm Genova Burns.... [...]
