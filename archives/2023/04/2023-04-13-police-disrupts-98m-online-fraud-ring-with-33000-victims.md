Title: Police disrupts $98M online fraud ring with 33,000 victims
Date: 2023-04-13T11:44:24-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-04-13-police-disrupts-98m-online-fraud-ring-with-33000-victims

[Source](https://www.bleepingcomputer.com/news/security/police-disrupts-98m-online-fraud-ring-with-33-000-victims/){:target="_blank" rel="noopener"}

> Europol and Eurojust announced today the arrest of five individuals believed to be part of a massive online investment fraud ring with at least 33,000 victims who lost an estimated €89 million (roughly $98 million). [...]
