Title: Microsoft 365 search outage affects Outlook, Teams, and SharePoint
Date: 2023-04-24T07:47:39-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-04-24-microsoft-365-search-outage-affects-outlook-teams-and-sharepoint

[Source](https://www.bleepingcomputer.com/news/security/microsoft-365-search-outage-affects-outlook-teams-and-sharepoint/){:target="_blank" rel="noopener"}

> Microsoft is investigating an ongoing issue preventing some customers from using the search functionality across multiple Microsoft 365 services. [...]
