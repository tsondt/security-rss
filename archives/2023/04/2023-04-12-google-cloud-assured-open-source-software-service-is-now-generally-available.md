Title: Google Cloud Assured Open Source Software service is now generally available
Date: 2023-04-12T16:00:00+00:00
Author: Andy Chang
Category: GCP Security
Tags: Security & Identity
Slug: 2023-04-12-google-cloud-assured-open-source-software-service-is-now-generally-available

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-assured-open-source-software-service-now-ga/){:target="_blank" rel="noopener"}

> Threats to the software supply chain and open source software (OSS) security continue to be major areas of concern for organizations creating apps and their developers. According to Mandiant’s M-Trends 2022 report, 17% of all security breaches start with a supply chain attack, the initial infection vector second only to exploits. Building on Google’s efforts to improve OSS security, we are announcing the general availability of the Assured Open Source Software (Assured OSS) service for Java and Python ecosystems. Available today at no cost, Assured OSS gives any organization that uses open source software the opportunity to leverage the security [...]
