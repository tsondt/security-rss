Title: Oh SNP! VMs get even more confidential
Date: 2023-04-26T16:00:00+00:00
Author: Joanna Young
Category: GCP Security
Tags: Security & Identity
Slug: 2023-04-26-oh-snp-vms-get-even-more-confidential

[Source](https://cloud.google.com/blog/products/identity-security/rsa-snp-vm-more-confidential/){:target="_blank" rel="noopener"}

> A Confidential Virtual Machine (VM) is a type of Google Cloud Compute Engine VM that helps ensure your data and applications stay private and encrypted even while in use. Confidential VMs can help customers maintain control of their data in the public cloud, achieve cryptographic isolation in a multi-tenant environment, and add an additional layer of defense and data protection against cloud operators, admins, and insiders. At Google Cloud, we are always looking for ways to raise the security bar. Today at the RSA Conference in San Francisco, we’ve raised it again by adding more hardware-based security protections to Confidential [...]
