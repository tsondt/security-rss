Title: Five arrested after 33,000 victims lose $98M to online investment fraud
Date: 2023-04-13T11:44:24-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-04-13-five-arrested-after-33000-victims-lose-98m-to-online-investment-fraud

[Source](https://www.bleepingcomputer.com/news/security/five-arrested-after-33-000-victims-lose-98m-to-online-investment-fraud/){:target="_blank" rel="noopener"}

> Europol and Eurojust announced today the arrest of five individuals believed to be part of a massive online investment fraud ring with at least 33,000 victims who lost an estimated €89 million (roughly $98 million). [...]
