Title: Firmware is on shaky ground – let's see what it's made of
Date: 2023-04-17T09:41:14+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2023-04-17-firmware-is-on-shaky-ground-lets-see-what-its-made-of

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/17/opinion_column/){:target="_blank" rel="noopener"}

> Old architectures just don't stack up Opinion Most data theft does clear harm to the victim, and often to its customers. But while embarrassing, the cyberattack against MSI in which source code was said to be stolen is harder to diagnose. It looks like a valuable company asset that's cost a lot to develop. That its theft may be no loss is a weird idea. But then, firmware is weirder than we give it credit for. It's even hard to say exactly what it is.... [...]
