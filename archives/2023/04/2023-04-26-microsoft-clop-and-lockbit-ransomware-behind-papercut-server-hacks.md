Title: Microsoft: Clop and LockBit ransomware behind PaperCut server hacks
Date: 2023-04-26T19:28:17-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-04-26-microsoft-clop-and-lockbit-ransomware-behind-papercut-server-hacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-clop-and-lockbit-ransomware-behind-papercut-server-hacks/){:target="_blank" rel="noopener"}

> ​Microsoft has attributed recent attacks on PaperCut servers to the Clop and LockBit ransomware operations, which used the vulnerabilities to steal corporate data. [...]
