Title: TLS inspection configuration for encrypted traffic and AWS Network Firewall
Date: 2023-04-04T20:04:41+00:00
Author: Shiva Vaidyanathan
Category: AWS Security
Tags: AWS Network Firewall;Best Practices;Intermediate (200);Security, Identity, & Compliance;Security;Security Blog
Slug: 2023-04-04-tls-inspection-configuration-for-encrypted-traffic-and-aws-network-firewall

[Source](https://aws.amazon.com/blogs/security/tls-inspection-configuration-for-encrypted-traffic-and-aws-network-firewall/){:target="_blank" rel="noopener"}

> AWS Network Firewall is a managed service that provides a convenient way to deploy essential network protections for your virtual private clouds (VPCs). In this blog, we are going to cover how to leverage the TLS inspection configuration with AWS Network Firewall and perform Deep Packet Inspection for encrypted traffic. We shall also discuss key [...]
