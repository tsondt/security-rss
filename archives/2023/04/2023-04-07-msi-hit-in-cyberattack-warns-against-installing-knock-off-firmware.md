Title: MSI hit in cyberattack, warns against installing knock-off firmware
Date: 2023-04-07T23:26:11+00:00
Author: Tobias Mann
Category: The Register
Tags: 
Slug: 2023-04-07-msi-hit-in-cyberattack-warns-against-installing-knock-off-firmware

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/07/msi_cyberattack_bios/){:target="_blank" rel="noopener"}

> 1.5TB of databases, source code, BIOS tools said to be stolen Owners of MSI-brand motherboards, GPUs, notebooks, PCs, and other equipment should exercise caution when updating their device's firmware or BIOS after the manufacturer revealed it has recently suffered a cyberattack.... [...]
