Title: Thanks for fixing the computer lab. Now tell us why we shouldn’t expel you?
Date: 2023-04-21T06:32:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-04-21-thanks-for-fixing-the-computer-lab-now-tell-us-why-we-shouldnt-expel-you

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/21/on_call/){:target="_blank" rel="noopener"}

> Guessing the admin password is cool. Using it, even for good, is dangerous On Call It’s always twelve o’clock somewhere, the saying goes, but Friday comes around but once a week and only this day does The Register offer a fresh instalment of On Call, our reader-contributed tales of tech support torture and turmoil.... [...]
