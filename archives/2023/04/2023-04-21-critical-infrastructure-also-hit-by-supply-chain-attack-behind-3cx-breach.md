Title: Critical infrastructure also hit by supply chain attack behind 3CX breach
Date: 2023-04-21T15:26:43-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-04-21-critical-infrastructure-also-hit-by-supply-chain-attack-behind-3cx-breach

[Source](https://www.bleepingcomputer.com/news/security/critical-infrastructure-also-hit-by-supply-chain-attack-behind-3cx-breach/){:target="_blank" rel="noopener"}

> The X_Trader software supply chain attack that led to last month's 3CX breach has also impacted at least several critical infrastructure organizations in the United States and Europe, according to Symantec's Threat Hunter Team. [...]
