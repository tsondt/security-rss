Title: Hackers can open Nexx garage doors remotely, and there's no fix
Date: 2023-04-05T11:28:31-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-05-hackers-can-open-nexx-garage-doors-remotely-and-theres-no-fix

[Source](https://www.bleepingcomputer.com/news/security/hackers-can-open-nexx-garage-doors-remotely-and-theres-no-fix/){:target="_blank" rel="noopener"}

> Multiple vulnerabilities discovered Nexx smart devices can be exploited to control garage doors, disable home alarms, or smart plugs. [...]
