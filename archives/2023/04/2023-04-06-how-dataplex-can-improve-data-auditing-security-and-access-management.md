Title: How Dataplex can improve data auditing, security, and access management
Date: 2023-04-06T16:00:00+00:00
Author: Gerard Salvador López
Category: GCP Security
Tags: Security & Identity;Data Analytics
Slug: 2023-04-06-how-dataplex-can-improve-data-auditing-security-and-access-management

[Source](https://cloud.google.com/blog/products/data-analytics/how-dataplex-can-improve-data-auditing-security-and-access-management/){:target="_blank" rel="noopener"}

> Data is one of the most important assets of any enterprise. It is essential for making informed decisions, improving efficiency, and providing a competitive edge. However, managing data comes with the responsibility of preventing data misuse. Especially in regulated industries, mishandling data can lead to significant financial and reputational damage. Negative outcomes such as data exfiltration, data access by unauthorized personnel, and inadvertent data deletion can arise if data is mis-managed. There are multiple ways to help protect data in enterprises. These include encryption, controlling access, and data backup. Encryption is the process of encoding data into ciphertext. If done [...]
