Title: Vice Society ransomware uses new PowerShell data theft tool in attacks
Date: 2023-04-14T15:46:58-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-14-vice-society-ransomware-uses-new-powershell-data-theft-tool-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/vice-society-ransomware-uses-new-powershell-data-theft-tool-in-attacks/){:target="_blank" rel="noopener"}

> The Vice Society ransomware gang is deploying a new, rather sophisticated PowerShell script to automate data theft from compromised networks. [...]
