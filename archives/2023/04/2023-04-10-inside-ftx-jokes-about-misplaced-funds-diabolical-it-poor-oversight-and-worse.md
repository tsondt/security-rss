Title: Inside FTX: Jokes about misplaced funds, diabolical IT, poor oversight, and worse
Date: 2023-04-10T21:43:29+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-04-10-inside-ftx-jokes-about-misplaced-funds-diabolical-it-poor-oversight-and-worse

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/10/ftx_liquidators_report/){:target="_blank" rel="noopener"}

> How's the saying go? $50m here, $50m there, pretty soon you're talking real money The liquidators picking over the remains of FTX have released their first formal report into Sam Bankman-Fried's imploded empire – and it somehow appears things are worse than feared.... [...]
