Title: Google named a 2023 Strong Performer in the Gartner Peer Insights™ Voice of the Customer for Security Information and Event Management
Date: 2023-04-06T16:00:00+00:00
Author: Ahnna Schini
Category: GCP Security
Tags: Security & Identity
Slug: 2023-04-06-google-named-a-2023-strong-performer-in-the-gartner-peer-insightstm-voice-of-the-customer-for-security-information-and-event-management

[Source](https://cloud.google.com/blog/products/identity-security/chronicle-siem-is-a-gartner-peer-insights-customers-choice/){:target="_blank" rel="noopener"}

> In 2016, Google’s "X", the moonshot factory, began working on a project aimed to take on cybersecurity. The belief that we could increase the speed and impact of security teams if it were much easier, faster, and more cost-effective for them to capture and analyze security-related clues from across their organizations gave birth to Chronicle, a modern cloud-native security analytics platform that later became part of Google Cloud. Like any new product, we dreamt of the day that Chronicle would add real-world value to customers' lives – so much value in fact, that security teams would want to share it [...]
