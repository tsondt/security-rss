Title: All Dutch govt networks to use RPKI to prevent BGP hijacking
Date: 2023-04-09T11:21:21-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-09-all-dutch-govt-networks-to-use-rpki-to-prevent-bgp-hijacking

[Source](https://www.bleepingcomputer.com/news/security/all-dutch-govt-networks-to-use-rpki-to-prevent-bgp-hijacking/){:target="_blank" rel="noopener"}

> The Dutch government will adopt the RPKI (Resource Public Key Infrastructure) standard on all its systems before the end of 2024 to upgrade the security of its internet routing. [...]
