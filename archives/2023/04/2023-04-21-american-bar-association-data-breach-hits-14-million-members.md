Title: American Bar Association data breach hits 1.4 million members
Date: 2023-04-21T09:56:10-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-04-21-american-bar-association-data-breach-hits-14-million-members

[Source](https://www.bleepingcomputer.com/news/security/american-bar-association-data-breach-hits-14-million-members/){:target="_blank" rel="noopener"}

> The American Bar Association (ABA) has suffered a data breach after hackers compromised its network and gained access to older credentials for 1,466,000 members. [...]
