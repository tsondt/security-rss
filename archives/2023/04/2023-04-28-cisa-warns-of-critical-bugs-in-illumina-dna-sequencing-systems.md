Title: CISA warns of critical bugs in Illumina DNA sequencing systems
Date: 2023-04-28T10:40:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-04-28-cisa-warns-of-critical-bugs-in-illumina-dna-sequencing-systems

[Source](https://www.bleepingcomputer.com/news/security/cisa-warns-of-critical-bugs-in-illumina-dna-sequencing-systems/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity Infrastructure Security Agency (CISA) and the FDA have issued an urgent alert about two vulnerabilities that impact Illumina's Universal Copy Service (UCS), used for DNA sequencing in medical facilities and labs worldwide. [...]
