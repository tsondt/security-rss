Title: Capita has 'evidence' customer data was stolen in digital burglary
Date: 2023-04-20T13:29:14+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-04-20-capita-has-evidence-customer-data-was-stolen-in-digital-burglary

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/20/capita_admits_to_evidence_that/){:target="_blank" rel="noopener"}

> Admits criminals accessed 4% of servers from March 22 until it spotted them at month-end Business process outsourcing and tech services player Capita says there is proof that some customer data was scooped up by cyber baddies that broke into its systems late last month.... [...]
