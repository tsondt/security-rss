Title: Capita IT breach gets worse as Black Basta claims it's now selling off stolen data
Date: 2023-04-18T07:25:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-18-capita-it-breach-gets-worse-as-black-basta-claims-its-now-selling-off-stolen-data

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/18/capita_breach_gets_worse/){:target="_blank" rel="noopener"}

> No worries, outsourcer only handles government tech contracts worth billions Black Basta, the extortionists who claimed they were the ones who lately broke into Capita, have reportedly put up for sale sensitive details, including bank account information, addresses, and passport photos, stolen from the IT outsourcing giant.... [...]
