Title: AWS Security Profile – Cryptography Edition: Panos Kampanakis, Principal Security Engineer
Date: 2023-04-17T21:09:10+00:00
Author: Roger Park
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Thought Leadership;AWS Security Profile;AWS Security Profiles;cryptography;Data protection;post-quantum cryptography;provable security;Security Blog
Slug: 2023-04-17-aws-security-profile-cryptography-edition-panos-kampanakis-principal-security-engineer

[Source](https://aws.amazon.com/blogs/security/aws-security-profile-panos-kampanakis/){:target="_blank" rel="noopener"}

> In the AWS Security Profile — Cryptography Edition series, we interview Amazon Web Services (AWS) thought leaders who help keep our customers safe and secure. This interview features Panos Kampanakis, Principal Security Engineer, AWS Cryptography. Panos shares thoughts on data protection, cloud security, post-quantum cryptography, and more. What do you do in your current role [...]
