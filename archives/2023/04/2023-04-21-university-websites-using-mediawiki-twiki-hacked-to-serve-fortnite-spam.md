Title: University websites using MediaWiki, TWiki hacked to serve Fortnite spam
Date: 2023-04-21T04:35:09-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-04-21-university-websites-using-mediawiki-twiki-hacked-to-serve-fortnite-spam

[Source](https://www.bleepingcomputer.com/news/security/university-websites-using-mediawiki-twiki-hacked-to-serve-fortnite-spam/){:target="_blank" rel="noopener"}

> Websites of multiple U.S. universities are serving Fortnite and 'gift card' spam. Researchers observed Wiki and documentation pages being hosted by universities including Stanford, MIT, Berkeley, UMass Amherst, Northeastern, Caltech, among others, were compromised. [...]
