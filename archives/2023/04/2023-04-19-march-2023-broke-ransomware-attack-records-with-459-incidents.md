Title: March 2023 broke ransomware attack records with 459 incidents
Date: 2023-04-19T03:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-19-march-2023-broke-ransomware-attack-records-with-459-incidents

[Source](https://www.bleepingcomputer.com/news/security/march-2023-broke-ransomware-attack-records-with-459-incidents/){:target="_blank" rel="noopener"}

> March 2023 was the most prolific month recorded by cybersecurity analysts in recent years, measuring 459 attacks, an increase of 91% from the previous month and 62% compared to March 2022. [...]
