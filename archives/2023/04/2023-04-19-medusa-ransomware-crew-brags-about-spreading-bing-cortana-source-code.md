Title: Medusa ransomware crew brags about spreading Bing, Cortana source code
Date: 2023-04-19T23:12:28+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-19-medusa-ransomware-crew-brags-about-spreading-bing-cortana-source-code

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/19/medusa_microsoft_data_dump/){:target="_blank" rel="noopener"}

> 'Does have a somewhat Lapsus$ish feel' we're told The Medusa ransomware gang has put online what it claims is a massive leak of internal Microsoft materials, including Bing and Cortana source code.... [...]
