Title: US seizes $112 million from cryptocurrency investment scammers
Date: 2023-04-03T15:10:31-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-04-03-us-seizes-112-million-from-cryptocurrency-investment-scammers

[Source](https://www.bleepingcomputer.com/news/security/us-seizes-112-million-from-cryptocurrency-investment-scammers/){:target="_blank" rel="noopener"}

> Today, the U.S. Department of Justice seized six virtual currency accounts containing over $112 million in funds stolen in cryptocurrency investment schemes. [...]
