Title: How to secure content production on Google Cloud with CSAP
Date: 2023-04-14T16:00:00+00:00
Author: Buzz Hays
Category: GCP Security
Tags: Media & Entertainment;Security & Identity
Slug: 2023-04-14-how-to-secure-content-production-on-google-cloud-with-csap

[Source](https://cloud.google.com/blog/products/identity-security/securing-content-production-on-google-cloud-with-csap/){:target="_blank" rel="noopener"}

> Content production is increasingly happening in the cloud. While moving a creative process that spans three centuries into the modern era brings challenges and risks, MovieLabs is one organization that takes digital transformation seriously. A nonprofit research and development group founded by a consortium of the major Hollywood studios, MovieLabs describes their mission as “exploring innovative solutions to industry challenges shared by both our member studios and the broader production, post-production and distribution ecosystem.” aside_block [StructValue([(u'title', u'Hear monthly from our Cloud CISO in your inbox'), (u'body', <wagtail.wagtailcore.rich_text.RichText object at 0x3e4ba5f6c710>), (u'btn_text', u'Subscribe today'), (u'href', u'https://go.chronicle.security/cloudciso-newsletter-signup?utm_source=cgc-blog&utm_medium=blog&utm_campaign=FY23-Cloud-CISO-Perspectives-newsletter-blog-embed-CTA&utm_content=-&utm_term=-'), (u'image', <GAEImage: gcat small.jpg>)])] In [...]
