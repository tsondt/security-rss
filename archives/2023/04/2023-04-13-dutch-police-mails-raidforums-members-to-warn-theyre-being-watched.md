Title: Dutch Police mails RaidForums members to warn they’re being watched
Date: 2023-04-13T12:42:40-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-04-13-dutch-police-mails-raidforums-members-to-warn-theyre-being-watched

[Source](https://www.bleepingcomputer.com/news/security/dutch-police-mails-raidforums-members-to-warn-theyre-being-watched/){:target="_blank" rel="noopener"}

> Dutch Police is sending emails to former RaidForums members, asking them to delete stolen data and stop illegal cyber activities and warning that they are not anonymous. [...]
