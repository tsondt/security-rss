Title: Microsoft April 2023 Patch Tuesday fixes 1 zero-day, 97 flaws
Date: 2023-04-11T13:28:06-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-04-11-microsoft-april-2023-patch-tuesday-fixes-1-zero-day-97-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-april-2023-patch-tuesday-fixes-1-zero-day-97-flaws/){:target="_blank" rel="noopener"}

> ​Today is Microsoft's April 2023 Patch Tuesday, and security updates fix one actively exploited zero-day vulnerability and a total of 97 flaws. [...]
