Title: Cops cuff teenage 'Robin Hood hacker' suspected of peddling stolen info
Date: 2023-04-06T07:33:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-06-cops-cuff-teenage-robin-hood-hacker-suspected-of-peddling-stolen-info

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/06/spanish_cops_arrest_alcasec/){:target="_blank" rel="noopener"}

> Luxury cars and designer duds don't seem very prince of thieves Spanish cops have arrested a 19-year-old suspected of stealing records belonging to half a million taxpayers and developing a database to sell stolen information to other cyber criminals.... [...]
