Title: Yellow Pages Canada confirms cyber attack as Black Basta leaks data
Date: 2023-04-24T03:22:54-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-04-24-yellow-pages-canada-confirms-cyber-attack-as-black-basta-leaks-data

[Source](https://www.bleepingcomputer.com/news/security/yellow-pages-canada-confirms-cyber-attack-as-black-basta-leaks-data/){:target="_blank" rel="noopener"}

> Yellow Pages Group, a Canadian directory publisher has confirmed to BleepingComputer that it has been hit by a cyber attack. Black Basta ransomware and extortion gang claims responsibility for the attack and has posted sensitive documents and data over the weekend. [...]
