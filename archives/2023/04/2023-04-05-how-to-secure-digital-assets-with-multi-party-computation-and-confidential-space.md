Title: How to secure digital assets with multi-party computation and Confidential Space
Date: 2023-04-05T16:00:00+00:00
Author: Bertrand Portier
Category: GCP Security
Tags: Security & Identity
Slug: 2023-04-05-how-to-secure-digital-assets-with-multi-party-computation-and-confidential-space

[Source](https://cloud.google.com/blog/products/identity-security/how-to-secure-digital-assets-with-multi-party-computation-and-confidential-space/){:target="_blank" rel="noopener"}

> In a previous blog post, we introduced how multi-party computation (MPC) can help reduce risk from single points of compromise and can facilitate instant, policy-compliant digital asset transactions. One of the quickest and easiest ways companies can implement MPC solutions is with Google Cloud's Confidential Space. This solution offers the benefit of being able to hold the assets online for real-time transactions, and allows multiple parties to collaborate in the transaction in a privacy-preserving and policy-compliant manner. Today, we describe a reference architecture for implementing MPC-compliant blockchain signing using Confidential Space. To illustrate the architecture, let’s imagine Company A, which [...]
