Title: iPhones hacked via invisible calendar invites to drop QuaDream spyware
Date: 2023-04-11T13:46:43-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-04-11-iphones-hacked-via-invisible-calendar-invites-to-drop-quadream-spyware

[Source](https://www.bleepingcomputer.com/news/security/iphones-hacked-via-invisible-calendar-invites-to-drop-quadream-spyware/){:target="_blank" rel="noopener"}

> Microsoft and Citizen Lab discovered commercial spyware made by an Israel-based company QuaDream used to compromise the iPhones of high-risk individuals using a zero-click exploit named ENDOFDAYS. [...]
