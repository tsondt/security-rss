Title: UK Runs Fake DDoS-for-Hire Sites
Date: 2023-04-03T11:05:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybercrime;denial of service;UK
Slug: 2023-04-03-uk-runs-fake-ddos-for-hire-sites

[Source](https://www.schneier.com/blog/archives/2023/04/uk-runs-fake-ddos-for-hire-sites.html){:target="_blank" rel="noopener"}

> Brian Krebs is reporting that the UK’s National Crime Agency is setting up fake DDoS-for-hire sites as part of a sting operation: The NCA says all of its fake so-called “booter” or “stresser” sites -­ which have so far been accessed by several thousand people—have been created to look like they offer the tools and services that enable cyber criminals to execute these attacks. “However, after users register, rather than being given access to cyber crime tools, their data is collated by investigators,” reads an NCA advisory on the program. “Users based in the UK will be contacted by the [...]
