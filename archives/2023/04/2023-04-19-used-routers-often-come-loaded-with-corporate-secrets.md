Title: Used routers often come loaded with corporate secrets
Date: 2023-04-19T13:28:33+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;hardware;routers;syndication
Slug: 2023-04-19-used-routers-often-come-loaded-with-corporate-secrets

[Source](https://arstechnica.com/?p=1932973){:target="_blank" rel="noopener"}

> Enlarge (credit: aquatarkus/Getty Images) You know that you're supposed to wipe your smartphone or laptop before you resell it or give it to your cousin. After all, there's a lot of valuable personal data on there that should stay in your control. Businesses and other institutions need to take the same approach, deleting their information from PCs, servers, and network equipment so it doesn't fall into the wrong hands. At the RSA security conference in San Francisco next week, though, researchers from the security firm ESET will present findings showing that more than half of secondhand enterprise routers they bought [...]
