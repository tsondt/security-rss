Title: Scaling security and compliance
Date: 2023-04-18T15:53:57+00:00
Author: Chad Woolf
Category: AWS Security
Tags: Best Practices;Foundational (100);Security, Identity, & Compliance;Thought Leadership;Auditing;Cloud Audit Academy;Compliance reports;Security;Security Blog
Slug: 2023-04-18-scaling-security-and-compliance

[Source](https://aws.amazon.com/blogs/security/scaling-security-and-compliance/){:target="_blank" rel="noopener"}

> At Amazon Web Services (AWS), we move fast and continually iterate to meet the evolving needs of our customers. We design services that can help our customers meet even the most stringent security and compliance requirements. Additionally, our service teams work closely with our AWS Security Guardians program to coordinate security efforts and to maintain [...]
