Title: Hackers start abusing Action1 RMM in ransomware attacks
Date: 2023-04-15T12:45:23-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-04-15-hackers-start-abusing-action1-rmm-in-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/hackers-start-abusing-action1-rmm-in-ransomware-attacks/){:target="_blank" rel="noopener"}

> Security researchers are warning that cybercriminals are increasingly using the Action1 remote access software for persistence on compromised networks and to execute commands, scripts, and binaries. [...]
