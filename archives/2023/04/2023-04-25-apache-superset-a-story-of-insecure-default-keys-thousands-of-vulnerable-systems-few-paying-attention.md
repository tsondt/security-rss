Title: Apache Superset: A story of insecure default keys, thousands of vulnerable systems, few paying attention
Date: 2023-04-25T22:35:29+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-04-25-apache-superset-a-story-of-insecure-default-keys-thousands-of-vulnerable-systems-few-paying-attention

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/25/apache_superset_cve/){:target="_blank" rel="noopener"}

> Two out of three public-facing app instances open to hijacking Apache Superset until earlier this year shipped with an insecure default configuration that miscreants could exploit to login and take over the data visualization application, steal data, and execute malicious code.... [...]
