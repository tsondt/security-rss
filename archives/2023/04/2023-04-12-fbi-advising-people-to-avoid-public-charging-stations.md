Title: FBI Advising People to Avoid Public Charging Stations
Date: 2023-04-12T11:11:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cell phones;FBI;malware;smartphones;USB
Slug: 2023-04-12-fbi-advising-people-to-avoid-public-charging-stations

[Source](https://www.schneier.com/blog/archives/2023/04/fbi-advising-people-to-avoid-public-charging-stations.html){:target="_blank" rel="noopener"}

> The FBI is warning people against using public phone-charging stations, worrying that the combination power-data port can be used to inject malware onto the devices: Avoid using free charging stations in airports, hotels, or shopping centers. Bad actors have figured out ways to use public USB ports to introduce malware and monitoring software onto devices that access these ports. Carry your own charger and USB cord and use an electrical outlet instead. How much of a risk is this, really? I am unconvinced, although I do carry a USB condom for charging stations I find suspicious. News article. [...]
