Title: UK cyber-argency warns of a new ‘class’ of Russian hackers
Date: 2023-04-19T12:57:46-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-19-uk-cyber-argency-warns-of-a-new-class-of-russian-hackers

[Source](https://www.bleepingcomputer.com/news/security/uk-cyber-argency-warns-of-a-new-class-of-russian-hackers/){:target="_blank" rel="noopener"}

> The United Kingdom's NCSC (National Cyber Security Centre) is warning of a heightened risk from attacks by state-aligned Russian hacktivists, urging all organizations in the country to apply recommended security measures. [...]
