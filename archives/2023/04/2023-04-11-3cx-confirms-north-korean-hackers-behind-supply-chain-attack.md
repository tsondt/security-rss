Title: 3CX confirms North Korean hackers behind supply chain attack
Date: 2023-04-11T12:08:45-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-04-11-3cx-confirms-north-korean-hackers-behind-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/3cx-confirms-north-korean-hackers-behind-supply-chain-attack/){:target="_blank" rel="noopener"}

> VoIP communications company 3CX confirmed today that a North Korean hacking group was behind last month's supply chain attack. [...]
