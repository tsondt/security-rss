Title: The Week in Ransomware - April 28th 2023 - Clop at it again
Date: 2023-04-28T14:42:39-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-04-28-the-week-in-ransomware-april-28th-2023-clop-at-it-again

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-april-28th-2023-clop-at-it-again/){:target="_blank" rel="noopener"}

> It has been a very quiet week for ransomware news, with only a few reports released and not much info about cyberattacks. An item of interest was Microsoft linking the recent PaperCut server attacks on the Clop and LockBit ransomware operation. [...]
