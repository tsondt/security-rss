Title: Microsoft, Fortra are this fed up with cyber-gangs abusing Cobalt Strike
Date: 2023-04-10T16:29:05+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-04-10-microsoft-fortra-are-this-fed-up-with-cyber-gangs-abusing-cobalt-strike

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/10/microsoft_fortra_cobalt_strike/){:target="_blank" rel="noopener"}

> Oh, sure, let's play a game of legal and technical whack-a-mole Microsoft and Fortra are taking legal and technical actions to thwart cyber-criminals from using the latter company's Cobalt Strike software to distribute malware.... [...]
