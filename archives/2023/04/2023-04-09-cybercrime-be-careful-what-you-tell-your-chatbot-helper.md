Title: Cybercrime: be careful what you tell your chatbot helper…
Date: 2023-04-09T11:00:49+00:00
Author: Kate O'Flaherty
Category: The Guardian
Tags: Chatbots;Artificial intelligence (AI);Cybercrime;Data and computer security;Computing;Technology;Internet
Slug: 2023-04-09-cybercrime-be-careful-what-you-tell-your-chatbot-helper

[Source](https://www.theguardian.com/technology/2023/apr/09/cybercrime-chatbot-privacy-security-helper-chatgpt-google-bard-microsoft-bing-chat){:target="_blank" rel="noopener"}

> Alluring and useful they may be, but the AI interfaces’ potential as gateways for fraud and intrusive data gathering is huge – and is only set to grow Concerns about the growing abilities of chatbots trained on large language models, such as OpenAI’s GPT-4, Google’s Bard and Microsoft’s Bing Chat, are making headlines. Experts warn of their ability to spread misinformation on a monumental scale, as well as the existential risk their development may pose to humanity. As if this isn’t worrying enough, a third area of concern has opened up – illustrated by Italy’s recent ban of ChatGPT on [...]
