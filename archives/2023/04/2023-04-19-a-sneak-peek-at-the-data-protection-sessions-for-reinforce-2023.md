Title: A sneak peek at the data protection sessions for re:Inforce 2023
Date: 2023-04-19T19:11:33+00:00
Author: Katie Collins
Category: AWS Security
Tags: Announcements;AWS re:Inforce;Events;Foundational (100);Security, Identity, & Compliance;Data protection;Live Events;re:Inforce;re:Inforce 2023;Security Blog
Slug: 2023-04-19-a-sneak-peek-at-the-data-protection-sessions-for-reinforce-2023

[Source](https://aws.amazon.com/blogs/security/a-sneak-peek-at-the-data-protection-sessions-for-reinforce-2023/){:target="_blank" rel="noopener"}

> A full conference pass is $1,099. Register today with the code secure150off to receive a limited time $150 discount, while supplies last. AWS re:Inforce is fast approaching, and this post can help you plan your agenda. AWS re:Inforce is a security learning conference where you can gain skills and confidence in cloud security, compliance, identity, [...]
