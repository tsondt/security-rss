Title: Announcing Firewall Insights support for firewall policies and trend-based analysis
Date: 2023-04-07T16:00:00+00:00
Author: Tracy Jiang
Category: GCP Security
Tags: Security & Identity
Slug: 2023-04-07-announcing-firewall-insights-support-for-firewall-policies-and-trend-based-analysis

[Source](https://cloud.google.com/blog/products/identity-security/announcing-firewall-insights-support-for-firewall-policies/){:target="_blank" rel="noopener"}

> Firewall Insights helps you understand and optimize your Cloud Firewall rules by providing insights, recommendations, and metrics about how your firewall rules are being used. We are excited to announce new enhancements for Firewall Insights that support hierarchical firewall policies and network firewall policies. These enhancements are now generally available to all customers. Previously, Firewall Insights provided support for Virtual Private Cloud (VPC) firewall rules. The latest release provides recommendations to optimize your hierarchical firewall policy and network firewall policy configuration in addition to VPC firewall rules. Firewall Insights can assist your migration from VPC firewall rules to network firewall [...]
