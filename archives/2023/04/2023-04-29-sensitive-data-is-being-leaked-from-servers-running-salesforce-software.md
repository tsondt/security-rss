Title: Sensitive data is being leaked from servers running Salesforce software
Date: 2023-04-29T00:39:52+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;privacy;Salesforce
Slug: 2023-04-29-sensitive-data-is-being-leaked-from-servers-running-salesforce-software

[Source](https://arstechnica.com/?p=1935543){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) Servers running software sold by Salesforce are leaking sensitive data managed by government agencies, banks, and other organizations, according to a post published Friday by KrebsOnSecurity. At least five separate sites run by the state of Vermont permitted access to sensitive data to anyone, Brian Krebs reported. The state’s Pandemic Unemployment Assistance program was among those affected. It exposed applicants’ full names, Social Security numbers, addresses, phone numbers, email addresses, and bank account numbers. Like the other organizations providing public access to private data, Vermont used Salesforce Community, a cloud-based software product designed to make [...]
