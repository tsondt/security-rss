Title: Hey Siri, use this ultrasound attack to disarm a smart-home system
Date: 2023-04-04T00:59:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-04-hey-siri-use-this-ultrasound-attack-to-disarm-a-smart-home-system

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/04/siri_alexa_cortana_google_nuit/){:target="_blank" rel="noopener"}

> We speak to the boffins behind latest trick to fool Google Assistant, Cortana, Alexa Academics in the US have developed an attack dubbed NUIT, for Near-Ultrasound Inaudible Trojan, that exploits vulnerabilities in smart device microphones and voice assistants to silently and remotely access smart phones and home devices.... [...]
