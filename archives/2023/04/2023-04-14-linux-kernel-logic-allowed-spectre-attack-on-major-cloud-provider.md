Title: Linux kernel logic allowed Spectre attack on 'major cloud provider'
Date: 2023-04-14T06:27:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-04-14-linux-kernel-logic-allowed-spectre-attack-on-major-cloud-provider

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/14/linux_kernel_spectre_flaw_fixed/){:target="_blank" rel="noopener"}

> Kernel 6.2 ditched a useful defense against ghostly chip design flaw The Spectre vulnerability that has haunted hardware and software makers since 2018 continues to defy efforts to bury it.... [...]
