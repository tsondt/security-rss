Title: Bank rewrote ads for infosec jobs to stop scaring away women
Date: 2023-04-04T05:37:20+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-04-04-bank-rewrote-ads-for-infosec-jobs-to-stop-scaring-away-women

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/04/westpac_rewrote_inosec_job_ads_for_women/){:target="_blank" rel="noopener"}

> Blokes happily bluffed; women played it by the book, leaving the bank struggling to hire Australia's Westpac bank re-wrote its job ads for infosec roles after finding the language it used deterred female candidates.... [...]
