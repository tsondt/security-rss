Title: Vietnam threatens to cut off two million mobile subscribers
Date: 2023-04-03T04:33:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-04-03-vietnam-threatens-to-cut-off-two-million-mobile-subscribers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/03/two_million_mobile_numbers_at/){:target="_blank" rel="noopener"}

> To scupper scams, account-holders must hand over personal info or else Almost two million mobile phone subscribers in Vietnam are at risk of having their services severed, thanks to a new government policy that seeks to curb spam.... [...]
