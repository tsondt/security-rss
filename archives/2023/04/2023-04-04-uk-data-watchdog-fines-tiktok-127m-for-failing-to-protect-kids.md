Title: UK data watchdog fines TikTok £12.7M for failing to protect kids
Date: 2023-04-04T13:42:14+00:00
Author: Richard Currie
Category: The Register
Tags: 
Slug: 2023-04-04-uk-data-watchdog-fines-tiktok-127m-for-failing-to-protect-kids

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/04/uk_watchdog_fines_tiktok/){:target="_blank" rel="noopener"}

> Some 1.4 million under-13s used the app in 2020 by the ICO's estimates Fresh off the back of an embarrassing "grilling" by US Congress on national security grounds, TikTok has received a more concrete reprimand from the UK's Information Commissioner's Office (ICO) – a fine of £12.7 million ($15.8 million) for "misusing children's data."... [...]
