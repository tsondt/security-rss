Title: Russia accuses NATO of launching 5,000 cyberattacks since 2022
Date: 2023-04-14T12:19:05-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-14-russia-accuses-nato-of-launching-5000-cyberattacks-since-2022

[Source](https://www.bleepingcomputer.com/news/security/russia-accuses-nato-of-launching-5-000-cyberattacks-since-2022/){:target="_blank" rel="noopener"}

> The Federal Security Service of the Russian Federation (FSB) has accused the United States and other NATO countries of launching over 5,000 cyberattacks against critical infrastructure in the country since the beginning of 2022. [...]
