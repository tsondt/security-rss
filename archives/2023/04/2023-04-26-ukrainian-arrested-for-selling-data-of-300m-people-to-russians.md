Title: Ukrainian arrested for selling data of 300M people to Russians
Date: 2023-04-26T17:35:05-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-04-26-ukrainian-arrested-for-selling-data-of-300m-people-to-russians

[Source](https://www.bleepingcomputer.com/news/security/ukrainian-arrested-for-selling-data-of-300m-people-to-russians/){:target="_blank" rel="noopener"}

> The Ukrainian cyber police have arrested a 36-year-old man from the city of Netishyn for selling the personal data and sensitive information of over 300 million people, citizens of Ukraine, and various European countries. [...]
