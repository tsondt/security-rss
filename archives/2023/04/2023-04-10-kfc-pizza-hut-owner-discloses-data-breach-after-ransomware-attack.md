Title: KFC, Pizza Hut owner discloses data breach after ransomware attack
Date: 2023-04-10T14:23:40-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-04-10-kfc-pizza-hut-owner-discloses-data-breach-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/kfc-pizza-hut-owner-discloses-data-breach-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Yum! Brands, the brand owner of the KFC, Pizza Hut, and Taco Bell fast food chains, is now sending data breach notification letters to an undisclosed number of individuals whose personal information was stolen in a January 13 ransomware attack. [...]
