Title: Chinese scientists calculate the Milky Way's mass as 805 billion times that of our Sun
Date: 2023-04-24T00:29:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-04-24-chinese-scientists-calculate-the-milky-ways-mass-as-805-billion-times-that-of-our-sun

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/24/asia_tech_news_roundup/){:target="_blank" rel="noopener"}

> ALSO: Australia says offensive hacking is working; DJI hit with $279m patent suit; Philippines Police leak data; and more Asia In Brief Chinese scientists have estimated the mass of the Milky Way.... [...]
