Title: With ICMP magic, you can snoop on vulnerable HiSilicon, Qualcomm-powered Wi-Fi
Date: 2023-04-07T07:30:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-04-07-with-icmp-magic-you-can-snoop-on-vulnerable-hisilicon-qualcomm-powered-wi-fi

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/07/wifi_access_icmp/){:target="_blank" rel="noopener"}

> WPA stands for will-provide-access, if you can successfully exploit a target's setup A vulnerability identified in at least 55 Wi-Fi router models can be exploited by miscreants to spy on victims' data as it's sent over a wireless network.... [...]
