Title: FBI (and Others) Shut Down Genesis Market
Date: 2023-04-05T15:55:02+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;botnets;credentials;cybercrime;FBI;law enforcement
Slug: 2023-04-05-fbi-and-others-shut-down-genesis-market

[Source](https://www.schneier.com/blog/archives/2023/04/fbi-and-others-shut-down-genesis-market.html){:target="_blank" rel="noopener"}

> Genesis Market is shut down : Active since 2018, Genesis Market’s slogan was, “Our store sells bots with logs, cookies, and their real fingerprints.” Customers could search for infected systems with a variety of options, including by Internet address or by specific domain names associated with stolen credentials. But earlier today, multiple domains associated with Genesis had their homepages replaced with a seizure notice from the FBI, which said the domains were seized pursuant to a warrant issued by the U.S. District Court for the Eastern District of Wisconsin. The U.S. Attorney’s Office for the Eastern District of Wisconsin did [...]
