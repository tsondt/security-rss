Title: SD Worx shuts down UK payroll, HR services after cyberattack
Date: 2023-04-10T11:32:23-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-04-10-sd-worx-shuts-down-uk-payroll-hr-services-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/sd-worx-shuts-down-uk-payroll-hr-services-after-cyberattack/){:target="_blank" rel="noopener"}

> Belgian HR and payroll giant SD Worx has suffered a cyberattack causing them to shut down all IT systems for its UK and Ireland services. [...]
