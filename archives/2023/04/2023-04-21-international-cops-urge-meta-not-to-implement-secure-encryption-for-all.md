Title: International cops urge Meta <em>not</em> to implement secure encryption for all
Date: 2023-04-21T10:28:43+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-04-21-international-cops-urge-meta-not-to-implement-secure-encryption-for-all

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/21/meta_encryption_police/){:target="_blank" rel="noopener"}

> Why? Well, think of the children, of course An international group of law enforcement agencies are urging Meta not to standardize end-to-end encryption on Facebook Messenger and Instagram, which they say will harm their ability to fight child sexual abuse material (CSAM) online.... [...]
