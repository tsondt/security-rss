Title: Investigate security events by using AWS CloudTrail Lake advanced queries
Date: 2023-04-13T18:29:17+00:00
Author: Rodrigo Ferroni
Category: AWS Security
Tags: AWS CloudTrail;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security;Security Blog
Slug: 2023-04-13-investigate-security-events-by-using-aws-cloudtrail-lake-advanced-queries

[Source](https://aws.amazon.com/blogs/security/investigate-security-events-by-using-aws-cloudtrail-lake-advanced-queries/){:target="_blank" rel="noopener"}

> This blog post shows you how to use AWS CloudTrail Lake capabilities to investigate CloudTrail activity across AWS Organizations in response to a security incident scenario. We will walk you through two security-related scenarios while we investigate CloudTrail activity. The method described in this post will help you with the investigation process, allowing you to [...]
