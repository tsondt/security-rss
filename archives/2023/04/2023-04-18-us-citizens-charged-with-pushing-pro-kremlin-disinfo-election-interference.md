Title: US citizens charged with pushing pro-Kremlin disinfo, election interference
Date: 2023-04-18T23:35:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-04-18-us-citizens-charged-with-pushing-pro-kremlin-disinfo-election-interference

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/18/americans_russians_doj_disinfomration/){:target="_blank" rel="noopener"}

> Also a bunch of Russians plus someone giving free trips to the Motherland Four US citizens have been accused of working on behalf of the Russian government to push pro-Kremlin propaganda and unduly influence elections in Florida.... [...]
