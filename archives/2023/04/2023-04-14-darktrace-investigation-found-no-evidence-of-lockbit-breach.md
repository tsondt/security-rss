Title: Darktrace: Investigation found no evidence of LockBit breach
Date: 2023-04-14T13:29:28-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-04-14-darktrace-investigation-found-no-evidence-of-lockbit-breach

[Source](https://www.bleepingcomputer.com/news/security/darktrace-investigation-found-no-evidence-of-lockbit-breach/){:target="_blank" rel="noopener"}

> Cybersecurity firm Darktrace says it found no evidence that the LockBit ransomware gang breached its network after the group added an entry to their dark web leak platform, implying that they stole data from the company's systems. [...]
