Title: Notorious stolen credential warehouse Genesis Market seized by FBI
Date: 2023-04-05T06:30:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-04-05-notorious-stolen-credential-warehouse-genesis-market-seized-by-fbi

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/05/fbi_seizes_stolen_data_mart/){:target="_blank" rel="noopener"}

> Operation Cookie Monster crumbles stolen data-as-a-service vendor A notorious source of stolen credentials, genesis.market, has had its website seized by the FBI.... [...]
