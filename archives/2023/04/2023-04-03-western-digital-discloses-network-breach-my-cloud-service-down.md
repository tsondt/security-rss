Title: Western Digital discloses network breach, My Cloud service down
Date: 2023-04-03T06:37:15-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-03-western-digital-discloses-network-breach-my-cloud-service-down

[Source](https://www.bleepingcomputer.com/news/security/western-digital-discloses-network-breach-my-cloud-service-down/){:target="_blank" rel="noopener"}

> Western Digital announced today that its network has been breached and an unauthorized party gained access to multiple company systems. [...]
