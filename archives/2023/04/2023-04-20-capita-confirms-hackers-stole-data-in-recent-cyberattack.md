Title: Capita confirms hackers stole data in recent cyberattack
Date: 2023-04-20T09:48:39-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-20-capita-confirms-hackers-stole-data-in-recent-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/capita-confirms-hackers-stole-data-in-recent-cyberattack/){:target="_blank" rel="noopener"}

> London-based professional outsourcing giant Capita has published an update on the cyber-incident that impacted it at the start of the month, now admitting that hackers exfiltrated data from its systems. [...]
