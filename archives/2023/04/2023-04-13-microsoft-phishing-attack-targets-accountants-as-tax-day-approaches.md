Title: Microsoft: Phishing attack targets accountants as Tax Day approaches
Date: 2023-04-13T18:21:11-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-04-13-microsoft-phishing-attack-targets-accountants-as-tax-day-approaches

[Source](https://www.bleepingcomputer.com/news/security/microsoft-phishing-attack-targets-accountants-as-tax-day-approaches/){:target="_blank" rel="noopener"}

> Microsoft is warning of a phishing campaign targeting accounting firms and tax preparers with remote access malware allowing initial access to corporate networks. [...]
