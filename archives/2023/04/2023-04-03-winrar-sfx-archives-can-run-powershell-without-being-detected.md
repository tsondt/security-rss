Title: WinRAR SFX archives can run PowerShell without being detected
Date: 2023-04-03T14:20:07-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-03-winrar-sfx-archives-can-run-powershell-without-being-detected

[Source](https://www.bleepingcomputer.com/news/security/winrar-sfx-archives-can-run-powershell-without-being-detected/){:target="_blank" rel="noopener"}

> Hackers are adding malicious functionality to WinRAR self-extracting archives that contain harmless decoy files, allowing them to plant backdoors without triggering the security agent on the target system. [...]
