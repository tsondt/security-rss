Title: Introducing AI-powered risk summaries in Security Command Center
Date: 2023-04-24T16:00:00+00:00
Author: Sarah Fender
Category: GCP Security
Tags: AI & Machine Learning;Security & Identity
Slug: 2023-04-24-introducing-ai-powered-risk-summaries-in-security-command-center

[Source](https://cloud.google.com/blog/products/identity-security/rsa-introducing-ai-powered-risk-summaries-security-command-center/){:target="_blank" rel="noopener"}

> Cloud security teams need expert guidance to protect against an increasing number of threats, and to help pinpoint where they are most at risk. They also need to make security issues understandable so that non-security specialists can help keep their organization safe. To meet both requirements, we will integrate Google Cloud Security AI Workbench with Security Command Center Premium, our security and risk management solution. Announced today at the RSA Conference in San Francisco, Security Command Center Premium with Security AI Workbench can provide operators with near-instant analysis of findings and possible attack paths, generated by a large language model [...]
