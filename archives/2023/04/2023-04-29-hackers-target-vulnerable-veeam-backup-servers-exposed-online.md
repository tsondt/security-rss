Title: Hackers target vulnerable Veeam backup servers exposed online
Date: 2023-04-29T10:41:16-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-29-hackers-target-vulnerable-veeam-backup-servers-exposed-online

[Source](https://www.bleepingcomputer.com/news/security/hackers-target-vulnerable-veeam-backup-servers-exposed-online/){:target="_blank" rel="noopener"}

> Veeam backup servers are being targeted by at least one group of threat actors known to work with multiple high-profile ransomware gangs. [...]
