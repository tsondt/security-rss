Title: Hacking the Layoff Process
Date: 2023-04-28T19:15:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;employment;hacking
Slug: 2023-04-28-hacking-the-layoff-process

[Source](https://www.schneier.com/blog/archives/2023/04/hacking-the-layoff-process.html){:target="_blank" rel="noopener"}

> My latest book, A Hacker’s Mind, is filled with stories about the rich and powerful hacking systems, but it was hard to find stories of the hacking by the less powerful. Here’s one I just found. An article on how layoffs at big companies work inadvertently suggests an employee hack to avoid being fired:...software performs a statistical analysis during terminations to see if certain groups are adversely affected, said such reviews can uncover other problems. On a list of layoff candidates, a company might find it is about to fire inadvertently an employee who previously opened a complaint against a [...]
