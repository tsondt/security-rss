Title: Using the iPhone Recovery Key to Lock Owners Out of Their iPhones
Date: 2023-04-21T14:19:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2023-04-21-using-the-iphone-recovery-key-to-lock-owners-out-of-their-iphones

[Source](https://www.schneier.com/blog/archives/2023/04/using-the-iphone-recovery-key-to-lock-owners-out-of-their-iphones.html){:target="_blank" rel="noopener"}

> This a good example of a security feature that can sometimes harm security: Apple introduced the optional recovery key in 2020 to protect users from online hackers. Users who turn on the recovery key, a unique 28-digit code, must provide it when they want to reset their Apple ID password. iPhone thieves with your passcode can flip on the recovery key and lock you out. And if you already have the recovery key enabled, they can easily generate a new one, which also locks you out. Apple’s policy gives users virtually no way back into their accounts without that recovery [...]
