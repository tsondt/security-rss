Title: HP to patch critical bug in LaserJet printers within 90 days
Date: 2023-04-04T18:46:37-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-04-hp-to-patch-critical-bug-in-laserjet-printers-within-90-days

[Source](https://www.bleepingcomputer.com/news/security/hp-to-patch-critical-bug-in-laserjet-printers-within-90-days/){:target="_blank" rel="noopener"}

> HP announced in a security bulletin this week that it would take up to 90 days to patch a critical-severity vulnerability that impacts the firmware of certain business-grade printers. [...]
