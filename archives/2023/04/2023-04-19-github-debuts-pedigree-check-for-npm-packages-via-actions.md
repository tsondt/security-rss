Title: GitHub debuts pedigree check for npm packages via Actions
Date: 2023-04-19T16:00:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-04-19-github-debuts-pedigree-check-for-npm-packages-via-actions

[Source](https://go.theregister.com/feed/www.theregister.com/2023/04/19/github_actions_npm_origins/){:target="_blank" rel="noopener"}

> Publishing provenance possibly prevents problems Developers who use GitHub Actions to build software packages for the npm registry can now add a command flag that will publish details about the code's origin.... [...]
