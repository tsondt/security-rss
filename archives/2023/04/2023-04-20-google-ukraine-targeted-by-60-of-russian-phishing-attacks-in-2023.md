Title: Google: Ukraine targeted by 60% of Russian phishing attacks in 2023
Date: 2023-04-20T14:47:35-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-04-20-google-ukraine-targeted-by-60-of-russian-phishing-attacks-in-2023

[Source](https://www.bleepingcomputer.com/news/security/google-ukraine-targeted-by-60-percent-of-russian-phishing-attacks-in-2023/){:target="_blank" rel="noopener"}

> Google's Threat Analysis Group (TAG) has been monitoring and disrupting Russian state-backed cyberattacks targeting Ukraine's critical infrastructure in 2023. [...]
