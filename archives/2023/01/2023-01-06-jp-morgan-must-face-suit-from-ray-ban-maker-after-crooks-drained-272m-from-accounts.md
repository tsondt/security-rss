Title: JP Morgan must face suit from Ray-Ban maker after crooks drained $272m from accounts
Date: 2023-01-06T09:32:10+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-01-06-jp-morgan-must-face-suit-from-ray-ban-maker-after-crooks-drained-272m-from-accounts

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/06/jp_morgan_lawsuit_essilor/){:target="_blank" rel="noopener"}

> Don't masquerade with the guy in shades, oh no A New York federal judge told JP Morgan Chase Bank this week that he would not toss a lawsuit accusing the bank of ignoring red flags when cybercrooks stole $272 million from the New York account of the company that makes Ray-Bans in 2019.... [...]
