Title: China aims to grow local infosec industry by 30 percent a year, to $22 billion by 2025
Date: 2023-01-16T01:59:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-01-16-china-aims-to-grow-local-infosec-industry-by-30-percent-a-year-to-22-billion-by-2025

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/16/china_infsec_industry_growth_plan/){:target="_blank" rel="noopener"}

> Optimistically suggests international collaboration – including on standards – will help it get there China's government has declared the nation's information security industry needs to grow – fast.... [...]
