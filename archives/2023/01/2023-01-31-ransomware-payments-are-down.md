Title: Ransomware Payments Are Down
Date: 2023-01-31T12:03:28+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;crime;cryptocurrency;cybercrime;extortion;ransomware
Slug: 2023-01-31-ransomware-payments-are-down

[Source](https://www.schneier.com/blog/archives/2023/01/ransomware-payments-are-down.html){:target="_blank" rel="noopener"}

> Chainalysis reports that worldwide ransomware payments were down in 2022. Ransomware attackers extorted at least $456.8 million from victims in 2022, down from $765.6 million the year before. As always, we have to caveat these findings by noting that the true totals are much higher, as there are cryptocurrency addresses controlled by ransomware attackers that have yet to be identified on the blockchain and incorporated into our data. When we published last year’s version of this report, for example, we had only identified $602 million in ransomware payments in 2021. Still, the trend is clear: Ransomware payments are significantly down. [...]
