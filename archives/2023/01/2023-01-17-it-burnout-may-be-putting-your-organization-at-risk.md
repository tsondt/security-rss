Title: IT Burnout may be Putting Your Organization at Risk
Date: 2023-01-17T10:06:12-05:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-01-17-it-burnout-may-be-putting-your-organization-at-risk

[Source](https://www.bleepingcomputer.com/news/security/it-burnout-may-be-putting-your-organization-at-risk/){:target="_blank" rel="noopener"}

> The heavy responsibility of securing organizations against cyber-attacks can be overwhelming for security professionals. There are concrete solutions to IT burnout that your organization should implement as soon as possible to mitigate the risks of burnout. [...]
