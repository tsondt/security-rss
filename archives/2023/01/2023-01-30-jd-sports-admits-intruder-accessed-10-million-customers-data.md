Title: JD Sports admits intruder accessed 10 million customers' data
Date: 2023-01-30T15:07:00+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-01-30-jd-sports-admits-intruder-accessed-10-million-customers-data

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/30/jd_sports_breach/){:target="_blank" rel="noopener"}

> No payment details exposed in breach, says retailer, but shoppers told to be 'vigilant about potential scams' Sports fashion retailer JD Sports has confirmed miscreants broke into a system that contained data on a whopping 10 million customers, but no payment information was among the mix.... [...]
