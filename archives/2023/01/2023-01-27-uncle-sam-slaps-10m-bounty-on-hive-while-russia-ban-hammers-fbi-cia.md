Title: Uncle Sam slaps $10m bounty on Hive while Russia ban-hammers FBI, CIA
Date: 2023-01-27T23:59:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-27-uncle-sam-slaps-10m-bounty-on-hive-while-russia-ban-hammers-fbi-cia

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/27/10m_hive_reward_russia/){:target="_blank" rel="noopener"}

> New meaning to sweetening the pot Uncle Sam has put up a $10 million reward for intel on Hive ransomware criminals' identities and whereabouts, while Russia has blocked the FBI and CIA websites, along with the Rewards for Justice site offering the bounty.... [...]
