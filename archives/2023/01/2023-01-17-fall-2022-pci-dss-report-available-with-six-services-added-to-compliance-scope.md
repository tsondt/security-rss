Title: Fall 2022 PCI DSS report available with six services added to compliance scope
Date: 2023-01-17T21:30:43+00:00
Author: Michael Oyeniya
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;Compliance;Compliance reports;PCI;PCI DSS;Security Blog
Slug: 2023-01-17-fall-2022-pci-dss-report-available-with-six-services-added-to-compliance-scope

[Source](https://aws.amazon.com/blogs/security/fall-2022-pci-dss-report-available-with-six-services-added-to-compliance-scope/){:target="_blank" rel="noopener"}

> We’re continuing to expand the scope of our assurance programs at Amazon Web Services (AWS) and are pleased to announce that six additional services have been added to the scope of our Payment Card Industry Data Security Standard (PCI DSS) certification. This provides our customers with more options to process and store their payment card [...]
