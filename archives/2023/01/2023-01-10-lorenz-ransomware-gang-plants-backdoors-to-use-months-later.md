Title: Lorenz ransomware gang plants backdoors to use months later
Date: 2023-01-10T16:30:56-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-01-10-lorenz-ransomware-gang-plants-backdoors-to-use-months-later

[Source](https://www.bleepingcomputer.com/news/security/lorenz-ransomware-gang-plants-backdoors-to-use-months-later/){:target="_blank" rel="noopener"}

> Security researchers are warning that patching critical vulnerabilities allowing access to the network is insufficient to defend against ransomware attacks. [...]
