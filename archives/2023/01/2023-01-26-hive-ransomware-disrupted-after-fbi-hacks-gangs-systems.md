Title: Hive ransomware disrupted after FBI hacks gang's systems
Date: 2023-01-26T10:14:55-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-01-26-hive-ransomware-disrupted-after-fbi-hacks-gangs-systems

[Source](https://www.bleepingcomputer.com/news/security/hive-ransomware-disrupted-after-fbi-hacks-gangs-systems/){:target="_blank" rel="noopener"}

> Today, the Hive ransomware Tor payment and data leak sites were seized as part of an international law enforcement operation involving the US Department of Justice, FBI, Secret Service, Europol, and Germany's BKA and Polizei. [...]
