Title: DER Entitlements: The (Brief) Return of the Psychic Paper
Date: 2023-01-12T08:59:00-08:00
Author: Google Project Zero (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2023-01-12-der-entitlements-the-brief-return-of-the-psychic-paper

[Source](https://googleprojectzero.blogspot.com/2023/01/der-entitlements-brief-return-of.html){:target="_blank" rel="noopener"}

> Posted by Ivan Fratric, Project Zero Note: The vulnerability discussed here, CVE-2022-42855, was fixed in iOS 15.7.2 and macOS Monterey 12.6.2. While the vulnerability did not appear to be exploitable on iOS 16 and macOS Ventura, iOS 16.2 and macOS Ventura 13.1 nevertheless shipped hardening changes related to it. Last year, I spent a lot of time researching the security of applications built on top of XMPP, an instant messaging protocol based on XML. More specifically, my research focused on how subtle quirks in XML parsing can be used to undermine the security of such applications. (If you are interested [...]
