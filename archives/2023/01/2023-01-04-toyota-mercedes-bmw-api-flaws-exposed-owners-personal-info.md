Title: Toyota, Mercedes, BMW API flaws exposed owners’ personal info
Date: 2023-01-04T11:05:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-04-toyota-mercedes-bmw-api-flaws-exposed-owners-personal-info

[Source](https://www.bleepingcomputer.com/news/security/toyota-mercedes-bmw-api-flaws-exposed-owners-personal-info/){:target="_blank" rel="noopener"}

> Security analysts disclosed severe API security flaws impacting numerous car makers, enabling them to access vehicle owner information, take over accounts, access internal systems, modify records, and track their position. [...]
