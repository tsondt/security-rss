Title: TikTok slapped with $5.4 million fine over cookie opt-out feature
Date: 2023-01-15T10:05:10-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-01-15-tiktok-slapped-with-54-million-fine-over-cookie-opt-out-feature

[Source](https://www.bleepingcomputer.com/news/security/tiktok-slapped-with-54-million-fine-over-cookie-opt-out-feature/){:target="_blank" rel="noopener"}

> France's data protection authority (CNIL) has fined TikTok UK and TikTok Ireland €5,000,000 for making it difficult for users of the platform to refuse cookies and for not sufficiently informing them about their purpose. [...]
