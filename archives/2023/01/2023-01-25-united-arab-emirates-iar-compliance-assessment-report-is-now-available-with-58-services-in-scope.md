Title: United Arab Emirates IAR compliance assessment report is now available with 58 services in scope
Date: 2023-01-25T21:11:10+00:00
Author: Ioana Mecu
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Abu Dhabi;Auditing;AWS security;Cloud Services Provider;Compliance;CSP;Dubai;IAR;Information Assurance Regulation;MEA;Middle East;Security;Security Blog;UAE
Slug: 2023-01-25-united-arab-emirates-iar-compliance-assessment-report-is-now-available-with-58-services-in-scope

[Source](https://aws.amazon.com/blogs/security/united-arab-emirates-iar-compliance-assessment-report-is-now-available-with-58-services-in-scope/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce the publication of our compliance assessment report on the Information Assurance Regulation (IAR) established by the Telecommunications and Digital Government Regulatory Authority (TDRA) of the United Arab Emirates. The report covers the AWS Middle East (UAE) Region, with 58 services in scope of the assessment. The IAR [...]
