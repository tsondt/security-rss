Title: US Supremes deny Pegasus spyware maker's immunity claim
Date: 2023-01-09T20:30:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-09-us-supremes-deny-pegasus-spyware-makers-immunity-claim

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/09/supreme_court_pegasus_spyware/){:target="_blank" rel="noopener"}

> NSO maintains that it's all legit The US Supreme Court has quashed spyware maker NSO Group's argument that it cannot be held legally responsible for using WhatsApp technology to deploy its Pegasus snoop-ware on users' phones.... [...]
