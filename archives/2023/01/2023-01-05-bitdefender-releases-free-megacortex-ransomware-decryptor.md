Title: Bitdefender releases free MegaCortex ransomware decryptor
Date: 2023-01-05T15:49:33-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-05-bitdefender-releases-free-megacortex-ransomware-decryptor

[Source](https://www.bleepingcomputer.com/news/security/bitdefender-releases-free-megacortex-ransomware-decryptor/){:target="_blank" rel="noopener"}

> Antivirus company Bitdefender has released a decryptor for the MegaCortex ransomware family, making it possible for victims of the once notorious gang to restore their data for free. [...]
