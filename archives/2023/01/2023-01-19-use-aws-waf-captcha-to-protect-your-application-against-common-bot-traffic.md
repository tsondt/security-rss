Title: Use AWS WAF CAPTCHA to protect your application against common bot traffic
Date: 2023-01-19T20:31:01+00:00
Author: Abhinav Bannerjee
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;bot;captcha;Security Blog;WAF
Slug: 2023-01-19-use-aws-waf-captcha-to-protect-your-application-against-common-bot-traffic

[Source](https://aws.amazon.com/blogs/security/use-aws-waf-captcha-to-protect-your-application-against-common-bot-traffic/){:target="_blank" rel="noopener"}

> In this blog post, you’ll learn how you can use a Completely Automated Public Turing test to tell Computers and Humans Apart (CAPTCHA) with other AWS WAF controls as part of a layered approach to provide comprehensive protection against bot traffic. We’ll describe a workflow that tracks the number of incoming requests to a site’s [...]
