Title: German cartel watchdog objects to the way Google processes user data
Date: 2023-01-11T16:15:08+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-01-11-german-cartel-watchdog-objects-to-the-way-google-processes-user-data

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/11/germany_google_data_complaint/){:target="_blank" rel="noopener"}

> Not transparent, not specific, and too easy to say yes to Google users don't have enough choice over whether – and to what extent – they agree to "far-reaching processing of their data across services," Germany's competition regulator says, adding that the tech giant should change its "data processing" terms and practices.... [...]
