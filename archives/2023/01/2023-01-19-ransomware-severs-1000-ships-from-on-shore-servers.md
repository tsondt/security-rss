Title: Ransomware severs 1,000 ships from on-shore servers
Date: 2023-01-19T11:01:15+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-01-19-ransomware-severs-1000-ships-from-on-shore-servers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/19/ransomware_attack_cuts_1000_ships/){:target="_blank" rel="noopener"}

> Get your eyepatch out: Cyber attacks on the high seas are trending A Norwegian maritime risk management business is getting a lesson in that very area, after a ransomware attack forced its ShipManager software offline and left 1,000 ships without a connection to on-shore servers.... [...]
