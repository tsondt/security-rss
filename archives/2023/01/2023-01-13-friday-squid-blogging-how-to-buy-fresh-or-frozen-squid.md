Title: Friday Squid Blogging: How to Buy Fresh or Frozen Squid
Date: 2023-01-13T22:08:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-01-13-friday-squid-blogging-how-to-buy-fresh-or-frozen-squid

[Source](https://www.schneier.com/blog/archives/2023/01/friday-squid-blogging-how-to-buy-fresh-or-frozen-squid.html){:target="_blank" rel="noopener"}

> Good advice on buying squid. I like to buy whole fresh squid and clean it myself. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
