Title: A widespread logic controller flaw raises the specter of Stuxnet
Date: 2023-01-11T19:41:27+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;cybersecurity;espionage;hacking;stuxnet;syndication
Slug: 2023-01-11-a-widespread-logic-controller-flaw-raises-the-specter-of-stuxnet

[Source](https://arstechnica.com/?p=1909188){:target="_blank" rel="noopener"}

> Enlarge In 2009, the computer worm Stuxnet crippled hundreds of centrifuges inside Iran’s Natanz uranium enrichment plant by targeting the software running on the facility’s industrial computers, known as programmable logic controllers. The exploited PLCs were made by the automation giant Siemens and were all models from the company’s ubiquitous, long-running SIMATIC S7 product series. Now, more than a decade later, Siemens disclosed today that a vulnerability in its S7-1500 series could be exploited by an attacker to silently install malicious firmware on the devices and take full control of them. The vulnerability was discovered by researchers at the embedded [...]
