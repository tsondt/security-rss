Title: Months after NSA disclosed Microsoft cert bug, datacenters remain unpatched
Date: 2023-01-26T02:07:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-26-months-after-nsa-disclosed-microsoft-cert-bug-datacenters-remain-unpatched

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/26/windows_cryptoapi_bug_akamai/){:target="_blank" rel="noopener"}

> You know when we all said quit using MD5? We really meant it Most Windows-powered datacenter systems and applications remain vulnerable to a spoofing bug in CryptoAPI that was disclosed by the NSA and the UK National Cyber Security Center (NCSC) and patched by Microsoft last year, according to Akamai's researchers.... [...]
