Title: Crims steal data on 40 million T-Mobile US customers
Date: 2023-01-20T01:33:40+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-01-20-crims-steal-data-on-40-million-t-mobile-us-customers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/20/t_mobile_us_data_breach/){:target="_blank" rel="noopener"}

> Sixth snafu in five years? Crooks have this useless carrier on speed dial T-Mobile US today said someone abused an API to download the personal information of 37 million subscribers.... [...]
