Title: Meet teler-waf: Security-focused HTTP middleware for the Go framework
Date: 2023-01-09T11:43:48+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-09-meet-teler-waf-security-focused-http-middleware-for-the-go-framework

[Source](https://portswigger.net/daily-swig/meet-teler-waf-security-focused-http-middleware-for-the-go-framework){:target="_blank" rel="noopener"}

> Protection against XSS, SQLi, and more web attacks for Go-based web applications [...]
