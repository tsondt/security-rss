Title: Google pays hacker duo $22k in bug bounties for flaws in multiple cloud projects
Date: 2023-01-19T16:29:29+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-19-google-pays-hacker-duo-22k-in-bug-bounties-for-flaws-in-multiple-cloud-projects

[Source](https://portswigger.net/daily-swig/google-pays-hacker-duo-22k-in-bug-bounties-for-flaws-in-multiple-cloud-projects){:target="_blank" rel="noopener"}

> Six payouts issued for bugs uncovered in Theia, Vertex AI, Compute Engine, and Cloud Workstations [...]
