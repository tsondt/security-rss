Title: U.S. sues Google for abusing dominance over online ad market
Date: 2023-01-24T14:03:22-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-24-us-sues-google-for-abusing-dominance-over-online-ad-market

[Source](https://www.bleepingcomputer.com/news/security/us-sues-google-for-abusing-dominance-over-online-ad-market/){:target="_blank" rel="noopener"}

> The U.S. Justice Department has filed a federal lawsuit today against Google for abusing its dominant position in the online advertising market. [...]
