Title: No more holidays for US telcos, FCC is cracking down
Date: 2023-01-08T10:01:15+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-01-08-no-more-holidays-for-us-telcos-fcc-is-cracking-down

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/08/in_brief_security/){:target="_blank" rel="noopener"}

> Also, LastPass faces class action, and Louisiana says that, while the internet may be for porn, ID is still required In Brief The Federal Communications Commission plans to overhaul its security reporting rules for the telecom industry to, among other things, eliminate a mandatory seven-day wait for informing customers of stolen data and expand the definition of what constitutes an incident.... [...]
