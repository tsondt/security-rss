Title: Bloke allegedly stole, sold private info belonging to 'tens of millions' globally
Date: 2023-01-26T07:34:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-26-bloke-allegedly-stole-sold-private-info-belonging-to-tens-of-millions-globally

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/26/crook_stole_tens_of_millions_private_data/){:target="_blank" rel="noopener"}

> If true, was it worth the $500k and prison jumpsuit? A man suspected of stealing personal data belonging to tens of millions of people worldwide and selling that info on cybercrime forums has been arrested by Dutch police.... [...]
