Title: FBI catches up with infosec and crypto communities, blames Lazarus Group for $100 million heist
Date: 2023-01-25T01:45:05+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-01-25-fbi-catches-up-with-infosec-and-crypto-communities-blames-lazarus-group-for-100-million-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/25/fbi_lazarus_harmony_crypto/){:target="_blank" rel="noopener"}

> Well played, feds. What's next? Ransomware is rampant? Strong passwords are important? The FBI has confirmed what cybersecurity researchers have been saying for months: the North Korean-sponsored Lazarus Group was behind the theft last year of $100 million in crypto assets from blockchain startup Harmony.... [...]
