Title: StrongPity hackers target Android users via trojanized Telegram app
Date: 2023-01-10T10:30:50-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-01-10-strongpity-hackers-target-android-users-via-trojanized-telegram-app

[Source](https://www.bleepingcomputer.com/news/security/strongpity-hackers-target-android-users-via-trojanized-telegram-app/){:target="_blank" rel="noopener"}

> The StrongPity APT hacking group is distributing a fake Shagle chat app that is a trojanized version of the Telegram for Android app with an added backdoor. [...]
