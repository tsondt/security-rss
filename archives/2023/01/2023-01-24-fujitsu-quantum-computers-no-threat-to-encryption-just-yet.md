Title: Fujitsu: Quantum computers no threat to encryption just yet
Date: 2023-01-24T19:47:11+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2023-01-24-fujitsu-quantum-computers-no-threat-to-encryption-just-yet

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/24/fujitsu_quantum_encryption/){:target="_blank" rel="noopener"}

> Heavily hyped tech bound for some sort of milestone by decade end Research conducted by Fujitsu suggests there is no need to panic about quantum computers being able to decode encrypted data – this is unlikely to happen in the near future, it claims.... [...]
