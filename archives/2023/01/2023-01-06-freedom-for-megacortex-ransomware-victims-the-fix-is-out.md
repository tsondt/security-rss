Title: Freedom for MegaCortex ransomware victims - the fix is out
Date: 2023-01-06T20:45:15+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-06-freedom-for-megacortex-ransomware-victims-the-fix-is-out

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/06/megacortex_ransomware_decryptor/){:target="_blank" rel="noopener"}

> Criminals hit 1,800 victims across 71 countries to the tune of $100m+ An international law enforcement effort has released a decryptor for victims of MegaCortex ransomware, widely used by cybercriminals to infect large corporations across 71 countries to the tune of more than $100 million in damages.... [...]
