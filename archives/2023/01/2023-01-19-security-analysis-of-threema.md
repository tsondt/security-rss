Title: Security Analysis of Threema
Date: 2023-01-19T12:21:31+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;authentication;cryptanalysis;encryption;side-channel attacks;threat models;vulnerabilities
Slug: 2023-01-19-security-analysis-of-threema

[Source](https://www.schneier.com/blog/archives/2023/01/security-analysis-of-threema.html){:target="_blank" rel="noopener"}

> A group of Swiss researchers have published an impressive security analysis of Threema. We provide an extensive cryptographic analysis of Threema, a Swiss-based encrypted messaging application with more than 10 million users and 7000 corporate customers. We present seven different attacks against the protocol in three different threat models. As one example, we present a cross-protocol attack which breaks authentication in Threema and which exploits the lack of proper key separation between different sub-protocols. As another, we demonstrate a compression-based side-channel attack that recovers users’ long-term private keys through observation of the size of Threema encrypted back-ups. We discuss remediations [...]
