Title: How to improve security incident investigations using Amazon Detective finding groups
Date: 2023-01-25T17:34:00+00:00
Author: Anna McAbee
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;Amazon Detective;Amazon GuardDuty;Cloud security;Incident response;Security Blog
Slug: 2023-01-25-how-to-improve-security-incident-investigations-using-amazon-detective-finding-groups

[Source](https://aws.amazon.com/blogs/security/how-to-improve-security-incident-investigations-using-amazon-detective-finding-groups/){:target="_blank" rel="noopener"}

> Uncovering the root cause of an Amazon GuardDuty finding can be a complex task, requiring security operations center (SOC) analysts to collect a variety of logs, correlate information across logs, and determine the full scope of affected resources. Sometimes you need to do this type of in-depth analysis because investigating individual security findings in insolation [...]
