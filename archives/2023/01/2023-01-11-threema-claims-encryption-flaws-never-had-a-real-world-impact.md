Title: Threema claims encryption flaws never had a real-world impact
Date: 2023-01-11T14:04:08-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2023-01-11-threema-claims-encryption-flaws-never-had-a-real-world-impact

[Source](https://www.bleepingcomputer.com/news/security/threema-claims-encryption-flaws-never-had-a-real-world-impact/){:target="_blank" rel="noopener"}

> A team of researchers from ETH Zurich has published a paper describing multiple security flaws in Threema, a secure end-to-end encrypted communications app. [...]
