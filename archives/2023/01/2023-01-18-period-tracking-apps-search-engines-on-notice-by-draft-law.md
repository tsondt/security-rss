Title: Period-tracking apps, search engines on notice by draft law
Date: 2023-01-18T18:31:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-18-period-tracking-apps-search-engines-on-notice-by-draft-law

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/18/washington_period_tracking_apps/){:target="_blank" rel="noopener"}

> And no more geofencing around health clinics either A bill proposed by Washingston state lawmakers would make it illegal for period-tracking apps, Google or any other website to sell consumers' health data while also making it harder for them to collect and share this personal information.... [...]
