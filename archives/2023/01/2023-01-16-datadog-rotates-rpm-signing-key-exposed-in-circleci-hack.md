Title: Datadog rotates RPM signing key exposed in CircleCI hack
Date: 2023-01-16T14:08:19-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-16-datadog-rotates-rpm-signing-key-exposed-in-circleci-hack

[Source](https://www.bleepingcomputer.com/news/security/datadog-rotates-rpm-signing-key-exposed-in-circleci-hack/){:target="_blank" rel="noopener"}

> Cloud security firm Datadog says that one of its RPM GPG signing keys and its passphrase have been exposed during a recent CircleCI security breach. [...]
