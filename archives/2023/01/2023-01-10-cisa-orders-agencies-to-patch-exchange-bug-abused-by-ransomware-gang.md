Title: CISA orders agencies to patch Exchange bug abused by ransomware gang
Date: 2023-01-10T18:22:48-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-10-cisa-orders-agencies-to-patch-exchange-bug-abused-by-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/cisa-orders-agencies-to-patch-exchange-bug-abused-by-ransomware-gang/){:target="_blank" rel="noopener"}

> The Cybersecurity and Infrastructure Security Agency (CISA) has added two more security vulnerabilities to its catalog of exploited bugs today. [...]
