Title: NSA asks Congress to let it get on with that warrantless data harvesting, again
Date: 2023-01-14T20:57:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-01-14-nsa-asks-congress-to-let-it-get-on-with-that-warrantless-data-harvesting-again

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/14/in_brief_security/){:target="_blank" rel="noopener"}

> Also: That Pokemon is actually a RAT, Uncle Sam fails a password audit In brief A US intelligence boss has asked Congress to reauthorize a controversial set of powers that give snoops warrantless authorization to surveil electronic communications in the name of fighting terrorism and so forth.... [...]
