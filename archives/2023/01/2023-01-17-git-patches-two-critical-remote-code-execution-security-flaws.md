Title: Git patches two critical remote code execution security flaws
Date: 2023-01-17T18:26:13-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-17-git-patches-two-critical-remote-code-execution-security-flaws

[Source](https://www.bleepingcomputer.com/news/security/git-patches-two-critical-remote-code-execution-security-flaws/){:target="_blank" rel="noopener"}

> Git has patched two critical severity security vulnerabilities that could allow attackers to execute arbitrary code after successfully exploiting heap-based buffer overflow weaknesses. [...]
