Title: Microsoft urges admins to patch on-premises Exchange servers
Date: 2023-01-26T18:02:41-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-01-26-microsoft-urges-admins-to-patch-on-premises-exchange-servers

[Source](https://www.bleepingcomputer.com/news/security/microsoft-urges-admins-to-patch-on-premises-exchange-servers/){:target="_blank" rel="noopener"}

> Microsoft urged customers today to keep their on-premises Exchange servers patched by applying the latest supported Cumulative Update (CU) to have them always ready to deploy an emergency security update. [...]
