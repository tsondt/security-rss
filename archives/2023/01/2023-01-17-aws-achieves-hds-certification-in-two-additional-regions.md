Title: AWS achieves HDS certification in two additional Regions
Date: 2023-01-17T16:24:08+00:00
Author: Janice Leung
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Auditing;AWS security;Security;Security Blog
Slug: 2023-01-17-aws-achieves-hds-certification-in-two-additional-regions

[Source](https://aws.amazon.com/blogs/security/aws-achieves-hds-certification-in-two-additional-regions/){:target="_blank" rel="noopener"}

> We’re excited to announce that two additional AWS Regions—Asia Pacific (Jakarta) and Europe (Milan)—have been granted the Health Data Hosting (Hébergeur de Données de Santé, HDS) certification. This alignment with HDS requirements demonstrates our continued commitment to adhere to the heightened expectations for cloud service providers. AWS customers who handle personal health data can use [...]
