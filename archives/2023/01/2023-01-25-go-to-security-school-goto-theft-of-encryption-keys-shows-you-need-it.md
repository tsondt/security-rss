Title: Go to security school, GoTo – theft of encryption keys shows you need it
Date: 2023-01-25T08:28:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-01-25-go-to-security-school-goto-theft-of-encryption-keys-shows-you-need-it

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/25/goto_security_incident_update/){:target="_blank" rel="noopener"}

> Ongoing investigation into cloud storage attack finds customer data exfiltrated Remote access outfit GoTo has admitted that a threat actor exfiltrated an encryption key that allowed access to "a portion" of encrypted backup files.... [...]
