Title: How to encrypt sensitive caller voice input in Amazon Lex
Date: 2023-01-16T21:56:58+00:00
Author: Herbert Guerrero
Category: AWS Security
Tags: Amazon Connect;Amazon Lex;Architecture;AWS Lambda;Intermediate (200);Security, Identity, & Compliance;Technical How-to;authentication;Connect;data encryption;Security Blog;Voice Input
Slug: 2023-01-16-how-to-encrypt-sensitive-caller-voice-input-in-amazon-lex

[Source](https://aws.amazon.com/blogs/security/how-to-encrypt-sensitive-caller-authentication-voice-input-in-amazon-lex/){:target="_blank" rel="noopener"}

> In the telecommunications industry, sensitive authentication and user data are typically received through mobile voice and keypads, and companies are responsible for protecting the data obtained through these channels. The increasing use of voice-driven interactive voice response (IVR) has resulted in a need to provide solutions that can protect user data that is gathered from [...]
