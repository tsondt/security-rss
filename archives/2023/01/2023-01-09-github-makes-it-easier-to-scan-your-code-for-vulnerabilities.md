Title: GitHub makes it easier to scan your code for vulnerabilities
Date: 2023-01-09T14:27:39-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-09-github-makes-it-easier-to-scan-your-code-for-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/github-makes-it-easier-to-scan-your-code-for-vulnerabilities/){:target="_blank" rel="noopener"}

> GitHub introduced a new option to set up code scanning for a repository known as "default setup," designed to help developers configure it automatically with just a few clicks. [...]
