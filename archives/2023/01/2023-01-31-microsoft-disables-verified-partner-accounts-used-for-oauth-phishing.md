Title: Microsoft disables verified partner accounts used for OAuth phishing
Date: 2023-01-31T10:13:53-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-01-31-microsoft-disables-verified-partner-accounts-used-for-oauth-phishing

[Source](https://www.bleepingcomputer.com/news/security/microsoft-disables-verified-partner-accounts-used-for-oauth-phishing/){:target="_blank" rel="noopener"}

> Microsoft has disabled multiple fraudulent, verified Microsoft Partner Network accounts for creating malicious OAuth applications that breached organizations' cloud environments to steal email. [...]
