Title: South Korea makes crypto crackdown a national justice priority
Date: 2023-01-31T04:28:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-01-31-south-korea-makes-crypto-crackdown-a-national-justice-priority

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/31/south_korea_crypto_money_laundering/){:target="_blank" rel="noopener"}

> It's listed alongside issues like tackling gang violence, drugs, and sex crimes South Korea's Ministry of Justice will create a "Virtual Currency Tracking System" to crack down on money laundering facilitated by cryptocurrencies, and rated the establishment of the facility among its priorities for the year.... [...]
