Title: FBI smokes ransomware Hive after secretly buzzing around gang's network for months
Date: 2023-01-26T20:30:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-26-fbi-smokes-ransomware-hive-after-secretly-buzzing-around-gangs-network-for-months

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/26/fbi_hive_ransomware/){:target="_blank" rel="noopener"}

> Uncle Sam doles out decryption keys to 300+ victims amid sting op The FBI said it has shut down the Hive's ransomware network, seizing control of the notorious gang's servers and websites, and thwarting the pesky criminals' ability to sting future victims.... [...]
