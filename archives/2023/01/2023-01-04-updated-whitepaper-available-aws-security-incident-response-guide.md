Title: Updated whitepaper available: AWS Security Incident Response Guide
Date: 2023-01-04T20:17:25+00:00
Author: Anna McAbee
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;AWS Incident Response;AWS Security Incident Response Guide
Slug: 2023-01-04-updated-whitepaper-available-aws-security-incident-response-guide

[Source](https://aws.amazon.com/blogs/security/updated-whitepaper-available-aws-security-incident-response-guide/){:target="_blank" rel="noopener"}

> The AWS Security Incident Response Guide focuses on the fundamentals of responding to security incidents within a customer’s Amazon Web Services (AWS) Cloud environment. You can use the guide to help build and iterate on your AWS security incident response program. Recently, we updated the AWS Security Incident Response Guide to more clearly explain what [...]
