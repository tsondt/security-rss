Title: Fake OnlyFans dating sites abuse UK Environment Agency open redirect
Date: 2023-01-09T11:31:41-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-09-fake-onlyfans-dating-sites-abuse-uk-environment-agency-open-redirect

[Source](https://www.bleepingcomputer.com/news/security/fake-onlyfans-dating-sites-abuse-uk-environment-agency-open-redirect/){:target="_blank" rel="noopener"}

> Threat actors abused an open redirect on the official website of the United Kingdom's Department for Environment, Food & Rural Affairs (DEFRA) to direct visitors to fake OnlyFans adult dating sites. [...]
