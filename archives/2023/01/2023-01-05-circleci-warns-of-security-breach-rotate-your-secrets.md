Title: CircleCI warns of security breach — rotate your secrets!
Date: 2023-01-05T00:39:59-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-01-05-circleci-warns-of-security-breach-rotate-your-secrets

[Source](https://www.bleepingcomputer.com/news/security/circleci-warns-of-security-breach-rotate-your-secrets/){:target="_blank" rel="noopener"}

> CircleCI, a software development service has disclosed a security incident and is urging users to rotate their secrets. The CI/CD platform touts having a user base comprising more than one million engineers who rely on the service for "speed and reliability" of their builds. [...]
