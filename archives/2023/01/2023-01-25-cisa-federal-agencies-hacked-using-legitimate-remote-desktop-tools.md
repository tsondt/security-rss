Title: CISA: Federal agencies hacked using legitimate remote desktop tools
Date: 2023-01-25T16:18:22-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-25-cisa-federal-agencies-hacked-using-legitimate-remote-desktop-tools

[Source](https://www.bleepingcomputer.com/news/security/cisa-federal-agencies-hacked-using-legitimate-remote-desktop-tools/){:target="_blank" rel="noopener"}

> CISA, the NSA, and MS-ISAC warned today in a joint advisory that attackers are increasingly using legitimate remote monitoring and management (RMM) software for malicious purposes. [...]
