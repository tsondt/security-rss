Title: Happy Lunar New Year: Beijing warns of enhanced surveillance during celebrations
Date: 2023-01-20T05:30:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-01-20-happy-lunar-new-year-beijing-warns-of-enhanced-surveillance-during-celebrations

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/20/chinese_new_year_surveillance/){:target="_blank" rel="noopener"}

> Censors are on the lookout for showering under a waterfall of money, overeating, and more conventional sins The Cyberspace Administration of China (CAC) has preempted celebrations for Lunar New Year – the Year of the Rabbit* commences on January 22 – by warning citizens to keep evidence of seasonal overindulgence off the internet.... [...]
