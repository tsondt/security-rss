Title: Lexmark warns of RCE bug affecting 100 printer models, PoC released
Date: 2023-01-26T15:08:01-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-26-lexmark-warns-of-rce-bug-affecting-100-printer-models-poc-released

[Source](https://www.bleepingcomputer.com/news/security/lexmark-warns-of-rce-bug-affecting-100-printer-models-poc-released/){:target="_blank" rel="noopener"}

> Lexmark has released a security firmware update to fix a severe vulnerability that could enable remote code execution (RCE) on more than 100 printer models. [...]
