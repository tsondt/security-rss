Title: Scattered Spider hackers use old Intel driver to bypass security
Date: 2023-01-11T16:55:47-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-11-scattered-spider-hackers-use-old-intel-driver-to-bypass-security

[Source](https://www.bleepingcomputer.com/news/security/scattered-spider-hackers-use-old-intel-driver-to-bypass-security/){:target="_blank" rel="noopener"}

> A financially motivated threat actor tracked as Scattered Spider was observed attempting to deploy Intel Ethernet diagnostics drivers in a BYOVD (Bring Your Own Vulnerable Driver) attack to evade detection from EDR (Endpoint Detection and Response) security products. [...]
