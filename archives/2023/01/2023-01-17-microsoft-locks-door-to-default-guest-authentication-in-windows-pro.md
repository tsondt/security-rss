Title: Microsoft locks door to default guest authentication in Windows Pro
Date: 2023-01-17T17:01:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-01-17-microsoft-locks-door-to-default-guest-authentication-in-windows-pro

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/17/microsoft_windows_pro_guest/){:target="_blank" rel="noopener"}

> Bringing OS version into sync with Enterprise and Education editions Microsoft wants to bulk up the security in Windows Pro editions by ensuring the SMB insecure guest authentication fallbacks are no longer the default setting in the operating system.... [...]
