Title: Experian Glitch Exposing Credit Files Lasted 47 Days
Date: 2023-01-25T19:58:46+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Web Fraud 2.0;Experian;Experian breach
Slug: 2023-01-25-experian-glitch-exposing-credit-files-lasted-47-days

[Source](https://krebsonsecurity.com/2023/01/experian-glitch-exposing-credit-files-lasted-47-days/){:target="_blank" rel="noopener"}

> On Dec. 23, 2022, KrebsOnSecurity alerted big-three consumer credit reporting bureau Experian that identity thieves had worked out how to bypass its security and access any consumer’s full credit report — armed with nothing more than a person’s name, address, date of birth, and Social Security number. Experian fixed the glitch, but remained silent about the incident for a month. This week, however, Experian acknowledged that the security failure persisted for nearly seven weeks, between Nov. 9, 2022 and Dec. 26, 2022. The tip about the Experian weakness came from Jenya Kushnir, a security researcher living in Ukraine who said [...]
