Title: Define a custom session duration and terminate active sessions in IAM Identity Center
Date: 2023-01-31T20:12:39+00:00
Author: Ron Cully
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;AWS IAM Identity Center;Cloud security;Identity providers;Security;Security Blog
Slug: 2023-01-31-define-a-custom-session-duration-and-terminate-active-sessions-in-iam-identity-center

[Source](https://aws.amazon.com/blogs/security/define-a-custom-session-duration-and-terminate-active-sessions-in-iam-identity-center/){:target="_blank" rel="noopener"}

> Managing access to accounts and applications requires a balance between delivering simple, convenient access and managing the risks associated with active user sessions. Based on your organization’s needs, you might want to make it simple for end users to sign in and to operate long enough to get their work done, without the disruptions associated [...]
