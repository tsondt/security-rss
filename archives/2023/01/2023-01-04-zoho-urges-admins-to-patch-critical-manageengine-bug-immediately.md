Title: Zoho urges admins to patch critical ManageEngine bug immediately
Date: 2023-01-04T14:52:15-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-04-zoho-urges-admins-to-patch-critical-manageengine-bug-immediately

[Source](https://www.bleepingcomputer.com/news/security/zoho-urges-admins-to-patch-critical-manageengine-bug-immediately/){:target="_blank" rel="noopener"}

> Business software provider Zoho has urged customers to patch a critical security flaw affecting multiple ManageEngine products. [...]
