Title: Microsoft: Over 100 threat actors deploy ransomware in attacks
Date: 2023-01-31T14:03:22-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-01-31-microsoft-over-100-threat-actors-deploy-ransomware-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-over-100-threat-actors-deploy-ransomware-in-attacks/){:target="_blank" rel="noopener"}

> Microsoft revealed today that its security teams are tracking more than 100 threat actors deploying ransomware during attacks. [...]
