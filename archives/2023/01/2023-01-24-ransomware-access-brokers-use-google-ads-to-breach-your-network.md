Title: Ransomware access brokers use Google ads to breach your network
Date: 2023-01-24T18:07:45-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-01-24-ransomware-access-brokers-use-google-ads-to-breach-your-network

[Source](https://www.bleepingcomputer.com/news/security/ransomware-access-brokers-use-google-ads-to-breach-your-network/){:target="_blank" rel="noopener"}

> A threat actor tracked as DEV-0569 uses Google Ads in widespread, ongoing advertising campaigns to distribute malware, steal victims' passwords, and ultimately breach networks for ransomware attacks. [...]
