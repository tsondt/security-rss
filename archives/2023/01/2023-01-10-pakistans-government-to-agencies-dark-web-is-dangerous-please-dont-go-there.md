Title: Pakistan’s government to agencies: Dark web is dangerous, please don’t go there
Date: 2023-01-10T02:29:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-01-10-pakistans-government-to-agencies-dark-web-is-dangerous-please-dont-go-there

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/10/pakistan_dark_web_warning/){:target="_blank" rel="noopener"}

> Advice follows embarrassing leak of audio from Prime Minister’s office Pakistan’s government has warned its agencies that the dark web exists, is home to all sorts of unpleasant people, and should be avoided.... [...]
