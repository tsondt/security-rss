Title: VMware fixes critical security bugs in vRealize log analysis tool
Date: 2023-01-24T17:01:36-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-24-vmware-fixes-critical-security-bugs-in-vrealize-log-analysis-tool

[Source](https://www.bleepingcomputer.com/news/security/vmware-fixes-critical-security-bugs-in-vrealize-log-analysis-tool/){:target="_blank" rel="noopener"}

> VMware released security patches on Tuesday to address vRealize Log Insight vulnerabilities that could enable attackers to gain remote execution on unpatched appliances. [...]
