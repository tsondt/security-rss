Title: US Cyber Command Operations During the 2022 Midterm Elections
Date: 2023-01-25T12:00:28+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;cybersecurity;hacking;national security policy;voting
Slug: 2023-01-25-us-cyber-command-operations-during-the-2022-midterm-elections

[Source](https://www.schneier.com/blog/archives/2023/01/us-cyber-command-operations-during-the-2022-midterm-elections.html){:target="_blank" rel="noopener"}

> The head of both US Cyber Command and the NSA, Gen. Paul Nakasone, broadly discussed that first organization’s offensive cyber operations during the runup to the 2022 midterm elections. He didn’t name names, of course: We did conduct operations persistently to make sure that our foreign adversaries couldn’t utilize infrastructure to impact us,” said Nakasone. “We understood how foreign adversaries utilize infrastructure throughout the world. We had that mapped pretty well. And we wanted to make sure that we took it down at key times.” Nakasone noted that Cybercom’s national mission force, aided by NSA, followed a “campaign plan” to [...]
