Title: Threats of Machine-Generated Text
Date: 2023-01-13T12:13:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;ChatGPT;machine learning;privacy;threat models
Slug: 2023-01-13-threats-of-machine-generated-text

[Source](https://www.schneier.com/blog/archives/2023/01/threats-of-machine-generated-text.html){:target="_blank" rel="noopener"}

> With the release of ChatGPT, I’ve read many random articles about this or that threat from the technology. This paper is a good survey of the field: what the threats are, how we might detect machine-generated text, directions for future research. It’s a solid grounding amongst all of the hype. Machine Generated Text: A Comprehensive Survey of Threat Models and Detection Methods Abstract: Advances in natural language generation (NLG) have resulted in machine generated text that is increasingly difficult to distinguish from human authored text. Powerful open-source models are freely available, and user-friendly tools democratizing access to generative models are [...]
