Title: Hacked Cellebrite and MSAB Software Released
Date: 2023-01-16T12:14:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberweapons;leaks;smartphones
Slug: 2023-01-16-hacked-cellebrite-and-msab-software-released

[Source](https://www.schneier.com/blog/archives/2023/01/hacked-cellebrite-and-msab-software-released.html){:target="_blank" rel="noopener"}

> Cellebrite is an cyberweapons arms manufacturer that sells smartphone forensic software to governments around the world. MSAB is a Swedish company that does the same thing. Someone has released software and documentation from both companies. [...]
