Title: Administrator of RSOCKS Proxy Botnet Pleads Guilty
Date: 2023-01-24T19:00:32+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Ne'er-Do-Well News;Denis Emelyantsev;IoT;IoT botnets;RSOCKS botnet;RSOCKS proxy;RUSdot;RUSdot Socks Server;Spamdot
Slug: 2023-01-24-administrator-of-rsocks-proxy-botnet-pleads-guilty

[Source](https://krebsonsecurity.com/2023/01/administrator-of-rsocks-proxy-botnet-pleads-guilty/){:target="_blank" rel="noopener"}

> Denis Emelyantsev, a 36-year-old Russian man accused of running a massive botnet called RSOCKS that stitched malware into millions of devices worldwide, pleaded guilty to two counts of computer crime violations in a California courtroom this week. The plea comes just months after Emelyantsev was extradited from Bulgaria, where he told investigators, “America is looking for me because I have enormous information and they need it.” A copy of the passport for Denis Emelyantsev, a.k.a. Denis Kloster, as posted to his Vkontakte page in 2019. First advertised in the cybercrime underground in 2014, RSOCKS was the web-based storefront for hacked [...]
