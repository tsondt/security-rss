Title: Lessons Learned from the Windows Remote Desktop Honeypot Report
Date: 2023-01-25T10:06:12-05:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-01-25-lessons-learned-from-the-windows-remote-desktop-honeypot-report

[Source](https://www.bleepingcomputer.com/news/security/lessons-learned-from-the-windows-remote-desktop-honeypot-report/){:target="_blank" rel="noopener"}

> Over several weeks in October of 2022, Specops collected 4.6 million attempted passwords on their Windows Remote Desktop honeypot system. Here is what they learned. [...]
