Title: Remote Vulnerabilities in Automobiles
Date: 2023-01-06T15:46:57+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;hacking;vulnerabilities
Slug: 2023-01-06-remote-vulnerabilities-in-automobiles

[Source](https://www.schneier.com/blog/archives/2023/01/remote-vulnerabilities-in-automobiles.html){:target="_blank" rel="noopener"}

> This group has found a ton of remote vulnerabilities in all sorts of automobiles. It’s enough to make you want to buy a car that is not Internet-connected. Unfortunately, that seems to be impossible. [...]
