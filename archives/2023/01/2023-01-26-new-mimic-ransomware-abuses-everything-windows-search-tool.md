Title: New Mimic ransomware abuses ‘Everything’ Windows search tool
Date: 2023-01-26T15:22:24-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-26-new-mimic-ransomware-abuses-everything-windows-search-tool

[Source](https://www.bleepingcomputer.com/news/security/new-mimic-ransomware-abuses-everything-windows-search-tool/){:target="_blank" rel="noopener"}

> A new ransomware family named 'Mimic' has been spotted in the wild abusing the APIs of a legitimate Windows file search tool called 'Everything' to achieve file enumeration. [...]
