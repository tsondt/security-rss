Title: Illegal Solaris darknet market hijacked by competitor Kraken
Date: 2023-01-18T14:21:19-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-18-illegal-solaris-darknet-market-hijacked-by-competitor-kraken

[Source](https://www.bleepingcomputer.com/news/security/illegal-solaris-darknet-market-hijacked-by-competitor-kraken/){:target="_blank" rel="noopener"}

> Solaris, a large darknet marketplace focused on drugs and illegal substances, has been taken over by a smaller competitor named 'Kraken,' who claims to have hacked it on January 13, 2022. [...]
