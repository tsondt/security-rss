Title: Microsoft Defender ASR rules strip icons, app shortcuts from Taskbar
Date: 2023-01-13T13:30:06+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-01-13-microsoft-defender-asr-rules-strip-icons-app-shortcuts-from-taskbar

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/13/happy_friday_13th_microsoft_defender/){:target="_blank" rel="noopener"}

> Happy Friday 13th sysadmins! Techies find workarounds but Redmond still 'investigating' Techies are reporting that Microsoft Defender for Endpoint attack surface reduction (ASR) rules have gone haywire and are removing icons and applications shortcuts from the Taskbar and Start Menu.... [...]
