Title: NIST Is Updating Its Cybersecurity Framework
Date: 2023-01-30T12:13:01+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;national security policy;NIST
Slug: 2023-01-30-nist-is-updating-its-cybersecurity-framework

[Source](https://www.schneier.com/blog/archives/2023/01/nist-is-updating-its-cybersecurity-framework.html){:target="_blank" rel="noopener"}

> NIST is planning a significant update of its Cybersecurity Framework. At this point, it’s asking for feedback and comments to its concept paper. Do the proposed changes reflect the current cybersecurity landscape (standards, risks, and technologies)? Are the proposed changes sufficient and appropriate? Are there other elements that should be considered under each area? Do the proposed changes support different use cases in various sectors, types, and sizes of organizations (and with varied capabilities, resources, and technologies)? Are there additional changes not covered here that should be considered? For those using CSF 1.1, would the proposed changes affect continued adoption [...]
