Title: New York man defrauded thousands using credit cards sold on dark web
Date: 2023-01-18T12:06:28-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-01-18-new-york-man-defrauded-thousands-using-credit-cards-sold-on-dark-web

[Source](https://www.bleepingcomputer.com/news/security/new-york-man-defrauded-thousands-using-credit-cards-sold-on-dark-web/){:target="_blank" rel="noopener"}

> A New York resident has pleaded guilty to charges of conspiracy to commit bank fraud using stolen credit cards purchased on dark web cybercrime marketplaces. [...]
