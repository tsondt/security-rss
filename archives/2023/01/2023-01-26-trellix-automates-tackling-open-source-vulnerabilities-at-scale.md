Title: Trellix automates tackling open source vulnerabilities at scale
Date: 2023-01-26T13:52:42+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-26-trellix-automates-tackling-open-source-vulnerabilities-at-scale

[Source](https://portswigger.net/daily-swig/trellix-automates-tackling-open-source-vulnerabilities-at-scale){:target="_blank" rel="noopener"}

> More than 61,000 vulnerabilities patched and counting [...]
