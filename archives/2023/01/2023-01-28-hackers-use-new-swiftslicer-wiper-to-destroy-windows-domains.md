Title: Hackers use new SwiftSlicer wiper to destroy Windows domains
Date: 2023-01-28T10:21:32-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-01-28-hackers-use-new-swiftslicer-wiper-to-destroy-windows-domains

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-new-swiftslicer-wiper-to-destroy-windows-domains/){:target="_blank" rel="noopener"}

> Security researchers have identified a new data-wiping malware they named SwiftSlicer that aims to overwrite crucial files used by the Windows operating system. [...]
