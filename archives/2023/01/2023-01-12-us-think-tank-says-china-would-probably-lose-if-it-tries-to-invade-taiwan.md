Title: US think tank says China would probably lose if it tries to invade Taiwan
Date: 2023-01-12T03:15:13+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-01-12-us-think-tank-says-china-would-probably-lose-if-it-tries-to-invade-taiwan

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/12/china_taiwan_wargame_scenarios/){:target="_blank" rel="noopener"}

> But even a short conflict would wreck the economy, which would be bad news for semiconductor supplies Three years from now, hypothetically, China launches an amphibious invasion of Taiwan. It does not go well, according to a top Washington think tank report.... [...]
