Title: Ex-GE engineer gets two years in prison after stealing turbine tech for China
Date: 2023-01-04T23:13:01+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-04-ex-ge-engineer-gets-two-years-in-prison-after-stealing-turbine-tech-for-china

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/04/ge_turbine_china_prison/){:target="_blank" rel="noopener"}

> Beijing isn't the only one spying on work computers, right GE? An ex-General Electric engineer has been sentenced to two years in prison after being convicted of stealing the US giant's turbine technology for China.... [...]
