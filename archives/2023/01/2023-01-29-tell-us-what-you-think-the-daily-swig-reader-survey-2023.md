Title: Tell us what you think: The Daily Swig reader survey 2023
Date: 2023-01-29T14:03:40+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-29-tell-us-what-you-think-the-daily-swig-reader-survey-2023

[Source](https://portswigger.net/daily-swig/tell-us-what-you-think-the-daily-swig-reader-survey-2023){:target="_blank" rel="noopener"}

> Have your say to be in with the chance to win Burp Suite swag... [...]
