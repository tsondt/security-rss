Title: Russian meddling in 2016 US presidential election was weak sauce
Date: 2023-01-10T22:00:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-01-10-russian-meddling-in-2016-us-presidential-election-was-weak-sauce

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/10/russian_election_meddling_us/){:target="_blank" rel="noopener"}

> Boffins find Twitter foreign influence campaign didn't have much pull Russian disinformation didn't materially affect the way people voted in the 2016 US presidential election, according to a research study published on Monday, though that doesn't make the effect totally inconsequential.... [...]
