Title: US offers $10M bounty for Hive ransomware links to foreign governments
Date: 2023-01-26T15:41:44-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-26-us-offers-10m-bounty-for-hive-ransomware-links-to-foreign-governments

[Source](https://www.bleepingcomputer.com/news/security/us-offers-10m-bounty-for-hive-ransomware-links-to-foreign-governments/){:target="_blank" rel="noopener"}

> The U.S. Department of State today offered up to $10 million for information that could help link the Hive ransomware group (or other threat actors) with foreign governments. [...]
