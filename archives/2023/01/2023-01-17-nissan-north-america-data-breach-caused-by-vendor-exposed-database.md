Title: Nissan North America data breach caused by vendor-exposed database
Date: 2023-01-17T09:50:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-17-nissan-north-america-data-breach-caused-by-vendor-exposed-database

[Source](https://www.bleepingcomputer.com/news/security/nissan-north-america-data-breach-caused-by-vendor-exposed-database/){:target="_blank" rel="noopener"}

> Nissan North America has begun sending data breach notifications informing customers of a breach at a third-party service provider that exposed customer information. [...]
