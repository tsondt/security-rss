Title: Ransomware profits drop 40% in 2022 as victims refuse to pay
Date: 2023-01-19T08:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-19-ransomware-profits-drop-40-in-2022-as-victims-refuse-to-pay

[Source](https://www.bleepingcomputer.com/news/security/ransomware-profits-drop-40-percent-in-2022-as-victims-refuse-to-pay/){:target="_blank" rel="noopener"}

> Ransomware gangs extorted from victims about $456.8 million throughout 2022, a drop of roughly 40% from the record-breaking $765 million recorded in the previous two years. [...]
