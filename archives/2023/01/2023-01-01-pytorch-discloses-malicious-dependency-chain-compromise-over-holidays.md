Title: PyTorch discloses malicious dependency chain compromise over holidays
Date: 2023-01-01T01:26:52-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-01-01-pytorch-discloses-malicious-dependency-chain-compromise-over-holidays

[Source](https://www.bleepingcomputer.com/news/security/pytorch-discloses-malicious-dependency-chain-compromise-over-holidays/){:target="_blank" rel="noopener"}

> PyTorch has identified a malicious dependency with the same name as the framework's 'torchtriton' library. This has led to a successful compromise via the dependency confusion attack vector. [...]
