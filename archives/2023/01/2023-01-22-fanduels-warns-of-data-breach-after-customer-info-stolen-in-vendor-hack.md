Title: FanDuels warns of data breach after customer info stolen in vendor hack
Date: 2023-01-22T13:56:45-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-01-22-fanduels-warns-of-data-breach-after-customer-info-stolen-in-vendor-hack

[Source](https://www.bleepingcomputer.com/news/security/fanduels-warns-of-data-breach-after-customer-info-stolen-in-vendor-hack/){:target="_blank" rel="noopener"}

> The FanDuel sportsbook and betting site is warning customers that their names and email addresses were exposed in a January 2023 MailChimp security breach, urging users to remain vigilant against phishing emails. [...]
