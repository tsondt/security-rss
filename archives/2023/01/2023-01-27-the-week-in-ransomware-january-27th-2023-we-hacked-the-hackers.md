Title: The Week in Ransomware - January 27th 2023 - 'We hacked the hackers'
Date: 2023-01-27T19:08:27-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-01-27-the-week-in-ransomware-january-27th-2023-we-hacked-the-hackers

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-january-27th-2023-we-hacked-the-hackers/){:target="_blank" rel="noopener"}

> For the most part, this week has been relatively quiet regarding ransomware attacks and researcher — that is, until the FBI announced the disruption of the Hive ransomware operation. [...]
