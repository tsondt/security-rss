Title: Rackspace blames ransomware woes on zero-day attack
Date: 2023-01-05T23:40:42+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-05-rackspace-blames-ransomware-woes-on-zero-day-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/05/rackspace_ransomware_gang/){:target="_blank" rel="noopener"}

> Play gang blamed, ProxyNotShell cleared and hosted Exchange doomed Rackspace has confirmed the Play ransomware gang was behind last month's hacking and said it won't bring back its hosted Microsoft Exchange email service, as it continues working to recover customers' email data lost in the December 2 ransomware attack.... [...]
