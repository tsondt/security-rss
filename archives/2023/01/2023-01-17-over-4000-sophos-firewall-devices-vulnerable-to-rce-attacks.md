Title: Over 4,000 Sophos Firewall devices vulnerable to RCE attacks
Date: 2023-01-17T13:53:06-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-17-over-4000-sophos-firewall-devices-vulnerable-to-rce-attacks

[Source](https://www.bleepingcomputer.com/news/security/over-4-000-sophos-firewall-devices-vulnerable-to-rce-attacks/){:target="_blank" rel="noopener"}

> Over 4,000 Sophos Firewall devices exposed to Internet access are vulnerable to attacks targeting a critical remote code execution (RCE) vulnerability. [...]
