Title: FCC wants telecom carriers to report data breaches faster
Date: 2023-01-06T13:11:19-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-06-fcc-wants-telecom-carriers-to-report-data-breaches-faster

[Source](https://www.bleepingcomputer.com/news/security/fcc-wants-telecom-carriers-to-report-data-breaches-faster/){:target="_blank" rel="noopener"}

> The U.S. Federal Communications Commission wants to strengthen federal law enforcement and modernize breach notification requirements for telecommunications companies so that they notify customers of security breaches faster. [...]
