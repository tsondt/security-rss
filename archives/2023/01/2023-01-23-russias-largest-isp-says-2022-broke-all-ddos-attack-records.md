Title: Russia’s largest ISP says 2022 broke all DDoS attack records
Date: 2023-01-23T15:18:02-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-23-russias-largest-isp-says-2022-broke-all-ddos-attack-records

[Source](https://www.bleepingcomputer.com/news/security/russia-s-largest-isp-says-2022-broke-all-ddos-attack-records/){:target="_blank" rel="noopener"}

> Russia's largest internet service provider Rostelecom says 2022 was a record year for Distributed denial of service attacks (DDoS) targeting organizations in the country. [...]
