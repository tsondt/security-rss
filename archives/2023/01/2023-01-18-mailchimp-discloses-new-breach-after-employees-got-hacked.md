Title: MailChimp discloses new breach after employees got hacked
Date: 2023-01-18T16:11:30-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-01-18-mailchimp-discloses-new-breach-after-employees-got-hacked

[Source](https://www.bleepingcomputer.com/news/security/mailchimp-discloses-new-breach-after-employees-got-hacked/){:target="_blank" rel="noopener"}

> Email marketing firm MailChimp suffered another breach after hackers accessed an internal customer support and account administration tool, allowing the threat actors to access the data of 133 customers. [...]
