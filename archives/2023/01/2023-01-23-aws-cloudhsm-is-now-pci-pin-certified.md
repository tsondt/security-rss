Title: AWS CloudHSM is now PCI PIN certified
Date: 2023-01-23T18:22:02+00:00
Author: Nivetha Chandran
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Compliance;Compliance reports;PCI;PCI DSS;Security Blog
Slug: 2023-01-23-aws-cloudhsm-is-now-pci-pin-certified

[Source](https://aws.amazon.com/blogs/security/aws-cloudhsm-is-now-pci-pin-certified/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce that AWS CloudHSM is certified for Payment Card Industry Personal Identification Number (PCI PIN) version 3.1. With CloudHSM, you can manage and access your keys on FIPS 140-2 Level 3 certified hardware, protected with customer-owned, single-tenant hardware security module (HSM) instances that run in your own virtual private [...]
