Title: GoTo says hackers stole customers' backups and encryption key
Date: 2023-01-24T08:43:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud
Slug: 2023-01-24-goto-says-hackers-stole-customers-backups-and-encryption-key

[Source](https://www.bleepingcomputer.com/news/security/goto-says-hackers-stole-customers-backups-and-encryption-key/){:target="_blank" rel="noopener"}

> GoTo (formerly LogMeIn) is warning customers that threat actors who breached its development environment in November 2022 stole encrypted backups containing customer information and an encryption key for a portion of that data. [...]
