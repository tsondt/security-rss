Title: LockBit: Sorry about the SickKids ransomware, not sorry about the rest
Date: 2023-01-04T00:59:55+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-04-lockbit-sorry-about-the-sickkids-ransomware-not-sorry-about-the-rest

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/04/lockbit_sickkids_ransomware/){:target="_blank" rel="noopener"}

> Blame it on the affiliate Notorious ransomware gang LockBit "formally apologized" for an extortion attack against Canada's largest children's hospital that the criminals blamed on a now-blocked affiliate group, and said it published a free decryptor for the victim to recover the files.... [...]
