Title: Microsoft took its macros and went home, so miscreants turned to Windows LNK files
Date: 2023-01-23T13:34:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-01-23-microsoft-took-its-macros-and-went-home-so-miscreants-turned-to-windows-lnk-files

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/23/threat_groups_malicious_lnk/){:target="_blank" rel="noopener"}

> Adapt or die Microsoft's move last year to block macros by default in Office applications is forcing miscreants to find other tools with which to launch cyberattacks, including the software vendor's LNK files – the shortcuts Windows uses to point to other files.... [...]
