Title: UK Cyber Security Centre's scary new story: One phish, two phish, Russia phish, Iran phish
Date: 2023-01-27T05:32:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-01-27-uk-cyber-security-centres-scary-new-story-one-phish-two-phish-russia-phish-iran-phish

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/27/uk_warns_against_russian_and/){:target="_blank" rel="noopener"}

> Nice people on LinkedIn want to harvest logins from politicians, boffins, and defense types The UK's National Cyber Security Centre (NCSC) has warned of two similar spear-phishing campaigns, one originating from Russia, the other from Iran.... [...]
