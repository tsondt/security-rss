Title: WhatsApp adds proxy support to help bypass Internet blocks
Date: 2023-01-05T12:19:07-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-05-whatsapp-adds-proxy-support-to-help-bypass-internet-blocks

[Source](https://www.bleepingcomputer.com/news/security/whatsapp-adds-proxy-support-to-help-bypass-internet-blocks/){:target="_blank" rel="noopener"}

> Starting today, WhatsApp now allows users to connect via proxy servers due to Internet shutdowns or if their governments block the service in their country. [...]
