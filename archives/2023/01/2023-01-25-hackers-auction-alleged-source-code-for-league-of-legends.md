Title: Hackers auction alleged source code for League of Legends
Date: 2023-01-25T14:34:52-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2023-01-25-hackers-auction-alleged-source-code-for-league-of-legends

[Source](https://www.bleepingcomputer.com/news/security/hackers-auction-alleged-source-code-for-league-of-legends/){:target="_blank" rel="noopener"}

> Threat actors are auctioning the alleged source code for Riot Game's League of Legends and the Packman anti-cheat software, confirmed to be stolen in a recent hack of the game company's developer environment. [...]
