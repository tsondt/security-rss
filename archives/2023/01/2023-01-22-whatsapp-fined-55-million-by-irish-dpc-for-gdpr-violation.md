Title: WhatsApp fined €5.5 million by Irish DPC for GDPR violation
Date: 2023-01-22T10:11:22-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-01-22-whatsapp-fined-55-million-by-irish-dpc-for-gdpr-violation

[Source](https://www.bleepingcomputer.com/news/security/whatsapp-fined-55-million-by-irish-dpc-for-gdpr-violation/){:target="_blank" rel="noopener"}

> The Irish Data Protection Commission (DPC) has fined WhatsApp Ireland €5.5 million ($5.95M) after confirming that the communications service has violated the GDPR (General Data Protection Regulation). [...]
