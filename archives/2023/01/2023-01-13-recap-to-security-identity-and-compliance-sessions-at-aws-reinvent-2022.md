Title: Recap to security, identity, and compliance sessions at AWS re:Invent 2022
Date: 2023-01-13T16:07:43+00:00
Author: Katie Collins
Category: AWS Security
Tags: AWS re:Invent;Events;Foundational (100);Security, Identity, & Compliance;AWS Re:Invent;AWS security;Cloud security;Live Events;Re:Invent 2022;Security Blog;Session Recordings
Slug: 2023-01-13-recap-to-security-identity-and-compliance-sessions-at-aws-reinvent-2022

[Source](https://aws.amazon.com/blogs/security/recap-to-security-identity-and-compliance-sessions-at-aws-reinvent-2022/){:target="_blank" rel="noopener"}

> AWS re:Invent returned to Las Vegas, NV, in November 2022. The conference featured over 2,200 sessions and hands-on labs and more than 51,000 attendees over 5 days. If you weren’t able to join us in person, or just want to revisit some of the security, identity, and compliance announcements and on-demand sessions, this blog post [...]
