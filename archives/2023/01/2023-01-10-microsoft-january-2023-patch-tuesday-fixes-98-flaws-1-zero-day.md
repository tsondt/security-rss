Title: Microsoft January 2023 Patch Tuesday fixes 98 flaws, 1 zero-day
Date: 2023-01-10T13:39:57-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-01-10-microsoft-january-2023-patch-tuesday-fixes-98-flaws-1-zero-day

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-january-2023-patch-tuesday-fixes-98-flaws-1-zero-day/){:target="_blank" rel="noopener"}

> ​Today is Microsoft's January 2023 Patch Tuesday, and with it comes fixes for an actively exploited zero-day vulnerability and a total of 98 flaws. [...]
