Title: Bluebottle hackers used signed Windows driver in attacks on banks
Date: 2023-01-05T07:00:55-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-01-05-bluebottle-hackers-used-signed-windows-driver-in-attacks-on-banks

[Source](https://www.bleepingcomputer.com/news/security/bluebottle-hackers-used-signed-windows-driver-in-attacks-on-banks/){:target="_blank" rel="noopener"}

> A signed Windows driver has been used in attacks on banks in French-speaking countries, likely from a threat actor that stole more than $11 million from various banks. [...]
