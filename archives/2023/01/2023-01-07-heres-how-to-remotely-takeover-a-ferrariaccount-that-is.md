Title: Here's how to remotely takeover a Ferrari...account, that is
Date: 2023-01-07T09:01:24+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-07-heres-how-to-remotely-takeover-a-ferrariaccount-that-is

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/07/car_hacking_ferrari_account/){:target="_blank" rel="noopener"}

> Connected cars. What could possibly go wrong? Multiple bugs affecting millions of vehicles from almost all major car brands could allow miscreants to perform any manner of mischief — in some cases including full takeovers — by exploiting vulnerabilities in the vehicles' telematic systems, automotive APIs and supporting infrastructure, according to security researchers.... [...]
