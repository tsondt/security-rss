Title: University of Texas latest US school to ban TikTok
Date: 2023-01-19T16:15:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-01-19-university-of-texas-latest-us-school-to-ban-tiktok

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/19/texas_university_bans_tiktok/){:target="_blank" rel="noopener"}

> Great, now staff and students can stop scrolling and get back to work Faculty and students at the University of Texas at Austin (UT) this week became the latest members of a public US university to lose access to Chinese video app TikTok via campus networks.... [...]
