Title: Trojan Puzzle attack trains AI assistants into suggesting malicious code
Date: 2023-01-10T15:20:13-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Technology
Slug: 2023-01-10-trojan-puzzle-attack-trains-ai-assistants-into-suggesting-malicious-code

[Source](https://www.bleepingcomputer.com/news/security/trojan-puzzle-attack-trains-ai-assistants-into-suggesting-malicious-code/){:target="_blank" rel="noopener"}

> Researchers at the universities of California, Virginia, and Microsoft have devised a new poisoning attack that could trick AI-based coding assistants into suggesting dangerous code. [...]
