Title: Tesla tackles CORS misconfigurations that left internal networks vulnerable
Date: 2023-01-05T16:51:34+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-05-tesla-tackles-cors-misconfigurations-that-left-internal-networks-vulnerable

[Source](https://portswigger.net/daily-swig/tesla-tackles-cors-misconfigurations-that-left-internal-networks-vulnerable){:target="_blank" rel="noopener"}

> Typosquatting ploy successfully bypassed firewalls of multiple organizations [...]
