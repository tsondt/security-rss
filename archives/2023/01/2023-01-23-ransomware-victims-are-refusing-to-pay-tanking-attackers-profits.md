Title: Ransomware victims are refusing to pay, tanking attackers’ profits
Date: 2023-01-23T17:40:08+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;chainalysis;Coveware;crypto ransomware;cryptocurrency;ransomware
Slug: 2023-01-23-ransomware-victims-are-refusing-to-pay-tanking-attackers-profits

[Source](https://arstechnica.com/?p=1911768){:target="_blank" rel="noopener"}

> Enlarge / Holding up corporations, utilities, and hospitals for malware-encrypted data used to be quite profitable. But it's a tough gig lately, you know? (credit: ifanfoto/Getty Images) Two new studies suggest that ransomware isn't the lucrative, enterprise-scale gotcha it used to be. Profits to attackers' wallets, and the percentage of victims paying, fell dramatically in 2022, according to two separate reports. Chainalysis, a blockchain analysis firm that has worked with a number of law enforcement and government agencies, suggests in a blog post that based on payments to cryptocurrency addresses it has identified as connected to ransomware attacks, payments to [...]
