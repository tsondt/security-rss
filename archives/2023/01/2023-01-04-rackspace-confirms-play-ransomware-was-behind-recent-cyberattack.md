Title: Rackspace confirms Play ransomware was behind recent cyberattack
Date: 2023-01-04T17:21:54-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-04-rackspace-confirms-play-ransomware-was-behind-recent-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/rackspace-confirms-play-ransomware-was-behind-recent-cyberattack/){:target="_blank" rel="noopener"}

> Texas-based cloud computing provider Rackspace has confirmed that the Play ransomware operation was behind a recent cyberattack that took down the company's hosted Microsoft Exchange environments. [...]
