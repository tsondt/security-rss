Title: Booklist Review of A Hacker’s Mind
Date: 2023-01-14T16:29:46+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;A Hacker's Mind;books;Schneier news
Slug: 2023-01-14-booklist-review-of-a-hackers-mind

[Source](https://www.schneier.com/blog/archives/2023/01/booklist-review-of-a-hackers-mind.html){:target="_blank" rel="noopener"}

> Booklist reviews A Hacker’s Mind : Author and public-interest security technologist Schneier ( Data and Goliath, 2015) defines a “hack” as an activity allowed by a system “that subverts the rules or norms of the system [...] at the expense of someone else affected by the system.” In accessing the security of a particular system, technologists such as Schneier look at how it might fail. In order to counter a hack, it becomes necessary to think like a hacker. Schneier lays out the ramifications of a variety of hacks, contrasting the hacking of the tax code to benefit the wealthy [...]
