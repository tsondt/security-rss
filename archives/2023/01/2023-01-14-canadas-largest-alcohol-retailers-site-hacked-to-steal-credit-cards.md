Title: Canada's largest alcohol retailer's site hacked to steal credit cards
Date: 2023-01-14T09:16:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-14-canadas-largest-alcohol-retailers-site-hacked-to-steal-credit-cards

[Source](https://www.bleepingcomputer.com/news/security/canadas-largest-alcohol-retailers-site-hacked-to-steal-credit-cards/){:target="_blank" rel="noopener"}

> The Liquor Control Board of Ontario (LCBO), a Canadian government enterprise and the country's largest beverage alcohol retailer, revealed that unknown attackers had breached its website to inject malicious code designed to steal customer and credit card information at check-out. [...]
