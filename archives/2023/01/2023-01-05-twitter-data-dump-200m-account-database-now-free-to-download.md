Title: Twitter data dump: 200m+ account database now free to download
Date: 2023-01-05T21:30:15+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-05-twitter-data-dump-200m-account-database-now-free-to-download

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/05/twitter_leak_200m_accounts/){:target="_blank" rel="noopener"}

> No passwords, but plenty of stuff for social engineering and doxxing More than 200 million Twitter users' information is now available for anyone to download for free.... [...]
