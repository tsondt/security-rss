Title: Deploy a dashboard for AWS WAF with minimal effort
Date: 2023-01-25T00:31:15+00:00
Author: Tomasz Stachlewski
Category: AWS Security
Tags: Amazon CloudFront;AWS WAF;Intermediate (200);Security, Identity, & Compliance;Security Blog
Slug: 2023-01-25-deploy-a-dashboard-for-aws-waf-with-minimal-effort

[Source](https://aws.amazon.com/blogs/security/deploy-dashboard-for-aws-waf-minimal-effort/){:target="_blank" rel="noopener"}

> January 24, 2023: This post was republished to update the code, architecture, and narrative. September 9, 2021: Amazon Elasticsearch Service has been renamed to Amazon OpenSearch Service. See details. In this post, we’ll show you how to deploy a solution in your Amazon Web Services (AWS) account that will provide a fully automated dashboard for [...]
