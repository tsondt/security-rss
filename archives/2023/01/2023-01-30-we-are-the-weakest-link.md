Title: We are the weakest link
Date: 2023-01-30T12:26:11+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-01-30-we-are-the-weakest-link

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/30/we_are_the_weakest_link/){:target="_blank" rel="noopener"}

> Mitigating the risks of human error in digital defenses Webinar It's a startling truth but 45 percent of workers in the US believe using public Wi-Fi is safe.... [...]
