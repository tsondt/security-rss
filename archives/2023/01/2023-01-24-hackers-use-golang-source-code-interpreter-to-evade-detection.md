Title: Hackers use Golang source code interpreter to evade detection
Date: 2023-01-24T06:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-24-hackers-use-golang-source-code-interpreter-to-evade-detection

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-golang-source-code-interpreter-to-evade-detection/){:target="_blank" rel="noopener"}

> A Chinese-speaking hacking group tracked as 'DragonSpark' was observed employing Golang source code interpretation to evade detection while launching espionage attacks against organizations in East Asia. [...]
