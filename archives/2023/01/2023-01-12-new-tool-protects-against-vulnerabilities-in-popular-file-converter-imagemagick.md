Title: New tool protects against vulnerabilities in popular file converter ImageMagick
Date: 2023-01-12T16:03:31+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-12-new-tool-protects-against-vulnerabilities-in-popular-file-converter-imagemagick

[Source](https://portswigger.net/daily-swig/new-tool-protects-against-vulnerabilities-in-popular-file-converter-imagemagick){:target="_blank" rel="noopener"}

> Library has somewhat of an image problem given history of serious bugs [...]
