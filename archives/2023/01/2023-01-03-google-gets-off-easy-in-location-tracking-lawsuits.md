Title: Google gets off easy in location tracking lawsuits
Date: 2023-01-03T16:00:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-01-03-google-gets-off-easy-in-location-tracking-lawsuits

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/03/google_tracking_settlements/){:target="_blank" rel="noopener"}

> $29.5 million and we don't have to admit wrongdoing? Where do we sign? Google has settled two more of the many location tracking lawsuits it had been facing over the past year, and this time the search giant is getting an even better deal: just $29.5 million to resolve complaints filed in Indiana and Washington DC with no admission of wrongdoing.... [...]
