Title: Royal ransomware claims attack on Queensland University of Technology
Date: 2023-01-03T11:40:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-03-royal-ransomware-claims-attack-on-queensland-university-of-technology

[Source](https://www.bleepingcomputer.com/news/security/royal-ransomware-claims-attack-on-queensland-university-of-technology/){:target="_blank" rel="noopener"}

> The Royal ransomware gang has claimed responsibility for a recent cyberattack on the Queensland University of Technology and begun to leak data allegedly stolen during the security breach. [...]
