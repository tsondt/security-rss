Title: Brave browser’s new Snowflake feature help bypass Tor blocks
Date: 2023-01-14T10:28:56-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-14-brave-browsers-new-snowflake-feature-help-bypass-tor-blocks

[Source](https://www.bleepingcomputer.com/news/security/brave-browser-s-new-snowflake-feature-help-bypass-tor-blocks/){:target="_blank" rel="noopener"}

> Brave Browser version 1.47 was released yesterday, adding the Snowflake extension in the software's settings, enabling users to turn their devices into proxies that help users in censored countries connect to Tor. [...]
