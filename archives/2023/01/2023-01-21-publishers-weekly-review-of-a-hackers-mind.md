Title: Publisher’s Weekly Review of A Hacker’s Mind
Date: 2023-01-21T12:18:34+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;A Hacker's Mind;books;Schneier news
Slug: 2023-01-21-publishers-weekly-review-of-a-hackers-mind

[Source](https://www.schneier.com/blog/archives/2023/01/publishers-weekly-review-of-a-hackers-mind.html){:target="_blank" rel="noopener"}

> Publisher’s Weekly reviewed A Hacker’s Mind —and it’s a starred review! “Hacking is something that the rich and powerful do, something that reinforces existing power structures,” contends security technologist Schneier ( Click Here to Kill Everybody ) in this excellent survey of exploitation. Taking a broad understanding of hacking as an “activity allowed by the system that subverts the... system,” Schneier draws on his background analyzing weaknesses in cybersecurity to examine how those with power take advantage of financial, legal, political, and cognitive systems. He decries how venture capitalists “hack” market dynamics by subverting the pressures of supply and demand, [...]
