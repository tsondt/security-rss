Title: What Twitter’s 200 million email leak really means
Date: 2023-01-07T12:40:44+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;hackers;syndication;Twitter
Slug: 2023-01-07-what-twitters-200-million-email-leak-really-means

[Source](https://arstechnica.com/?p=1908413){:target="_blank" rel="noopener"}

> Enlarge (credit: Rosie Struve; Getty Images) After reports at the end of 2022 that hackers were selling data stolen from 400 million Twitter users, researchers now say that a widely circulated trove of email addresses linked to about 200 million users is likely a refined version of the larger trove with duplicate entries removed. The social network has not yet commented on the massive exposure, but the cache of data clarifies the severity of the leak and who may be most at risk as a result of it. From June 2021 until January 2022, there was a bug in a [...]
