Title: Microsoft upgrades Defender to lock down Linux gear for its own good
Date: 2023-01-31T20:45:05+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-01-31-microsoft-upgrades-defender-to-lock-down-linux-gear-for-its-own-good

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/31/microsoft_defender_linux/){:target="_blank" rel="noopener"}

> Ballmer thought this kernel was cancer, Nadella may disagree Organizations using Microsoft's Defender for Endpoint will now be able to isolate Linux devices from their networks to contain intrusions and whatnot.... [...]
