Title: Wiretap lawsuit accuses Apple of tracking iPhone users who opted out
Date: 2023-01-10T15:30:08+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2023-01-10-wiretap-lawsuit-accuses-apple-of-tracking-iphone-users-who-opted-out

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/10/apple_wiretap_lawsuit/){:target="_blank" rel="noopener"}

> This is the company that claims: 'Privacy. That's iPhone' Apple "unlawfully records and uses consumers' personal information and activity," claims a new lawsuit accusing the company of tracking iPhone users' device data even when they've asked for tracking to be switched off.... [...]
