Title: Devs urged to rotate secrets after CircleCI suffers security breach
Date: 2023-01-05T14:38:56+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-05-devs-urged-to-rotate-secrets-after-circleci-suffers-security-breach

[Source](https://portswigger.net/daily-swig/devs-urged-to-rotate-secrets-after-circleci-suffers-security-breach){:target="_blank" rel="noopener"}

> DevOps platform advises customers to revoke API tokens [...]
