Title: Rail giant Wabtec discloses data breach after Lockbit ransomware attack
Date: 2023-01-03T15:13:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-03-rail-giant-wabtec-discloses-data-breach-after-lockbit-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/rail-giant-wabtec-discloses-data-breach-after-lockbit-ransomware-attack/){:target="_blank" rel="noopener"}

> U.S. rail and locomotive company Wabtec Corporation has disclosed a data breach that exposed personal and sensitive information. [...]
