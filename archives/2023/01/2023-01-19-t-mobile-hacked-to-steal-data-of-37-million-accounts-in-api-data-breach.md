Title: T-Mobile hacked to steal data of 37 million accounts in API data breach
Date: 2023-01-19T17:19:22-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-19-t-mobile-hacked-to-steal-data-of-37-million-accounts-in-api-data-breach

[Source](https://www.bleepingcomputer.com/news/security/t-mobile-hacked-to-steal-data-of-37-million-accounts-in-api-data-breach/){:target="_blank" rel="noopener"}

> T-Mobile disclosed a new data breach after a threat actor stole the personal information of 37 million current postpaid and prepaid customer accounts through one of its Application Programming Interfaces (APIs). [...]
