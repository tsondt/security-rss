Title: Savvy cybersecurity pros benefit from host of free resources to step up fight against hackers and cyber threats
Date: 2023-01-27T08:57:05+00:00
Author: Rob Jaques
Category: The Register
Tags: 
Slug: 2023-01-27-savvy-cybersecurity-pros-benefit-from-host-of-free-resources-to-step-up-fight-against-hackers-and-cyber-threats

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/27/savvy_cybersecurity_pros_benefit_from/){:target="_blank" rel="noopener"}

> Sign up to SANS Institute to keep up to speed with all aspects of the fast-evolving infosec sector Sponsored Post They say there's no such thing as a free lunch, but in fact there's a veritable feast of valuable resources online for infosec professionals which won't cost you anything.... [...]
