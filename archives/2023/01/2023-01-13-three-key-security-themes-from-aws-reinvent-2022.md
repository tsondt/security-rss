Title: Three key security themes from AWS re:Invent 2022
Date: 2023-01-13T20:34:45+00:00
Author: Anne Grahn
Category: AWS Security
Tags: AWS re:Invent;Events;Security, Identity, & Compliance;Thought Leadership;Amazon GuardDuty;Amazon Inspector;Amazon Macie;Amazon Security Lake;Amazon Verified Access;Amazon Verified Permissions;Amazon VPC lattice;AWS Config;AWS Lambda;AWS Wickr;cybersecurity;Data protection;Encryption;Identity and Access Management;Re:Invent 2022;reInvent;Security;Security Blog;threat detection;Zero Trust
Slug: 2023-01-13-three-key-security-themes-from-aws-reinvent-2022

[Source](https://aws.amazon.com/blogs/security/three-key-security-themes-from-aws-reinvent-2022/){:target="_blank" rel="noopener"}

> AWS re:Invent returned to Las Vegas, Nevada, November 28 to December 2, 2022. After a virtual event in 2020 and a hybrid 2021 edition, spirits were high as over 51,000 in-person attendees returned to network and learn about the latest AWS innovations. Now in its 11th year, the conference featured 5 keynotes, 22 leadership sessions, [...]
