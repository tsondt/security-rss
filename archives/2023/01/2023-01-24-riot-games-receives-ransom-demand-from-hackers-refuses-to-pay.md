Title: Riot Games receives ransom demand from hackers, refuses to pay
Date: 2023-01-24T12:23:16-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-24-riot-games-receives-ransom-demand-from-hackers-refuses-to-pay

[Source](https://www.bleepingcomputer.com/news/security/riot-games-receives-ransom-demand-from-hackers-refuses-to-pay/){:target="_blank" rel="noopener"}

> Riot Games says it will not pay a $10 million ransom demanded by attackers who stole League of Legends source code in last week's security breach. [...]
