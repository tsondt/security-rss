Title: Thinking of Hiring or Running a Booter Service? Think Again.
Date: 2023-01-18T02:30:15+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: DDoS-for-Hire;Ne'er-Do-Well News;Allison Nixon;booter services;ddos-for-hire;IPStresser;John Dobbs;Matthew Gatrel;stresser services;Unit 221B
Slug: 2023-01-18-thinking-of-hiring-or-running-a-booter-service-think-again

[Source](https://krebsonsecurity.com/2023/01/thinking-of-hiring-or-running-a-booter-service-think-again/){:target="_blank" rel="noopener"}

> Most people who operate DDoS-for-hire businesses attempt to hide their true identities and location. Proprietors of these so-called “booter” or “stresser” services — designed to knock websites and users offline — have long operated in a legally murky area of cybercrime law. But until recently, their biggest concern wasn’t avoiding capture or shutdown by the feds: It was minimizing harassment from unhappy customers or victims, and insulating themselves against incessant attacks from competing DDoS-for-hire services. And then there are booter store operators like John Dobbs, a 32-year-old computer science graduate student living in Honolulu, Hawaii. For at least a decade [...]
