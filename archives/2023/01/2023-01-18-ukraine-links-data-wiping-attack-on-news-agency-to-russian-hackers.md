Title: Ukraine links data-wiping attack on news agency to Russian hackers
Date: 2023-01-18T14:57:51-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-18-ukraine-links-data-wiping-attack-on-news-agency-to-russian-hackers

[Source](https://www.bleepingcomputer.com/news/security/ukraine-links-data-wiping-attack-on-news-agency-to-russian-hackers/){:target="_blank" rel="noopener"}

> The Computer Emergency Response Team of Ukraine (CERT-UA) has linked a destructive malware attack targeting the country's national news agency (Ukrinform) to Sandworm Russian military hackers. [...]
