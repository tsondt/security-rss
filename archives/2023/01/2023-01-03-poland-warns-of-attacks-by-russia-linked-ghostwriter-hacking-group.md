Title: Poland warns of attacks by Russia-linked Ghostwriter hacking group
Date: 2023-01-03T13:26:10-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-03-poland-warns-of-attacks-by-russia-linked-ghostwriter-hacking-group

[Source](https://www.bleepingcomputer.com/news/security/poland-warns-of-attacks-by-russia-linked-ghostwriter-hacking-group/){:target="_blank" rel="noopener"}

> The Polish government is warning of a spike in cyberattacks from Russia-linked hackers, including the state-sponsored hacking group known as GhostWriter. [...]
