Title: Bulk Surveillance of Money Transfers
Date: 2023-01-24T12:14:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;law enforcement;national security policy;privacy;surveillance
Slug: 2023-01-24-bulk-surveillance-of-money-transfers

[Source](https://www.schneier.com/blog/archives/2023/01/bulk-surveillance-of-money-transfers.html){:target="_blank" rel="noopener"}

> Just another obscure warrantless surveillance program. US law enforcement can access details of money transfers without a warrant through an obscure surveillance program the Arizona attorney general’s office created in 2014. A database stored at a nonprofit, the Transaction Record Analysis Center (TRAC), provides full names and amounts for larger transfers (above $500) sent between the US, Mexico and 22 other regions through services like Western Union, MoneyGram and Viamericas. The program covers data for numerous Caribbean and Latin American countries in addition to Canada, China, France, Malaysia, Spain, Thailand, Ukraine and the US Virgin Islands. Some domestic transfers also [...]
