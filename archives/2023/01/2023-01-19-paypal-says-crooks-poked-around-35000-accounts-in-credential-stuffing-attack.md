Title: PayPal says crooks poked around 35,000 accounts in credential stuffing attack
Date: 2023-01-19T23:45:04+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-19-paypal-says-crooks-poked-around-35000-accounts-in-credential-stuffing-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/19/paypal_data_breach/){:target="_blank" rel="noopener"}

> That passwordless option is looking really good right about now The personal information of 35,000 PayPal users was exposed in December, according to a notification letter sent to the online payment company's customers this week.... [...]
