Title: How to run AWS CloudHSM workloads in container environments
Date: 2023-01-25T21:59:27+00:00
Author: Derek Tumulak
Category: AWS Security
Tags: AWS CloudHSM;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Containers;Docker;Java;PKCS#11;Security Blog
Slug: 2023-01-25-how-to-run-aws-cloudhsm-workloads-in-container-environments

[Source](https://aws.amazon.com/blogs/security/how-to-run-aws-cloudhsm-workloads-on-docker-containers/){:target="_blank" rel="noopener"}

> January 25, 2023: We updated this post to reflect the fact that CloudHSM SDK3 does not support serverless environments and we strongly recommend deploying SDK5. AWS CloudHSM provides hardware security modules (HSMs) in the AWS Cloud. With CloudHSM, you can generate and use your own encryption keys in the AWS Cloud, and manage your keys [...]
