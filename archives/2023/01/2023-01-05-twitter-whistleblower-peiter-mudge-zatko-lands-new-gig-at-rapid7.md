Title: Twitter whistleblower Peiter 'Mudge' Zatko lands new gig at Rapid7
Date: 2023-01-05T12:30:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-01-05-twitter-whistleblower-peiter-mudge-zatko-lands-new-gig-at-rapid7

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/05/zatko_rapid7/){:target="_blank" rel="noopener"}

> A long way from password crackers for Windows NT for former L0pht legend Updated Former Twitter security chief and whistleblower Peiter "Mudge" Zatko has landed his first official role since he left the company, a part-time job as "executive in residence" with cybersecurity firm Rapid7.... [...]
