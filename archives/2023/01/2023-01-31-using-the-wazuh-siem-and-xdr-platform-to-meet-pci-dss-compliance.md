Title: Using the Wazuh SIEM and XDR platform to meet PCI DSS compliance
Date: 2023-01-31T10:05:10-05:00
Author: Sponsored by Wazuh
Category: BleepingComputer
Tags: Security
Slug: 2023-01-31-using-the-wazuh-siem-and-xdr-platform-to-meet-pci-dss-compliance

[Source](https://www.bleepingcomputer.com/news/security/using-the-wazuh-siem-and-xdr-platform-to-meet-pci-dss-compliance/){:target="_blank" rel="noopener"}

> Wazuh is a free, open source security platform that unifies XDR and SIEM capabilities. Here's how Wazuh helps implement PCI DSS compliance for your organization. [...]
