Title: Yandex denies hack, blames source code leak on former employee
Date: 2023-01-26T09:44:08-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-26-yandex-denies-hack-blames-source-code-leak-on-former-employee

[Source](https://www.bleepingcomputer.com/news/security/yandex-denies-hack-blames-source-code-leak-on-former-employee/){:target="_blank" rel="noopener"}

> A Yandex source code repository allegedly stolen by a former employee of the Russian technology company has been leaked as a Torrent on a popular hacking forum. [...]
