Title: AWS patches bypass bug in CloudTrail API monitoring tool
Date: 2023-01-23T13:01:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-23-aws-patches-bypass-bug-in-cloudtrail-api-monitoring-tool

[Source](https://portswigger.net/daily-swig/aws-patches-bypass-bug-in-cloudtrail-api-monitoring-tool){:target="_blank" rel="noopener"}

> Threat actors poking around AWS environments and API calls could stay under the radar [...]
