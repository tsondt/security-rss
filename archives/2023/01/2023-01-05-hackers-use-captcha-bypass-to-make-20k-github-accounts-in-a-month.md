Title: Hackers use CAPTCHA bypass to make 20K GitHub accounts in a month
Date: 2023-01-05T09:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-05-hackers-use-captcha-bypass-to-make-20k-github-accounts-in-a-month

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-captcha-bypass-to-make-20k-github-accounts-in-a-month/){:target="_blank" rel="noopener"}

> South African threat actors known as 'Automated Libra' has been improving its techniques to make a profit by using cloud platform resources for cryptocurrency mining. [...]
