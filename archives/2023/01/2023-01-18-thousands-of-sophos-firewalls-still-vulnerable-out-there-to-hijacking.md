Title: Thousands of Sophos firewalls still vulnerable out there to hijacking
Date: 2023-01-18T23:30:15+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-18-thousands-of-sophos-firewalls-still-vulnerable-out-there-to-hijacking

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/18/4000_buggy_sophos_firewalls/){:target="_blank" rel="noopener"}

> As hundreds of staff axed this week More than 4,000 public-facing Sophos firewalls remain vulnerable to a critical remote code execution bug disclosed last year and patched months later, according to security researchers.... [...]
