Title: Shady reward apps on Google Play amass 20 million downloads
Date: 2023-01-29T10:16:32-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2023-01-29-shady-reward-apps-on-google-play-amass-20-million-downloads

[Source](https://www.bleepingcomputer.com/news/security/shady-reward-apps-on-google-play-amass-20-million-downloads/){:target="_blank" rel="noopener"}

> A new category of activity tracking applications has been having massive success recently on Google Play, Android's official app store, having been downloaded on over 20 million devices. [...]
