Title: Ireland fines Meta $414m for using personal data without asking
Date: 2023-01-04T16:15:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-01-04-ireland-fines-meta-414m-for-using-personal-data-without-asking

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/04/meta_fined_390_for_using/){:target="_blank" rel="noopener"}

> Facebook, Insta told to pay up, make changes to data slurping process within 3 months A legal saga between Meta, Ireland and the European Union has reached a conclusion – at least for now – that forces the social media giant to remove data consent requirements from its terms of service in favor of explicit consent, and subjects it to a few hundred million more euros in fines for the trouble.... [...]
