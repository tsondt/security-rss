Title: 200 million Twitter users' email addresses allegedly leaked online
Date: 2023-01-04T15:16:06-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Technology
Slug: 2023-01-04-200-million-twitter-users-email-addresses-allegedly-leaked-online

[Source](https://www.bleepingcomputer.com/news/security/200-million-twitter-users-email-addresses-allegedly-leaked-online/){:target="_blank" rel="noopener"}

> A data leak described as containing email addresses for over 200 million Twitter users has been published on a popular hacker forum for about $2. BleepingComputer has confirmed the validity of many of the email addresses listed in the leak. [...]
