Title: Decarbonizing Cryptocurrencies through Taxation
Date: 2023-01-04T12:17:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;bitcoin;blockchain;courts;cryptocurrency;essays;laws
Slug: 2023-01-04-decarbonizing-cryptocurrencies-through-taxation

[Source](https://www.schneier.com/blog/archives/2023/01/decarbonizing-cryptocurrencies-through-taxation.html){:target="_blank" rel="noopener"}

> Maintaining bitcoin and other cryptocurrencies causes about 0.3 percent of global CO 2 emissions. That may not sound like a lot, but it’s more than the emissions of Switzerland, Croatia, and Norway combined. As many cryptocurrencies crash and the FTX bankruptcy moves into the litigation stage, regulators are likely to scrutinize the cryptocurrency world more than ever before. This presents a perfect opportunity to curb their environmental damage. The good news is that cryptocurrencies don’t have to be carbon intensive. In fact, some have near-zero emissions. To encourage polluting currencies to reduce their carbon footprint, we need to force buyers [...]
