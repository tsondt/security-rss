Title: Been hit by BianLian ransomware? Here's your get-out-of-jail-free card
Date: 2023-01-18T03:01:05+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-01-18-been-hit-by-bianlian-ransomware-heres-your-get-out-of-jail-free-card

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/18/bianlian_decryptor_avast/){:target="_blank" rel="noopener"}

> Avast issues a free decryptor so victims can get their data back Cybersecurity firm Avast has released a free decryptor for victims of BianLian – an emerging ransomware threat that came into the public eye in last year.... [...]
