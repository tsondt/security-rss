Title: AI-generated phishing emails just got much more convincing
Date: 2023-01-11T20:13:03+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-11-ai-generated-phishing-emails-just-got-much-more-convincing

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/11/gpt3_phishing_emails/){:target="_blank" rel="noopener"}

> Did a criminally minded robot write this? In part, yes. GPT-3 language models are being abused to do much more than write college essays, according to WithSecure researchers.... [...]
