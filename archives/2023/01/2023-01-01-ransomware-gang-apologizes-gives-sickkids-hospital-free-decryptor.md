Title: Ransomware gang apologizes, gives SickKids hospital free decryptor
Date: 2023-01-01T14:00:43-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-01-01-ransomware-gang-apologizes-gives-sickkids-hospital-free-decryptor

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-apologizes-gives-sickkids-hospital-free-decryptor/){:target="_blank" rel="noopener"}

> The LockBit ransomware gang has released a free decryptor for the Hospital for Sick Children (SickKids), saying one of its members violated rules by attacking the healthcare organization. [...]
