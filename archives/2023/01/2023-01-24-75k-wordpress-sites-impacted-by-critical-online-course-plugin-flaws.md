Title: 75k WordPress sites impacted by critical online course plugin flaws
Date: 2023-01-24T12:16:04-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-24-75k-wordpress-sites-impacted-by-critical-online-course-plugin-flaws

[Source](https://www.bleepingcomputer.com/news/security/75k-wordpress-sites-impacted-by-critical-online-course-plugin-flaws/){:target="_blank" rel="noopener"}

> The WordPress online course plugin 'LearnPress' was vulnerable to multiple critical-severity flaws, including pre-auth SQL injection and local file inclusion. [...]
