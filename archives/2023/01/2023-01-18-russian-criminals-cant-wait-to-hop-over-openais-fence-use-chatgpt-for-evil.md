Title: Russian criminals can't wait to hop over OpenAI's fence, use ChatGPT for evil
Date: 2023-01-18T00:01:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-18-russian-criminals-cant-wait-to-hop-over-openais-fence-use-chatgpt-for-evil

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/18/russia_openai_chatgpt_workarounds/){:target="_blank" rel="noopener"}

> Scriptkiddies rush to machine intelligence to make up for lack in skills Cybercriminals are famously fast adopters of new tools for nefarious purposes, and ChatGPT is no different in that regard.... [...]
