Title: UK warns of increased attacks from Russian, Iranian hackers
Date: 2023-01-26T12:19:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-26-uk-warns-of-increased-attacks-from-russian-iranian-hackers

[Source](https://www.bleepingcomputer.com/news/security/uk-warns-of-increased-attacks-from-russian-iranian-hackers/){:target="_blank" rel="noopener"}

> The U.K. National Cyber Security Centre (NCSC) has issued a warning of Russian and Iranian state-sponsored hackers increasingly targeting organizations and individuals. [...]
