Title: Meta to fight €390 million fine for breaching EU data privacy laws
Date: 2023-01-04T10:57:07-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-04-meta-to-fight-390-million-fine-for-breaching-eu-data-privacy-laws

[Source](https://www.bleepingcomputer.com/news/security/meta-to-fight-390-million-fine-for-breaching-eu-data-privacy-laws/){:target="_blank" rel="noopener"}

> The Irish Data Protection Commission (DPC) has fined Meta a total of €390 million after finding that it forced Facebook and Instagram users to consent to personal data processing for targeted advertising. [...]
