Title: AI and Political Lobbying
Date: 2023-01-18T12:19:28+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence;ChatGPT;laws
Slug: 2023-01-18-ai-and-political-lobbying

[Source](https://www.schneier.com/blog/archives/2023/01/ai-and-political-lobbying.html){:target="_blank" rel="noopener"}

> Launched just weeks ago, ChatGPT is already threatening to upend how we draft everyday communications like emails, college essays and myriad other forms of writing. Created by the company OpenAI, ChatGPT is a chatbot that can automatically respond to written prompts in a manner that is sometimes eerily close to human. But for all the consternation over the potential for humans to be replaced by machines in formats like poetry and sitcom scripts, a far greater threat looms: artificial intelligence replacing humans in the democratic processes—not through voting, but through lobbying. ChatGPT could automatically compose comments submitted in regulatory processes. [...]
