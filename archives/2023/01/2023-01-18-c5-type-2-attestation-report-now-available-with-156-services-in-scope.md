Title: C5 Type 2 attestation report now available with 156 services in scope
Date: 2023-01-18T19:36:10+00:00
Author: Julian Herlinghaus
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Auditing;AWS security;C5;Compliance;Security;Security Blog
Slug: 2023-01-18-c5-type-2-attestation-report-now-available-with-156-services-in-scope

[Source](https://aws.amazon.com/blogs/security/c5-type-2-attestation-report-now-available-with-156-services-in-scope/){:target="_blank" rel="noopener"}

> We continue to expand the scope of our assurance programs at Amazon Web Services (AWS), and we are pleased to announce that AWS has successfully completed the 2022 Cloud Computing Compliance Controls Catalogue (C5) attestation cycle with 156 services in scope. This alignment with C5 requirements demonstrates our ongoing commitment to adhere to the heightened [...]
