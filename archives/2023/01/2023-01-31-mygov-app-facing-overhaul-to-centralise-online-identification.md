Title: MyGov app facing overhaul to centralise online identification
Date: 2023-01-31T07:14:21+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Australia news;Data and computer security;Technology;Australian politics;Katy Gallagher (Australian politician);Bill Shorten;Centrelink
Slug: 2023-01-31-mygov-app-facing-overhaul-to-centralise-online-identification

[Source](https://www.theguardian.com/australia-news/2023/jan/31/mygov-app-facing-overhaul-to-centralise-online-identification){:target="_blank" rel="noopener"}

> Audit review of government tool housing Medicare and Centrelink services recommends significant changes Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast The federal government’s much-maligned myGov app faces an overhaul and could soon be used for online identity verification, as well as an increasing number of government services. An audit review, undertaken by former Telstra chief executive David Thodey and a panel of experts appointed by the Albanese government in September last year, recommended a significant overhaul of myGov, which houses Medicare and Centrelink. Sign [...]
