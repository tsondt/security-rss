Title: Logfile management is no fun. Now it's a nightmare thanks to critical-rated VMware flaws
Date: 2023-01-25T02:45:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-25-logfile-management-is-no-fun-now-its-a-nightmare-thanks-to-critical-rated-vmware-flaws

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/25/critical_vmware_flaws/){:target="_blank" rel="noopener"}

> You know the drill: patch before criminals use these bugs in vRealize to sniff your systems VMware has issued fixes for four vulnerabilities, including two critical 9.8-rated remote code execution bugs, in its vRealize Log Insight software.... [...]
