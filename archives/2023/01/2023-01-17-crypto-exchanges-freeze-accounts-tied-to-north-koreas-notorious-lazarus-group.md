Title: Crypto exchanges freeze accounts tied to North Korea’s notorious Lazarus Group
Date: 2023-01-17T06:29:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-01-17-crypto-exchanges-freeze-accounts-tied-to-north-koreas-notorious-lazarus-group

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/17/crypto_exchanges_freeze_lazarus_group/){:target="_blank" rel="noopener"}

> Well whaddya know, the crypto ecosystem did the right thing by stiffing the WannaCry bandits Two cryptocurrency exchanges have frozen accounts identified as having been used by North Korea’s notorious Lazarus Group.... [...]
