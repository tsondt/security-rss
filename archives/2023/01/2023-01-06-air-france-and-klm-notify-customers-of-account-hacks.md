Title: Air France and KLM notify customers of account hacks
Date: 2023-01-06T15:21:53-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-06-air-france-and-klm-notify-customers-of-account-hacks

[Source](https://www.bleepingcomputer.com/news/security/air-france-and-klm-notify-customers-of-account-hacks/){:target="_blank" rel="noopener"}

> Air France and KLM have informed Flying Blue customers that some of their personal information was exposed after their accounts were breached. [...]
