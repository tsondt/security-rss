Title: Introducing Security Command Center’s project-level, pay-as-you-go options
Date: 2023-01-30T17:00:00+00:00
Author: Anoop Kapoor
Category: GCP Security
Tags: Identity & Security
Slug: 2023-01-30-introducing-security-command-centers-project-level-pay-as-you-go-options

[Source](https://cloud.google.com/blog/products/identity-security/announcing-security-command-centers-project-level-pay-as-you-go-options/){:target="_blank" rel="noopener"}

> One of the toughest challenges security teams can face occurs when IT leaders recognize the urgency to secure their organization’s cloud workloads and data, but operational hurdles create delays that can leave resources vulnerable to cyberattacks. Giving security teams the flexibility to rapidly apply security controls to their most sensitive applications can help ensure that digital transformations occur as quickly and safely as possible. To help our customers apply protection quickly, we’re introducing two new capabilities and a new pricing model for Security Command Center (SCC). SCC is our built-in security and risk management solution that helps security and governance [...]
