Title: Microsoft: Cuba ransomware hacking Exchange servers via OWASSRF flaw
Date: 2023-01-12T14:53:02-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-01-12-microsoft-cuba-ransomware-hacking-exchange-servers-via-owassrf-flaw

[Source](https://www.bleepingcomputer.com/news/security/microsoft-cuba-ransomware-hacking-exchange-servers-via-owassrf-flaw/){:target="_blank" rel="noopener"}

> Microsoft says Cuba ransomware threat actors are hacking their way into victims' networks via Microsoft Exchange servers unpatched against a critical server-side request forgery (SSRF) vulnerability also exploited in Play ransomware attacks. [...]
