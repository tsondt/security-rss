Title: Euro-cops shut down crypto scam that bilked millions from unwitting punters
Date: 2023-01-13T06:30:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-13-euro-cops-shut-down-crypto-scam-that-bilked-millions-from-unwitting-punters

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/13/europol_crypto_investment_arrests/){:target="_blank" rel="noopener"}

> If the investment opportunity sounds too good to be true... European cops arrested 15 suspected scammers and shut down a multi-country network of call centers selling fake cryptocurrency that law enforcement said stole upwards of hundreds of million euros from victims.... [...]
