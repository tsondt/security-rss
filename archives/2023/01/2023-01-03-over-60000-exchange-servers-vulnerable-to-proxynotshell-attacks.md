Title: Over 60,000 Exchange servers vulnerable to ProxyNotShell attacks
Date: 2023-01-03T15:51:05-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-03-over-60000-exchange-servers-vulnerable-to-proxynotshell-attacks

[Source](https://www.bleepingcomputer.com/news/security/over-60-000-exchange-servers-vulnerable-to-proxynotshell-attacks/){:target="_blank" rel="noopener"}

> More than 60,000 Microsoft Exchange servers exposed online are yet to be patched against the CVE-2022-41082 remote code execution (RCE) vulnerability, one of the two security flaws targeted by ProxyNotShell exploits. [...]
