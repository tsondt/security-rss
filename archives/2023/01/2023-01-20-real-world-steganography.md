Title: Real-World Steganography
Date: 2023-01-20T12:25:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;China;espionage;steganography
Slug: 2023-01-20-real-world-steganography

[Source](https://www.schneier.com/blog/archives/2023/01/real-world-steganography.html){:target="_blank" rel="noopener"}

> From an article about Zheng Xiaoqing, an American convicted of spying for China: According to a Department of Justice (DOJ) indictment, the US citizen hid confidential files stolen from his employers in the binary code of a digital photograph of a sunset, which Mr Zheng then mailed to himself. [...]
