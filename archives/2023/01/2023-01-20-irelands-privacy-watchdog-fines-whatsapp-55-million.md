Title: Ireland’s privacy watchdog fines WhatsApp €5.5 million
Date: 2023-01-20T17:15:12+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2023-01-20-irelands-privacy-watchdog-fines-whatsapp-55-million

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/20/irelands_data_protection_watchdog_fines/){:target="_blank" rel="noopener"}

> You’ve got 6 months to get into compliance, it tells yak-yak app Ireland's data protection authority has fined WhatsApp Ireland €5.5 million for breaches of the GDPR relating to its service and told it comply with data processing laws within six months.... [...]
