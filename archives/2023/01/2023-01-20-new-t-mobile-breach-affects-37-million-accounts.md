Title: New T-Mobile Breach Affects 37 Million Accounts
Date: 2023-01-20T04:09:22+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;Latest Warnings;The Coming Storm;2023 T-Mobile Breach;Optus;T-Mobile breach
Slug: 2023-01-20-new-t-mobile-breach-affects-37-million-accounts

[Source](https://krebsonsecurity.com/2023/01/new-t-mobile-breach-affects-37-million-accounts/){:target="_blank" rel="noopener"}

> T-Mobile today disclosed a data breach affecting tens of millions of customer accounts, its second major data exposure in as many years. In a filing with federal regulators, T-Mobile said an investigation determined that someone abused its systems to harvest subscriber data tied to approximately 37 million current customer accounts. Image: customink.com In a filing today with the U.S. Securities and Exchange Commission, T-Mobile said a “bad actor” abused an application programming interface (API) to hoover up data on roughly 37 million current postpaid and prepaid customer accounts. The data stolen included customer name, billing address, email, phone number, date [...]
