Title: Royal Mail halts international services after cyberattack
Date: 2023-01-11T12:13:31-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-11-royal-mail-halts-international-services-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/royal-mail-halts-international-services-after-cyberattack/){:target="_blank" rel="noopener"}

> The Royal Mail, UK's leading mail delivery service, has stopped its international shipping services due to "severe service disruption" caused by what it described as a "cyber incident." [...]
