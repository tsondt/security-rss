Title: Zacks Investment Research data breach affects 820,000 clients
Date: 2023-01-25T13:45:11-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-25-zacks-investment-research-data-breach-affects-820000-clients

[Source](https://www.bleepingcomputer.com/news/security/zacks-investment-research-data-breach-affects-820-000-clients/){:target="_blank" rel="noopener"}

> Hackers breached Zacks Investment Research (Zacks) company last year and gained access to personal and sensitive information belonging to 820,000 customers. [...]
