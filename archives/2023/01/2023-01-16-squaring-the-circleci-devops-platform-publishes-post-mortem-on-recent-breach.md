Title: Squaring the CircleCI: DevOps platform publishes post-mortem on recent breach
Date: 2023-01-16T16:07:32+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-16-squaring-the-circleci-devops-platform-publishes-post-mortem-on-recent-breach

[Source](https://portswigger.net/daily-swig/squaring-the-circleci-devops-platform-publishes-post-mortem-on-recent-breach){:target="_blank" rel="noopener"}

> How the build pipeline was compromised [...]
