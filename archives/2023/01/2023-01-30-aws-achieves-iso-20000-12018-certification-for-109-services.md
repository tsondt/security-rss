Title: AWS achieves ISO 20000-1:2018 certification for 109 services
Date: 2023-01-30T18:47:38+00:00
Author: Rodrigo Fiuza
Category: AWS Security
Tags: Announcements;Security, Identity, & Compliance;Compliance;ISO 20000;Milan;MXP;Public Sector;Security Blog;Service Management
Slug: 2023-01-30-aws-achieves-iso-20000-12018-certification-for-109-services

[Source](https://aws.amazon.com/blogs/security/aws-achieves-iso-20000-12018-certification-for-109-services/){:target="_blank" rel="noopener"}

> We continue to expand the scope of our assurance programs at Amazon Web Services (AWS) and are pleased to announce that AWS Regions and AWS Edge locations are now certified by the International Organization for Standardization (ISO) 20000-1:2018 standard. This certification demonstrates our continuous commitment to adhere to the heightened expectations for cloud service providers. [...]
