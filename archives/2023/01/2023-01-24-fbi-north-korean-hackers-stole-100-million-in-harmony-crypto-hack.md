Title: FBI: North Korean hackers stole $100 million in Harmony crypto hack
Date: 2023-01-24T09:49:59-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-01-24-fbi-north-korean-hackers-stole-100-million-in-harmony-crypto-hack

[Source](https://www.bleepingcomputer.com/news/security/fbi-north-korean-hackers-stole-100-million-in-harmony-crypto-hack/){:target="_blank" rel="noopener"}

> The FBI has concluded its investigation on the $100 million worth of ETH heist that hit Harmony Horizon in June 2022 and validated that the hackers responsible for it are the Lazarus group and APT38. [...]
