Title: Auth0 fixes RCE flaw in JsonWebToken library used by 22,000 projects
Date: 2023-01-09T13:06:40-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-09-auth0-fixes-rce-flaw-in-jsonwebtoken-library-used-by-22000-projects

[Source](https://www.bleepingcomputer.com/news/security/auth0-fixes-rce-flaw-in-jsonwebtoken-library-used-by-22-000-projects/){:target="_blank" rel="noopener"}

> Auth0 fixed a remote code execution vulnerability in the immensely popular 'JsonWebToken' open-source library used by over 22,000 projects and downloaded over 36 million times per month on NPM. [...]
