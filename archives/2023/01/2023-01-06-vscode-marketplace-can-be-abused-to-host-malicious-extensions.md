Title: VSCode Marketplace can be abused to host malicious extensions
Date: 2023-01-06T14:11:45-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Microsoft;Security;Software
Slug: 2023-01-06-vscode-marketplace-can-be-abused-to-host-malicious-extensions

[Source](https://www.bleepingcomputer.com/news/microsoft/vscode-marketplace-can-be-abused-to-host-malicious-extensions/){:target="_blank" rel="noopener"}

> Threat analysts at AquaSec have experimented with the security of VSCode Marketplace and found that it's surprisingly easy to upload malicious extensions from accounts that appear verified on the platform. [...]
