Title: Microsoft Patch Tuesday, January 2023 Edition
Date: 2023-01-10T22:28:55+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;Security Tools;Time to Patch;CVE-2023-21563;CVE-2023-21674;CVE-2023-21678;CVE-2023-21743;CVE-2023-21745;CVE-2023-21762;Dustin Childs;Microsoft BitLocker;Satnam Narang;Tenable
Slug: 2023-01-10-microsoft-patch-tuesday-january-2023-edition

[Source](https://krebsonsecurity.com/2023/01/microsoft-patch-tuesday-january-2023-edition/){:target="_blank" rel="noopener"}

> Microsoft today released updates to fix nearly 100 security flaws in its Windows operating systems and other software. Highlights from the first Patch Tuesday of 2023 include a zero-day vulnerability in Windows, printer software flaws reported by the U.S. National Security Agency, and a critical Microsoft SharePoint Server bug that allows a remote, unauthenticated attacker to make an anonymous connection. At least 11 of the patches released today are rated “Critical” by Microsoft, meaning they could be exploited by malware or malcontents to seize remote control over vulnerable Windows systems with little or no help from users. Of particular concern [...]
