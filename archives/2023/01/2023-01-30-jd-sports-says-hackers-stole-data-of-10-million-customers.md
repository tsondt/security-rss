Title: JD Sports says hackers stole data of 10 million customers
Date: 2023-01-30T10:55:41-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-30-jd-sports-says-hackers-stole-data-of-10-million-customers

[Source](https://www.bleepingcomputer.com/news/security/jd-sports-says-hackers-stole-data-of-10-million-customers/){:target="_blank" rel="noopener"}

> UK sports apparel chain JD Sports is warning customers of a data breach after a server was hacked that contained online order information for 10 million customers. [...]
