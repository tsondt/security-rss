Title: Deserialized web security roundup: ‘Catastrophic cyber events’, another T-Mobile breach, more LastPass problems
Date: 2023-01-27T16:48:16+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-27-deserialized-web-security-roundup-catastrophic-cyber-events-another-t-mobile-breach-more-lastpass-problems

[Source](https://portswigger.net/daily-swig/deserialized-web-security-roundup-catastrophic-cyber-events-another-t-mobile-breach-more-lastpass-problems){:target="_blank" rel="noopener"}

> Your fortnightly rundown of AppSec vulnerabilities, new hacking techniques, and other cybersecurity news [...]
