Title: Ukraine: Sandworm hackers hit news agency with 5 data wipers
Date: 2023-01-27T13:10:49-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-27-ukraine-sandworm-hackers-hit-news-agency-with-5-data-wipers

[Source](https://www.bleepingcomputer.com/news/security/ukraine-sandworm-hackers-hit-news-agency-with-5-data-wipers/){:target="_blank" rel="noopener"}

> The Ukrainian Computer Emergency Response Team (CERT-UA) found a cocktail of five different data-wiping malware strains deployed on the network of the country's national news agency (Ukrinform) on January 17th. [...]
