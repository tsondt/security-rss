Title: Breaking RSA with a Quantum Computer
Date: 2023-01-03T17:38:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;China;cryptanalysis;cryptography;quantum computing;RSA
Slug: 2023-01-03-breaking-rsa-with-a-quantum-computer

[Source](https://www.schneier.com/blog/archives/2023/01/breaking-rsa-with-a-quantum-computer.html){:target="_blank" rel="noopener"}

> A group of Chinese researchers have just published a paper claiming that they can—although they have not yet done so—break 2048-bit RSA. This is something to take seriously. It might not be correct, but it’s not obviously wrong. We have long known from Shor’s algorithm that factoring with a quantum computer is easy. But it takes a big quantum computer, on the orders of millions of qbits, to factor anything resembling the key sizes we use today. What the researchers have done is combine classical lattice reduction factoring techniques with a quantum approximate optimization algorithm. This means that they only [...]
