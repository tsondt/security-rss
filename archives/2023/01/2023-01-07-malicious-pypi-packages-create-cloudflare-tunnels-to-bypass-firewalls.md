Title: Malicious PyPi packages create CloudFlare Tunnels to bypass firewalls
Date: 2023-01-07T10:12:06-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-07-malicious-pypi-packages-create-cloudflare-tunnels-to-bypass-firewalls

[Source](https://www.bleepingcomputer.com/news/security/malicious-pypi-packages-create-cloudflare-tunnels-to-bypass-firewalls/){:target="_blank" rel="noopener"}

> Six malicious packages on PyPI, the Python Package Index, were found installing information-stealing and RAT (remote access trojan) malware while using Cloudflare Tunnel to bypass firewall restrictions for remote access. [...]
