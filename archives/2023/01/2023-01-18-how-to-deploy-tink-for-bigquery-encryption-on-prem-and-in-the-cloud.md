Title: How to deploy Tink for BigQuery encryption on-prem and in the cloud
Date: 2023-01-18T17:00:00+00:00
Author: Oscar Pulido
Category: GCP Security
Tags: Identity & Security;Data Analytics
Slug: 2023-01-18-how-to-deploy-tink-for-bigquery-encryption-on-prem-and-in-the-cloud

[Source](https://cloud.google.com/blog/products/data-analytics/how-to-deploy-tink-for-bigquery-encryption-in-the-cloud-and-on-prem/){:target="_blank" rel="noopener"}

> Data security is a key focus for organizations moving their data warehouses from on-premises to cloud-first systems, such as BigQuery. In addition to storage-level encryption, whether using Google-managed or customer-managed keys, BigQuery also provides column-level encryption. Using BigQuery's SQL AEAD functions, organizations can enforce a more granular level of encryption to help protect sensitive customer data, such as government identity or credit card numbers, and help comply with security requirements. While BigQuery provides column-level encryption in the cloud, many organizations operate in hybrid-cloud environments. To prevent a scenario where data needs to be decrypted and re-encrypted each time it moves [...]
