Title: Microsoft Defender can now isolate compromised Linux endpoints
Date: 2023-01-31T03:14:26-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-01-31-microsoft-defender-can-now-isolate-compromised-linux-endpoints

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-defender-can-now-isolate-compromised-linux-endpoints/){:target="_blank" rel="noopener"}

> Microsoft announced today that it added device isolation support via Microsoft Defender for Endpoint (MDE) on onboarded Linux devices. [...]
