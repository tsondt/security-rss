Title: Royal Mail, cops probe 'cyber incident' that's knackered international mail
Date: 2023-01-11T22:57:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-11-royal-mail-cops-probe-cyber-incident-thats-knackered-international-mail

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/11/royal_mail_uk_cyber_incident/){:target="_blank" rel="noopener"}

> Don't go postal and call it a cyberattack because nobody knows (yet) what knocked out key system Royal Mail confirmed a "cyber incident" has disrupted its ability to send letters and packages abroad, and also caused some delays on post coming into the UK.... [...]
