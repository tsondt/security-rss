Title: How to query and visualize Macie sensitive data discovery results with Athena and QuickSight
Date: 2023-01-06T20:08:22+00:00
Author: Keith Rozario
Category: AWS Security
Tags: Intermediate (200);Security;Security, Identity, & Compliance;Technical How-to;Amazon Athena;Amazon Macie;Amazon QuickSight;Amazon S3;Data security investigations;Github;PHI;PII;Security Blog;Sensitive Data Discovery
Slug: 2023-01-06-how-to-query-and-visualize-macie-sensitive-data-discovery-results-with-athena-and-quicksight

[Source](https://aws.amazon.com/blogs/security/how-to-query-and-visualize-macie-sensitive-data-discovery-results-with-athena-and-quicksight/){:target="_blank" rel="noopener"}

> Amazon Macie is a fully managed data security service that uses machine learning and pattern matching to help you discover and protect sensitive data in Amazon Simple Storage Service (Amazon S3). With Macie, you can analyze objects in your S3 buckets to detect occurrences of sensitive data, such as personally identifiable information (PII), financial information, personal health information, and access credentials. [...]
