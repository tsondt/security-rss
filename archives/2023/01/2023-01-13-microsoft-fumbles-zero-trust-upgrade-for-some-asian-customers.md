Title: Microsoft fumbles zero trust upgrade for some Asian customers
Date: 2023-01-13T05:58:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-01-13-microsoft-fumbles-zero-trust-upgrade-for-some-asian-customers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/13/microsoft_gdap_double_byte_delays/){:target="_blank" rel="noopener"}

> Enhanced access privileges for partners choke on double-byte characters, contribute to global delays Microsoft has messed up a zero trust upgrade its service provider partners have been asked to implement for customers.... [...]
