Title: PayPal accounts breached in large-scale credential stuffing attack
Date: 2023-01-19T09:47:38-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-19-paypal-accounts-breached-in-large-scale-credential-stuffing-attack

[Source](https://www.bleepingcomputer.com/news/security/paypal-accounts-breached-in-large-scale-credential-stuffing-attack/){:target="_blank" rel="noopener"}

> PayPal is sending out notices of a data breach to thousands of users who had their accounts accessed by credential stuffing actors, resulting in the compromise of some personal data. [...]
