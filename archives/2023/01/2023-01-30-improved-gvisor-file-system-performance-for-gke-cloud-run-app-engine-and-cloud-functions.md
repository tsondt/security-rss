Title: Improved gVisor file system performance for GKE, Cloud Run, App Engine and Cloud Functions
Date: 2023-01-30T17:00:00+00:00
Author: Fabricio Voznika
Category: GCP Security
Tags: Identity & Security;Serverless;Containers & Kubernetes
Slug: 2023-01-30-improved-gvisor-file-system-performance-for-gke-cloud-run-app-engine-and-cloud-functions

[Source](https://cloud.google.com/blog/products/containers-kubernetes/gvisor-file-system-improvements-for-gke-and-serverless/){:target="_blank" rel="noopener"}

> Flexible application architectures, CI/CD pipelines, and container workloads often run untrusted code and hence should be isolated from sensitive infrastructure. One common solution has been to deploy defense-in-depth products (like GKE Sandbox which uses gVisor ) to isolate workloads with an extra layer of protection. Google Cloud’s serverless products (App Engine, Cloud Run, Cloud Functions) also use gVisor to sandbox application workloads. However, adding layers of defense can also introduce new performance challenges. We discovered one such challenge when gVisor’s user space kernel required several operations to walk file system paths. To address this and significantly increase gVisor performance, we [...]
