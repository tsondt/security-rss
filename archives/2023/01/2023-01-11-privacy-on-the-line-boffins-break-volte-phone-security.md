Title: Privacy on the line: Boffins break VoLTE phone security
Date: 2023-01-11T01:58:18+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-01-11-privacy-on-the-line-boffins-break-volte-phone-security

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/11/volte_phone_security/){:target="_blank" rel="noopener"}

> Call metadata can be ferreted out Boffins based in China and the UK have devised a telecom network attack that can expose call metadata during VoLTE/VoNR conversations.... [...]
