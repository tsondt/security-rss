Title: Best Kept Security Secrets: How VPC Service Controls can help build defense in depth
Date: 2023-01-05T17:00:00+00:00
Author: Seth Rosenblatt
Category: GCP Security
Tags: Identity & Security
Slug: 2023-01-05-best-kept-security-secrets-how-vpc-service-controls-can-help-build-defense-in-depth

[Source](https://cloud.google.com/blog/products/identity-security/vpc-service-controls-add-a-robust-security-layer/){:target="_blank" rel="noopener"}

> While cloud security skeptics might believe that data in the cloud is just one access configuration mistake away from a breach, the reality is that a well-designed set of defense in depth controls can help minimize the risk of configuration mistakes and other security issues. Our Virtual Private Cloud (VPC) Service Controls can play a vital role in creating an additional layer of security while also making it easier to manage your data in a way that most cloud services can't do today. Organizations across industries and business models use cloud services for activities such as processing their data, performing [...]
