Title: How Palo Alto Networks and Google Cloud help secure the future of omnichannel retail
Date: 2023-01-11T17:00:00+00:00
Author: Amit Chetal
Category: GCP Security
Tags: Partners;Identity & Security;Retail
Slug: 2023-01-11-how-palo-alto-networks-and-google-cloud-help-secure-the-future-of-omnichannel-retail

[Source](https://cloud.google.com/blog/topics/retail/keeping-omnichannel-retail-secure-with-panw-and-google-cloud/){:target="_blank" rel="noopener"}

> Editor’s note : To kick off the new year and in preparation for NRF The Big Show, we invited partners from across our retail ecosystem to share stories, best practices, and tips and tricks on how they are helping retailers transform during a time that continues to see tremendous change. Please enjoy this entry from our partner. The dramatic transformation of the retail industry has created a host of new security challenges. Store modernization continues to provide tremendous opportunities to create engaging experiences within connected stores, but success ultimately requires an entirely new approach to security. This is illustrated by [...]
