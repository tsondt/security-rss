Title: Google slays thousands of fake news vids posted by pro-China group Dragonbridge
Date: 2023-01-27T02:58:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-27-google-slays-thousands-of-fake-news-vids-posted-by-pro-china-group-dragonbridge

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/27/google_tag_dragonbridge_takedown/){:target="_blank" rel="noopener"}

> If you yell 'death to America' and no one watches the video, does it make a sound? Google's Threat Analysis Group (TAG) has burned more than 50,000 spammy fake news stories and other content posted by the pro-China 'Dragonbridge' gang.... [...]
