Title: The Week in Ransomware - January 13th 2023 - LockBit in the spotlight
Date: 2023-01-13T19:17:55-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-01-13-the-week-in-ransomware-january-13th-2023-lockbit-in-the-spotlight

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-january-13th-2023-lockbit-in-the-spotlight/){:target="_blank" rel="noopener"}

> The LockBit ransomware operation has again taken center stage in the ransomware news, as we learned yesterday they were behind the attack on Royal Mail. [...]
