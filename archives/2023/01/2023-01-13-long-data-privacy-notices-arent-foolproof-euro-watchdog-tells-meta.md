Title: Long data privacy notices aren't foolproof, Euro watchdog tells Meta
Date: 2023-01-13T11:30:06+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2023-01-13-long-data-privacy-notices-arent-foolproof-euro-watchdog-tells-meta

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/13/long_data_privacy_meta/){:target="_blank" rel="noopener"}

> As Meta reels from €390 million EU fine, the 'personalized ads' case might not be over, Max Schrem’s legal group says Lengthy privacy notices included in a social media platform's terms of service can do little to help it comply with transparency requirements under European law, according to recently revealed documents from a case in which Meta was fined €390 million ($414 million).... [...]
