Title: Canadian owes bosses for 'time theft' after work-tracking app sinks tribunal bid
Date: 2023-01-13T18:43:07+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2023-01-13-canadian-owes-bosses-for-time-theft-after-work-tracking-app-sinks-tribunal-bid

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/13/laptop_tracking_software_led_to/){:target="_blank" rel="noopener"}

> She hoped to score thousands but laptop app had other ideas A woman in Canada failed in her claim for wrongful dismissal due to evidence from software designed to track her work time activity.... [...]
