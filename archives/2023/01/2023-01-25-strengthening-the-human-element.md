Title: Strengthening the human element
Date: 2023-01-25T12:28:07+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-01-25-strengthening-the-human-element

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/25/strengthening_the_human_element/){:target="_blank" rel="noopener"}

> How to locate cybersecurity risks in remote working Webinar The implementation of lockdowns during the maelstrom of the Coronavirus pandemic led to fast track changes to traditional work practices. To meet the challenges of operating in a global emergency, businesses and organizations of every kind had to urgently find a way to keep operating.... [...]
