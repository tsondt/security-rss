Title: Reduce risk by implementing HttpOnly cookie authentication in Amazon API Gateway
Date: 2023-01-30T20:14:34+00:00
Author: Marc Borntraeger
Category: AWS Security
Tags: Advanced (300);Best Practices;Security, Identity, & Compliance;API Gateway;authentication;Http-Only cookie;OAuth2;OpenID Connect;Security;Security Blog
Slug: 2023-01-30-reduce-risk-by-implementing-httponly-cookie-authentication-in-amazon-api-gateway

[Source](https://aws.amazon.com/blogs/security/reduce-risk-by-implementing-httponly-cookie-authentication-in-amazon-api-gateway/){:target="_blank" rel="noopener"}

> Some web applications need to protect their authentication tokens or session IDs from cross-site scripting (XSS). It’s an Open Web Application Security Project (OWASP) best practice for session management to store secrets in the browsers’ cookie store with the HttpOnly attribute enabled. When cookies have the HttpOnly attribute set, the browser will prevent client-side JavaScript [...]
