Title: Ongoing Flipper Zero phishing attacks target infosec community
Date: 2023-01-03T17:26:41-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-03-ongoing-flipper-zero-phishing-attacks-target-infosec-community

[Source](https://www.bleepingcomputer.com/news/security/ongoing-flipper-zero-phishing-attacks-target-infosec-community/){:target="_blank" rel="noopener"}

> A new phishing campaign is exploiting the increasing interest of security community members towards Flipper Zero to steal their personal information and cryptocurrency. [...]
