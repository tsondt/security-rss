Title: US government announces third Hack The Pentagon challenge
Date: 2023-01-17T14:04:09+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-17-us-government-announces-third-hack-the-pentagon-challenge

[Source](https://portswigger.net/daily-swig/us-government-announces-third-hack-the-pentagon-challenge){:target="_blank" rel="noopener"}

> Ethical hackers and bug bounty hunters invited to test Department of Defense assets [...]
