Title: Gee, tanks: Russian hackers DDoS Germany for aiding Ukraine
Date: 2023-01-30T03:01:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-01-30-gee-tanks-russian-hackers-ddos-germany-for-aiding-ukraine

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/30/russian_hackers_ddos_germany/){:target="_blank" rel="noopener"}

> Also: a week of leaks; Riot Games says 'LoL' to source code ransom demands; and Yandex source also appears online in brief Russian hackers have proved yet again how quickly cyber attacks can be used to respond to global events with a series of DDoS attacks on German infrastructure and government websites in response to the country's plan to send tanks to Ukraine.... [...]
