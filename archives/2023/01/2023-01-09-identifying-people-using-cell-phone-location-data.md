Title: Identifying People Using Cell Phone Location Data
Date: 2023-01-09T12:14:01+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cell phones;geolocation;law enforcement;tracking
Slug: 2023-01-09-identifying-people-using-cell-phone-location-data

[Source](https://www.schneier.com/blog/archives/2023/01/identifying-people-using-cell-phone-location-data.html){:target="_blank" rel="noopener"}

> The two people who shut down four Washington power stations in December were arrested. This is the interesting part: Investigators identified Greenwood and Crahan almost immediately after the attacks took place by using cell phone data that allegedly showed both men in the vicinity of all four substations, according to court documents. Nowadays, it seems like an obvious thing to do—although the search is probably unconstitutional. But way back in 2012, the Canadian CSEC—that’s their NSA—did some top-secret work on this kind of thing. The document is part of the Snowden archive, and I wrote about it: The second application [...]
