Title: On Alec Baldwin’s Shooting
Date: 2023-01-26T12:08:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;courts;guns
Slug: 2023-01-26-on-alec-baldwins-shooting

[Source](https://www.schneier.com/blog/archives/2023/01/on-alec-baldwins-shooting.html){:target="_blank" rel="noopener"}

> We recently learned that Alec Baldwin is being charged with involuntary manslaughter for his accidental shooting on a movie set. I don’t know the details of the case, nor the intricacies of the law, but I have a question about movie props. Why was an actual gun used on the set? And why were actual bullets used on the set? Why wasn’t it a fake gun: plastic, or metal without a working barrel? Why does it have to fire blanks? Why can’t everyone just pretend, and let someone add the bang and the muzzle flash in post-production? Movies are filled [...]
