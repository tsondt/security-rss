Title: U.S. No Fly list shared on a hacking forum, government investigating
Date: 2023-01-30T07:00:00-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-01-30-us-no-fly-list-shared-on-a-hacking-forum-government-investigating

[Source](https://www.bleepingcomputer.com/news/security/us-no-fly-list-shared-on-a-hacking-forum-government-investigating/){:target="_blank" rel="noopener"}

> A U.S. No Fly list with over 1.5 million records of banned flyers and upwards of 250,000 'selectees' has been shared publicly on a hacking forum. BleepingComputer has confirmed, the list is the same TSA No Fly list that was discovered recently on an unsecured CommuteAir server. [...]
