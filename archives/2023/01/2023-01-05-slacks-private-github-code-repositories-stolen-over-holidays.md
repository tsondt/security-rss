Title: Slack's private GitHub code repositories stolen over holidays
Date: 2023-01-05T03:50:46-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2023-01-05-slacks-private-github-code-repositories-stolen-over-holidays

[Source](https://www.bleepingcomputer.com/news/security/slacks-private-github-code-repositories-stolen-over-holidays/){:target="_blank" rel="noopener"}

> Slack suffered a security incident over the holidays affecting some of its private GitHub code repositories. [...]
