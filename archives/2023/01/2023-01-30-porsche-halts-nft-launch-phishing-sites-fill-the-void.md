Title: Porsche halts NFT launch, phishing sites fill the void
Date: 2023-01-30T17:58:42-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-01-30-porsche-halts-nft-launch-phishing-sites-fill-the-void

[Source](https://www.bleepingcomputer.com/news/security/porsche-halts-nft-launch-phishing-sites-fill-the-void/){:target="_blank" rel="noopener"}

> Porsche cut its minting of a new NFT collection short after a dismal turnout and backlash from the crypto community, allowing threat actors to fill the void by creating phishing sites that steal digital assets from cryptocurrency wallets. [...]
