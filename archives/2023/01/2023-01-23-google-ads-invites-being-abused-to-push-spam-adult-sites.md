Title: Google Ads invites being abused to push spam, adult sites
Date: 2023-01-23T11:03:53-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security;Google
Slug: 2023-01-23-google-ads-invites-being-abused-to-push-spam-adult-sites

[Source](https://www.bleepingcomputer.com/news/security/google-ads-invites-being-abused-to-push-spam-adult-sites/){:target="_blank" rel="noopener"}

> Google Ads invites are being abused to deliver messages promoting spam and sex websites to users otherwise not necessarily using Google Ads or related products. [...]
