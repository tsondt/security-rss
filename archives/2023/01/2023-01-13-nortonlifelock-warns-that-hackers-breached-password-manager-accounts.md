Title: NortonLifeLock warns that hackers breached Password Manager accounts
Date: 2023-01-13T11:47:56-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-13-nortonlifelock-warns-that-hackers-breached-password-manager-accounts

[Source](https://www.bleepingcomputer.com/news/security/nortonlifelock-warns-that-hackers-breached-password-manager-accounts/){:target="_blank" rel="noopener"}

> Gen Digital, formerly Symantec Corporation and NortonLifeLock, is sending data breach notifications to customers, informing them that hackers have successfully breached Norton Password Manager accounts in credential-stuffing attacks. [...]
