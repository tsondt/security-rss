Title: Ransomware impacts over 200 govt, edu, healthcare orgs in 2022
Date: 2023-01-02T13:14:15-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-01-02-ransomware-impacts-over-200-govt-edu-healthcare-orgs-in-2022

[Source](https://www.bleepingcomputer.com/news/security/ransomware-impacts-over-200-govt-edu-healthcare-orgs-in-2022/){:target="_blank" rel="noopener"}

> Ransomware attacks in 2022 impacted more than 200 hundred larger organizations in the U.S. public sector in the government, educational, and healthcare verticals. [...]
