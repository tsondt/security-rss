Title: Hackers discover that vulnerabilities are rife in the auto industry
Date: 2023-01-11T17:31:10+00:00
Author: Jonathan M. Gitlin
Category: Ars Technica
Tags: Biz & IT;Cars;acura;BMW;car hacking;connected cars;cybersecurity;Ferrai;Ford;Genesis;hacking;honda;hyundai;Jaguar Land Rover;Kia;LoJack;Mercedes-Benz;Nissan;single sign-on;Sirius XM;Spireon;SSO;telematics
Slug: 2023-01-11-hackers-discover-that-vulnerabilities-are-rife-in-the-auto-industry

[Source](https://arstechnica.com/?p=1909119){:target="_blank" rel="noopener"}

> Enlarge (credit: Aurich Lawson | Getty Images) If you purchased a new car in the past few years, chances are good that it contains at least one embedded modem, which it uses to offer some connected services. The benefits, we've been told, are numerous and include convenience features like interior preheating on a cold morning, diagnostics that warn of failures before they happen, and safety features like teen driver monitoring. In some regions, connected cars are even mandatory, as in the European Union's eCall system. But if these systems sound like a potential security nightmare, that's because they often are. [...]
