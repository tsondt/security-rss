Title: Massive ad-fraud op dismantled after hitting millions of iOS devices
Date: 2023-01-21T10:06:12-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-21-massive-ad-fraud-op-dismantled-after-hitting-millions-of-ios-devices

[Source](https://www.bleepingcomputer.com/news/security/massive-ad-fraud-op-dismantled-after-hitting-millions-of-ios-devices/){:target="_blank" rel="noopener"}

> A massive ad fraud operation dubbed 'Vastflux' that spoofed more than 1,700 applications from 120 publishers, mostly for iOS, has been disrupted by security researchers at cybersecurity company HUMAN. [...]
