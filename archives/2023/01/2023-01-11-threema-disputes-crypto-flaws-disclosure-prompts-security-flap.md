Title: Threema disputes crypto flaws disclosure, prompts security flap
Date: 2023-01-11T15:41:31+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-11-threema-disputes-crypto-flaws-disclosure-prompts-security-flap

[Source](https://portswigger.net/daily-swig/threema-disputes-crypto-flaws-disclosure-prompts-security-flap){:target="_blank" rel="noopener"}

> ‘Condescending’ response to vulnerability disclosure angers infosec community [...]
