Title: QNAP fixes critical bug letting hackers inject malicious code
Date: 2023-01-30T12:25:13-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-30-qnap-fixes-critical-bug-letting-hackers-inject-malicious-code

[Source](https://www.bleepingcomputer.com/news/security/qnap-fixes-critical-bug-letting-hackers-inject-malicious-code/){:target="_blank" rel="noopener"}

> QNAP is warning customers to install QTS and QuTS firmware updates that fix a critical security vulnerability allowing remote attackers to inject malicious code on QNAP NAS devices. [...]
