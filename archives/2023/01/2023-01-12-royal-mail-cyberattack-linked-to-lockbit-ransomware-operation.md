Title: Royal Mail cyberattack linked to LockBit ransomware operation
Date: 2023-01-12T18:43:43-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-01-12-royal-mail-cyberattack-linked-to-lockbit-ransomware-operation

[Source](https://www.bleepingcomputer.com/news/security/royal-mail-cyberattack-linked-to-lockbit-ransomware-operation/){:target="_blank" rel="noopener"}

> A cyberattack on Royal Mail, UK's largest mail delivery service, has been linked to the LockBit ransomware operation. [...]
