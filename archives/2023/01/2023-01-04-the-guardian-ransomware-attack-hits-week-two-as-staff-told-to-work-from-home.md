Title: The Guardian ransomware attack hits week two as staff told to work from home
Date: 2023-01-04T20:00:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-04-the-guardian-ransomware-attack-hits-week-two-as-staff-told-to-work-from-home

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/04/guardian_ransomware_attack/){:target="_blank" rel="noopener"}

> UK data watchdog would like a word over failure to systems Long-standing British broadsheet The Guardian has told staff to continue working from home and notified the UK's data privacy watchdog about the security breach following a suspected ransomware attack before Christmas.... [...]
