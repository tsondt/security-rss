Title: Cybersecurity professionals upskill in Brazil and Mexico
Date: 2023-01-25T08:53:07+00:00
Author: Rob Jaques
Category: The Register
Tags: 
Slug: 2023-01-25-cybersecurity-professionals-upskill-in-brazil-and-mexico

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/25/cybersecurity_professionals_upskill_in_brazil/){:target="_blank" rel="noopener"}

> SANS Institute meets fast-growing demand for cyber security training in Latin America Sponsored Post The scale of cybersecurity threats facing Latin America was brought into focus by recently when it published details of NICKEL, a "China-based threat actor". The malware was used to attack global organisations with "a large amount of activity" targeting Central and South America, including Mexico and Brazil.... [...]
