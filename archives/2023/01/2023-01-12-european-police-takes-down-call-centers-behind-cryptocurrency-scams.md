Title: European police takes down call centers behind cryptocurrency scams
Date: 2023-01-12T12:25:37-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-01-12-european-police-takes-down-call-centers-behind-cryptocurrency-scams

[Source](https://www.bleepingcomputer.com/news/security/european-police-takes-down-call-centers-behind-cryptocurrency-scams/){:target="_blank" rel="noopener"}

> Multiple call centers across Europe controlled by a criminal organization involved in online investment fraud were taken down this week following a cross-border investigation started in June 2022. [...]
