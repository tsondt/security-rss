Title: Hackers push fake Pokemon NFT game to take over Windows devices
Date: 2023-01-08T10:05:10-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-08-hackers-push-fake-pokemon-nft-game-to-take-over-windows-devices

[Source](https://www.bleepingcomputer.com/news/security/hackers-push-fake-pokemon-nft-game-to-take-over-windows-devices/){:target="_blank" rel="noopener"}

> Threat actors are using a well-crafted Pokemon NFT card game website to distribute the NetSupport remote access tool and take control over victims' devices. [...]
