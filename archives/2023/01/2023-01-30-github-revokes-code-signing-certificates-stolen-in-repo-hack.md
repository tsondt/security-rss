Title: GitHub revokes code signing certificates stolen in repo hack
Date: 2023-01-30T13:27:03-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-30-github-revokes-code-signing-certificates-stolen-in-repo-hack

[Source](https://www.bleepingcomputer.com/news/security/github-revokes-code-signing-certificates-stolen-in-repo-hack/){:target="_blank" rel="noopener"}

> GitHub says that unknown attackers have stolen encrypted code-signing certificates for its Desktop and Atom applications after gaining access to some of its development and release planning repositories. [...]
