Title: Google nukes 50,000 accounts pushing Chinese disinformation
Date: 2023-01-26T13:17:19-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-26-google-nukes-50000-accounts-pushing-chinese-disinformation

[Source](https://www.bleepingcomputer.com/news/security/google-nukes-50-000-accounts-pushing-chinese-disinformation/){:target="_blank" rel="noopener"}

> Google's Threat Analysis Group terminated tens of thousands of accounts linked to a group known as "Dragonbridge" or "Spamouflage Dragon" that is disseminating pro-Chinese disinformation across multiple online platforms. [...]
