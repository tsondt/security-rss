Title: LAUSD says Vice Society ransomware gang stole contractors’ SSNs
Date: 2023-01-20T12:02:17-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-20-lausd-says-vice-society-ransomware-gang-stole-contractors-ssns

[Source](https://www.bleepingcomputer.com/news/security/lausd-says-vice-society-ransomware-gang-stole-contractors-ssns/){:target="_blank" rel="noopener"}

> Los Angeles Unified School District (LAUSD), the second-largest school district in the United States, says the Vice Society ransomware gang has stolen files containing contractors' personal information, including Social Security Numbers (SSNs). [...]
