Title: Friday Squid Blogging: Squid-Inspired Hydrogel
Date: 2023-01-27T22:59:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-01-27-friday-squid-blogging-squid-inspired-hydrogel

[Source](https://www.schneier.com/blog/archives/2023/01/friday-squid-blogging-squid-inspired-hydrogel.html){:target="_blank" rel="noopener"}

> Scientists have created a hydrogel “using squid mantle and creative chemistry.” As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
