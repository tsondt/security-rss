Title: Darknet drug markets move to custom Android apps for increased privacy
Date: 2023-01-09T15:13:05-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2023-01-09-darknet-drug-markets-move-to-custom-android-apps-for-increased-privacy

[Source](https://www.bleepingcomputer.com/news/security/darknet-drug-markets-move-to-custom-android-apps-for-increased-privacy/){:target="_blank" rel="noopener"}

> Online markets selling drugs and other illegal substances on the dark web have started to use custom Android apps for increased privacy and to evade law enforcement. [...]
