Title: Chick-fil-A investigates reports of hacked customer accounts
Date: 2023-01-06T17:15:14-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-06-chick-fil-a-investigates-reports-of-hacked-customer-accounts

[Source](https://www.bleepingcomputer.com/news/security/chick-fil-a-investigates-reports-of-hacked-customer-accounts/){:target="_blank" rel="noopener"}

> American fast-food restaurant chain Chick-fil-A is investigating what it described as "suspicious activity" linked to some of its customers' accounts. [...]
