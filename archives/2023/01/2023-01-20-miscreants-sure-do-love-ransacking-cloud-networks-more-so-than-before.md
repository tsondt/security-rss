Title: Miscreants sure do love ransacking cloud networks, more so than before
Date: 2023-01-20T06:27:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-01-20-miscreants-sure-do-love-ransacking-cloud-networks-more-so-than-before

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/20/cloud_networks_under_attack/){:target="_blank" rel="noopener"}

> Thanks for putting all your data in one basket As enterprises around the world continue to move to the cloud, cybercriminals are following right behind them.... [...]
