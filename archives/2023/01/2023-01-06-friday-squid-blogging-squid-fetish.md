Title: Friday Squid Blogging: Squid Fetish
Date: 2023-01-06T22:02:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-01-06-friday-squid-blogging-squid-fetish

[Source](https://www.schneier.com/blog/archives/2023/01/friday-squid-blogging-squid-fetish.html){:target="_blank" rel="noopener"}

> Seems that about 1.5% of people have a squid fetish. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
