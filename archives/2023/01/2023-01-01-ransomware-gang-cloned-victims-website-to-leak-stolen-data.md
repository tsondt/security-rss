Title: Ransomware gang cloned victim’s website to leak stolen data
Date: 2023-01-01T15:54:56-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-01-01-ransomware-gang-cloned-victims-website-to-leak-stolen-data

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-cloned-victim-s-website-to-leak-stolen-data/){:target="_blank" rel="noopener"}

> The ALPHV ransomware operators have gotten creative with their extortion tactic and, in at least one case, created a replica of the victim's site to publish stolen data on it. [...]
