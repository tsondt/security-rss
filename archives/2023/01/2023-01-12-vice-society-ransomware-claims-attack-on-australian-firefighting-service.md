Title: Vice Society ransomware claims attack on Australian firefighting service
Date: 2023-01-12T11:31:36-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-12-vice-society-ransomware-claims-attack-on-australian-firefighting-service

[Source](https://www.bleepingcomputer.com/news/security/vice-society-ransomware-claims-attack-on-australian-firefighting-service/){:target="_blank" rel="noopener"}

> Australia's Fire Rescue Victoria has disclosed a data breach caused by a December cyberattack that is now claimed by the Vice Society ransomware gang. [...]
