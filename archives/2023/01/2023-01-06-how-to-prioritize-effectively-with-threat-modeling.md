Title: How to prioritize effectively with threat modeling
Date: 2023-01-06T17:02:19+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2023-01-06-how-to-prioritize-effectively-with-threat-modeling

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/06/how_to_prioritize_effectively_with/){:target="_blank" rel="noopener"}

> Crisis? What Crisis! Webinar How does your security team prioritize work? When a new attack from a state actor hits the news, do you know if your team should drop everything to hunt for IOCs? Do you understand your security control coverage for the threat actors that might target your organization? Recently, the Red Canary corporate security team asked itself these questions when it was creating its own threat model.... [...]
