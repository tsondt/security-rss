Title: Amazon S3 will now encrypt all new data with AES-256 by default
Date: 2023-01-06T11:32:41-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Technology
Slug: 2023-01-06-amazon-s3-will-now-encrypt-all-new-data-with-aes-256-by-default

[Source](https://www.bleepingcomputer.com/news/security/amazon-s3-will-now-encrypt-all-new-data-with-aes-256-by-default/){:target="_blank" rel="noopener"}

> Amazon Simple Storage Service (S3) will now automatically encrypt all new objects added on buckets on the server side, using AES-256 by default. [...]
