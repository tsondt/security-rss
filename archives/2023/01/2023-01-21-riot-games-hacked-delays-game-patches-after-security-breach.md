Title: Riot Games hacked, delays game patches after security breach
Date: 2023-01-21T14:54:41-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-21-riot-games-hacked-delays-game-patches-after-security-breach

[Source](https://www.bleepingcomputer.com/news/security/riot-games-hacked-delays-game-patches-after-security-breach/){:target="_blank" rel="noopener"}

> Riot Games, the video game developer and publisher behind League of Legends and Valorant, says it will delay game patches after its development environment was compromised. [...]
