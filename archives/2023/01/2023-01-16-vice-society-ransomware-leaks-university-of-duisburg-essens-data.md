Title: Vice Society ransomware leaks University of Duisburg-Essen’s data
Date: 2023-01-16T14:22:08-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Education
Slug: 2023-01-16-vice-society-ransomware-leaks-university-of-duisburg-essens-data

[Source](https://www.bleepingcomputer.com/news/security/vice-society-ransomware-leaks-university-of-duisburg-essen-s-data/){:target="_blank" rel="noopener"}

> The Vice Society ransomware gang has claimed responsibility for the November 2022 cyberattack that forced the University of Duisburg-Essen (UDE) to reconstruct its IT infrastructure, a process that's still ongoing. [...]
