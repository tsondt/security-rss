Title: Finally, ransomware victims are refusing to pay up
Date: 2023-01-19T22:30:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-01-19-finally-ransomware-victims-are-refusing-to-pay-up

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/19/ransomware_payments_down/){:target="_blank" rel="noopener"}

> Near 50% drop in extorted dosh... or so it says here The amount of money paid to ransomware attackers dropped significantly in 2022, and not because the number of attacks fell.... [...]
