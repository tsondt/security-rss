Title: Identity Thieves Bypassed Experian Security to View Credit Reports
Date: 2023-01-09T14:05:15+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Ne'er-Do-Well News;Web Fraud 2.0;annualcreditreport.com;Experian;Jenya Kushnir;Sen. Ron Wyden
Slug: 2023-01-09-identity-thieves-bypassed-experian-security-to-view-credit-reports

[Source](https://krebsonsecurity.com/2023/01/identity-thieves-bypassed-experian-security-to-view-credit-reports/){:target="_blank" rel="noopener"}

> Identity thieves have been exploiting a glaring security weakness in the website of Experian, one of the big three consumer credit reporting bureaus. Normally, Experian requires that those seeking a copy of their credit report successfully answer several multiple choice questions about their financial history. But until the end of 2022, Experian’s website allowed anyone to bypass these questions and go straight to the consumer’s report. All that was needed was the person’s name, address, birthday and Social Security number. The vulnerability in Experian’s website was exploitable after one applied to see their credit file via annualcreditreport.com. In December, KrebsOnSecurity [...]
