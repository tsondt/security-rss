Title: Rackspace: Customer email data accessed in ransomware attack
Date: 2023-01-05T17:58:30-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-05-rackspace-customer-email-data-accessed-in-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/rackspace-customer-email-data-accessed-in-ransomware-attack/){:target="_blank" rel="noopener"}

> Rackspace revealed on Thursday that attackers behind last month's incident accessed some of its customers' Personal Storage Table (PST) files which can contain a wide range of information, including emails, calendar data, contacts, and tasks. [...]
