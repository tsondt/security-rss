Title: MetaMask warns of new 'Address Poisoning' cryptocurrency scam
Date: 2023-01-12T13:29:10-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-01-12-metamask-warns-of-new-address-poisoning-cryptocurrency-scam

[Source](https://www.bleepingcomputer.com/news/security/metamask-warns-of-new-address-poisoning-cryptocurrency-scam/){:target="_blank" rel="noopener"}

> Cryptocurrency wallet provider MetaMask is warning users of a new scam called 'Address Poisoning' used to trick users into sending funds to a scammer rather than an intended recipient. [...]
