Title: Ruby on Rails apps vulnerable to data theft through Ransack search
Date: 2023-01-26T17:27:49+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-26-ruby-on-rails-apps-vulnerable-to-data-theft-through-ransack-search

[Source](https://portswigger.net/daily-swig/ruby-on-rails-apps-vulnerable-to-data-theft-through-ransack-search){:target="_blank" rel="noopener"}

> Several applications were vulnerable to brute-force attacks; hundreds more could be at risk [...]
