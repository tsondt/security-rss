Title: Health insurer Aflac blames US partner for leak of Japanese cancer policy info
Date: 2023-01-11T03:29:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-01-11-health-insurer-aflac-blames-us-partner-for-leak-of-japanese-cancer-policy-info

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/11/japan_aflac_zurich_data_breaches/){:target="_blank" rel="noopener"}

> Zurich’s Japanese outpost also leaks a couple of million records Global insurer Aflac's Japanese branch has revealed that personal data describing more than three million customers of its cancer insurance product has been leaked online.... [...]
