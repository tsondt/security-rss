Title: Friday Squid Blogging: Another Giant Squid Captured on Video
Date: 2023-01-20T22:00:55+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid;video
Slug: 2023-01-20-friday-squid-blogging-another-giant-squid-captured-on-video

[Source](https://www.schneier.com/blog/archives/2023/01/friday-squid-blogging-another-giant-squid-captured-on-video.html){:target="_blank" rel="noopener"}

> Here’s a new video of a giant squid, filmed in the Sea of Japan. I believe it’s injured. It’s so close to the surface, and not really moving very much. “We didn’t see the kinds of agile movements that many fish and marine creatures normally show,” he said. “Its tentacles and fins were moving very slowly.” As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
