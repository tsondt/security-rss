Title: Swiss Army's Threema messaging app was full of holes – at least seven
Date: 2023-01-11T08:01:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-11-swiss-armys-threema-messaging-app-was-full-of-holes-at-least-seven

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/11/swiss_army_threema_bugs/){:target="_blank" rel="noopener"}

> At least the penknives are still secure A supposedly secure messaging app preferred by the Swiss government and army was infested with bugs – possibly for a long time – before an audit by ETH Zurich researchers.... [...]
