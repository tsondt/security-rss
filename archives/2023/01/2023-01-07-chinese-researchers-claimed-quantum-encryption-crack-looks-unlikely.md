Title: Chinese researchers' claimed quantum encryption crack looks unlikely
Date: 2023-01-07T12:00:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-01-07-chinese-researchers-claimed-quantum-encryption-crack-looks-unlikely

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/07/chinese_researchers_claimed_quantum_encryption/){:target="_blank" rel="noopener"}

> Near-term vulnerability of RSA-2048 keys not so near, says quantum boffin Scott Aaronson Briefly this week, it appeared that quantum computers might finally be ready to break 2048-bit RSA encryption, but that moment has passed.... [...]
