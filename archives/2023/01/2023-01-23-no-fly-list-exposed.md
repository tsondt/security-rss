Title: No-Fly List Exposed
Date: 2023-01-23T12:02:56+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;air travel;hacking;no-fly list;TSA
Slug: 2023-01-23-no-fly-list-exposed

[Source](https://www.schneier.com/blog/archives/2023/01/no-fly-list-exposed.html){:target="_blank" rel="noopener"}

> I can’t remember the last time I thought about the US no-fly list: the list of people so dangerous they should never be allowed to fly on an airplane, yet so innocent that we can’t arrest them. Back when I thought about it a lot, I realized that the TSA’s practice of giving it to every airline meant that it was not well protected, and it certainly ended up in the hands of every major government that wanted it. The list is back in the news today, having been left exposed on an insecure airline computer. (The airline is CommuteAir, [...]
