Title: DORA's implementation period starts now. What we're doing to prepare for the new law
Date: 2023-01-17T17:00:00+00:00
Author: Gillian Hamilton
Category: GCP Security
Tags: Identity & Security
Slug: 2023-01-17-doras-implementation-period-starts-now-what-were-doing-to-prepare-for-the-new-law

[Source](https://cloud.google.com/blog/products/identity-security/doras-implementation-period-starts-now-what-were-doing-to-prepare-for-the-new-law/){:target="_blank" rel="noopener"}

> Today is the start of the two year implementation period for the EU Digital Operational Resilience Act (DORA). Financial entities in the European Union (EU) and their critical ICT providers must be ready to comply with DORA by January 17, 2025. At Google Cloud, we firmly believe that DORA will be vital to accelerating digital innovation in the European financial services sector. We have been engaging with policymakers on DORA since September 2020. We are now excited to collaborate with customers and regulators to operationalize the new DORA requirements ahead of the deadline. As we approach the 2025 deadline, we [...]
