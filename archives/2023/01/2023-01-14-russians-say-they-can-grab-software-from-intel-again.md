Title: Russians say they can grab software from Intel again
Date: 2023-01-14T08:07:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-01-14-russians-say-they-can-grab-software-from-intel-again

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/14/intel_microsoft_russia/){:target="_blank" rel="noopener"}

> And Windows updates from Microsoft, too People in Russia can reportedly once again download drivers and some other software from Intel and Microsoft, which both withdrew from the nation after its invasion of Ukraine.... [...]
