Title: New 'Blank Image' attack hides phishing scripts in SVG files
Date: 2023-01-19T09:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-19-new-blank-image-attack-hides-phishing-scripts-in-svg-files

[Source](https://www.bleepingcomputer.com/news/security/new-blank-image-attack-hides-phishing-scripts-in-svg-files/){:target="_blank" rel="noopener"}

> An unusual phishing technique has been observed in the wild, hiding empty SVG files inside HTML attachments pretending to be DocuSign documents. [...]
