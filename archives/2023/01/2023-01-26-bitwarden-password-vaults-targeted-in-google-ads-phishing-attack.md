Title: Bitwarden password vaults targeted in Google ads phishing attack
Date: 2023-01-26T16:40:34-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-01-26-bitwarden-password-vaults-targeted-in-google-ads-phishing-attack

[Source](https://www.bleepingcomputer.com/news/security/bitwarden-password-vaults-targeted-in-google-ads-phishing-attack/){:target="_blank" rel="noopener"}

> Bitwarden and other password managers are being targeted in Google ads phishing campaigns to steal users' password vault credentials. [...]
