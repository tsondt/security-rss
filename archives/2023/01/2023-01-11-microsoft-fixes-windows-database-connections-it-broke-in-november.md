Title: Microsoft fixes Windows database connections it broke in November
Date: 2023-01-11T17:00:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-01-11-microsoft-fixes-windows-database-connections-it-broke-in-november

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/11/microsoft_database_connection_fix/){:target="_blank" rel="noopener"}

> January Patch Tuesday update resolves issue caused by Patch Tuesday update late in '22 Included in the usual tsunami of fixes Microsoft issued this week as part of Patch Tuesday was one that took care of a connectivity problem for applications using the Open Database Connectivity (ODBC) interface.... [...]
