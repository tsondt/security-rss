Title: Git security audit reveals critical overflow bugs
Date: 2023-01-20T15:00:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-20-git-security-audit-reveals-critical-overflow-bugs

[Source](https://portswigger.net/daily-swig/git-security-audit-reveals-critical-overflow-bugs){:target="_blank" rel="noopener"}

> Uncovered vulnerabilities include several high, medium, and low-security issues [...]
