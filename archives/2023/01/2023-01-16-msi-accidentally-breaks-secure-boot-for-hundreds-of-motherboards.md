Title: MSI accidentally breaks Secure Boot for hundreds of motherboards
Date: 2023-01-16T17:02:36-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2023-01-16-msi-accidentally-breaks-secure-boot-for-hundreds-of-motherboards

[Source](https://www.bleepingcomputer.com/news/security/msi-accidentally-breaks-secure-boot-for-hundreds-of-motherboards/){:target="_blank" rel="noopener"}

> Over 290 MSI motherboards are reportedly affected by an insecure default UEFI Secure Boot setting settings that allows any operating system image to run regardless of whether it has a wrong or missing signature. [...]
