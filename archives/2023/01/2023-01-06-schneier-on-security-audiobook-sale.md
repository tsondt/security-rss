Title: Schneier on Security Audiobook Sale
Date: 2023-01-06T20:04:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Schneier news;Schneier on Security (book)
Slug: 2023-01-06-schneier-on-security-audiobook-sale

[Source](https://www.schneier.com/blog/archives/2023/01/schneier-on-security-audiobook-sale.html){:target="_blank" rel="noopener"}

> I’m not sure why, but Audiobooks.com is offering the audiobook version of Schneier on Security at 50% off until January 17. [...]
