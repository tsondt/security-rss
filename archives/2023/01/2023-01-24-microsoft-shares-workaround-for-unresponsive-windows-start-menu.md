Title: Microsoft shares workaround for unresponsive Windows Start Menu
Date: 2023-01-24T15:04:50-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-24-microsoft-shares-workaround-for-unresponsive-windows-start-menu

[Source](https://www.bleepingcomputer.com/news/security/microsoft-shares-workaround-for-unresponsive-windows-start-menu/){:target="_blank" rel="noopener"}

> Microsoft has confirmed an issue causing the Windows Start menu to become unresponsive and some applications to no longer launch. [...]
