Title: Fall 2022 SOC reports now available in Spanish
Date: 2023-01-19T16:30:31+00:00
Author: Rodrigo Fiuza
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS SOC 1;AWS SOC 2;AWS SOC 3;AWS SOC Reports;Security Blog
Slug: 2023-01-19-fall-2022-soc-reports-now-available-in-spanish

[Source](https://aws.amazon.com/blogs/security/fall-2022-soc-reports-now-available-in-spanish/){:target="_blank" rel="noopener"}

> Spanish version >> We continue to listen to our customers, regulators, and stakeholders to understand their needs regarding audit, assurance, certification, and attestation programs at Amazon Web Services (AWS). We are pleased to announce that Fall 2022 System and Organization Controls (SOC) 1, SOC 2, and SOC 3 reports are now available in Spanish. These [...]
