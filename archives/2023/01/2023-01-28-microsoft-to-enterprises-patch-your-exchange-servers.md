Title: Microsoft to enterprises: Patch your Exchange servers
Date: 2023-01-28T01:03:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-01-28-microsoft-to-enterprises-patch-your-exchange-servers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/28/microsoft_patch_exchange_servers/){:target="_blank" rel="noopener"}

> If you want to keep the miscreants out, put the updates in, Redmond says Microsoft is urging organizations to protect their Exchange servers from cyberattacks by keeping them updated and hardened, since online criminals are still going after valuable data in the email system.... [...]
