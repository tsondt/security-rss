Title: Bitzlato crypto exchange seized for ransomware, drugs money laundering
Date: 2023-01-18T12:50:47-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-01-18-bitzlato-crypto-exchange-seized-for-ransomware-drugs-money-laundering

[Source](https://www.bleepingcomputer.com/news/security/bitzlato-crypto-exchange-seized-for-ransomware-drugs-money-laundering/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice arrested and charged Russian national Anatoly Legkodymov, the founder of the Hong Kong-registered cryptocurrency exchange Bitzlato, with helping cybercriminals allegedly launder illegally obtained money. [...]
