Title: This can’t be a real bomb threat: you've called a modem, not a phone
Date: 2023-01-13T07:29:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-01-13-this-cant-be-a-real-bomb-threat-youve-called-a-modem-not-a-phone

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/13/on_call/){:target="_blank" rel="noopener"}

> Security was nonetheless very, very, interested in hearing this comms engineer tell his tale On-Call Welcome once again to On-Call, The Register 's weekly compendium of tales from readers who were asked to deal with IT oddities and mostly emerged unscathed.... [...]
