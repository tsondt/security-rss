Title: Microsoft: Exchange Server 2013 reaches end of support in 90 days
Date: 2023-01-12T16:13:47-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-12-microsoft-exchange-server-2013-reaches-end-of-support-in-90-days

[Source](https://www.bleepingcomputer.com/news/security/microsoft-exchange-server-2013-reaches-end-of-support-in-90-days/){:target="_blank" rel="noopener"}

> Microsoft warned customers today that Exchange Server 2013 will reach its extended end-of-support (EOS) date 90 days from now, on April 11, 2023. [...]
