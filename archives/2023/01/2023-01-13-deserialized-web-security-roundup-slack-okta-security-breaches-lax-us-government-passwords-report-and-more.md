Title: Deserialized web security roundup – Slack, Okta security breaches, lax US government passwords report, and more&nbsp;
Date: 2023-01-13T18:31:17+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-13-deserialized-web-security-roundup-slack-okta-security-breaches-lax-us-government-passwords-report-and-more

[Source](https://portswigger.net/daily-swig/deserialized-web-security-roundup-slack-okta-security-breaches-lax-us-government-passwords-report-and-more-nbsp){:target="_blank" rel="noopener"}

> Your fortnightly rundown of AppSec vulnerabilities, new hacking techniques, and other cybersecurity news [...]
