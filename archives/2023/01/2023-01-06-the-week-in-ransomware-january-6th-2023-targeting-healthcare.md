Title: The Week in Ransomware - January 6th 2023 - Targeting Healthcare
Date: 2023-01-06T19:51:37-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-01-06-the-week-in-ransomware-january-6th-2023-targeting-healthcare

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-january-6th-2023-targeting-healthcare/){:target="_blank" rel="noopener"}

> This week saw a lot of ransomware news, ranging from new extortion tactics, to a ransomware gang giving away a free decryptor after attacking a children's hospital. [...]
