Title: Microsoft closes another door to attackers by blocking Excel XLL files from the internet
Date: 2023-01-25T21:59:06+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-01-25-microsoft-closes-another-door-to-attackers-by-blocking-excel-xll-files-from-the-internet

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/25/microsoft_excel_xll_closed/){:target="_blank" rel="noopener"}

> More of them used by baddies since Redmond blocked VBA macros Microsoft in March will start blocking Excel XLL add-ins from the internet to shut down an increasingly popular attack vector for miscreants.... [...]
