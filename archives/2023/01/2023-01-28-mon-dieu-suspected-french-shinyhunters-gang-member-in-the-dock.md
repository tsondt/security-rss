Title: Mon Dieu! Suspected French ShinyHunters gang member in the dock
Date: 2023-01-28T08:50:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-28-mon-dieu-suspected-french-shinyhunters-gang-member-in-the-dock

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/28/shinyhunters_sebastien_raoult/){:target="_blank" rel="noopener"}

> Man seized in Morocco is now presumably sleepless in Seattle A French citizen was scheduled to appear before a US court on Friday on a nine-count indictment related to his alleged involvement in the ShinyHunters cybercrime gang that trafficked in identity and corporate data theft and sometimes extortion.... [...]
