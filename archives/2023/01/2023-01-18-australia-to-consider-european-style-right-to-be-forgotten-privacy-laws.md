Title: Australia to consider European-style right to be forgotten privacy laws
Date: 2023-01-18T14:00:44+00:00
Author: Paul Karp
Category: The Guardian
Tags: Australian politics;Privacy;Law (Australia);Mark Dreyfus;Labor party;Business;Australia news;Data and computer security;Data protection
Slug: 2023-01-18-australia-to-consider-european-style-right-to-be-forgotten-privacy-laws

[Source](https://www.theguardian.com/australia-news/2023/jan/19/right-to-be-forgotten-australia-europe-gdpr-privacy-laws){:target="_blank" rel="noopener"}

> Attorney general promises ‘whole range’ of modernisations of the Privacy Act to follow new customer data protection laws Follow our Australia news live blog for the latest updates Get our morning and afternoon news emails, free app or daily news podcast The right to be forgotten and a right to sue for privacy breaches will be considered for the next tranche of Australian legislation, the attorney general has said. Mark Dreyfus made the comments on Monday, promising to consider European-style privacy reforms after his bill increasing penalties for companies that fail to protect customer data passed in 2022. Sign up [...]
