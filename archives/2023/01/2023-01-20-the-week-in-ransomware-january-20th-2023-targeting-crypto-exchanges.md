Title: The Week in Ransomware - January 20th 2023 - Targeting Crypto Exchanges
Date: 2023-01-20T17:08:39-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-01-20-the-week-in-ransomware-january-20th-2023-targeting-crypto-exchanges

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-january-20th-2023-targeting-crypto-exchanges/){:target="_blank" rel="noopener"}

> There has been quite a bit of ransomware news this week, with crypto exchanges being seized for alleged money laundering and researchers providing fascinating reports on the behavior of ransomware operators. [...]
