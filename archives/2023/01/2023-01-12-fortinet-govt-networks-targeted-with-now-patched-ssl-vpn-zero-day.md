Title: Fortinet: Govt networks targeted with now-patched SSL-VPN zero-day
Date: 2023-01-12T11:05:45-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-12-fortinet-govt-networks-targeted-with-now-patched-ssl-vpn-zero-day

[Source](https://www.bleepingcomputer.com/news/security/fortinet-govt-networks-targeted-with-now-patched-ssl-vpn-zero-day/){:target="_blank" rel="noopener"}

> Fortinet says unknown attackers exploited a FortiOS SSL-VPN zero-day vulnerability patched last month in attacks against government organizations and government-related targets. [...]
