Title: The FBI Identified a Tor User
Date: 2023-01-17T12:02:26+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;dark web;de-anonymization;FBI;hacking;NSA;privacy;surveillance;Tor
Slug: 2023-01-17-the-fbi-identified-a-tor-user

[Source](https://www.schneier.com/blog/archives/2023/01/the-fbi-identified-a-tor-user.html){:target="_blank" rel="noopener"}

> No details, though: According to the complaint against him, Al-Azhari allegedly visited a dark web site that hosts “unofficial propaganda and photographs related to ISIS” multiple times on May 14, 2019. In virtue of being a dark web site—­that is, one hosted on the Tor anonymity network—­it should have been difficult for the site owner’s or a third party to determine the real IP address of any of the site’s visitors. Yet, that’s exactly what the FBI did. It found Al-Azhari allegedly visited the site from an IP address associated with Al-Azhari’s grandmother’s house in Riverside, California. The FBI also [...]
