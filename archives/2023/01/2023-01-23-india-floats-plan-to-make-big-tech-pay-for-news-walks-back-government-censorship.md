Title: India floats plan to make big tech pay for news, walks back government censorship
Date: 2023-01-23T03:01:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-01-23-india-floats-plan-to-make-big-tech-pay-for-news-walks-back-government-censorship

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/23/asia_tech_news_roundup/){:target="_blank" rel="noopener"}

> PLUS: Taiwan’s new supercomputer; China-linked cybercrims strike; Australian content clampdown; and more Asia In Brief India's IT minister has signaled he is willing to revisit a proposal to use government fact checkers to decide what is fake news that should be removed from social media.... [...]
