Title: Amid FTX's burning wreckage, Japan outpost promises asset withdrawals in February
Date: 2023-01-31T05:29:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-01-31-amid-ftxs-burning-wreckage-japan-outpost-promises-asset-withdrawals-in-february

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/31/ftx_japan_february_withdrawals_planned/){:target="_blank" rel="noopener"}

> Well what do you know – plenty of hard-nosed regulation by central authorities actually protected investors Collapsed crypto exchange FTX's Japanese outpost has told customers it will permit them to withdraw assets in February.... [...]
