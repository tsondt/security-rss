Title: Avast releases free BianLian ransomware decryptor
Date: 2023-01-16T07:15:34-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2023-01-16-avast-releases-free-bianlian-ransomware-decryptor

[Source](https://www.bleepingcomputer.com/news/security/avast-releases-free-bianlian-ransomware-decryptor/){:target="_blank" rel="noopener"}

> Security software company Avast has released a free decryptor for the BianLian ransomware strain to help victims of the malware recover locked files without paying the hackers. [...]
