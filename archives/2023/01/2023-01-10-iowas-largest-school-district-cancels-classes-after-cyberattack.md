Title: Iowa’s largest school district cancels classes after cyberattack
Date: 2023-01-10T13:10:02-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Education
Slug: 2023-01-10-iowas-largest-school-district-cancels-classes-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/iowa-s-largest-school-district-cancels-classes-after-cyberattack/){:target="_blank" rel="noopener"}

> Des Moines Public Schools, the largest school district in Iowa, canceled all classes on Tuesday after taking all networked systems offline in response to "unusual activity" detected on its network one day before. [...]
