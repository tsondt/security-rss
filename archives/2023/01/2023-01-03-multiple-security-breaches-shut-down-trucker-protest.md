Title: 'Multiple security breaches' shut down trucker protest
Date: 2023-01-03T20:30:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-03-multiple-security-breaches-shut-down-trucker-protest

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/03/canada_truckers_security_snafu/){:target="_blank" rel="noopener"}

> 10-7, there buddy, sorry An anti-government protest by truckers in Canada has been called off following "multiple security breaches," according to organizers, who also cited "personal character attacks," as a reason for the withdrawal.... [...]
