Title: Visualize AWS WAF logs with an Amazon CloudWatch dashboard
Date: 2023-01-26T18:13:43+00:00
Author: Diana Alvarado
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;CloudWatch;Security;Security Blog;WAF
Slug: 2023-01-26-visualize-aws-waf-logs-with-an-amazon-cloudwatch-dashboard

[Source](https://aws.amazon.com/blogs/security/visualize-aws-waf-logs-with-an-amazon-cloudwatch-dashboard/){:target="_blank" rel="noopener"}

> AWS WAF is a web application firewall service that helps you protect your applications from common exploits that could affect your application’s availability and your security posture. One of the most useful ways to detect and respond to malicious web activity is to collect and analyze AWS WAF logs. You can perform this task conveniently [...]
