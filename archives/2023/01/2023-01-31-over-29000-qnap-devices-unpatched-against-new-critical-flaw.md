Title: Over 29,000 QNAP devices unpatched against new critical flaw
Date: 2023-01-31T18:14:46-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-31-over-29000-qnap-devices-unpatched-against-new-critical-flaw

[Source](https://www.bleepingcomputer.com/news/security/over-29-000-qnap-devices-unpatched-against-new-critical-flaw/){:target="_blank" rel="noopener"}

> Tens of thousands of QNAP network-attached storage (NAS) devices exposed online are waiting to be patched against a critical security flaw addressed by the Taiwanese company on Monday. [...]
