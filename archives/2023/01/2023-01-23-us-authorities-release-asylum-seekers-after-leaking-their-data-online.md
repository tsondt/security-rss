Title: US authorities release asylum seekers after leaking their data online
Date: 2023-01-23T05:01:05+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-01-23-us-authorities-release-asylum-seekers-after-leaking-their-data-online

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/23/infosec_news_roundup/){:target="_blank" rel="noopener"}

> Also: US terrorist no-fly list found left on unsecured server, Russian dark web drug markets go to war In brief Nearly 3,000 immigrants seeking asylum in the United States have been released from custody after Immigration and Customs Enforcement (ICE) officials inadvertently published their personal information online.... [...]
