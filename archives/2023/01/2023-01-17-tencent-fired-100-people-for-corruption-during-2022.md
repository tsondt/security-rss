Title: Tencent fired 100 people for corruption during 2022
Date: 2023-01-17T05:29:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-01-17-tencent-fired-100-people-for-corruption-during-2022

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/17/tencent_corruption_report/){:target="_blank" rel="noopener"}

> A couple have already been jailed, others shown the door for embezzling or arranging sham contracts Chinese web and gaming giant Tencent has admitted it fired more than 100 people in 2022 for various forms of corruption – some so serious it reported them to local police.... [...]
