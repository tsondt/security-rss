Title: Homeland Security, CISA builds AI-based cybersecurity analytics sandbox
Date: 2023-01-10T01:00:07+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-01-10-homeland-security-cisa-builds-ai-based-cybersecurity-analytics-sandbox

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/10/dhs_cisa_cybersecurity_sandbox/){:target="_blank" rel="noopener"}

> High-spec system is crucial to defending against the latest threats Two of the US government's leading security agencies are building a machine learning-based analytics environment to defend against rapidly evolving threats and create more resilient infrastructures for both government entities and private organizations.... [...]
