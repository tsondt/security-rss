Title: Twitter claims leaked data of 200M users not stolen from its systems
Date: 2023-01-11T15:18:58-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-11-twitter-claims-leaked-data-of-200m-users-not-stolen-from-its-systems

[Source](https://www.bleepingcomputer.com/news/security/twitter-claims-leaked-data-of-200m-users-not-stolen-from-its-systems/){:target="_blank" rel="noopener"}

> Twitter finally addressed reports that a dataset of email addresses linked to hundreds of millions of Twitter users was leaked and put up for sale online, saying that it found no evidence the data was obtained by exploiting a vulnerability in its systems. [...]
