Title: The wages of sin aren't that great if you're a developer choosing the dark side
Date: 2023-01-30T21:45:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-30-the-wages-of-sin-arent-that-great-if-youre-a-developer-choosing-the-dark-side

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/30/dark_web_it_employment_kaspersky/){:target="_blank" rel="noopener"}

> Salary report shows OKish pay, plus the possibility of getting ripped off and the whole prison thing Malware developers and penetration testers are in high demand across dark web job posting sites, with a few astonishing - but mostly average - wages.... [...]
