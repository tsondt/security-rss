Title: Over 19,000 end-of-life Cisco routers exposed to RCE attacks
Date: 2023-01-20T16:26:21-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-01-20-over-19000-end-of-life-cisco-routers-exposed-to-rce-attacks

[Source](https://www.bleepingcomputer.com/news/security/over-19-000-end-of-life-cisco-routers-exposed-to-rce-attacks/){:target="_blank" rel="noopener"}

> Over 19,000 end-of-life Cisco VPN routers on the Internet are exposed to attacks targeting a remote command execution exploit chain. [...]
