Title: PyTorch dependency poisoned with malicious code
Date: 2023-01-04T14:00:13+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-01-04-pytorch-dependency-poisoned-with-malicious-code

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/04/pypi_pytorch_dependency_attack/){:target="_blank" rel="noopener"}

> System data was exfiltrated during attack, but an anonymous person says it was a research project gone wrong An unknown attacker used the PyPI code repository to get developers to download a compromised PyTorch dependency that included malicious code designed to steal system data.... [...]
