Title: Nearly 300 MSI motherboards will run any old code in Secure Boot, no questions asked
Date: 2023-01-17T20:01:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-01-17-nearly-300-msi-motherboards-will-run-any-old-code-in-secure-boot-no-questions-asked

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/17/msi_motherboards_secure_boot/){:target="_blank" rel="noopener"}

> 'I believe they made this change deliberately' claims researcher The Secure Boot process on almost 300 different PC motherboard models manufactured by Micro-Star International (MSI) isn't secure, which is particularly problematic when "Secure" is part of the process description.... [...]
