Title: Apple emits emergency patch for older iPhones after snoops pounce on WebKit hole
Date: 2023-01-24T20:45:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-24-apple-emits-emergency-patch-for-older-iphones-after-snoops-pounce-on-webkit-hole

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/24/apple_iphone_bug_under_exploit/){:target="_blank" rel="noopener"}

> Also: Yay for Data Privacy Day! Apple has issued an emergency patch for older kit to fix a WebKit security flaw that Cupertino warns is under active attack.... [...]
