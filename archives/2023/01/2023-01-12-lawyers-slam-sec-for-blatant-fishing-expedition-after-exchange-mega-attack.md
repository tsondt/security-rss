Title: Lawyers slam SEC for 'blatant fishing expedition' after Exchange mega-attack
Date: 2023-01-12T20:06:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-01-12-lawyers-slam-sec-for-blatant-fishing-expedition-after-exchange-mega-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/12/sec_covington_hafnium/){:target="_blank" rel="noopener"}

> Not a 'whiff of wrongdoing' here, says attorney now fighting off Uncle Sam The US Securities and Exchange Commission (SEC) has sued international law firm Covington & Burling for details about 298 of the biz's clients whose information was accessed by a Chinese state-sponsored hacking group in November 2020.... [...]
