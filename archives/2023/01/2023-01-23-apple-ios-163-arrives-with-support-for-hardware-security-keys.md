Title: Apple iOS 16.3 arrives with support for hardware security keys
Date: 2023-01-23T17:56:22-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Apple;Security
Slug: 2023-01-23-apple-ios-163-arrives-with-support-for-hardware-security-keys

[Source](https://www.bleepingcomputer.com/news/apple/apple-ios-163-arrives-with-support-for-hardware-security-keys/){:target="_blank" rel="noopener"}

> Apple released iOS 16.3 today with long-awaited support for hardware security keys to provide extra protection against phishing attacks and unauthorized access to your devices. [...]
