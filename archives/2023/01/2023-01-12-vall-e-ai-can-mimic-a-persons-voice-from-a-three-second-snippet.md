Title: VALL-E AI can mimic a person’s voice from a three-second snippet
Date: 2023-01-12T08:30:09+00:00
Author: Jeff Burt
Category: The Register
Tags: 
Slug: 2023-01-12-vall-e-ai-can-mimic-a-persons-voice-from-a-three-second-snippet

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/12/microsoft_valle_ai/){:target="_blank" rel="noopener"}

> Are you really saying what I’m hearing? Microsoft researchers are working on a text-to-speech (TTS) model that can mimic a person's voice – complete with emotion and intonation – after a mere three seconds of training.... [...]
