Title: Mailchimp 'fesses up to second digital burglary in five months
Date: 2023-01-19T14:16:49+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-01-19-mailchimp-fesses-up-to-second-digital-burglary-in-five-months

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/19/mailchimp_fesses_up_to_2nd/){:target="_blank" rel="noopener"}

> Social engineering helped intruders break into customers' inboxes again Email marketing service Mailchimp has confirmed intruders have gained access to more than 100 customer accounts after successfully deploying a social engineering attack.... [...]
