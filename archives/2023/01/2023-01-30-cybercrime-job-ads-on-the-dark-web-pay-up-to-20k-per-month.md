Title: Cybercrime job ads on the dark web pay up to $20k per month
Date: 2023-01-30T16:38:14-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-30-cybercrime-job-ads-on-the-dark-web-pay-up-to-20k-per-month

[Source](https://www.bleepingcomputer.com/news/security/cybercrime-job-ads-on-the-dark-web-pay-up-to-20k-per-month/){:target="_blank" rel="noopener"}

> Cybercrime groups are increasingly running their operations as a business, promoting jobs on the dark web that offer developers and hackers competitive monthly salaries, paid time off, and paid sick leaves. [...]
