Title: Upcoming Speaking Engagements
Date: 2023-01-14T17:05:53+00:00
Author: Schneier.com Webmaster
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2023-01-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2023/01/upcoming-speaking-engagements-26.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking at Capricon, a four-day science fiction convention in Chicago. My talk is on “The Coming AI Hackers” and will be held Friday, February 3 at 1:00 PM. The list is maintained on this page. [...]
