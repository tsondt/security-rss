Title: For password protection, dump LastPass for open source Bitwarden
Date: 2023-01-16T11:30:11+00:00
Author: Steven J. Vaughan-Nichols
Category: The Register
Tags: 
Slug: 2023-01-16-for-password-protection-dump-lastpass-for-open-source-bitwarden

[Source](https://go.theregister.com/feed/www.theregister.com/2023/01/16/dump_lastpass_bitwarden/){:target="_blank" rel="noopener"}

> After the security breach last summer, staying put is playing with fire Opinion For better or worse, we still need passwords, and to protect and organize them, I recommend the open source Bitwarden password manager.... [...]
