Title: How to revoke federated users’ active AWS sessions
Date: 2023-01-16T17:43:34+00:00
Author: Matt Howard
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;AWS IAM Identity Center;AWS Identity and Access Management;Federation;IAM;Identity providers;Security;Security Blog;Single sign-on;SSO
Slug: 2023-01-16-how-to-revoke-federated-users-active-aws-sessions

[Source](https://aws.amazon.com/blogs/security/how-to-revoke-federated-users-active-aws-sessions/){:target="_blank" rel="noopener"}

> When you use a centralized identity provider (IdP) for human user access, changes that an identity administrator makes to a user within the IdP won’t invalidate the user’s existing active Amazon Web Services (AWS) sessions. This is due to the nature of session durations that are configured on assumed roles. This situation presents a challenge [...]
