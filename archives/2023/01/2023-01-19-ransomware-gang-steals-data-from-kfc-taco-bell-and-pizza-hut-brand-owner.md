Title: Ransomware gang steals data from KFC, Taco Bell, and Pizza Hut brand owner
Date: 2023-01-19T14:21:11-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-01-19-ransomware-gang-steals-data-from-kfc-taco-bell-and-pizza-hut-brand-owner

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-steals-data-from-kfc-taco-bell-and-pizza-hut-brand-owner/){:target="_blank" rel="noopener"}

> Yum! Brands, the fast food brand operator of KFC, Pizza Hut, Taco Bell, and The Habit Burger Grill fast-food restaurant chains, has been targeted by a ransomware attack that forced the closure of 300 locations in the United Kingdom. [...]
