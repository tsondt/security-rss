Title: How to set up ongoing replication from your third-party secrets manager to AWS Secrets Manager
Date: 2023-01-31T18:33:34+00:00
Author: Laurens Brinker
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;AWS KMS;AWS Lambda;AWS Secrets Manager;external secrets manager;HashiCorp;Key Management Service;KMS;lambda;Replication;secrets;Secrets Manager;Security;Security Blog;synchronization;third party secrets manager;Vault
Slug: 2023-01-31-how-to-set-up-ongoing-replication-from-your-third-party-secrets-manager-to-aws-secrets-manager

[Source](https://aws.amazon.com/blogs/security/how-to-set-up-ongoing-replication-from-your-third-party-secrets-manager-to-aws-secrets-manager/){:target="_blank" rel="noopener"}

> Secrets managers are a great tool to securely store your secrets and provide access to secret material to a set of individuals, applications, or systems that you trust. Across your environments, you might have multiple secrets managers hosted on different providers, which can increase the complexity of maintaining a consistent operating model for your secrets. [...]
