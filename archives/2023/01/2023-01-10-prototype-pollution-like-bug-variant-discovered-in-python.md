Title: Prototype pollution-like bug variant discovered in Python
Date: 2023-01-10T14:49:53+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-10-prototype-pollution-like-bug-variant-discovered-in-python

[Source](https://portswigger.net/daily-swig/prototype-pollution-like-bug-variant-discovered-in-python){:target="_blank" rel="noopener"}

> ‘Class pollution’ flaw similar to dangerous vulnerability type found in JavaScript and similar languages [...]
