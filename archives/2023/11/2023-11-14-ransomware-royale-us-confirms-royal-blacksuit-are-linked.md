Title: Ransomware royale: US confirms Royal, BlackSuit are linked
Date: 2023-11-14T14:45:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-14-ransomware-royale-us-confirms-royal-blacksuit-are-linked

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/14/us_confirms_royalblacksuit_ransomware_ties/){:target="_blank" rel="noopener"}

> Royal alone scored $275M in past year as FBI, other agencies hot on merging trail The FBI and the US govt's Cybersecurity and Infrastructure Security Agency (CISA) have released fresh guidance on the Royal ransomware operation, saying that evidence suggests it may soon undergo a long-speculated rebrand.... [...]
