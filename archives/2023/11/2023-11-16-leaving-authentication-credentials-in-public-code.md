Title: Leaving Authentication Credentials in Public Code
Date: 2023-11-16T12:10:04+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;credentials;vulnerabilities
Slug: 2023-11-16-leaving-authentication-credentials-in-public-code

[Source](https://www.schneier.com/blog/archives/2023/11/leaving-authentication-credentials-in-public-code.html){:target="_blank" rel="noopener"}

> Interesting article about a surprisingly common vulnerability: programmers leaving authentication credentials and other secrets in publicly accessible software code: Researchers from security firm GitGuardian this week reported finding almost 4,000 unique secrets stashed inside a total of 450,000 projects submitted to PyPI, the official code repository for the Python programming language. Nearly 3,000 projects contained at least one unique secret. Many secrets were leaked more than once, bringing the total number of exposed secrets to almost 57,000. [...] The credentials exposed provided access to a range of resources, including Microsoft Active Directory servers that provision and manage accounts in enterprise [...]
