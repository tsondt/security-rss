Title: LogoFAIL attack can install UEFI bootkits through bootup logos
Date: 2023-11-30T22:08:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-30-logofail-attack-can-install-uefi-bootkits-through-bootup-logos

[Source](https://www.bleepingcomputer.com/news/security/logofail-attack-can-install-uefi-bootkits-through-bootup-logos/){:target="_blank" rel="noopener"}

> Multiple security vulnerabilities collectively named LogoFAIL affect image-parsing components in the UEFI code from various vendors. Researchers warn that they could be exploited to hijack the execution flow of the booting process and to deliver bootkits. [...]
