Title: ID Theft Service Resold Access to USInfoSearch Data
Date: 2023-11-28T15:57:38+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;Experian;fake EDR;Hieu;JackieChan;Kodex;Martin Data LLC;Matt Donahue;Scott Hostettler;telegram;USinfoSearch
Slug: 2023-11-28-id-theft-service-resold-access-to-usinfosearch-data

[Source](https://krebsonsecurity.com/2023/11/id-theft-service-resold-access-to-usinfosearch-data/){:target="_blank" rel="noopener"}

> One of the cybercrime underground’s more active sellers of Social Security numbers, background and credit reports has been pulling data from hacked accounts at the U.S. consumer data broker USinfoSearch, KrebsOnSecurity has learned. Since at least February 2023, a service advertised on Telegram called USiSLookups has operated an automated bot that allows anyone to look up the SSN or background report on virtually any American. For prices ranging from $8 to $40 and payable via virtual currency, the bot will return detailed consumer background reports automatically in just a few moments. USiSLookups is the project of a cybercriminal who uses [...]
