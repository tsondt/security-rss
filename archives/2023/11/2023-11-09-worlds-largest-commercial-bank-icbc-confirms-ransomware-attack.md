Title: World’s largest commercial bank ICBC confirms ransomware attack
Date: 2023-11-09T14:52:23-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-09-worlds-largest-commercial-bank-icbc-confirms-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/worlds-largest-commercial-bank-icbc-confirms-ransomware-attack/){:target="_blank" rel="noopener"}

> The Industrial & Commercial Bank of China (ICBC) is restoring systems and services following a ransomware attack that disrupted the U.S. Treasury market, causing equities clearing issues. [...]
