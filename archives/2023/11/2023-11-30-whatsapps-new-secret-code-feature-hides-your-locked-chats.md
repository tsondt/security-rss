Title: WhatsApp's new Secret Code feature hides your locked chats
Date: 2023-11-30T16:32:14-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-30-whatsapps-new-secret-code-feature-hides-your-locked-chats

[Source](https://www.bleepingcomputer.com/news/security/whatsapps-new-secret-code-feature-hides-your-locked-chats/){:target="_blank" rel="noopener"}

> WhatsApp has introduced a new Secret Code feature that allows users to hide their locked chats by setting a custom password. [...]
