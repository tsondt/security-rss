Title: Samsung hit by new data breach impacting UK store customers
Date: 2023-11-15T18:07:50-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-11-15-samsung-hit-by-new-data-breach-impacting-uk-store-customers

[Source](https://www.bleepingcomputer.com/news/security/samsung-hit-by-new-data-breach-impacting-uk-store-customers/){:target="_blank" rel="noopener"}

> Samsung Electronics is notifying some of its customers of a data breach that exposed their personal information to an unauthorized individual. [...]
