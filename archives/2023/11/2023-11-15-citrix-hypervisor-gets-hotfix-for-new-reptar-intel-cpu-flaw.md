Title: Citrix Hypervisor gets hotfix for new Reptar Intel CPU flaw
Date: 2023-11-15T14:24:32-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-15-citrix-hypervisor-gets-hotfix-for-new-reptar-intel-cpu-flaw

[Source](https://www.bleepingcomputer.com/news/security/citrix-hypervisor-gets-hotfix-for-new-reptar-intel-cpu-flaw/){:target="_blank" rel="noopener"}

> Citrix has released hotfixes for two vulnerabilities impacting Citrix Hypervisor, one of them being the "Reptar" high-severity flaw that affects Intel CPUs for desktop and server systems. [...]
