Title: Mirai we go again: Zero-day flaws see routers and cameras co-opted into botnet
Date: 2023-11-23T08:25:06+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-23-mirai-we-go-again-zero-day-flaws-see-routers-and-cameras-co-opted-into-botnet

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/23/zeroday_routers_mirai_botnet/){:target="_blank" rel="noopener"}

> Akamai sounds the alarm – won't name the vendors yet, but there is a fix coming Akamai has uncovered two zero-day bugs capable of remote code execution, both being exploited to distribute the Mirai malware and built a botnet army for distributed denial of service (DDoS) attacks.... [...]
