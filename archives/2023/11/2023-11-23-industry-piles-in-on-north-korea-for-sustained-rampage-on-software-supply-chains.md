Title: Industry piles in on North Korea for sustained rampage on software supply chains
Date: 2023-11-23T13:38:05+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-23-industry-piles-in-on-north-korea-for-sustained-rampage-on-software-supply-chains

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/23/north_korea_attacks/){:target="_blank" rel="noopener"}

> Kim’s cyber cronies becoming more active, sophisticated in attempts to pwn global orgs The national cybersecurity organizations of the UK and the Republic of Korea (ROK) have issued a joint advisory warning of an increased volume and sophistication of North Korean software supply chain attacks.... [...]
