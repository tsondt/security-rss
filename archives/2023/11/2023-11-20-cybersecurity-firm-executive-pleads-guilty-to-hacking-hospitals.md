Title: Cybersecurity firm executive pleads guilty to hacking hospitals
Date: 2023-11-20T14:13:55-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-11-20-cybersecurity-firm-executive-pleads-guilty-to-hacking-hospitals

[Source](https://www.bleepingcomputer.com/news/security/cybersecurity-firm-executive-pleads-guilty-to-hacking-hospitals/){:target="_blank" rel="noopener"}

> The former chief operating officer of a cybersecurity company has pleaded guilty to hacking two hospitals, part of the Gwinnett Medical Center (GMC), in June 2021 to boost his company's business. [...]
