Title: The Week in Ransomware - November 3rd 2023 - Hive's Back
Date: 2023-11-03T17:08:44-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-11-03-the-week-in-ransomware-november-3rd-2023-hives-back

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-november-3rd-2023-hives-back/){:target="_blank" rel="noopener"}

> Over the past couple of months, ransomware attacks have been escalating as new operations launch, old ones return, and existing operations continue to target the enterprise. [...]
