Title: The XBOM vs SBOM debate
Date: 2023-11-21T13:56:13+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-11-21-the-xbom-vs-sbom-debate

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/21/the_great_xbom_vs_sbom/){:target="_blank" rel="noopener"}

> Why an eXtended Software Bill of Materials could be the next step up in cybersecurity Webinar A Software Bill of Materials (SBOM) has become a non-negotiable requirement to meet regulatory and buyer requirements. But does this provide enough protection if it can give only a partial view into interconnected and ever-changing application attack surfaces?... [...]
