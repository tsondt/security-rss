Title: Slovenia's largest power provider HSE hit by ransomware attack
Date: 2023-11-27T11:16:01-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-27-slovenias-largest-power-provider-hse-hit-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/slovenias-largest-power-provider-hse-hit-by-ransomware-attack/){:target="_blank" rel="noopener"}

> Slovenian power company Holding Slovenske Elektrarne (HSE) has suffered a ransomware attack that compromised its systems and encrypted files, yet the company says the incident did not disrupt electric power production. [...]
