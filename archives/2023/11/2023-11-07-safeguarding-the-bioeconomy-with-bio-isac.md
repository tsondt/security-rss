Title: Safeguarding the bioeconomy, with Bio-ISAC
Date: 2023-11-07T17:00:00+00:00
Author: Bill Reid
Category: GCP Security
Tags: Healthcare & Life Sciences;Security & Identity
Slug: 2023-11-07-safeguarding-the-bioeconomy-with-bio-isac

[Source](https://cloud.google.com/blog/products/identity-security/safeguarding-the-bioeconomy-with-bio-isac/){:target="_blank" rel="noopener"}

> The bioeconomy, according to the Congressional Research Service, is “the share of the economy based on products, services, and processes derived from biological resources.” This sector is composed of a complex network of biomedical, bioindustrial, and agricultural domains. Work in this space ranges from biological research, to production and distribution of products, as well as the equipment and systems used in those processes. As the bioeconomy grows and evolves, its manufacturers are increasingly reliant on digital technologies and interconnected systems. Cybersecurity is paramount. Ensuring the security of supply chains, industrial control systems, intellectual property and data, protecting the connected critical [...]
