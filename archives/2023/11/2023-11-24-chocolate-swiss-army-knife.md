Title: Chocolate Swiss Army Knife
Date: 2023-11-24T20:00:34+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;air travel;humor;weapons
Slug: 2023-11-24-chocolate-swiss-army-knife

[Source](https://www.schneier.com/blog/archives/2023/11/chocolate-swiss-army-knife.html){:target="_blank" rel="noopener"}

> It’s realistic looking. If I drop it in a bin with my keys and wallet, will the TSA confiscate it? [...]
