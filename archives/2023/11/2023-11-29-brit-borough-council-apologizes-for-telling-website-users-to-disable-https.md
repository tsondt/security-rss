Title: Brit borough council apologizes for telling website users to disable HTTPS
Date: 2023-11-29T09:30:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-29-brit-borough-council-apologizes-for-telling-website-users-to-disable-https

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/29/reading_borough_council_https/){:target="_blank" rel="noopener"}

> Planning portal back online with a more secure connection Reading Borough Council has securely restored its planning portal after facing criticism for recommending questionable tech security practices to users.... [...]
