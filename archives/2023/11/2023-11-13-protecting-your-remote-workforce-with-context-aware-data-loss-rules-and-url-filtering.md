Title: Protecting your remote workforce with context-aware data loss rules and URL filtering
Date: 2023-11-13T17:00:00+00:00
Author: Christopher Altman
Category: GCP Security
Tags: Security & Identity
Slug: 2023-11-13-protecting-your-remote-workforce-with-context-aware-data-loss-rules-and-url-filtering

[Source](https://cloud.google.com/blog/products/identity-security/protecting-your-remote-workforce-with-context-aware-data-loss-rules-and-url-filtering/){:target="_blank" rel="noopener"}

> As remote work remains the norm for many people, organizations continue to seek out solutions to protect corporate data regardless of where employees are located and which devices they use. In particular, monitoring for potential data exposures can be a challenging task when employees or contractors use unmanaged devices, as there is risk of downloading unauthorized data or accidentally uploading sensitive files to personal accounts. Traditional solutions to these challenges, such as using Virtual Desktop Infrastructure (VDI) or agent-based tools, can be costly and interrupt user productivity. We've added two secure enterprise browsing capabilities in Google Chrome to help implement [...]
