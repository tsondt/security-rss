Title: Beijing fosters foreign influencers to spread its propaganda
Date: 2023-11-27T03:31:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-11-27-beijing-fosters-foreign-influencers-to-spread-its-propaganda

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/27/china_foreign_inflluencers_aspi/){:target="_blank" rel="noopener"}

> They get access to both China's internet and global platforms, and cash in on both China is offering foreign influencers access to its vast market in return for content that sings its praises and helps to spreads Beijing's desired narratives more widely around the world, according to think tank the Australian Strategic Policy Institute (ASPI).... [...]
