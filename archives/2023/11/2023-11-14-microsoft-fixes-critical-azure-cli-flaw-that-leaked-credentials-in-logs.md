Title: Microsoft fixes critical Azure CLI flaw that leaked credentials in logs
Date: 2023-11-14T13:43:36-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-11-14-microsoft-fixes-critical-azure-cli-flaw-that-leaked-credentials-in-logs

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-fixes-critical-azure-cli-flaw-that-leaked-credentials-in-logs/){:target="_blank" rel="noopener"}

> Microsoft has fixed a critical security vulnerability that could let attackers steal credentials from GitHub Actions or Azure DevOps logs created using Azure CLI (short for Azure command-line interface). [...]
