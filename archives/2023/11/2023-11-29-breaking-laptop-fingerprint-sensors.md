Title: Breaking Laptop Fingerprint Sensors
Date: 2023-11-29T12:09:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;authentication;biometrics;fingerprints;identification;reports;sensors;vulnerabilities
Slug: 2023-11-29-breaking-laptop-fingerprint-sensors

[Source](https://www.schneier.com/blog/archives/2023/11/breaking-laptop-fingerprint-sensors.html){:target="_blank" rel="noopener"}

> They’re not that good : Security researchers Jesse D’Aguanno and Timo Teräs write that, with varying degrees of reverse-engineering and using some external hardware, they were able to fool the Goodix fingerprint sensor in a Dell Inspiron 15, the Synaptic sensor in a Lenovo ThinkPad T14, and the ELAN sensor in one of Microsoft’s own Surface Pro Type Covers. These are just three laptop models from the wide universe of PCs, but one of these three companies usually does make the fingerprint sensor in every laptop we’ve reviewed in the last few years. It’s likely that most Windows PCs with [...]
