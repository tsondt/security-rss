Title: AWS Security Profile: Chris Betz, CISO of AWS
Date: 2023-11-21T16:20:03+00:00
Author: Chris Betz
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Thought Leadership;AWS Security Profile;AWS Security Profiles;Networking;Security Blog
Slug: 2023-11-21-aws-security-profile-chris-betz-ciso-of-aws

[Source](https://aws.amazon.com/blogs/security/aws-security-profile-chris-betz-ciso-of-aws/){:target="_blank" rel="noopener"}

> In the AWS Security Profile series, we feature the people who work in Amazon Web Services (AWS) Security and help keep our customers safe and secure. This interview is with Chris Betz, Chief Information Security Officer (CISO), who began his role as CISO of AWS in August of 2023. How did you get started in [...]
