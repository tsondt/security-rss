Title: FTX crypto-villain Sam Bankman-Fried convicted on all charges
Date: 2023-11-03T01:10:44+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-11-03-ftx-crypto-villain-sam-bankman-fried-convicted-on-all-charges

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/03/sam_bankman_fried_ftx_convicted/){:target="_blank" rel="noopener"}

> Jury took just four hours to reach guilty verdicts Sam Bankman-Fried, the founder and former CEO of crypto exchange FTX and trading firm Alameda Research, has been found guilty of seven criminal charges.... [...]
