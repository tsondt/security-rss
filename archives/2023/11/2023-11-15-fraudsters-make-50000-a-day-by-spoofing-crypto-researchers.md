Title: Fraudsters make $50,000 a day by spoofing crypto researchers
Date: 2023-11-15T10:02:07-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-11-15-fraudsters-make-50000-a-day-by-spoofing-crypto-researchers

[Source](https://www.bleepingcomputer.com/news/security/fraudsters-make-50-000-a-day-by-spoofing-crypto-researchers/){:target="_blank" rel="noopener"}

> Multiple fake accounts impersonating cryptocurrency scam investigators and blockchain security companies are promoting phishing pages to drain wallets in an ongoing campaign on X (former Twitter). [...]
