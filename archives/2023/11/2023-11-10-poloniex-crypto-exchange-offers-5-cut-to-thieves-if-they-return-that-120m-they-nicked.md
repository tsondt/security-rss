Title: Poloniex crypto-exchange offers 5% cut to thieves if they return that $120M they nicked
Date: 2023-11-10T18:51:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-10-poloniex-crypto-exchange-offers-5-cut-to-thieves-if-they-return-that-120m-they-nicked

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/10/justin_sun_poloniex_reward/){:target="_blank" rel="noopener"}

> White hat bounty looks more like a beg bounty The founder of the Poloniex has offered to pay off thieves who drained an estimated $120 million of user funds from the cryptocurrency exchange in a raid on Friday.... [...]
