Title: Helping companies defend what attackers want most - their data
Date: 2023-11-28T14:18:06+00:00
Author: Avia Navickas, VP Product and Customer Marketing, Varonis
Category: The Register
Tags: 
Slug: 2023-11-28-helping-companies-defend-what-attackers-want-most-their-data

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/28/helping_companies_defend_what_attackers/){:target="_blank" rel="noopener"}

> Varonis introduces Athena AI to transform data security and incident response Partner Content Athena AI, the new generative AI layer that spans across the entire Varonis Data Security Platform, redefines how security teams protect data - from visibility to action.... [...]
