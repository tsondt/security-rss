Title: Capital Health Hospitals hit by cyberattack causing IT outages
Date: 2023-11-30T14:12:48-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-11-30-capital-health-hospitals-hit-by-cyberattack-causing-it-outages

[Source](https://www.bleepingcomputer.com/news/security/capital-health-hospitals-hit-by-cyberattack-causing-it-outages/){:target="_blank" rel="noopener"}

> Capital Health hospitals and physician offices across New Jersey are experiencing IT outages after a cyberattack hit the non-profit organization's network earlier this week. [...]
