Title: Kansas courts confirm data theft, ransom demand after cyberattack
Date: 2023-11-22T14:40:49-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-11-22-kansas-courts-confirm-data-theft-ransom-demand-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/kansas-courts-confirm-data-theft-ransom-demand-after-cyberattack/){:target="_blank" rel="noopener"}

> The Kansas Judicial Branch has published an update on a cybersecurity incident it suffered last month, confirming that hackers stole sensitive files containing confidential information from its systems. [...]
