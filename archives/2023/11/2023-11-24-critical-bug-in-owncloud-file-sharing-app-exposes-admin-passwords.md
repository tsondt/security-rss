Title: Critical bug in ownCloud file sharing app exposes admin passwords
Date: 2023-11-24T13:14:13-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud
Slug: 2023-11-24-critical-bug-in-owncloud-file-sharing-app-exposes-admin-passwords

[Source](https://www.bleepingcomputer.com/news/security/critical-bug-in-owncloud-file-sharing-app-exposes-admin-passwords/){:target="_blank" rel="noopener"}

> Open source file sharing software ownCloud is warning of three critical-severity security vulnerabilities, including one that can expose administrator passwords and mail server credentials. [...]
