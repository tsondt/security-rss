Title: FBI shares tactics of notorious Scattered Spider hacker collective
Date: 2023-11-16T16:55:02-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-16-fbi-shares-tactics-of-notorious-scattered-spider-hacker-collective

[Source](https://www.bleepingcomputer.com/news/security/fbi-shares-tactics-of-notorious-scattered-spider-hacker-collective/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation and the Cybersecurity and Infrastructure Security Agency released an advisory about the evasive threat actor tracked as Scattered Spider, a loosely knit hacking collective that now collaborates with the ALPHV/BlackCat Russian ransomware operation.. [...]
