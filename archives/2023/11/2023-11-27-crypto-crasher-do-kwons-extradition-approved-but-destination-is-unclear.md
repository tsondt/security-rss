Title: Crypto crasher Do Kwon's extradition approved, but destination is unclear
Date: 2023-11-27T05:33:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-11-27-crypto-crasher-do-kwons-extradition-approved-but-destination-is-unclear

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/27/do_kwon_extradition_approved/){:target="_blank" rel="noopener"}

> Hey Google, are the jails nicer in South Korea or the US? Terraform Labs founder Do Kwon – a wanted man in both South Korea and the United States – will soon face extradition from Montenegro after a court gave approval for his removal.... [...]
