Title: Boeing acknowledges cyberattack on parts and distribution biz
Date: 2023-11-02T03:31:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-11-02-boeing-acknowledges-cyberattack-on-parts-and-distribution-biz

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/02/boeing_cyber_incident/){:target="_blank" rel="noopener"}

> Won't say if it's LockBit, but LockBit appears to have claimed credit. Maybe payment, too Boeing has acknowledged a cyber incident just days after ransomware gang LockBit reportedly exfiltrated sensitive data from the aerospace defence contractor.... [...]
