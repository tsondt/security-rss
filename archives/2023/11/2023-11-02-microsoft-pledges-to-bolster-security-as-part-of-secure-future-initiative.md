Title: Microsoft pledges to bolster security as part of ‘Secure Future’ initiative
Date: 2023-11-02T11:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-11-02-microsoft-pledges-to-bolster-security-as-part-of-secure-future-initiative

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-pledges-to-bolster-security-as-part-of-secure-future-initiative/){:target="_blank" rel="noopener"}

> Microsoft announced today the 'Secure Future Initiative,' pledging to improve the built-in security of its products and platforms to better protect customers against escalating cybersecurity threats. [...]
