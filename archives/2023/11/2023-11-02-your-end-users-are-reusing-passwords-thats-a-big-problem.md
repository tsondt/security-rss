Title: Your end-users are reusing passwords – that’s a big problem
Date: 2023-11-02T10:01:02-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-11-02-your-end-users-are-reusing-passwords-thats-a-big-problem

[Source](https://www.bleepingcomputer.com/news/security/your-end-users-are-reusing-passwords-thats-a-big-problem/){:target="_blank" rel="noopener"}

> Password reuse is a difficult vulnerability for IT teams to get full visibility over. Learn more from Specops Software on how to mitigate the risk of compromised credentials. [...]
