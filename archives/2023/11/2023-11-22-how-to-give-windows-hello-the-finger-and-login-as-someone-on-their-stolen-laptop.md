Title: How to give Windows Hello the finger and login as someone on their stolen laptop
Date: 2023-11-22T22:36:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-22-how-to-give-windows-hello-the-finger-and-login-as-someone-on-their-stolen-laptop

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/22/windows_hello_fingerprint_bypass/){:target="_blank" rel="noopener"}

> Not that we're encouraging anyone to defeat this fingerprint authentication Hardware security hackers have detailed how it's possible to bypass Windows Hello's fingerprint authentication and login as someone else – if you can steal or be left alone with their vulnerable device.... [...]
