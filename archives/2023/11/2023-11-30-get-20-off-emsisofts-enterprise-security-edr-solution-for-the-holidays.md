Title: Get 20% off Emsisoft's Enterprise Security EDR solution for the holidays
Date: 2023-11-30T16:24:49-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-11-30-get-20-off-emsisofts-enterprise-security-edr-solution-for-the-holidays

[Source](https://www.bleepingcomputer.com/news/security/get-20-percent-off-emsisofts-enterprise-security-edr-solution-for-the-holidays/){:target="_blank" rel="noopener"}

> Emsisoft is having a holiday deal where you can get 20% off 1-year licenses of the Emsisoft Enterprise Security EDR solution through December 17th, 2023, with no license limits. [...]
