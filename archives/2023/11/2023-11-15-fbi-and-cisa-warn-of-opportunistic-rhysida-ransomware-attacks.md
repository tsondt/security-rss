Title: FBI and CISA warn of opportunistic Rhysida ransomware attacks
Date: 2023-11-15T12:46:59-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-15-fbi-and-cisa-warn-of-opportunistic-rhysida-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/fbi-and-cisa-warn-of-opportunistic-rhysida-ransomware-attacks/){:target="_blank" rel="noopener"}

> The FBI and CISA warned today of Rhysida ransomware gang's opportunistic attacks targeting organizations across multiple industry sectors. [...]
