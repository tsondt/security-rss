Title: US slaps sanctions on accused fave go-to money launderer of Russia's rich
Date: 2023-11-06T16:15:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-06-us-slaps-sanctions-on-accused-fave-go-to-money-launderer-of-russias-rich

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/06/us_sanctions_russian_woman_for/){:target="_blank" rel="noopener"}

> And that includes ransomware crims, claims US of alleged sanctions-buster A Russian woman the US accuses of being a career money launderer is the latest to be sanctioned by the country for her alleged role in moving hundreds of millions of dollars on behalf of oligarchs and ransomware criminals.... [...]
