Title: VMware discloses critical VCD Appliance auth bypass with no patch
Date: 2023-11-14T16:45:56-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-14-vmware-discloses-critical-vcd-appliance-auth-bypass-with-no-patch

[Source](https://www.bleepingcomputer.com/news/security/vmware-discloses-critical-vcd-appliance-auth-bypass-with-no-patch/){:target="_blank" rel="noopener"}

> VMware disclosed a critical and unpatched authentication bypass vulnerability affecting Cloud Director appliance deployments. [...]
