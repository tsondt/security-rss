Title: Marina Bay Sands discloses data breach impacting 665,000 customers
Date: 2023-11-07T09:37:07-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-07-marina-bay-sands-discloses-data-breach-impacting-665000-customers

[Source](https://www.bleepingcomputer.com/news/security/marina-bay-sands-discloses-data-breach-impacting-665-000-customers/){:target="_blank" rel="noopener"}

> The Marina Bay Sands (MBS) luxury resort and casino in Singapore has disclosed a data breach that impacts personal data of 665,000 customers. [...]
