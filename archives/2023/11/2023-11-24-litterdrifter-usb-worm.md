Title: LitterDrifter USB Worm
Date: 2023-11-24T12:04:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;malware;Russia;Ukraine;USB
Slug: 2023-11-24-litterdrifter-usb-worm

[Source](https://www.schneier.com/blog/archives/2023/11/litterdrifter-usb-worm.html){:target="_blank" rel="noopener"}

> A new worm that spreads via USB sticks is infecting computers in Ukraine and beyond. The group­—known by many names, including Gamaredon, Primitive Bear, ACTINIUM, Armageddon, and Shuckworm—has been active since at least 2014 and has been attributed to Russia’s Federal Security Service by the Security Service of Ukraine. Most Kremlin-backed groups take pains to fly under the radar; Gamaredon doesn’t care to. Its espionage-motivated campaigns targeting large numbers of Ukrainian organizations are easy to detect and tie back to the Russian government. The campaigns typically revolve around malware that aims to obtain as much information from targets as possible. [...]
