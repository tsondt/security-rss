Title: Criminal IP Becomes VirusTotal IP and URL Scan Contributor
Date: 2023-11-21T10:01:02-05:00
Author: Sponsored by Criminal IP
Category: BleepingComputer
Tags: Security
Slug: 2023-11-21-criminal-ip-becomes-virustotal-ip-and-url-scan-contributor

[Source](https://www.bleepingcomputer.com/news/security/criminal-ip-becomes-virustotal-ip-and-url-scan-contributor/){:target="_blank" rel="noopener"}

> The Criminal IP Threat Intelligence (CTI) search engine has integrated its IP address and URL scans into VirusTotal. Learn more from Criminal IP about how this integration can help you. [...]
