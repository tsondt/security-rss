Title: Highly invasive backdoor snuck into open source packages targets developers
Date: 2023-11-08T18:27:47+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;code libraries;developers;malware;open source
Slug: 2023-11-08-highly-invasive-backdoor-snuck-into-open-source-packages-targets-developers

[Source](https://arstechnica.com/?p=1982281){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Highly invasive malware targeting software developers is once again circulating in Trojanized code libraries, with the latest ones downloaded thousands of times in the last eight months, researchers said Wednesday. Since January, eight separate developer tools have contained hidden payloads with various nefarious capabilities, security firm Checkmarx reported. The most recent one was released last month under the name "pyobfgood." Like the seven packages that preceded it, pyobfgood posed as a legitimate obfuscation tool that developers could use to deter reverse engineering and tampering with their code. Once executed, it installed a payload, giving the attacker [...]
