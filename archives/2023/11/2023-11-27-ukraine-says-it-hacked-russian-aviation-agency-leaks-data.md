Title: Ukraine says it hacked Russian aviation agency, leaks data
Date: 2023-11-27T13:23:50-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government
Slug: 2023-11-27-ukraine-says-it-hacked-russian-aviation-agency-leaks-data

[Source](https://www.bleepingcomputer.com/news/security/ukraine-says-it-hacked-russian-aviation-agency-leaks-data/){:target="_blank" rel="noopener"}

> Ukraine's intelligence service, operating under the Defense Ministry, claims they hacked Russia's Federal Air Transport Agency, 'Rosaviatsia,' to expose a purported collapse of Russia's aviation sector. [...]
