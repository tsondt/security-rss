Title: Cloudflare Dashboard and APIs down after data center power outage
Date: 2023-11-02T12:13:49-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-02-cloudflare-dashboard-and-apis-down-after-data-center-power-outage

[Source](https://www.bleepingcomputer.com/news/security/cloudflare-dashboard-and-apis-down-after-data-center-power-outage/){:target="_blank" rel="noopener"}

> An ongoing Cloudflare outage has taken down many of its products, including the company's dashboard and related application programming interfaces (APIs) customers use to manage and read service configurations. [...]
