Title: Hackers breach US water facility via exposed Unitronics PLCs
Date: 2023-11-29T13:07:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-29-hackers-breach-us-water-facility-via-exposed-unitronics-plcs

[Source](https://www.bleepingcomputer.com/news/security/hackers-breach-us-water-facility-via-exposed-unitronics-plcs/){:target="_blank" rel="noopener"}

> CISA (Cybersecurity & Infrastructure Security Agency) is warning that threat actors breached a U.S. water facility by hacking into Unitronics programmable logic controllers (PLCs) exposed online. [...]
