Title: BlackCat ransomware claims breach of healthcare giant Henry Schein
Date: 2023-11-02T14:55:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-02-blackcat-ransomware-claims-breach-of-healthcare-giant-henry-schein

[Source](https://www.bleepingcomputer.com/news/security/blackcat-ransomware-claims-breach-of-healthcare-giant-henry-schein/){:target="_blank" rel="noopener"}

> The BlackCat (ALPHV) ransomware gang claims it breached the network of healthcare giant Henry Schein and stole dozens of terabytes of data, including payroll data and shareholder information. [...]
