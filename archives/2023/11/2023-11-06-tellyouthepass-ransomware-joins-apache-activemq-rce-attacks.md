Title: TellYouThePass ransomware joins Apache ActiveMQ RCE attacks
Date: 2023-11-06T10:34:36-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-06-tellyouthepass-ransomware-joins-apache-activemq-rce-attacks

[Source](https://www.bleepingcomputer.com/news/security/tellyouthepass-ransomware-joins-apache-activemq-rce-attacks/){:target="_blank" rel="noopener"}

> Internet-exposed Apache ActiveMQ servers are also targeted in TellYouThePass ransomware attacks targeting a critical remote code execution (RCE) vulnerability previously exploited as a zero-day. [...]
