Title: Detecting data deletion and threats to backups with Security Command Center
Date: 2023-11-01T16:00:00+00:00
Author: Salim Hafid
Category: GCP Security
Tags: Storage & Data Transfer;Security & Identity
Slug: 2023-11-01-detecting-data-deletion-and-threats-to-backups-with-security-command-center

[Source](https://cloud.google.com/blog/products/identity-security/introducing-ransomware-and-threat-detection-for-backup-and-dr-in-security-command-center/){:target="_blank" rel="noopener"}

> The threat landscape is rapidly evolving, with data destruction attacks increasingly pervasive and more sophisticated than in years past. Today’s threat actors are constantly identifying new pathways to target data and to limit organizations’ ability to recover from attacks. Backups have long been the solution to rapid recovery from data destruction, which is why they are often targeted by attackers. Effective mitigation against the threat of a destructive cyber attack requires that your backup solution be purpose-built for cyber resiliency with security a core component, not bolted on afterwards. Google Cloud Backup and DR features built-in support for Cloud Logging [...]
