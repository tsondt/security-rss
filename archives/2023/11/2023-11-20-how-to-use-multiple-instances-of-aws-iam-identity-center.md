Title: How to use multiple instances of AWS IAM Identity Center
Date: 2023-11-20T21:55:09+00:00
Author: Laura Reith
Category: AWS Security
Tags: AWS IAM Identity Center;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security;Security Blog
Slug: 2023-11-20-how-to-use-multiple-instances-of-aws-iam-identity-center

[Source](https://aws.amazon.com/blogs/security/how-to-use-multiple-instances-of-aws-iam-identity-center/){:target="_blank" rel="noopener"}

> Recently, AWS launched a new feature that allows deployment of account instances of AWS IAM Identity Center. With this launch, you can now have two types of IAM Identity Center instances: organization instances and account instances. An organization instance is the IAM Identity Center instance that’s enabled in the management account of your organization [...]
