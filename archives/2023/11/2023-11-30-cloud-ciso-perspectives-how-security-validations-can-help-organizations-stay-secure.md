Title: Cloud CISO Perspectives: How security validations can help organizations stay secure
Date: 2023-11-30T17:00:00+00:00
Author: Earl Matthews
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2023-11-30-cloud-ciso-perspectives-how-security-validations-can-help-organizations-stay-secure

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-how-security-validations-can-help-organizations-stay-secure/){:target="_blank" rel="noopener"}

> Welcome to the second Cloud CISO Perspectives for November 2023. This month, Mandiant Consulting’s Earl Matthews discusses Security Validation, a vital tool that can give CISOs better information for making security decisions, and can help organizations understand their true security posture and risk profile. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. aside_block <ListValue: [StructValue([('title', 'Board of Directors Insights Hub'), ('body', <wagtail.rich_text.RichText object at 0x3ef3a9e52af0>), ('btn_text', 'Visit the Hub'), ('href', 'https://cloud.google.com/solutions/security/board-of-directors'), [...]
