Title: Cyberattack on IT provider CTS impacts dozens of UK law firms
Date: 2023-11-24T12:13:18-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-24-cyberattack-on-it-provider-cts-impacts-dozens-of-uk-law-firms

[Source](https://www.bleepingcomputer.com/news/security/cyberattack-on-it-provider-cts-impacts-dozens-of-uk-law-firms/){:target="_blank" rel="noopener"}

> A cyberattack on CTS, a leading managed service provider (MSP) for law firms and other organizations in the UK legal sector, is behind a major outage impacting numerous law firms and home buyers in the country since Wednesday. [...]
