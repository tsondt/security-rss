Title: WhatsApp now lets users hide their location during calls
Date: 2023-11-08T09:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-08-whatsapp-now-lets-users-hide-their-location-during-calls

[Source](https://www.bleepingcomputer.com/news/security/whatsapp-now-lets-users-hide-their-location-during-calls/){:target="_blank" rel="noopener"}

> WhatsApp is rolling out a new privacy feature that helps Android and iOS users hide their location during calls by relaying the connection through WhatsApp servers. [...]
