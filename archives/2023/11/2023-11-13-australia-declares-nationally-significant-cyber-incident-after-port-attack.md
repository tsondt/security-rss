Title: Australia declares 'nationally significant cyber incident' after port attack
Date: 2023-11-13T00:45:21+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-11-13-australia-declares-nationally-significant-cyber-incident-after-port-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/13/asia_tech_news_roundup/){:target="_blank" rel="noopener"}

> PLUS: Citrix quits China; Cambodia deports Japanese scammers; Chinese tech CEO disappears; and more Asia in brief Australia's National Cyber Security Coordinator has described an attack on logistics company DP World as a "nationally significant cyber incident."... [...]
