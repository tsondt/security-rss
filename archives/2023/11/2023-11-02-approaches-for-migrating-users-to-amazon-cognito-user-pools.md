Title: Approaches for migrating users to Amazon Cognito user pools
Date: 2023-11-02T19:13:17+00:00
Author: Edward Sun
Category: AWS Security
Tags: Advanced (300);Amazon Cognito;Best Practices;Security, Identity, & Compliance;Amazon Cognito User Pools;AWS Lambda;migration;Security Blog
Slug: 2023-11-02-approaches-for-migrating-users-to-amazon-cognito-user-pools

[Source](https://aws.amazon.com/blogs/security/approaches-for-migrating-users-to-amazon-cognito-user-pools/){:target="_blank" rel="noopener"}

> Update: An earlier version of this post was published on September 14, 2017, on the Front-End Web and Mobile Blog. Amazon Cognito user pools offer a fully managed OpenID Connect (OIDC) identity provider so you can quickly add authentication and control access to your mobile app or web application. User pools scale to millions of [...]
