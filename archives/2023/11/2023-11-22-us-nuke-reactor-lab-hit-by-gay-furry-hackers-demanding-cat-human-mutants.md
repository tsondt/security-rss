Title: US nuke reactor lab hit by 'gay furry hackers' demanding cat-human mutants
Date: 2023-11-22T21:38:12+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-11-22-us-nuke-reactor-lab-hit-by-gay-furry-hackers-demanding-cat-human-mutants

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/22/nuclear_lab_hacked/){:target="_blank" rel="noopener"}

> Staff records swiped, leaked by gang who probably read one too many comics, sorry, graphic novels The self-described "gay furry hackers" of SiegedSec are back: this time boasting they've broken into America's biggest nuclear power lab's computer systems and stolen records on thousands of employees. Some of that data has already been leaked, it appears.... [...]
