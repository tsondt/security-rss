Title: Black Basta ransomware operation nets over $100M from victims in less than two years
Date: 2023-11-30T13:15:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-30-black-basta-ransomware-operation-nets-over-100m-from-victims-in-less-than-two-years

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/30/black_basta_ransomware_operation_extorts/){:target="_blank" rel="noopener"}

> Assumed Conti offshoot averages 7 figures for each successful attack but may have issues with, er, 'closing deals' The Black Basta ransomware group has reportedly generated upwards of $100 million in revenue since it started operations in April 2022.... [...]
