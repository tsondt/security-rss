Title: Meeting the challenge of OT security
Date: 2023-11-01T08:38:10+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-11-01-meeting-the-challenge-of-ot-security

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/01/meeting_the_challenge_of_operational/){:target="_blank" rel="noopener"}

> Learn how Britvic eliminates blind spots in Operational Technology systems Webinar Cyberattacks on industrial control systems are becoming more common, and there isn't likely to be a let up any time soon.... [...]
