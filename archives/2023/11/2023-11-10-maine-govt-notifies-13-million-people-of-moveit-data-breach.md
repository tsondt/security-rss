Title: Maine govt notifies 1.3 million people of MOVEit data breach
Date: 2023-11-10T11:21:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-10-maine-govt-notifies-13-million-people-of-moveit-data-breach

[Source](https://www.bleepingcomputer.com/news/security/maine-govt-notifies-13-million-people-of-moveit-data-breach/){:target="_blank" rel="noopener"}

> The State of Maine has announced that its systems were breached after threat actors exploited a vulnerability in the MOVEit file transfer tool and accessed personal information of about 1.3 million, which is close to the state's entire population. [...]
