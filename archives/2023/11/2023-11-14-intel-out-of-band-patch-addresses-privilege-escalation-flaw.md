Title: Intel out-of-band patch addresses privilege escalation flaw
Date: 2023-11-14T18:00:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-11-14-intel-out-of-band-patch-addresses-privilege-escalation-flaw

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/14/intel_outofband_patch/){:target="_blank" rel="noopener"}

> Sapphire Rapids, Alder Lake, and Raptor Lake chip families treated for 'Redundant Prefix' Intel on Tuesday issued an out-of-band security update to address a privilege escalation vulnerability in recent server and personal computer chips.... [...]
