Title: FBI Director: FISA Section 702 warrant requirement a 'de facto ban'
Date: 2023-11-15T14:00:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-15-fbi-director-fisa-section-702-warrant-requirement-a-de-facto-ban

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/15/fbi_director_fisa_section_702/){:target="_blank" rel="noopener"}

> War of words escalates as deadline draws near FBI director Christopher Wray made yet another impassioned plea to US lawmakers to kill a proposed warrant requirement for so-called "US person queries" of data collected via the Feds' favorite snooping tool, FISA Section 702.... [...]
