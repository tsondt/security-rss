Title: Weak session keys let snoops take a byte out of your Bluetooth traffic
Date: 2023-11-30T07:32:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-11-30-weak-session-keys-let-snoops-take-a-byte-out-of-your-bluetooth-traffic

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/30/bluetooth_bluffs_attacks_are_no/){:target="_blank" rel="noopener"}

> BLUFFS spying flaw present in iPhones, ThinkPad, plenty of chipsets Multiple Bluetooth chips from major vendors such as Qualcomm, Broadcom, Intel, and Apple are vulnerable to a pair of security flaws that allow a nearby miscreant to impersonate other devices and intercept data.... [...]
