Title: New Microsoft Exchange zero-days allow RCE, data theft attacks
Date: 2023-11-03T11:14:56-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-11-03-new-microsoft-exchange-zero-days-allow-rce-data-theft-attacks

[Source](https://www.bleepingcomputer.com/news/microsoft/new-microsoft-exchange-zero-days-allow-rce-data-theft-attacks/){:target="_blank" rel="noopener"}

> Microsoft Exchange is impacted by four zero-day vulnerabilities that attackers can exploit remotely to execute arbitrary code or disclose sensitive information on affected installations. [...]
