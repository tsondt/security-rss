Title: Fake Ledger Live app in Microsoft Store steals $768,000 in crypto
Date: 2023-11-07T18:06:38-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-11-07-fake-ledger-live-app-in-microsoft-store-steals-768000-in-crypto

[Source](https://www.bleepingcomputer.com/news/security/fake-ledger-live-app-in-microsoft-store-steals-768-000-in-crypto/){:target="_blank" rel="noopener"}

> Microsoft has recently removed from its store a fraudulent Ledger Live app for cryptocurrency management after multiple users lost at least $768,000 worth of cryptocurrency assets. [...]
