Title: UK data watchdog fines three text spammers for flouting electronic marketing rules
Date: 2023-11-03T11:17:15+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2023-11-03-uk-data-watchdog-fines-three-text-spammers-for-flouting-electronic-marketing-rules

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/03/ico_text_spam_fines/){:target="_blank" rel="noopener"}

> 'High-pressure' sales tactics targeted people registered with Telephone Preference Service A "debt management company" is itself facing a bill from Britain's data regulator for sending hundreds of thousands of text messages to households that opted not to receive marketing junk mail.... [...]
