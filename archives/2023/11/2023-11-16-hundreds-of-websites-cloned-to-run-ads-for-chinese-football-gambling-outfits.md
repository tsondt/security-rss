Title: Hundreds of websites cloned to run ads for Chinese football gambling outfits
Date: 2023-11-16T03:31:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-11-16-hundreds-of-websites-cloned-to-run-ads-for-chinese-football-gambling-outfits

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/16/qurium_cloned_websites_chinese_gambling/){:target="_blank" rel="noopener"}

> Linked to org that UK authorities found once failed its anti-money-laundering obligations Swedish digital rights organization Qurium has discovered around 250 cloned websites and suggested they exist to drive people to China-linked gambling sites.... [...]
