Title: In a first, cryptographic keys protecting SSH connections stolen in new attack
Date: 2023-11-13T12:30:15+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Features;Security;bitflips;encryption keys;SSH
Slug: 2023-11-13-in-a-first-cryptographic-keys-protecting-ssh-connections-stolen-in-new-attack

[Source](https://arstechnica.com/?p=1983026){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) For the first time, researchers have demonstrated that a large portion of cryptographic keys used to protect data in computer-to-server SSH traffic are vulnerable to complete compromise when naturally occurring computational errors occur while the connection is being established. Underscoring the importance of their discovery, the researchers used their findings to calculate the private portion of almost 200 unique SSH keys they observed in public Internet scans taken over the past seven years. The researchers suspect keys used in IPsec connections could suffer the same fate. SSH is the cryptographic protocol used in secure shell connections [...]
