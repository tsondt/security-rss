Title: Yamaha Motor confirms ransomware attack on Philippines subsidiary
Date: 2023-11-17T11:45:27-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-17-yamaha-motor-confirms-ransomware-attack-on-philippines-subsidiary

[Source](https://www.bleepingcomputer.com/news/security/yamaha-motor-confirms-ransomware-attack-on-philippines-subsidiary/){:target="_blank" rel="noopener"}

> Yamaha Motor's Philippines motorcycle manufacturing subsidiary was hit by a ransomware attack last month, resulting in the theft and leak of some employees' personal information. [...]
