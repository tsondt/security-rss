Title: LockBit ransomware leaks gigabytes of Boeing data
Date: 2023-11-12T18:49:00-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-11-12-lockbit-ransomware-leaks-gigabytes-of-boeing-data

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-leaks-gigabytes-of-boeing-data/){:target="_blank" rel="noopener"}

> The LockBit ransomware gang published data stolen from Boeing, one of the largest aerospace companies that services commercial airplanes and defense systems. [...]
