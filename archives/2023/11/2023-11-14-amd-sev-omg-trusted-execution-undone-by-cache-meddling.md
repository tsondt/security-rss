Title: AMD SEV OMG: Trusted execution undone by cache meddling
Date: 2023-11-14T18:30:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-11-14-amd-sev-omg-trusted-execution-undone-by-cache-meddling

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/14/amd_trusted_execution/){:target="_blank" rel="noopener"}

> Let's do the CacheWarp again Boffins based in Germany and Austria have found a flaw in AMD's SEV trusted execution environment that makes it less than trustworthy.... [...]
