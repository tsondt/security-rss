Title: Police takes down BulletProftLink large-scale phishing provider
Date: 2023-11-11T11:06:32-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-11-police-takes-down-bulletproftlink-large-scale-phishing-provider

[Source](https://www.bleepingcomputer.com/news/security/police-takes-down-bulletproftlink-large-scale-phishing-provider/){:target="_blank" rel="noopener"}

> The notorious BulletProftLink phishing-as-a-service (PhaaS) platform that provided more than 300 phishing templates has been seized, the Royal Malaysian Police announced. [...]
