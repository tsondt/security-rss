Title: Qilin ransomware claims attack on automotive giant Yanfeng
Date: 2023-11-28T14:39:29-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-28-qilin-ransomware-claims-attack-on-automotive-giant-yanfeng

[Source](https://www.bleepingcomputer.com/news/security/qilin-ransomware-claims-attack-on-automotive-giant-yanfeng/){:target="_blank" rel="noopener"}

> The Qilin ransomware group has claimed responsibility for a cyber attack on Yanfeng Automotive Interiors (Yanfeng), one of the world's largest automotive parts suppliers. [...]
