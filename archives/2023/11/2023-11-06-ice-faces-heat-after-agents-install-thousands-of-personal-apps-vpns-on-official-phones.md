Title: ICE faces heat after agents install thousands of personal apps, VPNs on official phones
Date: 2023-11-06T22:33:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-06-ice-faces-heat-after-agents-install-thousands-of-personal-apps-vpns-on-official-phones

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/06/ice_device_security/){:target="_blank" rel="noopener"}

> Audit: Craptastic security could potentially put govt info in hands of enemies America's immigration cops have pushed back against an official probe that concluded their lax mobile device security potentially put sensitive government information at risk of being stolen by foreign snoops.... [...]
