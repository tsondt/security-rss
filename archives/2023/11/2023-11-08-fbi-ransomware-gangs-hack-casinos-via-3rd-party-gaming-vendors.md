Title: FBI: Ransomware gangs hack casinos via 3rd party gaming vendors
Date: 2023-11-08T11:44:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-08-fbi-ransomware-gangs-hack-casinos-via-3rd-party-gaming-vendors

[Source](https://www.bleepingcomputer.com/news/security/fbi-ransomware-gangs-hack-casinos-via-3rd-party-gaming-vendors/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation is warning that ransomware threat actors are targeting casino servers and use legitimate system management tools to increase their permissions on the network. [...]
