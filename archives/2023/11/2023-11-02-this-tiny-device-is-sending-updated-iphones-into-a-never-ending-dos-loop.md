Title: This tiny device is sending updated iPhones into a never-ending DoS loop
Date: 2023-11-02T11:15:24+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;Uncategorized;denial-of-service;flipper zero;radio frequency hacking
Slug: 2023-11-02-this-tiny-device-is-sending-updated-iphones-into-a-never-ending-dos-loop

[Source](https://arstechnica.com/?p=1980496){:target="_blank" rel="noopener"}

> Enlarge / A fully updated iPhone (left) after being force crashed by a Flipper Zero (right). (credit: Jeroen van der Ham) One morning two weeks ago, security researcher Jeroen van der Ham was traveling by train in the Netherlands when his iPhone suddenly displayed a series of pop-up windows that made it nearly impossible to use his device. “My phone was getting these popups every few minutes and then my phone would reboot,” he wrote to Ars in an online interview. “I tried putting it in lock down mode, but it didn't help.” To van der Ham’s surprise and chagrin, [...]
