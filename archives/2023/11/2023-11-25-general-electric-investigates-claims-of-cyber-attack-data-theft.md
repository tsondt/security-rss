Title: General Electric investigates claims of cyber attack, data theft
Date: 2023-11-25T17:05:57-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-11-25-general-electric-investigates-claims-of-cyber-attack-data-theft

[Source](https://www.bleepingcomputer.com/news/security/general-electric-investigates-claims-of-cyber-attack-data-theft/){:target="_blank" rel="noopener"}

> General Electric is investigating claims that a threat actor breached the company's development environment in a cyberattack and leaked allegedly stolen data. [...]
