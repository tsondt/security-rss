Title: Hackers breach healthcare orgs via ScreenConnect remote access
Date: 2023-11-10T14:57:04-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-11-10-hackers-breach-healthcare-orgs-via-screenconnect-remote-access

[Source](https://www.bleepingcomputer.com/news/security/hackers-breach-healthcare-orgs-via-screenconnect-remote-access/){:target="_blank" rel="noopener"}

> Security researchers are warning that hackers are targeting multiple healthcare organizations in the U.S. by abusing the ScreenConnect remote access tool. [...]
