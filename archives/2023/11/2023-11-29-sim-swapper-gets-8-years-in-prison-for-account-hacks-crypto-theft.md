Title: SIM swapper gets 8 years in prison for account hacks, crypto theft
Date: 2023-11-29T14:26:04-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-11-29-sim-swapper-gets-8-years-in-prison-for-account-hacks-crypto-theft

[Source](https://www.bleepingcomputer.com/news/security/sim-swapper-gets-8-years-in-prison-for-account-hacks-crypto-theft/){:target="_blank" rel="noopener"}

> Amir Hossein Golshan, 25, was sentenced to eight years in prison by a Los Angeles District Court and ordered to pay $1.2 million in restitution for crimes involving SIM swapping, merchant fraud, support fraud, account hacking, and cryptocurrency theft. [...]
