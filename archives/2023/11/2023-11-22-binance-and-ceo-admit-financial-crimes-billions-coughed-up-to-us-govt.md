Title: Binance and CEO admit financial crimes, billions coughed up to US govt
Date: 2023-11-22T01:02:30+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-11-22-binance-and-ceo-admit-financial-crimes-billions-coughed-up-to-us-govt

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/22/binance_ceo_settlement/){:target="_blank" rel="noopener"}

> Chief quits, pays own penalty after helping crooks launder cash, aiding sanctions evaders The world's largest cryptocurrency exchange just got a little smaller, with the US Department of Justice announcing Binance and its CEO Changpeng Zhao have both pleaded guilty to a multitude of financial crimes. As a result Binance will fork out $10 billion to Uncle Sam in fines and settlements.... [...]
