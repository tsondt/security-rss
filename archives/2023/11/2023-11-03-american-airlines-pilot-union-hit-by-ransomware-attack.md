Title: American Airlines pilot union hit by ransomware attack
Date: 2023-11-03T13:45:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-03-american-airlines-pilot-union-hit-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/american-airlines-pilot-union-hit-by-ransomware-attack/){:target="_blank" rel="noopener"}

> Allied Pilots Association (APA), a labor union representing 15,000 American Airlines pilots, disclosed a ransomware attack that hit its systems on Monday. [...]
