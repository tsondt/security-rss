Title: Microsoft extends Windows Server 2012 ESUs to October 2026
Date: 2023-11-10T11:50:51-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-11-10-microsoft-extends-windows-server-2012-esus-to-october-2026

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-extends-windows-server-2012-esus-to-october-2026/){:target="_blank" rel="noopener"}

> Microsoft provides three more years of Windows Server 2012 Extended Security Updates (ESUs) until October 2026, allowing administrators more time to upgrade or migrate to Azure. [...]
