Title: Healthcare giant Henry Schein hit twice by BlackCat ransomware
Date: 2023-11-27T14:44:20-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-27-healthcare-giant-henry-schein-hit-twice-by-blackcat-ransomware

[Source](https://www.bleepingcomputer.com/news/security/healthcare-giant-henry-schein-hit-twice-by-blackcat-ransomware/){:target="_blank" rel="noopener"}

> American healthcare company Henry Schein has reported a second cyberattack this month by the BlackCat/ALPHV ransomware gang, who also breached their network in October. [...]
