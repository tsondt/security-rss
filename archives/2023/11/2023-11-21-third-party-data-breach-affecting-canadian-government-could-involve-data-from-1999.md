Title: Third-party data breach affecting Canadian government could involve data from 1999
Date: 2023-11-21T13:21:40+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-21-third-party-data-breach-affecting-canadian-government-could-involve-data-from-1999

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/21/thirdparty_data_breach_at_canadian/){:target="_blank" rel="noopener"}

> Any govt staffers who used relocation services over past 24 years could be at risk The government of Canada has confirmed its data was accessed after two of its third-party service providers were attacked.... [...]
