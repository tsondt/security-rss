Title: Who’s Behind the SWAT USA Reshipping Service?
Date: 2023-11-06T13:51:31+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Ne'er-Do-Well News;Web Fraud 2.0;Apathyp;apathyr8@jabber.ccc.de;Constella Intelligence;gezze@mail.ru;gezze@yandex.ru;Intel 471;Ivan Sherban;myNetWatchman;SWAT USA Drop service;triploo@mail.ru;Universalo;Verified
Slug: 2023-11-06-whos-behind-the-swat-usa-reshipping-service

[Source](https://krebsonsecurity.com/2023/11/whos-behind-the-swat-usa-reshipping-service/){:target="_blank" rel="noopener"}

> Last week, KrebsOnSecurity broke the news that one of the largest cybercrime services for laundering stolen merchandise was hacked recently, exposing its internal operations, finances and organizational structure. In today’s Part II, we’ll examine clues about the real-life identity of “ Fearlless,” the nickname chosen by the proprietor of the SWAT USA Drops service. Based in Russia, SWAT USA recruits people in the United States to reship packages containing pricey electronics that are purchased with stolen credit cards. As detailed in this Nov. 2 story, SWAT currently employs more than 1,200 U.S. residents, all of whom will be cut loose [...]
