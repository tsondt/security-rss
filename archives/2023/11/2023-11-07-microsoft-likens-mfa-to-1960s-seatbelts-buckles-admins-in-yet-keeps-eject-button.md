Title: Microsoft likens MFA to 1960s seatbelts, buckles admins in yet keeps eject button
Date: 2023-11-07T17:45:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-07-microsoft-likens-mfa-to-1960s-seatbelts-buckles-admins-in-yet-keeps-eject-button

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/07/microsoft_likens_mfa_to_1960s/){:target="_blank" rel="noopener"}

> Admins have 90 days to opt out before MFA is deployed automatically Microsoft is introducing three Conditional Access policies for sysadmins as it continues to promote the implementation of multi-factor authentication (MFA) in organizations.... [...]
