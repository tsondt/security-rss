Title: Stop social engineering at the IT help desk
Date: 2023-11-23T09:09:06+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2023-11-23-stop-social-engineering-at-the-it-help-desk

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/23/stop_social_engineering_at_the/){:target="_blank" rel="noopener"}

> How Secure Service Desk thwarts social engineering attacks and secures user verification Sponsored Post Ransomware can hit any organization at any time, and hackers are proving adept at social engineering techniques to gain access to sensitive data in any way they can.... [...]
