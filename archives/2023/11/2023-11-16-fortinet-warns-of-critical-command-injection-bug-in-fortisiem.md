Title: Fortinet warns of critical command injection bug in FortiSIEM
Date: 2023-11-16T10:20:07-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-16-fortinet-warns-of-critical-command-injection-bug-in-fortisiem

[Source](https://www.bleepingcomputer.com/news/security/fortinet-warns-of-critical-command-injection-bug-in-fortisiem/){:target="_blank" rel="noopener"}

> Fortinet is alerting customers of a critical OS command injection vulnerability in FortiSIEM report server that could be exploited by remote, unauthenticated attackers to execute commands through specially crafted API requests. [...]
