Title: Ace Hardware says 1,202 devices were hit during cyberattack
Date: 2023-11-02T16:52:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-02-ace-hardware-says-1202-devices-were-hit-during-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/ace-hardware-says-1-202-devices-were-hit-during-cyberattack/){:target="_blank" rel="noopener"}

> Ace Hardware confirmed that a cyberattack is preventing local stores and customers from placing orders as the company works to restore 196 servers. [...]
