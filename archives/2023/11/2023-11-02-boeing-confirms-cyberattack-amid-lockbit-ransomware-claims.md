Title: Boeing confirms cyberattack amid LockBit ransomware claims
Date: 2023-11-02T11:16:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-02-boeing-confirms-cyberattack-amid-lockbit-ransomware-claims

[Source](https://www.bleepingcomputer.com/news/security/boeing-confirms-cyberattack-amid-lockbit-ransomware-claims/){:target="_blank" rel="noopener"}

> Aerospace giant Boeing is investigating a cyberattack that impacted its parts and distribution business after the LockBit ransomware gang claimed that they breached the company's network and stole data. [...]
