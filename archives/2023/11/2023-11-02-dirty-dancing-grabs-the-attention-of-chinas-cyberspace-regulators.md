Title: Dirty dancing grabs the attention of China's cyberspace regulators
Date: 2023-11-02T02:45:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-11-02-dirty-dancing-grabs-the-attention-of-chinas-cyberspace-regulators

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/02/china_internet_regulations_fines_minors/){:target="_blank" rel="noopener"}

> Alibaba service fined as Beijing calls for online platforms to name major creators and deploy kid-mode services China's Cyberspace Administration (CAC) has punished Alibaba-owned search engine Quark and livestreaming platform NetEase for content it deemed vulgar.... [...]
