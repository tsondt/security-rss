Title: Secret White House Warrantless Surveillance Program
Date: 2023-11-27T11:59:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;AT&T;law enforcement;NSA;privacy;surveillance
Slug: 2023-11-27-secret-white-house-warrantless-surveillance-program

[Source](https://www.schneier.com/blog/archives/2023/11/secret-white-house-warrantless-surveillance-program.html){:target="_blank" rel="noopener"}

> There seems to be no end to warrantless surveillance : According to the letter, a surveillance program now known as Data Analytical Services (DAS) has for more than a decade allowed federal, state, and local law enforcement agencies to mine the details of Americans’ calls, analyzing the phone records of countless people who are not suspected of any crime, including victims. Using a technique known as chain analysis, the program targets not only those in direct phone contact with a criminal suspect but anyone with whom those individuals have been in contact as well. The DAS program, formerly known as [...]
