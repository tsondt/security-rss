Title: Okta October breach affected 134 orgs, biz admits
Date: 2023-11-06T14:01:12+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-11-06-okta-october-breach-affected-134-orgs-biz-admits

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/06/security_in_brief/){:target="_blank" rel="noopener"}

> Plus: CVSS 4.0 is here, this week's critical vulns, and 'incident' hit loan broker promises no late fees. Generous Infosec in brief Okta has confirmed details of its October breach, reporting that the incident led to the compromise of files belonging to 134 customers, "or less than 1 percent of Okta customers."... [...]
