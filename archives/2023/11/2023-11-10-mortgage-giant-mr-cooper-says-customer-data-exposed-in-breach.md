Title: Mortgage giant Mr. Cooper says customer data exposed in breach
Date: 2023-11-10T16:57:31-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-10-mortgage-giant-mr-cooper-says-customer-data-exposed-in-breach

[Source](https://www.bleepingcomputer.com/news/security/mortgage-giant-mr-cooper-says-customer-data-exposed-in-breach/){:target="_blank" rel="noopener"}

> Mr. Cooper, the largest home loan servicer in the United States, says it found evidence of customer data exposed during a cyberattack disclosed last week, on October 31. [...]
