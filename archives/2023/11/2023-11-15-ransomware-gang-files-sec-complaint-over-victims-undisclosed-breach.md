Title: Ransomware gang files SEC complaint over victim’s undisclosed breach
Date: 2023-11-15T21:02:47-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-11-15-ransomware-gang-files-sec-complaint-over-victims-undisclosed-breach

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-files-sec-complaint-over-victims-undisclosed-breach/){:target="_blank" rel="noopener"}

> The ALPHV/BlackCat ransomware operation has taken extortion to a new level by filing a U.S. Securities and Exchange Commission complaint against one of their alleged victims for not complying with the four-day rule to disclose a cyberattack. [...]
