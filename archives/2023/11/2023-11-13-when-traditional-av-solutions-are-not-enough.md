Title: When traditional AV solutions are not enough
Date: 2023-11-13T10:10:43+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-11-13-when-traditional-av-solutions-are-not-enough

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/13/when_traditional_av_solutions_are/){:target="_blank" rel="noopener"}

> Preventing cybercriminals from exfiltrating your data with ADX technology Webinar It seems counterintuitive to want to lock in a cybercriminal who has crept past all your defences to smuggle data out from under your nose.... [...]
