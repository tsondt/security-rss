Title: MySQL servers targeted by 'Ddostf' DDoS-as-a-Service botnet
Date: 2023-11-16T15:11:49-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-16-mysql-servers-targeted-by-ddostf-ddos-as-a-service-botnet

[Source](https://www.bleepingcomputer.com/news/security/mysql-servers-targeted-by-ddostf-ddos-as-a-service-botnet/){:target="_blank" rel="noopener"}

> MySQL servers are being targeted by the 'Ddostf' malware botnet to enslave them for a DDoS-as-a-Service platform whose firepower is rented to other cybercriminals. [...]
