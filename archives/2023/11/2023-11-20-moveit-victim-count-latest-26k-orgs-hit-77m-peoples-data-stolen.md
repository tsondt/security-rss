Title: MOVEit victim count latest: 2.6K+ orgs hit, 77M+ people's data stolen
Date: 2023-11-20T20:39:38+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-20-moveit-victim-count-latest-26k-orgs-hit-77m-peoples-data-stolen

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/20/moveit_victim_77m_medical/){:target="_blank" rel="noopener"}

> Real-life impact of buggy software laid bare – plus: Avast tries to profit from being caught up in attacks Quick show of hands: whose data hasn't been stolen in the mass exploitation of Progress Software's vulnerable MOVEit file transfer application? Anyone?... [...]
