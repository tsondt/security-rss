Title: Decoupling for Security
Date: 2023-11-08T12:08:09+00:00
Author: B. Schneier
Category: Bruce Schneier
Tags: Uncategorized;cloud computing;computer security;privacy
Slug: 2023-11-08-decoupling-for-security

[Source](https://www.schneier.com/blog/archives/2023/11/decoupling-for-security.html){:target="_blank" rel="noopener"}

> This is an excerpt from a longer paper. You can read the whole thing (complete with sidebars and illustrations) here. Our message is simple: it is possible to get the best of both worlds. We can and should get the benefits of the cloud while taking security back into our own hands. Here we outline a strategy for doing that. What Is Decoupling? In the last few years, a slew of ideas old and new have converged to reveal a path out of this morass, but they haven’t been widely recognized, combined, or used. These ideas, which we’ll refer to [...]
