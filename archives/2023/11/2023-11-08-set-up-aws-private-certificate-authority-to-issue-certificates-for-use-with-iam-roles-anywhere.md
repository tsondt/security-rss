Title: Set up AWS Private Certificate Authority to issue certificates for use with IAM Roles Anywhere
Date: 2023-11-08T14:34:04+00:00
Author: Chris Sciarrino
Category: AWS Security
Tags: Advanced (300);AWS Identity and Access Management (IAM);AWS Private Certificate Authority;Multicloud;Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2023-11-08-set-up-aws-private-certificate-authority-to-issue-certificates-for-use-with-iam-roles-anywhere

[Source](https://aws.amazon.com/blogs/security/set-up-aws-private-certificate-authority-to-issue-certificates-for-use-with-iam-roles-anywhere/){:target="_blank" rel="noopener"}

> Traditionally, applications or systems—defined as pieces of autonomous logic functioning without direct user interaction—have faced challenges associated with long-lived credentials such as access keys. In certain circumstances, long-lived credentials can increase operational overhead and the scope of impact in the event of an inadvertent disclosure. To help mitigate these risks and follow the best practice [...]
