Title: Former infosec COO pleads guilty to attacking hospitals to drum up business
Date: 2023-11-20T17:15:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-20-former-infosec-coo-pleads-guilty-to-attacking-hospitals-to-drum-up-business

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/20/former_infosec_coo_pleads_guilty/){:target="_blank" rel="noopener"}

> Admits to taking phones used for 'code blue' emergencies offline and more An Atlanta tech company's former COO has pleaded guilty to a 2018 incident in which he deliberately launched online attacks on two hospitals, later citing the incidents in sales pitches.... [...]
