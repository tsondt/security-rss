Title: Rhysida ransomware gang claims British Library cyberattack
Date: 2023-11-20T10:44:25-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-20-rhysida-ransomware-gang-claims-british-library-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/rhysida-ransomware-gang-claims-british-library-cyberattack/){:target="_blank" rel="noopener"}

> The Rhysida ransomware gang has claimed responsibility for a cyberattack on the British Library in October, which has caused a major ongoing IT outage. [...]
