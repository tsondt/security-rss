Title: How to improve cross-account access for SaaS applications accessing customer accounts
Date: 2023-11-30T14:31:07+00:00
Author: Ashwin Phadke
Category: AWS Security
Tags: Advanced (300);AWS Identity and Access Management (IAM);Security, Identity, & Compliance;Technical How-to;SaaS;Security Blog
Slug: 2023-11-30-how-to-improve-cross-account-access-for-saas-applications-accessing-customer-accounts

[Source](https://aws.amazon.com/blogs/security/how-to-improve-cross-account-access-for-saas-applications-accessing-customer-accounts/){:target="_blank" rel="noopener"}

> Several independent software vendors (ISVs) and software as a service (SaaS) providers need to access their customers’ Amazon Web Services (AWS) accounts, especially if the SaaS product accesses data from customer environments. SaaS providers have adopted multiple variations of this third-party access scenario. In some cases, the providers ask the customer for an access key [...]
