Title: Staples confirms cyberattack behind service outages, delivery issues
Date: 2023-11-30T12:22:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-30-staples-confirms-cyberattack-behind-service-outages-delivery-issues

[Source](https://www.bleepingcomputer.com/news/security/staples-confirms-cyberattack-behind-service-outages-delivery-issues/){:target="_blank" rel="noopener"}

> American office supply retailer Staples took down some of its systems earlier this week after a cyberattack to contain the breach's impact and protect customer data. [...]
