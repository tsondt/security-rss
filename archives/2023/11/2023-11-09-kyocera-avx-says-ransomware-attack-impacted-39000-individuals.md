Title: Kyocera AVX says ransomware attack impacted 39,000 individuals
Date: 2023-11-09T16:43:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-09-kyocera-avx-says-ransomware-attack-impacted-39000-individuals

[Source](https://www.bleepingcomputer.com/news/security/kyocera-avx-says-ransomware-attack-impacted-39-000-individuals/){:target="_blank" rel="noopener"}

> Kyocera AVX Components Corporation (KAVX) is sending notices of a data breach exposing personal information of 39,111 individuals following a ransomware attack. [...]
