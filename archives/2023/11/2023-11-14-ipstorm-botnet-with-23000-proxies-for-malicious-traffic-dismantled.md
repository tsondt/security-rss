Title: IPStorm botnet with 23,000 proxies for malicious traffic dismantled
Date: 2023-11-14T19:05:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2023-11-14-ipstorm-botnet-with-23000-proxies-for-malicious-traffic-dismantled

[Source](https://www.bleepingcomputer.com/news/security/ipstorm-botnet-with-23-000-proxies-for-malicious-traffic-dismantled/){:target="_blank" rel="noopener"}

> The U.S. Department of Justive announced today that Federal Bureau of Investigation took down the network and infrastructure of a botnet proxy service called IPStorm. [...]
