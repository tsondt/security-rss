Title: Strangely enough, no one wants to buy a ransomware group that has cops' attention
Date: 2023-11-10T15:36:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-10-strangely-enough-no-one-wants-to-buy-a-ransomware-group-that-has-cops-attention

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/10/ransomedvc_shut_down/){:target="_blank" rel="noopener"}

> Ransomed.vc shuts after 20% discount fails to entice bids Short-lived ransomware outfit Ransomed.vc claims to have shut down for good after a number of suspected arrests.... [...]
