Title: OpenCart owner turns air blue after researcher discloses serious vuln
Date: 2023-11-24T15:32:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-24-opencart-owner-turns-air-blue-after-researcher-discloses-serious-vuln

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/24/opencart_vulnerability_dispute/){:target="_blank" rel="noopener"}

> Web storefront maker fixed the flaw, but not before blasting infoseccer The owner of the e-commerce store management system OpenCart has responded with hostility to a security researcher disclosing a vulnerability in the product.... [...]
