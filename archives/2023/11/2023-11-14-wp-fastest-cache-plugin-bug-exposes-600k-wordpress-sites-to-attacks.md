Title: WP Fastest Cache plugin bug exposes 600K WordPress sites to attacks
Date: 2023-11-14T18:32:11-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-14-wp-fastest-cache-plugin-bug-exposes-600k-wordpress-sites-to-attacks

[Source](https://www.bleepingcomputer.com/news/security/wp-fastest-cache-plugin-bug-exposes-600k-wordpress-sites-to-attacks/){:target="_blank" rel="noopener"}

> The WordPress plugin WP Fastest Cache is vulnerable to an SQL injection vulnerability that could allow unauthenticated attackers to read the contents of the site's database. [...]
