Title: New CacheWarp AMD CPU attack lets hackers gain root in Linux VMs
Date: 2023-11-14T15:34:15-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-14-new-cachewarp-amd-cpu-attack-lets-hackers-gain-root-in-linux-vms

[Source](https://www.bleepingcomputer.com/news/security/new-cachewarp-amd-cpu-attack-lets-hackers-gain-root-in-linux-vms/){:target="_blank" rel="noopener"}

> A new software-based fault injection attack, CacheWarp, can let threat actors hack into AMD SEV-protected virtual machines by targeting memory writes to escalate privileges and gain remote code execution. [...]
