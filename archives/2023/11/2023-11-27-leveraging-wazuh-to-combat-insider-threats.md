Title: Leveraging Wazuh to combat insider threats
Date: 2023-11-27T10:02:04-05:00
Author: Sponsored by Wazuh
Category: BleepingComputer
Tags: Security
Slug: 2023-11-27-leveraging-wazuh-to-combat-insider-threats

[Source](https://www.bleepingcomputer.com/news/security/leveraging-wazuh-to-combat-insider-threats/){:target="_blank" rel="noopener"}

> Effective strategies for mitigating insider threats involve a combination of detective and preventive controls. Such controls are provided by the Wazuh SIEM and XDR platform. [...]
