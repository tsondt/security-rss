Title: China's top bank ICBC hit by ransomware, derailing global trades
Date: 2023-11-10T08:00:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-11-10-chinas-top-bank-icbc-hit-by-ransomware-derailing-global-trades

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/10/icbc_ransomware/){:target="_blank" rel="noopener"}

> CitrixBleed patch has been available for around a month China's largest bank, ICBC, was hit by ransomware that resulted in disruption of financial services (FS) systems on Thursday Beijing time, according to a notice on its website.... [...]
