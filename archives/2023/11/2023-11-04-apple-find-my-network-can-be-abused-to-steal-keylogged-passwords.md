Title: Apple 'Find My' network can be abused to steal keylogged passwords
Date: 2023-11-04T10:12:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Apple;Security
Slug: 2023-11-04-apple-find-my-network-can-be-abused-to-steal-keylogged-passwords

[Source](https://www.bleepingcomputer.com/news/apple/apple-find-my-network-can-be-abused-to-steal-keylogged-passwords/){:target="_blank" rel="noopener"}

> Apple's "Find My" location network can be abused by malicious actors to stealthily transmit sensitive information captured by keyloggers installed in keyboards. [...]
