Title: Microsoft launches Defender Bounty Program with $20,000 rewards
Date: 2023-11-21T14:13:42-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-11-21-microsoft-launches-defender-bounty-program-with-20000-rewards

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-launches-defender-bounty-program-with-20-000-rewards/){:target="_blank" rel="noopener"}

> Microsoft has unveiled a new bug bounty program aimed at the Microsoft Defender security platform, with rewards between $500 and $20,000. [...]
