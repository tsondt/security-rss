Title: Toyota confirms breach after Medusa ransomware threatens to leak data
Date: 2023-11-16T14:02:02-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-16-toyota-confirms-breach-after-medusa-ransomware-threatens-to-leak-data

[Source](https://www.bleepingcomputer.com/news/security/toyota-confirms-breach-after-medusa-ransomware-threatens-to-leak-data/){:target="_blank" rel="noopener"}

> Toyota Financial Services (TFS) has confirmed that it detected unauthorized access on some of its systems in Europe and Africa after Medusa ransomware claimed an attack on the company. [...]
