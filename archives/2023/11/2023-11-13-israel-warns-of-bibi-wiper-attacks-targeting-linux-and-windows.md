Title: Israel warns of BiBi wiper attacks targeting Linux and Windows
Date: 2023-11-13T11:53:42-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-13-israel-warns-of-bibi-wiper-attacks-targeting-linux-and-windows

[Source](https://www.bleepingcomputer.com/news/security/israel-warns-of-bibi-wiper-attacks-targeting-linux-and-windows/){:target="_blank" rel="noopener"}

> Data-wiping attacks are becoming more frequent on Israeli computers as researchers discovered variants of the BiBi malware family that destroys data on both Linux and Windows systems. [...]
