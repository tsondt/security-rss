Title: Russia's Sandworm – not just missile strikes – to blame for Ukrainian power blackouts
Date: 2023-11-09T08:00:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-09-russias-sandworm-not-just-missile-strikes-to-blame-for-ukrainian-power-blackouts

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/09/russias_sandworm_power_plant_attack/){:target="_blank" rel="noopener"}

> Online attack coincided with major military action, Mandiant says Blackouts in Ukraine last year were not just caused by missile strikes on the nation but also by a seemingly coordinated cyberattack on one of its power plants. That's according to Mandiant's threat intel team, which said Russia's Sandworm crew was behind the two-pronged power-outage and data-wiping attack.... [...]
