Title: Auto parts giant AutoZone warns of MOVEit data breach
Date: 2023-11-21T13:03:22-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-21-auto-parts-giant-autozone-warns-of-moveit-data-breach

[Source](https://www.bleepingcomputer.com/news/security/auto-parts-giant-autozone-warns-of-moveit-data-breach/){:target="_blank" rel="noopener"}

> AutoZone is warning tens of thousands of its customers that it suffered a data breach as part of the Clop MOVEit file transfer attacks. [...]
