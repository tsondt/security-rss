Title: Microsoft: Lazarus hackers breach CyberLink in supply chain attack
Date: 2023-11-22T13:06:25-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-11-22-microsoft-lazarus-hackers-breach-cyberlink-in-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/microsoft-lazarus-hackers-breach-cyberlink-in-supply-chain-attack/){:target="_blank" rel="noopener"}

> Microsoft says a North Korean hacking group has breached Taiwanese multimedia software company CyberLink and trojanized one of its installers to push malware in a supply chain attack targeting potential victims worldwide. [...]
