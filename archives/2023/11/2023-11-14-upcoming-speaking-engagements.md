Title: Upcoming Speaking Engagements
Date: 2023-11-14T17:01:22+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2023-11-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2023/11/upcoming-speaking-engagements-32.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking at the AI Summit New York on December 6, 2023. The list is maintained on this page. [...]
