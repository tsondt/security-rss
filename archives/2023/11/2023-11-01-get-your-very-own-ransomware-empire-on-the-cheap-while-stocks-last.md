Title: Get your very own ransomware empire on the cheap, while stocks last
Date: 2023-11-01T11:48:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-01-get-your-very-own-ransomware-empire-on-the-cheap-while-stocks-last

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/01/ransomedvc_owner_sells_operation/){:target="_blank" rel="noopener"}

> RansomedVC owner takes to Telegram to flog criminal enterprise The short-lived RansomedVC ransomware operation is being shopped around by its owner, who is claiming to offer a 20 percent discount just a day after first listing it for sale.... [...]
