Title: Dollar Tree hit by third-party data breach impacting 2 million people
Date: 2023-11-29T16:25:09-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-29-dollar-tree-hit-by-third-party-data-breach-impacting-2-million-people

[Source](https://www.bleepingcomputer.com/news/security/dollar-tree-hit-by-third-party-data-breach-impacting-2-million-people/){:target="_blank" rel="noopener"}

> Discount store chain Dollar Tree was impacted by a third-party data breach affecting 1,977,486 people after the hack of service provider Zeroed-In Technologies. [...]
