Title: Ransomware more efficient than ever, and baddies are still after your logs
Date: 2023-11-15T09:30:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-15-ransomware-more-efficient-than-ever-and-baddies-are-still-after-your-logs

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/15/ransomware_more_efficient_than_ever/){:target="_blank" rel="noopener"}

> Trying times for incident responders who battle fastest-ever ransomware blitz as attackers keep scrubbing evidence clean Organizations are still failing to implement adequate logging measures, increasing the difficulty faced by defenders and incident responders to identify the cause of infosec attacks.... [...]
