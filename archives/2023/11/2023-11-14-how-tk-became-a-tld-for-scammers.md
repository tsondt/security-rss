Title: How .tk Became a TLD for Scammers
Date: 2023-11-14T12:06:15+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;domain names;scams
Slug: 2023-11-14-how-tk-became-a-tld-for-scammers

[Source](https://www.schneier.com/blog/archives/2023/11/how-tk-became-a-tld-for-scammers.html){:target="_blank" rel="noopener"}

> Sad story of Tokelau, and how its top-level domain “became the unwitting host to the dark underworld by providing a never-ending supply of domain names that could be weaponized against internet users. Scammers began using.tk websites to do everything from harvesting passwords and payment information to displaying pop-up ads or delivering malware.” [...]
