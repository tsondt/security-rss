Title: 81K people's sensitive info feared stolen from Hilb after email inboxes ransacked
Date: 2023-11-03T20:26:45+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-03-81k-peoples-sensitive-info-feared-stolen-from-hilb-after-email-inboxes-ransacked

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/03/hilb_data_leak/){:target="_blank" rel="noopener"}

> Credit card numbers, security codes, SSNs, passwords, PINs? Yikes! Hilb Group has warned more than 81,000 people that around the start of 2023 criminals broke into the work email accounts of its employees and may have stolen a bunch of sensitive personal information.... [...]
