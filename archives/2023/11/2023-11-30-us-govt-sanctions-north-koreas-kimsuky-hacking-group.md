Title: US govt sanctions North Korea’s Kimsuky hacking group
Date: 2023-11-30T17:08:52-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-30-us-govt-sanctions-north-koreas-kimsuky-hacking-group

[Source](https://www.bleepingcomputer.com/news/security/us-govt-sanctions-north-koreas-kimsuky-hacking-group/){:target="_blank" rel="noopener"}

> The Treasury Department's Office of Foreign Assets Control (OFAC) has sanctioned the North Korean-backed Kimsuky hacking group for stealing intelligence in support of the country's strategic goals. [...]
