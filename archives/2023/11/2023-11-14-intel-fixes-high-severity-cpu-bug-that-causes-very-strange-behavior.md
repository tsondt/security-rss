Title: Intel fixes high-severity CPU bug that causes “very strange behavior”
Date: 2023-11-14T20:57:58+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;Uncategorized;central processing units;cloud;CPU;Intel;microcode;Virtual Machines
Slug: 2023-11-14-intel-fixes-high-severity-cpu-bug-that-causes-very-strange-behavior

[Source](https://arstechnica.com/?p=1983867){:target="_blank" rel="noopener"}

> Enlarge Intel on Tuesday pushed microcode updates to fix a high-severity CPU bug that has the potential to be maliciously exploited against cloud-based hosts. The flaw, affecting virtually all modern Intel CPUs, causes them to “enter a glitch state where the normal rules don’t apply,” Tavis Ormandy, one of several security researchers inside Google who discovered the bug, reported. Once triggered, the glitch state results in unexpected and potentially serious behavior, most notably system crashes that occur even when untrusted code is executed within a guest account of a virtual machine, which, under most cloud security models, is assumed to [...]
