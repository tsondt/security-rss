Title: Ransomware crooks SIM swap medical research biz exec, threaten to leak stolen data
Date: 2023-11-01T22:46:14+00:00
Author: Connor Jones, Chris Williams, and Iain Thomson
Category: The Register
Tags: 
Slug: 2023-11-01-ransomware-crooks-sim-swap-medical-research-biz-exec-threaten-to-leak-stolen-data

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/01/advarra_ransomware_alphv/){:target="_blank" rel="noopener"}

> Advarra probes intrusion claims, says 'the matter is contained' Ransomware crooks claim they've stolen data from a firm that helps other organizations run medical trials after one of its executives had their cellphone number and accounts hijacked.... [...]
