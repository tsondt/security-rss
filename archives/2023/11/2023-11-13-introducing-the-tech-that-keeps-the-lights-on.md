Title: Introducing the tech that keeps the lights on
Date: 2023-11-13T10:15:06+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2023-11-13-introducing-the-tech-that-keeps-the-lights-on

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/13/data_diodes_comment/){:target="_blank" rel="noopener"}

> Genuinely new ideas are rare in IT – this superhero is ready to make a real difference Opinion Cybersecurity has many supremely annoying aspects. It soaks up talent, time, and money like the English men's football squad, and like that benighted institution, the results never seem to change.... [...]
