Title: Okta hit by another breach, this one stealing employee data from 3rd-party vendor
Date: 2023-11-02T21:41:41+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;1password;breach;cloudflare;hacking;okta
Slug: 2023-11-02-okta-hit-by-another-breach-this-one-stealing-employee-data-from-3rd-party-vendor

[Source](https://arstechnica.com/?p=1980825){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Identity and authentication management provider Okta has been hit by another breach, this one against a third-party vendor that allowed hackers to steal personal information for 5,000 Okta employees. The compromise was carried out in late September against Rightway Healthcare, a service Okta uses to support employees and their dependents in finding health care providers and plan rates. An unidentified threat actor gained access to Rightway’s network and made off with an eligibility census file the vendor maintained on behalf of Okta. Okta learned of the compromise and data theft on October 12 and didn’t disclose [...]
