Title: Veeam warns of critical bugs in Veeam ONE monitoring platform
Date: 2023-11-06T16:58:27-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-06-veeam-warns-of-critical-bugs-in-veeam-one-monitoring-platform

[Source](https://www.bleepingcomputer.com/news/security/veeam-warns-of-critical-bugs-in-veeam-one-monitoring-platform/){:target="_blank" rel="noopener"}

> Veeam released hotfixes today to address four vulnerabilities in the company's Veeam ONE IT infrastructure monitoring and analytics platform, two of them critical. [...]
