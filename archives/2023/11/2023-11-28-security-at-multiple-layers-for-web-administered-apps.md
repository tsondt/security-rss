Title: Security at multiple layers for web-administered apps
Date: 2023-11-28T14:26:37+00:00
Author: Guy Morton
Category: AWS Security
Tags: Advanced (300);Best Practices;Security, Identity, & Compliance;Amazon CloudFront;Amazon GuardDuty;Amazon Virtual Private Cloud (Amazon VPC);Amazon VPC;AWS GuardDuty;AWS IAM;AWS VPC;Elastic Load Balancing;GuardDuty;IAM;Security Blog;VPC
Slug: 2023-11-28-security-at-multiple-layers-for-web-administered-apps

[Source](https://aws.amazon.com/blogs/security/security-at-multiple-layers-for-web-administered-apps/){:target="_blank" rel="noopener"}

> In this post, I will show you how to apply security at multiple layers of a web application hosted on AWS. Apply security at all layers is a design principle of the Security pillar of the AWS Well-Architected Framework. It encourages you to apply security at the network edge, virtual private cloud (VPC), load balancer, [...]
