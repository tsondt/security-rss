Title: UK may demand tech world tell it about upcoming security features
Date: 2023-11-07T16:34:11+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2023-11-07-uk-may-demand-tech-world-tell-it-about-upcoming-security-features

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/07/ukgov_wants_prior_notice_of/){:target="_blank" rel="noopener"}

> Campaigners say proposals to reform laws are 'dangerous' and an attack on safety The UK government has set in train plans to introduce legislation requiring tech companies to let it know when they plan to introduce new security technologies and could potentially force them to disable when required.... [...]
