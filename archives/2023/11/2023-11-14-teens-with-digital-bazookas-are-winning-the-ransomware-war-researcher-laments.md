Title: Teens with “digital bazookas” are winning the ransomware war, researcher laments
Date: 2023-11-14T14:57:34+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;Boeing;citrixbleed;lockbit;ransomware
Slug: 2023-11-14-teens-with-digital-bazookas-are-winning-the-ransomware-war-researcher-laments

[Source](https://arstechnica.com/?p=1983601){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) What do Boeing, an Australian shipping company, the world’s largest bank, and one of the world’s biggest law firms have in common? All four have suffered cybersecurity breaches, most likely at the hands of teenage hackers, after failing to patch a critical vulnerability that security experts have warned of for more than a month, according to a post published Monday. Besides the US jetliner manufacturer, the victims include DP World, the Australian branch of the Dubai-based logistics company DP World; Industrial and Commercial Bank of China; and Allen & Overy, a multinational law firm, according [...]
