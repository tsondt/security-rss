Title: PJ&A says cyberattack exposed data of nearly 9 million patients
Date: 2023-11-15T11:13:59-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-11-15-pja-says-cyberattack-exposed-data-of-nearly-9-million-patients

[Source](https://www.bleepingcomputer.com/news/security/pj-and-a-says-cyberattack-exposed-data-of-nearly-9-million-patients/){:target="_blank" rel="noopener"}

> PJ&A (Perry Johnson & Associates) is warning that a cyberattack in March 2023 exposed the personal information of almost nine million patients. [...]
