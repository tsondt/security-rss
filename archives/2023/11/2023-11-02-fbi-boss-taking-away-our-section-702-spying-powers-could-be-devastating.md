Title: FBI boss: Taking away our Section 702 spying powers could be 'devastating'
Date: 2023-11-02T01:22:55+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-02-fbi-boss-taking-away-our-section-702-spying-powers-could-be-devastating

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/02/fbi_section_702_spying/){:target="_blank" rel="noopener"}

> Of course, he would say that, wouldn't he? As the expiration date for the Feds' Section 702 surveillance powers draws closer, FBI Director Christopher Wray has warned a US Senate committee that his agents may not be able to stop the next major cyberattack if lawmakers allow the contentious spying authorization to lapse.... [...]
