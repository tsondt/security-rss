Title: QNAP warns of critical command injection flaws in QTS OS, apps
Date: 2023-11-06T07:47:37-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud
Slug: 2023-11-06-qnap-warns-of-critical-command-injection-flaws-in-qts-os-apps

[Source](https://www.bleepingcomputer.com/news/security/qnap-warns-of-critical-command-injection-flaws-in-qts-os-apps/){:target="_blank" rel="noopener"}

> QNAP Systems published security advisories for two critical command injection vulnerabilities that impact multiple versions of the QTS operating system and applications on its network-attached storage (NAS) devices. [...]
