Title: Writing IAM Policies: Grant Access to User-Specific Folders in an Amazon S3 Bucket
Date: 2023-11-14T18:06:18+00:00
Author: Dylan Souvage
Category: AWS Security
Tags: Amazon Simple Storage Service (S3);AWS IAM Identity Center;AWS Identity and Access Management (IAM);Intermediate (200);Security;Security, Identity, & Compliance;Storage;Technical How-to;Top Posts;Best of;Best Practices;How-to guides;IAM;IAM principals;S3;Security Blog
Slug: 2023-11-14-writing-iam-policies-grant-access-to-user-specific-folders-in-an-amazon-s3-bucket

[Source](https://aws.amazon.com/blogs/security/writing-iam-policies-grant-access-to-user-specific-folders-in-an-amazon-s3-bucket/){:target="_blank" rel="noopener"}

> November 14, 2023: We’ve updated this post to use IAM Identity Center and follow updated IAM best practices. In this post, we discuss the concept of folders in Amazon Simple Storage Service (Amazon S3) and how to use policies to restrict access to these folders. The idea is that by properly managing permissions, you can [...]
