Title: New Rust-based SysJoker backdoor linked to Hamas hackers
Date: 2023-11-26T10:09:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-26-new-rust-based-sysjoker-backdoor-linked-to-hamas-hackers

[Source](https://www.bleepingcomputer.com/news/security/new-rust-based-sysjoker-backdoor-linked-to-hamas-hackers/){:target="_blank" rel="noopener"}

> A new version of the multi-platform malware known as 'SysJoker' has been spotted, featuring a complete code rewrite in the Rust programming language. [...]
