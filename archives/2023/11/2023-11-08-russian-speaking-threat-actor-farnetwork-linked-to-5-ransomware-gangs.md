Title: Russian-speaking threat actor "farnetwork" linked to 5 ransomware gangs
Date: 2023-11-08T04:32:39-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-08-russian-speaking-threat-actor-farnetwork-linked-to-5-ransomware-gangs

[Source](https://www.bleepingcomputer.com/news/security/russian-speaking-threat-actor-farnetwork-linked-to-5-ransomware-gangs/){:target="_blank" rel="noopener"}

> The operator of the Nokoyawa ransomware-as-a-service (RaaS), a threat actor known as 'farnetwork', built experience over the years by helping the JSWORM, Nefilim, Karma, and Nemty affiliate programs with malware development and operation management. [...]
