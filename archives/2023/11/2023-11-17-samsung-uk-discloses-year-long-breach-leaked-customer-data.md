Title: Samsung UK discloses year-long breach, leaked customer data
Date: 2023-11-17T05:58:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-11-17-samsung-uk-discloses-year-long-breach-leaked-customer-data

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/17/uk_samsung_electronics_discloses_yearlong/){:target="_blank" rel="noopener"}

> Chaebol already the subject of suits for a pair of past indiscretions Updated The UK division of Samsung Electronics has allegedly alerted customers of a year-long data security breach – the third such incident the South Korean giant has experienced around the world in the past two years.... [...]
