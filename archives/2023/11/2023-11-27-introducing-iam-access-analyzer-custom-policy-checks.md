Title: Introducing IAM Access Analyzer custom policy checks
Date: 2023-11-27T14:00:04+00:00
Author: Mitch Beaumont
Category: AWS Security
Tags: Advanced (300);Announcements;Security, Identity, & Compliance;Technical How-to;Automated reasoning;AWS IAM;AWS Identity and Access Management;AWS Identity and Access Management (IAM);Devops;DevSecOps;IAM;IAM Access Analyzer;IAM policies;Identity and Access Management;Security Blog
Slug: 2023-11-27-introducing-iam-access-analyzer-custom-policy-checks

[Source](https://aws.amazon.com/blogs/security/introducing-iam-access-analyzer-custom-policy-checks/){:target="_blank" rel="noopener"}

> AWS Identity and Access Management (IAM) Access Analyzer was launched in late 2019. Access Analyzer guides customers toward least-privilege permissions across Amazon Web Services (AWS) by using analysis techniques, such as automated reasoning, to make it simpler for customers to set, verify, and refine IAM permissions. Today, we are excited to announce the general availability [...]
