Title: Microsoft Authenticator now blocks suspicious MFA alerts by default
Date: 2023-11-07T10:40:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-11-07-microsoft-authenticator-now-blocks-suspicious-mfa-alerts-by-default

[Source](https://www.bleepingcomputer.com/news/security/microsoft-authenticator-now-blocks-suspicious-mfa-alerts-by-default/){:target="_blank" rel="noopener"}

> Microsoft has introduced a new protective feature in the Authenticator app to block notifications that appear suspicious based on specific checks performed during the account login stage. [...]
