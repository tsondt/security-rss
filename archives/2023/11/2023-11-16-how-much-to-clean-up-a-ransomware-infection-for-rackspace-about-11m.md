Title: How much to clean up a ransomware infection? For Rackspace, about $11M
Date: 2023-11-16T21:23:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-16-how-much-to-clean-up-a-ransomware-infection-for-rackspace-about-11m

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/16/rackspace_ransomware_expenses/){:target="_blank" rel="noopener"}

> And that's not counting the incoming lawsuits. Thank goodness for insurance, eh? Rackspace's costs from last year's ransomware infection continue to mount. The cloud hosting biz has told America's financial watchdog, the SEC, its total expenses to date regarding that cyberattack have now reached about $11 million, though insurance has helped cover half of that.... [...]
