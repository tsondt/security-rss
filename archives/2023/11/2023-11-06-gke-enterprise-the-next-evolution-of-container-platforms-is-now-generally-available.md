Title: GKE Enterprise, the next evolution of container platforms, is now generally available
Date: 2023-11-06T16:00:00+00:00
Author: Dave Bartoletti
Category: GCP Security
Tags: Application Modernization;Security & Identity;Containers & Kubernetes
Slug: 2023-11-06-gke-enterprise-the-next-evolution-of-container-platforms-is-now-generally-available

[Source](https://cloud.google.com/blog/products/containers-kubernetes/gke-enterprise-is-now-ga/){:target="_blank" rel="noopener"}

> Today, we are thrilled to announce that GKE Enterprise, the premium edition of GKE, will be generally available on November 15, 2023. With GKE Enterprise, companies can increase development and deployment velocity across multiple teams, easily and securely run their most important business-critical workloads, and reduce total cost of ownership with a fully integrated and managed solution from Google Cloud. GKE Enterprise: the next evolution of Kubernetes from Google Cloud GKE Enterprise builds on Google Cloud’s leadership in containers and Kubernetes, bringing together the best of GKE and Anthos into an integrated and intuitive container platform, with a unified console [...]
