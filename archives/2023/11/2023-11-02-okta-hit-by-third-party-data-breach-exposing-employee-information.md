Title: Okta hit by third-party data breach exposing employee information
Date: 2023-11-02T10:09:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-11-02-okta-hit-by-third-party-data-breach-exposing-employee-information

[Source](https://www.bleepingcomputer.com/news/security/okta-hit-by-third-party-data-breach-exposing-employee-information/){:target="_blank" rel="noopener"}

> Okta is warning nearly 5,000 current and former employees that their personal information was exposed after a third-party vendor was breached. [...]
