Title: It’s Still Easy for Anyone to Become You at Experian
Date: 2023-11-11T17:59:07+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Experian;identity theft;Scott Anderson
Slug: 2023-11-11-its-still-easy-for-anyone-to-become-you-at-experian

[Source](https://krebsonsecurity.com/2023/11/its-still-easy-for-anyone-to-become-you-at-experian/){:target="_blank" rel="noopener"}

> In the summer of 2022, KrebsOnSecurity documented the plight of several readers who had their accounts at big-three consumer credit reporting bureau Experian hijacked after identity thieves simply re-registered the accounts using a different email address. Sixteen months later, Experian clearly has not addressed this gaping lack of security. I know that because my account at Experian was recently hacked, and the only way I could recover access was by recreating the account. Entering my SSN and birthday at Experian showed my identity was tied to an email address I did not authorize. I recently ordered a copy of my [...]
