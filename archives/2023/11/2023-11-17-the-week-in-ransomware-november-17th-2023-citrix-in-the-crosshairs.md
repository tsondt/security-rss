Title: The Week in Ransomware - November 17th 2023 - Citrix in the Crosshairs
Date: 2023-11-17T18:26:29-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-11-17-the-week-in-ransomware-november-17th-2023-citrix-in-the-crosshairs

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-november-17th-2023-citrix-in-the-crosshairs/){:target="_blank" rel="noopener"}

> Ransomware gangs target exposed Citrix Netscaler devices using a publicly available exploit to breach large organizations, steal data, and encrypt files. [...]
