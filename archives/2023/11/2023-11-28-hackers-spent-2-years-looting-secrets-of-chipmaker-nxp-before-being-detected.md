Title: Hackers spent 2+ years looting secrets of chipmaker NXP before being detected
Date: 2023-11-28T12:56:49+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;chimera;hacking;network breaches;NXP
Slug: 2023-11-28-hackers-spent-2-years-looting-secrets-of-chipmaker-nxp-before-being-detected

[Source](https://arstechnica.com/?p=1986661){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) A prolific espionage hacking group with ties to China spent over two years looting the corporate network of NXP, the Netherlands-based chipmaker whose silicon powers security-sensitive components found in smartphones, smartcards, and electric vehicles, a news outlet has reported. The intrusion, by a group tracked under names including "Chimera" and "G0114," lasted from late 2017 to the beginning of 2020, according to Netherlands national news outlet NRC Handelsblad, which cited “several sources” familiar with the incident. During that time, the threat actors periodically accessed employee mailboxes and network drives in search of chip designs and [...]
