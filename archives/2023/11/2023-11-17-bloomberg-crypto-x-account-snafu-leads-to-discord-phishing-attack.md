Title: Bloomberg Crypto X account snafu leads to Discord phishing attack
Date: 2023-11-17T18:01:50-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-17-bloomberg-crypto-x-account-snafu-leads-to-discord-phishing-attack

[Source](https://www.bleepingcomputer.com/news/security/bloomberg-crypto-x-account-snafu-leads-to-discord-phishing-attack/){:target="_blank" rel="noopener"}

> The official Twitter account for Bloomberg Crypto was used earlier today to redirect users to a deceptive website that stole Discord credentials in a phishing attack. [...]
