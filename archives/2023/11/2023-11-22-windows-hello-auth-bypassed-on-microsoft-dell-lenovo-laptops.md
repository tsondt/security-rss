Title: Windows Hello auth bypassed on Microsoft, Dell, Lenovo laptops
Date: 2023-11-22T14:08:26-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2023-11-22-windows-hello-auth-bypassed-on-microsoft-dell-lenovo-laptops

[Source](https://www.bleepingcomputer.com/news/security/windows-hello-auth-bypassed-on-microsoft-dell-lenovo-laptops/){:target="_blank" rel="noopener"}

> Security researchers bypassed Windows Hello fingerprint authentication on Dell Inspiron, Lenovo ThinkPad, and Microsoft Surface Pro X laptops in attacks exploiting security flaws found in the embedded fingerprint sensors. [...]
