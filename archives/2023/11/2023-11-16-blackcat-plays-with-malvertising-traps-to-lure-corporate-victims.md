Title: BlackCat plays with malvertising traps to lure corporate victims
Date: 2023-11-16T14:45:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-16-blackcat-plays-with-malvertising-traps-to-lure-corporate-victims

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/16/blackcat_ransomware_luring_corporate_targets/){:target="_blank" rel="noopener"}

> Ads for Slack and Cisco AnyConnect actually downloaded Nitrogen malware Affiliates of the ALPHV/BlackCat ransomware-as-a-service operation are turning to malvertising campaigns to establish an initial foothold in their victims' systems.... [...]
