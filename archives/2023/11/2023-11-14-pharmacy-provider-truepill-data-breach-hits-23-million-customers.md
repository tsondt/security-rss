Title: Pharmacy provider Truepill data breach hits 2.3 million customers
Date: 2023-11-14T12:36:32-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare;Legal
Slug: 2023-11-14-pharmacy-provider-truepill-data-breach-hits-23-million-customers

[Source](https://www.bleepingcomputer.com/news/security/pharmacy-provider-truepill-data-breach-hits-23-million-customers/){:target="_blank" rel="noopener"}

> Postmeds, doing business as 'Truepill,' is sending notifications of a data breach informing recipients that threat actors accessed their sensitive personal information. [...]
