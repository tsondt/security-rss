Title: Upcoming improvements to your AWS sign-in experience
Date: 2023-11-24T21:29:08+00:00
Author: Khaled Zaky
Category: AWS Security
Tags: Announcements;AWS Identity and Access Management (IAM);Intermediate (200);Security, Identity, & Compliance;IAM user sign-in;root user sign-in;Security Blog;Switch Role
Slug: 2023-11-24-upcoming-improvements-to-your-aws-sign-in-experience

[Source](https://aws.amazon.com/blogs/security/upcoming-improvements-to-your-aws-sign-in-experience/){:target="_blank" rel="noopener"}

> Starting in mid-2024, Amazon Web Services (AWS) will introduce a series of UI improvements to the AWS sign-in pages. Our primary focus is to revamp the UI, especially the root and AWS Identity and Access Management (IAM) user sign-in page and switch role page. With these design updates, we aim to facilitate smoother transitions and [...]
