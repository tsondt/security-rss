Title: Russian Reshipping Service ‘SWAT USA Drop’ Exposed
Date: 2023-11-02T19:55:34+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Web Fraud 2.0;drops;drops for stuff;Fearlless;reshipping mules;stuffers;SWAT USA Drop service
Slug: 2023-11-02-russian-reshipping-service-swat-usa-drop-exposed

[Source](https://krebsonsecurity.com/2023/11/russian-reshipping-service-swat-usa-drop-exposed/){:target="_blank" rel="noopener"}

> The login page for the criminal reshipping service SWAT USA Drop. One of the largest cybercrime services for laundering stolen merchandise was hacked recently, exposing its internal operations, finances and organizational structure. Here’s a closer look at the Russia-based SWAT USA Drop Service, which currently employs more than 1,200 people across the United States who are knowingly or unwittingly involved in reshipping expensive consumer goods purchased with stolen credit cards. Among the most common ways that thieves extract cash from stolen credit card accounts is through purchasing pricey consumer goods online and reselling them on the black market. Most online [...]
