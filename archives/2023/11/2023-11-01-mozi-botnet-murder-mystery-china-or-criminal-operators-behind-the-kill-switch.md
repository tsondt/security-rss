Title: Mozi botnet murder mystery: China or criminal operators behind the kill switch?
Date: 2023-11-01T20:00:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-01-mozi-botnet-murder-mystery-china-or-criminal-operators-behind-the-kill-switch

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/01/mozi_botnet_kill_switch/){:target="_blank" rel="noopener"}

> Middle Kingdom or self-immolation - there are a couple of theories The Mozi botnet has all but disappeared according to security folks who first noticed the prolific network's slowdown and then uncovered a kill switch for the IoT system. But they still have one unanswered question: "Who killed Mozi?"... [...]
