Title: DP World confirms data stolen in cyberattack, no ransomware used
Date: 2023-11-28T12:09:29-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-28-dp-world-confirms-data-stolen-in-cyberattack-no-ransomware-used

[Source](https://www.bleepingcomputer.com/news/security/dp-world-confirms-data-stolen-in-cyberattack-no-ransomware-used/){:target="_blank" rel="noopener"}

> International logistics giant DP World has confirmed that data was stolen during a cyber attack that disrupted its operations in Australia earlier this month. However, no ransomware payloads or encryption was used in the attack. [...]
