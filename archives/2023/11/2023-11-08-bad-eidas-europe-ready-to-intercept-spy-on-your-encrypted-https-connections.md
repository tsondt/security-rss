Title: Bad eIDAS: Europe ready to intercept, spy on your encrypted HTTPS connections
Date: 2023-11-08T08:27:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-11-08-bad-eidas-europe-ready-to-intercept-spy-on-your-encrypted-https-connections

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/08/europe_eidas_browser/){:target="_blank" rel="noopener"}

> EFF warns incoming rules may return web 'to the dark ages of 2011' Lawmakers in Europe are expected to adopt digital identity rules that civil society groups say will make the internet less secure and open up citizens to online surveillance.... [...]
