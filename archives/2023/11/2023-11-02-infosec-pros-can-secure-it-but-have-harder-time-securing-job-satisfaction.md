Title: Infosec pros can secure IT, but have harder time securing job satisfaction
Date: 2023-11-02T18:00:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-02-infosec-pros-can-secure-it-but-have-harder-time-securing-job-satisfaction

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/02/infosec_pros_burnout/){:target="_blank" rel="noopener"}

> Industry facing burnout scare as workplace issues snowball The proportion of cybersecurity professionals reporting low "happiness ratings" has risen sharply over the last 12 months, raising concerns about increasing burnout rates in the industry.... [...]
