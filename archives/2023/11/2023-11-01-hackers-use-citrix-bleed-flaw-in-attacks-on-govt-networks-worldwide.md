Title: Hackers use Citrix Bleed flaw in attacks on govt networks worldwide
Date: 2023-11-01T14:46:17-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-01-hackers-use-citrix-bleed-flaw-in-attacks-on-govt-networks-worldwide

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-citrix-bleed-flaw-in-attacks-on-govt-networks-worldwide/){:target="_blank" rel="noopener"}

> Threat actors are leveraging the 'Citrix Bleed' vulnerability, tracked as CVE-2023-4966, to target government, technical, and legal organizations in the Americas, Europe, Africa, and the Asia-Pacific region. [...]
