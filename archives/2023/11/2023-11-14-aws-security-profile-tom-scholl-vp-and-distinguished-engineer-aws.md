Title: AWS Security Profile: Tom Scholl, VP and Distinguished Engineer, AWS
Date: 2023-11-14T21:28:47+00:00
Author: Tom Scholl
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Thought Leadership;AWS Security Profile;DDoS;Networking;Security;Security Blog
Slug: 2023-11-14-aws-security-profile-tom-scholl-vp-and-distinguished-engineer-aws

[Source](https://aws.amazon.com/blogs/security/aws-security-profile-tom-scholl-vp-and-distinguished-engineer-aws/){:target="_blank" rel="noopener"}

> In the AWS Security Profile series, we feature the people who work in Amazon Web Services (AWS) Security and help keep our customers safe and secure. This interview is with Tom Scholl, VP and Distinguished Engineer for AWS. What do you do in your current role and how long have you been at AWS? I’m [...]
