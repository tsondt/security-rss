Title: The Privacy Disaster of Modern Smart Cars
Date: 2023-11-10T12:07:55+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;EULA;privacy
Slug: 2023-11-10-the-privacy-disaster-of-modern-smart-cars

[Source](https://www.schneier.com/blog/archives/2023/11/the-privacy-disaster-of-modern-smart-cars.html){:target="_blank" rel="noopener"}

> Article based on a Mozilla report. [...]
