Title: Locking down Industrial Control Systems
Date: 2023-11-30T08:47:12+00:00
Author: Jack Kirkstall
Category: The Register
Tags: 
Slug: 2023-11-30-locking-down-industrial-control-systems

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/30/locking_down_industrial_control_systems/){:target="_blank" rel="noopener"}

> SANS unveils online hub with valuable tools and information for cybersecurity professionals defending ICS Sponsored Post Industrial Control Systems (ICS) which can automate processes, increase productivity and reduce labour costs, are rapidly gaining worldwide enterprise traction.... [...]
