Title: Socks5Systemz proxy service infects 10,000 systems worldwide
Date: 2023-11-05T10:17:34-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-05-socks5systemz-proxy-service-infects-10000-systems-worldwide

[Source](https://www.bleepingcomputer.com/news/security/socks5systemz-proxy-service-infects-10-000-systems-worldwide/){:target="_blank" rel="noopener"}

> A proxy botnet called 'Socks5Systemz' has been infecting computers worldwide via the 'PrivateLoader' and 'Amadey' malware loaders, currently counting 10,000 infected devices. [...]
