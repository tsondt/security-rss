Title: Ransomware-hit British Library: Too open for business, or not open enough?
Date: 2023-11-27T09:30:07+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2023-11-27-ransomware-hit-british-library-too-open-for-business-or-not-open-enough

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/27/british_library_opinion_column/){:target="_blank" rel="noopener"}

> Unique institutions need unique security. Instead, they're fobbed off with the same old, same old Opinion The British Library’s showpiece site, in a listed red brick building in St Pancras, is presided over by a large bronze sculpture depicting Isaac Newton poring over a document he’s working with, measuring it with dividers.... [...]
