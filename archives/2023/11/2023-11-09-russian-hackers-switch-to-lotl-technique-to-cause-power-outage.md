Title: Russian hackers switch to LOTL technique to cause power outage
Date: 2023-11-09T06:12:26-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2023-11-09-russian-hackers-switch-to-lotl-technique-to-cause-power-outage

[Source](https://www.bleepingcomputer.com/news/security/russian-hackers-switch-to-lotl-technique-to-cause-power-outage/){:target="_blank" rel="noopener"}

> Russian state hackers have evolved their methods for breaching industrial control systems by adopting living-off-the-land techniques that enable reaching the final stage of the attack quicker and with less resources [...]
