Title: Downfall fallout: Intel knew AVX chips were insecure and did nothing, lawsuit claims
Date: 2023-11-09T22:20:44+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2023-11-09-downfall-fallout-intel-knew-avx-chips-were-insecure-and-did-nothing-lawsuit-claims

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/09/intel_downfall_lawsuit/){:target="_blank" rel="noopener"}

> Billions of data-leaking processors sold despite warnings and patch just made them slower, punters complain Intel has been sued by a handful of PC buyers who claim the x86 goliath failed to act when informed five years ago about faulty chip instructions that allowed the recent Downfall vulnerability, and during that period sold billions of insecure chips.... [...]
