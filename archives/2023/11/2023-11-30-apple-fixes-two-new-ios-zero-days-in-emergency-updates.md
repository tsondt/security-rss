Title: Apple fixes two new iOS zero-days in emergency updates
Date: 2023-11-30T14:42:34-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple;Security
Slug: 2023-11-30-apple-fixes-two-new-ios-zero-days-in-emergency-updates

[Source](https://www.bleepingcomputer.com/news/apple/apple-fixes-two-new-ios-zero-days-in-emergency-updates/){:target="_blank" rel="noopener"}

> Apple released emergency security updates to fix two zero-day vulnerabilities exploited in attacks and impacting iPhone, iPad, and Mac devices, reaching 20 zero-days patched since the start of the year. [...]
