Title: Okta: Breach Affected All Customer Support Users
Date: 2023-11-29T19:41:14+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;Latest Warnings;The Coming Storm;Okta breach
Slug: 2023-11-29-okta-breach-affected-all-customer-support-users

[Source](https://krebsonsecurity.com/2023/11/okta-breach-affected-all-customer-support-users/){:target="_blank" rel="noopener"}

> When KrebsOnSecurity broke the news on Oct. 20, 2023 that identity and authentication giant Okta had suffered a breach in its customer support department, Okta said the intrusion allowed hackers to steal sensitive data from fewer than one percent of its 18,000+ customers. But today, Okta revised that impact statement, saying the attackers also stole the name and email address for nearly all of its customer support users. Okta acknowledged last month that for several weeks beginning in late September 2023, intruders had access to its customer support case management system. That access allowed the hackers to steal authentication tokens [...]
