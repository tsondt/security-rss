Title: DP World cyberattack blocks thousands of containers in ports
Date: 2023-11-13T14:06:03-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-13-dp-world-cyberattack-blocks-thousands-of-containers-in-ports

[Source](https://www.bleepingcomputer.com/news/security/dp-world-cyberattack-blocks-thousands-of-containers-in-ports/){:target="_blank" rel="noopener"}

> A cyberattack on international logistics firm DP World Australia has severely disrupted the regular freight movement in multiple large Australian ports. [...]
