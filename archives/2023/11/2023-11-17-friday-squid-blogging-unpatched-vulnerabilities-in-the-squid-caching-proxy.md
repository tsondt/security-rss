Title: Friday Squid Blogging: Unpatched Vulnerabilities in the Squid Caching Proxy
Date: 2023-11-17T22:01:40+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;computer security;patching;proxies;squid;vulnerabilities;web
Slug: 2023-11-17-friday-squid-blogging-unpatched-vulnerabilities-in-the-squid-caching-proxy

[Source](https://www.schneier.com/blog/archives/2023/11/friday-squid-blogging-unpatched-vulnerabilities-in-the-squid-caching-proxy.html){:target="_blank" rel="noopener"}

> In a rare squid/security post, here’s an article about unpatched vulnerabilities in the Squid caching proxy. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
