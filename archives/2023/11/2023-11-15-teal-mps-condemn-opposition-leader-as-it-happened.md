Title: Teal MPs condemn opposition leader – as it happened
Date: 2023-11-15T07:42:46+00:00
Author: Cait Kelly and Amy Remeikis (earlier)
Category: The Guardian
Tags: Australia news;Indigenous voice to parliament;Australian politics;Indigenous Australians;Australian security and counter-terrorism;Aukus;Scott Morrison;Anthony Albanese;Labor party;Apec summit;Australia cost of living crisis;Australian economy;Data and computer security;PwC
Slug: 2023-11-15-teal-mps-condemn-opposition-leader-as-it-happened

[Source](https://www.theguardian.com/australia-news/live/2023/nov/15/australia-news-politics-live-question-time-anthony-albanese-peter-dutton-cost-of-living-gaza-protest-scott-morrison-aukus-data-breaches){:target="_blank" rel="noopener"}

> This blog is now closed. Peter Dutton accused of ‘weaponising antisemitism’ during fiery debate in parliament Get our morning and afternoon news emails, free app or daily news podcast And here is the standard of some of the “debate” among senior parliamentarians. Meanwhile, Australia’s sense of social cohesion is at its lowest recorded ebb. Continue reading... [...]
