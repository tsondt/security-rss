Title: Use scalable controls for AWS services accessing your resources
Date: 2023-11-16T20:02:17+00:00
Author: James Greenwood
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;AWS Organizations;IAM Condition Keys;IAM policies;Identity and Access Management;New Launch;Security;Security Blog
Slug: 2023-11-16-use-scalable-controls-for-aws-services-accessing-your-resources

[Source](https://aws.amazon.com/blogs/security/use-scalable-controls-for-aws-services-accessing-your-resources/){:target="_blank" rel="noopener"}

> Sometimes you want to configure an AWS service to access your resource in another service. For example, you can configure AWS CloudTrail, a service that monitors account activity across your AWS infrastructure, to write log data to your bucket in Amazon Simple Storage Service (Amazon S3). When you do this, you want assurance that the service [...]
