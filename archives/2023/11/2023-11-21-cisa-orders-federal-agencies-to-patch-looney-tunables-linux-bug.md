Title: CISA orders federal agencies to patch Looney Tunables Linux bug
Date: 2023-11-21T12:56:50-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Linux
Slug: 2023-11-21-cisa-orders-federal-agencies-to-patch-looney-tunables-linux-bug

[Source](https://www.bleepingcomputer.com/news/security/cisa-orders-federal-agencies-to-patch-looney-tunables-linux-bug/){:target="_blank" rel="noopener"}

> Today, CISA ordered U.S. federal agencies to secure their systems against an actively exploited vulnerability that lets attackers gain root privileges on many major Linux distributions. [...]
