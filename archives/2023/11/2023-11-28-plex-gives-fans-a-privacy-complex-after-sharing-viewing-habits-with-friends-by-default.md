Title: Plex gives fans a privacy complex after sharing viewing habits with friends by default
Date: 2023-11-28T20:30:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-28-plex-gives-fans-a-privacy-complex-after-sharing-viewing-habits-with-friends-by-default

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/28/plex_privacy/){:target="_blank" rel="noopener"}

> Grandma is watching what?! A Plex "feature" has infuriated some users after sharing with others what they are watching on the streaming service — and it appears this functionality is on by default.... [...]
