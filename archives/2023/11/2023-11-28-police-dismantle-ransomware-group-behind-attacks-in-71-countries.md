Title: Police dismantle ransomware group behind attacks in 71 countries
Date: 2023-11-28T04:04:29-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-28-police-dismantle-ransomware-group-behind-attacks-in-71-countries

[Source](https://www.bleepingcomputer.com/news/security/police-dismantle-ransomware-group-behind-attacks-in-71-countries/){:target="_blank" rel="noopener"}

> In cooperation with Europol and Eurojust, law enforcement agencies from seven nations have arrested in Ukraine the core members of a ransomware group linked to attacks against organizations in 71 countries. [...]
