Title: AWS KMS is now FIPS 140-2 Security Level 3. What does this mean for you?
Date: 2023-11-07T18:31:15+00:00
Author: Rushir Patel
Category: AWS Security
Tags: Announcements;AWS Key Management Service;Compliance;Intermediate (200);Security, Identity, & Compliance;AWS KMS;FIPS 140-2;Security;Security Blog
Slug: 2023-11-07-aws-kms-is-now-fips-140-2-security-level-3-what-does-this-mean-for-you

[Source](https://aws.amazon.com/blogs/security/aws-kms-now-fips-140-2-level-3-what-does-this-mean-for-you/){:target="_blank" rel="noopener"}

> AWS Key Management Service (AWS KMS) recently announced that its hardware security modules (HSMs) were given Federal Information Processing Standards (FIPS) 140-2 Security Level 3 certification from the U.S. National Institute of Standards and Technology (NIST). For organizations that rely on AWS cryptographic services, this higher security level validation has several benefits, including simpler set up and operation. In [...]
