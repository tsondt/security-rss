Title: Zyxel warns of multiple critical vulnerabilities in NAS devices
Date: 2023-11-30T10:11:45-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-30-zyxel-warns-of-multiple-critical-vulnerabilities-in-nas-devices

[Source](https://www.bleepingcomputer.com/news/security/zyxel-warns-of-multiple-critical-vulnerabilities-in-nas-devices/){:target="_blank" rel="noopener"}

> Zyxel has addressed multiple security issues, including three critical ones that could allow an unauthenticated attacker to execute operating system commands on vulnerable network-attached storage (NAS) devices. [...]
