Title: Spaf on the Morris Worm
Date: 2023-11-07T12:08:37+00:00
Author: B. Schneier
Category: Bruce Schneier
Tags: Uncategorized;computer security;history of computing;history of security;malware
Slug: 2023-11-07-spaf-on-the-morris-worm

[Source](https://www.schneier.com/blog/archives/2023/11/spaf-on-the-morris-worm.html){:target="_blank" rel="noopener"}

> Gene Spafford wrote an essay reflecting on the Morris Worm of 1988—35 years ago. His lessons from then are still applicable today. [...]
