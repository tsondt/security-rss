Title: Europol shutters ransomware operation with kingpin arrests
Date: 2023-11-28T13:45:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-28-europol-shutters-ransomware-operation-with-kingpin-arrests

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/28/europol_shutters_ransomware_operation/){:target="_blank" rel="noopener"}

> A few low-level stragglers remain on the loose, but biggest fish have been hooked International law enforcement investigators have made a number of high-profile arrests after tracking a major cybercrime group for more than four years.... [...]
