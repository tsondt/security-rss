Title: Inside Denmark’s hell week as critical infrastructure orgs faced cyberattacks
Date: 2023-11-13T14:33:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-13-inside-denmarks-hell-week-as-critical-infrastructure-orgs-faced-cyberattacks

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/13/inside_denmarks_hell_week_as/){:target="_blank" rel="noopener"}

> Zyxel zero days and nation-state actors (maybe) had a hand in the sector’s worst cybersecurity event on record Danish critical infrastructure faced the biggest online attack in the country's history in May, according to SektorCERT, Denmark's specialist organization for the cybersecurity of critical kit.... [...]
