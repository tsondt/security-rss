Title: NCSC says cyber-readiness of UK’s critical infrastructure isn’t up to scratch
Date: 2023-11-14T07:02:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-14-ncsc-says-cyber-readiness-of-uks-critical-infrastructure-isnt-up-to-scratch

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/14/ncsc_cyber_readiness/){:target="_blank" rel="noopener"}

> And the world's getting more and more dangerous The UK's National Cyber Security Centre (NCSC) has once again sounded its concern over the rising threat level to the nation's critical national infrastructure (CNI).... [...]
