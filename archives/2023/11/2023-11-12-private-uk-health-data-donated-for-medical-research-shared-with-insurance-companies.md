Title: Private UK health data donated for medical research shared with insurance companies
Date: 2023-11-12T07:00:14+00:00
Author: Shanti Das
Category: The Guardian
Tags: Data protection;Medical research;Health;Technology;Privacy;Society;Insurance industry;Business;Big data;Science;Biotechnology industry;Data and computer security
Slug: 2023-11-12-private-uk-health-data-donated-for-medical-research-shared-with-insurance-companies

[Source](https://www.theguardian.com/technology/2023/nov/12/private-uk-health-data-donated-medical-research-shared-insurance-companies){:target="_blank" rel="noopener"}

> Observer investigation reveals UK Biobank opened its biomedical database to insurance firms despite pledge it would not do so Sensitive health information donated for medical research by half a million UK citizens has been shared with insurance companies despite a pledge that it would not be. An Observer investigation has found that UK Biobank opened up its vast biomedical database to insurance sector firms several times between 2020 and 2023. The data was provided to insurance consultancy and tech firms for projects to create digital tools that help insurers predict a person’s risk of getting a chronic disease. The findings [...]
