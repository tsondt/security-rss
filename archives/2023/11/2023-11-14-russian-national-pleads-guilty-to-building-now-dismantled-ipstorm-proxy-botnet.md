Title: Russian national pleads guilty to building now-dismantled IPStorm proxy botnet
Date: 2023-11-14T23:23:51+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-11-14-russian-national-pleads-guilty-to-building-now-dismantled-ipstorm-proxy-botnet

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/14/russian_ipstorm_botnet/){:target="_blank" rel="noopener"}

> 23K nodes earned operator more than $500K – and now perhaps jail time The FBI says it has dismantled another botnet after collaring its operator, who admitted hijacking tens of thousands of machines around the world to create his network of obedient nodes.... [...]
