Title: 'Serial cybercriminal and scammer' jailed for 8 years, told to pay back $1.2M
Date: 2023-11-28T01:06:19+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-28-serial-cybercriminal-and-scammer-jailed-for-8-years-told-to-pay-back-12m

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/28/serial_cybercriminal_and_scammer_sentenced/){:target="_blank" rel="noopener"}

> Crook did everything from SIM swaps to fake verified badge scams A Los Angeles man has been jailed after pulling off SIM-swap attacks on victims, hijacking social media accounts, committing fraud with Zelle payments, and impersonating Apple support.... [...]
