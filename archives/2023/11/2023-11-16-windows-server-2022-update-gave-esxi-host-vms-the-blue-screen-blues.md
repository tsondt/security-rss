Title: Windows Server 2022 update gave ESXi host VMs the blue screen blues
Date: 2023-11-16T15:45:07+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-11-16-windows-server-2022-update-gave-esxi-host-vms-the-blue-screen-blues

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/16/microsoft_windows_server_patch/){:target="_blank" rel="noopener"}

> Wild idea: Maybe Microsoft could introduce a Quality Copilot to stop pushing broken patches Something likely to be absent from Microsoft's Ignite event is talk of a fix rolled out to deal with malfunctioning Windows Server 2022 Virtual Machines following a problematic update from the company.... [...]
