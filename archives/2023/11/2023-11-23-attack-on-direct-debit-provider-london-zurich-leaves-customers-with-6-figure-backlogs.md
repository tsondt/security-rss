Title: Attack on direct debit provider London &amp; Zurich leaves customers with 6-figure backlogs
Date: 2023-11-23T11:47:51+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-23-attack-on-direct-debit-provider-london-zurich-leaves-customers-with-6-figure-backlogs

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/23/ransomware_attack_at_london_zurich/){:target="_blank" rel="noopener"}

> Customers complain of poor comms during huge outage that’s sparked payroll fears A ransomware attack and resulting outages at direct debit collection company London & Zurich has forced at least one customer to take out a short-term loan as six-figure backlogs continue to cause cash flow mayhem.... [...]
