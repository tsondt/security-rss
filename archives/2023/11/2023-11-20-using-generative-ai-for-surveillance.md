Title: Using Generative AI for Surveillance
Date: 2023-11-20T11:57:37+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;ChatGPT;privacy;surveillance
Slug: 2023-11-20-using-generative-ai-for-surveillance

[Source](https://www.schneier.com/blog/archives/2023/11/using-generative-ai-for-surveillance.html){:target="_blank" rel="noopener"}

> Generative AI is going to be a powerful tool for data analysis and summarization. Here’s an example of it being used for sentiment analysis. My guess is that it isn’t very good yet, but that it will get better. [...]
