Title: Friday Squid Blogging: Eating Dancing Squid
Date: 2023-11-03T21:05:10+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-11-03-friday-squid-blogging-eating-dancing-squid

[Source](https://www.schneier.com/blog/archives/2023/11/friday-squid-blogging-eating-dancing-squid.html){:target="_blank" rel="noopener"}

> It’s not actually alive, but it twitches in response to soy sauce. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
