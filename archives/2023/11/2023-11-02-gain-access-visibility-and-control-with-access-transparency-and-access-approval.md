Title: Gain access visibility and control with Access Transparency and Access Approval
Date: 2023-11-02T16:00:00+00:00
Author: Anil Nandigam
Category: GCP Security
Tags: Security & Identity
Slug: 2023-11-02-gain-access-visibility-and-control-with-access-transparency-and-access-approval

[Source](https://cloud.google.com/blog/products/identity-security/gain-access-visibility-and-control-with-access-transparency-and-access-approval/){:target="_blank" rel="noopener"}

> Organizations within regulated industries must balance cloud benefits with potential security and regulatory concerns. These include strict requirements to audit and control the cloud provider, especially when it comes to accessing the organization’s data and workloads. At Google Cloud, we're focused on providing our customers many ways to help achieve their security, compliance, and regulatory outcomes. One such capability is Access Transparency and Access Approval, which provides our customers with direct oversight of Google Cloud access to their resources when customer assistance or disaster recovery operations are underway. Full visibility and control with Access Transparency and Access Approval Access Transparency [...]
