Title: Google Workspace weaknesses allow plaintext password theft
Date: 2023-11-15T18:30:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-15-google-workspace-weaknesses-allow-plaintext-password-theft

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/15/google_workspace_weaknesses_allow_plaintext/){:target="_blank" rel="noopener"}

> Exploits come with caveats, but Google says no fixes as user security should do the heavy lifting here Novel weaknesses in Google Workspace have been exposed by researchers, with exploits potentially leading to ransomware attacks, data exfiltration, and password decryption.... [...]
