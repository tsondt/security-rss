Title: Russian state-owned Sberbank hit by 1 million RPS DDoS attack
Date: 2023-11-08T13:14:53-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-08-russian-state-owned-sberbank-hit-by-1-million-rps-ddos-attack

[Source](https://www.bleepingcomputer.com/news/security/russian-state-owned-sberbank-hit-by-1-million-rps-ddos-attack/){:target="_blank" rel="noopener"}

> Russian financial organization Sberbank states in a press release that two weeks ago it faced the most powerful distributed denial of service (DDoS) attack in recent history. [...]
