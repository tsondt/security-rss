Title: Introducing new central configuration capabilities in AWS Security Hub
Date: 2023-11-27T20:09:17+00:00
Author: Nicholas Jaeger
Category: AWS Security
Tags: Announcements;AWS Organizations;AWS Security Hub;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2023-11-27-introducing-new-central-configuration-capabilities-in-aws-security-hub

[Source](https://aws.amazon.com/blogs/security/introducing-new-central-configuration-capabilities-in-aws-security-hub/){:target="_blank" rel="noopener"}

> As cloud environments—and security risks associated with them—become more complex, it becomes increasingly critical to understand your cloud security posture so that you can quickly and efficiently mitigate security gaps. AWS Security Hub offers close to 300 automated controls that continuously check whether the configuration of your cloud resources aligns with the best practices identified [...]
