Title: Uncle Sam probes cyberattack on Pennsylvania water system by suspected Iranian crew
Date: 2023-11-29T21:16:56+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-29-uncle-sam-probes-cyberattack-on-pennsylvania-water-system-by-suspected-iranian-crew

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/29/water_authority_ciso_iran/){:target="_blank" rel="noopener"}

> CISA calls for stronger IT defenses as Texas district also hit by ransomware crew CISA is investigating a cyberattack against a Pennsylvania water authority by suspected Iranian miscreants. The intrusion forced operators to switch a pumping station to manual control.... [...]
