Title: OpenAI confirms DDoS attacks behind ongoing ChatGPT outages
Date: 2023-11-09T03:18:49-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-09-openai-confirms-ddos-attacks-behind-ongoing-chatgpt-outages

[Source](https://www.bleepingcomputer.com/news/security/openai-confirms-ddos-attacks-behind-ongoing-chatgpt-outages/){:target="_blank" rel="noopener"}

> During the last 24 hours, OpenAI has been addressing what it describes as "periodic outages" linked to DDoS attacks affecting its API and ChatGPT services. [...]
