Title: Implement an early feedback loop with AWS developer tools to shift security left
Date: 2023-11-17T14:07:44+00:00
Author: Barry Conway
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Amazon CodeGuru;Devops;Security;Security Blog
Slug: 2023-11-17-implement-an-early-feedback-loop-with-aws-developer-tools-to-shift-security-left

[Source](https://aws.amazon.com/blogs/security/implement-an-early-feedback-loop-with-aws-developer-tools-to-shift-security-left/){:target="_blank" rel="noopener"}

> Early-feedback loops exist to provide developers with ongoing feedback through automated checks. This enables developers to take early remedial action while increasing the efficiency of the code review process and, in turn, their productivity. Early-feedback loops help provide confidence to reviewers that fundamental security and compliance requirements were validated before review. As part of this [...]
