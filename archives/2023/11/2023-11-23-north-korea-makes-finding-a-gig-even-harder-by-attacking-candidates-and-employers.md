Title: North Korea makes finding a gig even harder by attacking candidates and employers
Date: 2023-11-23T01:33:00+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-11-23-north-korea-makes-finding-a-gig-even-harder-by-attacking-candidates-and-employers

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/23/north_korea_attacks_job_market/){:target="_blank" rel="noopener"}

> That GitHub repo an interviewer wants you to work on could be malware Palo Alto Networks' Unit 42 has detailed a pair of job market hacking schemes linked to state-sponsored actors in North Korea: one in which the threat actors pose as job seekers, the other as would-be employers.... [...]
