Title: Citrix warns admins to kill NetScaler user sessions to block hackers
Date: 2023-11-21T11:36:02-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-21-citrix-warns-admins-to-kill-netscaler-user-sessions-to-block-hackers

[Source](https://www.bleepingcomputer.com/news/security/citrix-warns-admins-to-kill-netscaler-user-sessions-to-block-hackers/){:target="_blank" rel="noopener"}

> Citrix reminded admins today that they must take additional measures after patching their NetScaler appliances against the CVE-2023-4966 'Citrix Bleed' vulnerability to secure vulnerable devices against attacks. [...]
