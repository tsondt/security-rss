Title: Use private key JWT authentication between Amazon Cognito user pools and an OIDC IdP
Date: 2023-11-14T16:51:09+00:00
Author: Martin Pagel
Category: AWS Security
Tags: Amazon Cognito;Expert (400);Security, Identity, & Compliance;Technical How-to;Amazon Cognito User Pools;Security Blog
Slug: 2023-11-14-use-private-key-jwt-authentication-between-amazon-cognito-user-pools-and-an-oidc-idp

[Source](https://aws.amazon.com/blogs/security/use-private-key-jwt-authentication-between-amazon-cognito-user-pools-and-an-oidc-idp/){:target="_blank" rel="noopener"}

> With Amazon Cognito user pools, you can add user sign-up and sign-in features and control access to your web and mobile applications. You can enable your users who already have accounts with other identity providers (IdPs) to skip the sign-up step and sign in to your application by using an existing account through SAML 2.0 [...]
