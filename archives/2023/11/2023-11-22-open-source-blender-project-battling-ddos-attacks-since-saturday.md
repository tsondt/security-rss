Title: Open-source Blender project battling DDoS attacks since Saturday
Date: 2023-11-22T11:12:40-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-22-open-source-blender-project-battling-ddos-attacks-since-saturday

[Source](https://www.bleepingcomputer.com/news/security/open-source-blender-project-battling-ddos-attacks-since-saturday/){:target="_blank" rel="noopener"}

> Blender has confirmed that recent site outages have been caused by ongoing DDoS (distributed denial of service) attacks that started on Saturday. [...]
