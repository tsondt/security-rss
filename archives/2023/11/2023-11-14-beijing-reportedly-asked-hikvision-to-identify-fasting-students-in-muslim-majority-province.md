Title: Beijing reportedly asked Hikvision to identify fasting students in Muslim-majority province
Date: 2023-11-14T05:59:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-11-14-beijing-reportedly-asked-hikvision-to-identify-fasting-students-in-muslim-majority-province

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/14/hikvision_fasting_identification/){:target="_blank" rel="noopener"}

> University managment app also tracked library activity, holidays, and much more US-based research group IPVM has accused Chinese video surveillance equipment company Hikvision of engaging with a contract to develop technology that can identify Muslim students that are fasting during Ramadan, based on their dining records.... [...]
