Title: Ardent hospital ERs disrupted in 6 states after ransomware attack
Date: 2023-11-27T12:54:41-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-11-27-ardent-hospital-ers-disrupted-in-6-states-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/ardent-hospital-ers-disrupted-in-6-states-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Ardent Health Services, a healthcare provider operating 30 hospitals across five U.S. states, disclosed today that its systems were hit by a ransomware attack on Thursday. [...]
