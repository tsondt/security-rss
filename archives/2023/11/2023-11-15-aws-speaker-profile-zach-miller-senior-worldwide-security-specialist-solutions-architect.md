Title: AWS Speaker Profile: Zach Miller, Senior Worldwide Security Specialist Solutions Architect
Date: 2023-11-15T18:27:07+00:00
Author: Roger Park
Category: AWS Security
Tags: AWS re:Invent;Events;Foundational (100);Security, Identity, & Compliance;Thought Leadership;AWS Security Profile;AWS Security Profiles;cryptography;Live Events;re:Invent 2023;Security Blog
Slug: 2023-11-15-aws-speaker-profile-zach-miller-senior-worldwide-security-specialist-solutions-architect

[Source](https://aws.amazon.com/blogs/security/aws-speaker-profile-zach-miller-senior-worldwide-security-specialist-solutions-architect/){:target="_blank" rel="noopener"}

> In the AWS Speaker Profile series, we interview Amazon Web Services (AWS) thought leaders who help keep our customers safe and secure. This interview features Zach Miller, Senior Worldwide Security Specialist SA and re:Invent 2023 presenter of Securely modernize payment applications with AWS and Centrally manage application secrets with AWS Secrets Manager. Zach shares thoughts [...]
