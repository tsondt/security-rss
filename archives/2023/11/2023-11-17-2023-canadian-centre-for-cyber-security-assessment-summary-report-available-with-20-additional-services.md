Title: 2023 Canadian Centre for Cyber Security Assessment Summary report available with 20 additional services
Date: 2023-11-17T16:52:15+00:00
Author: Naranjan Goklani
Category: AWS Security
Tags: Announcements;AWS Artifact;Compliance;Federal;Foundational (100);Government;Public Sector;Security;Security, Identity, & Compliance;State or Local Government;AWS Canada (Central) Region;AWS Shared Responsibility Model;Canada;CCCS;CCCS Assessment;classification;Cloud security;Cyber Security;cybersecurity;Federal Government Canada;Government of Canada (GC);ITSG-33;Medium Cloud Security Profile;PBMM;Protected B;Security Blog
Slug: 2023-11-17-2023-canadian-centre-for-cyber-security-assessment-summary-report-available-with-20-additional-services

[Source](https://aws.amazon.com/blogs/security/2023-canadian-centre-for-cyber-security-assessment-summary-report-available-with-20-additional-services/){:target="_blank" rel="noopener"}

> At Amazon Web Services (AWS), we are committed to providing continued assurance to our customers through assessments, certifications, and attestations that support the adoption of current and new AWS services and features. We are pleased to announce the availability of the 2023 Canadian Centre for Cyber Security (CCCS) assessment summary report for AWS. With this assessment, a [...]
