Title: Bug hunters on your marks: TETRA radio encryption algorithms to enter public domain
Date: 2023-11-14T08:00:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-14-bug-hunters-on-your-marks-tetra-radio-encryption-algorithms-to-enter-public-domain

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/14/tetra_encryption_algorithms_open_sourced/){:target="_blank" rel="noopener"}

> Emergency comms standard had five nasty flaws but will be opened to academic research A set of encryption algorithms used to secure emergency radio communications will enter the public domain after an about-face by the European Telecommunications Standards Institute (ETSI).... [...]
