Title: FBI: Royal ransomware asked 350 victims to pay $275 million
Date: 2023-11-13T15:40:41-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-13-fbi-royal-ransomware-asked-350-victims-to-pay-275-million

[Source](https://www.bleepingcomputer.com/news/security/fbi-royal-ransomware-asked-350-victims-to-pay-275-million/){:target="_blank" rel="noopener"}

> The FBI and CISA revealed in a joint advisory that the Royal ransomware gang has breached the networks of at least 350 organizations worldwide since September 2022. [...]
