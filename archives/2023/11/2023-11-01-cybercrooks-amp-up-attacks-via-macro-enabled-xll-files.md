Title: Cybercrooks amp up attacks via macro-enabled XLL files
Date: 2023-11-01T14:45:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-01-cybercrooks-amp-up-attacks-via-macro-enabled-xll-files

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/01/xll_macro_attack_surge/){:target="_blank" rel="noopener"}

> Neither Excel nor PowerPoint safe as baddies continue to find ways around protections Cybercriminals are once again abusing macro-enabled Excel add-in (XLL) files in malware attacks at a vastly increased rate, according to new research.... [...]
