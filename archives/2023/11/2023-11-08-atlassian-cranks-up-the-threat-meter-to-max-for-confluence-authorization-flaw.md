Title: Atlassian cranks up the threat meter to max for Confluence authorization flaw
Date: 2023-11-08T14:00:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-08-atlassian-cranks-up-the-threat-meter-to-max-for-confluence-authorization-flaw

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/08/atlassian_confluence_flaw_upgraded/){:target="_blank" rel="noopener"}

> Attackers secure admin rights after vendor said they could only steal data Atlassian reassessed the severity rating of the recent improper authorization vulnerability in Confluence Data Center and Server, raising the CVSS score from 9.1 to a maximum of 10.... [...]
