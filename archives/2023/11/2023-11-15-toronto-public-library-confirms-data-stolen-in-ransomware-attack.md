Title: Toronto Public Library confirms data stolen in ransomware attack
Date: 2023-11-15T14:20:45-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-15-toronto-public-library-confirms-data-stolen-in-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/toronto-public-library-confirms-data-stolen-in-ransomware-attack/){:target="_blank" rel="noopener"}

> The Toronto Public Library (TPL) confirmed that the personal information of employees, customers, volunteers, and donors was stolen from a compromised file server during an October ransomware attack. [...]
