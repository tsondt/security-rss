Title: FTC’s Voice Cloning Challenge
Date: 2023-11-16T18:46:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;cloning;contests;voice recognition
Slug: 2023-11-16-ftcs-voice-cloning-challenge

[Source](https://www.schneier.com/blog/archives/2023/11/ftcs-voice-cloning-challenge.html){:target="_blank" rel="noopener"}

> The Federal Trade Commission is running a competition “to foster breakthrough ideas on preventing, monitoring, and evaluating malicious voice cloning.” [...]
