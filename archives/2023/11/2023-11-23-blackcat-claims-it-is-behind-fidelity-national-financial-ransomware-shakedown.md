Title: BlackCat claims it is behind Fidelity National Financial ransomware shakedown
Date: 2023-11-23T18:01:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-23-blackcat-claims-it-is-behind-fidelity-national-financial-ransomware-shakedown

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/23/blackcat_ransomware_fnf/){:target="_blank" rel="noopener"}

> One of US's largest underwriters forced to shut down a number of key systems Fortune 500 insurance biz Fidelity National Financial (FNF) has confirmed that it has fallen victim to a "cybersecurity incident."... [...]
