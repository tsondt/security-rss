Title: McLaren Health Care says data breach impacted 2.2 million people
Date: 2023-11-10T10:28:12-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-11-10-mclaren-health-care-says-data-breach-impacted-22-million-people

[Source](https://www.bleepingcomputer.com/news/security/mclaren-health-care-says-data-breach-impacted-22-million-people/){:target="_blank" rel="noopener"}

> McLaren Health Care (McLaren) is notifying nearly 2.2 million people of a data breach that occurred between late July and August this year, exposing sensitive personal information. [...]
