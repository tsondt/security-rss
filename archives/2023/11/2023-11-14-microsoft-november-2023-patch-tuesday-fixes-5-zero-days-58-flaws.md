Title: Microsoft November 2023 Patch Tuesday fixes 5 zero-days, 58 flaws
Date: 2023-11-14T14:00:21-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-11-14-microsoft-november-2023-patch-tuesday-fixes-5-zero-days-58-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-november-2023-patch-tuesday-fixes-5-zero-days-58-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's November 2023 Patch Tuesday, which includes security updates for a total of 58 flaws and five zero-day vulnerabilities. [...]
