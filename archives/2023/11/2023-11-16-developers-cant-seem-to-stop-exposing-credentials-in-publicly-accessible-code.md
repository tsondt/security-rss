Title: Developers can’t seem to stop exposing credentials in publicly accessible code
Date: 2023-11-16T01:19:12+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;code repositories;credentials;passwords;pypi
Slug: 2023-11-16-developers-cant-seem-to-stop-exposing-credentials-in-publicly-accessible-code

[Source](https://arstechnica.com/?p=1984368){:target="_blank" rel="noopener"}

> Enlarge (credit: Victor De Schwanberg/Science Photo Library via Getty Images) Despite more than a decade of reminding, prodding, and downright nagging, a surprising number of developers still can’t bring themselves to keep their code free of credentials that provide the keys to their kingdoms to anyone who takes the time to look for them. The lapse stems from immature coding practices in which developers embed cryptographic keys, security tokens, passwords, and other forms of credentials directly into the source code they write. The credentials make it easy for the underlying program to access databases or cloud services necessary for it [...]
