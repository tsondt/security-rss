Title: AWS achieves SNI 27001 certification for the AWS Asia Pacific (Jakarta) Region
Date: 2023-11-20T18:36:00+00:00
Author: Airish Mariano
Category: AWS Security
Tags: Announcements;Compliance;Foundational (100);Security, Identity, & Compliance;Certificate;Compliance reports;Security Blog;SNI
Slug: 2023-11-20-aws-achieves-sni-27001-certification-for-the-aws-asia-pacific-jakarta-region

[Source](https://aws.amazon.com/blogs/security/aws-achieves-sni-27001-certification-for-the-aws-asia-pacific-jakarta-region/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is proud to announce the successful completion of its first Standar Nasional Indonesia (SNI) certification for the AWS Asia Pacific (Jakarta) Region in Indonesia. SNI is the Indonesian National Standard, and it comprises a set of standards that are nationally applicable in Indonesia. AWS is now certified according to the SNI [...]
