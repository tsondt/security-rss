Title: Criminal IP & Cisco SecureX/XDR: Enhanced Cyber Threat Analysis
Date: 2023-11-13T10:02:01-05:00
Author: Sponsored by Criminal IP
Category: BleepingComputer
Tags: Security
Slug: 2023-11-13-criminal-ip-cisco-securexxdr-enhanced-cyber-threat-analysis

[Source](https://www.bleepingcomputer.com/news/security/criminal-ip-and-cisco-securex-xdr-enhanced-cyber-threat-analysis/){:target="_blank" rel="noopener"}

> The Criminal IP threat intelligence search engine by AI SPERA has recently integrated with Cisco SecureX/XDR, empowering organizations to stay ahead of malicious actors. Learn more about this integration from Criminal IP in this article. [...]
