Title: Ten Ways AI Will Change Democracy
Date: 2023-11-13T12:09:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;laws;LLM
Slug: 2023-11-13-ten-ways-ai-will-change-democracy

[Source](https://www.schneier.com/blog/archives/2023/11/ten-ways-ai-will-change-democracy.html){:target="_blank" rel="noopener"}

> Artificial intelligence will change so many aspects of society, largely in ways that we cannot conceive of yet. Democracy, and the systems of governance that surround it, will be no exception. In this short essay, I want to move beyond the “AI-generated disinformation” trope and speculate on some of the ways AI will change how democracy functions—in both large and small ways. When I survey how artificial intelligence might upend different aspects of modern society, democracy included, I look at four different dimensions of change: speed, scale, scope, and sophistication. Look for places where changes in degree result in changes [...]
