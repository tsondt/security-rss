Title: Ransomware attack on indie game maker wiped all player accounts
Date: 2023-11-27T15:06:14-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2023-11-27-ransomware-attack-on-indie-game-maker-wiped-all-player-accounts

[Source](https://www.bleepingcomputer.com/news/security/ransomware-attack-on-indie-game-maker-wiped-all-player-accounts/){:target="_blank" rel="noopener"}

> A ransomware attack on the "Ethyrial: Echoes of Yore" MMORPG last Friday destroyed 17,000 player accounts, deleting their in-game items and progress in the game. [...]
