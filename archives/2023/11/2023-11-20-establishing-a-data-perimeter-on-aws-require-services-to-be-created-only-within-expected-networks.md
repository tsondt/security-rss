Title: Establishing a data perimeter on AWS: Require services to be created only within expected networks
Date: 2023-11-20T15:27:07+00:00
Author: Harsha Sharma
Category: AWS Security
Tags: Advanced (300);Best Practices;Security, Identity, & Compliance;Technical How-to;Security Blog;Technical How-To
Slug: 2023-11-20-establishing-a-data-perimeter-on-aws-require-services-to-be-created-only-within-expected-networks

[Source](https://aws.amazon.com/blogs/security/establishing-a-data-perimeter-on-aws-require-services-to-be-created-only-within-expected-networks/){:target="_blank" rel="noopener"}

> Welcome to the fifth post in the Establishing a data perimeter on AWS series. Throughout this series, we’ve discussed how a set of preventative guardrails can create an always-on boundary to help ensure that your trusted identities are accessing your trusted resources over expected networks. In a previous post, we emphasized the importance of preventing [...]
