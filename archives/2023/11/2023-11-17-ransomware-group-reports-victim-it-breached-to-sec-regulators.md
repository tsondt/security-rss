Title: Ransomware group reports victim it breached to SEC regulators
Date: 2023-11-17T00:03:35+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;extortion;ransomware;Securities and Exchange Commission
Slug: 2023-11-17-ransomware-group-reports-victim-it-breached-to-sec-regulators

[Source](https://arstechnica.com/?p=1984663){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) One of the world’s most active ransomware groups has taken an unusual—if not unprecedented—tactic to pressure one of its victims to pay up: reporting the victim to the US Securities and Exchange Commission. The pressure tactic came to light in a post published on Wednesday on the dark web site run by AlphV, a ransomware crime syndicate that’s been in operation for two years. After first claiming to have breached the network of the publicly traded digital lending company MeridianLink, AlphV officials posted a screenshot of a complaint it said it filed with the SEC through [...]
