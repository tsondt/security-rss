Title: The Black Friday 2023 Security, IT, VPN, & Antivirus Deals
Date: 2023-11-22T11:15:00-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-11-22-the-black-friday-2023-security-it-vpn-antivirus-deals

[Source](https://www.bleepingcomputer.com/news/security/the-black-friday-2023-security-it-vpn-and-antivirus-deals/){:target="_blank" rel="noopener"}

> Black Friday 2023 is here, and great deals are live in computer security, software, online courses, system admin services, antivirus, and VPN software. [...]
