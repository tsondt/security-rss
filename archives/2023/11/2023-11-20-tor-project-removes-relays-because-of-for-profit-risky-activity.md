Title: Tor Project removes relays because of for-profit, risky activity
Date: 2023-11-20T20:06:15-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-11-20-tor-project-removes-relays-because-of-for-profit-risky-activity

[Source](https://www.bleepingcomputer.com/news/security/tor-project-removes-relays-because-of-for-profit-risky-activity/){:target="_blank" rel="noopener"}

> The Tor Project has explained its recent decision to remove multiple network relays that represented a threat to the safety and security of all Tor network users. [...]
