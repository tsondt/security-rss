Title: Welltok data breach exposes data of 8.5 million US patients
Date: 2023-11-22T13:22:11-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-11-22-welltok-data-breach-exposes-data-of-85-million-us-patients

[Source](https://www.bleepingcomputer.com/news/security/welltok-data-breach-exposes-data-of-85-million-us-patients/){:target="_blank" rel="noopener"}

> Healthcare SaaS provider Welltok is warning that a data breach exposed the personal data of nearly 8.5 million patients in the U.S. after a file transfer program used by the company was hacked in a data theft attack. [...]
