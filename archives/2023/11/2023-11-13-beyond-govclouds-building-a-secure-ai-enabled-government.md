Title: Beyond GovClouds: Building a Secure, AI-Enabled Government
Date: 2023-11-13T10:00:00+00:00
Author: Jeanette Manfra
Category: GCP Security
Tags: Security & Identity;Public Sector
Slug: 2023-11-13-beyond-govclouds-building-a-secure-ai-enabled-government

[Source](https://cloud.google.com/blog/topics/public-sector/beyond-govclouds-building-a-secure-ai-enabled-government/){:target="_blank" rel="noopener"}

> Google Cloud has a longstanding commitment to partnering with the U.S. government, helping agencies modernize technology systems, and shaping new frontiers in service delivery and mission success. Since establishing Google Public Sector last year, we've witnessed an ever-growing demand for cloud services as agencies leverage AI for critical work including improving cancer detection, transforming city services, and optimizing agency operations. Unlike some cloud vendors, Google has long believed that "GovClouds," isolated and often less-reliable versions of commercial clouds, can hinder the public sector's ability to innovate and scale. President Biden's Executive Order on AI further emphasizes the need for safe, [...]
