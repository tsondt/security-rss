Title: FCC adopts new rules to protect consumers from SIM-swapping attacks
Date: 2023-11-18T11:10:09-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-18-fcc-adopts-new-rules-to-protect-consumers-from-sim-swapping-attacks

[Source](https://www.bleepingcomputer.com/news/security/fcc-adopts-new-rules-to-protect-consumers-from-sim-swapping-attacks/){:target="_blank" rel="noopener"}

> The Federal Communications Commission (FCC) has revealed new rules to shield consumers from criminals who hijack their phone numbers in SIM swapping attacks and port-out fraud. [...]
