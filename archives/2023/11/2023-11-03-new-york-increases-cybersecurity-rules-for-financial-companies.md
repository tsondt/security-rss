Title: New York Increases Cybersecurity Rules for Financial Companies
Date: 2023-11-03T11:01:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;banking;computer security;cybersecurity;ransomware;regulation
Slug: 2023-11-03-new-york-increases-cybersecurity-rules-for-financial-companies

[Source](https://www.schneier.com/blog/archives/2023/11/new-york-increases-cybersecurity-rules-for-financial-companies.html){:target="_blank" rel="noopener"}

> Another example of a large and influential state doing things the federal government won’t: Boards of directors, or other senior committees, are charged with overseeing cybersecurity risk management, and must retain an appropriate level of expertise to understand cyber issues, the rules say. Directors must sign off on cybersecurity programs, and ensure that any security program has “sufficient resources” to function. In a new addition, companies now face significant requirements related to ransom payments. Regulated firms must now report any payment made to hackers within 24 hours of that payment. [...]
