Title: Digital Car Keys Are Coming
Date: 2023-11-28T20:19:58+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;keys;smartphones;transportation
Slug: 2023-11-28-digital-car-keys-are-coming

[Source](https://www.schneier.com/blog/archives/2023/11/digital-car-keys-are-coming.html){:target="_blank" rel="noopener"}

> Soon we will be able to unlock and start our cars from our phones. Let’s hope people are thinking about security. [...]
