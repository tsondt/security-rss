Title: US seizes Sinbad crypto mixer used by North Korean Lazarus hackers
Date: 2023-11-29T11:49:53-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-11-29-us-seizes-sinbad-crypto-mixer-used-by-north-korean-lazarus-hackers

[Source](https://www.bleepingcomputer.com/news/security/us-seizes-sinbad-crypto-mixer-used-by-north-korean-lazarus-hackers/){:target="_blank" rel="noopener"}

> The U.S. Department of the Treasury has sanctioned the Sinbad cryptocurrency mixing service for its use as a money-laundering tool by the North Korean Lazarus hacking group. [...]
