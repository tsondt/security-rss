Title: How to improve your security incident response processes with Jupyter notebooks
Date: 2023-11-06T18:26:04+00:00
Author: Tim Manik
Category: AWS Security
Tags: Amazon SageMaker;Expert (400);Security, Identity, & Compliance;Technical How-to;Incident response;SageMaker;Security Blog
Slug: 2023-11-06-how-to-improve-your-security-incident-response-processes-with-jupyter-notebooks

[Source](https://aws.amazon.com/blogs/security/how-to-improve-your-security-incident-response-processes-with-jupyter-notebooks/){:target="_blank" rel="noopener"}

> Customers face a number of challenges to quickly and effectively respond to a security event. To start, it can be difficult to standardize how to respond to a partic­ular security event, such as an Amazon GuardDuty finding. Additionally, silos can form with reliance on one security analyst who is designated to perform certain tasks, such [...]
