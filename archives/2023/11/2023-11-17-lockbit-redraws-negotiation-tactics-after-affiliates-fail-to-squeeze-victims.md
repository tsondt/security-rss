Title: LockBit redraws negotiation tactics after affiliates fail to squeeze victims
Date: 2023-11-17T18:04:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-17-lockbit-redraws-negotiation-tactics-after-affiliates-fail-to-squeeze-victims

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/17/lockbit_cracks_whip_on_affiliates/){:target="_blank" rel="noopener"}

> Cybercrime group worried over dwindling payments... didn't they tell them to Always Be Closing? In response to growing frustrations inside the LockBit organization, its leaders have overhauled the way they negotiate with ransomware victims going forward.... [...]
