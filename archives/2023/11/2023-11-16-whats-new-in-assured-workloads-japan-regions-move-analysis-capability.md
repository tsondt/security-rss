Title: What’s new in Assured Workloads: Japan regions, move analysis capability
Date: 2023-11-16T17:00:00+00:00
Author: Jatin Bhatia
Category: GCP Security
Tags: Security & Identity
Slug: 2023-11-16-whats-new-in-assured-workloads-japan-regions-move-analysis-capability

[Source](https://cloud.google.com/blog/products/identity-security/whats-new-in-assured-workloads-japan-region-migration-compliance-analysis/){:target="_blank" rel="noopener"}

> Assured Workloads is Google Cloud’s solution that allows our customers to run regulated workloads in many of our global regions without the need for sector-focused isolated regions or infrastructure. It can help our customers address their security and compliance requirements, without compromising on cloud innovations and security capabilities. We are excited to announce new capabilities in Assured Workloads to help organizations more easily achieve and maintain compliance with relevant global regulatory requirements: We have expanded Assured Workloads availability to our Japan regions. A new analysis tool — now available to all Google Cloud customers — that can identify potential policy [...]
