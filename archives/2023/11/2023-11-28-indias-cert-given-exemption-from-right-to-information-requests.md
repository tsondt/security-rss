Title: India's CERT given exemption from Right To Information requests
Date: 2023-11-28T06:31:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-11-28-indias-cert-given-exemption-from-right-to-information-requests

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/28/cert_in_rti_exemption/){:target="_blank" rel="noopener"}

> Activists worry investigations may stay secret, and then there's those odd incident reporting requirements India's government has granted its Computer Emergency Response Team, CERT-In, immunity from Right To Information (RTI) requests – the nation's equivalent of the freedom of information queries in the US, UK, or Australia.... [...]
