Title: The Rise of Ransomware in Healthcare: What IT Leaders Need to Know
Date: 2023-11-10T10:02:04-05:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2023-11-10-the-rise-of-ransomware-in-healthcare-what-it-leaders-need-to-know

[Source](https://www.bleepingcomputer.com/news/security/the-rise-of-ransomware-in-healthcare-what-it-leaders-need-to-know/){:target="_blank" rel="noopener"}

> Ransomware attacks are rapidly becoming the weapon of choice, making up over half of all attacks in the healthcare industry. Learn more from Specops Software on securing your organization from these attacks. [...]
