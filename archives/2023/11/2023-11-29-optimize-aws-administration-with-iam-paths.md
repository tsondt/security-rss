Title: Optimize AWS administration with IAM paths
Date: 2023-11-29T20:55:45+00:00
Author: David Rowe
Category: AWS Security
Tags: Advanced (300);Best Practices;Security, Identity, & Compliance;Technical How-to;AWS Identity and Access Management (IAM);Security;Security Blog
Slug: 2023-11-29-optimize-aws-administration-with-iam-paths

[Source](https://aws.amazon.com/blogs/security/optimize-aws-administration-with-iam-paths/){:target="_blank" rel="noopener"}

> As organizations expand their Amazon Web Services (AWS) environment and migrate workloads to the cloud, they find themselves dealing with many AWS Identity and Access Management (IAM) roles and policies. These roles and policies multiply because IAM fills a crucial role in securing and controlling access to AWS resources. Imagine you have a team creating [...]
