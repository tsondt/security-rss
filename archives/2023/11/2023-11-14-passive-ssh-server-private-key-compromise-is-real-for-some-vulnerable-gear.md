Title: Passive SSH server private key compromise is real ... for some vulnerable gear
Date: 2023-11-14T02:38:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-14-passive-ssh-server-private-key-compromise-is-real-for-some-vulnerable-gear

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/14/passive_ssh_key_compromise/){:target="_blank" rel="noopener"}

> OpenSSL, LibreSSL, OpenSSH users, don't worry – you can sit this one out An academic study has shown how it's possible for someone to snoop on certain devices' SSH connections and, with a bit of luck, impersonate that equipment after silently figuring out the hosts' private RSA keys.... [...]
