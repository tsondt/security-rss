Title: SonicWall swallows Solutions Granted amid cybersecurity demand surge
Date: 2023-11-17T15:01:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-17-sonicwall-swallows-solutions-granted-amid-cybersecurity-demand-surge

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/17/sonicwall_solutions_granted_acquisition/){:target="_blank" rel="noopener"}

> CEO Bob VanKirk makes near-20-year partnership official, teases big things coming to EMEA Channel-focused cybersecurity company SonicWall is buying Virginia-based MSSP Solutions Granted – its first acquisition in well over a decade.... [...]
