Title: 3,000 Apache ActiveMQ servers vulnerable to RCE attacks exposed online
Date: 2023-11-01T14:05:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-01-3000-apache-activemq-servers-vulnerable-to-rce-attacks-exposed-online

[Source](https://www.bleepingcomputer.com/news/security/3-000-apache-activemq-servers-vulnerable-to-rce-attacks-exposed-online/){:target="_blank" rel="noopener"}

> Over three thousand internet-exposed Apache ActiveMQ servers are vulnerable to a recently disclosed critical remote code execution (RCE) vulnerability. [...]
