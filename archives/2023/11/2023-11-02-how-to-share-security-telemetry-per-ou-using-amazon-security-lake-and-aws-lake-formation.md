Title: How to share security telemetry per OU using Amazon Security Lake and AWS Lake Formation
Date: 2023-11-02T13:25:16+00:00
Author: Chris Lamont-Smith
Category: AWS Security
Tags: Advanced (300);Amazon QuickSight;Amazon Security Lake;AWS Lake Formation;AWS Security Hub;Security, Identity, & Compliance;Security Blog
Slug: 2023-11-02-how-to-share-security-telemetry-per-ou-using-amazon-security-lake-and-aws-lake-formation

[Source](https://aws.amazon.com/blogs/security/how-to-share-security-telemetry-per-ou-using-amazon-security-lake-and-aws-lake-formation/){:target="_blank" rel="noopener"}

> Part 3 of a 3-part series Part 1 – Aggregating, searching, and visualizing log data from distributed sources with Amazon Athena and Amazon QuickSight Part 2 – How to visualize Amazon Security Lake findings with Amazon QuickSight This is the final part of a three-part series on visualizing security data using Amazon Security Lake and [...]
