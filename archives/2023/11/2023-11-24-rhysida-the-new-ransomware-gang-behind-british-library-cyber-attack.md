Title: Rhysida, the new ransomware gang behind British Library cyber-attack
Date: 2023-11-24T16:00:24+00:00
Author: Dan Milmo Global technology editor
Category: The Guardian
Tags: Cybercrime;British Library;Data and computer security;Hacking;Russia;Cryptocurrencies;Technology;Libraries;UK news
Slug: 2023-11-24-rhysida-the-new-ransomware-gang-behind-british-library-cyber-attack

[Source](https://www.theguardian.com/technology/2023/nov/24/rhysida-the-new-ransomware-gang-behind-british-library-cyber-attack){:target="_blank" rel="noopener"}

> Gang thought to be from Russia or CIS has attacked companies and institutions in several countries A new name was added to the cyber-rogues’ gallery of ransomware gangs this week after a criminal group called Rhysida claimed responsibility for an attack on the British Library. The library confirmed that personal data stolen in a cyber-attack last month has appeared for sale online. Continue reading... [...]
