Title: Critical Apache ActiveMQ flaw under attack by 'clumsy' ransomware crims
Date: 2023-11-02T17:15:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-02-critical-apache-activemq-flaw-under-attack-by-clumsy-ransomware-crims

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/02/apache_activemq_vulnerability/){:target="_blank" rel="noopener"}

> Over a week later and barely any patches for the 10/10 vulnerability have been applied Security researchers have confirmed that ransomware criminals are capitalizing on a maximum-severity vulnerability in Apache ActiveMQ.... [...]
