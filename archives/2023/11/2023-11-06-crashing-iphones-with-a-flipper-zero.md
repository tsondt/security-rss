Title: Crashing iPhones with a Flipper Zero
Date: 2023-11-06T14:45:30+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;Bluetooth;denial of service;hacking;iPhone
Slug: 2023-11-06-crashing-iphones-with-a-flipper-zero

[Source](https://www.schneier.com/blog/archives/2023/11/crashing-iphones-with-a-flipper-zero.html){:target="_blank" rel="noopener"}

> The Flipper Zero is an incredibly versatile hacking device. Now it can be used to crash iPhones in its vicinity by sending them a never-ending stream of pop-ups. These types of hacks have been possible for decades, but they require special equipment and a fair amount of expertise. The capabilities generally required expensive SDRs­—short for software-defined radios­—that, unlike traditional hardware-defined radios, use firmware and processors to digitally re-create radio signal transmissions and receptions. The $200 Flipper Zero isn’t an SDR in its own right, but as a software-controlled radio, it can do many of the same things at an affordable [...]
