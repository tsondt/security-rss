Title: Google ads push malicious CPU-Z app from fake Windows news site
Date: 2023-11-09T11:09:38-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2023-11-09-google-ads-push-malicious-cpu-z-app-from-fake-windows-news-site

[Source](https://www.bleepingcomputer.com/news/security/google-ads-push-malicious-cpu-z-app-from-fake-windows-news-site/){:target="_blank" rel="noopener"}

> A threat actor has been abusing Google Ads to distribute a trojanized version of the CPU-Z tool to deliver the Redline info-stealing malware. [...]
