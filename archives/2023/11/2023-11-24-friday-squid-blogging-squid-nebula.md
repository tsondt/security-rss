Title: Friday Squid Blogging: Squid Nebula
Date: 2023-11-24T22:04:40+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-11-24-friday-squid-blogging-squid-nebula

[Source](https://www.schneier.com/blog/archives/2023/11/friday-squid-blogging-squid-nebula.html){:target="_blank" rel="noopener"}

> Pretty photograph. The Squid Nebula is shown in blue, indicating doubly ionized oxygen—­which is when you ionize your oxygen once and then ionize it again just to make sure. (In all seriousness, it likely indicates a low-mass star nearing the end of its life). As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
