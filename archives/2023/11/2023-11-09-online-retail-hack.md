Title: Online Retail Hack
Date: 2023-11-09T12:09:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;A Hacker's Mind;fraud;hacking;noncomputer hacks;retail
Slug: 2023-11-09-online-retail-hack

[Source](https://www.schneier.com/blog/archives/2023/11/online-retail-hack.html){:target="_blank" rel="noopener"}

> Selling miniature replicas to unsuspecting shoppers: Online marketplaces sell tiny pink cowboy hats. They also sell miniature pencil sharpeners, palm-size kitchen utensils, scaled-down books and camping chairs so small they evoke the Stonehenge scene in “This Is Spinal Tap.” Many of the minuscule objects aren’t clearly advertised. [...] But there is no doubt some online sellers deliberately trick customers into buying smaller and often cheaper-to-produce items, Witcher said. Common tactics include displaying products against a white background rather than in room sets or on models, or photographing items with a perspective that makes them appear bigger than they really are. [...]
