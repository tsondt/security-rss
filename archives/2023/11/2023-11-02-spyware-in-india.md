Title: Spyware in India
Date: 2023-11-02T11:07:43+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;privacy;spyware;surveillance
Slug: 2023-11-02-spyware-in-india

[Source](https://www.schneier.com/blog/archives/2023/11/spyware-in-india.html){:target="_blank" rel="noopener"}

> Apple has warned leaders of the opposition government in India that their phones are being spied on: Multiple top leaders of India’s opposition parties and several journalists have received a notification from Apple, saying that “Apple believes you are being targeted by state-sponsored attackers who are trying to remotely compromise the iPhone associated with your Apple ID....” AccessNow puts this in context : For India to uphold fundamental rights, authorities must initiate an immediate independent inquiry, implement a ban on the use of rights-abusing commercial spyware, and make a commitment to reform the country’s surveillance laws. These latest warnings build [...]
