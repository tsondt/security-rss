Title: Okta data breach dilemma dwarfs earlier estimates
Date: 2023-11-29T17:01:05+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-29-okta-data-breach-dilemma-dwarfs-earlier-estimates

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/29/okta_misjudged_breach_scale/){:target="_blank" rel="noopener"}

> All customer support users told their info was accessed after analysis oversight Okta has admitted that the number of customers affected by its October customer support system data breach is far greater than previously thought.... [...]
