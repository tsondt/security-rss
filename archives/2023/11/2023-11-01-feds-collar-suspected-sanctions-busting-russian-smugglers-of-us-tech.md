Title: Feds collar suspected sanctions-busting Russian smugglers of US tech
Date: 2023-11-01T18:29:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-11-01-feds-collar-suspected-sanctions-busting-russian-smugglers-of-us-tech

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/01/russian_tech_smuggling_arrests/){:target="_blank" rel="noopener"}

> Parts sent to Moscow allegedly found on Ukrainian battlefields Three Russian nationals were arrested in New York yesterday on charges of moving electronics components worth millions to sanctioned entities in Russia, pieces of which were later recovered on battlefields in Ukraine.... [...]
