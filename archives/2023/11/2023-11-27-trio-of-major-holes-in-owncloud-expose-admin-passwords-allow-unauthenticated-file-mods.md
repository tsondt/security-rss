Title: Trio of major holes in ownCloud expose admin passwords, allow unauthenticated file mods
Date: 2023-11-27T18:28:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-27-trio-of-major-holes-in-owncloud-expose-admin-passwords-allow-unauthenticated-file-mods

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/27/three_major_vulnerabilities_in_owncloud/){:target="_blank" rel="noopener"}

> Mitigations require mix of updating libraries and manual customer action ownCloud has disclosed three critical vulnerabilities, the most serious of which leads to sensitive data exposure and carries a maximum severity score.... [...]
