Title: Securing frontline Operational Technology environments
Date: 2023-11-06T11:35:35+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2023-11-06-securing-frontline-operational-technology-environments

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/06/securing_frontline_operational_technology_environments/){:target="_blank" rel="noopener"}

> How Britvic outlawed security blind spots Webinar Organisations in multiple industries often face risks which can severely impact their operational resilience. Cyber criminals like to use ransomware and vulnerable third-party connections to hijack operational technology (OT) systems which can stop production in manufacturing environments, for example.... [...]
