Title: How to use chaos engineering in incident response
Date: 2023-11-03T13:30:33+00:00
Author: Kevin Low
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Security Blog;Technical How-To
Slug: 2023-11-03-how-to-use-chaos-engineering-in-incident-response

[Source](https://aws.amazon.com/blogs/security/how-to-use-chaos-engineering-in-incident-response/){:target="_blank" rel="noopener"}

> Simulations, tests, and game days are critical parts of preparing and verifying incident response processes. Customers often face challenges getting started and building their incident response function as the applications they build become increasingly complex. In this post, we will introduce the concept of chaos engineering and how you can use it to accelerate your [...]
