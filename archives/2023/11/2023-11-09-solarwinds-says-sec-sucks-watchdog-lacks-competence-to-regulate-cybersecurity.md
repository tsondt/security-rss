Title: SolarWinds says SEC sucks: Watchdog 'lacks competence' to regulate cybersecurity
Date: 2023-11-09T17:03:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-09-solarwinds-says-sec-sucks-watchdog-lacks-competence-to-regulate-cybersecurity

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/09/solarwinds_sec_filing/){:target="_blank" rel="noopener"}

> IT software slinger publishes fierce response to lawsuit brought last month SolarWinds has come out guns blazing to defend itself following the US Securities and Exchange Commission's announcement that it will be suing both the IT software maker and its CISO over the 2020 SUNBURST cyberattack.... [...]
