Title: Aggregating, searching, and visualizing log data from distributed sources with Amazon Athena and Amazon QuickSight
Date: 2023-11-02T13:07:30+00:00
Author: Pratima Singh
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Amazon Athena;Amazon QuickSight;AWS security;Cloud security;Security;Security Blog
Slug: 2023-11-02-aggregating-searching-and-visualizing-log-data-from-distributed-sources-with-amazon-athena-and-amazon-quicksight

[Source](https://aws.amazon.com/blogs/security/aggregating-searching-and-visualizing-log-data-from-distributed-sources-with-amazon-athena-and-amazon-quicksight/){:target="_blank" rel="noopener"}

> Part 1 of a 3-part series Part 2 – How to visualize Amazon Security Lake findings with Amazon QuickSight Part 3 – How to share security telemetry per Organizational Unit using Amazon Security Lake and AWS Lake Formation Customers using Amazon Web Services (AWS) can use a range of native and third-party tools to build [...]
