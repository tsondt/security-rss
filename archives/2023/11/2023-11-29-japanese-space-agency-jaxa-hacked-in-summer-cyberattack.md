Title: Japanese Space Agency JAXA hacked in summer cyberattack
Date: 2023-11-29T12:04:55-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-29-japanese-space-agency-jaxa-hacked-in-summer-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/japanese-space-agency-jaxa-hacked-in-summer-cyberattack/){:target="_blank" rel="noopener"}

> The Japan Aerospace Exploration Agency (JAXA) was hacked in a cyberattack over the summer, potentially compromising sensitive space-related technology and data. [...]
