Title: New Google survey: U.S. government workers want more choice in tech, worry about cyber attacks
Date: 2023-11-13T09:00:00+00:00
Author: Amit Zavery
Category: GCP Security
Tags: Security & Identity
Slug: 2023-11-13-new-google-survey-us-government-workers-want-more-choice-in-tech-worry-about-cyber-attacks

[Source](https://cloud.google.com/blog/products/identity-security/new-google-survey-us-government-workers-want-more-choice-in-tech-worry-about-cyber-attacks/){:target="_blank" rel="noopener"}

> Over the past four years, I’ve traveled the world meeting with current and potential Google Cloud customers and listening to their thoughts about the cloud and its future. It’s my team’s job to turn those thoughts into reality. What I have consistently heard is that while the cloud was once heralded as a flexible, elastic, pay-as-you-go technology, today it is at risk of being co-opted by legacy vendors who want to bring back 1990s-style restrictive software licensing, such as erecting barriers to interoperability and limiting customer choice with tying. These licensing tactics can reduce customer choice and create a “monoculture,” [...]
