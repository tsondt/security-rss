Title: Clorox CISO flushes self after multi-million-dollar cyberattack
Date: 2023-11-16T00:43:50+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-16-clorox-ciso-flushes-self-after-multi-million-dollar-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/16/clorox_ciso_washes_out/){:target="_blank" rel="noopener"}

> Plus: Ransomware crooks file SEC complaint against victim The Clorox Company's chief security officer has left her job in the wake of a corporate network breach that cost the manufacturer hundreds of millions of dollars.... [...]
