Title: Dutch hacker jailed for extortion, selling stolen data on RaidForums
Date: 2023-11-03T16:09:26-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-03-dutch-hacker-jailed-for-extortion-selling-stolen-data-on-raidforums

[Source](https://www.bleepingcomputer.com/news/security/dutch-hacker-jailed-for-extortion-selling-stolen-data-on-raidforums/){:target="_blank" rel="noopener"}

> A former Dutch cybersecurity professional was sentenced to four years in prison after being found guilty of hacking and blackmailing more than a dozen companies in the Netherlands and worldwide. [...]
