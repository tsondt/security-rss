Title: Okta: October data breach affects all customer support system users
Date: 2023-11-29T08:25:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-29-okta-october-data-breach-affects-all-customer-support-system-users

[Source](https://www.bleepingcomputer.com/news/security/okta-october-data-breach-affects-all-customer-support-system-users/){:target="_blank" rel="noopener"}

> Okta's investigation into the breach of its Help Center environment last month revealed that the hackers obtained data belonging to all customer support system users. [...]
