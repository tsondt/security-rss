Title: Researchers extract RSA keys from SSH server signing errors
Date: 2023-11-19T10:01:10-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-19-researchers-extract-rsa-keys-from-ssh-server-signing-errors

[Source](https://www.bleepingcomputer.com/news/security/researchers-extract-rsa-keys-from-ssh-server-signing-errors/){:target="_blank" rel="noopener"}

> A team of academic researchers from universities in California and Massachusetts demonstrated that it's possible under certain conditions for passive network attackers to retrieve secret RSA keys from naturally occurring errors leading to failed SSH (secure shell) connection attempts. [...]
