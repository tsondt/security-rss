Title: No, Okta, senior management, not an errant employee, caused you to get hacked
Date: 2023-11-04T00:31:12+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;okta;security breach;zero trust
Slug: 2023-11-04-no-okta-senior-management-not-an-errant-employee-caused-you-to-get-hacked

[Source](https://arstechnica.com/?p=1981227){:target="_blank" rel="noopener"}

> Enlarge (credit: Omar Marques/SOPA Images/LightRocket via Getty Images) Identity and authentication management provider Okta on Friday published an autopsy report on a recent breach that gave hackers administrative access to the Okta accounts of some of its customers. While the postmortem emphasizes the transgressions of an employee logging into a personal Google account on a work device, the biggest contributing factor was something the company understated: a badly configured service account. In a post, Okta chief security officer David Bradbury said that the most likely way the threat actor behind the attack gained access to parts of his company’s customer [...]
