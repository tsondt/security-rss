Title: Your password hygiene remains atrocious, says NordPass
Date: 2023-11-20T02:33:12+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-11-20-your-password-hygiene-remains-atrocious-says-nordpass

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/20/your_password_hygiene_is_still/){:target="_blank" rel="noopener"}

> ALSO: FCC cracks down on SIM-swap scams, old ZeroLogon targeted by new ransomware, and critical vulnerabilities Infosec in brief It's that time of year again – NordPass has released its annual list of the most common passwords. And while it seems some of you took last year's chiding to heart, most of you arguably swapped bad for worse.... [...]
