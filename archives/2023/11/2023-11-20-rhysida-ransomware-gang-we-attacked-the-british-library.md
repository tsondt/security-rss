Title: Rhysida ransomware gang: We attacked the British Library
Date: 2023-11-20T12:05:17+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-20-rhysida-ransomware-gang-we-attacked-the-british-library

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/20/rhysida_claims_british_library_ransomware/){:target="_blank" rel="noopener"}

> Crims post passport scans and internal forms up for 'auction' to prove it The Rhysida ransomware group says it's behind the highly disruptive October cyberattack on the British Library, leaking a snippet of stolen data in the process.... [...]
