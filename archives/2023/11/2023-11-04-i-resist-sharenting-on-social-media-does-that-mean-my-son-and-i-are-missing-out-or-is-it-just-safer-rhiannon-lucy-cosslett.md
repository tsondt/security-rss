Title: I resist sharenting on social media. Does that mean my son and I are missing out, or is it just safer? | Rhiannon Lucy Cosslett
Date: 2023-11-04T06:00:32+00:00
Author: Rhiannon Lucy Cosslett
Category: The Guardian
Tags: Social media;Parents and parenting;Facebook;Children;Life and style;Technology;Family;Social networking;Digital media;Internet safety;Data and computer security
Slug: 2023-11-04-i-resist-sharenting-on-social-media-does-that-mean-my-son-and-i-are-missing-out-or-is-it-just-safer-rhiannon-lucy-cosslett

[Source](https://www.theguardian.com/commentisfree/2023/nov/04/resist-sharenting-social-media-missing-out-or-safer){:target="_blank" rel="noopener"}

> Posting can turn into a privacy risk – and in a changing online landscape, it’s become another parental identity marker An old friend asked me recently why I never put my son’s face online. “Can you explain the not showing pics of babies thing to me?” she asked. “Everyone our age seems to obscure their baby’s face with emojis. I feel as if I’ve missed a key essay on the morality of baby pic social media publication.” I don’t do the emoji thing – in fact I’ve even stopped showing the back of his head, or any aspect of his [...]
