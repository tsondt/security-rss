Title: New BLUFFS attack lets attackers hijack Bluetooth connections
Date: 2023-11-28T16:58:46-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-28-new-bluffs-attack-lets-attackers-hijack-bluetooth-connections

[Source](https://www.bleepingcomputer.com/news/security/new-bluffs-attack-lets-attackers-hijack-bluetooth-connections/){:target="_blank" rel="noopener"}

> Researchers at Eurecom have developed six new attacks collectively named 'BLUFFS' that can break the secrecy of Bluetooth sessions, allowing for device impersonation and man-in-the-middle (MitM) attacks. [...]
