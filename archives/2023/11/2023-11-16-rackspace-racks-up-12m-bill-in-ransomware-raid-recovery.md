Title: Rackspace racks up $12M bill in ransomware raid recovery
Date: 2023-11-16T21:23:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-16-rackspace-racks-up-12m-bill-in-ransomware-raid-recovery

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/16/rackspace_ransomware_costs/){:target="_blank" rel="noopener"}

> And that's not counting the incoming lawsuits Rackspace's costs from last year's ransomware infection continue to mount: the cloud hosting biz told America's financial watchdog, the SEC, its total expenses to date regarding that cyberattack have reached $12 million – so far.... [...]
