Title: Toronto Public Library outages caused by Black Basta ransomware attack
Date: 2023-11-01T16:25:55-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-11-01-toronto-public-library-outages-caused-by-black-basta-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/toronto-public-library-outages-caused-by-black-basta-ransomware-attack/){:target="_blank" rel="noopener"}

> The Toronto Public Library is experiencing ongoing technical outages due to a Black Basta ransomware attack. [...]
