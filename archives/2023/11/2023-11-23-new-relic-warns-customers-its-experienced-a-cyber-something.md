Title: New Relic warns customers it's experienced a cyber … something
Date: 2023-11-23T04:58:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-11-23-new-relic-warns-customers-its-experienced-a-cyber-something

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/23/new_relic_cyber_incident_warning/){:target="_blank" rel="noopener"}

> Users told to hold tight and await instructions as investigation continues Web tracking and analytics outfit New Relic has issued a scanty security advisory warning customers it has experienced a scary cyber something.... [...]
