Title: Australia politics live: Dutton says Albanese should skip Apec trip and legislate to return released detainees to detention
Date: 2023-11-15T01:00:01+00:00
Author: Amy Remeikis
Category: The Guardian
Tags: Australia news;Indigenous voice to parliament;Australian politics;Indigenous Australians;Australian security and counter-terrorism;Aukus;Scott Morrison;Anthony Albanese;Labor party;Apec summit;Australia cost of living crisis;Australian economy;Data and computer security;PwC
Slug: 2023-11-15-australia-politics-live-dutton-says-albanese-should-skip-apec-trip-and-legislate-to-return-released-detainees-to-detention

[Source](https://www.theguardian.com/australia-news/live/2023/nov/15/australia-news-politics-live-question-time-anthony-albanese-peter-dutton-cost-of-living-gaza-protest-scott-morrison-aukus-data-breaches){:target="_blank" rel="noopener"}

> Home affairs minister says government ‘managing the mandatory decision of the high court in the interests of the Australian people within the law’. Follow the day’s news live Get our morning and afternoon news emails, free app or daily news podcast And here is the standard of some of the “debate” among senior parliamentarians. Meanwhile, Australia’s sense of social cohesion is at its lowest recorded ebb. Continue reading... [...]
