Title: Impatient LockBit says it's leaked 50GB of stolen Boeing files after ransom fails to land
Date: 2023-11-10T20:21:02+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-10-impatient-lockbit-says-its-leaked-50gb-of-stolen-boeing-files-after-ransom-fails-to-land

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/10/lockbit_leaks_boeing_files/){:target="_blank" rel="noopener"}

> Aerospace titan pores over data to see if dump is legit The LockBit crew is claiming to have leaked all of the data it stole from Boeing late last month, after the passenger jet giant apparently refused to pay the ransom demand.... [...]
