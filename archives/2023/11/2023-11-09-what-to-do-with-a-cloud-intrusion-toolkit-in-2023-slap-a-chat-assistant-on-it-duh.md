Title: What to do with a cloud intrusion toolkit in 2023? Slap a chat assistant on it, duh
Date: 2023-11-09T06:56:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-09-what-to-do-with-a-cloud-intrusion-toolkit-in-2023-slap-a-chat-assistant-on-it-duh

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/09/predatorai_infostealer_chatgpt/){:target="_blank" rel="noopener"}

> Don't worry, this half-baked Python script is for educational purposes onl-hahaha Infosec bods have detailed an underground cybersecurity tool dubbed Predator AI that not only can be used to compromise poorly secured cloud services and web apps, but has an optional chat-bot assistant that only kinda works.... [...]
