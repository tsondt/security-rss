Title: British Library: Ongoing outage caused by ransomware attack
Date: 2023-11-17T08:37:26-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-17-british-library-ongoing-outage-caused-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/british-library-ongoing-outage-caused-by-ransomware-attack/){:target="_blank" rel="noopener"}

> The British Library confirmed that a ransomware attack is behind a major outage that is still affecting services across several locations. [...]
