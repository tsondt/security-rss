Title: USB worm unleashed by Russian state hackers spreads worldwide
Date: 2023-11-22T00:02:29+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;gamaredon;kremlin;russia;USB;worm
Slug: 2023-11-22-usb-worm-unleashed-by-russian-state-hackers-spreads-worldwide

[Source](https://arstechnica.com/?p=1985993){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) A group of Russian-state hackers known for almost exclusively targeting Ukrainian entities has branched out in recent months, either accidentally or purposely, by allowing USB-based espionage malware to infect a variety of organizations in other countries. The group—known by many names, including Gamaredon, Primitive Bear, ACTINIUM, Armageddon, and Shuckworm—has been active since at least 2014 and has been attributed to Russia’s Federal Security Service by the Security Service of Ukraine. Most Kremlin-backed groups take pains to fly under the radar; Gamaredon doesn't care to. Its espionage-motivated campaigns targeting large numbers of Ukrainian organizations are easy to [...]
