Title: UK's cookie crumble: Data watchdog serves up tougher recipe for consent banners
Date: 2023-11-22T10:15:07+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2023-11-22-uks-cookie-crumble-data-watchdog-serves-up-tougher-recipe-for-consent-banners

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/22/uk_ico_cookie_action/){:target="_blank" rel="noopener"}

> 30 days to get compliant with tracking rules or face enforcement action The UK's Information Commissioner's Office (ICO) is getting tough on website design, insisting that opting out of cookies must be as simple as opting in.... [...]
