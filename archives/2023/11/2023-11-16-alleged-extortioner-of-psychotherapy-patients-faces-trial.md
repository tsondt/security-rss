Title: Alleged Extortioner of Psychotherapy Patients Faces Trial
Date: 2023-11-16T19:59:14+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Antti Kurittu;Julius Kivimäki;mikko hypponen;Petteri Järvinen;Vastaamo Psychotherapy Center;WithSecure
Slug: 2023-11-16-alleged-extortioner-of-psychotherapy-patients-faces-trial

[Source](https://krebsonsecurity.com/2023/11/alleged-extortioner-of-psychotherapy-patients-faces-trial/){:target="_blank" rel="noopener"}

> Prosecutors in Finland this week commenced their criminal trial against Julius Kivimäki, a 26-year-old Finnish man charged with extorting a once popular and now-bankrupt online psychotherapy practice and thousands of its patients. In a 2,200-page report, Finnish authorities laid out how they connected the extortion spree to Kivimäki, a notorious hacker who was convicted in 2015 of perpetrating tens of thousands of cybercrimes, including data breaches, payment fraud, operating a botnet and calling in bomb threats. In November 2022, Kivimäki was charged with attempting to extort money from the Vastaamo Psychotherapy Center. In that breach, which occurred in October 2020, [...]
