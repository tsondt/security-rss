Title: Singapore and Google partner on Web Risk to protect citizens from online scams and phishing
Date: 2023-11-08T17:00:00+00:00
Author: Badr Salmi
Category: GCP Security
Tags: Public Sector;Security & Identity
Slug: 2023-11-08-singapore-and-google-partner-on-web-risk-to-protect-citizens-from-online-scams-and-phishing

[Source](https://cloud.google.com/blog/products/identity-security/singapore-government--google-partner-to-protect-citizens-from-scams/){:target="_blank" rel="noopener"}

> With a growing number of internet users and ever-more services moving online, the security, scam, and fraud landscape is rapidly changing. Governments and the private sector are investing to provide protection for their employees and the end-users of their services. The Government Technology Agency of Singapore and Google Cloud are announcing a partnership to use Google Cloud Web Risk to better protect Singapore citizens and residents from online scams and phishing. Under the partnership, Google Cloud Web Risk will provide Singapore with threat intelligence about malicious websites and phishing pages through its various APIs. This information will then be used [...]
