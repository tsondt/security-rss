Title: Google Cloud sponsors CyberGreen Institute to advance research in Cyber Public Health
Date: 2023-11-06T17:00:00+00:00
Author: Bill Reid
Category: GCP Security
Tags: Healthcare & Life Sciences;Security & Identity
Slug: 2023-11-06-google-cloud-sponsors-cybergreen-institute-to-advance-research-in-cyber-public-health

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-sponsors-cybergreen-institute-to-advance-research-in-cyber-public-health/){:target="_blank" rel="noopener"}

> Software designed to cause harm has been described as a “virus” since the 1970s, and some have described hackers as the “ internet’s immune system.” While analogies framing cybersecurity in healthcare terminology have limits, we have seen over the decades an important and growing connection between cybersecurity and healthcare. Cyber public health embraces lessons from the development of public health and applies them to cybersecurity, which could be transformed by widespread use of the techniques and practices of public data-driven investigation, population thinking, and preventative measures. To support these efforts, Google Cloud is becoming an official sponsor of the CyberGreen [...]
