Title: Hacktivists breach U.S. nuclear research lab, steal employee data
Date: 2023-11-21T16:20:27-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-21-hacktivists-breach-us-nuclear-research-lab-steal-employee-data

[Source](https://www.bleepingcomputer.com/news/security/hacktivists-breach-us-nuclear-research-lab-steal-employee-data/){:target="_blank" rel="noopener"}

> The Idaho National Laboratory (INL) confirms they suffered a cyberattack after 'SiegedSec' hacktivists leaked stolen human resources data online. [...]
