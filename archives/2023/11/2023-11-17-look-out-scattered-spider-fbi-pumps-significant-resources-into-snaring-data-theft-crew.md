Title: Look out, Scattered Spider. FBI pumps 'significant' resources into snaring data-theft crew
Date: 2023-11-17T00:11:16+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-17-look-out-scattered-spider-fbi-pumps-significant-resources-into-snaring-data-theft-crew

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/17/fbi_scattered_spider_action/){:target="_blank" rel="noopener"}

> Absence of arrests doesn't mean nothing's happening, cyber-cops insist The FBI is applying "significant" resources to find members of the infamous Scattered Spider cyber-crime crew, which seemingly attacked a couple of high-profile casinos a few months ago and remains active, according to a senior bureau official.... [...]
