Title: Ethereum feature abused to steal $60 million from 99K victims
Date: 2023-11-13T16:41:10-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-11-13-ethereum-feature-abused-to-steal-60-million-from-99k-victims

[Source](https://www.bleepingcomputer.com/news/security/ethereum-feature-abused-to-steal-60-million-from-99k-victims/){:target="_blank" rel="noopener"}

> Malicious actors have been abusing Ethereum's 'Create2' function to bypass wallet security alerts and poison cryptocurrency addresses, which led to stealing $60,000,000 worth of cryptocurrency from 99,000 people in six months. [...]
