Title: 'Corrupt' cop jailed for tipping off pal to EncroChat dragnet
Date: 2023-11-04T07:37:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-04-corrupt-cop-jailed-for-tipping-off-pal-to-encrochat-dragnet

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/04/corrupt_cop_encrochat/){:target="_blank" rel="noopener"}

> Taking selfie with 'official sensitive' doc wasn't smartest idea, either A British court has sentenced a "corrupt" police analyst to almost four years behind bars for tipping off a friend that officers had compromised the EncroChat encrypted messaging app network.... [...]
