Title: Email Security Flaw Found in the Wild
Date: 2023-11-21T12:05:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cross-site scripting;e-mail;vulnerabilities;zero-day
Slug: 2023-11-21-email-security-flaw-found-in-the-wild

[Source](https://www.schneier.com/blog/archives/2023/11/email-security-flaw-found-in-the-wild.html){:target="_blank" rel="noopener"}

> Google’s Threat Analysis Group announced a zero-day against the Zimbra Collaboration email server that has been used against governments around the world. TAG has observed four different groups exploiting the same bug to steal email data, user credentials, and authentication tokens. Most of this activity occurred after the initial fix became public on Github. To ensure protection against these types of exploits, TAG urges users and organizations to keep software fully up-to-date and apply security updates as soon as they become available. The vulnerability was discovered in June. It has been patched. [...]
