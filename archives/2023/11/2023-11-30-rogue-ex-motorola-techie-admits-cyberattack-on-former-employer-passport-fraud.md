Title: Rogue ex-Motorola techie admits cyberattack on former employer, passport fraud
Date: 2023-11-30T01:15:00+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-30-rogue-ex-motorola-techie-admits-cyberattack-on-former-employer-passport-fraud

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/30/rogue_exmotorola_tech_pleads_guilty/){:target="_blank" rel="noopener"}

> Pro tip: Don't use your new work email to phish your old firm An ex-Motorola technician in the US has admitted he tried to fraudulently obtain a passport while awaiting trial for a cyberattack on his former employer.... [...]
