Title: Microsoft Patch Tuesday, November 2023 Edition
Date: 2023-11-14T23:00:59+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Security Tools;Time to Patch;CVE-2023-36025;CVE-2023-36033;CVE-2023-36036;CVE-2023-36038;CVE-2023-36050;CVE-2023-36413;CVE-2023-36439;Microsoft Patch Tuesday November 2023;sans internet storm center
Slug: 2023-11-14-microsoft-patch-tuesday-november-2023-edition

[Source](https://krebsonsecurity.com/2023/11/microsoft-patch-tuesday-november-2023-edition/){:target="_blank" rel="noopener"}

> Microsoft today released updates to fix more than five dozen security holes in its Windows operating systems and related software, including three “zero day” vulnerabilities that Microsoft warns are already being exploited in active attacks. The zero-day threats targeting Microsoft this month include CVE-2023-36025, a weakness that allows malicious content to bypass the Windows SmartScreen Security feature. SmartScreen is a built-in Windows component that tries to detect and block malicious websites and files. Microsoft’s security advisory for this flaw says attackers could exploit it by getting a Windows user to click on a booby-trapped link to a shortcut file. Kevin [...]
