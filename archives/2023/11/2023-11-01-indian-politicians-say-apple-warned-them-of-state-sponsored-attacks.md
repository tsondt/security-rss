Title: Indian politicians say Apple warned them of state-sponsored attacks
Date: 2023-11-01T05:02:25+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-11-01-indian-politicians-say-apple-warned-them-of-state-sponsored-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/01/india_apple_state_attack_warnings/){:target="_blank" rel="noopener"}

> Nobody knows which state, but government never quite shrugged off claims it uses spyware Indian politicians and media figures have reported that Apple has warned them their accounts may be under attack by state-sponsored actors.... [...]
