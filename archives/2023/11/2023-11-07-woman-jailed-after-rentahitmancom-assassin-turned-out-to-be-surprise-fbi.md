Title: Woman jailed after Rentahitman.com assassin turned out to be – surprise – FBI
Date: 2023-11-07T00:29:21+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-07-woman-jailed-after-rentahitmancom-assassin-turned-out-to-be-surprise-fbi

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/07/hitman_for_hire_jail/){:target="_blank" rel="noopener"}

> 18 months in the slammer no laughing matter, but the rest... maybe A 34-year-old woman has been jailed for 18 months after trying to use Rentahitman.com – no, really – to pay a contract killer to eliminate a rival she was beefing with. Her would-be assassin-for-hire unsurprisingly turned out to be an FBI agent.... [...]
