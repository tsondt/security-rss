Title: UK and South Korea: Hackers use zero-day in supply-chain attack
Date: 2023-11-24T12:28:14-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-24-uk-and-south-korea-hackers-use-zero-day-in-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/uk-and-south-korea-hackers-use-zero-day-in-supply-chain-attack/){:target="_blank" rel="noopener"}

> A joint advisory by the National Cyber Security Centre (NCSC) and Korea's National Intelligence Service (NIS) discloses a supply-chain attack executed by North Korean hackers involving the MagicLineThe National Cyber Security Centre (NCSC) and Korea's National Intelligence Service (NIS) warn that the North Korean Lazarus hacking grou [...]
