Title: US lawmakers have Chinese LiDAR on their threat-detection radar
Date: 2023-11-30T02:29:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2023-11-30-us-lawmakers-have-chinese-lidar-on-their-threat-detection-radar

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/30/us_lawmakers_examine_chinese_lidar/){:target="_blank" rel="noopener"}

> Amid fears Beijing could harvest spatial data, letter suggests Huawei-style bans may be needed A US congressional committee has questioned whether Chinese-made Light Detection and Ranging (LiDAR) devices might have a negative impact on national security, and suggested they may therefore be worthy of the same bans that prevent stateside adoption of other tech.... [...]
