Title: Google Play adds security audit badges for Android VPN apps
Date: 2023-11-03T12:48:58-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2023-11-03-google-play-adds-security-audit-badges-for-android-vpn-apps

[Source](https://www.bleepingcomputer.com/news/security/google-play-adds-security-audit-badges-for-android-vpn-apps/){:target="_blank" rel="noopener"}

> Google Play, Android's official app store, is now tagging VPN apps with an 'independent security reviews' badge if they conducted an independent security audit of their software and platform. [...]
