Title: Microsoft will roll out MFA-enforcing policies for admin portal access
Date: 2023-11-06T15:00:56-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-11-06-microsoft-will-roll-out-mfa-enforcing-policies-for-admin-portal-access

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-will-roll-out-mfa-enforcing-policies-for-admin-portal-access/){:target="_blank" rel="noopener"}

> Microsoft will roll out Conditional Access policies requiring multifactor authentication from administrators when signing into Microsoft admin portals such as Microsoft Entra, Microsoft 365, Exchange, and Azure. [...]
