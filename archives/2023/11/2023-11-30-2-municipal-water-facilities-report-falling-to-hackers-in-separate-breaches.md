Title: 2 municipal water facilities report falling to hackers in separate breaches
Date: 2023-11-30T00:42:01+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security
Slug: 2023-11-30-2-municipal-water-facilities-report-falling-to-hackers-in-separate-breaches

[Source](https://arstechnica.com/?p=1987313){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) In the stretch of a few days, two municipal water facilities that serve more than 2 million residents in parts of Pennsylvania and Texas have reported network security breaches that have hamstrung parts of their business or operational processes. In response to one of the attacks, the Municipal Water Authority of Aliquippa in western Pennsylvania temporarily shut down a pump providing drinking water from the facility’s treatment plant to the townships of Raccoon and Potter, according to reporting by the Beaver Countian. A photo the Water Authority provided to news outlets showed the front panel of [...]
