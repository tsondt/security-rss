Title: Building sensitive data remediation workflows in multi-account AWS environments
Date: 2023-11-15T21:21:13+00:00
Author: Darren Roback
Category: AWS Security
Tags: Advanced (300);Amazon Macie;Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2023-11-15-building-sensitive-data-remediation-workflows-in-multi-account-aws-environments

[Source](https://aws.amazon.com/blogs/security/building-sensitive-data-remediation-workflows-in-multi-account-aws-environments/){:target="_blank" rel="noopener"}

> The rapid growth of data has empowered organizations to develop better products, more personalized services, and deliver transformational outcomes for their customers. As organizations use Amazon Web Services (AWS) to modernize their data capabilities, they can sometimes find themselves with data spread across several AWS accounts, each aligned to distinct use cases and business units. [...]
