Title: Okta tells 5,000 of its own staff that their data was accessed in third-party breach
Date: 2023-11-02T15:37:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-02-okta-tells-5000-of-its-own-staff-that-their-data-was-accessed-in-third-party-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/02/okta_staff_personal_data/){:target="_blank" rel="noopener"}

> The hits keep on coming for troubled ID management biz Updated Okta has sent out breach notifications to almost 5,000 current and former employees, warning them that miscreants breached one of its third-party vendors and stole a file containing staff names, social security numbers, and health or medical insurance plan numbers.... [...]
