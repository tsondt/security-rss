Title: British Library begins contacting customers as Rhysida leaks data dump
Date: 2023-11-29T12:30:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-29-british-library-begins-contacting-customers-as-rhysida-leaks-data-dump

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/29/british_library_begins_contacting_customers/){:target="_blank" rel="noopener"}

> CRM databases were accessed and library users are advised to change passwords The Rhysida ransomware group has published most of the data it claimed to have stolen from the British Library a month after the attack was disclosed.... [...]
