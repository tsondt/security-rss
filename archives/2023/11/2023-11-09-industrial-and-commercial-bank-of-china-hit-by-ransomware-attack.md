Title: Industrial and Commercial Bank of China hit by ransomware attack
Date: 2023-11-09T14:52:23-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-09-industrial-and-commercial-bank-of-china-hit-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/industrial-and-commercial-bank-of-china-hit-by-ransomware-attack/){:target="_blank" rel="noopener"}

> The Industrial & Commercial Bank of China (ICBC) is restoring systems and services following a ransomware attack that disrupted the U.S. Treasury market, causing equities clearing issues. [...]
