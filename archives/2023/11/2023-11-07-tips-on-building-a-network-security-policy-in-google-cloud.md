Title: Tips on building a network security policy in Google Cloud
Date: 2023-11-07T17:00:00+00:00
Author: Anne Henmi
Category: GCP Security
Tags: Networking;Infrastructure Modernization;Security & Identity
Slug: 2023-11-07-tips-on-building-a-network-security-policy-in-google-cloud

[Source](https://cloud.google.com/blog/products/identity-security/tips-on-building-a-network-security-architecture-in-google-cloud/){:target="_blank" rel="noopener"}

> Changing the network security perspective In a data center, network security engineers tend to spend the bulk of their time managing individual devices: creating strong passwords and hardening configurations for networking devices and creating firewall rules for each endpoint. Since nothing is physical in the cloud networking world, the security focus should shift from hardware protection to software defined networking and protecting virtual devices. This means that you don’t have to worry about hardening router configs or configuring high availability. However, there’s a definitive paradigm shift you have to do coming from a data center to cloud infrastructure. Start with [...]
