Title: Fall 2023 SOC reports now available with 171 services in scope
Date: 2023-11-29T23:46:52+00:00
Author: Ryan Wilks
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS SOC Reports;Compliance;Security Blog;SOC
Slug: 2023-11-29-fall-2023-soc-reports-now-available-with-171-services-in-scope

[Source](https://aws.amazon.com/blogs/security/fall-2023-soc-reports-now-available-with-171-services-in-scope/){:target="_blank" rel="noopener"}

> At Amazon Web Services (AWS), we’re committed to providing our customers with continued assurance over the security, availability, confidentiality, and privacy of the AWS control environment. We’re proud to deliver the Fall 2023 System and Organizational (SOC) 1, 2, and 3 reports to support your confidence in AWS services. The reports cover the period October [...]
