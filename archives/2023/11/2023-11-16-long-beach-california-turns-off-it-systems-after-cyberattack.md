Title: Long Beach, California turns off IT systems after cyberattack
Date: 2023-11-16T17:23:30-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-11-16-long-beach-california-turns-off-it-systems-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/long-beach-california-turns-off-it-systems-after-cyberattack/){:target="_blank" rel="noopener"}

> The City of Long Beach in California is warning that they suffered a cyberattack on Tuesday that has led them to shut down portions of their IT network to prevent the attack's spread. [...]
