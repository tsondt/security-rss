Title: How to use the PassRole permission with IAM roles
Date: 2023-11-24T23:34:49+00:00
Author: Liam Wadman
Category: AWS Security
Tags: Advanced (300);AWS Identity and Access Management (IAM);Security, Identity, & Compliance;Technical How-to;AWS IAM;IAM roles;Privilege escalation;roles;Security Blog
Slug: 2023-11-24-how-to-use-the-passrole-permission-with-iam-roles

[Source](https://aws.amazon.com/blogs/security/how-to-use-the-passrole-permission-with-iam-roles/){:target="_blank" rel="noopener"}

> iam:PassRole is an AWS Identity and Access Management (IAM) permission that allows an IAM principal to delegate or pass permissions to an AWS service by configuring a resource such as an Amazon Elastic Compute Cloud (Amazon EC2) instance or AWS Lambda function with an IAM role. The service then uses that role to interact with [...]
