Title: Use CodeWhisperer to identify issues and use suggestions to improve code security in your IDE
Date: 2023-11-30T20:06:38+00:00
Author: Peter Grainger
Category: AWS Security
Tags: Amazon CodeGuru;Amazon CodeWhisperer;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2023-11-30-use-codewhisperer-to-identify-issues-and-use-suggestions-to-improve-code-security-in-your-ide

[Source](https://aws.amazon.com/blogs/security/use-codewhisperer-to-identify-issues-and-use-suggestions-to-improve-code-security-in-your-ide/){:target="_blank" rel="noopener"}

> I’ve always loved building things, but when I first began as a software developer, my least favorite part of the job was thinking about security. The security of those first lines of code just didn’t seem too important. Only after struggling through security reviews at the end of a project, did I realize that a [...]
