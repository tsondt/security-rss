Title: How to create an AMI hardening pipeline and automate updates to your ECS instance fleet
Date: 2023-11-03T17:18:05+00:00
Author: Nima Fotouhi
Category: AWS Security
Tags: Advanced (300);Amazon Elastic Container Service;Security, Identity, & Compliance;EC2 image builder;ECS;Security Blog
Slug: 2023-11-03-how-to-create-an-ami-hardening-pipeline-and-automate-updates-to-your-ecs-instance-fleet

[Source](https://aws.amazon.com/blogs/security/how-to-create-an-ami-hardening-pipeline-and-automate-updates-to-your-ecs-instance-fleet/){:target="_blank" rel="noopener"}

> Amazon Elastic Container Service (Amazon ECS) is a comprehensive managed container orchestrator that simplifies the deployment, maintenance, and scalability of container-based applications. With Amazon ECS, you can deploy your containerized application as a standalone task, or run a task as part of a service in your cluster. The Amazon ECS infrastructure for tasks includes Amazon [...]
