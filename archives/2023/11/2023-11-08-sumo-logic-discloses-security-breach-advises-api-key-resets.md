Title: Sumo Logic discloses security breach, advises API key resets
Date: 2023-11-08T13:31:24-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-08-sumo-logic-discloses-security-breach-advises-api-key-resets

[Source](https://www.bleepingcomputer.com/news/security/sumo-logic-discloses-security-breach-advises-api-key-resets/){:target="_blank" rel="noopener"}

> Security and data analytics company Sumo Logic disclosed a security breach after discovering that its AWS (Amazon Web Services) account was compromised last week. [...]
