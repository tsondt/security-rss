Title: Microsoft's bug bounty turns 10. Are these kinds of rewards making code more secure?
Date: 2023-11-22T10:58:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2023-11-22-microsofts-bug-bounty-turns-10-are-these-kinds-of-rewards-making-code-more-secure

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/22/microsofts_bug_bounty_moussouris/){:target="_blank" rel="noopener"}

> Katie Moussouris, who pioneered Redmond's program, says folks are focusing on the wrong thing Interview Microsoft's bug bounty program celebrated its tenth birthday this year, and has paid out $63 million to security researchers in that first decade – with $60 million awarded to bug hunters in the past five years alone, according to Redmond.... [...]
