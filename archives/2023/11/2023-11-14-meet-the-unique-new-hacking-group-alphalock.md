Title: Meet the Unique New "Hacking" Group: AlphaLock
Date: 2023-11-14T10:02:01-05:00
Author: Sponsored by Flare
Category: BleepingComputer
Tags: Security
Slug: 2023-11-14-meet-the-unique-new-hacking-group-alphalock

[Source](https://www.bleepingcomputer.com/news/security/meet-the-unique-new-hacking-group-alphalock/){:target="_blank" rel="noopener"}

> A Russian hacking group known as AlphaLock is launching a "pentest" marketplace and training platform to empower a new generation of threat actors. Learn more from Flare about the new hacking group. [...]
