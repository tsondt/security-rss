Title: Microsoft drops SMB1 firewall rules in new Windows 11 build
Date: 2023-11-08T14:56:57-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-11-08-microsoft-drops-smb1-firewall-rules-in-new-windows-11-build

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-drops-smb1-firewall-rules-in-new-windows-11-build/){:target="_blank" rel="noopener"}

> Windows 11 will no longer add SMB1 Windows Defender Firewall rules when creating new SMB shares starting with today's Canary Channel Insider Preview Build 25992 build. [...]
