Title: Cloud CISO Perspectives: Why ISACs are valuable security partners
Date: 2023-11-17T17:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2023-11-17-cloud-ciso-perspectives-why-isacs-are-valuable-security-partners

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-why-isacs-are-valuable-security-partners/){:target="_blank" rel="noopener"}

> Welcome to the first Cloud CISO Perspectives for November 2023. Google Cloud has announced multiple collaborations with sector-specific information sharing and analysis centers over the past 18 months, and in my column today I’ll be discussing why these ISACs are valuable partners for Google Cloud and our industry. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. aside_block <ListValue: [StructValue([('title', 'Board of Directors Insights Hub'), ('body', <wagtail.rich_text.RichText object at 0x3eb04994d820>), ('btn_text', 'Visit [...]
