Title: Ex-GCHQ software dev jailed for stabbing NSA staffer
Date: 2023-11-03T19:02:51+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-03-ex-gchq-software-dev-jailed-for-stabbing-nsa-staffer

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/03/gchq_software_dev_stabbing/){:target="_blank" rel="noopener"}

> Terrorist ideology suspected to be motivation A former software developer for Britain's cyberspy agency is facing years in the slammer after being sentenced for stabbing a National Security Agency (NSA) official multiple times.... [...]
