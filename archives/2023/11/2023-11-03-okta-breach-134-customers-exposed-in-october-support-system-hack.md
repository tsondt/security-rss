Title: Okta breach: 134 customers exposed in October support system hack
Date: 2023-11-03T10:18:26-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-03-okta-breach-134-customers-exposed-in-october-support-system-hack

[Source](https://www.bleepingcomputer.com/news/security/okta-breach-134-customers-exposed-in-october-support-system-hack/){:target="_blank" rel="noopener"}

> Okta says attackers who breached its customer support system last month gained access to files belonging to 134 customers, five of them later being targeted in session hijacking attacks with the help of stolen session tokens. [...]
