Title: Royal Mail cybersecurity still a bit of a mess, infosec bods claim
Date: 2023-11-13T06:31:13+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-11-13-royal-mail-cybersecurity-still-a-bit-of-a-mess-infosec-bods-claim

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/13/royal_mail_cybersecurity_still_a/){:target="_blank" rel="noopener"}

> Also: Most Mainers are MOVEit victims, NY radiology firm fined for not updating kit, and some critical vulnerabilities Infosec in brief After spending almost a year cleaning up after various security snafus, the UK's Royal Mail had an open redirect flaw on one of its sites, according to infosec types. We're told this vulnerability potentially exposes customers to malware infections and phishing attacks.... [...]
