Title: Mortgage giant Mr. Cooper hit by cyberattack impacting IT systems
Date: 2023-11-02T14:11:19-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2023-11-02-mortgage-giant-mr-cooper-hit-by-cyberattack-impacting-it-systems

[Source](https://www.bleepingcomputer.com/news/security/mortgage-giant-mr-cooper-hit-by-cyberattack-impacting-it-systems/){:target="_blank" rel="noopener"}

> U.S. mortgage lending giant Mr. Cooper was breached in a cyberattack that caused the company to shut down IT systems, including access to their online payment portal. [...]
