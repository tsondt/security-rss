Title: Apple to Add Manual Authentication to iMessage
Date: 2023-11-22T12:08:10+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;authentication;iPhone
Slug: 2023-11-22-apple-to-add-manual-authentication-to-imessage

[Source](https://www.schneier.com/blog/archives/2023/11/apple-to-add-manual-authentication-to-imessage.html){:target="_blank" rel="noopener"}

> Signal has had the ability to manually authenticate another account for years. iMessage is getting it : The feature is called Contact Key Verification, and it does just what its name says: it lets you add a manual verification step in an iMessage conversation to confirm that the other person is who their device says they are. (SMS conversations lack any reliable method for verification­—sorry, green-bubble friends.) Instead of relying on Apple to verify the other person’s identity using information stored securely on Apple’s servers, you and the other party read a short verification code to each other, either in [...]
