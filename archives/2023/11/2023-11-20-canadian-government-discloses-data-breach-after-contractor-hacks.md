Title: Canadian government discloses data breach after contractor hacks
Date: 2023-11-20T12:23:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Government
Slug: 2023-11-20-canadian-government-discloses-data-breach-after-contractor-hacks

[Source](https://www.bleepingcomputer.com/news/security/canadian-government-discloses-data-breach-after-contractor-hacks/){:target="_blank" rel="noopener"}

> The Canadian government says two of its contractors have been hacked, exposing sensitive information belonging to an undisclosed number of government employees. [...]
