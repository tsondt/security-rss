Title: Refine permissions for externally accessible roles using IAM Access Analyzer and IAM action last accessed
Date: 2023-11-01T18:51:27+00:00
Author: Nini Ren
Category: AWS Security
Tags: Amazon DynamoDB;AWS IAM Access Analyzer;AWS Identity and Access Management (IAM);Intermediate (200);Security;Access management;AWS IAM;IAM roles;least privilege;Policies;Security Blog
Slug: 2023-11-01-refine-permissions-for-externally-accessible-roles-using-iam-access-analyzer-and-iam-action-last-accessed

[Source](https://aws.amazon.com/blogs/security/refine-permissions-for-externally-accessible-roles-using-iam-access-analyzer-and-iam-action-last-accessed/){:target="_blank" rel="noopener"}

> When you build on Amazon Web Services (AWS) across accounts, you might use an AWS Identity and Access Management (IAM) role to allow an authenticated identity from outside your account—such as an IAM entity or a user from an external identity provider—to access the resources in your account. IAM roles have two types of policies [...]
