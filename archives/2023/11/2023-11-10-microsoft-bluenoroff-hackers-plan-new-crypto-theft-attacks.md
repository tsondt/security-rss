Title: Microsoft: BlueNoroff hackers plan new crypto-theft attacks
Date: 2023-11-10T15:40:58-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2023-11-10-microsoft-bluenoroff-hackers-plan-new-crypto-theft-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-bluenoroff-hackers-plan-new-crypto-theft-attacks/){:target="_blank" rel="noopener"}

> Microsoft warns that the BlueNoroff North Korean hacking group is setting up new attack infrastructure for upcoming social engineering campaigns on LinkedIn. [...]
