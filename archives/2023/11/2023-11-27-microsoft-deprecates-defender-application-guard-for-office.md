Title: Microsoft deprecates Defender Application Guard for Office
Date: 2023-11-27T16:00:57-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2023-11-27-microsoft-deprecates-defender-application-guard-for-office

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-deprecates-defender-application-guard-for-office/){:target="_blank" rel="noopener"}

> Microsoft is deprecating Defender Application Guard for Office and the Windows Security Isolation APIs, and it recommends Defender for Endpoint attack surface reduction rules, Protected View, and Windows Defender Application Control as an alternative. [...]
