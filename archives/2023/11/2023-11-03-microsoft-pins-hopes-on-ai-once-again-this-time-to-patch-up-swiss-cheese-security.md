Title: Microsoft pins hopes on AI once again – this time to patch up Swiss cheese security
Date: 2023-11-03T16:02:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-03-microsoft-pins-hopes-on-ai-once-again-this-time-to-patch-up-swiss-cheese-security

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/03/microsoft_secure_future_initiative/){:target="_blank" rel="noopener"}

> Secure Future Initiative needed in wake of tech evolution and unrelenting ransomware criminality Microsoft has made fresh commitments to harden the security of its software and cloud services after a year in which numerous members of the global infosec community criticized the company's tech defenses.... [...]
