Title: Royal Mail’s recovery from ransomware attack will cost business at least $12M
Date: 2023-11-16T12:31:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-16-royal-mails-recovery-from-ransomware-attack-will-cost-business-at-least-12m

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/16/royal_mail_recovery_from_ransomware/){:target="_blank" rel="noopener"}

> First time hard figure given on recovery costs for January incident Royal Mail's parent International Distributions Services has revealed for the first time the infrastructure costs associated with its January ransomware attack.... [...]
