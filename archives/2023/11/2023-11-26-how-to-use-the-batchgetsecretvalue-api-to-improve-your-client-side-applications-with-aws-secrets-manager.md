Title: How to use the BatchGetSecretValue API to improve your client-side applications with AWS Secrets Manager
Date: 2023-11-26T19:53:42+00:00
Author: Brendan Paul
Category: AWS Security
Tags: AWS Secrets Manager;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Cloud security;Secrets management;Security Blog
Slug: 2023-11-26-how-to-use-the-batchgetsecretvalue-api-to-improve-your-client-side-applications-with-aws-secrets-manager

[Source](https://aws.amazon.com/blogs/security/how-to-use-the-batchgetsecretsvalue-api-to-improve-your-client-side-applications-with-aws-secrets-manager/){:target="_blank" rel="noopener"}

> AWS Secrets Manager is a service that helps you manage, retrieve, and rotate database credentials, application credentials, OAuth tokens, API keys, and other secrets throughout their lifecycles. You can use Secrets Manager to help remove hard-coded credentials in application source code. Storing the credentials in Secrets Manager helps avoid unintended or inadvertent access by anyone [...]
