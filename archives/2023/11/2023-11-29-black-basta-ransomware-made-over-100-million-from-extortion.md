Title: Black Basta ransomware made over $100 million from extortion
Date: 2023-11-29T13:19:51-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2023-11-29-black-basta-ransomware-made-over-100-million-from-extortion

[Source](https://www.bleepingcomputer.com/news/security/black-basta-ransomware-made-over-100-million-from-extortion/){:target="_blank" rel="noopener"}

> Russia-linked ransomware gang Black Basta has raked in at least $100 million in ransom payments from more than 90 victims since it first surfaced in April 2022, according to joint research from Corvus Insurance and Elliptic. [...]
