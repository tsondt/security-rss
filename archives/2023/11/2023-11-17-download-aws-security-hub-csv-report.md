Title: Download AWS Security Hub CSV report
Date: 2023-11-17T20:04:35+00:00
Author: Pablo Pagani
Category: AWS Security
Tags: AWS Security Hub;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2023-11-17-download-aws-security-hub-csv-report

[Source](https://aws.amazon.com/blogs/security/download-aws-security-hub-csv-report/){:target="_blank" rel="noopener"}

> AWS Security Hub provides a comprehensive view of your security posture in Amazon Web Services (AWS) and helps you check your environment against security standards and best practices. In this post, I show you a solution to export Security Hub findings to a.csv file weekly and send an email notification to download the file from Amazon Simple [...]
