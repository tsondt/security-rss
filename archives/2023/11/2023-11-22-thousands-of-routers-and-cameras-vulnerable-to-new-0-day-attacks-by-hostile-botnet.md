Title: Thousands of routers and cameras vulnerable to new 0-day attacks by hostile botnet
Date: 2023-11-22T19:35:51+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;botnet;default credentials;Internet of things;mirai
Slug: 2023-11-22-thousands-of-routers-and-cameras-vulnerable-to-new-0-day-attacks-by-hostile-botnet

[Source](https://arstechnica.com/?p=1986211){:target="_blank" rel="noopener"}

> Enlarge (credit: Aurich Lawson / Ars Technica ) Miscreants are actively exploiting two new zero-day vulnerabilities to wrangle routers and video recorders into a hostile botnet used in distributed denial-of-service attacks, researchers from networking firm Akamai said Thursday. Both of the vulnerabilities, which were previously unknown to their manufacturers and to the security research community at large, allow for the remote execution of malicious code when the affected devices use default administrative credentials, according to an Akamai post. Unknown attackers have been exploiting the zero-days to compromise the devices so they can be infected with Mirai, a potent piece of [...]
