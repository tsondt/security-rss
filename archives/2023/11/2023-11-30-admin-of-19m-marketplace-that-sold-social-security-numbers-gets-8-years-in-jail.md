Title: Admin of $19M marketplace that sold social security numbers gets 8 years in jail
Date: 2023-11-30T18:30:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-30-admin-of-19m-marketplace-that-sold-social-security-numbers-gets-8-years-in-jail

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/30/administrator_of_19_million_ssndob/){:target="_blank" rel="noopener"}

> 24 million Americans thought to have had their personal data stolen and sold for pennies A Ukrainian national is facing an eight year prison sentence for running an online marketplace that sold the personal data of approximately 24 million US citizens.... [...]
