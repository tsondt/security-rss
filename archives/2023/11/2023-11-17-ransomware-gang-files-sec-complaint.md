Title: Ransomware Gang Files SEC Complaint
Date: 2023-11-17T16:31:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;data breaches;disclosure;extortion;ransomware
Slug: 2023-11-17-ransomware-gang-files-sec-complaint

[Source](https://www.schneier.com/blog/archives/2023/11/ransomware-gang-files-sec-complaint.html){:target="_blank" rel="noopener"}

> A ransomware gang, annoyed at not being paid, filed an SEC complaint against its victim for not disclosing its security breach within the required four days. This is over the top, but is just another example of the extreme pressure ransomware gangs put on companies after seizing their data. Gangs are now going through the data, looking for particularly important or embarrassing pieces of data to threaten executives with exposing. I have heard stories of executives’ families being threatened, of consensual porn being identified (people regularly mix work and personal email) and exposed, and of victims’ customers and partners being [...]
