Title: Sumo Logic wrestles with security breach, pins down customer data
Date: 2023-11-21T16:32:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-21-sumo-logic-wrestles-with-security-breach-pins-down-customer-data

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/21/sumo_logic_security_breach/){:target="_blank" rel="noopener"}

> Compromised AWS account led to fears that user info could have been exposed to cybercriminals Sumo Logic has confirmed that no customer data was compromised as a result of the potential security breach it discovered on November 3.... [...]
