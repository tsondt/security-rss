Title: The FCC says new rules will curb SIM swapping. I’m pessimistic
Date: 2023-11-18T18:38:48+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;FCC;federal communictions commission;fraud;port out;sim swapping
Slug: 2023-11-18-the-fcc-says-new-rules-will-curb-sim-swapping-im-pessimistic

[Source](https://arstechnica.com/?p=1985061){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images | Panuwat Sikham ) After years of inaction, the FCC this week said that it's finally going to protect consumers against a scam that takes control of their cell phone numbers by deceiving employees who work for mobile carriers. While commissioners congratulated themselves for the move, there’s little reason yet to believe it will stop a practice that has been all too common over the past decade. The scams, known as "SIM swapping" and "port-out fraud," both have the same objective: to wrest control of a cell phone number away from its rightful owner by tricking [...]
