Title: US sanctions Russian who laundered money for Ryuk ransomware affiliate
Date: 2023-11-06T12:20:08-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2023-11-06-us-sanctions-russian-who-laundered-money-for-ryuk-ransomware-affiliate

[Source](https://www.bleepingcomputer.com/news/security/us-sanctions-russian-who-laundered-money-for-ryuk-ransomware-affiliate/){:target="_blank" rel="noopener"}

> The U.S. Department of the Treasury's Office of Foreign Assets Control (OFAC) has sanctioned Russian national Ekaterina Zhdanova for laundering millions in cryptocurrency for various individuals, including ransomware actors. [...]
