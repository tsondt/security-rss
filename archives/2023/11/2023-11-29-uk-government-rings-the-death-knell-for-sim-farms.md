Title: UK government rings the death knell for SIM farms
Date: 2023-11-29T11:01:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-29-uk-government-rings-the-death-knell-for-sim-farms

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/29/uk_sim_farm_ban/){:target="_blank" rel="noopener"}

> Acts under the guise of protecting the public from fraud, yet history suggests Home Office has other motives The UK government plans to introduce new legislation to ban SIM farms, which it views as a widely abused means for carrying out cyber fraud.... [...]
