Title: Microsoft, Meta detail plans to fight election disinformation in 2024
Date: 2023-11-08T19:01:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2023-11-08-microsoft-meta-detail-plans-to-fight-election-disinformation-in-2024

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/08/microsoft_meta_election_disinformation/){:target="_blank" rel="noopener"}

> Strategies differ, though both have gaps that could hurt efficacy Microsoft and Meta have very different initiatives to combat misinformation in 2024, slated to be a busy election year all over the globe, but whether they'll be effective is another issue.... [...]
