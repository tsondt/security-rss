Title: Japan's space agency suffers cyber attack, points finger at Active Directory
Date: 2023-11-29T06:57:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2023-11-29-japans-space-agency-suffers-cyber-attack-points-finger-at-active-directory

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/29/jaxa_cyberattack/){:target="_blank" rel="noopener"}

> JAXA is having a tough time in cyberspace and outer space, the latter thanks to an electrical glitch Japan's Space Exploration Agency (JAXA) has reported a cyber incident.... [...]
