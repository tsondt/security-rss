Title: TransForm says ransomware data breach affects 267,000 patients
Date: 2023-11-07T18:37:51-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2023-11-07-transform-says-ransomware-data-breach-affects-267000-patients

[Source](https://www.bleepingcomputer.com/news/security/transform-says-ransomware-data-breach-affects-267-000-patients/){:target="_blank" rel="noopener"}

> Shared service provider TransForm has published an update on the cyberattack that recently impacted operations in multiple hospitals in Ontario, Canada, clarifying that it was a ransomware attack. [...]
