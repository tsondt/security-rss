Title: Monero Project admits thieves stole 6-figure sum from a wallet in mystery breach
Date: 2023-11-08T11:46:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-08-monero-project-admits-thieves-stole-6-figure-sum-from-a-wallet-in-mystery-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/08/monero_project_developers_announce_breach/){:target="_blank" rel="noopener"}

> It's the latest in a string of unusual wallet-draining attacks that began in April The Monero Project is admitting that one of its wallets was drained by an unknown source in September, losing the equivalent of around $437,000 at today's exchange rate.... [...]
