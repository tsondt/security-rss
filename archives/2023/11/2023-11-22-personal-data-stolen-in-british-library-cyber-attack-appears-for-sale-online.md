Title: Personal data stolen in British Library cyber-attack appears for sale online
Date: 2023-11-22T12:40:22+00:00
Author: Harriet Sherwood Arts and culture correspondent
Category: The Guardian
Tags: Cybercrime;British Library;UK news;London;Internet;Technology;Data and computer security;Libraries;Books
Slug: 2023-11-22-personal-data-stolen-in-british-library-cyber-attack-appears-for-sale-online

[Source](https://www.theguardian.com/technology/2023/nov/22/personal-data-stolen-in-british-library-cyber-attack-appears-for-sale-online){:target="_blank" rel="noopener"}

> Ransomware group Rhysida claims responsibility for hack and has posted images from library’s HR files The British Library has confirmed that personal data stolen in a cyber-attack has appeared online, apparently for sale to the highest bidder. The attack was carried out in October by a group known for such criminal activity, said the UK’s national library, which holds about 14m books and millions of other items. Continue reading... [...]
