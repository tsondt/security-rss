Title: Leader of pro-Russia DDoS crew Killnet 'unmasked' by Russian state media
Date: 2023-11-27T11:02:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2023-11-27-leader-of-pro-russia-ddos-crew-killnet-unmasked-by-russian-state-media

[Source](https://go.theregister.com/feed/www.theregister.com/2023/11/27/leader_of_prorussia_ddos_crew/){:target="_blank" rel="noopener"}

> Also: NXP China attack, Australia can't deliver on ransom payment ban (yet), and Justin Sun's very bad month Infosec in Brief Cybercriminals working out of Russia go to great lengths to conceal their real identities, and you won't ever find the state trying to unmask them either – as long as they keep supplying the attacks on Axis nations. It's the reason why we found it so amusing that of all the ways the identity of an organized cybercrime gang leader could be revealed, it was Russian state media that may have recently outed someone of note.... [...]
