Title: Use IAM Identity Center APIs to audit and manage application assignments
Date: 2023-11-27T18:17:19+00:00
Author: Laura Reith
Category: AWS Security
Tags: AWS IAM Identity Center;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2023-11-27-use-iam-identity-center-apis-to-audit-and-manage-application-assignments

[Source](https://aws.amazon.com/blogs/security/use-iam-identity-center-apis-to-audit-and-manage-application-assignments/){:target="_blank" rel="noopener"}

> You can now use AWS IAM Identity Center application assignment APIs to programmatically manage and audit user and group access to AWS managed applications. Previously, you had to use the IAM Identity Center console to manually assign users and groups to an application. Now, you can automate this task so that you scale more effectively as [...]
