Title: How to visualize Amazon Security Lake findings with Amazon QuickSight
Date: 2023-11-02T13:07:26+00:00
Author: Mark Keating
Category: AWS Security
Tags: Analytics;Architecture;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Amazon QuickSight;Amazon Security Lake;analytics;Security Blog
Slug: 2023-11-02-how-to-visualize-amazon-security-lake-findings-with-amazon-quicksight

[Source](https://aws.amazon.com/blogs/security/how-to-visualize-amazon-security-lake-findings-with-amazon-quicksight/){:target="_blank" rel="noopener"}

> Part 2 of a 3-part series Part 1 – Aggregating, searching, and visualizing log data from distributed sources with Amazon Athena and Amazon QuickSight Part 3 – How to share security telemetry per Organizational Unit using Amazon Security Lake and AWS Lake Formation In this post, we expand on the earlier blog post Ingest, transform, [...]
