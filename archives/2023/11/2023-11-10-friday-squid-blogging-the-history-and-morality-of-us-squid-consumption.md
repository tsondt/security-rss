Title: Friday Squid Blogging: The History and Morality of US Squid Consumption
Date: 2023-11-10T22:04:29+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2023-11-10-friday-squid-blogging-the-history-and-morality-of-us-squid-consumption

[Source](https://www.schneier.com/blog/archives/2023/11/friday-squid-blogging-the-history-and-morality-of-us-squid-consumption.html){:target="_blank" rel="noopener"}

> Really interesting article. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
