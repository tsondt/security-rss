Title: iPhone Users Urged to Update to Patch 2 Zero-Days
Date: 2022-08-19T15:25:56+00:00
Author: Elizabeth Montalbano
Category: Threatpost
Tags: Hacks;Mobile Security;News;Vulnerabilities;Apple iPhone;Apple vulnerabilities
Slug: 2022-08-19-iphone-users-urged-to-update-to-patch-2-zero-days

[Source](https://threatpost.com/iphone-users-urged-to-update-to-patch-2-zero-days-under-attack/180448/){:target="_blank" rel="noopener"}

> Separate fixes to macOS and iOS patch respective flaws in the kernel and WebKit that can allow threat actors to take over devices and are under attack. [...]
