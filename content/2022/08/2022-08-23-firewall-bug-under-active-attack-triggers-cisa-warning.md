Title: Firewall Bug Under Active Attack Triggers CISA Warning
Date: 2022-08-23T13:19:58+00:00
Author: Threatpost
Category: Threatpost
Tags: Vulnerabilities;Web Security
Slug: 2022-08-23-firewall-bug-under-active-attack-triggers-cisa-warning

[Source](https://threatpost.com/firewall-bug-under-active-attack-cisa-warning/180467/){:target="_blank" rel="noopener"}

> CISA is warning that Palo Alto Networks’ PAN-OS is under active attack and needs to be patched ASAP. [...]
