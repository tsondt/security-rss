Title: Twitter Whistleblower Complaint: The TL;DR Version
Date: 2022-08-24T14:17:04+00:00
Author: Threatpost
Category: Threatpost
Tags: Government;Privacy
Slug: 2022-08-24-twitter-whistleblower-complaint-the-tldr-version

[Source](https://threatpost.com/twitter-whistleblower-tldr-version/180472/){:target="_blank" rel="noopener"}

> Twitter is blasted for security and privacy lapses by the company’s former head of security who alleges the social media giant’s actions amount to a national security risk. [...]
