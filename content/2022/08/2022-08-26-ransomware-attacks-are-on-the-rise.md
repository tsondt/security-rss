Title: Ransomware Attacks are on the Rise
Date: 2022-08-26T16:44:27+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Malware;Vulnerabilities
Slug: 2022-08-26-ransomware-attacks-are-on-the-rise

[Source](https://threatpost.com/ransomware-attacks-are-on-the-rise/180481/){:target="_blank" rel="noopener"}

> Lockbit is by far this summer’s most prolific ransomware group, trailed by two offshoots of the Conti group. [...]
