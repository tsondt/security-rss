Title: Tentacles of ‘0ktapus’ Threat Group Victimize 130 Firms
Date: 2022-08-29T14:56:19+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Hacks;Privacy
Slug: 2022-08-29-tentacles-of-0ktapus-threat-group-victimize-130-firms

[Source](https://threatpost.com/0ktapus-victimize-130-firms/180487/){:target="_blank" rel="noopener"}

> Over 130 companies tangled in sprawling phishing campaign that spoofed a multi-factor authentication system. [...]
