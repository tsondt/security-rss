Title: Fake Reservation Links Prey on Weary Travelers
Date: 2022-08-22T13:59:06+00:00
Author: Nate Nelson
Category: Threatpost
Tags: Malware;Web Security
Slug: 2022-08-22-fake-reservation-links-prey-on-weary-travelers

[Source](https://threatpost.com/reservation-links-prey-on-travelers/180462/){:target="_blank" rel="noopener"}

> Fake travel reservations are exacting more pain from the travel weary, already dealing with the misery of canceled flights and overbooked hotels. [...]
