Title: GitHub Actions artifacts found leaking auth tokens in popular projects
Date: 2024-08-14T16:19:38-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-14-github-actions-artifacts-found-leaking-auth-tokens-in-popular-projects

[Source](https://www.bleepingcomputer.com/news/security/github-actions-artifacts-found-leaking-auth-tokens-in-popular-projects/){:target="_blank" rel="noopener"}

> Multiple high-profile open-source projects, including those from Google, Microsoft, AWS, and Red Hat, were found to leak GitHub authentication tokens through GitHub Actions artifacts in CI/CD workflows. [...]
