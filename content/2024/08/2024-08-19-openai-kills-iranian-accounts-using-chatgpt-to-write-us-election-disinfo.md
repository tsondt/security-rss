Title: OpenAI kills Iranian accounts using ChatGPT to write US election disinfo
Date: 2024-08-19T20:10:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-19-openai-kills-iranian-accounts-using-chatgpt-to-write-us-election-disinfo

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/19/openai_iranian_accounts/){:target="_blank" rel="noopener"}

> 12 on X and one on Instagram caught in the crackdown OpenAI has banned ChatGPT accounts linked to an Iranian crew suspected of spreading fake news on social media sites about the upcoming US presidential campaign.... [...]
