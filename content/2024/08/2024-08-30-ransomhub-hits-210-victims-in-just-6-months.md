Title: RansomHub hits 210 victims in just 6 months
Date: 2024-08-30T23:55:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-30-ransomhub-hits-210-victims-in-just-6-months

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/30/ransomhub/){:target="_blank" rel="noopener"}

> The ransomware gang recruits high-profile affiliates from LockBit and ALPHV As RansomHub continues to scoop up top talent from the fallen LockBit and ALPHV operations while accruing a smorgasbord of victims, security and law enforcement agencies in the US feel it's time to issue an official warning about the group that's gunning for ransomware supremacy.... [...]
