Title: Mega money, unfathomable violence pervade thriving underground doxxing scene
Date: 2024-08-12T14:24:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-12-mega-money-unfathomable-violence-pervade-thriving-underground-doxxing-scene

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/12/mega_money_and_unfathomable_violence/){:target="_blank" rel="noopener"}

> It also attracts exactly the type of unempathetic people you would think it does Black Hat Recently published interviews with known doxxers reveal the incredible finances behind the practice and how their extortion tactics are becoming increasingly violent.... [...]
