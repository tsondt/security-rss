Title: Israeli hacktivist group brags it took down Iran's internet
Date: 2024-08-02T16:40:13+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-08-02-israeli-hacktivist-group-brags-it-took-down-irans-internet

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/02/israeli_hacktivists/){:target="_blank" rel="noopener"}

> WeRedEvils alleges successful attack on infrastructure, including data theft Israel-based hacktivists are taking credit for an ongoing internet outage in Iran.... [...]
