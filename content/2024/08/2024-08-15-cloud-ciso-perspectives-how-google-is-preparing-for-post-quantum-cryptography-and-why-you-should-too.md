Title: Cloud CISO Perspectives: How Google is preparing for post-quantum cryptography (and why you should, too)
Date: 2024-08-15T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2024-08-15-cloud-ciso-perspectives-how-google-is-preparing-for-post-quantum-cryptography-and-why-you-should-too

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-why-we-need-to-get-ready-for-pqc/){:target="_blank" rel="noopener"}

> Welcome to the first Cloud CISO Perspectives for August 2024. Today I’m adapting our upcoming Perspectives on Security for the Board report. It examines three key cybersecurity topics from the vantage of the board of directors: multifactor authentication, digital sovereignty, and — the one I’ll be focusing on here — post-quantum cryptography. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI Security & CISO, Google Cloud aside_block <ListValue: [...]
