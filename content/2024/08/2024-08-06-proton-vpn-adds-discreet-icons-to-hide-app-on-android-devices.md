Title: Proton VPN adds ‘Discreet Icons’ to hide app on Android devices
Date: 2024-08-06T06:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-06-proton-vpn-adds-discreet-icons-to-hide-app-on-android-devices

[Source](https://www.bleepingcomputer.com/news/security/proton-vpn-adds-discreet-icons-to-hide-app-on-android-devices/){:target="_blank" rel="noopener"}

> Proton VPN has announced a series of updates to its Windows and Android apps to help users combat censorship, circumvent blocks, and protect themselves from authoritarian governments due to using forbidden tools. [...]
