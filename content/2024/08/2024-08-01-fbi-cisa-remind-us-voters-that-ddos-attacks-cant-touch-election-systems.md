Title: FBI, CISA remind US voters that DDoS attacks can't touch election systems
Date: 2024-08-01T15:07:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-01-fbi-cisa-remind-us-voters-that-ddos-attacks-cant-touch-election-systems

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/01/fbi_cisa_election_ddos/){:target="_blank" rel="noopener"}

> PSA comes amid multiple IT services crises in recent days US law enforcement and cybersecurity agencies are reminding the public that the country's voting systems will remain unaffected by distributed denial of service (DDoS) attacks as the next presidential election fast approaches.... [...]
