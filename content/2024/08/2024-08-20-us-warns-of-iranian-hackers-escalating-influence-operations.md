Title: US warns of Iranian hackers escalating influence operations
Date: 2024-08-20T07:56:23-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-20-us-warns-of-iranian-hackers-escalating-influence-operations

[Source](https://www.bleepingcomputer.com/news/security/us-warns-of-iranian-hackers-escalating-influence-operations/){:target="_blank" rel="noopener"}

> The U.S. government is warning of increased effort from Iran to influence upcoming elections through cyber operations targeting Presidential campaigns and the American public. [...]
