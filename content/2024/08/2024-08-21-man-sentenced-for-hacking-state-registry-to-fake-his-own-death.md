Title: Man sentenced for hacking state registry to fake his own death
Date: 2024-08-21T18:11:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;Government;Security
Slug: 2024-08-21-man-sentenced-for-hacking-state-registry-to-fake-his-own-death

[Source](https://www.bleepingcomputer.com/news/legal/man-sentenced-for-hacking-state-registry-to-fake-his-own-death/){:target="_blank" rel="noopener"}

> A 39-year old man from Somerset, Kentucky, was sentenced to 81 months in federal prison for identity theft and faking his own death in government registry systems. [...]
