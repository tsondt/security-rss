Title: Report: Tech misconceptions plague the IT world
Date: 2024-08-08T10:31:15+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-08-08-report-tech-misconceptions-plague-the-it-world

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/08/report_tech_misconceptions_plague_the/){:target="_blank" rel="noopener"}

> Just snapping the webcam shutter closed won't keep a user safe online New research shows that while many Brits will snap shut a laptop camera in the name of privacy, a worrying amount will just as happily shovel all manner of personal information into an online game in order to get a result they can share with their friends.... [...]
