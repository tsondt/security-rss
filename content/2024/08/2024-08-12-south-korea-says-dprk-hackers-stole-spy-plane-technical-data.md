Title: South Korea says DPRK hackers stole spy plane technical data
Date: 2024-08-12T16:22:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-12-south-korea-says-dprk-hackers-stole-spy-plane-technical-data

[Source](https://www.bleepingcomputer.com/news/security/south-korea-says-dprk-hackers-stole-spy-plane-technical-data/){:target="_blank" rel="noopener"}

> South Korea's ruling party, People Power Party (PPP), has issued an announcement stating that North Korean hackers have stolen crucial information about K2 tanks, the country's main battle tank, as well as its "Baekdu" and "Geumgang" spy planes. [...]
