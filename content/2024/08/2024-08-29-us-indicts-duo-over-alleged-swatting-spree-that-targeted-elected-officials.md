Title: US indicts duo over alleged Swatting spree that targeted elected officials
Date: 2024-08-29T22:28:14+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-08-29-us-indicts-duo-over-alleged-swatting-spree-that-targeted-elected-officials

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/29/us_indicts_swatted_duo/){:target="_blank" rel="noopener"}

> Apparently made over 100 fake crime reports and bomb threats The US government has indicted two men for allegedly reporting almost 120 fake emergencies or crimes in the hope of provoking action by armed law enforcement agencies.... [...]
