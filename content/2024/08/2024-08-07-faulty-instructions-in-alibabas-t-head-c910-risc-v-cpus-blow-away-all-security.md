Title: Faulty instructions in Alibaba's T-Head C910 RISC-V CPUs blow away all security
Date: 2024-08-07T17:00:15+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-08-07-faulty-instructions-in-alibabas-t-head-c910-risc-v-cpus-blow-away-all-security

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/07/riscv_business_thead_c910_vulnerable/){:target="_blank" rel="noopener"}

> Let's get physical, physical... I don't wanna hear your MMU talk Black Hat Computer security researchers at the CISPA Helmholtz Center for Information Security in Germany have found serious security flaws in some of Alibaba subsidiary T-Head Semiconductor's RISC-V processors.... [...]
