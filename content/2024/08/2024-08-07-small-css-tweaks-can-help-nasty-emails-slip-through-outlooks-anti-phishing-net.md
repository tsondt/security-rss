Title: Small CSS tweaks can help nasty emails slip through Outlook's anti-phishing net
Date: 2024-08-07T13:23:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-07-small-css-tweaks-can-help-nasty-emails-slip-through-outlooks-anti-phishing-net

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/07/small_css_tweaks_can_help/){:target="_blank" rel="noopener"}

> A simple HTML change and the warning is gone! Researchers say cybercriminals can have fun bypassing one of Microsoft's anti-phishing measures in Outlook with some simple CSS tweaks.... [...]
