Title: McLaren hospitals disruption linked to INC ransomware attack
Date: 2024-08-07T14:48:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-08-07-mclaren-hospitals-disruption-linked-to-inc-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/mclaren-hospitals-disruption-linked-to-inc-ransomware-attack/){:target="_blank" rel="noopener"}

> ​On Tuesday, IT and phone systems at McLaren Health Care hospitals were disrupted following an attack linked to the INC Ransom ransomware operation. [...]
