Title: Cognizant alleges Infosys swiped its trade secrets
Date: 2024-08-26T11:00:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-08-26-cognizant-alleges-infosys-swiped-its-trade-secrets

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/26/cognizant_subsidiary_sues_infosys_for/){:target="_blank" rel="noopener"}

> Sueball suggests outsourcer went out of bounds by developing competing product A subsidiary of IT outsourcer Cognizant filed a lawsuit on Friday in Texas federal court alleging that rival Infosys was involved in stealing trade secrets and engaging in anticompetitive behavior.... [...]
