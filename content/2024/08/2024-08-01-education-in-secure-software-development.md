Title: Education in Secure Software Development
Date: 2024-08-01T11:03:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;security education;software
Slug: 2024-08-01-education-in-secure-software-development

[Source](https://www.schneier.com/blog/archives/2024/08/education-in-secure-software-development.html){:target="_blank" rel="noopener"}

> The Linux Foundation and OpenSSF released a report on the state of education in secure software development....many developers lack the essential knowledge and skills to effectively implement secure software development. Survey findings outlined in the report show nearly one-third of all professionals directly involved in development and deployment ­ system operations, software developers, committers, and maintainers ­ self-report feeling unfamiliar with secure software development practices. This is of particular concern as they are the ones at the forefront of creating and maintaining the code that runs a company’s applications and systems. [...]
