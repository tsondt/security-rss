Title: AutoCanada discloses cyberattack impacting internal IT systems
Date: 2024-08-14T12:36:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-14-autocanada-discloses-cyberattack-impacting-internal-it-systems

[Source](https://www.bleepingcomputer.com/news/security/autocanada-discloses-cyberattack-impacting-internal-it-systems/){:target="_blank" rel="noopener"}

> Hackers targeted AutoCanada in a cyberattack last Sunday that impacted the automobile dealership group's internal IT systems, which may lead to disruptions. [...]
