Title: Sneaky SnakeKeylogger slithers into Windows inboxes to steal sensitive secrets
Date: 2024-08-05T14:28:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-05-sneaky-snakekeylogger-slithers-into-windows-inboxes-to-steal-sensitive-secrets

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/05/snakekeylogger_malware_windows/){:target="_blank" rel="noopener"}

> Malware logs users' keystrokes, pilfers credentials, exfiltrates data Criminals are preying on Windows users yet again, this time in an effort to hit them with a keylogger that can also steal credentials and take screenshots.... [...]
