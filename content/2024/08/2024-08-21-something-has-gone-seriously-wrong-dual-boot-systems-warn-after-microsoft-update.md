Title: “Something has gone seriously wrong,” dual-boot systems warn after Microsoft update
Date: 2024-08-21T00:17:10+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;microsoft;sbat;secure boot;security patches;shim
Slug: 2024-08-21-something-has-gone-seriously-wrong-dual-boot-systems-warn-after-microsoft-update

[Source](https://arstechnica.com/?p=2044381){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Last Tuesday, loads of Linux users—many running packages released as early as this year—started reporting their devices were failing to boot. Instead, they received a cryptic error message that included the phrase: “Something has gone seriously wrong.” The cause: an update Microsoft issued as part of its monthly patch release. It was intended to close a 2-year-old vulnerability in GRUB, an open source boot loader used to start up many Linux devices. The vulnerability, with a severity rating of 8.6 out of 10, made it possible for hackers to bypass secure boot, the industry standard for [...]
