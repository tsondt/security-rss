Title: US elections have never been more secure, says CISA chief
Date: 2024-08-08T12:56:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-08-08-us-elections-have-never-been-more-secure-says-cisa-chief

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/08/election_tech_is_fine_says/){:target="_blank" rel="noopener"}

> Election tech is fine – it's all those idiots buying into the propaganda that's worrying Jen Easterly Black Hat US Cybersecurity and Infrastructure Security Agency (CISA) director Jen Easterly and her counterparts from the UK and EU want the world to know that, when it comes to securing elections, they've never been more prepared.... [...]
