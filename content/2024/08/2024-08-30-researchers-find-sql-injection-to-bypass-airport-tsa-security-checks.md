Title: Researchers find SQL injection to bypass airport TSA security checks
Date: 2024-08-30T15:02:43-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-08-30-researchers-find-sql-injection-to-bypass-airport-tsa-security-checks

[Source](https://www.bleepingcomputer.com/news/security/researchers-find-sql-injection-to-bypass-airport-tsa-security-checks/){:target="_blank" rel="noopener"}

> Security researchers have found a vulnerability in a key air transport security system that allowed unauthorized individuals to potentially bypass airport security screenings and gain access to aircraft cockpits. [...]
