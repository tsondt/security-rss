Title: SEC ends probe into MOVEit attacks impacting 95 million people
Date: 2024-08-07T18:35:38-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Government;Legal
Slug: 2024-08-07-sec-ends-probe-into-moveit-attacks-impacting-95-million-people

[Source](https://www.bleepingcomputer.com/news/security/sec-ends-probe-into-moveit-attacks-impacting-95-million-people/){:target="_blank" rel="noopener"}

> The SEC concludes its investigation into Progress Software's handling of the widespread exploitation of a MOVEit Transfer zero-day flaw that exposed data of over 95 million people. [...]
