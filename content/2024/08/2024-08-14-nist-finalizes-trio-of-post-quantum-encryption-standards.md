Title: NIST finalizes trio of post-quantum encryption standards
Date: 2024-08-14T01:44:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-14-nist-finalizes-trio-of-post-quantum-encryption-standards

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/14/nist_postquantum_standards/){:target="_blank" rel="noopener"}

> Nicely ahead of that always-a-decade-away moment when all our info becomes an open book The National Institute of Standards and Technology (NIST) today released the long-awaited post-quantum encryption standards, designed to protect electronic information long into the future – when quantum computers are expected to break existing cryptographic algorithms.... [...]
