Title: Microsoft hosts a security summit but no press, public allowed
Date: 2024-08-28T22:20:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-28-microsoft-hosts-a-security-summit-but-no-press-public-allowed

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/28/microsoft_closed_security_summit/){:target="_blank" rel="noopener"}

> CrowdStrike, other vendors, friendly govt reps...but not anyone who would tell you what happened op-ed Microsoft will host a security summit next month with CrowdStrike and other "key" endpoint security partners joining the fun — and during which the CrowdStrike-induced outage that borked millions of Windows machines will undoubtedly be a top-line agenda item.... [...]
