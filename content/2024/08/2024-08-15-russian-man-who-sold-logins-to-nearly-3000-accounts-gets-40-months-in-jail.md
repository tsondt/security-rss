Title: Russian man who sold logins to nearly 3,000 accounts gets 40 months in jail
Date: 2024-08-15T12:22:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-15-russian-man-who-sold-logins-to-nearly-3000-accounts-gets-40-months-in-jail

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/15/russian_serial_credential_peddler_gets/){:target="_blank" rel="noopener"}

> He’ll also have to pay back $1.2 million from fraudulent transactions he facilitated A Russian national is taking a trip to prison in the US after being found guilty of peddling stolen credentials on a popular dark web marketplace.... [...]
