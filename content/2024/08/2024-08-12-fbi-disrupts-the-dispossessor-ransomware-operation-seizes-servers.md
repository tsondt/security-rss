Title: FBI disrupts the Dispossessor ransomware operation, seizes servers
Date: 2024-08-12T17:48:50-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-08-12-fbi-disrupts-the-dispossessor-ransomware-operation-seizes-servers

[Source](https://www.bleepingcomputer.com/news/security/fbi-disrupts-the-dispossessor-ransomware-operation-seizes-servers/){:target="_blank" rel="noopener"}

> The FBI announced on Monday that it seized the servers and websites of the Radar/Dispossessor ransomware operation following a joint international investigation. [...]
