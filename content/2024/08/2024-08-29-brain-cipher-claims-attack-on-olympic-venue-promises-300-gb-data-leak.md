Title: Brain Cipher claims attack on Olympic venue, promises 300 GB data leak
Date: 2024-08-29T12:32:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-29-brain-cipher-claims-attack-on-olympic-venue-promises-300-gb-data-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/29/brain_cipher_olympic_attack/){:target="_blank" rel="noopener"}

> French police reckon financial system targeted during Summer Games Nearly four weeks after the cyberattack on dozens of French national museums during the Olympic Games, the Brain Cipher ransomware group claims responsibility for the incident and says 300 GB of data will be leaked later today.... [...]
