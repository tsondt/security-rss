Title: CrowdStrike's meltdown didn't dent its market dominance … yet
Date: 2024-08-29T02:27:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-29-crowdstrikes-meltdown-didnt-dent-its-market-dominance-yet

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/29/crowdstrikes_q2_earnings/){:target="_blank" rel="noopener"}

> Total revenue for Q2 grew 32 percent CrowdStrike's major meltdown a month ago doesn't look like affecting the cyber security vendor's market dominance anytime soon, based on its earnings reported Wednesday.... [...]
