Title: NIST Releases First Post-Quantum Encryption Algorithms
Date: 2024-08-15T15:37:42+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptography;encryption;national security policy;NIST;quantum computing;security standards
Slug: 2024-08-15-nist-releases-first-post-quantum-encryption-algorithms

[Source](https://www.schneier.com/blog/archives/2024/08/nist-releases-first-post-quantum-encryption-algorithms.html){:target="_blank" rel="noopener"}

> From the Federal Register : After three rounds of evaluation and analysis, NIST selected four algorithms it will standardize as a result of the PQC Standardization Process. The public-key encapsulation mechanism selected was CRYSTALS-KYBER, along with three digital signature schemes: CRYSTALS-Dilithium, FALCON, and SPHINCS+. These algorithms are part of three NIST standards that have been finalized: FIPS 203: Module-Lattice-Based Key-Encapsulation Mechanism Standard FIPS 204: Module-Lattice-Based Digital Signature Standard FIPS 205: Stateless Hash-Based Digital Signature Standard NIST press release. My recent writings on post-quantum cryptographic standards. EDITED TO ADD: Good article : One – ML-KEM [PDF] (based on CRYSTALS-Kyber) – is [...]
