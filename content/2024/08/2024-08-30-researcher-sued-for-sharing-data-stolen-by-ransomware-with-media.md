Title: Researcher sued for sharing data stolen by ransomware with media
Date: 2024-08-30T10:44:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government;Legal
Slug: 2024-08-30-researcher-sued-for-sharing-data-stolen-by-ransomware-with-media

[Source](https://www.bleepingcomputer.com/news/security/researcher-sued-for-sharing-data-stolen-by-ransomware-with-media/){:target="_blank" rel="noopener"}

> The City of Columbus, Ohio, has filed a lawsuit against security researcher David Leroy Ross, aka Connor Goodwolf, accusing him of illegally downloading and disseminating data stolen from the City's IT network and leaked by the Rhysida ransomware gang. [...]
