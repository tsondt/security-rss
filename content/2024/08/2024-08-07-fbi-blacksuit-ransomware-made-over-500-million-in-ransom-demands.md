Title: FBI: BlackSuit ransomware made over $500 million in ransom demands
Date: 2024-08-07T18:26:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-08-07-fbi-blacksuit-ransomware-made-over-500-million-in-ransom-demands

[Source](https://www.bleepingcomputer.com/news/security/fbi-blacksuit-ransomware-made-over-500-million-in-ransom-demands/){:target="_blank" rel="noopener"}

> CISA and the FBI confirmed today that the Royal ransomware rebranded to BlackSuit and has demanded over $500 million from victims since it emerged more than two years ago. [...]
