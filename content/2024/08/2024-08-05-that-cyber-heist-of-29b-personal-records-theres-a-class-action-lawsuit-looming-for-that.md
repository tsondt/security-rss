Title: That cyber-heist of 2.9B personal records? There's a class-action lawsuit looming for that
Date: 2024-08-05T17:58:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-05-that-cyber-heist-of-29b-personal-records-theres-a-class-action-lawsuit-looming-for-that

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/05/national_public_data_lawsuit/){:target="_blank" rel="noopener"}

> Background check biz accused of negligence A lawsuit has accused a Florida data broker of carelessly failing to secure billions of records of people's private information, which was subsequently stolen from the biz and sold on an online criminal marketplace.... [...]
