Title: Russia tells citizens to switch off home surveillance because the Ukrainians are coming
Date: 2024-08-21T15:01:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-21-russia-tells-citizens-to-switch-off-home-surveillance-because-the-ukrainians-are-coming

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/21/russia_memo_ukraine_invasion/){:target="_blank" rel="noopener"}

> Forget about your love life too, no dating apps until the war is over Russia's Ministry of Internal Affairs is warning residents of under-siege regions to switch off home surveillance systems and dating apps to stop Ukraine from using them for intel-gathering purposes.... [...]
