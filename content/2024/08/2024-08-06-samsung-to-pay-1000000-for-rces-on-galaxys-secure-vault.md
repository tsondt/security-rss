Title: Samsung to pay $1,000,000 for RCEs on Galaxy’s secure vault
Date: 2024-08-06T13:13:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2024-08-06-samsung-to-pay-1000000-for-rces-on-galaxys-secure-vault

[Source](https://www.bleepingcomputer.com/news/security/samsung-to-pay-1-000-000-for-rces-on-galaxys-secure-vault/){:target="_blank" rel="noopener"}

> Samsung has launched a new bug bounty program for its mobile devices with rewards of up to $1,000,000 for reports demonstrating critical attack scenarios. [...]
