Title: The Present and Future of TV Surveillance
Date: 2024-08-27T11:08:01+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;privacy;surveillance;television;tracking
Slug: 2024-08-27-the-present-and-future-of-tv-surveillance

[Source](https://www.schneier.com/blog/archives/2024/08/the-present-and-future-of-tv-surveillance.html){:target="_blank" rel="noopener"}

> Ars Technica has a good article on what’s happening in the world of television surveillance. More than even I realized. [...]
