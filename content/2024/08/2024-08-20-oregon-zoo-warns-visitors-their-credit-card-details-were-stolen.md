Title: Oregon Zoo warns visitors their credit card details were stolen
Date: 2024-08-20T13:36:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-20-oregon-zoo-warns-visitors-their-credit-card-details-were-stolen

[Source](https://www.bleepingcomputer.com/news/security/oregon-zoo-warns-visitors-their-credit-card-details-were-stolen/){:target="_blank" rel="noopener"}

> Oregon Zoo is informing that visitors who purchased tickets online between December and June had their payment card information compromised. [...]
