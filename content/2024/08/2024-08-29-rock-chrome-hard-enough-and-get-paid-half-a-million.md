Title: Rock Chrome hard enough and get paid half a million
Date: 2024-08-29T16:30:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-08-29-rock-chrome-hard-enough-and-get-paid-half-a-million

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/29/google_chrome_vuln_rewards/){:target="_blank" rel="noopener"}

> Google revises Chrome Vulnerability Rewards Program with higher payouts for bug hunters Google's Chrome Vulnerability Rewards Program (VRP) is now significantly more rewarding – with a top payout that's at least twice as substantial.... [...]
