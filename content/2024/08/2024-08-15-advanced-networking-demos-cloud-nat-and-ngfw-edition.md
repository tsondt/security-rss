Title: Advanced networking demos - Cloud NAT and NGFW edition
Date: 2024-08-15T16:00:00+00:00
Author: Udit Bhatia
Category: GCP Security
Tags: Security & Identity;Developers & Practitioners;Networking
Slug: 2024-08-15-advanced-networking-demos-cloud-nat-and-ngfw-edition

[Source](https://cloud.google.com/blog/products/networking/cloud-nat-and-cloud-ngfw-demos/){:target="_blank" rel="noopener"}

> Are you curious about the new enhancements in Cloud NAT ? Have you explored Cloud NGFW Enterprise ? In this blog we dive into both of these topics with informative video demos on both that you can watch at any time. # 1- Cloud NAT and NGFWs Cloud NAT and Cloud NGFW are Google Cloud’s distributed cloud-managed network security services. Together they offer a comprehensive network security solution. Their integration allows for granular control of egress traffic, including allowing or denying access based on destination address and ports. The video talks about this integration for both first and third-party NGFWs. [...]
