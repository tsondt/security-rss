Title: Is Lenovo a blind spot in US anti-China security measures?
Date: 2024-08-14T09:37:08+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2024-08-14-is-lenovo-a-blind-spot-in-us-anti-china-security-measures

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/14/opinion_lenovo_jcdc/){:target="_blank" rel="noopener"}

> Questions raised as one of the world's largest PC makers joins America's critical defense team Opinion Lenovo's participation in a cybersecurity initiative has reopened old questions over the company's China origins, especially in light of the growing mistrust between Washington and Beijing over technology.... [...]
