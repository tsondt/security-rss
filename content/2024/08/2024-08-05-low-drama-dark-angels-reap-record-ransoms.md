Title: Low-Drama ‘Dark Angels’ Reap Record Ransoms
Date: 2024-08-05T19:52:35+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Ransomware;AmerisourceBergen Corporation;Bleeping Computer;Brett Stone-Gross;Cencora;Dark Angels;Dunghill Leak;ransomware;Sabre;sophos;Sysco;ThreatLabz;Zscaler
Slug: 2024-08-05-low-drama-dark-angels-reap-record-ransoms

[Source](https://krebsonsecurity.com/2024/08/low-drama-dark-angels-reap-record-ransoms/){:target="_blank" rel="noopener"}

> A ransomware group called Dark Angels made headlines this past week when it was revealed the crime group recently received a record $75 million data ransom payment from a Fortune 50 company. Security experts say the Dark Angels have been around since 2021, but the group doesn’t get much press because they work alone and maintain a low profile, picking one target at a time and favoring mass data theft over disrupting the victim’s operations. Image: Shutterstock. Security firm Zscaler ThreatLabz this month ranked Dark Angels as the top ransomware threat for 2024, noting that in early 2024 a victim [...]
