Title: US sends cybercriminals back to Russia in prisoner swap that freed WSJ journo, others
Date: 2024-08-02T00:50:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-02-us-sends-cybercriminals-back-to-russia-in-prisoner-swap-that-freed-wsj-journo-others

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/02/russia_prisoner_exchange_deal_cybercriminals/){:target="_blank" rel="noopener"}

> Techno-crooks greeted by grinning Putin after landing At least two Russian cybercriminals are among those being returned to their motherland as part of a multinational prisoner exchange deal announced Thursday.... [...]
