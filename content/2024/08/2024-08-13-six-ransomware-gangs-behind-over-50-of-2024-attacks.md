Title: Six ransomware gangs behind over 50% of 2024 attacks
Date: 2024-08-13T20:00:15+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-13-six-ransomware-gangs-behind-over-50-of-2024-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/13/lockbit_ransomware_stats/){:target="_blank" rel="noopener"}

> Plus many more newbies waiting in the wings Despite a law enforcement takedown six months ago, LockBit 3.0 remains the most prolific encryption and extortion gang, at least so far, this year, according to Palo Alto Networks' Unit 42.... [...]
