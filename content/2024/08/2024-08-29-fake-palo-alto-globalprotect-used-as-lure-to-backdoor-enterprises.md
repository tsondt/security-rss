Title: Fake Palo Alto GlobalProtect used as lure to backdoor enterprises
Date: 2024-08-29T14:29:43-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-29-fake-palo-alto-globalprotect-used-as-lure-to-backdoor-enterprises

[Source](https://www.bleepingcomputer.com/news/security/fake-palo-alto-globalprotect-used-as-lure-to-backdoor-enterprises/){:target="_blank" rel="noopener"}

> Threat actors target Middle Eastern organizations with malware disguised as the legitimate Palo Alto GlobalProtect Tool that can steal data and execute remote PowerShell commands to infiltrate internal networks further. [...]
