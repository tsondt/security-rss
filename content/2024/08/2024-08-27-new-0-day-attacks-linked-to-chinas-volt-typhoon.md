Title: New 0-Day Attacks Linked to China’s ‘Volt Typhoon’
Date: 2024-08-27T14:26:41+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Internet of Things (IoT);Latest Warnings;The Coming Storm;Black Lotus Labs;Bronze Silhouette;Christopher Wray;CVE-2024-39717;Cybersecurity & Infrastructure Security Agency;Federal Bureau of Investigation;Insidious Taurus;KV-botnet;Lumen Technologies;Michael Horka;national security agency;Ryan English;U.S. Department of Justice;Versa Director 22.1.4;Volt Typhoon
Slug: 2024-08-27-new-0-day-attacks-linked-to-chinas-volt-typhoon

[Source](https://krebsonsecurity.com/2024/08/new-0-day-attacks-linked-to-chinas-volt-typhoon/){:target="_blank" rel="noopener"}

> Malicious hackers are exploiting a zero-day vulnerability in Versa Director, a software product used by many Internet and IT service providers. Researchers believe the activity is linked to Volt Typhoon, a Chinese cyber espionage group focused on infiltrating critical U.S. networks and laying the groundwork for the ability to disrupt communications between the United States and Asia during any future armed conflict with China. Image: Shutterstock.com Versa Director systems are primarily used by Internet service providers (ISPs), as well as managed service providers (MSPs) that cater to the IT needs of many small to mid-sized businesses simultaneously. In a security [...]
