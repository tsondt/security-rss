Title: AMD won’t patch Sinkclose security bug on older Zen CPUs
Date: 2024-08-13T03:14:13+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-08-13-amd-wont-patch-sinkclose-security-bug-on-older-zen-cpus

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/13/amd_sinkclose_patches/){:target="_blank" rel="noopener"}

> Kernel mode not good enough for you? Maybe you'll like SMM of this Some AMD processors dating back to 2006 have a security vulnerability that's a boon for particularly underhand malware and rogue insiders, though the chip designer is only patching models made since 2020.... [...]
