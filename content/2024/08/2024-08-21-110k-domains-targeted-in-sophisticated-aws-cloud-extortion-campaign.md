Title: 110K domains targeted in 'sophisticated' AWS cloud extortion campaign
Date: 2024-08-21T17:23:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-21-110k-domains-targeted-in-sophisticated-aws-cloud-extortion-campaign

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/21/aws_extortion_campaign/){:target="_blank" rel="noopener"}

> If you needed yet another reminder of what happens when security basics go awry It's a good news day for organizations that don't leave their AWS environment files publicly exposed because infosec experts say those that do may be caught up in an extensive and sophisticated extortion campaign.... [...]
