Title: Nashville man arrested for running “laptop farm” to get jobs for North Koreans
Date: 2024-08-09T20:31:13+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;Identity theft;it jobs;North Korea;sanctions
Slug: 2024-08-09-nashville-man-arrested-for-running-laptop-farm-to-get-jobs-for-north-koreans

[Source](https://arstechnica.com/?p=2042326){:target="_blank" rel="noopener"}

> Enlarge Federal authorities have arrested a Nashville man on charges he hosted laptops at his residences in a scheme to deceive US companies into hiring foreign remote IT workers who funneled hundreds of thousands of dollars in income to fund North Korea’s weapons program. The scheme, federal prosecutors said, worked by getting US companies to unwittingly hire North Korean nationals, who used the stolen identity of a Georgia man to appear to be a US citizen. Under sanctions issued by the federal government, US employers are strictly forbidden from hiring citizens of North Korea. Once the North Korean nationals were [...]
