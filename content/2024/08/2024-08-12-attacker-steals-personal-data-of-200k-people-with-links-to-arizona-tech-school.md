Title: Attacker steals personal data of 200K+ people with links to Arizona tech school
Date: 2024-08-12T16:25:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-12-attacker-steals-personal-data-of-200k-people-with-links-to-arizona-tech-school

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/12/200k_with_links_to_arizona/){:target="_blank" rel="noopener"}

> Nearly 50 different data points were accessed by cybercrim An Arizona tech school will send letters to 208,717 current and former students, staff, and parents whose data was exposed during a January break-in that allowed an attacker to steal nearly 50 types of personal info.... [...]
