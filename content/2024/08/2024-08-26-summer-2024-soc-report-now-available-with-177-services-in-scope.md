Title: Summer 2024 SOC report now available with 177 services in scope
Date: 2024-08-26T18:10:51+00:00
Author: Brownell Combs
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS SOC Reports;Compliance;Security Blog;SOC
Slug: 2024-08-26-summer-2024-soc-report-now-available-with-177-services-in-scope

[Source](https://aws.amazon.com/blogs/security/summer-2024-soc-report-now-available-with-177-services-in-scope/){:target="_blank" rel="noopener"}

> We continue to expand the scope of our assurance programs at Amazon Web Services (AWS) and are pleased to announce that the Summer 2024 System and Organization Controls (SOC) 1 report is now available. The report covers 177 services over the 12-month period of July 1, 2023–June 30, 2024, so that customers have a full year of [...]
