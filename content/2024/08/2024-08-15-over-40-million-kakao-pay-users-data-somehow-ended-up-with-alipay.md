Title: Over 40 million Kakao Pay users' data somehow ended up with Alipay
Date: 2024-08-15T06:30:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-08-15-over-40-million-kakao-pay-users-data-somehow-ended-up-with-alipay

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/15/kakao_pay_data_leak/){:target="_blank" rel="noopener"}

> Payment arm of Korean messaging app denies any illegal activity Kakao Pay, a subsidiary of Korea's WhatsApp analog Kakao, handed over data from more than 40 million users to the Singaporean arm of Chinese payment platform Alipay, without user consent, Korea's financial watchdog revealed Tuesday.... [...]
