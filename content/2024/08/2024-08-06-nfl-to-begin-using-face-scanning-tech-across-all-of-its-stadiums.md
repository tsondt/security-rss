Title: NFL to begin using face scanning tech across all of its stadiums
Date: 2024-08-06T01:43:57+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-06-nfl-to-begin-using-face-scanning-tech-across-all-of-its-stadiums

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/06/nfl_face_scanning_tech/){:target="_blank" rel="noopener"}

> Smile for the camera to get in, or buy a beer without lining up The National Football League and all 32 of its teams will use tech from facial recognition software vendor Wicket to verify the identity of thousands of staff, media and fans as part of its credentialing program.... [...]
