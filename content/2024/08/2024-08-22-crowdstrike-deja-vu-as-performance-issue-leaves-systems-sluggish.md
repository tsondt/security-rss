Title: CrowdStrike deja vu as 'performance issue' leaves systems sluggish
Date: 2024-08-22T18:15:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-22-crowdstrike-deja-vu-as-performance-issue-leaves-systems-sluggish

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/22/crowdstrike_deja_vu/){:target="_blank" rel="noopener"}

> Not related to the massive outage in July, security biz spokesperson told us Some IT administrators suffered a moment of deja vu on Thursday morning as CrowdStrike blamed a cloud service issue for performance problems and lagging boot times affecting some of European customers.... [...]
