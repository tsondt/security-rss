Title: Windows Update downgrade attack "unpatches" fully-updated systems
Date: 2024-08-07T16:24:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-08-07-windows-update-downgrade-attack-unpatches-fully-updated-systems

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-update-downgrade-attack-unpatches-fully-updated-systems/){:target="_blank" rel="noopener"}

> SafeBreach security researcher Alon Leviev discovered a Windows Update downgrade attack that can "unpatch" fully-updated Windows 10, Windows 11, and Windows Server systems to reintroduce old vulnerabilities [...]
