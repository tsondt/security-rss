Title: Russia blocks Signal for 'violating' anti-terrorism laws
Date: 2024-08-09T15:07:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-08-09-russia-blocks-signal-for-violating-anti-terrorism-laws

[Source](https://www.bleepingcomputer.com/news/security/russia-blocks-signal-for-violating-anti-terrorism-laws/){:target="_blank" rel="noopener"}

> Russia's telecommunications watchdog Roskomnadzor has restricted access to the Signal encrypted messaging service for what it describes as violations of Russian anti-terrorism and anti-extremism legislation. [...]
