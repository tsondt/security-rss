Title: ADT confirms data breach after customer info leaked on hacking forum
Date: 2024-08-08T09:41:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-08-adt-confirms-data-breach-after-customer-info-leaked-on-hacking-forum

[Source](https://www.bleepingcomputer.com/news/security/adt-confirms-data-breach-after-customer-info-leaked-on-hacking-forum/){:target="_blank" rel="noopener"}

> ADT Inc. disclosed via a Form 8-K filing at the U.S. Securities and Exchange Commission (SEC) that hackers have gained access to its systems, which hold customer order details. [...]
