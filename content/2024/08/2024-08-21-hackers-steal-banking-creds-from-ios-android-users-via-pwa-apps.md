Title: Hackers steal banking creds from iOS, Android users via PWA apps
Date: 2024-08-21T16:57:58-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple;Google;Mobile
Slug: 2024-08-21-hackers-steal-banking-creds-from-ios-android-users-via-pwa-apps

[Source](https://www.bleepingcomputer.com/news/security/hackers-steal-banking-creds-from-ios-android-users-via-pwa-apps/){:target="_blank" rel="noopener"}

> Threat actors started to use progressive web applications to impersonate banking apps and steal credentials from Android and iOS users. [...]
