Title: Russian who sold 300,000 stolen credentials gets 40 months in prison
Date: 2024-08-14T19:11:56-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-08-14-russian-who-sold-300000-stolen-credentials-gets-40-months-in-prison

[Source](https://www.bleepingcomputer.com/news/security/russian-who-sold-300-000-stolen-credentials-gets-40-months-in-prison/){:target="_blank" rel="noopener"}

> ​Georgy Kavzharadze, a 27-year-old Russian national, has been sentenced to 40 months in prison for selling login credentials for over 300,000 accounts on Slilpp, the largest online marketplace of stolen logins, until its seizure in June 2021. [...]
