Title: Announcing AWS KMS Elliptic Curve Diffie-Hellman (ECDH) support
Date: 2024-08-19T14:16:02+00:00
Author: Patrick Palmer
Category: AWS Security
Tags: Advanced (300);Announcements;AWS Key Management Service;Security, Identity, & Compliance;AWS KMS;Cryptographic library;Encryption;Security Blog
Slug: 2024-08-19-announcing-aws-kms-elliptic-curve-diffie-hellman-ecdh-support

[Source](https://aws.amazon.com/blogs/security/announcing-aws-kms-elliptic-curve-diffie-hellman-ecdh-support/){:target="_blank" rel="noopener"}

> When using cryptography to protect data, protocol designers often prefer symmetric keys and algorithms for their speed and efficiency. However, when data is exchanged across an untrusted network such as the internet, it becomes difficult to ensure that only the exchanging parties can know the same key. Asymmetric key pairs and algorithms help to solve [...]
