Title: GitHub Enterprise Server vulnerable to critical auth bypass flaw
Date: 2024-08-21T10:15:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-21-github-enterprise-server-vulnerable-to-critical-auth-bypass-flaw

[Source](https://www.bleepingcomputer.com/news/security/github-enterprise-server-vulnerable-to-critical-auth-bypass-flaw/){:target="_blank" rel="noopener"}

> A critical vulnerability affecting multiple versions of GitHub Enterprise Server could be exploited to bypass authentication and enable an attacker to gain administrator privileges on the machine. [...]
