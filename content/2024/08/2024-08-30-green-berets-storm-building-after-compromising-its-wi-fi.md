Title: Green Berets storm building after compromising its Wi-Fi
Date: 2024-08-30T21:00:11+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-08-30-green-berets-storm-building-after-compromising-its-wi-fi

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/30/green_berets_wifi_hacking/){:target="_blank" rel="noopener"}

> Relax, it's just a drill. This time at least US Army Special Forces, aka the Green Berets, have been demonstrating their ability to use offensive cyber-security tools in the recent Swift Response 24 military exercises in May, the military has now confirmed.... [...]
