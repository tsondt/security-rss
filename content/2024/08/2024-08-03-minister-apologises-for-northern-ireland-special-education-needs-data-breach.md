Title: Minister apologises for Northern Ireland special education needs data breach
Date: 2024-08-03T11:27:32+00:00
Author: Shane Harrison
Category: The Guardian
Tags: Data protection;Northern Ireland;Education policy;Data and computer security;Education;Politics;UK news;Technology
Slug: 2024-08-03-minister-apologises-for-northern-ireland-special-education-needs-data-breach

[Source](https://www.theguardian.com/technology/article/2024/aug/03/minister-apologises-for-northern-ireland-special-education-needs-data-breach){:target="_blank" rel="noopener"}

> Paul Givan says details of 407 people mistakenly sent out included names, addresses and personal comments The education minister in Northern Ireland has “unreservedly” apologised after the personal details of more than 400 people who had offered to contribute to a review of special education needs were breached. The embarrassing data breach came to light on Thursday after the education department said it had mistakenly sent to 174 people a spreadsheet attachment that contained the names, email address and titles of 407 individuals who had expressed an interest in attending the end-to-end review of special education needs (SEN) events across [...]
