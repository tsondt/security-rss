Title: From Copilot to Copirate: How data thieves could hijack Microsoft's chatbot
Date: 2024-08-28T13:05:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-28-from-copilot-to-copirate-how-data-thieves-could-hijack-microsofts-chatbot

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/28/microsoft_copilot_copirate/){:target="_blank" rel="noopener"}

> Prompt injection, ASCII smuggling, and other swashbuckling attacks on the horizon Microsoft has fixed flaws in Copilot that allowed attackers to steal users' emails and other personal data by chaining together a series of LLM-specific attacks, beginning with prompt injection.... [...]
