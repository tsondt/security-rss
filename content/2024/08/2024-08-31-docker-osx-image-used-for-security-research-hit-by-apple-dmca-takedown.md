Title: Docker-OSX image used for security research hit by Apple DMCA takedown
Date: 2024-08-31T10:16:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple;Legal
Slug: 2024-08-31-docker-osx-image-used-for-security-research-hit-by-apple-dmca-takedown

[Source](https://www.bleepingcomputer.com/news/security/docker-osx-image-used-for-security-research-hit-by-apple-dmca-takedown/){:target="_blank" rel="noopener"}

> The popular Docker-OSX project has been removed from Docker Hub after Apple filed a DMCA (Digital Millennium Copyright Act) takedown request, alleging that it violated its copyright. [...]
