Title: India contemplates compulsory dynamic 2FA for digital payments
Date: 2024-08-02T03:30:09+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-08-02-india-contemplates-compulsory-dynamic-2fa-for-digital-payments

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/02/india_contemplates_compulsory_dynamic_2fa/){:target="_blank" rel="noopener"}

> SMS OTPs are overused, so bring on the tokens and biometrics India's central bank on Wednesday proposed a requirement for dynamically generated second authentication factors for most digital payments.... [...]
