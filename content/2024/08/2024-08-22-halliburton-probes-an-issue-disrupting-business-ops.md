Title: Halliburton probes 'an issue' disrupting business ops
Date: 2024-08-22T16:45:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-22-halliburton-probes-an-issue-disrupting-business-ops

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/22/halliburton_investigates_incident_amid_cyberattack/){:target="_blank" rel="noopener"}

> What could the problem be? Reportedly, a cyberattack Updated American oil giant Halliburton is investigating an "issue," reportedly a cyberattack, that has disrupted some business operations and global networks.... [...]
