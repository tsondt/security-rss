Title: Digital wallets can allow purchases with stolen credit cards
Date: 2024-08-20T01:29:47+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-08-20-digital-wallets-can-allow-purchases-with-stolen-credit-cards

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/20/digital_wallets_simplify_fraud/){:target="_blank" rel="noopener"}

> Researchers find it's possible to downgrade authentication checks, and shabby token refresh policies Digital wallets like Apple Pay, Google Pay, and PayPal can be used to conduct transactions using stolen and cancelled payment cards, according to academic security researchers.... [...]
