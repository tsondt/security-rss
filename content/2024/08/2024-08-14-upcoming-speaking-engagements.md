Title: Upcoming Speaking Engagements
Date: 2024-08-14T16:01:20+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2024-08-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2024/08/upcoming-speaking-engagements-39.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking at eCrime 2024 in Boston, Massachusetts, USA. The event runs from September 24 through 26, 2024, and my keynote is on the 24th. The list is maintained on this page. [...]
