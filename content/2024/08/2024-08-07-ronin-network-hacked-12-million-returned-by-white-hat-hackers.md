Title: Ronin Network hacked, $12 million returned by "white hat" hackers
Date: 2024-08-07T19:08:27-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-08-07-ronin-network-hacked-12-million-returned-by-white-hat-hackers

[Source](https://www.bleepingcomputer.com/news/security/ronin-network-hacked-12-million-returned-by-white-hat-hackers/){:target="_blank" rel="noopener"}

> Gambling blockchain Ronin Network suffered a security incident yesterday when white hat hackers exploited an undocumented vulnerability on the Ronin bridge to withdraw 4,000 ETH and 2 million USDC, totaling $12 million. [...]
