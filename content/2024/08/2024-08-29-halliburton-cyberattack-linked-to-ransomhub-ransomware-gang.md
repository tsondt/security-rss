Title: Halliburton cyberattack linked to RansomHub ransomware gang
Date: 2024-08-29T17:06:50-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-08-29-halliburton-cyberattack-linked-to-ransomhub-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/halliburton-cyberattack-linked-to-ransomhub-ransomware-gang/){:target="_blank" rel="noopener"}

> The RansomHub ransomware gang is behind the recent cyberattack on oil and gas services giant Halliburton, which disrupted the company's IT systems and business operations. [...]
