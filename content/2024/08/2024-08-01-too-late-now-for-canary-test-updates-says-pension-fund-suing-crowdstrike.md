Title: Too late now for canary test updates, says pension fund suing CrowdStrike
Date: 2024-08-01T18:40:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-01-too-late-now-for-canary-test-updates-says-pension-fund-suing-crowdstrike

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/01/crowdstrike_lawsuit/){:target="_blank" rel="noopener"}

> That horse has not just bolted, it's trampled all over kernel space CrowdStrike, after suggesting canary testing as a way to ensure it avoids future blunders leading to global computer outages, has been sued in federal court by investors for not using a phased approach in rolling out updates to customers in the first place.... [...]
