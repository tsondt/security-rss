Title: Friday Squid Blogging: Self-Healing Materials from Squid Teeth
Date: 2024-08-23T21:03:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-08-23-friday-squid-blogging-self-healing-materials-from-squid-teeth

[Source](https://www.schneier.com/blog/archives/2024/08/friday-squid-blogging-self-healing-materials-from-squid-teeth.html){:target="_blank" rel="noopener"}

> Making self-healing materials based on the teeth in squid suckers. Blog moderation policy. [...]
