Title: Unicoin hints at potential data meddling after G-Suite compromise
Date: 2024-08-16T19:43:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-16-unicoin-hints-at-potential-data-meddling-after-g-suite-compromise

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/16/unicoin_gsuite_compromise/){:target="_blank" rel="noopener"}

> Attacker locked out all staff for four days The cryptocurrency offshoot of reality TV and entrepreneurship show Unicorn Hunters has confirmed that an unknown attacker compromised its G-Suite, locking all staff out of their accounts.... [...]
