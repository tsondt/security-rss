Title: When Get-Out-The-Vote Efforts Look Like Phishing
Date: 2024-08-28T23:55:17+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;all-vote.com;Debra Cleaver;DomainTools.com;Movement Labs;Vote America;votewin.org;WDIV;Yoni Landau
Slug: 2024-08-28-when-get-out-the-vote-efforts-look-like-phishing

[Source](https://krebsonsecurity.com/2024/08/when-get-out-the-vote-efforts-look-like-phishing/){:target="_blank" rel="noopener"}

> Multiple media reports this week warned Americans to be on guard against a new phishing scam that arrives in a text message informing recipients they are not yet registered to vote. A bit of digging reveals the missives were sent by a California political consulting firm as part of a well-meaning but potentially counterproductive get-out-the-vote effort that had all the hallmarks of a phishing campaign. Image: WDIV Detroit on Youtube. On Aug. 27, the local Channel 4 affiliate WDIV in Detroit warned about a new SMS message wave that they said could prevent registered voters from casting their ballot. The [...]
