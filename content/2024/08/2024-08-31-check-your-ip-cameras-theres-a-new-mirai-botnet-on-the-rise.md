Title: Check your IP cameras: There's a new Mirai botnet on the rise
Date: 2024-08-31T18:22:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-08-31-check-your-ip-cameras-theres-a-new-mirai-botnet-on-the-rise

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/31/ip_cameras_mirai_botnet/){:target="_blank" rel="noopener"}

> Also, US offering $2.5M for Belarusian hacker, Backpage kingpins jailed, additional MOVEit victims, and more in brief A series of IP cameras still used all over the world, despite being well past their end of life, have been exploited to create a new Mirai botnet.... [...]
