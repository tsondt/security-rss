Title: Enzo Biochem ordered to cough up $4.5 million over lousy security that led to ransomware disaster
Date: 2024-08-14T17:02:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-14-enzo-biochem-ordered-to-cough-up-45-million-over-lousy-security-that-led-to-ransomware-disaster

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/14/enzo_biochem_ransomware_fine/){:target="_blank" rel="noopener"}

> Three state attorneys general probed the company and found plenty to chastise Biotech biz Enzo Biochem is being forced to pay three state attorneys general a $4.5 million penalty following a 2023 ransomware attack that compromised the data of more than 2.4 million people.... [...]
