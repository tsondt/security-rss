Title: Evolve your cloud security knowledge
Date: 2024-08-12T08:52:51+00:00
Author: Guy Matthews
Category: The Register
Tags: 
Slug: 2024-08-12-evolve-your-cloud-security-knowledge

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/12/evolve_your_cloud_security_knowledge/){:target="_blank" rel="noopener"}

> Let SANS help you get to grips with the shifting landscape of cloud security Sponsored Post Our reliance on the cloud continues to grow steadily, with a greater variety of services than ever being hosted in it.... [...]
