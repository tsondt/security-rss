Title: CannonDesign confirms Avos Locker ransomware data breach
Date: 2024-08-20T18:46:58-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-20-cannondesign-confirms-avos-locker-ransomware-data-breach

[Source](https://www.bleepingcomputer.com/news/security/cannondesign-confirms-avos-locker-ransomware-data-breach/){:target="_blank" rel="noopener"}

> The Cannon Corporation dba CannonDesign is sending notices of a data breach to more than 13,000 of its clients, informing that hackers breached and stole data from its network in an attack in early 2023. [...]
