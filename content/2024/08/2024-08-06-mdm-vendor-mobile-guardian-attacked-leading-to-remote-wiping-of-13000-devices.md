Title: MDM vendor Mobile Guardian attacked, leading to remote wiping of 13,000 devices
Date: 2024-08-06T04:25:45+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-08-06-mdm-vendor-mobile-guardian-attacked-leading-to-remote-wiping-of-13000-devices

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/06/mobile_guardian_mdm_attack/){:target="_blank" rel="noopener"}

> Singapore Ministry of Education orders software removed after string of snafus UK-based mobile device management vendor Mobile Guardian has admitted that on August 4 it suffered a security incident that involved unauthorized access to iOS and ChromeOS devices managed by its tools, which are currently unavailable. In Singapore, the incident resulted in 13,000 devices being remotely wiped and saw the nation's Education Ministry cut ties with the vendor.... [...]
