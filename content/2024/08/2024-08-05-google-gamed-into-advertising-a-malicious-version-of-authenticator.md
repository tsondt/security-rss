Title: Google gamed into advertising a malicious version of Authenticator
Date: 2024-08-05T02:00:06+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-08-05-google-gamed-into-advertising-a-malicious-version-of-authenticator

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/05/security_in_brief/){:target="_blank" rel="noopener"}

> Plus: CISA's AI hire; and claimed Canuck SIM swappers busted Infosec in brief Scammers have been using Google's own ad system to fool people into downloading a borked copy of the Chocolate Factory's Authenticator software.... [...]
