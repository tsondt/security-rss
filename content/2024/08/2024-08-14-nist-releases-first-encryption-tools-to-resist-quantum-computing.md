Title: NIST releases first encryption tools to resist quantum computing
Date: 2024-08-14T15:33:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-14-nist-releases-first-encryption-tools-to-resist-quantum-computing

[Source](https://www.bleepingcomputer.com/news/security/nist-releases-first-encryption-tools-to-resist-quantum-computing/){:target="_blank" rel="noopener"}

> The U.S. National Institute of Standards and Technology (NIST) has released the first three encryption standards designed to resist future cyberattacks based on quantum computing technology. [...]
