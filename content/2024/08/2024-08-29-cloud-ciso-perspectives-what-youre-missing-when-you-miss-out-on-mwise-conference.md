Title: Cloud CISO Perspectives: What you’re missing when you miss out on mWISE Conference
Date: 2024-08-29T16:00:00+00:00
Author: Peter Bailey
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2024-08-29-cloud-ciso-perspectives-what-youre-missing-when-you-miss-out-on-mwise-conference

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-what-youre-missing-when-you-miss-out-on-mwise-conference/){:target="_blank" rel="noopener"}

> Welcome to the second Cloud CISO Perspectives for August 2024. Today Google Cloud Security’s Peter Bailey talks about our upcoming Mandiant Worldwide Information Security Exchange (mWISE) Conference, and why mWISE can be one of the most valuable events this year for CISOs, security leaders, and those looking to rise through the ranks to attend. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI Security & CISO, Google Cloud [...]
