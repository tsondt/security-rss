Title: Phrack hacker zine publishes new edition after three years
Date: 2024-08-21T11:45:49-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-21-phrack-hacker-zine-publishes-new-edition-after-three-years

[Source](https://www.bleepingcomputer.com/news/security/phrack-hacker-zine-publishes-new-edition-after-three-years/){:target="_blank" rel="noopener"}

> Phrack #71 has been released online and is available to read for free. This issue is the first to be released since 2021, marking a new chapter in the influential online magazine's history. [...]
