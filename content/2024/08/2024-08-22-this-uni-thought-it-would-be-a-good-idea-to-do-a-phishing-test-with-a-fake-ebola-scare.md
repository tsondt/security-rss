Title: This uni thought it would be a good idea to do a phishing test with a fake Ebola scare
Date: 2024-08-22T10:32:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-08-22-this-uni-thought-it-would-be-a-good-idea-to-do-a-phishing-test-with-a-fake-ebola-scare

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/22/ucsc_phishing_test_ebola/){:target="_blank" rel="noopener"}

> Needless to say, it backfired in a big way University of California Santa Cruz (UCSC) students may be relieved to hear that an emailed warning about a staff member infected with the Ebola virus was just a phishing exercise.... [...]
