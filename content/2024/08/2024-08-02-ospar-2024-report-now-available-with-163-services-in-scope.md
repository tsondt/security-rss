Title: OSPAR 2024 report now available with 163 services in scope
Date: 2024-08-02T13:11:02+00:00
Author: Joseph Goh
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Compliance reports;OSPAR;Security Blog;Singapore
Slug: 2024-08-02-ospar-2024-report-now-available-with-163-services-in-scope

[Source](https://aws.amazon.com/blogs/security/ospar-2024-report-available-with-163-services-in-scope/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce the completion of our annual Outsourced Service Provider’s Audit Report (OSPAR) audit cycle on July 1, 2024. The 2024 OSPAR certification cycle includes the addition of 10 new services in scope, bringing the total number of services in scope to 163 in the AWS Asia Pacific (Singapore) [...]
