Title: Critical SAP flaw allows remote attackers to bypass authentication
Date: 2024-08-13T17:43:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-13-critical-sap-flaw-allows-remote-attackers-to-bypass-authentication

[Source](https://www.bleepingcomputer.com/news/security/critical-sap-flaw-allows-remote-attackers-to-bypass-authentication/){:target="_blank" rel="noopener"}

> SAP has released its security patch package for August 2024, addressing 17 vulnerabilities, including a critical authentication bypass that could allow remote attackers to fully compromise the system. [...]
