Title: Georgia's voter portal gets a crash course in client versus backend input validation
Date: 2024-08-07T04:05:15+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-08-07-georgias-voter-portal-gets-a-crash-course-in-client-versus-backend-input-validation

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/07/security_flaw_and_data_leak/){:target="_blank" rel="noopener"}

> Trying to cancel a citizen's registration would be caught by humans no matter what the page said, officials say The US state of Georgia has a website for cancelling voter registration, and it's had a bumpy start.... [...]
