Title: UK takes down major 'Russian Coms' caller ID spoofing platform
Date: 2024-08-01T13:53:04-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-08-01-uk-takes-down-major-russian-coms-caller-id-spoofing-platform

[Source](https://www.bleepingcomputer.com/news/security/uk-takes-down-russian-comms-caller-id-spoofing-platform-used-to-scam-170-000-people/){:target="_blank" rel="noopener"}

> The United Kingdom's National Crime Agency (NCA) has shut down Russian Coms, a major caller ID spoofing platform used by hundreds of criminals to make over 1.8 million scam calls. [...]
