Title: Matthew Green on Telegram’s Encryption
Date: 2024-08-28T11:00:40+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptanalysis;encryption;Telegram
Slug: 2024-08-28-matthew-green-on-telegrams-encryption

[Source](https://www.schneier.com/blog/archives/2024/08/matthew-green-on-telegrams-encryption.html){:target="_blank" rel="noopener"}

> Matthew Green wrote a really good blog post on what Telegram’s encryption is and is not. EDITED TO ADD (8/28): Another good explainer from Kaspersky. [...]
