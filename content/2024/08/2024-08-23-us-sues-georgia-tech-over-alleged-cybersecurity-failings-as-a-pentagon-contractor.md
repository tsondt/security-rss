Title: US sues Georgia Tech over alleged cybersecurity failings as a Pentagon contractor
Date: 2024-08-23T14:30:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-23-us-sues-georgia-tech-over-alleged-cybersecurity-failings-as-a-pentagon-contractor

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/23/us_georgia_tech_lawsuit/){:target="_blank" rel="noopener"}

> Rap sheet spells out major no-nos after disgruntled staff blow whistle The US is suing one of its leading research universities over a litany of alleged failures to meet cybersecurity standards set by the Department of Defense (DoD) for contract awardees.... [...]
