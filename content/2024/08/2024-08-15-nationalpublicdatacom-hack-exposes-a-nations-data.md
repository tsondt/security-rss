Title: NationalPublicData.com Hack Exposes a Nation’s Data
Date: 2024-08-15T22:38:36+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;The Coming Storm;Atlas Data Privacy Corp.;fbi;HaveIBeenPwned.com;InfraGard;Instant Checkmate;Jerico Pictures;National Criminal Data LLC;National Public Data breach;PeopleConnect;recordscheck.net;Sal Verini;Salvatore Verini;Shadowglade LLC;SXUL;Trinity Entertainment;Troy Hunt;TruthFinder;Twisted History LLC;USDoD;VX-Underground
Slug: 2024-08-15-nationalpublicdatacom-hack-exposes-a-nations-data

[Source](https://krebsonsecurity.com/2024/08/nationalpublicdata-com-hack-exposes-a-nations-data/){:target="_blank" rel="noopener"}

> A great many readers this month reported receiving alerts that their Social Security Number, name, address and other personal information were exposed in a breach at a little-known but aptly-named consumer data broker called NationalPublicData.com. This post examines what we know about a breach that has exposed hundreds of millions of consumer records. We’ll also take a closer look at the data broker that got hacked — a background check company founded by an actor and retired sheriff’s deputy from Florida. On July 21, 2024, denizens of the cybercrime community Breachforums released more than 4 terabytes of data they claimed [...]
