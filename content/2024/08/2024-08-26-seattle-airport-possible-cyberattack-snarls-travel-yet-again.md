Title: Seattle airport 'possible cyberattack' snarls travel yet again
Date: 2024-08-26T18:30:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-26-seattle-airport-possible-cyberattack-snarls-travel-yet-again

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/26/seattle_airport_cyberattack/){:target="_blank" rel="noopener"}

> No word yet on if ransomware is to blame The Port of Seattle, which operates the Seattle-Tacoma International Airport, is investigating a "possible cyberattack" after computer outages disrupted the airport's operations and delayed flights.... [...]
