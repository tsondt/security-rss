Title: U.S. Trades Cybercriminals to Russia in Prisoner Swap
Date: 2024-08-02T00:15:44+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Ransomware;Russia's War on Ukraine;Alexander Vinnik;Alsu Kurmasheva;BTC-e;Evan Gershkovich;German Moyzhes;Ivan Ermakov;Maxim Marchenko;Mt. Gox;Paul Whelan;Roman Seleznev;trickbot;Vadim Krasikov;Vladimir Putin;Vladislav Klyushin
Slug: 2024-08-02-us-trades-cybercriminals-to-russia-in-prisoner-swap

[Source](https://krebsonsecurity.com/2024/08/u-s-trades-5-cybercriminals-to-russia-in-prisoner-swap/){:target="_blank" rel="noopener"}

> Twenty-four prisoners were freed today in an international prisoner swap between Russia and Western countries. Among the eight Russians repatriated were several convicted cybercriminals. In return, Russia has reportedly released 16 prisoners, including Wall Street Journal reporter Evan Gershkovich and ex-U.S. Marine Paul Whelan. Among the more notable Russian hackers released in the prisoner swap is Roman Seleznev, 40, who was sentenced in 2017 to 27 years in prison for racketeering convictions tied to a lengthy career in stealing and selling payment card data. Seleznev earned this then-record sentence by operating some of the underground’s most bustling marketplaces for stolen [...]
