Title: Iran's Pioneer Kitten hits US networks via buggy Check Point, Palo Alto gear
Date: 2024-08-28T18:00:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-28-irans-pioneer-kitten-hits-us-networks-via-buggy-check-point-palo-alto-gear

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/28/iran_pioneer_kitten/){:target="_blank" rel="noopener"}

> The government-backed crew also enjoys ransomware as a side hustle Iranian government-backed cybercriminals have been hacking into US and foreign networks as recently as this month to steal sensitive data and deploy ransomware, and they're breaking in via vulnerable VPN and firewall devices from Check Point, Citrix, Palo Alto Networks and other manufacturers, according to Uncle Sam.... [...]
