Title: Cloud infrastructure entitlement management in AWS
Date: 2024-08-13T21:22:32+00:00
Author: Mathangi Ramesh
Category: AWS Security
Tags: AWS IAM Access Analyzer;AWS Identity and Access Management (IAM);Best Practices;Foundational (100);Security, Identity, & Compliance;IAM;Security Blog
Slug: 2024-08-13-cloud-infrastructure-entitlement-management-in-aws

[Source](https://aws.amazon.com/blogs/security/cloud-infrastructure-entitlement-management-in-aws/){:target="_blank" rel="noopener"}

> Customers use Amazon Web Services (AWS) to securely build, deploy, and scale their applications. As your organization grows, you want to streamline permissions management towards least privilege for your identities and resources. At AWS, we see two customer personas working towards least privilege permissions: security teams and developers. Security teams want to centrally inspect permissions [...]
