Title: Introducing delayed destruction for Secret Manager, a new way to protect your secrets
Date: 2024-08-21T16:00:00+00:00
Author: Yuriy Babenko
Category: GCP Security
Tags: Security & Identity
Slug: 2024-08-21-introducing-delayed-destruction-for-secret-manager-a-new-way-to-protect-your-secrets

[Source](https://cloud.google.com/blog/products/identity-security/introducing-delayed-destruction-a-new-way-to-protect-your-secrets/){:target="_blank" rel="noopener"}

> Secret Manager is a fully-managed, scalable service for storing, operating, auditing and accessing secrets used across Google Cloud services including GKE and Compute Engine. A critical part of any secrets management strategy is managing deletion and destruction of secrets. To provide customers with advanced capabilities in this area, we are pleased to announce the general availability of delayed destruction of secret versions for Secret Manager. This new capability helps to ensure that secret material cannot be erroneously deleted — either by accident or as part of an intended malicious attack. How It Works There are two main resources within Secret [...]
