Title: Ransomware rakes in record-breaking $450 million in first half of 2024
Date: 2024-08-19T16:17:38-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-08-19-ransomware-rakes-in-record-breaking-450-million-in-first-half-of-2024

[Source](https://www.bleepingcomputer.com/news/security/ransomware-rakes-in-record-breaking-450-million-in-first-half-of-2024/){:target="_blank" rel="noopener"}

> Ransomware victims have paid $459,800,000 to cybercriminals in the first half of 2024, setting the stage for a new record this year if ransom payments continue at this level. [...]
