Title: Firefox's Mozilla follows Google in losing trust in Entrust's TLS certificates
Date: 2024-08-01T12:28:26+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-01-firefoxs-mozilla-follows-google-in-losing-trust-in-entrusts-tls-certificates

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/01/mozilla_entrust/){:target="_blank" rel="noopener"}

> Compliance failures and unsatisfactory responses mount from the long-time certificate authority Mozilla is following in Google Chrome's footsteps in officially distrusting Entrust as a root certificate authority (CA) following what it says was a protracted period of compliance failures.... [...]
