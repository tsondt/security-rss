Title: DARPA, ARPA-H award $14m to 7 AIxCC semifinalists, with a catch
Date: 2024-08-15T19:15:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-08-15-darpa-arpa-h-award-14m-to-7-aixcc-semifinalists-with-a-catch

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/15/darpa_arpah_award_14m_to/){:target="_blank" rel="noopener"}

> Teams wanting the cash have to commit to handing their models to OpenSSF after next year's final One year after it began, the DARPA AI Cyber Challenge (AIxCC) has whittled its pool of contestants down to seven semifinalists.... [...]
