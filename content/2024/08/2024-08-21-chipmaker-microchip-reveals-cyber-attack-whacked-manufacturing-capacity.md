Title: Chipmaker Microchip reveals cyber attack whacked manufacturing capacity
Date: 2024-08-21T01:15:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-08-21-chipmaker-microchip-reveals-cyber-attack-whacked-manufacturing-capacity

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/21/microchip_technology_security_incident/){:target="_blank" rel="noopener"}

> Defense contractor gets hacked – what's the worst that could happen US semiconductor manufacturing firm Microchip Technology has revealed an "unauthorized party disrupted the Company's use of certain servers and some business operation."... [...]
