Title: How MSPs and MSSPs offer vCISO services with skilled CISOs in short supply
Date: 2024-08-07T10:01:02-04:00
Author: Sponsored by Cynomi
Category: BleepingComputer
Tags: Security
Slug: 2024-08-07-how-msps-and-mssps-offer-vciso-services-with-skilled-cisos-in-short-supply

[Source](https://www.bleepingcomputer.com/news/security/how-msps-and-mssps-offer-vciso-services-with-skilled-cisos-in-short-supply/){:target="_blank" rel="noopener"}

> With skilled CISOs in short supply, service providers are turning to virtual CISOs. A new eBook by Cynomi explains how service providers/MSPs can quickly and easily expand vCISO service offerings to their customers. [...]
