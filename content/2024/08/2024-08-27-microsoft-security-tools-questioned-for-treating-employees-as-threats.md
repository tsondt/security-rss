Title: Microsoft security tools questioned for treating employees as threats
Date: 2024-08-27T14:00:15+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-08-27-microsoft-security-tools-questioned-for-treating-employees-as-threats

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/27/microsoft_workplace_surveillance/){:target="_blank" rel="noopener"}

> Cracked Labs examines how workplace surveillance turns workers into suspects Software designed to address legitimate business concerns about cyber security and compliance treats employees as threats, normalizing intrusive surveillance in the workplace, according to a report by Cracked Labs.... [...]
