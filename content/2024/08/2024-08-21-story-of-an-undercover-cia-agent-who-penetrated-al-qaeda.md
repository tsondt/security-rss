Title: Story of an Undercover CIA Agent who Penetrated Al Qaeda
Date: 2024-08-21T13:56:29+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;al Qaeda;CIA;espionage;undercover
Slug: 2024-08-21-story-of-an-undercover-cia-agent-who-penetrated-al-qaeda

[Source](https://www.schneier.com/blog/archives/2024/08/story-of-an-undercover-cia-agent-who-penetrated-al-qaeda.html){:target="_blank" rel="noopener"}

> Rolling Stone has a long investigative story (non-paywalled version here ) about a CIA agent who spent years posing as an Islamic radical. Unrelated, but also in the “real life spies” file: a fake Sudanese diving resort run by Mossad. [...]
