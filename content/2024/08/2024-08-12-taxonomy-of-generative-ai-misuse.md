Title: Taxonomy of Generative AI Misuse
Date: 2024-08-12T10:14:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence;taxonomies
Slug: 2024-08-12-taxonomy-of-generative-ai-misuse

[Source](https://www.schneier.com/blog/archives/2024/08/taxonomy-of-generative-ai-misuse.html){:target="_blank" rel="noopener"}

> Interesting paper: “ Generative AI Misuse: A Taxonomy of Tactics and Insights from Real-World Data “: Generative, multimodal artificial intelligence (GenAI) offers transformative potential across industries, but its misuse poses significant risks. Prior research has shed light on the potential of advanced AI systems to be exploited for malicious purposes. However, we still lack a concrete understanding of how GenAI models are specifically exploited or abused in practice, including the tactics employed to inflict harm. In this paper, we present a taxonomy of GenAI misuse tactics, informed by existing academic literature and a qualitative analysis of approximately 200 observed incidents [...]
