Title: Leaked GitHub Python Token
Date: 2024-08-02T11:01:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;leaks;security analysis;supply chain
Slug: 2024-08-02-leaked-github-python-token

[Source](https://www.schneier.com/blog/archives/2024/08/leaked-github-python-token.html){:target="_blank" rel="noopener"}

> Here’s a disaster that didn’t happen : Cybersecurity researchers from JFrog recently discovered a GitHub Personal Access Token in a public Docker container hosted on Docker Hub, which granted elevated access to the GitHub repositories of the Python language, Python Package Index (PyPI), and the Python Software Foundation (PSF). JFrog discussed what could have happened : The implications of someone finding this leaked token could be extremely severe. The holder of such a token would have had administrator access to all of Python’s, PyPI’s and Python Software Foundation’s repositories, supposedly making it possible to carry out an extremely large scale [...]
