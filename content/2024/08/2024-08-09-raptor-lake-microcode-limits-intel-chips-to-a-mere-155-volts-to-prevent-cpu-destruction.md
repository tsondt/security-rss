Title: Raptor Lake microcode limits Intel chips to a mere 1.55 volts to prevent CPU destruction
Date: 2024-08-09T19:16:53+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-08-09-raptor-lake-microcode-limits-intel-chips-to-a-mere-155-volts-to-prevent-cpu-destruction

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/09/new_raptor_lake_microcode_limits/){:target="_blank" rel="noopener"}

> Is that a lot? Depends on the context. GHz, no. Voltage, yes Intel has divulged more details on its Raptor Lake family of 13th and 14th Gen Core processor failures and the 0x129 microcode that's supposed to prevent further damage from occurring.... [...]
