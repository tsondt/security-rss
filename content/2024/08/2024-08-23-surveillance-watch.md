Title: Surveillance Watch
Date: 2024-08-23T01:15:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;privacy;surveillance
Slug: 2024-08-23-surveillance-watch

[Source](https://www.schneier.com/blog/archives/2024/08/surveillance-watch.html){:target="_blank" rel="noopener"}

> This is a fantastic project mapping the global surveillance industry. [...]
