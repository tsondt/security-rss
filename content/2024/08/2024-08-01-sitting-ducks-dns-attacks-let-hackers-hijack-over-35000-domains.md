Title: Sitting Ducks DNS attacks let hackers hijack over 35,000 domains
Date: 2024-08-01T13:10:28-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-01-sitting-ducks-dns-attacks-let-hackers-hijack-over-35000-domains

[Source](https://www.bleepingcomputer.com/news/security/sitting-ducks-dns-attacks-let-hackers-hijack-over-35-000-domains/){:target="_blank" rel="noopener"}

> Threat actors have hijacked more than 35,000 registered domains in so-called Sitting Ducks attacks that allow claiming a domain without having access to the owner's account at the DNS provider or registrar. [...]
