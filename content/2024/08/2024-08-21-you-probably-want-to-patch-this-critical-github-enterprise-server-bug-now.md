Title: You probably want to patch this critical GitHub Enterprise Server bug now
Date: 2024-08-21T23:15:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-21-you-probably-want-to-patch-this-critical-github-enterprise-server-bug-now

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/21/patch_github_enterprise_bug/){:target="_blank" rel="noopener"}

> Unless you're cool with an unauthorized criminal enjoying admin privileges to comb through your code A critical bug in GitHub Enterprise Server could allow an attacker to gain unauthorized access to a user account with administrator privileges and then wreak havoc on an organization's code repositories.... [...]
