Title: How to counter adversarial AI
Date: 2024-08-01T15:05:12+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2024-08-01-how-to-counter-adversarial-ai

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/01/how_to_counter_adversarial_ai/){:target="_blank" rel="noopener"}

> Using Precision AI to stop cyber threats in real time Sponsored Hackers and cyber criminals are busy finding new ways of using AI to launch attacks on businesses and organisations often unprepared to deal with the speed, scale and sophistication of the assaults directed against them.... [...]
