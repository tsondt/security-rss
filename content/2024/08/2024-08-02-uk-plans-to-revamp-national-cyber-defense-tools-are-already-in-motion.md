Title: UK plans to revamp national cyber defense tools are already in motion
Date: 2024-08-02T10:34:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-02-uk-plans-to-revamp-national-cyber-defense-tools-are-already-in-motion

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/02/uk_ncscs_plans_to_revamp/){:target="_blank" rel="noopener"}

> Work aims to build on the success of NCSC's 2016 initiative – and private sector will play a part The UK's National Cyber Security Centre (NCSC) says it's in the planning stages of bringing a new suite of services to its existing Active Cyber Defence (ACD) program.... [...]
