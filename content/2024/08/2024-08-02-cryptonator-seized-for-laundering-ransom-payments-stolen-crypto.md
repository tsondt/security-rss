Title: Cryptonator seized for laundering ransom payments, stolen crypto
Date: 2024-08-02T13:27:25-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: CryptoCurrency;Legal;Security
Slug: 2024-08-02-cryptonator-seized-for-laundering-ransom-payments-stolen-crypto

[Source](https://www.bleepingcomputer.com/news/cryptocurrency/cryptonator-seized-for-laundering-ransom-payments-stolen-crypto/){:target="_blank" rel="noopener"}

> U.S. and German law enforcement seized the domain of the crypto wallet platform Cryptonator, used by ransomware gangs, darknet marketplaces, and other illicit services, and indicted its operator. [...]
