Title: Police take just 2 days to recover $40M stolen in business email scam
Date: 2024-08-07T11:35:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-07-police-take-just-2-days-to-recover-40m-stolen-in-business-email-scam

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/07/police_take_just_two_days/){:target="_blank" rel="noopener"}

> Timor-Leste is a known cybercrime hotspot Two days is all it took for Interpol to recover more than $40 million worth of stolen funds in a recent business email compromise (BEC) heist, the international cop shop said this week.... [...]
