Title: macOS Sequoia brings better Gatekeeper, stalkerware protections
Date: 2024-08-07T12:59:37-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple;Security
Slug: 2024-08-07-macos-sequoia-brings-better-gatekeeper-stalkerware-protections

[Source](https://www.bleepingcomputer.com/news/apple/macos-sequoia-brings-better-gatekeeper-stalkerware-protections/){:target="_blank" rel="noopener"}

> Apple's macOS Sequoia, now in beta testing, will make it harder to bypass Gatekeeper warnings and add system alerts for potential stalkerware threats. [...]
