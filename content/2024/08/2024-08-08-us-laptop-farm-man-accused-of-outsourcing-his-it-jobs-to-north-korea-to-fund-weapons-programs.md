Title: US 'laptop farm' man accused of outsourcing his IT jobs to North Korea to fund weapons programs
Date: 2024-08-08T20:55:20+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-08-08-us-laptop-farm-man-accused-of-outsourcing-his-it-jobs-to-north-korea-to-fund-weapons-programs

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/08/north_korea_laptop_farm_arrest/){:target="_blank" rel="noopener"}

> American and Brit firms thought they were employing a Westerner, but not so, it's alleged The FBI today arrested a Tennessee man suspected of running a "laptop farm" that got North Koreans, posing as Westerners, IT jobs at American and British companies.... [...]
