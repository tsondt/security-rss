Title: Hacker wipes 13,000 devices after breaching classroom management platform
Date: 2024-08-06T10:15:25-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Education
Slug: 2024-08-06-hacker-wipes-13000-devices-after-breaching-classroom-management-platform

[Source](https://www.bleepingcomputer.com/news/security/hacker-wipes-13-000-devices-after-breaching-classroom-management-platform/){:target="_blank" rel="noopener"}

> A hacker has breached Mobile Guardian, a digital classroom management platform used worldwide, and remotely wiped data from at least 13,000 student's iPads and Chromebooks. [...]
