Title: Using 1Password on Mac? Patch up if you don’t want your Vaults raided
Date: 2024-08-08T13:45:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-08-using-1password-on-mac-patch-up-if-you-dont-want-your-vaults-raided

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/08/using_1password_on_mac_patch/){:target="_blank" rel="noopener"}

> Hundreds of thousands of users potentially vulnerable Password manager 1Password is warning that all Mac users running versions before 8.10.36 are vulnerable to a bug that allows attackers to steal vault items.... [...]
