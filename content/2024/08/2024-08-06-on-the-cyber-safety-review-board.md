Title: On the Cyber Safety Review Board
Date: 2024-08-06T11:01:04+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;computer security;cybersecurity;hacking
Slug: 2024-08-06-on-the-cyber-safety-review-board

[Source](https://www.schneier.com/blog/archives/2024/08/a-better-investigatory-board-for-cyber-incidents.html){:target="_blank" rel="noopener"}

> When an airplane crashes, impartial investigatory bodies leap into action, empowered by law to unearth what happened and why. But there is no such empowered and impartial body to investigate CrowdStrike’s faulty update that recently unfolded, ensnarling banks, airlines, and emergency services to the tune of billions of dollars. We need one. To be sure, there is the White House’s Cyber Safety Review Board. On March 20, the CSRB released a report into last summer’s intrusion by a Chinese hacking group into Microsoft’s cloud environment, where it compromised the U.S. Department of Commerce, State Department, congressional offices, and several associated [...]
