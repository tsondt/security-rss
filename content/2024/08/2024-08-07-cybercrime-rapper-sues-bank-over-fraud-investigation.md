Title: Cybercrime Rapper Sues Bank over Fraud Investigation
Date: 2024-08-07T19:01:49+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;859-963-6243;constella.ai;Devon Turner;DomainTools.com;Punchmade Dev
Slug: 2024-08-07-cybercrime-rapper-sues-bank-over-fraud-investigation

[Source](https://krebsonsecurity.com/2024/08/cybercrime-rapper-sues-bank-over-fraud-investigation/){:target="_blank" rel="noopener"}

> A partial selfie posted by Punchmade Dev to his Twitter account. Yes, that is a functioning handheld card skimming device, encrusted in diamonds. Underneath that are more medallions, including a diamond-studded bitcoin and payment card. In January, KrebsOnSecurity wrote about rapper Punchmade Dev, whose music videos sing the praises of a cybercrime lifestyle. That story showed how Punchmade’s social media profiles promoted Punchmade-themed online stores selling bank account and payment card data. The subject of that piece, a 22-year-old Kentucky man, is now brazenly suing his financial institution after it blocked a $75,000 wire transfer and froze his account, citing [...]
