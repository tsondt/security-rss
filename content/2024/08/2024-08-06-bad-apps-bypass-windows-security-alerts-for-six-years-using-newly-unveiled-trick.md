Title: Bad apps bypass Windows security alerts for six years using newly unveiled trick
Date: 2024-08-06T14:41:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-06-bad-apps-bypass-windows-security-alerts-for-six-years-using-newly-unveiled-trick

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/06/bad_apps_bypass_windows_security/){:target="_blank" rel="noopener"}

> Windows SmartScreen and Smart App Control both have weaknesses of which to be wary Elastic Security Labs has lifted the lid on a slew of methods available to attackers who want to run malicious apps without triggering Windows' security warnings, including one in use for six years.... [...]
