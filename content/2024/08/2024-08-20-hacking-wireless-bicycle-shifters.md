Title: Hacking Wireless Bicycle Shifters
Date: 2024-08-20T11:08:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;firmware;hacking;Internet of Things;patching
Slug: 2024-08-20-hacking-wireless-bicycle-shifters

[Source](https://www.schneier.com/blog/archives/2024/08/hacking-wireless-bicycle-shifters.html){:target="_blank" rel="noopener"}

> This is yet another insecure Internet-of-things story, this one about wireless gear shifters for bicycles. These gear shifters are used in big-money professional bicycle races like the Tour de France, which provides an incentive to actually implement this attack. Research paper. Another news story. Slashdot thread. [...]
