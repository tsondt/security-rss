Title: Feds bust minor league Radar/Dispossessor ransomware gang
Date: 2024-08-13T15:23:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-13-feds-bust-minor-league-radardispossessor-ransomware-gang

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/13/feds_bust_minor_league_radardispossessor/){:target="_blank" rel="noopener"}

> The takedown may be small but any ransomware gang sent to the shops is good news in our book The Dispossessor ransomware group is the latest to enter the cybercrime graveyard with the Feds proudly laying claim to the takedown.... [...]
