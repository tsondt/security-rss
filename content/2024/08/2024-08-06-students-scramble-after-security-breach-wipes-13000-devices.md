Title: Students scramble after security breach wipes 13,000 devices
Date: 2024-08-06T21:26:03+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;device manager;mobile;mobile guardian;security breaches
Slug: 2024-08-06-students-scramble-after-security-breach-wipes-13000-devices

[Source](https://arstechnica.com/?p=2041407){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Students in Singapore are scrambling after a security breach wiped notes and all other data from school-issued iPads and Chromebooks running the mobile device management app Mobile Guardian. According to news reports, the mass wiping came as a shock to multiple students in Singapore, where the Mobile Guardian app has been the country’s official mobile device management provider for public schools since 2020. Singapore’s Ministry of Education said Monday that roughly 13,000 students from 26 secondary schools had their devices wiped remotely in the incident. The agency said it will remove the Mobile Guardian from all [...]
