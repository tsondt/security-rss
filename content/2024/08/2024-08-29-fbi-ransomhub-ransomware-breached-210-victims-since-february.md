Title: FBI: RansomHub ransomware breached 210 victims since February
Date: 2024-08-29T14:48:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-08-29-fbi-ransomhub-ransomware-breached-210-victims-since-february

[Source](https://www.bleepingcomputer.com/news/security/fbi-ransomhub-ransomware-breached-210-victims-since-february/){:target="_blank" rel="noopener"}

> ​Since surfacing in February 2024, RansomHub ransomware affiliates have breached over 200 victims from a wide range of critical U.S. infrastructure sectors. [...]
