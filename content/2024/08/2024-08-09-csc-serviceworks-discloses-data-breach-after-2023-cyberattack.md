Title: CSC ServiceWorks discloses data breach after 2023 cyberattack
Date: 2024-08-09T13:56:43-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-08-09-csc-serviceworks-discloses-data-breach-after-2023-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/csc-serviceworks-discloses-data-breach-after-2023-cyberattack/){:target="_blank" rel="noopener"}

> ​CSC ServiceWorks, a leading provider of commercial laundry services, has disclosed a data breach after the personal information of an undisclosed number of individuals was exposed in a 2023 cyberattack. [...]
