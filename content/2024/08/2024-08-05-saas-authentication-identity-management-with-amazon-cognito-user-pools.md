Title: SaaS authentication: Identity management with Amazon Cognito user pools
Date: 2024-08-05T16:29:42+00:00
Author: Shubhankar Sumar
Category: AWS Security
Tags: Amazon Cognito;Intermediate (200);Security, Identity, & Compliance;Technical How-to;SaaS;Security;Security Blog;Security token service;Tenant isolation
Slug: 2024-08-05-saas-authentication-identity-management-with-amazon-cognito-user-pools

[Source](https://aws.amazon.com/blogs/security/saas-authentication-identity-management-with-amazon-cognito-user-pools/){:target="_blank" rel="noopener"}

> Amazon Cognito is a customer identity and access management (CIAM) service that can scale to millions of users. Although the Cognito documentation details which multi-tenancy models are available, determining when to use each model can sometimes be challenging. In this blog post, we’ll provide guidance on when to use each model and review their pros [...]
