Title: Chinese hacking groups target Russian government, IT firms
Date: 2024-08-11T12:16:28-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-11-chinese-hacking-groups-target-russian-government-it-firms

[Source](https://www.bleepingcomputer.com/news/security/chinese-hacking-groups-target-russian-government-it-firms/){:target="_blank" rel="noopener"}

> A series of targeted cyberattacks that started at the end of July 2024, targeting dozens of systems used in Russian government organizations and IT companies, are linked to Chinese hackers of the APT31 and APT 27 groups. [...]
