Title: Toyota confirms breach after stolen data leaks on hacking forum
Date: 2024-08-19T16:51:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-08-19-toyota-confirms-breach-after-stolen-data-leaks-on-hacking-forum

[Source](https://www.bleepingcomputer.com/news/security/toyota-confirms-breach-after-stolen-data-leaks-on-hacking-forum/){:target="_blank" rel="noopener"}

> Toyota confirmed that its network was breached after a threat actor leaked an archive of 240GB of data stolen from the company's systems on a hacking forum. [...]
