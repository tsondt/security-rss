Title: Microsoft 365 anti-phishing feature can be bypassed with CSS
Date: 2024-08-07T01:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-08-07-microsoft-365-anti-phishing-feature-can-be-bypassed-with-css

[Source](https://www.bleepingcomputer.com/news/security/microsoft-365-anti-phishing-feature-can-be-bypassed-with-css/){:target="_blank" rel="noopener"}

> Researchers have demonstrated a method to bypass an anti-phishing measure in Microsoft 365 (formerly Office 365), elevating the risk of users opening malicious emails.` [...]
