Title: Alleged Karakut ransomware scumbag charged in US
Date: 2024-08-26T02:00:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-26-alleged-karakut-ransomware-scumbag-charged-in-us

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/26/karakut_ransomware_scum_charged/){:target="_blank" rel="noopener"}

> Plus: Microsoft issues workaround for dual-boot crashes; ARRL cops to ransom payment, and more Infosec in brief Deniss Zolotarjovs, a suspected member of the Russian Karakurt ransomware gang, has been charged in a US court with allegedly conspiring to commit money laundering, wire fraud and Hobbs Act extortion.... [...]
