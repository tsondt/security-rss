Title: Microsoft patches scary wormable hijack-my-box-via-IPv6 security bug and others
Date: 2024-08-14T00:45:21+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-14-microsoft-patches-scary-wormable-hijack-my-box-via-ipv6-security-bug-and-others

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/14/august_patch_tuesday_ipv6/){:target="_blank" rel="noopener"}

> Plus more pain for Intel which fixed 43 bugs, SAP and Adobe also in on the action Patch Tuesday Microsoft has disclosed 90 flaws in its products – six of which have already been exploited – and four others that are listed as publicly known.... [...]
