Title: Delta: CrowdStrike's offer to help in Falcon meltdown was too little, too late
Date: 2024-08-08T22:30:22+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-08-delta-crowdstrikes-offer-to-help-in-falcon-meltdown-was-too-little-too-late

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/08/delta_crowdstrikes_offer_for_help/){:target="_blank" rel="noopener"}

> Airline unimpressed with 'unhelpful and untimely' phone call from CEO, Falcon maker says claims untrue Delta Air Lines has come out swinging at CrowdStrike in a letter accusing the security giant of trying to "shift the blame" for the IT meltdown caused by its software – and that CrowdStrike CEO George Kurtz's offer of support was too little, too late.... [...]
