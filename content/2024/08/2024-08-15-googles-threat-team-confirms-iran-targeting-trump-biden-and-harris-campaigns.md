Title: Google’s threat team confirms Iran targeting Trump, Biden, and Harris campaigns
Date: 2024-08-15T18:26:50+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;Security;APT42;Donald Trump;gmail;google;google tag;Iran;Kamala Harris;phishing;President Biden;Roger Stone;spearphishing;threat analysis group
Slug: 2024-08-15-googles-threat-team-confirms-iran-targeting-trump-biden-and-harris-campaigns

[Source](https://arstechnica.com/?p=2043545){:target="_blank" rel="noopener"}

> Enlarge / Roger Stone, former adviser to Donald Trump's presidential campaign, center, during the Republican National Convention (RNC) in Milwaukee on July 17, 2024. (credit: Getty Images) Google's Threat Analysis Group confirmed Wednesday that they observed a threat actor backed by the Iranian government targeting Google accounts associated with US presidential campaigns, in addition to stepped-up attacks on Israeli targets. APT42, associated with Iran's Islamic Revolutionary Guard Corps, "consistently targets high-profile users in Israel and the US," the Threat Analysis Group (TAG) writes. The Iranian group uses hosted malware, phishing pages, malicious redirects, and other tactics to gain access to [...]
