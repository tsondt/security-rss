Title: Deadbeat dad faked his own death by hacking government databases
Date: 2024-08-21T07:35:15+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-08-21-deadbeat-dad-faked-his-own-death-by-hacking-government-databases

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/21/man_jailed_faking_death_online/){:target="_blank" rel="noopener"}

> Hoped to dodge child support payments, now faces 81 months inside – and a bigger bill than ever A US man has been sentenced to 81 months in jail for faking his own death by hacking government systems and officially marking himself as deceased.... [...]
