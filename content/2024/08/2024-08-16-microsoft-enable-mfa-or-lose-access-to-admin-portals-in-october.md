Title: Microsoft: Enable MFA or lose access to admin portals in October
Date: 2024-08-16T15:06:38-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-08-16-microsoft-enable-mfa-or-lose-access-to-admin-portals-in-october

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-enable-mfa-or-lose-access-to-admin-portals-in-october/){:target="_blank" rel="noopener"}

> Microsoft warned Entra global admins on Thursday to enable multi-factor authentication (MFA) for their tenants until October 15 to ensure users don't lose access to admin portals. [...]
