Title: Feds claim sinister sysadmin locked up thousands of Windows workstations, demanded ransom
Date: 2024-08-29T18:30:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-29-feds-claim-sinister-sysadmin-locked-up-thousands-of-windows-workstations-demanded-ransom

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/29/vm_engineer_extortion_allegations/){:target="_blank" rel="noopener"}

> Sordid search history 'evidence' in case that could see him spend 35 years for extortion and wire fraud A former infrastructure engineer who allegedly locked IT department colleagues out of their employer's systems, then threatened to shut down servers unless paid a ransom, has been arrested and charged after an FBI investigation.... [...]
