Title: Who uses LLM prompt injection attacks IRL? Mostly unscrupulous job seekers, jokesters and trolls
Date: 2024-08-13T10:46:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-13-who-uses-llm-prompt-injection-attacks-irl-mostly-unscrupulous-job-seekers-jokesters-and-trolls

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/13/who_uses_llm_prompt_injection/){:target="_blank" rel="noopener"}

> Because apps talking like pirates and creating ASCII art never gets old Despite worries about criminals using prompt injection to trick large language models (LLMs) into leaking sensitive data or performing other destructive actions, most of these types of AI shenanigans come from job seekers trying to get their resumes past automated HR screeners – and people protesting generative AI for various reasons, according to Russian security biz Kaspersky.... [...]
