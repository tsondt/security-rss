Title: Entrust faces years of groveling to regain browsers' trust, say rival chiefs
Date: 2024-08-08T08:33:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-08-entrust-faces-years-of-groveling-to-regain-browsers-trust-say-rival-chiefs

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/08/entrust_faces_years_of_groveling/){:target="_blank" rel="noopener"}

> Sectigo bosses claim it's only a matter of time before Microsoft and Apple drop Big E from their root stores too After falling down in the estimations of major browser makers Google and Mozilla, Entrust faces a lengthy fight on its hands to regain industry trust and once more issue trusted TLS certificates.... [...]
