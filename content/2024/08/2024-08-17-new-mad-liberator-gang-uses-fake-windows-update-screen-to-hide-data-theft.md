Title: New Mad Liberator gang uses fake Windows update screen to hide data theft
Date: 2024-08-17T10:33:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-17-new-mad-liberator-gang-uses-fake-windows-update-screen-to-hide-data-theft

[Source](https://www.bleepingcomputer.com/news/security/new-mad-liberator-gang-uses-fake-windows-update-screen-to-hide-data-theft/){:target="_blank" rel="noopener"}

> A new data extortion group tracked as Mad Liberator is targeting AnyDesk users and runs a fake Microsoft Windows update screen to distract while exfiltrating data from the target device. [...]
