Title: New Patent Application for Car-to-Car Surveillance
Date: 2024-08-05T11:07:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;patents;police
Slug: 2024-08-05-new-patent-application-for-car-to-car-surveillance

[Source](https://www.schneier.com/blog/archives/2024/08/new-patent-application-for-car-to-car-surveillance.html){:target="_blank" rel="noopener"}

> Ford has a new patent application for a system where cars monitor each other’s speeds, and then report then to some central authority. Slashdot thread. [...]
