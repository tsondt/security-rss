Title: Countdown is on: Last chance for discount registration at Mandiant’s mWISE 2024
Date: 2024-08-05T10:02:04-04:00
Author: Sponsored by mWISE 2024
Category: BleepingComputer
Tags: Security
Slug: 2024-08-05-countdown-is-on-last-chance-for-discount-registration-at-mandiants-mwise-2024

[Source](https://www.bleepingcomputer.com/news/security/countdown-is-on-last-chance-for-discount-registration-at-mandiants-mwise-2024/){:target="_blank" rel="noopener"}

> There is only a few days left to get $300 off the standard conference price at mWISE. Learn more from mWise 2024 about how to get the discount and the upcoming cybersecurity sessions. [...]
