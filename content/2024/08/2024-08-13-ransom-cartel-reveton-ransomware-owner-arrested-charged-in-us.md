Title: Ransom Cartel, Reveton ransomware owner arrested, charged in US
Date: 2024-08-13T09:33:37-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-08-13-ransom-cartel-reveton-ransomware-owner-arrested-charged-in-us

[Source](https://www.bleepingcomputer.com/news/security/ransom-cartel-reveton-ransomware-owner-arrested-charged-in-us/){:target="_blank" rel="noopener"}

> Belarusian-Ukrainian national Maksim Silnikau was arrested in Spain and is now extradited to the USA to face charges for creating the Ransom Cartel ransomware operation in 2021 and running a malvertising operation from 2013 to 2022. [...]
