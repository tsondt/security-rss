Title: Woman uses AirTags to nab alleged parcel-pinching scum
Date: 2024-08-28T07:30:15+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-08-28-woman-uses-airtags-to-nab-alleged-parcel-pinching-scum

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/28/airtag_mail_arrests/){:target="_blank" rel="noopener"}

> Phew! Consumer-grade tracking devices are good for more than finding your keys and stalking Theft of packages is an ongoing problem, so one California woman tried a high tech solution to the problem – and her use of Apple’s consumer-grade AirTags tracking devices led to two arrests.... [...]
