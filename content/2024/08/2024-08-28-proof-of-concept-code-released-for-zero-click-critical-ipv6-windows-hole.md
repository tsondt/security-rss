Title: Proof-of-concept code released for zero-click critical IPv6 Windows hole
Date: 2024-08-28T21:20:12+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-08-28-proof-of-concept-code-released-for-zero-click-critical-ipv6-windows-hole

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/28/proofofconcept_code_released_for_zeroclick/){:target="_blank" rel="noopener"}

> If you haven't deployed August's patches, get busy before others do Windows users who haven't yet installed the latest fixes to their operating systems will need to get a move on, as code now exists to exploit a critical Microsoft vulnerability announced by Redmond two weeks ago.... [...]
