Title: Hackers linked to $14M Holograph crypto heist arrested in Italy
Date: 2024-08-19T12:25:52-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;CryptoCurrency;Security
Slug: 2024-08-19-hackers-linked-to-14m-holograph-crypto-heist-arrested-in-italy

[Source](https://www.bleepingcomputer.com/news/legal/hackers-linked-to-14m-holograph-crypto-heist-arrested-in-italy/){:target="_blank" rel="noopener"}

> Suspected hackers behind the heist of $14,000,000 worth of cryptocurrency from blockchain tech firm Holograph were arrested in Italy after living a lavish lifestyle for weeks in the country. [...]
