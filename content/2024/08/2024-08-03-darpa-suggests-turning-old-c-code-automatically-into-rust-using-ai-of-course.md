Title: DARPA suggests turning old C code automatically into Rust – using AI, of course
Date: 2024-08-03T10:03:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-08-03-darpa-suggests-turning-old-c-code-automatically-into-rust-using-ai-of-course

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/03/darpa_c_to_rust/){:target="_blank" rel="noopener"}

> Who wants to make a TRACTOR pull request? To accelerate the transition to memory safe programming languages, the US Defense Advanced Research Projects Agency (DARPA) is driving the development of TRACTOR, a programmatic code conversion vehicle.... [...]
