Title: 'Digital arrest' scams are big in India and may be spreading
Date: 2024-08-13T05:37:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-08-13-digital-arrest-scams-are-big-in-india-and-may-be-spreading

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/13/india_digital_arrest_scams/){:target="_blank" rel="noopener"}

> Bad guys claim they're cops, keep you on hold for hours until you pay to make loved ones' crimes go away A woman in the Indian city of Delhi last week found herself under "digital arrest" – a form of scam in which victims make payments to criminals posing as law enforcement officers.... [...]
