Title: Encryption in transit over external networks: AWS guidance for NYDFS and beyond
Date: 2024-08-21T15:23:24+00:00
Author: Aravind Gopaluni
Category: AWS Security
Tags: Best Practices;Financial Services;Industries;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Thought Leadership;AWS Certificate Manager;AWS Direct Connect;AWS Nitro System;client-side encryption;Encryption;Security Blog;TLS
Slug: 2024-08-21-encryption-in-transit-over-external-networks-aws-guidance-for-nydfs-and-beyond

[Source](https://aws.amazon.com/blogs/security/encryption-in-transit-over-external-networks-aws-guidance-for-nydfs-and-beyond/){:target="_blank" rel="noopener"}

> On November 1, 2023, the New York State Department of Financial Services (NYDFS) issued its Second Amendment (the Amendment) to its Cybersecurity Requirements for Financial Services Companies adopted in 2017, published within Section 500 of 23 NYCRR 500 (the Cybersecurity Requirements; the Cybersecurity Requirements as amended by the Amendment, the Amended Cybersecurity Requirements). In the introduction [...]
