Title: National Public Data tells officials 'only' 1.3M people affected by intrusion
Date: 2024-08-19T13:15:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-19-national-public-data-tells-officials-only-13m-people-affected-by-intrusion

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/19/national_public_data_breach/){:target="_blank" rel="noopener"}

> Investigators previously said the number was much, much higher The data broker at the center of what may become one of the more significant breaches of the year is telling officials that just 1.3 million people were affected.... [...]
