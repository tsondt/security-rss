Title: Ransomware batters critical industries, but takedowns hint at relief
Date: 2024-08-22T12:23:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-22-ransomware-batters-critical-industries-but-takedowns-hint-at-relief

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/22/critical_industrial_ransomware/){:target="_blank" rel="noopener"}

> Whether attack slowdown continues downward trend is the million dollar question that security researchers can't answer Critical industrial organizations continued to be hammered by ransomware skids in July, while experts suggest the perps are growing in confidence that law enforcement won't intervene.... [...]
