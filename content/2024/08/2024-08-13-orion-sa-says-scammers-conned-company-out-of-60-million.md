Title: Orion SA says scammers conned company out of $60 million
Date: 2024-08-13T11:27:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-13-orion-sa-says-scammers-conned-company-out-of-60-million

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/13/orion_sa_says_scammers_conned/){:target="_blank" rel="noopener"}

> Incident sounds like a BEC fraud targeting an unwitting staffer Luxembourg-based chemicals and manufacturing giant Orion SA is telling US regulators that it will lose out on around $60 million after it was targeted by a criminal wire fraud scheme.... [...]
