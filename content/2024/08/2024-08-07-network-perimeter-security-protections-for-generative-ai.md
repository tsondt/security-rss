Title: Network perimeter security protections for generative AI
Date: 2024-08-07T13:38:37+00:00
Author: Riggs Goodman III
Category: AWS Security
Tags: Amazon Bedrock;AWS Shield;AWS WAF;Best Practices;Generative AI;Intermediate (200);Security, Identity, & Compliance;authorization;Security Blog
Slug: 2024-08-07-network-perimeter-security-protections-for-generative-ai

[Source](https://aws.amazon.com/blogs/security/network-perimeter-security-protections-for-generative-ai/){:target="_blank" rel="noopener"}

> Generative AI–based applications have grown in popularity in the last couple of years. Applications built with large language models (LLMs) have the potential to increase the value companies bring to their customers. In this blog post, we dive deep into network perimeter protection for generative AI applications. We’ll walk through the different areas of network [...]
