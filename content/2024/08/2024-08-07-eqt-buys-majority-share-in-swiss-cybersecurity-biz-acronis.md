Title: EQT buys majority share in Swiss cybersecurity biz Acronis
Date: 2024-08-07T10:06:29+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-08-07-eqt-buys-majority-share-in-swiss-cybersecurity-biz-acronis

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/07/eqt_buys_shares_acronis/){:target="_blank" rel="noopener"}

> Went at equivalent of $3.5B+ valuation for entire firm, though portion sold not specified Acronis, the Swiss disaster recovery turned cybersecurity firm and catch-all for managed service providers, has been majority acquired by Europe’s largest private equity firm, EQT.... [...]
