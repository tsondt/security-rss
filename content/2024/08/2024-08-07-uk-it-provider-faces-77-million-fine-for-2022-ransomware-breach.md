Title: UK IT provider faces $7.7 million fine for 2022 ransomware breach
Date: 2024-08-07T13:14:27-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-08-07-uk-it-provider-faces-77-million-fine-for-2022-ransomware-breach

[Source](https://www.bleepingcomputer.com/news/security/uk-it-provider-faces-77-million-fine-for-2022-ransomware-breach/){:target="_blank" rel="noopener"}

> The UK's Information Commissioner's Office (ICO) has announced a provisional decision to impose a fine of £6.09M ($7.74 million) on Advanced Computer Software Group Ltd (Advanced) for its failure to protect the personal information of tens of thousands when it was hit by ransomware in 2022. [...]
