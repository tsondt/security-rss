Title: Local Networks Go Global When Domain Names Collide
Date: 2024-08-23T14:12:31+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;The Coming Storm;Web Fraud 2.0;Active Directory;Andorra;DNS name devolution;Memphis Real-Time Crime Center;memrtcc.ad;Mike Barlow;Mike O'Connor;namespace collision;Philippe Caturegli;Seralys;Web Proxy Auto-Discovery Protocol;wpad.ad;wpad.dk
Slug: 2024-08-23-local-networks-go-global-when-domain-names-collide

[Source](https://krebsonsecurity.com/2024/08/local-networks-go-global-when-domain-names-collide/){:target="_blank" rel="noopener"}

> The proliferation of new top-level domains (TLDs) has exacerbated a well-known security weakness: Many organizations set up their internal Microsoft authentication systems years ago using domain names in TLDs that didn’t exist at the time. Meaning, they are continuously sending their Windows usernames and passwords to domain names they do not control and which are freely available for anyone to register. Here’s a look at one security researcher’s efforts to map and shrink the size of this insidious problem. At issue is a well-known security and privacy threat called “ namespace collision,” a situation where domain names intended to be [...]
