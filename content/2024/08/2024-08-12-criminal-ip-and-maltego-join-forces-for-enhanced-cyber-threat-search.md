Title: Criminal IP and Maltego Join Forces for Enhanced Cyber Threat Search
Date: 2024-08-12T10:02:04-04:00
Author: Sponsored by Criminal IP
Category: BleepingComputer
Tags: Security
Slug: 2024-08-12-criminal-ip-and-maltego-join-forces-for-enhanced-cyber-threat-search

[Source](https://www.bleepingcomputer.com/news/security/criminal-ip-and-maltego-join-forces-for-enhanced-cyber-threat-search/){:target="_blank" rel="noopener"}

> AI SPERA announced today that its IP address intelligence engine, Criminal IP, can now be integrated with Maltego's unified user interface and is available on the Maltego's marketplace, [...]
