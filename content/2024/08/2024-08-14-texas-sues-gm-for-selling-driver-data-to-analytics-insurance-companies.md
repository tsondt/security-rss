Title: Texas sues GM for selling driver data to analytics, insurance companies
Date: 2024-08-14T18:06:14+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-08-14-texas-sues-gm-for-selling-driver-data-to-analytics-insurance-companies

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/14/texas_sues_general_motors/){:target="_blank" rel="noopener"}

> Lone Star State alleges GM cashed in with "millions in lump sum payments" from the sale Texas has sued General Motors for what it said is a years-long scheme to collect and sell drivers' data to third parties - including insurance companies - without their knowledge or consent.... [...]
