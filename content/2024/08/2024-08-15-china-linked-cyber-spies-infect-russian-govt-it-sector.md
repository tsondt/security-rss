Title: China-linked cyber-spies infect Russian govt, IT sector
Date: 2024-08-15T02:50:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-15-china-linked-cyber-spies-infect-russian-govt-it-sector

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/15/suspected_chinese_attackers_hacked_russia/){:target="_blank" rel="noopener"}

> No, no, go ahead, don't let us stop you, Xi Cyber-spies suspected of connections with China have infected "dozens" of computers belonging to Russian government agencies and IT providers with backdoors and trojans since late July, according to Kaspersky.... [...]
