Title: Australian gold producer Evolution Mining hit by ransomware
Date: 2024-08-12T14:02:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-12-australian-gold-producer-evolution-mining-hit-by-ransomware

[Source](https://www.bleepingcomputer.com/news/security/australian-gold-producer-evolution-mining-hit-by-ransomware/){:target="_blank" rel="noopener"}

> Evolution Mining has informed that it has been targeted by a ransomware attack on August 8, 2024, which impacted its IT systems. [...]
