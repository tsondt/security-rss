Title: After nearly 3B personal records leak online, Florida data broker confirms it was ransacked by cyber-thieves
Date: 2024-08-16T20:45:11+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: 2024-08-16-after-nearly-3b-personal-records-leak-online-florida-data-broker-confirms-it-was-ransacked-by-cyber-thieves

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/16/national_public_data_theft/){:target="_blank" rel="noopener"}

> Names, addresses, Social Security numbers, more all out there A Florida firm has all but confirmed that millions of people's sensitive personal info was stolen from it by cybercriminals and publicly leaked.... [...]
