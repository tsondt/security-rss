Title: Fortune 50 biz coughed up record-breaking $75M ransom to halt leak of stolen data
Date: 2024-08-02T12:03:07+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-08-02-fortune-50-biz-coughed-up-record-breaking-75m-ransom-to-halt-leak-of-stolen-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/02/dark_angels_ransomware/){:target="_blank" rel="noopener"}

> They say crime doesn't pay. They're right – it's the victims doing the paying An unnamed Fortune 50 corporation paid a stonking $75 million to a ransomware gang to stop it leaking terabytes of stolen data.... [...]
