Title: Indian telcos to cut off scammy, spammy, telemarketers for two whole years
Date: 2024-08-14T06:29:14+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-08-14-indian-telcos-to-cut-off-scammy-spammy-telemarketers-for-two-whole-years

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/14/indian_telco_telemarketer_blacklist/){:target="_blank" rel="noopener"}

> There's a blockchain involved so it's totally going to stop you getting those calls India’s Telecom Regulatory Authority (TRAI) on Tuesday directed telcos to stop calls from unregistered telemarketers – and prevent them from using networks again for up to two years – as part of an effort to curb spam and scams.... [...]
