Title: Cencora confirms patient health info stolen in February attack
Date: 2024-08-01T12:30:10-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-08-01-cencora-confirms-patient-health-info-stolen-in-february-attack

[Source](https://www.bleepingcomputer.com/news/security/cencora-confirms-patient-health-info-stolen-in-february-attack/){:target="_blank" rel="noopener"}

> Pharmaceutical giant Cencora has confirmed that patients' protected health information and personally identifiable information (PII) was exposed in a February cyberattack. [...]
