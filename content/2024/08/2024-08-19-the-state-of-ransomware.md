Title: The State of Ransomware
Date: 2024-08-19T11:05:01+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;ransomware;reports
Slug: 2024-08-19-the-state-of-ransomware

[Source](https://www.schneier.com/blog/archives/2024/08/the-state-of-ransomware.html){:target="_blank" rel="noopener"}

> Palo Alto Networks published its semi-annual report on ransomware. From the Executive Summary: Unit 42 monitors ransomware and extortion leak sites closely to keep tabs on threat activity. We reviewed compromise announcements from 53 dedicated leak sites in the first half of 2024 and found 1,762 new posts. This averages to approximately 294 posts a month and almost 68 posts a week. Of the 53 ransomware groups whose leak sites we monitored, six of the groups accounted for more than half of the compromises observed. In February, we reported a 49% increase year-over-year in alleged victims posted on ransomware leak [...]
