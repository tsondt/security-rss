Title: 3AM ransomware stole data of 464,000 Kootenai Health patients
Date: 2024-08-13T11:23:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-08-13-3am-ransomware-stole-data-of-464000-kootenai-health-patients

[Source](https://www.bleepingcomputer.com/news/security/3am-ransomware-stole-data-of-464-000-kootenai-health-patients/){:target="_blank" rel="noopener"}

> Kootenai Health has disclosed a data breach impacting over 464,000 patients after their personal information was stolen and leaked by the 3AM ransomware operation. [...]
