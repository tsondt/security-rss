Title: How AWS tracks the cloud’s biggest security threats and helps shut them down
Date: 2024-08-05T13:00:04+00:00
Author: CJ Moses
Category: AWS Security
Tags: Amazon GuardDuty;Foundational (100);Security, Identity, & Compliance;Thought Leadership;Security;Security Blog
Slug: 2024-08-05-how-aws-tracks-the-clouds-biggest-security-threats-and-helps-shut-them-down

[Source](https://aws.amazon.com/blogs/security/how-aws-tracks-the-clouds-biggest-security-threats-and-helps-shut-them-down/){:target="_blank" rel="noopener"}

> Threat intelligence that can fend off security threats before they happen requires not just smarts, but the speed and worldwide scale that only AWS can offer. Organizations around the world trust Amazon Web Services (AWS) with their most sensitive data. One of the ways we help secure data on AWS is with an industry-leading threat [...]
