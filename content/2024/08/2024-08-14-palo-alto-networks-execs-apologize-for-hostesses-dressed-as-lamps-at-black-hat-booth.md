Title: Palo Alto Networks execs apologize for 'hostesses' dressed as lamps at Black Hat booth
Date: 2024-08-14T14:00:17+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-14-palo-alto-networks-execs-apologize-for-hostesses-dressed-as-lamps-at-black-hat-booth

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/14/palo_alto_networks_execs_apologize/){:target="_blank" rel="noopener"}

> Biz admits turning human women into faceless, sexualized furniture was a 'tone deaf' marketing ploy If you attended the Black Hat conference in Vegas last week and found yourself over in Palo Alto Networks' corner of the event, you may have encountered a marketing gimmick that has since been heavily criticized for misogyny.... [...]
