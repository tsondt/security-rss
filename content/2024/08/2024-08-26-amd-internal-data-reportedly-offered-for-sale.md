Title: AMD internal data reportedly offered for sale
Date: 2024-08-26T16:45:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-26-amd-internal-data-reportedly-offered-for-sale

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/26/amd_internal_data_intelbroker/){:target="_blank" rel="noopener"}

> Second sensitive info theft claimed by the same crims since June Digital data thieves have reportedly breached AMD's internal communications and are offering the allegedly stolen goods for sale.... [...]
