Title: US sues TikTok for violating children privacy protection laws
Date: 2024-08-02T16:49:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-08-02-us-sues-tiktok-for-violating-children-privacy-protection-laws

[Source](https://www.bleepingcomputer.com/news/security/us-sues-tiktok-for-violating-children-privacy-protection-laws/){:target="_blank" rel="noopener"}

> ​The U.S. Department of Justice has filed a lawsuit against social media platform TikTok and its parent company, ByteDance, alleging widespread violations of children's privacy laws. [...]
