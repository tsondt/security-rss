Title: SolarWinds left critical hardcoded credentials in its Web Help Desk product
Date: 2024-08-22T22:36:16+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-22-solarwinds-left-critical-hardcoded-credentials-in-its-web-help-desk-product

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/22/hardcoded_credentials_bug_solarwinds_whd/){:target="_blank" rel="noopener"}

> Why go to the effort of backdooring code when devs will basically do it for you accidentally anyway SolarWinds left hardcoded credentials in its Web Help Desk product that can be used by remote, unauthenticated attackers to log into vulnerable instances, access internal functionality, and modify sensitive data... [...]
