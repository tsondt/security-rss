Title: Friday Squid Blogging: SQUID Is a New Computational Tool for Analyzing Genomic AI
Date: 2024-08-09T19:04:37+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;squid
Slug: 2024-08-09-friday-squid-blogging-squid-is-a-new-computational-tool-for-analyzing-genomic-ai

[Source](https://www.schneier.com/blog/archives/2024/08/friday-squid-blogging-squid-is-a-new-computational-tool-for-analyzing-genomic-ai.html){:target="_blank" rel="noopener"}

> Yet another SQUID acronym : SQUID, short for Surrogate Quantitative Interpretability for Deepnets, is a computational tool created by Cold Spring Harbor Laboratory (CSHL) scientists. It’s designed to help interpret how AI models analyze the genome. Compared with other analysis tools, SQUID is more consistent, reduces background noise, and can lead to more accurate predictions about the effects of genetic mutations. Blog moderation policy. [...]
