Title: Problems with Georgia’s Voter Registration Portal
Date: 2024-08-07T11:10:00+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;fraud;Georgia;identification;national security policy;voting
Slug: 2024-08-07-problems-with-georgias-voter-registration-portal

[Source](https://www.schneier.com/blog/archives/2024/08/problems-with-georgias-voter-registration-portal.html){:target="_blank" rel="noopener"}

> It’s possible to cancel other people’s voter registration: On Friday, four days after Georgia Democrats began warning that bad actors could abuse the state’s new online portal for canceling voter registrations, the Secretary of State’s Office acknowledged to ProPublica that it had identified multiple such attempts......the portal suffered at least two security glitches that briefly exposed voters’ dates of birth, the last four digits of their Social Security numbers and their full driver’s license numbers—the exact information needed to cancel others’ voter registrations. I get that this is a hard problem to solve. We want the portal to be easy [...]
