Title: Linux kernel impacted by new SLUBStick cross-cache attack
Date: 2024-08-03T11:17:28-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-03-linux-kernel-impacted-by-new-slubstick-cross-cache-attack

[Source](https://www.bleepingcomputer.com/news/security/linux-kernel-impacted-by-new-slubstick-cross-cache-attack/){:target="_blank" rel="noopener"}

> A novel Linux Kernel cross-cache attack named SLUBStick has a 99% success in converting a limited heap vulnerability into an arbitrary memory read-and-write capability, letting the researchers elevate privileges or escape containers. [...]
