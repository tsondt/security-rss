Title: Billion-dollar bust as international op shutters Cryptonator wallet
Date: 2024-08-06T06:30:08+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-08-06-billion-dollar-bust-as-international-op-shutters-cryptonator-wallet

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/06/cryptonator_closure_international_operation/){:target="_blank" rel="noopener"}

> Chap named 'Roman Boss' accused of being just that at a cryptocash laundering outfit Users of Cryptonator – an online digital wallet and cryptocurrency exchange – received an unpleasant surprise last weekend after the service was shuttered in a combined operation run by the FBI, the US Internal Revenue Service (IRS), and German police.... [...]
