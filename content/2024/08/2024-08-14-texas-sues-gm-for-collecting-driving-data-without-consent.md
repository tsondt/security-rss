Title: Texas Sues GM for Collecting Driving Data without Consent
Date: 2024-08-14T16:48:46+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;business of security;cars;data collection;deception
Slug: 2024-08-14-texas-sues-gm-for-collecting-driving-data-without-consent

[Source](https://www.schneier.com/blog/archives/2024/08/texas-sues-gm-for-collecting-driving-data-without-consent.html){:target="_blank" rel="noopener"}

> Texas is suing General Motors for collecting driver data without consent and then selling it to insurance companies: From CNN : In car models from 2015 and later, the Detroit-based car manufacturer allegedly used technology to “collect, record, analyze, and transmit highly detailed driving data about each time a driver used their vehicle,” according to the AG’s statement. General Motors sold this information to several other companies, including to at least two companies for the purpose of generating “Driving Scores” about GM’s customers, the AG alleged. The suit said those two companies then sold these scores to insurance companies. Insurance [...]
