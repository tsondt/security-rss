Title: Keytronic reports losses of over $17 million after ransomware attack
Date: 2024-08-05T12:49:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-08-05-keytronic-reports-losses-of-over-17-million-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/keytronic-reports-losses-of-over-17-million-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Electronic manufacturing services provider Keytronic has revealed that it suffered losses of over $17 million due to a May ransomware attack. [...]
