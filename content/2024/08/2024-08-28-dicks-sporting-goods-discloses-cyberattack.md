Title: Dick's Sporting Goods discloses cyberattack
Date: 2024-08-28T16:20:07+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-08-28-dicks-sporting-goods-discloses-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/28/dickssporting_goods_runs_into_problems/){:target="_blank" rel="noopener"}

> Authorities probing unwanted intrusion; hard questions ahead Dick's Sporting Goods, America's largest retail chain for outdoorsy types, has admitted that it suffered a cyberattack last week.... [...]
