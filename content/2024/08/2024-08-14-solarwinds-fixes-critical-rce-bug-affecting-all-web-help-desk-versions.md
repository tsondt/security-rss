Title: SolarWinds fixes critical RCE bug affecting all Web Help Desk versions
Date: 2024-08-14T11:22:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-14-solarwinds-fixes-critical-rce-bug-affecting-all-web-help-desk-versions

[Source](https://www.bleepingcomputer.com/news/security/solarwinds-fixes-critical-rce-bug-affecting-all-web-help-desk-versions/){:target="_blank" rel="noopener"}

> A critical vulnerability in SolarWinds' Web Help Desk solution for customer support could be exploited to achieve remote code execution, the American business software developer warns in a security advisory today. [...]
