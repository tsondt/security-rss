Title: Are you blocking "keyboard walk" passwords in your Active Directory?
Date: 2024-08-16T10:01:02-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2024-08-16-are-you-blocking-keyboard-walk-passwords-in-your-active-directory

[Source](https://www.bleepingcomputer.com/news/security/are-you-blocking-keyboard-walk-passwords-in-your-active-directory/){:target="_blank" rel="noopener"}

> A common yet overlooked type of weak password are keyboard walk patterns. Learn more from Specops Software on finding and blocking keyboard walk passwords in your organization. [...]
