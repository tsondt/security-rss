Title: New CMoon USB worm targets Russians in data theft attacks
Date: 2024-08-07T17:23:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-07-new-cmoon-usb-worm-targets-russians-in-data-theft-attacks

[Source](https://www.bleepingcomputer.com/news/security/new-cmoon-usb-worm-targets-russians-in-data-theft-attacks/){:target="_blank" rel="noopener"}

> A new self-spreading worm named 'CMoon,' capable of stealing account credentials and other data, has been distributed in Russia since early July 2024 via a compromised gas supply company website. [...]
