Title: Hacker locks Unicoin staff out of Google accounts for 4 days
Date: 2024-08-20T11:17:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-08-20-hacker-locks-unicoin-staff-out-of-google-accounts-for-4-days

[Source](https://www.bleepingcomputer.com/news/security/hacker-locks-unicoin-staff-out-of-google-accounts-for-4-days/){:target="_blank" rel="noopener"}

> A hacker compromised Unicoin's Google Workspace (formerly G-Suite) account and changed the passwords for all company employees, locking them out of their corporate accounts for days. [...]
