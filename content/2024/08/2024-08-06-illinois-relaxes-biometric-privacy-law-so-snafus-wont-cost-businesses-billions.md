Title: Illinois relaxes biometric privacy law so snafus won't cost businesses billions
Date: 2024-08-06T02:45:08+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-08-06-illinois-relaxes-biometric-privacy-law-so-snafus-wont-cost-businesses-billions

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/06/illinois_bipa_amendment_reduces_penalties/){:target="_blank" rel="noopener"}

> Some scowl, some smile, as fines no longer apply every time your mugshot or fingerprint is shared The US state of Illinois has reduced penalties for breaches of its tough Biometric Information Privacy Act (BIPA).... [...]
