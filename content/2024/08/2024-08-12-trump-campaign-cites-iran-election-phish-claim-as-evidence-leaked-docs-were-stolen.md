Title: Trump campaign cites Iran election phish claim as evidence leaked docs were stolen
Date: 2024-08-12T05:34:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-08-12-trump-campaign-cites-iran-election-phish-claim-as-evidence-leaked-docs-were-stolen

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/12/trump_campaign_hacked_iran_claim/){:target="_blank" rel="noopener"}

> Dots have been joined, but hard evidence is not apparent Former US president Donald Trump's re-election campaign has claimed it's been the victim of a cyber attack.... [...]
