Title: Samsung boosts bug bounty to a cool million for cracks of the Knox Vault subsystem
Date: 2024-08-08T01:15:09+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-08-08-samsung-boosts-bug-bounty-to-a-cool-million-for-cracks-of-the-knox-vault-subsystem

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/08/samsung_microsoft_big_bug_bounty/){:target="_blank" rel="noopener"}

> Good luck, crackers: It's an isolated processor and storage enclave, and top dollar only comes from a remote attack Samsung has dangled its first $1 million bug bounty for anyone who successfully compromises Knox Vault – the isolated subsystem the Korean giant bakes into its smartphones to store info like credentials and run authentication routines.... [...]
