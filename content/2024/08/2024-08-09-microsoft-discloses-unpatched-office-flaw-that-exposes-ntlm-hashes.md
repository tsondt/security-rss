Title: Microsoft discloses unpatched Office flaw that exposes NTLM hashes
Date: 2024-08-09T12:14:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-08-09-microsoft-discloses-unpatched-office-flaw-that-exposes-ntlm-hashes

[Source](https://www.bleepingcomputer.com/news/security/microsoft-discloses-unpatched-office-flaw-that-exposes-ntlm-hashes/){:target="_blank" rel="noopener"}

> ​Microsoft has disclosed a high-severity vulnerability affecting Office 2016 that could expose NTLM hashes to a remote attacker. [...]
