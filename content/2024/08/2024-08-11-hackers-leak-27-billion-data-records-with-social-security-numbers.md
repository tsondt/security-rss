Title: Hackers leak 2.7 billion data records with Social Security numbers
Date: 2024-08-11T10:17:27-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-08-11-hackers-leak-27-billion-data-records-with-social-security-numbers

[Source](https://www.bleepingcomputer.com/news/security/hackers-leak-27-billion-data-records-with-social-security-numbers/){:target="_blank" rel="noopener"}

> Almost 2.7 billion records of personal information for people in the United States were leaked on a hacking forum, exposing names, social security numbers, all known physical addresses, and possible aliases. [...]
