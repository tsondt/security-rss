Title: Your copilot for improved cyber protection
Date: 2024-08-05T15:19:10+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2024-08-05-your-copilot-for-improved-cyber-protection

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/05/your_copilot_for_improved_cyber/){:target="_blank" rel="noopener"}

> Watch this video to learn how Palo Alto Networks is using GenAI to automate and simplify cybersecurity Sponsored Post Cyber security is complex right, particularly when you're tyring to monitor and configure multiple tools across a host of different on- and off-premise IT environments?... [...]
