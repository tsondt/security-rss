Title: Microsoft punches back at Delta Air Lines and its legal threats
Date: 2024-08-07T01:50:20+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-07-microsoft-punches-back-at-delta-air-lines-and-its-legal-threats

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/07/microsoft_delta_fight/){:target="_blank" rel="noopener"}

> SatNad himself offered CrowdStrike recovery help, Redmond says, before suggesting airline's IT is in a mess Microsoft has labelled Delta Air Lines' accusations it's partly to blame for the outages caused by CrowdStrike’s buggy software "false" and "misleading" – and insulted the state of the carrier’s IT infrastructure.... [...]
