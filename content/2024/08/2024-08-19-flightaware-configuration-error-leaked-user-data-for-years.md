Title: FlightAware configuration error leaked user data for years
Date: 2024-08-19T10:05:49-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-19-flightaware-configuration-error-leaked-user-data-for-years

[Source](https://www.bleepingcomputer.com/news/security/flightaware-configuration-error-leaked-user-data-for-years/){:target="_blank" rel="noopener"}

> Flight tracking platform FlightAware is asking some users to reset their account login passwords due to a data security incident that may have exposed personal information. [...]
