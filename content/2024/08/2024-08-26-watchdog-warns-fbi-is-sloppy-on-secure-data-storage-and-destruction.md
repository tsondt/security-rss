Title: Watchdog warns FBI is sloppy on secure data storage and destruction
Date: 2024-08-26T19:15:12+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-08-26-watchdog-warns-fbi-is-sloppy-on-secure-data-storage-and-destruction

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/26/fbi_data_security/){:target="_blank" rel="noopener"}

> National security data up for grabs, Office of the Inspector General finds update The FBI has made serious slip-ups in how it processes and destroys electronic storage media seized as part of investigations, according to an audit by the Department of Justice Office of the Inspector General.... [...]
