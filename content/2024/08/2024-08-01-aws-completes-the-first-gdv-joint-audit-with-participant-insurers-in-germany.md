Title: AWS completes the first GDV joint audit with participant insurers in Germany
Date: 2024-08-01T13:23:52+00:00
Author: Servet Gözel
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;audit;Compliance;Security;Security Blog
Slug: 2024-08-01-aws-completes-the-first-gdv-joint-audit-with-participant-insurers-in-germany

[Source](https://aws.amazon.com/blogs/security/aws-completes-the-first-gdv-joint-audit-with-participant-insurers-in-germany/){:target="_blank" rel="noopener"}

> We’re excited to announce that Amazon Web Services (AWS) has completed its first German Insurance Association (GDV) joint audit with GDV participant members, which provides assurance to customers in the German insurance industry for the security of their workloads on AWS. This is an important addition to the joint audits performed at AWS by our regulated customers within [...]
