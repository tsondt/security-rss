Title: Google raps Iran's APT42 for raining down spear-phishing attacks
Date: 2024-08-15T16:25:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-15-google-raps-irans-apt42-for-raining-down-spear-phishing-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/15/google_iran_apt42_campaigns/){:target="_blank" rel="noopener"}

> US politicians and Israeli officials among the top targets for the IRGC’s cyber unit Google has joined Microsoft in publishing intel on Iranian cyber influence activity following a recent uptick in attacks that led to data being leaked from the Trump re-election campaign.... [...]
