Title: Friday Squid Blogging: Treating Squid Parasites
Date: 2024-08-02T21:04:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-08-02-friday-squid-blogging-treating-squid-parasites

[Source](https://www.schneier.com/blog/archives/2024/08/friday-squid-blogging-treating-squid-parasites.html){:target="_blank" rel="noopener"}

> A newly discovered parasite that attacks squid eggs has been treated. Blog moderation policy. [...]
