Title: DuckDuckGo blocked in Indonesia over porn, gambling search results
Date: 2024-08-02T12:16:53-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government;Legal
Slug: 2024-08-02-duckduckgo-blocked-in-indonesia-over-porn-gambling-search-results

[Source](https://www.bleepingcomputer.com/news/security/duckduckgo-blocked-in-indonesia-over-porn-gambling-search-results/){:target="_blank" rel="noopener"}

> Privacy-focused search engine DuckDuckGo has been blocked in Indonesia by its government after citizens reportedly complained about pornographic and online gambling content in its search results. [...]
