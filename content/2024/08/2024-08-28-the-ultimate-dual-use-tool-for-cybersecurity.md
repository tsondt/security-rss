Title: The ultimate dual-use tool for cybersecurity
Date: 2024-08-28T09:02:06+00:00
Author: Robin Birtstone
Category: The Register
Tags: 
Slug: 2024-08-28-the-ultimate-dual-use-tool-for-cybersecurity

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/28/the_ultimate_dualuse_tool_for/){:target="_blank" rel="noopener"}

> Sword or plowshare? That depends on whether you're an attacker or a defender Sponsored Feature Artificial intelligence: saviour for cyber defenders, or shiny new toy for online thieves? As with most things in tech, the answer is a bit of both.... [...]
