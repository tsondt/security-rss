Title: Fighting AI fire with AI fire
Date: 2024-08-07T15:00:10+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2024-08-07-fighting-ai-fire-with-ai-fire

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/07/fighting_ai_fire_with_ai/){:target="_blank" rel="noopener"}

> Palo Alto Networks reveals how AI can be harnessed to strengthen cyber security defenses David Gordon Sponsored Post Hackers and cyber criminals are busy finding new ways of using AI to launch attacks on businesses and organizations often unprepared to deal with the speed, scale and sophistication of the assaults directed against them.... [...]
