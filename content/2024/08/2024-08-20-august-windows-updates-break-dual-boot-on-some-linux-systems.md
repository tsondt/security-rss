Title: August Windows updates break dual boot on some Linux systems
Date: 2024-08-20T12:28:26-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Linux;Security
Slug: 2024-08-20-august-windows-updates-break-dual-boot-on-some-linux-systems

[Source](https://www.bleepingcomputer.com/news/microsoft/august-windows-updates-break-dual-boot-on-some-linux-systems/){:target="_blank" rel="noopener"}

> According to user reports following this month's Patch Tuesday, the August 2024 Windows updates are breaking dual boot on some Linux systems with Secure Boot enabled. [...]
