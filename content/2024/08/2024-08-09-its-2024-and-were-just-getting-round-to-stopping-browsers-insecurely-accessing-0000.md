Title: It's 2024 and we're just getting round to stopping browsers insecurely accessing 0.0.0.0
Date: 2024-08-09T05:34:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-09-its-2024-and-were-just-getting-round-to-stopping-browsers-insecurely-accessing-0000

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/09/0000_day_bug/){:target="_blank" rel="noopener"}

> Can't reach someone's private server on localhost from outside? No problem A years-old security oversight has been addressed in basically all web browsers – Chromium-based browsers, including Microsoft Edge and Google Chrome, WebKit browsers like Apple's Safari, and Mozilla's Firefox.... [...]
