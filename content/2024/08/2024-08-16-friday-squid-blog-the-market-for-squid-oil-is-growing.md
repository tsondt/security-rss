Title: Friday Squid Blog: The Market for Squid Oil Is Growing
Date: 2024-08-16T21:06:32+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-08-16-friday-squid-blog-the-market-for-squid-oil-is-growing

[Source](https://www.schneier.com/blog/archives/2024/08/friday-squid-blog-the-market-for-squid-oil-is-growing.html){:target="_blank" rel="noopener"}

> How did I not know before now that there was a market for squid oil ? The squid oil market has experienced robust growth in recent years, expanding from $4.56 billion in 2023 to $4.94 billion in 2024 at a compound annual growth rate (CAGR) of 8.5%. The growth in the historic period can be attributed to global market growth, alternative to fish oil, cosmetics and skincare industry, sustainability practices, regulatory influence. Blog moderation policy. [...]
