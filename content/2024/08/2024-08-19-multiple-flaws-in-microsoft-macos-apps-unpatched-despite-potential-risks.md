Title: Multiple flaws in Microsoft macOS apps unpatched despite potential risks
Date: 2024-08-19T19:01:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-19-multiple-flaws-in-microsoft-macos-apps-unpatched-despite-potential-risks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/19/cisco_talos_microsoft_macos/){:target="_blank" rel="noopener"}

> Windows giant tells Cisco Talos it isn't fixing them Cisco Talos says eight vulnerabilities in Microsoft's macOS apps could be abused by nefarious types to record video and sound from a user's device, access sensitive data, log user input, and escalate privileges.... [...]
