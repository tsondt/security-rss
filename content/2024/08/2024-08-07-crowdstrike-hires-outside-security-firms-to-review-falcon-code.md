Title: CrowdStrike hires outside security firms to review Falcon code
Date: 2024-08-07T00:18:49+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-07-crowdstrike-hires-outside-security-firms-to-review-falcon-code

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/07/crowdstrike_full_incident_root_cause_analysis/){:target="_blank" rel="noopener"}

> And reveals the small mistake that bricked 8.5 million Windows boxes CrowdStrike has hired two outside security firms to review the Falcon sensor code that sparked a global IT outage last month – but it may not have an awful lot to find, because CrowdStrike has identified the simple mistake that caused the incident.... [...]
