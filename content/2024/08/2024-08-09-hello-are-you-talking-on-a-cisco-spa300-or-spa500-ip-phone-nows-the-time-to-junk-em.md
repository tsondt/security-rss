Title: Hello? Are you talking on a Cisco SPA300 or SPA500 IP phone? Now's the time to junk 'em
Date: 2024-08-09T00:30:15+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-08-09-hello-are-you-talking-on-a-cisco-spa300-or-spa500-ip-phone-nows-the-time-to-junk-em

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/09/cisco_ip_phone_critical_flaws/){:target="_blank" rel="noopener"}

> Multiple critical flaws found and they won't be fixed A boffin from British defence contractor BAE has found three critical flaws in Cisco's Small Business SPA300 and SPA500 IP phones – and another couple of nasties – none of which will be fixed or mitigated.... [...]
