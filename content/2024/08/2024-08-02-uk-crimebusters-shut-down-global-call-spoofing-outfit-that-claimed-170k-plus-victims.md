Title: UK crimebusters shut down global call-spoofing outfit that claimed 170K-plus victims
Date: 2024-08-02T06:40:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-02-uk-crimebusters-shut-down-global-call-spoofing-outfit-that-claimed-170k-plus-victims

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/02/russian_coms_phone_spoofing_service_shutdown/){:target="_blank" rel="noopener"}

> Suspected devs behind Russian Coms cuffed – now to find the users of the nastyware The UK's National Crime Agency (NCA) has shut down an outfit called Russian Coms – a call-spoofing service believed to have swindled hundreds of thousands of victims.... [...]
