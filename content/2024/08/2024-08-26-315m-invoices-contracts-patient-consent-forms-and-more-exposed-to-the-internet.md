Title: 31.5M invoices, contracts, patient consent forms, and more exposed to the internet
Date: 2024-08-26T13:00:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-26-315m-invoices-contracts-patient-consent-forms-and-more-exposed-to-the-internet

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/26/31m_invoices_business_files_exposed/){:target="_blank" rel="noopener"}

> Unprotected database with 12 years of biz records yanked offline Exclusive Nearly 2.7 TB of sensitive data — 31.5 million invoices, contracts, HIPPA patient consent forms, and other business documents regarding numerous companies across industries — has been exposed to the public internet in a non-password protected database for an unknown amount of time.... [...]
