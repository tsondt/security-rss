Title: INTERPOL recovers over $40 million stolen in a BEC attack
Date: 2024-08-06T14:38:36-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-08-06-interpol-recovers-over-40-million-stolen-in-a-bec-attack

[Source](https://www.bleepingcomputer.com/news/security/interpol-recovers-over-40-million-stolen-in-a-bec-attack/){:target="_blank" rel="noopener"}

> A global stop-payment mechanism created by INTERPOL successfully recovered over $40 million stolen in a BEC attack on a company in Singapore. [...]
