Title: Novel technique allows malicious apps to escape iOS and Android guardrails
Date: 2024-08-21T20:40:49+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;android;iOS;malicious apps;malware;phishing
Slug: 2024-08-21-novel-technique-allows-malicious-apps-to-escape-ios-and-android-guardrails

[Source](https://arstechnica.com/?p=2044637){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Phishers are using a novel technique to trick iOS and Android users into installing malicious apps that bypass safety guardrails built by both Apple and Google to prevent unauthorized apps. Both mobile operating systems employ mechanisms designed to help users steer clear of apps that steal their personal information, passwords, or other sensitive data. iOS bars the installation of all apps other than those available in its App Store, an approach widely known as the Walled Garden. Android, meanwhile, is set by default to allow only apps available in Google Play. Sideloading—or the installation of apps [...]
