Title: City of Columbus sues man after he discloses severity of ransomware attack
Date: 2024-08-30T20:00:41+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Policy;Security;lawsuits;personal information;ransomware
Slug: 2024-08-30-city-of-columbus-sues-man-after-he-discloses-severity-of-ransomware-attack

[Source](https://arstechnica.com/?p=2046614){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) A judge in Ohio has issued a temporary restraining order against a security researcher who presented evidence that a recent ransomware attack on the city of Columbus scooped up reams of sensitive personal information, contradicting claims made by city officials. The order, issued by a judge in Ohio's Franklin County, came after the city of Columbus fell victim to a ransomware attack on July 18 that siphoned 6.5 terabytes of the city’s data. A ransomware group known as Rhysida took credit for the attack and offered to auction off the data with a starting bid [...]
