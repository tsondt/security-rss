Title: US dismantles laptop farm used by undercover North Korean IT workers
Date: 2024-08-08T18:18:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-08-08-us-dismantles-laptop-farm-used-by-undercover-north-korean-it-workers

[Source](https://www.bleepingcomputer.com/news/security/us-dismantles-laptop-farm-used-by-undercover-north-korean-it-workers/){:target="_blank" rel="noopener"}

> ​​The U.S. Justice Department arrested a Nashville man charged with helping North Korean IT workers obtain remote work at companies across the United States and operating a laptop farm they used to pose as U.S.-based individuals. [...]
