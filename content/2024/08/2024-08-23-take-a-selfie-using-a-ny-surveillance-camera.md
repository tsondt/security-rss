Title: Take a Selfie Using a NY Surveillance Camera
Date: 2024-08-23T11:05:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cameras;cars;privacy;surveillance
Slug: 2024-08-23-take-a-selfie-using-a-ny-surveillance-camera

[Source](https://www.schneier.com/blog/archives/2024/08/take-a-selfie-using-a-ny-surveillance-camera.html){:target="_blank" rel="noopener"}

> This site will let you take a selfie with a New York City traffic surveillance camera. EDITED TO ADD: BoingBoing post. [...]
