Title: FBI warns of scammers posing as crypto exchange employees
Date: 2024-08-01T11:28:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-08-01-fbi-warns-of-scammers-posing-as-crypto-exchange-employees

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-scammers-posing-as-crypto-exchange-employees/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) warns of scammers posing as employees of cryptocurrency exchanges to steal funds from unsuspecting victims. [...]
