Title: US accuses man of being 'elite' ransomware pioneer they've hunted for years
Date: 2024-08-13T17:30:05+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-13-us-accuses-man-of-being-elite-ransomware-pioneer-theyve-hunted-for-years

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/13/j_p_morgan_suspect_indicted_charged/){:target="_blank" rel="noopener"}

> Authorities allege 'J.P. Morgan' practiced ‘extreme operational and online security’ The US has charged a suspect they claim is a Belarusian-Ukrainian cybercriminal whose offenses date back to 2011.... [...]
