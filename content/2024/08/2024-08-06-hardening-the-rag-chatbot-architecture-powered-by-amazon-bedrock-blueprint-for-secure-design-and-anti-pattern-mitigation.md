Title: Hardening the RAG chatbot architecture powered by Amazon Bedrock: Blueprint for secure design and anti-pattern mitigation
Date: 2024-08-06T13:06:53+00:00
Author: Magesh Dhanasekaran
Category: AWS Security
Tags: Advanced (300);Amazon Bedrock;Best Practices;Security, Identity, & Compliance;Technical How-to;artificial intelligence;Security Blog
Slug: 2024-08-06-hardening-the-rag-chatbot-architecture-powered-by-amazon-bedrock-blueprint-for-secure-design-and-anti-pattern-mitigation

[Source](https://aws.amazon.com/blogs/security/hardening-the-rag-chatbot-architecture-powered-by-amazon-bedrock-blueprint-for-secure-design-and-anti-pattern-migration/){:target="_blank" rel="noopener"}

> This blog post demonstrates how to use Amazon Bedrock with a detailed security plan to deploy a safe and responsible chatbot application. In this post, we identify common security risks and anti-patterns that can arise when exposing a large language model (LLM) in an application. Amazon Bedrock is built with features you can use to [...]
