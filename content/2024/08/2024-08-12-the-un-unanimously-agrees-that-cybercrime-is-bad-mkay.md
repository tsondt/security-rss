Title: The UN unanimously agrees that cybercrime is bad, mkay?
Date: 2024-08-12T02:30:13+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-08-12-the-un-unanimously-agrees-that-cybercrime-is-bad-mkay

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/12/in_brief_infosec/){:target="_blank" rel="noopener"}

> Also: British nuke subs get code from Russia; and BlackSuit begs for $500M Infosec in brief The United Nations often reaches consensus rather than complete agreement, but last week a proposal from Russia to cut down on cyber crime was unanimously approved.... [...]
