Title: UK health services call-handling vendor faces $7.7M fine over 2022 ransomware attack
Date: 2024-08-07T08:26:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-07-uk-health-services-call-handling-vendor-faces-77m-fine-over-2022-ransomware-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/07/ico_plans_to_fine_nhs/){:target="_blank" rel="noopener"}

> Nearly 83,000 people had their data stolen amid chaos that struck NHS healthcare The UK's data protection watchdog says it plans to fine a managed software provider to the NHS £6.09 million ($7.7 million) for failings that led to a 2022 ransomware attack.... [...]
