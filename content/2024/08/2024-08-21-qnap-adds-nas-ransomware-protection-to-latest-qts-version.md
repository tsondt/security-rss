Title: QNAP adds NAS ransomware protection to latest QTS version
Date: 2024-08-21T14:17:56-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-08-21-qnap-adds-nas-ransomware-protection-to-latest-qts-version

[Source](https://www.bleepingcomputer.com/news/security/qnap-adds-nas-ransomware-protection-to-latest-qts-version/){:target="_blank" rel="noopener"}

> ​Taiwanese hardware vendor QNAP has added a Security Center with ransomware protection capabilities to the latest version of its QTS operating system for network-attached storage (NAS) devices. [...]
