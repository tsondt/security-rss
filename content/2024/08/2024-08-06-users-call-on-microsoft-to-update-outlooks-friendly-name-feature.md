Title: Users call on Microsoft to update Outlook's friendly name feature
Date: 2024-08-06T12:18:10+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-08-06-users-call-on-microsoft-to-update-outlooks-friendly-name-feature

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/06/users_call_for_microsoft_to/){:target="_blank" rel="noopener"}

> That one weird thing in Outlook that gives phishers and scammers an in to an inbox Users are urging Microsoft to rethink how it shows sender email addresses in Outlook because phishing criminals are taking advantage, using helpful, friendly names to serve up emails loaded with malicious intent.... [...]
