Title: Point of entry: Why hackers target stolen credentials for initial access
Date: 2024-08-06T10:01:02-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2024-08-06-point-of-entry-why-hackers-target-stolen-credentials-for-initial-access

[Source](https://www.bleepingcomputer.com/news/security/point-of-entry-why-hackers-target-stolen-credentials-for-initial-access/){:target="_blank" rel="noopener"}

> Stolen credentials are a big problem, commonly used to breach networks in attacks. Learn more from Specops Software about checking the password hygiene of your Active Directory. [...]
