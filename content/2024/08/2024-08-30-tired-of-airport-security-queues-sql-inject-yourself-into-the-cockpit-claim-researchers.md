Title: Tired of airport security queues? SQL inject yourself into the cockpit, claim researchers
Date: 2024-08-30T13:28:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-30-tired-of-airport-security-queues-sql-inject-yourself-into-the-cockpit-claim-researchers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/30/sql_injection_known_crewmember/){:target="_blank" rel="noopener"}

> Infosec hounds say they spotted vulnerability during routine travel in the US Updated Cybersecurity researchers say they've found a vulnerability that allowed them to skip US airport security checks and even fly in the cockpit on some scheduled flights.... [...]
