Title: Surge in Magniber ransomware attacks impact home users worldwide
Date: 2024-08-04T10:17:34-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-08-04-surge-in-magniber-ransomware-attacks-impact-home-users-worldwide

[Source](https://www.bleepingcomputer.com/news/security/surge-in-magniber-ransomware-attacks-impact-home-users-worldwide/){:target="_blank" rel="noopener"}

> [...]
