Title: Mac and Windows users infected by software updates delivered over hacked ISP
Date: 2024-08-05T23:43:06+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;DNS poisoning;domain name system;infections;malware
Slug: 2024-08-05-mac-and-windows-users-infected-by-software-updates-delivered-over-hacked-isp

[Source](https://arstechnica.com/?p=2041175){:target="_blank" rel="noopener"}

> Enlarge (credit: Marco Verch Professional Photographer and Speaker ) Hackers delivered malware to Windows and Mac users by compromising their Internet service provider and then tampering with software updates delivered over unsecure connections, researchers said. The attack, researchers from security firm Volexity said, worked by hacking routers or similar types of device infrastructure of an unnamed ISP. The attackers then used their control of the devices to poison domain name system responses for legitimate hostnames providing updates for at least six different apps written for Windows or macOS. The apps affected were the 5KPlayer, Quick Heal, Rainmeter, Partition Wizard, and [...]
