Title: On the Voynich Manuscript
Date: 2024-08-13T11:04:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;cryptanalysis;history of cryptography
Slug: 2024-08-13-on-the-voynich-manuscript

[Source](https://www.schneier.com/blog/archives/2024/08/on-the-voynich-manuscript.html){:target="_blank" rel="noopener"}

> Really interesting article on the ancient-manuscript scholars who are applying their techniques to the Voynich Manuscript. No one has been able to understand the writing yet, but there are some new understandings: Davis presented her findings at the medieval-studies conference and published them in 2020 in the journal Manuscript Studies. She had hardly solved the Voynich, but she’d opened it to new kinds of investigation. If five scribes had come together to write it, the manuscript was probably the work of a community, rather than of a single deranged mind or con artist. Why the community used its own language, [...]
