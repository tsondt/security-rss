Title: Microsoft discloses Office zero-day, still working on a patch
Date: 2024-08-09T12:14:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-08-09-microsoft-discloses-office-zero-day-still-working-on-a-patch

[Source](https://www.bleepingcomputer.com/news/security/microsoft-discloses-office-zero-day-still-working-on-a-patch/){:target="_blank" rel="noopener"}

> ​Microsoft has disclosed a high-severity zero-day vulnerability affecting Office 2016 and later, which is still waiting for a patch. [...]
