Title: Iran named as source of Trump campaign phish, leaks
Date: 2024-08-20T06:29:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-08-20-iran-named-as-source-of-trump-campaign-phish-leaks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/20/iran_trump_phishing_attribution/){:target="_blank" rel="noopener"}

> Political stirrer Roger Stone may have been a weak link after personal emails cracked US authorities have named Iran as the likely source of a recent attack on the campaign of the US Republican Party's presidential nominee, Donald Trump.... [...]
