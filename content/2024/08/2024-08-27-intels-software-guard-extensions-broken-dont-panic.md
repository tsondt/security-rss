Title: Intel's Software Guard Extensions broken? Don't panic
Date: 2024-08-27T19:59:33+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-08-27-intels-software-guard-extensions-broken-dont-panic

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/27/intel_root_key_xeons/){:target="_blank" rel="noopener"}

> More of a storm in a teacup Today's news that Intel's Software Guard Extensions (SGX) security system is open to abuse may be overstated.... [...]
