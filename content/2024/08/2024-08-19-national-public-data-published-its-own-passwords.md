Title: National Public Data Published Its Own Passwords
Date: 2024-08-19T16:23:31+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Data Breaches;The Coming Storm;Atlas Data Privacy Corp.;CreationNext;National Public Data breach;npd.pentester.com;npdbreach.com;Salvatore Verini;USInfoSearch.com
Slug: 2024-08-19-national-public-data-published-its-own-passwords

[Source](https://krebsonsecurity.com/2024/08/national-public-data-published-its-own-passwords/){:target="_blank" rel="noopener"}

> New details are emerging about a breach at National Public Data (NPD), a consumer data broker that recently spilled hundreds of millions of Americans’ Social Security Numbers, addresses, and phone numbers online. KrebsOnSecurity has learned that another NPD data broker which shares access to the same consumer records inadvertently published the passwords to its back-end database in a file that was freely available from its homepage until today. In April, a cybercriminal named USDoD began selling data stolen from NPD. In July, someone leaked what was taken, including the names, addresses, phone numbers and in some cases email addresses for [...]
