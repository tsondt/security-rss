Title: Iran hunts down double agents with fake recruiting sites, Mandiant reckons
Date: 2024-08-30T04:27:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-30-iran-hunts-down-double-agents-with-fake-recruiting-sites-mandiant-reckons

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/30/iran_dissident_recruitment_scam/){:target="_blank" rel="noopener"}

> Farsi-language posts target possibly-pro-Israel individuals Government-backed Iranian actors allegedly set up dozens of fake recruiting websites and social media accounts to hunt down double agents and dissidents suspected of collaborating with the nation’s enemies, including Israel.... [...]
