Title: Cisco warns of critical RCE zero-days in end of life IP phones
Date: 2024-08-08T17:27:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2024-08-08-cisco-warns-of-critical-rce-zero-days-in-end-of-life-ip-phones

[Source](https://www.bleepingcomputer.com/news/security/cisco-warns-of-critical-rce-zero-days-in-end-of-life-ip-phones/){:target="_blank" rel="noopener"}

> Cisco is warning of multiple critical remote code execution zero-days in the web-based management interface of the end-of-life Small Business SPA 300 and SPA 500 series IP phones. [...]
