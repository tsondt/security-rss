Title: National Public Data confirms breach exposing Social Security numbers
Date: 2024-08-16T13:18:50-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-08-16-national-public-data-confirms-breach-exposing-social-security-numbers

[Source](https://www.bleepingcomputer.com/news/security/national-public-data-confirms-breach-exposing-social-security-numbers/){:target="_blank" rel="noopener"}

> Background check service National Public Data confirms that hackers breached its systems after threat actors leaked a stolen database with millions of social security numbers and other sensitive personal information. [...]
