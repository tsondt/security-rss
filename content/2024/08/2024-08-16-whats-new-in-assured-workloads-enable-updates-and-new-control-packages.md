Title: What’s New in Assured Workloads: Enable updates and new control packages
Date: 2024-08-16T16:00:00+00:00
Author: Mikaela Heijer
Category: GCP Security
Tags: Security & Identity
Slug: 2024-08-16-whats-new-in-assured-workloads-enable-updates-and-new-control-packages

[Source](https://cloud.google.com/blog/products/identity-security/whats-new-in-assured-workloads-enable-updates-and-new-control-packages/){:target="_blank" rel="noopener"}

> Compliance isn't a one-time goal; it's an ongoing process. As your organization and the regulatory environment evolve, so too does Assured Workloads. Here are the latest additions to our portfolio of software-defined controls and policies that can make supporting your compliance requirements easier on Google Cloud. Introducing Compliance Updates An Assured Workloads control package is a set of software-defined controls that support a compliance standard or regulation. These packages may include mechanisms to enforce data residency, data sovereignty, personnel access restrictions, and more, depending on the particular statute’s control objectives. To give customers flexibility in configuring their cloud estate, Assured [...]
