Title: Sonic Automotive says ransomware-linked CDK software outage cost it $30M
Date: 2024-08-06T16:42:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-06-sonic-automotive-says-ransomware-linked-cdk-software-outage-cost-it-30m

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/06/sonic_automotive_says_cdk_disruption/){:target="_blank" rel="noopener"}

> Misery loves company – all of its competitors were also negatively impacted One of the US's largest car dealerships says the IT outage caused by CDK Global's June ransomware attack cost it approximately $30 million.... [...]
