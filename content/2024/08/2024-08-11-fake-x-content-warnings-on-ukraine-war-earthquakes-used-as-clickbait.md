Title: Fake X content warnings on Ukraine war, earthquakes used as clickbait
Date: 2024-08-11T11:18:29-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-08-11-fake-x-content-warnings-on-ukraine-war-earthquakes-used-as-clickbait

[Source](https://www.bleepingcomputer.com/news/security/fake-x-content-warnings-on-ukraine-war-earthquakes-used-as-clickbait/){:target="_blank" rel="noopener"}

> X has always had a bot problem, but now scammers are utilizing the Ukraine war and earthquake warnings in Japan to entice users into clicking on fake content warnings and videos that lead to scam adult sites, malicious browser extensions, and shady affiliate sites. [...]
