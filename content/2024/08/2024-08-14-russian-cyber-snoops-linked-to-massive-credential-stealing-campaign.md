Title: Russian cyber snoops linked to massive credential-stealing campaign
Date: 2024-08-14T18:45:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-14-russian-cyber-snoops-linked-to-massive-credential-stealing-campaign

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/14/russias_fsb_cyber_phishing/){:target="_blank" rel="noopener"}

> Citizen Lab also spots a COLDWASTREL swimming in the Rivers of Phish Russia's Federal Security Service (FSB) cyberspies, joined by a new digital snooping crew, have been conducting a massive online phishing espionage campaign via phishing against targets in the US and Europe over the past two years, according to the University of Toronto's Citizen Lab.... [...]
