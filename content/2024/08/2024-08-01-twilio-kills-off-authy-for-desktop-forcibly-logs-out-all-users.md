Title: Twilio kills off Authy for desktop, forcibly logs out all users
Date: 2024-08-01T17:06:59-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-08-01-twilio-kills-off-authy-for-desktop-forcibly-logs-out-all-users

[Source](https://www.bleepingcomputer.com/news/security/twilio-kills-off-authy-for-desktop-forcibly-logs-out-all-users/){:target="_blank" rel="noopener"}

> Twilio has finally killed off its Authy for Desktop application, forcibly logging users out of the desktop application. [...]
