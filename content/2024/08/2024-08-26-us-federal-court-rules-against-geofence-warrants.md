Title: US Federal Court Rules Against Geofence Warrants
Date: 2024-08-26T11:05:44+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;courts;data privacy;geolocation;laws
Slug: 2024-08-26-us-federal-court-rules-against-geofence-warrants

[Source](https://www.schneier.com/blog/archives/2024/08/us-federal-court-rules-against-geofence-warrants.html){:target="_blank" rel="noopener"}

> This is a big deal. A US Appeals Court ruled that geofence warrants—these are general warrants demanding information about all people within a geographical boundary—are unconstitutional. The decision seems obvious to me, but you can’t take anything for granted. [...]
