Title: CISA warns of hackers abusing Cisco Smart Install feature
Date: 2024-08-08T13:23:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-08-08-cisa-warns-of-hackers-abusing-cisco-smart-install-feature

[Source](https://www.bleepingcomputer.com/news/security/cisa-warns-of-hackers-abusing-cisco-smart-install-feature/){:target="_blank" rel="noopener"}

> CISA recommends disabling the legacy Cisco Smart Install feature after seeing it abused by threat actors in recent attacks to steal sensitive data, such as system configuration files. [...]
