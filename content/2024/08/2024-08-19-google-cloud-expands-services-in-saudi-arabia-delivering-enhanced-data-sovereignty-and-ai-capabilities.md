Title: Google Cloud expands services in Saudi Arabia, delivering enhanced data sovereignty and AI capabilities
Date: 2024-08-19T16:00:00+00:00
Author: Bader Almadi
Category: GCP Security
Tags: Infrastructure;SAP on Google Cloud;Security & Identity
Slug: 2024-08-19-google-cloud-expands-services-in-saudi-arabia-delivering-enhanced-data-sovereignty-and-ai-capabilities

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-expands-services-in-saudi-arabia-delivering-enhanced-data-sovereignty-and-ai-capabilities/){:target="_blank" rel="noopener"}

> Building on the launch of the Google Cloud region in the Kingdom of Saudi Arabia (KSA) in November 2023, we are excited to announce new data sovereignty, security, and AI capabilities for the Dammam region. These new offerings can help support the digital transformation journeys of organizations operating in Saudi Arabia that need to meet certain regulatory requirements, including multinational enterprises operating in the Kingdom. Sovereign Controls by CNTXT Google Cloud is committed to working with local, trusted partners to help our customers meet digital sovereignty requirements. In Saudi Arabia, Google Cloud has partnered with CNTXT to offer Sovereign Controls [...]
