Title: 512-bit RSA key in home energy system gives control of “virtual power plant”
Date: 2024-08-09T13:07:30+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;Uncategorized;encryption;hacking;power management;RSA;solar
Slug: 2024-08-09-512-bit-rsa-key-in-home-energy-system-gives-control-of-virtual-power-plant

[Source](https://arstechnica.com/?p=2042026){:target="_blank" rel="noopener"}

> Enlarge When Ryan Castellucci recently acquired solar panels and a battery storage system for their home just outside of London, they were drawn to the ability to use an open source dashboard to monitor and control the flow of electricity being generated. Instead, they gained much, much more—some 200 megawatts of programmable capacity to charge or discharge to the grid at will. That’s enough energy to power roughly 40,000 homes. Castellucci, whose pronouns are they/them, acquired this remarkable control after gaining access to the administrative account for GivEnergy, the UK-based energy management provider who supplied the systems. In addition to [...]
