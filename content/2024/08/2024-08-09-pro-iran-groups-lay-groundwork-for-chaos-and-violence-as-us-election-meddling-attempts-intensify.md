Title: Pro-Iran groups lay groundwork for 'chaos and violence' as US election meddling attempts intensify
Date: 2024-08-09T13:01:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-09-pro-iran-groups-lay-groundwork-for-chaos-and-violence-as-us-election-meddling-attempts-intensify

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/09/iran_state_groups_lay_groundwork/){:target="_blank" rel="noopener"}

> Political officials, advisors targeted in cyber attacks as fake news sites deliver lefty zingers Microsoft says Iran's efforts to influence the November US presidential election have gathered pace recently and there are signs that point toward its intent to incite violence against key figures.... [...]
