Title: Microsoft disables BitLocker security fix, advises manual mitigation
Date: 2024-08-15T11:26:59-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-08-15-microsoft-disables-bitlocker-security-fix-advises-manual-mitigation

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-disables-bitlocker-security-fix-advises-manual-mitigation/){:target="_blank" rel="noopener"}

> Microsoft has disabled a fix for a BitLocker security feature bypass vulnerability due to firmware incompatibility issues that were causing patched Windows devices to go into BitLocker recovery mode. [...]
