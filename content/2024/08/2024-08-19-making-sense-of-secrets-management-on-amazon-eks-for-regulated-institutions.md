Title: Making sense of secrets management on Amazon EKS for regulated institutions
Date: 2024-08-19T17:43:34+00:00
Author: Piyush Mattoo
Category: AWS Security
Tags: Advanced (300);Amazon Elastic Kubernetes Service;AWS Secrets Manager;Best Practices;Security & Governance;Security, Identity, & Compliance;Technical How-to;Amazon EKS;Containers;governance;Security Blog
Slug: 2024-08-19-making-sense-of-secrets-management-on-amazon-eks-for-regulated-institutions

[Source](https://aws.amazon.com/blogs/security/making-sense-of-secrets-management-on-amazon-eks-for-regulated-institutions/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) customers operating in a regulated industry, such as the financial services industry (FSI) or healthcare, are required to meet their regulatory and compliance obligations, such as the Payment Card Industry Data Security Standard (PCI DSS) or Health Insurance Portability and Accountability Act (HIPPA). AWS offers regulated customers tools, guidance and third-party audit reports [...]
