Title: People-Search Site Removal Services Largely Ineffective
Date: 2024-08-09T13:24:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;privacy;reports;searches
Slug: 2024-08-09-people-search-site-removal-services-largely-ineffective

[Source](https://www.schneier.com/blog/archives/2024/08/people-search-site-removal-services-largely-ineffective.html){:target="_blank" rel="noopener"}

> Consumer Reports has a new study of people-search site removal services, concluding that they don’t really work: As a whole, people-search removal services are largely ineffective. Private information about each participant on the people-search sites decreased after using the people-search removal services. And, not surprisingly, the removal services did save time compared with manually opting out. But, without exception, information about each participant still appeared on some of the 13 people-search sites at the one-week, one-month, and four-month intervals. We initially found 332 instances of information about the 28 participants who would later be signed up for removal services (that [...]
