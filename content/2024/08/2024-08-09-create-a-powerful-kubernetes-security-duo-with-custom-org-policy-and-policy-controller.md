Title: Create a powerful Kubernetes security duo with Custom Org Policy and Policy Controller
Date: 2024-08-09T16:00:00+00:00
Author: Akshay Datar
Category: GCP Security
Tags: Containers & Kubernetes;Security & Identity
Slug: 2024-08-09-create-a-powerful-kubernetes-security-duo-with-custom-org-policy-and-policy-controller

[Source](https://cloud.google.com/blog/products/identity-security/create-a-powerful-kubernetes-security-duo-with-custom-org-policy-and-policy-controller/){:target="_blank" rel="noopener"}

> To help customers implement defense in depth strategies, Google Cloud offers multiple layers of centralized resource governance controls that can help organizations securely scale their Google Cloud adoption across thousands of projects, APIs, and developers. These controls can help administrators strengthen security and support compliance across their entire org, without introducing additional overhead in the development process. Google Cloud custom Org Policy and Policy Controller are two effective and complementary controls we offer specifically for Google Kubernetes Engine (GKE). Together, these controls can help you achieve comprehensive governance and compliance at scale and secure your GKE clusters. Adding guardrails can [...]
