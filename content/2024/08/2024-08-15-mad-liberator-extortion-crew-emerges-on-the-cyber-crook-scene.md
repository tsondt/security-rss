Title: Mad Liberator extortion crew emerges on the cyber-crook scene
Date: 2024-08-15T10:27:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-08-15-mad-liberator-extortion-crew-emerges-on-the-cyber-crook-scene

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/15/mad_liberator_extortion/){:target="_blank" rel="noopener"}

> Anydesk is its access tool of choice A new extortion gang called Mad Liberator uses social engineering and the remote-access tool Anydesk to steal organizations' data and then demand a ransom payment, according to Sophos X-Ops.... [...]
