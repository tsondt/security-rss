Title: Zero-click Windows TCP/IP RCE impacts all systems with IPv6 enabled, patch now
Date: 2024-08-14T16:51:31-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-08-14-zero-click-windows-tcpip-rce-impacts-all-systems-with-ipv6-enabled-patch-now

[Source](https://www.bleepingcomputer.com/news/microsoft/zero-click-windows-tcp-ip-rce-impacts-all-systems-with-ipv6-enabled-patch-now/){:target="_blank" rel="noopener"}

> Microsoft warned customers this Tuesday to patch a critical TCP/IP remote code execution (RCE) vulnerability with an increased likelihood of exploitation that impacts all Windows systems with IPv6 enabled. [...]
