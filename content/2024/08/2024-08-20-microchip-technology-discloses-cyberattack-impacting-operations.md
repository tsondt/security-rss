Title: Microchip Technology discloses cyberattack impacting operations
Date: 2024-08-20T17:06:58-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-08-20-microchip-technology-discloses-cyberattack-impacting-operations

[Source](https://www.bleepingcomputer.com/news/security/microchip-technology-discloses-cyberattack-impacting-operations/){:target="_blank" rel="noopener"}

> American chipmaker Microchip Technology Incorporated has disclosed that a cyberattack impacted its systems over the weekend, disrupting operations across multiple manufacturing facilities. [...]
