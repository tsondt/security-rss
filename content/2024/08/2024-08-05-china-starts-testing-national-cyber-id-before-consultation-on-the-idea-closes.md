Title: China starts testing national cyber-ID before consultation on the idea closes
Date: 2024-08-05T05:15:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-08-05-china-starts-testing-national-cyber-id-before-consultation-on-the-idea-closes

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/05/china_cyber_id_pilot/){:target="_blank" rel="noopener"}

> Eighty-one apps signed up to pilot facial recognition and real name ID system Chinese app developers have signed up to beta test a national cyberspace ID system that will use facial recognition technology and the real names of users, according to Chinese media.... [...]
