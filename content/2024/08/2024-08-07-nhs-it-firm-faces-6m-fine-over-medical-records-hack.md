Title: NHS IT firm faces £6m fine over medical records hack
Date: 2024-08-07T07:27:41+00:00
Author: Matthew Weaver
Category: The Guardian
Tags: NHS;Information commissioner;England;Cybercrime;UK news;Internet;Society;Technology;Health;Data and computer security;Data protection
Slug: 2024-08-07-nhs-it-firm-faces-6m-fine-over-medical-records-hack

[Source](https://www.theguardian.com/society/article/2024/aug/07/nhs-it-firm-fine-advanced-software-medical-records-ransomware-attack-hack){:target="_blank" rel="noopener"}

> Watchdog provisionally finds Advanced failed to act to protect data of 82,946 after ransomware attack in England A software provider faces being fined more than £6m over a 2022 ransomware attack that disrupted NHS and social care services in England, the data protection regulator has announced. The Information Commissioner’s Office (ICO) said it had provisionally found that Advanced Computer Software Group had failed to implement measures to protect the personal information of 82,946 people who were affected by the attack, which included some sensitive information. Continue reading... [...]
