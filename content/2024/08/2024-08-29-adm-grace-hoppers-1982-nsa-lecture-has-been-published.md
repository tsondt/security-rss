Title: Adm. Grace Hopper’s 1982 NSA Lecture Has Been Published
Date: 2024-08-29T15:58:00+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;history of computing;history of security;NSA;videos
Slug: 2024-08-29-adm-grace-hoppers-1982-nsa-lecture-has-been-published

[Source](https://www.schneier.com/blog/archives/2024/08/adm-grace-hoppers-1982-nsa-lecture-has-been-published.html){:target="_blank" rel="noopener"}

> The “ long lost lecture ” by Adm. Grace Hopper has been published by the NSA. (Note that there are two parts.) It’s a wonderful talk: funny, engaging, wise, prescient. Remember that talk was given in 1982, less than a year before the ARPANET switched to TCP/IP and the internet went operational. She was a remarkable person. Listening to it, and thinking about the audience of NSA engineers, I wonder how much of what she’s talking about as the future of computing—miniaturization, parallelization—was being done in the present and in secret. [...]
