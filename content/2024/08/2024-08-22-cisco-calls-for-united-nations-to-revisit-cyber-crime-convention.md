Title: Cisco calls for United Nations to revisit cyber-crime convention
Date: 2024-08-22T06:32:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-08-22-cisco-calls-for-united-nations-to-revisit-cyber-crime-convention

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/22/cisco_criticizes_un_cybercrime_convention/){:target="_blank" rel="noopener"}

> Echoes human rights groups' concerns that it could suppress free speech and more Networking giant Cisco has suggested the United Nations' first-ever convention against cyber-crime is dangerously flawed and should be revised before being put to a formal vote.... [...]
