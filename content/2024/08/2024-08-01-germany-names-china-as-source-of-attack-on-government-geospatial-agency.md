Title: Germany names China as source of attack on government geospatial agency
Date: 2024-08-01T05:59:59+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-08-01-germany-names-china-as-source-of-attack-on-government-geospatial-agency

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/01/germany_accuses_china_of_cyberattack/){:target="_blank" rel="noopener"}

> Meanwhile, US apparently considers further AI hardware sanctions Germany's government has named China-controlled actors as the perpetrators of a 2021 cyber attack on the Federal Office of Cartography and Geodesy (BKG) – the official mapping agency.... [...]
