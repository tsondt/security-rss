Title: CrowdStrike unhappy about Delta's 'litigation threat,' claims airline refused 'free on-site help'
Date: 2024-08-05T12:32:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-05-crowdstrike-unhappy-about-deltas-litigation-threat-claims-airline-refused-free-on-site-help

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/05/crowdstrike_is_not_at_all/){:target="_blank" rel="noopener"}

> Vendor plans to aggressively defend its case before listing catalog of shortcomings at the airline CrowdStrike says it is "highly disappointed" and rejects the claims made by Delta and its lawyers that the vendor exhibited gross negligence in the events that led to the global IT outage a little over two weeks ago.... [...]
