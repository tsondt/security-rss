Title: Litespeed Cache bug exposes millions of WordPress sites to takeover attacks
Date: 2024-08-21T13:22:14-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-08-21-litespeed-cache-bug-exposes-millions-of-wordpress-sites-to-takeover-attacks

[Source](https://www.bleepingcomputer.com/news/security/litespeed-cache-bug-exposes-millions-of-wordpress-sites-to-takeover-attacks/){:target="_blank" rel="noopener"}

> A critical vulnerability in the LiteSpeed Cache WordPress plugin can let attackers take over millions of websites after creating rogue admin accounts. [...]
