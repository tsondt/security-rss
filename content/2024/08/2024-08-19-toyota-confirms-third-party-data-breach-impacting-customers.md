Title: Toyota confirms third-party data breach impacting customers
Date: 2024-08-19T16:51:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-08-19-toyota-confirms-third-party-data-breach-impacting-customers

[Source](https://www.bleepingcomputer.com/news/security/toyota-confirms-third-party-data-breach-impacting-customers/){:target="_blank" rel="noopener"}

> Toyota confirmed that customer data was exposed in a third-party data breach after a threat actor leaked an archive of 240GB of stolen data on a hacking forum. [...]
