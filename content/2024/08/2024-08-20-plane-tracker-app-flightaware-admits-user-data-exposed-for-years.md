Title: Plane tracker app FlightAware admits user data exposed for years
Date: 2024-08-20T14:30:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-08-20-plane-tracker-app-flightaware-admits-user-data-exposed-for-years

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/20/flightaware_data_exposure/){:target="_blank" rel="noopener"}

> Privacy blunder alert omits number of key details Updated Popular flight-tracking app FlightAware has admitted that it was exposing a bunch of users' data for more than three years.... [...]
