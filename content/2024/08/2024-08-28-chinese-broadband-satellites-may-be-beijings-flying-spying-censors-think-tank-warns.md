Title: Chinese broadband satellites may be Beijing's flying spying censors, think tank warns
Date: 2024-08-28T01:58:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-08-28-chinese-broadband-satellites-may-be-beijings-flying-spying-censors-think-tank-warns

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/28/aspi_china_satellite_broadband_risk/){:target="_blank" rel="noopener"}

> Ground stations are the perfect place for the Great Firewall to block things China finds unpleasant The multiple constellations of broadband-beaming satellites planned by Chinese companies could conceivably run the nation's "Great Firewall" content censorship system, according to think tank The Australian Strategic Policy Institute. And if they do, using the services outside China will be dangerous.... [...]
