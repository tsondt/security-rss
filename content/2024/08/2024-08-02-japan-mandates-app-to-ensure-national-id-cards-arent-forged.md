Title: Japan mandates app to ensure national ID cards aren't forged
Date: 2024-08-02T06:00:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-08-02-japan-mandates-app-to-ensure-national-id-cards-arent-forged

[Source](https://go.theregister.com/feed/www.theregister.com/2024/08/02/japan_smartcard_verification_app/){:target="_blank" rel="noopener"}

> First delays, then data leaks – now fraud detection needed at point of use The Japanese government has released details of of an app that verifies the legitimacy of its troubled My Number Card – a national identity document.... [...]
