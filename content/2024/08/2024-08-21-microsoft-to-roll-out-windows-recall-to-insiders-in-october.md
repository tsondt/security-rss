Title: Microsoft to roll out Windows Recall to Insiders in October
Date: 2024-08-21T15:17:22-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-08-21-microsoft-to-roll-out-windows-recall-to-insiders-in-october

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-to-roll-out-windows-recall-to-insiders-in-october/){:target="_blank" rel="noopener"}

> Microsoft announced today that it will start rolling out its AI-powered Windows Recall feature to Insiders with Copilot+ PCs in October. [...]
