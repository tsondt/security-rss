Title: Six 0-Days Lead Microsoft’s August 2024 Patch Push
Date: 2024-08-13T21:43:05+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;CVE-2024-38106;CVE-2024-38107;CVE-2024-38178;CVE-2024-38189;CVE-2024-38193;CVE-2024-38213;Kev Breen;mark of the web;Microsoft Project;Windows Edge;Zero Day Initiative
Slug: 2024-08-13-six-0-days-lead-microsofts-august-2024-patch-push

[Source](https://krebsonsecurity.com/2024/08/six-0-days-lead-microsofts-august-2024-patch-push/){:target="_blank" rel="noopener"}

> Microsoft today released updates to fix at least 90 security vulnerabilities in Windows and related software, including a whopping six zero-day flaws that are already being actively exploited by attackers. Image: Shutterstock. This month’s bundle of update joy from Redmond includes patches for security holes in Office,.NET, Visual Studio, Azure, Co-Pilot, Microsoft Dynamics, Teams, Secure Boot, and of course Windows itself. Of the six zero-day weaknesses Microsoft addressed this month, half are local privilege escalation vulnerabilities — meaning they are primarily useful for attackers when combined with other flaws or access. CVE-2024-38106, CVE-2024-38107 and CVE-2024-38193 all allow an attacker to [...]
