Title: Hackers posing as Ukraine’s Security Service infect 100 govt PCs
Date: 2024-08-12T14:14:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-08-12-hackers-posing-as-ukraines-security-service-infect-100-govt-pcs

[Source](https://www.bleepingcomputer.com/news/security/hackers-posing-as-ukraines-security-service-infect-100-govt-pcs/){:target="_blank" rel="noopener"}

> Attackers impersonating the Security Service of Ukraine (SSU) have used malicious spam emails to target and compromise systems belonging to the country's government agencies. [...]
