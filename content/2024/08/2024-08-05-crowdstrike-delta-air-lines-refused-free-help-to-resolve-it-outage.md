Title: Crowdstrike: Delta Air Lines refused free help to resolve IT outage
Date: 2024-08-05T16:09:42-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Business;Legal
Slug: 2024-08-05-crowdstrike-delta-air-lines-refused-free-help-to-resolve-it-outage

[Source](https://www.bleepingcomputer.com/news/security/crowdstrike-delta-air-lines-refused-free-help-to-resolve-it-outage/){:target="_blank" rel="noopener"}

> The legal spars between Delta Air Lines and CrowdStrike are heating up, with the cybersecurity firm claiming that Delta's extended IT outage was caused by poor disaster recovery plans and the airline refusing to accept free onsite help in restoring Windows devices. [...]
