Title: France's Grand Palais discloses cyberattack during Olympic games
Date: 2024-08-06T12:00:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-08-06-frances-grand-palais-discloses-cyberattack-during-olympic-games

[Source](https://www.bleepingcomputer.com/news/security/frances-grand-palais-discloses-cyberattack-during-olympic-games/){:target="_blank" rel="noopener"}

> The Grand Palais Réunion des musées nationaux (Rmn) in France is warning that it suffered a cyberattack on Saturday night, August 3, 2024. [...]
