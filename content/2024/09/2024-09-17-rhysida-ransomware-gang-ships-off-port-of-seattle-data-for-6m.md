Title: Rhysida ransomware gang ships off Port of Seattle data for $6M
Date: 2024-09-17T16:45:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-17-rhysida-ransomware-gang-ships-off-port-of-seattle-data-for-6m

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/17/rhysida_port_of_seattle/){:target="_blank" rel="noopener"}

> Auction acts as payback after authority publicly refuses to pay up The trend of ransomware crews claiming to sell stolen data privately instead of leaking it online continues with Rhysida marketing the data allegedly belonging to Port of Seattle for 100 Bitcoin (around $5.9 million).... [...]
