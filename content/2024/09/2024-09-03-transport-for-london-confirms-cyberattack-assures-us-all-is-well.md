Title: Transport for London confirms cyberattack, assures us all is well
Date: 2024-09-03T09:40:03+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-09-03-transport-for-london-confirms-cyberattack-assures-us-all-is-well

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/03/tfl_cyberattack/){:target="_blank" rel="noopener"}

> Government body claims there is no evidence of customer data being compromised Transport for London (TfL) – responsible for much of the public network carrying people around England's capital – is battling to stay on top of an unfolding "cyber security incident."... [...]
