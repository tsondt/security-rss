Title: 23andMe settles class-action breach lawsuit for $30 million
Date: 2024-09-16T02:30:12+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-09-16-23andme-settles-class-action-breach-lawsuit-for-30-million

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/16/security_in_brief/){:target="_blank" rel="noopener"}

> Also: Apple to end NSO Group lawsuit; Malicious Python dev job offers; Dark web kingpins busted; and more Infosec In Brief Genetic testing outfit 23andMe has settled a proposed class action case related to a 2023 data breach for $30 million.... [...]
