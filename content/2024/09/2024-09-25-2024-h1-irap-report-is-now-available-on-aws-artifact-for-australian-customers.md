Title: 2024 H1 IRAP report is now available on AWS Artifact for Australian customers
Date: 2024-09-25T13:50:51+00:00
Author: Patrick Chang
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Auditing;Australia;Australia Regions;AWS Artifact;AWS security;Compliance;Compliance reports;IRAP;IRAP PROTECTED;Security;Security Blog
Slug: 2024-09-25-2024-h1-irap-report-is-now-available-on-aws-artifact-for-australian-customers

[Source](https://aws.amazon.com/blogs/security/2024-h1-irap-report-is-now-available-on-aws-artifact-for-australian-customers/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is excited to announce that a new Information Security Registered Assessors Program (IRAP) report (2024 H1) is now available through AWS Artifact. An independent Australian Signals Directorate (ASD) certified IRAP assessor completed the IRAP assessment of AWS in August 2024. The new IRAP report includes an additional seven AWS services that are now assessed at the [...]
