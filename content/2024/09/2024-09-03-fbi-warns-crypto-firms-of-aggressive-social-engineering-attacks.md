Title: FBI warns crypto firms of aggressive social engineering attacks
Date: 2024-09-03T13:43:56-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-09-03-fbi-warns-crypto-firms-of-aggressive-social-engineering-attacks

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-crypto-firms-of-aggressive-social-engineering-attacks/){:target="_blank" rel="noopener"}

> The FBI warns of North Korean hackers aggressively targeting cryptocurrency companies and their employees in sophisticated social engineering attacks, aiming to deploy malware that steals their crypto assets. [...]
