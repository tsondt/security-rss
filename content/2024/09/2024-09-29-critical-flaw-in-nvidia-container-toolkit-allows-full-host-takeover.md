Title: Critical flaw in NVIDIA Container Toolkit allows full host takeover
Date: 2024-09-29T10:23:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Artificial Intelligence;Cloud
Slug: 2024-09-29-critical-flaw-in-nvidia-container-toolkit-allows-full-host-takeover

[Source](https://www.bleepingcomputer.com/news/security/critical-flaw-in-nvidia-container-toolkit-allows-full-host-takeover/){:target="_blank" rel="noopener"}

> A critical vulnerability in NVIDIA Container Toolkit impacts all AI applications in a cloud or on-premise environment that rely on it to access GPU resources. [...]
