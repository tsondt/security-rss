Title: Feeld dating app's security too open-minded as private data swings into public view
Date: 2024-09-13T18:22:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-13-feeld-dating-apps-security-too-open-minded-as-private-data-swings-into-public-view

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/13/feeld_dating_app_failures/){:target="_blank" rel="noopener"}

> No love for months-long wait to fix this, either Security researchers have revealed a litany of failures in the Feeld dating app that could be abused to access all manner of private user data, including the most sensitive images not intended to be kept or shared.... [...]
