Title: D-Link fixes critical RCE, hardcoded password flaws in WiFi 6 routers
Date: 2024-09-16T10:24:54-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2024-09-16-d-link-fixes-critical-rce-hardcoded-password-flaws-in-wifi-6-routers

[Source](https://www.bleepingcomputer.com/news/security/d-link-fixes-critical-rce-hardcoded-password-flaws-in-wifi-6-routers/){:target="_blank" rel="noopener"}

> D-Link has fixed critical vulnerabilities in three popular wireless router models that allow remote attackers to execute arbitrary code or access the devices using hardcoded credentials. [...]
