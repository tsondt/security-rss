Title: Microsoft overhauls security for publishing Edge extensions
Date: 2024-09-30T17:49:48-04:00
Author: Mayank Parmar
Category: BleepingComputer
Tags: Microsoft;Security;Software
Slug: 2024-09-30-microsoft-overhauls-security-for-publishing-edge-extensions

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-overhauls-security-for-publishing-edge-extensions/){:target="_blank" rel="noopener"}

> Microsoft has introduced an updated version of the "Publish API for Edge extension developers" that increases the security for developer accounts and the updating of browser extensions. [...]
