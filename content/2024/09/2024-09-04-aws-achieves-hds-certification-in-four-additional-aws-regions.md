Title: AWS achieves HDS certification in four additional AWS Regions
Date: 2024-09-04T20:10:58+00:00
Author: Janice Leung
Category: AWS Security
Tags: Announcements;Compliance;Foundational (100);Security, Identity, & Compliance;Auditing;AWS security;HDS;HDS certification;Healthcare;Security;Security Blog
Slug: 2024-09-04-aws-achieves-hds-certification-in-four-additional-aws-regions

[Source](https://aws.amazon.com/blogs/security/aws-achieves-hds-certification-in-four-additional-aws-regions/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce that four additional AWS Regions—Asia Pacific (Hong Kong), Asia Pacific (Osaka), Asia Pacific (Hyderabad), and Israel (Tel Aviv)—have been granted the Health Data Hosting (Hébergeur de Données de Santé, HDS) certification, increasing the scope to 24 global AWS Regions. The Agence du Numérique en Santé (ANS), the French [...]
