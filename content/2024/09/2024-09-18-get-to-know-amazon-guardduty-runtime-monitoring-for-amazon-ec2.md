Title: Get to know Amazon GuardDuty Runtime Monitoring for Amazon EC2
Date: 2024-09-18T13:03:46+00:00
Author: Scott Ward
Category: AWS Security
Tags: Amazon GuardDuty;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2024-09-18-get-to-know-amazon-guardduty-runtime-monitoring-for-amazon-ec2

[Source](https://aws.amazon.com/blogs/security/get-to-know-amazon-guardduty-runtime-monitoring-for-amazon-ec2/){:target="_blank" rel="noopener"}

> In this blog post, I take you on a deep dive into Amazon GuardDuty Runtime Monitoring for EC2 instances and key capabilities that are part of the feature. Throughout the post, I provide insights around deployment strategies for Runtime Monitoring and detail how it can deliver security value by detecting threats against your Amazon Elastic [...]
