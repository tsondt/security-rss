Title: To patch this server, we need to get someone drunk
Date: 2024-09-06T07:28:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-09-06-to-patch-this-server-we-need-to-get-someone-drunk

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/06/on_call/){:target="_blank" rel="noopener"}

> When maintenance windows are hard to open, a little lubrication helps On Call The Register understands consuming alcohol is quite a popular way to wind down from the working week, but each Friday we get the party started early with a new and sober instalment of On Call, the reader contributed column in which you share stories about the emotional hangovers you've earned delivering tech support.... [...]
