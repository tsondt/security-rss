Title: Payment gateway data breach affects 1.7 million credit card owners
Date: 2024-09-09T10:34:45-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-09-payment-gateway-data-breach-affects-17-million-credit-card-owners

[Source](https://www.bleepingcomputer.com/news/security/payment-gateway-data-breach-affects-17-million-credit-card-owners/){:target="_blank" rel="noopener"}

> Payment gateway provider Slim CD has disclosed a data breach that compromised credit card and personal data belonging to almost 1.7 million individuals. [...]
