Title: Discord rolls out end-to-end encryption for audio, video calls
Date: 2024-09-18T18:04:32-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2024-09-18-discord-rolls-out-end-to-end-encryption-for-audio-video-calls

[Source](https://www.bleepingcomputer.com/news/security/discord-rolls-out-end-to-end-encryption-for-audio-video-calls/){:target="_blank" rel="noopener"}

> Discord has introduced the DAVE protocol, a custom end-to-end encryption (E2EE) protocol designed to protect audio and video calls on the platform from unauthorized interceptions. [...]
