Title: New whitepaper available: Building security from the ground up with Secure by Design
Date: 2024-09-12T16:58:26+00:00
Author: Bertram Dorn
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Application security;Automation;Cloud security;Cybersecurity awareness;Security;Security Blog;Whitepaper
Slug: 2024-09-12-new-whitepaper-available-building-security-from-the-ground-up-with-secure-by-design

[Source](https://aws.amazon.com/blogs/security/new-whitepaper-available-building-security-from-the-ground-up-with-secure-by-design/){:target="_blank" rel="noopener"}

> Developing secure products and services is imperative for organizations that are looking to strengthen operational resilience and build customer trust. However, system design often prioritizes performance, functionality, and user experience over security. This approach can lead to vulnerabilities across the supply chain. As security threats continue to evolve, the concept of Secure by Design (SbD) [...]
