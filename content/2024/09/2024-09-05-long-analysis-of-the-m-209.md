Title: Long Analysis of the M-209
Date: 2024-09-05T11:05:49+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;encryption;history of cryptography;reports
Slug: 2024-09-05-long-analysis-of-the-m-209

[Source](https://www.schneier.com/blog/archives/2024/09/long-analysis-of-the-m-209.html){:target="_blank" rel="noopener"}

> Really interesting analysis of the American M-209 encryption device and its security. [...]
