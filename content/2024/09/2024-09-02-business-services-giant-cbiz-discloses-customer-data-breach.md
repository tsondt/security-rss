Title: Business services giant CBIZ discloses customer data breach
Date: 2024-09-02T11:34:39-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-02-business-services-giant-cbiz-discloses-customer-data-breach

[Source](https://www.bleepingcomputer.com/news/security/business-services-giant-cbiz-discloses-customer-data-breach/){:target="_blank" rel="noopener"}

> CBIZ Benefits & Insurance Services (CBIZ) has disclosed a data breach that involves unauthorized access of client information stored in specific databases. [...]
