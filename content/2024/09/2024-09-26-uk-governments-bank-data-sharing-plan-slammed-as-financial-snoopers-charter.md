Title: UK government's bank data sharing plan slammed as 'financial snoopers' charter'
Date: 2024-09-26T08:31:06+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2024-09-26-uk-governments-bank-data-sharing-plan-slammed-as-financial-snoopers-charter

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/26/uk_benefits_bank_accounts/){:target="_blank" rel="noopener"}

> Access to account info needed to tackle benefit fraud, latest bill claims Privacy campaigners are criticizing UK proposals to force banks to share data from the accounts of government benefit claimants, saying the ploy amounts to "a financial snoopers' charter targeted to automate suspicion."... [...]
