Title: Media giant AFP hit by cyberattack impacting news delivery services
Date: 2024-09-30T10:19:59-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-30-media-giant-afp-hit-by-cyberattack-impacting-news-delivery-services

[Source](https://www.bleepingcomputer.com/news/security/media-giant-afp-hit-by-cyberattack-impacting-news-delivery-services/){:target="_blank" rel="noopener"}

> Global news agency AFP (Agence France-Presse) is warning that it suffered a cyberattack on Friday, which impacted IT systems and content delivery services for its partners. [...]
