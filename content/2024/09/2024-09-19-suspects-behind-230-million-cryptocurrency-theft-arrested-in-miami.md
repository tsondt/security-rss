Title: Suspects behind $230 million cryptocurrency theft arrested in Miami
Date: 2024-09-19T18:57:01-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-19-suspects-behind-230-million-cryptocurrency-theft-arrested-in-miami

[Source](https://www.bleepingcomputer.com/news/security/suspects-behind-230-million-cryptocurrency-theft-arrested-in-miami/){:target="_blank" rel="noopener"}

> Two suspects were arrested in Miami this week and charged with conspiracy to steal and launder over $230 million in cryptocurrency using crypto exchanges and mixing services. [...]
