Title: RansomHub ransomware abuses Kaspersky TDSSKiller to disable EDR software
Date: 2024-09-10T14:29:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-10-ransomhub-ransomware-abuses-kaspersky-tdsskiller-to-disable-edr-software

[Source](https://www.bleepingcomputer.com/news/security/ransomhub-ransomware-abuses-kaspersky-tdsskiller-to-disable-edr-software/){:target="_blank" rel="noopener"}

> The RansomHub ransomware gang has been using TDSSKiller, a legitimate tool from Kaspersky, to disable endpoint detection and response (EDR) services on target systems. [...]
