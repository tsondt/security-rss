Title: Transport for London discloses ongoing “cyber security incident”
Date: 2024-09-02T14:20:46-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-02-transport-for-london-discloses-ongoing-cyber-security-incident

[Source](https://www.bleepingcomputer.com/news/security/transport-for-london-discloses-ongoing-cyber-security-incident/){:target="_blank" rel="noopener"}

> Transport for London (TfL), the city's transport authority, is investigating an ongoing cyberattack that has yet to impact its services. [...]
