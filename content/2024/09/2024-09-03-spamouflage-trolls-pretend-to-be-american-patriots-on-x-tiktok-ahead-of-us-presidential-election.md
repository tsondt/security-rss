Title: Spamouflage trolls pretend to be American patriots on X, TikTok ahead of US presidential election
Date: 2024-09-03T18:15:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-03-spamouflage-trolls-pretend-to-be-american-patriots-on-x-tiktok-ahead-of-us-presidential-election

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/03/spamouflage_trolls_us_elections/){:target="_blank" rel="noopener"}

> No, Abbey is not really a "pure patriotic girl" Spamouflage, the Beijing-linked trolls known for spreading fake news about American politics, is back with new accounts on X and TikTok that claim to be frustrated US voters in "more aggressive" attempts to influence the upcoming presidential election.... [...]
