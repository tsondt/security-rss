Title: Managing identity source transition for AWS IAM Identity Center
Date: 2024-09-25T17:53:12+00:00
Author: Xiaoxue Xu
Category: AWS Security
Tags: Advanced (300);AWS IAM Identity Center;Security, Identity, & Compliance;Technical How-to;Compliance;Security Blog
Slug: 2024-09-25-managing-identity-source-transition-for-aws-iam-identity-center

[Source](https://aws.amazon.com/blogs/security/managing-identity-source-transition-for-aws-iam-identity-center/){:target="_blank" rel="noopener"}

> AWS IAM Identity Center manages user access to Amazon Web Services (AWS) resources, including both AWS accounts and applications. You can use IAM Identity Center to create and manage user identities within the Identity Center identity store or to connect seamlessly to other identity sources. Organizations might change the configuration of their identity source in [...]
