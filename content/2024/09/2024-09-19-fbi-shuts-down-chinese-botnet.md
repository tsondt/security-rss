Title: FBI Shuts Down Chinese Botnet
Date: 2024-09-19T15:40:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;botnets;China;hacking
Slug: 2024-09-19-fbi-shuts-down-chinese-botnet

[Source](https://www.schneier.com/blog/archives/2024/09/fbi-shuts-down-chinese-botnet.html){:target="_blank" rel="noopener"}

> The FBI has shut down a botnet run by Chinese hackers: The botnet malware infected a number of different types of internet-connected devices around the world, including home routers, cameras, digital video recorders, and NAS drives. Those devices were used to help infiltrate sensitive networks related to universities, government agencies, telecommunications providers, and media organizations.... The botnet was launched in mid-2021, according to the FBI, and infected roughly 260,000 devices as of June 2024. The operation to dismantle the botnet was coordinated by the FBI, the NSA, and the Cyber National Mission Force (CNMF), according to a press release dated [...]
