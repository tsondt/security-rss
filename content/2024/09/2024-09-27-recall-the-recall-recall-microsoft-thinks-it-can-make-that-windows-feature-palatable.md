Title: Recall the Recall recall? Microsoft thinks it can make that Windows feature palatable
Date: 2024-09-27T20:18:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-09-27-recall-the-recall-recall-microsoft-thinks-it-can-make-that-windows-feature-palatable

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/27/microsoft_has_some_thoughts_about/){:target="_blank" rel="noopener"}

> AI screengrab service to be opt-in, features encryption, biometrics, enclaves, more Microsoft has revised the Recall feature for its Copilot+ PCs and insists that the self-surveillance system is secure.... [...]
