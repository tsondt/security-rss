Title: Major sales and ops overhaul leads to much more activity ... for Meow ransomware gang
Date: 2024-09-11T18:45:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-11-major-sales-and-ops-overhaul-leads-to-much-more-activity-for-meow-ransomware-gang

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/11/meow_ransomware_ops_revamp_more_attacks/){:target="_blank" rel="noopener"}

> You hate to see it The Meow ransomware group has grabbed the second most active gang spot in an unexpected surge in activity following a major brand overhaul.... [...]
