Title: Ever wonder how crooks get the credentials to unlock stolen phones?
Date: 2024-09-19T21:41:34+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;lost phone;phishing;phishing-as-a-service;phone theft
Slug: 2024-09-19-ever-wonder-how-crooks-get-the-credentials-to-unlock-stolen-phones

[Source](https://arstechnica.com/?p=2051165){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) A coalition of law-enforcement agencies said it shut down a service that facilitated the unlocking of more than 1.2 million stolen or lost mobile phones so they could be used by someone other than their rightful owner. The service was part of iServer, a phishing-as-a-service platform that has been operating since 2018. The Argentina-based iServer sold access to a platform that offered a host of phishing-related services through email, texts, and voice calls. One of the specialized services offered was designed to help people in possession of large numbers of stolen or lost mobile devices to [...]
