Title: Ex-senior New York State staffer charged in cash-for-favors scandal with China
Date: 2024-09-04T00:53:37+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-04-ex-senior-new-york-state-staffer-charged-in-cash-for-favors-scandal-with-china

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/04/new_york_aide_china_agent/){:target="_blank" rel="noopener"}

> Bagging two posh properties, three luxury cars on a govt salary a bit of a giveaway – allegedly The US Department of Justice has accused a now-former senior official of the New York State government of illegally advancing the interests of the Chinese government and communist party.... [...]
