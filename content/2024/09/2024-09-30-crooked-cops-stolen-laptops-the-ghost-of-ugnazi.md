Title: Crooked Cops, Stolen Laptops & the Ghost of UGNazi
Date: 2024-09-30T21:33:10+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;SIM Swapping;Web Fraud 2.0;Assad Faiq;CosmoTheGod;Dream Agency;Enzo Zellocchi;Eric Taylor;Facebook;fbi;Iris Au;Kenneth Childs;Ledger breach;Mir Islam;Rise Agency;Shopify;Tassilo Heinrich;The Godfather;Tomi Masters;Troy Woody Jr.;Vurg;Zort
Slug: 2024-09-30-crooked-cops-stolen-laptops-the-ghost-of-ugnazi

[Source](https://krebsonsecurity.com/2024/09/crooked-cops-stolen-laptops-the-ghost-of-ugnazi/){:target="_blank" rel="noopener"}

> A California man accused of failing to pay taxes on tens of millions of dollars allegedly earned from cybercrime also paid local police officers hundreds of thousands of dollars to help him extort, intimidate and silence rivals and former business partners, the government alleges. KrebsOnSecurity has learned that many of the man’s alleged targets were members of UGNazi, a hacker group behind multiple high-profile breaches and cyberattacks back in 2012. A photo released by the government allegedly showing Iza posing with several LASD officers on his payroll. A federal complaint (PDF) filed last week said the Federal Bureau of Investigation [...]
