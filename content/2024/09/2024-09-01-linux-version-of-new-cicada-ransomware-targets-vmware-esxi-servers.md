Title: Linux version of new Cicada ransomware targets VMware ESXi servers
Date: 2024-09-01T10:14:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-01-linux-version-of-new-cicada-ransomware-targets-vmware-esxi-servers

[Source](https://www.bleepingcomputer.com/news/security/linux-version-of-new-cicada-ransomware-targets-vmware-esxi-servers/){:target="_blank" rel="noopener"}

> A new ransomware-as-a-service (RaaS) operation named Cicada3301 has already listed 19 victims on its extortion portal, as it quickly attacked companies worldwide. [...]
