Title: Meta fixes easily bypassed WhatsApp ‘View Once’ privacy feature
Date: 2024-09-09T13:40:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-09-meta-fixes-easily-bypassed-whatsapp-view-once-privacy-feature

[Source](https://www.bleepingcomputer.com/news/security/meta-fixes-easily-bypassed-whatsapp-view-once-privacy-feature/){:target="_blank" rel="noopener"}

> A privacy flaw in WhatsApp, an instant messenger with over 2 billion users worldwide, is being exploited by attackers to bypass the app's "View once" feature and view messages again. [...]
