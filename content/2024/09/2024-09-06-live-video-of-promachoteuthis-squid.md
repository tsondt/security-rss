Title: Live Video of Promachoteuthis Squid
Date: 2024-09-06T21:09:43+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid;video
Slug: 2024-09-06-live-video-of-promachoteuthis-squid

[Source](https://www.schneier.com/blog/archives/2024/09/live-video-of-promachoteuthis-squid.html){:target="_blank" rel="noopener"}

> The first live video of the Promachoteuthis squid, filmed at a newly discovered seamount off the coast of Chile. Blog moderation policy. [...]
