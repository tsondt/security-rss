Title: Systems used by courts and governments across the US riddled with vulnerabilities
Date: 2024-09-30T20:30:26+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;courts;governments;vulnerabilities
Slug: 2024-09-30-systems-used-by-courts-and-governments-across-the-us-riddled-with-vulnerabilities

[Source](https://arstechnica.com/?p=2053460){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Public records systems that courts and governments rely on to manage voter registrations and legal filings have been riddled with vulnerabilities that made it possible for attackers to falsify registration databases and add, delete, or modify official documents. Over the past year, software developer turned security researcher Jason Parker has found and reported dozens of critical vulnerabilities in no fewer than 19 commercial platforms used by hundreds of courts, government agencies, and police departments across the country. Most of the vulnerabilities were critical. One flaw he uncovered in the voter registration cancellation portal for the state [...]
