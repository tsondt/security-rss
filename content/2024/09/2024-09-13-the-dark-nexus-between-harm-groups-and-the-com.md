Title: The Dark Nexus Between Harm Groups and ‘The Com’
Date: 2024-09-13T12:16:45+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Breadcrumbs;Ne'er-Do-Well News;SIM Swapping;Web Fraud 2.0;2992;303;404;545;555;6996;7997;8884;Advance Auto Parts;Ali Winston;ALPHV;Aspertaine;AT&T;Beige Group;Black Cat;Bloomberg;BreachForums;ChumLul;Court;CrowdStrike;Cultist;CVLT;Der Spiegel;Discord;Doxbin;EA Games;H3ll;Harm Nation;James Thomas Andrew McCarty;Judische;Kalana Limkin;Kaskar;Kayte;Kingbob;KT;Kya Christian Nelson;Leak Society;Lending Tree;Mandiant;mark rasch;MGM Resorts;microsoft;Minecraft;Neiman Marcus;Nicholas "Convict" Ceraolo;NMK;Noah Michael Urban;NVIDIA;Okta;pompompurin;RCMP;Recorder;Roblox;Royal Canadian Mounted Police;Sagar "Weep" Singh;Samsung;Santander Bank;Scattered Spider;Slit Town;Snowflake;Sosa;Star Chat;Steam;SWATting;T-Mobile;telegram;The Com;The Washington Post;Ticketmaster;Twitch;U.S. Department of Justice;U.S. Drug Enforcement Agency;UNC5537;Unit 221B;ViLE;violence-as-a-service;vSphere;Waifu;wired
Slug: 2024-09-13-the-dark-nexus-between-harm-groups-and-the-com

[Source](https://krebsonsecurity.com/2024/09/the-dark-nexus-between-harm-groups-and-the-com/){:target="_blank" rel="noopener"}

> A cyberattack that shut down two of the top casinos in Las Vegas last year quickly became one of the most riveting security stories of 2023. It was the first known case of native English-speaking hackers in the United States and Britain teaming up with ransomware gangs based in Russia. But that made-for-Hollywood narrative has eclipsed a far more hideous trend: Many of these young, Western cybercriminals are also members of fast-growing online groups that exist solely to bully, stalk, harass and extort vulnerable teens into physically harming themselves and others. Image: Shutterstock. In September 2023, a Russian ransomware group [...]
