Title: Rogue WHOIS server gives researcher superpowers no one should ever have
Date: 2024-09-11T10:00:40+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Features;Security;HTTPS;tls;trust;WHOIS
Slug: 2024-09-11-rogue-whois-server-gives-researcher-superpowers-no-one-should-ever-have

[Source](https://arstechnica.com/?p=2048683){:target="_blank" rel="noopener"}

> Enlarge (credit: Aurich Lawson | Getty Images) It’s not every day that a security researcher acquires the ability to generate counterfeit HTTPS certificates, track email activity, and the position to execute code of his choice on thousands of servers—all in a single blow that cost only $20 and a few minutes to land. But that’s exactly what happened recently to Benjamin Harris. Harris, the CEO and founder of security firm watchTowr, did all of this by registering the domain dotmobiregistry.net. The domain was once the official home of the authoritative WHOIS server for.mobi, a top-level domain used to indicate that [...]
