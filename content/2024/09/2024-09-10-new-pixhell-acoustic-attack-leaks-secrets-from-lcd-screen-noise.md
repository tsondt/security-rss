Title: New PIXHELL acoustic attack leaks secrets from LCD screen noise
Date: 2024-09-10T15:23:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-10-new-pixhell-acoustic-attack-leaks-secrets-from-lcd-screen-noise

[Source](https://www.bleepingcomputer.com/news/security/new-pixhell-acoustic-attack-leaks-secrets-from-lcd-screen-noise/){:target="_blank" rel="noopener"}

> A novel acoustic attack named 'PIXHELL' can leak secrets from air-gapped and audio-gapped systems, and without requiring speakers, through the LCD monitors they connect to. [...]
