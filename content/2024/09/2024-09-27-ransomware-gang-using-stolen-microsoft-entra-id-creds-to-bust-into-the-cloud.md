Title: Ransomware gang using stolen Microsoft Entra ID creds to bust into the cloud
Date: 2024-09-27T13:35:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-27-ransomware-gang-using-stolen-microsoft-entra-id-creds-to-bust-into-the-cloud

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/27/microsoft_storm_0501/){:target="_blank" rel="noopener"}

> Defenders beware: Data theft, extortion, and backdoors on Storm-0501's agenda Microsoft's latest threat intelligence blog issues a warning to all organizations about Storm-0501's recent shift in tactics, targeting, and backdooring hybrid cloud environments.... [...]
