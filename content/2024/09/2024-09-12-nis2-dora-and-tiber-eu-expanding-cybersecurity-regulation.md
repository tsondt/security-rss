Title: NIS2, DORA, and Tiber-EU expanding cybersecurity regulation
Date: 2024-09-12T08:57:15+00:00
Author: Annaliese Ingrams
Category: The Register
Tags: 
Slug: 2024-09-12-nis2-dora-and-tiber-eu-expanding-cybersecurity-regulation

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/12/nis2_dora_and_tibereu_expanding/){:target="_blank" rel="noopener"}

> Get essential insights for IT security compliance and effectiveness from SANS Webinar As cybersecurity threats evolve, so do the regulations designed to protect businesses.... [...]
