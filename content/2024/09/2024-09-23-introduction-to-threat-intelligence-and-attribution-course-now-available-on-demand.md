Title: Introduction to Threat Intelligence and Attribution course, now available on-demand
Date: 2024-09-23T16:00:00+00:00
Author: Brett Reschke
Category: GCP Security
Tags: Security & Identity
Slug: 2024-09-23-introduction-to-threat-intelligence-and-attribution-course-now-available-on-demand

[Source](https://cloud.google.com/blog/products/identity-security/introduction-to-intelligence-and-attribution-course-now-on-demand/){:target="_blank" rel="noopener"}

> Ask 10 cybersecurity experts to define “attribution” and they would likely provide as many different answers. The term has become an industry buzzword for the process by which evidence of a breach is converted into a public disclosure naming the attackers responsible. In reality, attribution is the result of intelligence analysis and it can help organizations understand who might target them for a cyberattack and why they would be targeted. Google Threat Intelligence and Google Cloud Security proudly announce the latest edition of “Introduction to Threat Intelligence and Attribution,” now available on-demand through Mandiant Academy. This is the latest course [...]
