Title: Australia’s government spent the week boxing Big Tech
Date: 2024-09-13T04:47:55+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-09-13-australias-government-spent-the-week-boxing-big-tech

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/13/australia_vs_big_tech/){:target="_blank" rel="noopener"}

> With social media age limits, anti-scam laws, privacy tweaks, and misinformation rules Elon Musk labelled 'fascist' Australia's government has spent the week reining in Big Tech.... [...]
