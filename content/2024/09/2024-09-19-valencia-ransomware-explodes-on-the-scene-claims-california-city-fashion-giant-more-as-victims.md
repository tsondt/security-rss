Title: Valencia Ransomware explodes on the scene, claims California city, fashion giant, more as victims
Date: 2024-09-19T23:24:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-19-valencia-ransomware-explodes-on-the-scene-claims-california-city-fashion-giant-more-as-victims

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/19/valencia_ransomware_california_city/){:target="_blank" rel="noopener"}

> Boasts 'appear to be credible' experts tell El Reg A California city, a Spanish fashion giant, an Indian paper manufacturer, and two pharmaceutical companies are the alleged victims of what looks like a new ransomware gang that started leaking stolen info this week.... [...]
