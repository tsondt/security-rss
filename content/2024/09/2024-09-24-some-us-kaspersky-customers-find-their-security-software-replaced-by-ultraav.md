Title: Some US Kaspersky customers find their security software replaced by 'UltraAV'
Date: 2024-09-24T01:01:12+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-24-some-us-kaspersky-customers-find-their-security-software-replaced-by-ultraav

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/24/ultraav_kaspersky_antivirus/){:target="_blank" rel="noopener"}

> Back story to replacement for banned security app isn't enormously reassuring Some US-based users of Kaspersky antivirus products have found their software replaced by product from by a low-profile entity named "UltraAV" – a change they didn't ask for, and which has delivered them untested and largely unknown software from a source with a limited track record.... [...]
