Title: Ransomware gangs now abuse Microsoft Azure tool for data theft
Date: 2024-09-17T12:14:59-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud
Slug: 2024-09-17-ransomware-gangs-now-abuse-microsoft-azure-tool-for-data-theft

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gangs-now-abuse-microsoft-azure-tool-for-data-theft/){:target="_blank" rel="noopener"}

> Ransomware gangs like BianLian and Rhysida increasingly use Microsoft's Azure Storage Explorer and AzCopy to steal data from breached networks and store it in Azure Blob storage. [...]
