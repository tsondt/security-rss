Title: Despite cyberattacks, water security standards remain a pipe dream
Date: 2024-09-07T12:33:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-07-despite-cyberattacks-water-security-standards-remain-a-pipe-dream

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/07/us_water_cyberattacks/){:target="_blank" rel="noopener"}

> White House floats round two of regulations Feature It sounds like the start of a bad joke: Digital trespassers from China, Russia, and Iran break into US water systems.... [...]
