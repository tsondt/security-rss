Title: Refine unused access using IAM Access Analyzer recommendations
Date: 2024-09-18T19:09:34+00:00
Author: Stéphanie Mbappe
Category: AWS Security
Tags: AWS IAM Access Analyzer;AWS Identity and Access Management (IAM);Intermediate (200);Security, Identity, & Compliance;Technical How-to;AWS IAM;AWS IAM policies;AWS Identity and Access Management;IAM Access Analyzer;IAM policies;least privilege;Security Blog
Slug: 2024-09-18-refine-unused-access-using-iam-access-analyzer-recommendations

[Source](https://aws.amazon.com/blogs/security/refine-unused-access-using-iam-access-analyzer-recommendations/){:target="_blank" rel="noopener"}

> As a security team lead, your goal is to manage security for your organization at scale and ensure that your team follows AWS Identity and Access Management (IAM) security best practices, such as the principle of least privilege. As your developers build on AWS, you need visibility across your organization to make sure that teams [...]
