Title: WhatsApp's 'View Once' could be 'View Whenever' due to a flaw
Date: 2024-09-09T22:15:09+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-09-whatsapps-view-once-could-be-view-whenever-due-to-a-flaw

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/09/whatsapp_view_once_flaw/){:target="_blank" rel="noopener"}

> It promised vanishing messages, but now 'it's privacy theater' Video A popular privacy feature in WhatsApp is "completely broken and can be trivially bypassed," according to developers at cryptowallet startup Zengo.... [...]
