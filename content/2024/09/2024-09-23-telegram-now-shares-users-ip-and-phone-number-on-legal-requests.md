Title: Telegram now shares users’ IP and phone number on legal requests
Date: 2024-09-23T15:33:36-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-23-telegram-now-shares-users-ip-and-phone-number-on-legal-requests

[Source](https://www.bleepingcomputer.com/news/security/telegram-now-shares-users-ip-and-phone-number-on-legal-requests/){:target="_blank" rel="noopener"}

> Telegram will now share users' phone numbers and IP addresses with law enforcement if they are found to be violating the platform's rules following a valid legal request. [...]
