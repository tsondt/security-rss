Title: Tor insists its network is safe after German cops convict CSAM dark-web admin
Date: 2024-09-19T06:39:06+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-19-tor-insists-its-network-is-safe-after-german-cops-convict-csam-dark-web-admin

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/19/tor_police_germany/){:target="_blank" rel="noopener"}

> Outdated software blamed for cracks in the armor The Tor project has insisted its privacy-preserving powers remain potent, countering German reports that user anonymity on its network can be and has been compromised by police.... [...]
