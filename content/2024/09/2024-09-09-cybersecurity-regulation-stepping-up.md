Title: Cybersecurity regulation stepping up
Date: 2024-09-09T09:00:16+00:00
Author: Annaliese Ingrams
Category: The Register
Tags: 
Slug: 2024-09-09-cybersecurity-regulation-stepping-up

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/09/cybersecurity_regulation_stepping_up/){:target="_blank" rel="noopener"}

> Understanding new NIS2, DORA, and Tiber-EU legislation is essential to improving IT security, explains SANS Webinar As cybersecurity regulations tighten, organisations face new challenges that require more than just compliance checklists.... [...]
