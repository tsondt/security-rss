Title: Iran's cyber-goons emailed stolen Trump info to Team Biden – which ignored them
Date: 2024-09-19T20:46:22+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-19-irans-cyber-goons-emailed-stolen-trump-info-to-team-biden-which-ignored-them

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/19/iran_trump_hack_info_biden/){:target="_blank" rel="noopener"}

> To be fair, Joe was probably taking a nap The Iranian cyber snoops who stole files from the Trump campaign, with the intention of leaking those documents, tried to slip the data to the Biden camp — but were apparently ignored, according to Uncle Sam.... [...]
