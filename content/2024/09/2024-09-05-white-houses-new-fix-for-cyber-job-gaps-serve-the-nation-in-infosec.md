Title: White House’s new fix for cyber job gaps: Serve the nation in infosec
Date: 2024-09-05T22:04:05+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-09-05-white-houses-new-fix-for-cyber-job-gaps-serve-the-nation-in-infosec

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/05/white_house_cyber_jobs/){:target="_blank" rel="noopener"}

> Now do your patriotic duty and fill one of those 500k open roles, please? The White House has unveiled a new strategy to fill some of the hundreds of thousands of critical cybersecurity vacancies across the US: Pitch cyber as a national service.... [...]
