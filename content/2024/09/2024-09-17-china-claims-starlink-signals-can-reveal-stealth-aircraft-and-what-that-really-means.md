Title: China claims Starlink signals can reveal stealth aircraft – and what that really means
Date: 2024-09-17T04:29:09+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-17-china-claims-starlink-signals-can-reveal-stealth-aircraft-and-what-that-really-means

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/17/china_starlink_stealth/){:target="_blank" rel="noopener"}

> If this really was that useful, they wouldn't be telling us According to a Chinese state-sanctioned study, signals from SpaceX Starlink broadband internet satellites could be used to track US stealth fighters, such as the F-22.... [...]
