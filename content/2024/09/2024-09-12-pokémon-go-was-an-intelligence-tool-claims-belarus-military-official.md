Title: Pokémon GO was an intelligence tool, claims Belarus military official
Date: 2024-09-12T06:32:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-09-12-pokémon-go-was-an-intelligence-tool-claims-belarus-military-official

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/12/pokemon_go_spying_belarus_claims/){:target="_blank" rel="noopener"}

> Augmented reality meets warped reality A defense ministry official from Belarus has claimed augmented reality game Pokémon GO was a tool of Western intelligence agencies.... [...]
