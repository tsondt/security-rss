Title: Feds charge 3 Iranians with 'hack-and-leak' of Trump 2024 campaign
Date: 2024-09-27T21:45:04+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-27-feds-charge-3-iranians-with-hack-and-leak-of-trump-2024-campaign

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/27/us_charges_iran_trump_campaign_hack/){:target="_blank" rel="noopener"}

> Snoops allegedly camped out in inboxes well into September The US Department of Justice has charged three Iranians for their involvement in a "wide-ranging hacking campaign" during which they allegedly stole massive amounts of materials from Donald Trump's 2024 presidential campaign and then leaked the information to media organizations.... [...]
