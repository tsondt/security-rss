Title: So you paid a ransom demand … and now the decryptor doesn't work
Date: 2024-09-11T13:30:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-11-so-you-paid-a-ransom-demand-and-now-the-decryptor-doesnt-work

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/11/ransomware_decryptor_not_working/){:target="_blank" rel="noopener"}

> A really big oh sh*t moment, for sure For C-suite execs and security leaders, discovering your organization has been breached by network intruders, your critical systems locked up, and your data stolen, and then receiving a ransom demand, is probably the worst day of your professional life.... [...]
