Title: Mind your header! There's nothing refreshing about phishers' latest tactic
Date: 2024-09-12T09:15:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-12-mind-your-header-theres-nothing-refreshing-about-phishers-latest-tactic

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/12/http_headers/){:target="_blank" rel="noopener"}

> It could lead to a costly BEC situation Palo Alto's Unit 42 threat intel team wants to draw the security industry's attention to an increasingly common tactic used by phishers to harvest victims' credentials.... [...]
