Title: List of Old NSA Training Videos
Date: 2024-09-03T16:03:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptography;FOIA;history of cryptography;history of security;NSA;videos
Slug: 2024-09-03-list-of-old-nsa-training-videos

[Source](https://www.schneier.com/blog/archives/2024/09/list-of-old-nsa-training-videos.html){:target="_blank" rel="noopener"}

> The NSA’s “ National Cryptographic School Television Catalogue ” from 1991 lists about 600 COMSEC and SIGINT training videos. There are a bunch explaining the operations of various cryptographic equipment, and a few code words I have never heard of before. [...]
