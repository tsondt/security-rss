Title: Bug Left Some Windows PCs Dangerously Unpatched
Date: 2024-09-10T21:46:21+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;CVE-2024-38217;CVE-2024-38226;CVE-2024-43491;Immersive Labs;Kev Breen;mark of the web;microsoft;Microsoft Office;Patch Tuesday September 2024;Rapid7;Satnam Narang
Slug: 2024-09-10-bug-left-some-windows-pcs-dangerously-unpatched

[Source](https://krebsonsecurity.com/2024/09/bug-left-some-windows-pcs-dangerously-unpatched/){:target="_blank" rel="noopener"}

> Microsoft Corp. today released updates to fix at least 79 security vulnerabilities in its Windows operating systems and related software, including multiple flaws that are already showing up in active attacks. Microsoft also corrected a critical bug that has caused some Windows 10 PCs to remain dangerously unpatched against actively exploited vulnerabilities for several months this year. By far the most curious security weakness Microsoft disclosed today has the snappy name of CVE-2024-43491, which Microsoft says is a vulnerability that led to the rolling back of fixes for some vulnerabilities affecting “optional components” on certain Windows 10 systems produced in [...]
