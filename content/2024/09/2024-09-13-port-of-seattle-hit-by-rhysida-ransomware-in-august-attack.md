Title: Port of Seattle hit by Rhysida ransomware in August attack
Date: 2024-09-13T18:54:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-13-port-of-seattle-hit-by-rhysida-ransomware-in-august-attack

[Source](https://www.bleepingcomputer.com/news/security/port-of-seattle-says-rhysida-ransomware-was-behind-august-attack/){:target="_blank" rel="noopener"}

> Port of Seattle, the United States government agency overseeing Seattle's seaport and airport, confirmed on Friday that the Rhysida ransomware operation was behind a cyberattack impacting its systems over the last three weeks. [...]
