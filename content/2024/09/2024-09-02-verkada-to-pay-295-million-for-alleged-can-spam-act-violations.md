Title: Verkada to pay $2.95 million for alleged CAN-SPAM Act violations
Date: 2024-09-02T12:06:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-09-02-verkada-to-pay-295-million-for-alleged-can-spam-act-violations

[Source](https://www.bleepingcomputer.com/news/security/verkada-to-pay-295-million-for-alleged-can-spam-act-violations/){:target="_blank" rel="noopener"}

> The Federal Trade Commission (FTC) requires security camera vendor Verkada to create a comprehensive information security program as part of a settlement after multiple security failures enabled hackers to access live video feeds from internet-connected cameras. [...]
