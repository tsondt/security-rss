Title: YubiKeys are vulnerable to cloning attacks thanks to newly discovered side channel
Date: 2024-09-03T17:58:06+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;2fa;cryptography;ecdsa;elliptic curve digital signature algorithm;encryption;side channels;two-factor authentication
Slug: 2024-09-03-yubikeys-are-vulnerable-to-cloning-attacks-thanks-to-newly-discovered-side-channel

[Source](https://arstechnica.com/?p=2046777){:target="_blank" rel="noopener"}

> Enlarge (credit: Yubico) The YubiKey 5, the most widely used hardware token for two-factor authentication based on the FIDO standard, contains a cryptographic flaw that makes the finger-size device vulnerable to cloning when an attacker gains temporary physical access to it, researchers said Tuesday. The cryptographic flaw, known as a side channel, resides in a small microcontroller used in a large number of other authentication devices, including smartcards used in banking, electronic passports, and the accessing of secure areas. While the researchers have confirmed all YubiKey 5 series models can be cloned, they haven’t tested other devices using the microcontroller, [...]
