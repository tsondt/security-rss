Title: WhatsApp still working on making View Once chats actually disappear for all
Date: 2024-09-18T00:16:06+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-18-whatsapp-still-working-on-making-view-once-chats-actually-disappear-for-all

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/18/whatsapp_view_once_flaw_unfixed/){:target="_blank" rel="noopener"}

> So far it's more like View Forever Updated Meta's efforts to stop people repeatedly viewing WhatsApp’s so-called View Once messages – photos, videos, and voice recordings that disappear from chats after a recipient sees them – so far remain incomplete.... [...]
