Title: How $20 and a lapsed domain allowed security pros to undermine internet integrity
Date: 2024-09-11T11:00:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-11-how-20-and-a-lapsed-domain-allowed-security-pros-to-undermine-internet-integrity

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/11/watchtowr_black_hat_whois/){:target="_blank" rel="noopener"}

> What happens at Black Hat... While trying to escape the Las Vegas heat during Black Hat last month, watchTowr Labs researchers decided to poke around for weaknesses in the WHOIS protocol.... [...]
