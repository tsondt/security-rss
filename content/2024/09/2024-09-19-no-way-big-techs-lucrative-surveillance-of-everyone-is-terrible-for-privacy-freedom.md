Title: No way? Big Tech's 'lucrative surveillance' of everyone is terrible for privacy, freedom
Date: 2024-09-19T21:48:54+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-09-19-no-way-big-techs-lucrative-surveillance-of-everyone-is-terrible-for-privacy-freedom

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/19/social_media_data_harvesting_handling_ftc/){:target="_blank" rel="noopener"}

> Says Lina Khan in latest push to rein in Meta, Google, Amazon and pals Buried beneath the endless feeds and attention-grabbing videos of the modern internet is a network of data harvesting and sale that's perhaps far more vast than most people realize, and it desperately needs regulation.... [...]
