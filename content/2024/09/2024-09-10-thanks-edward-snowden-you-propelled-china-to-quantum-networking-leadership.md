Title: Thanks, Edward Snowden: You propelled China to quantum networking leadership
Date: 2024-09-10T05:30:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-09-10-thanks-edward-snowden-you-propelled-china-to-quantum-networking-leadership

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/10/china_quantum_innovation_itif_report/){:target="_blank" rel="noopener"}

> Beijing aimed research at immediate needs – like blocking leaks – while the US sought abstract knowledge China has an undeniable lead in quantum networking technology – a state of affairs that should give the US pause, despite its lead in quantum computing.... [...]
