Title: Methodology for incident response on generative AI workloads
Date: 2024-09-16T19:32:04+00:00
Author: Anna McAbee
Category: AWS Security
Tags: Generative AI;Intermediate (200);Security, Identity, & Compliance;Security Blog
Slug: 2024-09-16-methodology-for-incident-response-on-generative-ai-workloads

[Source](https://aws.amazon.com/blogs/security/methodology-for-incident-response-on-generative-ai-workloads/){:target="_blank" rel="noopener"}

> The AWS Customer Incident Response Team (CIRT) has developed a methodology that you can use to investigate security incidents involving generative AI-based applications. To respond to security events related to a generative AI workload, you should still follow the guidance and principles outlined in the AWS Security Incident Response Guide. However, generative AI workloads require [...]
