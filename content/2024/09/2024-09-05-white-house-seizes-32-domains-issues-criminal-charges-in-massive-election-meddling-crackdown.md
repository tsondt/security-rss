Title: White House seizes 32 domains, issues criminal charges in massive election-meddling crackdown
Date: 2024-09-05T02:27:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-05-white-house-seizes-32-domains-issues-criminal-charges-in-massive-election-meddling-crackdown

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/05/biden_cracks_down_on_putins/){:target="_blank" rel="noopener"}

> Russia has seemingly decided who it wants Putin the Oval Office The Biden administration on Wednesday seized 32 websites and charged two employees of a state-owned media outlet connected to a $10 million scheme to distribute pro-Kremlin propaganda, and claimed the actions were necessary to counter Russia’s attempts to influence the upcoming US presidential election.... [...]
