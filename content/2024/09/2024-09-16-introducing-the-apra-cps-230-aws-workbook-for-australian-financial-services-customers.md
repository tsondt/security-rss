Title: Introducing the APRA CPS 230 AWS Workbook for Australian financial services customers
Date: 2024-09-16T13:50:11+00:00
Author: Krish De
Category: AWS Security
Tags: Announcements;Financial Services;Foundational (100);Security, Identity, & Compliance;Australia;AWS workbook;Security Blog
Slug: 2024-09-16-introducing-the-apra-cps-230-aws-workbook-for-australian-financial-services-customers

[Source](https://aws.amazon.com/blogs/security/introducing-the-apra-cps-230-aws-workbook-for-australian-financial-services-customers/){:target="_blank" rel="noopener"}

> The Australian Prudential Regulation Authority (APRA) has established the CPS 230 Operational Risk Management standard to verify that regulated entities are resilient to operational risks and disruptions. CPS 230 requires regulated financial entities to effectively manage their operational risks, maintain critical operations during disruptions, and manage the risks associated with service providers. Amazon Web Services [...]
