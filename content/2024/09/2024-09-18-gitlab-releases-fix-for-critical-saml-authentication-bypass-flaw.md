Title: GitLab releases fix for critical SAML authentication bypass flaw
Date: 2024-09-18T14:37:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-18-gitlab-releases-fix-for-critical-saml-authentication-bypass-flaw

[Source](https://www.bleepingcomputer.com/news/security/gitlab-releases-fix-for-critical-saml-authentication-bypass-flaw/){:target="_blank" rel="noopener"}

> GitLab has released security updates to address a critical SAML authentication bypass vulnerability impacting self-managed installations of the GitLab Community Edition (CE) and Enterprise Edition (EE). [...]
