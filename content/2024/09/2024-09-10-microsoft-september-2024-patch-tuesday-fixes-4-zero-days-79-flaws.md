Title: Microsoft September 2024 Patch Tuesday fixes 4 zero-days, 79 flaws
Date: 2024-09-10T13:32:02-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-09-10-microsoft-september-2024-patch-tuesday-fixes-4-zero-days-79-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-september-2024-patch-tuesday-fixes-4-zero-days-79-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's September 2024 Patch Tuesday, which includes security updates for 79 flaws, including four actively exploited and one publicly disclosed zero-days. [...]
