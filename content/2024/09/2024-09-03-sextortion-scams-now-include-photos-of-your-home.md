Title: Sextortion Scams Now Include Photos of Your Home
Date: 2024-09-03T15:45:49+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Web Fraud 2.0;fbi;Google Maps;sextortion
Slug: 2024-09-03-sextortion-scams-now-include-photos-of-your-home

[Source](https://krebsonsecurity.com/2024/09/sextortion-scams-now-include-photos-of-your-home/){:target="_blank" rel="noopener"}

> An old but persistent email scam known as “ sextortion ” has a new personalized touch: The missives, which claim that malware has captured webcam footage of recipients pleasuring themselves, now include a photo of the target’s home in a bid to make threats about publishing the videos more frightening and convincing. This week, several readers reported receiving sextortion emails that addressed them by name and included images of their street or front yard that were apparently lifted from an online mapping application such as Google Maps. The message purports to have been sent from a hacker who’s compromised your [...]
