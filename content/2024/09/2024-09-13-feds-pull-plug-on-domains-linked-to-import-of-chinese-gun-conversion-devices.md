Title: Feds pull plug on domains linked to import of Chinese gun conversion devices
Date: 2024-09-13T01:58:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-09-13-feds-pull-plug-on-domains-linked-to-import-of-chinese-gun-conversion-devices

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/13/gun_switch_domains_seized/){:target="_blank" rel="noopener"}

> Illegal goods allegedly shipped to the US labeled as toys or jewels The US Attorney's Office in the District of Massachusetts has seized more than 350 internet domains allegedly used by Chinese outfits to sell US residents kits that convert semiautomatic pistols into fully automatic guns – and silence them as they fire.... [...]
