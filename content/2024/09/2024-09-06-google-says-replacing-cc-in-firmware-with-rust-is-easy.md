Title: Google says replacing C/C++ in firmware with Rust is easy
Date: 2024-09-06T21:44:14+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-09-06-google-says-replacing-cc-in-firmware-with-rust-is-easy

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/06/google_rust_c_code_language/){:target="_blank" rel="noopener"}

> Not so much when trying to convert coding veterans Google recently rewrote the firmware for protected virtual machines in its Android Virtualization Framework using the Rust programming language and wants you to do the same, assuming you deal with firmware.... [...]
