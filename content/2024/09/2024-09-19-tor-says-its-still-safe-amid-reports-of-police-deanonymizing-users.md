Title: Tor says it’s "still safe" amid reports of police deanonymizing users
Date: 2024-09-19T15:15:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-09-19-tor-says-its-still-safe-amid-reports-of-police-deanonymizing-users

[Source](https://www.bleepingcomputer.com/news/security/tor-says-its-still-safe-amid-reports-of-police-deanonymizing-users/){:target="_blank" rel="noopener"}

> The Tor Project is attempting to assure users that the network is still safe after a recent investigative report warned that law enforcement from Germany and other countries are working together to deanonymize users through timing attacks. [...]
