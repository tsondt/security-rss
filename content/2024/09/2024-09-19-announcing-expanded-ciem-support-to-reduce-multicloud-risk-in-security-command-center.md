Title: Announcing expanded CIEM support to reduce multicloud risk in Security Command Center
Date: 2024-09-19T16:00:00+00:00
Author: Hareesha Tamatam
Category: GCP Security
Tags: Security & Identity
Slug: 2024-09-19-announcing-expanded-ciem-support-to-reduce-multicloud-risk-in-security-command-center

[Source](https://cloud.google.com/blog/products/identity-security/new-ciem-support-in-security-command-center-can-help-reduce-risk/){:target="_blank" rel="noopener"}

> Identities can be a major source of cloud risk when they’re not properly managed. Compromised credentials are frequently used to gain unauthorized access to cloud environments, which often magnifies that risk since many user and service accounts are granted access to cloud services and assets beyond their required scope. This means that if just one credential is stolen by an adversary, or abused by a malicious insider, companies may be at risk of data exfiltration and resource compromise. To help make identity management easier, we have integrated Cloud Infrastructure Entitlement Management (CIEM) into Security Command Center, our multicloud security and [...]
