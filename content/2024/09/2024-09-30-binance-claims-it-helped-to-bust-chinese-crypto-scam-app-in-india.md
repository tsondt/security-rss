Title: Binance claims it helped to bust Chinese crypto scam app in India
Date: 2024-09-30T01:28:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-09-30-binance-claims-it-helped-to-bust-chinese-crypto-scam-app-in-india

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/30/asia_tech_news_in_brief/){:target="_blank" rel="noopener"}

> Plus: SpaceX plans Vietnam investment; Yahoo ! Japan content moderation secrets; LG offloads Chinese display factory; and more ASIA IN BRIEF It's not often The Register writes about a cryptocurrency outfit being on the right side of a scam or crime, but last week crypto exchange Binance claimed it helped Indian authorities to investigate a scam gaming app.... [...]
