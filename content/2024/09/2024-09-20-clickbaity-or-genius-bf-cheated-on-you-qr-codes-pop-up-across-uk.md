Title: Clickbaity or genius? 'BF cheated on you' QR codes pop up across UK
Date: 2024-09-20T10:10:48-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2024-09-20-clickbaity-or-genius-bf-cheated-on-you-qr-codes-pop-up-across-uk

[Source](https://www.bleepingcomputer.com/news/security/clickbaity-or-genius-bf-cheated-on-you-qr-codes-pop-up-across-uk/){:target="_blank" rel="noopener"}

> A new wave of QR codes has popped up across UK claiming to share a video of a boyfriend who "cheated" on a girl named Emily last night. Clickbaity or genius? [...]
