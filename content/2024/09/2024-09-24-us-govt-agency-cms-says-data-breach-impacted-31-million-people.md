Title: U.S. govt agency CMS says data breach impacted 3.1 million people
Date: 2024-09-24T14:01:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Healthcare;Government;Security
Slug: 2024-09-24-us-govt-agency-cms-says-data-breach-impacted-31-million-people

[Source](https://www.bleepingcomputer.com/news/healthcare/us-govt-agency-cms-says-data-breach-impacted-31-million-people/){:target="_blank" rel="noopener"}

> The Centers for Medicare & Medicaid Services (CMS) federal agency announced earlier this month that health and personal information of more than three million health plan beneficiaries was exposed in the MOVEit attacks Cl0p ransomware conducted last year. [...]
