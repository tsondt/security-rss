Title: FBI boss says China 'burned down' 260,000-device botnet when confronted by Feds
Date: 2024-09-18T21:06:46+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-18-fbi-boss-says-china-burned-down-260000-device-botnet-when-confronted-by-feds

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/18/fbi_flax_typhoon_ransomware/){:target="_blank" rel="noopener"}

> Plus: Wray tells how bureau helps certain victims negotiate with ransomware crooks China-backed spies are said to have tore down their own 260,000-device botnet after the FBI and its international pals went after them.... [...]
