Title: Hackers inject malicious JS in Cisco store to steal credit cards, credentials
Date: 2024-09-04T11:48:06-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-09-04-hackers-inject-malicious-js-in-cisco-store-to-steal-credit-cards-credentials

[Source](https://www.bleepingcomputer.com/news/security/hackers-inject-malicious-js-in-cisco-store-to-steal-credit-cards-credentials/){:target="_blank" rel="noopener"}

> Cisco's site for selling company-themed merchandise is currently offline and under maintenance due to hackers compromising it with JavaScript code that steals sensitive customer details provided at checkout. [...]
