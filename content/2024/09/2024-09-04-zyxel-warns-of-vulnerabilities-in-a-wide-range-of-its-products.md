Title: Zyxel warns of vulnerabilities in a wide range of its products
Date: 2024-09-04T18:57:46+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;network hardware;patches;vulnerabilities;zyxel
Slug: 2024-09-04-zyxel-warns-of-vulnerabilities-in-a-wide-range-of-its-products

[Source](https://arstechnica.com/?p=2047312){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Networking hardware-maker Zyxel is warning of nearly a dozen vulnerabilities in a wide array of its products. If left unpatched, some of them could enable the complete takeover of the devices, which can be targeted as an initial point of entry into large networks. The most serious vulnerability, tracked as CVE-2024-7261, can be exploited to “allow an unauthenticated attacker to execute OS commands by sending a crafted cookie to a vulnerable device,” Zyxel warned. The flaw, with a severity rating of 9.8 out of 10, stems from the “improper neutralization of special elements in the parameter [...]
