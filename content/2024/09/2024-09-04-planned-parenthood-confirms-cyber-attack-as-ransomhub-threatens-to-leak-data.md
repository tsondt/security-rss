Title: Planned Parenthood confirms cyber-attack as RansomHub threatens to leak data
Date: 2024-09-04T20:33:53+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-04-planned-parenthood-confirms-cyber-attack-as-ransomhub-threatens-to-leak-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/04/planned_parenthood_cybersecurity_incident/){:target="_blank" rel="noopener"}

> 93GB of info feared pilfered in Montana by heartless crooks Planned Parenthood of Montana's chief exec says the org is responding to a cyber-attack on its systems, and has drafted in federal law enforcement and infosec professionals to help investigate and rebuild its IT environment.... [...]
