Title: Temu denies breach after hacker claims theft of 87 million data records
Date: 2024-09-17T16:58:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-17-temu-denies-breach-after-hacker-claims-theft-of-87-million-data-records

[Source](https://www.bleepingcomputer.com/news/security/temu-denies-breach-after-hacker-claims-theft-of-87-million-data-records/){:target="_blank" rel="noopener"}

> Temu denies it was hacked or suffered a data breach after a threat actor claimed to be selling a stolen database containing 87 million records of customer information. [...]
