Title: Transport for London confirms customer data stolen in cyberattack
Date: 2024-09-12T11:17:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-12-transport-for-london-confirms-customer-data-stolen-in-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/transport-for-london-confirms-customer-data-stolen-in-cyberattack/){:target="_blank" rel="noopener"}

> Transport for London (TfL) has determined that the cyberattack on September 1 impacts customer data, including names, contact details, email addresses, and home addresses. [...]
