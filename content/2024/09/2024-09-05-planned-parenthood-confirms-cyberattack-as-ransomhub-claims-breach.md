Title: Planned Parenthood confirms cyberattack as RansomHub claims breach
Date: 2024-09-05T01:29:28-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-09-05-planned-parenthood-confirms-cyberattack-as-ransomhub-claims-breach

[Source](https://www.bleepingcomputer.com/news/security/planned-parenthood-confirms-cyberattack-as-ransomhub-claims-breach/){:target="_blank" rel="noopener"}

> Planned Parenthood has confirmed it suffered a cyberattack affecting its IT systems, forcing it to take parts of its infrastructure offline to contain the damage. [...]
