Title: Novel attack on Windows spotted in phishing campaign run from and targeting China
Date: 2024-09-02T03:06:24+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-09-02-novel-attack-on-windows-spotted-in-phishing-campaign-run-from-and-targeting-china

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/02/securonix_china_slowtempest_campaign/){:target="_blank" rel="noopener"}

> Resources hosted at Tencent Cloud involved in Cobalt Strike campaign Chinese web champ Tencent's cloud is being used by unknown attackers as part of a phishing campaign that aims to achieve persistent network access at Chinese entities.... [...]
