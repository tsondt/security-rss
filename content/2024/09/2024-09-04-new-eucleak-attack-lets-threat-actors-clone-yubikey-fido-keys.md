Title: New Eucleak attack lets threat actors clone YubiKey FIDO keys
Date: 2024-09-04T13:48:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-04-new-eucleak-attack-lets-threat-actors-clone-yubikey-fido-keys

[Source](https://www.bleepingcomputer.com/news/security/new-eucleak-attack-lets-threat-actors-clone-yubikey-fido-keys/){:target="_blank" rel="noopener"}

> A new "EUCLEAK" flaw found in FIDO devices using the Infineon SLE78 security microcontroller, like Yubico's YubiKey 5 Series, allows attackers to extract Elliptic Curve Digital Signature Algorithm (ECDSA) secret keys and clone the FIDO device. [...]
