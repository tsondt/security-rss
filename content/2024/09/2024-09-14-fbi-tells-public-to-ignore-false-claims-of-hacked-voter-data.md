Title: FBI tells public to ignore false claims of hacked voter data
Date: 2024-09-14T10:26:32-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-14-fbi-tells-public-to-ignore-false-claims-of-hacked-voter-data

[Source](https://www.bleepingcomputer.com/news/security/fbi-tells-public-to-ignore-false-claims-of-hacked-voter-data/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) and the Cybersecurity and Infrastructure Security Agency (CISA) are alerting the public of false claims that the U.S. voter registration data has been compromised in cyberattacks. [...]
