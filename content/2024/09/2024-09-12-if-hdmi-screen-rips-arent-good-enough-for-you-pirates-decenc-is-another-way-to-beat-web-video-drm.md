Title: If HDMI screen rips aren't good enough for you pirates, DeCENC is another way to beat web video DRM
Date: 2024-09-12T07:25:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-09-12-if-hdmi-screen-rips-arent-good-enough-for-you-pirates-decenc-is-another-way-to-beat-web-video-drm

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/12/cenc_encryption_stream_attack/){:target="_blank" rel="noopener"}

> Academically interesting technique for poking holes in paywalled tech specs An anti-piracy system to protect online video streams from unauthorized copying is flawed – and can be broken to allow streamed media from Amazon, Netflix, and others to be saved, replayed, and spread at will, we're told.... [...]
