Title: FBI: Reported cryptocurrency losses reached $5.6 billion in 2023
Date: 2024-09-12T14:27:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency;Legal
Slug: 2024-09-12-fbi-reported-cryptocurrency-losses-reached-56-billion-in-2023

[Source](https://www.bleepingcomputer.com/news/security/fbi-reported-cryptocurrency-losses-reached-56-billion-in-2023/){:target="_blank" rel="noopener"}

> The FBI says that 2023 was a record year for cryptocurrency fraud, with total losses exceeding $5.6 billion, based on nearly 70,000 reports received through the Internet Crime Complaint Center (IC3). [...]
