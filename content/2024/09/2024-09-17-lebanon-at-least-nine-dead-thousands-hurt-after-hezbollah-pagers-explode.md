Title: Lebanon: At least nine dead, thousands hurt after Hezbollah pagers explode
Date: 2024-09-17T18:30:08+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-17-lebanon-at-least-nine-dead-thousands-hurt-after-hezbollah-pagers-explode

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/17/hezbollah_lebanon_explosive_pagers/){:target="_blank" rel="noopener"}

> Eight-year-old among those slain, Israel blamed, Iran's Lebanese ambassador wounded, it's said Lebanon says at least nine people, including an eight-year-old girl, were killed today after pagers used by Hezbollah members exploded across the country. Israel has been blamed.... [...]
