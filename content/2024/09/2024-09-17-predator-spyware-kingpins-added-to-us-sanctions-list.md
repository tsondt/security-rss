Title: Predator spyware kingpins added to US sanctions list
Date: 2024-09-17T13:44:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-17-predator-spyware-kingpins-added-to-us-sanctions-list

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/17/predator_spyware_sanctions/){:target="_blank" rel="noopener"}

> Designations come as new infrastructure spins up in Africa Five individuals and one company with ties to spyware developer Intellexa are the latest to earn sanctions as the US expands efforts to stamp out spyware.... [...]
