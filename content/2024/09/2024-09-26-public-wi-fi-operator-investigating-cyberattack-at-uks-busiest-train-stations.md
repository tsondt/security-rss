Title: Public Wi-Fi operator investigating cyberattack at UK's busiest train stations
Date: 2024-09-26T10:29:53+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-26-public-wi-fi-operator-investigating-cyberattack-at-uks-busiest-train-stations

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/26/public_wifi_operator_investigating_cyberattack/){:target="_blank" rel="noopener"}

> See it, say it... not sorted just yet as network access remains offline Updated A cybersecurity incident is being probed at Network Rail, the UK non-departmental public body responsible for repairing and developing train infrastructure, after unsavory messaging was displayed to those connecting to major stations' free Wi-Fi portals.... [...]
