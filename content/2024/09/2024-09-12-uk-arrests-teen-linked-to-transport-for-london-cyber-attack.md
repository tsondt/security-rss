Title: UK arrests teen linked to Transport for London cyber attack
Date: 2024-09-12T12:36:47-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-09-12-uk-arrests-teen-linked-to-transport-for-london-cyber-attack

[Source](https://www.bleepingcomputer.com/news/security/uk-arrests-teen-linked-to-transport-for-london-cyber-attack/){:target="_blank" rel="noopener"}

> U.K.'s National Crime Agency says it arrested a 17-year-old teenager who is suspected of being connected to the cyberattack on Transport for London, the city's public transportation agency. [...]
