Title: Criminal IP Earns PCI DSS v4.0 Certification for Top-Level Security
Date: 2024-09-04T10:02:37-04:00
Author: Sponsored by Criminal IP
Category: BleepingComputer
Tags: Security
Slug: 2024-09-04-criminal-ip-earns-pci-dss-v40-certification-for-top-level-security

[Source](https://www.bleepingcomputer.com/news/security/criminal-ip-earns-pci-dss-v40-certification-for-top-level-security/){:target="_blank" rel="noopener"}

> AI Spera has achieved PCI DSS v4.0 certification for its threat intel search engine solution, Criminal IP. Learn more from the Criminal IP cyber threat intelligence search engine. [...]
