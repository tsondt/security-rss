Title: 10 nasty software bugs put thousands of fuel storage tanks at risk of cyberattacks
Date: 2024-09-24T15:30:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-24-10-nasty-software-bugs-put-thousands-of-fuel-storage-tanks-at-risk-of-cyberattacks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/24/security_bugs_fuel_storage_tanks/){:target="_blank" rel="noopener"}

> Thousands of devices remain vulnerable, US most exposed to the threat Tens of thousands of fuel storage tanks in critical infrastructure facilities remain vulnerable to zero-day attacks due to buggy Automatic Tank Gauge systems from multiple vendors, say infosec researchers.... [...]
