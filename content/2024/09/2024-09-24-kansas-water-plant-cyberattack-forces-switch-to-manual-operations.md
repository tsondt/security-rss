Title: Kansas water plant cyberattack forces switch to manual operations
Date: 2024-09-24T15:52:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-24-kansas-water-plant-cyberattack-forces-switch-to-manual-operations

[Source](https://www.bleepingcomputer.com/news/security/kansas-water-plant-cyberattack-forces-switch-to-manual-operations/){:target="_blank" rel="noopener"}

> Arkansas City, a small city in Cowley County, Kansas, was forced to switch its water treatment facility to manual operations over the weekend to contain a cyberattack detected on Sunday morning. [...]
