Title: 1.7M potentially pwned after payment services provider takes a year to notice break-in
Date: 2024-09-09T16:00:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-09-17m-potentially-pwned-after-payment-services-provider-takes-a-year-to-notice-break-in

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/09/slim_cd_breach/){:target="_blank" rel="noopener"}

> Criminals with plenty of time on their hands may now have credit card details Around 1.7 million people will receive a letter from Florida-based Slim CD, if they haven't already, after the company detected an intrusion dating back nearly a year.... [...]
