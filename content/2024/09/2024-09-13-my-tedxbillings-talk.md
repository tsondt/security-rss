Title: My TedXBillings Talk
Date: 2024-09-13T18:02:26+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;democracy;Schneier news;TED;video
Slug: 2024-09-13-my-tedxbillings-talk

[Source](https://www.schneier.com/blog/archives/2024/09/my-tedxbillings-talk.html){:target="_blank" rel="noopener"}

> Over the summer, I gave a talk about AI and democracy at TedXBillings. The recording is https://www.youtube.com/watch?v=uqC4nb7fLpY”>live. Please share. I’m hoping for more than 200 views.... [...]
