Title: Red team hacker on how she 'breaks into buildings and pretends to be the bad guy'
Date: 2024-09-29T16:39:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-29-red-team-hacker-on-how-she-breaks-into-buildings-and-pretends-to-be-the-bad-guy

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/29/interview_with_a_social_engineering/){:target="_blank" rel="noopener"}

> Alethe Denis exposes tricks that made you fall for that return-to-office survey Interview A hacker walked into a "very big city" building on a Wednesday morning with no keys to any doors or elevators, determined to steal sensitive data by breaking into both the physical space and the corporate Wi-Fi network.... [...]
