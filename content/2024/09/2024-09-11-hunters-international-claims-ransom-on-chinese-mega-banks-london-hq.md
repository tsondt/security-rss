Title: Hunters International claims ransom on Chinese mega-bank's London HQ
Date: 2024-09-11T18:00:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-11-hunters-international-claims-ransom-on-chinese-mega-banks-london-hq

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/11/hunters_ransom_icbc_london/){:target="_blank" rel="noopener"}

> Allegedly swiped more than 5.2M files and threatens to publish the lot Ransomware gang Hunters International reportedly claims to have stolen more than 5.2 million files belonging to the London branch of the Industrial and Commercial Bank of China (ICBC), a Chinese state-owned bank and financial service corporation, and set a deadline of September 13 to release all the data.... [...]
