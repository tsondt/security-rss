Title: Secure Boot-neutering PKfail debacle is more prevalent than anyone knew
Date: 2024-09-16T22:13:12+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;pkfail;platform keys;rootkits;secure boot;uefi
Slug: 2024-09-16-secure-boot-neutering-pkfail-debacle-is-more-prevalent-than-anyone-knew

[Source](https://arstechnica.com/?p=2050182){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) A supply chain failure that compromises Secure Boot protections on computing devices from across the device-making industry extends to a much larger number of models than previously known, including those used in ATMs, point-of-sale terminals, and voting machines. The debacle was the result of non-production test platform keys used in hundreds of device models for more than a decade. These cryptographic keys form the root-of-trust anchor between the hardware device and the firmware that runs on it. The test production keys—stamped with phrases such as “DO NOT TRUST” in the certificates—were never intended to be used [...]
