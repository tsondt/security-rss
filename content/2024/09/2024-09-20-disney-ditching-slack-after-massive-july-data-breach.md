Title: Disney ditching Slack after massive July data breach
Date: 2024-09-20T14:33:01-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-09-20-disney-ditching-slack-after-massive-july-data-breach

[Source](https://www.bleepingcomputer.com/news/security/disney-ditching-slack-after-massive-july-data-breach/){:target="_blank" rel="noopener"}

> The Walt Disney Company is reportedly ditching Slack after a July data breach exposed over 1TB of confidential messages and files posted to the company's internal communication channels. [...]
