Title: Healthcare giant to pay $65M settlement after crooks stole and leaked nude patient pics
Date: 2024-09-12T02:24:17+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-12-healthcare-giant-to-pay-65m-settlement-after-crooks-stole-and-leaked-nude-patient-pics

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/12/lvhn_lawsuit_ransom/){:target="_blank" rel="noopener"}

> Would paying a ransom – or better security – have been cheaper and safer? A US healthcare giant will pay out $65 million to settle a class-action lawsuit brought by its own patients after ransomware crooks stole their data – including their nude photographs – and published at least some of them online.... [...]
