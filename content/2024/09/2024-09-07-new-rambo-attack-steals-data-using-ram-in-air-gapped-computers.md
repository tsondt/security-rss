Title: New RAMBO attack steals data using RAM in air-gapped computers
Date: 2024-09-07T10:15:25-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-07-new-rambo-attack-steals-data-using-ram-in-air-gapped-computers

[Source](https://www.bleepingcomputer.com/news/security/new-rambo-attack-steals-data-using-ram-in-air-gapped-computers/){:target="_blank" rel="noopener"}

> A novel side-channel attack dubbed "RAMBO" (Radiation of Air-gapped Memory Bus for Offense) generates electromagnetic radiation from a device's RAM to send data from air-gapped computers. [...]
