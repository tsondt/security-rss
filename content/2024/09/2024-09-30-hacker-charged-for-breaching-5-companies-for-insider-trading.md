Title: Hacker charged for breaching 5 companies for insider trading
Date: 2024-09-30T18:02:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-09-30-hacker-charged-for-breaching-5-companies-for-insider-trading

[Source](https://www.bleepingcomputer.com/news/security/hacker-charged-for-breaching-5-companies-for-insider-trading/){:target="_blank" rel="noopener"}

> The U.S. Securities and Exchange Commission (SEC) charged Robert B. Westbrook, a U.K. citizen, with hacking into the computer systems of five U.S. public companies to access confidential earnings information and conduct insider trading. [...]
