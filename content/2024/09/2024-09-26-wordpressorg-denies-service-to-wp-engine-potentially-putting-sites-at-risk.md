Title: WordPress.org denies service to WP Engine, potentially putting sites at risk
Date: 2024-09-26T01:45:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-09-26-wordpressorg-denies-service-to-wp-engine-potentially-putting-sites-at-risk

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/26/wordpressorg_denies_service_to_wp/){:target="_blank" rel="noopener"}

> That escalated quickly Updated WordPress on Wednesday escalated its conflict with WP Engine, a hosting provider, by blocking the latter's servers from accessing WordPress.org resources – and therefore from potentially vital software updates.... [...]
