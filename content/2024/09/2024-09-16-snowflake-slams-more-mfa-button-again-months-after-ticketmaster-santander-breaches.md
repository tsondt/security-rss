Title: Snowflake slams 'more MFA' button again – months after Ticketmaster, Santander breaches
Date: 2024-09-16T16:45:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-16-snowflake-slams-more-mfa-button-again-months-after-ticketmaster-santander-breaches

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/16/snowflake_mfa_default/){:target="_blank" rel="noopener"}

> Now it's the default for all new accounts Snowflake continues to push forward in strengthening its users' cybersecurity posture by making multi-factor authentication the default for all new accounts.... [...]
