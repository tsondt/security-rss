Title: Israel’s Pager Attacks and Supply Chain Vulnerabilities
Date: 2024-09-24T11:05:34+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberterrorism;cyberwar;Internet of Things;supply chain
Slug: 2024-09-24-israels-pager-attacks-and-supply-chain-vulnerabilities

[Source](https://www.schneier.com/blog/archives/2024/09/israels-pager-attacks.html){:target="_blank" rel="noopener"}

> Israel’s brazen attacks on Hezbollah last week, in which hundreds of pagers and two-way radios exploded and killed at least 37 people, graphically illustrated a threat that cybersecurity experts have been warning about for years: Our international supply chains for computerized equipment leave us vulnerable. And we have no good means to defend ourselves. Though the deadly operations were stunning, none of the elements used to carry them out were particularly new. The tactics employed by Israel, which has neither confirmed nor denied any role, to hijack an international supply chain and embed plastic explosives in Hezbollah devices have been [...]
