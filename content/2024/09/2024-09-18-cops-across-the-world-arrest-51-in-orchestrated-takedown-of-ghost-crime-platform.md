Title: Cops across the world arrest 51 in orchestrated takedown of Ghost crime platform
Date: 2024-09-18T12:16:40+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-18-cops-across-the-world-arrest-51-in-orchestrated-takedown-of-ghost-crime-platform

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/18/51_arrests_ghost_platform/){:target="_blank" rel="noopener"}

> Italian mafia mobsters and Irish crime families scuppered by international cops Hours after confirming they had pwned the supposedly uncrackable encrypted messaging platform used for all manner of organized crime, Ghost, cops have now named the suspect they cuffed last night, who is charged with being the alleged mastermind.... [...]
