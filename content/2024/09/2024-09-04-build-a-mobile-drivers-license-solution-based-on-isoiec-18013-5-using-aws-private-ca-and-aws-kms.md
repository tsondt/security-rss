Title: Build a mobile driver’s license solution based on ISO/IEC 18013-5 using AWS Private CA and AWS KMS
Date: 2024-09-04T17:33:17+00:00
Author: Ram Ramani
Category: AWS Security
Tags: Advanced (300);AWS Certificate Manager;AWS Key Management Service;Security, Identity, & Compliance;Security Blog
Slug: 2024-09-04-build-a-mobile-drivers-license-solution-based-on-isoiec-18013-5-using-aws-private-ca-and-aws-kms

[Source](https://aws.amazon.com/blogs/security/build-a-mobile-drivers-license-solution-based-on-iso-iec-18013-5-using-aws-private-ca-and-aws-kms/){:target="_blank" rel="noopener"}

> A mobile driver’s license (mDL) is a digital representation of a physical driver’s license that’s stored on a mobile device. An mDL is a significant improvement over physical credentials, which can be lost, stolen, counterfeited, damaged, or contain outdated information, and can expose unconsented personally identifiable information (PII). Organizations are working together to use mDLs across [...]
