Title: Thousands of orgs at risk of knowledge base data leaks via ServiceNow misconfigurations
Date: 2024-09-19T14:02:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-19-thousands-of-orgs-at-risk-of-knowledge-base-data-leaks-via-servicenow-misconfigurations

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/19/servicenow_knowledge_base_leaks/){:target="_blank" rel="noopener"}

> Better check your widgets, people Security researchers say that thousands of companies are potentially leaking secrets from their internal knowledge base (KB) articles via ServiceNow misconfigurations.... [...]
