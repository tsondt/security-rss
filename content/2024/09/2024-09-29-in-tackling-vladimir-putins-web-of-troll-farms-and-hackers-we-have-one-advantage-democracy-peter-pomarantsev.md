Title: In tackling Vladimir Putin’s web of troll farms and hackers, we have one advantage: democracy | Peter Pomarantsev
Date: 2024-09-29T08:00:29+00:00
Author: Peter Pomerantsev
Category: The Guardian
Tags: Russia;RT;Ukraine;Europe;World news;Information;Food security;Data and computer security;Technology;Media
Slug: 2024-09-29-in-tackling-vladimir-putins-web-of-troll-farms-and-hackers-we-have-one-advantage-democracy-peter-pomarantsev

[Source](https://www.theguardian.com/commentisfree/2024/sep/29/in-tackling-vladimir-putins-web-of-troll-farms-and-hackers-we-have-one-advantage-democracy){:target="_blank" rel="noopener"}

> By focusing on its strengths and pooling information, the west can disrupt Russia’s war machine – but there’s no time to lose Russia is a “mafia state” trying to expand into a “mafia empire”, the foreign secretary, David Lammy, told the UN, nailing the dual nature of Vladimir Putin’s political model. On one hand Russia represents something very old – a world of bullying empires that invade smaller countries, grab their resources and indoctrinate their people into thinking they are inferior. But it is also something very new, weaponising corruption, criminal networks, assassinations and tech-driven psy-ops to subvert open societies. [...]
