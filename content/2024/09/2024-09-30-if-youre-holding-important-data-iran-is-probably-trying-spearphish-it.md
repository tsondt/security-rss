Title: If you're holding important data, Iran is probably trying spearphish it
Date: 2024-09-30T13:35:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-30-if-youre-holding-important-data-iran-is-probably-trying-spearphish-it

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/30/iran_spearphishing/){:target="_blank" rel="noopener"}

> It's election year for more than 50 countries and the Islamic Republic threatens a bunch of them US and UK national security agencies are jointly warning about Iranian spearphishing campaigns, which remain an ongoing threat to various industries and governments.... [...]
