Title: Cicada3301 ransomware’s Linux encryptor targets VMware ESXi systems
Date: 2024-09-01T10:14:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-01-cicada3301-ransomwares-linux-encryptor-targets-vmware-esxi-systems

[Source](https://www.bleepingcomputer.com/news/security/cicada3301-ransomwares-linux-encryptor-targets-vmware-esxi-systems/){:target="_blank" rel="noopener"}

> A new ransomware-as-a-service (RaaS) operation named Cicada3301 has already listed 19 victims on its extortion portal, as it quickly attacked companies worldwide. [...]
