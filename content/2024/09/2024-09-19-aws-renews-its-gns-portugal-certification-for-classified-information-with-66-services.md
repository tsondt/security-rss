Title: AWS renews its GNS Portugal certification for classified information with 66 services
Date: 2024-09-19T14:48:10+00:00
Author: Daniel Fuertes
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS security;Certificate;Certification;Compliance;Compliance reports;cybersecurity;Portugal;Public Sector;report;Security Blog
Slug: 2024-09-19-aws-renews-its-gns-portugal-certification-for-classified-information-with-66-services

[Source](https://aws.amazon.com/blogs/security/aws-renews-its-gns-portugal-certification-for-classified-information-with-66-services/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) announces that it has successfully renewed the Portuguese GNS (Gabinete Nacional de Segurança, National Security Cabinet) certification in the AWS Regions and edge locations in the European Union. This accreditation confirms that AWS cloud infrastructure, security controls, and operational processes adhere to the stringent requirements set forth by the Portuguese government [...]
