Title: NIST Recommends Some Common-Sense Password Rules
Date: 2024-09-27T11:01:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;NIST;passwords;reports
Slug: 2024-09-27-nist-recommends-some-common-sense-password-rules

[Source](https://www.schneier.com/blog/archives/2024/09/nist-recommends-some-common-sense-password-rules.html){:target="_blank" rel="noopener"}

> NIST’s second draft of its “ SP 800-63-4 “—its digital identify guidelines—finally contains some really good rules about passwords: The following requirements apply to passwords: lVerifiers and CSPs SHALL require passwords to be a minimum of eight characters in length and SHOULD require passwords to be a minimum of 15 characters in length. Verifiers and CSPs SHOULD permit a maximum password length of at least 64 characters. Verifiers and CSPs SHOULD accept all printing ASCII [RFC20] characters and the space character in passwords. Verifiers and CSPs SHOULD accept Unicode [ISO/ISC 10646] characters in passwords. Each Unicode code point SHALL be [...]
