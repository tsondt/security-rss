Title: Quantum computing is coming – are you ready?
Date: 2024-09-05T15:08:11+00:00
Author: Joseph Martins
Category: The Register
Tags: 
Slug: 2024-09-05-quantum-computing-is-coming-are-you-ready

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/05/quantum_computing_is_coming_are/){:target="_blank" rel="noopener"}

> Are you prepared for the day that quantum computing breaks today’s encryption? Sponsored Feature The internet is all about transparency and openness - connecting people and information, shoppers and vendors, or businesses. But it's also all about security and trust.... [...]
