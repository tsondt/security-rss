Title: Kremlin-linked COLDRIVER crooks take pro-democracy NGOs for phishy ride
Date: 2024-09-09T13:45:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-09-kremlin-linked-coldriver-crooks-take-pro-democracy-ngos-for-phishy-ride

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/09/russia_coldriver_ngo_phishing/){:target="_blank" rel="noopener"}

> The latest of many attempts to stifle perceived threats to Putin's regime A pro-democracy NGO in Russia says it looks like the Kremlin-linked COLDRIVER group was behind last month's hack-and-leak job that saw files and inboxes dumped online.... [...]
