Title: Putin really wants Trump back in the White House
Date: 2024-09-18T19:34:23+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-18-putin-really-wants-trump-back-in-the-white-house

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/18/russia_putin_trump_white_house/){:target="_blank" rel="noopener"}

> US govt, Microsoft report on Kremlin trolls' latest antics to Make America Grate Again Russia really wants Donald Trump to be the next US President, judging by reports from American government agencies and now Microsoft's threat intelligence team.... [...]
