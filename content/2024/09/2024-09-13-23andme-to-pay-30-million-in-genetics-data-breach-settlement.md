Title: 23andMe to pay $30 million in genetics data breach settlement
Date: 2024-09-13T14:58:30-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-13-23andme-to-pay-30-million-in-genetics-data-breach-settlement

[Source](https://www.bleepingcomputer.com/news/security/23andme-to-pay-30-million-in-genetics-data-breach-settlement/){:target="_blank" rel="noopener"}

> DNA testing giant 23andMe has agreed to pay $30 million to settle a lawsuit over a data breach that exposed the personal information of 6.4 million customers in 2023. [...]
