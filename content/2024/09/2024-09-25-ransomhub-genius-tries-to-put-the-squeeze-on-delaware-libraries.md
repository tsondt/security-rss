Title: RansomHub genius tries to put the squeeze on Delaware Libraries
Date: 2024-09-25T17:30:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-25-ransomhub-genius-tries-to-put-the-squeeze-on-delaware-libraries

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/25/delaware_libraries_ransomware_attack/){:target="_blank" rel="noopener"}

> Extorting underfunded public services for $1M isn't a good look Despite being top of the ransomware tree at the moment, RansomHub – specifically, one of its affiliates – clearly isn't that bright as they are reportedly trying to extort Delaware Libraries for around $1 million.... [...]
