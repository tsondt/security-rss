Title: How to manage shadow IT and reduce your attack surface
Date: 2024-09-23T10:01:11-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2024-09-23-how-to-manage-shadow-it-and-reduce-your-attack-surface

[Source](https://www.bleepingcomputer.com/news/security/how-to-manage-shadow-it-and-reduce-your-attack-surface/){:target="_blank" rel="noopener"}

> In today's fast-paced business environment, employees increasingly turn to unauthorized IT solutions, called Shadow IT, to streamline their work and boost productivity. This article explores the prevalence of shadow IT, the risks it poses and discusses strategies for managing it. [...]
