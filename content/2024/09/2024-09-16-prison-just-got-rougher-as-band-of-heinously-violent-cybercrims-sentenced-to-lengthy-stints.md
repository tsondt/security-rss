Title: Prison just got rougher as band of heinously violent cybercrims sentenced to lengthy stints
Date: 2024-09-16T12:15:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-16-prison-just-got-rougher-as-band-of-heinously-violent-cybercrims-sentenced-to-lengthy-stints

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/16/prison_just_got_rougher_as/){:target="_blank" rel="noopener"}

> Orchestrators of abductions, torture, crypto thefts, and more get their comeuppance One cybercriminal of the most violent kind will spend his best years behind bars, as will 11 of his thug pals for a string of cryptocurrency robberies in the US.... [...]
