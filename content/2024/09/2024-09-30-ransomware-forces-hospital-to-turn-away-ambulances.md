Title: Ransomware forces hospital to turn away ambulances
Date: 2024-09-30T22:16:18+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-30-ransomware-forces-hospital-to-turn-away-ambulances

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/30/texan_hospital_ransomware/){:target="_blank" rel="noopener"}

> Only level-one trauma unit in 400 miles crippled Ransomware scumbags have caused a vital hospital to turn away ambulances after infecting its computer systems with malware.... [...]
