Title: Microsoft removes revenge porn from Bing search using new tool
Date: 2024-09-05T16:56:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Artificial Intelligence;Technology
Slug: 2024-09-05-microsoft-removes-revenge-porn-from-bing-search-using-new-tool

[Source](https://www.bleepingcomputer.com/news/security/microsoft-removes-revenge-porn-from-bing-search-using-new-tool/){:target="_blank" rel="noopener"}

> Microsoft announced today that it has partnered with StopNCII to proactively remove harmful intimate images and videos from Bing using digital hashes people create from their sensitive media. [...]
