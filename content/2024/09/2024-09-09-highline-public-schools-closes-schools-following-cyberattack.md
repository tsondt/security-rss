Title: Highline Public Schools closes schools following cyberattack
Date: 2024-09-09T15:06:58-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Education
Slug: 2024-09-09-highline-public-schools-closes-schools-following-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/highline-public-schools-closes-schools-following-cyberattack/){:target="_blank" rel="noopener"}

> Highline Public Schools, a K-12 district in Washington state, has shut down all schools and canceled school activities after its technology systems were compromised in a cyberattack. [...]
