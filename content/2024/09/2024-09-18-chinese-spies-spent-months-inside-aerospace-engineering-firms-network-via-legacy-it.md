Title: Chinese spies spent months inside aerospace engineering firm's network via legacy IT
Date: 2024-09-18T17:00:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-18-chinese-spies-spent-months-inside-aerospace-engineering-firms-network-via-legacy-it

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/18/chinese_spies_found_on_us_hq_firm_network/){:target="_blank" rel="noopener"}

> Getting sloppy, Xi Exclusive Chinese state-sponsored spies have been spotted inside a global engineering firm's network, having gained initial entry using an admin portal's default credentials on an IBM AIX server.... [...]
