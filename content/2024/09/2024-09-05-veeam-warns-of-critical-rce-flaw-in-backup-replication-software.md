Title: Veeam warns of critical RCE flaw in Backup & Replication software
Date: 2024-09-05T10:17:39-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-05-veeam-warns-of-critical-rce-flaw-in-backup-replication-software

[Source](https://www.bleepingcomputer.com/news/security/veeam-warns-of-critical-rce-flaw-in-backup-and-replication-software/){:target="_blank" rel="noopener"}

> Veeam has released security updates for several of its products as part of a single September 2024 security bulletin that addresses 18 high and critical severity flaws in Veeam Backup & Replication, Service Provider Console, and One. [...]
