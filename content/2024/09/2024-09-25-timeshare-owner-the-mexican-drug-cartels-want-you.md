Title: Timeshare Owner? The Mexican Drug Cartels Want You
Date: 2024-09-25T16:26:12+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Latest Warnings;Ne'er-Do-Well News;Constella Intelligence;Datasur host;Escshieldsecurity Network;Jalisco drug cartel;telemarketing scams;timeshare scams
Slug: 2024-09-25-timeshare-owner-the-mexican-drug-cartels-want-you

[Source](https://krebsonsecurity.com/2024/09/timeshare-owner-the-mexican-drug-cartels-want-you/){:target="_blank" rel="noopener"}

> The FBI is warning timeshare owners to be wary of a prevalent telemarketing scam involving a violent Mexican drug cartel that tries to trick people into believing someone wants to buy their property. This is the story of a couple who recently lost more than $50,000 to an ongoing timeshare scam that spans at least two dozen phony escrow, title and realty firms. One of the phony real estate companies trying to scam people out of money over fake offers to buy their timeshares. One evening in late 2022, someone phoned Mr. & Mrs. Dimitruk, a retired couple from Ontario, [...]
