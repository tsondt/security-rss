Title: Chinese hackers linked to cybercrime syndicate arrested in Singapore
Date: 2024-09-11T09:43:37-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;Security
Slug: 2024-09-11-chinese-hackers-linked-to-cybercrime-syndicate-arrested-in-singapore

[Source](https://www.bleepingcomputer.com/news/legal/chinese-hackers-linked-to-PlugX-malware-arrested-in-singapore/){:target="_blank" rel="noopener"}

> Six Chinese nationals and a Singaporean have been arrested on Monday in Singapore for their alleged role in malicious cyber activities committed in connection with a "global syndicate." [...]
