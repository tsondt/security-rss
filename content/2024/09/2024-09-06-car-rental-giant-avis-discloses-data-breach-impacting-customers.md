Title: Car rental giant Avis discloses data breach impacting customers
Date: 2024-09-06T14:04:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-06-car-rental-giant-avis-discloses-data-breach-impacting-customers

[Source](https://www.bleepingcomputer.com/news/security/car-rental-giant-avis-discloses-data-breach-impacting-customers/){:target="_blank" rel="noopener"}

> American car rental giant Avis disclosed a data breach after attackers breached one of its business applications last month and stole customer personal information. [...]
