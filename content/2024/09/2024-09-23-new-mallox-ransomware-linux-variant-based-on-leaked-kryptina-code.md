Title: New Mallox ransomware Linux variant based on leaked Kryptina code
Date: 2024-09-23T14:29:15-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Linux
Slug: 2024-09-23-new-mallox-ransomware-linux-variant-based-on-leaked-kryptina-code

[Source](https://www.bleepingcomputer.com/news/security/new-mallox-ransomware-linux-variant-based-on-leaked-kryptina-code/){:target="_blank" rel="noopener"}

> An affiliate of the Mallox ransomware operation, also known as TargetCompany, was spotted using a slightly modified version of the Kryptina ransomware to attack Linux systems. [...]
