Title: Cyber crooks shut down UK, US schools, thousands of kids affected
Date: 2024-09-11T22:43:33+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-11-cyber-crooks-shut-down-uk-us-schools-thousands-of-kids-affected

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/11/uk_us_school_ransomware/){:target="_blank" rel="noopener"}

> No class: Black Suit ransomware gang boasts of 200GB haul from one raid Cybercriminals closed some schools in America and Britain this week, preventing kindergarteners in Washington state from attending their first-ever school day and shutting down all internet-based systems for Biggin Hill-area students in England for the next three weeks.... [...]
