Title: Copilot for Microsoft 365 might boost productivity if you survive the compliance minefield
Date: 2024-09-04T21:15:12+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-09-04-copilot-for-microsoft-365-might-boost-productivity-if-you-survive-the-compliance-minefield

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/04/copilot_microsoft_365_compliance/){:target="_blank" rel="noopener"}

> Loads of governance issues to worry about, and the chance it might spout utter garbage Microsoft has published a Transparency Note for Copilot for Microsoft 365, warning enterprises to ensure user access rights are correctly managed before rolling out the technology.... [...]
