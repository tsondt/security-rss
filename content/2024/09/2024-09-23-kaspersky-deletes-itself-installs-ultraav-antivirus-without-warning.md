Title: Kaspersky deletes itself, installs UltraAV antivirus without warning
Date: 2024-09-23T13:16:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-23-kaspersky-deletes-itself-installs-ultraav-antivirus-without-warning

[Source](https://www.bleepingcomputer.com/news/security/kaspersky-deletes-itself-installs-ultraav-antivirus-without-warning/){:target="_blank" rel="noopener"}

> Starting Thursday, Kaspersky deleted its anti-malware software from computers across the United States and replaced it with UltraAV's antivirus solution without warning. [...]
