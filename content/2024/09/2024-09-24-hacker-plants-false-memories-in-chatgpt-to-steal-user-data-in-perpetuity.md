Title: Hacker plants false memories in ChatGPT to steal user data in perpetuity
Date: 2024-09-24T20:56:26+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: AI;Biz & IT;Security;ChatGPT;openai;privacy;prompt injection
Slug: 2024-09-24-hacker-plants-false-memories-in-chatgpt-to-steal-user-data-in-perpetuity

[Source](https://arstechnica.com/?p=2052198){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) When security researcher Johann Rehberger recently reported a vulnerability in ChatGPT that allowed attackers to store false information and malicious instructions in a user’s long-term memory settings, OpenAI summarily closed the inquiry, labeling the flaw a safety issue, not, technically speaking, a security concern. So Rehberger did what all good researchers do: He created a proof-of-concept exploit that used the vulnerability to exfiltrate all user input in perpetuity. OpenAI engineers took notice and issued a partial fix earlier this month. Strolling down memory lane The vulnerability abused long-term conversation memory, a feature OpenAI began testing in [...]
