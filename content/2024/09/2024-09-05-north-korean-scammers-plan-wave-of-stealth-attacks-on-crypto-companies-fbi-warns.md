Title: North Korean scammers plan wave of stealth attacks on crypto companies, FBI warns
Date: 2024-09-05T01:17:42+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-05-north-korean-scammers-plan-wave-of-stealth-attacks-on-crypto-companies-fbi-warns

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/05/fbi_north_korean_scammers_prepping/){:target="_blank" rel="noopener"}

> Feds warn of 'highly tailored, difficult-to-detect social engineering campaigns' The FBI has warned that North Korean operatives are plotting "complex and elaborate" social engineering attacks against employees of decentralized finance (DeFi) organizations, as part of ongoing efforts to steal cryptocurrency.... [...]
