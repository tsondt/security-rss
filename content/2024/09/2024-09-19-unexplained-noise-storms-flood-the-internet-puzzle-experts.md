Title: Unexplained ‘Noise Storms’ flood the Internet, puzzle experts
Date: 2024-09-19T09:57:21-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-19-unexplained-noise-storms-flood-the-internet-puzzle-experts

[Source](https://www.bleepingcomputer.com/news/security/unexplained-noise-storms-flood-the-internet-puzzle-experts/){:target="_blank" rel="noopener"}

> Internet intelligence firm GreyNoise reports that it has been tracking large waves of "Noise Storms" containing spoofed internet traffic since January 2020. However, despite extensive analysis, it has not concluded its origin and purpose. [...]
