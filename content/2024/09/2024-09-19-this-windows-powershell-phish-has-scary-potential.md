Title: This Windows PowerShell Phish Has Scary Potential
Date: 2024-09-19T19:39:09+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Web Fraud 2.0;CAPTCHA;GitHub;Lumma Stealer;Microsoft PowerShell;Virustotal.com
Slug: 2024-09-19-this-windows-powershell-phish-has-scary-potential

[Source](https://krebsonsecurity.com/2024/09/this-windows-powershell-phish-has-scary-potential/){:target="_blank" rel="noopener"}

> Many GitHub users this week received a novel phishing email warning of critical security holes in their code. Those who clicked the link for details were asked to distinguish themselves from bots by pressing a combination of keyboard keys that causes Microsoft Windows to download password-stealing malware. While it’s unlikely that many programmers fell for this scam, it’s notable because less targeted versions of it are likely to be far more successful against the average Windows user. A reader named Chris shared an email he received this week that spoofed GitHub’s security team and warned: “Hey there! We have detected [...]
