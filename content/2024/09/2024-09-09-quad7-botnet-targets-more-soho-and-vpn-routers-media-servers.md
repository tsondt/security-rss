Title: Quad7 botnet targets more SOHO and VPN routers, media servers
Date: 2024-09-09T17:30:29-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-09-quad7-botnet-targets-more-soho-and-vpn-routers-media-servers

[Source](https://www.bleepingcomputer.com/news/security/quad7-botnet-targets-more-soho-and-vpn-routers-media-servers/){:target="_blank" rel="noopener"}

> The Quad7 botnet is expanding its targeting scope with the addition of new clusters and custom implants that now also target Zyxel VPN appliances and Ruckus wireless routers. [...]
