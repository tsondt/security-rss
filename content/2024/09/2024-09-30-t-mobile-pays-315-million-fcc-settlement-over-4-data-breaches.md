Title: T-Mobile pays $31.5 million FCC settlement over 4 data breaches
Date: 2024-09-30T15:20:51-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-30-t-mobile-pays-315-million-fcc-settlement-over-4-data-breaches

[Source](https://www.bleepingcomputer.com/news/security/t-mobile-pays-315-million-fcc-settlement-over-4-data-breaches/){:target="_blank" rel="noopener"}

> The Federal Communications Commission (FCC) announced a $31.5 million settlement with T-Mobile over multiple data breaches that compromised the personal information of millions of U.S. consumers. [...]
