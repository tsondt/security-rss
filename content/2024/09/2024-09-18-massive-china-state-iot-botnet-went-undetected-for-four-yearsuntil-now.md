Title: Massive China-state IoT botnet went undetected for four years—until now
Date: 2024-09-18T19:58:42+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security
Slug: 2024-09-18-massive-china-state-iot-botnet-went-undetected-for-four-yearsuntil-now

[Source](https://arstechnica.com/?p=2050629){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) The FBI has dismantled a massive network of compromised devices that Chinese state-sponsored hackers have used for four years to mount attacks on government agencies, telecoms, defense contractors, and other targets in the US and Taiwan. The botnet was made up primarily of small office and home office routers, surveillance cameras, network-attached storage, and other Internet-connected devices located all over the world. Over the past four years, US officials said, 260,000 such devices have cycled through the sophisticated network, which is organized in three tiers that allow the botnet to operate with efficiency and precision. At [...]
