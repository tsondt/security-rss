Title: Microsoft Office 2024 to disable ActiveX controls by default
Date: 2024-09-06T12:15:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-09-06-microsoft-office-2024-to-disable-activex-controls-by-default

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-office-2024-to-disable-activex-controls-by-default/){:target="_blank" rel="noopener"}

> ​After Office 2024 launches in October, Microsoft will disable ActiveX controls by default in Word, Excel, PowerPoint, and Visio client apps. [...]
