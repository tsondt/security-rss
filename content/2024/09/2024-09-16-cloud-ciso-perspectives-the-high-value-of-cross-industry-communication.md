Title: Cloud CISO Perspectives: The high value of cross-industry communication
Date: 2024-09-16T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2024-09-16-cloud-ciso-perspectives-the-high-value-of-cross-industry-communication

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-the-high-value-of-cross-industry-communication/){:target="_blank" rel="noopener"}

> Welcome to the first Cloud CISO Perspectives for September 2024. Today I’m taking a look at how our initiatives to drive cybersecurity collaboration across industries, regulators and governments, IT consortia, and researchers and universities can help make everyone safer online. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI Security & CISO, Google Cloud aside_block <ListValue: [StructValue([('title', 'Get vital CISO Insights with Google Cloud'), ('body', <wagtail.rich_text.RichText object at [...]
