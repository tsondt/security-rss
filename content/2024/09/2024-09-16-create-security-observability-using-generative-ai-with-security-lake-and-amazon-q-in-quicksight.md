Title: Create security observability using generative AI with Security Lake and Amazon Q in QuickSight
Date: 2024-09-16T16:31:47+00:00
Author: Priyank Ghedia
Category: AWS Security
Tags: Advanced (300);Amazon Q;Amazon QuickSight;Amazon Security Lake;AWS CloudTrail;AWS Lake Formation;AWS Security Hub;Security, Identity, & Compliance;AI;analytics;Security;Security architecture;Security Blog
Slug: 2024-09-16-create-security-observability-using-generative-ai-with-security-lake-and-amazon-q-in-quicksight

[Source](https://aws.amazon.com/blogs/security/create-security-observability-using-generative-ai-with-security-lake-and-amazon-q-in-quicksight/){:target="_blank" rel="noopener"}

> Generative artificial intelligence (AI) is now a household topic and popular across various public applications. Users enter prompts to get answers to questions, write code, create images, improve their writing, and synthesize information. As people become familiar with generative AI, businesses are looking for ways to apply these concepts to their enterprise use cases in [...]
