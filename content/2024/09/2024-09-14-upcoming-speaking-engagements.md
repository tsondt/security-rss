Title: Upcoming Speaking Engagements
Date: 2024-09-14T16:01:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2024-09-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2024/09/upcoming-speaking-engagements-40.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking at eCrime 2024 in Boston, Massachusetts, USA. The event runs from September 24 through 26, 2024, and my keynote is at 8:45 AM ET on the 24th. I’m briefly speaking at the EPIC Champion of Freedom Awards in Washington, DC on September 25, 2024. I’m speaking at SOSS Fusion 2024 in Atlanta, Georgia, USA. The event will be held on October 22 and 23, 2024, and my talk is at 9:15 AM ET on October 22, 2024. The list is maintained on this page. [...]
