Title: PKfail Secure Boot bypass remains a significant risk two months later
Date: 2024-09-17T09:32:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2024-09-17-pkfail-secure-boot-bypass-remains-a-significant-risk-two-months-later

[Source](https://www.bleepingcomputer.com/news/security/pkfail-secure-boot-bypass-remains-a-significant-risk-two-months-later/){:target="_blank" rel="noopener"}

> Roughly nine percent of tested firmware images use non-production cryptographic keys that are publicly known or leaked in data breaches, leaving many Secure Boot devices vulnerable to UEFI bootkit malware attacks. [...]
