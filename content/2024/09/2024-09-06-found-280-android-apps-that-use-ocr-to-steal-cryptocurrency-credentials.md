Title: Found: 280 Android apps that use OCR to steal cryptocurrency credentials
Date: 2024-09-06T20:23:10+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security
Slug: 2024-09-06-found-280-android-apps-that-use-ocr-to-steal-cryptocurrency-credentials

[Source](https://arstechnica.com/?p=2047970){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Researchers have discovered more than 280 malicious apps for Android that use optical character recognition to steal cryptocurrency wallet credentials from infected devices. The apps masquerade as official ones from banks, government services, TV streaming services, and utilities. In fact, they scour infected phones for text messages, contacts, and all stored images and surreptitiously send them to remote servers controlled by the app developers. The apps are available from malicious sites and are distributed in phishing messages sent to targets. There’s no indication that any of the apps were available through Google Play. A high level [...]
