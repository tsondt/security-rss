Title: Fortinet confirms data breach after hacker claims to steal 440GB of files
Date: 2024-09-12T14:01:59-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-09-12-fortinet-confirms-data-breach-after-hacker-claims-to-steal-440gb-of-files

[Source](https://www.bleepingcomputer.com/news/security/fortinet-confirms-data-breach-after-hacker-claims-to-steal-440gb-of-files/){:target="_blank" rel="noopener"}

> Cybersecurity giant Fortinet has confirmed it suffered a data breach after a threat actor claimed to steal 440GB of files from the company's Microsoft Sharepoint server. [...]
