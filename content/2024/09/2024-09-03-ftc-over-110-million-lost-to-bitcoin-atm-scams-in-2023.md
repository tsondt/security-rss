Title: FTC: Over $110 million lost to Bitcoin ATM scams in 2023
Date: 2024-09-03T16:40:02-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-03-ftc-over-110-million-lost-to-bitcoin-atm-scams-in-2023

[Source](https://www.bleepingcomputer.com/news/security/ftc-americans-lost-over-110-million-to-bitcoin-atm-scams-in-2023/){:target="_blank" rel="noopener"}

> ​The U.S. Federal Trade Commission (FTC) has reported a massive increase in losses to Bitcoin ATM scams, nearly ten times the amount from 2020 and reaching over $110 million in 2023. [...]
