Title: HPE patches three critical security holes in Aruba PAPI
Date: 2024-09-26T19:30:14+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-26-hpe-patches-three-critical-security-holes-in-aruba-papi

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/26/hpe_aruba_patch_papi/){:target="_blank" rel="noopener"}

> More 9.8 bugs? Ay, papi! Aruba access points running AOS-8 and AOS-10 need to be patched urgently after HPE emitted fixes for three critical flaws in its networking subsidiary's networking access points.... [...]
