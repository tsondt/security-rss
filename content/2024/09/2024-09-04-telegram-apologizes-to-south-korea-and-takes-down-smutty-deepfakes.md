Title: Telegram apologizes to South Korea and takes down smutty deepfakes
Date: 2024-09-04T04:28:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-09-04-telegram-apologizes-to-south-korea-and-takes-down-smutty-deepfakes

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/04/telegram_south_korea_deepfake_apology/){:target="_blank" rel="noopener"}

> Unclear if this is a sign controversial service is cleaning up its act everywhere Controversial social network Telegram has co-operated with South Korean authorities and taken down 25 videos depicting sex crimes.... [...]
