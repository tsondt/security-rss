Title: Germany’s CDU still struggling to restore data months after June cyberattack
Date: 2024-09-16T14:32:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-16-germanys-cdu-still-struggling-to-restore-data-months-after-june-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/16/nein_luck_for_germanys_cdu/){:target="_blank" rel="noopener"}

> Putting a spanner in work for plans of opposition party to launch a comeback during next year's elections One of Germany's major political parties is still struggling to restore member data more than three months after a June cyberattack targeting its systems.... [...]
