Title: Remotely Exploding Pagers
Date: 2024-09-17T15:54:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;bombs;Hezbollah;terrorism
Slug: 2024-09-17-remotely-exploding-pagers

[Source](https://www.schneier.com/blog/archives/2024/09/remotely-exploding-pagers.html){:target="_blank" rel="noopener"}

> Wow. It seems they all exploded simultaneously, which means they were triggered. Were they each tampered with physically, or did someone figure out how to trigger a thermal runaway remotely? Supply chain attack? Malicious code update, or natural vulnerability? I have no idea, but I expect we will all learn over the next few days. EDITED TO ADD: I’m reading nine killed and 2,800 injured. That’s a lot of collateral damage. (I haven’t seen a good number as to the number of pagers yet.) EDITED TO ADD: Reuters writes : “The pagers that detonated were the latest model brought in [...]
