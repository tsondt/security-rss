Title: Russian security firm Dr.Web disconnects all servers after breach
Date: 2024-09-18T11:49:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-18-russian-security-firm-drweb-disconnects-all-servers-after-breach

[Source](https://www.bleepingcomputer.com/news/security/russian-security-firm-drweb-disconnects-all-servers-after-breach/){:target="_blank" rel="noopener"}

> On Tuesday, Russian anti-malware company Doctor Web (Dr.Web) disclosed a security breach after its systems were targeted in a cyberattack over the weekend. [...]
