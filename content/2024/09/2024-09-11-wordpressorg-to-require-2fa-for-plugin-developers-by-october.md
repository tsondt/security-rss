Title: WordPress.org to require 2FA for plugin developers by October
Date: 2024-09-11T13:33:49-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-11-wordpressorg-to-require-2fa-for-plugin-developers-by-october

[Source](https://www.bleepingcomputer.com/news/security/wordpressorg-to-require-2fa-for-plugin-developers-by-october/){:target="_blank" rel="noopener"}

> Starting October 1st, WordPress.org accounts that can push updates and changes to plugins and themes will be required to activate two-factor authentication (2FA) on their accounts. [...]
