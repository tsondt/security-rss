Title: Security Researcher Sued for Disproving Government Statements
Date: 2024-09-04T11:03:40+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;courts;data loss;lies;ransomware
Slug: 2024-09-04-security-researcher-sued-for-disproving-government-statements

[Source](https://www.schneier.com/blog/archives/2024/09/security-researcher-sued-for-disproving-government-statements.html){:target="_blank" rel="noopener"}

> This story seems straightforward. A city is the victim of a ransomware attack. They repeatedly lie to the media about the severity of the breach. A security researcher repeatedly proves their statements to be lies. The city gets mad and sues the researcher. Let’s hope the judge throws the case out, but—still—it will serve as a warning to others. [...]
