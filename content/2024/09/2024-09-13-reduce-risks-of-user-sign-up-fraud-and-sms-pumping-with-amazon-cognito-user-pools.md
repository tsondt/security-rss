Title: Reduce risks of user sign-up fraud and SMS pumping with Amazon Cognito user pools
Date: 2024-09-13T13:46:19+00:00
Author: Edward Sun
Category: AWS Security
Tags: Advanced (300);Amazon Cognito;AWS WAF;Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2024-09-13-reduce-risks-of-user-sign-up-fraud-and-sms-pumping-with-amazon-cognito-user-pools

[Source](https://aws.amazon.com/blogs/security/reduce-risks-of-user-sign-up-fraud-and-sms-pumping-with-amazon-cognito-user-pools/){:target="_blank" rel="noopener"}

> If you have a customer facing application, you might want to enable self-service sign-up, which allows potential customers on the internet to create an account and gain access to your applications. While it’s necessary to allow valid users to sign up to your application, self-service options can open the door to unintended use or sign-ups. [...]
