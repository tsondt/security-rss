Title: LiteSpeed Cache bug exposes 6 million WordPress sites to takeover attacks
Date: 2024-09-05T12:58:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-05-litespeed-cache-bug-exposes-6-million-wordpress-sites-to-takeover-attacks

[Source](https://www.bleepingcomputer.com/news/security/litespeed-cache-bug-exposes-6-million-wordpress-sites-to-takeover-attacks/){:target="_blank" rel="noopener"}

> Yet, another critical severity vulnerability has been discovered in LiteSpeed Cache, a caching plugin for speeding up user browsing in over 6 million WordPress sites. [...]
