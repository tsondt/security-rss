Title: Microsoft says it broke some Windows 10 patching – as it fixes flaws under attack
Date: 2024-09-11T01:27:09+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-11-microsoft-says-it-broke-some-windows-10-patching-as-it-fixes-flaws-under-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/11/patch_tuesday_september_2024/){:target="_blank" rel="noopener"}

> CISA wants you to leap on Citrix and Ivanti issues. Adobe, Intel, SAP also bid for patching priorities Patch Tuesday Another Patch Tuesday has dawned, as usual with the unpleasant news that there are pressing security weaknesses and blunders to address.... [...]
