Title: Avis alerts nearly 300k car renters that crooks stole their info
Date: 2024-09-09T16:45:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-09-avis-alerts-nearly-300k-car-renters-that-crooks-stole-their-info

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/09/avis_data_breach_car_rental/){:target="_blank" rel="noopener"}

> 'Insider wrongdoing' to blame for the breach Avis Rent A Car System has alerted 299,006 customers across multiple US states that their personal information was stolen in an August data breach.... [...]
