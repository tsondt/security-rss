Title: Mastercard splurges $2.65B on another big cyber purchase – Recorded Future
Date: 2024-09-12T19:00:14+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-12-mastercard-splurges-265b-on-another-big-cyber-purchase-recorded-future

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/12/mastercard_recorded_future/){:target="_blank" rel="noopener"}

> Oh, turns out there are some things money can buy Mastercard has added another security asset to its growing portfolio, laying down $2.65 billion for threat intelligence giant Recorded Future.... [...]
