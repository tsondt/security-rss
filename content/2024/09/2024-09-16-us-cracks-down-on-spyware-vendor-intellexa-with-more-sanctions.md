Title: US cracks down on spyware vendor Intellexa with more sanctions
Date: 2024-09-16T12:33:09-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-16-us-cracks-down-on-spyware-vendor-intellexa-with-more-sanctions

[Source](https://www.bleepingcomputer.com/news/security/us-cracks-down-on-spyware-vendor-intellexa-with-more-sanctions/){:target="_blank" rel="noopener"}

> Today, the U.S. Department of the Treasury has sanctioned five executives and one entity linked to the Intellexa Consortium for developing and distributing Predator commercial spyware. [...]
