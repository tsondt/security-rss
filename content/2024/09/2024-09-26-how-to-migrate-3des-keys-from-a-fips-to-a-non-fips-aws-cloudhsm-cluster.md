Title: How to migrate 3DES keys from a FIPS to a non-FIPS AWS CloudHSM cluster
Date: 2024-09-26T13:23:36+00:00
Author: Roshith Alankandy
Category: AWS Security
Tags: Advanced (300);AWS CloudHSM;Security, Identity, & Compliance;cryptography;FIPS 140-2;Security Blog
Slug: 2024-09-26-how-to-migrate-3des-keys-from-a-fips-to-a-non-fips-aws-cloudhsm-cluster

[Source](https://aws.amazon.com/blogs/security/how-to-migrate-3des-keys-from-a-fips-to-a-non-fips-aws-cloudhsm-cluster/){:target="_blank" rel="noopener"}

> On August 20, 2024, we announced the general availability of the new AWS CloudHSM hardware security module (HSM) instance type hsm2m.medium, referred to in this post as hsm2. This new type comes with additional features compared to the previous CloudHSM instance type hsm1.medium (hsm1). The new features include the following: Support for Federal Information Processing [...]
