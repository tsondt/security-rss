Title: Clever Social Engineering Attack Using Captchas
Date: 2024-09-20T15:32:37+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;captchas;malware;social engineering
Slug: 2024-09-20-clever-social-engineering-attack-using-captchas

[Source](https://www.schneier.com/blog/archives/2024/09/clever-social-engineering-attack-using-captchas.html){:target="_blank" rel="noopener"}

> This is really interesting. It’s a phishing attack targeting GitHub users, tricking them to solve a fake Captcha that actually runs a script that is copied to the command line. Clever. [...]
