Title: How to prevent account takeovers with new certificate-based access
Date: 2024-09-17T16:00:00+00:00
Author: Christopher Altman
Category: GCP Security
Tags: Security & Identity
Slug: 2024-09-17-how-to-prevent-account-takeovers-with-new-certificate-based-access

[Source](https://cloud.google.com/blog/products/identity-security/how-to-prevent-account-takeovers-with-new-certificate-based-access/){:target="_blank" rel="noopener"}

> Stolen credentials are one of the top attack vectors used by attackers to gain unauthorized access to user accounts and steal information. At Google, we’re continually evolving security capabilities and practices to make our cloud the most trusted cloud. To help protect your organization from stolen credentials, cookie theft, and accidental credential loss, we’re excited to announce the general availability of certificate-based access in our Identity and Access Management portfolio. Certificate-based access (CBA) uses mutual TLS (mTLS) to ensure that user credentials are bound to a device certificate before authorizing access to cloud resources. CBA provides strong protection requiring X.509 [...]
