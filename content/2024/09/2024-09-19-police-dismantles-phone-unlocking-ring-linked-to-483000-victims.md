Title: Police dismantles phone unlocking ring linked to 483,000 victims
Date: 2024-09-19T11:53:01-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-19-police-dismantles-phone-unlocking-ring-linked-to-483000-victims

[Source](https://www.bleepingcomputer.com/news/security/police-dismantles-iserver-phone-unlocking-network-linked-to-483-000-victims/){:target="_blank" rel="noopener"}

> A joint law enforcement operation has dismantled an international criminal network that used the iServer automated phishing-as-a-service platform to unlock the stolen or lost mobile phones of 483,000 victims worldwide. [...]
