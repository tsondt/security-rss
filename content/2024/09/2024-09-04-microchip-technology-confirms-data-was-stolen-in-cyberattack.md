Title: Microchip Technology confirms data was stolen in cyberattack
Date: 2024-09-04T18:05:04-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-04-microchip-technology-confirms-data-was-stolen-in-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/microchip-technology-confirms-data-was-stolen-in-cyberattack/){:target="_blank" rel="noopener"}

> American semiconductor supplier Microchip Technology Incorporated has confirmed that employee information was stolen from systems compromised in an August cyberattack, which was later claimed by the Play ransomware gang. [...]
