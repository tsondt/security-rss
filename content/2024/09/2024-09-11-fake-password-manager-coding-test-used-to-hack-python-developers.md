Title: Fake password manager coding test used to hack Python developers
Date: 2024-09-11T17:09:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-11-fake-password-manager-coding-test-used-to-hack-python-developers

[Source](https://www.bleepingcomputer.com/news/security/fake-password-manager-coding-test-used-to-hack-python-developers/){:target="_blank" rel="noopener"}

> Members of the North Korean hacker group Lazarus posing as recruiters are baiting Python developers with coding test project for password management products that include malware. [...]
