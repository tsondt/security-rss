Title: Windows Server 2025 previews security updates without restarts
Date: 2024-09-20T15:07:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-09-20-windows-server-2025-previews-security-updates-without-restarts

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-server-2025-hotpatching-in-public-preview-installs-security-updates-without-restarts/){:target="_blank" rel="noopener"}

> ​Microsoft announced today that Hotpatching is now available in public preview for Windows Server 2025, allowing installation of security updates without restarting. [...]
