Title: Telegram CEO was 'too free' on content moderation, says Russian minister
Date: 2024-09-02T16:35:14+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2024-09-02-telegram-ceo-was-too-free-on-content-moderation-says-russian-minister

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/02/russias_foreign_minister_claims_telegram/){:target="_blank" rel="noopener"}

> CEO Pavel Durov charged in France, messaging platform insists it abides by EU laws Telegram CEO Pavel Durov, who was cuffed and charged by the French police last week, was "too free" in his approach to managing the global messaging platform, according to Russia's foreign minister.... [...]
