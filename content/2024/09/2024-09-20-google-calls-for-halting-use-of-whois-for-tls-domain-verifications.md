Title: Google calls for halting use of WHOIS for TLS domain verifications
Date: 2024-09-20T20:53:25+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;tls;transport layer security;WHOIS
Slug: 2024-09-20-google-calls-for-halting-use-of-whois-for-tls-domain-verifications

[Source](https://arstechnica.com/?p=2051511){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Certificate authorities and browser makers are planning to end the use of WHOIS data verifying domain ownership following a report that demonstrated how threat actors could abuse the process to obtain fraudulently issued TLS certificates. TLS certificates are the cryptographic credentials that underpin HTTPS connections, a critical component of online communications verifying that a server belongs to a trusted entity and encrypts all traffic passing between it and an end user. These credentials are issued by any one of hundreds of CAs (certificate authorities) to domain owners. The rules for how certificates are issued and the [...]
