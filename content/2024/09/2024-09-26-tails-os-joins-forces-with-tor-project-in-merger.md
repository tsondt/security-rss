Title: Tails OS joins forces with Tor Project in merger
Date: 2024-09-26T20:57:23+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;anonymity;privacy;tails;Tor network;Tor Project
Slug: 2024-09-26-tails-os-joins-forces-with-tor-project-in-merger

[Source](https://arstechnica.com/?p=2052906){:target="_blank" rel="noopener"}

> Enlarge (credit: The Tor Project) The Tor Project, the nonprofit that maintains software for the Tor anonymity network, is joining forces with Tails, the maker of a portable operating system that uses Tor. Both organizations seek to pool resources, lower overhead, and collaborate more closely on their mission of online anonymity. Tails and the Tor Project began discussing the possibility of merging late last year, the two organizations said. At the time, Tails was maxing out its current resources. The two groups ultimately decided it would be mutually beneficial for them to come together. Amnesic onion routing “Rather than expanding [...]
