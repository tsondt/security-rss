Title: Victims lose $70K to one single wallet-draining app on Google's Play Store
Date: 2024-09-26T14:08:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-26-victims-lose-70k-to-one-single-wallet-draining-app-on-googles-play-store

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/26/victims_lose_70k_to_play/){:target="_blank" rel="noopener"}

> Attackers got 10K people to download 'trusted' web3 brand cheat before Mountain View intervened The latest in a long line of cryptocurrency wallet-draining attacks has stolen $70,000 from people who downloaded a dodgy app in a single campaign researchers describe as a world-first.... [...]
