Title: Chrome switching to NIST-approved ML-KEM quantum encryption
Date: 2024-09-16T12:22:41-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2024-09-16-chrome-switching-to-nist-approved-ml-kem-quantum-encryption

[Source](https://www.bleepingcomputer.com/news/security/chrome-switching-to-nist-approved-ml-kem-quantum-encryption/){:target="_blank" rel="noopener"}

> Google announced updates in the post-quantum cryptographic key encapsulation mechanism used in the Chrome browser, specifically, the swap of Kyber used in hybrid key exchanges with Module Lattice Key Encapsulation Mechanism (ML-KEM). [...]
