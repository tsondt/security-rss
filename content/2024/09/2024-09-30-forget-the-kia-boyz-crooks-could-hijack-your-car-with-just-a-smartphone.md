Title: Forget the Kia Boyz: Crooks could hijack your car with just a smartphone
Date: 2024-09-30T03:02:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-09-30-forget-the-kia-boyz-crooks-could-hijack-your-car-with-just-a-smartphone

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/30/infosec_in_brief/){:target="_blank" rel="noopener"}

> Plus: UK man charged with compromising firms for stock secrets; ransomware actor foils self; and more Infosec In Brief Put away that screwdriver and USB charging cable – the latest way to steal a Kia just requires a cellphone and the victim's license plate number.... [...]
