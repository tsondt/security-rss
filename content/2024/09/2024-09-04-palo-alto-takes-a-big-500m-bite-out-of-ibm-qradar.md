Title: Palo Alto takes a big $500M bite out of IBM QRadar
Date: 2024-09-04T22:15:15+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-04-palo-alto-takes-a-big-500m-bite-out-of-ibm-qradar

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/04/palo_alto_networks_ibm_qradar/){:target="_blank" rel="noopener"}

> Big Blue also shifts to Prisma SASE to secure its 250,000 workforce Palo Alto Networks has completed its purchase of IBM's QRadar SaaS offering, spending $500 million to buy up the service's customers and hopefully shift them into its own Cortex platform.... [...]
