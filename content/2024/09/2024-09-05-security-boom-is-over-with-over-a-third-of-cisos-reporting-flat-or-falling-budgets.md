Title: Security boom is over, with over a third of CISOs reporting flat or falling budgets
Date: 2024-09-05T14:34:10+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-05-security-boom-is-over-with-over-a-third-of-cisos-reporting-flat-or-falling-budgets

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/05/security_spending_boom_slowing/){:target="_blank" rel="noopener"}

> Good news? Security is still getting a growing part of IT budget It looks like security budgets are coming up against belt-tightening policies, with chief security officers reporting budgets rising more slowly than ever and over a third saying their spending this year will be flat or even reduced.... [...]
