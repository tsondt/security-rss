Title: Progress LoadMaster vulnerable to 10/10 severity RCE flaw
Date: 2024-09-08T10:11:21-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-08-progress-loadmaster-vulnerable-to-1010-severity-rce-flaw

[Source](https://www.bleepingcomputer.com/news/security/progress-loadmaster-vulnerable-to-10-10-severity-rce-flaw/){:target="_blank" rel="noopener"}

> Progress Software has issued an emergency fix for a maximum (10/10) severity vulnerability impacting its LoadMaster and LoadMaster Multi-Tenant (MT) Hypervisor products that allows attackers to remotely execute commands on the device. [...]
