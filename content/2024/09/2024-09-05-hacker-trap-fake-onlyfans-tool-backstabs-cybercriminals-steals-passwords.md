Title: Hacker trap: Fake OnlyFans tool backstabs cybercriminals, steals passwords
Date: 2024-09-05T05:15:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-05-hacker-trap-fake-onlyfans-tool-backstabs-cybercriminals-steals-passwords

[Source](https://www.bleepingcomputer.com/news/security/hacker-trap-fake-onlyfans-tool-backstabs-cybercriminals-steals-passwords/){:target="_blank" rel="noopener"}

> Hackers are targeting other hackers with a fake OnlyFans tool that claims to help steal accounts but instead infects threat actors with the Lumma stealer information-stealing malware. [...]
