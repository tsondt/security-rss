Title: Germany seizes 47 crypto exchanges used by ransomware gangs
Date: 2024-09-19T10:28:38-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency;Government
Slug: 2024-09-19-germany-seizes-47-crypto-exchanges-used-by-ransomware-gangs

[Source](https://www.bleepingcomputer.com/news/security/germany-seizes-47-crypto-exchanges-used-by-ransomware-gangs/){:target="_blank" rel="noopener"}

> German law enforcement seized 47 cryptocurrency exchange services hosted in the country that facilitated illegal money laundering activities for cybercriminals, including ransomware gangs. [...]
