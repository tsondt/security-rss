Title: VMware patches remote make-me-root holes in vCenter Server, Cloud Foundation
Date: 2024-09-17T20:50:24+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-17-vmware-patches-remote-make-me-root-holes-in-vcenter-server-cloud-foundation

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/17/vmware_vcenter_patch/){:target="_blank" rel="noopener"}

> Bug reports made in China Broadcom has emitted a pair of patches for vulnerabilities in VMware vCenter Server that a miscreant with network access to the software could exploit to completely commandeer a system. This also affects Cloud Foundation.... [...]
