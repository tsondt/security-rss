Title: D-Link says it is not fixing four RCE flaws in DIR-846W routers
Date: 2024-09-03T11:46:59-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2024-09-03-d-link-says-it-is-not-fixing-four-rce-flaws-in-dir-846w-routers

[Source](https://www.bleepingcomputer.com/news/security/d-link-says-it-is-not-fixing-four-rce-flaws-in-dir-846w-routers/){:target="_blank" rel="noopener"}

> D-Link is warning that four remote code execution (RCE) flaws impacting all hardware and firmware versions of its DIR-846W router will not be fixed as the products are no longer supported. [...]
