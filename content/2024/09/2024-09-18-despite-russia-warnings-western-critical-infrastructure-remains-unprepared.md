Title: Despite Russia warnings, Western critical infrastructure remains unprepared
Date: 2024-09-18T09:15:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-18-despite-russia-warnings-western-critical-infrastructure-remains-unprepared

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/18/russia_west_critical_infrastructure/){:target="_blank" rel="noopener"}

> 'Lives will be lost' as Moscow ramps up offensive cyber military units Feature As Russian special forces push more overtly into online operations, network defenders should be on the hunt for digital intruders looking to carry out cyberattacks that end in physical destruction and harm.... [...]
