Title: Cisco's Smart Licensing Utility flaws suggest it's pretty dumb on security
Date: 2024-09-05T18:15:08+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-05-ciscos-smart-licensing-utility-flaws-suggest-its-pretty-dumb-on-security

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/05/cisco_smart_licensing_utility_flaws/){:target="_blank" rel="noopener"}

> Two critical holes including hardcoded admin credential If you're running Cisco's supposedly Smart Licensing Utility, there are two flaws you ought to patch right now.... [...]
