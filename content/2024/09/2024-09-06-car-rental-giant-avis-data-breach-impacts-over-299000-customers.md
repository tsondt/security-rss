Title: Car rental giant Avis data breach impacts over 299,000 customers
Date: 2024-09-06T14:04:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Software
Slug: 2024-09-06-car-rental-giant-avis-data-breach-impacts-over-299000-customers

[Source](https://www.bleepingcomputer.com/news/security/car-rental-giant-avis-data-breach-impacts-over-299-000-customers/){:target="_blank" rel="noopener"}

> American car rental giant Avis disclosed a data breach after attackers breached one of its business applications last month and stole customer personal information. [...]
