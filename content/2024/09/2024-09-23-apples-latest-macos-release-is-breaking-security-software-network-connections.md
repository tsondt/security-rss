Title: Apple's latest macOS release is breaking security software, network connections
Date: 2024-09-23T00:50:38+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-09-23-apples-latest-macos-release-is-breaking-security-software-network-connections

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/23/security_in_brief/){:target="_blank" rel="noopener"}

> PLUS: Payer of $75M ransom reportedly identified; Craigslist founder becomes security philanthropist, and more Infosec In Brief Something's wrong with macOS Sequoia, and it's breaking security software installed on some updated Apple systems.... [...]
