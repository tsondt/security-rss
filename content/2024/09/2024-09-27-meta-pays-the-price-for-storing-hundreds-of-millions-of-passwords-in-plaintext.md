Title: Meta pays the price for storing hundreds of millions of passwords in plaintext
Date: 2024-09-27T17:53:28+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;authentication;cryptography;hashing;passwords;privacy
Slug: 2024-09-27-meta-pays-the-price-for-storing-hundreds-of-millions-of-passwords-in-plaintext

[Source](https://arstechnica.com/?p=2053118){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Officials in Ireland have fined Meta $101 million for storing hundreds of millions of user passwords in plaintext and making them broadly available to company employees. Meta disclosed the lapse in early 2019. The company said that apps for connecting to various Meta-owned social networks had logged user passwords in plaintext and stored them in a database that had been searched by roughly 2,000 company engineers, who collectively queried the stash more than 9 million times. Meta investigated for five years Meta officials said at the time that the error was found during a routine security [...]
