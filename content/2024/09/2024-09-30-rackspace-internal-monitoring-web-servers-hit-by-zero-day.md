Title: Rackspace internal monitoring web servers hit by zero-day
Date: 2024-09-30T23:08:37+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-30-rackspace-internal-monitoring-web-servers-hit-by-zero-day

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/30/rackspace_zero_day_attack/){:target="_blank" rel="noopener"}

> Intruders accessed machines via tool bundled with ScienceLogic, 'limited' info taken, customers told not to worry Exclusive Rackspace has told customers intruders exploited a zero-day bug in a third-party application it was using, and abused that vulnerability to break into its internal performance monitoring environment.... [...]
