Title: AT&T pays $13 million FCC settlement over 2023 data breach
Date: 2024-09-17T13:36:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-17-att-pays-13-million-fcc-settlement-over-2023-data-breach

[Source](https://www.bleepingcomputer.com/news/security/atandt-pays-13-million-fcc-settlement-over-2023-data-breach/){:target="_blank" rel="noopener"}

> The Federal Communications Commission (FCC) has reached a $13 million settlement with AT&T to resolve a probe into whether the telecom giant failed to protect customer data after a vendor's cloud environment was breached three years ago. [...]
