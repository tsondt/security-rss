Title: US indicts two over socially engineered $230M+ crypto heist
Date: 2024-09-20T17:29:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-20-us-indicts-two-over-socially-engineered-230m-crypto-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/20/us_indicts_two_over_socially/){:target="_blank" rel="noopener"}

> Just one victim milked of nearly a quarter of a billion bucks Two individuals are in cuffs and facing serious charges in connection to a major theft of cryptocurrency worth more than $230 million from a single victim.... [...]
