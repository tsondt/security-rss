Title: Red team tool ‘MacroPack’ abused in attacks to deploy Brute Ratel
Date: 2024-09-04T16:31:46-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-04-red-team-tool-macropack-abused-in-attacks-to-deploy-brute-ratel

[Source](https://www.bleepingcomputer.com/news/security/red-team-tool-macropack-abused-in-attacks-to-deploy-brute-ratel/){:target="_blank" rel="noopener"}

> The MacroPack framework, initially designed for Red Team exercises, is being abused by threat actors to deploy malicious payloads, including Havoc, Brute Ratel, and PhatomCore. [...]
