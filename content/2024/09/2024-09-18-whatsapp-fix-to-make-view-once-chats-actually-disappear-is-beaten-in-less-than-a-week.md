Title: WhatsApp fix to make View Once chats actually disappear is beaten in less than a week
Date: 2024-09-18T00:16:06+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-18-whatsapp-fix-to-make-view-once-chats-actually-disappear-is-beaten-in-less-than-a-week

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/18/whatsapp_view_once_flaw_unfixed/){:target="_blank" rel="noopener"}

> View Forever, more like it, as Meta's privacy feature again revealed to be futile with a little light hacking A fix deployed by Meta to stop people repeatedly viewing WhatsApp’s so-called View Once messages – photos, videos, and voice recordings that disappear from chats after a recipient sees them – has been defeated in less than a week by white-hat hackers.... [...]
