Title: Cisco warns of backdoor admin account in Smart Licensing Utility
Date: 2024-09-04T12:58:59-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-04-cisco-warns-of-backdoor-admin-account-in-smart-licensing-utility

[Source](https://www.bleepingcomputer.com/news/security/cisco-warns-of-backdoor-admin-account-in-smart-licensing-utility/){:target="_blank" rel="noopener"}

> Cisco has removed a backdoor account in the Cisco Smart Licensing Utility (CSLU) that can be used to log into unpatched systems with administrative privileges. [...]
