Title: That doomsday critical Linux bug: It's CUPS. May lead to remote hijacking of devices
Date: 2024-09-26T17:34:01+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-26-that-doomsday-critical-linux-bug-its-cups-may-lead-to-remote-hijacking-of-devices

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/26/cups_linux_rce_disclosed/){:target="_blank" rel="noopener"}

> No patches yet, can be mitigated, requires user interaction Final update After days of anticipation, what was billed as one or more critical unauthenticated remote-code execution vulnerabilities in all Linux systems was today finally revealed.... [...]
