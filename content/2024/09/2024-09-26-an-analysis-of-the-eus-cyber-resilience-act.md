Title: An Analysis of the EU’s Cyber Resilience Act
Date: 2024-09-26T11:03:29+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;EU;laws;resilience
Slug: 2024-09-26-an-analysis-of-the-eus-cyber-resilience-act

[Source](https://www.schneier.com/blog/archives/2024/09/an-analysis-of-the-eus-cyber-resilience-act.html){:target="_blank" rel="noopener"}

> A good —long, complex—analysis of the EU’s new Cyber Resilience Act. [...]
