Title: Deja blues... LockBit boasts once again of ransoming IRS-authorized eFile.com
Date: 2024-09-18T20:14:28+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-18-deja-blues-lockbit-boasts-once-again-of-ransoming-irs-authorized-efilecom

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/18/lockbit_tax_site_claim/){:target="_blank" rel="noopener"}

> Add 'ransomware' to the list of certainties in life? In an intriguing move, notorious ransomware gang LockBit claims once again to have compromised eFile.com, which offers online services for electronically filing tax returns with the US Internal Revenue Service (IRS).... [...]
