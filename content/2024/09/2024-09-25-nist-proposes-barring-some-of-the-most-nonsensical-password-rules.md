Title: NIST proposes barring some of the most nonsensical password rules
Date: 2024-09-25T22:39:38+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;authentication;NIST;passwords
Slug: 2024-09-25-nist-proposes-barring-some-of-the-most-nonsensical-password-rules

[Source](https://arstechnica.com/?p=2052508){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) The National Institute of Standards and Technology (NIST), the federal body that sets technology standards for governmental agencies, standards organizations, and private companies, has proposed barring some of the most vexing and nonsensical password requirements. Chief among them: mandatory resets, required or restricted use of certain characters, and the use of security questions. Choosing strong passwords and storing them safely is one of the most challenging parts of a good cybersecurity regimen. More challenging still is complying with password rules imposed by employers, federal agencies, and providers of online services. Frequently, the rules—ostensibly to enhance security [...]
