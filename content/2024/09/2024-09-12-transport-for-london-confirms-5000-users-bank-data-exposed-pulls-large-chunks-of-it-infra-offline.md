Title: Transport for London confirms 5,000 users' bank data exposed, pulls large chunks of IT infra offline
Date: 2024-09-12T14:54:50+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-09-12-transport-for-london-confirms-5000-users-bank-data-exposed-pulls-large-chunks-of-it-infra-offline

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/12/transport_for_londons_cyber_attack/){:target="_blank" rel="noopener"}

> NCA confirms arrest of 17-year-old 'on suspicion of Computer Misuse Act offences' – now bailed Transport for London's ongoing cyber incident has taken a dark turn as the organization confirmed that some data, including bank details, might have been accessed, and 30,000 employees' passwords will need to be reset via in-person appointments.... [...]
