Title: How to reduce cyber risk during employee onboarding
Date: 2024-09-19T10:02:12-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2024-09-19-how-to-reduce-cyber-risk-during-employee-onboarding

[Source](https://www.bleepingcomputer.com/news/security/how-to-reduce-cyber-risk-during-employee-onboarding/){:target="_blank" rel="noopener"}

> Onboarding new employees is an important time for any organization but comes with a unique set of security risks. Learn more from Specops Software about these risks and how to mitigate them. [...]
