Title: CISA boss: Makers of insecure software must stop enabling today's cyber villains
Date: 2024-09-20T00:33:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-20-cisa-boss-makers-of-insecure-software-must-stop-enabling-todays-cyber-villains

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/20/cisa_software_cybercrime_villains/){:target="_blank" rel="noopener"}

> Write better code, urges Jen Easterly. And while you're at it, give crime gangs horrible names like 'Evil Ferret' Software suppliers who ship buggy, insecure code need to stop enabling cyber criminals who exploit those vulnerabilities to rob victims, Jen Easterly, boss of the US government's Cybersecurity and Infrastructure Security Agency, has argued.... [...]
