Title: Ukraine bans Telegram on military, govt devices over security risks
Date: 2024-09-20T13:37:48-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-20-ukraine-bans-telegram-on-military-govt-devices-over-security-risks

[Source](https://www.bleepingcomputer.com/news/security/ukraine-bans-telegram-on-military-govt-devices-over-security-risks/){:target="_blank" rel="noopener"}

> Ukraine's National Coordination Centre for Cybersecurity (NCCC) has restricted the use of the Telegram messaging app within government agencies, military units, and critical infrastructure, citing national security concerns. [...]
