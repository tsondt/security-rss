Title: How to defend against brute force and password spray attacks
Date: 2024-09-09T10:02:27-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2024-09-09-how-to-defend-against-brute-force-and-password-spray-attacks

[Source](https://www.bleepingcomputer.com/news/security/how-to-defend-against-brute-force-and-password-spray-attacks/){:target="_blank" rel="noopener"}

> While not very sophisticated, brute force password attacks pose a significant threat to an organization's security. Learn more from Specops Software about these types of attacks and how to defend against them. [...]
