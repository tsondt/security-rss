Title: TfL requires in-person password resets for 30,000 employees after hack
Date: 2024-09-13T17:12:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-13-tfl-requires-in-person-password-resets-for-30000-employees-after-hack

[Source](https://www.bleepingcomputer.com/news/security/tfl-requires-in-person-password-resets-for-30-000-employees-after-hack/){:target="_blank" rel="noopener"}

> ​Transport for London (TfL) says that all staff (roughly 30,000 employees) must attend in-person appointments to verify their identities and reset passwords following a cybersecurity incident disclosed almost two weeks ago. [...]
