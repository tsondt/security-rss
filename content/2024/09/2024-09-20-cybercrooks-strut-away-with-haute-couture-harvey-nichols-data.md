Title: Cybercrooks strut away with haute couture Harvey Nichols data
Date: 2024-09-20T09:27:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-20-cybercrooks-strut-away-with-haute-couture-harvey-nichols-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/20/highstreet_swank_dealer_harvey_nichols/){:target="_blank" rel="noopener"}

> Nothing high-end about the sparsely detailed, poorly publicized breach High-end British department store Harvey Nichols is writing to customers to confirm some of their data was exposed in a recent cyberattack.... [...]
