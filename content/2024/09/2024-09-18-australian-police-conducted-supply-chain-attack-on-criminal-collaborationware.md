Title: Australian Police conducted supply chain attack on criminal collaborationware
Date: 2024-09-18T02:32:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-09-18-australian-police-conducted-supply-chain-attack-on-criminal-collaborationware

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/18/afp_operation_kraken_ghost_crimeware_app/){:target="_blank" rel="noopener"}

> Sting led to cuffing of alleged operator behind Ghost – an app for drug trafficking, money laundering, and violence-as-a-service Australia's Federal Police (AFP) yesterday arrested and charged a man with creating and administering an app named Ghost that was allegedly "a dedicated encrypted communication platform... built solely for the criminal underworld" and which enabled crims to arrange acts of violence, launder money, and traffic illicit drugs.... [...]
