Title: Data watchdog fines Clearview AI $33M for 'illegal' data collection
Date: 2024-09-03T15:30:06+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-09-03-data-watchdog-fines-clearview-ai-33m-for-illegal-data-collection

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/03/clearview_ai_dutch_fine/){:target="_blank" rel="noopener"}

> Selfie-scraper again claims European law does not apply to it The Dutch Data Protection Authority (DPA) has fined controversial facial recognition company Clearview AI €30.5 million ($33 million) over the "illegal" collation of images.... [...]
