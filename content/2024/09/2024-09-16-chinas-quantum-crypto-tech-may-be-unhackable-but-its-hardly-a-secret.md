Title: China’s quantum* crypto tech may be unhackable, but it's hardly a secret
Date: 2024-09-16T08:30:15+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2024-09-16-chinas-quantum-crypto-tech-may-be-unhackable-but-its-hardly-a-secret

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/16/opinion_column_quantum/){:target="_blank" rel="noopener"}

> * Quite Unlikely A New Technology’s Useful, Man Opinion We have a new call to arms in the 21st century battlefront between the West and China. The Middle Kingdom is building an uncrackable national infrastructure based on quantum key distribution (QKD). The laws of physics are being used against us, and we're not keeping up, claims a think tank.... [...]
