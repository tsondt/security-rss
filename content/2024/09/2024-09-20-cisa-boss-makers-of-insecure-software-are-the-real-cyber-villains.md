Title: CISA boss: Makers of insecure software are the real cyber villains
Date: 2024-09-20T00:33:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-20-cisa-boss-makers-of-insecure-software-are-the-real-cyber-villains

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/20/cisa_sloppy_vendors_cybercrime_villains/){:target="_blank" rel="noopener"}

> Write better code, urges Jen Easterly. And while you're at it, give crime gangs horrible names like 'Evil Ferret' Software developers who ship buggy, insecure code are the real villains in the cyber crime story, according to Jen Easterly, boss of the US government's Cybersecurity and Infrastructure Security Agency.... [...]
