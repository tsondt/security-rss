Title: Zyxel warns of critical OS command injection flaw in routers
Date: 2024-09-03T15:59:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2024-09-03-zyxel-warns-of-critical-os-command-injection-flaw-in-routers

[Source](https://www.bleepingcomputer.com/news/security/zyxel-warns-of-critical-os-command-injection-flaw-in-routers/){:target="_blank" rel="noopener"}

> Zyxel has released security updates to address a critical vulnerability impacting multiple models of its business routers, potentially allowing unauthenticated attackers to perform OS command injection. [...]
