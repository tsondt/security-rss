Title: How to spot a North Korean agent before they get comfy inside payroll
Date: 2024-09-24T12:01:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-24-how-to-spot-a-north-korean-agent-before-they-get-comfy-inside-payroll

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/24/mandiant_north_korea_workers/){:target="_blank" rel="noopener"}

> Mandiant publishes cheat sheet for weeding out fraudulent IT staff Against a backdrop of rising exposure to North Korean agents seeking (mainly) US IT roles, organizations now have a cheat sheet to help spot potential operatives.... [...]
