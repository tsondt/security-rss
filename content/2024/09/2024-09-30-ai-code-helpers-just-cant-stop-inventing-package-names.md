Title: AI code helpers just can't stop inventing package names
Date: 2024-09-30T03:59:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-09-30-ai-code-helpers-just-cant-stop-inventing-package-names

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/30/ai_code_helpers_invent_packages/){:target="_blank" rel="noopener"}

> LLMs are helpful, but don't use them for anything important AI models just can't seem to stop making things up. As two recent studies point out, that proclivity underscores prior warnings not to rely on AI advice for anything that really matters.... [...]
