Title: Verkada to pay $2.95M for security failures leading to breaches
Date: 2024-09-02T12:06:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-09-02-verkada-to-pay-295m-for-security-failures-leading-to-breaches

[Source](https://www.bleepingcomputer.com/news/security/verkada-to-pay-295m-for-security-failures-leading-to-breaches/){:target="_blank" rel="noopener"}

> The Federal Trade Commission (FTC) proposes a $2.95 million penalty on security camera vendor Verkada for multiple security failures that enabled hackers to access live video feeds from 150,000 internet-connected cameras. [...]
