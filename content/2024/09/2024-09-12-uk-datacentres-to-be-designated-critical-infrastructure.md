Title: UK datacentres to be designated critical infrastructure
Date: 2024-09-12T07:42:13+00:00
Author: PA Media
Category: The Guardian
Tags: Data protection;Technology;Data and computer security;UK news;Business;Politics;Cybercrime;Internet
Slug: 2024-09-12-uk-datacentres-to-be-designated-critical-infrastructure

[Source](https://www.theguardian.com/technology/2024/sep/12/uk-datacentres-critical-infrastructure-cyber-protection){:target="_blank" rel="noopener"}

> Facilities to receive greater protection in attempt to reduce potential impact of adverse incidents or attacks Datacentres in the UK are to be designated as critical national infrastructure in an effort to protect them from cyber-attacks and IT blackouts, the government has said. The buildings store much of the data generated in the UK, including photos taken on smartphones, financial information and NHS records. Continue reading... [...]
