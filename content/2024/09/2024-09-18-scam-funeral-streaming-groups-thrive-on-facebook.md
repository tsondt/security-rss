Title: Scam ‘Funeral Streaming’ Groups Thrive on Facebook
Date: 2024-09-18T13:43:51+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Latest Warnings;Web Fraud 2.0;148.251.54.196;apkdownloadweb;Constella Intelligence;domaintools;facebook funeral scams;Mohammod Abdullah Khondokar;Mohammod Mehedi Hasan;Webhostbd
Slug: 2024-09-18-scam-funeral-streaming-groups-thrive-on-facebook

[Source](https://krebsonsecurity.com/2024/09/scam-funeral-streaming-groups-thrive-on-facebook/){:target="_blank" rel="noopener"}

> Scammers are flooding Facebook with groups that purport to offer video streaming of funeral services for the recently deceased. Friends and family who follow the links for the streaming services are then asked to cough up their credit card information. Recently, these scammers have branched out into offering fake streaming services for nearly any kind of event advertised on Facebook. Here’s a closer look at the size of this scheme, and some findings about who may be responsible. One of the many scam funeral group pages on Facebook. Clicking to view the “live stream” of the funeral takes one to [...]
