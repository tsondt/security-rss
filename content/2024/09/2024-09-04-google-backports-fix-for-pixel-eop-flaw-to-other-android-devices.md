Title: Google backports fix for Pixel EoP flaw to other Android devices
Date: 2024-09-04T11:16:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2024-09-04-google-backports-fix-for-pixel-eop-flaw-to-other-android-devices

[Source](https://www.bleepingcomputer.com/news/security/google-backports-fix-for-pixel-eop-flaw-to-other-android-devices/){:target="_blank" rel="noopener"}

> Google has released the September 2024 Android security updates to fix 34 vulnerabilities, including CVE-2024-32896, an actively exploited elevation of privilege flaw that was previously fixed on Pixel devices. [...]
