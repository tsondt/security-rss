Title: Crypto scams rake in $5.6B a year for cyberscum lowlifes, FBI says
Date: 2024-09-10T14:29:35+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-10-crypto-scams-rake-in-56b-a-year-for-cyberscum-lowlifes-fbi-says

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/10/crypto_scams_rake_in_56/){:target="_blank" rel="noopener"}

> Elderly people report the greatest losses The FBI just dropped its annual report examining the costs of crypto-related cybercrime, painting a predictably grim picture as total losses in the US exceeded $5.6 billion in 2023 – a 45 percent year-on-year increase.... [...]
