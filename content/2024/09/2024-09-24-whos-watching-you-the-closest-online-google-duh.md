Title: Who’s watching you the closest online? Google, duh
Date: 2024-09-24T19:45:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-24-whos-watching-you-the-closest-online-google-duh

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/24/google_online_tracker/){:target="_blank" rel="noopener"}

> Four Chocolate Factory trackers cracked the Top 25 in all regions Google, once again, is the "undisputed leader" when it comes to monitoring people's behavior on the internet, according to Kaspersky's annual web tracking report.... [...]
