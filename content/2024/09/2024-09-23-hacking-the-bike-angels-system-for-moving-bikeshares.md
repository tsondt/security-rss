Title: Hacking the “Bike Angels” System for Moving Bikeshares
Date: 2024-09-23T15:46:56+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;hacking;noncomputer hacks;scams
Slug: 2024-09-23-hacking-the-bike-angels-system-for-moving-bikeshares

[Source](https://www.schneier.com/blog/archives/2024/09/hacking-the-bike-angels-system-for-moving-bikeshares.html){:target="_blank" rel="noopener"}

> I always like a good hack. And this story delivers. Basically, the New York City bikeshare program has a system to reward people who move bicycles from full stations to empty ones. By deliberately moving bikes to create artificial problems, and exploiting exactly how the system calculates rewards, some people are making a lot of money. At 10 a.m. on a Tuesday last month, seven Bike Angels descended on the docking station at Broadway and 53rd Street, across from the Ed Sullivan Theater. Each rider used his own special blue key -­- a reward from Citi Bike—­ to unlock a [...]
