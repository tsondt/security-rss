Title: Sextortion scams now use your "cheating" spouse’s name as a lure
Date: 2024-09-07T11:14:25-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-09-07-sextortion-scams-now-use-your-cheating-spouses-name-as-a-lure

[Source](https://www.bleepingcomputer.com/news/security/sextortion-scams-now-use-your-cheating-spouses-name-as-a-lure/){:target="_blank" rel="noopener"}

> A new variant of the ongoing sextortion email scams is now targeting spouses, saying that their husband or wife is cheating on them, with links to the alleged proof. [...]
