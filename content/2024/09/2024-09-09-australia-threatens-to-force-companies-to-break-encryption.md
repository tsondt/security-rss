Title: Australia Threatens to Force Companies to Break Encryption
Date: 2024-09-09T11:03:31+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Australia;backdoors;crypto wars;encryption;laws
Slug: 2024-09-09-australia-threatens-to-force-companies-to-break-encryption

[Source](https://www.schneier.com/blog/archives/2024/09/australia-threatens-to-force-companies-to-break-encryption.html){:target="_blank" rel="noopener"}

> In 2018, Australia passed the Assistance and Access Act, which—among other things—gave the government the power to force companies to break their own encryption. The Assistance and Access Act includes key components that outline investigatory powers between government and industry. These components include: Technical Assistance Requests (TARs): TARs are voluntary requests for assistance accessing encrypted data from law enforcement to teleco and technology companies. Companies are not legally obligated to comply with a TAR but law enforcement sends requests to solicit cooperation. Technical Assistance Notices (TANs): TANS are compulsory notices (such as computer access warrants) that require companies to assist [...]
