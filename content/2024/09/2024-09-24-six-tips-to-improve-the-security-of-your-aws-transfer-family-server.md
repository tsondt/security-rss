Title: Six tips to improve the security of your AWS Transfer Family server
Date: 2024-09-24T13:46:28+00:00
Author: John Jamail
Category: AWS Security
Tags: AWS Transfer Family;Best Practices;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2024-09-24-six-tips-to-improve-the-security-of-your-aws-transfer-family-server

[Source](https://aws.amazon.com/blogs/security/six-tips-to-improve-the-security-of-your-aws-transfer-family-server/){:target="_blank" rel="noopener"}

> AWS Transfer Family is a secure transfer service that lets you transfer files directly into and out of Amazon Web Services (AWS) storage services using popular protocols such as AS2, SFTP, FTPS, and FTP. When you launch a Transfer Family server, there are multiple options that you can choose depending on what you need to [...]
