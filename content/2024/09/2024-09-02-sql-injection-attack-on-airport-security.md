Title: SQL Injection Attack on Airport Security
Date: 2024-09-02T11:07:04+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;air travel;SQL injection;TSA
Slug: 2024-09-02-sql-injection-attack-on-airport-security

[Source](https://www.schneier.com/blog/archives/2024/09/sql-injection-attack-on-airport-security.html){:target="_blank" rel="noopener"}

> Interesting vulnerability :...a special lane at airport security called Known Crewmember (KCM). KCM is a TSA program that allows pilots and flight attendants to bypass security screening, even when flying on domestic personal trips. The KCM process is fairly simple: the employee uses the dedicated lane and presents their KCM barcode or provides the TSA agent their employee number and airline. Various forms of ID need to be presented while the TSA agent’s laptop verifies the employment status with the airline. If successful, the employee can access the sterile area without any screening at all. A similar system also exists [...]
