Title: Automate detection and response to website defacement with Amazon CloudWatch Synthetics
Date: 2024-09-20T16:41:45+00:00
Author: Agus Komang
Category: AWS Security
Tags: Advanced (300);Amazon CloudWatch;Customer Solutions;Security, Identity, & Compliance;Security;Security Blog
Slug: 2024-09-20-automate-detection-and-response-to-website-defacement-with-amazon-cloudwatch-synthetics

[Source](https://aws.amazon.com/blogs/security/automate-detection-and-response-to-website-defacement-with-amazon-cloudwatch-synthetics/){:target="_blank" rel="noopener"}

> Website defacement occurs when threat actors gain unauthorized access to a website, most commonly a public website, and replace content on the site with their own messages. In this blog post, we show you how to detect website defacement, and then automate both defacement verification and your defacement response by using Amazon CloudWatch Synthetics visual [...]
