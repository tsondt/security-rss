Title: Construction firms breached in brute force attacks on accounting software
Date: 2024-09-17T15:42:59-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-17-construction-firms-breached-in-brute-force-attacks-on-accounting-software

[Source](https://www.bleepingcomputer.com/news/security/construction-firms-breached-in-brute-force-attacks-on-accounting-software/){:target="_blank" rel="noopener"}

> Hackers are brute-forcing passwords for highly privileged accounts on exposed Foundation accounting servers, widely used in the construction industry, to breach corporate networks. [...]
