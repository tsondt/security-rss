Title: CISA boss: Makers of insecure software are enablers of the real villains
Date: 2024-09-20T00:33:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-20-cisa-boss-makers-of-insecure-software-are-enablers-of-the-real-villains

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/20/cisa_sloppy_vendors_cybercrime_villains/){:target="_blank" rel="noopener"}

> Write better code, urges Jen Easterly. And while you're at it, give crime gangs horrible names like 'Evil Ferret' Software suppliers who ship buggy, insecure code are the true baddies in the cyber crime story, Jen Easterly, boss of the US government's Cybersecurity and Infrastructure Security Agency, has argued.... [...]
