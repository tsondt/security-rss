Title: Generative AI Security: Getting ready for Salesforce Einstein Copilot
Date: 2024-09-24T10:02:04-04:00
Author: Sponsored by Varonis
Category: BleepingComputer
Tags: Security
Slug: 2024-09-24-generative-ai-security-getting-ready-for-salesforce-einstein-copilot

[Source](https://www.bleepingcomputer.com/news/security/generative-ai-security-getting-ready-for-salesforce-einstein-copilot/){:target="_blank" rel="noopener"}

> Salesforce's Einstein Copilot can provide insights and perform tasks help streamline daily processes. However, it also comes with risks that you should takes steps to mitigate. Learn more from Varonis on how to prepare for Salesforce Einstein Copilot, [...]
