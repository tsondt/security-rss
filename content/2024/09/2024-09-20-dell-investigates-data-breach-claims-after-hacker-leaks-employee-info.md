Title: Dell investigates data breach claims after hacker leaks employee info
Date: 2024-09-20T12:30:47-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-20-dell-investigates-data-breach-claims-after-hacker-leaks-employee-info

[Source](https://www.bleepingcomputer.com/news/security/dell-investigates-data-breach-claims-after-hacker-leaks-employee-info/){:target="_blank" rel="noopener"}

> Dell has confirmed to BleepingComputer that they are investigating recent claims that it suffered a data breach after a threat actor leaked the data for over 10,000 employees. [...]
