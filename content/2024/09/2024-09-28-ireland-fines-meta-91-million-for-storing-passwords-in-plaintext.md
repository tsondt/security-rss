Title: Ireland fines Meta €91 million for storing passwords in plaintext
Date: 2024-09-28T10:16:23-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;Security
Slug: 2024-09-28-ireland-fines-meta-91-million-for-storing-passwords-in-plaintext

[Source](https://www.bleepingcomputer.com/news/legal/ireland-fines-meta-91-million-for-storing-passwords-in-plaintext/){:target="_blank" rel="noopener"}

> The Data Protection Commission (DPC) in Ireland has fined Meta Platforms Ireland Limited (MPIL) €91 million ($100 million) for storing in plaintext passwords of hundreds of millions of users. [...]
