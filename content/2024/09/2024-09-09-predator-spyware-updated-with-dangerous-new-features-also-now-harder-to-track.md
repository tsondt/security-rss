Title: Predator spyware updated with dangerous new features, also now harder to track
Date: 2024-09-09T02:00:23+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-09-09-predator-spyware-updated-with-dangerous-new-features-also-now-harder-to-track

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/09/predator_spyware_trump_crypto/){:target="_blank" rel="noopener"}

> Plus: Trump family X accounts hijacked to promote crypto scam; Fog ransomware spreads; Hijacked PyPI packages; and more Infosec in brief After activating its chameleon field and going to ground following press attention earlier this year, the dangerous Predator commercial spyware kit is back – with upgrades.... [...]
