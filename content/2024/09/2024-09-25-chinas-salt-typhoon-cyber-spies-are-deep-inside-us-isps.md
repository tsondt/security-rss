Title: China's Salt Typhoon cyber spies are deep inside US ISPs
Date: 2024-09-25T21:46:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-25-chinas-salt-typhoon-cyber-spies-are-deep-inside-us-isps

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/25/chinas_salt_typhoon_cyber_spies/){:target="_blank" rel="noopener"}

> Expecting a longer storm season this year? Updated Another Beijing-linked cyberspy crew, this one dubbed Salt Typhoon, has reportedly been spotted on networks belonging to US internet service providers in stealthy data-stealing missions and potential preparation for future cyberattacks.... [...]
