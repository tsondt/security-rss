Title: White House thinks it's time to fix the insecure glue of the internet: Yup, BGP
Date: 2024-09-03T22:34:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-09-03-white-house-thinks-its-time-to-fix-the-insecure-glue-of-the-internet-yup-bgp

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/03/white_house_bgp_security/){:target="_blank" rel="noopener"}

> Better late than never The White House on Tuesday indicated it hopes to shore up the weak security of internet routing, specifically the Border Gateway Protocol (BGP).... [...]
