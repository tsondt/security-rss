Title: Ivanti fixes maximum severity RCE bug in Endpoint Management software
Date: 2024-09-10T15:28:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-10-ivanti-fixes-maximum-severity-rce-bug-in-endpoint-management-software

[Source](https://www.bleepingcomputer.com/news/security/ivanti-fixes-maximum-severity-rce-bug-in-endpoint-management-software/){:target="_blank" rel="noopener"}

> Ivanti has fixed a maximum severity vulnerability in its Endpoint Management software (EPM) that can let unauthenticated attackers gain remote code execution on the core server. [...]
