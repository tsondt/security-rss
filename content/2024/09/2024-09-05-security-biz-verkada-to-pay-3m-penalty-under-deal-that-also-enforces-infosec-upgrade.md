Title: Security biz Verkada to pay $3M penalty under deal that also enforces infosec upgrade
Date: 2024-09-05T04:28:07+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-05-security-biz-verkada-to-pay-3m-penalty-under-deal-that-also-enforces-infosec-upgrade

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/05/verkada_ftc_settlement/){:target="_blank" rel="noopener"}

> Allowed access to 150k cameras, some in sensitive spots, but has been done for spamming Physical security biz Verkada has agreed to cough up $2.95 million following an investigation by the US Federal Trade Commission (FTC) – but the payment won’t make good its past security failings, including a blunder that led to CCTV footage of Tesla, Cloudflare, and others being snooped on. Instead, the fine is about spam.... [...]
