Title: EU kicks off an inquiry into Google's AI model
Date: 2024-09-12T12:15:08+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-09-12-eu-kicks-off-an-inquiry-into-googles-ai-model

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/12/google_ai_model_inquiry_eu/){:target="_blank" rel="noopener"}

> Privacy regulator taking a closer look at data privacy and PaLM 2 The European Union's key regulator for data privacy, Ireland's Data Protection Commission (DPC), has launched a cross-border inquiry into Google's AI model to ascertain if it complies with the bloc's rules.... [...]
