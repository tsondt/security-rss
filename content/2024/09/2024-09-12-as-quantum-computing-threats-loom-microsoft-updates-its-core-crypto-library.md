Title: As quantum computing threats loom, Microsoft updates its core crypto library
Date: 2024-09-12T00:20:08+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;cryptography;encryption;microsoft;quantum computing
Slug: 2024-09-12-as-quantum-computing-threats-loom-microsoft-updates-its-core-crypto-library

[Source](https://arstechnica.com/?p=2049244){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Microsoft has updated a key cryptographic library with two new encryption algorithms designed to withstand attacks from quantum computers. The updates were made last week to SymCrypt, a core cryptographic code library for handing cryptographic functions in Windows and Linux. The library, started in 2006, provides operations and algorithms developers can use to safely implement secure encryption, decryption, signing, verification, hashing, and key exchange in the apps they create. The library supports federal certification requirements for cryptographic modules used in some governmental environments. Massive overhaul underway Despite the name, SymCrypt supports both symmetric and asymmetric algorithms. [...]
