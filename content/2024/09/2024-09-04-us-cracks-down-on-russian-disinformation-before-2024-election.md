Title: US cracks down on Russian disinformation before 2024 election
Date: 2024-09-04T16:23:35-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-04-us-cracks-down-on-russian-disinformation-before-2024-election

[Source](https://www.bleepingcomputer.com/news/security/us-cracks-down-on-russian-disinformation-before-2024-election/){:target="_blank" rel="noopener"}

> The FBI seized 32 web domains used by the Doppelgänger Russian-linked influence operation network in a disinformation campaign targeting the American public ahead of this year's presidential election. [...]
