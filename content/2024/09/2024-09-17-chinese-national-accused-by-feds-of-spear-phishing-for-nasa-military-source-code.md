Title: Chinese national accused by Feds of spear-phishing for NASA, military source code
Date: 2024-09-17T02:26:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-17-chinese-national-accused-by-feds-of-spear-phishing-for-nasa-military-source-code

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/17/chinese_national_nasa_phishing_indictment/){:target="_blank" rel="noopener"}

> May have reeled in blueprints related to weapons development A Chinese national has been accused of conducting a years-long spear-phishing campaign that aimed to steal source code from the US Army and NASA, plus other highly sensitive software used in aerospace engineering and military applications.... [...]
