Title: So how's Microsoft's Secure Future Initiative going?
Date: 2024-09-23T15:00:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-23-so-hows-microsofts-secure-future-initiative-going

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/23/microsoft_secure_future_initiative/){:target="_blank" rel="noopener"}

> 34,000 engineers pledged to the cause, but no word on exec pay Microsoft took a victory lap today, touting the 34,000 full-time engineers it has dedicated to its Secure Future Initiative (SFI) since it launched almost a year ago and making public its first progress report on efforts to improve security in its products and services.... [...]
