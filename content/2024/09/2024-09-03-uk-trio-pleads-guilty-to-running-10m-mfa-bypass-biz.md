Title: UK trio pleads guilty to running $10M MFA bypass biz
Date: 2024-09-03T21:30:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-09-03-uk-trio-pleads-guilty-to-running-10m-mfa-bypass-biz

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/03/uk_trio_pleads_guilty_mfa_bypass/){:target="_blank" rel="noopener"}

> Crew bragged they could help crooks raid victims' bank accounts Updated A trio of men have pleaded guilty to running a multifactor authentication (MFA) bypass ring in the UK, which authorities estimate has raked in millions in less than two years.... [...]
