Title: Google Chrome gets a mind of its own for some security fixes
Date: 2024-09-12T16:00:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-09-12-google-chrome-gets-a-mind-of-its-own-for-some-security-fixes

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/12/google_chrome_safety_check/){:target="_blank" rel="noopener"}

> Browser becomes more proactive about trimming unneeded permissions and deceptive notifications Google has enhanced Chrome's Safety Check so that it can make some security decisions on the user's behalf.... [...]
