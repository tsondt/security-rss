Title: Google Password Manager now automatically syncs your passkeys
Date: 2024-09-19T12:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google;Security
Slug: 2024-09-19-google-password-manager-now-automatically-syncs-your-passkeys

[Source](https://www.bleepingcomputer.com/news/google/google-password-manager-now-automatically-syncs-your-passkeys/){:target="_blank" rel="noopener"}

> Google announced that starting today, passkeys added to Google Password Manager will automatically sync between Windows, macOS, Linux, Android, and ChromeOS devices for logged-in users. [...]
