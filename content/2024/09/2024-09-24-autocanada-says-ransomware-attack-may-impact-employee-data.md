Title: AutoCanada says ransomware attack "may" impact employee data
Date: 2024-09-24T17:34:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-24-autocanada-says-ransomware-attack-may-impact-employee-data

[Source](https://www.bleepingcomputer.com/news/security/autocanada-says-ransomware-attack-may-impact-employee-data/){:target="_blank" rel="noopener"}

> AutoCanada is warning that employee data may have been exposed in an August cyberattack claimed by the Hunters International ransomware gang. [...]
