Title: Clearview AI fined €30.5 million for unlawful data collection
Date: 2024-09-03T13:12:53-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;Security
Slug: 2024-09-03-clearview-ai-fined-305-million-for-unlawful-data-collection

[Source](https://www.bleepingcomputer.com/news/legal/clearview-ai-fined-305-million-by-dutch-dpa-for-unlawful-data-collection/){:target="_blank" rel="noopener"}

> The Dutch Data Protection Authority (Dutch DPA) has imposed a fine of €30.5 million ($33.7 million) on Clearview AI for unlawful data collection using facial recognition, including photos of Dutch citizens. [...]
