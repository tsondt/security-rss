Title: Friday Squid Blogging: Squid as a Legislative Negotiating Tactic
Date: 2024-09-13T21:00:24+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-09-13-friday-squid-blogging-squid-as-a-legislative-negotiating-tactic

[Source](https://www.schneier.com/blog/archives/2024/09/friday-squid-blogging-squid-as-a-legislative-negotiating-tactic.html){:target="_blank" rel="noopener"}

> This is an odd story of serving squid during legislative negotiations in the Philippines. [...]
