Title: T-Mobile US to cough up $31.5M after that long string of security SNAFUs
Date: 2024-09-30T21:59:17+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-30-t-mobile-us-to-cough-up-315m-after-that-long-string-of-security-snafus

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/30/tmobile_data_breaches_settlement/){:target="_blank" rel="noopener"}

> At least seven intrusions in five years? Yeah, those promises of improvement more than 'long overdue' T-Mobile US has agreed to fork out $31.5 million to improve its cybersecurity and pay a fine after a string of network intrusions affected millions of customers between 2021 and 2023.... [...]
