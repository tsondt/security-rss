Title: Revival Hijack supply-chain attack threatens 22,000 PyPI packages
Date: 2024-09-04T09:43:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-04-revival-hijack-supply-chain-attack-threatens-22000-pypi-packages

[Source](https://www.bleepingcomputer.com/news/security/revival-hijack-supply-chain-attack-threatens-22-000-pypi-packages/){:target="_blank" rel="noopener"}

> Threat actors are utilizing an attack called "Revival Hijack," where they register new PyPi projects using the names of previously deleted packages to conduct supply chain attacks. [...]
