Title: Microsoft: Vanilla Tempest hackers hit healthcare with INC ransomware
Date: 2024-09-18T15:02:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Healthcare;Security
Slug: 2024-09-18-microsoft-vanilla-tempest-hackers-hit-healthcare-with-inc-ransomware

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-vanilla-tempest-hackers-hit-healthcare-with-inc-ransomware/){:target="_blank" rel="noopener"}

> ​Microsoft says a ransomware affiliate it tracks as Vanilla Tempest now targets U.S. healthcare organizations in INC ransomware attacks. [...]
