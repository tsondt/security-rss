Title: How AWS WAF threat intelligence features help protect the player experience for betting and gaming customers
Date: 2024-09-24T16:50:11+00:00
Author: Harith Gaddamanugu
Category: AWS Security
Tags: Advanced (300);AWS WAF;Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2024-09-24-how-aws-waf-threat-intelligence-features-help-protect-the-player-experience-for-betting-and-gaming-customers

[Source](https://aws.amazon.com/blogs/security/how-aws-waf-threat-intelligence-features-help-protect-the-player-experience-for-betting-and-gaming-customers/){:target="_blank" rel="noopener"}

> The betting and gaming industry has grown into a data-rich landscape that presents an enticing target for sophisticated bots. The sensitive personally identifiable information (PII) that is collected and the financial data involved in betting and in-game economies is especially valuable. Microtransactions and in-game purchases are frequently targeted, making them an ideal case for safeguarding [...]
