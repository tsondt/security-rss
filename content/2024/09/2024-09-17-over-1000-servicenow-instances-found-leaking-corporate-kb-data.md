Title: Over 1,000 ServiceNow instances found leaking corporate KB data
Date: 2024-09-17T05:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-17-over-1000-servicenow-instances-found-leaking-corporate-kb-data

[Source](https://www.bleepingcomputer.com/news/security/over-1-000-servicenow-instances-found-leaking-corporate-kb-data/){:target="_blank" rel="noopener"}

> Over 1,000 misconfigured ServiceNow enterprise instances were found exposing Knowledge Base (KB) articles that contained sensitive corporate information to external users and potential threat actors. [...]
