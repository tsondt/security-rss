Title: JPCERT shares Windows Event Log tips to detect ransomware attacks
Date: 2024-09-30T15:22:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-30-jpcert-shares-windows-event-log-tips-to-detect-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/jpcert-shares-windows-event-log-tips-to-detect-ransomware-attacks/){:target="_blank" rel="noopener"}

> Japan's Computer Emergency Response Center (JPCERT/CC) has shared tips on detecting different ransomware gang's attacks based on entries in Windows Event Logs, providing timely detection of ongoing attacks before they spread too far into a network. [...]
