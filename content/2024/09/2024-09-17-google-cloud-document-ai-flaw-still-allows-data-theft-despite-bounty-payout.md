Title: Google Cloud Document AI flaw (still) allows data theft despite bounty payout
Date: 2024-09-17T20:15:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-17-google-cloud-document-ai-flaw-still-allows-data-theft-despite-bounty-payout

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/17/google_cloud_document_ai_flaw/){:target="_blank" rel="noopener"}

> Chocolate Factory downgrades risk, citing the need for attacker access Overly permissive settings in Google Cloud's Document AI service could be abused by data thieves to break into Cloud Storage buckets and steal sensitive information.... [...]
