Title: Patch now: Critical Nvidia bug allows container escape, complete host takeover
Date: 2024-09-26T21:42:46+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-26-patch-now-critical-nvidia-bug-allows-container-escape-complete-host-takeover

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/26/critical_nvidia_bug_container_escape/){:target="_blank" rel="noopener"}

> 33% of cloud environments using the toolkit impacted, we're told A critical bug in Nvidia's widely used Container Toolkit could allow a rogue user or software to escape their containers and ultimately take complete control of the underlying host.... [...]
