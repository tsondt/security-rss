Title: Activating your defender's advantage at mWISE ‘24
Date: 2024-09-18T15:00:00+00:00
Author: Tiffany Wissner
Category: GCP Security
Tags: Security & Identity
Slug: 2024-09-18-activating-your-defenders-advantage-at-mwise-24

[Source](https://cloud.google.com/blog/products/identity-security/activating-your-defenders-advantage-at-mwise-24/){:target="_blank" rel="noopener"}

> To stay ahead of evolving threats, security leaders and practitioners must tap into a vital but underutilized tool to strengthen their defenses: collaboration. The power of communication and knowledge-sharing among peers can help defenders seize the advantage when fighting threat actors who repeat the same tactics, techniques, and procedures (TTPs) to target multiple industries across multiple regions. Security experts from around the world are convening starting today at mWISE Conference 2024, in Denver, Colo., to collaborate on the latest solutions, compare experiences, and bolster their defenses. As part of this shared mission to combat threats, today we are announcing new [...]
