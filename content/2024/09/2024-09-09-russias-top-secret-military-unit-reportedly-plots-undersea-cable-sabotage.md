Title: Russia's top-secret military unit reportedly plots undersea cable 'sabotage'
Date: 2024-09-09T20:15:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-09-russias-top-secret-military-unit-reportedly-plots-undersea-cable-sabotage

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/09/russia_readies_submarine_cable_sabotage/){:target="_blank" rel="noopener"}

> US alarmed by heightened Kremlin naval activity worldwide Russia's naval activity near undersea cables is reportedly drawing the scrutiny of US officials, further sparking concerns that the Kremlin may be plotting to "sabotage" underwater infrastructure via a secretive, dedicated military unit called the General Staff Main Directorate for Deep Sea Research (GUGI).... [...]
