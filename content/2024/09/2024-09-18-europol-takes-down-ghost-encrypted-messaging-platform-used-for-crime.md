Title: Europol takes down "Ghost" encrypted messaging platform used for crime
Date: 2024-09-18T17:06:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government
Slug: 2024-09-18-europol-takes-down-ghost-encrypted-messaging-platform-used-for-crime

[Source](https://www.bleepingcomputer.com/news/security/europol-takes-down-ghost-encrypted-messaging-platform-used-for-crime/){:target="_blank" rel="noopener"}

> Europol and law enforcement from nine countries successfully dismantled an encrypted communications platform called "Ghost," which was used by organized crime such as drug trafficking and money laundering. [...]
