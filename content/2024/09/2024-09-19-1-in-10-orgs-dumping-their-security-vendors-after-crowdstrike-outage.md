Title: 1 in 10 orgs dumping their security vendors after CrowdStrike outage
Date: 2024-09-19T16:13:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-19-1-in-10-orgs-dumping-their-security-vendors-after-crowdstrike-outage

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/19/german_crowdstrike_reaction/){:target="_blank" rel="noopener"}

> Many left reeling from July's IT meltdown, but not to worry, it was all unavoidable Germany's Federal Office for Information Security (BSI) says one in ten organizations in the country affected by CrowdStrike's outage in July are dropping their current vendor's products.... [...]
