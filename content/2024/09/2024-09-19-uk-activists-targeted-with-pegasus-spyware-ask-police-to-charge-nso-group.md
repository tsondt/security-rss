Title: UK activists targeted with Pegasus spyware ask police to charge NSO Group
Date: 2024-09-19T12:16:44+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-19-uk-activists-targeted-with-pegasus-spyware-ask-police-to-charge-nso-group

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/19/pegasus_spyware_met_police_complaint/){:target="_blank" rel="noopener"}

> 4 file complaint with London's Met, alleging malware maker helped autocratic states violate their privacy Four UK-based proponents of human rights and critics of Middle Eastern states today filed a report with London's Metropolitan Police they hope will lead to charges against Pegasus peddler NSO Group.... [...]
