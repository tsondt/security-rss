Title: I stole 20GB of data from Capgemini – and now I'm leaking it, says cyber-crook
Date: 2024-09-12T20:48:01+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-12-i-stole-20gb-of-data-from-capgemini-and-now-im-leaking-it-says-cyber-crook

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/12/capgemini_breach_data_dump/){:target="_blank" rel="noopener"}

> Allegedly pilfered database has source code, private keys, staff info, T-Mobile VM logs, more A miscreant claims to have broken into Capgemini and leaked a large amount of sensitive data stolen from the technology services giant – including source code, credentials, and T-Mobile's virtual machine logs.... [...]
