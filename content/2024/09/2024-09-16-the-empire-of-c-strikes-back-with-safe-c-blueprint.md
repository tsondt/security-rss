Title: The empire of C++ strikes back with Safe C++ blueprint
Date: 2024-09-16T20:08:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-09-16-the-empire-of-c-strikes-back-with-safe-c-blueprint

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/16/safe_c_plusplus/){:target="_blank" rel="noopener"}

> You pipsqueaks want memory safety? We'll show you memory safety! We'll borrow that borrow checker After two years of being beaten with the memory-safety stick, the C++ community has published a proposal to help developers write less vulnerable code.... [...]
