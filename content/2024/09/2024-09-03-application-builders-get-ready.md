Title: Application builders get ready
Date: 2024-09-03T08:51:12+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2024-09-03-application-builders-get-ready

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/03/application_builders_get_ready/){:target="_blank" rel="noopener"}

> Head down to Grey Matter ISV Partner Day to learn about the latest Microsoft technologies Sponsored Post This year's Grey Matter ISV Partner Day will bring together Microsoft-focused ISVs, SaaS Providers and application builders from the UK and Ireland to learn about the latest Microsoft technologies from the software company's own experts.... [...]
