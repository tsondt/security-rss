Title: How EA Sports protects their game servers with Cloud Armor
Date: 2024-09-13T16:00:00+00:00
Author: Lihi Shadmi
Category: GCP Security
Tags: Customers;Security & Identity
Slug: 2024-09-13-how-ea-sports-protects-their-game-servers-with-cloud-armor

[Source](https://cloud.google.com/blog/products/identity-security/how-electronic-arts-protects-their-game-servers-with-cloud-armor/){:target="_blank" rel="noopener"}

> Electronic Arts (EA) is a global leader in digital interactive entertainment, known for its cutting-edge games, innovative services, and powerful technologies. So when EA Sports FC, a leading brand in the gaming industry, needed to choose a cloud provider to host its gaming infrastructure, they selected Google Cloud Armor to protect its game servers and enhance its DDoS resiliency. Distributed denial-of-service (DDoS) attacks can have a devastating impact on gaming companies. They can disrupt gameplay, prevent players from accessing games, and even cause damage to game servers. This can lead to lost revenue, customer dissatisfaction, and tarnish the company's reputation. [...]
