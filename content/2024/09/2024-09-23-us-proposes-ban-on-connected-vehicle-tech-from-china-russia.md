Title: US proposes ban on connected vehicle tech from China, Russia
Date: 2024-09-23T19:05:50-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-23-us-proposes-ban-on-connected-vehicle-tech-from-china-russia

[Source](https://www.bleepingcomputer.com/news/security/us-proposes-ban-on-connected-vehicle-tech-from-china-russia/){:target="_blank" rel="noopener"}

> Today, the Biden administration announced new proposed measures to defend the United States' national security from potential threats linked to connected vehicle technologies originating from China and Russia. [...]
