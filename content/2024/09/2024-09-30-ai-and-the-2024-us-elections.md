Title: AI and the 2024 US Elections
Date: 2024-09-30T11:00:40+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;deepfake;democracy;laws;LLM;regulation
Slug: 2024-09-30-ai-and-the-2024-us-elections

[Source](https://www.schneier.com/blog/archives/2024/09/ai-and-the-2024-us-elections.html){:target="_blank" rel="noopener"}

> For years now, AI has undermined the public’s ability to trust what it sees, hears, and reads. The Republican National Committee released a provocative ad offering an “AI-generated look into the country’s possible future if Joe Biden is re-elected,” showing apocalyptic, machine-made images of ruined cityscapes and chaos at the border. Fake robocalls purporting to be from Biden urged New Hampshire residents not to vote in the 2024 primary election. This summer, the Department of Justice cracked down on a Russian bot farm that was using AI to impersonate Americans on social media, and OpenAI disrupted an Iranian group using [...]
