Title: The fingerpointing starts as cyber incident at London transport body continues
Date: 2024-09-05T10:00:11+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-09-05-the-fingerpointing-starts-as-cyber-incident-at-london-transport-body-continues

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/05/the_fingerpointing_starts_as_the/){:target="_blank" rel="noopener"}

> Network admins take a ride on the Fright Bus The Transport for London (TfL) "cyber incident" is heading into its third day amid claims that a popular appliance might have been the gateway for criminals to gain access to the organization's network.... [...]
