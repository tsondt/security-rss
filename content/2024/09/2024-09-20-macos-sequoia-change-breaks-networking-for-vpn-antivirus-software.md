Title: macOS Sequoia change breaks networking for VPN, antivirus software
Date: 2024-09-20T11:45:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Apple;Security
Slug: 2024-09-20-macos-sequoia-change-breaks-networking-for-vpn-antivirus-software

[Source](https://www.bleepingcomputer.com/news/apple/macos-sequoia-change-breaks-networking-for-vpn-antivirus-software/){:target="_blank" rel="noopener"}

> Users of macOS 15 'Sequoia' are reporting network connection errors when using certain endpoint detection and response (EDR) or virtual private network (VPN) solutions, and web browsers. [...]
