Title: Cloud CISO Perspectives: How CISOs can work with cloud providers to improve incident response
Date: 2024-09-27T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2024-09-27-cloud-ciso-perspectives-how-cisos-can-work-with-cloud-providers-to-improve-incident-response

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-how-cisos-can-work-with-cloud-providers-to-improve-incident-response/){:target="_blank" rel="noopener"}

> Welcome to the second Cloud CISO Perspectives for September 2024. Today, Google Cloud’s Vinod D’Souza and Chris Cornillie examine the vital role that CISOs play in working with cloud providers to improve their organization’s incident preparedness. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI Security & CISO, Google Cloud aside_block <ListValue: [StructValue([('title', 'Get vital CISO Insights with Google Cloud'), ('body', <wagtail.rich_text.RichText object at 0x3e89f0b75610>), ('btn_text', 'Visit the [...]
