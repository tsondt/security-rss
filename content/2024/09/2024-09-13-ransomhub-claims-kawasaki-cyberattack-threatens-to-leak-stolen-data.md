Title: RansomHub claims Kawasaki cyberattack, threatens to leak stolen data
Date: 2024-09-13T11:26:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-13-ransomhub-claims-kawasaki-cyberattack-threatens-to-leak-stolen-data

[Source](https://www.bleepingcomputer.com/news/security/ransomhub-claims-kawasaki-cyberattack-threatens-to-leak-stolen-data/){:target="_blank" rel="noopener"}

> Kawasaki Motors Europe has announced that it's recovering from a cyberattack that caused service disruptions as the RansomHub ransomware gang threatens to leak stolen data. [...]
