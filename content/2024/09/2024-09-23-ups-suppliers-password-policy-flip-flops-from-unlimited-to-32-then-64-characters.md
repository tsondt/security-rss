Title: UPS supplier's password policy flip-flops from unlimited, to 32, then 64 characters
Date: 2024-09-23T12:01:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-23-ups-suppliers-password-policy-flip-flops-from-unlimited-to-32-then-64-characters

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/23/cyberpower_password_changes/){:target="_blank" rel="noopener"}

> That 'third party' person sure is responsible for a lot of IT blunders, eh? A major IT hardware manufacturer is correcting a recent security update after customers complained of a password character limit being introduced when there previously wasn't one.... [...]
