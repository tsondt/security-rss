Title: Musician charged with $10M streaming royalties fraud using AI and bots
Date: 2024-09-05T12:49:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Artificial Intelligence
Slug: 2024-09-05-musician-charged-with-10m-streaming-royalties-fraud-using-ai-and-bots

[Source](https://www.bleepingcomputer.com/news/security/musician-charged-with-10m-streaming-royalties-fraud-using-ai-and-bots/){:target="_blank" rel="noopener"}

> North Carolina musician Michael Smith was indicted for collecting over $10 million in royalty payments from Spotify, Amazon Music, Apple Music, and YouTube Music using AI-generated songs streamed by thousands of bots in a massive streaming fraud scheme. [...]
