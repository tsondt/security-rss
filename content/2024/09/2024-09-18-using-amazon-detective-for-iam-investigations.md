Title: Using Amazon Detective for IAM investigations
Date: 2024-09-18T16:06:31+00:00
Author: Ahmed Adekunle
Category: AWS Security
Tags: Amazon Detective;Amazon GuardDuty;AWS Identity and Access Management (IAM);Intermediate (200);Security, Identity, & Compliance;Technical How-to;AWS IAM;IAM;Security Blog
Slug: 2024-09-18-using-amazon-detective-for-iam-investigations

[Source](https://aws.amazon.com/blogs/security/using-amazon-detective-for-iam-investigations/){:target="_blank" rel="noopener"}

> Uncovering AWS Identity and Access Management (IAM) users and roles potentially involved in a security event can be a complex task, requiring security analysts to gather and analyze data from various sources, and determine the full scope of affected resources. Amazon Detective includes Detective Investigation, a feature that you can use to investigate IAM users [...]
