Title: CrowdStrike apologizes to Congress for 'perfect storm' that caused global IT outage
Date: 2024-09-25T01:23:43+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-25-crowdstrike-apologizes-to-congress-for-perfect-storm-that-caused-global-it-outage

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/25/crowdstrike_to_congress_perfect_storm/){:target="_blank" rel="noopener"}

> Argues worse could happen if it loses kernel access CrowdStrike is "deeply sorry" for the "perfect storm of issues" that saw its faulty software update crash millions of Windows machines, leading to the grounding of thousands of planes, passengers stranded at airports, the cancellation of surgeries, and disruption to emergency services hotlines among many more inconveniences.... [...]
