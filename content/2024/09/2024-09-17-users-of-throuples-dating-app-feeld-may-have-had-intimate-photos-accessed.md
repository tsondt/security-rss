Title: Users of ‘throuples’ dating app Feeld may have had intimate photos accessed
Date: 2024-09-17T11:18:38+00:00
Author: Rob Davies
Category: The Guardian
Tags: Technology startups;Technology sector;Business;Technology;UK news;Information commissioner;Data and computer security;Dating
Slug: 2024-09-17-users-of-throuples-dating-app-feeld-may-have-had-intimate-photos-accessed

[Source](https://www.theguardian.com/business/2024/sep/17/dating-app-feeld-personal-data-cybersecurity){:target="_blank" rel="noopener"}

> Alternative relationships site says it has resolved concerns about data security that tech firm claims to have uncovered Business live – latest updates Users of Feeld, a dating app aimed at alternative relationships, could have had sensitive data including messages, private photos and details of their sexuality accessed or even edited, it has emerged, after cybersecurity experts exposed a string of security “vulnerabilities”. Feeld, registered in the UK, reported soaring revenues and profits this month, thanks to millions of downloads from non-monogamous, queer and kinky users across the world. Continue reading... [...]
