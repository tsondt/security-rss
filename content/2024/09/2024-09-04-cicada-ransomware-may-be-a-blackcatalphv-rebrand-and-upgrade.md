Title: Cicada ransomware may be a BlackCat/ALPHV rebrand and upgrade
Date: 2024-09-04T14:29:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-04-cicada-ransomware-may-be-a-blackcatalphv-rebrand-and-upgrade

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/04/cicada_ransomware_blackcat_links/){:target="_blank" rel="noopener"}

> Researchers find many similarities, and nasty new customizations such as embedded compromised user credentials The Cicada3301 ransomware, which has claimed at least 20 victims since it was spotted in June, shares "striking similarities" with the notorious BlackCat ransomware, according to security researchers at Israeli outfit endpoint security outfit Morphisec.... [...]
