Title: Fortinet admits miscreant got hold of customer data in the cloud
Date: 2024-09-13T00:58:11+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-13-fortinet-admits-miscreant-got-hold-of-customer-data-in-the-cloud

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/13/fortinet_data_loss/){:target="_blank" rel="noopener"}

> That would explain this 440GB leak, then Fortinet has admitted that bad actors accessed cloud-hosted data about its customers, but insisted it was a "limited number" of files. The question is: how limited is "limited"?... [...]
