Title: MoneyGram confirms a cyberattack is behind dayslong outage
Date: 2024-09-24T08:48:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-24-moneygram-confirms-a-cyberattack-is-behind-dayslong-outage

[Source](https://www.bleepingcomputer.com/news/security/moneygram-confirms-a-cyberattack-is-behind-dayslong-outage/){:target="_blank" rel="noopener"}

> Money transfer giant MoneyGram has confirmed it suffered a cyberattack after dealing with system outages and customer complaints about lack of service since Friday. [...]
