Title: Iranian hackers charged for ‘hack-and-leak’ plot to influence election
Date: 2024-09-27T15:47:58-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-27-iranian-hackers-charged-for-hack-and-leak-plot-to-influence-election

[Source](https://www.bleepingcomputer.com/news/security/iranian-hackers-charged-for-hack-and-leak-plot-to-influence-election/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice unsealed an indictment charging three Iranian hackers with a "hack-and-leak" campaign that aimed to influence the 2024 U.S. presidential election. [...]
