Title: U.S. Indicts 2 Top Russian Hackers, Sanctions Cryptex
Date: 2024-09-26T14:54:07+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;The Coming Storm;Web Fraud 2.0;alex holden;Arpa Plus;briansclub;Chainanlysis;Cryptex;CS Proxy Solutions CY;Dark Angels;Financial Crimes Enforcement Network;Hold Security;Home Depot breach;Joker's stash;Liberty Reserve;Mazafaka;Novaya Gazeta;Orbest Investments LP;Organized Crime and Corruption Project;Perfect Money;PinPays;pm2btc;Progate Solutions;Rich Sanders;RM Everton Ltd;Sergey Sergeevich Ivanov;Sergey Sergeevich Omelnitskii;Taleon;target breach;The Laundromat;Timur Kamilevich Shakhmametov;U.S. Department of Justice;UAPS;Universal Anonymous Payment System;unlimited cashouts;v1pee;Vega
Slug: 2024-09-26-us-indicts-2-top-russian-hackers-sanctions-cryptex

[Source](https://krebsonsecurity.com/2024/09/u-s-indicts-2-top-russian-hackers-sanctions-cryptex/){:target="_blank" rel="noopener"}

> The United States today unveiled sanctions and indictments against the alleged proprietor of Joker’s Stash, a now-defunct cybercrime store that peddled tens of millions of payment cards stolen in some of the largest data breaches of the past decade. The government also indicted and sanctioned a top Russian cybercriminal known as Taleon, whose cryptocurrency exchange Cryptex has evolved into one of Russia’s most active money laundering networks. A 2016 screen shot of the Joker’s Stash homepage. The links have been redacted. The U.S. Department of Justice (DOJ) today unsealed an indictment against a 38-year-old man from Novosibirsk, Russia for allegedly [...]
