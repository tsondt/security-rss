Title: New Chrome Zero-Day
Date: 2024-09-10T11:04:29+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Chrome;cryptocurrency;Microsoft;North Korea;zero-day
Slug: 2024-09-10-new-chrome-zero-day

[Source](https://www.schneier.com/blog/archives/2024/09/new-chrome-zero-day.html){:target="_blank" rel="noopener"}

> According to Microsoft researchers, North Korean hackers have been using a Chrome zero-day exploit to steal cryptocurrency. [...]
