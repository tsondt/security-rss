Title: India to train 5,000 'Cyber Commandos'
Date: 2024-09-11T04:32:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-09-11-india-to-train-5000-cyber-commandos

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/11/india_cyber_commandos/){:target="_blank" rel="noopener"}

> Minister reckons dedicated cops necessary to protect digital transactions India has announced a plan to train a specialized wing of 5000 "Cyber Commandos" in the next five years, as part of its efforts to address cyber crime.... [...]
