Title: Cisco merch shoppers stung in Magecart attack
Date: 2024-09-06T20:00:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-06-cisco-merch-shoppers-stung-in-magecart-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/06/cisco_merch_adobe_magento_attack/){:target="_blank" rel="noopener"}

> The 'security issue' was caused by a 9.8-rated Magento flaw Adobe patched back in June Bad news for anyone who purchased a Cisco hoodie earlier this month: Suspected Russia-based attackers injected data-stealing JavaScript into the networking giant's online store selling Cisco-branded merch.... [...]
