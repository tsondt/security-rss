Title: Microsoft Defender adds detection of unsecure Wi-Fi networks
Date: 2024-09-30T16:47:38-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-09-30-microsoft-defender-adds-detection-of-unsecure-wi-fi-networks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-defender-now-automatically-detects-unsecure-wi-fi-networks/){:target="_blank" rel="noopener"}

> Microsoft Defender now automatically detects and notifies users with a Microsoft 365 Personal or Family subscription when they're connected to unsecured Wi-Fi networks. [...]
