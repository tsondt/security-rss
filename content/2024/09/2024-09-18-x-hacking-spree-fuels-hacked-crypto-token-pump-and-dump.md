Title: X hacking spree fuels "$HACKED" crypto token pump-and-dump
Date: 2024-09-18T15:07:27-04:00
Author: Mayank Parmar
Category: BleepingComputer
Tags: Security;CryptoCurrency;Software
Slug: 2024-09-18-x-hacking-spree-fuels-hacked-crypto-token-pump-and-dump

[Source](https://www.bleepingcomputer.com/news/security/x-hacking-spree-fuels-hacked-crypto-token-pump-and-dump/){:target="_blank" rel="noopener"}

> An X account hacking spree has fueled a successful pump-and-dump scheme for the $HACKED Solana token, with people rushing to buy the coin. [...]
