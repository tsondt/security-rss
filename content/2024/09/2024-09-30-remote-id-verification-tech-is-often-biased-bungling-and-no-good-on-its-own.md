Title: Remote ID verification tech is often biased, bungling, and no good on its own
Date: 2024-09-30T12:40:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-09-30-remote-id-verification-tech-is-often-biased-bungling-and-no-good-on-its-own

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/30/remote_identity_verification_biased/){:target="_blank" rel="noopener"}

> Only 2 out of 5 tested products were equitable across demographics A study by the US General Services Administration (GSA) has revealed that five remote identity verification (RiDV) technologies are unreliable, inconsistent, and marred by bias across different demographic groups.... [...]
