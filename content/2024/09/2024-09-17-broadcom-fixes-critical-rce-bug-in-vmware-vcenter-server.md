Title: Broadcom fixes critical RCE bug in VMware vCenter Server
Date: 2024-09-17T15:57:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-17-broadcom-fixes-critical-rce-bug-in-vmware-vcenter-server

[Source](https://www.bleepingcomputer.com/news/security/broadcom-fixes-critical-rce-bug-in-vmware-vcenter-server/){:target="_blank" rel="noopener"}

> Broadcom has fixed a critical VMware vCenter Server vulnerability that attackers can exploit to gain remote code execution on unpatched servers via a network packet. [...]
