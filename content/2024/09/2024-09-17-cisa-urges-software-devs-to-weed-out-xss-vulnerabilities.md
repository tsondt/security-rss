Title: CISA urges software devs to weed out XSS vulnerabilities
Date: 2024-09-17T12:39:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-17-cisa-urges-software-devs-to-weed-out-xss-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/cisa-urges-software-devs-to-weed-out-xss-vulnerabilities/){:target="_blank" rel="noopener"}

> CISA and the FBI urged tech companies to review their software and eliminate cross-site scripting (XSS) vulnerabilities before shipping. [...]
