Title: Cloud threats have execs the most freaked out because they're not prepared
Date: 2024-09-30T11:30:17+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-09-30-cloud-threats-have-execs-the-most-freaked-out-because-theyre-not-prepared

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/30/pwc_security_survey/){:target="_blank" rel="noopener"}

> Ransomware? More like 'we don't care' for everyone but CISOs Efficiency and scalability are key benefits of enterprise cloud computing, but they come at a cost. Security threats specific to cloud environments are the leading cause of concern among top executives and they're also the ones organizations are least prepared to address.... [...]
