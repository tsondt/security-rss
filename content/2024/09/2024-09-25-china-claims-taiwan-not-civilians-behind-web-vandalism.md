Title: China claims Taiwan, not civilians, behind web vandalism
Date: 2024-09-25T01:25:34+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-09-25-china-claims-taiwan-not-civilians-behind-web-vandalism

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/25/china_anonymous_64_taiwan_accusations/){:target="_blank" rel="noopener"}

> Taipei laughs it off – and so does Beijing, which says political slurs hit sites nobody reads anyway Taiwan has dismissed Chinese allegations that its military sponsored a recent wave of anti-Beijing cyber attacks.... [...]
