Title: Russian military hackers linked to critical infrastructure attacks
Date: 2024-09-05T13:59:31-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-05-russian-military-hackers-linked-to-critical-infrastructure-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-and-allies-link-russian-military-hackers-behind-critical-infrastructure-attacks-to-gru-unit-29155/){:target="_blank" rel="noopener"}

> The United States and its allies have linked a group of Russian hackers (tracked as Cadet Blizzard and Ember Bear) behind global critical infrastructure attacks to Unit 29155 of Russia's Main Directorate of the General Staff of the Armed Forces (also known as GRU). [...]
