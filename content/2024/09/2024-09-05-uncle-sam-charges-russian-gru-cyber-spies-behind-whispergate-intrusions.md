Title: Uncle Sam charges Russian GRU cyber-spies behind 'WhisperGate intrusions'
Date: 2024-09-05T19:44:28+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-05-uncle-sam-charges-russian-gru-cyber-spies-behind-whispergate-intrusions

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/05/uncle_sam_charges_russian_gru/){:target="_blank" rel="noopener"}

> Feds post $10M bounty for each of the six's whereabouts The US today charged five Russian military intelligence officers and one civilian for their alleged involvement with the data-wiping WhisperGate campaign conducted against Ukraine in January 2022 before the ground invasion began.... [...]
