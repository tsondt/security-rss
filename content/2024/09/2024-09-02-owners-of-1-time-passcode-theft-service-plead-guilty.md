Title: Owners of 1-Time Passcode Theft Service Plead Guilty
Date: 2024-09-02T16:46:35+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Web Fraud 2.0;Aza Siddeeque;Callum Picari;National Crime Agency;otp agency;RaidForums;SMSRanger;Vijayasidhurshan Vijayanathan
Slug: 2024-09-02-owners-of-1-time-passcode-theft-service-plead-guilty

[Source](https://krebsonsecurity.com/2024/09/owners-of-1-time-passcode-theft-service-plead-guilty/){:target="_blank" rel="noopener"}

> Three men in the United Kingdom have pleaded guilty to operating otp[.]agency, a once popular online service that helped attackers intercept the one-time passcodes (OTPs) that many websites require as a second authentication factor in addition to passwords. Launched in November 2019, OTP Agency was a service for intercepting one-time passcodes needed to log in to various websites. Scammers who had already stolen someone’s bank account credentials could enter the target’s phone number and name, and the service would initiate an automated phone call to the target that warned them about unauthorized activity on their account. The call would prompt [...]
