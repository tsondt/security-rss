Title: Lebanon now hit with deadly walkie-talkie blasts as Israel declares ‘new phase’ of war
Date: 2024-09-18T17:54:23+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-09-18-lebanon-now-hit-with-deadly-walkie-talkie-blasts-as-israel-declares-new-phase-of-war

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/18/lebanon_walkie_talkie_explosion_isreal_war/){:target="_blank" rel="noopener"}

> Second wave of exploding gear kills at least 14 today First it was pagers, now Lebanon is being rocked by Hezbollah's walkie-talkies detonating across the country, leaving more than a dozen dead.... [...]
