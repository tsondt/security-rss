Title: Telegram will now hand over IP addresses, phone numbers of suspects to cops
Date: 2024-09-23T22:10:14+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-09-23-telegram-will-now-hand-over-ip-addresses-phone-numbers-of-suspects-to-cops

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/23/telegram_tcs_suspect_info/){:target="_blank" rel="noopener"}

> Maybe a spell in a French cell changed Durov's mind In a volte-face, Telegram CEO Pavel Durov announced that the made-in-Russia messaging platform will become a lot less cozy for criminals.... [...]
