Title: Halliburton confirms data stolen in recent cyberattack
Date: 2024-09-03T08:57:21-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-09-03-halliburton-confirms-data-stolen-in-recent-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/halliburton-confirms-data-stolen-in-recent-cyberattack/){:target="_blank" rel="noopener"}

> Oil and gas giant Halliburton has confirmed in a filing today to the Securities and Exchange Commission (SEC) that data was stolen in the recent attack linked to the RansomHub ransomware gang. [...]
