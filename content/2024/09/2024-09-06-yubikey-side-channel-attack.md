Title: YubiKey Side-Channel Attack
Date: 2024-09-06T15:16:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;cloning;security analysis;security tokens;side-channel attacks
Slug: 2024-09-06-yubikey-side-channel-attack

[Source](https://www.schneier.com/blog/archives/2024/09/yubikey-side-channel-attack.html){:target="_blank" rel="noopener"}

> There is a side-channel attack against YubiKey access tokens that allows someone to clone a device. It’s a complicated attack, requiring the victim’s username and password, and physical access to their YubiKey—as well as some technical expertise and equipment. Still, nice piece of security analysis. [...]
