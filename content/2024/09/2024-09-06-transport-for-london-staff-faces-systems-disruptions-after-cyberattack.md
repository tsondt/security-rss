Title: Transport for London staff faces systems disruptions after cyberattack
Date: 2024-09-06T15:49:08-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-09-06-transport-for-london-staff-faces-systems-disruptions-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/transport-for-london-staff-faces-systems-disruptions-after-cyberattack/){:target="_blank" rel="noopener"}

> ​Transport for London, the city's public transportation agency, revealed today that its staff has limited access to systems and email due to measures implemented in response to a Sunday cyberattack. [...]
