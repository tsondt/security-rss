Title: 1.3 million Android-based TV boxes backdoored; researchers still don’t know how
Date: 2024-09-13T20:20:23+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Google;Security;Android Open Source Project;AOSP;backdoors;streaming devices
Slug: 2024-09-13-13-million-android-based-tv-boxes-backdoored-researchers-still-dont-know-how

[Source](https://arstechnica.com/?p=2049773){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Researchers still don’t know the cause of a recently discovered malware infection affecting almost 1.3 million streaming devices running an open source version of Android in almost 200 countries. Security firm Doctor Web reported Thursday that malware named Android.Vo1d has backdoored the Android-based boxes by putting malicious components in their system storage area, where they can be updated with additional malware at any time by command-and-control servers. Google representatives said the infected devices are running operating systems based on the Android Open Source Project, a version overseen by Google but distinct from Android TV, a proprietary [...]
