Title: Cambodian senator sanctioned by US over alleged forced labor cyber-scam camps
Date: 2024-09-13T05:29:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-09-13-cambodian-senator-sanctioned-by-us-over-alleged-forced-labor-cyber-scam-camps

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/13/cambodian_senator_sanctioned_for_cyberscams/){:target="_blank" rel="noopener"}

> Do not go on holiday to the O Smach Resort The US Department of the Treasury’s Office of Foreign Assets Control issued sanctions on Thursday against Cambodian entrepreneur and senator Ly Yong Phat, for his "role in serious human rights abuse related to the treatment of trafficked workers subjected to forced labor in online scam centers."... [...]
