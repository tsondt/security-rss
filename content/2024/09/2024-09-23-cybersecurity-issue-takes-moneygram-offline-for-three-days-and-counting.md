Title: 'Cybersecurity issue' takes MoneyGram offline for three days – and counting
Date: 2024-09-23T21:32:50+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-09-23-cybersecurity-issue-takes-moneygram-offline-for-three-days-and-counting

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/23/moneygram_cybersecurity_issue/){:target="_blank" rel="noopener"}

> Still no ‘R’ word, but smells like ransomware from here A "cybersecurity issue" has shut down MoneyGram's systems and payment services since Friday, and the fintech leader has yet to update customers as to when it expects to have its global money transfer services back up and running.... [...]
