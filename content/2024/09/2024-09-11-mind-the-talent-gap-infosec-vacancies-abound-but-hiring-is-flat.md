Title: Mind the talent gap: Infosec vacancies abound, but hiring is flat
Date: 2024-09-11T10:10:43+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-09-11-mind-the-talent-gap-infosec-vacancies-abound-but-hiring-is-flat

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/11/mind_the_talent_gap_infosec/){:target="_blank" rel="noopener"}

> ISC2 argues security training needs to steer toward what hiring managers want The shortfall between the number of working security professionals and the number of security job openings has reached 4.8 million – a new high, according to cyber security non-profit ISC2.... [...]
