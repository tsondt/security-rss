Title: US proposes ban on Chinese, Russian connected car tech over security fears
Date: 2024-09-23T18:25:25+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-09-23-us-proposes-ban-on-chinese-russian-connected-car-tech-over-security-fears

[Source](https://go.theregister.com/feed/www.theregister.com/2024/09/23/us_ban_china_russia_car_tech/){:target="_blank" rel="noopener"}

> No room for your spy mobiles on our streets The US Commerce Department has decided not to wait for the inevitable, and today announced plans that would ban connected vehicle technology - and vehicles using it - from Chinese and Russian sources.... [...]
