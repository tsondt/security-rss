Title: Safer by default: Automate access control with Sensitive Data Protection and conditional IAM
Date: 2024-09-12T16:00:00+00:00
Author: Jordanna Chord
Category: GCP Security
Tags: Security & Identity
Slug: 2024-09-12-safer-by-default-automate-access-control-with-sensitive-data-protection-and-conditional-iam

[Source](https://cloud.google.com/blog/products/identity-security/automate-access-control-with-sensitive-data-protection-and-conditional-iam/){:target="_blank" rel="noopener"}

> The first step towards protecting sensitive data begins with knowing where it exists. Continuous data monitoring can help you stay one step ahead of data security risks and set proper access controls to ensure data is used for the right reasons while minimizing unnecessary friction. Google Cloud’s Sensitive Data Protection can automatically discover sensitive data assets and attach tags to your data assets based on sensitivity. Using IAM conditions, you can grant or deny access to data based on the presence or absence of a sensitivity level tag key or tag value. This feature can help you: Automate access control [...]
