Title: How to implement relationship-based access control with Amazon Verified Permissions and Amazon Neptune
Date: 2024-09-30T18:00:17+00:00
Author: Henry Ho
Category: AWS Security
Tags: Amazon Cognito;Amazon Neptune;Amazon Verified Permissions;Customer Solutions;Expert (400);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2024-09-30-how-to-implement-relationship-based-access-control-with-amazon-verified-permissions-and-amazon-neptune

[Source](https://aws.amazon.com/blogs/security/how-to-implement-relationship-based-access-control-with-amazon-verified-permissions-and-amazon-neptune/){:target="_blank" rel="noopener"}

> Externalized authorization for custom applications is a security approach where access control decisions are managed outside of the application logic. Instead of embedding authorization rules within the application’s code, these rules are defined as policies, which are evaluated by a separate system to make an authorization decision. This separation enhances an application’s security posture by [...]
