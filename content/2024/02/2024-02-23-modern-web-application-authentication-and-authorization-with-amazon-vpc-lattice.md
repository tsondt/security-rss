Title: Modern web application authentication and authorization with Amazon VPC Lattice
Date: 2024-02-23T16:47:55+00:00
Author: Nigel Brittain
Category: AWS Security
Tags: Expert (400);Security, Identity, & Compliance;Technical How-to;Amazon VPC lattice;Identity;Security;Security Blog
Slug: 2024-02-23-modern-web-application-authentication-and-authorization-with-amazon-vpc-lattice

[Source](https://aws.amazon.com/blogs/security/modern-web-application-authentication-and-authorization-with-amazon-vpc-lattice/){:target="_blank" rel="noopener"}

> When building API-based web applications in the cloud, there are two main types of communication flow in which identity is an integral consideration: User-to-Service communication: Authenticate and authorize users to communicate with application services and APIs Service-to-Service communication: Authenticate and authorize application services to talk to each other To design an authentication and authorization solution for these [...]
