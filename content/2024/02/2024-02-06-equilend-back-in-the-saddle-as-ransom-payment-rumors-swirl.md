Title: EquiLend back in the saddle as ransom payment rumors swirl
Date: 2024-02-06T15:45:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-06-equilend-back-in-the-saddle-as-ransom-payment-rumors-swirl

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/06/equilend_back_in_action_as/){:target="_blank" rel="noopener"}

> Still no word on how the intruders broke in or the full extent of any possible data compromise Global securities finance tech company EquiLend's systems are now back online after announcing a disruptive ransomware attack nearly two weeks ago.... [...]
