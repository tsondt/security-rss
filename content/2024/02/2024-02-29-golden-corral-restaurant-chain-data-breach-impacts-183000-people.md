Title: Golden Corral restaurant chain data breach impacts 183,000 people
Date: 2024-02-29T17:14:17-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-29-golden-corral-restaurant-chain-data-breach-impacts-183000-people

[Source](https://www.bleepingcomputer.com/news/security/golden-corral-restaurant-chain-data-breach-impacts-183-000-people/){:target="_blank" rel="noopener"}

> The Golden Corral American restaurant chain disclosed a data breach after attackers behind an August cyberattack stole the personal information of over 180,000 people. [...]
