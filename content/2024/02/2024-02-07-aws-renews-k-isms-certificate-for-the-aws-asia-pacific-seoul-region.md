Title: AWS renews K-ISMS certificate for the AWS Asia Pacific (Seoul) Region
Date: 2024-02-07T04:43:41+00:00
Author: Joseph Goh
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Auditing;AWS security;Certification;Compliance;ISMS;K-ISMS;K-ISMS-P;KISA;Korea certification;Korea Internet and Security Agency;Korea-Information Security Management System Certification;Korean Ministry of Science and ICT;MSIT;Security Blog
Slug: 2024-02-07-aws-renews-k-isms-certificate-for-the-aws-asia-pacific-seoul-region

[Source](https://aws.amazon.com/blogs/security/aws-renews-k-isms-certificate-for-the-asia-pacific/){:target="_blank" rel="noopener"}

> We’re excited to announce that Amazon Web Services (AWS) has successfully renewed certification under the Korea Information Security Management System (K-ISMS) standard (effective from December 16, 2023, to December 15, 2026). The certification assessment covered the operation of infrastructure (including compute, storage, networking, databases, and security) in the AWS Asia Pacific (Seoul) Region. AWS was [...]
