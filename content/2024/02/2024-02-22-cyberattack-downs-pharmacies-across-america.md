Title: Cyberattack downs pharmacies across America
Date: 2024-02-22T21:13:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-22-cyberattack-downs-pharmacies-across-america

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/22/change_healthcare_outage/){:target="_blank" rel="noopener"}

> Prescription orders hit after IT supplier Change Healthcare pulls plug on systems Updated IT provider Change Healthcare has confirmed it shut down some of its systems following a cyberattack, disrupting prescription orders and other services at pharmacies across the US.... [...]
