Title: Turns out cops are super interested in subpoenaing suspects' push notifications
Date: 2024-02-29T22:30:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-02-29-turns-out-cops-are-super-interested-in-subpoenaing-suspects-push-notifications

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/29/push_notification_privacy/){:target="_blank" rel="noopener"}

> Those little popups may reveal location, device details, IP address, and more More than 130 petitions seeking access to push notification metadata have been filed in US courts, according to a Washington Post investigation – a finding that underscores the lack of privacy protection available to users of mobile devices.... [...]
