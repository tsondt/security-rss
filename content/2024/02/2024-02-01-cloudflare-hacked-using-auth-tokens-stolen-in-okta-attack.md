Title: Cloudflare hacked using auth tokens stolen in Okta attack
Date: 2024-02-01T15:53:09-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-01-cloudflare-hacked-using-auth-tokens-stolen-in-okta-attack

[Source](https://www.bleepingcomputer.com/news/security/cloudflare-hacked-using-auth-tokens-stolen-in-okta-attack/){:target="_blank" rel="noopener"}

> Cloudflare disclosed today that its internal Atlassian server was breached by a suspected 'nation state attacker' who accessed its Confluence wiki, Jira bug database, and Bitbucket source code management system. [...]
