Title: Arrests in $400M SIM-Swap Tied to Heist at FTX?
Date: 2024-02-01T18:41:37+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Ne'er-Do-Well News;SIM Swapping;0ktapus;ALPHV ransomware;Ars Technica;BlackCat ransomware;Carter Rohn;CISA;Elliptic;Emily Hernandez;fbi;FTX;Kroll;Nick Bax;Powell SIM Swapping Crew;R$;Robert Powell;Scattered Spider;SIM swapping;Tom Robinson;Unciphered
Slug: 2024-02-01-arrests-in-400m-sim-swap-tied-to-heist-at-ftx

[Source](https://krebsonsecurity.com/2024/02/arrests-in-400m-sim-swap-tied-to-heist-at-ftx/){:target="_blank" rel="noopener"}

> Three Americans were charged this week with stealing more than $400 million in a November 2022 SIM-swapping attack. The U.S. government did not name the victim organization, but there is every indication that the money was stolen from the now-defunct cryptocurrency exchange FTX, which had just filed for bankruptcy on that same day. A graphic illustrating the flow of more than $400 million in cryptocurrencies stolen from FTX on Nov. 11-12, 2022. Image: Elliptic.co. An indictment unsealed this week and first reported on by Ars Technica alleges that Chicago man Robert Powell, a.k.a. “R,” “R$” and “ElSwapo1,” was the ringleader [...]
