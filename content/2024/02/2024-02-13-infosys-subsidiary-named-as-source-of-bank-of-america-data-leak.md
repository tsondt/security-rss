Title: Infosys subsidiary named as source of Bank of America data leak
Date: 2024-02-13T05:28:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-02-13-infosys-subsidiary-named-as-source-of-bank-of-america-data-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/13/infosys_bank_of_america_leak/){:target="_blank" rel="noopener"}

> Looks like LockBit took a swipe at an outsourced life insurance application Indian tech services giant Infosys has been named as the source of a data leak suffered by the Bank of America.... [...]
