Title: Singapore's monetary authority advises banks to get busy protecting against quantum decryption
Date: 2024-02-21T00:59:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-02-21-singapores-monetary-authority-advises-banks-to-get-busy-protecting-against-quantum-decryption

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/21/mas_warns_quantum_computing/){:target="_blank" rel="noopener"}

> No time like the present, says central bank The Monetary Authority of Singapore (MAS) advised on Monday that financial institutions need to stay agile enough to adopt post-quantum cryptography (PQC) and quantum key distribution (QKD) technology, without significantly impacting systems as part of cyber security measures.... [...]
