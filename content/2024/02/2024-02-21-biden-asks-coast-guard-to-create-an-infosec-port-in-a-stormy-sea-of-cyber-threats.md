Title: Biden asks Coast Guard to create an infosec port in a stormy sea of cyber threats
Date: 2024-02-21T22:10:13+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-02-21-biden-asks-coast-guard-to-create-an-infosec-port-in-a-stormy-sea-of-cyber-threats

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/21/uscg_cybersecurity_powers/){:target="_blank" rel="noopener"}

> Oh hear us when we cry to thee for those in peril on the sea President Biden has empowered the US Coast Guard (USCG) to get a tighter grip on cybersecurity at American ports – including authorizing yet another incident reporting rule.... [...]
