Title: Interpol operation Synergia takes down 1,300 servers used for cybercrime
Date: 2024-02-02T07:56:28-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;Security
Slug: 2024-02-02-interpol-operation-synergia-takes-down-1300-servers-used-for-cybercrime

[Source](https://www.bleepingcomputer.com/news/legal/interpol-operation-synergia-takes-down-1-300-servers-used-for-cybercrime/){:target="_blank" rel="noopener"}

> An international law enforcement operation code-named 'Synergia' has taken down over 1,300 command and control servers used in ransomware, phishing, and malware campaigns. [...]
