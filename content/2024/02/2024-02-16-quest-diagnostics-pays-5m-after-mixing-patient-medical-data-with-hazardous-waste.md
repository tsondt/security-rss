Title: Quest Diagnostics pays $5M after mixing patient medical data with hazardous waste
Date: 2024-02-16T01:20:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-16-quest-diagnostics-pays-5m-after-mixing-patient-medical-data-with-hazardous-waste

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/16/quest_diagnostics_california_settlement/){:target="_blank" rel="noopener"}

> Will cough up less than two days of annual profit in settlement – and California calls this a win Quest Diagnostics has agreed to pay almost $5 million to settle allegations it illegally dumped protected health information – and hazardous waste – at its facilities across California.... [...]
