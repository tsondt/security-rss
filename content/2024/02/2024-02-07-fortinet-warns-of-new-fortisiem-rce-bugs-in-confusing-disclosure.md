Title: Fortinet warns of new FortiSIEM RCE bugs in confusing disclosure
Date: 2024-02-07T19:55:41-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2024-02-07-fortinet-warns-of-new-fortisiem-rce-bugs-in-confusing-disclosure

[Source](https://www.bleepingcomputer.com/news/security/fortinet-warns-of-new-fortisiem-rce-bugs-in-confusing-disclosure/){:target="_blank" rel="noopener"}

> Fortinet is warning of two new unpatched patch bypasses for a critical remote code execution vulnerability in FortiSIEM, Fortinet's SIEM solution. [...]
