Title: NIST updates Cybersecurity Framework after a decade of lessons
Date: 2024-02-27T18:45:14+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-02-27-nist-updates-cybersecurity-framework-after-a-decade-of-lessons

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/27/nist_cybersecurity_framework_2/){:target="_blank" rel="noopener"}

> The original was definitely getting a bit long in the tooth for modern challenges After ten years operating under the original model, and two years working to revise it, the National Institute of Standards and Technology (NIST) has released version 2.0 of its Cybersecurity Framework (CSF).... [...]
