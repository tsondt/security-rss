Title: Fake LastPass lookalike made it into Apple App Store
Date: 2024-02-08T21:59:40+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-02-08-fake-lastpass-lookalike-made-it-into-apple-app-store

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/08/lastpass_lookalike_apple_app_store/){:target="_blank" rel="noopener"}

> No walled garden can keep out every weed, we suppose LastPass says a rogue application impersonating its popular password manager made it past Apple's gatekeepers and was listed in the iOS App Store for unsuspecting folks to download and install.... [...]
