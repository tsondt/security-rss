Title: ALPHV blackmails Canadian pipeline after 'stealing 190GB of vital info'
Date: 2024-02-13T19:20:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-13-alphv-blackmails-canadian-pipeline-after-stealing-190gb-of-vital-info

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/13/alphv_canadian_pipeline/){:target="_blank" rel="noopener"}

> Gang still going after critical infrastructure because it's, you know, critical Updated Canada's Trans-Northern Pipelines has allegedly been infiltrated by the ALPHV/BlackCat ransomware crew, which claims to have stolen 190 GB of data from the oil distributor.... [...]
