Title: Introducing Security Command Center protection for Vertex AI
Date: 2024-02-27T18:00:00+00:00
Author: Vandhana Ramadurai
Category: GCP Security
Tags: Security & Identity
Slug: 2024-02-27-introducing-security-command-center-protection-for-vertex-ai

[Source](https://cloud.google.com/blog/products/identity-security/introducing-security-command-center-protection-for-vertex-ai/){:target="_blank" rel="noopener"}

> As developers use Vertex AI, Google Cloud’s end-to-end AI platform that includes intuitive tooling to build the next generation of AI applications, IT teams are called on to bolster cloud infrastructure security. Aligned with the Secure AI Framework (SAIF) we introduced last year, we want to share approaches and tools customers can use to help secure AI models, products, and technologies. We recommend starting with Google Cloud’s Organization Policy Service. Organization policies define constraints on how cloud resources can be configured, and include Vertex AI-specific policies that keep developers operating within centrally-defined guardrails. Security Command Center Premium, our built-in security [...]
