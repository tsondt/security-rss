Title: Check if you're in Google Chrome's third-party cookie phaseout test
Date: 2024-02-03T14:14:48-05:00
Author: Mayank Parmar
Category: BleepingComputer
Tags: Google;Security
Slug: 2024-02-03-check-if-youre-in-google-chromes-third-party-cookie-phaseout-test

[Source](https://www.bleepingcomputer.com/news/google/check-if-youre-in-google-chromes-third-party-cookie-phaseout-test/){:target="_blank" rel="noopener"}

> Google has started testing the phasing out of third-party cookies on Chrome, affecting about 1% of its users or approximately 30 million people. Learn how to check if you are part of the initial test. [...]
