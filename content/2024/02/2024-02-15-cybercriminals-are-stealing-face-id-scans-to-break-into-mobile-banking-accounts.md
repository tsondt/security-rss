Title: Cybercriminals are stealing Face ID scans to break into mobile banking accounts
Date: 2024-02-15T14:00:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-15-cybercriminals-are-stealing-face-id-scans-to-break-into-mobile-banking-accounts

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/15/cybercriminals_stealing_face_id/){:target="_blank" rel="noopener"}

> Deepfake-enabled attacks against Android and iOS users are netting criminals serious cash Cybercriminals are targeting iOS users with malware that steals Face ID scans to break into and pilfer money from bank accounts – thought to be a world first.... [...]
