Title: LockBit ransomware disrupted by global police operation
Date: 2024-02-19T16:38:48-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-19-lockbit-ransomware-disrupted-by-global-police-operation

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-disrupted-by-global-police-operation/){:target="_blank" rel="noopener"}

> Law enforcement agencies from 11 countries have disrupted the notorious LockBit ransomware operation in a joint operation known as ''Operation Cronos." [...]
