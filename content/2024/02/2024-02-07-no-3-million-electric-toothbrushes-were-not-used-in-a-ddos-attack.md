Title: No, 3 million electric toothbrushes were not used in a DDoS attack
Date: 2024-02-07T12:21:29-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-02-07-no-3-million-electric-toothbrushes-were-not-used-in-a-ddos-attack

[Source](https://www.bleepingcomputer.com/news/security/no-3-million-electric-toothbrushes-were-not-used-in-a-ddos-attack/){:target="_blank" rel="noopener"}

> A widely reported story that 3 million electric toothbrushes were hacked with malware to conduct distributed denial of service (DDoS) attacks is likely a hypothetical scenario instead of an actual attack. [...]
