Title: Criminal IP ASM: A new cybersecurity listing on Microsoft Azure
Date: 2024-02-06T10:02:04-05:00
Author: Sponsored by Criminal IP
Category: BleepingComputer
Tags: Security
Slug: 2024-02-06-criminal-ip-asm-a-new-cybersecurity-listing-on-microsoft-azure

[Source](https://www.bleepingcomputer.com/news/security/criminal-ip-asm-a-new-cybersecurity-listing-on-microsoft-azure/){:target="_blank" rel="noopener"}

> AI SPERA, a leader in Cyber Threat Intelligence (CTI)-based solutions, today announced that Criminal IP ASM (Attack Surface Management) is now available on the Microsoft Azure Marketplace. [...]
