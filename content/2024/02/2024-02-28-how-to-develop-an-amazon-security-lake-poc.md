Title: How to develop an Amazon Security Lake POC
Date: 2024-02-28T14:25:25+00:00
Author: Anna McAbee
Category: AWS Security
Tags: Amazon Security Lake;Intermediate (200);Security, Identity, & Compliance;Technical How-to;AWS Incident Response;AWS security;Cloud security;Incident response;Logging;Monitoring;Security;Security Blog
Slug: 2024-02-28-how-to-develop-an-amazon-security-lake-poc

[Source](https://aws.amazon.com/blogs/security/how-to-develop-an-amazon-security-lake-poc/){:target="_blank" rel="noopener"}

> You can use Amazon Security Lake to simplify log data collection and retention for Amazon Web Services (AWS) and non-AWS data sources. To make sure that you get the most out of your implementation requires proper planning. In this post, we will show you how to plan and implement a proof of concept (POC) for [...]
