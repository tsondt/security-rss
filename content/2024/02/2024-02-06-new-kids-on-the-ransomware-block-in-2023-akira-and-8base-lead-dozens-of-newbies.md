Title: New kids on the ransomware block in 2023: Akira and 8Base lead dozens of newbies
Date: 2024-02-06T10:16:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-06-new-kids-on-the-ransomware-block-in-2023-akira-and-8base-lead-dozens-of-newbies

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/06/akira_and_8base_new_ransomware_research/){:target="_blank" rel="noopener"}

> How good are your takedowns when fresh gangs are linked to previous ops, though? At least 25 new ransomware gangs emerged in 2023, with Akira and 8Base proving the most "successful," research reveals.... [...]
