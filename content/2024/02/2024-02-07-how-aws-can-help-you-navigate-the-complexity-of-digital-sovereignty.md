Title: How AWS can help you navigate the complexity of digital sovereignty
Date: 2024-02-07T21:56:47+00:00
Author: Max Peterson
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Thought Leadership;AWS Digital Sovereignty Pledge;AWS Nitro System;Data protection;Digital Sovereignty;EU Data Protection;Security Blog
Slug: 2024-02-07-how-aws-can-help-you-navigate-the-complexity-of-digital-sovereignty

[Source](https://aws.amazon.com/blogs/security/how-aws-can-help-you-navigate-the-complexity-of-digital-sovereignty/){:target="_blank" rel="noopener"}

> Customers from around the world often tell me that digital sovereignty is a top priority as they look to meet new compliance and industry regulations. In fact, 82% of global organizations are either currently using, planning to use, or considering sovereign cloud solutions in the next two years, according to the International Data Corporation (IDC). [...]
