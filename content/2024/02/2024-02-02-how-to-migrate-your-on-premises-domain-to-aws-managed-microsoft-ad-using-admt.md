Title: How to migrate your on-premises domain to AWS Managed Microsoft AD using ADMT
Date: 2024-02-02T17:53:54+00:00
Author: Austin Webber
Category: AWS Security
Tags: AWS Directory Service;Security, Identity, & Compliance;Uncategorized;ADMT MicrosoftAD;directory service migration;Migrate active directory;Migrate MicrosoftAD;Security Blog
Slug: 2024-02-02-how-to-migrate-your-on-premises-domain-to-aws-managed-microsoft-ad-using-admt

[Source](https://aws.amazon.com/blogs/security/how-to-migrate-your-on-premises-domain-to-aws-managed-microsoft-ad-using-admt/){:target="_blank" rel="noopener"}

> February 2, 2024: We’ve updated this post to fix broken links and added a note on migrating passwords. Customers often ask us how to migrate their on-premises Active Directory (AD) domain to AWS so they can be free of the operational management of their AD infrastructure. Frequently they are unsure how to make the migration [...]
