Title: The Week in Ransomware - February 2nd 2024 - No honor among thieves
Date: 2024-02-02T18:33:19-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-02-02-the-week-in-ransomware-february-2nd-2024-no-honor-among-thieves

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-february-2nd-2024-no-honor-among-thieves/){:target="_blank" rel="noopener"}

> Attacks on hospitals continued this week, with ransomware operations disrupting patient care as they force organization to respond to cyberattacks. [...]
