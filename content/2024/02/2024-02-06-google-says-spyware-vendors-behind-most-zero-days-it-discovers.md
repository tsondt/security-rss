Title: Google says spyware vendors behind most zero-days it discovers
Date: 2024-02-06T12:27:54-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2024-02-06-google-says-spyware-vendors-behind-most-zero-days-it-discovers

[Source](https://www.bleepingcomputer.com/news/security/google-says-spyware-vendors-behind-most-zero-days-it-discovers/){:target="_blank" rel="noopener"}

> Commercial spyware vendors (CSV) were behind 80% of the zero-day vulnerabilities Google's Threat Analysis Group (TAG) discovered in 2023 and used to spy on devices worldwide. [...]
