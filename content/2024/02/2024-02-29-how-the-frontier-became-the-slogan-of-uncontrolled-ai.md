Title: How the “Frontier” Became the Slogan of Uncontrolled AI
Date: 2024-02-29T12:00:19+00:00
Author: B. Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;laws;LLM;regulation
Slug: 2024-02-29-how-the-frontier-became-the-slogan-of-uncontrolled-ai

[Source](https://www.schneier.com/blog/archives/2024/02/how-the-frontier-became-the-slogan-of-uncontrolled-ai.html){:target="_blank" rel="noopener"}

> Artificial intelligence (AI) has been billed as the next frontier of humanity: the newly available expanse whose exploration will drive the next era of growth, wealth, and human flourishing. It’s a scary metaphor. Throughout American history, the drive for expansion and the very concept of terrain up for grabs—land grabs, gold rushes, new frontiers—have provided a permission structure for imperialism and exploitation. This could easily hold true for AI. This isn’t the first time the concept of a frontier has been used as a metaphor for AI, or technology in general. As early as 2018, the powerful foundation models powering [...]
