Title: Lurie Children's Hospital took systems offline after cyberattack
Date: 2024-02-02T11:23:48-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-02-02-lurie-childrens-hospital-took-systems-offline-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/lurie-childrens-hospital-took-systems-offline-after-cyberattack/){:target="_blank" rel="noopener"}

> Lurie Children's Hospital in Chicago was forced to take IT systems offline after a cyberattack, disrupting normal operations and delaying medical care in some instances. [...]
