Title: 2023 C5 Type 2 attestation report available, including two new Regions and 170 services in scope
Date: 2024-02-05T17:51:19+00:00
Author: Julian Herlinghaus
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Auditing;AWS security;C5;Compliance;Security;Security Blog
Slug: 2024-02-05-2023-c5-type-2-attestation-report-available-including-two-new-regions-and-170-services-in-scope

[Source](https://aws.amazon.com/blogs/security/2023-c5-type-2-attestation-report-available-including-two-new-regions-and-170-services-in-scope/){:target="_blank" rel="noopener"}

> We continue to expand the scope of our assurance programs at Amazon Web Services (AWS), and we’re pleased to announce that AWS has successfully completed the 2023 Cloud Computing Compliance Controls Catalogue (C5) attestation cycle with 170 services in scope. This alignment with C5 requirements demonstrates our ongoing commitment to adhere to the heightened expectations [...]
