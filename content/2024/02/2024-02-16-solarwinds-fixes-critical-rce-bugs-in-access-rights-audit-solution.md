Title: SolarWinds fixes critical RCE bugs in access rights audit solution
Date: 2024-02-16T13:32:44-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-16-solarwinds-fixes-critical-rce-bugs-in-access-rights-audit-solution

[Source](https://www.bleepingcomputer.com/news/security/solarwinds-fixes-critical-rce-bugs-in-access-rights-audit-solution/){:target="_blank" rel="noopener"}

> SolarWinds has patched five remote code execution (RCE) flaws in its Access Rights Manager (ARM) solution, including three critical severity vulnerabilities that allow unauthenticated exploitation. [...]
