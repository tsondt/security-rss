Title: On the Insecurity of Software Bloat
Date: 2024-02-15T12:04:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cloud computing;cybersecurity;vulnerabilities
Slug: 2024-02-15-on-the-insecurity-of-software-bloat

[Source](https://www.schneier.com/blog/archives/2024/02/on-the-insecurity-of-software-bloat.html){:target="_blank" rel="noopener"}

> Good essay on software bloat and the insecurities it causes. The world ships too much code, most of it by third parties, sometimes unintended, most of it uninspected. Because of this, there is a huge attack surface full of mediocre code. Efforts are ongoing to improve the quality of code itself, but many exploits are due to logic fails, and less progress has been made scanning for those. Meanwhile, great strides could be made by paring down just how much code we expose to the world. This will increase time to market for products, but legislation is around the corner [...]
