Title: AWS Payment Cryptography is PCI PIN and P2PE certified
Date: 2024-02-27T20:03:31+00:00
Author: Tim Winston
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Compliance;Compliance reports;Financial Services;PCI;Security Blog
Slug: 2024-02-27-aws-payment-cryptography-is-pci-pin-and-p2pe-certified

[Source](https://aws.amazon.com/blogs/security/aws-payment-cryptography-is-pci-pin-and-p2pe-certified/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce that AWS Payment Cryptography is certified for Payment Card Industry Personal Identification Number (PCI PIN) version 3.1 and as a PCI Point-to-Point Encryption (P2PE) version 3.1 Decryption Component. With Payment Cryptography, your payment processing applications can use payment hardware security modules (HSMs) that are PCI PIN Transaction [...]
