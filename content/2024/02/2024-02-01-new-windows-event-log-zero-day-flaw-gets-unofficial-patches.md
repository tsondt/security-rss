Title: New Windows Event Log zero-day flaw gets unofficial patches
Date: 2024-02-01T10:34:58-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-02-01-new-windows-event-log-zero-day-flaw-gets-unofficial-patches

[Source](https://www.bleepingcomputer.com/news/microsoft/new-windows-event-log-zero-day-flaw-gets-unofficial-patches/){:target="_blank" rel="noopener"}

> Free unofficial patches are available for a new Windows zero-day vulnerability dubbed 'EventLogCrasher' that lets attackers remotely crash the Event Log service on devices within the same Windows domain. [...]
