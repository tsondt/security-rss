Title: JetBrains urges swift patching of latest critical TeamCity flaw
Date: 2024-02-07T12:33:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-07-jetbrains-urges-swift-patching-of-latest-critical-teamcity-flaw

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/07/jetbrains_teamcity_critical_vuln/){:target="_blank" rel="noopener"}

> Cloud version is safe, but no assurances offered about possible on-prem exploits JetBrains is encouraging all users of TeamCity (on-prem) to upgrade to the latest version following the disclosure of a critical vulnerability in the CI/CD tool.... [...]
