Title: Verizon insider data breach hits over 63,000 employees
Date: 2024-02-06T11:02:05-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-02-06-verizon-insider-data-breach-hits-over-63000-employees

[Source](https://www.bleepingcomputer.com/news/security/verizon-insider-data-breach-hits-over-63-000-employees/){:target="_blank" rel="noopener"}

> Verizon Communications is warning that an insider data breach impacts almost half its workforce, exposing sensitive employee information. [...]
