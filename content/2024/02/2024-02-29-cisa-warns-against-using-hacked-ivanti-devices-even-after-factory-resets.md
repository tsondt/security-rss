Title: CISA warns against using hacked Ivanti devices even after factory resets
Date: 2024-02-29T15:35:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-29-cisa-warns-against-using-hacked-ivanti-devices-even-after-factory-resets

[Source](https://www.bleepingcomputer.com/news/security/cisa-warns-against-using-hacked-ivanti-devices-even-after-factory-resets/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity and Infrastructure Security Agency (CISA) revealed today that attackers who breached Ivanti appliances using one of multiple actively exploited vulnerabilities can maintain root persistence even after performing factory resets. [...]
