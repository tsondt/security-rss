Title: Verizon says 63K employees' info fell into the wrong hands – an insider this time
Date: 2024-02-06T19:00:15+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-06-verizon-says-63k-employees-info-fell-into-the-wrong-hands-an-insider-this-time

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/06/verizon_internal_privacy_breach/){:target="_blank" rel="noopener"}

> Telco says it's a private matter, data 'not shared externally' Verizon is notifying more than 63,000 people, mostly current employees, that an insider, accidentally or otherwise, had inappropriate access to their personal data.... [...]
