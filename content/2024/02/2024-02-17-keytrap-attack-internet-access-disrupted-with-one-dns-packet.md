Title: KeyTrap attack: Internet access disrupted with one DNS packet
Date: 2024-02-17T11:08:19-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-02-17-keytrap-attack-internet-access-disrupted-with-one-dns-packet

[Source](https://www.bleepingcomputer.com/news/security/keytrap-attack-internet-access-disrupted-with-one-dns-packet/){:target="_blank" rel="noopener"}

> A serious vulnerability named KeyTrap in the Domain Name System Security Extensions (DNSSEC) feature could be exploited to deny internet access to applications for an extended period. [...]
