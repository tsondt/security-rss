Title: Prudential Financial breached in data theft cyberattack
Date: 2024-02-13T17:35:54-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-13-prudential-financial-breached-in-data-theft-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/prudential-financial-breached-in-data-theft-cyberattack/){:target="_blank" rel="noopener"}

> Prudential Financial has disclosed that its network was breached last week, with the attackers stealing employee and contractor data before being blocked from compromised systems one day later. [...]
