Title: Apple promises to protect iMessage chats from quantum computers
Date: 2024-02-21T21:09:02+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-21-apple-promises-to-protect-imessage-chats-from-quantum-computers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/21/apple_postquantum_security/){:target="_blank" rel="noopener"}

> Easy to defend against stuff that may never actually work – oh there we go again, being all cynical like Apple says it's going to upgrade the cryptographic protocol used by iMessage to hopefully prevent the decryption of conversations by quantum computers, should those machines ever exist in a meaningful way.... [...]
