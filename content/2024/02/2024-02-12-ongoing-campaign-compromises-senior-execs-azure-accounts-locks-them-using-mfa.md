Title: Ongoing campaign compromises senior execs’ Azure accounts, locks them using MFA
Date: 2024-02-12T23:41:17+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;account takeovers;email account comprmises;Microsoft Azure;phishing
Slug: 2024-02-12-ongoing-campaign-compromises-senior-execs-azure-accounts-locks-them-using-mfa

[Source](https://arstechnica.com/?p=2002911){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Hundreds of Microsoft Azure accounts, some belonging to senior executives, are being targeted by unknown attackers in an ongoing campaign that's aiming to steal sensitive data and financial assets from dozens of organizations, researchers with security firm Proofpoint said Monday. The campaign attempts to compromise targeted Azure environments by sending account owners emails that integrate techniques for credential phishing and account takeovers. The threat actors are doing so by combining individualized phishing lures with shared documents. Some of the documents embed links that, when clicked, redirect users to a phishing webpage. The wide breadth of roles [...]
