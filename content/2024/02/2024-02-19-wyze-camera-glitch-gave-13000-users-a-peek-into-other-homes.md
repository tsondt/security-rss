Title: Wyze camera glitch gave 13,000 users a peek into other homes
Date: 2024-02-19T12:20:19-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-19-wyze-camera-glitch-gave-13000-users-a-peek-into-other-homes

[Source](https://www.bleepingcomputer.com/news/security/wyze-camera-glitch-gave-13-000-users-a-peek-into-other-homes/){:target="_blank" rel="noopener"}

> ​Wyze shared more details on a security incident that impacted thousands of users on Friday and said that at least 13,000 customers could get a peek into other users' homes. [...]
