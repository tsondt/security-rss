Title: Mitigating AI security risks
Date: 2024-02-15T16:50:09+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2024-02-15-mitigating-ai-security-risks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/15/mitigating_ai_security_risks/){:target="_blank" rel="noopener"}

> From APIs to Zero Trust Webinar It has become possible to swiftly and inexpensively train, validate and deploy AI models and applications, yet while we embrace innovation, are we aware of the security risks?... [...]
