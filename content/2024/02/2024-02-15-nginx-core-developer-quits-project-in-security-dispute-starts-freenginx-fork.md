Title: Nginx core developer quits project in security dispute, starts “freenginx” fork
Date: 2024-02-15T20:04:56+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;Tech;cves;f5;freenginx;hosting;Invasion of Ukraine;Nginx;open source;russia;Ukraine;web servers
Slug: 2024-02-15-nginx-core-developer-quits-project-in-security-dispute-starts-freenginx-fork

[Source](https://arstechnica.com/?p=2003602){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) A core developer of Nginx, currently the world's most popular web server, has quit the project, stating that he no longer sees it as "a free and open source project... for the public good." His fork, freenginx, is "going to be run by developers, and not corporate entities," writes Maxim Dounin, and will be "free from arbitrary corporate actions." Dounin is one of the earliest and still most active coders on the open source Nginx project and one of the first employees of Nginx, Inc., a company created in 2011 to commercially support the steadily growing [...]
