Title: HPE investigates new breach after data for sale on hacking forum
Date: 2024-02-05T13:33:51-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-05-hpe-investigates-new-breach-after-data-for-sale-on-hacking-forum

[Source](https://www.bleepingcomputer.com/news/security/hpe-investigates-new-breach-after-data-for-sale-on-hacking-forum/){:target="_blank" rel="noopener"}

> Hewlett Packard Enterprise (HPE) is investigating a potential new breach after a threat actor put allegedly stolen data up for sale on a hacking forum, claiming it contains HPE credentials and other sensitive information. [...]
