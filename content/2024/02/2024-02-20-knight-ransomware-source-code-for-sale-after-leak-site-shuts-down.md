Title: Knight ransomware source code for sale after leak site shuts down
Date: 2024-02-20T11:28:57-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-02-20-knight-ransomware-source-code-for-sale-after-leak-site-shuts-down

[Source](https://www.bleepingcomputer.com/news/security/knight-ransomware-source-code-for-sale-after-leak-site-shuts-down/){:target="_blank" rel="noopener"}

> The alleged source code for the third iteration of the Knight ransomware is being offered for sale to a single buyer on a hacker forum by a representative of the operation. [...]
