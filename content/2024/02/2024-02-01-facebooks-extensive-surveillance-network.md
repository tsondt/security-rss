Title: Facebook’s Extensive Surveillance Network
Date: 2024-02-01T12:06:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;data privacy;Facebook;Meta;privacy;reports;surveillance;tracking
Slug: 2024-02-01-facebooks-extensive-surveillance-network

[Source](https://www.schneier.com/blog/archives/2024/02/facebooks-extensive-surveillance-network.html){:target="_blank" rel="noopener"}

> Consumer Reports is reporting that Facebook has built a massive surveillance network: Using a panel of 709 volunteers who shared archives of their Facebook data, Consumer Reports found that a total of 186,892 companies sent data about them to the social network. On average, each participant in the study had their data sent to Facebook by 2,230 companies. That number varied significantly, with some panelists’ data listing over 7,000 companies providing their data. The Markup helped Consumer Reports recruit participants for the study. Participants downloaded an archive of the previous three years of their data from their Facebook settings, then [...]
