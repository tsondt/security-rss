Title: Half of polled infosec pros say their degree was less than useful for real-world work
Date: 2024-02-07T20:31:16+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-07-half-of-polled-infosec-pros-say-their-degree-was-less-than-useful-for-real-world-work

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/07/kaspersky_infosec_cso/){:target="_blank" rel="noopener"}

> The other half paid attention in class? Half of infosec professionals polled by Kaspersky said any cybersecurity knowledge they picked up from their higher education is at best somewhat useful for doing their day jobs. On the other hand, half said the know-how was at least very useful. We're a glass half-empty lot.... [...]
