Title: 200,000 Facebook Marketplace user records leaked on hacking forum
Date: 2024-02-13T14:30:57-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-13-200000-facebook-marketplace-user-records-leaked-on-hacking-forum

[Source](https://www.bleepingcomputer.com/news/security/200-000-facebook-marketplace-user-records-leaked-on-hacking-forum/){:target="_blank" rel="noopener"}

> A threat actor leaked 200,000 records on a hacker forum, claiming they contained the mobile phone numbers, email addresses, and other personal information of Facebook Marketplace users. [...]
