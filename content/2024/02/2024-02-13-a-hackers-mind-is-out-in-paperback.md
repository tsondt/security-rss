Title: A Hacker’s Mind is Out in Paperback
Date: 2024-02-13T20:13:29+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;A Hacker's Mind;books;Schneier news
Slug: 2024-02-13-a-hackers-mind-is-out-in-paperback

[Source](https://www.schneier.com/blog/archives/2024/02/a-hackers-mind-is-out-in-paperback.html){:target="_blank" rel="noopener"}

> The paperback version of A Hacker’s Mind has just been published. It’s the same book, only a cheaper format. But—and this is the real reason I am posting this—Amazon has significantly discounted the hardcover to $15 to get rid of its stock. This is much cheaper than I am selling it for, and cheaper even than the paperback. So if you’ve been waiting for a price drop, this is your chance. [...]
