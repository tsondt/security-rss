Title: Ex-Post Office boss ‘gave Fujitsu bonus contract despite warnings’
Date: 2024-02-13T06:00:33+00:00
Author: Daniel Boffey Chief reporter
Category: The Guardian
Tags: Post Office Horizon scandal;Post Office;UK news;Software;Fujitsu;Computing;Business;Data and computer security
Slug: 2024-02-13-ex-post-office-boss-gave-fujitsu-bonus-contract-despite-warnings

[Source](https://www.theguardian.com/uk-news/2024/feb/13/ex-post-office-boss-gave-fujitsu-bonus-contract-despite-warnings-whistleblowers-says){:target="_blank" rel="noopener"}

> Exclusive: Whistleblowers say Paula Vennells agreed to move archive, which risked destroying data that could clear operators The former Post Office boss Paula Vennells gave Fujitsu a bonus contract in 2013 to take over an archive of branch data, despite warnings such a move would destroy evidence that might clear operators, whistleblowers have said. Transaction information was “replatformed” on cost grounds from a “gold standard” external storage system known as Centera to one owned by the Japanese software company running the Post Office’s Horizon IT network. Continue reading... [...]
