Title: ALPHV/BlackCat responsible for Change Healthcare cyberattack
Date: 2024-02-26T20:40:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-26-alphvblackcat-responsible-for-change-healthcare-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/26/alphv_healthcare_unitedhealth/){:target="_blank" rel="noopener"}

> US government's bounty hasn't borne fruit as whack-a-mole game goes on Updated The ALPHV/BlackCat ransomware gang is reportedly responsible for the massive Change Healthcare cyberattack that has disrupted pharmacies across the US since last week.... [...]
