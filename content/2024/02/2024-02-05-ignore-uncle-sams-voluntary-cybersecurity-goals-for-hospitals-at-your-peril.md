Title: Ignore Uncle Sam's 'voluntary' cybersecurity goals for hospitals at your peril
Date: 2024-02-05T19:30:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-02-05-ignore-uncle-sams-voluntary-cybersecurity-goals-for-hospitals-at-your-peril

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/05/us_voluntary_cybersecurity_goals_hospitals/){:target="_blank" rel="noopener"}

> What is on HHS paper will most likely become law, Google security boss says Interview If you are responsible for infosec at a US hospital or other healthcare organization, and you treat the government's new "voluntary" cybersecurity performance goals (CPGs) as, well, voluntary, you're ignoring the writing on the wall.... [...]
