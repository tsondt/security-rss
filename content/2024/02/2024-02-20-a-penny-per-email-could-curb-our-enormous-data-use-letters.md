Title: A penny per email could curb our enormous data use | Letters
Date: 2024-02-20T16:54:02+00:00
Author: Guardian Staff
Category: The Guardian
Tags: Technology;Energy efficiency;Computing;Digital media;Climate crisis;Energy;Environment;Email;WhatsApp;Data and computer security
Slug: 2024-02-20-a-penny-per-email-could-curb-our-enormous-data-use-letters

[Source](https://www.theguardian.com/technology/2024/feb/20/a-penny-per-email-could-curb-our-enormous-data-use){:target="_blank" rel="noopener"}

> Mike McClelland proposes a 1p charge for messages in response to an article on the downside of Ireland’s datacentre boom. Plus a letter from Sue Stephenson The long read ( Power grab: the hidden costs of Ireland’s datacentre boom, 15 February ) highlights the enormous cost in terms of energy consumption and carbon emissions of our collective love affair with the seemingly free ability to send emails, text and WhatsApp messages every minute of the day. There is an enormous cost to us all in terms of data storage – a fact of which we are barely cognisant. Think how [...]
