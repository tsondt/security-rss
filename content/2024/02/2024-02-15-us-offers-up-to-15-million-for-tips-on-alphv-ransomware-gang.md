Title: US offers up to $15 million for tips on ALPHV ransomware gang
Date: 2024-02-15T13:57:12-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-15-us-offers-up-to-15-million-for-tips-on-alphv-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/us-offers-up-to-15-million-for-tips-on-alphv-ransomware-gang/){:target="_blank" rel="noopener"}

> The U.S. State Department is offering rewards of up to $10 million for information that could lead to the identification or location of ALPHV/Blackcat ransomware gang leaders. [...]
