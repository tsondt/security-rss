Title: US announces visa ban on those linked to commercial spyware
Date: 2024-02-05T15:26:06-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-05-us-announces-visa-ban-on-those-linked-to-commercial-spyware

[Source](https://www.bleepingcomputer.com/news/security/us-announces-visa-ban-on-those-linked-to-commercial-spyware/){:target="_blank" rel="noopener"}

> Secretary of State Antony J. Blinken announced today a new visa restriction policy that will enable the Department of State to ban those linked to commercial spyware from entering the United States. [...]
