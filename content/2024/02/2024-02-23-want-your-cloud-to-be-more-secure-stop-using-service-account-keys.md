Title: Want your cloud to be more secure? Stop using service account keys
Date: 2024-02-23T17:00:00+00:00
Author: Luis Urena
Category: GCP Security
Tags: Security & Identity
Slug: 2024-02-23-want-your-cloud-to-be-more-secure-stop-using-service-account-keys

[Source](https://cloud.google.com/blog/products/identity-security/want-your-cloud-to-be-more-secure-stop-using-service-account-keys/){:target="_blank" rel="noopener"}

> Securing cloud credentials has emerged as a challenge on the scale of Moby Dick: It presents an enormous problem, and simple solutions remain elusive. Credential security problems are also widespread: More than 69% of cloud compromises were caused by credential issues, including weak passwords, no passwords, and exposed APIs, according to Google Cloud’s Q3 2023 Threat Horizons Report. However, unlike Moby Dick, this story may have a happy ending. Organizations can get started on reducing the risk they face from credential-related compromises by protecting service accounts. Service accounts are essential tools in cloud management. They make API calls on behalf [...]
