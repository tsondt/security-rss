Title: Microsoft expands free logging capabilities after May breach
Date: 2024-02-21T17:31:33-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-21-microsoft-expands-free-logging-capabilities-after-may-breach

[Source](https://www.bleepingcomputer.com/news/security/microsoft-expands-free-logging-capabilities-after-may-breach/){:target="_blank" rel="noopener"}

> Microsoft has expanded free logging capabilities for all Purview Audit standard customers, including U.S. federal agencies, six months after disclosing that Chinese hackers stole U.S. government emails undetected in an Exchange Online breach between May and June 2023. [...]
