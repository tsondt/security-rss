Title: Microsoft Outlook December updates trigger ICS security alerts
Date: 2024-02-05T17:03:54-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-02-05-microsoft-outlook-december-updates-trigger-ics-security-alerts

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-outlook-december-updates-trigger-ics-security-alerts/){:target="_blank" rel="noopener"}

> Microsoft is investigating an issue that triggers Outlook security alerts when trying to open.ICS calendar files after installing December 2023 Patch Tuesday Office security updates. [...]
