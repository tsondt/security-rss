Title: Back from the dead: LockBit taunts cops, threatens to leak Trump docs
Date: 2024-02-26T19:14:04+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-26-back-from-the-dead-lockbit-taunts-cops-threatens-to-leak-trump-docs

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/26/lockbit_back_in_action/){:target="_blank" rel="noopener"}

> Officials have until March 2 to cough up or stolen data gets leaked Updated LockBit claims it's back in action just days after an international law enforcement effort seized the ransomware gang's servers and websites, and retrieved more than 1,000 decryption keys to assist victims.... [...]
