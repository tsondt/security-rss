Title: Chinese hackers fail to rebuild botnet after FBI takedown
Date: 2024-02-07T10:00:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-07-chinese-hackers-fail-to-rebuild-botnet-after-fbi-takedown

[Source](https://www.bleepingcomputer.com/news/security/chinese-hackers-fail-to-rebuild-botnet-after-fbi-takedown/){:target="_blank" rel="noopener"}

> Chinese Volt Typhoon state hackers failed to revive a botnet recently taken down by the FBI, which was previously used in attacks targeting critical infrastructure across the United States. [...]
