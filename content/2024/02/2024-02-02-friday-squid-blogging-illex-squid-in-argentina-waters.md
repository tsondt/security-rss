Title: Friday Squid Blogging: Illex Squid in Argentina Waters
Date: 2024-02-02T22:03:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-02-02-friday-squid-blogging-illex-squid-in-argentina-waters

[Source](https://www.schneier.com/blog/archives/2024/02/friday-squid-blogging-illex-squid-in-argentina-waters.html){:target="_blank" rel="noopener"}

> Argentina is reporting that there is a good population of illex squid in its waters ready for fishing, and is working to ensure that Chinese fishing boats don’t take it all. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
