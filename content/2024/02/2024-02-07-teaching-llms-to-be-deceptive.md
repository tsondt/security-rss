Title: Teaching LLMs to Be Deceptive
Date: 2024-02-07T12:04:25+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;deception;LLM
Slug: 2024-02-07-teaching-llms-to-be-deceptive

[Source](https://www.schneier.com/blog/archives/2024/02/teaching-llms-to-be-deceptive.html){:target="_blank" rel="noopener"}

> Interesting research: “ Sleeper Agents: Training Deceptive LLMs that Persist Through Safety Training “: Abstract: Humans are capable of strategically deceptive behavior: behaving helpfully in most situations, but then behaving very differently in order to pursue alternative objectives when given the opportunity. If an AI system learned such a deceptive strategy, could we detect it and remove it using current state-of-the-art safety training techniques? To study this question, we construct proof-of-concept examples of deceptive behavior in large language models (LLMs). For example, we train models that write secure code when the prompt states that the year is 2023, but insert [...]
