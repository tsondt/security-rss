Title: North Korean hackers linked to defense sector supply-chain attack
Date: 2024-02-19T15:24:37-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-02-19-north-korean-hackers-linked-to-defense-sector-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/north-korean-hackers-linked-to-defense-sector-supply-chain-attack/){:target="_blank" rel="noopener"}

> In an advisory today Germany's federal intelligence agency (BfV) and South Korea's National Intelligence Service (NIS) warn of an ongoing cyber-espionage operation targeting the global defense sector on behalf of the North Korean government. [...]
