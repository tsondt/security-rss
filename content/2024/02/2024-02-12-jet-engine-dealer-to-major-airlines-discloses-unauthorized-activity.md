Title: Jet engine dealer to major airlines discloses 'unauthorized activity'
Date: 2024-02-12T17:15:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-12-jet-engine-dealer-to-major-airlines-discloses-unauthorized-activity

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/12/jet_engine_dealer_to_major/){:target="_blank" rel="noopener"}

> Pulls part of system offline as Black Basta docs suggest the worst Willis Lease Finance Corporation has admitted to US regulators that it fell prey to a "cybersecurity incident" after data purportedly stolen from the biz was posted to the Black Basta ransomware group's leak blog.... [...]
