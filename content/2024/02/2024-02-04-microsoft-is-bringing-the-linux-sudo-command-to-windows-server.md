Title: Microsoft is bringing the Linux sudo command to Windows Server
Date: 2024-02-04T12:26:16-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-02-04-microsoft-is-bringing-the-linux-sudo-command-to-windows-server

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-is-bringing-the-linux-sudo-command-to-windows-server/){:target="_blank" rel="noopener"}

> Microsoft is bringing the Linux 'sudo' feature to Windows Server 2025, offering a new way for admins to elevate privileges for console applications. [...]
