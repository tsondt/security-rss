Title: Chinese hackers hid in US infrastructure network for 5 years
Date: 2024-02-07T15:08:11-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-07-chinese-hackers-hid-in-us-infrastructure-network-for-5-years

[Source](https://www.bleepingcomputer.com/news/security/chinese-hackers-hid-in-us-infrastructure-network-for-5-years/){:target="_blank" rel="noopener"}

> The Chinese Volt Typhoon cyber-espionage group infiltrated a critical infrastructure network in the United States and remained undetected for at least five years before being discovered, according to a joint advisory from CISA, the NSA, the FBI, and partner Five Eyes agencies. [...]
