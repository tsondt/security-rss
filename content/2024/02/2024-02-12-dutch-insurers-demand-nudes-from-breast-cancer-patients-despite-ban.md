Title: Dutch insurers demand nudes from breast cancer patients despite ban
Date: 2024-02-12T19:15:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-12-dutch-insurers-demand-nudes-from-breast-cancer-patients-despite-ban

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/12/dutch_insurers_breast_cancer/){:target="_blank" rel="noopener"}

> No photos? No, second operation Dutch health insurers are reportedly forcing breast cancer patients to submit photos of their breasts prior to reconstructive surgery despite a government ban on precisely that.... [...]
