Title: Critical infrastructure software maker confirms ransomware attack
Date: 2024-02-20T09:36:30-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-02-20-critical-infrastructure-software-maker-confirms-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/critical-infrastructure-software-maker-confirms-ransomware-attack/){:target="_blank" rel="noopener"}

> PSI Software SE, a German software developer for complex production and logistics processes, has confirmed that the cyber incident it disclosed last week is a ransomware attack that impacted its internal infrastructure. [...]
