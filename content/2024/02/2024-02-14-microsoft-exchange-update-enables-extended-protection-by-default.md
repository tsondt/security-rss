Title: Microsoft Exchange update enables Extended Protection by default
Date: 2024-02-14T12:34:26-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-02-14-microsoft-exchange-update-enables-extended-protection-by-default

[Source](https://www.bleepingcomputer.com/news/security/microsoft-exchange-update-enables-extended-protection-by-default/){:target="_blank" rel="noopener"}

> Microsoft is automatically enabling Windows Extended Protection on Exchange servers after installing this month's 2024 H1 Cumulative Update (aka CU14). [...]
