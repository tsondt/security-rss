Title: Wyze admits 13,000 users could have viewed strangers' camera feeds
Date: 2024-02-20T15:15:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-20-wyze-admits-13000-users-could-have-viewed-strangers-camera-feeds

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/20/wyze_admits_13000_users_allowed_feed_access/){:target="_blank" rel="noopener"}

> Customers report feeling violated following the security snafu Smart home security camera slinger Wyze is telling customers that a cybersecurity "incident" allowed thousands of users to see other people's camera feeds.... [...]
