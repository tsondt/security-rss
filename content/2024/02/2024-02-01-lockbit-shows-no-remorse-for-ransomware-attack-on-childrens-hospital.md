Title: LockBit shows no remorse for ransomware attack on children's hospital
Date: 2024-02-01T14:15:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-01-lockbit-shows-no-remorse-for-ransomware-attack-on-childrens-hospital

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/01/lockbit_ransomware_attack_hospital/){:target="_blank" rel="noopener"}

> It even had the gall to set the ransom demand at $800K... for a nonprofit Ransomware gang LockBit is claiming responsibility for an attack on a Chicago children's hospital in an apparent deviation from its previous policy of not targeting nonprofits.... [...]
