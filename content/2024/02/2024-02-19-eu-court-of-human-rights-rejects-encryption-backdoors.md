Title: EU Court of Human Rights Rejects Encryption Backdoors
Date: 2024-02-19T16:15:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;backdoors;encryption;EU;human rights
Slug: 2024-02-19-eu-court-of-human-rights-rejects-encryption-backdoors

[Source](https://www.schneier.com/blog/archives/2024/02/eu-court-of-human-rights-rejects-encryption-backdoors.html){:target="_blank" rel="noopener"}

> The European Court of Human Rights has ruled that breaking end-to-end encryption by adding backdoors violates human rights : Seemingly most critically, the [Russian] government told the ECHR that any intrusion on private lives resulting from decrypting messages was “necessary” to combat terrorism in a democratic society. To back up this claim, the government pointed to a 2017 terrorist attack that was “coordinated from abroad through secret chats via Telegram.” The government claimed that a second terrorist attack that year was prevented after the government discovered it was being coordinated through Telegram chats. However, privacy advocates backed up Telegram’s claims [...]
