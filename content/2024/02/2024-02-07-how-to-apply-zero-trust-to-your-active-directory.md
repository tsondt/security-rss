Title: How to Apply Zero Trust to your Active Directory
Date: 2024-02-07T10:05:10-05:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2024-02-07-how-to-apply-zero-trust-to-your-active-directory

[Source](https://www.bleepingcomputer.com/news/security/how-to-apply-zero-trust-to-your-active-directory/){:target="_blank" rel="noopener"}

> With cyberattacks happening everyday, how can we apply zero trust principles towards keeping our Active Directory secure? Learn more from Specops Software on how to apply zero trust principles. [...]
