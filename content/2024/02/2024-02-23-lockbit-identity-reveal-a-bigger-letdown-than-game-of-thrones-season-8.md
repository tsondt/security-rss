Title: LockBit identity reveal a bigger letdown than Game of Thrones Season 8
Date: 2024-02-23T16:25:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-23-lockbit-identity-reveal-a-bigger-letdown-than-game-of-thrones-season-8

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/23/lockbit_identity_reveal/){:target="_blank" rel="noopener"}

> NCA still left enough for onlookers to wonder if there's anything more to come The grand finale of the week of LockBit leaks was slated to expose the real identity of LockBitSupp – the alias of the gang's public spokesperson – but the reveal has fallen short of expectations.... [...]
