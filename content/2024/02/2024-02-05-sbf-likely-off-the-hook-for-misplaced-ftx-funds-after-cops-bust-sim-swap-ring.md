Title: SBF likely off the hook for misplaced FTX funds after cops bust SIM swap ring
Date: 2024-02-05T01:27:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-02-05-sbf-likely-off-the-hook-for-misplaced-ftx-funds-after-cops-bust-sim-swap-ring

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/05/sbf_off_the_hook_for/){:target="_blank" rel="noopener"}

> PLUS: more glibc vulns discovered; DraftKings hacker sentenced; and a hefty dose of critical vulnerabilities Infosec In Brief The recent indictment of a massive SIM-swapping ring may mean convicted crypto conman Sam Bankman-Fried is innocent of at least one allegation still hanging over his head: The theft of more than $400 million in crypto hacked from wallets belonging to his crypto firm, FTX, just before it declared bankruptcy.... [...]
