Title: Hackers abuse Google Cloud Run in massive banking trojan campaign
Date: 2024-02-21T16:07:36-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud;Google
Slug: 2024-02-21-hackers-abuse-google-cloud-run-in-massive-banking-trojan-campaign

[Source](https://www.bleepingcomputer.com/news/security/hackers-abuse-google-cloud-run-in-massive-banking-trojan-campaign/){:target="_blank" rel="noopener"}

> Security researchers are warning of hackers abusing the Google Cloud Run service to distribute massive volumes of banking trojans like Astaroth, Mekotio, and Ousaban. [...]
