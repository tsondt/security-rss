Title: Australian spy chief fears sabotage of critical infrastructure
Date: 2024-02-29T01:58:15+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-02-29-australian-spy-chief-fears-sabotage-of-critical-infrastructure

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/29/asio_threat_assessment_2024/){:target="_blank" rel="noopener"}

> And accuses a former Australian politician of having 'sold out their country' The director general of security at Australia's Security Intelligence Organisation (ASIO) has delivered his annual threat assessment, revealing ongoing attempts by adversaries to map digital infrastructure with a view to disrupting important services at delicate moments.... [...]
