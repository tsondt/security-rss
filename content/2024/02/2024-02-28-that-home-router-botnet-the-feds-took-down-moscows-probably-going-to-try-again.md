Title: That home router botnet the Feds took down? Moscow's probably going to try again
Date: 2024-02-28T04:32:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-02-28-that-home-router-botnet-the-feds-took-down-moscows-probably-going-to-try-again

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/28/ubiquiti_botnet_second_warning/){:target="_blank" rel="noopener"}

> Non-techies told to master firmware upgrades and firewall rules. For the infosec hardheads: have some IOCs Authorities from eleven nations have delivered a sequel to the January takedown of a botnet run by Russia on compromised Ubiquiti Edge OS routers – in the form of a warning that Russia may try again, so owners of the devices should take precautions.... [...]
