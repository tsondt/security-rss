Title: Romanian hospital ransomware crisis attributed to third-party breach
Date: 2024-02-14T15:48:05+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-14-romanian-hospital-ransomware-crisis-attributed-to-third-party-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/14/romanian_hospital_ransomware_crisis/){:target="_blank" rel="noopener"}

> Emergency impacting more than 100 facilities appears to be caused by incident at software provider The Romanian national cybersecurity agency (DNSC) has pinned the outbreak of ransomware cases across the country's hospitals to an incident at a service provider.... [...]
