Title: Chinese 'connected' cars are a national security threat, says Biden
Date: 2024-02-29T19:01:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-02-29-chinese-connected-cars-are-a-national-security-threat-says-biden

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/29/chinese_connected_cars_are_a/){:target="_blank" rel="noopener"}

> No Chinese automakers sell cars in the US, but the feds are still going to investigate whether they're a threat Concerned over the chance that Chinese-made cars could pose a future threat to national security, Biden's administration is proposing plans to probe potential threats posed by "connected" vehicles made in the Middle Kingdom.... [...]
