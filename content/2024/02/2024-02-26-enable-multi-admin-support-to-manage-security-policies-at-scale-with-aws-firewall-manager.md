Title: Enable multi-admin support to manage security policies at scale with AWS Firewall Manager
Date: 2024-02-26T21:23:24+00:00
Author: Mun Hossain
Category: AWS Security
Tags: AWS Firewall Manager;AWS Organizations;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2024-02-26-enable-multi-admin-support-to-manage-security-policies-at-scale-with-aws-firewall-manager

[Source](https://aws.amazon.com/blogs/security/enable-multi-admin-support-to-manage-security-policies-at-scale-with-aws-firewall-manager/){:target="_blank" rel="noopener"}

> The management of security services across organizations has evolved over the years, and can vary depending on the size of your organization, the type of industry, the number of services to be administered, and compliance regulations and legislation. When compliance standards require you to set up scoped administrative control of event monitoring and auditing, we [...]
