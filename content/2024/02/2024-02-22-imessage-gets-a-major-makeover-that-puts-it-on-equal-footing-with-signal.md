Title: iMessage gets a major makeover that puts it on equal footing with Signal
Date: 2024-02-22T00:37:16+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Apple;Biz & IT;Security;iMessage;messaging apps;MESSENGER;quantum computing;signal
Slug: 2024-02-22-imessage-gets-a-major-makeover-that-puts-it-on-equal-footing-with-signal

[Source](https://arstechnica.com/?p=2005125){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) iMessage is getting a major makeover that makes it among the two messaging apps most prepared to withstand the coming advent of quantum computing, largely at parity with Signal or arguably incrementally more hardened. On Wednesday, Apple said messages sent through iMessage will now be protected by two forms of end-to-end encryption (E2EE), whereas before, it had only one. The encryption being added, known as PQ3, is an implementation of a new algorithm called Kyber that, unlike the algorithms iMessage has used until now, can’t be broken with quantum computing. Apple isn’t replacing the older [...]
