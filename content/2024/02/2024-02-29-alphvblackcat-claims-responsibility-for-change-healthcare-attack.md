Title: ALPHV/BlackCat claims responsibility for Change Healthcare attack
Date: 2024-02-29T00:29:15+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-29-alphvblackcat-claims-responsibility-for-change-healthcare-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/29/alphv_change_healthcare/){:target="_blank" rel="noopener"}

> Brags it lifted 6TB of data, but let's remember these people are criminals and not worthy of much trust Updated The ALPHV/BlackCat cybercrime gang has taken credit – if that's the word – for a ransomware infection at Change Healthcare that has disrupted thousands of pharmacies and hospitals across the US, and also claimed that the amount of sensitive data stolen and affected health-care organizations is much larger than the victims initially disclosed.... [...]
