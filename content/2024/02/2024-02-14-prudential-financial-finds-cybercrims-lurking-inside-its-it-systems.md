Title: Prudential Financial finds cybercrims lurking inside its IT systems
Date: 2024-02-14T17:24:11+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2024-02-14-prudential-financial-finds-cybercrims-lurking-inside-its-it-systems

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/14/prudential_financial_finds_cybercrims_lurking/){:target="_blank" rel="noopener"}

> Some company admin and customers data exposed, but bad guys were there for 'only' a day Prudential Financial, the second largest life insurance company in the US and eight largest worldwide, is dealing with a digital break-in that exposed some internal company and customer records to a criminal group.... [...]
