Title: FBI: Give us warrantless Section 702 snooping powers – or China wins
Date: 2024-02-09T00:26:18+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-09-fbi-give-us-warrantless-section-702-snooping-powers-or-china-wins

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/09/fbi_volt_typhoon_section_702/){:target="_blank" rel="noopener"}

> Never mind the court orders obtained to thwart Volt Typhoon botnet Analysis The FBI's latest PR salvo, as it fights to preserve its warrantless snooping powers on Americans via FISA Section 702, is more big talk of cyberattacks by the Chinese government.... [...]
