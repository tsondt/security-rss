Title: Prolific cybercrime gang disrupted by joint UK, US and EU operation
Date: 2024-02-19T23:57:16+00:00
Author: Reuters
Category: The Guardian
Tags: Cybercrime;Technology;Internet;Malware;Data and computer security
Slug: 2024-02-19-prolific-cybercrime-gang-disrupted-by-joint-uk-us-and-eu-operation

[Source](https://www.theguardian.com/technology/2024/feb/19/prolific-cybercrime-gang-lockbit-disrupted-uk-us-eu-operation-cronos){:target="_blank" rel="noopener"}

> LockBit’s website under control of security agencies from both sides of Atlantic, according to post LockBit, a notorious cybercrime gang that holds its victims’ data to ransom, has been disrupted in a rare international law enforcement operation by Britain’s National Crime Agency, the FBI, Europol and a coalition of international police agencies, according to a post on the gang’s extortion website. “This site is now under the control of the National Crime Agency of the UK, working in close cooperation with the FBI and the international law enforcement taskforce ‘Operation Cronos’,” the post said on Monday. Continue reading... [...]
