Title: Avast ordered to stop selling browsing data from its browsing privacy apps
Date: 2024-02-23T20:37:03+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;Policy;Tech;Avast;avast antivirus;browsing data;federal trade commission;FTC;jumpshot;privacy
Slug: 2024-02-23-avast-ordered-to-stop-selling-browsing-data-from-its-browsing-privacy-apps

[Source](https://arstechnica.com/?p=2005605){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Avast, a name known for its security research and antivirus apps, has long offered Chrome extensions, mobile apps, and other tools aimed at increasing privacy. Avast's apps would "block annoying tracking cookies that collect data on your browsing activities," and prevent web services from "tracking your online activity." Deep in its privacy policy, Avast said information that it collected would be "anonymous and aggregate." In its fiercest rhetoric, Avast's desktop software claimed it would stop "hackers making money off your searches." All of that language was offered up while Avast was collecting users' browser information from [...]
