Title: Ongoing Microsoft Azure account hijacking campaign targets executives
Date: 2024-02-12T14:16:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud
Slug: 2024-02-12-ongoing-microsoft-azure-account-hijacking-campaign-targets-executives

[Source](https://www.bleepingcomputer.com/news/security/ongoing-microsoft-azure-account-hijacking-campaign-targets-executives/){:target="_blank" rel="noopener"}

> A phishing campaign detected in late November 2023 has compromised hundreds of user accounts in dozens of Microsoft Azure environments, including those of senior executives. [...]
