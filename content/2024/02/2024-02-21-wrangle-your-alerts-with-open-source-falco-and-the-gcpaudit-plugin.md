Title: Wrangle your alerts with open source Falco and the gcpaudit plugin
Date: 2024-02-21T17:00:00+00:00
Author: Michele Chubirka
Category: GCP Security
Tags: Containers & Kubernetes;Security & Identity
Slug: 2024-02-21-wrangle-your-alerts-with-open-source-falco-and-the-gcpaudit-plugin

[Source](https://cloud.google.com/blog/products/identity-security/wrangle-your-alerts-with-open-source-falco-and-the-gcpaudit-plugin/){:target="_blank" rel="noopener"}

> Monitoring microservices in the cloud has become an increasingly cumbersome exercise for teams struggling to keep pace with developers’ rapid application release velocity. One way to make things easier for overloaded security teams is to use the open-source runtime security platform Falco to quickly identify suspicious behavior in Linux containers and applications. The overarching goal of Falco is to uncomplicate visibility in rapidly-deployed, cloud-first workloads everywhere, making life less stressful for your engineering organization. Falco can help with other vital security tasks, too. You can use it with Google Kubernetes Engine (GKE), for example, to monitor cluster and container workload [...]
