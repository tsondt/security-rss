Title: Authorities dismantled LockBit before it could unleash revamped variant
Date: 2024-02-22T19:45:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-22-authorities-dismantled-lockbit-before-it-could-unleash-revamped-variant

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/22/lockbit_dismantled_new_variant/){:target="_blank" rel="noopener"}

> New features aimed to stamp out problems of the past Law enforcement's disruption of the LockBit ransomware crew comes as the criminal group was working on bringing a brand-new variant to market, research reveals.... [...]
