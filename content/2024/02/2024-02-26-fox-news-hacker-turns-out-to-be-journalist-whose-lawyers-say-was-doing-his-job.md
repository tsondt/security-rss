Title: Fox News 'hacker' turns out to be journalist whose lawyers say was doing his job
Date: 2024-02-26T11:48:05+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-02-26-fox-news-hacker-turns-out-to-be-journalist-whose-lawyers-say-was-doing-his-job

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/26/in_brief_security/){:target="_blank" rel="noopener"}

> Also, another fake iOS app slips into the store, un-cybersafe EV chargers leave UK shelves, and critical vulns Infosec in brief A Florida journalist has been arrested and charged with breaking into protected computer systems in a case his lawyers say was less "hacking," more "good investigative journalism."... [...]
