Title: Denmark orders schools to stop sending student data to Google
Date: 2024-02-07T15:15:44-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Google;Education;Legal;Security
Slug: 2024-02-07-denmark-orders-schools-to-stop-sending-student-data-to-google

[Source](https://www.bleepingcomputer.com/news/google/denmark-orders-schools-to-stop-sending-student-data-to-google/){:target="_blank" rel="noopener"}

> The Danish data protection authority (Datatilsynet) has issued an injunction regarding student data being funneled to Google through the use of Chromebooks and Google Workspace services in the country's schools. [...]
