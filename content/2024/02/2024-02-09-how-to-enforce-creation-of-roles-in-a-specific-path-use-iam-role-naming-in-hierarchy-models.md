Title: How to enforce creation of roles in a specific path: Use IAM role naming in hierarchy models
Date: 2024-02-09T21:00:10+00:00
Author: Varun Sharma
Category: AWS Security
Tags: AWS Identity and Access Management (IAM);Intermediate (200);Security, Identity, & Compliance;Technical How-to;Top Posts;AWS IAM role;IAM role;Security Blog
Slug: 2024-02-09-how-to-enforce-creation-of-roles-in-a-specific-path-use-iam-role-naming-in-hierarchy-models

[Source](https://aws.amazon.com/blogs/security/how-to-enforce-creation-of-roles-in-a-specific-path-use-iam-role-naming-in-hierarchy-models/){:target="_blank" rel="noopener"}

> An AWS Identity and Access Management (IAM) role is an IAM identity that you create in your AWS account that has specific permissions. An IAM role is similar to an IAM user because it’s an AWS identity with permission policies that determine what the identity can and cannot do on AWS. However, as outlined in [...]
