Title: The ever-present state of cyber security alert
Date: 2024-02-09T14:09:06+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2024-02-09-the-ever-present-state-of-cyber-security-alert

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/09/the_everpresent_state_of_cyber/){:target="_blank" rel="noopener"}

> Should you be paying more attention to securing your AI models from attack? Webinar As artificial intelligence (AI) technology becomes increasingly complex so do the threats from bad actors. It is like a forever war.... [...]
