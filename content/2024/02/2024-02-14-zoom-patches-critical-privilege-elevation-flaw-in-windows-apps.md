Title: Zoom patches critical privilege elevation flaw in Windows apps
Date: 2024-02-14T15:32:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2024-02-14-zoom-patches-critical-privilege-elevation-flaw-in-windows-apps

[Source](https://www.bleepingcomputer.com/news/security/zoom-patches-critical-privilege-elevation-flaw-in-windows-apps/){:target="_blank" rel="noopener"}

> The Zoom desktop and VDI clients and the Meeting SDK for Windows are vulnerable to an improper input validation flaw that could allow an unauthenticated attacker to conduct privilege escalation on the target system over the network. [...]
