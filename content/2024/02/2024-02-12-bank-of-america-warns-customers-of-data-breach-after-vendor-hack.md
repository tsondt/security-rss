Title: Bank of America warns customers of data breach after vendor hack
Date: 2024-02-12T18:32:03-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-12-bank-of-america-warns-customers-of-data-breach-after-vendor-hack

[Source](https://www.bleepingcomputer.com/news/security/bank-of-america-warns-customers-of-data-breach-after-vendor-hack/){:target="_blank" rel="noopener"}

> Bank of America is warning customers of a data breach exposing their personal information after one of its service providers was hacked last year. [...]
