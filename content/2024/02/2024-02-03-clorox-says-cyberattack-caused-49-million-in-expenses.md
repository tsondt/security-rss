Title: Clorox says cyberattack caused $49 million in expenses
Date: 2024-02-03T16:34:14-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-02-03-clorox-says-cyberattack-caused-49-million-in-expenses

[Source](https://www.bleepingcomputer.com/news/security/clorox-says-cyberattack-caused-49-million-in-expenses/){:target="_blank" rel="noopener"}

> Clorox has confirmed that a September 2023 cyberattack has so far cost the company $49 million in expenses related to the response to the incident. [...]
