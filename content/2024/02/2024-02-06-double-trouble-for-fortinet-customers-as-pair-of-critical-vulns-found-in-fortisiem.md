Title: Double trouble for Fortinet customers as pair of critical vulns found in FortiSIEM
Date: 2024-02-06T13:30:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-06-double-trouble-for-fortinet-customers-as-pair-of-critical-vulns-found-in-fortisiem

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/06/fortinet_fortisiem_vulns/){:target="_blank" rel="noopener"}

> Admins should get a move on while info is scarce and exploits aren't yet available Fortinet's FortiSIEM product is vulnerable to two new maximum-severity security vulnerabilities that allow for remote code execution.... [...]
