Title: Feds Seize LockBit Ransomware Websites, Offer Decryption Tools, Troll Affiliates
Date: 2024-02-20T17:09:00+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Ransomware;Artur Sungatov;Bassterlord;Europol;fbi;Ivan Gennadievich Kondratyev;LockBit;LockBitSupp;Mark Stockley;Mikhail Matveev;Mikhail Vasiliev;NCA;Operation Cronos;ProDaft;Ruslan Magomedovich Astamirov;U.K. National Crime Agency
Slug: 2024-02-20-feds-seize-lockbit-ransomware-websites-offer-decryption-tools-troll-affiliates

[Source](https://krebsonsecurity.com/2024/02/feds-seize-lockbit-ransomware-websites-offer-decryption-tools-troll-affiliates/){:target="_blank" rel="noopener"}

> U.S. and U.K. authorities have seized the darknet websites run by LockBit, a prolific and destructive ransomware group that has claimed more than 2,000 victims worldwide and extorted over $120 million in payments. Instead of listing data stolen from ransomware victims who didn’t pay, LockBit’s victim shaming website now offers free recovery tools, as well as news about arrests and criminal charges involving LockBit affiliates. Investigators used the existing design on LockBit’s victim shaming website to feature press releases and free decryption tools. Dubbed “ Operation Cronos,” the law enforcement action involved the seizure of nearly three-dozen servers; the arrest [...]
