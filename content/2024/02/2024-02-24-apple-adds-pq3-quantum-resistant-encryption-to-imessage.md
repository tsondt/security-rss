Title: Apple adds PQ3 quantum-resistant encryption to iMessage
Date: 2024-02-24T11:04:25-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple;Software
Slug: 2024-02-24-apple-adds-pq3-quantum-resistant-encryption-to-imessage

[Source](https://www.bleepingcomputer.com/news/security/apple-adds-pq3-quantum-resistant-encryption-to-imessage/){:target="_blank" rel="noopener"}

> Apple is adding to the iMessage instant messaging service a new post-quantum cryptographic protocol named PQ3, designed to defend encryption from quantum attacks. [...]
