Title: Meta says risk of account theft after phone number recycling isn't its problem to solve
Date: 2024-02-13T08:27:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-02-13-meta-says-risk-of-account-theft-after-phone-number-recycling-isnt-its-problem-to-solve

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/13/meta_phone_security_number_recycling/){:target="_blank" rel="noopener"}

> Leaves it to carriers, promoting a complaint to Irish data cops from Big Tech's bête noire Meta has acknowledged that phone number reuse that allows takeovers of its accounts "is a concern," but the ad biz insists the issue doesn't qualify for its bug bounty program and is a matter for telecom companies to sort out.... [...]
