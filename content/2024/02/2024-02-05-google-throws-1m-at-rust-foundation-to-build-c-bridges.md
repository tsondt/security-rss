Title: Google throws $1m at Rust Foundation to build C++ bridges
Date: 2024-02-05T22:58:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-02-05-google-throws-1m-at-rust-foundation-to-build-c-bridges

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/05/google_rust_donation/){:target="_blank" rel="noopener"}

> Chocolate Factory matches Microsoft money for memory safety Google on Monday donated $1 million to the Rust Foundation specifically to improve interoperability between the language and C++.... [...]
