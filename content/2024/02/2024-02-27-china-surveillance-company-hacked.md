Title: China Surveillance Company Hacked
Date: 2024-02-27T12:03:59+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;China;hacking;surveillance
Slug: 2024-02-27-china-surveillance-company-hacked

[Source](https://www.schneier.com/blog/archives/2024/02/china-surveillance-company-hacked.html){:target="_blank" rel="noopener"}

> Last week, someone posted something like 570 files, images and chat logs from a Chinese company called I-Soon. I-Soon sells hacking and espionage services to Chinese national and local government. Lots of details in the news articles. These aren’t details about the tools or techniques, more the inner workings of the company. And they seem to primarily be hacking regionally. [...]
