Title: David Kahn
Date: 2024-02-02T20:06:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;books;cryptanalysis;history of cryptography
Slug: 2024-02-02-david-kahn

[Source](https://www.schneier.com/blog/archives/2024/02/david-kahn.html){:target="_blank" rel="noopener"}

> David Kahn has died. His groundbreaking book, The Codebreakers was the first serious book I read about codebreaking, and one of the primary reasons I entered this field. He will be missed. [...]
