Title: Joomla fixes XSS flaws that could expose sites to RCE attacks
Date: 2024-02-21T17:55:46-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-02-21-joomla-fixes-xss-flaws-that-could-expose-sites-to-rce-attacks

[Source](https://www.bleepingcomputer.com/news/security/joomla-fixes-xss-flaws-that-could-expose-sites-to-rce-attacks/){:target="_blank" rel="noopener"}

> Five vulnerabilities have been discovered in the Joomla content management system that could be leveraged to execute arbitrary code on vulnerable websites. [...]
