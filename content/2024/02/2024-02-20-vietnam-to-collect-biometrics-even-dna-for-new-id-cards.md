Title: Vietnam to collect biometrics - even DNA - for new ID cards
Date: 2024-02-20T04:58:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-02-20-vietnam-to-collect-biometrics-even-dna-for-new-id-cards

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/20/vietnam_id_cards_dna/){:target="_blank" rel="noopener"}

> Iris scan, voice samples and blood type to be included in database The Vietnamese government will begin collecting biometric information from its citizens for identification purposes beginning in July this year.... [...]
