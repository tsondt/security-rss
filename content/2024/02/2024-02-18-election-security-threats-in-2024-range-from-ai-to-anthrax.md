Title: Election security threats in 2024 range from AI to … anthrax?
Date: 2024-02-18T16:27:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-18-election-security-threats-in-2024-range-from-ai-to-anthrax

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/18/election_security_threats_2024/){:target="_blank" rel="noopener"}

> Unsettling reading as Presidents' Day approaches In time for the long Presidents' Day weekend in the US there have been multiple warnings about what will undoubtedly be a challenging and potentially dangerous year for voting processes and government workers.... [...]
