Title: US offers $15 million bounty for info on LockBit ransomware gang
Date: 2024-02-21T11:22:09-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-21-us-offers-15-million-bounty-for-info-on-lockbit-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/us-offers-15-million-bounty-for-info-on-lockbit-ransomware-gang/){:target="_blank" rel="noopener"}

> The U.S. State Department is now also offering rewards of up to $15 million to anyone who can provide information about LockBit ransomware gang members and their associates. [...]
