Title: LockBit ransomware gang has over $110 million in unspent bitcoin
Date: 2024-02-23T13:13:34-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-02-23-lockbit-ransomware-gang-has-over-110-million-in-unspent-bitcoin

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-gang-has-over-110-million-in-unspent-bitcoin/){:target="_blank" rel="noopener"}

> The LockBit ransomware gang received more than $125 million in ransom payments over the past 18 months, according to the analysis of hundreds of cryptocurrency wallets associated with the operation. [...]
