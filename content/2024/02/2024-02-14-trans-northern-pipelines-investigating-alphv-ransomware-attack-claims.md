Title: Trans-Northern Pipelines investigating ALPHV ransomware attack claims
Date: 2024-02-14T10:24:55-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-14-trans-northern-pipelines-investigating-alphv-ransomware-attack-claims

[Source](https://www.bleepingcomputer.com/news/security/trans-northern-pipelines-investigating-alphv-ransomware-attack-claims/){:target="_blank" rel="noopener"}

> Trans-Northern Pipelines (TNPI) has confirmed its internal network was breached in November 2023 and that it's now investigating claims of data theft made by the ALPHV/BlackCat ransomware gang. [...]
