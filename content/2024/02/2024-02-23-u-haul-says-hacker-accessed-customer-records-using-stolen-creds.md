Title: U-Haul says hacker accessed customer records using stolen creds
Date: 2024-02-23T09:16:37-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-02-23-u-haul-says-hacker-accessed-customer-records-using-stolen-creds

[Source](https://www.bleepingcomputer.com/news/security/u-haul-says-hacker-accessed-customer-records-using-stolen-creds/){:target="_blank" rel="noopener"}

> U-Haul has started informing customers that a hacker used stolen account credentials to access an internal system for dealers and team members to track customer reservations. [...]
