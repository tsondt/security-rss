Title: LockBit claims ransomware attack on Fulton County, Georgia
Date: 2024-02-14T18:07:39-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-02-14-lockbit-claims-ransomware-attack-on-fulton-county-georgia

[Source](https://www.bleepingcomputer.com/news/security/lockbit-claims-ransomware-attack-on-fulton-county-georgia/){:target="_blank" rel="noopener"}

> The LockBit ransomware gang claims to be behind the recent cyberattack on Fulton County, Georgia, and is threatening to publish "confidential" documents if a ransom is not paid. [...]
