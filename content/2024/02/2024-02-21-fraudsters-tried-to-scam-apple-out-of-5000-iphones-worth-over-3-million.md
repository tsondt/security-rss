Title: Fraudsters tried to scam Apple out of 5,000 iPhones worth over $3 million
Date: 2024-02-21T15:27:22-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-21-fraudsters-tried-to-scam-apple-out-of-5000-iphones-worth-over-3-million

[Source](https://www.bleepingcomputer.com/news/security/fraudsters-tried-to-scam-apple-out-of-5-000-iphones-worth-over-3-million/){:target="_blank" rel="noopener"}

> Two Chinese nationals face 20 years in prison after being caught and convicted of submitting over 5,000 fake iPhones worth more than $3 million to Apple with the goal of having them replaced with genuine devices. [...]
