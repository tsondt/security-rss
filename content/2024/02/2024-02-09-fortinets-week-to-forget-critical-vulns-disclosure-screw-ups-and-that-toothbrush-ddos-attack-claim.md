Title: Fortinet's week to forget: Critical vulns, disclosure screw-ups, and <em>that</em> toothbrush DDoS attack claim
Date: 2024-02-09T14:30:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-09-fortinets-week-to-forget-critical-vulns-disclosure-screw-ups-and-that-toothbrush-ddos-attack-claim

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/09/a_look_at_fortinet_week/){:target="_blank" rel="noopener"}

> An orchestra of fails for the security vendor We've had to write the word "Fortinet" so often lately that we're considering making a macro just to make our lives a little easier after what the company's reps will surely agree has been a week sent from hell.... [...]
