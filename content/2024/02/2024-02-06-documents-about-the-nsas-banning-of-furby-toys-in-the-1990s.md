Title: Documents about the NSA’s Banning of Furby Toys in the 1990s
Date: 2024-02-06T17:03:06+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;children;FOIA;NSA
Slug: 2024-02-06-documents-about-the-nsas-banning-of-furby-toys-in-the-1990s

[Source](https://www.schneier.com/blog/archives/2024/02/documents-about-the-nsas-banning-of-furby-toys-in-the-1990s.html){:target="_blank" rel="noopener"}

> Via a FOIA request, we have documents from the NSA about their banning of Furby toys. [...]
