Title: China could be doing better at censorship, think tank finds
Date: 2024-02-21T04:31:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-02-21-china-could-be-doing-better-at-censorship-think-tank-finds

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/21/china_censorship_underfunded/){:target="_blank" rel="noopener"}

> Complex overlapping bureaucracy sometimes lacks the funds and skills to do it right China's censorship regime remains pervasive and far reaching, but the bureaucratic apparatus implementing it is unevenly developed and is not always well funded, according to a report released on Tuesday.... [...]
