Title: North Korean hackers now launder stolen crypto via YoMix tumbler
Date: 2024-02-16T09:31:47-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-02-16-north-korean-hackers-now-launder-stolen-crypto-via-yomix-tumbler

[Source](https://www.bleepingcomputer.com/news/security/north-korean-hackers-now-launder-stolen-crypto-via-yomix-tumbler/){:target="_blank" rel="noopener"}

> The North Korean hacker collective Lazarus, infamous for having carried out numerous large-scale cryptocurrency heists over the years, has switched to using YoMix bitcoin mixer to launder stolen proceeds. [...]
