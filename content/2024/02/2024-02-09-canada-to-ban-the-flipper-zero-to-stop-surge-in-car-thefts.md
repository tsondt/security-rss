Title: Canada to ban the Flipper Zero to stop surge in car thefts
Date: 2024-02-09T14:16:07-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-09-canada-to-ban-the-flipper-zero-to-stop-surge-in-car-thefts

[Source](https://www.bleepingcomputer.com/news/security/canada-to-ban-the-flipper-zero-to-stop-surge-in-car-thefts/){:target="_blank" rel="noopener"}

> The Canadian government plans to ban the Flipper Zero and similar devices after tagging them as tools thieves can use to steal cars. [...]
