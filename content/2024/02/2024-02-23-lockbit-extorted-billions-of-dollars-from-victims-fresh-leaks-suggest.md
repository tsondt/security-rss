Title: LockBit extorted billions of dollars from victims, fresh leaks suggest
Date: 2024-02-23T22:30:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-23-lockbit-extorted-billions-of-dollars-from-victims-fresh-leaks-suggest

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/23/lockbit_extorted_billions_of_dollars/){:target="_blank" rel="noopener"}

> Investigating LockBit’s finances has blown previous estimates of the operation’s wealth out of the water Authorities digging into LockBit's finances believe the group may have generated more than $1 billion in ransom fees over its four-year lifespan.... [...]
