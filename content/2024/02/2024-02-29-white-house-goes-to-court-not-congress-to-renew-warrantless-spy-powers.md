Title: White House goes to court, not Congress, to renew warrantless spy powers
Date: 2024-02-29T21:44:15+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-29-white-house-goes-to-court-not-congress-to-renew-warrantless-spy-powers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/29/fisa_section_702_wyden/){:target="_blank" rel="noopener"}

> Choose your own FISA Section 702 adventure: End-run around lawmakers or business as usual? The Biden Administration has asked a court, rather than Congress, to renew controversial warrantless surveillance powers used by American intelligence and due to expire within weeks. It's a move that is either business as usual or an end-run around spying reforms, depending on who in Washington you believe.... [...]
