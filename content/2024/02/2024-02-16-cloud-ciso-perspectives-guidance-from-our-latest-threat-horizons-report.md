Title: Cloud CISO Perspectives: Guidance from our latest Threat Horizons report
Date: 2024-02-16T17:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2024-02-16-cloud-ciso-perspectives-guidance-from-our-latest-threat-horizons-report

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-guidance-from-our-latest-threat-horizons-report/){:target="_blank" rel="noopener"}

> Welcome to the first Cloud CISO Perspectives for February 2024. Today I’ll be looking at our latest Threat Horizons report, which provides a forward-thinking view of cloud security with intelligence on emerging threats and actionable recommendations from Google's security experts. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI Security & CISO, Google Cloud aside_block <ListValue: [StructValue([('title', 'Board of Directors Insights Hub'), ('body', <wagtail.rich_text.RichText object at 0x3ec471ad1670>), ('btn_text', [...]
