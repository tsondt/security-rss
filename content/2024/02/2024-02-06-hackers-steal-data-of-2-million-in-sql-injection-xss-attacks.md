Title: Hackers steal data of 2 million in SQL injection, XSS attacks
Date: 2024-02-06T02:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-02-06-hackers-steal-data-of-2-million-in-sql-injection-xss-attacks

[Source](https://www.bleepingcomputer.com/news/security/hackers-steal-data-of-2-million-in-sql-injection-xss-attacks/){:target="_blank" rel="noopener"}

> A threat group named 'ResumeLooters' has stolen the personal data of over two million job seekers after compromising 65 legitimate job listing and retail sites using SQL injection and cross-site scripting (XSS) attacks. [...]
