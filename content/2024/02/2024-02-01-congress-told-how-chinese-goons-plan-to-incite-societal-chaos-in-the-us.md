Title: Congress told how Chinese goons plan to incite 'societal chaos' in the US
Date: 2024-02-01T01:30:03+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-02-01-congress-told-how-chinese-goons-plan-to-incite-societal-chaos-in-the-us

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/01/china_attack_warning/){:target="_blank" rel="noopener"}

> American public is way ahead of them Chinese attackers are preparing to "wreak havoc" on American infrastructure and "cause societal chaos" in the US, infosec, and law enforcement bosses told a US House committee on Wednesday.... [...]
