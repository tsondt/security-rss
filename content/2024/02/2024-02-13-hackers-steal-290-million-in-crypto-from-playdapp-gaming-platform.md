Title: Hackers steal $290 million in crypto from PlayDapp gaming platform
Date: 2024-02-13T11:31:17-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency;Gaming
Slug: 2024-02-13-hackers-steal-290-million-in-crypto-from-playdapp-gaming-platform

[Source](https://www.bleepingcomputer.com/news/security/hackers-steal-290-million-in-crypto-from-playdapp-gaming-platform/){:target="_blank" rel="noopener"}

> Hackers are believed to have used a stolen private key to mint and steal over $290 million in PLA tokens, a cryptocurrency used within the PlayDapp ecosystem. [...]
