Title: Feds dismantle Russian GRU botnet built on 1,000-plus home, small biz routers
Date: 2024-02-15T21:11:29+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-15-feds-dismantle-russian-gru-botnet-built-on-1000-plus-home-small-biz-routers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/15/feds_go_fancy_bear_hunting/){:target="_blank" rel="noopener"}

> Beijing, now Moscow.... Who else is hiding in broadband gateways? The US government today said it disrupted a botnet that Russia's GRU military intelligence unit used for phishing expeditions, spying, credential harvesting, and data theft against American and foreign governments and other strategic targets.... [...]
