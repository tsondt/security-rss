Title: AnyDesk says hackers breached its production servers, reset passwords
Date: 2024-02-02T17:16:03-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-02-02-anydesk-says-hackers-breached-its-production-servers-reset-passwords

[Source](https://www.bleepingcomputer.com/news/security/anydesk-says-hackers-breached-its-production-servers-reset-passwords/){:target="_blank" rel="noopener"}

> AnyDesk confirmed today that it suffered a recent cyberattack that allowed hackers to gain access to the company's production systems. BleepingComputer has learned that source code and private code signing keys were stolen during the attack. [...]
