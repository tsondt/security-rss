Title: Rise of deepfake threats means biometric security measures won't be enough
Date: 2024-02-01T18:45:08+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2024-02-01-rise-of-deepfake-threats-means-biometric-security-measures-wont-be-enough

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/01/deepfake_threat_biometrics/){:target="_blank" rel="noopener"}

> Defenses need a rethink in face of increasing sophistication Cyber attacks using AI-generated deepfakes to bypass facial biometrics security will lead a third of organizations to doubt the adequacy of identity verification and authentication tools as standalone protections.... [...]
