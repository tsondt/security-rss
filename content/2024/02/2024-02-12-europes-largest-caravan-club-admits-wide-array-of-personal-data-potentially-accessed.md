Title: Europe's largest caravan club admits wide array of personal data potentially accessed
Date: 2024-02-12T12:45:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-12-europes-largest-caravan-club-admits-wide-array-of-personal-data-potentially-accessed

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/12/europes_largest_caravan_and_rv/){:target="_blank" rel="noopener"}

> Experts also put an end to social media security updates The Caravan and Motorhome Club (CAMC) and the experts it drafted to help clean up the mess caused by a January cyberattack still can't figure out whether members' data was stolen.... [...]
