Title: Ransomware gangs are paying attention to infostealers, so why aren't you?
Date: 2024-02-29T16:27:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-29-ransomware-gangs-are-paying-attention-to-infostealers-so-why-arent-you

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/29/infostealers_increased_use/){:target="_blank" rel="noopener"}

> Analysts warn of big leap in cred-harvesting malware activity last year There appears to be an uptick in interest among cybercriminals in infostealers – malware designed to swipe online account passwords, financial info, and other sensitive data from infected PCs – as a relatively cheap and easy way to get a foothold in organizations' IT environments to deploy devastating ransomware.... [...]
