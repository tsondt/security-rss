Title: DuckDuckGo browser gets end-to-end encrypted sync feature
Date: 2024-02-14T08:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2024-02-14-duckduckgo-browser-gets-end-to-end-encrypted-sync-feature

[Source](https://www.bleepingcomputer.com/news/security/duckduckgo-browser-gets-end-to-end-encrypted-sync-feature/){:target="_blank" rel="noopener"}

> The DuckDuckGo browser has unveiled a new end-to-end encrypted Sync & Backup feature that lets users privately and securely synchronize their bookmarks, passwords, and Email Protection settings across multiple devices. [...]
