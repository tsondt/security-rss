Title: Palo Alto investor sues over 28% share tumble
Date: 2024-02-28T17:00:11+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2024-02-28-palo-alto-investor-sues-over-28-share-tumble

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/28/palo_alto_class_action/){:target="_blank" rel="noopener"}

> Lawsuit alleges it misled investors with claims new AI products were 'facilitating greater platformization' and more Updated Palo Alto Networks (PAN) is facing a proposed class action lawsuit that alleges investors were deceived about the traction of its platform tactics and hurt by an unexpectedly low billings forecast that crashed the share price.... [...]
