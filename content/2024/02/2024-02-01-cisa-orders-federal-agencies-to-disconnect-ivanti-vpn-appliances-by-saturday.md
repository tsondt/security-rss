Title: CISA orders federal agencies to disconnect Ivanti VPN appliances by Saturday
Date: 2024-02-01T08:49:46-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-01-cisa-orders-federal-agencies-to-disconnect-ivanti-vpn-appliances-by-saturday

[Source](https://www.bleepingcomputer.com/news/security/cisa-orders-federal-agencies-to-disconnect-ivanti-vpn-appliances-by-saturday/){:target="_blank" rel="noopener"}

> CISA has ordered U.S. federal agencies to disconnect all Ivanti Connect Secure and Policy Secure VPN appliances vulnerable to multiple actively exploited bugs before Saturday. [...]
