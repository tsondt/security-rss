Title: SaaS access control using Amazon Verified Permissions with a per-tenant policy store
Date: 2024-02-13T17:26:52+00:00
Author: Manuel Heinkel
Category: AWS Security
Tags: Advanced (300);Amazon Verified Permissions;Best Practices;SaaS;Security, Identity, & Compliance;Technical How-to;ISV;Multitenancy;Security;Security Blog;Tenant isolation
Slug: 2024-02-13-saas-access-control-using-amazon-verified-permissions-with-a-per-tenant-policy-store

[Source](https://aws.amazon.com/blogs/security/saas-access-control-using-amazon-verified-permissions-with-a-per-tenant-policy-store/){:target="_blank" rel="noopener"}

> Access control is essential for multi-tenant software as a service (SaaS) applications. SaaS developers must manage permissions, fine-grained authorization, and isolation. In this post, we demonstrate how you can use Amazon Verified Permissions for access control in a multi-tenant document management SaaS application using a per-tenant policy store approach. We also describe how to enforce the [...]
