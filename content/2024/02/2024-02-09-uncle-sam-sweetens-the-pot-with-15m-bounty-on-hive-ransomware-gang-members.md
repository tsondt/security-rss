Title: Uncle Sam sweetens the pot with $15M bounty on Hive ransomware gang members
Date: 2024-02-09T02:57:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-09-uncle-sam-sweetens-the-pot-with-15m-bounty-on-hive-ransomware-gang-members

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/09/hive_leaders_bounty/){:target="_blank" rel="noopener"}

> Honor among thieves about to be put to the test The US government has placed an extra $5 million bounty on Hive ransomware gang members – its second such reward in a year. And it also comes a little over 11 months since the FBI said it had shut down the criminal organization's network.... [...]
