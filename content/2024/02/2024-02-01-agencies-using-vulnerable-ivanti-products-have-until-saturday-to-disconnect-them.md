Title: Agencies using vulnerable Ivanti products have until Saturday to disconnect them
Date: 2024-02-01T23:45:04+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;CISA;connect secure;ivanti;zerodays
Slug: 2024-02-01-agencies-using-vulnerable-ivanti-products-have-until-saturday-to-disconnect-them

[Source](https://arstechnica.com/?p=2000723){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Federal civilian agencies have until midnight Saturday morning to sever all network connections to Ivanti VPN software, which is currently under mass exploitation by multiple threat groups. The US Cybersecurity and Infrastructure Security Agency mandated the move on Wednesday after disclosing three critical vulnerabilities in recent weeks. Three weeks ago, Ivanti disclosed two critical vulnerabilities that it said threat actors were already actively exploiting. The attacks, the company said, targeted “a limited number of customers” using the company’s Connect Secure and Policy Secure VPN products. Security firm Volexity said on the same day that the vulnerabilities [...]
