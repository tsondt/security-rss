Title: Blackbaud settles with FTC after that IT breach exposed millions of people's info
Date: 2024-02-02T21:12:20+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-02-02-blackbaud-settles-with-ftc-after-that-it-breach-exposed-millions-of-peoples-info

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/02/ftc_blackbaud_settlement/){:target="_blank" rel="noopener"}

> Cloud software slinger admits no guilt, promises better basic security hygiene Blackbaud, which had data on millions of people stolen from it by one or more crooks, has promised to shore up its IT defenses in a proposed deal with the FTC.... [...]
