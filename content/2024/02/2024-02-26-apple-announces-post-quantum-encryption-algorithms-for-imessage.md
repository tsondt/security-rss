Title: Apple Announces Post-Quantum Encryption Algorithms for iMessage
Date: 2024-02-26T12:04:34+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;Apple;cryptanalysis;encryption;NIST;quantum computing;security standards
Slug: 2024-02-26-apple-announces-post-quantum-encryption-algorithms-for-imessage

[Source](https://www.schneier.com/blog/archives/2024/02/apple-announces-post-quantum-encryption-algorithms-for-imessage.html){:target="_blank" rel="noopener"}

> Apple announced PQ3, its post-quantum encryption standard based on the Kyber secure key-encapsulation protocol, one of the post-quantum algorithms selected by NIST in 2022. There’s a lot of detail in the Apple blog post, and more in Douglas Stabila’s security analysis. I am of two minds about this. On the one hand, it’s probably premature to switch to any particular post-quantum algorithms. The mathematics of cryptanalysis for these lattice and other systems is still rapidly evolving, and we’re likely to break more of them—and learn a lot in the process—over the coming few years. But if you’re going to make [...]
