Title: Dozens in Jordan targeted by authorities using NSO spyware, report finds
Date: 2024-02-01T09:00:47+00:00
Author: Stephanie Kirchgaessner in Washington
Category: The Guardian
Tags: Jordan;Israel;Surveillance;Espionage;Malware;Middle East and north Africa;Technology;World news;Data and computer security;Human rights;Privacy;Gaza
Slug: 2024-02-01-dozens-in-jordan-targeted-by-authorities-using-nso-spyware-report-finds

[Source](https://www.theguardian.com/world/2024/feb/01/jordan-targeting-lawyers-jouranlists-israeli-nso-spyware-report){:target="_blank" rel="noopener"}

> Findings suggest Jordan is relying on cyberweapon to quash dissent and its use is ‘staggeringly widespread’ About three dozen journalists, lawyers and human rights workers in Jordan have been targeted by authorities using powerful spyware made by Israel’s NSO Group amid a broad crackdown on press freedoms and political participation, according to a report by the advocacy group Access Now. The information suggests the Jordanian government has used the Israeli cyberweapon against members of civil society, including at least one American citizen living in Jordan, between 2019 and September 2023. Continue reading... [...]
