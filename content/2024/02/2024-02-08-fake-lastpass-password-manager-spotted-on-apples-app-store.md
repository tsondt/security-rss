Title: Fake LastPass password manager spotted on Apple’s App Store
Date: 2024-02-08T12:02:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple
Slug: 2024-02-08-fake-lastpass-password-manager-spotted-on-apples-app-store

[Source](https://www.bleepingcomputer.com/news/security/fake-lastpass-password-manager-spotted-on-apples-app-store/){:target="_blank" rel="noopener"}

> LastPass is warning that a fake copy of its app is being distributed on the Apple App Store, likely used as a phishing app to steal users' credentials. [...]
