Title: ExpressVPN bug has been leaking some DNS requests for years
Date: 2024-02-11T10:09:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2024-02-11-expressvpn-bug-has-been-leaking-some-dns-requests-for-years

[Source](https://www.bleepingcomputer.com/news/security/expressvpn-bug-has-been-leaking-some-dns-requests-for-years/){:target="_blank" rel="noopener"}

> ExpressVPN has removed the split tunneling feature from the latest version of its software after finding that a bug exposed the domains users were visiting to configured DNS servers. [...]
