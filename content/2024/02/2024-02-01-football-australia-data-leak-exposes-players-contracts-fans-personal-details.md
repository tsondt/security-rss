Title: Football Australia data leak exposes players’ contracts, fans’ personal details
Date: 2024-02-01T04:12:01+00:00
Author: Jack Snape
Category: The Guardian
Tags: Football Australia;Australia sport;Data and computer security;Cybercrime;Sport
Slug: 2024-02-01-football-australia-data-leak-exposes-players-contracts-fans-personal-details

[Source](https://www.theguardian.com/sport/2024/feb/01/football-australia-data-leak-breach-exposes-players-contracts-fans-personal-details){:target="_blank" rel="noopener"}

> Australian players, fans and FA customers potentially affected FA contacts the Information Commissioner over the breach Passports, player contracts and more have been available online for almost two years due to a Football Australia (FA) data breach which cyber security researchers say includes information on every Australian fan and customer of the governing body. Lithuanian group Cybernews detected the leak and informed the FA, allowing football officials to plug the hole before the issue was made public on Thursday morning. The FA made contact with the Office of the Australian Information Commissioner (OAIC) regarding a potential data breach late on [...]
