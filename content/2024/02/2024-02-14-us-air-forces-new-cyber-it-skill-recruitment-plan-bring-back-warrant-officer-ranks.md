Title: US Air Force's new cyber, IT skill recruitment plan: Bring back warrant officer ranks
Date: 2024-02-14T18:34:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-02-14-us-air-forces-new-cyber-it-skill-recruitment-plan-bring-back-warrant-officer-ranks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/14/us_air_forces_new_cyber/){:target="_blank" rel="noopener"}

> Officer pay, limited command duties and writing 'code for your country' Skilled IT professionals considering a career change have a new option, as the US Air Force is reintroducing warrant officer ranks exclusively "within the cyber and information technology professions."... [...]
