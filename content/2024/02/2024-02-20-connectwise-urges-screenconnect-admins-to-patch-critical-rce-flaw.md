Title: ConnectWise urges ScreenConnect admins to patch critical RCE flaw
Date: 2024-02-20T11:48:59-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-20-connectwise-urges-screenconnect-admins-to-patch-critical-rce-flaw

[Source](https://www.bleepingcomputer.com/news/security/connectwise-urges-screenconnect-admins-to-patch-critical-rce-flaw/){:target="_blank" rel="noopener"}

> ConnectWise warned customers to patch their ScreenConnect servers immediately against a maximum severity flaw that can be used in remote code execution (RCE) attacks. [...]
