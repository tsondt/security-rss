Title: Insomniac Games alerts employees hit by ransomware data breach
Date: 2024-02-23T13:56:54-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-23-insomniac-games-alerts-employees-hit-by-ransomware-data-breach

[Source](https://www.bleepingcomputer.com/news/security/insomniac-games-alerts-employees-hit-by-ransomware-data-breach/){:target="_blank" rel="noopener"}

> Sony subsidiary Insomniac Games is sending data breach notification letters to employees whose personal information was stolen and leaked online following a Rhysida ransomware attack in November. [...]
