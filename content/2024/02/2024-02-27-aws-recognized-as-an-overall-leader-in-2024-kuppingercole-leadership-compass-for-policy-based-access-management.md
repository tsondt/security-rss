Title: AWS recognized as an Overall Leader in 2024 KuppingerCole Leadership Compass for Policy Based Access Management
Date: 2024-02-27T14:07:14+00:00
Author: Julian Lovelock
Category: AWS Security
Tags: Amazon Verified Permissions;Announcements;Foundational (100);Security, Identity, & Compliance;authorization;Security Blog
Slug: 2024-02-27-aws-recognized-as-an-overall-leader-in-2024-kuppingercole-leadership-compass-for-policy-based-access-management

[Source](https://aws.amazon.com/blogs/security/aws-recognized-as-overall-leader-in-2023kuppingercole-leadership-compass/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) was recognized by KuppingerCole Analysts AG as an Overall Leader in the firm’s Leadership Compass report for Policy Based Access Management. The Leadership Compass report reveals Amazon Verified Permissions as an Overall Leader (as shown in Figure 1), a Product Leader for functional strength, and an Innovation Leader for open source [...]
