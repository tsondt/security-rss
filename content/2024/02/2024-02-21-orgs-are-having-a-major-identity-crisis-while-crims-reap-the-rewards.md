Title: Orgs are having a major identity crisis while crims reap the rewards
Date: 2024-02-21T08:15:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-21-orgs-are-having-a-major-identity-crisis-while-crims-reap-the-rewards

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/21/identity_related_cyber_threats/){:target="_blank" rel="noopener"}

> Hacking your way in is so 2022 – logging in is much easier Identity-related threats pose an increasing risk to those protecting networks because attackers – ranging from financially motivated crime gangs and nation-state backed crews – increasingly prefer to log in using stolen credentials instead of exploiting vulnerabilities or social engineering.... [...]
