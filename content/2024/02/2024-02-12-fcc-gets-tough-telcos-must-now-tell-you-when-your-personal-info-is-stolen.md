Title: FCC gets tough: Telcos must now tell you when your personal info is stolen
Date: 2024-02-12T18:45:15+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-02-12-fcc-gets-tough-telcos-must-now-tell-you-when-your-personal-info-is-stolen

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/12/fcc_gets_tough_on_telcos/){:target="_blank" rel="noopener"}

> Yep, cell carriers didn't have to do this before The FCC's updated reporting requirements mean telcos in America will have just seven days to officially disclose that a criminal has broken into their systems.... [...]
