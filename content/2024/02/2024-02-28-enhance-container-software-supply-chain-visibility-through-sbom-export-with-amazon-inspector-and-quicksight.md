Title: Enhance container software supply chain visibility through SBOM export with Amazon Inspector and QuickSight
Date: 2024-02-28T17:59:18+00:00
Author: Jason Ng
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;Amazon Athena;Amazon Elastic Container Registry;Amazon EventBridge;Amazon Inspector;Amazon QuickSight;Amazon S3;analytics;AWS Glue;AWS Lambda;Quicksight;Security;Security Blog
Slug: 2024-02-28-enhance-container-software-supply-chain-visibility-through-sbom-export-with-amazon-inspector-and-quicksight

[Source](https://aws.amazon.com/blogs/security/enhance-container-software-supply-chain-visibility-through-sbom-export-with-amazon-inspector-and-quicksight/){:target="_blank" rel="noopener"}

> In this post, I’ll show how you can export software bills of materials (SBOMs) for your containers by using an AWS native service, Amazon Inspector, and visualize the SBOMs through Amazon QuickSight, providing a single-pane-of-glass view of your organization’s software supply chain. The concept of a bill of materials (BOM) originated in the manufacturing industry [...]
