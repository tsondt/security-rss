Title: On Passkey Usability
Date: 2024-02-12T16:49:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;passwords;usability
Slug: 2024-02-12-on-passkey-usability

[Source](https://www.schneier.com/blog/archives/2024/02/on-passkey-usability.html){:target="_blank" rel="noopener"}

> Matt Burgess tries to only use passkeys. The results are mixed. [...]
