Title: FBI’s LockBit Takedown Postponed a Ticking Time Bomb in Fulton County, Ga.
Date: 2024-02-26T02:17:55+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Ransomware;AN-Security breach;CNN;Donald Trump;Europol;fbi;Fulton County hack;George Chidi;LockBit;LockBitSupp;National Crime Agency;VX-Underground;XSS
Slug: 2024-02-26-fbis-lockbit-takedown-postponed-a-ticking-time-bomb-in-fulton-county-ga

[Source](https://krebsonsecurity.com/2024/02/fbis-lockbit-takedown-postponed-a-ticking-time-bomb-in-fulton-county-ga/){:target="_blank" rel="noopener"}

> The FBI’s takedown of the LockBit ransomware group last week came as LockBit was preparing to release sensitive data stolen from government computer systems in Fulton County, Ga. But LockBit is now regrouping, and the gang says it will publish the stolen Fulton County data on March 2 unless paid a ransom. LockBit claims the cache includes documents tied to the county’s ongoing criminal prosecution of former President Trump, but court watchers say teaser documents published by the crime gang suggest a total leak of the Fulton County data could put lives at risk and jeopardize a number of other [...]
