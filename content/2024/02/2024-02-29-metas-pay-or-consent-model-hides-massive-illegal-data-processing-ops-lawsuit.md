Title: Meta's pay-or-consent model hides 'massive illegal data processing ops': lawsuit
Date: 2024-02-29T13:00:07+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2024-02-29-metas-pay-or-consent-model-hides-massive-illegal-data-processing-ops-lawsuit

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/29/meta_gdpr_complaints/){:target="_blank" rel="noopener"}

> GDPR claim alleges Facebook parent's 'commercial surveillance practices are fundamentally illegal' Consumer groups are filing legal complaints in the EU in a coordinated attempt to use data protection law to stop Meta from giving local users a "fake choice" between paying up and consenting to being profiled and tracked via data collection.... [...]
