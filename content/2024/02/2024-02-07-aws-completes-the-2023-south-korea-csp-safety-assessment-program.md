Title: AWS completes the 2023 South Korea CSP Safety Assessment Program
Date: 2024-02-07T20:07:51+00:00
Author: Andy Hsia
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;audit;Auditing;Certification;Compliance;Compliance reports;CSP;Korea;Security Blog
Slug: 2024-02-07-aws-completes-the-2023-south-korea-csp-safety-assessment-program

[Source](https://aws.amazon.com/blogs/security/aws-completes-the-2023-south-korea-csp-safety-assessment-program/){:target="_blank" rel="noopener"}

> We’re excited to announce that Amazon Web Services (AWS) has completed the 2023 South Korea Cloud Service Providers (CSP) Safety Assessment Program, also known as the Regulation on Supervision on Electronic Financial Transactions (RSEFT) Audit Program. The financial sector in South Korea is required to abide by a variety of cybersecurity standards and regulations. Key [...]
