Title: Ukrainian police arrest father and son in suspected LockBit affiliate double act
Date: 2024-02-22T15:30:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-22-ukrainian-police-arrest-father-and-son-in-suspected-lockbit-affiliate-double-act

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/22/ukrainian_police_arrest_father_and/){:target="_blank" rel="noopener"}

> If they did it, it gives new meaning to quality family time. Meanwhile, key LockBit leaders remain at large Today's edition of the week-long LockBit leaks reveals a father-son duo was apprehended in Ukraine as part of the series of takedown-related arrests this week.... [...]
