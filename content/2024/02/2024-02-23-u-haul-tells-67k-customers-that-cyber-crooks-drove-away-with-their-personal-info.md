Title: U-Haul tells 67K customers that cyber-crooks drove away with their personal info
Date: 2024-02-23T20:06:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-23-u-haul-tells-67k-customers-that-cyber-crooks-drove-away-with-their-personal-info

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/23/uhaul_data_breach/){:target="_blank" rel="noopener"}

> Thieves broke into IT system using stolen login U-Haul is alerting tens of thousands of folks that miscreants used stolen credentials to break into one of its systems and access customer records that contained some personal data.... [...]
