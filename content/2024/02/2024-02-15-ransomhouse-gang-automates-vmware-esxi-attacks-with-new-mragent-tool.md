Title: RansomHouse gang automates VMware ESXi attacks with new MrAgent tool
Date: 2024-02-15T13:52:24-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-02-15-ransomhouse-gang-automates-vmware-esxi-attacks-with-new-mragent-tool

[Source](https://www.bleepingcomputer.com/news/security/ransomhouse-gang-automates-vmware-esxi-attacks-with-new-mragent-tool/){:target="_blank" rel="noopener"}

> The RansomHouse ransomware operation has created a new tool named 'MrAgent' that automates the deployment of its data encrypter across multiple VMware ESXi hypervisors. [...]
