Title: Details of a Phone Scam
Date: 2024-02-21T12:08:56+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;CIA;fraud;scams;social engineering
Slug: 2024-02-21-details-of-a-phone-scam

[Source](https://www.schneier.com/blog/archives/2024/02/details-of-a-phone-scam.html){:target="_blank" rel="noopener"}

> First-person account of someone who fell for a scam, that started as a fake Amazon service rep and ended with a fake CIA agent, and lost $50,000 cash. And this is not a naive or stupid person. The details are fascinating. And if you think it couldn’t happen to you, think again. Given the right set of circumstances, it can. It happened to Cory Doctorow. [...]
