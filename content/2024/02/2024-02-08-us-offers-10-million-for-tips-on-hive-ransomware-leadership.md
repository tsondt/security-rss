Title: US offers $10 million for tips on Hive ransomware leadership
Date: 2024-02-08T12:59:42-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-08-us-offers-10-million-for-tips-on-hive-ransomware-leadership

[Source](https://www.bleepingcomputer.com/news/security/us-offers-10-million-for-tips-on-hive-ransomware-leadership/){:target="_blank" rel="noopener"}

> The U.S. State Department offers rewards of up to $10 million for information that could help locate, identify, or arrest members of the Hive ransomware gang. [...]
