Title: Double trouble for Fortinet as it issues critical FortiSIEM vulns
Date: 2024-02-06T13:30:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-06-double-trouble-for-fortinet-as-it-issues-critical-fortisiem-vulns

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/06/fortinet_fortisiem_vulns/){:target="_blank" rel="noopener"}

> Please stand by 73 hours for vendor response...* Updated Fortinet's FortiSIEM product is vulnerable to two maximum-severity security vulnerabilities that allow for remote code execution, or at least according to two freshly published CVEs.*... [...]
