Title: A Self-Enforcing Protocol to Solve Gerrymandering
Date: 2024-02-02T12:01:55+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;game theory
Slug: 2024-02-02-a-self-enforcing-protocol-to-solve-gerrymandering

[Source](https://www.schneier.com/blog/archives/2024/02/a-self-enforcing-protocol-to-solve-gerrymandering.html){:target="_blank" rel="noopener"}

> In 2009, I wrote : There are several ways two people can divide a piece of cake in half. One way is to find someone impartial to do it for them. This works, but it requires another person. Another way is for one person to divide the piece, and the other person to complain (to the police, a judge, or his parents) if he doesn’t think it’s fair. This also works, but still requires another person—­at least to resolve disputes. A third way is for one person to do the dividing, and for the other person to choose the half [...]
