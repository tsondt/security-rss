Title: New Image/Video Prompt Injection Attacks
Date: 2024-02-22T17:08:49+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;Google;LLM;video
Slug: 2024-02-22-new-imagevideo-prompt-injection-attacks

[Source](https://www.schneier.com/blog/archives/2024/02/new-image-video-prompt-injection-attacks.html){:target="_blank" rel="noopener"}

> Simon Willison has been playing with the video processing capabilities of the new Gemini Pro 1.5 model from Google, and it’s really impressive. Which means a lot of scary new video prompt injection attacks. And remember, given the current state of technology, prompt injection attacks are impossible to prevent in general. [...]
