Title: Friday Squid Blogging: A Penguin Named “Squid”
Date: 2024-02-09T22:09:30+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-02-09-friday-squid-blogging-a-penguin-named-squid

[Source](https://www.schneier.com/blog/archives/2024/02/friday-squid-blogging-a-penguin-named-squid.html){:target="_blank" rel="noopener"}

> Amusing story about a penguin named “Squid.” As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
