Title: Fat Patch Tuesday, February 2024 Edition
Date: 2024-02-13T22:28:48+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;Security Tools;Time to Patch;Adam Barnett;CVE-2024-21351;CVE-2024-21410;CVE-2024-21412;CVE-2024-21413;Immersive Labs;Kevin Breen;Microsoft Office;Patch Tuesday February 2024;Rapid7;Satnam Narang;Tenable;trend micro;Windows SmartScreen
Slug: 2024-02-13-fat-patch-tuesday-february-2024-edition

[Source](https://krebsonsecurity.com/2024/02/fat-patch-tuesday-february-2024-edition/){:target="_blank" rel="noopener"}

> Microsoft Corp. today pushed software updates to plug more than 70 security holes in its Windows operating systems and related products, including two zero-day vulnerabilities that are already being exploited in active attacks. Top of the heap on this Fat Patch Tuesday is CVE-2024-21412, a “security feature bypass” in the way Windows handles Internet Shortcut Files that Microsoft says is being targeted in active exploits. Redmond’s advisory for this bug says an attacker would need to convince or trick a user into opening a malicious shortcut file. Researchers at Trend Micro have tied the ongoing exploitation of CVE-2024-21412 to an [...]
