Title: BTC-e server admin indicted for laundering ransom payments, stolen crypto
Date: 2024-02-02T10:33:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;CryptoCurrency;Security
Slug: 2024-02-02-btc-e-server-admin-indicted-for-laundering-ransom-payments-stolen-crypto

[Source](https://www.bleepingcomputer.com/news/legal/btc-e-server-admin-indicted-for-laundering-ransom-payments-stolen-crypto/){:target="_blank" rel="noopener"}

> Aliaksandr Klimenka, a Belarusian and Cypriot national, has been indicted in the U.S. for his involvement in an international cybercrime money laundering operation. [...]
