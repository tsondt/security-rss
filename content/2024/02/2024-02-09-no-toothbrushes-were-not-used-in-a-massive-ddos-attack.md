Title: No, Toothbrushes Were Not Used in a Massive DDoS Attack
Date: 2024-02-09T18:10:57+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;botnets;denial of service;fake news;Internet of Things
Slug: 2024-02-09-no-toothbrushes-were-not-used-in-a-massive-ddos-attack

[Source](https://www.schneier.com/blog/archives/2024/02/no-toothbrushes-were-not-used-in-a-massive-ddos-attack.html){:target="_blank" rel="noopener"}

> The widely reported story last week that 1.5 million smart toothbrushes were hacked and used in a DDoS attack is false. Near as I can tell, a German reporter talking to someone at Fortinet got it wrong, and then everyone else ran with it without reading the German text. It was a hypothetical, which Fortinet eventually confirmed. Or maybe it was a stock-price hack. [...]
