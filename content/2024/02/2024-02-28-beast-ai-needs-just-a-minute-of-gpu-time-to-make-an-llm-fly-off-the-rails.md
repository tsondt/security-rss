Title: BEAST AI needs just a minute of GPU time to make an LLM fly off the rails
Date: 2024-02-28T23:08:50+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-02-28-beast-ai-needs-just-a-minute-of-gpu-time-to-make-an-llm-fly-off-the-rails

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/28/beast_llm_adversarial_prompt_injection_attack/){:target="_blank" rel="noopener"}

> Talk about gone in 60 seconds Computer scientists have developed an efficient way to craft prompts that elicit harmful responses from large language models (LLMs).... [...]
