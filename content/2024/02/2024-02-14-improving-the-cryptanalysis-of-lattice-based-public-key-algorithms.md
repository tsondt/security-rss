Title: Improving the Cryptanalysis of Lattice-Based Public-Key Algorithms
Date: 2024-02-14T12:08:03+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;cryptanalysis;cryptography;NIST;quantum computing
Slug: 2024-02-14-improving-the-cryptanalysis-of-lattice-based-public-key-algorithms

[Source](https://www.schneier.com/blog/archives/2024/02/improving-the-cryptanalysis-of-lattice-based-public-key-algorithms.html){:target="_blank" rel="noopener"}

> The winner of the Best Paper Award at Crypto this year was a significant improvement to lattice-based cryptanalysis. This is important, because a bunch of NIST’s post-quantum options base their security on lattice problems. I worry about standardizing on post-quantum algorithms too quickly. We are still learning a lot about the security of these systems, and this paper is an example of that learning. News story. [...]
