Title: ALPHV gang claims it's the attacker that broke into Prudential Financial, LoanDepot
Date: 2024-02-19T14:02:26+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-19-alphv-gang-claims-its-the-attacker-that-broke-into-prudential-financial-loandepot

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/19/alphv_claims_cyberattacks_on_prudential/){:target="_blank" rel="noopener"}

> Ransomware group continues to exploit US regulatory requirements to its advantage The ALPHV/BlackCat ransomware group is claiming responsibility for attacks on both Prudential Financial and LoanDepot, making a series of follow-on allegations against them.... [...]
