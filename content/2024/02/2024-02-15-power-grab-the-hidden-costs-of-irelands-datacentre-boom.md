Title: Power grab: the hidden costs of Ireland’s datacentre boom
Date: 2024-02-15T05:00:08+00:00
Author: Jessica Traynor
Category: The Guardian
Tags: Ireland;Data and computer security;Google;Technology;Facebook
Slug: 2024-02-15-power-grab-the-hidden-costs-of-irelands-datacentre-boom

[Source](https://www.theguardian.com/world/2024/feb/15/power-grab-hidden-costs-of-ireland-datacentre-boom){:target="_blank" rel="noopener"}

> Datacentres are part of Ireland’s vision of itself as a tech hub. There are now more than 80, using vast amounts of electricity. Have we entrusted our memories to a system that might destroy them? In the doldrum days between Christmas and New Year, we take a family trip to see a datacentre. Over the past two decades, datacentres have become a common sight on the outskirts of Dublin and many other Irish cities and towns. Situated in industrial business parks, they are easy to miss. But these buildings are critical to the maintenance of contemporary life: inside their walls [...]
