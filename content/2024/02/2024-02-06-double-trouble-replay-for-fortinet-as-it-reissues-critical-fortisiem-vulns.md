Title: Double trouble replay for Fortinet as it reissues critical FortiSIEM vulns
Date: 2024-02-06T13:30:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-06-double-trouble-replay-for-fortinet-as-it-reissues-critical-fortisiem-vulns

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/06/fortinet_fortisiem_vulns/){:target="_blank" rel="noopener"}

> So good it posted them twice Updated Fortinet's FortiSIEM product is vulnerable to two maximum-severity security vulnerabilities that allow for remote code execution, and it recently told the world about this again*.... [...]
