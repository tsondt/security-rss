Title: Identify Java nested dependencies with Amazon Inspector SBOM Generator
Date: 2024-02-12T17:55:15+00:00
Author: Chi Tran
Category: AWS Security
Tags: Amazon Inspector;Intermediate (200);Security, Identity, & Compliance;Technical How-to;log4j;Security Blog
Slug: 2024-02-12-identify-java-nested-dependencies-with-amazon-inspector-sbom-generator

[Source](https://aws.amazon.com/blogs/security/identify-java-nested-dependencies-with-amazon-inspector-sbom-generator/){:target="_blank" rel="noopener"}

> Amazon Inspector is an automated vulnerability management service that continually scans Amazon Web Services (AWS) workloads for software vulnerabilities and unintended network exposure. Amazon Inspector currently supports vulnerability reporting for Amazon Elastic Compute Cloud (Amazon EC2) instances, container images stored in Amazon Elastic Container Registry (Amazon ECR), and AWS Lambda. Java archive files (JAR, WAR, [...]
