Title: German battery maker Varta halts production after cyberattack
Date: 2024-02-14T12:02:37-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-02-14-german-battery-maker-varta-halts-production-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/german-battery-maker-varta-halts-production-after-cyberattack/){:target="_blank" rel="noopener"}

> Battery maker VARTA AG announced yesterday that it was targeted by a cyberattack that forced it to shut down IT systems, causing production to stop at its plants. [...]
