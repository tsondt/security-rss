Title: Cactus ransomware claim to steal 1.5TB of Schneider Electric data
Date: 2024-02-19T14:35:58-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-19-cactus-ransomware-claim-to-steal-15tb-of-schneider-electric-data

[Source](https://www.bleepingcomputer.com/news/security/cactus-ransomware-claim-to-steal-15tb-of-schneider-electric-data/){:target="_blank" rel="noopener"}

> The Cactus ransomware gang claims they stole 1.5TB of data from Schneider Electric after breaching the company's network last month. [...]
