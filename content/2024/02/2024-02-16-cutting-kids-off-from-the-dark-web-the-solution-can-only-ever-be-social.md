Title: Cutting kids off from the dark web – the solution can only ever be social
Date: 2024-02-16T12:01:05+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-16-cutting-kids-off-from-the-dark-web-the-solution-can-only-ever-be-social

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/16/dark_web_kids_limit_uk/){:target="_blank" rel="noopener"}

> Expert weighs in after Brianna Ghey murder amid worrying rates of child cybercrime The murder of 16-year-old schoolgirl Brianna Ghey has kickstarted a debate around limiting children's access to the dark web in the UK, with experts highlighting the difficulty in achieving this.... [...]
