Title: Insider steals 79,000 email addresses at work to promote own business
Date: 2024-02-20T11:01:32+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-20-insider-steals-79000-email-addresses-at-work-to-promote-own-business

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/20/insider_steals_79000_email_addresses/){:target="_blank" rel="noopener"}

> After saying they're very sorry, they escape with a slap on the wrist A former council staff member in the district where William Shakespeare was born ransacked databases filled with residents' information to help drum up new business for their outside venture.... [...]
