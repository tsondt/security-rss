Title: New Leak Shows Business Side of China’s APT Menace
Date: 2024-02-22T13:27:47+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Ne'er-Do-Well News;Citizen Lab;Dakota Cary;i-SOON;Mei Danowski;Poison Carp;SentinelOne;U.S. Department of Justice;Will Thomas;Wu Haibo
Slug: 2024-02-22-new-leak-shows-business-side-of-chinas-apt-menace

[Source](https://krebsonsecurity.com/2024/02/new-leak-shows-business-side-of-chinas-apt-menace/){:target="_blank" rel="noopener"}

> A new data leak that appears to have come from one of China’s top private cybersecurity firms provides a rare glimpse into the commercial side of China’s many state-sponsored hacking groups. Experts say the leak illustrates how Chinese government agencies increasingly are contracting out foreign espionage campaigns to the nation’s burgeoning and highly competitive cybersecurity industry. A marketing slide deck promoting i-SOON’s Advanced Persistent Threat (APT) capabilities. A large cache of more than 500 documents published to GitHub last week indicate the records come from i-SOON, a technology company headquartered in Shanghai that is perhaps best known for providing cybersecurity [...]
