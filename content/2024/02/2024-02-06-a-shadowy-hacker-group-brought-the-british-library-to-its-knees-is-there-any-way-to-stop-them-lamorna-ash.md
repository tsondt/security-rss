Title: A shadowy hacker group brought the British Library to its knees. Is there any way to stop them? | Lamorna Ash
Date: 2024-02-06T12:13:10+00:00
Author: Lamorna Ash
Category: The Guardian
Tags: Cybercrime;British Library;Internet;Technology;Hacking;Data and computer security;Russia;Libraries;Europe;UK news
Slug: 2024-02-06-a-shadowy-hacker-group-brought-the-british-library-to-its-knees-is-there-any-way-to-stop-them-lamorna-ash

[Source](https://www.theguardian.com/commentisfree/2024/feb/06/hacker-british-library-cybersecurity-cybercrime-uk){:target="_blank" rel="noopener"}

> The future of cybercrime resembles an arms race between an industry of hackers-for-hire and the UK’s weak defences It is not quite accurate to say that the cyber-attack against the British Library took place on 28 October 2023. Most probably, Rhysida, the hacker gang that orchestrated the attack and is thought to be Russian, had already been creeping undetected through the digital territories of the British Library for months, Enrico Mariconti, a lecturer in security and crime science at UCL, told me. Once it broke through to the library’s virtual private network (VPN) – the remote connection that allows employees [...]
