Title: Hackers got nearly 7 million people’s data from 23andMe. The firm blamed users in ‘very dumb’ move
Date: 2024-02-15T14:26:22+00:00
Author: Mack DeGeurin
Category: The Guardian
Tags: Hacking;Technology;Genetics;Data and computer security;Privacy
Slug: 2024-02-15-hackers-got-nearly-7-million-peoples-data-from-23andme-the-firm-blamed-users-in-very-dumb-move

[Source](https://www.theguardian.com/technology/2024/feb/15/23andme-hack-data-genetic-data-selling-response){:target="_blank" rel="noopener"}

> The company pointed at people who ‘failed to update their passwords’ as sensitive data was offered for sale on forums Three years ago, a man in Florida named JL decided, on a whim, to send a tube of his spit to the genetic testing site 23andMe in exchange for an ancestry report. JL, like millions of other 23andMe participants before him, says he was often asked about his ethnicity and craved a deeper insight into his identity. He said he was surprised by the diversity of his test results, which showed he had some Ashkenazi Jewish heritage. JL said he [...]
