Title: US says China's Volt Typhoon is readying destructive cyberattacks
Date: 2024-02-07T19:11:00+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-07-us-says-chinas-volt-typhoon-is-readying-destructive-cyberattacks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/07/us_chinas_volt_typhoon_attacks/){:target="_blank" rel="noopener"}

> 12 international govt agencies sound the alarm, critical infrastructure at the heart of threats The US government today confirmed China's Volt Typhoon crew comprised "multiple" critical infrastructure orgs' IT networks in America – and Uncle Sam warned that the Beijing-backed spies are readying "disruptive or destructive cyberattacks" against those targets.... [...]
