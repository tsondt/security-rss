Title: Korean eggheads crack Rhysida ransomware and release free decryptor tool
Date: 2024-02-13T01:47:03+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-13-korean-eggheads-crack-rhysida-ransomware-and-release-free-decryptor-tool

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/13/rhysida_ransomware_decrypted/){:target="_blank" rel="noopener"}

> Great news for victims of gang behind the big British Library hit in October Some smart folks have found a way to automatically unscramble documents encrypted by the Rhysida ransomware, and used that know-how to produce and release a handy recovery tool for victims.... [...]
