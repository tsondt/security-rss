Title: Cybercrims: When we hit IT, they sometimes pay, but when we hit OT... jackpot
Date: 2024-02-27T09:30:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-27-cybercrims-when-we-hit-it-they-sometimes-pay-but-when-we-hit-ot-jackpot

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/27/manufacturing_sector_malware/){:target="_blank" rel="noopener"}

> Or so says opsec firm, which confirms 70% of all industrial org ransomware in 2023 targeted manufacturers Analysis Cybercriminals follow the money, and increasingly last year that led them to ransomware attacks against the manufacturing industry.... [...]
