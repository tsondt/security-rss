Title: Integris Health says data breach impacts 2.4 million patients
Date: 2024-02-13T14:28:02-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-02-13-integris-health-says-data-breach-impacts-24-million-patients

[Source](https://www.bleepingcomputer.com/news/security/integris-health-says-data-breach-impacts-24-million-patients/){:target="_blank" rel="noopener"}

> Integris Health has reported to U.S. authorities that the data breach it suffered last November exposed personal information belonging to almost 2.4 million people. [...]
