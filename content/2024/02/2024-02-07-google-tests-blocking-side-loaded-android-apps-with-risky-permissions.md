Title: Google tests blocking side-loaded Android apps with risky permissions
Date: 2024-02-07T13:57:34-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2024-02-07-google-tests-blocking-side-loaded-android-apps-with-risky-permissions

[Source](https://www.bleepingcomputer.com/news/security/google-tests-blocking-side-loaded-android-apps-with-risky-permissions/){:target="_blank" rel="noopener"}

> Google has launched a new pilot program to fight financial fraud by blocking the sideloading of Android APK files that request access to risky permissions. [...]
