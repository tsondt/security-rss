Title: Crooks hook hundreds of exec accounts after phishing in Azure C-suite pond
Date: 2024-02-13T14:20:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-13-crooks-hook-hundreds-of-exec-accounts-after-phishing-in-azure-c-suite-pond

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/13/exec_accounts_phishing_campaign/){:target="_blank" rel="noopener"}

> Plenty of successful attacks observed with dangerous follow-on activity The number of senior business executives stymied by an ongoing phishing campaign continues to rise with cybercriminals registering hundreds of cloud account takeovers (ATOs) since spinning it up in November.... [...]
