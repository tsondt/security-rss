Title: Cloud CISO Perspectives: Building better cyber defenses with AI
Date: 2024-02-28T17:00:00+00:00
Author: Charley Snyder
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2024-02-28-cloud-ciso-perspectives-building-better-cyber-defenses-with-ai

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-building-better-cyber-defenses-with-ai/){:target="_blank" rel="noopener"}

> Welcome to the second Cloud CISO Perspectives for February 2024. Today, Charley Snyder, a Google expert on the intersection of security and public policy, talks about our announcements at February’s Munich Security Conference — and why the conference plays a vital role in public policy conversations. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI Security & CISO, Google Cloud aside_block <ListValue: [StructValue([('title', 'Get vital CISO Insights with [...]
