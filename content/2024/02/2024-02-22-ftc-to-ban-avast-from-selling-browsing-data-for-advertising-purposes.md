Title: FTC to ban Avast from selling browsing data for advertising purposes
Date: 2024-02-22T11:48:24-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-22-ftc-to-ban-avast-from-selling-browsing-data-for-advertising-purposes

[Source](https://www.bleepingcomputer.com/news/security/ftc-to-ban-avast-from-selling-browsing-data-for-advertising-purposes/){:target="_blank" rel="noopener"}

> The U.S. Federal Trade Commission (FTC) will order Avast to pay $16.5 million and ban the company from selling the users' web browsing data or licensing it for advertising purposes. [...]
