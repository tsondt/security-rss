Title: From Cybercrime Saul Goodman to the Russian GRU
Date: 2024-02-07T17:10:18+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Russia's War on Ukraine;Aleksei Valerievich Safronov;Congressional Research Service;Constella Intelligence;Djamix;DomainTools.com;GRU;Guardia Civil;mark rasch;Mazafaka;Meduza;Stalker
Slug: 2024-02-07-from-cybercrime-saul-goodman-to-the-russian-gru

[Source](https://krebsonsecurity.com/2024/02/from-cybercrime-saul-goodman-to-the-russian-gru/){:target="_blank" rel="noopener"}

> In 2021, the exclusive Russian cybercrime forum Mazafaka was hacked. The leaked user database shows one of the forum’s founders was an attorney who advised Russia’s top hackers on the legal risks of their work, and what to do if they got caught. A review of this user’s hacker identities shows that during his time on the forums he served as an officer in the special forces of the GRU, the foreign military intelligence agency of the Russian Federation. Launched in 2001 under the tagline “Network terrorism,” Mazafaka would evolve into one of the most guarded Russian-language cybercrime communities. The [...]
