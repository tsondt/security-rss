Title: Volt Typhoon not the only Chinese crew lurking in US energy, critical networks
Date: 2024-02-07T22:50:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-07-volt-typhoon-not-the-only-chinese-crew-lurking-in-us-energy-critical-networks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/07/its_not_just_volt_typhoon/){:target="_blank" rel="noopener"}

> Presumably American TLAs are all over Beijing's infrastructure, too... right? Volt Typhoon isn't the only Chinese spying crew infiltrating computer networks in America's energy sector and other critical organizations with the aim of wrecking equipment and causing other headaches, the US government has said.... [...]
