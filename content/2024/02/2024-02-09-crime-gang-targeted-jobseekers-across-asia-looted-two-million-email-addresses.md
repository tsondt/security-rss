Title: Crime gang targeted jobseekers across Asia, looted two million email addresses
Date: 2024-02-09T04:03:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-02-09-crime-gang-targeted-jobseekers-across-asia-looted-two-million-email-addresses

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/09/resume_looters_jobs_ads_malicious_code/){:target="_blank" rel="noopener"}

> That listing for a gig that looked too good to be true may have been carrying SQL injection code Singapore-based infosec firm Group-IB has detected a group that spent the last two months of 2023 stealing personal info from websites operated by jobs boards and retailers websites across Asia.... [...]
