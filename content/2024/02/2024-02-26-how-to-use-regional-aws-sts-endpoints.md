Title: How to use Regional AWS STS endpoints
Date: 2024-02-26T18:09:35+00:00
Author: Darius Januskis
Category: AWS Security
Tags: AWS Identity and Access Management (IAM);AWS Security Token Service;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security Blog;Security token service;STS
Slug: 2024-02-26-how-to-use-regional-aws-sts-endpoints

[Source](https://aws.amazon.com/blogs/security/how-to-use-regional-aws-sts-endpoints/){:target="_blank" rel="noopener"}

> This blog post provides recommendations that you can use to help improve resiliency in the unlikely event of disrupted availability of the global (now legacy) AWS Security Token Service (AWS STS) endpoint. Although the global (legacy) AWS STS endpoint https://sts.amazonaws.com is highly available, it’s hosted in a single AWS Region—US East (N. Virginia)—and like other [...]
