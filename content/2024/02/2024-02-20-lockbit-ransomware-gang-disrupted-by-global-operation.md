Title: LockBit ransomware gang disrupted by global operation
Date: 2024-02-20T01:17:58+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-02-20-lockbit-ransomware-gang-disrupted-by-global-operation

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/20/lockbit_down_operation_cronos/){:target="_blank" rel="noopener"}

> Website has been seized and replaced with law enforcement logos from eleven nations Updated Notorious ransomware gang LockBit's website has been taken over by law enforcement authorities, who claim they have disrupted the group's operations and will soon reveal the extent of an operation against the group.... [...]
