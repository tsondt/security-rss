Title: Critical Cisco bug exposes Expressway gateways to CSRF attacks
Date: 2024-02-07T13:22:29-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-07-critical-cisco-bug-exposes-expressway-gateways-to-csrf-attacks

[Source](https://www.bleepingcomputer.com/news/security/critical-cisco-bug-exposes-expressway-gateways-to-csrf-attacks/){:target="_blank" rel="noopener"}

> Cisco has patched several vulnerabilities affecting its Expressway Series collaboration gateways, two of them rated as critical severity and exposing vulnerable devices to cross-site request forgery (CSRF) attacks. [...]
