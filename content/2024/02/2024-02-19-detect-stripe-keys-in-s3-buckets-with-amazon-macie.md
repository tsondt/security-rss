Title: Detect Stripe keys in S3 buckets with Amazon Macie
Date: 2024-02-19T18:58:35+00:00
Author: Koulick Ghosh
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Technical How-to;Amazon Macie;Amazon S3;credentials;Keys;PCI;PII;Security Blog;Sensitive Data Discovery
Slug: 2024-02-19-detect-stripe-keys-in-s3-buckets-with-amazon-macie

[Source](https://aws.amazon.com/blogs/security/detect-stripe-keys-in-s3-buckets-with-amazon-macie/){:target="_blank" rel="noopener"}

> Many customers building applications on Amazon Web Services (AWS) use Stripe global payment services to help get their product out faster and grow revenue, especially in the internet economy. It’s critical for customers to securely and properly handle the credentials used to authenticate with Stripe services. Much like your AWS API keys, which enable access [...]
