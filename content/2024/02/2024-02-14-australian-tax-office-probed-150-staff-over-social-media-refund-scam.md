Title: Australian Tax Office probed 150 staff over social media refund scam
Date: 2024-02-14T04:45:15+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-02-14-australian-tax-office-probed-150-staff-over-social-media-refund-scam

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/14/ato_operation_protego_tax_scam/){:target="_blank" rel="noopener"}

> $1.3 billion lost as identity fraud – and greed – saw 57,000 or more seek unearned tax refunds One hundred and fifty people who worked for the Australian Taxation Office (ATO) have been investigated – and some prosecuted – for participating in a tax refund scam promoted on Facebook and TikTok.... [...]
