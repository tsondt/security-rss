Title: Molly White Reviews Blockchain Book
Date: 2024-02-13T12:07:03+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;blockchain;books
Slug: 2024-02-13-molly-white-reviews-blockchain-book

[Source](https://www.schneier.com/blog/archives/2024/02/molly-white-reviews-blockchain-book.html){:target="_blank" rel="noopener"}

> Molly White—of “ Web3 is Going Just Great ” fame— reviews Chris Dixon’s blockchain solutions book: Read Write Own : In fact, throughout the entire book, Dixon fails to identify a single blockchain project that has successfully provided a non-speculative service at any kind of scale. The closest he ever comes is when he speaks of how “for decades, technologists have dreamed of building a grassroots internet access provider”. He describes one project that “got further than anyone else”: Helium. He’s right, as long as you ignore the fact that Helium was providing LoRaWAN, not Internet, that by the time [...]
