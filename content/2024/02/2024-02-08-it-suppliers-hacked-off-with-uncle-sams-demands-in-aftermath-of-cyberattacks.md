Title: IT suppliers hacked off with Uncle Sam's demands in aftermath of cyberattacks
Date: 2024-02-08T00:06:00+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-02-08-it-suppliers-hacked-off-with-uncle-sams-demands-in-aftermath-of-cyberattacks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/08/us_tech_industry_changes/){:target="_blank" rel="noopener"}

> Plan says to hand over keys to networks – and report intrusions within eight hours of discovery Organizations that sell IT services to Uncle Sam are peeved at proposed changes to procurement rules that would require them to allow US government agencies full access to their systems in the event of a security incident.... [...]
