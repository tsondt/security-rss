Title: AnyDesk revokes signing certs, portal passwords after crooks sneak into systems
Date: 2024-02-05T18:30:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-02-05-anydesk-revokes-signing-certs-portal-passwords-after-crooks-sneak-into-systems

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/05/anydesk_breach/){:target="_blank" rel="noopener"}

> Horse, meet stable door AnyDesk has copped to an IT security "incident" in which criminals broke into the remote-desktop software maker's production systems. The biz has told customers to expect disruption as it attempts to lock down its infrastructure.... [...]
