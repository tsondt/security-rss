Title: Anycubic 3D printers hacked worldwide to expose security flaw
Date: 2024-02-28T18:06:15-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Technology
Slug: 2024-02-28-anycubic-3d-printers-hacked-worldwide-to-expose-security-flaw

[Source](https://www.bleepingcomputer.com/news/security/anycubic-3d-printers-hacked-worldwide-to-expose-security-flaw/){:target="_blank" rel="noopener"}

> According to a wave of online reports from Anycubic customers, someone hacked their 3D printers to warn that the devices are exposed to attacks. [...]
