Title: Russia-based LockBit ransomware hackers attempt comeback
Date: 2024-02-26T16:34:23+00:00
Author: Dan Milmo Global business editor
Category: The Guardian
Tags: Cybercrime;Internet;Technology;UK news;Business;NCA (National Crime Agency);Crime;US news;Hacking;Malware;World news;Data and computer security
Slug: 2024-02-26-russia-based-lockbit-ransomware-hackers-attempt-comeback

[Source](https://www.theguardian.com/technology/2024/feb/26/russian-based-lockbit-ransomware-hackers-attempt-comeback){:target="_blank" rel="noopener"}

> Gang sets up new site on dark web and releases rambling statement explaining how it was infiltrated by law enforcement agencies The LockBit ransomware gang is attempting a comeback days after its operations were severely disrupted by a coordinated international crackdown. The Russia-based group has set up a new site on the dark web to advertise a small number of alleged victims and leak stolen data, as well as releasing a rambling statement explaining how it had been hobbled by the UK’s National Crime Agency, the FBI, Europol and other police agencies in an operation last week. Continue reading... [...]
