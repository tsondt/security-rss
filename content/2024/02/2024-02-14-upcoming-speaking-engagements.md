Title: Upcoming Speaking Engagements
Date: 2024-02-14T17:01:13+00:00
Author: B. Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2024-02-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2024/02/upcoming-speaking-engagements-34.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking at the Munich Security Conference (MSC) 2024 in Munich, Germany, on Friday, February 16, 2024. I’m giving a keynote at a symposium on “AI and Trust” at Generative AI, Free Speech, & Public Discourse. The symposium will be held at Columbia University in New York City and online, on Tuesday, February 20, 2024. The list is maintained on this page. [...]
