Title: Cybercrime duo accused of picking $2.5M from Apple's orchard
Date: 2024-02-08T14:00:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-08-cybercrime-duo-accused-of-picking-25m-from-apples-orchard

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/08/security_research_apple_fraud/){:target="_blank" rel="noopener"}

> Security researcher buddies allegedly tag team a four-month virtual gift card heist at Cupertino tech giant A cybersecurity researcher and his pal are facing charges in California after they allegedly defrauded an unnamed company, almost certainly Apple, out of $2.5 million.... [...]
