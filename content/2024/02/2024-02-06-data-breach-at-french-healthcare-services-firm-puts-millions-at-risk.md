Title: Data breach at French healthcare services firm puts millions at risk
Date: 2024-02-06T13:36:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-02-06-data-breach-at-french-healthcare-services-firm-puts-millions-at-risk

[Source](https://www.bleepingcomputer.com/news/security/data-breach-at-french-healthcare-services-firm-puts-millions-at-risk/){:target="_blank" rel="noopener"}

> French healthcare services firm Viamedis suffered a cyberattack that exposed the data of policyholders and healthcare professionals in the country. [...]
