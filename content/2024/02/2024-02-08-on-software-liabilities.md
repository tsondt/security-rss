Title: On Software Liabilities
Date: 2024-02-08T12:00:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;cybersecurity;software liability;vulnerabilities
Slug: 2024-02-08-on-software-liabilities

[Source](https://www.schneier.com/blog/archives/2024/02/on-software-liabilities.html){:target="_blank" rel="noopener"}

> Over on Lawfare, Jim Dempsey published a really interesting proposal for software liability: “Standard for Software Liability: Focus on the Product for Liability, Focus on the Process for Safe Harbor.” Section 1 of this paper sets the stage by briefly describing the problem to be solved. Section 2 canvasses the different fields of law (warranty, negligence, products liability, and certification) that could provide a starting point for what would have to be legislative action establishing a system of software liability. The conclusion is that all of these fields would face the same question: How buggy is too buggy? Section 3 [...]
