Title: The spyware business is booming despite government crackdowns
Date: 2024-02-07T08:31:05+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-07-the-spyware-business-is-booming-despite-government-crackdowns

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/07/spyware_business_booming/){:target="_blank" rel="noopener"}

> 'Almost zero data being shared across the industry on this particular threat,' we're told Updated The commercial spyware economy – despite government and big tech's efforts to crack down – appears to be booming.... [...]
