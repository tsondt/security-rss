Title: Biden will veto attempts to kill off SEC's security breach reporting rules
Date: 2024-02-01T17:15:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-02-01-biden-will-veto-attempts-to-kill-off-secs-security-breach-reporting-rules

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/01/senate_resolution_to_undo_sec/){:target="_blank" rel="noopener"}

> Senate, House can try but won't make it past the Prez, says White House The Biden administration has expressed to congressional representatives its strong opposition to undoing the Securities and Exchange Commission's (SEC) strict data breach reporting rule.... [...]
