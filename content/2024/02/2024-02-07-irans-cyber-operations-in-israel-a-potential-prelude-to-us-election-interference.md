Title: Iran's cyber operations in Israel a potential prelude to US election interference
Date: 2024-02-07T16:30:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-07-irans-cyber-operations-in-israel-a-potential-prelude-to-us-election-interference

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/07/irans_cyber_operations_in_israel/){:target="_blank" rel="noopener"}

> Tactics are more sophisticated and supported in greater numbers Iran's anti-Israel cyber operations are providing a window into the techniques the country may deploy in the run-up to the 2024 US Presidential elections, Microsoft says.... [...]
