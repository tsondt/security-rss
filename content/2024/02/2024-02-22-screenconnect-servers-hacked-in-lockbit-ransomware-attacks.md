Title: ScreenConnect servers hacked in LockBit ransomware attacks
Date: 2024-02-22T13:34:58-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-22-screenconnect-servers-hacked-in-lockbit-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/screenconnect-servers-hacked-in-lockbit-ransomware-attacks/){:target="_blank" rel="noopener"}

> Attackers are exploiting a maximum severity authentication bypass vulnerability to breach unpatched ScreenConnect servers and deploy LockBit ransomware payloads on compromised networks. [...]
