Title: Friday Squid Blogging: Illex Squid and Climate Change
Date: 2024-02-23T22:04:28+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-02-23-friday-squid-blogging-illex-squid-and-climate-change

[Source](https://www.schneier.com/blog/archives/2024/02/friday-squid-blogging-illex-squid-and-climate-change.html){:target="_blank" rel="noopener"}

> There are correlations between the populations of the Illex Argentines squid and water temperatures. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
