Title: FTC orders Blackbaud to boost security after massive data breach
Date: 2024-02-01T17:23:56-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-01-ftc-orders-blackbaud-to-boost-security-after-massive-data-breach

[Source](https://www.bleepingcomputer.com/news/security/ftc-orders-blackbaud-to-boost-security-after-massive-data-breach/){:target="_blank" rel="noopener"}

> Blackbaud has settled with the Federal Trade Commission after being charged with poor security and reckless data retention practices, leading to a May 2020 ransomware attack and a data breach affecting millions of people. [...]
