Title: Police arrest LockBit ransomware members, release decryptor in global crackdown
Date: 2024-02-20T06:30:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-20-police-arrest-lockbit-ransomware-members-release-decryptor-in-global-crackdown

[Source](https://www.bleepingcomputer.com/news/security/police-arrest-lockbit-ransomware-members-release-decryptor-in-global-crackdown/){:target="_blank" rel="noopener"}

> Law enforcement arrested two operators of the LockBit ransomware gang in Poland and Ukraine, created a decryption tool to recover encrypted files for free, and seized over 200 crypto-wallets after hacking the cybercrime gang's servers in an international crackdown operation. [...]
