Title: India to make its digital currency programmable
Date: 2024-02-09T05:15:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-02-09-india-to-make-its-digital-currency-programmable

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/09/india_programmable_money/){:target="_blank" rel="noopener"}

> Reserve Bank also wants a national 2FA framework The Reserve Bank of India (RBI) announced on Thursday it would make its digital currency programmable, and ensure it can be exchanged when citizens are offline.... [...]
