Title: Pentagon launches nuke-spotting satellites amid Russian space bomb rumors
Date: 2024-02-15T20:12:42+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-02-15-pentagon-launches-nuke-spotting-satellites-amid-russian-space-bomb-rumors

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/15/pentagon_missile_satellite_network/){:target="_blank" rel="noopener"}

> Dungeons and Dragons, high-waisted jeans, Cold War sabre rattling – the '80s are back, baby Updated Last night's launch of six Pentagon missile-detection satellites was well timed as fears mount that Russia is considering putting nuclear weapons into space.... [...]
