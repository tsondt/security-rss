Title: VMware urges admins to remove deprecated, vulnerable auth plug-in
Date: 2024-02-20T16:00:16-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-20-vmware-urges-admins-to-remove-deprecated-vulnerable-auth-plug-in

[Source](https://www.bleepingcomputer.com/news/security/vmware-urges-admins-to-remove-deprecated-vulnerable-auth-plug-in/){:target="_blank" rel="noopener"}

> VMware urged admins today to remove a discontinued authentication plugin exposed to authentication relay and session hijack attacks in Windows domain environments via two security vulnerabilities left unpatched. [...]
