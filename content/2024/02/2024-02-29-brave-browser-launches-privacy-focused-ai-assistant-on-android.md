Title: Brave browser launches privacy-focused AI assistant on Android
Date: 2024-02-29T15:42:01-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Artificial Intelligence;Mobile
Slug: 2024-02-29-brave-browser-launches-privacy-focused-ai-assistant-on-android

[Source](https://www.bleepingcomputer.com/news/security/brave-browser-launches-privacy-focused-ai-assistant-on-android/){:target="_blank" rel="noopener"}

> Brave Software is the next company to jump into AI, announcing a new privacy-preserving AI assistant called "Leo" is rolling out on the Android version of its browser through the latest release, version 1.63. [...]
