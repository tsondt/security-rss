Title: Alpha ransomware linked to NetWalker operation dismantled in 2021
Date: 2024-02-16T11:07:21-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-02-16-alpha-ransomware-linked-to-netwalker-operation-dismantled-in-2021

[Source](https://www.bleepingcomputer.com/news/security/alpha-ransomware-linked-to-netwalker-operation-dismantled-in-2021/){:target="_blank" rel="noopener"}

> Security researchers analyzing the Alpha ransomware payload and modus operandi discovered overlaps with the now-defunct Netwalker ransomware operation. [...]
