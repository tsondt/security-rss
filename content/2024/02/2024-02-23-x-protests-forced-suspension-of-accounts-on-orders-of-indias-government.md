Title: X protests forced suspension of accounts on orders of India's government
Date: 2024-02-23T05:32:14+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-02-23-x-protests-forced-suspension-of-accounts-on-orders-of-indias-government

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/23/x_india_farmers_compulsory_takedown/){:target="_blank" rel="noopener"}

> Nonprofit SFLC links orders to farming protests The global government affairs team at X (née Twitter) has suspended some accounts and posts in India after receiving executive orders to do so from the country's government, backed by threat of penalties including significant fines and imprisonment.... [...]
