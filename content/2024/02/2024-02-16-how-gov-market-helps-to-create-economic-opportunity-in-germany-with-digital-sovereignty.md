Title: How Gov Market helps to create economic opportunity in Germany with digital sovereignty
Date: 2024-02-16T17:00:00+00:00
Author: Melanie Peterson
Category: GCP Security
Tags: Security & Identity;Customers
Slug: 2024-02-16-how-gov-market-helps-to-create-economic-opportunity-in-germany-with-digital-sovereignty

[Source](https://cloud.google.com/blog/topics/customers/how-data-sovereignty-can-help-create-economic-opportunity-in-germany/){:target="_blank" rel="noopener"}

> In Germany, public procurement can be a highly complex and time-consuming process. More than 30,000 procurement entities vie for contracts across a fragmented regulatory landscape, with different rules applying at the federal, state, and municipal levels. Since public procurement procedures are often time-consuming, particularly for large or high-value contracts, small and medium-sized enterprises (SMEs) often can’t compete for public contracts. While procurement itself is supported by digital procurement platforms (such as eVergabe ) planning and processes during tendering and contract execution are largely not standardized, require many manual steps, and are not provided by universal digital marketplaces as in other [...]
