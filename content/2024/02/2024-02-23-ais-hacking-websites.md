Title: AIs Hacking Websites
Date: 2024-02-23T16:14:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence;hacking;LLM
Slug: 2024-02-23-ais-hacking-websites

[Source](https://www.schneier.com/blog/archives/2024/02/ais-hacking-websites.html){:target="_blank" rel="noopener"}

> New research : LLM Agents can Autonomously Hack Websites Abstract: In recent years, large language models (LLMs) have become increasingly capable and can now interact with tools (i.e., call functions), read documents, and recursively call themselves. As a result, these LLMs can now function autonomously as agents. With the rise in capabilities of these agents, recent work has speculated on how LLM agents would affect cybersecurity. However, not much is known about the offensive capabilities of LLM agents. In this work, we show that LLM agents can autonomously hack websites, performing tasks as complex as blind database schema extraction and [...]
