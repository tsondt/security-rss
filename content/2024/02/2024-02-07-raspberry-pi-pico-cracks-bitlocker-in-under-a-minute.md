Title: Raspberry Pi Pico cracks BitLocker in under a minute
Date: 2024-02-07T15:30:09+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-02-07-raspberry-pi-pico-cracks-bitlocker-in-under-a-minute

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/07/breaking_bitlocker_pi_pico/){:target="_blank" rel="noopener"}

> Windows encryption feature defeated by $10 and a YouTube tutorial We're very familiar with the many projects in which Raspberry Pi hardware is used, from giving old computers a new lease of life through to running the animated displays so beloved by retailers. But cracking BitLocker? We doubt the company will be bragging too much about that particular application.... [...]
