Title: Uncle Sam tells nosy nations to keep their hands off Americans' personal data
Date: 2024-02-28T10:17:05+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-28-uncle-sam-tells-nosy-nations-to-keep-their-hands-off-americans-personal-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/28/white_house_sensitive_data/){:target="_blank" rel="noopener"}

> Biden readies executive order targeting China, Russia, and pals US President Joe Biden is expected to sign an executive order today that aims to prevent the sale or transfer of Americans' sensitive personal information and government-related data to adversarial countries including China and Russia.... [...]
