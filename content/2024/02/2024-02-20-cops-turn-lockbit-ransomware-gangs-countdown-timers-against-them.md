Title: Cops turn LockBit ransomware gang's countdown timers against them
Date: 2024-02-20T16:00:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-20-cops-turn-lockbit-ransomware-gangs-countdown-timers-against-them

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/20/nca_lockbit_takedown/){:target="_blank" rel="noopener"}

> Authorities dismantle cybercrime royalty by making mockery of their leak site In seizing and dismantling LockBit's infrastructure, Western cops are now making a mockery of the ransomware criminals by promising a long, drawn-out disclosure of the gang's secrets.... [...]
