Title: GitHub besieged by millions of malicious repositories in ongoing attack
Date: 2024-02-28T22:12:25+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;GitHub;repositories;supply chain attack
Slug: 2024-02-28-github-besieged-by-millions-of-malicious-repositories-in-ongoing-attack

[Source](https://arstechnica.com/?p=2006797){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) GitHub is struggling to contain an ongoing attack that’s flooding the site with millions of code repositories. These repositories contain obfuscated malware that steals passwords and cryptocurrency from developer devices, researchers said. The malicious repositories are clones of legitimate ones, making them hard to distinguish to the casual eye. An unknown party has automated a process that forks legitimate repositories, meaning the source code is copied so developers can use it in an independent project that builds on the original one. The result is millions of forks with names identical to the original one that add [...]
