Title: U.S. Internet Leaked Years of Internal, Customer Emails
Date: 2024-02-14T16:45:46+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Latest Warnings;alex holden;Hold Security;Securence;Travis Carter;U.S. Internet Corp.
Slug: 2024-02-14-us-internet-leaked-years-of-internal-customer-emails

[Source](https://krebsonsecurity.com/2024/02/u-s-internet-leaked-years-of-internal-customer-emails/){:target="_blank" rel="noopener"}

> The Minnesota-based Internet provider U.S. Internet Corp. has a business unit called Securence, which specializes in providing filtered, secure email services to businesses, educational institutions and government agencies worldwide. But until it was notified last week, U.S. Internet was publishing more than a decade’s worth of its internal email — and that of thousands of Securence clients — in plain text out on the Internet and just a click away for anyone with a Web browser. Headquartered in Minnetonka, Minn., U.S. Internet is a regional ISP that provides fiber and wireless Internet service. The ISP’s Securence division bills itself “a [...]
