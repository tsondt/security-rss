Title: Ransomware associated with LockBit still spreading 2 days after server takedown
Date: 2024-02-22T22:28:13+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;connectwise;lockbit;ransomware;screenconnect
Slug: 2024-02-22-ransomware-associated-with-lockbit-still-spreading-2-days-after-server-takedown

[Source](https://arstechnica.com/?p=2005464){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) Two days after an international team of authorities struck a major blow at LockBit, one of the Internet’s most prolific ransomware syndicates, researchers have detected a new round of attacks that are installing malware associated with the group. The attacks, detected in the past 24 hours, are exploiting two critical vulnerabilities in ScreenConnect, a remote desktop application sold by Connectwise. According to researchers at two security firms—SophosXOps and Huntress—attackers who successfully exploit the vulnerabilities go on to install LockBit ransomware and other post-exploit malware. It wasn’t immediately clear if the ransomware was the official LockBit [...]
