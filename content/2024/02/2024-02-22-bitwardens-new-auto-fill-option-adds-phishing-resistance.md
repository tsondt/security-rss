Title: Bitwarden’s new auto-fill option adds phishing resistance
Date: 2024-02-22T14:12:58-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2024-02-22-bitwardens-new-auto-fill-option-adds-phishing-resistance

[Source](https://www.bleepingcomputer.com/news/security/bitwardens-new-auto-fill-option-adds-phishing-resistance/){:target="_blank" rel="noopener"}

> The Bitwarden open-source password management service has introduced a new inline auto-fill menu that addresses the risk of user credentials being stolen through malicious form fields. [...]
