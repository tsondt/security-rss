Title: DEF CON is canceled! No, really this time – but the show will go on
Date: 2024-02-07T00:59:15+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-02-07-def-con-is-canceled-no-really-this-time-but-the-show-will-go-on

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/07/def_con_canceled/){:target="_blank" rel="noopener"}

> Longtime host Caesars ends relationship at short notice It's an annual meme that DEF CON infosec conference has been canceled, but this time it actually happened, ish.... [...]
