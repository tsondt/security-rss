Title: AWS HITRUST Shared Responsibility Matrix for HITRUST CSF v11.2 now available
Date: 2024-02-22T22:49:37+00:00
Author: Mark Weech
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Auditing;AWS HITRUST;AWS HITRUST Inheritance;Compliance reports;Security Assurance;Security Blog
Slug: 2024-02-22-aws-hitrust-shared-responsibility-matrix-for-hitrust-csf-v112-now-available

[Source](https://aws.amazon.com/blogs/security/aws-hitrust-shared-responsibility-matrix-for-hitrust-csf-v11-2-now-available/){:target="_blank" rel="noopener"}

> The latest version of the AWS HITRUST Shared Responsibility Matrix (SRM)—SRM version 1.4.2—is now available. To request a copy, choose SRM version 1.4.2 from the HITRUST website. SRM version 1.4.2 adds support for the HITRUST Common Security Framework (CSF) v11.2 assessments in addition to continued support for previous versions of HITRUST CSF assessments v9.1–v11.2. As [...]
