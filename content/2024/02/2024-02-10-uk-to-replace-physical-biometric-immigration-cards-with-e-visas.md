Title: UK to replace physical biometric immigration cards with e-visas
Date: 2024-02-10T05:00:00-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2024-02-10-uk-to-replace-physical-biometric-immigration-cards-with-e-visas

[Source](https://www.bleepingcomputer.com/news/security/uk-to-replace-physical-biometric-immigration-cards-with-e-visas/){:target="_blank" rel="noopener"}

> By 2025, Britain is set to ditch physical immigration documents like Biometric Residence Permits (BRPs) and Biometric Residence Cards (BRCs) in a bid to make its borders digital, in-line with developed countries like Australia. Understand what these Home Office changes mean for existing BRP and BRC holders, and what you need to do. [...]
