Title: Simplify DORA compliance with Google Cloud's updated contracts
Date: 2024-02-06T17:00:00+00:00
Author: Thiébaut Meyer
Category: GCP Security
Tags: Security & Identity
Slug: 2024-02-06-simplify-dora-compliance-with-google-clouds-updated-contracts

[Source](https://cloud.google.com/blog/products/identity-security/simplify-dora-compliance-with-google-clouds-updated-contracts/){:target="_blank" rel="noopener"}

> With less than one year to prepare for the EU Digital Operational Resilience Act (Regulation (EU) 2022/2554 - ‘DORA’) coming into force, today we are sharing more information about how Google Cloud plans to support financial entities with their DORA compliance. As an organization, we are committed to DORA compliance and a cross-functional team at Google Cloud has been working to prepare for DORA since the requirements were finalized in 2022. This includes implementing operational changes and enhancing our customer support model. Starting today, to help customers ensure their Google Cloud contracts are DORA-compliant by January 17, 2025, financial entities [...]
