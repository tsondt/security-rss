Title: 20 million Cutout.Pro user records leaked on data breach forum
Date: 2024-02-29T10:56:19-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-02-29-20-million-cutoutpro-user-records-leaked-on-data-breach-forum

[Source](https://www.bleepingcomputer.com/news/security/20-million-cutoutpro-user-records-leaked-on-data-breach-forum/){:target="_blank" rel="noopener"}

> AI service Cutout.Pro has suffered a data breach exposing the personal information of 20 million members, including email addresses, hashed and salted passwords, IP addresses, and names. [...]
