Title: UnitedHealth confirms Optum hack behind US healthcare billing outage
Date: 2024-02-23T04:41:07-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-02-23-unitedhealth-confirms-optum-hack-behind-us-healthcare-billing-outage

[Source](https://www.bleepingcomputer.com/news/security/unitedhealth-confirms-optum-hack-behind-us-healthcare-billing-outage/){:target="_blank" rel="noopener"}

> Healthcare giant UnitedHealth Group confirmed that its subsidiary Optum was forced to shut down IT systems and various services after a cyberattack by "nation-state" hackers on the Change Healthcare platform. [...]
