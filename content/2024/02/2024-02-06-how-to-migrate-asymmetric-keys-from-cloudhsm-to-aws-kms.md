Title: How to migrate asymmetric keys from CloudHSM to AWS KMS
Date: 2024-02-06T19:51:58+00:00
Author: Mani Manasa Mylavarapu
Category: AWS Security
Tags: Advanced (300);AWS CloudHSM;AWS Key Management Service;Security, Identity, & Compliance;Technical How-to;AWS Key Management Service (KMS);AWS KMS;CloudHSM;Security Blog
Slug: 2024-02-06-how-to-migrate-asymmetric-keys-from-cloudhsm-to-aws-kms

[Source](https://aws.amazon.com/blogs/security/how-to-migrate-asymmetric-keys-from-cloudhsm-to-aws-kms/){:target="_blank" rel="noopener"}

> In June 2023, Amazon Web Services (AWS) introduced a new capability to AWS Key Management Service (AWS KMS): you can now import asymmetric key materials such as RSA or elliptic-curve cryptography (ECC) private keys for your signing workflow into AWS KMS. This means that you can move your asymmetric keys that are managed outside of [...]
