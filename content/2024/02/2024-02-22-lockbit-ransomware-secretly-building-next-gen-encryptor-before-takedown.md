Title: LockBit ransomware secretly building next-gen encryptor before takedown
Date: 2024-02-22T08:51:11-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-02-22-lockbit-ransomware-secretly-building-next-gen-encryptor-before-takedown

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-secretly-building-next-gen-encryptor-before-takedown/){:target="_blank" rel="noopener"}

> LockBit ransomware developers were secretly building a new version of their file encrypting malware, dubbed LockBit-NG-Dev - likely a future LockBit 4.0, when law enforcement took down the cybercriminal's infrastructure earlier this week. [...]
