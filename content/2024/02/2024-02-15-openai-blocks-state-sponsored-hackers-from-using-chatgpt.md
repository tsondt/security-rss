Title: OpenAI blocks state-sponsored hackers from using ChatGPT
Date: 2024-02-15T10:56:55-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Artificial Intelligence
Slug: 2024-02-15-openai-blocks-state-sponsored-hackers-from-using-chatgpt

[Source](https://www.bleepingcomputer.com/news/security/openai-blocks-state-sponsored-hackers-from-using-chatgpt/){:target="_blank" rel="noopener"}

> OpenAI has removed accounts used by state-sponsored threat groups from Iran, North Korea, China, and Russia, that were abusing its artificial intelligence chatbot, ChatGPT. [...]
