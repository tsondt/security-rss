Title: FBI disrupts Moobot botnet used by Russian military hackers
Date: 2024-02-15T13:00:13-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-15-fbi-disrupts-moobot-botnet-used-by-russian-military-hackers

[Source](https://www.bleepingcomputer.com/news/security/fbi-disrupts-moobot-botnet-used-by-russian-military-hackers/){:target="_blank" rel="noopener"}

> The FBI took down a botnet of small office/home office (SOHO) routers used by Russia's Main Intelligence Directorate of the General Staff (GRU) to proxy malicious traffic and to target the United States and its allies in spearphishing and credential theft attacks. [...]
