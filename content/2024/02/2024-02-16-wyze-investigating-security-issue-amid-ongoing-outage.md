Title: Wyze investigating 'security issue' amid ongoing outage
Date: 2024-02-16T16:42:35-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-16-wyze-investigating-security-issue-amid-ongoing-outage

[Source](https://www.bleepingcomputer.com/news/security/wyze-investigating-security-issue-amid-ongoing-outage/){:target="_blank" rel="noopener"}

> Wyze Labs is investigating a security issue while experiencing a service outage that has been causing connectivity issues since this morning. [...]
