Title: Zoom stomps critical privilege escalation bug plus 6 other flaws
Date: 2024-02-15T15:30:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-15-zoom-stomps-critical-privilege-escalation-bug-plus-6-other-flaws

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/15/zoom_privilege_escalation/){:target="_blank" rel="noopener"}

> All desktop and mobile apps vulnerable to at least one of the vulnerabilities Video conferencing giant Zoom today opened up about a fresh batch of security vulnerabilities affecting its products, including a critical privilege escalation flaw.... [...]
