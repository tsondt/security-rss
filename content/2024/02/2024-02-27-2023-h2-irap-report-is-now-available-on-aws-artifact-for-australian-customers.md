Title: 2023 H2 IRAP report is now available on AWS Artifact for Australian customers
Date: 2024-02-27T16:55:18+00:00
Author: Patrick Chang
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Auditing;AWS Artifact;AWS security;Compliance;IRAP;IRAP PROTECTED;Security;Security Blog
Slug: 2024-02-27-2023-h2-irap-report-is-now-available-on-aws-artifact-for-australian-customers

[Source](https://aws.amazon.com/blogs/security/2023-h2-irap-report-is-now-available-on-aws-artifact-for-australian-customers/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is excited to announce that a new Information Security Registered Assessors Program (IRAP) report (2023 H2) is now available through AWS Artifact. An independent Australian Signals Directorate (ASD) certified IRAP assessor completed the IRAP assessment of AWS in December 2023. The new IRAP report includes an additional seven AWS services that are now assessed at the [...]
