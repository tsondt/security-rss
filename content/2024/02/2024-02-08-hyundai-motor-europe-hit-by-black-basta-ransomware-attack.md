Title: Hyundai Motor Europe hit by Black Basta ransomware attack
Date: 2024-02-08T15:16:15-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-02-08-hyundai-motor-europe-hit-by-black-basta-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/hyundai-motor-europe-hit-by-black-basta-ransomware-attack/){:target="_blank" rel="noopener"}

> Car maker Hyundai Motor Europe suffered a Black Basta ransomware attack, with the threat actors claiming to have stolen three terabytes of corporate data. [...]
