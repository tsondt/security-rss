Title: ALPHV ransomware claims loanDepot, Prudential Financial breaches
Date: 2024-02-16T18:46:26-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-16-alphv-ransomware-claims-loandepot-prudential-financial-breaches

[Source](https://www.bleepingcomputer.com/news/security/alphv-ransomware-claims-loandepot-prudential-financial-breaches/){:target="_blank" rel="noopener"}

> The ALPHV/Blackcat ransomware gang has claimed responsibility for the recent network breaches of Fortune 500 company Prudential Financial and mortgage lender loanDepot. [...]
