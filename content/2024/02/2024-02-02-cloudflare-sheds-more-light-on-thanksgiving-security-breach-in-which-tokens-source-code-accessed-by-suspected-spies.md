Title: Cloudflare sheds more light on Thanksgiving security breach in which tokens, source code accessed by suspected spies
Date: 2024-02-02T01:12:46+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-02-02-cloudflare-sheds-more-light-on-thanksgiving-security-breach-in-which-tokens-source-code-accessed-by-suspected-spies

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/02/cloudflare_okta_atlassian/){:target="_blank" rel="noopener"}

> Atlassian systen compromised via October Okta intrusion Cloudflare has just detailed how suspected government spies gained access to its internal Atlassian installation using credentials stolen via a security breach at Okta in October.... [...]
