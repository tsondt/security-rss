Title: OpenAI shuts down China, Russia, Iran, N Korea accounts caught doing naughty things
Date: 2024-02-15T00:10:17+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2024-02-15-openai-shuts-down-china-russia-iran-n-korea-accounts-caught-doing-naughty-things

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/15/openai_microsoft_spying/){:target="_blank" rel="noopener"}

> You don't need us to craft phishing emails or write malware, super-lab sniffs OpenAI has shut down five accounts it asserts were used by government agents to generate phishing emails and malicious software scripts as well as research ways to evade malware detection.... [...]
