Title: Malicious AI models on Hugging Face backdoor users’ machines
Date: 2024-02-28T17:12:17-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-02-28-malicious-ai-models-on-hugging-face-backdoor-users-machines

[Source](https://www.bleepingcomputer.com/news/security/malicious-ai-models-on-hugging-face-backdoor-users-machines/){:target="_blank" rel="noopener"}

> At least 100 instances of malicious AI ML models were found on the Hugging Face platform, some of which can execute code on the victim's machine, giving attackers a persistent backdoor. [...]
