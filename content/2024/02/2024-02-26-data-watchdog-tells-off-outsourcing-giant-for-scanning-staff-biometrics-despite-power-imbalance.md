Title: Data watchdog tells off outsourcing giant for scanning staff biometrics despite 'power imbalance'
Date: 2024-02-26T12:41:40+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2024-02-26-data-watchdog-tells-off-outsourcing-giant-for-scanning-staff-biometrics-despite-power-imbalance

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/26/uk_data_protection_watchdog_halts/){:target="_blank" rel="noopener"}

> 2,000 employees at 38 facilities had data processed 'unlawfully', ICO says A data protection watchdog in the UK has issued an enforcement notice to stop Serco from using facial recognition tech and fingerprint scanning to monitor staff at 38 leisure centers it runs.... [...]
