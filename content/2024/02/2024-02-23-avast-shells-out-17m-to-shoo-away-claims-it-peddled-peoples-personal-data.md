Title: Avast shells out $17M to shoo away claims it peddled people's personal data
Date: 2024-02-23T00:56:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-02-23-avast-shells-out-17m-to-shoo-away-claims-it-peddled-peoples-personal-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/23/avast_ftc_settlement/){:target="_blank" rel="noopener"}

> A name that's commonly shouted by pirates might be a clue, me hearties! Avast has agreed to cough up $16.5 million after the FTC accused the antivirus vendor of selling customer information to third parties.... [...]
