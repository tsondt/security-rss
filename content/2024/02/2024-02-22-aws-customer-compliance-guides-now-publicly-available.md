Title: AWS Customer Compliance Guides now publicly available
Date: 2024-02-22T18:37:25+00:00
Author: Kevin Donohue
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Compliance;CSF;HIPAA;IRAP;ISMAP;ISO;NIST;NIST CSF;NIST SP 800-53;risk;Security Blog;Shared Responsibility Model
Slug: 2024-02-22-aws-customer-compliance-guides-now-publicly-available

[Source](https://aws.amazon.com/blogs/security/aws-customer-compliance-guides-now-publicly-available/){:target="_blank" rel="noopener"}

> The AWS Global Security & Compliance Acceleration (GSCA) Program has released AWS Customer Compliance Guides (CCGs) on the AWS Compliance Resources page to help customers, AWS Partners, and assessors quickly understand how industry-leading compliance frameworks map to AWS service documentation and security best practices. CCGs offer security guidance mapped to 16 different compliance frameworks for more than [...]
