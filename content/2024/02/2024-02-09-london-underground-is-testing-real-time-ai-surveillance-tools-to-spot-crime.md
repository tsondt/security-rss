Title: London Underground is testing real-time AI surveillance tools to spot crime
Date: 2024-02-09T19:02:28+00:00
Author: WIRED
Category: Ars Technica
Tags: AI;Biz & IT;Security;AI surveillance;Freedom of information act;london underground;syndication
Slug: 2024-02-09-london-underground-is-testing-real-time-ai-surveillance-tools-to-spot-crime

[Source](https://arstechnica.com/?p=2002361){:target="_blank" rel="noopener"}

> Enlarge (credit: John Keeble/Getty Images ) Thousands of people using the London Underground had their movements, behavior, and body language watched by AI surveillance software designed to see if they were committing crimes or were in unsafe situations, new documents obtained by WIRED reveal. The machine-learning software was combined with live CCTV footage to try to detect aggressive behavior and guns or knives being brandished, as well as looking for people falling onto Tube tracks or dodging fares. From October 2022 until the end of September 2023, Transport for London (TfL), which operates the city’s Tube and bus network, tested [...]
