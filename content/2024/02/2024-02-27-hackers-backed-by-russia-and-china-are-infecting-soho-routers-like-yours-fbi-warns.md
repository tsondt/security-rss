Title: Hackers backed by Russia and China are infecting SOHO routers like yours, FBI warns
Date: 2024-02-27T20:57:10+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;APT28;routers;volt typhoon
Slug: 2024-02-27-hackers-backed-by-russia-and-china-are-infecting-soho-routers-like-yours-fbi-warns

[Source](https://arstechnica.com/?p=2006319){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) The FBI and partners from 10 other countries are urging owners of Ubiquiti EdgeRouters to check their gear for signs they’ve been hacked and are being used to conceal ongoing malicious operations by Russian state hackers. The Ubiquiti EdgeRouters make an ideal hideout for hackers. The inexpensive gear, used in homes and small offices, runs a version of Linux that can host malware that surreptitiously runs behind the scenes. The hackers then use the routers to conduct their malicious activities. Rather than using infrastructure and IP addresses that are known to be hostile, the connections [...]
