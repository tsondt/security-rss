Title: Rust can help make software secure – but it's no cure-all
Date: 2024-02-08T07:28:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-02-08-rust-can-help-make-software-secure-but-its-no-cure-all

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/08/rust_software_memory_safety/){:target="_blank" rel="noopener"}

> Security is a process, not a product. Nor a language Memory-safety flaws represent the majority of high-severity problems for Google and Microsoft, but they're not necessarily associated with the majority of vulnerabilities that actually get exploited.... [...]
