Title: Fulton County, Security Experts Call LockBit’s Bluff
Date: 2024-02-29T22:18:54+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Ransomware;Brett Callow;Emsisoft;fbi;Fulton County hack;LockBit;LockBitSupp;NCA;RedSense;Robb Pitts
Slug: 2024-02-29-fulton-county-security-experts-call-lockbits-bluff

[Source](https://krebsonsecurity.com/2024/02/fulton-county-security-experts-call-lockbits-bluff/){:target="_blank" rel="noopener"}

> The ransomware group LockBit told officials with Fulton County, Ga. they could expect to see their internal documents published online this morning unless the county paid a ransom demand. LockBit removed Fulton County’s listing from its victim shaming website this morning, claiming the county had paid. But county officials said they did not pay, nor did anyone make payment on their behalf. Security experts say LockBit was likely bluffing and probably lost most of the data when the gang’s servers were seized this month by U.S. and U.K. law enforcement. The LockBit website included a countdown timer until the promised [...]
