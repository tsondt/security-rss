Title: Americans lost record $10 billion to fraud in 2023, FTC warns
Date: 2024-02-09T10:21:12-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-09-americans-lost-record-10-billion-to-fraud-in-2023-ftc-warns

[Source](https://www.bleepingcomputer.com/news/security/americans-lost-record-10-billion-to-fraud-in-2023-ftc-warns/){:target="_blank" rel="noopener"}

> The U.S. Federal Trade Commission (FTC) says Americans lost over $10 billion to scammers in 2023, marking a 14% increase in reported losses compared to the previous year. [...]
