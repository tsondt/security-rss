Title: Seized ransomware network LockBit rewired to expose hackers to world
Date: 2024-02-20T13:16:19+00:00
Author: Alex Hern
Category: The Guardian
Tags: Cybercrime;NCA (National Crime Agency);Internet;Technology;FBI;Malware;Data and computer security;Business;Crime;Hacking;UK news;US news
Slug: 2024-02-20-seized-ransomware-network-lockbit-rewired-to-expose-hackers-to-world

[Source](https://www.theguardian.com/technology/2024/feb/20/uk-and-fbi-lock-cybercrime-group-out-of-lockbit-website){:target="_blank" rel="noopener"}

> Four arrested and LockBit victims will get help to recover data after joint operation in UK, US and Europe The entire “command and control” apparatus for the ransomware group LockBit is now in possession of law enforcement, the UK’s National Crime Agency has revealed, after it emerged that it had seized the criminal gang’s website in a coordinated international operation. The flood of data hacked back from the hackers has already led to four arrests, and the authorities promised on Tuesday to repurpose the technology to expose the group’s operations to the world. Continue reading... [...]
