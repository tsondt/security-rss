Title: Canada declares Flipper Zero public enemy No. 1 in car-theft crackdown
Date: 2024-02-10T01:24:51+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Cars;Policy;Security;Canada;car theft;flipper zero;hacking;radio
Slug: 2024-02-10-canada-declares-flipper-zero-public-enemy-no-1-in-car-theft-crackdown

[Source](https://arstechnica.com/?p=2002579){:target="_blank" rel="noopener"}

> Enlarge / A Flipper Zero device (credit: https://flipperzero.one/) Canadian Prime Minister Justin Trudeau has identified an unlikely public enemy No. 1 in his new crackdown on car theft: the Flipper Zero, a $200 piece of open source hardware used to capture, analyze and interact with simple radio communications. On Thursday, the Innovation, Science and Economic Development Canada agency said it will “pursue all avenues to ban devices used to steal vehicles by copying the wireless signals for remote keyless entry, such as the Flipper Zero, which would allow for the removal of those devices from the Canadian marketplace through collaboration [...]
