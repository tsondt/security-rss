Title: China warns of fake digital currency wallets fleecing netizens
Date: 2024-02-27T04:02:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-02-27-china-warns-of-fake-digital-currency-wallets-fleecing-netizens

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/27/china_fake_ecny_app_warning/){:target="_blank" rel="noopener"}

> Scammers' tactics are tiresomely familiar: get-rich-quick schemes and data harvesting China's Ministry of Industry and Information Technology has warned local netizens that fake wallet apps for the nation's central bank digital currency (CBDC) are already circulating and being abused by scammers.... [...]
