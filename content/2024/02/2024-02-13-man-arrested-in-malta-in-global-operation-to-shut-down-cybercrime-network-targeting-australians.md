Title: Man arrested in Malta in global operation to shut down cybercrime network targeting Australians
Date: 2024-02-13T01:14:52+00:00
Author: Daisy Dumas
Category: The Guardian
Tags: Australian federal police;Trojans;Malware;Data and computer security;Australia news;Cybercrime;US news;Malta;Nigeria;Technology;World news
Slug: 2024-02-13-man-arrested-in-malta-in-global-operation-to-shut-down-cybercrime-network-targeting-australians

[Source](https://www.theguardian.com/australia-news/2024/feb/13/daniel-meli-arrest-malta-cybercrime-australia-trojan-software){:target="_blank" rel="noopener"}

> Federal police warn they will track down alleged criminals using Warzone trojan software Follow our Australia news live blog for latest updates Get our morning and afternoon news emails, free app or daily news podcast A man has been arrested as part of an international operation to shut down a global cybercrime network targeting Australians as Australian federal police warn they will track down other people alleged to be involved in the use of the malicious software. Daniel Meli, 27, was arrested in Malta on 7 February for allegedly selling and training criminals in the use of Warzone, a remote [...]
