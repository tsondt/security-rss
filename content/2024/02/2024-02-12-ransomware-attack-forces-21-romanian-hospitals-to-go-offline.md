Title: Ransomware attack forces 21 Romanian hospitals to go offline
Date: 2024-02-12T07:39:31-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-12-ransomware-attack-forces-21-romanian-hospitals-to-go-offline

[Source](https://www.bleepingcomputer.com/news/security/ransomware-attack-forces-21-romanian-hospitals-to-go-offline/){:target="_blank" rel="noopener"}

> At least 21 hospitals in Romania were knocked offline after a ransomware attack took down their healthcare management system. [...]
