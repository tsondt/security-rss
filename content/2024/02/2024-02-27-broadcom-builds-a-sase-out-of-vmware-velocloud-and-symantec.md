Title: Broadcom builds a SASE out of VMware VeloCloud and Symantec
Date: 2024-02-27T07:28:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-02-27-broadcom-builds-a-sase-out-of-vmware-velocloud-and-symantec

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/27/vmware_symantec_sase/){:target="_blank" rel="noopener"}

> First integration across properties, as end user compute division readies to leave home Broadcom has delivered on its 2023 teaser of integration between VMware's SD-WAN and Symantec's Security Service Edge, by today debuting the "VMware VeloCloud SASE, Secured by Symantec" at Mobile World Congress in Barcelona.... [...]
