Title: Juniper Support Portal Exposed Customer Device Info
Date: 2024-02-09T15:34:21+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Juniper Networks;Logan George;Nicholas Weaver
Slug: 2024-02-09-juniper-support-portal-exposed-customer-device-info

[Source](https://krebsonsecurity.com/2024/02/juniper-support-portal-exposed-customer-device-info/){:target="_blank" rel="noopener"}

> Until earlier this week, the support website for networking equipment vendor Juniper Networks was exposing potentially sensitive information tied to customer products, including which devices customers bought, as well as each product’s warranty status, service contracts and serial numbers. Juniper said it has since fixed the problem, and that the inadvertent data exposure stemmed from a recent upgrade to its support portal. Sunnyvale, Calif. based Juniper Networks makes high-powered Internet routers and switches, and its products are used in some of the world’s largest organizations. Earlier this week KrebsOnSecurity heard from a reader responsible for managing several Juniper devices, who [...]
