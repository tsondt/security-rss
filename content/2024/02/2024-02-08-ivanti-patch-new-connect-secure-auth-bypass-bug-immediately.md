Title: Ivanti: Patch new Connect Secure auth bypass bug immediately
Date: 2024-02-08T14:45:52-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-08-ivanti-patch-new-connect-secure-auth-bypass-bug-immediately

[Source](https://www.bleepingcomputer.com/news/security/ivanti-patch-new-connect-secure-auth-bypass-bug-immediately/){:target="_blank" rel="noopener"}

> Today, Ivanti warned of a new authentication bypass vulnerability impacting Connect Secure, Policy Secure, and ZTA gateways, urging admins to secure their appliances immediately. [...]
