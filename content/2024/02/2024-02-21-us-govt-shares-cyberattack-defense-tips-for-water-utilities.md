Title: US govt shares cyberattack defense tips for water utilities
Date: 2024-02-21T13:39:17-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-21-us-govt-shares-cyberattack-defense-tips-for-water-utilities

[Source](https://www.bleepingcomputer.com/news/security/us-govt-shares-cyberattack-defense-tips-for-water-utilities/){:target="_blank" rel="noopener"}

> CISA, the FBI, and the Environmental Protection Agency (EPA) shared a list of defense measures U.S. water utilities should implement to better defend their systems against cyberattacks [...]
