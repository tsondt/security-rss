Title: Microsoft finally expands free logging—but only for govt agencies
Date: 2024-02-21T17:31:33-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-21-microsoft-finally-expands-free-loggingbut-only-for-govt-agencies

[Source](https://www.bleepingcomputer.com/news/security/microsoft-finally-expands-free-logging-but-only-for-govt-agencies/){:target="_blank" rel="noopener"}

> Microsoft has expanded free Purview Audit logging capabilities for all U.S. federal agencies six months after disclosing that Chinese hackers stole U.S. government emails undetected in an Exchange Online breach between May and June 2023. [...]
