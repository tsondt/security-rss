Title: Leaky Vessels flaws allow hackers to escape Docker, runc containers
Date: 2024-02-04T10:17:34-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud
Slug: 2024-02-04-leaky-vessels-flaws-allow-hackers-to-escape-docker-runc-containers

[Source](https://www.bleepingcomputer.com/news/security/leaky-vessels-flaws-allow-hackers-to-escape-docker-runc-containers/){:target="_blank" rel="noopener"}

> Four vulnerabilities collectively called "Leaky Vessels" allow hackers to escape containers and access data on the underlying host operating system. [...]
