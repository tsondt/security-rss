Title: Hacker arrested for selling bank accounts of US, Canadian users
Date: 2024-02-18T10:06:38-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-02-18-hacker-arrested-for-selling-bank-accounts-of-us-canadian-users

[Source](https://www.bleepingcomputer.com/news/security/hacker-arrested-for-selling-bank-accounts-of-us-canadian-users/){:target="_blank" rel="noopener"}

> Ukraine's cyber police arrested a 31-year-old for running a cybercrime operation that gained access to bank accounts of American and Canadian users and sold them on the dark web. [...]
