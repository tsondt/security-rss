Title: China's Volt Typhoon spies broke into emergency network of 'large' US city
Date: 2024-02-14T21:00:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-14-chinas-volt-typhoon-spies-broke-into-emergency-network-of-large-us-city

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/14/volt_typhoon_emergency_network/){:target="_blank" rel="noopener"}

> Jeez, not now, Xi. Can't you see we've got an election and Ukraine and Gaza and cost of living and layoffs and... The Chinese government's Volt Typhoon spy team has apparently already compromised a large US city's emergency services network and has been spotted snooping around America's telecommunications' providers as well.... [...]
