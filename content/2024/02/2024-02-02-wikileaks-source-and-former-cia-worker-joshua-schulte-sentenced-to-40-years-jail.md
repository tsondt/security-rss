Title: Wikileaks source and former CIA worker Joshua Schulte sentenced to 40 years jail
Date: 2024-02-02T03:58:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-02-02-wikileaks-source-and-former-cia-worker-joshua-schulte-sentenced-to-40-years-jail

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/02/vault_7_wikileaks_leaker_joshua/){:target="_blank" rel="noopener"}

> 'Vault 7' leak detailed cyber-ops including forged digital certs Joshua Schulte, a former CIA employee and software engineer accused of sharing material with WikiLeaks, was sentenced to 40 years in prison by the US Southern District of New York on Thursday.... [...]
