Title: Just one bad packet can bring down a vulnerable DNS server thanks to DNSSEC
Date: 2024-02-13T23:27:26+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-02-13-just-one-bad-packet-can-bring-down-a-vulnerable-dns-server-thanks-to-dnssec

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/13/dnssec_vulnerability_internet/){:target="_blank" rel="noopener"}

> 'You don't have to do more than that to disconnect an entire network' El Reg told as patches emerge A single packet can exhaust the processing capacity of a vulnerable DNS server, effectively disabling the machine, by exploiting a 20-plus-year-old design flaw in the DNSSEC specification.... [...]
