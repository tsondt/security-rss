Title: Friday Squid Blogging: Vegan Squid-Ink Pasta
Date: 2024-02-16T22:04:11+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-02-16-friday-squid-blogging-vegan-squid-ink-pasta

[Source](https://www.schneier.com/blog/archives/2024/02/friday-squid-blogging-vegan-squid-ink-pasta.html){:target="_blank" rel="noopener"}

> It uses black beans for color and seaweed for flavor. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
