Title: Ransomware payments reached record $1.1 billion in 2023
Date: 2024-02-07T09:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-02-07-ransomware-payments-reached-record-11-billion-in-2023

[Source](https://www.bleepingcomputer.com/news/security/ransomware-payments-reached-record-11-billion-in-2023/){:target="_blank" rel="noopener"}

> Ransomware payments in 2023 soared above $1.1 billion for the first time, shattering previous records and reversing the decline seen in 2022, marking the year as an exceptionally profitable period for ransomware gangs. [...]
