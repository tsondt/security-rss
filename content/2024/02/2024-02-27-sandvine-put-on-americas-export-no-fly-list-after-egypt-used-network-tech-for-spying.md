Title: Sandvine put on America's export no-fly list after Egypt used network tech for spying
Date: 2024-02-27T20:22:38+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-02-27-sandvine-put-on-americas-export-no-fly-list-after-egypt-used-network-tech-for-spying

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/27/sandvine_us_entity_list/){:target="_blank" rel="noopener"}

> Canadian network box maker floats in denial The US Commerce Department has blacklisted Sandvine for selling its networking monitoring technology to Egypt, where the Feds say the gear was used to spy on political and human-rights activists.... [...]
