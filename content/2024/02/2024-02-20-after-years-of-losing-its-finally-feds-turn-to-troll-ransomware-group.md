Title: After years of losing, it’s finally feds’ turn to troll ransomware group
Date: 2024-02-20T21:29:33+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;hacking;lockbit;ransomware;takedowns
Slug: 2024-02-20-after-years-of-losing-its-finally-feds-turn-to-troll-ransomware-group

[Source](https://arstechnica.com/?p=2004713){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) After years of being outmaneuvered by snarky ransomware criminals who tease and brag about each new victim they claim, international authorities finally got their chance to turn the tables, and they aren't squandering it. The top-notch trolling came after authorities from the US, UK, and Europol took down most of the infrastructure belonging to LockBit, a ransomware syndicate that has extorted more than $120 million from thousands of victims around the world. On Tuesday, most of the sites LockBit uses to shame its victims for being hacked, pressure them into paying, and brag of their hacking [...]
