Title: Critical flaw in Shim bootloader impacts major Linux distros
Date: 2024-02-07T10:55:17-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Linux
Slug: 2024-02-07-critical-flaw-in-shim-bootloader-impacts-major-linux-distros

[Source](https://www.bleepingcomputer.com/news/security/critical-flaw-in-shim-bootloader-impacts-major-linux-distros/){:target="_blank" rel="noopener"}

> A critical vulnerability in the Shim Linux bootloader enables attackers to execute code and take control of a target system before the kernel is loaded, bypassing existing security mechanisms. [...]
