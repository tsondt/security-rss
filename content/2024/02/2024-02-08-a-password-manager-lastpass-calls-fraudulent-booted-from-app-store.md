Title: A password manager LastPass calls “fraudulent” booted from App Store
Date: 2024-02-08T22:16:25+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Apple;Biz & IT;Security;App Store;apple;lastpass
Slug: 2024-02-08-a-password-manager-lastpass-calls-fraudulent-booted-from-app-store

[Source](https://arstechnica.com/?p=2002178){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) As Apple has stepped up its promotion of its App Store as a safer and more trustworthy source of apps, its operators scrambled Thursday to correct a major threat to that narrative: a listing that password manager maker LastPass said was a “fraudulent app impersonating” its brand. At the time this article on Ars went live, Apple had removed the app—titled LassPass and bearing a logo strikingly similar to the one used by LastPass—from its App Store. At the same time, Apple allowed a separate app submitted by the same developer to remain. Apple provided no [...]
