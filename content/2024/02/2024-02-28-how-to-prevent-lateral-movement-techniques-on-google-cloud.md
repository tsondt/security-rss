Title: How to prevent lateral movement techniques on Google Cloud
Date: 2024-02-28T11:00:00+00:00
Author: Wendy Walasek
Category: GCP Security
Tags: Compute;Security & Identity
Slug: 2024-02-28-how-to-prevent-lateral-movement-techniques-on-google-cloud

[Source](https://cloud.google.com/blog/products/identity-security/how-to-prevent-lateral-movement-techniques-on-google-cloud/){:target="_blank" rel="noopener"}

> Cybercriminals often use lateral movement techniques when exploring a compromised network to slide sideways, from devices to applications, as they hunt for vulnerabilities, escalate access privileges, and seek to reach their ultimate target. Research published today by Palo Alto Networks highlights several techniques that exploit misconfigurations which could allow a malicious actor to move laterally in cloud environments. While these misconfiguration problems aren’t new, Palo Alto Networks’ research showcases real-world scenarios in which cybercriminals have abused cloud administration permissions to access unauthorized content across cloud providers, including AWS, Azure, and Google Cloud. In this post, we explain the misconfigurations that [...]
