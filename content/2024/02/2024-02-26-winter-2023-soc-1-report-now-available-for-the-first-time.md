Title: Winter 2023 SOC 1 report now available for the first time
Date: 2024-02-26T15:08:54+00:00
Author: Brownell Combs
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS SOC Reports;Compliance;Security Blog;SOC
Slug: 2024-02-26-winter-2023-soc-1-report-now-available-for-the-first-time

[Source](https://aws.amazon.com/blogs/security/winter-2023-soc-1-report-now-available-for-the-first-time/){:target="_blank" rel="noopener"}

> We continue to expand the scope of our assurance programs at Amazon Web Services (AWS) and are pleased to announce the first ever Winter 2023 AWS System and Organization Controls (SOC) 1 report. The new Winter SOC report demonstrates our continuous commitment to adhere to the heightened expectations for cloud service providers. The report covers [...]
