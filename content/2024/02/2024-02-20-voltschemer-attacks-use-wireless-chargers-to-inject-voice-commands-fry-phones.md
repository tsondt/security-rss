Title: VoltSchemer attacks use wireless chargers to inject voice commands, fry phones
Date: 2024-02-20T15:38:46-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile;Technology
Slug: 2024-02-20-voltschemer-attacks-use-wireless-chargers-to-inject-voice-commands-fry-phones

[Source](https://www.bleepingcomputer.com/news/security/voltschemer-attacks-use-wireless-chargers-to-inject-voice-commands-fry-phones/){:target="_blank" rel="noopener"}

> A team of academic researchers show that a new set of attacks called 'VoltSchemer' can inject voice commands to manipulate a smartphone's voice assistant through the magnetic field emitted by an off-the-shelf wireless charger. [...]
