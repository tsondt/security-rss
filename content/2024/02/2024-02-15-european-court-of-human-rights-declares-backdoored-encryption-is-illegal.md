Title: European Court of Human Rights declares backdoored encryption is illegal
Date: 2024-02-15T07:26:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-02-15-european-court-of-human-rights-declares-backdoored-encryption-is-illegal

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/15/echr_backdoor_encryption/){:target="_blank" rel="noopener"}

> Surprising third-act twist as Russian case means more freedom for all The European Court of Human Rights (ECHR) has ruled that laws requiring crippled encryption and extensive data retention violate the European Convention on Human Rights – a decision that may derail European data surveillance legislation known as Chat Control.... [...]
