Title: GitHub enables push protection by default to stop secrets leak
Date: 2024-02-29T13:57:35-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-29-github-enables-push-protection-by-default-to-stop-secrets-leak

[Source](https://www.bleepingcomputer.com/news/security/github-enables-push-protection-by-default-to-stop-secrets-leak/){:target="_blank" rel="noopener"}

> GitHub has enabled push protection by default for all public repositories to prevent accidental exposure of secrets such as access tokens and API keys when pushing new code. [...]
