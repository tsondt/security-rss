Title: Lurie Children's Hospital back to pen and paper after cyberattack
Date: 2024-02-05T14:45:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-05-lurie-childrens-hospital-back-to-pen-and-paper-after-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/05/lurie_childrens_hospital_cyberattack/){:target="_blank" rel="noopener"}

> It's the second Chicago hospital to disclose a major incident in the same week For the second time in one week, cybercriminals have targeted a Chicago children's hospital, this time causing significant operational disruption.... [...]
