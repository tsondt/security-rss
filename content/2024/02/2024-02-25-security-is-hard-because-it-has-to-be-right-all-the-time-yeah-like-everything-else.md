Title: Security is hard because it has to be right all the time? Yeah, like everything else
Date: 2024-02-25T16:09:12+00:00
Author: Larry Peterson
Category: The Register
Tags: 
Slug: 2024-02-25-security-is-hard-because-it-has-to-be-right-all-the-time-yeah-like-everything-else

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/25/security_not_different/){:target="_blank" rel="noopener"}

> It takes only one bottleneck or single point of failure to ruin your week Systems Approach One refrain you often hear is that security must be built in from the ground floor; that retrofitting security to an existing system is the source of design complications, or worse, outright flawed designs.... [...]
