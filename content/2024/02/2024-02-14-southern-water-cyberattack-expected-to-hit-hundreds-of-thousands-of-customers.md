Title: Southern Water cyberattack expected to hit hundreds of thousands of customers
Date: 2024-02-14T12:38:24+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-02-14-southern-water-cyberattack-expected-to-hit-hundreds-of-thousands-of-customers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/14/southern_water_cyberattack/){:target="_blank" rel="noopener"}

> Brit utility also curiously disappears from Black Basta leak site Southern Water has admitted between five and ten percent of its customers had their details stolen from the British utilities giant during a January cyberattack.... [...]
