Title: Feds post $15 million bounty for info on ALPHV/Blackcat ransomware crew
Date: 2024-02-19T01:29:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-02-19-feds-post-15-million-bounty-for-info-on-alphvblackcat-ransomware-crew

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/19/infosec_news_in_brief/){:target="_blank" rel="noopener"}

> ALSO: EncroChat crims still getting busted; ransomware takes down CO public defenders office; and crit vulns infosec in brief The US government is offering bounties up to $15 million as a reward for anyone willing to help it take out the APLHV/Blackcat ransomware gang.... [...]
