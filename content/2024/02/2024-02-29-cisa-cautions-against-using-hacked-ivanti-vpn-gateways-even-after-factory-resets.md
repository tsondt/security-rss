Title: CISA cautions against using hacked Ivanti VPN gateways even after factory resets
Date: 2024-02-29T15:35:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-29-cisa-cautions-against-using-hacked-ivanti-vpn-gateways-even-after-factory-resets

[Source](https://www.bleepingcomputer.com/news/security/cisa-cautions-against-using-hacked-ivanti-vpn-gateways-even-after-factory-resets/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity and Infrastructure Security Agency (CISA) revealed today that attackers who hack Ivanti VPN appliances using one of multiple actively exploited vulnerabilities may be able to maintain root persistence even after performing factory resets. [...]
