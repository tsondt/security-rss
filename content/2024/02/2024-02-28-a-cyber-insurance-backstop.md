Title: A Cyber Insurance Backstop
Date: 2024-02-28T12:02:58+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;computer security;cyber;cyberattack;insurance
Slug: 2024-02-28-a-cyber-insurance-backstop

[Source](https://www.schneier.com/blog/archives/2024/02/a-cyber-insurance-backstop.html){:target="_blank" rel="noopener"}

> In the first week of January, the pharmaceutical giant Merck quietly settled its years-long lawsuit over whether or not its property and casualty insurers would cover a $700 million claim filed after the devastating NotPetya cyberattack in 2017. The malware ultimately infected more than 40,000 of Merck’s computers, which significantly disrupted the company’s drug and vaccine production. After Merck filed its $700 million claim, the pharmaceutical giant’s insurers argued that they were not required to cover the malware’s damage because the cyberattack was widely attributed to the Russian government and therefore was excluded from standard property and casualty insurance coverage [...]
