Title: FCC orders telecom carriers to report PII data breaches within 30 days
Date: 2024-02-12T16:50:51-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-02-12-fcc-orders-telecom-carriers-to-report-pii-data-breaches-within-30-days

[Source](https://www.bleepingcomputer.com/news/security/fcc-orders-telecom-carriers-to-report-pii-data-breaches-within-30-days/){:target="_blank" rel="noopener"}

> Starting March 13th, telecommunications companies must report data breaches impacting customers' personally identifiable information within 30 days, as required by FCC's updated data breach reporting requirements. [...]
