Title: Microsoft February 2024 Patch Tuesday fixes 2 zero-days, 73 flaws
Date: 2024-02-13T14:07:17-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-02-13-microsoft-february-2024-patch-tuesday-fixes-2-zero-days-73-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-february-2024-patch-tuesday-fixes-2-zero-days-73-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's February 2024 Patch Tuesday, which includes security updates for 73 flaws and two actively exploited zero-days. [...]
