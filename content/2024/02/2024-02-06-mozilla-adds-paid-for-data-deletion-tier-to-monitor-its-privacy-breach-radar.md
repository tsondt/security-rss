Title: Mozilla adds paid-for data-deletion tier to Monitor, its privacy-breach radar
Date: 2024-02-06T21:54:27+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-02-06-mozilla-adds-paid-for-data-deletion-tier-to-monitor-its-privacy-breach-radar

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/06/mozilla_monitor_data/){:target="_blank" rel="noopener"}

> Firefox maker promises to lean on personal info brokers to scrub records Mozilla on Tuesday expanded its free privacy-monitoring service with a paid-for tier called Mozilla Monitor Plus that will try to get data brokers to delete their copies of subscribers' personal information.... [...]
