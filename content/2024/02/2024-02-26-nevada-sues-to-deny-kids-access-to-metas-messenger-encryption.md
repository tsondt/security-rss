Title: Nevada sues to deny kids access to Meta's Messenger encryption
Date: 2024-02-26T22:00:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-02-26-nevada-sues-to-deny-kids-access-to-metas-messenger-encryption

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/26/nevada_meta_encryption/){:target="_blank" rel="noopener"}

> State government says it's thinking of the children A law firm acting on behalf of the Nevada Attorney General Aaron Ford has asked a state court to issue a temporary restraining order (TRO) denying minors access to encrypted communication in Meta's Messenger application.... [...]
