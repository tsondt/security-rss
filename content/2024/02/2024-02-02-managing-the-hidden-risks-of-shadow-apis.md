Title: Managing the hidden risks of shadow APIs
Date: 2024-02-02T03:00:07+00:00
Author: Kunaciilan Nallappan, Regional Vice President for Asia Pacific at F5
Category: The Register
Tags: 
Slug: 2024-02-02-managing-the-hidden-risks-of-shadow-apis

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/02/managing_the_hidden_risks_of/){:target="_blank" rel="noopener"}

> How F5 Distributed Cloud Services seal security gaps in modern app development amid growing attack surface Partner Content Application programming interfaces (APIs) play a significant role in today's digital economy, but at the same time they can also represent a data security vulnerability.... [...]
