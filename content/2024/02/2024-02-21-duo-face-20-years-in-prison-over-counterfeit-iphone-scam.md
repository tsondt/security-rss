Title: Duo face 20 years in prison over counterfeit iPhone scam
Date: 2024-02-21T18:30:06+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2024-02-21-duo-face-20-years-in-prison-over-counterfeit-iphone-scam

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/21/counterfeit_iphone_scam/){:target="_blank" rel="noopener"}

> Sent 5,000+ fake handsets to Apple for repair in hope of getting real ones back Two Chinese nationals are facing a maximum of 20 years in prison after being convicted of mailing thousands of fake iPhones to Apple for repair in the hope they'd be replaced with new handsets.... [...]
