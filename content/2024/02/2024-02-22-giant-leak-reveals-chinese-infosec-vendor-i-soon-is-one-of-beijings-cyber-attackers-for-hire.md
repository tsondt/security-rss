Title: Giant leak reveals Chinese infosec vendor I-Soon is one of Beijing's cyber-attackers for hire
Date: 2024-02-22T06:31:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-02-22-giant-leak-reveals-chinese-infosec-vendor-i-soon-is-one-of-beijings-cyber-attackers-for-hire

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/22/i_soon_china_infosec_leak/){:target="_blank" rel="noopener"}

> Trove reveals RATs that can pop major OSes, campaigns against offshore and local targets A cache of stolen documents posted to GitHub appears to reveal how a Chinese infosec vendor named I-Soon offers rent-a-hacker services for Beijing.... [...]
