Title: A startup allegedly “hacked the world.” Then came the censorship—and now the backlash.
Date: 2024-02-02T19:26:43+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Security;hackers;syndication
Slug: 2024-02-02-a-startup-allegedly-hacked-the-world-then-came-the-censorshipand-now-the-backlash

[Source](https://arstechnica.com/?p=2000875){:target="_blank" rel="noopener"}

> Enlarge (credit: WIRED staff/Getty Images ) Hacker-for-hire firms like NSO Group and Hacking Team have become notorious for enabling their customers to spy on vulnerable members of civil society. But as far back as a decade ago in India, a startup called Appin Technology and its subsidiaries allegedly played a similar cyber-mercenary role while attracting far less attention. Over the past two years, a collection of people with direct and indirect links to that company have been working to keep it that way, using a campaign of legal threats to silence publishers and anyone else reporting on Appin Technology’s alleged [...]
