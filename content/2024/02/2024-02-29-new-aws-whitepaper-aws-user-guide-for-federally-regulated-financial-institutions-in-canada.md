Title: New AWS whitepaper: AWS User Guide for Federally Regulated Financial Institutions in Canada
Date: 2024-02-29T18:48:09+00:00
Author: Dan MacKay
Category: AWS Security
Tags: Announcements;Compliance;Foundational (100);Security, Identity, & Compliance;AWS Compliance;AWS Financial Services Guide;AWS security;banking;Canada;Cyber Risk Management;Financial Services;OSFI;Security;Security Blog
Slug: 2024-02-29-new-aws-whitepaper-aws-user-guide-for-federally-regulated-financial-institutions-in-canada

[Source](https://aws.amazon.com/blogs/security/new-aws-whitepaper-aws-user-guide-for-federally-regulated-financial-institutions-in-canada/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) has released a new whitepaper to help financial services customers in Canada accelerate their use of the AWS Cloud. The new AWS User Guide for Federally Regulated Financial Institutions in Canada helps AWS customers navigate the regulatory expectations of the Office of the Superintendent of Financial Institutions (OSFI) in a shared responsibility environment. [...]
