Title: Mon Dieu! Nearly half the French population have data nabbed in massive breach
Date: 2024-02-12T07:27:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-02-12-mon-dieu-nearly-half-the-french-population-have-data-nabbed-in-massive-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/12/infosec_news_roundup/){:target="_blank" rel="noopener"}

> PLUS: Juniper's support portal leaks customer info; Canada moves to ban Flipper Zero; Critical vulns Infosec In Brief Nearly half the citizens of France have had their data exposed in a massive security breach at two third-party healthcare payment servicers, the French data privacy watchdog disclosed last week.... [...]
