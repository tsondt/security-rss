Title: A common goal for European cyber security
Date: 2024-02-21T08:21:09+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2024-02-21-a-common-goal-for-european-cyber-security

[Source](https://go.theregister.com/feed/www.theregister.com/2024/02/21/a_common_goal_for_european/){:target="_blank" rel="noopener"}

> Complying with the EU’s NIS2 Directive Webinar It was growing threat levels and an increase in reported cybersecurity attacks since digitalization which pushed the European Union to introduce the original Network and Information Security (NIS) Directive in 2016.... [...]
