Title: Deepfake Fraud
Date: 2024-02-05T16:10:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;deepfake;fraud
Slug: 2024-02-05-deepfake-fraud

[Source](https://www.schneier.com/blog/archives/2024/02/deepfake-fraud.html){:target="_blank" rel="noopener"}

> A deepfake video conference call—with everyone else on the call a fake— fooled a finance worker into sending $25M to the criminals’ account. [...]
