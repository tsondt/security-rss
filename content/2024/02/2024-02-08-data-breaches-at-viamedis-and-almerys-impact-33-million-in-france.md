Title: Data breaches at Viamedis and Almerys impact 33 million in France
Date: 2024-02-08T10:44:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-02-08-data-breaches-at-viamedis-and-almerys-impact-33-million-in-france

[Source](https://www.bleepingcomputer.com/news/security/data-breaches-at-viamedis-and-almerys-impact-33-million-in-france/){:target="_blank" rel="noopener"}

> Data breaches at two French healthcare payment service providers, Viamedis and Almerys, have now been determined to impact over 33 million people in the country. [...]
