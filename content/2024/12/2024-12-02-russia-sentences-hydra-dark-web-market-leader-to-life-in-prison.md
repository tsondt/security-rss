Title: Russia sentences Hydra dark web market leader to life in prison
Date: 2024-12-02T14:40:44-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-02-russia-sentences-hydra-dark-web-market-leader-to-life-in-prison

[Source](https://www.bleepingcomputer.com/news/security/russia-sentences-hydra-dark-web-market-leader-to-life-in-prison/){:target="_blank" rel="noopener"}

> Russian authorities have sentenced the leader of the criminal group behind the now-closed dark web platform Hydra Market to life in prison. [...]
