Title: Details about the iOS Inactivity Reboot Feature
Date: 2024-12-02T12:08:40+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;iOS;iPhone;law enforcement;reverse engineering
Slug: 2024-12-02-details-about-the-ios-inactivity-reboot-feature

[Source](https://www.schneier.com/blog/archives/2024/12/details-about-the-ios-inactivity-reboot-feature.html){:target="_blank" rel="noopener"}

> I recently wrote about the new iOS feature that forces an iPhone to reboot after it’s been inactive for a longish period of time. Here are the technical details, discovered through reverse engineering. The feature triggers after seventy-two hours of inactivity, even it is remains connected to Wi-Fi. [...]
