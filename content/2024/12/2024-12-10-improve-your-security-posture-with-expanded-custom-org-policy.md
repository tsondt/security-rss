Title: Improve your security posture with expanded Custom Org Policy
Date: 2024-12-10T20:00:00+00:00
Author: Akshay Datar
Category: GCP Security
Tags: Containers & Kubernetes;Security & Identity
Slug: 2024-12-10-improve-your-security-posture-with-expanded-custom-org-policy

[Source](https://cloud.google.com/blog/products/identity-security/announcing-expanded-custom-org-policy-portfolio-of-supported-products/){:target="_blank" rel="noopener"}

> When it comes to securing cloud resources, one of the most important tools for administrators is the ability to set guardrails for resource configurations that can be applied consistently across the environment, centrally managed, and safely rolled out. Google Cloud's custom Organization Policy is a powerful tool that can help organizations safeguard cloud resources. Administrators can use custom organization policies to set granular resource configurations in order to enhance security posture, address regulatory requirements, and increase operational efficiencies, all without impacting development velocity. Today, we are excited to announce that custom Org Policy is now adding support for more than [...]
