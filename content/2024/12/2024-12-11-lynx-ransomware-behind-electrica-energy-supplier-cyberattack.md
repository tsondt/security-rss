Title: Lynx ransomware behind Electrica energy supplier cyberattack
Date: 2024-12-11T11:28:50-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-11-lynx-ransomware-behind-electrica-energy-supplier-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/lynx-ransomware-behind-electrica-energy-supplier-cyberattack/){:target="_blank" rel="noopener"}

> ​The Romanian National Cybersecurity Directorate (DNSC) says the Lynx ransomware gang breached Electrica Group, one of the largest electricity suppliers in the country. [...]
