Title: How Cryptocurrency Turns to Cash in Russian Banks
Date: 2024-12-11T21:38:48+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Russia's War on Ukraine;Web Fraud 2.0;Binance;Blaven Technologies;Chainalysis;CloudFlare;Cryptomus;CTV News;FINTRAC;Icon Tech SRO;Investigative Journalism Foundation;Mezhundarondnaya IBU SRO;Peter German;PQ Hosting;RCMP;Richard Sanders;Vira Krychka;WS Management and Advisory Corporation Ltd;Xeltox Enterprises
Slug: 2024-12-11-how-cryptocurrency-turns-to-cash-in-russian-banks

[Source](https://krebsonsecurity.com/2024/12/how-cryptocurrency-turns-to-cash-in-russian-banks/){:target="_blank" rel="noopener"}

> A financial firm registered in Canada has emerged as the payment processor for dozens of Russian cryptocurrency exchanges and websites hawking cybercrime services aimed at Russian-speaking customers, new research finds. Meanwhile, an investigation into the Vancouver street address used by this company shows it is home to dozens of foreign currency dealers, money transfer businesses, and cryptocurrency exchanges — none of which are physically located there. Richard Sanders is a blockchain analyst and investigator who advises the law enforcement and intelligence community. Sanders spent most of 2023 in Ukraine, traveling with Ukrainian soldiers while mapping the shifting landscape of Russian [...]
