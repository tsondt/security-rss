Title: Clop ransomware claims responsibility for Cleo data theft attacks
Date: 2024-12-15T15:15:00-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-12-15-clop-ransomware-claims-responsibility-for-cleo-data-theft-attacks

[Source](https://www.bleepingcomputer.com/news/security/clop-ransomware-claims-responsibility-for-cleo-data-theft-attacks/){:target="_blank" rel="noopener"}

> The Clop ransomware gang has confirmed to BleepingComputer that they are behind the recent Cleo data-theft attacks, utilizing zero-day exploits to breach corporate networks and steal data. [...]
