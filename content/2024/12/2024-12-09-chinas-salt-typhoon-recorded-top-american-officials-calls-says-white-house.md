Title: China's Salt Typhoon recorded top American officials' calls, says White House
Date: 2024-12-09T19:01:40+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-12-09-chinas-salt-typhoon-recorded-top-american-officials-calls-says-white-house

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/09/white_house_salt_typhoon/){:target="_blank" rel="noopener"}

> No word yet on who was snooped on. Any bets? Chinese cyberspies recorded "very senior" US political figures' calls, according to White House security boss Anne Neuberger.... [...]
