Title: Yearlong supply-chain attack targeting security pros steals 390K credentials
Date: 2024-12-13T21:46:18+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;credential theft;cryptomining;GitHub;npm;supply chain attacks
Slug: 2024-12-13-yearlong-supply-chain-attack-targeting-security-pros-steals-390k-credentials

[Source](https://arstechnica.com/security/2024/12/yearlong-supply-chain-attack-targeting-security-pros-steals-390k-credentials/){:target="_blank" rel="noopener"}

> A sophisticated and ongoing supply-chain attack operating for the past year has been stealing sensitive login credentials from both malicious and benevolent security personnel by infecting them with Trojanized versions of open source software from GitHub and NPM, researchers said. The campaign, first reported three weeks ago by security firm Checkmarx and again on Friday by Datadog Security Labs, uses multiple avenues to infect the devices of researchers in security and other technical fields. One is through packages that have been available on open source repositories for over a year. They install a professionally developed backdoor that takes pains to [...]
