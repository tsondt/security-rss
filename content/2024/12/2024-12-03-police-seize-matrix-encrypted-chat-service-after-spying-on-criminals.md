Title: Police seize Matrix encrypted chat service after spying on criminals
Date: 2024-12-03T10:27:43-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-12-03-police-seize-matrix-encrypted-chat-service-after-spying-on-criminals

[Source](https://www.bleepingcomputer.com/news/security/police-seize-matrix-encrypted-chat-service-after-spying-on-criminals/){:target="_blank" rel="noopener"}

> An international law enforcement operation codenamed 'Operation Passionflower' has shut down MATRIX, an encrypted messaging platform used by cybercriminals to coordinate illegal activities while evading police. [...]
