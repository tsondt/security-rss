Title: QR codes bypass browser isolation for malicious C2 communication
Date: 2024-12-08T10:27:37-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-12-08-qr-codes-bypass-browser-isolation-for-malicious-c2-communication

[Source](https://www.bleepingcomputer.com/news/security/qr-codes-bypass-browser-isolation-for-malicious-c2-communication/){:target="_blank" rel="noopener"}

> Mandiant has identified a novel method to bypass contemporary browser isolation technology and achieve command-and-control C2 operations. [...]
