Title: Vodka maker Stoli files for bankruptcy in US after ransomware attack
Date: 2024-12-03T17:00:24-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-03-vodka-maker-stoli-files-for-bankruptcy-in-us-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/vodka-maker-stoli-files-for-bankruptcy-in-us-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Stoli Group's U.S. companies have filed for bankruptcy following an August ransomware attack and Russian authorities seizing the company's remaining distilleries in the country. [...]
