Title: Auto parts giant LKQ says cyberattack disrupted Canadian business unit
Date: 2024-12-13T18:31:24-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-12-13-auto-parts-giant-lkq-says-cyberattack-disrupted-canadian-business-unit

[Source](https://www.bleepingcomputer.com/news/security/auto-parts-giant-lkq-says-cyberattack-disrupted-canadian-business-unit/){:target="_blank" rel="noopener"}

> Automobile parts giant LKQ Corporation disclosed that one of its business units in Canada was hacked, allowing threat actors to steal data from the company. [...]
