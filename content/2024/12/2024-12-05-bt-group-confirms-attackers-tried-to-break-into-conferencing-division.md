Title: BT Group confirms attackers tried to break into Conferencing division
Date: 2024-12-05T11:03:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-12-05-bt-group-confirms-attackers-tried-to-break-into-conferencing-division

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/05/bt_group_confirms_attempted_attack/){:target="_blank" rel="noopener"}

> Sensitive data allegedly stolen from US subsidiary following Black Basta post BT Group confirmed it is dealing with an attempted attack on one of its legacy business units after the Black Basta ransomware group claimed they broke in.... [...]
