Title: Google Cloud and Swift pioneer advanced AI and federated learning tech to help combat payments fraud
Date: 2024-12-10T17:00:00+00:00
Author: Arun Santhanagopalan
Category: GCP Security
Tags: AI & Machine Learning;Security & Identity
Slug: 2024-12-10-google-cloud-and-swift-pioneer-advanced-ai-and-federated-learning-tech-to-help-combat-payments-fraud

[Source](https://cloud.google.com/blog/products/identity-security/google-cloud-and-swift-pioneer-advanced-ai-and-federated-learning-tech/){:target="_blank" rel="noopener"}

> Conventional fraud detection methods have a hard time keeping up with increasingly sophisticated criminal tactics. Existing systems often rely on the limited data of individual institutions, and this hinders the detection of intricate schemes that span multiple banks and jurisdictions. To better combat fraud in cross-border payments, Swift, the global provider of secure financial messaging services, is working with Google Cloud to develop anti-fraud technologies that use advanced AI and federated learning. In the first half of 2025, Swift plans to roll out a sandbox with synthetic data to prototype learning from historic fraud, working with 12 global financial institutions, [...]
