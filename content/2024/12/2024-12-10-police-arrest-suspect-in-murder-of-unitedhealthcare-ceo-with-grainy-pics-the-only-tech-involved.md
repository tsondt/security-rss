Title: Police arrest suspect in murder of UnitedHealthcare CEO, with grainy pics the only tech involved
Date: 2024-12-10T00:58:14+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-12-10-police-arrest-suspect-in-murder-of-unitedhealthcare-ceo-with-grainy-pics-the-only-tech-involved

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/10/unitedhealthcare_shooting_camera/){:target="_blank" rel="noopener"}

> McDonald's worker called it in, cops swooped, found 'gun, suppressor, manifesto' Police in Pennsylvania have arrested a man suspected of shooting dead the CEO of insurer UnitedHealthcare in New York City, thanks to a McDonald's employee who recognized the suspect in a burger joint – and largely without help from technology.... [...]
