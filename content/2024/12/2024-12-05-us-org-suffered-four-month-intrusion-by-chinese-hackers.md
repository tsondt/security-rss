Title: U.S. org suffered four month intrusion by Chinese hackers
Date: 2024-12-05T17:15:05-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-12-05-us-org-suffered-four-month-intrusion-by-chinese-hackers

[Source](https://www.bleepingcomputer.com/news/security/us-org-suffered-four-month-intrusion-by-chinese-hackers/){:target="_blank" rel="noopener"}

> A large U.S. organization with significant presence in China has been reportedly breached by China-based threat actors who persisted on its networks from April to August 2024. [...]
