Title: Securing the future: building a culture of security
Date: 2024-12-11T21:42:59+00:00
Author: Carter Spriggs
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Thought Leadership;cybersecurity;report;Security Blog
Slug: 2024-12-11-securing-the-future-building-a-culture-of-security

[Source](https://aws.amazon.com/blogs/security/securing-the-future-building-a-culture-of-security/){:target="_blank" rel="noopener"}

> According to a 2024 Verizon report, nearly 70% of data breaches occurred because a person was manipulated by social engineering or made some type of error. This highlights the importance of human-layer defenses in an organization’s security strategy. In addition to technology, tools, and processes, security requires awareness and action from everyone in an organization [...]
