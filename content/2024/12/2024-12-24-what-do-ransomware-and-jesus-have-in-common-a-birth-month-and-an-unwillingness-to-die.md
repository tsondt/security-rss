Title: What do ransomware and Jesus have in common? A birth month and an unwillingness to die
Date: 2024-12-24T11:31:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-12-24-what-do-ransomware-and-jesus-have-in-common-a-birth-month-and-an-unwillingness-to-die

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/24/ransomware_in_2024/){:target="_blank" rel="noopener"}

> 35 years since AIDS first borked a PC and we're still no closer to a solution Feature Your Christmas holidays looked quite different in the '80s to how they do today. While some will remember what it was like to wake up on the 25th back then, some of you won't even have been born. The food hasn't changed much. Turkey, stuffing, Brussels sprouts... that's all been around for some time.... [...]
