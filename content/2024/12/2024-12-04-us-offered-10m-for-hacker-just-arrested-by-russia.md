Title: U.S. Offered $10M for Hacker Just Arrested by Russia
Date: 2024-12-04T14:08:45+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Aleksandr Ermakov;Boriselcin;Daryna Antoniuk;Intel 471;Mikhail Lenin;Mikhail Matveev;Mikhail Shefel;rescator;Shtazi-IT;Sugarlocker;Wazawaka
Slug: 2024-12-04-us-offered-10m-for-hacker-just-arrested-by-russia

[Source](https://krebsonsecurity.com/2024/12/u-s-offered-10m-for-hacker-just-arrested-by-russia/){:target="_blank" rel="noopener"}

> In January 2022, KrebsOnSecurity identified a Russian man named Mikhail Matveev as “ Wazawaka,” a cybercriminal who was deeply involved in the formation and operation of multiple ransomware groups. The U.S. government indicted Matveev as a top ransomware purveyor a year later, offering $10 million for information leading to his arrest. Last week, the Russian government reportedly arrested Matveev and charged him with creating malware used to extort companies. An FBI wanted poster for Matveev. Matveev, a.k.a. “Wazawaka” and “ Boriselcin ” worked with at least three different ransomware gangs that extorted hundreds of millions of dollars from companies, schools, [...]
