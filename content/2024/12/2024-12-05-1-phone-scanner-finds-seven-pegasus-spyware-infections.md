Title: $1 phone scanner finds seven Pegasus spyware infections
Date: 2024-12-05T17:00:06+00:00
Author: Lily Hay Newman, wired.com
Category: Ars Technica
Tags: Biz & IT;Security;nso group;pegasus;spyware;syndication
Slug: 2024-12-05-1-phone-scanner-finds-seven-pegasus-spyware-infections

[Source](https://arstechnica.com/security/2024/12/1-phone-scanner-finds-seven-pegasus-spyware-infections/){:target="_blank" rel="noopener"}

> In recent years, commercial spyware has been deployed by more actors against a wider range of victims, but the prevailing narrative has still been that the malware is used in targeted attacks against an extremely small number of people. At the same time, though, it has been difficult to check devices for infection, leading individuals to navigate an ad hoc array of academic institutions and NGOs that have been on the front lines of developing forensic techniques to detect mobile spyware. On Tuesday, the mobile device security firm iVerify is publishing findings from a spyware detection feature it launched in [...]
