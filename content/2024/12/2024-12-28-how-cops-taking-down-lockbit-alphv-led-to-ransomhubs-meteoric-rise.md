Title: How cops taking down LockBit, ALPHV led to RansomHub's meteoric rise
Date: 2024-12-28T12:34:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-12-28-how-cops-taking-down-lockbit-alphv-led-to-ransomhubs-meteoric-rise

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/28/lockbit_alphv_disruptions_ransomhub_rise/){:target="_blank" rel="noopener"}

> Cut off one head, two more grow back in its place RansomHub, the ransomware collective that emerged earlier this year, quickly gained momentum, outpacing its criminal colleagues and hitting its victims especially hard. The group named and shamed hundreds of organizations on its leak site, while demanding exorbitant payments across various industries.... [...]
