Title: Heart surgery device maker's security bypassed, data encrypted and stolen
Date: 2024-12-10T12:30:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-12-10-heart-surgery-device-makers-security-bypassed-data-encrypted-and-stolen

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/10/artivion_security_incident/){:target="_blank" rel="noopener"}

> Sounds like th-aorta get this sorted quickly A manufacturer of devices used in heart surgeries says it's dealing with "a cybersecurity incident" that bears all the hallmarks of a ransomware attack.... [...]
