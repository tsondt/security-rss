Title: US Treasury Department breached through remote support platform
Date: 2024-12-30T17:19:42-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-12-30-us-treasury-department-breached-through-remote-support-platform

[Source](https://www.bleepingcomputer.com/news/security/us-treasury-department-breached-through-remote-support-platform/){:target="_blank" rel="noopener"}

> Chinese state-sponsored threat actors hacked the U.S. Treasury Department after breaching a remote support platform used by the federal agency. [...]
