Title: The Cyber Threat Intelligence Program Design Playbook is now available
Date: 2024-12-16T17:00:00+00:00
Author: Brett Reschke
Category: GCP Security
Tags: Security & Identity
Slug: 2024-12-16-the-cyber-threat-intelligence-program-design-playbook-is-now-available

[Source](https://cloud.google.com/blog/products/identity-security/cti-program-design-playbook-is-now-available/){:target="_blank" rel="noopener"}

> As cybersecurity threats have grown more sophisticated and prevalent, we’ve seen organizations develop robust cyber threat intelligence (CTI) programs to help bolster defenses. However, creating and maturing a CTI program remains a challenge because it requires people, processes, technologies, and metrics to validate success. To help organizations better operationalize threat intelligence, we’ve published the CTI Program Design Playbook. This training track is the inaugural release from Mandiant Academy’s newest on-demand training, developed for professionals who actively defend networks. It’s designed to provide you with the essential knowledge and skills to design, build, operate, and optimize a CTI program from the [...]
