Title: Lights out for 18 more DDoS booters in pre-Christmas Operation PowerOFF push
Date: 2024-12-12T12:01:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-12-12-lights-out-for-18-more-ddos-booters-in-pre-christmas-operation-poweroff-push

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/12/operation_poweroff_ddos_takedowns/){:target="_blank" rel="noopener"}

> Holiday cheer comes in the form of three arrests and 27 shuttered domains The Europol-coordinated Operation PowerOFF struck again this week as cross-border cops pulled the plug on 27 more domains tied to distributed denial of service (DDoS) criminality.... [...]
