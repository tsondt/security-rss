Title: Ransomware scum blow holes in Cleo software patches, Cl0p (sort of) claims responsibility
Date: 2024-12-16T23:45:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-12-16-ransomware-scum-blow-holes-in-cleo-software-patches-cl0p-sort-of-claims-responsibility

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/16/ransomware_attacks_exploit_cleo_bug/){:target="_blank" rel="noopener"}

> But can you really take crims at their word? Supply chain integration vendor Cleo has urged its customers to upgrade three of its products after an October security update was circumvented, leading to widespread ransomware attacks that Russia-linked gang Cl0p has claimed are its evil work.... [...]
