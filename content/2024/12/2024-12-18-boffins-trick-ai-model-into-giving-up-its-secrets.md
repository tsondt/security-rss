Title: Boffins trick AI model into giving up its secrets
Date: 2024-12-18T15:30:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-12-18-boffins-trick-ai-model-into-giving-up-its-secrets

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/18/ai_model_reveal_itself/){:target="_blank" rel="noopener"}

> All it took to make an Google Edge TPU give up model hyperparameters was specific hardware, a novel attack technique... and several days Computer scientists from North Carolina State University have devised a way to copy AI models running on Google Edge Tensor Processing Units (TPUs), as used in Google Pixel phones and third-party machine learning accelerators.... [...]
