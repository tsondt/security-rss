Title: OpenWrt orders router firmware updates after supply chain attack scare
Date: 2024-12-09T14:04:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-12-09-openwrt-orders-router-firmware-updates-after-supply-chain-attack-scare

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/09/openwrt_firmware_vulnerabilities/){:target="_blank" rel="noopener"}

> A couple of bugs lead to a potentially bad time OpenWrt users should upgrade their images to the same version to protect themselves from a possible supply chain attack reported to the open source Wi-Fi router project last week.... [...]
