Title: OpenWrt Sysupgrade flaw let hackers push malicious firmware images
Date: 2024-12-09T17:33:02-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2024-12-09-openwrt-sysupgrade-flaw-let-hackers-push-malicious-firmware-images

[Source](https://www.bleepingcomputer.com/news/security/openwrt-sysupgrade-flaw-let-hackers-push-malicious-firmware-images/){:target="_blank" rel="noopener"}

> A flaw in OpenWrt's Attended Sysupgrade feature used to build custom, on-demand firmware images could have allowed for the distribution of malicious firmware packages. [...]
