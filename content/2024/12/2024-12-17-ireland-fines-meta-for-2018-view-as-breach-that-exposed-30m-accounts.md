Title: Ireland fines Meta for 2018 'View As' breach that exposed 30M accounts
Date: 2024-12-17T15:30:15+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-12-17-ireland-fines-meta-for-2018-view-as-breach-that-exposed-30m-accounts

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/17/ireland_fines_meta_for_2018/){:target="_blank" rel="noopener"}

> €251 million? Zuck can find that in his couch cushions, but Meta still vows to appeal It's been six years since miscreants abused some sloppy Facebook code to steal access tokens belonging to 30 million users, and the slow-turning wheels of Irish justice have finally caught up with a €251 million ($264 million) fine for the social media biz.... [...]
