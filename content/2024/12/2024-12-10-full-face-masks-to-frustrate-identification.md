Title: Full-Face Masks to Frustrate Identification
Date: 2024-12-10T12:06:20+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;identification
Slug: 2024-12-10-full-face-masks-to-frustrate-identification

[Source](https://www.schneier.com/blog/archives/2024/12/full-face-masks-to-frustrate-identification.html){:target="_blank" rel="noopener"}

> This is going to be interesting. It’s a video of someone trying on a variety of printed full-face masks. They won’t fool anyone for long, but will survive casual scrutiny. And they’re cheap and easy to swap. [...]
