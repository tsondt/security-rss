Title: Algorithms Are Coming for Democracy—but It’s Not All Bad
Date: 2024-12-03T12:00:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;AI;democracy
Slug: 2024-12-03-algorithms-are-coming-for-democracybut-its-not-all-bad

[Source](https://www.schneier.com/blog/archives/2024/12/algorithms-are-coming-for-democracy-but-its-not-all-bad.html){:target="_blank" rel="noopener"}

> In 2025, AI is poised to change every aspect of democratic politics —but it won’t necessarily be for the worse. India’s prime minister, Narendra Modi, has used AI to translate his speeches for his multilingual electorate in real time, demonstrating how AI can help diverse democracies to be more inclusive. AI avatars were used by presidential candidates in South Korea in electioneering, enabling them to provide answers to thousands of voters’ questions simultaneously. We are also starting to see AI tools aid fundraising and get-out-the-vote efforts. AI techniques are starting to augment more traditional polling methods, helping campaigns get cheaper [...]
