Title: One third of adults can't delete device data
Date: 2024-12-24T09:29:09+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-12-24-one-third-of-adults-cant-delete-device-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/24/uk_device_data_deletion/){:target="_blank" rel="noopener"}

> Easier to let those old phones gather dust in a drawer, survey finds The UK's Information Commissioner's Office (ICO) has warned that many adults don't know how to wipe their old devices, and a worrying number of young people just don't care.... [...]
