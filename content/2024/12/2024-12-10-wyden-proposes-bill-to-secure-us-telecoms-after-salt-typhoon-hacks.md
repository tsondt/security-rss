Title: Wyden proposes bill to secure US telecoms after Salt Typhoon hacks
Date: 2024-12-10T16:38:26-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Government
Slug: 2024-12-10-wyden-proposes-bill-to-secure-us-telecoms-after-salt-typhoon-hacks

[Source](https://www.bleepingcomputer.com/news/security/wyden-proposes-bill-to-secure-us-telecoms-after-salt-typhoon-hacks/){:target="_blank" rel="noopener"}

> U.S. Senator Ron Wyden of Oregon announced a new bill to secure the networks of American telecommunications companies breached by Salt Typhoon Chinese state hackers earlier this year. [...]
