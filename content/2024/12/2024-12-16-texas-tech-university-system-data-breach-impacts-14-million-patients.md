Title: Texas Tech University System data breach impacts 1.4 million patients
Date: 2024-12-16T17:17:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-12-16-texas-tech-university-system-data-breach-impacts-14-million-patients

[Source](https://www.bleepingcomputer.com/news/security/texas-tech-university-system-data-breach-impacts-14-million-patients/){:target="_blank" rel="noopener"}

> The Texas Tech University Health Sciences Center and its El Paso counterpart suffered a cyberattack that disrupted computer systems and applications, potentially exposing the data of 1.4 million patients. [...]
