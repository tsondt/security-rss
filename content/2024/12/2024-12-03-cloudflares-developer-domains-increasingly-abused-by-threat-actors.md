Title: Cloudflare’s developer domains increasingly abused by threat actors
Date: 2024-12-03T16:00:32-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-12-03-cloudflares-developer-domains-increasingly-abused-by-threat-actors

[Source](https://www.bleepingcomputer.com/news/security/cloudflares-developer-domains-increasingly-abused-by-threat-actors/){:target="_blank" rel="noopener"}

> Cloudflare's 'pages.dev' and 'workers.dev' domains, used for deploying web pages and facilitating serverless computing, are being increasingly abused by cybercriminals for phishing and other malicious activities. [...]
