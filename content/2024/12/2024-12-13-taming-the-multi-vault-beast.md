Title: Taming the multi-vault beast
Date: 2024-12-13T09:02:09+00:00
Author: Thomas Segura, Technical Content Writer, GitGuardian
Category: The Register
Tags: 
Slug: 2024-12-13-taming-the-multi-vault-beast

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/13/taming_the_multivault_beast/){:target="_blank" rel="noopener"}

> GitGuardian takes on enterprise secrets sprawl Partner Content With Non-Human Identities (NHIs) now outnumbering human users 100 to one in enterprise environments, managing secrets across multiple vaults has become a significant security concern.... [...]
