Title: Passkey technology is elegant, but it’s most definitely not usable security
Date: 2024-12-30T12:00:53+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Features;Security;login;passkeys;passwords;phishing;signin
Slug: 2024-12-30-passkey-technology-is-elegant-but-its-most-definitely-not-usable-security

[Source](https://arstechnica.com/security/2024/12/passkey-technology-is-elegant-but-its-most-definitely-not-usable-security/){:target="_blank" rel="noopener"}

> It's that time again, when families and friends gather and implore the more technically inclined among them to troubleshoot problems they're having behind the device screens all around them. One of the most vexing and most common problems is logging into accounts in a way that's both secure and reliable. Using the same password everywhere is easy, but in an age of mass data breaches and precision-orchestrated phishing attacks, it's also highly unadvisable. Then again, creating hundreds of unique passwords, storing them securely, and keeping them out of the hands of phishers and database hackers is hard enough for experts, [...]
