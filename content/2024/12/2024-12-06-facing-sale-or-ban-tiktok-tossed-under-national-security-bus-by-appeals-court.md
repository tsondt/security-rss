Title: Facing sale or ban, TikTok tossed under national security bus by appeals court
Date: 2024-12-06T22:00:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-12-06-facing-sale-or-ban-tiktok-tossed-under-national-security-bus-by-appeals-court

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/06/appeals_court_backs_tiktok_ban/){:target="_blank" rel="noopener"}

> Video slinger looks to Supremes for salvation, though anything could happen under Trump A US federal appeals court has rejected a challenge to the law that prevents popular apps that collect data on Americans from being controlled by a foreign adversary.... [...]
