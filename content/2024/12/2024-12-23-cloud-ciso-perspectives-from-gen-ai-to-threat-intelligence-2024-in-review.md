Title: Cloud CISO Perspectives: From gen AI to threat intelligence: 2024 in review
Date: 2024-12-23T17:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2024-12-23-cloud-ciso-perspectives-from-gen-ai-to-threat-intelligence-2024-in-review

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-from-gen-AI-to-threat-intelligence-2024-in-review/){:target="_blank" rel="noopener"}

> Welcome to the second Cloud CISO Perspectives for December 2024. To close out the year, I’m sharing the top Google Cloud security updates in 2024 that attracted the most interest from the security community. There’s a lot of AI, of course, as well as a few surprises. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI Security & CISO, Google Cloud aside_block <ListValue: [StructValue([('title', 'Get vital board insights [...]
