Title: Over 3.1 million fake "stars" on GitHub projects used to boost rankings
Date: 2024-12-31T10:13:12-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2024-12-31-over-31-million-fake-stars-on-github-projects-used-to-boost-rankings

[Source](https://www.bleepingcomputer.com/news/security/over-31-million-fake-stars-on-github-projects-used-to-boost-rankings/){:target="_blank" rel="noopener"}

> GitHub has a problem with inauthentic "stars" used to artificially inflate the popularity of scam and malware distribution repositories, helping them reach more unsuspecting users. [...]
