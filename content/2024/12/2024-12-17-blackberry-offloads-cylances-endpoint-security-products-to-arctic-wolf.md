Title: BlackBerry offloads Cylance's endpoint security products to Arctic Wolf
Date: 2024-12-17T06:02:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-12-17-blackberry-offloads-cylances-endpoint-security-products-to-arctic-wolf

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/17/blackberry_cylance_sale_arctic_wolf/){:target="_blank" rel="noopener"}

> Fresh attempt to mix the perfect cocktail of IoT and Infosec BlackBerry's ambition to mix infosec and the Internet of Things has been squeezed, after the Canadian firm announced it is offloading Cylance's endpoint security products.... [...]
