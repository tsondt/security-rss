Title: Badass Russian techie outsmarts FSB, flees Putinland all while being tracked with spyware
Date: 2024-12-06T12:32:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-12-06-badass-russian-techie-outsmarts-fsb-flees-putinland-all-while-being-tracked-with-spyware

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/06/badass_russian_techie_outsmarts_fsb/){:target="_blank" rel="noopener"}

> Threatened with life in prison, Kyiv charity worker gives middle finger to state spies A Russian programmer defied the Federal Security Service (FSB) by publicizing the fact his phone was infected with spyware after being confiscated by authorities.... [...]
