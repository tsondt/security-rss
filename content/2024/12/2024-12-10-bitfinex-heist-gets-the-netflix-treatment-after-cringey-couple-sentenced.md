Title: Bitfinex heist gets the Netflix treatment after 'cringey couple' sentenced
Date: 2024-12-10T10:15:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-12-10-bitfinex-heist-gets-the-netflix-treatment-after-cringey-couple-sentenced

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/10/bitfinex_bitcoin_heist_netflix/){:target="_blank" rel="noopener"}

> Streamer's trademark dramatic style takes on Bitcoin Bonnie and Clyde A documentary examining the 2016 Bitfinex burglars hits Netflix, bringing the curious case to living rooms for the first time.... [...]
