Title: Why Phishers Love New TLDs Like .shop, .top and .xyz
Date: 2024-12-03T13:27:31+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;The Coming Storm;@chenlun;and Mobile Anti-Abuse Working Group;Anti-Phishing Working Group;Coalition Against Unsolicited Commercial Email;ICANN;Interisle Consulting;Internet Corporation for Assigned Names and Numbers;John Levine;malware;Messaging;new gTLDs;phishing;spam;U.S. Postal Service
Slug: 2024-12-03-why-phishers-love-new-tlds-like-shop-top-and-xyz

[Source](https://krebsonsecurity.com/2024/12/why-phishers-love-new-tlds-like-shop-top-and-xyz/){:target="_blank" rel="noopener"}

> Phishing attacks increased nearly 40 percent in the year ending August 2024, with much of that growth concentrated at a small number of new generic top-level domains (gTLDs) — such as.shop,.top,.xyz — that attract scammers with rock-bottom prices and no meaningful registration requirements, new research finds. Meanwhile, the nonprofit entity that oversees the domain name industry is moving forward with plans to introduce a slew of new gTLDs. Image: Shutterstock. A study on phishing data released by Interisle Consulting finds that new gTLDs introduced in the last few years command just 11 percent of the market for new domains, but [...]
