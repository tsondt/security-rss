Title: Shape the future of UK cyber security
Date: 2024-12-05T09:03:07+00:00
Author: Contributed by the SANS Institute and the Department for Science, Innovation and Technology
Category: The Register
Tags: 
Slug: 2024-12-05-shape-the-future-of-uk-cyber-security

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/05/shape_the_future_of_uk/){:target="_blank" rel="noopener"}

> Support the industry by sponsoring the UK Cyber Team Competition Partner Content The opportunity to identify, foster and nurture talented young people towards a cyber security career should always be grabbed with both hands.... [...]
