Title: WhatsApp finally fixes View Once flaw that allowed theft of supposedly vanishing pics
Date: 2024-12-10T07:30:09+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-12-10-whatsapp-finally-fixes-view-once-flaw-that-allowed-theft-of-supposedly-vanishing-pics

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/10/whatsapp_view_once/){:target="_blank" rel="noopener"}

> And it only took four months, tut WhatsApp has fixed a problem with its View Once feature, designed to protect people's privacy with automatically disappearing pictures and videos.... [...]
