Title: Malicious ads push Lumma infostealer via fake CAPTCHA pages
Date: 2024-12-16T14:32:07-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-12-16-malicious-ads-push-lumma-infostealer-via-fake-captcha-pages

[Source](https://www.bleepingcomputer.com/news/security/malicious-ads-push-lumma-infostealer-via-fake-captcha-pages/){:target="_blank" rel="noopener"}

> A large-scale malvertising campaign distributed the Lumma Stealer info-stealing malware through fake CAPTCHA verification pages that prompt users to run PowerShell commands to verify they are not a bot. [...]
