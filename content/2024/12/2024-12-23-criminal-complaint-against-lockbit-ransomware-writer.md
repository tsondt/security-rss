Title: Criminal Complaint against LockBit Ransomware Writer
Date: 2024-12-23T17:04:02+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;courts;cryptocurrency;ransomware
Slug: 2024-12-23-criminal-complaint-against-lockbit-ransomware-writer

[Source](https://www.schneier.com/blog/archives/2024/12/criminal-complaint-against-lockbit-ransomware-writer.html){:target="_blank" rel="noopener"}

> The Justice Department has published the criminal complaint against Dmitry Khoroshev, for building and maintaining the LockBit ransomware. [...]
