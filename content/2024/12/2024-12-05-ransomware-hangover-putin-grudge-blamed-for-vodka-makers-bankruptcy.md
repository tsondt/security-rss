Title: Ransomware hangover, Putin grudge blamed for vodka maker's bankruptcy
Date: 2024-12-05T08:30:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-12-05-ransomware-hangover-putin-grudge-blamed-for-vodka-makers-bankruptcy

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/05/putin_ransomware_stoli_group/){:target="_blank" rel="noopener"}

> Stoli Group on the rocks in the US Two US subsidiaries of alcohol giant Stoli Group filed for bankruptcy protection this week over financial difficulties exacerbated by an August ransomware attack.... [...]
