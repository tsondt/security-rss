Title: 390,000 WordPress accounts stolen from hackers in supply chain attack
Date: 2024-12-14T10:17:27-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-14-390000-wordpress-accounts-stolen-from-hackers-in-supply-chain-attack

[Source](https://www.bleepingcomputer.com/news/security/390-000-wordpress-accounts-stolen-from-hackers-in-supply-chain-attack/){:target="_blank" rel="noopener"}

> A threat actor tracked as MUT-1244 has stolen over 390,000 WordPress credentials in a large-scale, year-long campaign targeting other threat actors using a trojanized WordPress credentials checker. [...]
