Title: FTC bans data brokers from selling Americans’ sensitive location data
Date: 2024-12-03T11:01:45-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-03-ftc-bans-data-brokers-from-selling-americans-sensitive-location-data

[Source](https://www.bleepingcomputer.com/news/security/ftc-bans-data-brokers-from-selling-americans-sensitive-location-data/){:target="_blank" rel="noopener"}

> Today, the FTC banned data brokers Mobilewalla and Gravy Analytics from harvesting and selling Americans' location tracking data linked to sensitive locations, like churches, healthcare facilities, military installations, and schools. [...]
