Title: WPForms bug allows Stripe refunds on millions of WordPress sites
Date: 2024-12-10T15:00:10-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-12-10-wpforms-bug-allows-stripe-refunds-on-millions-of-wordpress-sites

[Source](https://www.bleepingcomputer.com/news/security/wpforms-bug-allows-stripe-refunds-on-millions-of-wordpress-sites/){:target="_blank" rel="noopener"}

> A vulnerability in WPForms, a WordPress plugin used in over 6 million websites, could allow subscriber-level users to issue arbitrary Stripe refunds or cancel subscriptions. [...]
