Title: Trust Issues in AI
Date: 2024-12-09T12:01:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;AI;LLM
Slug: 2024-12-09-trust-issues-in-ai

[Source](https://www.schneier.com/blog/archives/2024/12/trust-issues-in-ai.html){:target="_blank" rel="noopener"}

> For a technology that seems startling in its modernity, AI sure has a long history. Google Translate, OpenAI chatbots, and Meta AI image generators are built on decades of advancements in linguistics, signal processing, statistics, and other fields going back to the early days of computing—and, often, on seed funding from the U.S. Department of Defense. But today’s tools are hardly the intentional product of the diverse generations of innovators that came before. We agree with Morozov that the “refuseniks,” as he calls them, are wrong to see AI as “irreparably tainted” by its origins. AI is better understood as [...]
