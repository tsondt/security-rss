Title: Citrix shares mitigations for ongoing Netscaler password spray attacks
Date: 2024-12-13T17:10:23-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-12-13-citrix-shares-mitigations-for-ongoing-netscaler-password-spray-attacks

[Source](https://www.bleepingcomputer.com/news/security/citrix-shares-mitigations-for-ongoing-netscaler-password-spray-attacks/){:target="_blank" rel="noopener"}

> Citrix Netscaler is the latest target in widespread password spray attacks targeting edge networking devices and cloud platforms this year to breach corporate networks. [...]
