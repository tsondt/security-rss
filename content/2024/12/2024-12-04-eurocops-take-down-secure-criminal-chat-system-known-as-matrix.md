Title: Eurocops take down 'secure' criminal chat system known as Matrix
Date: 2024-12-04T08:32:10+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-12-04-eurocops-take-down-secure-criminal-chat-system-known-as-matrix

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/04/eurocop_crack_matrix/){:target="_blank" rel="noopener"}

> They took the red pill Updated French and Dutch police have taken down the Matrix chat app, which was designed by criminals for criminals to be a secure encrypted messaging tool.... [...]
