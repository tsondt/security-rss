Title: New Android NoviSpy spyware linked to Qualcomm zero-day bugs
Date: 2024-12-16T10:06:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2024-12-16-new-android-novispy-spyware-linked-to-qualcomm-zero-day-bugs

[Source](https://www.bleepingcomputer.com/news/security/new-android-novispy-spyware-linked-to-qualcomm-zero-day-bugs/){:target="_blank" rel="noopener"}

> The Serbian government exploited Qualcomm zero-days to unlock and infect Android devices with a new spyware named 'NoviSpy,' used to spy on activists, journalists, and protestors. [...]
