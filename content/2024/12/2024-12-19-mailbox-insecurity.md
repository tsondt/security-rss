Title: Mailbox Insecurity
Date: 2024-12-19T15:24:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;locks;mail;physical security;theft
Slug: 2024-12-19-mailbox-insecurity

[Source](https://www.schneier.com/blog/archives/2024/12/mailbox-insecurity.html){:target="_blank" rel="noopener"}

> It turns out that all cluster mailboxes in the Denver area have the same master key. So if someone robs a postal carrier, they can open any mailbox. I get that a single master key makes the whole system easier, but it’s very fragile security. [...]
