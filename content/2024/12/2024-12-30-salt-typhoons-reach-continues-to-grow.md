Title: Salt Typhoon’s Reach Continues to Grow
Date: 2024-12-30T12:05:00+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;China;hacking;telecom
Slug: 2024-12-30-salt-typhoons-reach-continues-to-grow

[Source](https://www.schneier.com/blog/archives/2024/12/salt-typhoons-reach-continues-to-grow.html){:target="_blank" rel="noopener"}

> The US government has identified a ninth telecom that was successfully hacked by Salt Typhoon. [...]
