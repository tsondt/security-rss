Title: BT unit took servers offline after Black Basta ransomware breach
Date: 2024-12-04T13:37:32-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-04-bt-unit-took-servers-offline-after-black-basta-ransomware-breach

[Source](https://www.bleepingcomputer.com/news/security/bt-conferencing-division-took-servers-offline-after-black-basta-ransomware-attack/){:target="_blank" rel="noopener"}

> Multinational telecommunications giant BT Group (formerly British Telecom) has confirmed that its BT Conferencing business division shut down some of its servers following a Black Basta ransomware breach. [...]
