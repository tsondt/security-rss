Title: Radiant links $50 million crypto heist to North Korean hackers
Date: 2024-12-09T15:25:43-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-12-09-radiant-links-50-million-crypto-heist-to-north-korean-hackers

[Source](https://www.bleepingcomputer.com/news/security/radiant-links-50-million-crypto-heist-to-north-korean-hackers/){:target="_blank" rel="noopener"}

> Radiant Capital now says that North Korean threat actors are behind the $50 million cryptocurrency heist that occurred after hackers breached its systems in an October 16 cyberattack. [...]
