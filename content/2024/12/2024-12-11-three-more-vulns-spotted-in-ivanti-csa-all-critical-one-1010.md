Title: Three more vulns spotted in Ivanti CSA, all critical, one 10/10
Date: 2024-12-11T12:04:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-12-11-three-more-vulns-spotted-in-ivanti-csa-all-critical-one-1010

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/11/ivanti_vulns_critical/){:target="_blank" rel="noopener"}

> Patch up, everyone – that admin portal is mighty attractive to your friendly cyberattacker Ivanti just put out a security advisory warning of three critical vulnerabilities in its Cloud Services Application (CSA), including a perfect 10.... [...]
