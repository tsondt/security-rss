Title: Friday Squid Blogging: Safe Quick Undercarriage Immobilization Device
Date: 2024-12-06T22:05:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;law enforcement;squid
Slug: 2024-12-06-friday-squid-blogging-safe-quick-undercarriage-immobilization-device

[Source](https://www.schneier.com/blog/archives/2024/12/friday-squid-blogging-safe-quick-undercarriage-immobilization-device.html){:target="_blank" rel="noopener"}

> Fifteen years ago I blogged about a different SQUID. Here’s an update : Fleeing drivers are a common problem for law enforcement. They just won’t stop unless persuaded­—persuaded by bullets, barriers, spikes, or snares. Each option is risky business. Shooting up a fugitive’s car is one possibility. But what if children or hostages are in it? Lay down barriers, and the driver might swerve into a school bus. Spike his tires, and he might fishtail into a van­—if the spikes stop him at all. Existing traps, made from elastic, may halt a Hyundai, but they’re no match for a Hummer. [...]
