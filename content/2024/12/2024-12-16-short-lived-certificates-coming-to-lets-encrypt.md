Title: Short-Lived Certificates Coming to Let’s Encrypt
Date: 2024-12-16T12:06:56+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;certificates;encryption
Slug: 2024-12-16-short-lived-certificates-coming-to-lets-encrypt

[Source](https://www.schneier.com/blog/archives/2024/12/short-lived-certificates-coming-to-lets-encrypt.html){:target="_blank" rel="noopener"}

> Starting next year : Our longstanding offering won’t fundamentally change next year, but we are going to introduce a new offering that’s a big shift from anything we’ve done before—short-lived certificates. Specifically, certificates with a lifetime of six days. This is a big upgrade for the security of the TLS ecosystem because it minimizes exposure time during a key compromise event. Because we’ve done so much to encourage automation over the past decade, most of our subscribers aren’t going to have to do much in order to switch to shorter lived certificates. We, on the other hand, are going to [...]
