Title: Scumbag gets 30 years in the clink for running CSAM dark-web chatrooms, abusing kids
Date: 2024-12-13T22:50:11+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-12-13-scumbag-gets-30-years-in-the-clink-for-running-csam-dark-web-chatrooms-abusing-kids

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/13/texas_it_admin_csam/){:target="_blank" rel="noopener"}

> 'Today’s sentencing is more than just a punishment. It’s a message' A Texan who ran a forum on the dark web where depraved netizens could swap child sex abuse material (CSAM), and chat freely about abusing kids, has been sentenced to 30 years in prison.... [...]
