Title: AT&T and Verizon say networks secure after Salt Typhoon breach
Date: 2024-12-30T04:18:10-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-30-att-and-verizon-say-networks-secure-after-salt-typhoon-breach

[Source](https://www.bleepingcomputer.com/news/security/atandt-and-verizon-say-networks-secure-after-salt-typhoon-breach/){:target="_blank" rel="noopener"}

> AT&T and Verizon confirmed they were breached in a massive Chinese espionage campaign targeting telecom carriers worldwide but said the hackers have now been evicted from their networks. [...]
