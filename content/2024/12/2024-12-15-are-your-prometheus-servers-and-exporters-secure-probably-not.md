Title: Are your Prometheus servers and exporters secure? Probably not
Date: 2024-12-15T23:58:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-12-15-are-your-prometheus-servers-and-exporters-secure-probably-not

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/15/prometheus_servers_exporters_exposed/){:target="_blank" rel="noopener"}

> Plus: Netscaler brute force barrage; BeyondTrust API key stolen; and more Infosec in brief There's a problem of titanic proportions brewing for users of the Prometheus open source monitoring toolkit: hundreds of thousands of servers and exporters are exposed to the internet, creating significant security risks and leaving organizations vulnerable to attack.... [...]
