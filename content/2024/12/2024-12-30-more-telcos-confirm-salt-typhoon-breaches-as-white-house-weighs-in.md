Title: More telcos confirm Salt Typhoon breaches as White House weighs in
Date: 2024-12-30T23:30:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-12-30-more-telcos-confirm-salt-typhoon-breaches-as-white-house-weighs-in

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/30/att_verizon_confirm_salt_typhoon_breach/){:target="_blank" rel="noopener"}

> The intrusions allowed Beijing to 'geolocate millions of individuals' AT&T, Verizon, and Lumen Technologies confirmed that Chinese government-backed snoops accessed portions of their systems earlier this year, while the White House added another, yet-unnamed telecommunications company to the list of those breached by Salt Typhoon.... [...]
