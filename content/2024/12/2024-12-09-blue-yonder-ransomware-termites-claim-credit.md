Title: Blue Yonder ransomware termites claim credit
Date: 2024-12-09T03:01:13+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-12-09-blue-yonder-ransomware-termites-claim-credit

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/09/security_in_brief/){:target="_blank" rel="noopener"}

> Also: Mystery US firm compromised by Chinese hackers for months; Safe links that aren't; Polish spy boss arrested, and more Infosec in brief Still smarting over that grocery disruption caused by a ransomware attack on supply chain SaaS vendor Blue Yonder? Well, now you have someone to point a finger at: the Termite ransomware gang.... [...]
