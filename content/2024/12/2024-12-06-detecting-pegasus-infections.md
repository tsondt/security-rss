Title: Detecting Pegasus Infections
Date: 2024-12-06T12:09:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;machine learning;malware;smartphones;spyware
Slug: 2024-12-06-detecting-pegasus-infections

[Source](https://www.schneier.com/blog/archives/2024/12/detecting-pegasus-infections.html){:target="_blank" rel="noopener"}

> This tool seems to do a pretty good job. The company’s Mobile Threat Hunting feature uses a combination of malware signature-based detection, heuristics, and machine learning to look for anomalies in iOS and Android device activity or telltale signs of spyware infection. For paying iVerify customers, the tool regularly checks devices for potential compromise. But the company also offers a free version of the feature for anyone who downloads the iVerify Basics app for $1. These users can walk through steps to generate and send a special diagnostic utility file to iVerify and receive analysis within hours. Free users can [...]
