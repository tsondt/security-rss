Title: Interpol wants everyone to stop saying 'pig butchering'
Date: 2024-12-17T23:29:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-12-17-interpol-wants-everyone-to-stop-saying-pig-butchering

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/17/interpol_stop_saying_pig_butchering/){:target="_blank" rel="noopener"}

> Victims' feelings might get hurt, global cops contend, and that could hinder reporting Interpol wants to put an end to the online scam known as "pig butchering" – through linguistic policing, rather than law enforcement.... [...]
