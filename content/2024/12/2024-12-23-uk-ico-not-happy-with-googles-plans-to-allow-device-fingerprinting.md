Title: UK ICO not happy with Google's plans to allow device fingerprinting
Date: 2024-12-23T09:31:14+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-12-23-uk-ico-not-happy-with-googles-plans-to-allow-device-fingerprinting

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/23/uk_ico_not_happy_with/){:target="_blank" rel="noopener"}

> Also, Ascension notifies 5.6M victims, Krispy Kreme bandits come forward, LockBit 4.0 released, and more in brief Google has announced plans to allow its business customers to begin "fingerprinting" users next year, and the UK Information Commissioner's Office (ICO) isn't happy about it.... [...]
