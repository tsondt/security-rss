Title: Korea arrests CEO for adding DDoS feature to satellite receivers
Date: 2024-12-02T16:11:31-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware;Legal
Slug: 2024-12-02-korea-arrests-ceo-for-adding-ddos-feature-to-satellite-receivers

[Source](https://www.bleepingcomputer.com/news/security/korea-arrests-ceo-for-adding-ddos-feature-to-satellite-receivers/){:target="_blank" rel="noopener"}

> South Korean police have arrested a CEO and five employees for manufacturing over 240,000 satellite receivers pre-loaded or later updated to include DDoS attack functionality at a purchaser's request. [...]
