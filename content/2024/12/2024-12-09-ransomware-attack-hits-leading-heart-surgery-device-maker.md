Title: Ransomware attack hits leading heart surgery device maker
Date: 2024-12-09T18:00:51-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-09-ransomware-attack-hits-leading-heart-surgery-device-maker

[Source](https://www.bleepingcomputer.com/news/security/ransomware-attack-hits-leading-heart-surgery-device-maker/){:target="_blank" rel="noopener"}

> ​Artivion, a leading manufacturer of heart surgery medical devices, has disclosed a November 21 ransomware attack that disrupted its operations and forced it to take some systems offline. [...]
