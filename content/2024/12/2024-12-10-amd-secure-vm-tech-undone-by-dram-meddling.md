Title: AMD secure VM tech undone by DRAM meddling
Date: 2024-12-10T16:00:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-12-10-amd-secure-vm-tech-undone-by-dram-meddling

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/10/amd_secure_vm_tech_undone/){:target="_blank" rel="noopener"}

> Boffins devise BadRAM attack to pilfer secrets from SEV-SNP encrypted memory Researchers have found that the security mechanism AMD uses to protect virtual machine memory can be bypassed with $10 of hardware – and perhaps not even that.... [...]
