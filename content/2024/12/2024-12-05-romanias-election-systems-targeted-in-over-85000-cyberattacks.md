Title: Romania's election systems targeted in over 85,000 cyberattacks
Date: 2024-12-05T18:57:01-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-12-05-romanias-election-systems-targeted-in-over-85000-cyberattacks

[Source](https://www.bleepingcomputer.com/news/security/romanias-election-systems-targeted-in-over-85-000-cyberattacks/){:target="_blank" rel="noopener"}

> A declassified report from Romania's Intelligence Service says that the country's election infrastructure was targeted by more than 85,000 cyberattacks. [...]
