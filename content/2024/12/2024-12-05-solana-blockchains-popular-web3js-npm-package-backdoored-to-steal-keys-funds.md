Title: Solana blockchain's popular web3.js npm package backdoored to steal keys, funds
Date: 2024-12-05T23:13:34+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-12-05-solana-blockchains-popular-web3js-npm-package-backdoored-to-steal-keys-funds

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/05/solana_javascript_sdk_compromised/){:target="_blank" rel="noopener"}

> Damage likely limited to those running bots with private key access Malware-poisoned versions of the widely used JavaScript library @solana/web3.js were distributed via the npm package registry, according to an advisory issued Wednesday by project maintainer Steven Luscher.... [...]
