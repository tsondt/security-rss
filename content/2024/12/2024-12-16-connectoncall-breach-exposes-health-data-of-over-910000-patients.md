Title: ConnectOnCall breach exposes health data of over 910,000 patients
Date: 2024-12-16T12:28:35-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-12-16-connectoncall-breach-exposes-health-data-of-over-910000-patients

[Source](https://www.bleepingcomputer.com/news/security/connectoncall-breach-exposes-health-data-of-over-910-000-patients/){:target="_blank" rel="noopener"}

> Healthcare software as a service (SaaS) company Phreesia is notifying over 910,000 people that their personal and health data was exposed in a May breach of its subsidiary ConnectOnCall. [...]
