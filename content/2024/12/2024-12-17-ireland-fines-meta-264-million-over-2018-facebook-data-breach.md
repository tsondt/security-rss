Title: Ireland fines Meta $264 million over 2018 Facebook data breach
Date: 2024-12-17T11:06:54-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-12-17-ireland-fines-meta-264-million-over-2018-facebook-data-breach

[Source](https://www.bleepingcomputer.com/news/security/ireland-fines-meta-264-million-over-2018-facebook-data-breach/){:target="_blank" rel="noopener"}

> The Irish Data Protection Commission (DPC) fined Meta €251 million ($263.6M) over General Data Protection Regulation (GDPR) violations arising from a 2018 personal data breach impacting 29 million Facebook accounts. [...]
