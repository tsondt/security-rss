Title: Google Timeline location purge causes collateral damage
Date: 2024-12-13T21:08:15+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-12-13-google-timeline-location-purge-causes-collateral-damage

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/13/google_timeline_purge/){:target="_blank" rel="noopener"}

> Privacy measure leaves some mourning lost memories A year ago, Google announced plans to save people's Location History, which it now calls Timeline, locally on devices rather than on its servers.... [...]
