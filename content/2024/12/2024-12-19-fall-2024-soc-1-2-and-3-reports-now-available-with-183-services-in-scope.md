Title: Fall 2024 SOC 1, 2, and 3 reports now available with 183 services in scope
Date: 2024-12-19T17:15:57+00:00
Author: Paul Hong
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS SOC Reports;Compliance;Security Blog;SOC
Slug: 2024-12-19-fall-2024-soc-1-2-and-3-reports-now-available-with-183-services-in-scope

[Source](https://aws.amazon.com/blogs/security/fall-2024-soc-1-2-and-3-reports-now-available-with-183-services-in-scope/){:target="_blank" rel="noopener"}

> We continue to expand the scope of our assurance programs at Amazon Web Services (AWS) and are pleased to announce that the Fall 2024 System and Organization Controls (SOC) 1, 2, and 3 reports are now available. The reports cover 183 services over the 12-month period from October 1, 2023 to September 30, 2024, so [...]
