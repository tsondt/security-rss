Title: Crooks stole AWS credentials from misconfigured sites then kept them in open S3 bucket
Date: 2024-12-09T16:15:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-12-09-crooks-stole-aws-credentials-from-misconfigured-sites-then-kept-them-in-open-s3-bucket

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/09/aws_credentials_stolen/){:target="_blank" rel="noopener"}

> ShinyHunters-linked heist thought to have been ongoing since March Exclusive A massive online heist targeting AWS customers during which digital crooks abused misconfigurations in public websites and stole source code, thousands of credentials, and other secrets remains "ongoing to this day," according to security researchers.... [...]
