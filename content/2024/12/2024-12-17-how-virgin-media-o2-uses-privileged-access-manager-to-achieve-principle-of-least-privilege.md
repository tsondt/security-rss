Title: How Virgin Media O2 uses Privileged Access Manager to achieve principle of least privilege
Date: 2024-12-17T17:00:00+00:00
Author: Henry Tze
Category: GCP Security
Tags: Security & Identity;Customers
Slug: 2024-12-17-how-virgin-media-o2-uses-privileged-access-manager-to-achieve-principle-of-least-privilege

[Source](https://cloud.google.com/blog/topics/customers/how-virgin-media-o2-uses-privileged-access-manager-to-achieve-least-privilege/){:target="_blank" rel="noopener"}

> Editor’s note : Virgin Media O2 provides internet and communications services to more than 48.5 million subscribers, and teams are also responsible for supporting more than 16,000 employees. Virgin Media O2 is committed to empowering customers with outstanding customer-centered products and essential connectivity — all powered behind the scenes by Google Cloud, the backbone of the company’s daily operations. In this guest blog, Henry Tze shares his DevOps insights. From virtual machines to applications to storage assets, at Virgin Media O2 we have a huge cloud footprint, all hosted in Google Cloud. Google Cloud enables us to process vast amounts [...]
