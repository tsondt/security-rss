Title: Generative AI adoption and compliance: Simplifying the path forward with AWS Audit Manager
Date: 2024-12-13T17:43:01+00:00
Author: Kurt Kumar
Category: AWS Security
Tags: AWS Audit Manager;Configuration, compliance, and auditing;Intermediate (200);Management & Governance;Management Tools;Security, Identity, & Compliance;Thought Leadership;Security Blog
Slug: 2024-12-13-generative-ai-adoption-and-compliance-simplifying-the-path-forward-with-aws-audit-manager

[Source](https://aws.amazon.com/blogs/security/generative-ai-adoption-and-compliance-simplifying-the-path-forward-with-aws-audit-manager/){:target="_blank" rel="noopener"}

> As organizations increasingly use generative AI to streamline processes, enhance efficiency, and gain a competitive edge in today’s fast-paced business environment, they seek mechanisms for measuring and monitoring their use of AI services. To help you navigate the process of adopting generative AI technologies and proactively measure your generative AI implementation, AWS developed the AWS [...]
