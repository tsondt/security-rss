Title: AI and the 2024 Elections
Date: 2024-12-04T12:09:23+00:00
Author: B. Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2024-12-04-ai-and-the-2024-elections

[Source](https://www.schneier.com/blog/archives/2024/12/ai-and-the-2024-elections.html){:target="_blank" rel="noopener"}

> It’s been the biggest year for elections in human history: 2024 is a “ super-cycle ” year in which 3.7 billion eligible voters in 72 countries had the chance to go the polls. These are also the first AI elections, where many feared that deepfakes and artificial intelligence-generated misinformation would overwhelm the democratic processes. As 2024 draws to a close, it’s instructive to take stock of how democracy did. In a Pew survey of Americans from earlier this fall, nearly eight times as many respondents expected AI to be used for mostly bad purposes in the 2024 election as those [...]
