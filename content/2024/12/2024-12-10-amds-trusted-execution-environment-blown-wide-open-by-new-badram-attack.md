Title: AMD’s trusted execution environment blown wide open by new BadRAM attack
Date: 2024-12-10T17:08:09+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;AMD;cloud computing;sev-snp
Slug: 2024-12-10-amds-trusted-execution-environment-blown-wide-open-by-new-badram-attack

[Source](https://arstechnica.com/information-technology/2024/12/new-badram-attack-neuters-security-assurances-in-amd-epyc-processors/){:target="_blank" rel="noopener"}

> One of the oldest maxims in hacking is that once an attacker has physical access to a device, it’s game over for its security. The basis is sound. It doesn’t matter how locked down a phone, computer, or other machine is; if someone intent on hacking it gains the ability to physically manipulate it, the chances of success are all but guaranteed. In the age of cloud computing, this widely accepted principle is no longer universally true. Some of the world’s most sensitive information—health records, financial account information, sealed legal documents, and the like—now often resides on servers that receive [...]
