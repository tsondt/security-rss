Title: Veeam warns of critical RCE bug in Service Provider Console
Date: 2024-12-03T13:07:39-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-03-veeam-warns-of-critical-rce-bug-in-service-provider-console

[Source](https://www.bleepingcomputer.com/news/security/veeam-warns-of-critical-rce-bug-in-service-provider-console/){:target="_blank" rel="noopener"}

> ​Veeam released security updates today to address two Service Provider Console (VSPC) vulnerabilities, including a critical remote code execution (RCE) discovered during internal testing. [...]
