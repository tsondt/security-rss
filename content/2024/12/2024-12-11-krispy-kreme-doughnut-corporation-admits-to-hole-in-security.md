Title: Krispy Kreme Doughnut Corporation admits to hole in security
Date: 2024-12-11T19:00:11+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-12-11-krispy-kreme-doughnut-corporation-admits-to-hole-in-security

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/11/krispy_kreme_cybercrime/){:target="_blank" rel="noopener"}

> Belly-busting biz says it's been hit by cowardly custards Doughnut slinger Krispy Kreme has admitted to an attack that has left many customers unable to order online.... [...]
