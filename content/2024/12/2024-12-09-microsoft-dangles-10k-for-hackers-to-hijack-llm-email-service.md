Title: Microsoft dangles $10K for hackers to hijack LLM email service
Date: 2024-12-09T11:05:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-12-09-microsoft-dangles-10k-for-hackers-to-hijack-llm-email-service

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/09/microsoft_llm_prompt_injection_challenge/){:target="_blank" rel="noopener"}

> Outsmart an AI, win a little Christmas cash Microsoft and friends have challenged AI hackers to break a simulated LLM-integrated email client with a prompt injection attack – and the winning teams will share a $10,000 prize pool.... [...]
