Title: Iran-linked crew used custom 'cyberweapon' in US critical infrastructure attacks
Date: 2024-12-13T23:56:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-12-13-iran-linked-crew-used-custom-cyberweapon-in-us-critical-infrastructure-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/13/iran_cyberweapon_us_attacks/){:target="_blank" rel="noopener"}

> IOCONTROL targets IoT and OT devices from a ton of makers, apparently An Iranian government-linked cybercriminal crew used custom malware called IOCONTROL to attack and remotely control US and Israel-based water and fuel management systems, according to security researchers.... [...]
