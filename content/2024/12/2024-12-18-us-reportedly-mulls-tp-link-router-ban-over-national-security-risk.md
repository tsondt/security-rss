Title: US reportedly mulls TP-Link router ban over national security risk
Date: 2024-12-18T20:52:58+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-12-18-us-reportedly-mulls-tp-link-router-ban-over-national-security-risk

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/18/us_govt_probes_tplink_routers/){:target="_blank" rel="noopener"}

> It could end up like Huawei -Trump's gonna get ya, get ya, get ya updated The Feds may ban the sale of TP-Link routers in the US over ongoing national security concerns about Chinese-made devices being used in cyberattacks.... [...]
