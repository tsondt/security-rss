Title: Cops arrest suspected admin of German-language crime bazaar
Date: 2024-12-04T15:30:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-12-04-cops-arrest-suspected-admin-of-german-language-crime-bazaar

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/04/germany_crimenetwork_arrest/){:target="_blank" rel="noopener"}

> Drugs, botnets, forged docs, and more generated fortune for platform sellers German authorities say they have again shut down the perhaps unwisely named Crimenetwork platform and arrested a suspected admin.... [...]
