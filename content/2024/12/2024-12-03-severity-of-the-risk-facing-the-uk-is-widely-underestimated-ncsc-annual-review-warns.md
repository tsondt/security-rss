Title: Severity of the risk facing the UK is widely underestimated, NCSC annual review warns
Date: 2024-12-03T11:45:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-12-03-severity-of-the-risk-facing-the-uk-is-widely-underestimated-ncsc-annual-review-warns

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/03/ncsc_annual_review/){:target="_blank" rel="noopener"}

> National cyber emergencies increased threefold this year The number of security threats in the UK that hit the country's National Cyber Security Centre's (NCSC) maximum severity threshold has tripled compared to the previous 12 months.... [...]
