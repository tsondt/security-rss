Title: New Android spyware found on phone seized by Russian FSB
Date: 2024-12-05T12:17:25-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2024-12-05-new-android-spyware-found-on-phone-seized-by-russian-fsb

[Source](https://www.bleepingcomputer.com/news/security/new-android-spyware-found-on-phone-seized-by-russian-fsb/){:target="_blank" rel="noopener"}

> After a Russian programmer was detained by Russia's Federal Security Service (FSB) for fifteen days and his phone confiscated, it was discovered that a new spyware was secretly installed on his device upon its return. [...]
