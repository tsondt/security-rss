Title: Microsoft: Another Chinese cyberspy crew targeting US critical orgs 'as of yesterday'
Date: 2024-12-06T01:03:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-12-06-microsoft-another-chinese-cyberspy-crew-targeting-us-critical-orgs-as-of-yesterday

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/06/chinese_cyberspy_us_data/){:target="_blank" rel="noopener"}

> Redmond threat intel maven talks explains this persistent pain to The Reg A Chinese government-linked group that Microsoft tracks as Storm-2077 has been actively targeting critical organizations and US government agencies as of yesterday, according to Redmond's threat intel team.... [...]
