Title: AWS post-quantum cryptography migration plan
Date: 2024-12-05T17:32:07+00:00
Author: Matthew Campagna
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Thought Leadership;cryptography;post-quantum cryptography;Security Blog
Slug: 2024-12-05-aws-post-quantum-cryptography-migration-plan

[Source](https://aws.amazon.com/blogs/security/aws-post-quantum-cryptography-migration-plan/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is migrating to post-quantum cryptography (PQC). Like other security and compliance features in AWS, we will deliver PQC as part of our shared responsibility model. This means that some PQC features will be transparently enabled for all customers while others will be options that customers can choose to implement to help meet their [...]
