Title: Former NSA cyberspy's not-so-secret hobby: Hacking Christmas lights
Date: 2024-12-25T13:27:13+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-12-25-former-nsa-cyberspys-not-so-secret-hobby-hacking-christmas-lights

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/25/joyce_christmas_lights/){:target="_blank" rel="noopener"}

> Rob Joyce explains how it's done Video In 2018, Rob Joyce, then Donald Trump's White House Cybersecurity Coordinator, gave a surprise talk at the legendary hacking conference Shmoocon about his hobby.... [...]
