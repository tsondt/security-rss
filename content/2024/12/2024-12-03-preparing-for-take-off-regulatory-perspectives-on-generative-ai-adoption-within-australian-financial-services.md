Title: Preparing for take-off: Regulatory perspectives on generative AI adoption within Australian financial services
Date: 2024-12-03T15:23:18+00:00
Author: Julian Busic
Category: AWS Security
Tags: Artificial Intelligence;Best Practices;Financial Services;Foundational (100);Generative AI;Security, Identity, & Compliance;Thought Leadership;Security Blog
Slug: 2024-12-03-preparing-for-take-off-regulatory-perspectives-on-generative-ai-adoption-within-australian-financial-services

[Source](https://aws.amazon.com/blogs/security/preparing-for-take-off-regulatory-perspectives-on-generative-ai-adoption-within-australian-financial-services/){:target="_blank" rel="noopener"}

> The Australian financial services regulator, the Australian Prudential Regulation Authority (APRA), has provided its most substantial guidance on generative AI to date in Member Therese McCarthy Hockey’s remarks to the AFIA Risk Summit 2024. The guidance gives a green light for banks, insurance companies, and superannuation funds to accelerate their adoption of this transformative technology, [...]
