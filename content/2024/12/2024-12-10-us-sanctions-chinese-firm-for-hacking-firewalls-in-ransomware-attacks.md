Title: US sanctions Chinese firm for hacking firewalls in ransomware attacks
Date: 2024-12-10T11:37:01-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-10-us-sanctions-chinese-firm-for-hacking-firewalls-in-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-sanctions-chinese-firm-for-hacking-firewalls-in-ragnarok-ransomware-attacks/){:target="_blank" rel="noopener"}

> The U.S. Treasury Department has sanctioned Chinese cybersecurity company Sichuan Silence and one of its employees for their involvement in a series of Ragnarok ransomware attacks targeting U.S. critical infrastructure companies and many other victims worldwide in April 2020. [...]
