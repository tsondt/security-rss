Title: Police shuts down Manson cybercrime market, arrests key suspects
Date: 2024-12-05T12:44:54-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-05-police-shuts-down-manson-cybercrime-market-arrests-key-suspects

[Source](https://www.bleepingcomputer.com/news/security/police-shuts-down-manson-cybercrime-market-fake-shops-arrests-key-suspects/){:target="_blank" rel="noopener"}

> German law enforcement has seized over 50 servers that hosted the Manson Market cybercrime marketplace and fake online shops used in phishing operations. [...]
