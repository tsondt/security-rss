Title: Australia moves to drop some cryptography by 2030 – before quantum carves it up
Date: 2024-12-17T03:58:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-12-17-australia-moves-to-drop-some-cryptography-by-2030-before-quantum-carves-it-up

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/17/australia_dropping_crypto_keys/){:target="_blank" rel="noopener"}

> The likes of SHA-256, RSA, ECDSA and ECDH won't be welcome in just five years Australia's chief cyber security agency has decided local orgs should stop using the tech that forms the current cryptographic foundation of the internet by the year 2030 – years before other nations plan to do so – over fears that advances in quantum computing could render it insecure.... [...]
