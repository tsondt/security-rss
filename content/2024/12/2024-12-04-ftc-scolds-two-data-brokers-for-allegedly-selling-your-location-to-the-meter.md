Title: FTC scolds two data brokers for allegedly selling your location to the meter
Date: 2024-12-04T02:29:07+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-12-04-ftc-scolds-two-data-brokers-for-allegedly-selling-your-location-to-the-meter

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/04/ftc_data_brokers/){:target="_blank" rel="noopener"}

> 'Where we go is who we are' totally isn't a creepy ad slogan at all The FTC has reached a settlement with two data brokerages over allegations they harvested precise location data that shows when people entered hospitals, places of worship, and even attended protests supporting the late George Floyd.... [...]
