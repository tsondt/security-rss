Title: Russia takes unusual route to hack Starlink-connected devices in Ukraine
Date: 2024-12-11T23:18:42+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;backdoors;nation state hacking;phishing;turla
Slug: 2024-12-11-russia-takes-unusual-route-to-hack-starlink-connected-devices-in-ukraine

[Source](https://arstechnica.com/security/2024/12/russia-takes-unusual-route-to-hack-starlink-connected-devices-in-ukraine/){:target="_blank" rel="noopener"}

> Russian nation-state hackers have followed an unusual path to gather intel in the country's ongoing invasion of Ukraine—appropriating the infrastructure of fellow threat actors and using it to infect electronic devices its adversary’s military personnel are using on the front line. On at least two occasions this year, the Russian hacking group, tracked under names including Turla, Waterbug, Snake, and Venomous Bear, has used servers and malware used by separate threat groups in attacks targeting front-line Ukrainian military forces, Microsoft said Wednesday. In one case, Secret Blizzard—the name Microsoft uses to track the group—leveraged the infrastructure of a cybercrime group [...]
