Title: Customer data from 800,000 electric cars and owners exposed online
Date: 2024-12-28T10:16:36-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-12-28-customer-data-from-800000-electric-cars-and-owners-exposed-online

[Source](https://www.bleepingcomputer.com/news/security/customer-data-from-800-000-electric-cars-and-owners-exposed-online/){:target="_blank" rel="noopener"}

> Volkswagen's automotive software company, Cariad, exposed data collected from around 800,000 electric cars. The info could be linked to drivers' names and reveal precise vehicle locations. [...]
