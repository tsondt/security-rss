Title: U.S. Army Soldier Arrested in AT&T, Verizon Extortions
Date: 2024-12-31T04:05:51+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Allison Nixon;Cameron John Wagenius;Connor Riley Moucka;Judische;Kiberphant0m;Unit 221B
Slug: 2024-12-31-us-army-soldier-arrested-in-att-verizon-extortions

[Source](https://krebsonsecurity.com/2024/12/u-s-army-soldier-arrested-in-att-verizon-extortions/){:target="_blank" rel="noopener"}

> Federal authorities have arrested and indicted a 20-year-old U.S. Army soldier on suspicion of being Kiberphant0m, a cybercriminal who has been selling and leaking sensitive customer call records stolen earlier this year from AT&T and Verizon. As first reported by KrebsOnSecurity last month, the accused is a communications specialist who was recently stationed in South Korea. One of several selfies on the Facebook page of Cameron Wagenius. Cameron John Wagenius was arrested near the Army base in Fort Hood, Texas on Dec. 20, after being indicted on two criminal counts of unlawful transfer of confidential phone records. The sparse, two-page [...]
