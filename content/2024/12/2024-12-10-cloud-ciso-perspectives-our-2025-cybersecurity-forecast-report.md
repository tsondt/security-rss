Title: Cloud CISO Perspectives: Our 2025 Cybersecurity Forecast report
Date: 2024-12-10T17:00:00+00:00
Author: Nick Godfrey
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2024-12-10-cloud-ciso-perspectives-our-2025-cybersecurity-forecast-report

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-our-2025-cybersecurity-forecast-report/){:target="_blank" rel="noopener"}

> Welcome to the first Cloud CISO Perspectives for December 2024. Today, Nick Godfrey, senior director, Office of the CISO, shares our Forecast report for the coming year, with additional insights from our Office of the CISO colleagues. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI Security & CISO, Google Cloud aside_block <ListValue: [StructValue([('title', 'Get vital board insights with Google Cloud'), ('body', <wagtail.rich_text.RichText object at 0x3e85805d90a0>), ('btn_text', 'Visit [...]
