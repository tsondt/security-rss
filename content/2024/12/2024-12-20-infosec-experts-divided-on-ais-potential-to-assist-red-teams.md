Title: Infosec experts divided on AI's potential to assist red teams
Date: 2024-12-20T03:22:15+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-12-20-infosec-experts-divided-on-ais-potential-to-assist-red-teams

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/20/gen_ai_red_teaming/){:target="_blank" rel="noopener"}

> Yes, LLMs can do the heavy lifting. But good luck getting one to give evidence CANALYS FORUMS APAC Generative AI is being enthusiastically adopted in almost every field, but infosec experts are divided on whether it is truly helpful for red team raiders who test enterprise systems.... [...]
