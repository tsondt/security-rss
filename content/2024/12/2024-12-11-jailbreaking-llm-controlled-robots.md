Title: Jailbreaking LLM-Controlled Robots
Date: 2024-12-11T12:02:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;hacking;LLM;robotics;social engineering
Slug: 2024-12-11-jailbreaking-llm-controlled-robots

[Source](https://www.schneier.com/blog/archives/2024/12/jailbreaking-llm-controlled-robots.html){:target="_blank" rel="noopener"}

> Surprising no one, it’s easy to trick an LLM-controlled robot into ignoring its safety instructions. [...]
