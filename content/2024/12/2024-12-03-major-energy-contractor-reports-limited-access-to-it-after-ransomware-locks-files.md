Title: Major energy contractor reports 'limited' access to IT after ransomware locks files
Date: 2024-12-03T20:00:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-12-03-major-energy-contractor-reports-limited-access-to-it-after-ransomware-locks-files

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/03/us_energy_contractor_englobal_ransomware/){:target="_blank" rel="noopener"}

> ENGlobal customers include the Pentagon as well as major oil and gas producers American energy contractor ENGlobal disclosed that access to its IT systems remains limited following a ransomware infection in late November.... [...]
