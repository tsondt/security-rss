Title: AWS Network Firewall Geographic IP Filtering launch
Date: 2024-12-06T21:21:31+00:00
Author: Prasanjit Tiwari
Category: AWS Security
Tags: Announcements;Best Practices;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Application security;AWS Network Firewall;Compliance;Security;Security Blog
Slug: 2024-12-06-aws-network-firewall-geographic-ip-filtering-launch

[Source](https://aws.amazon.com/blogs/security/aws-network-firewall-geographic-ip-filtering-launch/){:target="_blank" rel="noopener"}

> AWS Network Firewall is a managed service that provides a convenient way to deploy essential network protections for your virtual private clouds (VPCs). In this blog post, we discuss Geographic IP Filtering, a new feature of Network Firewall that you can use to filter traffic based on geographic location and meet compliance requirements. Customers with [...]
