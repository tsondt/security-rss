Title: Chinese hackers use Visual Studio Code tunnels for remote access
Date: 2024-12-10T06:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-12-10-chinese-hackers-use-visual-studio-code-tunnels-for-remote-access

[Source](https://www.bleepingcomputer.com/news/security/chinese-hackers-use-visual-studio-code-tunnels-for-remote-access/){:target="_blank" rel="noopener"}

> Chinese hackers targeting large IT service providers in Southern Europe were seen abusing Visual Studio Code (VSCode) tunnels to maintain persistent access to compromised systems. [...]
