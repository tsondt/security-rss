Title: Ultralytics Supply-Chain Attack
Date: 2024-12-13T16:33:58+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;supply chain
Slug: 2024-12-13-ultralytics-supply-chain-attack

[Source](https://www.schneier.com/blog/archives/2024/12/ultralytics-supply-chain-attack.html){:target="_blank" rel="noopener"}

> Last week, we saw a supply-chain attack against the Ultralytics AI library on GitHub. A quick summary : On December 4, a malicious version 8.3.41 of the popular AI library ultralytics ­—which has almost 60 million downloads—was published to the Python Package Index (PyPI) package repository. The package contained downloader code that was downloading the XMRig coinminer. The compromise of the project’s build environment was achieved by exploiting a known and previously reported GitHub Actions script injection. Lots more details at that link. Also here. Seth Michael Larson—the security developer in residence with the Python Software Foundation, responsible for, among [...]
