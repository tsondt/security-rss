Title: Microsoft won't let customers opt out of passkey push
Date: 2024-12-18T17:30:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-12-18-microsoft-wont-let-customers-opt-out-of-passkey-push

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/18/microsoft_passkey_push/){:target="_blank" rel="noopener"}

> Enrollment invitations will continue until security improves Microsoft last week lauded the success of its efforts to convince customers to use passkeys instead of passwords, without actually quantifying that success.... [...]
