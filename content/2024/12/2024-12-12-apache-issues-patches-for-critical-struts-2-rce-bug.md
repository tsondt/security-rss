Title: Apache issues patches for critical Struts 2 RCE bug
Date: 2024-12-12T13:31:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-12-12-apache-issues-patches-for-critical-struts-2-rce-bug

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/12/apache_struts_2_vuln/){:target="_blank" rel="noopener"}

> More details released after devs allowed weeks to apply fixes We now know the remote code execution vulnerability in Apache Struts 2 disclosed back in November carries a near-maximum severity rating following the publication of the CVE.... [...]
