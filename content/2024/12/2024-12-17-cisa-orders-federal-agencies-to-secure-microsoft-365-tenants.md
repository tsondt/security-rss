Title: CISA orders federal agencies to secure Microsoft 365 tenants
Date: 2024-12-17T15:01:13-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-12-17-cisa-orders-federal-agencies-to-secure-microsoft-365-tenants

[Source](https://www.bleepingcomputer.com/news/security/cisa-orders-federal-agencies-to-secure-microsoft-365-tenants/){:target="_blank" rel="noopener"}

> ​CISA has issued this year's first binding operational directive (BOD 25-01), ordering federal civilian agencies to secure their Microsoft 365 cloud environments by implementing a list of required configuration baselines. [...]
