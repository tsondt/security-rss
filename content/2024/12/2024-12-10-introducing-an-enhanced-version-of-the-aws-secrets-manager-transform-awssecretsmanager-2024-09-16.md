Title: Introducing an enhanced version of the AWS Secrets Manager transform: AWS::SecretsManager-2024-09-16
Date: 2024-12-10T18:26:12+00:00
Author: Sanjay Varma Datla
Category: AWS Security
Tags: Announcements;AWS Secrets Manager;Best Practices;Intermediate (200);Security, Identity, & Compliance;AWS CloudFormation;Security Blog
Slug: 2024-12-10-introducing-an-enhanced-version-of-the-aws-secrets-manager-transform-awssecretsmanager-2024-09-16

[Source](https://aws.amazon.com/blogs/security/introducing-an-enhanced-version-of-the-aws-secrets-manager-transform-awssecretsmanager-2024-09-16/){:target="_blank" rel="noopener"}

> We’re pleased to announce an enhanced version of the AWS Secrets Manager transform: AWS::SecretsManager-2024-09-16. This update is designed to simplify infrastructure management by reducing the need for manual security updates, bug fixes, and runtime upgrades. AWS Secrets Manager helps you manage, retrieve, and rotate database credentials, API keys, and other secrets throughout their lifecycles. Some AWS services [...]
