Title: Inside the incident: Uncovering an advanced phishing attack
Date: 2024-12-10T10:01:11-05:00
Author: Sponsored by Varonis
Category: BleepingComputer
Tags: Security
Slug: 2024-12-10-inside-the-incident-uncovering-an-advanced-phishing-attack

[Source](https://www.bleepingcomputer.com/news/security/inside-the-incident-uncovering-an-advanced-phishing-attack/){:target="_blank" rel="noopener"}

> Recently, Varonis investigated a phishing campaign in which a malicious email enabled a threat actor to access the organization. This blog post will reveal the tactics used to avoid detection and share what was discovered during the investigation. [...]
