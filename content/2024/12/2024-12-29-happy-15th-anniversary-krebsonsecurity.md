Title: Happy 15th Anniversary, KrebsOnSecurity!
Date: 2024-12-29T23:48:44+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Other
Slug: 2024-12-29-happy-15th-anniversary-krebsonsecurity

[Source](https://krebsonsecurity.com/2024/12/happy-15th-anniversary-krebsonsecurity/){:target="_blank" rel="noopener"}

> Image: Shutterstock, Dreamansions. KrebsOnSecurity.com turns 15 years old today! Maybe it’s indelicate to celebrate the birthday of a cybercrime blog that mostly publishes bad news, but happily many of 2024’s most engrossing security stories were about bad things happening to bad guys. It’s also an occasion to note that despite my publishing fewer stories than ever this past year, we somehow managed to attract near record levels of readership (thank you!). In case you missed any of them, here’s a recap of 2024’s most-read stories. In January, KrebsOnSecurity told the story of a Canadian man who was falsely charged with [...]
