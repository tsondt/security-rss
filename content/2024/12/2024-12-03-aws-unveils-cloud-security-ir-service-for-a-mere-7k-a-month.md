Title: AWS unveils cloud security IR service for a mere $7K a month
Date: 2024-12-03T01:30:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-12-03-aws-unveils-cloud-security-ir-service-for-a-mere-7k-a-month

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/03/amazon_cloud_security_incident_response/){:target="_blank" rel="noopener"}

> Tap into the infinite scalability... of pricing Re:Invent Amazon Web Services has a new incident response service that combines automation and people to protect customers' AWS accounts - at a hefty price.... [...]
