Title: Hacking Digital License Plates
Date: 2024-12-17T17:04:26+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;firmware;hacking;vulnerabilities
Slug: 2024-12-17-hacking-digital-license-plates

[Source](https://www.schneier.com/blog/archives/2024/12/hacking-digital-license-plates.html){:target="_blank" rel="noopener"}

> Not everything needs to be digital and “smart.” License plates, for example : Josep Rodriguez, a researcher at security firm IOActive, has revealed a technique to “jailbreak” digital license plates sold by Reviver, the leading vendor of those plates in the US with 65,000 plates already sold. By removing a sticker on the back of the plate and attaching a cable to its internal connectors, he’s able to rewrite a Reviver plate’s firmware in a matter of minutes. Then, with that custom firmware installed, the jailbroken license plate can receive commands via Bluetooth from a smartphone app to instantly change [...]
