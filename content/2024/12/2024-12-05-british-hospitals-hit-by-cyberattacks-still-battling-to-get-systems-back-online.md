Title: British hospitals hit by cyberattacks still battling to get systems back online
Date: 2024-12-05T12:25:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-12-05-british-hospitals-hit-by-cyberattacks-still-battling-to-get-systems-back-online

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/05/hospital_cyberattack/){:target="_blank" rel="noopener"}

> Children's hospital and cardiac unit say criminals broke in via shared 'digital gateway service' Both National Health Service trusts that oversee the various hospitals hit by separate cyberattacks last week have confirmed they're still in the process of restoring systems.... [...]
