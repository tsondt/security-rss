Title: Microsoft adds another problem to the Windows 11 24H2 naughty list
Date: 2024-12-27T17:30:07+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-12-27-microsoft-adds-another-problem-to-the-windows-11-24h2-naughty-list

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/27/microsoft_windows_11_security_update/){:target="_blank" rel="noopener"}

> Santa Satya pops one more issue into his sack just in time for Christmas The trickle of known issues with Windows 11 24H2 has continued with a new one just in time for festive season: installed the operating system using removable media? There's a chance it might stop receiving security updates.... [...]
