Title: Fully patched Cleo products under renewed 'zero-day-ish' mass attack
Date: 2024-12-10T13:32:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-12-10-fully-patched-cleo-products-under-renewed-zero-day-ish-mass-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/10/cleo_vulnerability/){:target="_blank" rel="noopener"}

> Thousands of servers targeted while customers wait for patches Researchers at security shop Huntress are seeing mass exploitation of a vulnerability affecting three Cleo file management products, even on patched systems.... [...]
