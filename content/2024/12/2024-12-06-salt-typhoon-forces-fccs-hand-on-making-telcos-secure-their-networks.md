Title: Salt Typhoon forces FCC's hand on making telcos secure their networks
Date: 2024-12-06T18:27:14+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2024-12-06-salt-typhoon-forces-fccs-hand-on-making-telcos-secure-their-networks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/06/salt_typhoon_fcc_proposal/){:target="_blank" rel="noopener"}

> Proposal pushes stricter infosec safeguards after Chinese state baddies expose vulns The head of America's Federal Communications Commission (FCC) wants to force telecoms operators to tighten network security in the wake of the Salt Typhoon revelations, and to submit an annual report detailing measures taken.... [...]
