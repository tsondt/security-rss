Title: Interpol nabs thousands, seizes millions in global cybercrime-busting op
Date: 2024-12-01T20:24:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-12-01-interpol-nabs-thousands-seizes-millions-in-global-cybercrime-busting-op

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/01/interpol_cybercrime_busting/){:target="_blank" rel="noopener"}

> Also, script kiddies still a threat, Tornado Cash is back, UK firms lose billions to avoidable attacks, and more Infosec in brief Interpol and its financial supporters in the South Korean government are back with another round of anti-cybercrime arrests via the fifth iteration of Operation HAECHI, this time nabbing more than 5,500 people suspected of scamming and seizing hundreds of millions in digital and fiat currencies.... [...]
