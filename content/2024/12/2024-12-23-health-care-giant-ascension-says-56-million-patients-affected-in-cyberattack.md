Title: Health care giant Ascension says 5.6 million patients affected in cyberattack
Date: 2024-12-23T17:21:39+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;Data breaches;health care;ransomware
Slug: 2024-12-23-health-care-giant-ascension-says-56-million-patients-affected-in-cyberattack

[Source](https://arstechnica.com/information-technology/2024/12/health-care-giant-ascension-says-5-6-million-patients-affected-in-cyberattack/){:target="_blank" rel="noopener"}

> Health care company Ascension lost sensitive data for nearly 5.6 million individuals in a cyberattack that was attributed to a notorious ransomware gang, according to documents filed with the attorney general of Maine. Ascension owns 140 hospitals and scores of assisted living facilities. In May, the organization was hit with an attack that caused mass disruptions as staff was forced to move to manual processes that caused errors, delayed or lost lab results, and diversions of ambulances to other hospitals. Ascension managed to restore most services by mid-June. At the time, the company said the attackers had stolen protected health [...]
