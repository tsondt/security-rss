Title: Ultralytics AI model hijacked to infect thousands with cryptominer
Date: 2024-12-06T13:54:22-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Artificial Intelligence;Software
Slug: 2024-12-06-ultralytics-ai-model-hijacked-to-infect-thousands-with-cryptominer

[Source](https://www.bleepingcomputer.com/news/security/ultralytics-ai-model-hijacked-to-infect-thousands-with-cryptominer/){:target="_blank" rel="noopener"}

> The popular Ultralytics YOLO11 AI model was compromised in a supply chain attack to deploy cryptominers on devices running versions 8.3.41 and 8.3.42 from the Python Package Index (PyPI) [...]
