Title: Russia blocks Viber in latest attempt to censor communications
Date: 2024-12-13T13:10:40-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-13-russia-blocks-viber-in-latest-attempt-to-censor-communications

[Source](https://www.bleepingcomputer.com/news/security/russia-blocks-viber-in-latest-attempt-to-censor-communications/){:target="_blank" rel="noopener"}

> Russian telecommunications watchdog Roskomnadzor has blocked the Viber encrypted messaging app, used by hundreds of millions worldwide, for violating the country's legislation. [...]
