Title: Firefox ditches Do Not Track because nobody was listening anyway
Date: 2024-12-12T08:49:12+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-12-12-firefox-ditches-do-not-track-because-nobody-was-listening-anyway

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/12/firefox_do_not_track/){:target="_blank" rel="noopener"}

> Few websites actually respect the option, says Mozilla When Firefox 135 is released in February, it'll ship with one less feature: Mozilla plans to remove the Do Not Track toggle from its Privacy and Security settings.... [...]
