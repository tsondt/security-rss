Title: Krispy Kreme cyberattack impacts online orders and operations
Date: 2024-12-11T09:44:43-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-12-11-krispy-kreme-cyberattack-impacts-online-orders-and-operations

[Source](https://www.bleepingcomputer.com/news/security/krispy-kreme-cyberattack-impacts-online-orders-and-operations/){:target="_blank" rel="noopener"}

> US doughnut chain Krispy Kreme suffered a cyberattack in November that impacted portions of its business operations, including placing online orders. [...]
