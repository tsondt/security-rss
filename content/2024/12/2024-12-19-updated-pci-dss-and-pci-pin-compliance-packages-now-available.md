Title: Updated PCI DSS and PCI PIN compliance packages now available
Date: 2024-12-19T17:22:52+00:00
Author: Nivetha Chandran
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Compliance;Compliance reports;PCI;PCI DSS;Security Blog
Slug: 2024-12-19-updated-pci-dss-and-pci-pin-compliance-packages-now-available

[Source](https://aws.amazon.com/blogs/security/updated-pci-dss-and-pci-pin-compliance-packages-now-available/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce enhancements to our Payment Card Industry (PCI) compliance portfolio, further empowering AWS customers to build and manage secure, compliant payment environments with greater ease and flexibility. PCI Data Security Standard (DSS): Our latest AWS PCI DSS v4 Attestation of Compliance (AOC) is now available and includes six [...]
