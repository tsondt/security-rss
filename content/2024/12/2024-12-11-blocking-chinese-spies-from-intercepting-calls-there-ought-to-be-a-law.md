Title: Blocking Chinese spies from intercepting calls? There ought to be a law
Date: 2024-12-11T23:03:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-12-11-blocking-chinese-spies-from-intercepting-calls-there-ought-to-be-a-law

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/11/telecom_cybersecurity_standards/){:target="_blank" rel="noopener"}

> Sen. Wyden blasts FCC's 'failure' amid Salt Typhoon hacks US telecoms carriers would be required to implement minimum cyber security standards and ensure their systems are not susceptible to hacks by nation-state attackers – like Salt Typhoon – under legislation proposed by senator Ron Wyden (D-OR).... [...]
