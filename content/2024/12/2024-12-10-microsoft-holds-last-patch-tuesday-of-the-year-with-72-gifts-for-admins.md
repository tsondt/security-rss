Title: Microsoft holds last Patch Tuesday of the year with 72 gifts for admins
Date: 2024-12-10T20:48:18+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-12-10-microsoft-holds-last-patch-tuesday-of-the-year-with-72-gifts-for-admins

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/10/microsoft_patch_tuesday/){:target="_blank" rel="noopener"}

> Twas the night before Christmas, and all through the house, patching was done with the click of a mouse Patch Tuesday Microsoft hasn't added too much coal to the stocking this Patch Tuesday, with just 72 fixes, only one of which scored more than nine on the CVSS threat ranking scale.... [...]
