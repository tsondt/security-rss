Title: Rhode Island confirms data breach after Brain Cipher ransomware attack
Date: 2024-12-16T11:51:49-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-12-16-rhode-island-confirms-data-breach-after-brain-cipher-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/rhode-island-confirms-data-breach-after-brain-cipher-ransomware-attack/){:target="_blank" rel="noopener"}

> Rhode Island is warning that its RIBridges system, managed by Deloitte, suffered a data breach exposing residents' personal information after the Brain Cipher ransomware gang hacked its systems. [...]
