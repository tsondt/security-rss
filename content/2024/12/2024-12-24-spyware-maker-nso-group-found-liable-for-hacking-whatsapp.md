Title: Spyware Maker NSO Group Found Liable for Hacking WhatsApp
Date: 2024-12-24T12:04:24+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;hacking;Israel;spyware;WhatsApp
Slug: 2024-12-24-spyware-maker-nso-group-found-liable-for-hacking-whatsapp

[Source](https://www.schneier.com/blog/archives/2024/12/spyware-maker-nso-group-found-liable-for-hacking-whatsapp.html){:target="_blank" rel="noopener"}

> A judge has found that NSO Group, maker of the Pegasus spyware, has violated the US Computer Fraud and Abuse Act by hacking WhatsApp in order to spy on people using it. Jon Penney and I wrote a legal paper on the case. [...]
