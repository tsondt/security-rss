Title: Windows Tooling Updates: OleView.NET
Date: 2024-12-12T15:27:00-08:00
Author: Google Project Zero (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2024-12-12-windows-tooling-updates-oleviewnet

[Source](https://googleprojectzero.blogspot.com/2024/12/windows-tooling-updates-oleviewnet.html){:target="_blank" rel="noopener"}

> Posted by James Forshaw, Google Project Zero This is a short blog post about some recent improvements I've been making to the OleView.NET tool which has been released as part of version 1.16. The tool is designed to discover the attack surface of Windows COM and find security vulnerabilities such as privilege escalation and remote code execution. The updates were recently presented at the Microsoft Bluehat conference in Redmond under the name " DCOM Research for Everyone! ". This blog expands on the topics discussed to give a bit more background and detail that couldn't be fit within the 45-minute [...]
