Title: The Windows Registry Adventure #5: The regf file format
Date: 2024-12-19T11:03:00-08:00
Author: Google Project Zero (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2024-12-19-the-windows-registry-adventure-5-the-regf-file-format

[Source](https://googleprojectzero.blogspot.com/2024/12/the-windows-registry-adventure-5-regf.html){:target="_blank" rel="noopener"}

> Posted by Mateusz Jurczyk, Google Project Zero As previously mentioned in the second installment of the blog post series ( "A brief history of the feature" ), the binary format used to encode registry hives from Windows NT 3.1 up to the modern Windows 11 is called regf. In a way, it is quite special, because it represents a registry subtree simultaneously on disk and in memory, as opposed to most other common file formats. Documents, images, videos, etc. are generally designed to store data efficiently on disk, and they are subsequently parsed to and from different in-memory representations whenever [...]
