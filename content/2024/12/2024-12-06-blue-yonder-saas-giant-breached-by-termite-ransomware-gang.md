Title: Blue Yonder SaaS giant breached by Termite ransomware gang
Date: 2024-12-06T11:35:54-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-06-blue-yonder-saas-giant-breached-by-termite-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/blue-yonder-saas-giant-breached-by-termite-ransomware-gang/){:target="_blank" rel="noopener"}

> ​The Termite ransomware gang has officially claimed responsibility for the November breach of software as a service (SaaS) provider Blue Yonder. [...]
