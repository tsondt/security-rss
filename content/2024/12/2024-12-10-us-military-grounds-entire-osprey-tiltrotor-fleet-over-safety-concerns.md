Title: US military grounds entire Osprey tiltrotor fleet over safety concerns
Date: 2024-12-10T19:06:06+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-12-10-us-military-grounds-entire-osprey-tiltrotor-fleet-over-safety-concerns

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/10/us_military_osprey/){:target="_blank" rel="noopener"}

> Boeing-Bell V-22 can't outfly its checkered past, it seems The US Navy, Air Force, and Marine Corps have grounded their fleet of Boeing-Bell-made Osprey V-22s on safety grounds.... [...]
