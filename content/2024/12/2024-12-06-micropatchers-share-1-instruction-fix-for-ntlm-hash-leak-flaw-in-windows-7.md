Title: Micropatchers share 1-instruction fix for NTLM hash leak flaw in Windows 7+
Date: 2024-12-06T23:34:18+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-12-06-micropatchers-share-1-instruction-fix-for-ntlm-hash-leak-flaw-in-windows-7

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/06/opatch_zeroday_microsoft/){:target="_blank" rel="noopener"}

> Microsoft's OS sure loves throwing your creds at remote systems Acros Security claims to have found an unpatched bug in Microsoft Windows 7 and onward that can be exploited to steal users' OS account credentials.... [...]
