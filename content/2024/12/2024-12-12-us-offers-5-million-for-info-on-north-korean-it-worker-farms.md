Title: US offers $5 million for info on North Korean IT worker farms
Date: 2024-12-12T15:24:14-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-12-us-offers-5-million-for-info-on-north-korean-it-worker-farms

[Source](https://www.bleepingcomputer.com/news/security/us-offers-5-million-for-info-on-north-korean-it-worker-farms/){:target="_blank" rel="noopener"}

> ​The U.S. State Department is offering a reward of up to $5 million for information that could help disrupt the activities of North Korean front companies and employees who generated over $88 million via illegal remote IT work schemes in six years. [...]
