Title: New fake Ledger data breach emails try to steal crypto wallets
Date: 2024-12-17T17:04:40-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-12-17-new-fake-ledger-data-breach-emails-try-to-steal-crypto-wallets

[Source](https://www.bleepingcomputer.com/news/security/new-fake-ledger-data-breach-emails-try-to-steal-crypto-wallets/){:target="_blank" rel="noopener"}

> A new Ledger phishing campaign is underway that pretends to be a data breach notification asking you to verify your recovery phrase, which is then stolen and used to steal your cryptocurrency. [...]
