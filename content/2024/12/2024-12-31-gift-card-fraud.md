Title: Gift Card Fraud
Date: 2024-12-31T12:02:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;crime;fraud
Slug: 2024-12-31-gift-card-fraud

[Source](https://www.schneier.com/blog/archives/2024/12/gift-card-fraud.html){:target="_blank" rel="noopener"}

> It’s becoming an organized crime tactic : Card draining is when criminals remove gift cards from a store display, open them in a separate location, and either record the card numbers and PINs or replace them with a new barcode. The crooks then repair the packaging, return to a store and place the cards back on a rack. When a customer unwittingly selects and loads money onto a tampered card, the criminal is able to access the card online and steal the balance. [...] In card draining, the runners assist with removing, tampering and restocking of gift cards, according to [...]
