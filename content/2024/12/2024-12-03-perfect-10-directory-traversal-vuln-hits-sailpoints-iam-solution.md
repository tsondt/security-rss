Title: Perfect 10 directory traversal vuln hits SailPoint's IAM solution
Date: 2024-12-03T23:45:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-12-03-perfect-10-directory-traversal-vuln-hits-sailpoints-iam-solution

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/03/sailpoint_identityiq_vulnerability/){:target="_blank" rel="noopener"}

> 20-year-old info disclosure class bug still pervades security software It's time to rev up those patch engines after SailPoint disclosed a perfect 10/10 severity vulnerability in its identity and access management (IAM) platform IdentityIQ.... [...]
