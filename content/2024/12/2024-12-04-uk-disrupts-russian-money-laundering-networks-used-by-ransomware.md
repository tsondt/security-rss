Title: UK disrupts Russian money laundering networks used by ransomware
Date: 2024-12-04T15:18:10-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-04-uk-disrupts-russian-money-laundering-networks-used-by-ransomware

[Source](https://www.bleepingcomputer.com/news/security/uk-disrupts-russian-money-laundering-networks-used-by-ransomware/){:target="_blank" rel="noopener"}

> ​A law enforcement operation led by the United Kingdom's National Crime Agency (NCA) has disrupted two Russian money laundering networks working with criminals worldwide, including ransomware gangs. [...]
