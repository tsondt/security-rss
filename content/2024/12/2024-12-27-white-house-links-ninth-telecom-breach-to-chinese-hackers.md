Title: White House links ninth telecom breach to Chinese hackers
Date: 2024-12-27T15:02:03-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-27-white-house-links-ninth-telecom-breach-to-chinese-hackers

[Source](https://www.bleepingcomputer.com/news/security/white-house-links-ninth-telecom-breach-to-chinese-hackers/){:target="_blank" rel="noopener"}

> A White House official has added a ninth U.S. telecommunications company to the list of telecoms breached in a Chinese hacking campaign that impacted dozens of countries. [...]
