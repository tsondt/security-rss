Title: Friday Squid Blogging: Squid Sticker
Date: 2024-12-20T22:00:59+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-12-20-friday-squid-blogging-squid-sticker

[Source](https://www.schneier.com/blog/archives/2024/12/friday-squid-blogging-squid-sticker.html){:target="_blank" rel="noopener"}

> A sticker for your water bottle. Blog moderation policy. [...]
