Title: Introducing the AWS Network Firewall CloudWatch Dashboard
Date: 2024-12-12T19:04:14+00:00
Author: Ajinkya Patil
Category: AWS Security
Tags: Amazon CloudWatch;AWS Network Firewall;Intermediate (200);Monitoring and observability;Security, Identity, & Compliance;Technical How-to;Monitoring;Observability;Security Blog
Slug: 2024-12-12-introducing-the-aws-network-firewall-cloudwatch-dashboard

[Source](https://aws.amazon.com/blogs/security/introducing-the-aws-network-firewall-cloudwatch-dashboard/){:target="_blank" rel="noopener"}

> Amazon CloudWatch dashboards are customizable pages in the CloudWatch console that you can use to monitor your resources in a single view. This post focuses on deploying a CloudWatch dashboard that you can use to create a customizable monitoring solution for your AWS Network Firewall firewall. It’s designed to provide deeper insights into your firewall’s [...]
