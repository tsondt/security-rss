Title: US Treasury Department outs the blast radius of BeyondTrust's key leak
Date: 2024-12-31T15:30:07+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-12-31-us-treasury-department-outs-the-blast-radius-of-beyondtrusts-key-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/31/us_treasury_department_hacked/){:target="_blank" rel="noopener"}

> Data pilfered as miscreants roamed affected workstations The US Department of the Treasury has admitted that miscreants were in its systems, accessing documents in what has been called a "major incident."... [...]
