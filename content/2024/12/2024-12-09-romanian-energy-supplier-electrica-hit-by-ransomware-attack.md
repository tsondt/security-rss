Title: Romanian energy supplier Electrica hit by ransomware attack
Date: 2024-12-09T11:38:50-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-09-romanian-energy-supplier-electrica-hit-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/romanian-energy-supplier-electrica-hit-by-ransomware-attack/){:target="_blank" rel="noopener"}

> Electrica Group, a key player in the Romanian electricity distribution and supply market, is investigating a ransomware attack that was still "in progress" earlier today. [...]
