Title: British Army zaps drones out of the sky with laser trucks
Date: 2024-12-12T10:26:07+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2024-12-12-british-army-zaps-drones-out-of-the-sky-with-laser-trucks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/12/british_army_drone_laser/){:target="_blank" rel="noopener"}

> High-energy weapon proves its mettle in testing The British Army has successfully destroyed flying drones for the first time using a high-energy laser mounted on an armored vehicle. If perfected, the technology could form an effective counter-measure against drone attacks.... [...]
