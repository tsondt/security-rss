Title: Police seizes largest German online crime marketplace, arrests admin
Date: 2024-12-03T12:16:33-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-12-03-police-seizes-largest-german-online-crime-marketplace-arrests-admin

[Source](https://www.bleepingcomputer.com/news/security/police-seizes-largest-german-online-crime-marketplace-arrests-admin/){:target="_blank" rel="noopener"}

> Germany has taken down the largest online cybercrime marketplace in the country, named "Crimenetwork," and arrested its administrator for facilitating the sale of drugs, stolen data, and illegal services. [...]
