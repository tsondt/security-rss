Title: Suspected LockBit dev, facing US extradition, 'did it for the money'
Date: 2024-12-23T13:31:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-12-23-suspected-lockbit-dev-facing-us-extradition-did-it-for-the-money

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/23/lockbit_ransomware_dev_extradition/){:target="_blank" rel="noopener"}

> Dual Russian-Israeli national arrested in August An alleged LockBit ransomware developer is in custody in Israel and awaiting extradition to the United States.... [...]
