Title: Data on 760K workers from Xerox, Nokia, BofA, Morgan Stanley and more dumped online
Date: 2024-12-03T02:57:16+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-12-03-data-on-760k-workers-from-xerox-nokia-bofa-morgan-stanley-and-more-dumped-online

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/03/760k_xerox_nokia_bofa_morgan/){:target="_blank" rel="noopener"}

> Yet another result of the MOVEit mess Hundreds of thousands of employees from major corporations including Xerox, Nokia, Koch, Bank of America, Morgan Stanley and others appear to be the latest victims in a massive data breach linked to last year's attacks on file transfer tool MOVEit.... [...]
