Title: How Chinese insiders are stealing data scooped up by President Xi's national surveillance system
Date: 2024-12-08T17:00:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-12-08-how-chinese-insiders-are-stealing-data-scooped-up-by-president-xis-national-surveillance-system

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/08/chinese_insiders_stealing_data/){:target="_blank" rel="noopener"}

> 'It's a double-edged sword,' security researchers tell The Reg Feature Chinese tech company employees and government workers are siphoning off user data and selling it online - and even high-ranking Chinese Communist Party officials and FBI-wanted hackers' sensitive information is being peddled by the Middle Kingdom's thriving illegal data ecosystem.... [...]
