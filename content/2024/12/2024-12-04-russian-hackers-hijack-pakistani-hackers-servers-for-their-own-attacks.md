Title: Russian hackers hijack Pakistani hackers' servers for their own attacks
Date: 2024-12-04T12:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-12-04-russian-hackers-hijack-pakistani-hackers-servers-for-their-own-attacks

[Source](https://www.bleepingcomputer.com/news/security/russian-turla-hackers-hijack-pakistani-apt-servers-for-cyber-espionage-attacks/){:target="_blank" rel="noopener"}

> The notorious Russian cyber-espionage group Turla is hacking other hackers, hijacking the Pakistani threat actor Storm-0156's infrastructure to launch their own covert attacks on already compromised networks. [...]
