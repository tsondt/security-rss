Title: CISA warns water facilities to secure HMI systems exposed online
Date: 2024-12-13T14:34:35-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-13-cisa-warns-water-facilities-to-secure-hmi-systems-exposed-online

[Source](https://www.bleepingcomputer.com/news/security/cisa-warns-water-facilities-to-secure-hmi-systems-exposed-online/){:target="_blank" rel="noopener"}

> CISA and the Environmental Protection Agency (EPA) warned water facilities today to secure Internet-exposed Human Machine Interfaces (HMIs) from cyberattacks. [...]
