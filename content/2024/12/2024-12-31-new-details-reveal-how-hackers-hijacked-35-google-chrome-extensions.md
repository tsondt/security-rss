Title: New details reveal how hackers hijacked 35 Google Chrome extensions
Date: 2024-12-31T13:54:11-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-12-31-new-details-reveal-how-hackers-hijacked-35-google-chrome-extensions

[Source](https://www.bleepingcomputer.com/news/security/new-details-reveal-how-hackers-hijacked-35-google-chrome-extensions/){:target="_blank" rel="noopener"}

> New details have emerged about a phishing campaign targeting Chrome browser extension developers that led to the compromise of at least thirty-five extensions to inject data-stealing code, including those from cybersecurity firm Cyberhaven. [...]
