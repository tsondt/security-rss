Title: Russian cyber spies hide behind other hackers to target Ukraine
Date: 2024-12-11T12:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-12-11-russian-cyber-spies-hide-behind-other-hackers-to-target-ukraine

[Source](https://www.bleepingcomputer.com/news/security/russian-cyber-spies-hide-behind-other-hackers-to-target-ukraine/){:target="_blank" rel="noopener"}

> Russian cyber-espionage group Turla, aka "Secret Blizzard," is utilizing other threat actors' infrastructure to target Ukrainian military devices connected via Starlink. [...]
