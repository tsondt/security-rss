Title: AWS completes the CCCS PBHVA assessment with 149 services and features in scope
Date: 2024-12-19T19:58:55+00:00
Author: Naranjan Goklani
Category: AWS Security
Tags: Announcements;Compliance;Foundational (100);Security, Identity, & Compliance;AWS Artifact;AWS Canada (Central) Region;AWS Shared Responsibility Model;Canada;CCCS;CCCS Assessment;classification;Cloud security;Cyber Security;cybersecurity;Federal Government Canada;Government of Canada (GC);ITSG-33;Medium Cloud Security Profile;Protected B;Public Sector;Security Blog
Slug: 2024-12-19-aws-completes-the-cccs-pbhva-assessment-with-149-services-and-features-in-scope

[Source](https://aws.amazon.com/blogs/security/aws-completes-the-cccs-pbhva-assessment-with-149-services-and-features-in-scope/){:target="_blank" rel="noopener"}

> We continue to expand the scope of our assurance programs at Amazon Web Services (AWS) and are pleased to announce the successful completion of our first ever Protected B High Value Assets (PBHVA) assessment with 149 assessed services and features. Completion of this assessment effective October 4, 2024, makes AWS the first cloud service provider [...]
