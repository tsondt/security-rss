Title: Don't fall for a mail asking for rapid Docusign action – it may be an Azure account hijack phish
Date: 2024-12-19T05:30:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-12-19-dont-fall-for-a-mail-asking-for-rapid-docusign-action-it-may-be-an-azure-account-hijack-phish

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/19/docusign_lure_azure_account_takeover/){:target="_blank" rel="noopener"}

> Recent campaign targeted 20,000 folk across UK and Europe with this tactic, Unit 42 warns Unknown criminals went on a phishing expedition that targeted about 20,000 users across the automotive, chemical and industrial compound manufacturing sectors in Europe, and tried to steal account credentials and then hijack the victims' Microsoft Azure cloud infrastructure.... [...]
