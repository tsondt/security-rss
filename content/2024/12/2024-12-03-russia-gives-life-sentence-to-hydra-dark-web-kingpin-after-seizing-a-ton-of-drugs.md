Title: Russia gives life sentence to Hydra dark web kingpin after seizing a ton of drugs
Date: 2024-12-03T07:29:13+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-12-03-russia-gives-life-sentence-to-hydra-dark-web-kingpin-after-seizing-a-ton-of-drugs

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/03/russia_hydra_sentencing/){:target="_blank" rel="noopener"}

> No exaggeration – literally a ton. Plus, 15 co-conspirators also put behind bars A Russian court has handed a life sentence to the head of the infamous online drugs souk Hydra, and 15 of his co-conspirators will also spend many years behind bars.... [...]
