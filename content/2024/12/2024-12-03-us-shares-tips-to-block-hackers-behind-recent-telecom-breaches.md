Title: US shares tips to block hackers behind recent telecom breaches
Date: 2024-12-03T14:49:06-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-03-us-shares-tips-to-block-hackers-behind-recent-telecom-breaches

[Source](https://www.bleepingcomputer.com/news/security/us-shares-tips-to-block-hackers-behind-recent-telecom-breaches/){:target="_blank" rel="noopener"}

> ​CISA released guidance today to help network defenders harden their systems against attacks coordinated by the Salt Typhoon Chinese threat group that breached multiple major global telecommunications providers earlier this year. [...]
