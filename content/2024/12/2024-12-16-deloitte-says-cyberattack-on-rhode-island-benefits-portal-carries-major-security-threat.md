Title: Deloitte says cyberattack on Rhode Island benefits portal carries 'major security threat'
Date: 2024-12-16T18:01:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-12-16-deloitte-says-cyberattack-on-rhode-island-benefits-portal-carries-major-security-threat

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/16/deloitte_rhode_island_attack/){:target="_blank" rel="noopener"}

> Personal and financial data probably stolen A cyberattack on a Deloitte-managed government system in Rhode Island carries a "high probability" of sensitive data theft, the state says.... [...]
