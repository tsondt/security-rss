Title: T-Mobile US CSO: Spies jumped from one telco to another in a way 'I've not seen in my career'
Date: 2024-12-05T00:52:57+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-12-05-t-mobile-us-cso-spies-jumped-from-one-telco-to-another-in-a-way-ive-not-seen-in-my-career

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/05/tmobile_cso_telecom_attack/){:target="_blank" rel="noopener"}

> Security chief talks to El Reg as Feds urge everyone to use encrypted chat interview While Chinese-government-backed spies maintained access to US telecommunications providers' networks for months – and in some cases still haven't been booted out – T-Mobile US thwarted successful attacks on its systems "within a single-digit number of days," according to the carrier's security boss Jeff Simon.... [...]
