Title: China's cyber intrusions took a sinister turn in 2024
Date: 2024-12-31T12:15:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-12-31-chinas-cyber-intrusions-took-a-sinister-turn-in-2024

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/31/china_cyber_intrusions_2024/){:target="_blank" rel="noopener"}

> From targeted espionage to pre-positioning - not that they are mutually exclusive The Chinese government's intrusions into America's telecommunications and other critical infrastructure networks this year appears to signal a shift from cyberspying as usual to prepping for destructive attacks.... [...]
