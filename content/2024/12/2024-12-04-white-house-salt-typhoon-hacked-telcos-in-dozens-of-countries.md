Title: White House: Salt Typhoon hacked telcos in dozens of countries
Date: 2024-12-04T18:00:27-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-04-white-house-salt-typhoon-hacked-telcos-in-dozens-of-countries

[Source](https://www.bleepingcomputer.com/news/security/white-house-salt-typhoon-hacked-telcos-in-dozens-of-countries/){:target="_blank" rel="noopener"}

> ​Chinese state hackers, known as Salt Typhoon, have breached telecommunications companies in dozens of countries, President Biden's deputy national security adviser Anne Neuberger said today. [...]
