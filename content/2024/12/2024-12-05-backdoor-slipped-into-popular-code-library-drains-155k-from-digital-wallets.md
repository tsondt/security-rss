Title: Backdoor slipped into popular code library, drains ~$155k from digital wallets
Date: 2024-12-05T12:35:15+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;backdoor;cryptocurrency;solana;supply chain attack
Slug: 2024-12-05-backdoor-slipped-into-popular-code-library-drains-155k-from-digital-wallets

[Source](https://arstechnica.com/information-technology/2024/12/backdoor-slips-into-popular-code-library-drains-155k-from-digital-wallets/){:target="_blank" rel="noopener"}

> Hackers pocketed as much as $155,000 by sneaking a backdoor into a code library used by developers of smart contract apps that work with the cryptocurrency known as Solana. The supply-chain attack targeted solana-web3.js, a collection of JavaScript code used by developers of decentralized apps for interacting with the Solana blockchain. These “dapps” allow people to sign smart contracts that, in theory, operate autonomously in executing currency trades among two or more parties when certain agreed-upon conditions are met. The backdoor came in the form of code that collected private keys and wallet addresses when apps that directly handled private [...]
