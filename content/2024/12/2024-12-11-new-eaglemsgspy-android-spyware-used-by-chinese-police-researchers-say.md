Title: New EagleMsgSpy Android spyware used by Chinese police, researchers say
Date: 2024-12-11T16:03:24-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2024-12-11-new-eaglemsgspy-android-spyware-used-by-chinese-police-researchers-say

[Source](https://www.bleepingcomputer.com/news/security/new-eaglemsgspy-android-spyware-used-by-chinese-police-researchers-say/){:target="_blank" rel="noopener"}

> A previously undocumented Android spyware called 'EagleMsgSpy' has been discovered and is believed to be used by law enforcement agencies in China to monitor mobile devices. [...]
