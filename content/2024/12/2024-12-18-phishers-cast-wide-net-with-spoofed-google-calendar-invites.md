Title: Phishers cast wide net with spoofed Google Calendar invites
Date: 2024-12-18T00:58:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-12-18-phishers-cast-wide-net-with-spoofed-google-calendar-invites

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/18/google_calendar_spoofed_in_phishing_campaign/){:target="_blank" rel="noopener"}

> Not that you needed another reason to enable the 'known senders' setting Criminals are spoofing Google Calendar emails in a financially motivated phishing expedition that has already affected about 300 organizations with more than 4,000 emails sent over four weeks, according to Check Point researchers.... [...]
