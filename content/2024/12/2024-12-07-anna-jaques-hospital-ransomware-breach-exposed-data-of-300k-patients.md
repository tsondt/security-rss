Title: Anna Jaques Hospital ransomware breach exposed data of 300K patients
Date: 2024-12-07T10:12:22-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-12-07-anna-jaques-hospital-ransomware-breach-exposed-data-of-300k-patients

[Source](https://www.bleepingcomputer.com/news/security/anna-jaques-hospital-ransomware-breach-exposed-data-of-300k-patients/){:target="_blank" rel="noopener"}

> Anna Jaques Hospital has confirmed on its website that a ransomware attack it suffered almost precisely a year ago, on December 25, 2023, has exposed sensitive health data for over 316,000 patients. [...]
