Title: FTC warns of online task job scams hooking victims like gambling
Date: 2024-12-13T14:51:36-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-12-13-ftc-warns-of-online-task-job-scams-hooking-victims-like-gambling

[Source](https://www.bleepingcomputer.com/news/security/ftc-warns-of-online-task-job-scams-hooking-victims-like-gambling/){:target="_blank" rel="noopener"}

> The Federal Trade Commission (FTC) warns about a significant rise in gambling-like online job scams, known as "task scams," that draw people into earning cash through repetitive tasks, with the promises of earning more if they deposit their own money. [...]
