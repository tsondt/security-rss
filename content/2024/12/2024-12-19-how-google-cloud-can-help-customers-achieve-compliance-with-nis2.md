Title: How Google Cloud can help customers achieve compliance with NIS2
Date: 2024-12-19T17:00:00+00:00
Author: Jeanette Manfra
Category: GCP Security
Tags: Security & Identity
Slug: 2024-12-19-how-google-cloud-can-help-customers-achieve-compliance-with-nis2

[Source](https://cloud.google.com/blog/products/identity-security/how-google-cloud-can-help-customers-achieve-compliance-with-nis2/){:target="_blank" rel="noopener"}

> With the European Commission’s adoption of the Network and Information Systems Directive 2.0, or NIS2, Europe is taking an essential step forward in its strategy to protect consumers, businesses, and government organizations from escalating threats in cyberspace. NIS2 will help to drive a consistent high level of security and resilience across key sectors of the European economy. NIS2 also represents a seismic shift in the expectations and obligations of private entities to adhere to cybersecurity best practices, and to factor security into everyday business decisions. For European organizations, including Google Cloud customers, NIS2 may require new investments in security tools, [...]
