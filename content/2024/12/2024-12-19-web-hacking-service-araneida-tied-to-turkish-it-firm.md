Title: Web Hacking Service ‘Araneida’ Tied to Turkish IT Firm
Date: 2024-12-19T17:07:30+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Ne'er-Do-Well News;The Coming Storm;Acunetix;Altug Sara;altugsara321@gmail.com;Araneida Scanner;Bilitro Yazilim;domaintools;Fin7;Invicti Security;Matt Sciberras;Neil Roseman;ori0nbusiness@protonmail.com;Silent Push;U.S. Department of Health and Human Services;Zach Edwards
Slug: 2024-12-19-web-hacking-service-araneida-tied-to-turkish-it-firm

[Source](https://krebsonsecurity.com/2024/12/web-hacking-service-araneida-tied-to-turkish-it-firm/){:target="_blank" rel="noopener"}

> Cybercriminals are selling hundreds of thousands of credential sets stolen with the help of a cracked version of Acunetix, a powerful commercial web app vulnerability scanner, new research finds. The cracked software is being resold as a cloud-based attack tool by at least two different services, one of which KrebsOnSecurity traced to an information technology firm based in Turkey. Araneida Scanner. Cyber threat analysts at Silent Push said they recently received reports from a partner organization that identified an aggressive scanning effort against their website using an Internet address previously associated with a campaign by FIN7, a notorious Russia-based hacking [...]
