Title: Upcoming Speaking Events
Date: 2024-12-14T17:01:50+00:00
Author: B. Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2024-12-14-upcoming-speaking-events

[Source](https://www.schneier.com/blog/archives/2024/12/upcoming-speaking-events-2.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking at a joint meeting of the Boston Chapter of the IEEE Computer Society and GBC/ACM, in Boston, Massachusetts, USA, at 7:00 PM ET on Thursday, January 9, 2025. The event will take place at the Massachusetts Institute of Technology in Room 32-G449 (Kiva), as well as online via Zoom. Please register in advance if you plan to attend (whether online or in person). The list is maintained on this page. [...]
