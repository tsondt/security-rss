Title: Russian cyberspies target Android users with new spyware
Date: 2024-12-13T12:43:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2024-12-13-russian-cyberspies-target-android-users-with-new-spyware

[Source](https://www.bleepingcomputer.com/news/security/russian-gamaredon-cyberspies-target-android-users-with-new-spyware/){:target="_blank" rel="noopener"}

> Russian cyberspies Gamaredon has been discovered using two Android spyware families named 'BoneSpy' and 'PlainGnome' to spy on and steal data from mobile devices. [...]
