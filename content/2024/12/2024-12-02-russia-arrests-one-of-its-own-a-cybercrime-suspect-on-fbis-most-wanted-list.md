Title: Russia arrests one of its own – a cybercrime suspect on FBI's most wanted list
Date: 2024-12-02T12:38:17+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-12-02-russia-arrests-one-of-its-own-a-cybercrime-suspect-on-fbis-most-wanted-list

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/02/russia_ransomware_arrest/){:target="_blank" rel="noopener"}

> The latest in an unusual change of fortune for group once protected by the Kremlin An alleged former affiliate of the LockBit and Babuk ransomware operations, who also just happens to be one of the most wanted cybercriminals in the US, is now reportedly in handcuffs.... [...]
