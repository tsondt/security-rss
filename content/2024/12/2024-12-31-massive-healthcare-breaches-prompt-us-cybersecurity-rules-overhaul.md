Title: Massive healthcare breaches prompt US cybersecurity rules overhaul
Date: 2024-12-31T02:32:22-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-12-31-massive-healthcare-breaches-prompt-us-cybersecurity-rules-overhaul

[Source](https://www.bleepingcomputer.com/news/security/massive-healthcare-breaches-prompt-us-cybersecurity-rules-overhaul/){:target="_blank" rel="noopener"}

> The U.S. Department of Health and Human Services (HHS) has proposed updates to the Health Insurance Portability and Accountability Act of 1996 (HIPAA) to secure patients' health data following a surge in massive healthcare data leaks. [...]
