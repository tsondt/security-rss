Title: Spain busts voice phishing ring for defrauding 10,000 bank customers
Date: 2024-12-12T11:44:03-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-12-12-spain-busts-voice-phishing-ring-for-defrauding-10000-bank-customers

[Source](https://www.bleepingcomputer.com/news/security/spain-busts-voice-phishing-ring-for-defrauding-10-000-bank-customers/){:target="_blank" rel="noopener"}

> The Spanish police, working with colleagues in Peru, conducted a simultaneous crackdown on a large-scale voice phishing (vishing) scam ring in the two countries, arresting 83 individuals. [...]
