Title: Trump administration wants to go on cyber offensive against China
Date: 2024-12-16T19:30:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-12-16-trump-administration-wants-to-go-on-cyber-offensive-against-china

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/16/trump_administration_china_offensive/){:target="_blank" rel="noopener"}

> The US has never attacked Chinese critical infrastructure before, right? President-elect Donald Trump's team wants to go on the offensive against America's cyber adversaries, though it isn't clear how the incoming administration plans to achieve this.... [...]
