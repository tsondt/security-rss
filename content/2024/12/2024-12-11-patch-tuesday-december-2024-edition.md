Title: Patch Tuesday, December 2024 Edition
Date: 2024-12-11T01:53:13+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Other;Adam Barnett;CVE-2024-49112;CVE-2024-49138;Fortra;Immersive Labs;LDAP;Lightweight Directory Access Protocol;Microsoft Patch Tuesday December 2024;Rapid7;Rob Reeves;Tenable;Tyler Reguly;Windows Common Log File System (CLFS) driver
Slug: 2024-12-11-patch-tuesday-december-2024-edition

[Source](https://krebsonsecurity.com/2024/12/patch-tuesday-december-2024-edition/){:target="_blank" rel="noopener"}

> Microsoft today released updates to plug at least 70 security holes in Windows and Windows software, including one vulnerability that is already being exploited in active attacks. The zero-day seeing exploitation involves CVE-2024-49138, a security weakness in the Windows Common Log File System (CLFS) driver — used by applications to write transaction logs — that could let an authenticated attacker gain “system” level privileges on a vulnerable Windows device. The security firm Rapid7 notes there have been a series of zero-day elevation of privilege flaws in CLFS over the past few years. “Ransomware authors who have abused previous CLFS vulnerabilities [...]
