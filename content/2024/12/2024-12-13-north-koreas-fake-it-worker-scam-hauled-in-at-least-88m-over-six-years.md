Title: North Korea's fake IT worker scam hauled in at least $88M over six years
Date: 2024-12-13T00:32:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-12-13-north-koreas-fake-it-worker-scam-hauled-in-at-least-88m-over-six-years

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/13/doj_dpkr_fake_tech_worker_indictment/){:target="_blank" rel="noopener"}

> DoJ thinks it's found the folks that ran it, and some of the 'IT warriors' sent out to fleece employers North Korea's fake IT worker scams netted the hermit kingdom $88 million over six years, according to the US Department of Justice, which thinks it's found the people who run them.... [...]
