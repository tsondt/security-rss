Title: Hackers steal ZAGG customers' credit cards in third-party breach
Date: 2024-12-28T11:57:28-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-12-28-hackers-steal-zagg-customers-credit-cards-in-third-party-breach

[Source](https://www.bleepingcomputer.com/news/security/hackers-steal-zagg-customers-credit-cards-in-third-party-breach/){:target="_blank" rel="noopener"}

> ZAGG Inc. is informing customers that their credit card data has been exposed to unauthorized individuals after hackers compromised a third-party application provided by the company's e-commerce provider, BigCommerce. [...]
