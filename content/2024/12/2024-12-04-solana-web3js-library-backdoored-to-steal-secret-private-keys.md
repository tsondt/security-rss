Title: Solana Web3.js library backdoored to steal secret, private keys
Date: 2024-12-04T12:31:04-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-12-04-solana-web3js-library-backdoored-to-steal-secret-private-keys

[Source](https://www.bleepingcomputer.com/news/security/solana-web3js-library-backdoored-to-steal-secret-private-keys/){:target="_blank" rel="noopener"}

> The legitimate Solana JavaScript SDK was temporarily compromised yesterday in a supply chain attack, with the library backdoored with malicious code to steal cryptocurrency private keys and drain wallets. [...]
