Title: How to Lose a Fortune with Just One Bad Click
Date: 2024-12-18T13:17:59+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Ne'er-Do-Well News;Web Fraud 2.0;(650) 203-0000;Coinbase;Daniel from Google;Gemini AI;Google Assistant;Google Docs;Google Forms;Google Photos;Graham Cluely;Junseth;Minecraft;SwanCoin;Trezor
Slug: 2024-12-18-how-to-lose-a-fortune-with-just-one-bad-click

[Source](https://krebsonsecurity.com/2024/12/how-to-lose-a-fortune-with-just-one-bad-click/){:target="_blank" rel="noopener"}

> Image: Shutterstock, iHaMoo. Adam Griffin is still in disbelief over how quickly he was robbed of nearly $500,000 in cryptocurrencies. A scammer called using a real Google phone number to warn his Gmail account was being hacked, sent email security alerts directly from google.com, and ultimately seized control over the account by convincing him to click “yes” to a Google prompt on his mobile device. Griffin is a battalion chief firefighter in the Seattle area, and on May 6 he received a call from someone claiming they were from Google support saying his account was being accessed from Germany. A [...]
