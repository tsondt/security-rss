Title: Casino Players Using Hidden Cameras for Cheating
Date: 2024-12-27T12:03:55+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cameras;cheating;gambling
Slug: 2024-12-27-casino-players-using-hidden-cameras-for-cheating

[Source](https://www.schneier.com/blog/archives/2024/12/casino-players-using-hidden-cameras-for-cheating.html){:target="_blank" rel="noopener"}

> The basic strategy is to place a device with a hidden camera in a position to capture normally hidden card values, which are interpreted by an accomplice off-site and fed back to the player via a hidden microphone. Miniaturization is making these devices harder to detect. Presumably AI will soon obviate the need for an accomplice. [...]
