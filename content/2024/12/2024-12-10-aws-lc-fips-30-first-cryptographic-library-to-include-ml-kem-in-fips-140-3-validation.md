Title: AWS-LC FIPS 3.0: First cryptographic library to include ML-KEM in FIPS 140-3 validation
Date: 2024-12-10T16:28:08+00:00
Author: Jake Massimo
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Compliance;cryptography;Federal Information Processing Standard;FIPS;FIPS 140;FIPS 140-2;NIST;Open source;post quantum;Security Blog
Slug: 2024-12-10-aws-lc-fips-30-first-cryptographic-library-to-include-ml-kem-in-fips-140-3-validation

[Source](https://aws.amazon.com/blogs/security/aws-lc-fips-3-0-first-cryptographic-library-to-include-ml-kem-in-fips-140-3-validation/){:target="_blank" rel="noopener"}

> We’re excited to announce that AWS-LC FIPS 3.0 has been added to the National Institute of Standards and Technology (NIST) Cryptographic Module Validation Program (CMVP) modules in process list. This latest validation of AWS-LC introduces support for Module Lattice-Based Key Encapsulation Mechanisms (ML-KEM), the new FIPS standardized post-quantum cryptographic algorithm. This is a significant step towards enhancing the [...]
