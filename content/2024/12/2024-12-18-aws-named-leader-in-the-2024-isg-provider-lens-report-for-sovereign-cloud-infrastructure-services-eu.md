Title: AWS named Leader in the 2024 ISG Provider Lens report for Sovereign Cloud Infrastructure Services (EU)
Date: 2024-12-18T20:49:45+00:00
Author: Marta Taggart
Category: AWS Security
Tags: Announcements;Europe;Foundational (100);Security, Identity, & Compliance;AWS Digital Sovereignty Pledge;Digital Sovereignty;EU Data Protection;report;Security Blog;Sovereign-by-design
Slug: 2024-12-18-aws-named-leader-in-the-2024-isg-provider-lens-report-for-sovereign-cloud-infrastructure-services-eu

[Source](https://aws.amazon.com/blogs/security/aws-named-leader-in-the-2024-isg-provider-lens-report-for-sovereign-cloud-infrastructure-services-eu/){:target="_blank" rel="noopener"}

> For the second year in a row, Amazon Web Services (AWS) is named as a Leader in the Information Services Group (ISG) Provider Lens Quadrant report for Sovereign Cloud Infrastructure Services (EU), published on December 18, 2024. ISG is a leading global technology research, analyst, and advisory firm that serves as a trusted business partner [...]
