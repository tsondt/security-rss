Title: Citrix goes shopping in Europe and returns with gifts for security-conscious customers
Date: 2024-12-12T05:02:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-12-12-citrix-goes-shopping-in-europe-and-returns-with-gifts-for-security-conscious-customers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/12/citrix_acquires_devicetrust_strong_network/){:target="_blank" rel="noopener"}

> Acquires two companies that help those on the nice list keep naughty list types at bay Citrix has gone on a European shopping trip, and come home with its bag of gifts bulging thanks to a pair of major buys: infosec outfits deviceTRUST and Strong Network.... [...]
