Title: US arrests Scattered Spider suspect linked to telecom hacks
Date: 2024-12-05T15:31:18-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-05-us-arrests-scattered-spider-suspect-linked-to-telecom-hacks

[Source](https://www.bleepingcomputer.com/news/security/us-arrests-scattered-spider-suspect-linked-to-telecom-hacks/){:target="_blank" rel="noopener"}

> ​U.S. authorities have arrested a 19-year-old teenager linked to the notorious Scattered Spider cybercrime gang who is now charged with breaching a U.S. financial institution and two unnamed telecommunications firms. [...]
