Title: Scams Based on Fake Google Emails
Date: 2024-12-26T16:09:30+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;AI;cryptocurrency;cybercrime;cybersecurity;Google;scams
Slug: 2024-12-26-scams-based-on-fake-google-emails

[Source](https://www.schneier.com/blog/archives/2024/12/scams-based-on-fake-google-emails.html){:target="_blank" rel="noopener"}

> Scammers are hacking Google Forms to send email to victims that come from google.com. Brian Krebs reports on the effects. Boing Boing post. [...]
