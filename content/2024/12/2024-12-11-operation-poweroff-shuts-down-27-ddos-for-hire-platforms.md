Title: Operation PowerOFF shuts down 27 DDoS-for-hire platforms
Date: 2024-12-11T11:34:57-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-12-11-operation-poweroff-shuts-down-27-ddos-for-hire-platforms

[Source](https://www.bleepingcomputer.com/news/security/operation-poweroff-shuts-down-27-ddos-for-hire-platforms/){:target="_blank" rel="noopener"}

> Law enforcement agencies from 15 countries have taken 27 DDoS-for-hire services offline, also known as "booters" or "stressers," arrested three administrators, and identified 300 customers of the platforms. [...]
