Title: Kali Linux 2024.4 released with 14 new tools, deprecates some features
Date: 2024-12-16T16:33:19-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Linux
Slug: 2024-12-16-kali-linux-20244-released-with-14-new-tools-deprecates-some-features

[Source](https://www.bleepingcomputer.com/news/security/kali-linux-20244-released-with-14-new-tools-deprecates-some-features/){:target="_blank" rel="noopener"}

> Kali Linux has released version 2024.4, the fourth and final version of 2024, and it is now available with fourteen new tools, numerous improvements, and deprecates some features. [...]
