Title: Novel phishing campaign uses corrupted Word documents to evade security
Date: 2024-12-01T10:20:30-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-12-01-novel-phishing-campaign-uses-corrupted-word-documents-to-evade-security

[Source](https://www.bleepingcomputer.com/news/security/novel-phishing-campaign-uses-corrupted-word-documents-to-evade-security/){:target="_blank" rel="noopener"}

> A novel phishing attack abuses Microsoft's Word file recovery feature by sending corrupted Word documents as email attachments, allowing them to bypass security software due to their damaged state but still be recoverable by the application. [...]
