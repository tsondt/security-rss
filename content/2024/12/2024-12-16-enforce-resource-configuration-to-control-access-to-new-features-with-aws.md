Title: Enforce resource configuration to control access to new features with AWS
Date: 2024-12-16T20:18:32+00:00
Author: Yossi Cohen
Category: AWS Security
Tags: AWS CloudFormation;AWS Identity and Access Management (IAM);AWS Organizations;Customer Solutions;Expert (400);Security, Identity, & Compliance;Thought Leadership;Security Blog
Slug: 2024-12-16-enforce-resource-configuration-to-control-access-to-new-features-with-aws

[Source](https://aws.amazon.com/blogs/security/enforce-resource-configuration-to-control-access-to-new-features-with-aws/){:target="_blank" rel="noopener"}

> Establishing and maintaining an effective security and governance posture has never been more important for enterprises. This post explains how you, as a security administrator, can use Amazon Web Services (AWS) to enforce resource configurations in a manner that is designed to be secure, scalable, and primarily focused on feature gating. In this context, feature [...]
