Title: US names Chinese national it alleges was behind 2020 attack on Sophos firewalls
Date: 2024-12-11T05:02:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-12-11-us-names-chinese-national-it-alleges-was-behind-2020-attack-on-sophos-firewalls

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/11/sichuan_silence_sophos_zeroday_sanctions/){:target="_blank" rel="noopener"}

> Also sanctions his employer – an outfit called Sichuan Silence linked to Ragnarok ransomware The US Departments of Treasury and Justice have named a Chinese business and one of its employees as the actors behind the 2020 exploit of a zero-day flaw in Sophos firewalls... [...]
