Title: Winnti hackers target other threat actors with new Glutton PHP backdoor
Date: 2024-12-15T10:19:29-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-12-15-winnti-hackers-target-other-threat-actors-with-new-glutton-php-backdoor

[Source](https://www.bleepingcomputer.com/news/security/winnti-hackers-target-other-threat-actors-with-new-glutton-php-backdoor/){:target="_blank" rel="noopener"}

> ​The Chinese Winnti hacking group is using a new PHP backdoor named 'Glutton' in attacks on organizations in China and the U.S., and also in attacks on other cybercriminals. [...]
