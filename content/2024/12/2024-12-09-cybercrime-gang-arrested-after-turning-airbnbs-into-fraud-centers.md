Title: Cybercrime gang arrested after turning Airbnbs into fraud centers
Date: 2024-12-09T11:55:34-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-12-09-cybercrime-gang-arrested-after-turning-airbnbs-into-fraud-centers

[Source](https://www.bleepingcomputer.com/news/security/cybercrime-gang-arrested-after-turning-airbnbs-into-fraud-centers/){:target="_blank" rel="noopener"}

> Eight members of an international cybercrime network that stole millions of Euros from victims and set up Airbnb fraud centers were arrested in Belgium and the Netherlands. [...]
