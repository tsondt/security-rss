Title: Telco security is a dumpster fire and everyone's getting burned
Date: 2024-12-02T09:30:12+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2024-12-02-telco-security-is-a-dumpster-fire-and-everyones-getting-burned

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/02/telco_security_opinion/){:target="_blank" rel="noopener"}

> The politics of cybersecurity are too important to be left to the politicians Opinion Here's a front-page headline you won't see these days: CHINA'S SPIES ARE TAPPING OUR PHONES. Not that they're not – they are – but, like the environment, there's so much cybersecurity horror in the media that, yes, of course they are. And?... [...]
