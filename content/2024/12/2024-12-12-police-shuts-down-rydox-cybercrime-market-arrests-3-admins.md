Title: Police shuts down Rydox cybercrime market, arrests 3 admins
Date: 2024-12-12T17:11:28-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-12-12-police-shuts-down-rydox-cybercrime-market-arrests-3-admins

[Source](https://www.bleepingcomputer.com/news/security/police-shuts-down-rydox-cybercrime-market-arrests-3-admins/){:target="_blank" rel="noopener"}

> International law enforcement operation seizes the Rydox cybercrime marketplace and arrests three administrators. [...]
