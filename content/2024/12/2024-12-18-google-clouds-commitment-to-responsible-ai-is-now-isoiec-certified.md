Title: Google Cloud's commitment to responsible AI is now ISO/IEC certified
Date: 2024-12-18T17:00:00+00:00
Author: Jeanette Manfra
Category: GCP Security
Tags: AI & Machine Learning;Security & Identity
Slug: 2024-12-18-google-clouds-commitment-to-responsible-ai-is-now-isoiec-certified

[Source](https://cloud.google.com/blog/products/identity-security/google-clouds-commitment-to-responsible-ai-is-now-iso-iec-certified/){:target="_blank" rel="noopener"}

> With the rapid advancement and adoption of AI, organizations face increasing pressure to ensure their AI systems are developed and used responsibly. This includes considerations around bias, fairness, transparency, privacy, and security. A comprehensive framework for managing the risks and opportunities associated with AI can help by offering a structured approach to building trust and mitigating potential harm. The ISO/IEC 42001:2023 standard provides a framework for addressing the unique challenges AI poses, and we're excited to announce that Google Cloud has achieved an accredited ISO/IEC 42001:2023 certification for our AI management system. This certification helps demonstrate our commitment to developing [...]
