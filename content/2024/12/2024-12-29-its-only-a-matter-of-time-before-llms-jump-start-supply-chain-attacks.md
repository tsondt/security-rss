Title: It's only a matter of time before LLMs jump start supply-chain attacks
Date: 2024-12-29T18:20:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-12-29-its-only-a-matter-of-time-before-llms-jump-start-supply-chain-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/29/llm_supply_chain_attacks/){:target="_blank" rel="noopener"}

> 'The greatest concern is with spear phishing and social engineering' Interview Now that criminals have realized there's no need to train their own LLMs for any nefarious purposes - it's much cheaper and easier to steal credentials and then jailbreak existing ones - the threat of a large-scale supply chain attack using generative AI becomes more real.... [...]
