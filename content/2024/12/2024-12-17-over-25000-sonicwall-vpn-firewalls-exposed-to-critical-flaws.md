Title: Over 25,000 SonicWall VPN Firewalls exposed to critical flaws
Date: 2024-12-17T10:27:14-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-12-17-over-25000-sonicwall-vpn-firewalls-exposed-to-critical-flaws

[Source](https://www.bleepingcomputer.com/news/security/over-25-000-sonicwall-vpn-firewalls-exposed-to-critical-flaws/){:target="_blank" rel="noopener"}

> Over 25,000 publicly accessible SonicWall SSLVPN devices are vulnerable to critical severity flaws, with 20,000 using a SonicOS/OSX firmware version that the vendor no longer supports. [...]
