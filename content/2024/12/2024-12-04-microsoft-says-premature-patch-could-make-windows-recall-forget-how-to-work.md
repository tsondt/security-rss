Title: Microsoft says premature patch could make Windows Recall forget how to work
Date: 2024-12-04T14:03:15+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-12-04-microsoft-says-premature-patch-could-make-windows-recall-forget-how-to-work

[Source](https://go.theregister.com/feed/www.theregister.com/2024/12/04/microsoft_update_recall_bug/){:target="_blank" rel="noopener"}

> Installed the final non-security preview update of 2024? Best not hop onto the Dev Channel Microsoft has pinned down why some eager Windows Insiders could not persuade the Recall preview to save any snapshots. It's all down to a pesky non-security preview.... [...]
