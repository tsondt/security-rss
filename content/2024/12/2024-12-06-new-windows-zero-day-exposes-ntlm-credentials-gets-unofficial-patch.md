Title: New Windows zero-day exposes NTLM credentials, gets unofficial patch
Date: 2024-12-06T11:32:05-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-12-06-new-windows-zero-day-exposes-ntlm-credentials-gets-unofficial-patch

[Source](https://www.bleepingcomputer.com/news/security/new-windows-zero-day-exposes-ntlm-credentials-gets-unofficial-patch/){:target="_blank" rel="noopener"}

> A new zero-day vulnerability has been discovered that allows attackers to capture NTLM credentials by simply tricking the target into viewing a malicious file in Windows Explorer. [...]
