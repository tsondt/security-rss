Title: Bitcoin ATM firm Byte Federal hacked via GitLab flaw, 58K users exposed
Date: 2024-12-12T11:02:37-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-12-12-bitcoin-atm-firm-byte-federal-hacked-via-gitlab-flaw-58k-users-exposed

[Source](https://www.bleepingcomputer.com/news/security/bitcoin-atm-firm-byte-federal-hacked-via-gitlab-flaw-58k-users-exposed/){:target="_blank" rel="noopener"}

> US Bitcoin ATM operator Byte Federal has disclosed a data breach that exposed the data of 58,000 customers after its systems were breached using a GitLab vulnerability. [...]
