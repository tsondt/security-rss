Title: Nebraska man pleads guilty to $3.5 million cryptojacking scheme
Date: 2024-12-05T19:05:31-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-12-05-nebraska-man-pleads-guilty-to-35-million-cryptojacking-scheme

[Source](https://www.bleepingcomputer.com/news/security/nebraska-man-pleads-guilty-to-35-million-cryptojacking-scheme/){:target="_blank" rel="noopener"}

> ​A Nebraska man pleaded guilty on Thursday to operating a large-scale cryptojacking operation after being arrested and charged in April. [...]
