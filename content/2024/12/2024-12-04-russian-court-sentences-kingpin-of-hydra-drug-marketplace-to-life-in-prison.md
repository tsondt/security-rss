Title: Russian court sentences kingpin of Hydra drug marketplace to life in prison
Date: 2024-12-04T12:15:10+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;courts;dark net;drugs;ransomware
Slug: 2024-12-04-russian-court-sentences-kingpin-of-hydra-drug-marketplace-to-life-in-prison

[Source](https://arstechnica.com/information-technology/2024/12/russian-court-sentences-kingpin-of-hydra-drug-marketplace-to-life-in-prison/){:target="_blank" rel="noopener"}

> A Russian court has issued a life sentence to a man found guilty of being the kingpin of a dark web drug marketplace that supplied more than a metric ton of narcotics and psychotropic substances to customers around the world. On Monday, the court found that Stanislav Moiseyev oversaw Hydra, a Russian-language market that operated an anonymous website that matched sellers of drugs and other illicit wares with buyers. Hydra was dismantled in 2022 after authorities in Germany seized servers and other infrastructure used by the sprawling billion-dollar enterprise and a stash of bitcoin worth millions of dollars. At the [...]
