Title: FBI: U.S. lost record $12.5 billion to online crime in 2023
Date: 2024-03-07T07:53:13-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-07-fbi-us-lost-record-125-billion-to-online-crime-in-2023

[Source](https://www.bleepingcomputer.com/news/security/fbi-us-lost-record-125-billion-to-online-crime-in-2023/){:target="_blank" rel="noopener"}

> FBI's Internet Crime Complaint Center (IC3) has released its 2023 Internet Crime Report, which recorded a 22% increase in reported losses compared to 2022, amounting to a record of $12.5 billion. [...]
