Title: Nissan confirms ransomware attack exposed data of 100,000 people
Date: 2024-03-14T09:04:25-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-14-nissan-confirms-ransomware-attack-exposed-data-of-100000-people

[Source](https://www.bleepingcomputer.com/news/security/nissan-confirms-ransomware-attack-exposed-data-of-100-000-people/){:target="_blank" rel="noopener"}

> Nissan Oceania is warning of a data breach impacting 100,000 people after suffering a cyberattack in December 2023 that was claimed by the Akira ransomware operation. [...]
