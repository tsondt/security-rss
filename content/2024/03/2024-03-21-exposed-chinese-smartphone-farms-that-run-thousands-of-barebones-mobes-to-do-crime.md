Title: Exposed: Chinese smartphone farms that run thousands of barebones mobes to do crime
Date: 2024-03-21T06:32:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-03-21-exposed-chinese-smartphone-farms-that-run-thousands-of-barebones-mobes-to-do-crime

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/21/china_smartphone_farms/){:target="_blank" rel="noopener"}

> Operators pack twenty phones into a chassis – then rack 'em and stack 'em ready to do evil Chinese upstarts are selling smartphone motherboards – and kit to run and manage them at scale – to operators of outfits that use them to commit various scams and crimes, according to an undercover investigation by state television broadcaster China Central Television (CCTV) revealed late last week.... [...]
