Title: Uncle Sam's had it up to here with 'unforgivable' SQL injection flaws
Date: 2024-03-26T16:45:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-26-uncle-sams-had-it-up-to-here-with-unforgivable-sql-injection-flaws

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/26/fbi_cisa_sql_injection/){:target="_blank" rel="noopener"}

> Software slackers urged to up their game The US has clearly had enough of software vendors shipping products with "unforgivable" vulnerabilities, and is now urging them to launch formal code reviews to stamp out SQL injection flaws.... [...]
