Title: These 17,000 unpatched Microsoft Exchange servers are a ticking time bomb
Date: 2024-03-28T07:45:06+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-03-28-these-17000-unpatched-microsoft-exchange-servers-are-a-ticking-time-bomb

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/28/germany_microsoft_exchange_patch/){:target="_blank" rel="noopener"}

> One might say this is a wurst case scenario The German Federal Office for Information Security (BIS) has issued an urgent alert about the poor state of Microsoft Exchange Server patching in the country.... [...]
