Title: New acoustic attack determines keystrokes from typing patterns
Date: 2024-03-17T10:22:43-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-17-new-acoustic-attack-determines-keystrokes-from-typing-patterns

[Source](https://www.bleepingcomputer.com/news/security/new-acoustic-attack-determines-keystrokes-from-typing-patterns/){:target="_blank" rel="noopener"}

> Researchers have demonstrated a new acoustic side-channel attack on keyboards that can deduce user input based on their typing patterns, even in poor conditions, such as environments with noise. [...]
