Title: French unemployment agency data breach impacts 43 million people
Date: 2024-03-14T09:32:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-14-french-unemployment-agency-data-breach-impacts-43-million-people

[Source](https://www.bleepingcomputer.com/news/security/french-unemployment-agency-data-breach-impacts-43-million-people/){:target="_blank" rel="noopener"}

> France Travail, formerly known as Pôle Emploi, is warning that hackers breached its systems and may leak or exploit personal details of an estimated 43 million individuals. [...]
