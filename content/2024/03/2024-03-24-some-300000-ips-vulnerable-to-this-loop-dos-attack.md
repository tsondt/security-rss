Title: Some 300,000 IPs vulnerable to this Loop DoS attack
Date: 2024-03-24T18:37:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-24-some-300000-ips-vulnerable-to-this-loop-dos-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/24/loop_ip_vulnerable/){:target="_blank" rel="noopener"}

> Easy to exploit, not yet exploited, not widely patched – pick three As many as 300,000 servers or devices on the public internet are thought to be vulnerable right now to the recently disclosed Loop Denial-of-Service technique that works against some UDP-based application-level services.... [...]
