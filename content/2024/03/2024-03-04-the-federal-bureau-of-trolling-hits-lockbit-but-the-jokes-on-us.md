Title: The federal bureau of trolling hits LockBit, but the joke's on us
Date: 2024-03-04T09:30:13+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2024-03-04-the-federal-bureau-of-trolling-hits-lockbit-but-the-jokes-on-us

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/04/opinion/){:target="_blank" rel="noopener"}

> When you can't lock 'em up, lock 'em out Opinion The best cop shows excel at mind games: who's tricking whom, who really wins, and what price they pay. A twist of humor adds to the drama and keeps us hooked. It's rare enough in real life, far less so in the grim meat grinder of cybersecurity, yet sometimes it happens. It's happening right now.... [...]
