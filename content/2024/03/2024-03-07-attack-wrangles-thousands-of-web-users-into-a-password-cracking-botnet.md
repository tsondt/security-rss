Title: Attack wrangles thousands of web users into a password-cracking botnet
Date: 2024-03-07T22:29:46+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;botnets;password cracking;passwords;websites
Slug: 2024-03-07-attack-wrangles-thousands-of-web-users-into-a-password-cracking-botnet

[Source](https://arstechnica.com/?p=2008817){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Attackers have transformed hundreds of hacked sites running WordPress software into command-and-control servers that force visitors’ browsers to perform password-cracking attacks. A web search for the JavaScript that performs the attack showed it was hosted on 708 sites at the time this post went live on Ars, up from 500 two days ago. Denis Sinegubko, the researcher who spotted the campaign, said at the time that he had seen thousands of visitor computers running the script, which caused them to reach out to thousands of domains in an attempt to guess the passwords of usernames with [...]
