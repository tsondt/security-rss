Title: Canonical cracks down on crypto cons following Snap Store scam spree
Date: 2024-03-28T11:45:13+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2024-03-28-canonical-cracks-down-on-crypto-cons-following-snap-store-scam-spree

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/28/canonical_snap_store_scams/){:target="_blank" rel="noopener"}

> In happier news, Ubuntu Pro extended support now goes up to 12 years After multiple waves of cryptocurrency credential-stealing apps were uploaded to the Snap store, Canonical is changing its policies.... [...]
