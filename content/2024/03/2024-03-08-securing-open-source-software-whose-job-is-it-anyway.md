Title: Securing open source software: Whose job is it, anyway?
Date: 2024-03-08T01:02:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-08-securing-open-source-software-whose-job-is-it-anyway

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/08/securing_opensource_software_whose_job/){:target="_blank" rel="noopener"}

> CISA announces more help, and calls on app makers to step up The US government and some of the largest open source foundations and package repositories have announced a series of initiatives intended to improve software supply-chain security, while also repeating calls for developers to increase support for such efforts.... [...]
