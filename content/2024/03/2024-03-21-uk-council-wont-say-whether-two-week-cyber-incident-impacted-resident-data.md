Title: UK council won't say whether two-week 'cyber incident' impacted resident data
Date: 2024-03-21T11:37:52+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-21-uk-council-wont-say-whether-two-week-cyber-incident-impacted-resident-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/21/shock_uk_councils_recovery_from/){:target="_blank" rel="noopener"}

> Security experts insist ransomware is involved but Leicester zips its lips Leicester City Council continues to battle a suspected ransomware attack while keeping schtum about the key details.... [...]
