Title: Stanford University failed to detect ransomware intruders for 4 months
Date: 2024-03-13T12:05:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-13-stanford-university-failed-to-detect-ransomware-intruders-for-4-months

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/13/stanford_university_ransomware/){:target="_blank" rel="noopener"}

> 27,000 individuals had data stolen, which for some included names and social security numbers Stanford University says the cybersecurity incident it dealt with last year was indeed ransomware, which it failed to spot for more than four months.... [...]
