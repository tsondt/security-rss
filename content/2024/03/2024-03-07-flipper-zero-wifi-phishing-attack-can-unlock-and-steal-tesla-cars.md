Title: Flipper Zero WiFi phishing attack can unlock and steal Tesla cars
Date: 2024-03-07T12:07:51-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Technology
Slug: 2024-03-07-flipper-zero-wifi-phishing-attack-can-unlock-and-steal-tesla-cars

[Source](https://www.bleepingcomputer.com/news/security/flipper-zero-wifi-phishing-attack-can-unlock-and-steal-tesla-cars/){:target="_blank" rel="noopener"}

> An easy phishing attack using a Flipper Zero device can lead to compromising Tesla accounts, unlocking cars, and starting them. The attack works on the latest Tesla app, version 4.30.6, and Tesla software version 11.1 2024.2.7. [...]
