Title: Critical Fortinet flaw may impact 150,000 exposed devices
Date: 2024-03-08T15:37:05-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-03-08-critical-fortinet-flaw-may-impact-150000-exposed-devices

[Source](https://www.bleepingcomputer.com/news/security/critical-fortinet-flaw-may-impact-150-000-exposed-devices/){:target="_blank" rel="noopener"}

> Scans on the public web show that approximately 150,000 Fortinet FortiOS and FortiProxy secure web gateway systems are vulnerable to CVE-2024-21762, a critical security issue that allows executing code without authentication. [...]
