Title: Yacht dealer to the stars attacked by Rhysida ransomware gang
Date: 2024-03-21T15:30:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-21-yacht-dealer-to-the-stars-attacked-by-rhysida-ransomware-gang

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/21/luxury_yacht_dealer_rhysida/){:target="_blank" rel="noopener"}

> MarineMax may be in choppy waters after 'stolen data' given million-dollar price tag The Rhysida ransomware group claims it was responsible for the cyberattack at US luxury yacht dealer MarineMax earlier this month.... [...]
