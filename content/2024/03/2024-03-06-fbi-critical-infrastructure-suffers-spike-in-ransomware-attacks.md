Title: FBI: Critical infrastructure suffers spike in ransomware attacks
Date: 2024-03-06T20:49:31+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-06-fbi-critical-infrastructure-suffers-spike-in-ransomware-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/06/fbi_ransomware_cybercrime_costs/){:target="_blank" rel="noopener"}

> Jump in overall cybercrime reports, $60M-plus reportedly lost to extortionists alone, Feds reckon Digital crimes potentially cost victims more than $12.5 billion last year, according to the FBI's latest Internet Crime Complaint Center (IC3) annual report.... [...]
