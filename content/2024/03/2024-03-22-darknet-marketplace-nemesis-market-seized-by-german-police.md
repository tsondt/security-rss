Title: Darknet marketplace Nemesis Market seized by German police
Date: 2024-03-22T12:12:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-22-darknet-marketplace-nemesis-market-seized-by-german-police

[Source](https://www.bleepingcomputer.com/news/security/darknet-marketplace-nemesis-market-seized-by-german-police/){:target="_blank" rel="noopener"}

> The German police have seized infrastructure for the darknet Nemesis Market cybercrime marketplace in Germany and Lithuania, disrupting the site's operation. [...]
