Title: Ukraine claims it hacked Russian Ministry of Defense servers
Date: 2024-03-04T10:41:49-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-04-ukraine-claims-it-hacked-russian-ministry-of-defense-servers

[Source](https://www.bleepingcomputer.com/news/security/ukraine-claims-it-hacked-russian-ministry-of-defense-servers/){:target="_blank" rel="noopener"}

> The Main Intelligence Directorate (GUR) of Ukraine's Ministry of Defense claims that it breached the servers of the Russian Ministry of Defense (Minoborony) and stole sensitive documents. [...]
