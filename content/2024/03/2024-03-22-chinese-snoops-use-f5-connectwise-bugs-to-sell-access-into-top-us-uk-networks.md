Title: Chinese snoops use F5, ConnectWise bugs to sell access into top US, UK networks
Date: 2024-03-22T22:02:48+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-22-chinese-snoops-use-f5-connectwise-bugs-to-sell-access-into-top-us-uk-networks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/22/china_f5_connectwise_unc5174/){:target="_blank" rel="noopener"}

> Crew may well be working under contract for Beijing Chinese spies exploited a couple of critical-severity bugs in F5 and ConnectWise equipment earlier this year to sell access to compromised US defense organizations, UK government agencies, and hundreds of other entities, according to Mandiant.... [...]
