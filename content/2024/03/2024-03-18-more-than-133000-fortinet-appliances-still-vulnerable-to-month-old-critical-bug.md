Title: More than 133,000 Fortinet appliances still vulnerable to month-old critical bug
Date: 2024-03-18T19:00:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-18-more-than-133000-fortinet-appliances-still-vulnerable-to-month-old-critical-bug

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/18/more_than_133000_fortinet_appliances/){:target="_blank" rel="noopener"}

> A huge attack surface for a vulnerability with various PoCs available The volume of Fortinet boxes exposed to the public internet and vulnerable to a month-old critical security flaw in FortiOS is still extremely high, despite a gradual increase in patching.... [...]
