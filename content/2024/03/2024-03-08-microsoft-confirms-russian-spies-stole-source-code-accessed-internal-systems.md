Title: Microsoft confirms Russian spies stole source code, accessed internal systems
Date: 2024-03-08T16:56:46+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-08-microsoft-confirms-russian-spies-stole-source-code-accessed-internal-systems

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/08/microsoft_confirms_russian_spies_stole/){:target="_blank" rel="noopener"}

> Still 'no evidence' of any compromised customer-facing systems, we're told Microsoft has now confirmed that the Russian cyberspies who broke into its executives' email accounts stole source code and gained access to internal systems. The Redmond giant also characterized the intrusion as "ongoing."... [...]
