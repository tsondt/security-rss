Title: Lawsuit claims gift card fraud is the gift that keeps on giving, to Google
Date: 2024-03-07T01:15:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-03-07-lawsuit-claims-gift-card-fraud-is-the-gift-that-keeps-on-giving-to-google

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/07/google_sued_for_profiting_gift_card_fraud/){:target="_blank" rel="noopener"}

> Play Store commissions are a nice little earner, wherever they come from Google has been accused of profiting from gift card scams.... [...]
