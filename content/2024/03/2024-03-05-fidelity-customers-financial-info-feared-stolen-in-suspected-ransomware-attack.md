Title: Fidelity customers' financial info feared stolen in suspected ransomware attack
Date: 2024-03-05T19:28:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-05-fidelity-customers-financial-info-feared-stolen-in-suspected-ransomware-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/05/fidelity_financial_info_stolen/){:target="_blank" rel="noopener"}

> Insurance giant blames Infosys, LockBit claims credit Criminals have probably stolen nearly 30,000 Fidelity Investments Life Insurance customers' personal and financial information — including bank account and routing numbers, credit card numbers and security or access codes — after breaking into Infosys' IT systems in the fall.... [...]
