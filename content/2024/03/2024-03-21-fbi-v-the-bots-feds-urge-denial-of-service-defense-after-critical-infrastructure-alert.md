Title: FBI v the bots: Feds urge denial-of-service defense after critical infrastructure alert
Date: 2024-03-21T22:20:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-21-fbi-v-the-bots-feds-urge-denial-of-service-defense-after-critical-infrastructure-alert

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/21/fbi_ddos_advice/){:target="_blank" rel="noopener"}

> You better watch out, you better not cry, better not pout, they're telling you why The US government has recommended a series of steps that critical infrastructure operators should take to prevent distributed-denial-of-service (DDoS) attacks.... [...]
