Title: A Close Up Look at the Consumer Data Broker Radaris
Date: 2024-03-08T13:02:48+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Breadcrumbs;affiliate.ru;American Russian Media Inc.;arrestfacts.com;Atlas Data Privacy Corp.;Bitseller Expert Ltd;bizstanding.com;centeda.com;Channel One;clubset.com;consumer reporting agencies;Daniel's Law;Dating Factory;difive.com;Dmitry Lybarsky;epop@comby.com;FACT Magazine;Fair Credit Reporting Act;Gary Nard;Gary Norden;gary1@eprofit.com;gary@barksy.com;homeflock.com;homemetry.com;Igor Lybarsky;instantcheckmate.com;kworld.com;newenglandfacts.com;NTV;PersonTrust.com;projectlab.com;pub360.com;publicreports.com;Radaris.com;radaris.ru;rehold.com;rosconcert.com;trustoria.com;TruthFinder;U.S. Department of Justice;U.S. Federal Communications Commission;U.S. Federal Trade Commission;Unipoint Technology Inc.;virtory.com
Slug: 2024-03-08-a-close-up-look-at-the-consumer-data-broker-radaris

[Source](https://krebsonsecurity.com/2024/03/a-close-up-look-at-the-consumer-data-broker-radaris/){:target="_blank" rel="noopener"}

> If you live in the United States, the data broker Radaris likely knows a great deal about you, and they are happy to sell what they know to anyone. But how much do we know about Radaris? Publicly available data indicates that in addition to running a dizzying array of people-search websites, the co-founders of Radaris operate multiple Russian-language dating services and affiliate programs. It also appears many of their businesses have ties to a California marketing firm that works with a Russian state-run media conglomerate currently sanctioned by the U.S. government. Formed in 2009, Radaris is a vast people-search [...]
