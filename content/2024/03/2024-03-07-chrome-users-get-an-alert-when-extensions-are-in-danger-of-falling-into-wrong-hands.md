Title: Chrome users – get an alert when extensions are in danger of falling into wrong hands
Date: 2024-03-07T19:45:27+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-03-07-chrome-users-get-an-alert-when-extensions-are-in-danger-of-falling-into-wrong-hands

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/07/chrome_extension_changes/){:target="_blank" rel="noopener"}

> Under New Management is an early-warning system for potential poisoning of add-ons with malware Millions of Chrome users now have a way to guard against the threat of extension subversion, that is, if they don't mind installing yet another browser extension.... [...]
