Title: Former telecom manager admits to doing SIM swaps for $1,000
Date: 2024-03-15T11:26:17-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-03-15-former-telecom-manager-admits-to-doing-sim-swaps-for-1000

[Source](https://www.bleepingcomputer.com/news/security/former-telecom-manager-admits-to-doing-sim-swaps-for-1-000/){:target="_blank" rel="noopener"}

> A former manager at a telecommunications company in New Jersey pleaded guilty to conspiracy charges for accepting money to perform unauthorized SIM swaps that enabled an accomplice to hack customer accounts. [...]
