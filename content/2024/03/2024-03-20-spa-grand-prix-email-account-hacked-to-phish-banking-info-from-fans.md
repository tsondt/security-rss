Title: Spa Grand Prix email account hacked to phish banking info from fans
Date: 2024-03-20T16:02:23-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-20-spa-grand-prix-email-account-hacked-to-phish-banking-info-from-fans

[Source](https://www.bleepingcomputer.com/news/security/spa-grand-prix-email-account-hacked-to-phish-banking-info-from-fans/){:target="_blank" rel="noopener"}

> Hackers hijacked the official contact email for the Belgian Grand Prix event and used it to lure fans to a fake website promising a €50 gift voucher. [...]
