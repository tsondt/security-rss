Title: VMware urges emergency action to blunt hypervisor flaws
Date: 2024-03-07T07:30:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-03-07-vmware-urges-emergency-action-to-blunt-hypervisor-flaws

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/07/vmware_usb_critical_flaws/){:target="_blank" rel="noopener"}

> Critical vulns in USB under ESXi and desktop hypervisors found by Chinese researchers at cracking contest Hypervisors are supposed to provide an inviolable isolation layer between virtual machines and hardware. But hypervisor heavyweight VMware by Broadcom yesterday revealed its hypervisors are not quite so inviolable as it might like.... [...]
