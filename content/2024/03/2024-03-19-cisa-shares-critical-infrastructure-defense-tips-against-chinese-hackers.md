Title: CISA shares critical infrastructure defense tips against Chinese hackers
Date: 2024-03-19T16:18:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-03-19-cisa-shares-critical-infrastructure-defense-tips-against-chinese-hackers

[Source](https://www.bleepingcomputer.com/news/security/cisa-shares-critical-infrastructure-defense-tips-against-chinese-hackers/){:target="_blank" rel="noopener"}

> CISA, the NSA, the FBI, and several other agencies in the U.S. and worldwide warned critical infrastructure leaders to protect their systems against the Chinese Volt Typhoon hacking group. [...]
