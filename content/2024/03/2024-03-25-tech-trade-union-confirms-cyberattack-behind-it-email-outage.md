Title: Tech trade union confirms cyberattack behind IT, email outage
Date: 2024-03-25T15:31:56+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-25-tech-trade-union-confirms-cyberattack-behind-it-email-outage

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/25/cwu_security_incident/){:target="_blank" rel="noopener"}

> Systems have been pulled offline as a precaution Exclusive The Communications Workers Union (CWU), which represents hundreds of thousands of employees in sectors across the UK economy including tech and telecoms, is currently working to mitigate a cyberattack.... [...]
