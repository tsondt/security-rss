Title: Iranian charged over attacks against US defense contractors, government agencies
Date: 2024-03-01T18:30:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-03-01-iranian-charged-over-attacks-against-us-defense-contractors-government-agencies

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/01/iranian_cyberattack_charges/){:target="_blank" rel="noopener"}

> $10M bounty for anyone with info leading to Alireza Shafie Nasab's identification or location The US Department of Justice has unsealed an indictment accusing an Iranian national of a years-long campaign that compromised hundreds of thousands of accounts and attempting to infiltrate US defense contractors and multiple government agencies.... [...]
