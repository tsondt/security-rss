Title: US prescription market hamstrung for 9 days (so far) by ransomware attack
Date: 2024-03-01T21:59:46+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;alphv;critical infrastructure;healthcare;ransomware
Slug: 2024-03-01-us-prescription-market-hamstrung-for-9-days-so-far-by-ransomware-attack

[Source](https://arstechnica.com/?p=2007373){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Nine days after a Russian-speaking ransomware syndicate took down the biggest US health care payment processor, pharmacies, health care providers, and patients were still scrambling to fill prescriptions for medicines, many of which are lifesaving. On Thursday, UnitedHealth Group accused a notorious ransomware gang known both as AlphV and Black Cat of hacking its subsidiary Optum. Optum provides a nationwide network called Change Healthcare, which allows health care providers to manage customer payments and insurance claims. With no easy way for pharmacies to calculate what costs were covered by insurance companies, many had to turn to [...]
