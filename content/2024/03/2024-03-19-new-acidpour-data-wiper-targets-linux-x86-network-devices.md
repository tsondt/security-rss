Title: New AcidPour data wiper targets Linux x86 network devices
Date: 2024-03-19T10:33:23-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Linux
Slug: 2024-03-19-new-acidpour-data-wiper-targets-linux-x86-network-devices

[Source](https://www.bleepingcomputer.com/news/security/new-acidpour-data-wiper-targets-linux-x86-network-devices/){:target="_blank" rel="noopener"}

> A new destructive malware named AcidPour was spotted in the wild, featuring data-wiper functionality and targeting Linux x86 IoT and networking devices. [...]
