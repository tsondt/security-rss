Title: UnitedHealth brings some Change Healthcare pharmacy services back online
Date: 2024-03-08T12:54:22-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare;Technology
Slug: 2024-03-08-unitedhealth-brings-some-change-healthcare-pharmacy-services-back-online

[Source](https://www.bleepingcomputer.com/news/security/unitedhealth-brings-some-change-healthcare-pharmacy-services-back-online/){:target="_blank" rel="noopener"}

> Optum's Change Healthcare has started to bring systems back online after suffering a crippling BlackCat ransomware attack last month that led to widespread disruption to the US healthcare system. [...]
