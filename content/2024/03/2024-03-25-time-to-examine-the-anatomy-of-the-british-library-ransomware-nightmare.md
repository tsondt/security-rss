Title: Time to examine the anatomy of the British Library ransomware nightmare
Date: 2024-03-25T09:30:10+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2024-03-25-time-to-examine-the-anatomy-of-the-british-library-ransomware-nightmare

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/25/opinion_column/){:target="_blank" rel="noopener"}

> Mistakes years in the making tell a universal story that must not be ignored Opinion Quiz time: name one thing you know about the Library of Alexandria. Points deducted for "it’s a library. In Alexandria." Looking things up is cheating and you know it.... [...]
