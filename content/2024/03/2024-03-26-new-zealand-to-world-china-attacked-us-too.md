Title: New Zealand to world: China attacked us, too!
Date: 2024-03-26T03:30:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-03-26-new-zealand-to-world-china-attacked-us-too

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/26/new_zealand_china_attack/){:target="_blank" rel="noopener"}

> Reveals 2021 incident that saw parliamentary agencies briefly probed The government of South Pacific island nation New Zealand has revealed that it, too, has been attacked by China.... [...]
