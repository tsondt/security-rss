Title: AI and the Evolution of Social Media
Date: 2024-03-19T11:05:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;Facebook;Google;Internet and society;LLM;privacy;social media;surveillance;Twitter
Slug: 2024-03-19-ai-and-the-evolution-of-social-media

[Source](https://www.schneier.com/blog/archives/2024/03/ai-and-the-evolution-of-social-media.html){:target="_blank" rel="noopener"}

> Oh, how the mighty have fallen. A decade ago, social media was celebrated for sparking democratic uprisings in the Arab world and beyond. Now front pages are splashed with stories of social platforms’ role in misinformation, business conspiracy, malfeasance, and risks to mental health. In a 2022 survey, Americans blamed social media for the coarsening of our political discourse, the spread of misinformation, and the increase in partisan polarization. Today, tech’s darling is artificial intelligence. Like social media, it has the potential to change the world in many ways, some favorable to democracy. But at the same time, it has [...]
