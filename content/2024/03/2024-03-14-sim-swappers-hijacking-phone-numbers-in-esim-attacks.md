Title: SIM swappers hijacking phone numbers in eSIM attacks
Date: 2024-03-14T14:08:43-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-14-sim-swappers-hijacking-phone-numbers-in-esim-attacks

[Source](https://www.bleepingcomputer.com/news/security/sim-swappers-hijacking-phone-numbers-in-esim-attacks/){:target="_blank" rel="noopener"}

> SIM swappers have adapted their attacks to steal a target's phone number by porting it into a new eSIM card, a digital SIM stored in a rewritable chip present on many recent smartphone models. [...]
