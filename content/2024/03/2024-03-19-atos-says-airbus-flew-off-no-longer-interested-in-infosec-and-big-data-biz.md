Title: Atos says Airbus flew off, no longer interested in infosec and big data biz
Date: 2024-03-19T12:30:14+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2024-03-19-atos-says-airbus-flew-off-no-longer-interested-in-infosec-and-big-data-biz

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/19/atos_says_airbus_talks_off/){:target="_blank" rel="noopener"}

> Ailing tech integrator takes a hard hit... share price down by up to 20% this morning Atos' share price sank as much as 20 percent this morning on confirmation that Airbus is no longer interested in buying the big data and security (BDS) parts of the crumbling tech empire.... [...]
