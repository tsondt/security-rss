Title: How Public AI Can Strengthen Democracy
Date: 2024-03-07T12:00:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;LLM
Slug: 2024-03-07-how-public-ai-can-strengthen-democracy

[Source](https://www.schneier.com/blog/archives/2024/03/how-public-ai-can-strengthen-democracy.html){:target="_blank" rel="noopener"}

> With the world’s focus turning to misinformation, manipulation, and outright propaganda ahead of the 2024 U.S. presidential election, we know that democracy has an AI problem. But we’re learning that AI has a democracy problem, too. Both challenges must be addressed for the sake of democratic governance and public protection. Just three Big Tech firms (Microsoft, Google, and Amazon) control about two-thirds of the global market for the cloud computing resources used to train and deploy AI models. They have a lot of the AI talent, the capacity for large-scale innovation, and face few public regulations for their products and [...]
