Title: Microsoft Copilot for Security prepares for April liftoff
Date: 2024-03-13T16:00:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-03-13-microsoft-copilot-for-security-prepares-for-april-liftoff

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/13/microsoft_copilot_for_security_ready/){:target="_blank" rel="noopener"}

> Automated AI helper intended to make security more manageable Microsoft Copilot for Security, a subscription AI security service, will be generally available on April 1, 2024, the company announced on Wednesday.... [...]
