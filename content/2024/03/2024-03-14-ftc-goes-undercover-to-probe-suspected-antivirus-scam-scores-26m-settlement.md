Title: FTC goes undercover to probe suspected antivirus scam, scores $26M settlement
Date: 2024-03-14T20:24:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-03-14-ftc-goes-undercover-to-probe-suspected-antivirus-scam-scores-26m-settlement

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/14/ftc_antivirus_probe/){:target="_blank" rel="noopener"}

> Imagine trying to trick folks into buying $500 of unnecessary repairs – and they turn out to be federal agents A pair of tech support businesses accused of swindling marks out of their hard-earned cash have agreed to cough up a $26 million settlement following an undercover probe by the FTC.... [...]
