Title: FTC warns scammers are impersonating its employees to steal money
Date: 2024-03-19T15:19:30-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-03-19-ftc-warns-scammers-are-impersonating-its-employees-to-steal-money

[Source](https://www.bleepingcomputer.com/news/security/ftc-warns-scammers-are-impersonating-its-employees-to-steal-money/){:target="_blank" rel="noopener"}

> The U.S. Federal Trade Commission (FTC) warned today that scammers are impersonating its employees to steal thousands of dollars from Americans. [...]
