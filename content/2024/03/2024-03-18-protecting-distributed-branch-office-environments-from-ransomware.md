Title: Protecting distributed branch office environments from ransomware
Date: 2024-03-18T03:00:11+00:00
Author: Jack Kirkstall
Category: The Register
Tags: 
Slug: 2024-03-18-protecting-distributed-branch-office-environments-from-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/18/protecting_distributed_branch_office_environments/){:target="_blank" rel="noopener"}

> As ransomware becomes more sophisticated, detection tools should be upgraded to cover every site and location Sponsored Feature Ransomware gangs that steal and encrypt vital business data before extorting payment for its decryption and restoration are ramping up global attacks at an ever-increasing rate. In fact, cyber security experts agree that ransomware now represents one of - if not the most - serious cybersecurity threats currently facing governments, public/private sector organisations and enterprises around the world.... [...]
