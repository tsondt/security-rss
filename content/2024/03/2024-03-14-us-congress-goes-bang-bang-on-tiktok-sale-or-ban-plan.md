Title: US Congress goes bang, bang, on TikTok sale-or-ban plan
Date: 2024-03-14T01:46:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-03-14-us-congress-goes-bang-bang-on-tiktok-sale-or-ban-plan

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/14/us_congress_passes_tiktok_ban/){:target="_blank" rel="noopener"}

> Bill proposes to do to China what China already does to the US – make life hard for foreign social networks The United States House of Representatives on Wednesday passed the Protecting Americans from Foreign Adversary Controlled Applications Act – a law aimed at forcing TikTok's Chinese parent ByteDance to sell the app's US operations or face the prospect of a ban.... [...]
