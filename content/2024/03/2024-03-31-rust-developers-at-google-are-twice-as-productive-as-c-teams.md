Title: Rust developers at Google are twice as productive as C++ teams
Date: 2024-03-31T16:33:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-03-31-rust-developers-at-google-are-twice-as-productive-as-c-teams

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/31/rust_google_c/){:target="_blank" rel="noopener"}

> Code shines up nicely in production, says Chocolate Factory's Bergstrom Echoing the past two years of Rust evangelism and C/C++ ennui, Google reports that Rust shines in production, to the point that its developers are twice as productive using the language compared to C++.... [...]
