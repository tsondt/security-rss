Title: Microsoft March 2024 Patch Tuesday fixes 60 flaws, 18 RCE bugs
Date: 2024-03-12T13:52:40-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-03-12-microsoft-march-2024-patch-tuesday-fixes-60-flaws-18-rce-bugs

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-march-2024-patch-tuesday-fixes-60-flaws-18-rce-bugs/){:target="_blank" rel="noopener"}

> Today is Microsoft's March 2024 Patch Tuesday, and security updates have been released for 60 vulnerabilities, including eighteen remote code execution flaws. [...]
