Title: Unsaflok flaw can let hackers unlock millions of hotel doors
Date: 2024-03-21T14:14:11-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2024-03-21-unsaflok-flaw-can-let-hackers-unlock-millions-of-hotel-doors

[Source](https://www.bleepingcomputer.com/news/security/unsaflok-flaw-can-let-hackers-unlock-millions-of-hotel-doors/){:target="_blank" rel="noopener"}

> Security vulnerabilities in over 3 million Saflok electronic RFID locks deployed in 13,000 hotels and homes worldwide allowed researchers to easily unlock any door in a hotel by forging a pair of keycards. [...]
