Title: Beijing-backed cyberspies attacked 70+ orgs across 23 countries
Date: 2024-03-19T21:00:40+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-19-beijing-backed-cyberspies-attacked-70-orgs-across-23-countries

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/19/china_cyberspies_earth_krahang/){:target="_blank" rel="noopener"}

> Plus potential links to I-Soon, researchers say Chinese cyberspies have compromised at least 70 organizations, mostly government entities, and targeted more than 116 victims across the globe, according to security researchers.... [...]
