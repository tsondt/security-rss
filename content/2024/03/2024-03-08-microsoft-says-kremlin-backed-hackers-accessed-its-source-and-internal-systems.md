Title: Microsoft says Kremlin-backed hackers accessed its source and internal systems
Date: 2024-03-08T18:42:39+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;apts;microsoft;midnight blizzard;source code
Slug: 2024-03-08-microsoft-says-kremlin-backed-hackers-accessed-its-source-and-internal-systems

[Source](https://arstechnica.com/?p=2008953){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) Microsoft said that Kremlin-backed hackers who breached its corporate network in January have expanded their access since then in follow-on attacks that are targeting customers and have compromised the company's source code and internal systems. The intrusion, which the software company disclosed in January, was carried out by Midnight Blizzard, the name used to track a hacking group widely attributed to the Federal Security Service, a Russian intelligence agency. Microsoft said at the time that Midnight Blizzard gained access to senior executives’ email accounts for months after first exploiting a weak password in a test [...]
