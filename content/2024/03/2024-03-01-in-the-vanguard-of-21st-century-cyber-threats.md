Title: In the vanguard of 21st century cyber threats
Date: 2024-03-01T16:00:08+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2024-03-01-in-the-vanguard-of-21st-century-cyber-threats

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/01/in_the_vanguard_of_21st/){:target="_blank" rel="noopener"}

> Everything you need to know about quantum safe encryption Webinar The quantum threat might seem futuristic, more like something you'd encounter in a science fiction film. But it's arguably already a danger to real cyber security defences.... [...]
