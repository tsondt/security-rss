Title: Burglars Using Wi-Fi Jammers to Disable Security Cameras
Date: 2024-03-13T11:07:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Internet of Things;jamming;theft;Wi-Fi
Slug: 2024-03-13-burglars-using-wi-fi-jammers-to-disable-security-cameras

[Source](https://www.schneier.com/blog/archives/2024/03/burglars-using-wi-fi-jammers-to-disable-security-cameras.html){:target="_blank" rel="noopener"}

> The arms race continues, as burglars are learning how to use jammers to disable Wi-Fi security cameras. [...]
