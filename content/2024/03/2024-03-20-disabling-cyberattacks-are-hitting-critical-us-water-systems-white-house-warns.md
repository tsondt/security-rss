Title: “Disabling cyberattacks” are hitting critical US water systems, White House warns
Date: 2024-03-20T00:26:15+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;critical infrastructure;cybersecurity;wastewater;water
Slug: 2024-03-20-disabling-cyberattacks-are-hitting-critical-us-water-systems-white-house-warns

[Source](https://arstechnica.com/?p=2011436){:target="_blank" rel="noopener"}

> Enlarge / Aerial view of a sewage treatment plant. (credit: Getty Images) The Biden administration on Tuesday warned the nation’s governors that drinking water and wastewater utilities in their states are facing “disabling cyberattacks” by hostile foreign nations that are targeting mission-critical plant operations. “Disabling cyberattacks are striking water and wastewater systems throughout the United States,” Jake Sullivan, assistant to the President for National Security Affairs, and Michael S. Regan, administrator of the Environmental Protection Agency, wrote in a letter. “These attacks have the potential to disrupt the critical lifeline of clean and safe drinking water, as well as impose [...]
