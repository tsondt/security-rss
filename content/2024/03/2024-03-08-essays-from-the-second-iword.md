Title: Essays from the Second IWORD
Date: 2024-03-08T18:38:32+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2024-03-08-essays-from-the-second-iword

[Source](https://www.schneier.com/blog/archives/2024/03/essays-from-the-second-iword.html){:target="_blank" rel="noopener"}

> The Ash Center has posted a series of twelve essays stemming from the Second Interdisciplinary Workshop on Reimagining Democracy ( IWORD 2023 ). Aviv Ovadya, Democracy as Approximation: A Primer for “AI for Democracy” Innovators Kathryn Peters, Permission and Participation Claudia Chwalisz, Moving Beyond the Paradigm of “Democracy”: 12 Questions Riley Wong, Privacy-Preserving Data Governance Christine Tran, Recommendations for Implementing Jail Voting: Identifying Common Themes Niclas Boehmer, The Double-Edged Sword of Algorithmic Governance: Transparency at Stake Manon Revel, Can We Talk? An Argument for More Dialogues in Academia Aditi Juneja, Ensuring We Have A Democracy in 2076 Nick Couldry, Resonance, [...]
