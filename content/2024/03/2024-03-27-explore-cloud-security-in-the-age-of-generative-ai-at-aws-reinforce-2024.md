Title: Explore cloud security in the age of generative AI at AWS re:Inforce 2024
Date: 2024-03-27T20:55:45+00:00
Author: Chris Betz
Category: AWS Security
Tags: Announcements;AWS re:Inforce;Foundational (100);Security, Identity, & Compliance;re:Inforce;Security Blog
Slug: 2024-03-27-explore-cloud-security-in-the-age-of-generative-ai-at-aws-reinforce-2024

[Source](https://aws.amazon.com/blogs/security/explore-cloud-security-in-the-age-of-generative-ai-at-aws-reinforce-2024/){:target="_blank" rel="noopener"}

> As the Chief Information Security Officer (CISO) at AWS, I’m personally committed to helping security teams of all skill levels and sizes navigate security for generative artificial intelligence (AI). As a former AWS customer, I know the value of hands-on security learning and talking in-person to the people who build and run AWS security. That’s [...]
