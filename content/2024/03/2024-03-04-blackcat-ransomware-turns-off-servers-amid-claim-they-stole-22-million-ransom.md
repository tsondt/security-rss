Title: BlackCat ransomware turns off servers amid claim they stole $22 million ransom
Date: 2024-03-04T12:44:36-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-03-04-blackcat-ransomware-turns-off-servers-amid-claim-they-stole-22-million-ransom

[Source](https://www.bleepingcomputer.com/news/security/blackcat-ransomware-turns-off-servers-amid-claim-they-stole-22-million-ransom/){:target="_blank" rel="noopener"}

> The ALPHV/BlackCat ransomware gang has shut down its servers amid claims that they scammed the affiliate responsible for the attack on Optum, the operator of the Change Healthcare platform, of $22 million. [...]
