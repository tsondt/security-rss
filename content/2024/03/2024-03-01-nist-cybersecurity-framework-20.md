Title: NIST Cybersecurity Framework 2.0
Date: 2024-03-01T12:08:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;infrastructure;NIST
Slug: 2024-03-01-nist-cybersecurity-framework-20

[Source](https://www.schneier.com/blog/archives/2024/03/nist-cybersecurity-framework-2-0.html){:target="_blank" rel="noopener"}

> NIST has released version 2.0 of the Cybersecurity Framework: The CSF 2.0, which supports implementation of the National Cybersecurity Strategy, has an expanded scope that goes beyond protecting critical infrastructure, such as hospitals and power plants, to all organizations in any sector. It also has a new focus on governance, which encompasses how organizations make and carry out informed decisions on cybersecurity strategy. The CSF’s governance component emphasizes that cybersecurity is a major source of enterprise risk that senior leaders should consider alongside others such as finance and reputation. [...] The framework’s core is now organized around six key functions: [...]
