Title: Filipino police free hundreds of slaves toiling in romance scam operation
Date: 2024-03-18T05:46:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-03-18-filipino-police-free-hundreds-of-slaves-toiling-in-romance-scam-operation

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/18/phillipines_cyberslavery_gang_busted/){:target="_blank" rel="noopener"}

> 875 workers liberated after falling for promises of lucrative work, nine arrested Filipino police rescued 875 "workers" – including 504 foreigners – in a raid late last week on a firm that posed as an online gaming company but in reality operated a forced labor camp that housed romance scam operators.... [...]
