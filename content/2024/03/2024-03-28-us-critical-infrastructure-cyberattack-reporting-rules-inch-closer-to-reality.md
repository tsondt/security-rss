Title: US critical infrastructure cyberattack reporting rules inch closer to reality
Date: 2024-03-28T13:30:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-28-us-critical-infrastructure-cyberattack-reporting-rules-inch-closer-to-reality

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/28/critical_infrastructure_cyberattack_reporting/){:target="_blank" rel="noopener"}

> After all, it's only about keeping the essentials on – no rush America's long-awaited cyber attack reporting rules for critical infrastructure operators are inching closer to implementation, after the Feds posted a notice of proposed rulemaking for the Cyber Incident Reporting for Critical Infrastructure Act (CIRCIA).... [...]
