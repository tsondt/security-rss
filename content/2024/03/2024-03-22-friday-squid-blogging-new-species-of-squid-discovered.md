Title: Friday Squid Blogging: New Species of Squid Discovered
Date: 2024-03-22T21:03:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-03-22-friday-squid-blogging-new-species-of-squid-discovered

[Source](https://www.schneier.com/blog/archives/2024/03/friday-squid-blogging-new-species-of-squid-discovered.html){:target="_blank" rel="noopener"}

> A new species of squid was discovered, along with about a hundred other species. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
