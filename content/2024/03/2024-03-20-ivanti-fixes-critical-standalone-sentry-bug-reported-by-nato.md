Title: Ivanti fixes critical Standalone Sentry bug reported by NATO
Date: 2024-03-20T13:08:58-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-03-20-ivanti-fixes-critical-standalone-sentry-bug-reported-by-nato

[Source](https://www.bleepingcomputer.com/news/security/ivanti-fixes-critical-standalone-sentry-bug-reported-by-nato/){:target="_blank" rel="noopener"}

> Ivanti warned customers to immediately patch a critical severity Standalone Sentry vulnerability reported by NATO Cyber Security Centre researchers. [...]
