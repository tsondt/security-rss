Title: Ransomware ban backers insist thugs must be cut off from payday
Date: 2024-03-04T14:30:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-04-ransomware-ban-backers-insist-thugs-must-be-cut-off-from-payday

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/04/experts_echo_calls_for_ransomware/){:target="_blank" rel="noopener"}

> Increasingly clear number of permanent solutions is narrowing Global law enforcement authorities' attempts to shutter the LockBit ransomware crew have sparked a fresh call for a ban on ransomware payments to perpetrators.... [...]
