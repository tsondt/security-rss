Title: Jailbreaking LLMs with ASCII Art
Date: 2024-03-12T11:12:15+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence;chatbots;hacking;LLM
Slug: 2024-03-12-jailbreaking-llms-with-ascii-art

[Source](https://www.schneier.com/blog/archives/2024/03/jailbreaking-llms-with-ascii-art.html){:target="_blank" rel="noopener"}

> Researchers have demonstrated that putting words in ASCII art can cause LLMs—GPT-3.5, GPT-4, Gemini, Claude, and Llama2—to ignore their safety instructions. Research paper. [...]
