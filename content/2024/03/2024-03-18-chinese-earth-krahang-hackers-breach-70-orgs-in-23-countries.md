Title: Chinese Earth Krahang hackers breach 70 orgs in 23 countries
Date: 2024-03-18T16:49:17-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-18-chinese-earth-krahang-hackers-breach-70-orgs-in-23-countries

[Source](https://www.bleepingcomputer.com/news/security/chinese-earth-krahang-hackers-breach-70-orgs-in-23-countries/){:target="_blank" rel="noopener"}

> A sophisticated hacking campaign attributed to a Chinese Advanced Persistent Threat (APT) group known as 'Earth Krahang' has breached 70 organizations and targeted at least 116 across 45 countries. [...]
