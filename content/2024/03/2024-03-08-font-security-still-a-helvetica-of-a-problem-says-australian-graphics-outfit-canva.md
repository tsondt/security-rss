Title: Font security 'still a Helvetica of a problem' says Australian graphics outfit Canva
Date: 2024-03-08T03:57:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-03-08-font-security-still-a-helvetica-of-a-problem-says-australian-graphics-outfit-canva

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/08/canva_font_security/){:target="_blank" rel="noopener"}

> Who knew that unzipping a font archive could unleash a malicious file Online graphic design platform Canva went looking for security problems in fonts, and found three – in "strange places."... [...]
