Title: LockBit ransomware kingpin gets 4 years behind bars
Date: 2024-03-14T18:26:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-14-lockbit-ransomware-kingpin-gets-4-years-behind-bars

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/14/lockbit_ransomware_criminal_sentenced/){:target="_blank" rel="noopener"}

> Canadian-Russian said to have turned to a life of cybercrime during pandemic, now must pay the price – literally A LockBit ransomware kingpin has been sentenced to almost four years behind bars and ordered to pay more than CA$860,000 ($635,000, £500,000) in restitution to some of his victims by a Canadian court as he awaits extradition to the US.... [...]
