Title: MiTM phishing attack can let attackers unlock and steal a Tesla
Date: 2024-03-07T12:07:51-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Technology
Slug: 2024-03-07-mitm-phishing-attack-can-let-attackers-unlock-and-steal-a-tesla

[Source](https://www.bleepingcomputer.com/news/security/mitm-phishing-attack-can-let-attackers-unlock-and-steal-a-tesla/){:target="_blank" rel="noopener"}

> Researchers demonstrated how they could conduct a Man-in-the-Middle (MiTM) phishing attack to compromise Tesla accounts, unlocking cars, and starting them. The attack works on the latest Tesla app, version 4.30.6, and Tesla software version 11.1 2024.2.7. [...]
