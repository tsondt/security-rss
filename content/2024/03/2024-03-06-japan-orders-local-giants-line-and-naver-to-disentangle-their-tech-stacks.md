Title: Japan orders local giants LINE and NAVER to disentangle their tech stacks
Date: 2024-03-06T03:29:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-03-06-japan-orders-local-giants-line-and-naver-to-disentangle-their-tech-stacks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/06/japan_line_naver_infosec_guidance/){:target="_blank" rel="noopener"}

> Government mighty displeased about a shared Active Directory that led to a big data leak Japan's government has ordered local tech giants LINE and NAVER to disentangle their tech stacks, after a data breach saw over 510,000 users' data exposed.... [...]
