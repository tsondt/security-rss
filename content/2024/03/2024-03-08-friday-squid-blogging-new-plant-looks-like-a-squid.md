Title: Friday Squid Blogging: New Plant Looks Like a Squid
Date: 2024-03-08T22:11:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-03-08-friday-squid-blogging-new-plant-looks-like-a-squid

[Source](https://www.schneier.com/blog/archives/2024/03/friday-squid-blogging-new-plant-looks-like-a-squid.html){:target="_blank" rel="noopener"}

> Newly discovered plant looks like a squid. And it’s super weird: The plant, which grows to 3 centimetres tall and 2 centimetres wide, emerges to the surface for as little as a week each year. It belongs to a group of plants known as fairy lanterns and has been given the scientific name Relictithismia kimotsukiensis. Unlike most other plants, fairy lanterns don’t produce the green pigment chlorophyll, which is necessary for photosynthesis. Instead, they get their energy from fungi. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t [...]
