Title: Backdoor found in widely used Linux utility breaks encrypted SSH connections
Date: 2024-03-29T18:50:34+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;backdoors;Linux;supply chain attack;xz utils
Slug: 2024-03-29-backdoor-found-in-widely-used-linux-utility-breaks-encrypted-ssh-connections

[Source](https://arstechnica.com/?p=2013674){:target="_blank" rel="noopener"}

> Enlarge / Internet Backdoor in a string of binary code in a shape of an eye. (credit: Getty Images) Researchers have found a malicious backdoor in a compression tool that made its way into widely used Linux distributions, including those from Red Hat and Debian. The compression utility, known as xz Utils, introduced the malicious code in versions ​​5.6.0 and 5.6.1, according to Andres Freund, the developer who discovered it. There are no known reports of those versions being incorporated into any production releases for major Linux distributions, but both Red Hat and Debian reported that recently published beta releases [...]
