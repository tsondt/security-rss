Title: Hugging Face, the GitHub of AI, hosted code that backdoored user devices
Date: 2024-03-01T18:02:29+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: AI;Biz & IT;Security;hacking;Hugging Face;machine learning;malware
Slug: 2024-03-01-hugging-face-the-github-of-ai-hosted-code-that-backdoored-user-devices

[Source](https://arstechnica.com/?p=2007291){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Code uploaded to AI developer platform Hugging Face covertly installed backdoors and other types of malware on end-user machines, researchers from security firm JFrog said Thursday in a report that’s a likely harbinger of what’s to come. In all, JFrog researchers said, they found roughly 100 submissions that performed hidden and unwanted actions when they were downloaded and loaded onto an end-user device. Most of the flagged machine learning models—all of which went undetected by Hugging Face—appeared to be benign proofs of concept uploaded by researchers or curious users. JFrog researchers said in an email that [...]
