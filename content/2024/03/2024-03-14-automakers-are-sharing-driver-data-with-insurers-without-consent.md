Title: Automakers Are Sharing Driver Data with Insurers without Consent
Date: 2024-03-14T11:01:43+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;insurance;privacy;surveillance
Slug: 2024-03-14-automakers-are-sharing-driver-data-with-insurers-without-consent

[Source](https://www.schneier.com/blog/archives/2024/03/automakers-are-sharing-driver-data-with-insurers-without-consent.html){:target="_blank" rel="noopener"}

> Kasmir Hill has the story : Modern cars are internet-enabled, allowing access to services like navigation, roadside assistance and car apps that drivers can connect to their vehicles to locate them or unlock them remotely. In recent years, automakers, including G.M., Honda, Kia and Hyundai, have started offering optional features in their connected-car apps that rate people’s driving. Some drivers may not realize that, if they turn on these features, the car companies then give information about how they drive to data brokers like LexisNexis [who then sell it to insurance companies]. Automakers and data brokers that have partnered to [...]
