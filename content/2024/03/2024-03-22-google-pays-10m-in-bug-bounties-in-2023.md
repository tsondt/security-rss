Title: Google Pays $10M in Bug Bounties in 2023
Date: 2024-03-22T11:01:39+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;economics of security;Google;vulnerabilities
Slug: 2024-03-22-google-pays-10m-in-bug-bounties-in-2023

[Source](https://www.schneier.com/blog/archives/2024/03/google-pays-10m-in-bug-bounties-in-2023.html){:target="_blank" rel="noopener"}

> BleepingComputer has the details. It’s $2M less than in 2022, but it’s still a lot. The highest reward for a vulnerability report in 2023 was $113,337, while the total tally since the program’s launch in 2010 has reached $59 million. For Android, the world’s most popular and widely used mobile operating system, the program awarded over $3.4 million. Google also increased the maximum reward amount for critical vulnerabilities concerning Android to $15,000, driving increased community reports. During security conferences like ESCAL8 and hardwea.io, Google awarded $70,000 for 20 critical discoveries in Wear OS and Android Automotive OS and another $116,000 [...]
