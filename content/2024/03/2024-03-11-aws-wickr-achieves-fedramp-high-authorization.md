Title: AWS Wickr achieves FedRAMP High authorization
Date: 2024-03-11T20:35:37+00:00
Author: Anne Grahn
Category: AWS Security
Tags: Announcements;AWS Wickr;Federal;Foundational (100);Government;Public Sector;Security, Identity, & Compliance;AWS Compliance;AWS security;data privacy;Data protection;Encryption;FedRAMP;Security Blog
Slug: 2024-03-11-aws-wickr-achieves-fedramp-high-authorization

[Source](https://aws.amazon.com/blogs/security/aws-wickr-achieves-fedramp-high-authorization/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is excited to announce that AWS Wickr has achieved Federal Risk and Authorization Management Program (FedRAMP) authorization at the High impact level from the FedRAMP Joint Authorization Board (JAB). FedRAMP is a U.S. government–wide program that promotes the adoption of secure cloud services by providing a standardized approach to security and [...]
