Title: China encouraged armed offensive against Myanmar government to protest proliferation of online scams
Date: 2024-03-28T04:28:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-03-28-china-encouraged-armed-offensive-against-myanmar-government-to-protest-proliferation-of-online-scams

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/28/scam_centers_china_myanmar/){:target="_blank" rel="noopener"}

> Report claims Beijing is most displeased by junta's failure to address slave labor scam settlements The military junta controlling Myanmar has struggled to control all of its territory thanks in part to China backing rebel forces as a way of expressing its displeasure about cyberscam centers operating from the country.... [...]
