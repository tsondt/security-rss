Title: US to probe Change Healthcare's data protection standards as lawsuits mount
Date: 2024-03-14T14:03:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-14-us-to-probe-change-healthcares-data-protection-standards-as-lawsuits-mount

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/14/change_healthcare_ransomware_investigation/){:target="_blank" rel="noopener"}

> Services slowly coming back online but providers still struggling Change Healthcare is being investigated over the alleged 6 TB data theft by the ALPHV ransomware group as it continues recovery efforts.... [...]
