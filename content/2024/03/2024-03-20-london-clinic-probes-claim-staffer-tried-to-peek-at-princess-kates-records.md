Title: London Clinic probes claim staffer tried to peek at Princess Kate's records
Date: 2024-03-20T15:30:21+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2024-03-20-london-clinic-probes-claim-staffer-tried-to-peek-at-princess-kates-records

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/20/london_clinic_probes_claims_staffer/){:target="_blank" rel="noopener"}

> First: Not being able buy a meat pie with a credit card. Now this The London Clinic where the Princess of Wales had surgery at the start of this year says it is investigating claims an employee tried to access her medical records.... [...]
