Title: Canada's anti-money laundering agency offline after cyberattack
Date: 2024-03-06T12:30:42-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-06-canadas-anti-money-laundering-agency-offline-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/canadas-anti-money-laundering-agency-offline-after-cyberattack/){:target="_blank" rel="noopener"}

> The Financial Transactions and Reports Analysis Centre of Canada (FINTRAC) has announced that a "cyber incident" forced it to take its corporate systems offline as a precaution. [...]
