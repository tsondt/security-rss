Title: Fake Leather wallet app on Apple App Store is a crypto drainer
Date: 2024-03-11T10:54:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple;CryptoCurrency
Slug: 2024-03-11-fake-leather-wallet-app-on-apple-app-store-is-a-crypto-drainer

[Source](https://www.bleepingcomputer.com/news/security/fake-leather-wallet-app-on-apple-app-store-is-a-crypto-drainer/){:target="_blank" rel="noopener"}

> The developers of the Leather cryptocurrency wallet are warning of a fake app on the Apple App Store, with users reporting it is a wallet drainer that stole their digital assets. [...]
