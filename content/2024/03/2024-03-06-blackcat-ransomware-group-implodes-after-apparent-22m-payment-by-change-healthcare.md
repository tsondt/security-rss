Title: BlackCat Ransomware Group Implodes After Apparent $22M Payment by Change Healthcare
Date: 2024-03-06T00:22:56+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Ransomware;ALPHV ransomware;BlackCat ransomware;Change Healthcare;Dmitry Smilyanets;Emsisoft;Fabian Wosar;fbi;LockBit;NCA;Optum;RAMP;Recorded Future;wired.com
Slug: 2024-03-06-blackcat-ransomware-group-implodes-after-apparent-22m-payment-by-change-healthcare

[Source](https://krebsonsecurity.com/2024/03/blackcat-ransomware-group-implodes-after-apparent-22m-ransom-payment-by-change-healthcare/){:target="_blank" rel="noopener"}

> There are indications that U.S. healthcare giant Change Healthcare has made a $22 million extortion payment to the infamous BlackCat ransomware group (a.k.a. “ ALPHV “) as the company struggles to bring services back online amid a cyberattack that has disrupted prescription drug services nationwide for weeks. However, the cybercriminal who claims to have given BlackCat access to Change’s network says the crime gang cheated them out of their share of the ransom, and that they still have the sensitive data Change reportedly paid the group to destroy. Meanwhile, the affiliate’s disclosure appears to have prompted BlackCat to cease operations [...]
