Title: Cops visit school of 'wrong person's child,' mix up victims and suspects in epic data fail
Date: 2024-03-01T12:40:35+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2024-03-01-cops-visit-school-of-wrong-persons-child-mix-up-victims-and-suspects-in-epic-data-fail

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/01/west_midlands_police_data_protection/){:target="_blank" rel="noopener"}

> Data watchdog reprimands police force for confusing 2 people with same name and birthday to disastrous results The UK's Information Commissioner's Office has put the West Midlands Police (WMP) on the naughty step after the force was found to have repeatedly mixed up two people's personal data for years.... [...]
