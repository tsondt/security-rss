Title: Seoul accuses North Korea of stealing southern chipmakers' designs
Date: 2024-03-04T20:00:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-04-seoul-accuses-north-korea-of-stealing-southern-chipmakers-designs

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/04/north_korea_hacking_chipmakers/){:target="_blank" rel="noopener"}

> Kim Jong Un's all in for home-built silicon says warning North Korean government spies have broken into the servers of at least two chipmakers and stolen product designs as part of attempts to spur Kim Jong Un's plans for a domestic semiconductor industry, according to Seoul's security agency.... [...]
