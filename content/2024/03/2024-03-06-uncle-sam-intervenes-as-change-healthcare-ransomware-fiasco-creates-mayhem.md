Title: Uncle Sam intervenes as Change Healthcare ransomware fiasco creates mayhem
Date: 2024-03-06T00:30:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-06-uncle-sam-intervenes-as-change-healthcare-ransomware-fiasco-creates-mayhem

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/06/us_government_change_ransomware_intervention/){:target="_blank" rel="noopener"}

> As the crooks behind the attack - probably ALPHV/BlackCat - fake their own demise The US government has stepped in to help hospitals and other healthcare providers affected by the Change Healthcare ransomware infection, offering more relaxed Medicare rules and urging advanced funding to providers.... [...]
