Title: Nissan to alert 100,000 Aussies and Kiwis about data loss incident
Date: 2024-03-14T00:32:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-14-nissan-to-alert-100000-aussies-and-kiwis-about-data-loss-incident

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/14/nissan_oceania_to_contact_100k/){:target="_blank" rel="noopener"}

> Akira ransomware crooks brag of stealing thousands of ID documents during break-in Over the next few weeks, Nissan Oceania will make contact with around 100,000 people in Australia and New Zealand whose data was pilfered in a December 2023 attack on its systems – perhaps by the Akira ransomware gang.... [...]
