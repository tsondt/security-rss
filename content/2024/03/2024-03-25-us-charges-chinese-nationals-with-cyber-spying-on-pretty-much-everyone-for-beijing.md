Title: US charges Chinese nationals with cyber-spying on pretty much everyone for Beijing
Date: 2024-03-25T22:15:45+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-25-us-charges-chinese-nationals-with-cyber-spying-on-pretty-much-everyone-for-beijing

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/25/china_apt31_charges/){:target="_blank" rel="noopener"}

> Plus: Alleged front sanctioned, UK blames PRC for Electoral Commission theft, and does America need a Cyber Force? The United States on Monday accused seven Chinese men of breaking into computer networks, email accounts, and cloud storage belonging to numerous critical infrastructure organizations, companies, and individuals, including US businesses, politicians, and their political parties.... [...]
