Title: Hackers steal Windows NTLM authentication hashes in phishing attacks
Date: 2024-03-04T16:15:19-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-04-hackers-steal-windows-ntlm-authentication-hashes-in-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/hackers-steal-windows-ntlm-authentication-hashes-in-phishing-attacks/){:target="_blank" rel="noopener"}

> The hacking group known as TA577 has recently shifted tactics by using phishing emails to steal NT LAN Manager (NTLM) authentication hashes to perform account hijacks. [...]
