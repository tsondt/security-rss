Title: NSA shares zero-trust guidance to limit adversaries on the network
Date: 2024-03-05T18:29:36-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-03-05-nsa-shares-zero-trust-guidance-to-limit-adversaries-on-the-network

[Source](https://www.bleepingcomputer.com/news/security/nsa-shares-zero-trust-guidance-to-limit-adversaries-on-the-network/){:target="_blank" rel="noopener"}

> The National Security Agency is sharing new guidance to help organizations limit an adversary's movement on the internal network by adopting zero-trust framework principles. [...]
