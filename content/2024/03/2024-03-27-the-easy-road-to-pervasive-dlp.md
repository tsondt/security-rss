Title: The easy road to pervasive DLP
Date: 2024-03-27T03:16:14+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2024-03-27-the-easy-road-to-pervasive-dlp

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/27/the_easy_road_to_pervasive/){:target="_blank" rel="noopener"}

> How Forcepoint Data Security Everywhere does what it says on the tin Sponsored Post The coronavirus pandemic appears to have changed the employment landscape forever, with estimates suggesting that up to a quarter of staff still spend some of their working week outside of the office compared to just 6 percent prior to 2020.... [...]
