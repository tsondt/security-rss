Title: Thousands of phones and routers swept into proxy service, unbeknownst to users
Date: 2024-03-26T19:56:09+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;android;anonymous proxy;botnets;Phones;routers
Slug: 2024-03-26-thousands-of-phones-and-routers-swept-into-proxy-service-unbeknownst-to-users

[Source](https://arstechnica.com/?p=2012682){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Crooks are working overtime to anonymize their illicit online activities using thousands of devices of unsuspecting users, as evidenced by two unrelated reports published Tuesday. The first, from security firm Lumen, reports that roughly 40,000 home and office routers have been drafted into a criminal enterprise that anonymizes illicit Internet activities, with another 1,000 new devices being added each day. The malware responsible is a variant of TheMoon, a malicious code family dating back to at least 2014. In its earliest days, TheMoon almost exclusively infected Linksys E1000 series routers. Over the years it branched out [...]
