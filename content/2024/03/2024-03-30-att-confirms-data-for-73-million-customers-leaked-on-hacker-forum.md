Title: AT&T confirms data for 73 million customers leaked on hacker forum
Date: 2024-03-30T12:52:47-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-03-30-att-confirms-data-for-73-million-customers-leaked-on-hacker-forum

[Source](https://www.bleepingcomputer.com/news/security/atandt-confirms-data-for-73-million-customers-leaked-on-hacker-forum/){:target="_blank" rel="noopener"}

> AT&T has finally confirmed it is impacted by a data breach affecting 73 million current and former customers after initially denying the leaked data originated from them. [...]
