Title: Record breach of French government exposes up to 43 million people's data
Date: 2024-03-14T16:06:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-14-record-breach-of-french-government-exposes-up-to-43-million-peoples-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/14/mega_data_breach_at_french/){:target="_blank" rel="noopener"}

> Zut alors! Department for registering and helping unemployed people broken into A French government department - responsible for registering and assisting unemployed people - is the latest victim of a mega data breach that compromised the information of up to 43 million citizens.... [...]
