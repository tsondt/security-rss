Title: Surveillance through Push Notifications
Date: 2024-03-06T12:06:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;FBI;identification;police;privacy;surveillance
Slug: 2024-03-06-surveillance-through-push-notifications

[Source](https://www.schneier.com/blog/archives/2024/03/surveillance-through-push-notifications.html){:target="_blank" rel="noopener"}

> The Washington Post is reporting on the FBI’s increasing use of push notification data—”push tokens”—to identify people. The police can request this data from companies like Apple and Google without a warrant. The investigative technique goes back years. Court orders that were issued in 2019 to Apple and Google demanded that the companies hand over information on accounts identified by push tokens linked to alleged supporters of the Islamic State terrorist group. But the practice was not widely understood until December, when Sen. Ron Wyden (D-Ore.), in a letter to Attorney General Merrick Garland, said an investigation had revealed that [...]
