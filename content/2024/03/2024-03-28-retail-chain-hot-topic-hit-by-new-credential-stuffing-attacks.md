Title: Retail chain Hot Topic hit by new credential stuffing attacks
Date: 2024-03-28T15:04:04-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-03-28-retail-chain-hot-topic-hit-by-new-credential-stuffing-attacks

[Source](https://www.bleepingcomputer.com/news/security/retail-chain-hot-topic-hit-by-new-credential-stuffing-attacks/){:target="_blank" rel="noopener"}

> American retailer Hot Topic disclosed that two waves of credential stuffing attacks in November exposed affected customers' personal information and partial payment data. [...]
