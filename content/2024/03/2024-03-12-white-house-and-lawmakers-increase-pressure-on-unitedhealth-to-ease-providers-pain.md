Title: White House and lawmakers increase pressure on UnitedHealth to ease providers' pain
Date: 2024-03-12T00:02:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-12-white-house-and-lawmakers-increase-pressure-on-unitedhealth-to-ease-providers-pain

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/12/white_house_pressures_unitedhealth/){:target="_blank" rel="noopener"}

> US senator calls cyber attack 'inexcusable,' calls for mandatory security rules The Biden administration and US lawmakers are turning up the pressure on UnitedHealth group to ease medical providers' pain after the ransomware attack on Change Healthcare, by expediting payments to hospitals, physicians and pharmacists – among other tactics.... [...]
