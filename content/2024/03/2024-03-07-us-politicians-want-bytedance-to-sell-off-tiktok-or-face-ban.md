Title: US politicians want ByteDance to sell off TikTok or face ban
Date: 2024-03-07T06:05:43+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-03-07-us-politicians-want-bytedance-to-sell-off-tiktok-or-face-ban

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/07/tiktok_divest_ban/){:target="_blank" rel="noopener"}

> The American mind must not be at the mercy of Chinese algorithms A group of US lawmakers introduced legislation on Tuesday that, if passed, would force Chinese internet concern ByteDance to divest TikTok – its most valuable property – or see it banned in the US.... [...]
