Title: Friday Squid Blogging: The Geopolitics of Eating Squid
Date: 2024-03-29T21:02:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-03-29-friday-squid-blogging-the-geopolitics-of-eating-squid

[Source](https://www.schneier.com/blog/archives/2024/03/68676.html){:target="_blank" rel="noopener"}

> New York Times op-ed on the Chinese dominance of the squid industry: China’s domination in seafood has raised deep concerns among American fishermen, policymakers and human rights activists. They warn that China is expanding its maritime reach in ways that are putting domestic fishermen around the world at a competitive disadvantage, eroding international law governing sea borders and undermining food security, especially in poorer countries that rely heavily on fish for protein. In some parts of the world, frequent illegal incursions by Chinese ships into other nations’ waters are heightening military tensions. American lawmakers are concerned because the United States, [...]
