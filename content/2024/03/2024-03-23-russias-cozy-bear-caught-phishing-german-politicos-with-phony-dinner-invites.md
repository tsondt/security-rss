Title: Russia's Cozy Bear caught phishing German politicos with phony dinner invites
Date: 2024-03-23T07:51:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-23-russias-cozy-bear-caught-phishing-german-politicos-with-phony-dinner-invites

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/23/russia_cozy_bear_german_politicians_phishing/){:target="_blank" rel="noopener"}

> Forget the Riesling, bring on the WINELOADER The Kremlin's cyberspies targeted German political parties in a phishing campaign that used emails disguised as dinner party invitations, according to Mandiant.... [...]
