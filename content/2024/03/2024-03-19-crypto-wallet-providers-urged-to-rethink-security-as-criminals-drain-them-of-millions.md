Title: Crypto wallet providers urged to rethink security as criminals drain them of millions
Date: 2024-03-19T14:30:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-19-crypto-wallet-providers-urged-to-rethink-security-as-criminals-drain-them-of-millions

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/19/crypto_wallet_providers_urged_to/){:target="_blank" rel="noopener"}

> Innovative Ethereum feature exploited as victims say goodbye to assets Infosec researchers are noting rising cryptocurrency attacks and have encouraged wallet security providers to up their collective game.... [...]
