Title: Passwords are Costing Your Organization Money - How to Minimize Those Costs
Date: 2024-03-05T10:02:04-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-03-05-passwords-are-costing-your-organization-money-how-to-minimize-those-costs

[Source](https://www.bleepingcomputer.com/news/security/passwords-are-costing-your-organization-money-how-to-minimize-those-costs/){:target="_blank" rel="noopener"}

> Getting rid of passwords completely isn't a realistic option for most orgs, but there are things you can do to make them more secure. Learn more from Specops Software on maximizing security while mitigating costs. [...]
