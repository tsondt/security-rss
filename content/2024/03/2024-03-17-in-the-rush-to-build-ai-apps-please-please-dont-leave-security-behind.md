Title: In the rush to build AI apps, please, please don't leave security behind
Date: 2024-03-17T11:04:08+00:00
Author: Katyanna Quach
Category: The Register
Tags: 
Slug: 2024-03-17-in-the-rush-to-build-ai-apps-please-please-dont-leave-security-behind

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/17/ai_supply_chain/){:target="_blank" rel="noopener"}

> Supply-chain attacks are definitely possible and could lead to data theft, system hijacking, and more Feature While in a rush to understand, build, and ship AI products, developers and data scientists are being urged to be mindful of security and not fall prey to supply-chain attacks.... [...]
