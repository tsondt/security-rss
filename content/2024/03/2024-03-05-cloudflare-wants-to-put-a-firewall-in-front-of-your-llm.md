Title: Cloudflare wants to put a firewall in front of your LLM
Date: 2024-03-05T01:32:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-05-cloudflare-wants-to-put-a-firewall-in-front-of-your-llm

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/05/cloudflare_firewall_ai/){:target="_blank" rel="noopener"}

> Claims to protect against DDoS, sensitive data leakage Cloudflare has tweaked its web application firewall (WAF) to add protections for applications using large language models.... [...]
