Title: Over 12 million auth secrets and keys leaked on GitHub in 2023
Date: 2024-03-12T11:23:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-12-over-12-million-auth-secrets-and-keys-leaked-on-github-in-2023

[Source](https://www.bleepingcomputer.com/news/security/over-12-million-auth-secrets-and-keys-leaked-on-github-in-2023/){:target="_blank" rel="noopener"}

> GitHub users accidentally exposed 12.8 million authentication and sensitive secrets in over 3 million public repositories during 2023, with the vast majority remaining valid after five days. [...]
