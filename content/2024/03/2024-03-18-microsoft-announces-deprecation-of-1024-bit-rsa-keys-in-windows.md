Title: Microsoft announces deprecation of 1024-bit RSA keys in Windows
Date: 2024-03-18T15:51:30-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-03-18-microsoft-announces-deprecation-of-1024-bit-rsa-keys-in-windows

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-announces-deprecation-of-1024-bit-rsa-keys-in-windows/){:target="_blank" rel="noopener"}

> Microsoft has announced that RSA keys shorter than 2048 bits will soon be deprecated in Windows Transport Layer Security (TLS) to provide increased security. [...]
