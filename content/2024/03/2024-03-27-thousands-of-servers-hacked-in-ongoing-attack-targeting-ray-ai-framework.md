Title: Thousands of servers hacked in ongoing attack targeting Ray AI framework
Date: 2024-03-27T22:40:53+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: AI;Biz & IT;Security
Slug: 2024-03-27-thousands-of-servers-hacked-in-ongoing-attack-targeting-ray-ai-framework

[Source](https://arstechnica.com/?p=2013046){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Thousands of servers storing AI workloads and network credentials have been hacked in an ongoing attack campaign targeting a reported vulnerability in Ray, a computing framework used by OpenAI, Uber, and Amazon. The attacks, which have been active for at least seven months, have led to the tampering of AI models. They have also resulted in the compromise of network credentials, allowing access to internal networks and databases and tokens for accessing accounts on platforms including OpenAI, Hugging Face, Stripe, and Azure. Besides corrupting models and stealing credentials, attackers behind the campaign have installed cryptocurrency miners [...]
