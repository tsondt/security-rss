Title: Member of LockBit ransomware group sentenced to 4 years in prison
Date: 2024-03-14T23:15:46+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;lockbit;ransomware
Slug: 2024-03-14-member-of-lockbit-ransomware-group-sentenced-to-4-years-in-prison

[Source](https://arstechnica.com/?p=2010404){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images | Charles O'Rear) A dual Canadian-Russian national has been sentenced to four years in prison for his role in infecting more than 1,000 victims with the LockBit ransomware and then extorting them for tens of millions of dollars. Mikhail Vasiliev, a 33-year-old who most recently lived in Ontario, Canada, was arrested in November 2022 and charged with conspiring to infect protected computers with ransomware and sending ransom demands to victims. Last month, he pleaded guilty to eight counts of cyber extortion, mischief, and weapons charges. During an October 2022 raid on Vasiliev’s Bradford, Ontario home, Canadian [...]
