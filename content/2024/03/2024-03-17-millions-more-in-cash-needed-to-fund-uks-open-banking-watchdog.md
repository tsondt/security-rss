Title: Millions more in cash needed to fund UK’s open-banking watchdog
Date: 2024-03-17T12:06:03+00:00
Author: Kalyeena Makortoff Banking correspondent
Category: The Guardian
Tags: Banking;Financial sector;Regulators;Financial Conduct Authority;Business;UK news;Fintech;Scams;Privacy;Data and computer security;Consumer affairs;Money;Consumer rights
Slug: 2024-03-17-millions-more-in-cash-needed-to-fund-uks-open-banking-watchdog

[Source](https://www.theguardian.com/business/2024/mar/17/millions-more-needed-fund-uk-open-banking-watchdog){:target="_blank" rel="noopener"}

> Exclusive: £10m needed for regulator charged with developing tools to thwart financial crime and protect consumers Banks are under pressure to stump up millions of pounds in interim funding for the organisation that polices open banking, with regulators saying the new money is needed to prevent financial crime and protect consumers if things “go wrong”. Large banks including NatWest, HSBC, Lloyds and Santander UK were among more than 40 City firms summoned by the Financial Conduct Authority (FCA) last week to discuss a cash injection into Open Banking Limited (OPL), the body that oversees innovation in this area. Continue reading... [...]
