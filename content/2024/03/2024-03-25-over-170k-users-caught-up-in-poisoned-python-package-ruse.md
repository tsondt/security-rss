Title: Over 170K users caught up in poisoned Python package ruse
Date: 2024-03-25T18:00:09+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-03-25-over-170k-users-caught-up-in-poisoned-python-package-ruse

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/25/python_package_malware/){:target="_blank" rel="noopener"}

> Supply chain attack targeted GitHub community of Top.gg Discord server More than 170,000 users are said to have been affected by an attack using fake Python infrastructure with "successful exploitation of multiple victims."... [...]
