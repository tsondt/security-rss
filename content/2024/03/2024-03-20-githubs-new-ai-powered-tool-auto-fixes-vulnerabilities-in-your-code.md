Title: GitHub’s new AI-powered tool auto-fixes vulnerabilities in your code
Date: 2024-03-20T14:52:43-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Software
Slug: 2024-03-20-githubs-new-ai-powered-tool-auto-fixes-vulnerabilities-in-your-code

[Source](https://www.bleepingcomputer.com/news/security/githubs-new-ai-powered-tool-auto-fixes-vulnerabilities-in-your-code/){:target="_blank" rel="noopener"}

> GitHub introduced a new AI-powered feature capable of speeding up vulnerability fixes while coding. This feature is in public beta and automatically enabled on all private repositories for GitHub Advanced Security (GHAS) customers [...]
