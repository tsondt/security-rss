Title: March Patch Tuesday sees Hyper-V join the guest-host escape club
Date: 2024-03-13T00:16:15+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-13-march-patch-tuesday-sees-hyper-v-join-the-guest-host-escape-club

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/13/patch_tuesday_march_2024/){:target="_blank" rel="noopener"}

> Critical bugs galore among 61 Microsoft fixes, 56 from Adobe, a dozen from SAP, and a fistful from Fortinet Patch Tuesday Microsoft's monthly patch drop has arrived, delivering a mere 61 CVE-tagged vulnerabilities – none listed as under active attack or already known to the public.... [...]
