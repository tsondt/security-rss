Title: Hackers target FCC, crypto firms in advanced Okta phishing attacks
Date: 2024-03-02T11:18:09-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-02-hackers-target-fcc-crypto-firms-in-advanced-okta-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/hackers-target-fcc-crypto-firms-in-advanced-okta-phishing-attacks/){:target="_blank" rel="noopener"}

> A new phishing kit named CryptoChameleon is being used to target Federal Communications Commission (FCC) employees, using specially crafted single sign-on (SSO) pages for Okta that appear remarkably similar to the originals. [...]
