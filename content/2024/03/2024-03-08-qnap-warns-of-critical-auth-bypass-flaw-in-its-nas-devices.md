Title: QNAP warns of critical auth bypass flaw in its NAS devices
Date: 2024-03-08T15:03:49-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-08-qnap-warns-of-critical-auth-bypass-flaw-in-its-nas-devices

[Source](https://www.bleepingcomputer.com/news/security/qnap-warns-of-critical-auth-bypass-flaw-in-its-nas-devices/){:target="_blank" rel="noopener"}

> QNAP warns of vulnerabilities in its NAS software products, including QTS, QuTS hero, QuTScloud, and myQNAPcloud, that could allow attackers to access devices. [...]
