Title: Here's why Twitter sends you to a different site than what you clicked
Date: 2024-03-20T04:47:04-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2024-03-20-heres-why-twitter-sends-you-to-a-different-site-than-what-you-clicked

[Source](https://www.bleepingcomputer.com/news/security/heres-why-twitter-sends-you-to-a-different-site-than-what-you-clicked/){:target="_blank" rel="noopener"}

> Users of the social media platform X (Twitter) have often been left puzzled when they click on a post with an external link but arrive at an entirely unexpected website from the one displayed. A Twitter ad spotted below by a security researcher shows forbes.com as its destination but instead takes you to a Telegram account. [...]
