Title: Cloud CISO Perspectives: Get ready for Next ‘24: What you need to know
Date: 2024-03-28T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2024-03-28-cloud-ciso-perspectives-get-ready-for-next-24-what-you-need-to-know

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-get-ready-for-next-24-what-you-need-to-know/){:target="_blank" rel="noopener"}

> Welcome to the second Cloud CISO Perspectives for March 2024. Today, Brian Roddy, vice president of security product management, Google Cloud, talks about some of the important cybersecurity hot topics that will be driving our announcements at Google Cloud Next in April. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI Security & CISO, Google Cloud aside_block <ListValue: [StructValue([('title', 'Get vital CISO Insights with Google Cloud'), ('body', <wagtail.rich_text.RichText [...]
