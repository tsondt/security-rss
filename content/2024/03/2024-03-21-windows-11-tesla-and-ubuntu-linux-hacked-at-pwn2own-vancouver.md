Title: Windows 11, Tesla, and Ubuntu Linux hacked at Pwn2Own Vancouver
Date: 2024-03-21T03:07:46-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Linux;Microsoft;Software
Slug: 2024-03-21-windows-11-tesla-and-ubuntu-linux-hacked-at-pwn2own-vancouver

[Source](https://www.bleepingcomputer.com/news/security/windows-11-tesla-and-ubuntu-linux-hacked-at-pwn2own-vancouver/){:target="_blank" rel="noopener"}

> On the first day of Pwn2Own Vancouver 2024, contestants demoed 19 zero-day vulnerabilities in Windows 11, Tesla, Ubuntu Linux and other devices and software to win $732,500 and a Tesla Model 3 car. [...]
