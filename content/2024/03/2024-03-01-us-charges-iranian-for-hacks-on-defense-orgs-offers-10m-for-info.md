Title: U.S. charges Iranian for hacks on defense orgs, offers $10M for info
Date: 2024-03-01T09:47:51-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government;Legal
Slug: 2024-03-01-us-charges-iranian-for-hacks-on-defense-orgs-offers-10m-for-info

[Source](https://www.bleepingcomputer.com/news/security/us-charges-iranian-for-hacks-on-defense-orgs-offers-10m-for-info/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice (DoJ) has unveiled an indictment against Alireza Shafie Nasab, a 39-year-old Iranian national, for his role in a cyber-espionage campaign targeting U.S. government and defense entities. [...]
