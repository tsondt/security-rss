Title: Five Eyes tell critical infra orgs: Take these actions now to protect against China's Volt Typhoon
Date: 2024-03-20T10:15:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-20-five-eyes-tell-critical-infra-orgs-take-these-actions-now-to-protect-against-chinas-volt-typhoon

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/20/five_eyes_volt_typhoon/){:target="_blank" rel="noopener"}

> Unless you want to be the next Change Healthcare, that is The Feds and friends yesterday issued yet another warning about China's Volt Typhoon gang, this time urging critical infrastructure owners and operators to protect their facilities against destructive cyber attacks that may be brewing.... [...]
