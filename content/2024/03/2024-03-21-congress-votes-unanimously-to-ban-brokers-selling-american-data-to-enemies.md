Title: Congress votes unanimously to ban brokers selling American data to enemies
Date: 2024-03-21T20:30:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-03-21-congress-votes-unanimously-to-ban-brokers-selling-american-data-to-enemies

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/21/congress_votes_unanimously_to_ban/){:target="_blank" rel="noopener"}

> At least we can all agree on something The US House of Representatives has passed a bill that would prohibit data brokers from selling Americans' data to foreign adversaries with an unusual degree of bipartisan support: It passed without a single opposing vote.... [...]
