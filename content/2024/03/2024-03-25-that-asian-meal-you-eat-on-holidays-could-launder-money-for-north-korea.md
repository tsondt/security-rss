Title: That Asian meal you eat on holidays could launder money for North Korea
Date: 2024-03-25T06:32:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-03-25-that-asian-meal-you-eat-on-holidays-could-launder-money-for-north-korea

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/25/un_north_korea_report/){:target="_blank" rel="noopener"}

> United Nations finds IT contract and crypto scams are just two of DPRK's illicit menu items If you dine out at an Asian restaurant on your next holiday, the United Nations thinks your meal could help North Korea to launder money.... [...]
