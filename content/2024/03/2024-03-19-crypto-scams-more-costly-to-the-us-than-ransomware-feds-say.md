Title: Crypto scams more costly to the US than ransomware, Feds say
Date: 2024-03-19T20:00:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-19-crypto-scams-more-costly-to-the-us-than-ransomware-feds-say

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/19/crypto_scams_cost/){:target="_blank" rel="noopener"}

> Latest figures paint grim picture of how viciously the elderly are targeted The FBI says investment fraud was the form of cybercrime that incurred the greatest financial loss for Americans last year.... [...]
