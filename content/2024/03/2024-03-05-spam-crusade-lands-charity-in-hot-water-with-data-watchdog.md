Title: Spam crusade lands charity in hot water with data watchdog
Date: 2024-03-05T09:30:09+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2024-03-05-spam-crusade-lands-charity-in-hot-water-with-data-watchdog

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/05/penny_appeal_ico_investigation/){:target="_blank" rel="noopener"}

> Penny Appeal sent more than 460,000 texts asking for money to help war-torn countries, no opt out Typically it is energy improvement peddlers or debt help specialists that are disgraced by Britain's data watchdog for spamming unsuspecting households, but the latest entrant in the hall of shame is a charity.... [...]
