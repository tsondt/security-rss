Title: Truck-to-truck worm could infect – and disrupt – entire US commercial fleet
Date: 2024-03-22T00:03:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-22-truck-to-truck-worm-could-infect-and-disrupt-entire-us-commercial-fleet

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/22/boffins_tucktotruck_worm/){:target="_blank" rel="noopener"}

> The device that makes it possible is required in all American big rigs, and has poor security Vulnerabilities in common Electronic Logging Devices (ELDs) required in US commercial trucks could be present in over 14 million medium- and heavy-duty rigs, according to boffins at Colorado State University.... [...]
