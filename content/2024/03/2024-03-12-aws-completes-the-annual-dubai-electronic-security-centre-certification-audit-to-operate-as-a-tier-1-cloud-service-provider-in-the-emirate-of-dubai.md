Title: AWS completes the annual Dubai Electronic Security Centre certification audit to operate as a Tier 1 cloud service provider in the Emirate of Dubai
Date: 2024-03-12T19:55:12+00:00
Author: Vishal Pabari
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Abu Dhabi;Auditing;AWS security;Cloud Service Provider;Compliance;CSA STAR;CSP;DESC;Dubai Electronic Security Centre;Emirate of Dubai;Government;IAR;Information Security Regulation;ISO;Licence;Middle East;Public Sector;Security;Security Blog;UAE
Slug: 2024-03-12-aws-completes-the-annual-dubai-electronic-security-centre-certification-audit-to-operate-as-a-tier-1-cloud-service-provider-in-the-emirate-of-dubai

[Source](https://aws.amazon.com/blogs/security/aws-completes-the-annual-dubai-electronic-security-centre-certification-audit-to-operate-as-a-tier-1-cloud-service-provider-in-the-emirate-of-dubai/){:target="_blank" rel="noopener"}

> We’re excited to announce that Amazon Web Services (AWS) has completed the annual Dubai Electronic Security Centre (DESC) certification audit to operate as a Tier 1 cloud service provider (CSP) for the AWS Middle East (UAE) Region. This alignment with DESC requirements demonstrates our continuous commitment to adhere to the heightened expectations for CSPs. Government [...]
