Title: FTX crypto-crook Sam Bankman-Fried gets 25 years in prison
Date: 2024-03-28T16:19:22+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-03-28-ftx-crypto-crook-sam-bankman-fried-gets-25-years-in-prison

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/28/ftx_sbf_sentenced_25_years/){:target="_blank" rel="noopener"}

> Could have been worse: Prosecutors wanted decades more Fallen crypto-king Sam Bankman-Fried has been jailed for 25 years after New York federal judge Lewis Kaplan expressed disbelief at almost every argument from his legal team.... [...]
