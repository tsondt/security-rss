Title: 'Thousands' of businesses at mercy of miscreants thanks to unpatched Ray AI flaw
Date: 2024-03-27T20:40:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-27-thousands-of-businesses-at-mercy-of-miscreants-thanks-to-unpatched-ray-ai-flaw

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/27/ray_ai_framework_bug/){:target="_blank" rel="noopener"}

> Anyscale claims issue is 'long-standing design decision' – as users are raided by intruders Thousands of companies remain vulnerable to a remote-code-execution bug in Ray, an open-source AI framework used by Amazon, OpenAI, and others, that is being abused by miscreants in the wild to steal sensitive data and illicitly mine for cryptocurrency.... [...]
