Title: How AWS is helping customers achieve their digital sovereignty and resilience goals
Date: 2024-03-26T19:57:27+00:00
Author: Max Peterson
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS Digital Sovereignty Pledge;Digital Sovereignty;Resilience;Resiliency;Security Blog
Slug: 2024-03-26-how-aws-is-helping-customers-achieve-their-digital-sovereignty-and-resilience-goals

[Source](https://aws.amazon.com/blogs/security/how-aws-is-helping-customers-achieve-their-digital-sovereignty-and-resilience-goals/){:target="_blank" rel="noopener"}

> As we’ve innovated and expanded the Amazon Web Services (AWS) Cloud, we continue to prioritize making sure customers are in control and able to meet regulatory requirements anywhere they operate. With the AWS Digital Sovereignty Pledge, which is our commitment to offering all AWS customers the most advanced set of sovereignty controls and features available [...]
