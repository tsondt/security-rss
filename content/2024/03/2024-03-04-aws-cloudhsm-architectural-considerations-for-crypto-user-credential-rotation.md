Title: AWS CloudHSM architectural considerations for crypto user credential rotation
Date: 2024-03-04T21:23:57+00:00
Author: Shankar Rajagopalan
Category: AWS Security
Tags: AWS CloudHSM;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2024-03-04-aws-cloudhsm-architectural-considerations-for-crypto-user-credential-rotation

[Source](https://aws.amazon.com/blogs/security/aws-cloudhsm-architectural-considerations-for-crypto-user-credential-rotation/){:target="_blank" rel="noopener"}

> This blog post provides architectural guidance on AWS CloudHSM crypto user credential rotation and is intended for those using or considering using CloudHSM. CloudHSM is a popular solution for secure cryptographic material management. By using this service, organizations can benefit from a robust mechanism to manage their own dedicated FIPS 140-2 level 3 hardware security [...]
