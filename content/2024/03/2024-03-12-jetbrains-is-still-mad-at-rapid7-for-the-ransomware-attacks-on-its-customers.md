Title: JetBrains is still mad at Rapid7 for the ransomware attacks on its customers
Date: 2024-03-12T16:30:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-12-jetbrains-is-still-mad-at-rapid7-for-the-ransomware-attacks-on-its-customers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/12/jetbrains_is_still_mad_at/){:target="_blank" rel="noopener"}

> War of words wages on between vendors divided Last week, we wrote about how security outfit Rapid7 threw JetBrains, the company behind the popular CI/CD platform TeamCity, under the bus over allegations of silent patching. Now, JetBrains has gone on the offensive.... [...]
