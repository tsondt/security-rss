Title: Mozilla Drops Onerep After CEO Admits to Running People-Search Networks
Date: 2024-03-22T19:02:41+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Web Fraud 2.0;Dimitiri Shelest;firefox;Firefox Monitor;HaveIBeenPwned;HaveIBeenPwned.com;Mozilla Foundation;Mozilla Monitor;Nuwber;OneRep;Spamit;Troy Hunt
Slug: 2024-03-22-mozilla-drops-onerep-after-ceo-admits-to-running-people-search-networks

[Source](https://krebsonsecurity.com/2024/03/mozilla-drops-onerep-after-ceo-admits-to-running-people-search-networks/){:target="_blank" rel="noopener"}

> The nonprofit organization that supports the Firefox web browser said today it is winding down its new partnership with Onerep, an identity protection service recently bundled with Firefox that offers to remove users from hundreds of people-search sites. The move comes just days after a report by KrebsOnSecurity forced Onerep’s CEO to admit that he has founded dozens of people-search networks over the years. Mozilla Monitor. Image Mozilla Monitor Plus video on Youtube. Mozilla only began bundling Onerep in Firefox last month, when it announced the reputation service would be offered on a subscription basis as part of Mozilla Monitor [...]
