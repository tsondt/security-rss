Title: Migrate your Windows PKI from Microsoft Active Directory Certificate Services to AWS Private CA Connector for Active Directory
Date: 2024-03-22T15:43:22+00:00
Author: Axel Larsson
Category: AWS Security
Tags: Advanced (300);AWS Private Certificate Authority;Security, Identity, & Compliance;Technical How-to;AWS Private CA;Security Blog
Slug: 2024-03-22-migrate-your-windows-pki-from-microsoft-active-directory-certificate-services-to-aws-private-ca-connector-for-active-directory

[Source](https://aws.amazon.com/blogs/security/migrate-your-windows-pki-from-microsoft-active-directory-certificate-services-to-aws-private-ca-connector-for-active-directory/){:target="_blank" rel="noopener"}

> When you migrate your Windows environment to Amazon Web Services (AWS), you might need to address certificate management for computers and users in your Active Directory domain. Today, Windows administrators commonly use Active Directory Certificate Services (AD CS) to support this task. In this post, we will show you how to migrate AD CS to [...]
