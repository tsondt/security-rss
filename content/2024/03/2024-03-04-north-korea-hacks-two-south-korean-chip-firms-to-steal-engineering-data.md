Title: North Korea hacks two South Korean chip firms to steal engineering data
Date: 2024-03-04T09:46:59-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-04-north-korea-hacks-two-south-korean-chip-firms-to-steal-engineering-data

[Source](https://www.bleepingcomputer.com/news/security/north-korea-hacks-two-south-korean-chip-firms-to-steal-engineering-data/){:target="_blank" rel="noopener"}

> The National Intelligence Service (NIS) in South Korea warns that North Korean hackers target domestic semiconductor manufacturers in cyber espionage attacks. [...]
