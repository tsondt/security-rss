Title: Hacked WordPress sites use visitors' browsers to hack other sites
Date: 2024-03-06T17:35:05-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-03-06-hacked-wordpress-sites-use-visitors-browsers-to-hack-other-sites

[Source](https://www.bleepingcomputer.com/news/security/hacked-wordpress-sites-use-visitors-browsers-to-hack-other-sites/){:target="_blank" rel="noopener"}

> Hackers are conducting widescale attacks on WordPress sites to inject scripts that force visitors' browsers to bruteforce passwords for other sites. [...]
