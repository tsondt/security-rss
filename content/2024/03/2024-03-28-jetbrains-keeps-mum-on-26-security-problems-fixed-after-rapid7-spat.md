Title: JetBrains keeps mum on 26 'security problems' fixed after Rapid7 spat
Date: 2024-03-28T17:26:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-28-jetbrains-keeps-mum-on-26-security-problems-fixed-after-rapid7-spat

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/28/jetbrains_fixes_26_security_problems/){:target="_blank" rel="noopener"}

> Vendor takes hardline approach to patch disclosure to new levels JetBrains TeamCity users are urged to apply the latest version upgrade this week after the vendor disclosed 26 new security issues in the CI/CD web application.... [...]
