Title: Thread Hijacking: Phishes That Prey on Your Curiosity
Date: 2024-03-28T23:56:13+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Ne'er-Do-Well News;Web Fraud 2.0;Adam Kidan;Brett Sholtis;Empire Workforce Solutions;LancasterOnline.com;multi-persona phishing;phishing;proofpoint;Ryan Kalember;thread hijacking;Tom Murse
Slug: 2024-03-28-thread-hijacking-phishes-that-prey-on-your-curiosity

[Source](https://krebsonsecurity.com/2024/03/thread-hijacking-phishes-that-prey-on-your-curiosity/){:target="_blank" rel="noopener"}

> Thread hijacking attacks. They happen when someone you know has their email account compromised, and you are suddenly dropped into an existing conversation between the sender and someone else. These missives draw on the recipient’s natural curiosity about being copied on a private discussion, which is modified to include a malicious link or attachment. Here’s the story of a thread hijacking attack in which a journalist was copied on a phishing email from the unwilling subject of a recent scoop. In Sept. 2023, the Pennsylvania news outlet LancasterOnline.com published a story about Adam Kidan, a wealthy businessman with a criminal [...]
