Title: Microsoft confirms memory leak in March Windows Server security update
Date: 2024-03-25T01:15:21+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-03-25-microsoft-confirms-memory-leak-in-march-windows-server-security-update

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/25/microsoft_confirms_memory_leak_in/){:target="_blank" rel="noopener"}

> ALSO: Viasat hack wiper malware is back, users are the number one cause of data loss, and critical vulns Infosec in brief If your Windows domain controllers have been crashing since a security update was installed earlier this month, there's no longer any need to speculate why: Microsoft has admitted it introduced a memory leak in its March patches and fixed the issue.... [...]
