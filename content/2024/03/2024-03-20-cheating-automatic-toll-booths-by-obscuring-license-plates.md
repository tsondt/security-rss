Title: Cheating Automatic Toll Booths by Obscuring License Plates
Date: 2024-03-20T11:08:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;cheating
Slug: 2024-03-20-cheating-automatic-toll-booths-by-obscuring-license-plates

[Source](https://www.schneier.com/blog/archives/2024/03/cheating-automatic-toll-booths-by-obscuring-license-plates.html){:target="_blank" rel="noopener"}

> The Wall Street Journal is reporting on a variety of techniques drivers are using to obscure their license plates so that automatic readers can’t identify them and charge tolls properly. Some drivers have power-washed paint off their plates or covered them with a range of household items such as leaf-shaped magnets, Bramwell-Stewart said. The Port Authority says officers in 2023 roughly doubled the number of summonses issued for obstructed, missing or fictitious license plates compared with the prior year. Bramwell-Stewart said one driver from New Jersey repeatedly used what’s known in the streets as a flipper, which lets you remotely [...]
