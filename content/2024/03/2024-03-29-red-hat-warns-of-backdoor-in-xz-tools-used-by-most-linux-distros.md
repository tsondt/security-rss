Title: Red Hat warns of backdoor in XZ tools used by most Linux distros
Date: 2024-03-29T13:50:43-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-03-29-red-hat-warns-of-backdoor-in-xz-tools-used-by-most-linux-distros

[Source](https://www.bleepingcomputer.com/news/security/red-hat-warns-of-backdoor-in-xz-tools-used-by-most-linux-distros/){:target="_blank" rel="noopener"}

> Today, Red Hat warned users to immediately stop using systems running Fedora development and experimental versions because of a backdoor found in the latest XZ Utils data compression tools and libraries. [...]
