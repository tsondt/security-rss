Title: Licensing AI Engineers
Date: 2024-03-25T11:04:34+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence;ethics;trust
Slug: 2024-03-25-licensing-ai-engineers

[Source](https://www.schneier.com/blog/archives/2024/03/licensing-ai-engineers.html){:target="_blank" rel="noopener"}

> The debate over professionalizing software engineers is decades old. (The basic idea is that, like lawyers and architects, there should be some professional licensing requirement for software engineers.) Here’s a law journal article recommending the same idea for AI engineers. This Article proposes another way: professionalizing AI engineering. Require AI engineers to obtain licenses to build commercial AI products, push them to collaborate on scientifically-supported, domain-specific technical standards, and charge them with policing themselves. This Article’s proposal addresses AI harms at their inception, influencing the very engineering decisions that give rise to them in the first place. By wresting control [...]
