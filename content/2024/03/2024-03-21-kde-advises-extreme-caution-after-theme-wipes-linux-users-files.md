Title: KDE advises extreme caution after theme wipes Linux user's files
Date: 2024-03-21T15:05:18-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Linux;Security
Slug: 2024-03-21-kde-advises-extreme-caution-after-theme-wipes-linux-users-files

[Source](https://www.bleepingcomputer.com/news/linux/kde-advises-extreme-caution-after-theme-wipes-linux-users-files/){:target="_blank" rel="noopener"}

> On Wednesday, the KDE team warned Linux users to exercise "extreme caution" when installing global themes, even from the official KDE Store, because these themes run arbitrary code on devices to customize the desktop's appearance. [...]
