Title: How to secure Ray on Google Kubernetes Engine
Date: 2024-03-21T21:00:00+00:00
Author: Cynthia Thomas
Category: GCP Security
Tags: Security & Identity;Containers & Kubernetes
Slug: 2024-03-21-how-to-secure-ray-on-google-kubernetes-engine

[Source](https://cloud.google.com/blog/products/containers-kubernetes/securing-ray-to-run-on-google-kubernetes-engine/){:target="_blank" rel="noopener"}

> When developers are innovating quickly, security can be an afterthought. That’s even true for AI/ML workloads, where the stakes are high for organizations trying to protect valuable models and data. When you deploy an AI workload on Google Kubernetes Engine (GKE), you can benefit from the many security tools available in Google Cloud infrastructure. In this blog, we share security insights and hardening techniques for training AI/ML workloads on one framework in particular — Ray. Ray needs security hardening As a distributed compute framework for AI applications, Ray has grown in popularity in recent years, and deploying it on GKE [...]
