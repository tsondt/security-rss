Title: AWS completes the annual UAE Information Assurance Regulation compliance assessment
Date: 2024-03-12T16:37:06+00:00
Author: Vishal Pabari
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Abu Dhabi;Auditing;AWS security;Cloud Services Provider;Compliance;CSP;Dubai;IAR;Information Assurance Regulation;MEA;Middle East;Security;Security Blog;UAE
Slug: 2024-03-12-aws-completes-the-annual-uae-information-assurance-regulation-compliance-assessment

[Source](https://aws.amazon.com/blogs/security/aws-completes-the-annual-uae-information-assurance-regulation-compliance-assessment/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce the publication of our annual compliance assessment report on the Information Assurance Regulation (IAR) established by the Telecommunications and Digital Government Regulatory Authority (TDRA) of the United Arab Emirates (UAE). The report covers the AWS Middle East (UAE) Region. The IAR provides management and technical information security [...]
