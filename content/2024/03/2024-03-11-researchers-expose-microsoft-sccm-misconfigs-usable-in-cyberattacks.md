Title: Researchers expose Microsoft SCCM misconfigs usable in cyberattacks
Date: 2024-03-11T15:15:21-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-03-11-researchers-expose-microsoft-sccm-misconfigs-usable-in-cyberattacks

[Source](https://www.bleepingcomputer.com/news/security/researchers-expose-microsoft-sccm-misconfigs-usable-in-cyberattacks/){:target="_blank" rel="noopener"}

> Security researchers have created a knowledge base repository for attack and defense techniques based on improperly setting up Microsoft's Configuration Manager, which could allow an attacker to execute payloads or become a domain controller. [...]
