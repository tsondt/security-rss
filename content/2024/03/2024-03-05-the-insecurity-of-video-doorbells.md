Title: The Insecurity of Video Doorbells
Date: 2024-03-05T12:05:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Internet of Things;physical security;video
Slug: 2024-03-05-the-insecurity-of-video-doorbells

[Source](https://www.schneier.com/blog/archives/2024/03/the-insecurity-of-video-doorbells.html){:target="_blank" rel="noopener"}

> Consumer Reports has analyzed a bunch of popular Internet-connected video doorbells. Their security is terrible. First, these doorbells expose your home IP address and WiFi network name to the internet without encryption, potentially opening your home network to online criminals. [...] Anyone who can physically access one of the doorbells can take over the device—no tools or fancy hacking skills needed. [...]
