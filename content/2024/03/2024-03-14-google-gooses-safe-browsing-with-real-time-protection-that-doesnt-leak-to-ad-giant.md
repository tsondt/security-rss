Title: Google gooses Safe Browsing with real-time protection that doesn't leak to ad giant
Date: 2024-03-14T17:58:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-03-14-google-gooses-safe-browsing-with-real-time-protection-that-doesnt-leak-to-ad-giant

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/14/google_safe_browsing_update/){:target="_blank" rel="noopener"}

> Rare occasion when you do want Big Tech to make a hash of it Google has enhanced its Safe Browsing service to enable real-time protection in Chrome for desktop, iOS, and soon Android against risky websites, without sending browsing history data to the ad biz.... [...]
