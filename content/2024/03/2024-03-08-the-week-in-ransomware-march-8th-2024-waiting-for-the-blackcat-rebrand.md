Title: The Week in Ransomware - March 8th 2024 - Waiting for the BlackCat rebrand
Date: 2024-03-08T17:25:25-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-03-08-the-week-in-ransomware-march-8th-2024-waiting-for-the-blackcat-rebrand

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-march-8th-2024-waiting-for-the-blackcat-rebrand/){:target="_blank" rel="noopener"}

> We saw another ransomware operation shut down this week after first getting breached by law enforcement and then targeting critical infrastructure, putting them further in the spotlight of the US government. [...]
