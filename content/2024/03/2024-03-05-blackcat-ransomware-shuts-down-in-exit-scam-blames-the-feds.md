Title: BlackCat ransomware shuts down in exit scam, blames the "feds"
Date: 2024-03-05T10:49:16-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-03-05-blackcat-ransomware-shuts-down-in-exit-scam-blames-the-feds

[Source](https://www.bleepingcomputer.com/news/security/blackcat-ransomware-shuts-down-in-exit-scam-blames-the-feds/){:target="_blank" rel="noopener"}

> The BlackCat ransomware gang is pulling an exit scam, trying to shut down and run off with affiliates' money by pretending the FBI seized their site and infrastructure. [...]
