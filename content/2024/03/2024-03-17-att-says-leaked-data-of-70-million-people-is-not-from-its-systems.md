Title: AT&T says leaked data of 70 million people is not from its systems
Date: 2024-03-17T19:24:32-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-03-17-att-says-leaked-data-of-70-million-people-is-not-from-its-systems

[Source](https://www.bleepingcomputer.com/news/security/att-says-leaked-data-of-70-million-people-is-not-from-its-systems/){:target="_blank" rel="noopener"}

> AT&T says a massive trove of data impacting 71 million people did not originate from its systems after a hacker leaked it on a cybercrime forum and claimed it was stolen in a 2021 breach of the company. [...]
