Title: Friday Squid Blogging: New Extinct Species of Vampire Squid Discovered
Date: 2024-03-01T22:05:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;squid
Slug: 2024-03-01-friday-squid-blogging-new-extinct-species-of-vampire-squid-discovered

[Source](https://www.schneier.com/blog/archives/2024/03/friday-squid-blogging-new-extinct-species-of-vampire-squid-discovered.html){:target="_blank" rel="noopener"}

> Paleontologists have discovered a 183-million-year-old species of vampire squid. Prior research suggests that the vampyromorph lived in the shallows off an island that once existed in what is now the heart of the European mainland. The research team believes that the remarkable degree of preservation of this squid is due to unique conditions at the moment of the creature’s death. Water at the bottom of the sea where it ventured would have been poorly oxygenated, causing the creature to suffocate. In addition to killing the squid, it would have prevented other creatures from feeding on its remains, allowing it to [...]
