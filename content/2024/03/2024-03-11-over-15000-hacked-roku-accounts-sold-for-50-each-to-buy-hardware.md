Title: Over 15,000 hacked Roku accounts sold for 50¢ each to buy hardware
Date: 2024-03-11T13:49:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-11-over-15000-hacked-roku-accounts-sold-for-50-each-to-buy-hardware

[Source](https://www.bleepingcomputer.com/news/security/over-15-000-hacked-roku-accounts-sold-for-50-each-to-buy-hardware/){:target="_blank" rel="noopener"}

> Roku has disclosed a data breach impacting over 15,000 customers after hacked accounts were used to make fraudulent purchases of hardware and streaming subscriptions. [...]
