Title: Execs in Japan busted for winning dev bids then outsourcing to North Koreans
Date: 2024-03-28T06:30:15+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-03-28-execs-in-japan-busted-for-winning-dev-bids-then-outsourcing-to-north-koreans

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/28/japan_nk_arrests/){:target="_blank" rel="noopener"}

> Government issues stern warning over despot money-making scheme Two executives were issued arrest warrants in Japan on Wednesday, reportedly for charges related to establishing a business that outsourced work to North Korean IT engineers.... [...]
