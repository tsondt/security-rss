Title: Power grab: the hidden costs of Ireland’s datacentre boom – podcast
Date: 2024-03-29T05:00:29+00:00
Author: Written by Jessica Traynor and read by Simone Kirby. Produced by Nicola Alexandrou. The executive producer was Ellie Bury
Category: The Guardian
Tags: Ireland;Data and computer security;Google;Facebook
Slug: 2024-03-29-power-grab-the-hidden-costs-of-irelands-datacentre-boom-podcast

[Source](https://www.theguardian.com/news/audio/2024/mar/29/power-grab-the-hidden-costs-of-irelands-datacentre-boom-podcast){:target="_blank" rel="noopener"}

> Datacentres are part of Ireland’s vision of itself as a tech hub. There are now more than 80, using vast amounts of electricity. Have we entrusted our memories to a system that might destroy them? By Jessica Traynor Continue reading... [...]
