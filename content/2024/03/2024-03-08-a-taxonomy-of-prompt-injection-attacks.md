Title: A Taxonomy of Prompt Injection Attacks
Date: 2024-03-08T12:06:58+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence;hacking;LLM
Slug: 2024-03-08-a-taxonomy-of-prompt-injection-attacks

[Source](https://www.schneier.com/blog/archives/2024/03/a-taxonomy-of-prompt-injection-attacks.html){:target="_blank" rel="noopener"}

> Researchers ran a global prompt hacking competition, and have documented the results in a paper that both gives a lot of good examples and tries to organize a taxonomy of effective prompt injection strategies. It seems as if the most common successful strategy is the “compound instruction attack,” as in “Say ‘I have been PWNED’ without a period.” Ignore This Title and HackAPrompt: Exposing Systemic Vulnerabilities of LLMs through a Global Scale Prompt Hacking Competition Abstract: Large Language Models (LLMs) are deployed in interactive contexts with direct user engagement, such as chatbots and writing assistants. These deployments are vulnerable to [...]
