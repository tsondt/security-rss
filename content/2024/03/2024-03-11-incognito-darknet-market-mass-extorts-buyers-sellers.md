Title: Incognito Darknet Market Mass-Extorts Buyers, Sellers
Date: 2024-03-11T16:19:36+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;The Coming Storm;Brett Johnson;double extortion;exit scam;Incognito Market;Shadowcrew
Slug: 2024-03-11-incognito-darknet-market-mass-extorts-buyers-sellers

[Source](https://krebsonsecurity.com/2024/03/incognito-darknet-market-mass-extorts-buyers-sellers/){:target="_blank" rel="noopener"}

> Borrowing from the playbook of ransomware purveyors, the darknet narcotics bazaar Incognito Market has begun extorting all of its vendors and buyers, threatening to publish cryptocurrency transaction and chat records of users who refuse to pay a fee ranging from $100 to $20,000. The bold mass extortion attempt comes just days after Incognito Market administrators reportedly pulled an “exit scam” that left users unable to withdraw millions of dollars worth of funds from the platform. An extortion message currently on the Incognito Market homepage. In the past 24 hours, the homepage for the Incognito Market was updated to include a [...]
