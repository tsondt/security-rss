Title: Tor’s new WebTunnel bridges mimic HTTPS traffic to evade censorship
Date: 2024-03-12T12:49:24-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-03-12-tors-new-webtunnel-bridges-mimic-https-traffic-to-evade-censorship

[Source](https://www.bleepingcomputer.com/news/security/tors-new-webtunnel-bridges-mimic-https-traffic-to-evade-censorship/){:target="_blank" rel="noopener"}

> The Tor Project officially introduced WebTunnel, a new bridge type specifically designed to help bypass censorship targeting the Tor network by hiding connections in plain sight. [...]
