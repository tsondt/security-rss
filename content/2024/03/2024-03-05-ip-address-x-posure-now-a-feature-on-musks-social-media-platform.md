Title: IP address X-posure now a feature on Musk's social media platform
Date: 2024-03-05T16:18:04+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-03-05-ip-address-x-posure-now-a-feature-on-musks-social-media-platform

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/05/ip_address_xposure_now_a/){:target="_blank" rel="noopener"}

> If you're still on X you'd better disable this insecure-by-default calling feature Video and audio calling features for X Premium users added last year to Elon Musk's version of Twitter have been expanded to everyone on the platform, and we're warning Reg readers yet again to disable the feature - this time because it appears to expose user IP addresses.... [...]
