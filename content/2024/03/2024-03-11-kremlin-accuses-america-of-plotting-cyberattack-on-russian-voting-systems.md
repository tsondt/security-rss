Title: Kremlin accuses America of plotting cyberattack on Russian voting systems
Date: 2024-03-11T21:58:50+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-11-kremlin-accuses-america-of-plotting-cyberattack-on-russian-voting-systems

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/11/kremlin_accuses_us_election_cyberattack/){:target="_blank" rel="noopener"}

> Don't worry, we have a strong suspicion Putin's still gonna win The Kremlin has accused the United States of meddling in Russia's upcoming presidential election, and even accused Uncle Sam of planning a cyberattack on the country's online voting system.... [...]
