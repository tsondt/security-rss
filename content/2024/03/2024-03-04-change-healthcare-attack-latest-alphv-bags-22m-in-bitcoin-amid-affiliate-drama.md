Title: Change Healthcare attack latest: ALPHV bags $22M in Bitcoin amid affiliate drama
Date: 2024-03-04T21:01:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-04-change-healthcare-attack-latest-alphv-bags-22m-in-bitcoin-amid-affiliate-drama

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/04/alphv_ransom_payment/){:target="_blank" rel="noopener"}

> No honor among thieves? ALPHV/BlackCat, the gang behind the Change Healthcare cyberattack, has received more than $22 million in Bitcoin in what might be a ransomware payment.... [...]
