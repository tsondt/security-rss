Title: Possible China link to Change Healthcare ransomware attack
Date: 2024-03-07T18:30:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-07-possible-china-link-to-change-healthcare-ransomware-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/07/china_link_change_healthcare_ransomware/){:target="_blank" rel="noopener"}

> Alleged crim bought SmartScreen Killer, Cobalt Strike on dark-web markets A criminal claiming to be an ALPHV/BlackCat affiliate — the gang responsible for the widely disruptive Change Healthcare ransomware infection last month — may have ties to Chinese government-backed cybercrime syndicates.... [...]
