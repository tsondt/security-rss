Title: White House and EPA warn of hackers breaching water systems
Date: 2024-03-19T18:04:28-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-03-19-white-house-and-epa-warn-of-hackers-breaching-water-systems

[Source](https://www.bleepingcomputer.com/news/security/white-house-and-epa-warn-of-hackers-breaching-water-systems/){:target="_blank" rel="noopener"}

> U.S. National Security Advisor Jake Sullivan and Environmental Protection Agency (EPA) Administrator Michael Regan warned governors today that hackers are "striking" critical infrastructure across the country's water sector. [...]
