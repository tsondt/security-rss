Title: How to access AWS resources from Microsoft Entra ID tenants using AWS Security Token Service
Date: 2024-03-05T16:14:18+00:00
Author: Vasanth Selvaraj
Category: AWS Security
Tags: AWS Security Token Service;Intermediate (200);Security, Identity, & Compliance;Technical How-to;AWS STS;Security Blog
Slug: 2024-03-05-how-to-access-aws-resources-from-microsoft-entra-id-tenants-using-aws-security-token-service

[Source](https://aws.amazon.com/blogs/security/how-to-access-aws-resources-from-microsoft-entra-id-tenants-using-aws-security-token-service/){:target="_blank" rel="noopener"}

> Use of long-term access keys for authentication between cloud resources increases the risk of key exposure and unauthorized secrets reuse. Amazon Web Services (AWS) has developed a solution to enable customers to securely authenticate Azure resources with AWS resources using short-lived tokens to reduce risks to secure authentication. In this post, we guide you through [...]
