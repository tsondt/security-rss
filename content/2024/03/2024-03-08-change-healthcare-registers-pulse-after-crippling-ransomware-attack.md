Title: Change Healthcare registers pulse after crippling ransomware attack
Date: 2024-03-08T14:33:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-08-change-healthcare-registers-pulse-after-crippling-ransomware-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/08/change_healthcare_restores_first_system/){:target="_blank" rel="noopener"}

> Remaining services are expected to return in the coming weeks after $22M ALPHV ransom Change Healthcare has taken the first steps toward a full recovery from the ransomware attack in February by bringing its electronic prescription services back online.... [...]
