Title: American Express admits card data exposed and blames third party
Date: 2024-03-04T23:04:16+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-03-04-american-express-admits-card-data-exposed-and-blames-third-party

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/04/american_express/){:target="_blank" rel="noopener"}

> Don't leave home without... IT security A security failure at a third-party vendor exposed an untold number of American Express card numbers, expiry dates, and other data to persons unknown.... [...]
