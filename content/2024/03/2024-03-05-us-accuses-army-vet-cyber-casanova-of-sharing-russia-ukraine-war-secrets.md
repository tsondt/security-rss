Title: US accuses Army vet cyber-Casanova of sharing Russia-Ukraine war secrets
Date: 2024-03-05T17:06:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-05-us-accuses-army-vet-cyber-casanova-of-sharing-russia-ukraine-war-secrets

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/05/us_indicts_army_vet/){:target="_blank" rel="noopener"}

> Where better to expose confidential data than on a dating app? Yet another US military man is facing a potentially significant stretch in prison after allegedly sending secret national defense information (NDI) overseas.... [...]
