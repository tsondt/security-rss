Title: How to improve resilience to DDoS attacks with Cloud Armor Advanced rate limiting capabilities
Date: 2024-03-19T16:00:00+00:00
Author: Lihi Shadmi
Category: GCP Security
Tags: Security & Identity
Slug: 2024-03-19-how-to-improve-resilience-to-ddos-attacks-with-cloud-armor-advanced-rate-limiting-capabilities

[Source](https://cloud.google.com/blog/products/identity-security/how-to-improve-resilience-to-ddos-attacks-with-cloud-armor-advanced-rate-limiting-capabilities/){:target="_blank" rel="noopener"}

> In recent years, we've been seeing an exponential surge in the number, volume, and complexity of distributed denial-of-service (DDoS) attacks. In September 2023, we saw the largest DDoS attack to date, which peaked at 398 million requests per second. As the threat landscape evolves, enterprises face a pressing need for effective measures to mitigate these attacks. Google Cloud Armor's always-on Layer 3 and Layer 4 DDoS defense, Web Application Firewall (WAF), Adaptive Protection, bot management, threat intelligence, and rate-limiting capabilities can help enterprises build a comprehensive DDoS mitigation strategy. Internet-facing applications are vulnerable to application layer DDoS attacks (such as [...]
