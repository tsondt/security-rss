Title: Vans claims cyber crooks didn't run off with its customers' financial info
Date: 2024-03-24T10:08:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-24-vans-claims-cyber-crooks-didnt-run-off-with-its-customers-financial-info

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/24/vans_breach_disclosure/){:target="_blank" rel="noopener"}

> Just 35.5M names, addresses, emails, phone numbers... no biggie Clothing and footwear giant VF Corporation is letting 35.5 million of its customers know they may find themselves victims of identity theft following last year's security breach.... [...]
