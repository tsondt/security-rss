Title: VMware sandbox escape bugs are so critical, patches are released for end-of-life products
Date: 2024-03-06T20:19:39+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;sandbox;Virtual Machine;vmware;vulnerabilities
Slug: 2024-03-06-vmware-sandbox-escape-bugs-are-so-critical-patches-are-released-for-end-of-life-products

[Source](https://arstechnica.com/?p=2008406){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) VMware is urging customers to patch critical vulnerabilities that make it possible for hackers to break out of sandbox and hypervisor protections in all versions, including out-of-support ones, of VMware ESXi, Workstation, Fusion, and Cloud Foundation products. A constellation of four vulnerabilities—two carrying severity ratings of 9.3 out of a possible 10—are serious because they undermine the fundamental purpose of the VMware products, which is to run sensitive operations inside a virtual machine that’s segmented from the host machine. VMware officials said that the prospect of a hypervisor escape warranted an immediate response under the company’s [...]
