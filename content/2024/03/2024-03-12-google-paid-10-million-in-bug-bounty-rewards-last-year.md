Title: Google paid $10 million in bug bounty rewards last year
Date: 2024-03-12T12:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Google;Security
Slug: 2024-03-12-google-paid-10-million-in-bug-bounty-rewards-last-year

[Source](https://www.bleepingcomputer.com/news/google/google-paid-10-million-in-bug-bounty-rewards-last-year/){:target="_blank" rel="noopener"}

> Google awarded $10 million to 632 researchers from 68 countries in 2023 for finding and responsibly reporting security flaws in the company's products and services. [...]
