Title: Recent ‘MFA Bombing’ Attacks Targeting Apple Users
Date: 2024-03-26T15:37:54+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;The Coming Storm;apple;Apple Recovery Key;Kishan Bagaria;MFA bombing;MFA fatigue;Parth Patel;PeopleDataLabs
Slug: 2024-03-26-recent-mfa-bombing-attacks-targeting-apple-users

[Source](https://krebsonsecurity.com/2024/03/recent-mfa-bombing-attacks-targeting-apple-users/){:target="_blank" rel="noopener"}

> Several Apple customers recently reported being targeted in elaborate phishing attacks that involve what appears to be a bug in Apple’s password reset feature. In this scenario, a target’s Apple devices are forced to display dozens of system-level prompts that prevent the devices from being used until the recipient responds “Allow” or “Don’t Allow” to each prompt. Assuming the user manages not to fat-finger the wrong button on the umpteenth password reset request, the scammers will then call the victim while spoofing Apple support in the caller ID, saying the user’s account is under attack and that Apple support needs [...]
