Title: U.S. sanctions Predator spyware operators for spying on Americans
Date: 2024-03-05T13:09:37-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;Mobile;Security
Slug: 2024-03-05-us-sanctions-predator-spyware-operators-for-spying-on-americans

[Source](https://www.bleepingcomputer.com/news/legal/us-sanctions-predator-spyware-operators-for-spying-on-americans/){:target="_blank" rel="noopener"}

> The U.S. has imposed sanctions on two individuals and five entities linked to the development and distribution of the Predator commercial spyware used to target Americans, including government officials and journalists. [...]
