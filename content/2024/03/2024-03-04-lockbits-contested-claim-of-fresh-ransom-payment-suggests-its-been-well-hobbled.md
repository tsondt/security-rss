Title: LockBit's contested claim of fresh ransom payment suggests it's been well hobbled
Date: 2024-03-04T03:15:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-03-04-lockbits-contested-claim-of-fresh-ransom-payment-suggests-its-been-well-hobbled

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/04/in_brief/){:target="_blank" rel="noopener"}

> ALSO: CISA warns Ivanti vuln mitigations might not work, SAML hijack doesn't need ADFS, and crit vulns Infosec in brief The infamous LockBit ransomware gang has been busy in the ten days since an international law enforcement operation took down many of its systems. But despite its posturing, the gang might have suffered more than it's letting on.... [...]
