Title: Air National Guardsman Teixeira to admit he was Pentagon files leaker
Date: 2024-03-01T22:03:15+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-01-air-national-guardsman-teixeira-to-admit-he-was-pentagon-files-leaker

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/01/pentagon_leaks_teixeira/){:target="_blank" rel="noopener"}

> Turns out bragging on Discord has unfortunate consequences Jack Teixeira, the Air National Guardsman accused of leaking dozens of classified Pentagon documents, is expected to plead guilty in a US court on Monday.... [...]
