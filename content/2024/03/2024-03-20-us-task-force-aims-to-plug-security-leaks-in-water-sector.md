Title: US task force aims to plug security leaks in water sector
Date: 2024-03-20T18:32:17+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-20-us-task-force-aims-to-plug-security-leaks-in-water-sector

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/20/us_water_sector_cybersecurity/){:target="_blank" rel="noopener"}

> From a trickle to a flood, threats now seen as too great to ignore US government is urging state officials to band together to improve the cybersecurity of the country's water sector amid growing threats from foreign adversaries.... [...]
