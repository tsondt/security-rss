Title: Australian techie jailed for accessing museum's accounting system and buying himself stuff
Date: 2024-03-20T01:45:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-03-20-australian-techie-jailed-for-accessing-museums-accounting-system-and-buying-himself-stuff

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/20/australian_techie_jailed_accounting_fraud/){:target="_blank" rel="noopener"}

> Also down under, researchers find security-cleared workers leaking details of their gigs An Australian IT contractor has been sentenced to 30 months jail for ripping off the National Maritime Museum.... [...]
