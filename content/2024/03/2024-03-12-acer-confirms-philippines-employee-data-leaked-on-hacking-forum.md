Title: Acer confirms Philippines employee data leaked on hacking forum
Date: 2024-03-12T15:31:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-12-acer-confirms-philippines-employee-data-leaked-on-hacking-forum

[Source](https://www.bleepingcomputer.com/news/security/acer-confirms-philippines-employee-data-leaked-on-hacking-forum/){:target="_blank" rel="noopener"}

> Acer Philippines confirmed that employee data was stolen in an attack on a third-party vendor who manages the company's employee attendance data after a threat actor leaked the data on a hacking forum. [...]
