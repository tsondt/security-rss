Title: As if working at Helldesk weren't bad enough, IT helpers now targeted by cybercrims
Date: 2024-03-15T19:00:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-15-as-if-working-at-helldesk-werent-bad-enough-it-helpers-now-targeted-by-cybercrims

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/15/it_helpdeskers_under_increased_threat/){:target="_blank" rel="noopener"}

> Wave of Okta attacks mark what researchers are calling the biggest security trend of the year IT helpdesk workers are increasingly the target of cybercriminals – a trend researchers have described as "the most noteworthy" of the past year.... [...]
