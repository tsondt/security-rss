Title: Swiss cheese security? Play ransomware gang milks government of 65,000 files
Date: 2024-03-08T12:35:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-08-swiss-cheese-security-play-ransomware-gang-milks-government-of-65000-files

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/08/swiss_government_files_ransomware/){:target="_blank" rel="noopener"}

> Classified docs, readable passwords, and thousands of personal information nabbed in Xplain breach The Swiss government had around 65,000 files related to it stolen by the Play ransomware gang during an attack on an IT supplier, its National Cyber Security Center (NCSC) says.... [...]
