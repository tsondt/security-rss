Title: Infosec teams must be allowed to fail, argues Gartner
Date: 2024-03-18T07:29:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-03-18-infosec-teams-must-be-allowed-to-fail-argues-gartner

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/18/gartner_infosec_failure_advice/){:target="_blank" rel="noopener"}

> But failing to recover from incidents is unforgivable because 'adrenalin does not scale' Zero tolerance of failure by information security professionals is unrealistic, and makes it harder for cyber security folk to do the essential part of their job: recovering fast from inevitable attacks, according to Gartner analysts Chris Mixter and Dennis Xu.... [...]
