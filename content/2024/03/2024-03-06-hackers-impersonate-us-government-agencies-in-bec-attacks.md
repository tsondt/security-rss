Title: Hackers impersonate U.S. government agencies in BEC attacks
Date: 2024-03-06T15:34:09-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-06-hackers-impersonate-us-government-agencies-in-bec-attacks

[Source](https://www.bleepingcomputer.com/news/security/hackers-impersonate-us-government-agencies-in-bec-attacks/){:target="_blank" rel="noopener"}

> A gang of hackers specialized in business email compromise (BEC) attacks and tracked as TA4903 has been impersonating various U.S. government entities to lure targets into opening malicious files carrying links to fake bidding processes. [...]
