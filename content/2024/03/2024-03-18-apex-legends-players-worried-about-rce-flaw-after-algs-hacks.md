Title: Apex Legends players worried about RCE flaw after ALGS hacks
Date: 2024-03-18T12:09:07-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2024-03-18-apex-legends-players-worried-about-rce-flaw-after-algs-hacks

[Source](https://www.bleepingcomputer.com/news/security/apex-legends-players-worried-about-rce-flaw-after-algs-hacks/){:target="_blank" rel="noopener"}

> Electronic Arts has postponed the North American (NA) finals of the ongoing Apex Legends Global Series (ALGS) after hackers compromised players mid-match during the tournament. [...]
