Title: Meta sues ex infra VP for allegedly stealing top-secret datacenter blueprints
Date: 2024-03-12T22:39:12+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-03-12-meta-sues-ex-infra-vp-for-allegedly-stealing-top-secret-datacenter-blueprints

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/12/meta_vp_infrastructure_allegations/){:target="_blank" rel="noopener"}

> Exec accused of using own work PC to swipe confidential AI and staffing docs for stealth cloud startup An ex-Meta veep has been sued by his former bosses for "brazenly disloyal and dishonest conduct" – and by that, they mean he allegedly stole confidential documents to help him build and recruit colleagues for an AI cloud startup.... [...]
