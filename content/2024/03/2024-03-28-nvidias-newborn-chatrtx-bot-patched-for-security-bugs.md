Title: Nvidia's newborn ChatRTX bot patched for security bugs
Date: 2024-03-28T15:33:13+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-03-28-nvidias-newborn-chatrtx-bot-patched-for-security-bugs

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/28/nvidia_chatrtx_security_flaws/){:target="_blank" rel="noopener"}

> Flaws enable privilege escalation and remote code execution Nvidia's AI-powered ChatRTX app launched just six week ago but already has received patches for two security vulnerabilities that enabled attack vectors, including privilege escalation and remote code execution.... [...]
