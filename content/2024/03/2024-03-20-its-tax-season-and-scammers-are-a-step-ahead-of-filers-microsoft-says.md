Title: It's tax season, and scammers are a step ahead of filers, Microsoft says
Date: 2024-03-20T19:30:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-20-its-tax-season-and-scammers-are-a-step-ahead-of-filers-microsoft-says

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/20/its_tax_season_and_scammers/){:target="_blank" rel="noopener"}

> Phishing season started early with crims intent on the hooking early filers As the digital wolves dress in sheep's tax forms, Microsoft has thrown a spotlight on a crafty 2024 phishing expedition, unraveled in January, that preys on the unsuspecting herd of early tax filers.... [...]
