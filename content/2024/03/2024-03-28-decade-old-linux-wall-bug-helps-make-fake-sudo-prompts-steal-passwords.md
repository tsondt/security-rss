Title: Decade-old Linux ‘wall’ bug helps make fake SUDO prompts, steal passwords
Date: 2024-03-28T17:03:49-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Linux
Slug: 2024-03-28-decade-old-linux-wall-bug-helps-make-fake-sudo-prompts-steal-passwords

[Source](https://www.bleepingcomputer.com/news/security/decade-old-linux-wall-bug-helps-make-fake-sudo-prompts-steal-passwords/){:target="_blank" rel="noopener"}

> A vulnerability has been discovered in the 'util-linux' library that could allow unprivileged users to put arbitrary text on other users' terminals using the 'wall' command. [...]
