Title: Friday Squid Blogging: Operation Squid
Date: 2024-03-15T21:08:32+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;drug trade;squid
Slug: 2024-03-15-friday-squid-blogging-operation-squid

[Source](https://www.schneier.com/blog/archives/2024/03/friday-squid-blogging-operation-squid.html){:target="_blank" rel="noopener"}

> Operation Squid found 1.3 tons of cocaine hidden in frozen fish. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
