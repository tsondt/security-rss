Title: US moves to recover $2.3 million from "pig butchers" on Binance
Date: 2024-03-15T14:02:48-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: CryptoCurrency;Legal;Security
Slug: 2024-03-15-us-moves-to-recover-23-million-from-pig-butchers-on-binance

[Source](https://www.bleepingcomputer.com/news/cryptocurrency/us-moves-to-recover-23-million-from-pig-butchers-on-binance/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice (DoJ) is recovering $2.3 million worth of cryptocurrency linked to a "pig butchering" fraud scheme that victimized at least 37 people across the United States. [...]
