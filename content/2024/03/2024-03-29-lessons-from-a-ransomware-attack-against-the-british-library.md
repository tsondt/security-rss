Title: Lessons from a Ransomware Attack against the British Library
Date: 2024-03-29T11:03:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;ransomware;reports
Slug: 2024-03-29-lessons-from-a-ransomware-attack-against-the-british-library

[Source](https://www.schneier.com/blog/archives/2024/03/lessons-from-a-ransomware-attack-against-the-british-library.html){:target="_blank" rel="noopener"}

> You might think that libraries are kind of boring, but this self-analysis of a 2023 ransomware and extortion attack against the British Library is anything but. [...]
