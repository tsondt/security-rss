Title: Tech support firms Restoro, Reimage fined $26 million for scare tactics
Date: 2024-03-14T12:40:02-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-03-14-tech-support-firms-restoro-reimage-fined-26-million-for-scare-tactics

[Source](https://www.bleepingcomputer.com/news/security/tech-support-firms-restoro-reimage-fined-26-million-for-scare-tactics/){:target="_blank" rel="noopener"}

> Tech support companies Restoro and Reimage will pay $26 million to settle charges that they used scare tactics to trick their customers into paying for unnecessary computer repair services. [...]
