Title: Google Chrome gets real-time phishing protection later this month
Date: 2024-03-14T12:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google;Security
Slug: 2024-03-14-google-chrome-gets-real-time-phishing-protection-later-this-month

[Source](https://www.bleepingcomputer.com/news/google/google-chrome-gets-real-time-phishing-protection-later-this-month/){:target="_blank" rel="noopener"}

> Google will roll out a Safe Browsing update later this month that will provide real-time malware and phishing protection to all Chrome users, without compromising their browsing privacy. [...]
