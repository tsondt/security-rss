Title: UK council yanks IT systems and phone lines offline following cyber ambush
Date: 2024-03-12T11:45:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-12-uk-council-yanks-it-systems-and-phone-lines-offline-following-cyber-ambush

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/12/leicester_city_council_stays_shtum/){:target="_blank" rel="noopener"}

> Targeting recovery this week, officials still trying to 'dentify the nature of the incident' Leicester City Council says IT systems and a number of its critical service phone lines will remain down until later this week at the earliest following a "cyber incident".... [...]
