Title: Meta accused of snarfing people's Snapchat data via traffic decryption
Date: 2024-03-27T15:30:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-03-27-meta-accused-of-snarfing-peoples-snapchat-data-via-traffic-decryption

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/27/meta_snapchat_data/){:target="_blank" rel="noopener"}

> I ain't afraid of no ghosts, but in this case... To spy on rival Snapchat and get data on how the app was being used, Meta – when it was operating as Facebook – allegedly initiated a program called Project Ghostbusters, which intercepted data traffic from mobile apps. And it used that data to harm its competitors' ad business.... [...]
