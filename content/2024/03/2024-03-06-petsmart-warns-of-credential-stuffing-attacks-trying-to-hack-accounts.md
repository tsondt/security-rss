Title: PetSmart warns of credential stuffing attacks trying to hack accounts
Date: 2024-03-06T19:25:59-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-03-06-petsmart-warns-of-credential-stuffing-attacks-trying-to-hack-accounts

[Source](https://www.bleepingcomputer.com/news/security/petsmart-warns-of-credential-stuffing-attacks-trying-to-hack-accounts/){:target="_blank" rel="noopener"}

> Pet retail giant PetSmart is warning some customers their passwords were reset due to an ongoing credential stuffing attack attempting to breach accounts. [...]
