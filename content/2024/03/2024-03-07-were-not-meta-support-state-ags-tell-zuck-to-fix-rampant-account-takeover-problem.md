Title: We're not Meta support: State AGs tell Zuck to fix rampant account takeover problem
Date: 2024-03-07T21:45:15+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-03-07-were-not-meta-support-state-ags-tell-zuck-to-fix-rampant-account-takeover-problem

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/07/82_of_us_ags_agree/){:target="_blank" rel="noopener"}

> 'We refuse to operate as customer service representatives' A group of 41 US state attorneys general, tired of serving as a customer complaint clearinghouse for Facebook and Instagram users, have sent a letter to Meta asking it to figure out how to reduce a "dramatic and persistent spike" in account takeovers.... [...]
