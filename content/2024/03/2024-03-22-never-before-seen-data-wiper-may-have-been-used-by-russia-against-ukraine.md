Title: Never-before-seen data wiper may have been used by Russia against Ukraine
Date: 2024-03-22T00:37:51+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;data wipers;russia;Ukraine
Slug: 2024-03-22-never-before-seen-data-wiper-may-have-been-used-by-russia-against-ukraine

[Source](https://arstechnica.com/?p=2012093){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Researchers have unearthed never-before-seen wiper malware tied to the Kremlin and an operation two years ago that took out more than 10,000 satellite modems located mainly in Ukraine on the eve of Russia’s invasion of its neighboring country. AcidPour, as researchers from security firm Sentinel One have named the new malware, has stark similarities to AcidRain, a wiper discovered in March 2022 that Viasat has confirmed was used in the attack on its modems earlier that month. Wipers are malicious applications designed to destroy stored data or render devices inoperable. Viasat said AcidRain was installed on [...]
