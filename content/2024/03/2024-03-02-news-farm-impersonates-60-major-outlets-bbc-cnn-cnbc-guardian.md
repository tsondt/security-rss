Title: News farm impersonates 60+ major outlets: BBC, CNN, CNBC, Guardian...
Date: 2024-03-02T11:31:19-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2024-03-02-news-farm-impersonates-60-major-outlets-bbc-cnn-cnbc-guardian

[Source](https://www.bleepingcomputer.com/news/security/news-farm-impersonates-60-plus-major-outlets-bbc-cnn-cnbc-guardian/){:target="_blank" rel="noopener"}

> BleepingComputer has discovered a content farm operating some 60+ domains named after popular media outlets, including the BBC, CNBC, CNN, Forbes, Huffington Post, The Guardian, and Washington Post, among others. These sites build SEO for their online gambling ventures and sell "press release" slots at hefty prices. [...]
