Title: Capita says 2023 cyberattack costs a factor as it reports staggering £100M+ loss
Date: 2024-03-06T12:31:52+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-06-capita-says-2023-cyberattack-costs-a-factor-as-it-reports-staggering-100m-loss

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/06/capita_says_2023_cyberattack_recovery/){:target="_blank" rel="noopener"}

> Additional cuts announced, sparking fears of further layoffs Outsourcing giant Capita today reported a net loss of £106.6 million ($135.6 million) for calendar 2023, with the costly cyberattack by criminals making a hefty dent in its annual financials.... [...]
