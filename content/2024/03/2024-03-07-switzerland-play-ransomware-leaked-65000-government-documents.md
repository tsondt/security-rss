Title: Switzerland: Play ransomware leaked 65,000 government documents
Date: 2024-03-07T15:27:04-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-07-switzerland-play-ransomware-leaked-65000-government-documents

[Source](https://www.bleepingcomputer.com/news/security/switzerland-play-ransomware-leaked-65-000-government-documents/){:target="_blank" rel="noopener"}

> The National Cyber Security Centre (NCSC) of Switzerland has released a report on its analysis of a data breach following a ransomware attack on Xplain, disclosing that the incident impacted thousands of sensitive Federal government files. [...]
