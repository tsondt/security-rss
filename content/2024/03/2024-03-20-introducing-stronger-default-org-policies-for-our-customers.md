Title: Introducing stronger default Org Policies for our customers
Date: 2024-03-20T16:00:00+00:00
Author: Aakashi Kapoor
Category: GCP Security
Tags: Security & Identity
Slug: 2024-03-20-introducing-stronger-default-org-policies-for-our-customers

[Source](https://cloud.google.com/blog/products/identity-security/introducing-stronger-default-org-policies-for-our-customers/){:target="_blank" rel="noopener"}

> Google Cloud strives to make good security outcomes easier for customers to achieve. As part of this continued effort, we are releasing an updated and stronger set of security defaults that are automatically implemented for new customers. Google Cloud customers with a verified domain receive an organization resource, which is used as the root of the resource hierarchy and to provide scalable controls to your cloud environment through the use of the Organization Policy Service. With the release of secure-by-default organization resources, potentially insecure postures and outcomes are addressed with a bundle of organization policies that are enforced as soon [...]
