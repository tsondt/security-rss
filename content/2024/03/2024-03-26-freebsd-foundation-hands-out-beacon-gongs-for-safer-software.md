Title: FreeBSD Foundation hands out Beacon gongs for safer software
Date: 2024-03-26T10:15:13+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2024-03-26-freebsd-foundation-hands-out-beacon-gongs-for-safer-software

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/26/beacon_awards_freebsd/){:target="_blank" rel="noopener"}

> Multiple CHERI-related projects win money for important research that prizes safety over speed The inaugural Beacon Awards has handed three prizes to projects working on safer software for CHERI-enabled hardware running on the CheriBSD operating system.... [...]
