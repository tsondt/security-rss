Title: Street newspaper appears to have Big Issue with Qilin ransomware gang
Date: 2024-03-27T11:00:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-27-street-newspaper-appears-to-have-big-issue-with-qilin-ransomware-gang

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/27/big_issue_qilin_cyberattack/){:target="_blank" rel="noopener"}

> The days of cybercriminals having something of a moral compass are over The parent company of The Big Issue, a street newspaper and social enterprise for homeless people, is wrestling with a cybersecurity incident claimed by the Qilin ransomware gang.... [...]
