Title: Don't be like these 900+ websites and expose millions of passwords via Firebase
Date: 2024-03-18T21:29:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-03-18-dont-be-like-these-900-websites-and-expose-millions-of-passwords-via-firebase

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/18/google_firebase_cloud_security/){:target="_blank" rel="noopener"}

> Warning: Poorly configured Google Cloud databases spill billing info, plaintext credentials At least 900 websites built with Google's Firebase, a cloud database, have been misconfigured, leaving credentials, personal info, and other sensitive data inadvertently exposed to the public internet, according to security researchers.... [...]
