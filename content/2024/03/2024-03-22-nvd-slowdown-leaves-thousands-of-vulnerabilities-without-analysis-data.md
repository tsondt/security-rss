Title: NVD slowdown leaves thousands of vulnerabilities without analysis data
Date: 2024-03-22T13:45:07+00:00
Author: Steven J. Vaughan-Nichols
Category: The Register
Tags: 
Slug: 2024-03-22-nvd-slowdown-leaves-thousands-of-vulnerabilities-without-analysis-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/22/opinion_column_nist/){:target="_blank" rel="noopener"}

> Security world reacts as NIST does a lot less of oft criticized, 'almost always thankless' work Opinion The United States National Institute of Standards and Technology (NIST) has almost completely stopped adding analysis to Common Vulnerabilities and Exposures (CVEs) listed in the National Vulnerability Database. That means big headaches for anyone using CVEs to maintain their security.... [...]
