Title: How to choose a known, trusted supplier for open source software
Date: 2024-03-26T16:00:00+00:00
Author: Andy Chang
Category: GCP Security
Tags: Open Source;Partners;Security & Identity
Slug: 2024-03-26-how-to-choose-a-known-trusted-supplier-for-open-source-software

[Source](https://cloud.google.com/blog/products/identity-security/how-to-choose-a-known-trusted-supplier-for-open-source-software/){:target="_blank" rel="noopener"}

> Open-source software is used throughout the technology industry to help developers build software tools, apps, and services. While developers building with open-source software can (and often do) benefit greatly from the work of others, they should also conduct appropriate due diligence to protect against software supply chain attacks. With an increasing focus on managing open-source software supply chain risk, both Citi and Google strive to apply more rigor across risk mitigation, especially while choosing known and trusted suppliers where open source components are sourced from. Key open source attack vectors The diagram above highlights key open source attack vectors. We [...]
