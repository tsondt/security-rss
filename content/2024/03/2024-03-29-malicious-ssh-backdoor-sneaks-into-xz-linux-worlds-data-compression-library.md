Title: Malicious SSH backdoor sneaks into xz, Linux world's data compression library
Date: 2024-03-29T21:58:36+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-03-29-malicious-ssh-backdoor-sneaks-into-xz-linux-worlds-data-compression-library

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/29/malicious_backdoor_xz/){:target="_blank" rel="noopener"}

> Red Hat in all caps says STOP USAGE OF ANY FEDORA RAWHIDE INSTANCES Red Hat on Friday warned that a malicious backdoor found in the widely used data compression software library xz may be present in instances of Fedora Linux 40 and in the Fedora Rawhide developer distribution.... [...]
