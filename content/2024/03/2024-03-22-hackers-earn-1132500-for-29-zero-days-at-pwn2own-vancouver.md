Title: Hackers earn $1,132,500 for 29 zero-days at Pwn2Own Vancouver
Date: 2024-03-22T01:13:24-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-03-22-hackers-earn-1132500-for-29-zero-days-at-pwn2own-vancouver

[Source](https://www.bleepingcomputer.com/news/security/hackers-earn-1-132-500-for-29-zero-days-at-pwn2own-vancouver/){:target="_blank" rel="noopener"}

> Pwn2Own Vancouver 2024 has ended with security researchers collecting $1,132,500 after demoing 29 zero-days (and some bug collisions). [...]
