Title: American Express credit cards exposed in third-party data breach
Date: 2024-03-04T08:38:19-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-03-04-american-express-credit-cards-exposed-in-third-party-data-breach

[Source](https://www.bleepingcomputer.com/news/security/american-express-credit-cards-exposed-in-third-party-data-breach/){:target="_blank" rel="noopener"}

> American Express is warning customers that credit cards were exposed in a third-party data breach after a merchant processor was hacked. [...]
