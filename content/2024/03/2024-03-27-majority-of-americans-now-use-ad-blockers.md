Title: Majority of Americans now use ad blockers
Date: 2024-03-27T21:26:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-03-27-majority-of-americans-now-use-ad-blockers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/27/america_ad_blocker/){:target="_blank" rel="noopener"}

> We're dreaming of a white list, because we're just like the ones you used to know More than half of Americans are using ad blocking software, and among advertising, programming, and security professionals that fraction is more like two-thirds to three-quarters.... [...]
