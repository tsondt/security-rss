Title: Hackers abuse QEMU to covertly tunnel network traffic in cyberattacks
Date: 2024-03-05T11:47:33-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-05-hackers-abuse-qemu-to-covertly-tunnel-network-traffic-in-cyberattacks

[Source](https://www.bleepingcomputer.com/news/security/hackers-abuse-qemu-to-covertly-tunnel-network-traffic-in-cyberattacks/){:target="_blank" rel="noopener"}

> Malicious actors were detected abusing the open-source hypervisor platform QEMU as a tunneling tool in a cyberattack against a large company. [...]
