Title: The Week in Ransomware - March 1st 2024 - Healthcare under siege
Date: 2024-03-01T15:32:14-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-03-01-the-week-in-ransomware-march-1st-2024-healthcare-under-siege

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-march-1st-2024-healthcare-under-siege/){:target="_blank" rel="noopener"}

> Ransomware attacks on healthcare over the last few months have been relentless, with numerous ransomware operations targeting hospitals and medical services, causing disruption to patient care and access to prescription drugs in the USA. [...]
