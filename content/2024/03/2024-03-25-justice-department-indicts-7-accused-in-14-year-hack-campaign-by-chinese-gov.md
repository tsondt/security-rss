Title: Justice Department indicts 7 accused in 14-year hack campaign by Chinese gov
Date: 2024-03-25T20:20:53+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Policy;Security;apt31;hacking;people's republic of china;zirconium
Slug: 2024-03-25-justice-department-indicts-7-accused-in-14-year-hack-campaign-by-chinese-gov

[Source](https://arstechnica.com/?p=2012482){:target="_blank" rel="noopener"}

> Enlarge (credit: peterschreiber.media | Getty Images) The US Justice Department on Monday unsealed an indictment charging seven men with hacking or attempting to hack dozens of US companies in a 14-year campaign furthering an economic espionage and foreign intelligence gathering by the Chinese government. All seven defendants, federal prosecutors alleged, were associated with Wuhan Xiaoruizhi Science & Technology Co., Ltd. a front company created by the Hubei State Security Department, an outpost of the Ministry of State Security located in Wuhan province. The MSS, in turn, has funded an advanced persistent threat group tracked under names including APT31, Zirconium Violet [...]
