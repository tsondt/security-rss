Title: Public AI as an Alternative to Corporate AI
Date: 2024-03-21T11:03:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;LLM
Slug: 2024-03-21-public-ai-as-an-alternative-to-corporate-ai

[Source](https://www.schneier.com/blog/archives/2024/03/public-ai-as-an-alternative-to-corporate-ai.html){:target="_blank" rel="noopener"}

> This mini-essay was my contribution to a round table on Power and Governance in the Age of AI. It’s nothing I haven’t said here before, but for anyone who hasn’t read my longer essays on the topic, it’s a shorter introduction. The increasingly centralized control of AI is an ominous sign. When tech billionaires and corporations steer AI, we get AI that tends to reflect the interests of tech billionaires and corporations, instead of the public. Given how transformative this technology will be for the world, this is a problem. To benefit society as a whole we need an AI [...]
