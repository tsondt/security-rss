Title: Mozilla fixes $100,000 Firefox zero-days following two-day hackathon
Date: 2024-03-25T15:00:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-25-mozilla-fixes-100000-firefox-zero-days-following-two-day-hackathon

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/25/mozilla_fixes_firefox_zerodays/){:target="_blank" rel="noopener"}

> Users may have to upgrade twice to protect their browsers Mozilla has swiftly patched a pair of critical Firefox zero-days after a researcher debuted them at a Vancouver cybersec competition.... [...]
