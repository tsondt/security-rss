Title: ASCII art elicits harmful responses from 5 major AI chatbots
Date: 2024-03-16T00:17:24+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: AI;Biz & IT;Security;Artificial Intelligence;ascii;jailbreak;large language model;LLM
Slug: 2024-03-16-ascii-art-elicits-harmful-responses-from-5-major-ai-chatbots

[Source](https://arstechnica.com/?p=2010646){:target="_blank" rel="noopener"}

> Enlarge / Some ASCII art of our favorite visual cliche for a hacker. (credit: Getty Images) Researchers have discovered a new way to hack AI assistants that uses a surprisingly old-school method: ASCII art. It turns out that chat-based large language models such as GPT-4 get so distracted trying to process these representations that they forget to enforce rules blocking harmful responses, such as those providing instructions for building bombs. ASCII art became popular in the 1970s, when the limitations of computers and printers prevented them from displaying images. As a result, users depicted images by carefully choosing and arranging [...]
