Title: Okta says data leaked on hacking forum not from its systems
Date: 2024-03-11T16:16:27-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-11-okta-says-data-leaked-on-hacking-forum-not-from-its-systems

[Source](https://www.bleepingcomputer.com/news/security/okta-says-data-leaked-on-hacking-forum-not-from-its-systems/){:target="_blank" rel="noopener"}

> Okta denies that its company data was leaked after a threat actor shared files allegedly stolen during an October 2023 cyberattack on a hacker forum. [...]
