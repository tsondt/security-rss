Title: Ransomware can mean life or death at hospitals. DEF CON hackers to the rescue?
Date: 2024-03-26T13:15:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-26-ransomware-can-mean-life-or-death-at-hospitals-def-con-hackers-to-the-rescue

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/26/aixcc_healthcare/){:target="_blank" rel="noopener"}

> ARPA-H joins DARPA's AIxCC, adds $20M to cash rewards Interview As ransomware gangs target critical infrastructure – especially hospitals and other healthcare organizations – DARPA has added another government agency partner to its Artificial Intelligence Cyber Challenge (AIxCC).... [...]
