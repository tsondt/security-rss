Title: Cop shop rapped for 'completely avoidable' web form blunder
Date: 2024-03-15T11:34:11+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2024-03-15-cop-shop-rapped-for-completely-avoidable-web-form-blunder

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/15/cop_shop_rapped_for_web/){:target="_blank" rel="noopener"}

> Made public highly sensitive data on complaints about Metropolitan Police Service The London Mayor's Office for Policing and Crime is being rapped by regulators for untidy tech practices that made public the personal data of hundreds of people who filed complaints against the Metropolitan Police Service.... [...]
