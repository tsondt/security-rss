Title: How an infamous ransomware gang found itself hacked – podcast
Date: 2024-03-13T03:00:01+00:00
Author: Presented by Nosheen Iqbal with Alex Hern and Jon DiMaggio; produced by Ned Carter Miles; executive editor Homa Khaleeli
Category: The Guardian
Tags: Cybercrime;Hacking;Data and computer security;Podcasts;Internet;World news
Slug: 2024-03-13-how-an-infamous-ransomware-gang-found-itself-hacked-podcast

[Source](https://www.theguardian.com/news/audio/2024/mar/13/how-an-infamous-ransomware-gang-found-itself-hacked-lockbit-podcast){:target="_blank" rel="noopener"}

> LockBit was a sophisticated criminal operation, offering the tools needed to steal a company’s data and hold it to ransom. Then it was itself hacked. Alex Hern reports A ransomware site on the dark web has allowed criminals to extort hospitals, businesses and schools for years. By encrypting data or threatening to post data online, hackers have cost companies millions of pounds. It’s called LockBit, and it was very successful until one day last month when hackers who logged on to the site found it had been hacked by authorities including the UK National Crime Agency and the FBI. These [...]
