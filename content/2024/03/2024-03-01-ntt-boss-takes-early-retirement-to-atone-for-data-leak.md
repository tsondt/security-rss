Title: NTT boss takes early retirement to atone for data leak
Date: 2024-03-01T05:27:05+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-03-01-ntt-boss-takes-early-retirement-to-atone-for-data-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/01/ntt_boss_resigns_dataleak/){:target="_blank" rel="noopener"}

> No mere mea culpa would suffice after 9.2 million records leaked over a decade, warnings were ignored, and lies were told NTT West president Masaaki Moribayashi announced his resignation on Thursday, effective at the end of March, in atonement for the leak of data pertaining to 9.28 million customers that came to light last October.... [...]
