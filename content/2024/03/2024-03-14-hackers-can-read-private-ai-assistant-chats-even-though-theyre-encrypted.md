Title: Hackers can read private AI-assistant chats even though they’re encrypted
Date: 2024-03-14T12:30:55+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: AI;Biz & IT;Features;Security;Artificial Intelligence;encryption;gpt;privacy;side channel
Slug: 2024-03-14-hackers-can-read-private-ai-assistant-chats-even-though-theyre-encrypted

[Source](https://arstechnica.com/?p=2010039){:target="_blank" rel="noopener"}

> Enlarge (credit: Aurich Lawson | Getty Images) AI assistants have been widely available for a little more than a year, and they already have access to our most private thoughts and business secrets. People ask them about becoming pregnant or terminating or preventing pregnancy, consult them when considering a divorce, seek information about drug addiction, or ask for edits in emails containing proprietary trade secrets. The providers of these AI-powered chat services are keenly aware of the sensitivity of these discussions and take active steps—mainly in the form of encrypting them—to prevent potential snoops from reading other people’s interactions. But [...]
