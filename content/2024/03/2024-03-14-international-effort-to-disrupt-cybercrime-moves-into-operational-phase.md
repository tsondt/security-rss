Title: International effort to disrupt cybercrime moves into operational phase
Date: 2024-03-14T15:00:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-14-international-effort-to-disrupt-cybercrime-moves-into-operational-phase

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/14/wef_cybercrime_atlas/){:target="_blank" rel="noopener"}

> Will the WEF experiment work? The Cybercrime Atlas, a massive undertaking that aims to disrupt cybercriminals across the globe, enters its operational phase in 2024, two years after organizers laid the groundwork at the RSA Conference.... [...]
