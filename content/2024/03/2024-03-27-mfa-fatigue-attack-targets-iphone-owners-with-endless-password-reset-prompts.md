Title: “MFA Fatigue” attack targets iPhone owners with endless password reset prompts
Date: 2024-03-27T18:10:10+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Apple;Biz & IT;Security;apple;icloud;mobile phishing;password reset;phishing;rate limiting
Slug: 2024-03-27-mfa-fatigue-attack-targets-iphone-owners-with-endless-password-reset-prompts

[Source](https://arstechnica.com/?p=2012822){:target="_blank" rel="noopener"}

> Enlarge / They look like normal notifications, but opening an iPhone with one or more of these stacked up, you won't be able to do much of anything until you tap "Allow" or "Don't Allow." And they're right next to each other. (credit: Kevin Purdy) Human weaknesses are a rich target for phishing attacks. Making humans click "Don't Allow" over and over again in a phone prompt that can't be skipped is an angle some iCloud attackers are taking—and likely having some success. Brian Krebs' at Krebs on Security detailed the attacks in a recent post, noting that "MFA Fatigue [...]
