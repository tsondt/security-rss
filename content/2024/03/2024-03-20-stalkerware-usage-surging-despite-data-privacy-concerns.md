Title: Stalkerware usage surging, despite data privacy concerns
Date: 2024-03-20T13:15:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-20-stalkerware-usage-surging-despite-data-privacy-concerns

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/20/stalkerware_usage_surging_despite_data/){:target="_blank" rel="noopener"}

> At least 31,031 people affected last year Stalkerware has reached "pandemic proportions," according to Kaspersky, which documented a total of 31,031 people affected by the intrusive software in 2023 – up almost six percent on the prior year.... [...]
