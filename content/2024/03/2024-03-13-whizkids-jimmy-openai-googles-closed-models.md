Title: Whizkids jimmy OpenAI, Google's closed models
Date: 2024-03-13T08:34:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-03-13-whizkids-jimmy-openai-googles-closed-models

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/13/researchers_pry_open_closed_models/){:target="_blank" rel="noopener"}

> Infosec folk aren’t thrilled that if you poke APIs enough, you learn AI's secrets Boffins have managed to pry open closed AI services from OpenAI and Google with an attack that recovers an otherwise hidden portion of transformer models.... [...]
