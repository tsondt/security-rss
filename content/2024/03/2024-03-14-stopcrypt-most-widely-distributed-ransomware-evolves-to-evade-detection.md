Title: StopCrypt: Most widely distributed ransomware evolves to evade detection
Date: 2024-03-14T16:59:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-14-stopcrypt-most-widely-distributed-ransomware-evolves-to-evade-detection

[Source](https://www.bleepingcomputer.com/news/security/stopcrypt-most-widely-distributed-ransomware-evolves-to-evade-detection/){:target="_blank" rel="noopener"}

> A new variant of StopCrypt ransomware (aka STOP) was spotted in the wild, employing a multi-stage execution process that involves shellcodes to evade security tools. [...]
