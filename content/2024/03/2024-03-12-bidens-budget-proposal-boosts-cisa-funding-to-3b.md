Title: Biden's budget proposal boosts CISA funding to $3B
Date: 2024-03-12T18:30:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-12-bidens-budget-proposal-boosts-cisa-funding-to-3b

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/12/bidens_budget_proposal_boosts_cisas/){:target="_blank" rel="noopener"}

> Plus almost $1.5b for health-care cybersecurity US President Joe Biden has asked Congress to approve an extra $103 million in funding for the Cybersecurity and Infrastructure Security Agency, bringing CISA's total budget to $3 billion.... [...]
