Title: Serial extortionist of medical facilities pleads guilty to cybercrime charges
Date: 2024-03-20T14:33:05+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-20-serial-extortionist-of-medical-facilities-pleads-guilty-to-cybercrime-charges

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/20/serial_extortionist_of_medical_facilities/){:target="_blank" rel="noopener"}

> Robert Purbeck even went as far as threatening a dentist with the sale of his child’s data A cyberattacker and extortionist of a medical center has pleaded guilty to federal computer fraud and abuse charges in the US.... [...]
