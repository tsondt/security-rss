Title: Using LLMs to Unredact Text
Date: 2024-03-11T11:01:55+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;LLM;machine learning
Slug: 2024-03-11-using-llms-to-unredact-text

[Source](https://www.schneier.com/blog/archives/2024/03/using-llms-to-unredact-text.html){:target="_blank" rel="noopener"}

> Initial results in using LLMs to unredact text based on the size of the individual-word redaction rectangles. This feels like something that a specialized ML system could be trained on. [...]
