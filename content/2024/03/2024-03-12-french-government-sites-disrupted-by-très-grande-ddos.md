Title: French government sites disrupted by <i>très grande</i> DDoS
Date: 2024-03-12T06:26:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-03-12-french-government-sites-disrupted-by-très-grande-ddos

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/12/france_ddos/){:target="_blank" rel="noopener"}

> Russia and Sudan top the list of suspects Several French government websites have been disrupted by a severe distributed denial of service attack.... [...]
