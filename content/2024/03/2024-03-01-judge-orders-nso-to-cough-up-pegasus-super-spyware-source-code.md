Title: Judge orders NSO to cough up Pegasus super-spyware source code
Date: 2024-03-01T21:34:29+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-03-01-judge-orders-nso-to-cough-up-pegasus-super-spyware-source-code

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/01/nso_pegasus_source_code/){:target="_blank" rel="noopener"}

> /* Hope no one ever reads these functions lmao */ NSO Group, the Israel-based maker of super-charged snoopware Pegasus, has been ordered by a federal judge in California to share the source code for "all relevant spyware" with Meta's WhatsApp.... [...]
