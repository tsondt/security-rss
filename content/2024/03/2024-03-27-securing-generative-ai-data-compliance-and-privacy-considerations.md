Title: Securing generative AI: data, compliance, and privacy considerations
Date: 2024-03-27T19:11:52+00:00
Author: Mark Keating
Category: AWS Security
Tags: Amazon Bedrock;Artificial Intelligence;Best Practices;Generative AI;Intermediate (200);Security, Identity, & Compliance;artificial intelligence;Security Blog
Slug: 2024-03-27-securing-generative-ai-data-compliance-and-privacy-considerations

[Source](https://aws.amazon.com/blogs/security/securing-generative-ai-data-compliance-and-privacy-considerations/){:target="_blank" rel="noopener"}

> Generative artificial intelligence (AI) has captured the imagination of organizations and individuals around the world, and many have already adopted it to help improve workforce productivity, transform customer experiences, and more. When you use a generative AI-based service, you should understand how the information that you enter into the application is stored, processed, shared, and [...]
