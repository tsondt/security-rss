Title: Ukraine arrests hackers trying to sell 100 million stolen accounts
Date: 2024-03-19T14:15:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-03-19-ukraine-arrests-hackers-trying-to-sell-100-million-stolen-accounts

[Source](https://www.bleepingcomputer.com/news/security/ukraine-arrests-hackers-trying-to-sell-100-million-stolen-accounts/){:target="_blank" rel="noopener"}

> The Ukrainian cyber police, in collaboration with investigators from the national police (ГУНП), have arrested three individuals who are accused of hijacking over 100 million emails and Instagram accounts worldwide. [...]
