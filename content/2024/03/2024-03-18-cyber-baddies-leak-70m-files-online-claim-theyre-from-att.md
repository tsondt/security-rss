Title: Cyber baddies leak 70M+ files online, claim they're from AT&amp;T
Date: 2024-03-18T16:45:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-18-cyber-baddies-leak-70m-files-online-claim-theyre-from-att

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/18/att_alleged_data_leak/){:target="_blank" rel="noopener"}

> Telco reckons data is old, isn't from its systems More than 70 million records, allegedly stolen from AT&T in 2021, were dumped on a cybercrime forum at the weekend.... [...]
