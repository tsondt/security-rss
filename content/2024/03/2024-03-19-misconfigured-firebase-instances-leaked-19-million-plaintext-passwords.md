Title: Misconfigured Firebase instances leaked 19 million plaintext passwords
Date: 2024-03-19T19:25:29-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-03-19-misconfigured-firebase-instances-leaked-19-million-plaintext-passwords

[Source](https://www.bleepingcomputer.com/news/security/misconfigured-firebase-instances-leaked-19-million-plaintext-passwords/){:target="_blank" rel="noopener"}

> Three cybersecurity researchers discovered close to 19 million plaintext passwords exposed on the public internet by misconfigured instances of Firebase, a Google platform for hosting databases, cloud computing, and app development. [...]
