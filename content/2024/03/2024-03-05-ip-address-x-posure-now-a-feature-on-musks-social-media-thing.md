Title: IP address X-posure now a feature on Musk's social media thing
Date: 2024-03-05T16:18:04+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-03-05-ip-address-x-posure-now-a-feature-on-musks-social-media-thing

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/05/ip_address_xposure_now_a/){:target="_blank" rel="noopener"}

> Just a little FYI Video and audio calling features for X Premium users added last year to Elon Musk's version of Twitter have been expanded to everyone on the platform, and FYI: It may reveal your IP address to those you're nattering away to.... [...]
