Title: GitHub struggles to keep up with automated malicious forks
Date: 2024-03-01T00:45:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-03-01-github-struggles-to-keep-up-with-automated-malicious-forks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/01/github_automated_fork_campaign/){:target="_blank" rel="noopener"}

> Cloned then compromised, bad repos are forked faster than they can be removed A malware distribution campaign that began last May with a handful of malicious software packages uploaded to the Python Package Index (PyPI) has spread to GitHub and expanded to reach at least 100,000 compromised repositories.... [...]
