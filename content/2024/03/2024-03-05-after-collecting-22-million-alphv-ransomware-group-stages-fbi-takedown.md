Title: After collecting $22 million, AlphV ransomware group stages FBI takedown
Date: 2024-03-05T22:28:13+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;alphv;blackcat;exit scam;ransomware
Slug: 2024-03-05-after-collecting-22-million-alphv-ransomware-group-stages-fbi-takedown

[Source](https://arstechnica.com/?p=2008120){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) The ransomware group responsible for hamstringing the prescription drug market for two weeks has suddenly gone dark, just days after receiving a $22 million payment and standing accused of scamming an affiliate out of its share of the loot. The events involve AlphV, a ransomware group also known as BlackCat. Two weeks ago, it took down Change Healthcare, the biggest US health care payment processor, leaving pharmacies, health care providers, and patients scrambling to fill prescriptions for medicines. On Friday, the bitcoin ledger shows, the group received nearly $22 million in cryptocurrency, stoking suspicions the [...]
