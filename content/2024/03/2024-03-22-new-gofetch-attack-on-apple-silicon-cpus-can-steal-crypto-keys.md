Title: New GoFetch attack on Apple Silicon CPUs can steal crypto keys
Date: 2024-03-22T11:01:56-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple
Slug: 2024-03-22-new-gofetch-attack-on-apple-silicon-cpus-can-steal-crypto-keys

[Source](https://www.bleepingcomputer.com/news/security/new-gofetch-attack-on-apple-silicon-cpus-can-steal-crypto-keys/){:target="_blank" rel="noopener"}

> A new side-channel attack called "GoFetch" impacts Apple M1, M2, and M3 processors and can be used to steal secret cryptographic keys from data in the CPU's cache. [...]
