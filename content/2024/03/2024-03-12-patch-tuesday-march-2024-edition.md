Title: Patch Tuesday, March 2024 Edition
Date: 2024-03-12T20:36:33+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Security Tools;Time to Patch;adobe acrobat;Adobe AI Assistant;Adobe Animate;Adobe Bridge;Adobe Experience Manager;Adobe Premier Pro;Automox;ColdFusion 2023 and 2021;CVE-2024-21334;CVE-2024-21390;CVE-2024-21433;CVE-2024-21435;CVE-2024-21437;CVE-2024-23225;CVE-2024-23296;CVE-2024-26170;CVE-2024-26182;Immersive Labs;iOS 16.7.6;iOS 17.4;iPadOS 17.4;Jason Kitka;Kevin Breen;Lightroom;Microsoft Authenticator;Microsoft Azure;Satnam Narang;Tenable
Slug: 2024-03-12-patch-tuesday-march-2024-edition

[Source](https://krebsonsecurity.com/2024/03/patch-tuesday-march-2024-edition/){:target="_blank" rel="noopener"}

> Apple and Microsoft recently released software updates to fix dozens of security holes in their operating systems. Microsoft today patched at least 60 vulnerabilities in its Windows OS. Meanwhile, Apple’s new macOS Sonoma addresses at least 68 security weaknesses, and its latest update for iOS fixes two zero-day flaws. Last week, Apple pushed out an urgent software update to its flagship iOS platform, warning that there were at least two zero-day exploits for vulnerabilities being used in the wild (CVE-2024-23225 and CVE-2024-23296). The security updates are available in iOS 17.4, iPadOS 17.4, and iOS 16.7.6. Apple’s macOS Sonoma 14.4 Security [...]
