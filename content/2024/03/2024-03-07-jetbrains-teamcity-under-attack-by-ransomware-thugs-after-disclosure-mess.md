Title: JetBrains TeamCity under attack by ransomware thugs after disclosure mess
Date: 2024-03-07T16:34:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-07-jetbrains-teamcity-under-attack-by-ransomware-thugs-after-disclosure-mess

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/07/teamcity_exploits_lead_to_ransomware/){:target="_blank" rel="noopener"}

> More than 1,000 servers remain unpatched and vulnerable Security researchers are increasingly seeing active exploit attempts using the latest vulnerabilities in JetBrains' TeamCity that in some cases are leading to ransomware deployment.... [...]
