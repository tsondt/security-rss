Title: US govt probes if ransomware gang stole Change Healthcare data
Date: 2024-03-13T16:16:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-03-13-us-govt-probes-if-ransomware-gang-stole-change-healthcare-data

[Source](https://www.bleepingcomputer.com/news/security/us-govt-probes-if-ransomware-gang-stole-change-healthcare-data/){:target="_blank" rel="noopener"}

> The U.S. Department of Health and Human Services is investigating whether protected health information was stolen in a ransomware attack that hit UnitedHealthcare Group (UHG) subsidiary Optum, which operates the Change Healthcare platform, in late February. [...]
