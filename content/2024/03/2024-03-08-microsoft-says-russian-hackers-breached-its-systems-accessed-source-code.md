Title: Microsoft says Russian hackers breached its systems, accessed source code
Date: 2024-03-08T10:31:11-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-03-08-microsoft-says-russian-hackers-breached-its-systems-accessed-source-code

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-says-russian-hackers-breached-its-systems-accessed-source-code/){:target="_blank" rel="noopener"}

> Microsoft says the Russian 'Midnight Blizzard' hacking group recently accessed some of its internal systems and source code repositories using authentication secrets stolen during a January cyberattack. [...]
