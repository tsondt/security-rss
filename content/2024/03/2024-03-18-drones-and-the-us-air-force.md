Title: Drones and the US Air Force
Date: 2024-03-18T11:03:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;defense;Department of Defense;drones;economics of security;war
Slug: 2024-03-18-drones-and-the-us-air-force

[Source](https://www.schneier.com/blog/archives/2024/03/drones-and-the-us-air-force.html){:target="_blank" rel="noopener"}

> Fascinating analysis of the use of drones on a modern battlefield—that is, Ukraine—and the inability of the US Air Force to react to this change. The F-35A certainly remains an important platform for high-intensity conventional warfare. But the Air Force is planning to buy 1,763 of the aircraft, which will remain in service through the year 2070. These jets, which are wholly unsuited for countering proliferated low-cost enemy drones in the air littoral, present enormous opportunity costs for the service as a whole. In a set of comments posted on LinkedIn last month, defense analyst T.X. Hammes estimated the following. [...]
