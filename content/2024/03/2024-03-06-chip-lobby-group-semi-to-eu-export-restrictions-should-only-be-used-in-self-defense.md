Title: Chip lobby group SEMI to EU: Export restrictions should only be used in self-defense
Date: 2024-03-06T08:23:14+00:00
Author: Tobias Mann
Category: The Register
Tags: 
Slug: 2024-03-06-chip-lobby-group-semi-to-eu-export-restrictions-should-only-be-used-in-self-defense

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/06/chip_semi_eu/){:target="_blank" rel="noopener"}

> Please don't scare away foreign investors - who do you think pays for this stuff? SEMI, an industry association representing 3,000 chip vendors, would really appreciate it if the European Union would back off plans to impose export controls on China, arguing that they should only be used as a "last resort" to protect national security.... [...]
