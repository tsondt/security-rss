Title: ChatGPT side-channel attack has easy fix: Token obfuscation
Date: 2024-03-18T02:31:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-03-18-chatgpt-side-channel-attack-has-easy-fix-token-obfuscation

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/18/chatgpt_sidechannel_attack_has_easy/){:target="_blank" rel="noopener"}

> Also: Roblox-themed infostealer on the prowl, telco insider pleads guilty to swapping SIMs, and some crit vulns Infosec in brief Almost as quickly as a paper came out last week revealing an AI side-channel vulnerability, Cloudflare researchers have figured out how to solve it: just obscure your token size.... [...]
