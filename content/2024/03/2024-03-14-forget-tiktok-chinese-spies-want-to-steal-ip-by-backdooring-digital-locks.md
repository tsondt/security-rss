Title: Forget TikTok – Chinese spies want to steal IP by backdooring digital locks
Date: 2024-03-14T23:35:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-14-forget-tiktok-chinese-spies-want-to-steal-ip-by-backdooring-digital-locks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/14/chinese_espionage_safe_locks/){:target="_blank" rel="noopener"}

> Uncle Sam can use this snooping tool, too, but that's beside the point There's another Chinese-manufactured product – joining the likes of TikTok, cars and semiconductors – that poses a national security risk to Americans: electronic locks, such as those used in safes.... [...]
