Title: Cisco warns of password-spraying attacks targeting VPN services
Date: 2024-03-28T12:37:32-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-28-cisco-warns-of-password-spraying-attacks-targeting-vpn-services

[Source](https://www.bleepingcomputer.com/news/security/cisco-warns-of-password-spraying-attacks-targeting-vpn-services/){:target="_blank" rel="noopener"}

> Cisco has shared a set of recommendations for customers to mitigate password-spraying attacks that have been targeting Remote Access VPN (RAVPN) services configured on Cisco Secure Firewall devices. [...]
