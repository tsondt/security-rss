Title: British Library pushes the cloud button, says legacy IT estate cause of hefty rebuild
Date: 2024-03-11T13:30:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-11-british-library-pushes-the-cloud-button-says-legacy-it-estate-cause-of-hefty-rebuild

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/11/british_library_slaps_the_cloud/){:target="_blank" rel="noopener"}

> Five months in and the mammoth post-ransomware recovery has barely begun The British Library says legacy IT is the overwhelming factor delaying efforts to recover from the Rhysida ransomware attack in late 2023.... [...]
