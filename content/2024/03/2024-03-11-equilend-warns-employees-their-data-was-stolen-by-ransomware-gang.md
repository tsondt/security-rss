Title: Equilend warns employees their data was stolen by ransomware gang
Date: 2024-03-11T14:00:02-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-03-11-equilend-warns-employees-their-data-was-stolen-by-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/equilend-warns-employees-their-data-was-stolen-by-ransomware-gang/){:target="_blank" rel="noopener"}

> New York-based securities lending platform EquiLend Holdings confirmed in data breach notification letters sent to employees that their data was stolen in a January ransomware attack. [...]
