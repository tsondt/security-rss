Title: Apple fans deluged with phony password reset requests
Date: 2024-03-27T22:06:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-03-27-apple-fans-deluged-with-phony-password-reset-requests

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/27/apple_passcode_attack/){:target="_blank" rel="noopener"}

> Beware support calls offering a fix Apple device owners, consider yourselves warned: a targeted multi-factor authentication bombing campaign is under way, with the goal of exhausting iUsers into allowing an unwanted password reset.... [...]
