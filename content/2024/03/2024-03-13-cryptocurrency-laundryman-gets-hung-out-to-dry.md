Title: Cryptocurrency laundryman gets hung out to dry
Date: 2024-03-13T16:45:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-13-cryptocurrency-laundryman-gets-hung-out-to-dry

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/13/bitcoin_fog_conviction/){:target="_blank" rel="noopener"}

> Bitcoin Fog washed hundreds of millions for criminals The operator of the world's longest-running Bitcoin money laundering service faces a 50-year prison sentence after being found guilty in a US court.... [...]
