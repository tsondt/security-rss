Title: Nissan to let 100,000 Aussies and Kiwis know their data was stolen in cyberattack
Date: 2024-03-14T00:32:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-14-nissan-to-let-100000-aussies-and-kiwis-know-their-data-was-stolen-in-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/14/nissan_oceania_100k_affected/){:target="_blank" rel="noopener"}

> Akira ransomware crooks brag of swiping thousands of ID documents during break-in Over the next few weeks, Nissan Oceania will make contact with around 100,000 people in Australia and New Zealand whose data was pilfered in a December 2023 attack on its systems – perhaps by the Akira ransomware gang.... [...]
