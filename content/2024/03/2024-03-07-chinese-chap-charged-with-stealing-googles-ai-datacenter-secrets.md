Title: Chinese chap charged with stealing Google’s AI datacenter secrets
Date: 2024-03-07T00:37:23+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-03-07-chinese-chap-charged-with-stealing-googles-ai-datacenter-secrets

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/07/doj_google_ai_theft_indictment/){:target="_blank" rel="noopener"}

> Moonlighted for two PRC companies after side-stepping Big G's security The US Department of Justice on Wednesday revealed an indictment that charges a former Google employee with leaking the ad giant’s AI tech to two Chinese companies – after easily defeating the Big G’s security controls.... [...]
