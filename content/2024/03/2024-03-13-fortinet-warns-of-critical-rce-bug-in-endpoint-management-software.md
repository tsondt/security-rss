Title: Fortinet warns of critical RCE bug in endpoint management software
Date: 2024-03-13T14:48:11-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-03-13-fortinet-warns-of-critical-rce-bug-in-endpoint-management-software

[Source](https://www.bleepingcomputer.com/news/security/fortinet-warns-of-critical-rce-bug-in-endpoint-management-software/){:target="_blank" rel="noopener"}

> Fortinet patched a critical vulnerability in its FortiClient Enterprise Management Server (EMS) software that can allow attackers to gain remote code execution (RCE) on vulnerable servers. [...]
