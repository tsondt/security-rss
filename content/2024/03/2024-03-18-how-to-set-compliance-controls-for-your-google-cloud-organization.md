Title: How to set compliance controls for your Google Cloud Organization
Date: 2024-03-18T16:00:00+00:00
Author: Anil Nandigam
Category: GCP Security
Tags: Public Sector;Healthcare & Life Sciences;Security & Identity
Slug: 2024-03-18-how-to-set-compliance-controls-for-your-google-cloud-organization

[Source](https://cloud.google.com/blog/products/identity-security/how-to-set-compliance-controls-for-your-google-cloud-organization/){:target="_blank" rel="noopener"}

> Assured Workloads is a modern cloud solution that allows companies to more easily run regulated workloads in many of Google Cloud’s global regions. Assured Workloads can help you ensure comprehensive data protection and regulatory compliance across your Google Cloud Organization. It allows you to apply specific security and compliance controls to a folder in support of your compliance requirements. Assured Workloads supports many compliance programs to create regulated boundaries in Google Cloud. Assured Workloads in action Many companies have requirements to meet multiple global compliance standards. For example, if your company must adhere to compliance requirements in more than one [...]
