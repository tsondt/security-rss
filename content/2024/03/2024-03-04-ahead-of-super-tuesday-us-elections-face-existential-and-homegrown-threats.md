Title: Ahead of Super Tuesday, US elections face existential and homegrown threats
Date: 2024-03-04T01:15:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-04-ahead-of-super-tuesday-us-elections-face-existential-and-homegrown-threats

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/04/super_tuesday_election_security/){:target="_blank" rel="noopener"}

> Misinformation is rife, AI makes it easier to create, and 42 percent of the planet’s inhabitants get to vote this year Feature Two US intelligence bigwigs last week issued stark warnings about foreign threats to American election integrity and security – and the nation's ability to counter these adversaries.... [...]
