Title: How do you lot feel about Pay or say OK to ads model, asks ICO
Date: 2024-03-11T11:16:15+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-03-11-how-do-you-lot-feel-about-pay-or-say-ok-to-ads-model-asks-ico

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/11/ico_pay_or_ads/){:target="_blank" rel="noopener"}

> And does it count as consent? The UK's Information Commissioner's Office (ICO) has opened a consultation on "consent or pay" business models. We're sure readers of The Register will have a fair few things to say.... [...]
