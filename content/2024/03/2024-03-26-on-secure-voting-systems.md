Title: On Secure Voting Systems
Date: 2024-03-26T11:08:16+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;reports;voting
Slug: 2024-03-26-on-secure-voting-systems

[Source](https://www.schneier.com/blog/archives/2024/03/on-secure-voting-systems.html){:target="_blank" rel="noopener"}

> Andrew Appel shepherded a public comment —signed by twenty election cybersecurity experts, including myself—on best practices for ballot marking devices and vote tabulation. It was written for the Pennsylvania legislature, but it’s general in nature. From the executive summary: We believe that no system is perfect, with each having trade-offs. Hand-marked and hand-counted ballots remove the uncertainty introduced by use of electronic machinery and the ability of bad actors to exploit electronic vulnerabilities to remotely alter the results. However, some portion of voters mistakenly mark paper ballots in a manner that will not be counted in the way the voter [...]
