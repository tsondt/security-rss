Title: Avoid high cyber insurance costs by improving Active Directory security
Date: 2024-03-19T10:02:04-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2024-03-19-avoid-high-cyber-insurance-costs-by-improving-active-directory-security

[Source](https://www.bleepingcomputer.com/news/security/avoid-high-cyber-insurance-costs-by-improving-active-directory-security/){:target="_blank" rel="noopener"}

> With the growing number of data breaches and cyberattacks, insurance premiums are increasing. Learn more from Specops Software about how securing an Activity Directory could lead to lower cyber insurance premiums. [...]
