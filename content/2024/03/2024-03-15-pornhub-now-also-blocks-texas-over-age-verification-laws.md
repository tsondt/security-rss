Title: PornHub now also blocks Texas over age verification laws
Date: 2024-03-15T13:11:49-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Government;Technology
Slug: 2024-03-15-pornhub-now-also-blocks-texas-over-age-verification-laws

[Source](https://www.bleepingcomputer.com/news/security/pornhub-now-also-blocks-texas-over-age-verification-laws/){:target="_blank" rel="noopener"}

> PornHub has now added Texas to its blocklist, preventing users in the state from accessing its site in protest of age verification laws. [...]
