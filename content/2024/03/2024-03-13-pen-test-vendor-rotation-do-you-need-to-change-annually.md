Title: Pen test vendor rotation: do you need to change annually?
Date: 2024-03-13T10:02:04-04:00
Author: Sponsored by Outpost24
Category: BleepingComputer
Tags: Security
Slug: 2024-03-13-pen-test-vendor-rotation-do-you-need-to-change-annually

[Source](https://www.bleepingcomputer.com/news/security/pen-test-vendor-rotation-do-you-need-to-change-annually/){:target="_blank" rel="noopener"}

> Organizations commonly change their pen test providers annually. Learn more from Outpost24 about the drawbacks of rotating pentest providers and the benefits of the Penetration Testing as a Service (PTaaS) model. [...]
