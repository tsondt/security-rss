Title: CEO of Data Privacy Company Onerep.com Founded Dozens of People-Search Firms
Date: 2024-03-14T21:13:38+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;375-292-7027-786;ahavoila.com;azersab.com;Constella Intelligence;constella.ai;d.sh@nuwber.com;Dimitri Shelest;dmitrcox@gmail.com;findita.com;findmedo.com;folkscan.com;huntize.com;ifindy.com;jupery.com;look2man.com;lookerun.com;manyp.com;nuwber.at;nuwber.ch;nuwber.dk;nuwber.fr;onerep.com;peeepl.br.com;peeepl.co.uk;peeepl.in;peeepl.it;peepull.com;Permanente Medicine;perserch.com;persuer.com;pervent.com;piplenter.com;piplfind.com;piplscan.com;popopke.com;pplcrwlr.dk;pplcrwlr.fr;pplcrwlr.in;pplcrwlr.jp;pplsorce.com;qimeo.com;scoutu2.com;search64.com;searchay.com;seekmi.com;selfabc.com;socsee.com;srching.com;toolooks.com;upearch.com;viadin.ca;viadin.com;viadin.de;viadin.hk;waatp1.fr;waatpp.de;webmeek.com
Slug: 2024-03-14-ceo-of-data-privacy-company-onerepcom-founded-dozens-of-people-search-firms

[Source](https://krebsonsecurity.com/2024/03/ceo-of-data-privacy-company-onerep-com-founded-dozens-of-people-search-firms/){:target="_blank" rel="noopener"}

> The data privacy company Onerep.com bills itself as a Virginia-based service for helping people remove their personal information from almost 200 people-search websites. However, an investigation into the history of onerep.com finds this company is operating out of Belarus and Cyprus, and that its founder has launched dozens of people-search services over the years. Onerep’s “Protect” service starts at $8.33 per month for individuals and $15/mo for families, and promises to remove your personal information from nearly 200 people-search sites. Onerep also markets its service to companies seeking to offer their employees the ability to have their data continuously removed [...]
