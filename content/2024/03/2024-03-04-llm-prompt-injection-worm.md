Title: LLM Prompt Injection Worm
Date: 2024-03-04T12:01:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence;LLM;malware
Slug: 2024-03-04-llm-prompt-injection-worm

[Source](https://www.schneier.com/blog/archives/2024/03/llm-prompt-injection-worm.html){:target="_blank" rel="noopener"}

> Researchers have demonstrated a worm that spreads through prompt injection. Details : In one instance, the researchers, acting as attackers, wrote an email including the adversarial text prompt, which “poisons” the database of an email assistant using retrieval-augmented generation (RAG), a way for LLMs to pull in extra data from outside its system. When the email is retrieved by the RAG, in response to a user query, and is sent to GPT-4 or Gemini Pro to create an answer, it “jailbreaks the GenAI service” and ultimately steals data from the emails, Nassi says. “The generated response containing the sensitive user [...]
