Title: Microsoft faces bipartisan criticism for alleged censorship on Bing in China
Date: 2024-03-21T21:25:13+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2024-03-21-microsoft-faces-bipartisan-criticism-for-alleged-censorship-on-bing-in-china

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/21/microsoft_bing_china_criticism/){:target="_blank" rel="noopener"}

> Redmond says it does what it's told, but still thinks users are better off Microsoft is the subject of growing criticism in the US over allegations that its Bing search engine censors results for users in China that relate to sensitive subjects the state wants blocked.... [...]
