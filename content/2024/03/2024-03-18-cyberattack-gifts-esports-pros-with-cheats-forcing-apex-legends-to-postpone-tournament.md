Title: Cyberattack gifts esports pros with cheats, forcing Apex Legends to postpone tournament
Date: 2024-03-18T13:15:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-18-cyberattack-gifts-esports-pros-with-cheats-forcing-apex-legends-to-postpone-tournament

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/18/cyberattack_gifts_esports_pros_with/){:target="_blank" rel="noopener"}

> Virtual gunslingers forcibly became cheaters via mystery means Updated Esports pros competing in the Apex Legends Global Series (ALGS) Pro League tournament were forced to abandon their match today due to a suspected cyberattack.... [...]
