Title: PyPI halted new users and projects while it fended off supply-chain attack
Date: 2024-03-28T18:50:22+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;malware;open source;pypi;repositories
Slug: 2024-03-28-pypi-halted-new-users-and-projects-while-it-fended-off-supply-chain-attack

[Source](https://arstechnica.com/?p=2013233){:target="_blank" rel="noopener"}

> Enlarge / Supply-chain attacks, like the latest PyPI discovery, insert malicious code into seemingly functional software packages used by developers. They're becoming increasingly common. (credit: Getty Images) PyPI, a vital repository for open source developers, temporarily halted new project creation and new user registration following an onslaught of package uploads that executed malicious code on any device that installed them. Ten hours later, it lifted the suspension. Short for the Python Package Index, PyPI is the go-to source for apps and code libraries written in the Python programming language. Fortune 500 corporations and independent developers alike rely on the repository [...]
