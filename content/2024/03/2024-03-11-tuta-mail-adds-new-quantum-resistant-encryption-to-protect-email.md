Title: Tuta Mail adds new quantum-resistant encryption to protect email
Date: 2024-03-11T17:21:42-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2024-03-11-tuta-mail-adds-new-quantum-resistant-encryption-to-protect-email

[Source](https://www.bleepingcomputer.com/news/security/tuta-mail-adds-new-quantum-resistant-encryption-to-protect-email/){:target="_blank" rel="noopener"}

> Tuta Mail has announced TutaCrypt, a new post-quantum encryption protocol to secure communications from powerful and anticipated decryption attacks. [...]
