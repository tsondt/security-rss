Title: NetRise elevates their user experience with Google Cloud
Date: 2024-03-20T16:00:00+00:00
Author: Michael Scott
Category: GCP Security
Tags: Partners;Security & Identity;Cloud SQL;Databases
Slug: 2024-03-20-netrise-elevates-their-user-experience-with-google-cloud

[Source](https://cloud.google.com/blog/products/databases/netrise-traces-supply-chain-security-with-google-managed-services/){:target="_blank" rel="noopener"}

> Editor’s note : NetRise, a cybersecurity company, has developed a platform to address software supply chain vulnerabilities, especially within the Extended Internet of Things (XIoT) and Cyber Physical Systems (CPS). Its latest solution, Trace, utilizes large language models and Cloud SQL for PostgreSQL for efficient vulnerability detection and code-origin tracing. By integrating with Google Cloud's fully managed services, NetRise has reduced processing times and strengthened scalability. The partnership between NetRise and Google Cloud not only offers improved security evaluation but also promises to shape future product security practices across the industry. NetRise ’s platform can empower users to identify risks [...]
