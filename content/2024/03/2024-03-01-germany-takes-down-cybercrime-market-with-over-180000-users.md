Title: Germany takes down cybercrime market with over 180,000 users
Date: 2024-03-01T11:45:55-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;Security
Slug: 2024-03-01-germany-takes-down-cybercrime-market-with-over-180000-users

[Source](https://www.bleepingcomputer.com/news/legal/germany-takes-down-cybercrime-market-with-over-180-000-users/){:target="_blank" rel="noopener"}

> The Düsseldorf Police in Germany have seized Crimemarket, a massive German-speaking illicit trading platform with over 180,000 users, arresting six people, including one of its operators. [...]
