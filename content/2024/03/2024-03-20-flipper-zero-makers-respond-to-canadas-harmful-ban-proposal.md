Title: Flipper Zero makers respond to Canada’s ‘harmful’ ban proposal
Date: 2024-03-20T09:48:21-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2024-03-20-flipper-zero-makers-respond-to-canadas-harmful-ban-proposal

[Source](https://www.bleepingcomputer.com/news/security/flipper-zero-makers-respond-to-canadas-harmful-ban-proposal/){:target="_blank" rel="noopener"}

> The makers of Flipper Zero have responded to the Canadian government's plan to ban the device in the country, arguing that it is wrongfully accused of facilitating car thefts. [...]
