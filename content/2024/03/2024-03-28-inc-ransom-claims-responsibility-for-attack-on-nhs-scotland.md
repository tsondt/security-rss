Title: INC Ransom claims responsibility for attack on NHS Scotland
Date: 2024-03-28T10:27:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-28-inc-ransom-claims-responsibility-for-attack-on-nhs-scotland

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/28/nhs_scotland_cyberattack/){:target="_blank" rel="noopener"}

> Sensitive documents dumped on leak site amid claims of 3 TB of data stolen in total NHS Scotland says it managed to contain a ransomware group's malware to a regional branch, preventing the spread of infection across the entire institution.... [...]
