Title: VMware fixes critical sandbox escape flaws in ESXi, Workstation, and Fusion
Date: 2024-03-06T10:39:22-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-06-vmware-fixes-critical-sandbox-escape-flaws-in-esxi-workstation-and-fusion

[Source](https://www.bleepingcomputer.com/news/security/vmware-fixes-critical-sandbox-escape-flaws-in-esxi-workstation-and-fusion/){:target="_blank" rel="noopener"}

> VMware released security updates to fix critical sandbox escape vulnerabilities in VMware ESXi, Workstation, Fusion, and Cloud Foundation products, allowing attackers to escape virtual machines and access the host operating system. [...]
