Title: Duvel says it has "more than enough" beer after ransomware attack
Date: 2024-03-06T13:15:13-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-06-duvel-says-it-has-more-than-enough-beer-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/duvel-says-it-has-more-than-enough-beer-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Duvel Moortgat Brewery was hit by a ransomware attack late last night, bringing to a halt the beer production in the company's bottling facilities [...]
