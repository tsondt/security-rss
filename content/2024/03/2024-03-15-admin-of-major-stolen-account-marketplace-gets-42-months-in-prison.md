Title: Admin of major stolen account marketplace gets 42 months in prison
Date: 2024-03-15T12:07:37-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-03-15-admin-of-major-stolen-account-marketplace-gets-42-months-in-prison

[Source](https://www.bleepingcomputer.com/news/security/admin-of-major-stolen-account-marketplace-gets-42-months-in-prison/){:target="_blank" rel="noopener"}

> Moldovan national Sandu Boris Diaconu has been sentenced to 42 months in prison for operating E-Root, a major online marketplace that sold access to hacked computers worldwide. [...]
