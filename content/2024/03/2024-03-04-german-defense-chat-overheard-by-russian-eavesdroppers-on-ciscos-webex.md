Title: German defense chat overheard by Russian eavesdroppers on Cisco's WebEx
Date: 2024-03-04T17:45:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-04-german-defense-chat-overheard-by-russian-eavesdroppers-on-ciscos-webex

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/04/germany_confirms_russia_leak_genuine/){:target="_blank" rel="noopener"}

> Officials can't tell whether the tape was edited, but fear Kremlin has more juicy bits to release in the future The German Ministry of Defense (Bundeswehr) has confirmed that a recording of a call between high-ranking officials discussing war efforts in Ukraine, leaked by Russian media, is legitimate.... [...]
