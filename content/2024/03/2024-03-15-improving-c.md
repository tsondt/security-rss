Title: Improving C++
Date: 2024-03-15T11:05:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;programming
Slug: 2024-03-15-improving-c

[Source](https://www.schneier.com/blog/archives/2024/03/improving-c.html){:target="_blank" rel="noopener"}

> C++ guru Herb Sutter writes about how we can improve the programming language for better security. The immediate problem “is” that it’s Too Easy By DefaultTM to write security and safety vulnerabilities in C++ that would have been caught by stricter enforcement of known rules for type, bounds, initialization, and lifetime language safety. His conclusion: We need to improve software security and software safety across the industry, especially by improving programming language safety in C and C++, and in C++ a 98% improvement in the four most common problem areas is achievable in the medium term. But if we focus [...]
