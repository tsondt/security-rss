Title: LockBit ransomware affiliate gets four years in jail, to pay $860k
Date: 2024-03-13T07:42:50-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-03-13-lockbit-ransomware-affiliate-gets-four-years-in-jail-to-pay-860k

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-affiliate-gets-four-years-in-jail-to-pay-860k/){:target="_blank" rel="noopener"}

> Russian-Canadian cybercriminal Mikhail Vasiliev has been sentenced to four years in prison by an Ontario court for his involvement in the LockBit ransomware operation. [...]
