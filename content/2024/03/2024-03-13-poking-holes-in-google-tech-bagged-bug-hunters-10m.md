Title: Poking holes in Google tech bagged bug hunters $10M
Date: 2024-03-13T18:00:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-03-13-poking-holes-in-google-tech-bagged-bug-hunters-10m

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/13/google_2023_bug_bounties/){:target="_blank" rel="noopener"}

> A $2M drop from previous year. So... things are more secure? Google awarded $10 million to 632 bug hunters last year through its vulnerability reward programs.... [...]
