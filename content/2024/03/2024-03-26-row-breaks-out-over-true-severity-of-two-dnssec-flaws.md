Title: Row breaks out over true severity of two DNSSEC flaws
Date: 2024-03-26T08:24:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-03-26-row-breaks-out-over-true-severity-of-two-dnssec-flaws

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/26/software_risk_scores/){:target="_blank" rel="noopener"}

> Some of us would be happy being rated 7.5 out of 10, just sayin' Updated Two DNSSEC vulnerabilities were disclosed last month with similar descriptions and the same severity score, but they are not the same issue.... [...]
