Title: Securing generative AI: Applying relevant security controls
Date: 2024-03-19T17:30:20+00:00
Author: Maitreya Ranganath
Category: AWS Security
Tags: Advanced (300);Amazon Bedrock;Artificial Intelligence;Best Practices;Generative AI;Security, Identity, & Compliance;artificial intelligence;Security Blog
Slug: 2024-03-19-securing-generative-ai-applying-relevant-security-controls

[Source](https://aws.amazon.com/blogs/security/securing-generative-ai-applying-relevant-security-controls/){:target="_blank" rel="noopener"}

> This is part 3 of a series of posts on securing generative AI. We recommend starting with the overview post Securing generative AI: An introduction to the Generative AI Security Scoping Matrix, which introduces the scoping matrix detailed in this post. This post discusses the considerations when implementing security controls to protect a generative AI [...]
