Title: New ‘Loop DoS’ attack may impact up to 300,000 online systems
Date: 2024-03-20T15:40:54-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-03-20-new-loop-dos-attack-may-impact-up-to-300000-online-systems

[Source](https://www.bleepingcomputer.com/news/security/new-loop-dos-attack-may-impact-up-to-300-000-online-systems/){:target="_blank" rel="noopener"}

> A new denial-of-service attack dubbed 'Loop DoS' targeting application layer protocols can pair network services into an indefinite communication loop that creates large volumes of traffic. [...]
