Title: How Commerzbank safeguards its data with VPC Service Controls
Date: 2024-03-20T16:00:00+00:00
Author: Sriram Balasubramanian
Category: GCP Security
Tags: Security & Identity;Customers
Slug: 2024-03-20-how-commerzbank-safeguards-its-data-with-vpc-service-controls

[Source](https://cloud.google.com/blog/topics/customers/how-commerzbank-safeguards-its-data-with-vpc-service-controls/){:target="_blank" rel="noopener"}

> Google Cloud’s VPC Service Controls (VPC-SC) can help enterprises keep their sensitive data secure while using built-in storage and data processing capabilities. Since its inception, VPC-SC has been deployed as a foundational security control by many Google Cloud customers. As an integral part of a defense-in-depth solution, VPC-SC can play a crucial role in helping prevent data exfiltration from Google Cloud due to insider threats or credential compromise. How does it work? VPC-SC allows Google Cloud customers to create isolation perimeters around their managed cloud resources and networks. Once an isolation perimeter is established, access to managed resources across the [...]
