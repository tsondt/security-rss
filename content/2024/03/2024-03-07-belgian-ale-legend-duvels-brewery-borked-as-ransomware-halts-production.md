Title: Belgian ale legend Duvel's brewery borked as ransomware halts production
Date: 2024-03-07T12:45:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-07-belgian-ale-legend-duvels-brewery-borked-as-ransomware-halts-production

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/07/no_piss_up_in_duvels/){:target="_blank" rel="noopener"}

> Biz reassures quaffers it has enough beer, expects quick recovery before weekend Belgian beer brewer Duvel says a ransomware attack has brought its facility to a standstill while its IT team works to remediate the damage.... [...]
