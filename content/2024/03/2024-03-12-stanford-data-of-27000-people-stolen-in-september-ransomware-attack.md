Title: Stanford: Data of 27,000 people stolen in September ransomware attack
Date: 2024-03-12T15:49:35-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-03-12-stanford-data-of-27000-people-stolen-in-september-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/stanford-data-of-27-000-people-stolen-in-september-ransomware-attack/){:target="_blank" rel="noopener"}

> Stanford University says the personal information of 27,000 individuals was stolen in a ransomware attack impacting its Department of Public Safety (SUDPS) network. [...]
