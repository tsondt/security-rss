Title: Apple's trademark tight lips extend to new iPhone, iPad zero-days
Date: 2024-03-06T17:01:43+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-06-apples-trademark-tight-lips-extend-to-new-iphone-ipad-zero-days

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/06/iphone_ipad_zero_days/){:target="_blank" rel="noopener"}

> Two flaws fixed, one knee bent to the EU, and a budding cybersecurity star feature in iOS 17.4 Apple's latest security patches address four vulnerabilities affecting iOS and iPadOS, including two zero-days that intel suggests attackers have already exploited.... [...]
