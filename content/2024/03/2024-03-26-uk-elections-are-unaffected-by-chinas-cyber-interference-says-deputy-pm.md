Title: UK elections are unaffected by China's cyber-interference, says deputy PM
Date: 2024-03-26T09:30:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-03-26-uk-elections-are-unaffected-by-chinas-cyber-interference-says-deputy-pm

[Source](https://go.theregister.com/feed/www.theregister.com/2024/03/26/uk_elections_are_unaffected_by/){:target="_blank" rel="noopener"}

> Sanctions galore for APT31, which has been blamed for two major attacks on democracy The UK's deputy prime minister, Oliver Dowden, says China has been unsuccessful in its attempts to undermine UK elections.... [...]
