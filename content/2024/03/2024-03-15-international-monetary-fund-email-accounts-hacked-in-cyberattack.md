Title: International Monetary Fund email accounts hacked in cyberattack
Date: 2024-03-15T15:48:22-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-03-15-international-monetary-fund-email-accounts-hacked-in-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/international-monetary-fund-email-accounts-hacked-in-cyberattack/){:target="_blank" rel="noopener"}

> The International Monetary Fund (IMF) disclosed a cyber incident on Friday after unknown attackers breached 11 IMF email accounts earlier this year. [...]
