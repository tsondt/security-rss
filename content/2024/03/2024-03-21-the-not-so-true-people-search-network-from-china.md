Title: The Not-so-True People-Search Network from China
Date: 2024-03-21T03:18:26+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Alibaba Cloud;Alina Clark;BeenVerified;CocoDoc;Cocofinder;CocoSign;DomainTools.com;Eden Cheng;FastPeopleSearch;FindPeopleFast;Forbes.com;H.I.G. Capital;Harriet Chan;Instant Checkmate;Intelius;Marilyn Gaskell;NeighborWho;NumberGuru;OneRep;Ownerly;PeopleConnect Inc.;PeopleFinderFree;PeopleLooker;PeopleSmart;Radaris;Ross Cohen;Sally Stevens;Shenzhen Duiyun Technology Co;Spokeo;Stephen Curry;The Lifetime Value Co.;TruePeopleSearch;TruthFinder;U.S. Federal Trade Commission
Slug: 2024-03-21-the-not-so-true-people-search-network-from-china

[Source](https://krebsonsecurity.com/2024/03/the-not-so-true-people-search-network-from-china/){:target="_blank" rel="noopener"}

> It’s not unusual for the data brokers behind people-search websites to use pseudonyms in their day-to-day lives (you would, too). Some of these personal data purveyors even try to reinvent their online identities in a bid to hide their conflicts of interest. But it’s not every day you run across a US-focused people-search network based in China whose principal owners all appear to be completely fabricated identities. Responding to a reader inquiry concerning the trustworthiness of a site called TruePeopleSearch[.]net, KrebsOnSecurity began poking around. The site offers to sell reports containing photos, police records, background checks, civil judgments, contact information [...]
