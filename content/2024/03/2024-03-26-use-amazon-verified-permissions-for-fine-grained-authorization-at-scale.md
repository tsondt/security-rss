Title: Use Amazon Verified Permissions for fine-grained authorization at scale
Date: 2024-03-26T16:51:38+00:00
Author: Abhishek Panday
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;Amazon Verified Permissions;Security;Security Blog
Slug: 2024-03-26-use-amazon-verified-permissions-for-fine-grained-authorization-at-scale

[Source](https://aws.amazon.com/blogs/security/use-amazon-verified-permissions-for-fine-grained-authorization-at-scale/){:target="_blank" rel="noopener"}

> Implementing user authentication and authorization for custom applications requires significant effort. For authentication, customers often use an external identity provider (IdP) such as Amazon Cognito. Yet, authorization logic is typically implemented in code. This code can be prone to errors, especially as permissions models become complex, and presents significant challenges when auditing permissions and deciding [...]
