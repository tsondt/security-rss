Title: The Week in Ransomware - May 10th 2024 - Chipping away at LockBit
Date: 2024-05-10T18:01:42-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-05-10-the-week-in-ransomware-may-10th-2024-chipping-away-at-lockbit

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-may-10th-2024-chipping-away-at-lockbit/){:target="_blank" rel="noopener"}

> After many months of taunting law enforcement and offering a million-dollar reward to anyone who could reveal his identity, the FBI and NCA have done just that, revealing the name of LockBitSupp, the operator of the LockBit ransomware operation. [...]
