Title: Snowflake denies miscreants melted its security to steal data from top customers
Date: 2024-05-31T21:44:08+00:00
Author: Tobias Mann
Category: The Register
Tags: 
Slug: 2024-05-31-snowflake-denies-miscreants-melted-its-security-to-steal-data-from-top-customers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/31/snowflake_breach_report/){:target="_blank" rel="noopener"}

> Infosec house claims Ticketmaster, Santander hit via cloud storage Infosec analysts at Hudson Rock believe Snowflake was compromised by miscreants who used that intrusion to steal data on hundreds of millions of people from Ticketmaster, Santander, and potentially other customers of the cloud storage provider. Snowflake denies its security was defeated.... [...]
