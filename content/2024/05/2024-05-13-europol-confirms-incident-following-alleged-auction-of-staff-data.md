Title: Europol confirms incident following alleged auction of staff data
Date: 2024-05-13T11:45:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-13-europol-confirms-incident-following-alleged-auction-of-staff-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/13/europol_data_breach/){:target="_blank" rel="noopener"}

> Intelligence-sharing platform remains down for maintenance Europol is investigating a cybercriminal's claims that they stole confidential data from a number of the agency's sources.... [...]
