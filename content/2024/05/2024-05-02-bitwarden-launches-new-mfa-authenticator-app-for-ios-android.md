Title: Bitwarden launches new MFA Authenticator app for iOS, Android
Date: 2024-05-02T16:20:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Software;Security
Slug: 2024-05-02-bitwarden-launches-new-mfa-authenticator-app-for-ios-android

[Source](https://www.bleepingcomputer.com/news/software/bitwarden-launches-new-mfa-authenticator-app-for-ios-android/){:target="_blank" rel="noopener"}

> Bitwarden, the creator of the popular open-source password manager, has just launched a new authenticator app called Bitwarden Authenticator, which is available for iOS and Android devices. [...]
