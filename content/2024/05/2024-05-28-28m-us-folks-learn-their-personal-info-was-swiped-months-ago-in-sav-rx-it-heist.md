Title: 2.8M US folks learn their personal info was swiped months ago in Sav-Rx IT heist
Date: 2024-05-28T22:20:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-28-28m-us-folks-learn-their-personal-info-was-swiped-months-ago-in-sav-rx-it-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/28/savrx_data_theft/){:target="_blank" rel="noopener"}

> Theft happened in October, only now are details coming to light Sav-Rx has started notifying about 2.8 million people that their personal information was likely stolen during an IT intrusion that happened more than seven months ago.... [...]
