Title: Microsoft to start killing off VBScript in second half of 2024
Date: 2024-05-22T14:34:38-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-05-22-microsoft-to-start-killing-off-vbscript-in-second-half-of-2024

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-to-start-killing-off-vbscript-in-second-half-of-2024/){:target="_blank" rel="noopener"}

> Microsoft announced today that it will start deprecating VBScript in the second half of 2024 by making it an on-demand feature until it's completely removed. [...]
