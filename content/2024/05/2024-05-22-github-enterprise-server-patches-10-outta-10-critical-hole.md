Title: GitHub Enterprise Server patches 10-outta-10 critical hole
Date: 2024-05-22T07:31:09+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-05-22-github-enterprise-server-patches-10-outta-10-critical-hole

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/22/github_enterprise_server_patch/){:target="_blank" rel="noopener"}

> On the bright side, someone made up to $30,000+ for finding it GitHub has patched its Enterprise Server software to fix a security flaw that scored a 10 out of 10 CVSS severity score.... [...]
