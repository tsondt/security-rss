Title: Indian stock exchange finally encrypting all messages to traders
Date: 2024-05-30T05:36:33+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-05-30-indian-stock-exchange-finally-encrypting-all-messages-to-traders

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/30/bse_eti_encryption/){:target="_blank" rel="noopener"}

> Requests for pricing will soon be encrypted, after implementation deadline was extended India's Bombay Stock Exchange (BSE) has told market participants they need to adopt encryption – which, shockingly, isn't already implemented – for certain messages sent to its trading platforms when using its Enhanced Trading Interface (ETI).... [...]
