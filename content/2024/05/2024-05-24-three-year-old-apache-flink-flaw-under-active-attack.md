Title: Three-year-old Apache Flink flaw under active attack
Date: 2024-05-24T00:59:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-24-three-year-old-apache-flink-flaw-under-active-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/24/apache_flink_flaw_cisa/){:target="_blank" rel="noopener"}

> We know IT admins have busy schedules but c'mon An improper access control bug in Apache Flink that was fixed in January 2021 has been added to the US government's Known Exploited Vulnerabilities Catalog, meaning criminals are right now abusing the flaw in the wild to compromise targets.... [...]
