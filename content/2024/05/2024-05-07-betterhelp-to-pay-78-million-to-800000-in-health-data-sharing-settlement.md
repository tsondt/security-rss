Title: BetterHelp to pay $7.8 million to 800,000 in health data sharing settlement
Date: 2024-05-07T12:44:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-07-betterhelp-to-pay-78-million-to-800000-in-health-data-sharing-settlement

[Source](https://www.bleepingcomputer.com/news/security/betterhelp-to-pay-78-million-to-800-000-in-health-data-sharing-settlement/){:target="_blank" rel="noopener"}

> BetterHelp has agreed to pay $7.8 million in a settlement agreement with the U.S. Federal Trade Commission (FTC) over allegations of misusing and sharing consumer health data for advertising purposes. [...]
