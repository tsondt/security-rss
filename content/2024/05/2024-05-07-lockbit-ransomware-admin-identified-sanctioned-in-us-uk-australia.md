Title: LockBit ransomware admin identified, sanctioned in US, UK, Australia
Date: 2024-05-07T10:04:28-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-05-07-lockbit-ransomware-admin-identified-sanctioned-in-us-uk-australia

[Source](https://www.bleepingcomputer.com/news/security/lockbit-ransomware-admin-identified-sanctioned-in-us-uk-australia/){:target="_blank" rel="noopener"}

> The FBI, UK National Crime Agency, and Europol have unveiled sweeping indictments and sanctions against the admin of the LockBit ransomware operation, with the identity of the Russian threat actor revealed for the first time. [...]
