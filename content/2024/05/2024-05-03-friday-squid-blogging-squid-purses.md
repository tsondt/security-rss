Title: Friday Squid Blogging: Squid Purses
Date: 2024-05-03T21:05:57+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-05-03-friday-squid-blogging-squid-purses

[Source](https://www.schneier.com/blog/archives/2024/05/friday-squid-blogging-squid-purses.html){:target="_blank" rel="noopener"}

> Squid-shaped purses for sale. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
