Title: Cyber cops plead for info on elusive Emotet mastermind
Date: 2024-05-31T19:21:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-31-cyber-cops-plead-for-info-on-elusive-emotet-mastermind

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/31/cyber_cops_plead_for_info/){:target="_blank" rel="noopener"}

> Follows arrests and takedowns of recent days After the big dog revelations from the past week, the cops behind Operation Endgame are now calling for help in tracking down the brains behind the Emotet operation.... [...]
