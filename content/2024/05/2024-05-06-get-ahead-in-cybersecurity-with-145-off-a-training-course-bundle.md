Title: Get ahead in cybersecurity with $145 off a training course bundle
Date: 2024-05-06T07:17:34-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-05-06-get-ahead-in-cybersecurity-with-145-off-a-training-course-bundle

[Source](https://www.bleepingcomputer.com/news/security/get-ahead-in-cybersecurity-with-145-off-a-training-course-bundle/){:target="_blank" rel="noopener"}

> Cybersecurity is everyone's concern, and for IT workers, a key skill on their resume. This five-course exam prep bundle helps you get more advanced credentials for $49.99, $145 off the $195 MSRP. [...]
