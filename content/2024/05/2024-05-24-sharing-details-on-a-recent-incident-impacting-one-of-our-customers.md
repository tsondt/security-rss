Title: Sharing details on a recent incident impacting one of our customers
Date: 2024-05-24T13:00:00+00:00
Author: Google Cloud Customer Support
Category: GCP Security
Tags: Customers;Security & Identity;Infrastructure
Slug: 2024-05-24-sharing-details-on-a-recent-incident-impacting-one-of-our-customers

[Source](https://cloud.google.com/blog/products/infrastructure/details-of-google-cloud-gcve-incident/){:target="_blank" rel="noopener"}

> A Google Cloud incident earlier this month impacted our customer, UniSuper, in Australia. While our first priority was to work with our customer to get them fully operational, soon after the incident started, we publicly acknowledged the incident in a joint statement with the customer. With our customer’s systems fully up and running, we have completed our internal review. We are sharing information publicly to clarify the nature of the incident and ensure there is an accurate account in the interest of transparency. Google Cloud has taken steps to ensure this particular and isolated incident cannot happen again. The impact [...]
