Title: Can I phone a friend? How cops circumvent face recognition bans
Date: 2024-05-20T16:13:05+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-05-20-can-i-phone-a-friend-how-cops-circumvent-face-recognition-bans

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/20/cops_circumvent_facial_recognition/){:target="_blank" rel="noopener"}

> Just ask a pal in a neighboring town with laxer restrictions Updated Police in multiple major US cities have figured out a trick to circumvent bans on facial recognition technology. Just ask a friend in a city without any such restrictions to do it for you.... [...]
