Title: Ticketmaster confirms massive breach after stolen data for sale online
Date: 2024-05-31T17:43:50-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-05-31-ticketmaster-confirms-massive-breach-after-stolen-data-for-sale-online

[Source](https://www.bleepingcomputer.com/news/security/ticketmaster-confirms-massive-breach-after-stolen-data-for-sale-online/){:target="_blank" rel="noopener"}

> Live Nation has confirmed that Ticketmaster suffered a data breach after its data was stolen from a third-party cloud database provider, which is believed to be Snowflake. [...]
