Title: CISA: Black Basta ransomware breached over 500 orgs worldwide
Date: 2024-05-11T10:09:08-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-11-cisa-black-basta-ransomware-breached-over-500-orgs-worldwide

[Source](https://www.bleepingcomputer.com/news/security/cisa-black-basta-ransomware-breached-over-500-orgs-worldwide/){:target="_blank" rel="noopener"}

> ​CISA and the FBI said today that Black Basta ransomware affiliates breached over 500 organizations between April 2022 and May 2024. [...]
