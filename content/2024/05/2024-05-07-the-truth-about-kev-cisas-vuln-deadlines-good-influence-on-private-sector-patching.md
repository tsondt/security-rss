Title: The truth about KEV: CISA’s vuln deadlines good influence on private-sector patching
Date: 2024-05-07T11:30:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-07-the-truth-about-kev-cisas-vuln-deadlines-good-influence-on-private-sector-patching

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/07/cisas_vulnerability_deadlines/){:target="_blank" rel="noopener"}

> More work to do as most deadlines are missed and worst bugs still take months to fix The deadlines associated with CISA's Known Exploited Vulnerabilities (KEV) catalog only apply to federal agencies, but fresh research shows they're having a positive impact on private organizations too.... [...]
