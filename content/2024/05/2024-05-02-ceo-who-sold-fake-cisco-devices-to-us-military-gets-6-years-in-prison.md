Title: CEO who sold fake Cisco devices to US military gets 6 years in prison
Date: 2024-05-02T18:01:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-02-ceo-who-sold-fake-cisco-devices-to-us-military-gets-6-years-in-prison

[Source](https://www.bleepingcomputer.com/news/security/ceo-who-sold-fake-cisco-devices-to-us-military-gets-6-years-in-prison/){:target="_blank" rel="noopener"}

> Onur Aksoy, the CEO of a group of companies controlling multiple online storefronts, was sentenced to six and a half years in prison for selling $100 million worth of counterfeit Cisco network equipment to government, health, education, and military organizations worldwide. [...]
