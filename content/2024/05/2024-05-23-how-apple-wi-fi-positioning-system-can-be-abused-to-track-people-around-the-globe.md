Title: How Apple Wi-Fi Positioning System can be abused to track people around the globe
Date: 2024-05-23T06:34:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-05-23-how-apple-wi-fi-positioning-system-can-be-abused-to-track-people-around-the-globe

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/23/apple_wifi_positioning_system/){:target="_blank" rel="noopener"}

> SpaceX is smart on this, Cupertino and GL.iNet not so much In-depth Academics have shown how Apple's Wi-Fi Positioning System (WPS) can be abused to create a global privacy nightmare.... [...]
