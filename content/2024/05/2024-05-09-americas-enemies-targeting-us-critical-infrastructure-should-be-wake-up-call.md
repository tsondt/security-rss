Title: America's enemies targeting US critical infrastructure should be 'wake-up call'
Date: 2024-05-09T17:45:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-09-americas-enemies-targeting-us-critical-infrastructure-should-be-wake-up-call

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/09/china_russia_iran_infrastructure/){:target="_blank" rel="noopener"}

> Having China, Russia, and Iran routinely rummaging around is cause for concern, says ex-NSA man RSAC Digital intruders from China, Russia, and Iran breaking into US water systems this year should be a "wake-up call," according to former National Security Agency cyber boss Rob Joyce.... [...]
