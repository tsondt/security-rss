Title: Ransomware negotiator weighs in on the extortion payment debate with El Reg
Date: 2024-05-12T20:03:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-12-ransomware-negotiator-weighs-in-on-the-extortion-payment-debate-with-el-reg

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/12/ransomware_negotiator_payments/){:target="_blank" rel="noopener"}

> As gang tactics get nastier while attacks hit all-time highs Interview Ransomware hit an all-time high last year, with more than 60 criminal gangs listing at least 4,500 victims – and these infections don't show any signs of slowing.... [...]
