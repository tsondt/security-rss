Title: Law enforcement operation takes aim at an often-overlooked cybercrime linchpin
Date: 2024-05-30T19:41:08+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;droppers;europol;fbi;law enforcement;malware
Slug: 2024-05-30-law-enforcement-operation-takes-aim-at-an-often-overlooked-cybercrime-linchpin

[Source](https://arstechnica.com/?p=2027800){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) An international cast of law enforcement agencies has struck a blow at a cybercrime linchpin that’s as obscure as it is instrumental in the mass-infection of devices: so-called droppers, the sneaky software that’s used to install ransomware, spyware, and all manner of other malware. Europol said Wednesday it made four arrests, took down 100 servers, and seized 2,000 domain names that were facilitating six of the best-known droppers. Officials also added eight fugitives linked to the enterprises to Europe’s Most Wanted list. The droppers named by Europol are IcedID, SystemBC, Pikabot, Smokeloader, Bumblebee, and Trickbot. Droppers [...]
