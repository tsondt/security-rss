Title: New Lawsuit Attempting to Make Adversarial Interoperability Legal
Date: 2024-05-06T11:03:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;copyright;courts
Slug: 2024-05-06-new-lawsuit-attempting-to-make-adversarial-interoperability-legal

[Source](https://www.schneier.com/blog/archives/2024/05/new-lawsuit-attempting-to-make-adversarial-interoperability-legal.html){:target="_blank" rel="noopener"}

> Lots of complicated details here: too many for me to summarize well. It involves an obscure Section 230 provision—and an even more obscure typo. Read this. [...]
