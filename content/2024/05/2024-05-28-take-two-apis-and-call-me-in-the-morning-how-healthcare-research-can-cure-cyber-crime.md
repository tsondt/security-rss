Title: Take two APIs and call me in the morning: How healthcare research can cure cyber crime
Date: 2024-05-28T08:30:10+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2024-05-28-take-two-apis-and-call-me-in-the-morning-how-healthcare-research-can-cure-cyber-crime

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/28/take_two_apis_and_call/){:target="_blank" rel="noopener"}

> In evolving smarter security, open source is the missing link Opinion Some ideas work better than others. Take DARPA, the US Defense Advanced Research Projects Agency. Launched by US President Dwight Eisenhower in 1957 response to Sputnik, its job is to create and test concepts that may be useful in thwarting enemies. Along the way, it's helped make happen GPS, weather satellites, PC technology, and something called the internet.... [...]
