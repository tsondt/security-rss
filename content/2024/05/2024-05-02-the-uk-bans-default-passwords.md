Title: The UK Bans Default Passwords
Date: 2024-05-02T11:05:03+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;botnets;Internet of Things;passwords;UK
Slug: 2024-05-02-the-uk-bans-default-passwords

[Source](https://www.schneier.com/blog/archives/2024/05/the-uk-bans-default-passwords.html){:target="_blank" rel="noopener"}

> The UK is the first country to ban default passwords on IoT devices. On Monday, the United Kingdom became the first country in the world to ban default guessable usernames and passwords from these IoT devices. Unique passwords installed by default are still permitted. The Product Security and Telecommunications Infrastructure Act 2022 (PSTI) introduces new minimum-security standards for manufacturers, and demands that these companies are open with consumers about how long their products will receive security updates for. The UK may be the first country, but as far as I know, California is the first jurisdiction. It banned default passwords [...]
