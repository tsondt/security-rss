Title: New Attack Against Self-Driving Car AI
Date: 2024-05-10T16:01:00+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence;cars;cyberattack;side-channel attacks
Slug: 2024-05-10-new-attack-against-self-driving-car-ai

[Source](https://www.schneier.com/blog/archives/2024/05/new-attack-against-self-driving-car-ai.html){:target="_blank" rel="noopener"}

> This is another attack that convinces the AI to ignore road signs : Due to the way CMOS cameras operate, rapidly changing light from fast flashing diodes can be used to vary the color. For example, the shade of red on a stop sign could look different on each line depending on the time between the diode flash and the line capture. The result is the camera capturing an image full of lines that don’t quite match each other. The information is cropped and sent to the classifier, usually based on deep neural networks, for interpretation. Because it’s full of [...]
