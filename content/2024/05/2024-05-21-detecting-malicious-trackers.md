Title: Detecting Malicious Trackers
Date: 2024-05-21T11:09:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;Bluetooth;false positives;Google;security standards;stalking;tracking
Slug: 2024-05-21-detecting-malicious-trackers

[Source](https://www.schneier.com/blog/archives/2024/05/detecting-malicious-trackers.html){:target="_blank" rel="noopener"}

> From Slashdot : Apple and Google have launched a new industry standard called “ Detecting Unwanted Location Trackers ” to combat the misuse of Bluetooth trackers for stalking. Starting Monday, iPhone and Android users will receive alerts when an unknown Bluetooth device is detected moving with them. The move comes after numerous cases of trackers like Apple’s AirTags being used for malicious purposes. Several Bluetooth tag companies have committed to making their future products compatible with the new standard. Apple and Google said they will continue collaborating with the Internet Engineering Task Force to further develop this technology and address [...]
