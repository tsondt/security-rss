Title: UK confirms Ministry of Defence payroll data exposed in data breach
Date: 2024-05-07T15:41:53-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-05-07-uk-confirms-ministry-of-defence-payroll-data-exposed-in-data-breach

[Source](https://www.bleepingcomputer.com/news/security/uk-confirms-ministry-of-defence-payroll-data-exposed-in-data-breach/){:target="_blank" rel="noopener"}

> The UK Government confirmed today that a threat actor recently breached the country's Ministry of Defence and gained access to part of the Armed Forces payment network. [...]
