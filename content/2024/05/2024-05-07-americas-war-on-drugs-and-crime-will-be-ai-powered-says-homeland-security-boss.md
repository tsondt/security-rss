Title: America's War on Drugs and Crime will be AI powered, says Homeland Security boss
Date: 2024-05-07T23:47:16+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-07-americas-war-on-drugs-and-crime-will-be-ai-powered-says-homeland-security-boss

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/07/dhs_ai_civil_liberties/){:target="_blank" rel="noopener"}

> Or at least it might well be if these trial programs work out, with some civil lib oversight etc etc etc RSAC AI is a double-edged sword in that the government can see ways in which the tech can protect and also be used to attack Americans, says US Homeland Security Secretary Alejandro Mayorkas.... [...]
