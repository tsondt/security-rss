Title: Coalfire evaluates Google Cloud AI: ‘Mature,’ ready for governance, compliance
Date: 2024-05-30T16:00:00+00:00
Author: Nick Godfrey
Category: GCP Security
Tags: AI & Machine Learning;Security & Identity
Slug: 2024-05-30-coalfire-evaluates-google-cloud-ai-mature-ready-for-governance-compliance

[Source](https://cloud.google.com/blog/products/identity-security/coalfire-evaluates-google-cloud-ai-mature-ready-for-governance-compliance/){:target="_blank" rel="noopener"}

> At Google Cloud, we've long demonstrated our commitment to responsible AI development and transparency in our work to support safer and more accountable products, earn and keep our customers’ trust, and foster a culture of responsible innovation. We understand that AI comes with complexities and risks, and to ensure our readiness for the future landscape of AI compliance we proactively benchmark ourselves against emerging AI governance frameworks. To put our commitments into practice, we invited Coalfire, a respected leader in cybersecurity, to examine our current processes, measure alignment and maturity toward the objectives defined in the National Institute of Standards [...]
