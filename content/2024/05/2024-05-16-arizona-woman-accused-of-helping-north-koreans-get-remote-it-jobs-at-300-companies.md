Title: Arizona woman accused of helping North Koreans get remote IT jobs at 300 companies
Date: 2024-05-16T22:49:22+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;balistic missiles;information technology;North Korea
Slug: 2024-05-16-arizona-woman-accused-of-helping-north-koreans-get-remote-it-jobs-at-300-companies

[Source](https://arstechnica.com/?p=2025081){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images | the-lightwriter) An Arizona woman has been accused of helping generate millions of dollars for North Korea’s ballistic missile program by helping citizens of that country land IT jobs at US-based Fortune 500 companies. Christina Marie Chapman, 49, of Litchfield Park, Arizona, raised $6.8 million in the scheme, federal prosecutors said in an indictment unsealed Thursday. Chapman allegedly funneled the money to North Korea’s Munitions Industry Department, which is involved in key aspects of North Korea’s weapons program, including its development of ballistic missiles. Part of the alleged scheme involved Chapman and co-conspirators compromising the identities [...]
