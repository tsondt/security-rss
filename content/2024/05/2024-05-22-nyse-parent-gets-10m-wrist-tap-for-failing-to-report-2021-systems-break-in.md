Title: NYSE parent gets $10M wrist tap for failing to report 2021 systems break-in
Date: 2024-05-22T19:30:13+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-05-22-nyse-parent-gets-10m-wrist-tap-for-failing-to-report-2021-systems-break-in

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/22/nyse_parent_gets_10m_wrist/){:target="_blank" rel="noopener"}

> Intercontinental Exchange's Q1 revenue exceeded $1B – that'll sure teach 'em The New York Stock Exchange's parent company has just been hit with a $10 million fine for failing to properly inform the Securities and Exchange Commission (SEC) of a 2021 cyber intrusion.... [...]
