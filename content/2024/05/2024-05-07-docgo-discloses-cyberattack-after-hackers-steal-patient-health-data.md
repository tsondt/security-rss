Title: DocGo discloses cyberattack after hackers steal patient health data
Date: 2024-05-07T18:20:15-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-05-07-docgo-discloses-cyberattack-after-hackers-steal-patient-health-data

[Source](https://www.bleepingcomputer.com/news/security/docgo-discloses-cyberattack-after-hackers-steal-patient-health-data/){:target="_blank" rel="noopener"}

> Mobile medical care firm DocGo confirmed it suffered a cyberattack after threat actors breached its systems and stole patient health data. [...]
