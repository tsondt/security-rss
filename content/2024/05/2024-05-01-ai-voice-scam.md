Title: AI Voice Scam
Date: 2024-05-01T11:09:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;scams;social engineering
Slug: 2024-05-01-ai-voice-scam

[Source](https://www.schneier.com/blog/archives/2024/05/ai-voice-scam.html){:target="_blank" rel="noopener"}

> Scammers tricked a company into believing they were dealing with a BBC presenter. They faked her voice, and accepted money intended for her. [...]
