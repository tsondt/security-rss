Title: Bayer and 12 other major drug companies caught up in Cencora data loss
Date: 2024-05-27T02:59:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-27-bayer-and-12-other-major-drug-companies-caught-up-in-cencora-data-loss

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/27/security_in_brief/){:target="_blank" rel="noopener"}

> Plus: US water systems fail at cyber security Infosec in brief More than a dozen big pharmaceutical suppliers have begun notifying people that their medical records were stolen when US drug wholesaler Cencora was breached in February.... [...]
