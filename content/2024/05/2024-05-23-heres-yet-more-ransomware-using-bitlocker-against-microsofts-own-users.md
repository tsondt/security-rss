Title: Here's yet more ransomware using BitLocker against Microsoft's own users
Date: 2024-05-23T21:21:15+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-23-heres-yet-more-ransomware-using-bitlocker-against-microsofts-own-users

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/23/ransomware_abuses_microsoft_bitlocker/){:target="_blank" rel="noopener"}

> ShrinkLocker throws steel and vaccine makers into the hurt locker Updated Yet more ransomware is using Microsoft BitLocker to encrypt corporate files, steal the decryption key, and then extort a payment from victim organizations, according to Kaspersky.... [...]
