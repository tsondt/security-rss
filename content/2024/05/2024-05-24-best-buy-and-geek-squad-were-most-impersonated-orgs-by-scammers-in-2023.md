Title: Best Buy and Geek Squad were most impersonated orgs by scammers in 2023
Date: 2024-05-24T22:23:12+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-05-24-best-buy-and-geek-squad-were-most-impersonated-orgs-by-scammers-in-2023

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/24/ftc_scam_report/){:target="_blank" rel="noopener"}

> But criminals posing as Microsoft workers scored the most ill-gotten gains The Federal Trade Commission (FTC) has shared data on the most impersonated companies in 2023, which include Best Buy, Amazon, and PayPal in the top three.... [...]
