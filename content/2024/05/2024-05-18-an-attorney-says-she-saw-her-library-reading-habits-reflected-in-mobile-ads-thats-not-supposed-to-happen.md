Title: An attorney says she saw her library reading habits reflected in mobile ads. That's not supposed to happen
Date: 2024-05-18T17:04:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-05-18-an-attorney-says-she-saw-her-library-reading-habits-reflected-in-mobile-ads-thats-not-supposed-to-happen

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/18/mystery_of_the_targeted_mobile_ads/){:target="_blank" rel="noopener"}

> Follow us down this deep rabbit hole of privacy policy after privacy policy Feature In April, attorney Christine Dudley was listening to a book on her iPhone while playing a game on her Android tablet when she started to see in-game ads that reflected the audiobooks she recently checked out of the San Francisco Public Library.... [...]
