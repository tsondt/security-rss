Title: How two brothers allegedly swiped $25M in a 12-second Ethereum heist
Date: 2024-05-18T06:29:07+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-05-18-how-two-brothers-allegedly-swiped-25m-in-a-12-second-ethereum-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/18/brother_etherium_heist/){:target="_blank" rel="noopener"}

> Feds scoff at blockchain integrity while software bug said to have been at heart of the matter The US Department of Justice has booked two brothers on allegations that they exploited open source software used in the Ethereum blockchain world to bag $25 million (£20 million).... [...]
