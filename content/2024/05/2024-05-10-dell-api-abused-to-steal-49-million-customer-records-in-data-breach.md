Title: Dell API abused to steal 49 million customer records in data breach
Date: 2024-05-10T15:30:07-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-05-10-dell-api-abused-to-steal-49-million-customer-records-in-data-breach

[Source](https://www.bleepingcomputer.com/news/security/dell-api-abused-to-steal-49-million-customer-records-in-data-breach/){:target="_blank" rel="noopener"}

> The threat actor behind the recent Dell data breach revealed they scraped information of 49 million customer records using an partner portal API they accessed as a fake company. [...]
