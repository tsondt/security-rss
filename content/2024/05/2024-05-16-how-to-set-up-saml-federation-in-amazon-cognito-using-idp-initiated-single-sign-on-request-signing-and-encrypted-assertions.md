Title: How to set up SAML federation in Amazon Cognito using IdP-initiated single sign-on, request signing, and encrypted assertions
Date: 2024-05-16T16:57:51+00:00
Author: Vishal Jakharia
Category: AWS Security
Tags: Advanced (300);Amazon Cognito;Security, Identity, & Compliance;Technical How-to;authentication;OAuth2;SAML;Security Blog
Slug: 2024-05-16-how-to-set-up-saml-federation-in-amazon-cognito-using-idp-initiated-single-sign-on-request-signing-and-encrypted-assertions

[Source](https://aws.amazon.com/blogs/security/how-to-set-up-saml-federation-in-amazon-cognito-using-idp-initiated-single-sign-on-request-signing-and-encrypted-assertions/){:target="_blank" rel="noopener"}

> When an identity provider (IdP) serves multiple service providers (SPs), IdP-initiated single sign-on provides a consistent sign-in experience that allows users to start the authentication process from one centralized portal or dashboard. It helps administrators have more control over the authentication process and simplifies the management. However, when you support IdP-initiated authentication, the SP (Amazon [...]
