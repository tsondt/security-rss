Title: Crook brags about US Army and $75b defense biz pwnage
Date: 2024-05-15T22:30:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-15-crook-brags-about-us-army-and-75b-defense-biz-pwnage

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/15/us_army_contractor_data_loss/){:target="_blank" rel="noopener"}

> More government data allegedly stolen by prolific criminals An extortionist claims to have stolen files from the US Army Aviation and Missile Command in August 2023, and now claims they are selling access to a $75 billion aerospace and defense company.... [...]
