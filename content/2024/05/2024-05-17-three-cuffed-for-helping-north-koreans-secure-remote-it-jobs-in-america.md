Title: Three cuffed for 'helping North Koreans' secure remote IT jobs in America
Date: 2024-05-17T18:34:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-17-three-cuffed-for-helping-north-koreans-secure-remote-it-jobs-in-america

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/17/three_arrested_for_helping_north_korea/){:target="_blank" rel="noopener"}

> Your local nail tech could be a secret agent for Kim’s cunning plan Three individuals accused of helping North Korea fund its weapons programs using US money are now in handcuffs.... [...]
