Title: IBM Sells Cybersecurity Group
Date: 2024-05-20T11:04:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;IBM;Resilient Systems
Slug: 2024-05-20-ibm-sells-cybersecurity-group

[Source](https://www.schneier.com/blog/archives/2024/05/ibm-sells-cybersecurity-group.html){:target="_blank" rel="noopener"}

> IBM is selling its QRadar product suite to Palo Alto Networks, for an undisclosed—but probably surprisingly small—sum. I have a personal connection to this. In 2016, IBM bought Resilient Systems, the startup I was a part of. It became part if IBM’s cybersecurity offerings, mostly and weirdly subservient to QRadar. That was what seemed to be the problem at IBM. QRadar was IBM’s first acquisition in the cybersecurity space, and it saw everything through the lens of that SIEM system. I left the company two years after the acquisition, and near as I could tell, it never managed to figure [...]
