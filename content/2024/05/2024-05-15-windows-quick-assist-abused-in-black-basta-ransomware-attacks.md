Title: Windows Quick Assist abused in Black Basta ransomware attacks
Date: 2024-05-15T13:06:50-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-05-15-windows-quick-assist-abused-in-black-basta-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/windows-quick-assist-abused-in-black-basta-ransomware-attacks/){:target="_blank" rel="noopener"}

> ​Financially motivated cybercriminals abuse the Windows Quick Assist feature in social engineering attacks to deploy Black Basta ransomware payloads on victims' networks. [...]
