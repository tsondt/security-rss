Title: 'Cyberattack' shutters Christie's website days before $840M art mega-auction
Date: 2024-05-13T19:36:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-13-cyberattack-shutters-christies-website-days-before-840m-art-mega-auction

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/13/cyberattack_shutters_christies_website/){:target="_blank" rel="noopener"}

> Going once, going twice, going offline Christie's website remains offline as of Monday after a "technology security issue" shut it down Thursday night – just days before the venerable auction house planned to flog $840 million of art.... [...]
