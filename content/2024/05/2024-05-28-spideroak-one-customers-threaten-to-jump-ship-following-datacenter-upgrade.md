Title: SpiderOak One customers threaten to jump ship following datacenter upgrade
Date: 2024-05-28T16:45:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-28-spideroak-one-customers-threaten-to-jump-ship-following-datacenter-upgrade

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/28/spideroak_one_customers_threaten_to/){:target="_blank" rel="noopener"}

> One tricky cluster is causing outrage among longstanding customers Over a month after an April datacenter upgrade coincided with problems with some of its customers' backups, secure storage biz SpiderOak still isn't fully operational, and some angry users say they're ready to cut ties.... [...]
