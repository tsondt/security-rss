Title: With ransomware whales becoming so dominant, would-be challengers ask 'what's the point?'
Date: 2024-05-21T11:01:32+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-21-with-ransomware-whales-becoming-so-dominant-would-be-challengers-ask-whats-the-point

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/21/with_ransomware_whales_becoming_so/){:target="_blank" rel="noopener"}

> Fewer rivals on the scene as big-gang success soars The number of new ransomware strains in circulation has more than halved over the past 12 months, suggesting there is little need for innovation given the success of the existing tools used by top gangs.... [...]
