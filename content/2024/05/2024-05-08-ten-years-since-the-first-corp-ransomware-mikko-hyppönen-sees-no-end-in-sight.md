Title: Ten years since the first corp ransomware, Mikko Hyppönen sees no end in sight
Date: 2024-05-08T07:31:15+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-05-08-ten-years-since-the-first-corp-ransomware-mikko-hyppönen-sees-no-end-in-sight

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/08/mikko_ransomware_decade/){:target="_blank" rel="noopener"}

> On the plus side, infosec's a good bet for a long, stable career Interview This year is an unfortunate anniversary for information security: We're told it's a decade since ransomware started infecting corporations.... [...]
