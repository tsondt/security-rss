Title: Google takes shots at Microsoft for shoddy security record with enterprise apps
Date: 2024-05-20T17:47:10+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-05-20-google-takes-shots-at-microsoft-for-shoddy-security-record-with-enterprise-apps

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/20/google_takes_shots_at_microsoft/){:target="_blank" rel="noopener"}

> Also, feds who switch to Google Workspace for 3 years get an extra year for free Updated Google has taken a victory lap in the wake of high-profile intrusions into Microsoft's systems, and says businesses should ditch Exchange and OneDrive for Gmail and Google Drive.... [...]
