Title: My TED Talks
Date: 2024-05-03T18:13:24+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Schneier news;TED;videos
Slug: 2024-05-03-my-ted-talks

[Source](https://www.schneier.com/blog/archives/2024/05/my-ted-talks.html){:target="_blank" rel="noopener"}

> I have spoken at several TED conferences over the years. TEDxPSU 2010: “ Reconceptualizing Security ” TEDxCambridge 2013: “ The Battle for Power on the Internet ” TEDMed 2016: “ Who Controls Your Medical Data ?” I’m putting this here because I want all three links in one place. [...]
