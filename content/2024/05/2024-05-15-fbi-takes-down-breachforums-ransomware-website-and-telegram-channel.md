Title: FBI takes down BreachForums ransomware website and Telegram channel
Date: 2024-05-15T22:31:28+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-05-15-fbi-takes-down-breachforums-ransomware-website-and-telegram-channel

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/15/fbi_breachforums_ransomware/){:target="_blank" rel="noopener"}

> No more illicit gains, for a while at least The FBI, in combination with police around the world, have taken control of the website and Telegram channel of ransomware brokerage site BreachForums.... [...]
