Title: British Columbia investigating cyberattacks on government networks
Date: 2024-05-09T12:34:18-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-09-british-columbia-investigating-cyberattacks-on-government-networks

[Source](https://www.bleepingcomputer.com/news/security/british-columbia-investigating-cyberattacks-on-government-networks/){:target="_blank" rel="noopener"}

> The Government of British Columbia is investigating multiple "cybersecurity incidents" that have impacted the Canadian province's government networks. [...]
