Title: Open source programming language R patches gnarly arbitrary code exec flaw
Date: 2024-05-01T00:59:11+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-05-01-open-source-programming-language-r-patches-gnarly-arbitrary-code-exec-flaw

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/01/r_programming_language_ace_vuln/){:target="_blank" rel="noopener"}

> An ACE in the hole for miscreants Updated The open source R programming language – popular among statisticians and data scientists for performing visualization, machine learning, and suchlike – has patched an arbitrary code execution hole that scored a preliminary CVSS severity rating of 8.8 out of 10.... [...]
