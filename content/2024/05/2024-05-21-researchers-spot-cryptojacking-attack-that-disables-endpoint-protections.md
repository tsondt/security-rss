Title: Researchers spot cryptojacking attack that disables endpoint protections
Date: 2024-05-21T19:14:55+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security
Slug: 2024-05-21-researchers-spot-cryptojacking-attack-that-disables-endpoint-protections

[Source](https://arstechnica.com/?p=2026052){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Malware recently spotted in the wild uses sophisticated measures to disable antivirus protections, destroy evidence of infection, and permanently infect machines with cryptocurrency-mining software, researchers said Tuesday. Key to making the unusually complex system of malware operate is a function in the main payload, named GhostEngine, that disables Microsoft Defender or any other antivirus or endpoint-protection software that may be running on the targeted computer. It also hides any evidence of compromise. “The first objective of the GhostEngine malware is to incapacitate endpoint security solutions and disable specific Windows event logs, such as Security and System [...]
