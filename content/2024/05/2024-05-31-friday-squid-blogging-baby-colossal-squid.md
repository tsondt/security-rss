Title: Friday Squid Blogging: Baby Colossal Squid
Date: 2024-05-31T21:02:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid;video
Slug: 2024-05-31-friday-squid-blogging-baby-colossal-squid

[Source](https://www.schneier.com/blog/archives/2024/05/friday-squid-blogging-baby-colossal-squid.html){:target="_blank" rel="noopener"}

> This video might be a juvenile colossal squid. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
