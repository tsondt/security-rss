Title: Monday.com removes "Share Update" feature abused for phishing attacks
Date: 2024-05-09T18:17:15-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-05-09-mondaycom-removes-share-update-feature-abused-for-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/mondaycom-removes-share-update-feature-abused-for-phishing-attacks/){:target="_blank" rel="noopener"}

> Project management platform Monday.com has removed its "Share Update" feature after threat actors abused it in phishing attacks. [...]
