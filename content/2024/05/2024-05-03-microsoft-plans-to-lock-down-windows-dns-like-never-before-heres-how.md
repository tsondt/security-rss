Title: Microsoft plans to lock down Windows DNS like never before. Here’s how.
Date: 2024-05-03T23:42:26+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;domain name system;microsoft;Windows;ztdns
Slug: 2024-05-03-microsoft-plans-to-lock-down-windows-dns-like-never-before-heres-how

[Source](https://arstechnica.com/?p=2021987){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Translating numerical IP addresses into human-readable domain names has long been fraught with gaping security risks. After all, lookups are rarely end-to-end encrypted. The servers providing domain name lookups provide translations for virtually any IP address—even when they’re known to be malicious. And many end-user devices can easily be configured to stop using authorized lookup servers and instead use malicious ones. Microsoft on Friday provided a peek at a comprehensive framework that aims to sort out the Domain Name System (DNS) mess so that it’s better locked down inside Windows networks. It’s called ZTDNS (zero trust [...]
