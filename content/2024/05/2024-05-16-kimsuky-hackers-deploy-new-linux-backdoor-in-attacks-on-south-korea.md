Title: Kimsuky hackers deploy new Linux backdoor in attacks on South Korea
Date: 2024-05-16T09:28:37-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-16-kimsuky-hackers-deploy-new-linux-backdoor-in-attacks-on-south-korea

[Source](https://www.bleepingcomputer.com/news/security/kimsuky-hackers-deploy-new-linux-backdoor-in-attacks-on-south-korea/){:target="_blank" rel="noopener"}

> The North Korean hacker group Kimsuki has been using a new Linux malware called Gomir that is a version of the GoBear backdoor delivered via trojanized software installers. [...]
