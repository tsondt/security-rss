Title: Introducing Google Threat Intelligence: Actionable threat intelligence at Google scale
Date: 2024-05-06T13:00:00+00:00
Author: Sandra Joyce
Category: GCP Security
Tags: Security & Identity
Slug: 2024-05-06-introducing-google-threat-intelligence-actionable-threat-intelligence-at-google-scale

[Source](https://cloud.google.com/blog/products/identity-security/introducing-google-threat-intelligence-actionable-threat-intelligence-at-google-scale-at-rsa/){:target="_blank" rel="noopener"}

> For decades, threat intelligence solutions have had two main challenges: They lack a comprehensive view of the threat landscape, and to get value from intelligence, organizations have to spend excess time, energy, and money trying to collect and operationalize the data. Today at the RSA Conference in San Francisco, we are announcing Google Threat Intelligence, a new offering that combines the unmatched depth of our Mandiant frontline expertise, the global reach of the VirusTotal community, and the breadth of visibility only Google can deliver, based on billions of signals across devices and emails. Google Threat Intelligence includes Gemini in Threat [...]
