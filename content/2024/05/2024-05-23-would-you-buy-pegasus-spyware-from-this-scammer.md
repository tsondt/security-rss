Title: Would you buy Pegasus spyware from this scammer?
Date: 2024-05-23T05:45:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-05-23-would-you-buy-pegasus-spyware-from-this-scammer

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/23/fake_pegasus_spyware_circulating/){:target="_blank" rel="noopener"}

> You shouldn't – Indian infosec researchers warn you'll get random junk instead Indian infosec firm CloudSEK warned on Wednesday that scammers are selling counterfeit code advertised as the NSO Group's notorious Pegasus spyware.... [...]
