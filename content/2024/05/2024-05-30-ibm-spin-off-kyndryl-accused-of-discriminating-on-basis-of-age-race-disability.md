Title: IBM spin-off Kyndryl accused of discriminating on basis of age, race, disability
Date: 2024-05-30T11:14:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-05-30-ibm-spin-off-kyndryl-accused-of-discriminating-on-basis-of-age-race-disability

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/30/kyndryl_accused_of_discriminating/){:target="_blank" rel="noopener"}

> Five current and former employees file formal charges with US employment watchdog Exclusive Kyndryl, the IT services firm spun out of IBM, has been accused by multiple employees within its CISO Defense security group of discrimination on the basis of age, race, and disability, in both internal complaints and formal charges filed with the US Equal Employment Opportunity Commission (EEOC).... [...]
