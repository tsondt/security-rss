Title: NCSC CTO: Broken market must be fixed to usher in new tech
Date: 2024-05-16T09:33:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-16-ncsc-cto-broken-market-must-be-fixed-to-usher-in-new-tech

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/16/ncsc_cto_broken_market_must/){:target="_blank" rel="noopener"}

> It may take ten years but vendors must be held accountable for the vulnerabilities they introduce CYBERUK National Cyber Security Centre (NCSC) CTO Ollie Whitehouse kicked off day two of Britain's cyber watchdog's annual shindig, CYBERUK, with a tirade about the tech market, pulling it apart to demonstrate why he believes it's at fault for many of the security problems the industry is facing today.... [...]
