Title: Block accused of mass compliance failures that saw digi-dollars reach terrorists
Date: 2024-05-02T00:30:46+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-05-02-block-accused-of-mass-compliance-failures-that-saw-digi-dollars-reach-terrorists

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/02/prosecutors_probe_block_square_cashapp/){:target="_blank" rel="noopener"}

> Developer of Square and Cash App reportedly has big back-end problems it was slow to fix Fintech biz Block is reportedly under investigation by US prosecutors over claims by a former employee that lax compliance checks mean its Square and Cash App services may have been used by terrorists – or in countries that US orgs are not permitted to do business.... [...]
