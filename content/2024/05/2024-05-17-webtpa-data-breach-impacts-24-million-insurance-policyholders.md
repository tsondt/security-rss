Title: WebTPA data breach impacts 2.4 million insurance policyholders
Date: 2024-05-17T10:45:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-05-17-webtpa-data-breach-impacts-24-million-insurance-policyholders

[Source](https://www.bleepingcomputer.com/news/security/webtpa-data-breach-impacts-24-million-insurance-policyholders/){:target="_blank" rel="noopener"}

> The WebTPA Employer Services (WebTPA) data breach disclosed earlier this month is impacting close to 2.5 million individuals, the U.S. Department of Health and Human Services notes. [...]
