Title: Fed-run LockBit site back from the dead and vows to really spill the beans on gang
Date: 2024-05-06T23:42:35+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-05-06-fed-run-lockbit-site-back-from-the-dead-and-vows-to-really-spill-the-beans-on-gang

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/06/lockbit_website_police/){:target="_blank" rel="noopener"}

> After very boring first reveal, this could be the real deal Cops around the world have relaunched LockBit's website after they shut it down in February – and it's now counting down the hours to reveal documents that could unmask the ransomware group.... [...]
