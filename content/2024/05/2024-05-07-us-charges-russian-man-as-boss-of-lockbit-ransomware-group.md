Title: U.S. Charges Russian Man as Boss of LockBit Ransomware Group
Date: 2024-05-07T17:36:14+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Russia's War on Ukraine;Dmitry Yuryevich Khoroshev;LockBitSupp;U.S. Attorney Philip R. Sellinger;U.S. Department of Justice;Дмитрий Юрьевич Хорошев
Slug: 2024-05-07-us-charges-russian-man-as-boss-of-lockbit-ransomware-group

[Source](https://krebsonsecurity.com/2024/05/u-s-charges-russian-man-as-boss-of-lockbit-ransomware-group/){:target="_blank" rel="noopener"}

> The United States joined the United Kingdom and Australia today in sanctioning 31-year-old Russian national Dmitry Yuryevich Khoroshev as the alleged leader of the infamous ransomware group LockBit. The U.S. Department of Justice also indicted Khoroshev and charged him with using Lockbit to attack more than 2,000 victims and extort at least $100 million in ransomware payments. Image: U.K. National Crime Agency. Khoroshev (Дмитрий Юрьевич Хорошев), a resident of Voronezh, Russia, was charged in a 26-count indictment by a grand jury in New Jersey. “Dmitry Khoroshev conceived, developed, and administered Lockbit, the most prolific ransomware variant and group in the [...]
