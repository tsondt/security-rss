Title: AT&T delays Microsoft 365 email delivery due to spam wave
Date: 2024-05-09T13:58:30-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Technology;Microsoft;Security
Slug: 2024-05-09-att-delays-microsoft-365-email-delivery-due-to-spam-wave

[Source](https://www.bleepingcomputer.com/news/technology/att-delays-microsoft-365-email-delivery-due-to-spam-wave/){:target="_blank" rel="noopener"}

> AT&T's email servers are blocking connections from Microsoft 365 due to a "high volume" spam wave originating from Microsoft's service. [...]
