Title: BBC suffers data breach impacting current, former employees
Date: 2024-05-30T10:02:53-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud
Slug: 2024-05-30-bbc-suffers-data-breach-impacting-current-former-employees

[Source](https://www.bleepingcomputer.com/news/security/bbc-suffers-data-breach-impacting-current-former-employees/){:target="_blank" rel="noopener"}

> The BBC has disclosed a data security incident that occurred on May 21, involving unauthorized access to files hosted on a cloud-based service, compromising the personal information of BBC Pension Scheme members. [...]
