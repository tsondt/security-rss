Title: Undersea cables must have high-priority protection before they become top targets
Date: 2024-05-08T21:01:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-08-undersea-cables-must-have-high-priority-protection-before-they-become-top-targets

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/08/undersea_cables_targets/){:target="_blank" rel="noopener"}

> It's 'essential to national security' ex-Navy intel officer tells us Interview As undersea cables carry increasing amounts of information, cyber and physical attacks against them will cause a greater impact on the wider internet.... [...]
