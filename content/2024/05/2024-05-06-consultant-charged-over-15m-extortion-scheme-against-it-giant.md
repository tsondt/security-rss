Title: Consultant charged over $1.5M extortion scheme against IT giant
Date: 2024-05-06T17:00:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-06-consultant-charged-over-15m-extortion-scheme-against-it-giant

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/06/consultant_extortion_charges/){:target="_blank" rel="noopener"}

> Accused of stealing data after losing his job A cybersecurity expert could face a 20-year prison sentence after being accused of trying to extort a multinational IT infrastructure services biz to the tune of $1.5 million.... [...]
