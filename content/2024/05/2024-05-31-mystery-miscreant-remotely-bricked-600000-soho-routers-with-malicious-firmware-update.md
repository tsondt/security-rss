Title: Mystery miscreant remotely bricked 600,000 SOHO routers with malicious firmware update
Date: 2024-05-31T00:15:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-31-mystery-miscreant-remotely-bricked-600000-soho-routers-with-malicious-firmware-update

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/31/pumoking_eclipse_remote_router_attack/){:target="_blank" rel="noopener"}

> Source and motive of 'Pumpkin Eclipse' assault unknown Unknown miscreants broke into more than 600,000 routers belonging to a single ISP late last year and deployed malware on the devices before totally disabling them, according to security researchers.... [...]
