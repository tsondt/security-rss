Title: First LockBit, now BreachForums: Are cops winning the war or just a few battles?
Date: 2024-05-17T11:37:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-17-first-lockbit-now-breachforums-are-cops-winning-the-war-or-just-a-few-battles

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/17/cops_crime_winning/){:target="_blank" rel="noopener"}

> TLDR: Peace in our time is really really hard Interview On Wednesday the FBI and international cops celebrated yet another cybercrime takedown – of ransomware brokerage site BreachForums – just a week after doxing and imposing sanctions on the LockBit ransomware crew's kingpin, and two months after compromising the gang's website.... [...]
