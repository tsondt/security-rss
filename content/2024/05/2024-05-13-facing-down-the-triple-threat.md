Title: Facing down the triple threat
Date: 2024-05-13T09:24:38+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2024-05-13-facing-down-the-triple-threat

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/13/facing_down_the_triple_threat/){:target="_blank" rel="noopener"}

> The Register’s Tim Philips gets down and dirty on cyber security in this interview with Rubrik CISO Richard Cassidy Sponsored Post There were hard words about the state of Britain's cyber security in parliament recently, but it's not just the country's critical national infrastructure which may be underprepared to tackle the army of hackers and nation state-backed cyber criminals intent on causing it disruption.... [...]
