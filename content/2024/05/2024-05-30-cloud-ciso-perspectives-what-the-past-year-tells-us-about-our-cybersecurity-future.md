Title: Cloud CISO Perspectives: What the past year tells us about our cybersecurity future
Date: 2024-05-30T16:00:00+00:00
Author: Kevin Mandia
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2024-05-30-cloud-ciso-perspectives-what-the-past-year-tells-us-about-our-cybersecurity-future

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-what-the-past-year-tells-us-about-our-cybersecurity-future/){:target="_blank" rel="noopener"}

> Welcome to the second Cloud CISO Perspectives for May 2024. In this update, Mandiant founder and outgoing CEO Kevin Mandia shares the highlights from his keynote address at the RSA Conference earlier this month. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI Security and CISO, Google Cloud aside_block <ListValue: [StructValue([('title', 'Get vital CISO Insights with Google Cloud'), ('body', <wagtail.rich_text.RichText object at 0x3e5a345760d0>), ('btn_text', 'Visit the hub'), ('href', [...]
