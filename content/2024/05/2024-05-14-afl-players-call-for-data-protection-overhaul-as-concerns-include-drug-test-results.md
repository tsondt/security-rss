Title: AFL players call for data protection overhaul as concerns include drug test results
Date: 2024-05-14T09:00:13+00:00
Author: Jack Snape
Category: The Guardian
Tags: AFL;Australian rules football;Port Adelaide;Australia sport;Data and computer security;Cybercrime;Sport
Slug: 2024-05-14-afl-players-call-for-data-protection-overhaul-as-concerns-include-drug-test-results

[Source](https://www.theguardian.com/sport/article/2024/may/14/afl-players-data-protection-leak-breach-drug-test-results){:target="_blank" rel="noopener"}

> AFLPA want to protect medical records and performance data Port Adelaide players’ personal information was leaked last year A fear of illicit drug test results and psychologist session notes being leaked onto the dark web is helping drive a call from AFL players to improve data collection and storage in the sport. The leaking of Port Adelaide players’ personal information following a data breach late last year has awoken the industry to the risk of hackers, and the AFL Players Association (AFLPA) issued an urgent plea on Tuesday for an improvement in collection and storage practices. Continue reading... [...]
