Title: New Attack on VPNs
Date: 2024-05-07T15:32:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;VPN
Slug: 2024-05-07-new-attack-on-vpns

[Source](https://www.schneier.com/blog/archives/2024/05/new-attack-on-vpns.html){:target="_blank" rel="noopener"}

> This attack has been feasible for over two decades: Researchers have devised an attack against nearly all virtual private network applications that forces them to send and receive some or all traffic outside of the encrypted tunnel designed to protect it from snooping or tampering. TunnelVision, as the researchers have named their attack, largely negates the entire purpose and selling point of VPNs, which is to encapsulate incoming and outgoing Internet traffic in an encrypted tunnel and to cloak the user’s IP address. The researchers believe it affects all VPN applications when they’re connected to a hostile network and that [...]
