Title: Zoom adds post-quantum end-to-end encryption to video meetings
Date: 2024-05-21T09:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-21-zoom-adds-post-quantum-end-to-end-encryption-to-video-meetings

[Source](https://www.bleepingcomputer.com/news/security/zoom-adds-post-quantum-end-to-end-encryption-to-video-meetings/){:target="_blank" rel="noopener"}

> Zoom has announced the global availability of post-quantum end-to-end encryption (E2EE) for Zoom Meetings, with Zoom Phone and Zoom Rooms to follow soon. [...]
