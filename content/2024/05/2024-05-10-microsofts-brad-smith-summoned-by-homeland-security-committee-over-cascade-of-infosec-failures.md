Title: Microsoft's Brad Smith summoned by Homeland Security committee over 'cascade' of infosec failures
Date: 2024-05-10T15:01:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-10-microsofts-brad-smith-summoned-by-homeland-security-committee-over-cascade-of-infosec-failures

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/10/microsoft_president_brad_smith_summoned/){:target="_blank" rel="noopener"}

> Major intrusions by both China and Russia leave a lot to be answered for The US government wants to make Microsoft's vice chair and president, Brad Smith, the latest tech figurehead to field questions from a House committee on its recent cybersecurity failings.... [...]
