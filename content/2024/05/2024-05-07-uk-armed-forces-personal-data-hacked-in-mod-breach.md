Title: UK armed forces’ personal data hacked in MoD breach
Date: 2024-05-07T08:16:46+00:00
Author: Tom Ambrose and agency
Category: The Guardian
Tags: Hacking;Ministry of Defence;Data and computer security;UK news;China
Slug: 2024-05-07-uk-armed-forces-personal-data-hacked-in-mod-breach

[Source](https://www.theguardian.com/technology/article/2024/may/06/uk-military-personnels-data-hacked-in-mod-payroll-breach){:target="_blank" rel="noopener"}

> Defence secretary to address MPs after names and bank details of armed forces members targeted by unnamed attacker The Ministry of Defence has suffered a significant data breach and the personal information of UK military personnel has been hacked. A third-party payroll system used by the MoD, which includes names and bank details of current and past members of the armed forces, was targeted in the attack. A very small number of addresses may also have been accessed. Continue reading... [...]
