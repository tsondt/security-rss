Title: How Did Authorities Identify the Alleged Lockbit Boss?
Date: 2024-05-13T11:26:27+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Ne'er-Do-Well News;Ransomware;3k@xakep.ru;7.9521020220;antichat;Cerber;Constella Intelligence;d.horoshev@gmail.com;Dmitrij Ju Horoshev;Dmitry Yuriyevich Khoroshev;exploit;ICQ number 669316;Intel 471;khoroshev1@icloud.com;LockBit;LockBitSupp;NeroWolfe;pin@darktower.su;Putinkrab;ransomware-as-a-service;sitedev5@yandex.ru;stairwell.ru;tkaner.com;U.S. Department of the Treasury;Verified
Slug: 2024-05-13-how-did-authorities-identify-the-alleged-lockbit-boss

[Source](https://krebsonsecurity.com/2024/05/how-did-authorities-identify-the-alleged-lockbit-boss/){:target="_blank" rel="noopener"}

> Last week, the United States joined the U.K. and Australia in sanctioning and charging a Russian man named Dmitry Yuryevich Khoroshev as the leader of the infamous LockBit ransomware group. LockBit’s leader “ LockBitSupp ” claims the feds named the wrong guy, saying the charges don’t explain how they connected him to Khoroshev. This post examines the activities of Khoroshev’s many alter egos on the cybercrime forums, and tracks the career of a gifted malware author who has written and sold malicious code for the past 14 years. Dmitry Yuryevich Khoroshev. Image: treasury.gov. On May 7, the U.S. Department of [...]
