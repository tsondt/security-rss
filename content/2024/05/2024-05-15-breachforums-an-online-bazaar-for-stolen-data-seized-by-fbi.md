Title: BreachForums, an online bazaar for stolen data, seized by FBI
Date: 2024-05-15T22:37:55+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security
Slug: 2024-05-15-breachforums-an-online-bazaar-for-stolen-data-seized-by-fbi

[Source](https://arstechnica.com/?p=2024837){:target="_blank" rel="noopener"}

> Enlarge / The front page of BreachForums. The FBI and law enforcement partners worldwide have seized BreachForums, a website that openly trafficked malware and data stolen in hacks. The site has operated for years as an online trading post where criminals could buy and sell all kinds of compromised data, including passwords, customer records, and other often-times sensitive data. Last week, a site user advertised the sale of Dell customer data that was obtained from a support portal, forcing the computer maker to issue a vague warning to those affected. Also last week, Europol confirmed to Bleeping Computer that some [...]
