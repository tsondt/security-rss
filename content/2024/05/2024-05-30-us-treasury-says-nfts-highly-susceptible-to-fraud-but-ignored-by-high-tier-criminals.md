Title: US Treasury says NFTs 'highly susceptible' to fraud, but ignored by high-tier criminals
Date: 2024-05-30T21:47:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-05-30-us-treasury-says-nfts-highly-susceptible-to-fraud-but-ignored-by-high-tier-criminals

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/30/us_treasury_nfts/){:target="_blank" rel="noopener"}

> Narco kingpins aren't coming for your apes, but internet con artists still are The US Treasury Department has assessed the risk of non-fungible tokens (NFTs) being used for illicit finance, and has found them wanting for lack of proper roadblocks preventing illegal applications.... [...]
