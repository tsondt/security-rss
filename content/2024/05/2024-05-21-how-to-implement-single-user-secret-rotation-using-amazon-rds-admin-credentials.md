Title: How to implement single-user secret rotation using Amazon RDS admin credentials
Date: 2024-05-21T20:02:31+00:00
Author: Adithya Solai
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;Amazon RDS;Security;Security Blog
Slug: 2024-05-21-how-to-implement-single-user-secret-rotation-using-amazon-rds-admin-credentials

[Source](https://aws.amazon.com/blogs/security/how-to-implement-single-user-secret-rotation-using-amazon-rds-admin-credentials/){:target="_blank" rel="noopener"}

> You might have security or compliance standards that prevent a database user from changing their own credentials and from having multiple users with identical permissions. AWS Secrets Manager offers two rotation strategies for secrets that contain Amazon Relational Database Service (Amazon RDS) credentials: single-user and alternating-user. In the preceding scenario, neither single-user rotation nor alternating-user rotation would [...]
