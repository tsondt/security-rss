Title: ShinyHunters claims Santander breach, selling data for 30M customers
Date: 2024-05-31T11:47:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-05-31-shinyhunters-claims-santander-breach-selling-data-for-30m-customers

[Source](https://www.bleepingcomputer.com/news/security/shinyhunters-claims-santander-breach-selling-data-for-30m-customers/){:target="_blank" rel="noopener"}

> A threat actor known as ShinyHunters is claiming to be selling a massive trove of Santander Bank data, including information for 30 million customers, employees, and bank account data, two weeks after the bank reported a data breach. [...]
