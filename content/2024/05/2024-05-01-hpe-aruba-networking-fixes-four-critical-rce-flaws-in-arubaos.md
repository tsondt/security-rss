Title: HPE Aruba Networking fixes four critical RCE flaws in ArubaOS
Date: 2024-05-01T18:31:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-01-hpe-aruba-networking-fixes-four-critical-rce-flaws-in-arubaos

[Source](https://www.bleepingcomputer.com/news/security/hpe-aruba-networking-fixes-four-critical-rce-flaws-in-arubaos/){:target="_blank" rel="noopener"}

> HPE Aruba Networking has issued its April 2024 security advisory detailing critical remote code execution (RCE) vulnerabilities impacting multiple versions of ArubaOS, its proprietary network operating system. [...]
