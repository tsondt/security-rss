Title: Owner of Incognito dark web drugs market arrested in New York
Date: 2024-05-20T15:36:09-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-20-owner-of-incognito-dark-web-drugs-market-arrested-in-new-york

[Source](https://www.bleepingcomputer.com/news/security/owner-of-incognito-dark-web-drugs-market-arrested-in-new-york/){:target="_blank" rel="noopener"}

> The alleged owner and operator of Incognito Market, a dark web marketplace for selling illegal narcotics online, was arrested at the John F. Kennedy Airport in New York on May 18. [...]
