Title: How to issue use-case bound certificates with AWS Private CA
Date: 2024-05-30T13:34:29+00:00
Author: Chris Morris
Category: AWS Security
Tags: Advanced (300);AWS Private Certificate Authority;Security, Identity, & Compliance;Technical How-to;AWS Private CA;certificates;Security Blog
Slug: 2024-05-30-how-to-issue-use-case-bound-certificates-with-aws-private-ca

[Source](https://aws.amazon.com/blogs/security/how-to-issue-use-case-bound-certificates-with-aws-private-ca/){:target="_blank" rel="noopener"}

> In this post, we’ll show how you can use AWS Private Certificate Authority (AWS Private CA) to issue a wide range of X.509 certificates that are tailored for specific use cases. These use-case bound certificates have their intended purpose defined within the certificate components, such as the Key Usage and Extended Key usage extensions. We [...]
