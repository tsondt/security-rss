Title: Kaspersky hits back at claims its AI helped Russia develop military drone systems
Date: 2024-05-03T21:30:05+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-05-03-kaspersky-hits-back-at-claims-its-ai-helped-russia-develop-military-drone-systems

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/03/kaspersky_russia_military_drone_claims/){:target="_blank" rel="noopener"}

> Ready, set, sanctions? If volunteer intelligence gatherers are correct, the US may have a good reason to impose sanctions on Russian infosec firm Kaspersky, whose AI was allegedly used to help Russia produce drones for its war on Ukraine.... [...]
