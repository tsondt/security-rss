Title: These dangerous scammers don’t even bother to hide their crimes
Date: 2024-05-04T11:37:56+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Security;syndication
Slug: 2024-05-04-these-dangerous-scammers-dont-even-bother-to-hide-their-crimes

[Source](https://arstechnica.com/?p=2022009){:target="_blank" rel="noopener"}

> Enlarge (credit: Kuzmik_A/Getty Images ) Most scammers and cybercriminals operate in the digital shadows and don’t want you to know how they make money. But that’s not the case for the Yahoo Boys, a loose collective of young men in West Africa who are some of the web’s most prolific—and increasingly dangerous—scammers. Thousands of people are members of dozens of Yahoo Boy groups operating across Facebook, WhatsApp, and Telegram, a WIRED analysis has found. The scammers, who deal in types of fraud that total hundreds of millions of dollars each year, also have dozens of accounts on TikTok, YouTube, and [...]
