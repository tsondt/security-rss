Title: Pretty much all the headaches at MSPs stem from cybersecurity
Date: 2024-05-30T10:30:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-30-pretty-much-all-the-headaches-at-msps-stem-from-cybersecurity

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/30/msps_security_nightmare/){:target="_blank" rel="noopener"}

> More cybercrime means more problems as understaffed teams stretched to the limit Managed Service Partners (MSPs) say cybersecurity dwarfs all other main concerns about staying competitive in today's market.... [...]
