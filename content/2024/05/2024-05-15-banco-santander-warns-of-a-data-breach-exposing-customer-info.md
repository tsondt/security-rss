Title: Banco Santander warns of a data breach exposing customer info
Date: 2024-05-15T10:11:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-15-banco-santander-warns-of-a-data-breach-exposing-customer-info

[Source](https://www.bleepingcomputer.com/news/security/banco-santander-warns-of-a-data-breach-exposing-customer-info/){:target="_blank" rel="noopener"}

> Banco Santander S.A. announced it suffered a data breach impacting customers after an unauthorized actor accessed a database hosted by one of its third-party service providers. [...]
