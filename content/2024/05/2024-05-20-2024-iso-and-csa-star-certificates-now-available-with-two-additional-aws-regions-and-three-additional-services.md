Title: 2024 ISO and CSA STAR certificates now available with two additional AWS Regions and three additional services
Date: 2024-05-20T19:51:18+00:00
Author: Atulsing Patil
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS CSA STAR;AWS ISO;AWS ISO Certificates;AWS ISO20000;AWS ISO22301;AWS ISO27001;AWS ISO27017;AWS ISO27018;AWS ISO27701;AWS ISO9001;Security Blog
Slug: 2024-05-20-2024-iso-and-csa-star-certificates-now-available-with-two-additional-aws-regions-and-three-additional-services

[Source](https://aws.amazon.com/blogs/security/2024-iso-and-csa-star-certificates-now-available-with-two-additional-aws-regions-and-three-additional-services/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) successfully completed a special onboarding audit with no findings for ISO 9001:2015, 27001:2022, 27017:2015, 27018:2019, 27701:2019, 20000-1:2018, and 22301:2019, and Cloud Security Alliance (CSA) STAR Cloud Controls Matrix (CCM) v4.0. Ernst and Young CertifyPoint auditors conducted the audit and reissued the certificates on May 16, 2024. The objective of the audit [...]
