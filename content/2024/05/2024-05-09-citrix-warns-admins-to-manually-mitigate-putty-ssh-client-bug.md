Title: Citrix warns admins to manually mitigate PuTTY SSH client bug
Date: 2024-05-09T15:27:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-09-citrix-warns-admins-to-manually-mitigate-putty-ssh-client-bug

[Source](https://www.bleepingcomputer.com/news/security/citrix-warns-admins-to-manually-mitigate-putty-ssh-client-bug/){:target="_blank" rel="noopener"}

> Citrix notified customers this week to manually mitigate a PuTTY SSH client vulnerability that could allow attackers to steal a XenCenter admin's private SSH key. [...]
