Title: Big Tech is not much help when fighting a junta, and FOSS doesn't ride to the rescue
Date: 2024-05-21T03:35:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-05-21-big-tech-is-not-much-help-when-fighting-a-junta-and-foss-doesnt-ride-to-the-rescue

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/21/activist_tech_inadequate_myanmar/){:target="_blank" rel="noopener"}

> Opponents of Myanmar’s internet-nobbling military government don't like when Facebook asks for their real names Big Tech isn't much help if you're an activist trying to work against a military junta, and FOSS tools aren't a great alternative either, according to opponents of Myanmar's military regime.... [...]
