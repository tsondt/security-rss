Title: CISA says 'no more' to decades-old directory traversal bugs
Date: 2024-05-06T13:37:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-06-cisa-says-no-more-to-decades-old-directory-traversal-bugs

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/06/cisa_alert_dt_bugs/){:target="_blank" rel="noopener"}

> Recent attacks on healthcare thrust infosec agency into alert mode CISA is calling on the software industry to stamp out directory traversal vulnerabilities following recent high-profile exploits of the 20-year-old class of bugs.... [...]
