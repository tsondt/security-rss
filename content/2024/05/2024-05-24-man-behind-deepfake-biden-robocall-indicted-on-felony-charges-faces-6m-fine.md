Title: Man behind deepfake Biden robocall indicted on felony charges, faces $6M fine
Date: 2024-05-24T23:21:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-24-man-behind-deepfake-biden-robocall-indicted-on-felony-charges-faces-6m-fine

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/24/biden_robocall_charges/){:target="_blank" rel="noopener"}

> FCC wants to hit this political genius with first-of-a-kind punishment The political consultant who admitted paying $150 to create a deepfake anti-Biden robocall has been indicted on charges of felony voter suppression and misdemeanor impersonation of a candidate.... [...]
