Title: Aussie cops probe MediSecure's 'large-scale ransomware data breach'
Date: 2024-05-17T23:31:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-17-aussie-cops-probe-medisecures-large-scale-ransomware-data-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/17/medisecure_ransomware_attack/){:target="_blank" rel="noopener"}

> Throw another healthcare biz on the barby, mate Australian prescriptions provider MediSecure is the latest healthcare org to fall victim to a ransomware attack, with crooks apparently stealing patients' personal and health data.... [...]
