Title: FCC names and shames Royal Tiger AI robocall crew
Date: 2024-05-14T21:30:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-14-fcc-names-and-shames-royal-tiger-ai-robocall-crew

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/14/fcc_royal_tiger_robocalls/){:target="_blank" rel="noopener"}

> Agency is on the lookout for a Prince among men The US Federal Communications Commission has named its first robocall gang, dubbing the crew "Royal Tiger," and detailed its operations in an attempt to encourage international action against the scammers.... [...]
