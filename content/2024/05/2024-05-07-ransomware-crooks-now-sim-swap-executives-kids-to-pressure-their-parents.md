Title: Ransomware crooks now SIM swap executives' kids to pressure their parents
Date: 2024-05-07T02:10:30+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-07-ransomware-crooks-now-sim-swap-executives-kids-to-pressure-their-parents

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/07/ransomware_evolves_from_mere_extortion/){:target="_blank" rel="noopener"}

> Extortionists turning to 'psychological attacks', Mandiant CTO says RSAC Ransomware infections have morphed into "a psychological attack against the victim organization," as criminals use increasingly personal and aggressive tactics to force victims to pay up, according to Google-owned Mandiant.... [...]
