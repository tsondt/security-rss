Title: Google guru roasts useless phishing tests, calls for fire drill-style overhaul
Date: 2024-05-23T19:01:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-23-google-guru-roasts-useless-phishing-tests-calls-for-fire-drill-style-overhaul

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/23/google_phishing_tests/){:target="_blank" rel="noopener"}

> Current approaches aren't working and demonize security teams A Google security bigwig has had enough of federally mandated phishing tests, saying they make colleagues hate IT teams for no added benefit.... [...]
