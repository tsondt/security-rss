Title: LockBit dethroned as leading ransomware gang for first time post-takedown
Date: 2024-05-22T11:00:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-22-lockbit-dethroned-as-leading-ransomware-gang-for-first-time-post-takedown

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/22/lockbit_dethroned_as_leading_ransomware/){:target="_blank" rel="noopener"}

> Rivals ready to swoop in but drop in overall attacks illustrates LockBit’s influence The takedown of LockBit in February is starting to bear fruit for rival gangs with Play overtaking it after an eight-month period of LockBit topping the attack charts.... [...]
