Title: Go after UnitedHealth, not us, 100+ medical groups urge Uncle Sam
Date: 2024-05-22T22:05:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-22-go-after-unitedhealth-not-us-100-medical-groups-urge-uncle-sam

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/22/change_healthcare_hippa/){:target="_blank" rel="noopener"}

> Why should we get its paperwork? More than 100 medical industry groups have asked the Feds to make UnitedHealth Group, not them, go through the rigmarole of notifying everyone about the Change Healthcare ransomware infection.... [...]
