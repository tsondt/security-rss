Title: Treasury Sanctions Creators of 911 S5 Proxy Botnet
Date: 2024-05-28T20:38:32+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Other;911 S5;911s5;Biz Pattaya Group Company Limited;DewVPN;Jingping Liu;Lily Suites Company Limited;MaskVPN;PaladinVPN;proxy;residential proxies;Riley Kilmer;spur.us;U.S. Department of the Treasury;Yanni Zheng;Yunhe Wang
Slug: 2024-05-28-treasury-sanctions-creators-of-911-s5-proxy-botnet

[Source](https://krebsonsecurity.com/2024/05/treasury-sanctions-creators-of-911-s5-proxy-botnet/){:target="_blank" rel="noopener"}

> The U.S. Department of the Treasury today unveiled sanctions against three Chinese nationals for allegedly operating 911 S5, an online anonymity service that for many years was the easiest and cheapest way to route one’s Web traffic through malware-infected computers around the globe. KrebsOnSecurity identified one of the three men in a July 2022 investigation into 911 S5, which was massively hacked and then closed ten days later. The 911 S5 botnet-powered proxy service, circa July 2022. From 2015 to July 2022, 911 S5 sold access to hundreds of thousands of Microsoft Windows computers daily, as “proxies” that allowed customers [...]
