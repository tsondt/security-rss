Title: More FedRAMP High authorized services are now available in Assured Workloads
Date: 2024-05-24T16:00:00+00:00
Author: Archana Ramamoorthy
Category: GCP Security
Tags: Security & Identity
Slug: 2024-05-24-more-fedramp-high-authorized-services-are-now-available-in-assured-workloads

[Source](https://cloud.google.com/blog/products/identity-security/more-fedramp-high-authorized-services-are-now-available-in-assured-workloads/){:target="_blank" rel="noopener"}

> Google Cloud's commitment to empower federal agencies with advanced technology reached a significant milestone this week with the addition of more than 100 new FedRAMP High authorized cloud services. Customers in the U.S. government and commercial organizations supplying the U.S. government can now use popular Google Cloud products authorized for FedRAMP. These products include several Vertex AI services, Cloud Build and Cloud Run, Cloud Filestore, as well as powerful security controls on our secure-by-design, secure-by-default cloud platform such as VPC Service Controls, Cloud Armor, Cloud Load Balancing services, and reCAPTCHA. Several government agencies are already taking advantage of Google AI [...]
