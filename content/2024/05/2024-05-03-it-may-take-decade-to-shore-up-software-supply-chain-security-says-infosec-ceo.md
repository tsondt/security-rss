Title: It may take decade to shore up software supply chain security, says infosec CEO
Date: 2024-05-03T17:30:15+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-05-03-it-may-take-decade-to-shore-up-software-supply-chain-security-says-infosec-ceo

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/03/it_might_take_a_decade/){:target="_blank" rel="noopener"}

> Sure, we're waking to the risk, but we gotta get outta bed, warns Endor Labs founder Varun Badhwar interview The more cybersecurity news you read, the more often you seem to see a familiar phrase: Software supply chain (SSC) vulnerabilities. Varun Badhwar, founder and CEO at security firm Endor Labs, doesn't believe that's by coincidence.... [...]
