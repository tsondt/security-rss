Title: Zero-Trust DNS
Date: 2024-05-16T11:03:58+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;DNS;Microsoft
Slug: 2024-05-16-zero-trust-dns

[Source](https://www.schneier.com/blog/archives/2024/05/zero-trust-dns.html){:target="_blank" rel="noopener"}

> Microsoft is working on a promising-looking protocol to lock down DNS. ZTDNS aims to solve this decades-old problem by integrating the Windows DNS engine with the Windows Filtering Platform—the core component of the Windows Firewall—directly into client devices. Jake Williams, VP of research and development at consultancy Hunter Strategy, said the union of these previously disparate engines would allow updates to be made to the Windows firewall on a per-domain name basis. The result, he said, is a mechanism that allows organizations to, in essence, tell clients “only use our DNS server, that uses TLS, and will only resolve certain [...]
