Title: US senator claims UnitedHealth's CEO, board appointed 'unqualified' CISO
Date: 2024-05-31T21:29:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-31-us-senator-claims-unitedhealths-ceo-board-appointed-unqualified-ciso

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/31/ron_wyden_letter_unitedhealth/){:target="_blank" rel="noopener"}

> Similar cases have resulted in serious sanctions, and they were on a far smaller scale Serial tech and digital privacy critic Senator Ron Wyden (D-OR) laid into UnitedHealth Group's (UHG) CEO for appointing a CISO Wyden deemed "unqualified"– a decision he claims likely led to its ransomware catastrophe of late.... [...]
