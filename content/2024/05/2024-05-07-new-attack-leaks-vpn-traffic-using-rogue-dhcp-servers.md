Title: New attack leaks VPN traffic using rogue DHCP servers
Date: 2024-05-07T14:46:52-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2024-05-07-new-attack-leaks-vpn-traffic-using-rogue-dhcp-servers

[Source](https://www.bleepingcomputer.com/news/security/new-tunnelvision-attack-leaks-vpn-traffic-using-rogue-dhcp-servers/){:target="_blank" rel="noopener"}

> A new attack dubbed "TunnelVision" can route traffic outside a VPN's encryption tunnel, allowing attackers to snoop on unencrypted traffic while maintaining the appearance of a secure VPN connection. [...]
