Title: AWS completes the 2024 Cyber Essentials Plus certification
Date: 2024-05-29T15:43:06+00:00
Author: Tariro Dongo
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS Compliance;AWS security;cybersecurity;Security Blog;United Kingdom
Slug: 2024-05-29-aws-completes-the-2024-cyber-essentials-plus-certification

[Source](https://aws.amazon.com/blogs/security/aws-completes-the-2024-cyber-essentials-plus-certification/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce the successful renewal of the United Kingdom Cyber Essentials Plus certification. The Cyber Essentials Plus certificate is valid for one year until March 22, 2025. Cyber Essentials Plus is a UK Government–backed, industry-supported certification scheme intended to help organizations demonstrate controls against common cyber security threats. An [...]
