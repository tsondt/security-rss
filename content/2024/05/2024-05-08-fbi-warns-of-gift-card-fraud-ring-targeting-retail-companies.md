Title: FBI warns of gift card fraud ring targeting retail companies
Date: 2024-05-08T13:25:55-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-05-08-fbi-warns-of-gift-card-fraud-ring-targeting-retail-companies

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-gift-card-fraud-ring-targeting-retail-companies/){:target="_blank" rel="noopener"}

> The FBI warned retail companies in the United States that a financially motivated hacking group has been targeting employees in their gift card departments in phishing attacks since at least January 2024. [...]
