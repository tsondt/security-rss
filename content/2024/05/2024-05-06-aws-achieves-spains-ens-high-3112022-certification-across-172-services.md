Title: AWS achieves Spain’s ENS High 311/2022 certification across 172 services
Date: 2024-05-06T13:23:36+00:00
Author: Daniel Fuertes
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS security;CCN;Certificate;Certification;Compliance;Compliance reports;cybersecurity;ENS;ENS High Standard;España;Esquema Nacional de Seguridad;High;Public Sector;report;Security Blog;Spain
Slug: 2024-05-06-aws-achieves-spains-ens-high-3112022-certification-across-172-services

[Source](https://aws.amazon.com/blogs/security/aws-achieves-spains-ens-high-311-2022-certification-across-172-services/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) has recently renewed the Esquema Nacional de Seguridad (ENS) High certification, upgrading to the latest version regulated under Royal Decree 311/2022. The ENS establishes security standards that apply to government agencies and public organizations in Spain and service providers on which Spanish public services depend. This security framework has gone through [...]
