Title: City of Wichita breach claimed by LockBit ransomware gang
Date: 2024-05-08T12:16:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-08-city-of-wichita-breach-claimed-by-lockbit-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/city-of-wichita-breach-claimed-by-lockbit-ransomware-gang/){:target="_blank" rel="noopener"}

> The LockBit ransomware gang has claimed responsibility for a disruptive cyberattack on the City of Wichita, which has forced the City's authorities to shut down IT systems used for online bill payment, including court fines, water bills, and public transportation. [...]
