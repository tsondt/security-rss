Title: Cybercriminals raid BBC pension database, steal records of over 25,000 people
Date: 2024-05-30T14:02:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-30-cybercriminals-raid-bbc-pension-database-steal-records-of-over-25000-people

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/30/cybercriminals_raid_bbc_pension_database/){:target="_blank" rel="noopener"}

> This just in: We lost your personal info, but here's 2 years' worth of Experian The BBC has emailed more than 25,000 current and former employees on one of its pension schemes after an unauthorized party broke into a database and stole their personal data.... [...]
