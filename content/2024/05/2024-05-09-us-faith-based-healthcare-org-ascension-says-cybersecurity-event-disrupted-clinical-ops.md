Title: US faith-based healthcare org Ascension says 'cybersecurity event' disrupted clinical ops
Date: 2024-05-09T19:15:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-09-us-faith-based-healthcare-org-ascension-says-cybersecurity-event-disrupted-clinical-ops

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/09/us_faithbased_healthcare_org_ascension/){:target="_blank" rel="noopener"}

> Sources claim ransomware is to blame Healthcare organization Ascension is the latest of its kind in the US to say its network has been affected by what it believes to be a "cybersecurity event."... [...]
