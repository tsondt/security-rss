Title: North Korea building cash reserves using ransomware, video games
Date: 2024-05-29T13:00:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-29-north-korea-building-cash-reserves-using-ransomware-video-games

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/29/north_korea_using_ransomware_and/){:target="_blank" rel="noopener"}

> Microsoft says Kim’s hermit nation is pivoting to latest tools as it evolves in cyberspace A brand-new cybercrime group that Microsoft ties to North Korea is tricking targets using fake job opportunities to launch malware and ransomware, all for financial gain.... [...]
