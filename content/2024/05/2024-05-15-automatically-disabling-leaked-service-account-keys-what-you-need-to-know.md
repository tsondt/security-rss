Title: Automatically disabling leaked service account keys: What you need to know
Date: 2024-05-15T16:00:00+00:00
Author: Allan Martucci
Category: GCP Security
Tags: Security & Identity
Slug: 2024-05-15-automatically-disabling-leaked-service-account-keys-what-you-need-to-know

[Source](https://cloud.google.com/blog/products/identity-security/automatically-disabling-leaked-service-account-keys-what-you-need-to-know/){:target="_blank" rel="noopener"}

> At Google, we’re always evolving our security capabilities and practices to make our cloud the most trusted cloud. As part of this continued effort, Google Cloud’s Identity and Access Management (IAM) recently released even stronger security defaults that can help you bolster the baseline security and control of your cloud environment. We offer several options when you need to allow external applications to access Google Cloud APIs and resources including guidance on how to choose a more secure alternative to service account keys. However, many organizations still depend on service account keys for external authentication, which can create security issues [...]
