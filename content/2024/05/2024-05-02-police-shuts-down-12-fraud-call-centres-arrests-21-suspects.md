Title: Police shuts down 12 fraud call centres, arrests 21 suspects
Date: 2024-05-02T14:21:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-02-police-shuts-down-12-fraud-call-centres-arrests-21-suspects

[Source](https://www.bleepingcomputer.com/news/security/police-shuts-down-12-fraud-call-centres-arrests-21-suspects/){:target="_blank" rel="noopener"}

> Law enforcement shut down 12 phone fraud call centers in Albania, Bosnia and Herzegovina, Kosovo, and Lebanon, behind thousands of scam calls daily. [...]
