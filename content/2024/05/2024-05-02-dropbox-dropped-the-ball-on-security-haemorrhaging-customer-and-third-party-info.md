Title: Dropbox dropped the ball on security, haemorrhaging customer and third-party info
Date: 2024-05-02T00:58:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-05-02-dropbox-dropped-the-ball-on-security-haemorrhaging-customer-and-third-party-info

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/02/dropbox_sign_attack/){:target="_blank" rel="noopener"}

> Only from its digital doc-signing service, which is isolated from its cloudy storage Dropbox has revealed a major attack on its systems that saw customers' personal information accessed by unknown and unauthorized entities.... [...]
