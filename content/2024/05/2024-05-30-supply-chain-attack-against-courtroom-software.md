Title: Supply Chain Attack against Courtroom Software
Date: 2024-05-30T11:04:43+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;backdoors;courts;supply chain
Slug: 2024-05-30-supply-chain-attack-against-courtroom-software

[Source](https://www.schneier.com/blog/archives/2024/05/supply-chain-attack-against-courtroom-software.html){:target="_blank" rel="noopener"}

> No word on how this backdoor was installed: A software maker serving more than 10,000 courtrooms throughout the world hosted an application update containing a hidden backdoor that maintained persistent communication with a malicious website, researchers reported Thursday, in the latest episode of a supply-chain attack. The software, known as the JAVS Viewer 8, is a component of the JAVS Suite 8, an application package courtrooms use to record, play back, and manage audio and video from proceedings. Its maker, Louisville, Kentucky-based Justice AV Solutions, says its products are used in more than 10,000 courtrooms throughout the US and 11 [...]
