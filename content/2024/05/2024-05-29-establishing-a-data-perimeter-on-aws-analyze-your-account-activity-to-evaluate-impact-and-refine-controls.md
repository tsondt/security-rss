Title: Establishing a data perimeter on AWS: Analyze your account activity to evaluate impact and refine controls
Date: 2024-05-29T20:19:27+00:00
Author: Achraf Moussadek-Kabdani
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;Data protection;Identity;Network security;Security Blog;service control policies
Slug: 2024-05-29-establishing-a-data-perimeter-on-aws-analyze-your-account-activity-to-evaluate-impact-and-refine-controls

[Source](https://aws.amazon.com/blogs/security/establishing-a-data-perimeter-on-aws-analyze-your-account-activity-to-evaluate-impact-and-refine-controls/){:target="_blank" rel="noopener"}

> A data perimeter on Amazon Web Services (AWS) is a set of preventive controls you can use to help establish a boundary around your data in AWS Organizations. This boundary helps ensure that your data can be accessed only by trusted identities from within networks you expect and that the data cannot be transferred outside [...]
