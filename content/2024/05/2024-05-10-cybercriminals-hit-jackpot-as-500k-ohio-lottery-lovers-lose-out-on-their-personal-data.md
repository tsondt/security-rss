Title: Cybercriminals hit jackpot as 500k+ Ohio Lottery lovers lose out on their personal data
Date: 2024-05-10T18:15:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-10-cybercriminals-hit-jackpot-as-500k-ohio-lottery-lovers-lose-out-on-their-personal-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/10/cybercriminals_hit_jackpot_as_over/){:target="_blank" rel="noopener"}

> Not a lotto luck for these powerball hunters More than half a million gamblers with a penchant for powerballs will be receiving some fairly unwelcome news very soon, if not already, as cybercriminals have made off with their personal data.... [...]
