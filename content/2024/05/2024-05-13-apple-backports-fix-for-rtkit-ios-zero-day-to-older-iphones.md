Title: Apple backports fix for RTKit iOS zero-day to older iPhones
Date: 2024-05-13T17:47:02-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Apple;Security
Slug: 2024-05-13-apple-backports-fix-for-rtkit-ios-zero-day-to-older-iphones

[Source](https://www.bleepingcomputer.com/news/apple/apple-backports-fix-for-rtkit-ios-zero-day-to-older-iphones/){:target="_blank" rel="noopener"}

> Apple has backported security patches released in March to older iPhones and iPads, fixing an iOS Kernel zero-day tagged as exploited in attacks. [...]
