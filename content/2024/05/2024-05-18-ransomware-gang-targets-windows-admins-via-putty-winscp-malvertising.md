Title: Ransomware gang targets Windows admins via PuTTy, WinSCP malvertising
Date: 2024-05-18T14:23:11-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-05-18-ransomware-gang-targets-windows-admins-via-putty-winscp-malvertising

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-targets-windows-admins-via-putty-winscp-malvertising/){:target="_blank" rel="noopener"}

> A ransomware operation targets Windows system administrators by taking out Google ads to promote fake download sites for Putty and WinSCP. [...]
