Title: Bitbucket artifact files can leak plaintext authentication secrets
Date: 2024-05-21T15:05:46-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-21-bitbucket-artifact-files-can-leak-plaintext-authentication-secrets

[Source](https://www.bleepingcomputer.com/news/security/bitbucket-artifact-files-can-leak-plaintext-authentication-secrets/){:target="_blank" rel="noopener"}

> Threat actors were found breaching AWS accounts using authentication secrets leaked as plaintext in Atlassian Bitbucket artifact objects. [...]
