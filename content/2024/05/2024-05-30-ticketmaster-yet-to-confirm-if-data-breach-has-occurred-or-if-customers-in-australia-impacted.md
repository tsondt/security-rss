Title: Ticketmaster yet to confirm if data breach has occurred or if customers in Australia impacted
Date: 2024-05-30T04:38:15+00:00
Author: Jordyn Beazley
Category: The Guardian
Tags: Australia news;Data and computer security;Cybercrime;Technology
Slug: 2024-05-30-ticketmaster-yet-to-confirm-if-data-breach-has-occurred-or-if-customers-in-australia-impacted

[Source](https://www.theguardian.com/australia-news/article/2024/may/30/ticketmaster-hack-data-breach-australia-yet-to-confirm){:target="_blank" rel="noopener"}

> The notorious ShinyHunters collective is claiming hack of personal details of 560 million global customers Follow our Australia news live blog for latest updates Get our morning and afternoon news emails, free app or daily news podcast Ticketmaster is yet to confirm whether it has experienced a major data breach or if Australians are impacted, after a notorious hacker collective claimed it had the personal details of millions of the ticketing giant’s global customers for sale. The collective, ShinyHunters, claimed on Wednesday on the dark web it had the personal details of 560 million Ticketmaster customers available for a one-time [...]
