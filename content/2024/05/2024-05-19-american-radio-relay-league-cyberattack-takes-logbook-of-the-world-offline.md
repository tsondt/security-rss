Title: American Radio Relay League cyberattack takes Logbook of the World offline
Date: 2024-05-19T17:15:05-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-05-19-american-radio-relay-league-cyberattack-takes-logbook-of-the-world-offline

[Source](https://www.bleepingcomputer.com/news/security/arrl-cyberattack-takes-logbook-of-the-world-offline/){:target="_blank" rel="noopener"}

> The American Radio Relay League (ARRL) warns it suffered a cyberattack, which disrupted its IT systems and online operations, including email and the Logbook of the World. [...]
