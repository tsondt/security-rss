Title: Uncle Sam urges action after Black Basta ransomware infects Ascension
Date: 2024-05-13T18:30:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-13-uncle-sam-urges-action-after-black-basta-ransomware-infects-ascension

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/13/cisa_ascension_ransomware/){:target="_blank" rel="noopener"}

> Emergency ambulances diverted while techies restore systems US information security agencies have published advisories on how to detect and thwart the Black Basta ransomware gang – after the crew claimed responsibility for the recent attack on US healthcare provider Ascension.... [...]
