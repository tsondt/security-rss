Title: Brit security guard biz exposes 1.2M files via unprotected database
Date: 2024-05-07T10:30:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-07-brit-security-guard-biz-exposes-12m-files-via-unprotected-database

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/07/uk_security_company_breach/){:target="_blank" rel="noopener"}

> Thousands of ID cards plus CCTV snaps of suspects found online Exclusive A UK-based physical security business let its guard down, exposing nearly 1.3 million documents via a public-facing database, according to an infosec researcher.... [...]
