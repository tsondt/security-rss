Title: IT worker sued over ‘vengeful’ cyber harassment of policeman who issued a jaywalking ticket
Date: 2024-05-30T13:00:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-30-it-worker-sued-over-vengeful-cyber-harassment-of-policeman-who-issued-a-jaywalking-ticket

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/30/it_worker_defamation_lawsuit_police/){:target="_blank" rel="noopener"}

> His hospital employer also hit with lawsuit for not stepping in sooner In an ongoing civil lawsuit, an IT worker is accused of launching a "destructive cyber campaign of hate and revenge" against a police officer and his family after being issued a ticket for jaywalking.... [...]
