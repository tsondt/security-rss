Title: Is Your Computer Part of ‘The Largest Botnet Ever?’
Date: 2024-05-29T19:21:12+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Other;911 S5;Brett Leatherman;Cloud Router;DewVPN;MaskVPN;PaladinVPN;Proxygate;Shield VPN;Shine VPN;U.S. Department of the Treasury;Yunhe Wang
Slug: 2024-05-29-is-your-computer-part-of-the-largest-botnet-ever

[Source](https://krebsonsecurity.com/2024/05/is-your-computer-part-of-the-largest-botnet-ever/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice (DOJ) today said they arrested the alleged operator of 911 S5, a ten-year-old online anonymity service that was powered by what the director of the FBI called “likely the world’s largest botnet ever.” The arrest coincided with the seizure of the 911 S5 website and supporting infrastructure, which the government says turned computers running various “free VPN” products into Internet traffic relays that facilitated billions of dollars in online fraud and cybercrime. The Cloud Router homepage, which was seized by the FBI this past weekend. Cloud Router was previously called 911 S5. On May 24, [...]
