Title: US arrests suspects behind $73M ‘pig butchering’ laundering scheme
Date: 2024-05-17T11:57:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-05-17-us-arrests-suspects-behind-73m-pig-butchering-laundering-scheme

[Source](https://www.bleepingcomputer.com/news/security/us-arrests-suspects-behind-73m-pig-butchering-laundering-scheme/){:target="_blank" rel="noopener"}

> ​The U.S. Department of Justice charged two suspects for allegedly leading a crime ring that laundered at least $73 million from cryptocurrency investment scams, also known as "pig butchering." [...]
