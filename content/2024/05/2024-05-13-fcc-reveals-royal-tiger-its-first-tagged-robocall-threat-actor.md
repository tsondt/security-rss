Title: FCC reveals Royal Tiger, its first tagged robocall threat actor
Date: 2024-05-13T16:45:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-13-fcc-reveals-royal-tiger-its-first-tagged-robocall-threat-actor

[Source](https://www.bleepingcomputer.com/news/security/fcc-reveals-royal-tiger-its-first-tagged-robocall-threat-actor/){:target="_blank" rel="noopener"}

> The Federal Communications Commission (FCC) has named its first officially designated robocall threat actor 'Royal Tiger,' a move aiming to help international partners and law enforcement more easily track individuals and entities behind repeat robocall campaigns. [...]
