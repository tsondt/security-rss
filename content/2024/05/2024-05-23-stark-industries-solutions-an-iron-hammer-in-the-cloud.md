Title: Stark Industries Solutions: An Iron Hammer in the Cloud
Date: 2024-05-23T23:32:43+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;DDoS-for-Hire;Russia's War on Ukraine;Andrey Nesterenko;arbor;AS44477;Blue Charlie;Callisto Group;COLDRIVER;Comcast Cable Communications;Computer Technologies Institute Ltd;Constella Intelligence;Correctiv.org;DDoSia;dfyz;dfyz_bk@bk.ru;DON CHICHO;EGIhosting;ESET;Federal State Autonomous Educational Establishment of Additional Professional Education Center of Realization of State Educational Policy and Informational Technologies;Green Floid LLC;Information Technology Laboratories Group;Innovation IT Solutions Corp;Integrated Technologies Laboratory;Intel 471;Internet Research Agency;ITL LLC;jeffrey carr;LockBit;Max Tulyev;MercenarieS TeaM;MIRhosting;NetAssist;NETSCOUT;NoName057(16);Perfect Quality Hosting;PQ Hosting Plus S.R.L.;Prolocation;Proxyline;Raymond Dijkxhoorn;Recorded Future;Richard Hummel;SEABORGIUM;Serverius-as;spamhaus;Stark Industries Solutions;SURBL;team cymru;Ukrinform
Slug: 2024-05-23-stark-industries-solutions-an-iron-hammer-in-the-cloud

[Source](https://krebsonsecurity.com/2024/05/stark-industries-solutions-an-iron-hammer-in-the-cloud/){:target="_blank" rel="noopener"}

> The homepage of Stark Industries Solutions. Two weeks before Russia invaded Ukraine in February 2022, a large, mysterious new Internet hosting firm called Stark Industries Solutions materialized and quickly became the epicenter of massive distributed denial-of-service (DDoS) attacks on government and commercial targets in Ukraine and Europe. An investigation into Stark Industries reveals it is being used as a global proxy network that conceals the true source of cyberattacks and disinformation campaigns against enemies of Russia. At least a dozen patriotic Russian hacking groups have been launching DDoS attacks since the start of the war at a variety of targets [...]
