Title: Mastodon delays firm fix for link previews DDoSing sites
Date: 2024-05-06T19:50:11+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-05-06-mastodon-delays-firm-fix-for-link-previews-ddosing-sites

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/06/mastodon_delays_fix_ddos/){:target="_blank" rel="noopener"}

> Decentralization is great until everyone wants to grab data from your web server Updated Mastodon has pushed back an update that's expected to fully address the issue of link previews sparking accidental distributed denial of service (DDoS) attacks.... [...]
