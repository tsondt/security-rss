Title: GhostEngine mining attacks kill EDR security using vulnerable drivers
Date: 2024-05-21T18:30:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-21-ghostengine-mining-attacks-kill-edr-security-using-vulnerable-drivers

[Source](https://www.bleepingcomputer.com/news/security/ghostengine-mining-attacks-kill-edr-security-using-vulnerable-drivers/){:target="_blank" rel="noopener"}

> A malicious crypto mining campaign codenamed 'REF4578,' has been discovered deploying a malicious payload named GhostEngine that uses vulnerable drivers to turn off security products and deploy an XMRig miner. [...]
