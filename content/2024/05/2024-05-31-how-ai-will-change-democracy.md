Title: How AI Will Change Democracy
Date: 2024-05-31T11:04:01+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;democracy;laws;LLM
Slug: 2024-05-31-how-ai-will-change-democracy

[Source](https://www.schneier.com/blog/archives/2024/05/how-ai-will-change-democracy.html){:target="_blank" rel="noopener"}

> I don’t think it’s an exaggeration to predict that artificial intelligence will affect every aspect of our society. Not by doing new things. But mostly by doing things that are already being done by humans, perfectly competently. Replacing humans with AIs isn’t necessarily interesting. But when an AI takes over a human task, the task changes. In particular, there are potential changes over four dimensions: Speed, scale, scope and sophistication. The problem with AIs trading stocks isn’t that they’re better than humans—it’s that they’re faster. But computers are better at chess and Go because they use more sophisticated strategies than [...]
