Title: City of Wichita shuts down IT network after ransomware attack
Date: 2024-05-06T10:34:36-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-05-06-city-of-wichita-shuts-down-it-network-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/city-of-wichita-shuts-down-it-network-after-ransomware-attack/){:target="_blank" rel="noopener"}

> The City of Wichita, Kansas, disclosed it was forced to shut down portions of its network after suffering a weekend ransomware attack. [...]
