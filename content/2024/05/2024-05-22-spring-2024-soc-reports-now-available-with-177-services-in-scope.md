Title: Spring 2024 SOC reports now available with 177 services in scope
Date: 2024-05-22T13:24:02+00:00
Author: Brownell Combs
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS SOC Reports;Compliance;Security Blog;SOC
Slug: 2024-05-22-spring-2024-soc-reports-now-available-with-177-services-in-scope

[Source](https://aws.amazon.com/blogs/security/spring-2024-soc-reports-now-available-with-177-services-in-scope/){:target="_blank" rel="noopener"}

> We continue to expand the scope of our assurance programs at Amazon Web Services (AWS) and are pleased to announce that the Spring 2024 System and Organization Controls (SOC) 1, 2, and 3 reports are now available. The reports cover the 12-month period from April 1, 2023 to March 31, 2024, so that customers have [...]
