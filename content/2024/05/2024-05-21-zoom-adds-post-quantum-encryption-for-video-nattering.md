Title: Zoom adds 'post-quantum' encryption for video nattering
Date: 2024-05-21T19:45:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-21-zoom-adds-post-quantum-encryption-for-video-nattering

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/21/zoom_postquantum_e2ee/){:target="_blank" rel="noopener"}

> Guess we all have imaginary monsters to fear Zoom has rolled out what it claims is post-quantum end-to-end encryption (E2EE) for video conferencing, saying it will make it available for Phone and Rooms "soon."... [...]
