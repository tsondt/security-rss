Title: DoJ, ByteDance ask court: Hurry up and rule on TikTok ban already
Date: 2024-05-20T13:30:09+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-05-20-doj-bytedance-ask-court-hurry-up-and-rule-on-tiktok-ban-already

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/20/doj_and_bytedance_ask_court/){:target="_blank" rel="noopener"}

> Forced selloff case will likely be appealed again... see you in (Supreme) court The US Department of Justice and Bytedance spent a rare moment unified on Friday when the duo asked for a fast-tracked court schedule for the Chinese short video apps divest or ban case.... [...]
