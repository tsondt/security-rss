Title: A sneak peek at the data protection sessions for re:Inforce 2024
Date: 2024-05-16T18:49:04+00:00
Author: Katie Collins
Category: AWS Security
Tags: Announcements;AWS re:Inforce;Events;Foundational (100);Security, Identity, & Compliance;Live Events;Security;Security Blog
Slug: 2024-05-16-a-sneak-peek-at-the-data-protection-sessions-for-reinforce-2024

[Source](https://aws.amazon.com/blogs/security/a-sneak-peek-at-the-data-protection-sessions-for-reinforce-2024/){:target="_blank" rel="noopener"}

> Join us in Philadelphia, Pennsylvania on June 10–12, 2024 for AWS re:Inforce, a security learning conference where you can gain skills and confidence in cloud security, compliance, identity, and privacy. As an attendee, you have access to hundreds of technical and non-technical sessions, an Expo featuring Amazon Web Services (AWS) experts and AWS Security Competency [...]
