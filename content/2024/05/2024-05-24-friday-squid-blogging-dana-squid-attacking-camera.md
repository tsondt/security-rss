Title: Friday Squid Blogging: Dana Squid Attacking Camera
Date: 2024-05-24T21:03:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid;video
Slug: 2024-05-24-friday-squid-blogging-dana-squid-attacking-camera

[Source](https://www.schneier.com/blog/archives/2024/05/friday-squid-blogging-dana-squid-attacking-camera.html){:target="_blank" rel="noopener"}

> Fantastic footage of a Dana squid attacking a camera at a depth of about a kilometer. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
