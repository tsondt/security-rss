Title: BreachForums returns just weeks after FBI-led takedown
Date: 2024-05-28T18:45:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-28-breachforums-returns-just-weeks-after-fbi-led-takedown

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/28/breachforums_back_online/){:target="_blank" rel="noopener"}

> Website whack-a-mole getting worse BreachForums is back online just weeks after the notorious dark-web marketplace for stolen data was seized by law enforcement.... [...]
