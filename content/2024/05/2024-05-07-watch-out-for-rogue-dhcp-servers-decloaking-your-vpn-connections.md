Title: Watch out for rogue DHCP servers decloaking your VPN connections
Date: 2024-05-07T21:50:23+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-05-07-watch-out-for-rogue-dhcp-servers-decloaking-your-vpn-connections

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/07/vpn_tunnelvision_dhcp/){:target="_blank" rel="noopener"}

> Avoid traffic-redirecting snoops who have TunnelVision A newly discovered vulnerability undermines countless VPN clients in that their traffic can be quietly routed away from their encrypted tunnels and intercepted by snoops on the network.... [...]
