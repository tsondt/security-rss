Title: Chinese hackers hide on military and govt networks for 6 years
Date: 2024-05-22T09:25:37-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-22-chinese-hackers-hide-on-military-and-govt-networks-for-6-years

[Source](https://www.bleepingcomputer.com/news/security/unfading-sea-haze-hackers-hide-on-military-and-govt-networks-for-6-years/){:target="_blank" rel="noopener"}

> A previously unknown threat actor dubbed "Unfading Sea Haze" has been targeting military and government entities in the South China Sea region since 2018, remaining undetected all this time. [...]
