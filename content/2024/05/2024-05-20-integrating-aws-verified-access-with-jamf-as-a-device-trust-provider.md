Title: Integrating AWS Verified Access with Jamf as a device trust provider
Date: 2024-05-20T14:39:51+00:00
Author: Mayank Gupta
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2024-05-20-integrating-aws-verified-access-with-jamf-as-a-device-trust-provider

[Source](https://aws.amazon.com/blogs/security/integrating-aws-verified-access-with-jamf-as-a-device-trust-provider/){:target="_blank" rel="noopener"}

> In this post, we discuss how to architect Zero Trust based remote connectivity to your applications hosted within Amazon Web Services (AWS). Specifically, we show you how to integrate AWS Verified Access with Jamf as a device trust provider. This post is an extension of our previous post explaining how to integrate AWS Verified Access [...]
