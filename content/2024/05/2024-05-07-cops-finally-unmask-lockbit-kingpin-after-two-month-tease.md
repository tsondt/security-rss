Title: Cops finally unmask 'LockBit kingpin' after two-month tease
Date: 2024-05-07T15:08:58+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-07-cops-finally-unmask-lockbit-kingpin-after-two-month-tease

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/07/alleged_lockbit_kingpin_charged_sanctioned/){:target="_blank" rel="noopener"}

> Dmitry Yuryevich Khoroshev's $10M question is answered at last Updated Police have finally named who they firmly believe is the kingpin of the LockBit ransomware ring: Dmitry Yuryevich Khoroshev.... [...]
