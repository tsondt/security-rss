Title: US govt warns of pro-Russian hacktivists targeting water facilities
Date: 2024-05-01T15:14:36-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-05-01-us-govt-warns-of-pro-russian-hacktivists-targeting-water-facilities

[Source](https://www.bleepingcomputer.com/news/security/us-govt-warns-of-pro-russian-hacktivists-targeting-water-facilities/){:target="_blank" rel="noopener"}

> The US government is warning that pro-Russian hacktivists are seeking out and hacking into unsecured operational technology (OT) systems used to disrupt critical infrastructure operations. [...]
