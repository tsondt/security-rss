Title: Data of 560 million Ticketmaster customers for sale after alleged breach
Date: 2024-05-30T16:34:09-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-30-data-of-560-million-ticketmaster-customers-for-sale-after-alleged-breach

[Source](https://www.bleepingcomputer.com/news/security/data-of-560-million-ticketmaster-customers-for-sale-after-alleged-breach/){:target="_blank" rel="noopener"}

> ​A threat actor known as ShinyHunters is selling what they claim is the personal and financial information of 560 million Ticketmaster customers on the recently revived BreachForums hacking forum for $500,000. [...]
