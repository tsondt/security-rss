Title: End-to-end encryption may be the bane of cops, but they can't close that Pandora's Box
Date: 2024-05-05T13:30:14+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-05-05-end-to-end-encryption-may-be-the-bane-of-cops-but-they-cant-close-that-pandoras-box

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/05/e2ee_police/){:target="_blank" rel="noopener"}

> Internet Society's Robin Wilton tells us the war on privacy won't be won by the plod interview Police can complain all they like about strong end-to-end encryption making their jobs harder, but it doesn't matter because the technology is here and won't go away.... [...]
