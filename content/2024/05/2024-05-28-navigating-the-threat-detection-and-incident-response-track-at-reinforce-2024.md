Title: Navigating the threat detection and incident response track at re:Inforce 2024
Date: 2024-05-28T13:00:50+00:00
Author: Nisha Amthul
Category: AWS Security
Tags: Announcements;AWS re:Inforce;Foundational (100);Security, Identity, & Compliance;Amazon Detective;Amazon GuardDuty;Amazon Inspector;Amazon Security Lake;AWS Security Hub;Live Events;re:Inforce 2024;Security;Security Blog
Slug: 2024-05-28-navigating-the-threat-detection-and-incident-response-track-at-reinforce-2024

[Source](https://aws.amazon.com/blogs/security/navigating-the-threat-detection-and-incident-response-track-at-reinforce-2024/){:target="_blank" rel="noopener"}

> A full conference pass is $1,099. Register today with the code flashsale150 to receive a limited time $150 discount, while supplies last. We’re counting down to AWS re:Inforce, our annual cloud security event! We are thrilled to invite security enthusiasts and builders to join us in Philadelphia, PA, from June 10–12 for an immersive two-and-a-half-day [...]
