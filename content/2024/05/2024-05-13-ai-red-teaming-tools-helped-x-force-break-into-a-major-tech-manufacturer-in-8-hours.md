Title: AI red-teaming tools helped X-Force break into a major tech manufacturer 'in 8 hours'
Date: 2024-05-13T14:00:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-13-ai-red-teaming-tools-helped-x-force-break-into-a-major-tech-manufacturer-in-8-hours

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/13/ai_xforce_red_penetration/){:target="_blank" rel="noopener"}

> Hint: It's the 'the largest' maker of a key computer component RSAC An unnamed tech business hired IBM's X-Force penetration-testing team to break into its network to test its security. With the help of AI automation, Big Blue said it was able to do so within hours.... [...]
