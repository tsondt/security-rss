Title: FBI Seizes BreachForums Website
Date: 2024-05-17T11:09:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;data breaches;FBI;leaks;ransomware
Slug: 2024-05-17-fbi-seizes-breachforums-website

[Source](https://www.schneier.com/blog/archives/2024/05/fbi-seizes-breachforums-website.html){:target="_blank" rel="noopener"}

> The FBI has seized the BreachForums website, used by ransomware criminals to leak stolen corporate data. If law enforcement has gained access to the hacking forum’s backend data, as they claim, they would have email addresses, IP addresses, and private messages that could expose members and be used in law enforcement investigations. [...] The FBI is requesting victims and individuals contact them with information about the hacking forum and its members to aid in their investigation. The seizure messages include ways to contact the FBI about the seizure, including an email, a Telegram account, a TOX account, and a dedicated [...]
