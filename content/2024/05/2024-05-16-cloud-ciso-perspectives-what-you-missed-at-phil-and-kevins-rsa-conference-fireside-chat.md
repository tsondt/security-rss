Title: Cloud CISO Perspectives: What you missed at Phil and Kevin’s RSA Conference fireside chat
Date: 2024-05-16T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2024-05-16-cloud-ciso-perspectives-what-you-missed-at-phil-and-kevins-rsa-conference-fireside-chat

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-what-you-missed-at-phil-and-kevins-rsa-conference-fireside-chat/){:target="_blank" rel="noopener"}

> Welcome to the first Cloud CISO Perspectives for May 2024. In this update, I’ll review my RSA Conference fireside chat with Mandiant CEO Kevin Mandia. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI Security & CISO, Google Cloud aside_block <ListValue: [StructValue([('title', 'Get vital CISO Insights with Google Cloud'), ('body', <wagtail.rich_text.RichText object at 0x3ec9cb9cc580>), ('btn_text', 'Visit the hub'), ('href', 'https://cloud.google.com/solutions/security/leaders?utm_source=cloud_sfdc&utm_medium=email&utm_campaign=FY23-Q2-global-PROD418-email-oi-dgcsm-CISOPerspectivesNewsletter&utm_content=ciso-hub&utm_term=-'), ('image', <GAEImage: GCAT-replacement-logo-A>)])]> What you missed at our [...]
