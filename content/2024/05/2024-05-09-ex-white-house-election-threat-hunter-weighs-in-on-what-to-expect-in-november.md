Title: Ex-White House election threat hunter weighs in on what to expect in November
Date: 2024-05-09T21:03:02+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-09-ex-white-house-election-threat-hunter-weighs-in-on-what-to-expect-in-november

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/09/exwhite_house_election_threat_hunter/){:target="_blank" rel="noopener"}

> Spoiler alert: We're gonna talk about AI Interview Mick Baccio, global security advisor at Splunk, has watched the evolution of election security threats in real time.... [...]
