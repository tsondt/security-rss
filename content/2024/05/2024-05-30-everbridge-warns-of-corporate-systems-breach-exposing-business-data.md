Title: Everbridge warns of corporate systems breach exposing business data
Date: 2024-05-30T11:45:26-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-30-everbridge-warns-of-corporate-systems-breach-exposing-business-data

[Source](https://www.bleepingcomputer.com/news/security/everbridge-warns-of-corporate-systems-breach-exposing-business-data/){:target="_blank" rel="noopener"}

> Everbridge, an American software company focused on crisis management and public warning solutions, notified customers that unknown attackers had accessed files containing business and user data in a recent corporate systems breach. [...]
