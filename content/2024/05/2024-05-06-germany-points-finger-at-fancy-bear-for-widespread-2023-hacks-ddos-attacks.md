Title: Germany points finger at Fancy Bear for widespread 2023 hacks, DDoS attacks
Date: 2024-05-06T02:30:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-05-06-germany-points-finger-at-fancy-bear-for-widespread-2023-hacks-ddos-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/06/infosec_in_brief/){:target="_blank" rel="noopener"}

> Also: Microsoft promises to git gud on cybersecurity; unqualified attackers are targeting your water systems, and more Infosec in brief It was just around a year ago that a spate of allegedly Russian-orchestrated cyberattacks hit government agencies in Germany, and now German officials claim to know for a fact who did it: APT28, or Fancy Bear, a Russian threat actor linked to the GRU intelligence service.... [...]
