Title: Chrome Enterprise expands ecosystem to strengthen endpoint security and Zero Trust access
Date: 2024-05-06T13:00:00+00:00
Author: Kiran Nair
Category: GCP Security
Tags: Chrome Enterprise;Security & Identity
Slug: 2024-05-06-chrome-enterprise-expands-ecosystem-to-strengthen-endpoint-security-and-zero-trust-access

[Source](https://cloud.google.com/blog/products/identity-security/chrome-enterprise-expands-ecosystem-to-strengthen-endpoint-security-at-rsa/){:target="_blank" rel="noopener"}

> The modern workplace relies on web-based applications and cloud services, making browsers and their sensitive data a primary target for attackers. While the risks are significant, Chrome Enterprise can help organizations simplify and strengthen their endpoint security with secure enterprise browsing. Following our recent Chrome Enterprise Premium launch, today at the RSA Conference in San Francisco, we’re announcing a growing ecosystem of security providers who are working with us to extend Chrome Enterprise’s browser-based protections and help enterprises protect their users working on the web and across corporate applications. Expanding Zero Trust protections with Zscaler Chrome Enterprise Premium offers advanced [...]
