Title: Google is named a Visionary in its first 2024 Gartner® Magic Quadrant™ for SIEM
Date: 2024-05-15T16:00:00+00:00
Author: Sunil Potti
Category: GCP Security
Tags: Security & Identity
Slug: 2024-05-15-google-is-named-a-visionary-in-its-first-2024-gartner-magic-quadranttm-for-siem

[Source](https://cloud.google.com/blog/products/identity-security/google-is-named-a-visionary-in-the-2024-gartner-magic-quadrant-for-siem/){:target="_blank" rel="noopener"}

> We’re excited to share that Gartner has recognized Google as a Visionary in the 2024 Gartner® Magic QuadrantTM for Security Information and Event Management (SIEM). Participating for the first time in the Magic Quadrant for SIEM, we have been positioned as a Visionary because of our powerful search capabilities that help drive threat hunting and rapid investigation, as well as our fully integrated SOAR platform and integrated threat intelligence from Mandiant and VirusTotal. “Google’s product enhancements for more classic SIEM functions have opened up Google Security Operations as a viable SIEM competitor,” 2024 Gartner® Magic QuadrantTM for SIEM. The journey [...]
