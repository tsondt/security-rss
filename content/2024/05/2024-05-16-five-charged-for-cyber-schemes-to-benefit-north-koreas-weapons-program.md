Title: Five charged for cyber schemes to benefit North Korea's weapons program
Date: 2024-05-16T15:17:13-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-16-five-charged-for-cyber-schemes-to-benefit-north-koreas-weapons-program

[Source](https://www.bleepingcomputer.com/news/security/five-charged-for-cyber-schemes-to-benefit-north-koreas-weapons-program/){:target="_blank" rel="noopener"}

> ​The U.S. Justice Department charged five individuals today, a U.S. Citizen woman, a Ukrainian man, and three foreign nationals, for their involvement in cyber schemes that generated revenue for North Korea's nuclear weapons program. [...]
