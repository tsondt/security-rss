Title: Chinese national cuffed on charges of running 'likely the world's largest botnet ever'
Date: 2024-05-29T23:58:12+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-05-29-chinese-national-cuffed-on-charges-of-running-likely-the-worlds-largest-botnet-ever

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/29/911s5_botnet_arrest/){:target="_blank" rel="noopener"}

> DoJ says 911 S5 crew earned $100M from 19 million PCs pwned by fake VPNs US authorities have arrested the alleged administrator of what FBI director Christopher Wray has described as "likely the world's largest botnet ever," comprising 19 million compromised Windows machines used by its operators to reap millions of dollars over the last decade.... [...]
