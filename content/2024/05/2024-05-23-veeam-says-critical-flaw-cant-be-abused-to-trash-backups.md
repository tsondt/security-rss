Title: Veeam says critical flaw can't be abused to trash backups
Date: 2024-05-23T14:30:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-23-veeam-says-critical-flaw-cant-be-abused-to-trash-backups

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/23/veeam_critical_vulnerability_backups/){:target="_blank" rel="noopener"}

> It's still a rough one, so patch up Veeam says the recent critical vulnerability in its Backup Enterprise Manager (VBEM) can't be used by cybercriminals to delete an organization's backups.... [...]
