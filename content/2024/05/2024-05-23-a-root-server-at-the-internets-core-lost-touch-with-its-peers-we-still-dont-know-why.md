Title: A root-server at the Internet’s core lost touch with its peers. We still don’t know why.
Date: 2024-05-23T17:10:34+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;DNS;DNSSEC;domain name system;root servers
Slug: 2024-05-23-a-root-server-at-the-internets-core-lost-touch-with-its-peers-we-still-dont-know-why

[Source](https://arstechnica.com/?p=2026566){:target="_blank" rel="noopener"}

> Enlarge For more than four days, a server at the very core of the Internet’s domain name system was out of sync with its 12 root server peers due to an unexplained glitch that could have caused stability and security problems worldwide. This server, maintained by Internet carrier Cogent Communications, is one of the 13 root servers that provision the Internet’s root zone, which sits at the top of the hierarchical distributed database known as the domain name system, or DNS. Here's a simplified recap of the way the domain name system works and how root servers fit in: When [...]
