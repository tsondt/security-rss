Title: Canada's London Drugs confirms ransomware attack after LockBit demands $25M
Date: 2024-05-22T20:00:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-22-canadas-london-drugs-confirms-ransomware-attack-after-lockbit-demands-25m

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/22/london_drugs_ransomware/){:target="_blank" rel="noopener"}

> Pharmacy says it's 'unwilling and unable to pay ransom' Canadian pharmacy chain London Drugs has confirmed that ransomware thugs stole some of its corporate files containing employee information and says it is "unwilling and unable to pay ransom to these cybercriminals."... [...]
