Title: Twitch ditches Safety Advisory Council, relaunches with vetted 'ambassadors'
Date: 2024-05-31T23:13:05+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-05-31-twitch-ditches-safety-advisory-council-relaunches-with-vetted-ambassadors

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/31/twitch_safety_advisory_council/){:target="_blank" rel="noopener"}

> Who needs experts when you have an army of hand-picked super users telling you what you want to hear? Twitch has reportedly dismantled its Safety Advisory Council, and apparently plans to replace the panel with chosen "ambassadors."... [...]
