Title: AWS plans to invest €7.8B into the AWS European Sovereign Cloud, set to launch by the end of 2025
Date: 2024-05-15T06:06:57+00:00
Author: Max Peterson
Category: AWS Security
Tags: Announcements;Europe;Foundational (100);Security, Identity, & Compliance;Thought Leadership;AWS Digital Sovereignty Pledge;data sovereignty;Digital Sovereignty;European Economic Area;European Union;Germany;Security Blog;Sovereign-by-design
Slug: 2024-05-15-aws-plans-to-invest-78b-into-the-aws-european-sovereign-cloud-set-to-launch-by-the-end-of-2025

[Source](https://aws.amazon.com/blogs/security/aws-plans-to-invest-e7-8b-into-the-aws-european-sovereign-cloud-set-to-launch-by-the-end-of-2025/){:target="_blank" rel="noopener"}

> English | German Amazon Web Services (AWS) continues to believe it’s essential that our customers have control over their data and choices for how they secure and manage that data in the cloud. AWS gives customers the flexibility to choose how and where they want to run their workloads, including a proven track record of [...]
