Title: Ticketek customer details exposed in cyber security breach
Date: 2024-05-31T23:50:56+00:00
Author: Graham Readfearn
Category: The Guardian
Tags: Data and computer security;Cybercrime;Australia news
Slug: 2024-05-31-ticketek-customer-details-exposed-in-cyber-security-breach

[Source](https://www.theguardian.com/technology/article/2024/jun/01/ticketek-customer-details-exposed-in-cyber-security-breach){:target="_blank" rel="noopener"}

> Clare O’Neil says incident affecting many Australians but appears restricted to the release of names, dates of birth and email addresses Get our morning and afternoon news emails, free app or daily news podcast Ticketek has been hit by a “cyber incident” with personal information of Australian customers stolen from a third-party global cloud-based platform. The cybersecurity minister, Clare O’Neil, said late on Friday night the breach was “affecting many Australians” but appeared restricted to the release of names, dates of birth and email addresses. Sign up for Guardian Australia’s free morning and afternoon email newsletters for your daily news [...]
