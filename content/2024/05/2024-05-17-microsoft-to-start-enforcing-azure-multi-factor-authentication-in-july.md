Title: Microsoft to start enforcing Azure multi-factor authentication in July
Date: 2024-05-17T14:53:52-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-05-17-microsoft-to-start-enforcing-azure-multi-factor-authentication-in-july

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-will-start-enforcing-azure-multi-factor-authentication-MFA-in-july-2024/){:target="_blank" rel="noopener"}

> Starting in July, Microsoft will begin gradually enforcing multi-factor authentication (MFA) for all users signing into Azure to administer resources. [...]
