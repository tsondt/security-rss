Title: Ascension healthcare takes systems offline after cyberattack
Date: 2024-05-08T17:28:36-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-05-08-ascension-healthcare-takes-systems-offline-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/ascension-healthcare-takes-systems-offline-after-cyberattack/){:target="_blank" rel="noopener"}

> ​Ascension, one of the largest private healthcare systems in the United States, has taken some of its systems offline to investigate what it describes as a "cyber security event." [...]
