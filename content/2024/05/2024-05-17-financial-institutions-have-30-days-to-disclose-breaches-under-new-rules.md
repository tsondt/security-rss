Title: Financial institutions have 30 days to disclose breaches under new rules
Date: 2024-05-17T19:27:45+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Policy;Security
Slug: 2024-05-17-financial-institutions-have-30-days-to-disclose-breaches-under-new-rules

[Source](https://arstechnica.com/?p=2025242){:target="_blank" rel="noopener"}

> Enlarge (credit: Brendan Smialowski / Getty Images ) The Securities and Exchange Commission (SEC) will require some financial institutions to disclose security breaches within 30 days of learning about them. On Wednesday, the SEC adopted changes to Regulation S-P, which governs the treatment of the personal information of consumers. Under the amendments, institutions must notify individuals whose personal information was compromised “as soon as practicable, but not later than 30 days” after learning of unauthorized network access or use of customer data. The new requirements will be binding on broker-dealers (including funding portals), investment companies, registered investment advisers, and transfer [...]
