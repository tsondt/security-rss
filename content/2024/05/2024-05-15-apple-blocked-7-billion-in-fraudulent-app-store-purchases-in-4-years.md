Title: Apple blocked $7 billion in fraudulent App Store purchases in 4 years
Date: 2024-05-15T13:42:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple;Mobile
Slug: 2024-05-15-apple-blocked-7-billion-in-fraudulent-app-store-purchases-in-4-years

[Source](https://www.bleepingcomputer.com/news/security/apple-blocked-7-billion-in-fraudulent-app-store-purchases-in-4-years/){:target="_blank" rel="noopener"}

> Apple's antifraud technology has blocked more than $7 billion in potentially fraudulent transactions in four years, the company states in its latest annual fraud prevention analysis. [...]
