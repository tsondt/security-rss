Title: UnitedHealth's 'egregious negligence' led to Change Healthcare ransomware infection
Date: 2024-05-08T02:58:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-08-unitedhealths-egregious-negligence-led-to-change-healthcare-ransomware-infection

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/08/unitedhealths_egregious_negligence/){:target="_blank" rel="noopener"}

> 'I'm blown away by the fact that they weren't using MFA' Interview The cybersecurity practices that led up to the stunning Change Healthcare ransomware infection indicate "egregious negligence" on the part of parent company UnitedHealth, according to Tom Kellermann, SVP of cyber strategy at Contrast Security.... [...]
