Title: US charges 16 over 'depraved' grandparent scams
Date: 2024-05-01T17:00:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-01-us-charges-16-over-depraved-grandparent-scams

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/01/us_charges_16_grandparent_scammers/){:target="_blank" rel="noopener"}

> Vulnerable elderly people tricked into paying tens of thousands over fake car accidents Sixteen people are facing charges from US prosecutors for allegedly preying on the elderly and scamming them out of millions of dollars.... [...]
