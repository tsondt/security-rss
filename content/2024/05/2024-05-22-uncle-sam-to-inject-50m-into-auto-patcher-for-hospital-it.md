Title: Uncle Sam to inject $50M into auto-patcher for hospital IT
Date: 2024-05-22T00:46:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-22-uncle-sam-to-inject-50m-into-auto-patcher-for-hospital-it

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/22/50m_investment_hospital_security/){:target="_blank" rel="noopener"}

> Boffins, why not simply invent an algorithm that autonomously fixes flaws, thereby ending ransomware forever The US government's Advanced Research Projects Agency for Health (ARPA-H) has pledged more than $50 million to fund the development of technology that aims to automate the process of securing hospital IT environments.... [...]
