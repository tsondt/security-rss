Title: Nissan North America data breach impacts over 53,000 employees
Date: 2024-05-15T15:32:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-15-nissan-north-america-data-breach-impacts-over-53000-employees

[Source](https://www.bleepingcomputer.com/news/security/nissan-north-america-data-breach-impacts-over-53-000-employees/){:target="_blank" rel="noopener"}

> Nissan North America (Nissan) suffered a data breach last year when a threat actor targeted the company's external VPN and shut down systems to receive a ransom. [...]
