Title: INC ransomware source code selling on hacking forums for $300,000
Date: 2024-05-13T16:22:55-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-13-inc-ransomware-source-code-selling-on-hacking-forums-for-300000

[Source](https://www.bleepingcomputer.com/news/security/inc-ransomware-source-code-selling-on-hacking-forums-for-300-000/){:target="_blank" rel="noopener"}

> A cybercriminal using the name "salfetka" claims to be selling the source code of INC Ransom, a ransomware-as-a-service (RaaS) operation launched in August 2023. [...]
