Title: Poland says Russian military hackers target its govt networks
Date: 2024-05-09T19:14:50-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Government
Slug: 2024-05-09-poland-says-russian-military-hackers-target-its-govt-networks

[Source](https://www.bleepingcomputer.com/news/security/poland-says-russian-military-hackers-target-its-govt-networks/){:target="_blank" rel="noopener"}

> Poland says a state-backed threat group linked to Russia's military intelligence service (GRU) has been targeting Polish government institutions throughout the week. [...]
