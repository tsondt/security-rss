Title: Hackers use DNS tunneling for network scanning, tracking victims
Date: 2024-05-13T13:50:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-13-hackers-use-dns-tunneling-for-network-scanning-tracking-victims

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-dns-tunneling-for-network-scanning-tracking-victims/){:target="_blank" rel="noopener"}

> Threat actors are using Domain Name System (DNS) tunneling to track when their targets open phishing emails and click on malicious links, and to scan networks for potential vulnerabilities. [...]
