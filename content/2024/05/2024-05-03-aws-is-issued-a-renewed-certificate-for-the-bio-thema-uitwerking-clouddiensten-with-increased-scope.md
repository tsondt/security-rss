Title: AWS is issued a renewed certificate for the BIO Thema-uitwerking Clouddiensten with increased scope
Date: 2024-05-03T18:18:40+00:00
Author: Ka Yie Lee
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Compliance;Data protection;EU Data Protection;Public Sector;Security Blog
Slug: 2024-05-03-aws-is-issued-a-renewed-certificate-for-the-bio-thema-uitwerking-clouddiensten-with-increased-scope

[Source](https://aws.amazon.com/blogs/security/aws-is-issued-a-renewed-certificate-for-the-bio-thema-uitwerking-clouddiensten-with-increased-scope/){:target="_blank" rel="noopener"}

> We’re pleased to announce that Amazon Web Services (AWS) demonstrated continuous compliance with the Baseline Informatiebeveiliging Overheid (BIO) Thema-uitwerking Clouddiensten while increasing the AWS services and AWS Regions in scope. This alignment with the BIO Thema-uitwerking Clouddiensten requirements demonstrates our commitment to adhere to the heightened expectations for cloud service providers. AWS customers across the Dutch public sector can [...]
