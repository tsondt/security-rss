Title: Google rolls back reCaptcha update to fix Firefox issues
Date: 2024-05-03T13:07:43-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Google;Software
Slug: 2024-05-03-google-rolls-back-recaptcha-update-to-fix-firefox-issues

[Source](https://www.bleepingcomputer.com/news/security/google-rolls-back-recaptcha-update-to-fix-firefox-issues/){:target="_blank" rel="noopener"}

> Google has rolled back a recent release of its reCaptcha captcha script after a bug caused the service to no longer work on Firefox for Windows. [...]
