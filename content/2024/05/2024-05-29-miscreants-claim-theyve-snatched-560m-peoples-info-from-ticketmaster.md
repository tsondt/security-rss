Title: Miscreants claim they've snatched 560M people's info from Ticketmaster
Date: 2024-05-29T23:00:35+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-29-miscreants-claim-theyve-snatched-560m-peoples-info-from-ticketmaster

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/29/breachforums_ticketmaster_data/){:target="_blank" rel="noopener"}

> All that data allegedly going for a song on revived BreachForums Updated Ticketmaster is believed to have had its IT breached by cybercriminals who claim to have stolen 1.3TB of data on 560 million of the corporation's customers – and are now selling all that info for $500,000.... [...]
