Title: You want us to think of the children? Couldn't agree more
Date: 2024-05-13T08:30:10+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2024-05-13-you-want-us-to-think-of-the-children-couldnt-agree-more

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/13/e2ee_comment/){:target="_blank" rel="noopener"}

> But breaking E2EE and blanket bans aren't thinking at all Opinion If your cranky uncle was this fixated about anything, you'd always be somewhere else at Christmas. Yet here we are again. Europol has been sounding off at Meta for harming children. Not for the way it's actually harming children, but because – repeat after me – end-to-end encryption is hiding child sexual abuse material from the eyes of the law. "E2EE = CSAM" is the new slogan of fear.... [...]
