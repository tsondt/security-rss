Title: Telegram CEO calls out rival Signal, claiming it has ties to US government
Date: 2024-05-14T14:30:13+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-05-14-telegram-ceo-calls-out-rival-signal-claiming-it-has-ties-to-us-government

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/14/telegram_ceo_calls_out_rival/){:target="_blank" rel="noopener"}

> Drama between two of the leading secure messaging services Telegram CEO Pavel Durov issued a scathing criticism of Signal, alleging the messaging service is not secure and has ties to US intelligence agencies.... [...]
