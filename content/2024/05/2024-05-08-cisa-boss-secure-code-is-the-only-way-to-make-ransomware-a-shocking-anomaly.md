Title: CISA boss: Secure code is the 'only way to make ransomware a shocking anomaly'
Date: 2024-05-08T16:00:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-08-cisa-boss-secure-code-is-the-only-way-to-make-ransomware-a-shocking-anomaly

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/08/cisa_ransomware_rsac/){:target="_blank" rel="noopener"}

> And it would seriously inconvenience the Chinese and Russians, too RSAC There's a way to vastly reduce the scale and scope of ransomware attacks plaguing critical infrastructure, according to CISA director Jen Easterly: Make software secure by design.... [...]
