Title: Patch up – 4 critical bugs in ArubaOS lead to remote code execution
Date: 2024-05-02T20:30:58+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-02-patch-up-4-critical-bugs-in-arubaos-lead-to-remote-code-execution

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/02/hpe_aruba_patches/){:target="_blank" rel="noopener"}

> Ten vulnerabilities in total for admins to apply Network admins are being urged to patch a bundle of critical vulnerabilities in ArubaOS that lead to remote code execution as a privileged user.... [...]
