Title: How to use AWS managed applications with IAM Identity Center
Date: 2024-05-13T19:55:00+00:00
Author: Liam Wadman
Category: AWS Security
Tags: AWS IAM Identity Center;Foundational (100);Security, Identity, & Compliance;Technical How-to;IAM;IAM Identity Center;Identity;Security;Security Blog
Slug: 2024-05-13-how-to-use-aws-managed-applications-with-iam-identity-center

[Source](https://aws.amazon.com/blogs/security/how-to-use-aws-managed-applications-with-iam-identity-center/){:target="_blank" rel="noopener"}

> AWS IAM Identity Center is the preferred way to provide workforce access to Amazon Web Services (AWS) accounts, and enables you to provide workforce access to many AWS managed applications, such as Amazon Q Developer (Formerly known as Code Whisperer). As we continue to release more AWS managed applications, customers have told us they want [...]
