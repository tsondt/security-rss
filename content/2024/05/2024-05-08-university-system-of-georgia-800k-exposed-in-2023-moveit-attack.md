Title: University System of Georgia: 800K exposed in 2023 MOVEit attack
Date: 2024-05-08T17:48:59-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Education
Slug: 2024-05-08-university-system-of-georgia-800k-exposed-in-2023-moveit-attack

[Source](https://www.bleepingcomputer.com/news/security/university-system-of-georgia-800k-exposed-in-2023-moveit-attack/){:target="_blank" rel="noopener"}

> The University System of Georgia (USG) is sending data breach notifications to 800,000 individuals whose data was exposed in the 2023 Clop MOVEit attacks. [...]
