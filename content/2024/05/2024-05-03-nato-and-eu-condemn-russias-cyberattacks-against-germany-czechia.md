Title: NATO and EU condemn Russia's cyberattacks against Germany, Czechia
Date: 2024-05-03T11:47:35-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-03-nato-and-eu-condemn-russias-cyberattacks-against-germany-czechia

[Source](https://www.bleepingcomputer.com/news/security/nato-and-eu-condemn-russias-cyberattacks-against-germany-czechia/){:target="_blank" rel="noopener"}

> ​NATO and the European Union, with international partners, formally condemned a long-term cyber espionage campaign against European countries conducted by the Russian threat group APT28. [...]
