Title: Rare Interviews with Enigma Cryptanalyst Marian Rejewski
Date: 2024-05-03T11:10:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptanalysis;Enigma;history of cryptography;war
Slug: 2024-05-03-rare-interviews-with-enigma-cryptanalyst-marian-rejewski

[Source](https://www.schneier.com/blog/archives/2024/05/rare-interviews-with-enigma-cryptanalyst-marian-rejewski.html){:target="_blank" rel="noopener"}

> The Polish Embassy has posted a series of short interview segments with Marian Rejewski, the first person to crack the Enigma. Details from his biography. [...]
