Title: Florida man gets 6 years behind bars for flogging fake Cisco kit to US military
Date: 2024-05-02T20:58:13+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-05-02-florida-man-gets-6-years-behind-bars-for-flogging-fake-cisco-kit-to-us-military

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/02/fake_cisco_prison/){:target="_blank" rel="noopener"}

> Operation busted after dodgy devices ended up at Air Force Miami resident Onur Aksoy has been sentenced to six and a half years in prison for running a multi-million-dollar operation selling fake Cisco equipment that ended up in the US military.... [...]
