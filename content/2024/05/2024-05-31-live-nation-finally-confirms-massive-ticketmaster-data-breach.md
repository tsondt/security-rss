Title: Live Nation finally confirms massive Ticketmaster data breach
Date: 2024-05-31T17:43:50-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-05-31-live-nation-finally-confirms-massive-ticketmaster-data-breach

[Source](https://www.bleepingcomputer.com/news/security/live-nation-finally-confirms-massive-ticketmaster-data-breach/){:target="_blank" rel="noopener"}

> Live Nation has confirmed that Ticketmaster suffered a data breach after its data was stolen from a third-party cloud database provider, which is believed to be Snowflake. [...]
