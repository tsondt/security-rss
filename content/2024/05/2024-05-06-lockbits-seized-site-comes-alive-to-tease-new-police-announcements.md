Title: Lockbit's seized site comes alive to tease new police announcements
Date: 2024-05-06T07:06:12-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-05-06-lockbits-seized-site-comes-alive-to-tease-new-police-announcements

[Source](https://www.bleepingcomputer.com/news/security/lockbits-seized-site-comes-alive-to-tease-new-police-announcements/){:target="_blank" rel="noopener"}

> The NCA, FBI, and Europol have revived a seized LockBit ransomware data leak site to hint at new information being revealed by law enforcement this Tuesday. [...]
