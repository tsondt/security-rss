Title: DropBox says hackers stole customer data, auth secrets from eSignature service
Date: 2024-05-01T18:22:48-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-05-01-dropbox-says-hackers-stole-customer-data-auth-secrets-from-esignature-service

[Source](https://www.bleepingcomputer.com/news/security/dropbox-says-hackers-stole-customer-data-auth-secrets-from-esignature-service/){:target="_blank" rel="noopener"}

> Cloud storage firm DropBox says hackers breached production systems for its DropBox Sign eSignature platform and gained access to authentication tokens, MFA keys, hashed passwords, and customer information. [...]
