Title: Researchers crack 11-year-old password, recover $3 million in bitcoin
Date: 2024-05-29T15:42:22+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;cryptocurrency;hacking;passwords;syndication
Slug: 2024-05-29-researchers-crack-11-year-old-password-recover-3-million-in-bitcoin

[Source](https://arstechnica.com/?p=2027419){:target="_blank" rel="noopener"}

> Enlarge (credit: Flavio Coelho/Getty Images) Two years ago when “Michael,” an owner of cryptocurrency, contacted Joe Grand to help recover access to about $2 million worth of bitcoin he stored in encrypted format on his computer, Grand turned him down. Michael, who is based in Europe and asked to remain anonymous, stored the cryptocurrency in a password-protected digital wallet. He generated a password using the RoboForm password manager and stored that password in a file encrypted with a tool called TrueCrypt. At some point, that file got corrupted, and Michael lost access to the 20-character password he had generated to [...]
