Title: Investigating lateral movements with Amazon Detective investigation and Security Lake integration
Date: 2024-05-14T23:10:54+00:00
Author: Yue Zhu
Category: AWS Security
Tags: Amazon Detective;Amazon GuardDuty;Amazon Security Lake;AWS Identity and Access Management (IAM);Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2024-05-14-investigating-lateral-movements-with-amazon-detective-investigation-and-security-lake-integration

[Source](https://aws.amazon.com/blogs/security/investigating-lateral-movements-with-amazon-detective-investigation-and-security-lake-integration/){:target="_blank" rel="noopener"}

> According to the MITRE ATT&CK framework, lateral movement consists of techniques that threat actors use to enter and control remote systems on a network. In Amazon Web Services (AWS) environments, threat actors equipped with illegitimately obtained credentials could potentially use APIs to interact with infrastructures and services directly, and they might even be able to use [...]
