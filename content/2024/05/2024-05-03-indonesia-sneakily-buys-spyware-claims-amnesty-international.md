Title: Indonesia sneakily buys spyware, claims Amnesty International
Date: 2024-05-03T04:33:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-05-03-indonesia-sneakily-buys-spyware-claims-amnesty-international

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/03/amnesty_indonesia_surveillance/){:target="_blank" rel="noopener"}

> A 'murky' web sees many purchases run through Singapore in a way that hides potential users Indonesia has acquired spyware and surveillance technologies through a "murky network" that extends into Israel, Greece, Singapore and Malaysia for equipment sourcing, according to Amnesty International.... [...]
