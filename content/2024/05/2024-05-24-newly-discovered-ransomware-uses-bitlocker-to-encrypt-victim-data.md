Title: Newly discovered ransomware uses BitLocker to encrypt victim data
Date: 2024-05-24T22:06:57+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;BitLocker;encryption;ransomware
Slug: 2024-05-24-newly-discovered-ransomware-uses-bitlocker-to-encrypt-victim-data

[Source](https://arstechnica.com/?p=2027056){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) A previously unknown piece of ransomware, dubbed ShrinkLocker, encrypts victim data using the BitLocker feature built into the Windows operating system. BitLocker is a full-volume encryptor that debuted in 2007 with the release of Windows Vista. Users employ it to encrypt entire hard drives to prevent people from reading or modifying data in the event they get physical access to the disk. Starting with the rollout of Windows 10, BitLocker by default has used the 128-bit and 256-bit XTS-AES encryption algorithm, giving the feature extra protection from attacks that rely on manipulating cipher text to [...]
