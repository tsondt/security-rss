Title: Privacy Implications of Tracking Wireless Access Points
Date: 2024-05-29T11:01:24+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;geolocation;privacy;Wi-Fi
Slug: 2024-05-29-privacy-implications-of-tracking-wireless-access-points

[Source](https://www.schneier.com/blog/archives/2024/05/privacy-implications-of-tracking-wireless-access-points.html){:target="_blank" rel="noopener"}

> Brian Krebs reports on research into geolocating routers: Apple and the satellite-based broadband service Starlink each recently took steps to address new research into the potential security and privacy implications of how their services geolocate devices. Researchers from the University of Maryland say they relied on publicly available data from Apple to track the location of billions of devices globally—including non-Apple devices like Starlink systems—and found they could use this data to monitor the destruction of Gaza, as well as the movements and in many cases identities of Russian and Ukrainian troops. Really fascinating implications to this research. Research paper: [...]
