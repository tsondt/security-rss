Title: OpenAI is very smug after thwarting five ineffective AI covert influence ops
Date: 2024-05-30T23:29:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-05-30-openai-is-very-smug-after-thwarting-five-ineffective-ai-covert-influence-ops

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/30/openai_stops_five_ineffective_ai/){:target="_blank" rel="noopener"}

> That said, use of generative ML to sway public opinion may not always be weak sauce OpenAI on Thursday said it has disrupted five covert influence operations that were attempting to use its AI services to manipulate public opinion and elections.... [...]
