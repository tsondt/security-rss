Title: TikTok sues America to undo divest-or-die law
Date: 2024-05-07T19:02:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-05-07-tiktok-sues-america-to-undo-divest-or-die-law

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/07/tiktok_bytedance_sue_usa/){:target="_blank" rel="noopener"}

> Nothing like folks in Beijing lecturing us on the Constitution TikTok and its China-based parent ByteDance sued the US government today to prevent the forced sale or shutdown of the video-sharing giant.... [...]
