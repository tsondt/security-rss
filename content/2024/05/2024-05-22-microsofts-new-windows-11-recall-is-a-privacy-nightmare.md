Title: Microsoft's new Windows 11 Recall is a privacy nightmare
Date: 2024-05-22T12:02:24-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-05-22-microsofts-new-windows-11-recall-is-a-privacy-nightmare

[Source](https://www.bleepingcomputer.com/news/microsoft/microsofts-new-windows-11-recall-is-a-privacy-nightmare/){:target="_blank" rel="noopener"}

> Microsoft's announcement of the new AI-powered Windows 11 Recall feature has sparked a lot of concern, with many thinking that it has created massive privacy risks and a new attack vector that threat actors can exploit to steal data. [...]
