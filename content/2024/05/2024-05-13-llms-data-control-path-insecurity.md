Title: LLMs’ Data-Control Path Insecurity
Date: 2024-05-13T11:04:08+00:00
Author: B. Schneier
Category: Bruce Schneier
Tags: Uncategorized;computer security;hacking;LLM;noncomputer hacks;phones;vulnerabilities
Slug: 2024-05-13-llms-data-control-path-insecurity

[Source](https://www.schneier.com/blog/archives/2024/05/llms-data-control-path-insecurity.html){:target="_blank" rel="noopener"}

> Back in the 1960s, if you played a 2,600Hz tone into an AT&T pay phone, you could make calls without paying. A phone hacker named John Draper noticed that the plastic whistle that came free in a box of Captain Crunch cereal worked to make the right sound. That became his hacker name, and everyone who knew the trick made free pay-phone calls. There were all sorts of related hacks, such as faking the tones that signaled coins dropping into a pay phone and faking tones used by repair equipment. AT&T could sometimes change the signaling tones, make them more [...]
