Title: Suspected supply chain attack backdoors courtroom recording software
Date: 2024-05-24T20:29:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-24-suspected-supply-chain-attack-backdoors-courtroom-recording-software

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/24/suspected_supply_chain_attack_backdoors/){:target="_blank" rel="noopener"}

> An open and shut case, but the perps remain at large – whoever they are Justice is served... or should that be saved now that audio-visual software deployed in more than 10,000 courtrooms is once again secure after researchers uncovered evidence that it had been backdoored for weeks.... [...]
