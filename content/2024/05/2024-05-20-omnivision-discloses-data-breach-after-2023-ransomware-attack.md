Title: OmniVision discloses data breach after 2023 ransomware attack
Date: 2024-05-20T16:51:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-20-omnivision-discloses-data-breach-after-2023-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/omnivision-discloses-data-breach-after-2023-ransomware-attack/){:target="_blank" rel="noopener"}

> The California-based imaging sensors manufacturer OmniVision is warning of a data breach after the company suffered a Cactus ransomware attack last year. [...]
