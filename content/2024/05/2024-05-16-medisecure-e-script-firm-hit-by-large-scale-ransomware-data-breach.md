Title: MediSecure e-script firm hit by ‘large-scale’ ransomware data breach
Date: 2024-05-16T13:08:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-05-16-medisecure-e-script-firm-hit-by-large-scale-ransomware-data-breach

[Source](https://www.bleepingcomputer.com/news/security/medisecure-e-script-firm-hit-by-large-scale-ransomware-data-breach/){:target="_blank" rel="noopener"}

> Electronic prescription provider MediSecure in Australia has shut down its website and phone lines following a ransomware attack believed to originate from a third-party vendor. [...]
