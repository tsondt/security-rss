Title: Crims abusing Microsoft Quick Assist to deploy Black Basta ransomware
Date: 2024-05-16T23:30:05+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-16-crims-abusing-microsoft-quick-assist-to-deploy-black-basta-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/16/microsoft_quick_assist_crime/){:target="_blank" rel="noopener"}

> Spoiler alert: it's not really IT support controlling your device A cybercrime gang has been abusing Microsoft's Quick Assist application in social engineering attacks that ultimately allow the crew to infect victims with Black Basta ransomware.... [...]
