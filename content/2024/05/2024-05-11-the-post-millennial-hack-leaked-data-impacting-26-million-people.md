Title: The Post Millennial hack leaked data impacting 26 million people
Date: 2024-05-11T11:17:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-11-the-post-millennial-hack-leaked-data-impacting-26-million-people

[Source](https://www.bleepingcomputer.com/news/security/the-post-millennial-hack-leaked-data-impacting-26-million-people/){:target="_blank" rel="noopener"}

> Have I Been Pwned has added the information for 26,818,266 people whose data was leaked in a recent hack of The Post Millennial conservative news website. [...]
