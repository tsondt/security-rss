Title: FlyingYeti phishing crew grounded after abominable Ukraine attacks
Date: 2024-05-31T06:27:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-31-flyingyeti-phishing-crew-grounded-after-abominable-ukraine-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/31/crowdforce_flyingyeti_ukraine/){:target="_blank" rel="noopener"}

> Kremlin-aligned gang used Cloudflare and GitHub resources, and they didn't like that one bit Cloudflare's threat intel team claims to have thwarted a month-long phishing and espionage attack targeting Ukraine which it has attributed to Russia-aligned gang FlyingYeti.... [...]
