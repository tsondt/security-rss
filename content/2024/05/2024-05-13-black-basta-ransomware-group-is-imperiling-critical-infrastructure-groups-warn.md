Title: Black Basta ransomware group is imperiling critical infrastructure, groups warn
Date: 2024-05-13T19:55:52+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;black basta;CISA;critical infrastructure;ransomware
Slug: 2024-05-13-black-basta-ransomware-group-is-imperiling-critical-infrastructure-groups-warn

[Source](https://arstechnica.com/?p=2024051){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Federal agencies, health care associations, and security researchers are warning that a ransomware group tracked under the name Black Basta is ravaging critical infrastructure sectors in attacks that have targeted more than 500 organizations in the past two years. One of the latest casualties of the native Russian-speaking group, according to CNN, is Ascension, a St. Louis-based health care system that includes 140 hospitals in 19 states. A network intrusion that struck the nonprofit last week ​​took down many of its automated processes for handling patient care, including its systems for managing electronic health records and [...]
