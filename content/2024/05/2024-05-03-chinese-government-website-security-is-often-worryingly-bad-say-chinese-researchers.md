Title: Chinese government website security is often worryingly bad, say Chinese researchers
Date: 2024-05-03T02:34:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-05-03-chinese-government-website-security-is-often-worryingly-bad-say-chinese-researchers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/03/china_gov_web_vuln/){:target="_blank" rel="noopener"}

> Bad configurations, insecure versions of jQuery, and crummy cookies are some of myriad problems Exclusive Five Chinese researchers examined the configurations of nearly 14,000 government websites across the country and found worrying lapses that could lead to malicious attacks, according to a not-yet-peer-reviewed study released last week.... [...]
