Title: Ohio Lottery ransomware attack impacts over 538,000 individuals
Date: 2024-05-10T11:38:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-10-ohio-lottery-ransomware-attack-impacts-over-538000-individuals

[Source](https://www.bleepingcomputer.com/news/security/ohio-lottery-ransomware-attack-impacts-over-538-000-individuals/){:target="_blank" rel="noopener"}

> ​The Ohio Lottery is sending data breach notification letters to over 538,000 individuals affected by a cyberattack that hit the organization's systems on Christmas Eve. [...]
