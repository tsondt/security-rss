Title: Rockwell Automation warns admins to take ICS devices offline
Date: 2024-05-21T13:48:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-21-rockwell-automation-warns-admins-to-take-ics-devices-offline

[Source](https://www.bleepingcomputer.com/news/security/rockwell-automation-warns-admins-to-take-ics-devices-offline/){:target="_blank" rel="noopener"}

> Rockwell Automation warned customers to disconnect all industrial control systems (ICSs) not designed for online exposure from the Internet due to increasing malicious activity worldwide. [...]
