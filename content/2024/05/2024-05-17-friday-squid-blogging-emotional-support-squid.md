Title: Friday Squid Blogging: Emotional Support Squid
Date: 2024-05-17T21:04:39+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-05-17-friday-squid-blogging-emotional-support-squid

[Source](https://www.schneier.com/blog/archives/2024/05/friday-squid-blogging-emotional-support-squid-2.html){:target="_blank" rel="noopener"}

> When asked what makes this an “emotional support squid” and not just another stuffed animal, its creator says: They’re emotional support squid because they’re large, and cuddly, but also cheerfully bright and derpy. They make great neck pillows (and you can fidget with the arms and tentacles) for travelling, and, on a more personal note, when my mum was sick in the hospital I gave her one and she said it brought her “great comfort” to have her squid tucked up beside her and not be a nuisance while she was sleeping. As usual, you can also use this squid [...]
