Title: The Week in Ransomware - May 17th 2024 - Mailbombing is back
Date: 2024-05-17T17:30:56-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-05-17-the-week-in-ransomware-may-17th-2024-mailbombing-is-back

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-may-17th-2024-mailbombing-is-back/){:target="_blank" rel="noopener"}

> This week was pretty quiet on the ransomware front, with most of the attention on the seizure of the BreachForums data theft forum. However, that does not mean there was nothing of interest released this week about ransomware. [...]
