Title: Qantas app exposed sensitive traveler details to random users
Date: 2024-05-01T09:21:07-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-01-qantas-app-exposed-sensitive-traveler-details-to-random-users

[Source](https://www.bleepingcomputer.com/news/security/qantas-app-exposed-sensitive-traveler-details-to-random-users/){:target="_blank" rel="noopener"}

> Qantas Airways confirms that some of its customers were impacted by a misconfiguration in its app that exposed sensitive information and boarding passes to random users. [...]
