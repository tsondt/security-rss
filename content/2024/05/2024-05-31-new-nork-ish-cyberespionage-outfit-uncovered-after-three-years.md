Title: New Nork-ish cyberespionage outfit uncovered after three years
Date: 2024-05-31T15:25:36+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-31-new-nork-ish-cyberespionage-outfit-uncovered-after-three-years

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/31/new_norkish_cyberespionage_outfit_uncovered/){:target="_blank" rel="noopener"}

> Sector-agnostic group is after your data, wherever you are Infosec researchers revealed today a previously unknown cybercrime group that's been on the prowl for three years and is behaving like some of the more dangerous cyber baddies under Kim Jong-Un's watch.... [...]
