Title: Singing River Health System: Data of 895,000 stolen in ransomware attack
Date: 2024-05-14T16:08:33-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-05-14-singing-river-health-system-data-of-895000-stolen-in-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/singing-river-health-system-data-of-895-000-stolen-in-ransomware-attack/){:target="_blank" rel="noopener"}

> The Singing River Health System is warning that it is now estimating that 895,204 people are impacted by a ransomware attack it suffered in August 2023. [...]
