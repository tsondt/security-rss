Title: Botnet sent millions of emails in LockBit Black ransomware campaign
Date: 2024-05-13T15:08:18-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-13-botnet-sent-millions-of-emails-in-lockbit-black-ransomware-campaign

[Source](https://www.bleepingcomputer.com/news/security/botnet-sent-millions-of-emails-in-lockbit-black-ransomware-campaign/){:target="_blank" rel="noopener"}

> Since April, millions of phishing emails have been sent through the Phorpiex botnet to conduct a large-scale LockBit Black ransomware campaign. [...]
