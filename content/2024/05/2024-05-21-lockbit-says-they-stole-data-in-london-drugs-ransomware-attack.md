Title: LockBit says they stole data in London Drugs ransomware attack
Date: 2024-05-21T17:23:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-21-lockbit-says-they-stole-data-in-london-drugs-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/lockbit-says-they-stole-data-in-london-drugs-ransomware-attack/){:target="_blank" rel="noopener"}

> Today, the LockBit ransomware gang claimed they were behind the April cyberattack on Canadian pharmacy chain London Drugs and is now threatening to publish stolen data online after allegedly failed negotiations. [...]
