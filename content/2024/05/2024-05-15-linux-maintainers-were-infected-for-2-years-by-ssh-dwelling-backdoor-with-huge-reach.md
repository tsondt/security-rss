Title: Linux maintainers were infected for 2 years by SSH-dwelling backdoor with huge reach
Date: 2024-05-15T16:56:03+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;Uncategorized;ebury;Linux;openssh;secure shell;SSH
Slug: 2024-05-15-linux-maintainers-were-infected-for-2-years-by-ssh-dwelling-backdoor-with-huge-reach

[Source](https://arstechnica.com/?p=2024591){:target="_blank" rel="noopener"}

> Enlarge (credit: BeeBright / Getty Images / iStockphoto ) Infrastructure used to maintain and distribute the Linux operating system kernel was infected for two years, starting in 2009, by sophisticated malware that managed to get a hold of one of the developers’ most closely guarded resources: the /etc/shadow files that stored encrypted password data for more than 550 system users, researchers said Tuesday. The unknown attackers behind the compromise infected at least four servers inside kernel.org, the Internet domain underpinning the sprawling Linux development and distribution network, the researchers from security firm ESET said. After obtaining the cryptographic hashes for [...]
