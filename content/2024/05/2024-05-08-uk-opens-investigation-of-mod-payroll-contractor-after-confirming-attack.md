Title: UK opens investigation of MoD payroll contractor after confirming attack
Date: 2024-05-08T11:15:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-08-uk-opens-investigation-of-mod-payroll-contractor-after-confirming-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/08/uk_opens_investigation_into_contractor/){:target="_blank" rel="noopener"}

> China vehemently denies involvement UK Government has confirmed a cyberattack on the payroll system used by the Ministry of Defence (MoD) led to "malign" forces accessing data on current and a limited number of former armed forces personnel.... [...]
