Title: New BIG-IP Next Central Manager bugs allow device takeover
Date: 2024-05-08T15:52:37-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-08-new-big-ip-next-central-manager-bugs-allow-device-takeover

[Source](https://www.bleepingcomputer.com/news/security/new-big-ip-next-central-manager-bugs-allow-device-takeover/){:target="_blank" rel="noopener"}

> F5 has fixed two high-severity BIG-IP Next Central Manager vulnerabilities, which can be exploited to gain admin control and create rogue accounts on any managed assets. [...]
