Title: Julian Assange can appeal extradition to the US, London High Court rules
Date: 2024-05-20T22:05:24+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-20-julian-assange-can-appeal-extradition-to-the-us-london-high-court-rules

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/20/julian_assange_appeal_extradition/){:target="_blank" rel="noopener"}

> Let me go, Brandon WikiLeaks founder Julian Assange can appeal his extradition to the US from the UK, the High Court of England and Wales ruled Monday.... [...]
