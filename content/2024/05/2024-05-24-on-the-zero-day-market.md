Title: On the Zero-Day Market
Date: 2024-05-24T11:07:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;cyberespionage;spyware;zero-day
Slug: 2024-05-24-on-the-zero-day-market

[Source](https://www.schneier.com/blog/archives/2024/05/on-the-zero-day-market.html){:target="_blank" rel="noopener"}

> New paper: “ Zero Progress on Zero Days: How the Last Ten Years Created the Modern Spyware Market “: Abstract: Spyware makes surveillance simple. The last ten years have seen a global market emerge for ready-made software that lets governments surveil their citizens and foreign adversaries alike and to do so more easily than when such work required tradecraft. The last ten years have also been marked by stark failures to control spyware and its precursors and components. This Article accounts for and critiques these failures, providing a socio-technical history since 2014, particularly focusing on the conversation about trade in [...]
