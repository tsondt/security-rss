Title: Patch Tuesday, May 2024 Edition
Date: 2024-05-14T20:19:23+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;acrobat;Adobe Aero;Adobe Animate;Adobe Framemaker;Adobe Substance 3D Painter;CVE-2024-30040;CVE-2024-30044;CVE-2024-30051;Google Chrome;Illustrator;Immersive Labs;Kevin Breen;macOS Sonoma 14.5 update;MSHTML;Qakbot;reader;Satnam Narang;Sharepoint;Tenable
Slug: 2024-05-14-patch-tuesday-may-2024-edition

[Source](https://krebsonsecurity.com/2024/05/patch-tuesday-may-2024-edition/){:target="_blank" rel="noopener"}

> Microsoft today released updates to fix more than 60 security holes in Windows computers and supported software, including two “zero-day” vulnerabilities in Windows that are already being exploited in active attacks. There are also important security patches available for macOS and Adobe users, and for the Chrome Web browser, which just patched its own zero-day flaw. First, the zero-days. CVE-2024-30051 is an “elevation of privilege” bug in a core Windows library. Satnam Narang at Tenable said this flaw is being used as part of post-compromise activity to elevate privileges as a local attacker. “CVE-2024-30051 is used to gain initial access [...]
