Title: DMM Bitcoin warns that hackers stole $300 million in Bitcoin
Date: 2024-05-31T17:06:07-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-05-31-dmm-bitcoin-warns-that-hackers-stole-300-million-in-bitcoin

[Source](https://www.bleepingcomputer.com/news/security/dmm-bitcoin-warns-that-hackers-stole-300-million-in-bitcoin/){:target="_blank" rel="noopener"}

> Japanese bitcoin exchange DMM Bitcoin is warning that 4,502.9 Bitcoin (BTC), or approximately $308 million (48.2 billion yen), has been stolen from one of its wallets today, making it the most significant cryptocurrency heist of 2024. [...]
