Title: GitHub warns of SAML auth bypass flaw in Enterprise Server
Date: 2024-05-21T11:01:43-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-21-github-warns-of-saml-auth-bypass-flaw-in-enterprise-server

[Source](https://www.bleepingcomputer.com/news/security/github-warns-of-saml-auth-bypass-flaw-in-enterprise-server/){:target="_blank" rel="noopener"}

> GitHub has fixed a maximum severity (CVSS v4 score: 10.0) authentication bypass vulnerability tracked as CVE-2024-4985, which impacts GitHub Enterprise Server (GHES) instances using SAML single sign-on (SSO) authentication. [...]
