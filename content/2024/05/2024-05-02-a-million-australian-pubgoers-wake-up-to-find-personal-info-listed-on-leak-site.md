Title: A million Australian pubgoers wake up to find personal info listed on leak site
Date: 2024-05-02T04:01:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-05-02-a-million-australian-pubgoers-wake-up-to-find-personal-info-listed-on-leak-site

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/02/australian_pubs_data_breach/){:target="_blank" rel="noopener"}

> Man arrested and blackmail charges expected after allegations of unpaid contractors and iffy infosec Updated Over a million records describing Australians who visited local pubs and clubs have apparently been posted online.... [...]
