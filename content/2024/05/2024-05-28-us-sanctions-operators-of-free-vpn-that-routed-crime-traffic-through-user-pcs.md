Title: US sanctions operators of “free VPN” that routed crime traffic through user PCs
Date: 2024-05-28T23:28:48+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;botnets;residential proxy services;Treasury Department
Slug: 2024-05-28-us-sanctions-operators-of-free-vpn-that-routed-crime-traffic-through-user-pcs

[Source](https://arstechnica.com/?p=2027288){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) The US Treasury Department has sanctioned three Chinese nationals for their involvement in a VPN-powered botnet with more than 19 million residential IP addresses they rented out to cybercriminals to obfuscate their illegal activities, including COVID-19 aid scams and bomb threats. The criminal enterprise, the Treasury Department said Tuesday, was a residential proxy service known as 911 S5. Such services provide a bank of IP addresses belonging to everyday home users for customers to route Internet connections through. When accessing a website or other Internet service, the connection appears to originate with the home user. In [...]
