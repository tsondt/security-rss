Title: Dell warns of data breach, 49 million customers allegedly affected
Date: 2024-05-09T11:21:59-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-05-09-dell-warns-of-data-breach-49-million-customers-allegedly-affected

[Source](https://www.bleepingcomputer.com/news/security/dell-warns-of-data-breach-49-million-customers-allegedly-affected/){:target="_blank" rel="noopener"}

> Dell is warning customers of a data breach after a threat actor claimed to have stolen information for approximately 49 million customers. [...]
