Title: British Library's candid ransomware comms driven by 'emotional intelligence'
Date: 2024-05-20T09:32:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-20-british-librarys-candid-ransomware-comms-driven-by-emotional-intelligence

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/20/the_british_library_owes_lauded/){:target="_blank" rel="noopener"}

> It quickly realized ‘dry’ progress updates weren’t cutting it CyberUK Emotional intelligence was at the heart of the British Library's widely hailed response to its October ransomware attack, according to CEO Roly Keating.... [...]
