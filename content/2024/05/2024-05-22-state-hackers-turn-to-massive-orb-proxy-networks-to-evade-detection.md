Title: State hackers turn to massive ORB proxy networks to evade detection
Date: 2024-05-22T13:37:48-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-05-22-state-hackers-turn-to-massive-orb-proxy-networks-to-evade-detection

[Source](https://www.bleepingcomputer.com/news/security/state-hackers-turn-to-massive-orb-proxy-networks-to-evade-detection/){:target="_blank" rel="noopener"}

> Security researchers are warning that China-linked state-backed hackers are increasingly relying on vast proxy networks of virtual private servers and compromised connected devices for cyberespionage operations. [...]
