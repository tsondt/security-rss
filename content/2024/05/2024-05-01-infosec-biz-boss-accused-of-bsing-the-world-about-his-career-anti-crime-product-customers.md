Title: Infosec biz boss accused of BS'ing the world about his career, anti-crime product, customers
Date: 2024-05-01T18:58:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-01-infosec-biz-boss-accused-of-bsing-the-world-about-his-career-anti-crime-product-customers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/01/sec_blount_settlement/){:target="_blank" rel="noopener"}

> Intrusion investors went through Blount farce trauma, says SEC Jack Blount, the now-ex CEO of Intrusion, has settled with the SEC over allegations he made false and misleading statements about his infosec firm's product as well as his own background and experience.... [...]
