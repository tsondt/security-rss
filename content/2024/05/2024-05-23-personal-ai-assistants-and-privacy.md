Title: Personal AI Assistants and Privacy
Date: 2024-05-23T11:00:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;Microsoft;privacy;trust
Slug: 2024-05-23-personal-ai-assistants-and-privacy

[Source](https://www.schneier.com/blog/archives/2024/05/personal-ai-assistants-and-privacy.html){:target="_blank" rel="noopener"}

> Microsoft is trying to create a personal digital assistant: At a Build conference event on Monday, Microsoft revealed a new AI-powered feature called “Recall” for Copilot+ PCs that will allow Windows 11 users to search and retrieve their past activities on their PC. To make it work, Recall records everything users do on their PC, including activities in apps, communications in live meetings, and websites visited for research. Despite encryption and local storage, the new feature raises privacy concerns for certain Windows users. I wrote about this AI trust problem last year: One of the promises of generative AI is [...]
