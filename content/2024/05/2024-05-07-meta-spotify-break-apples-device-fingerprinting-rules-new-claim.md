Title: Meta, Spotify break Apple's device fingerprinting rules – new claim
Date: 2024-05-07T01:05:27+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-05-07-meta-spotify-break-apples-device-fingerprinting-rules-new-claim

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/07/apple_fingerprinting_rules/){:target="_blank" rel="noopener"}

> And the iOS titan doesn't seem that bothered with data leaking out Updated Last week, Apple began requiring iOS developers justify the use of a specific set of APIs that could be used for device fingerprinting.... [...]
