Title: Europol op shutters 12 scam call centers and cuffs 21 suspected fraudsters
Date: 2024-05-03T05:34:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-03-europol-op-shutters-12-scam-call-centers-and-cuffs-21-suspected-fraudsters

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/03/operation_pandora_europol/){:target="_blank" rel="noopener"}

> Cops prevented crims from bilking victims out of more than €10m - but couldn't stop crime against art A Europol-led operation dubbed “Pandora” has shut down a dozen phone scam centers, and arrested 21 suspects. The cops reckon the action prevented criminals from bilking victims out of more than €10 million (£8.6 million, $11 million).... [...]
