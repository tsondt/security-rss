Title: Upcoming Speaking Engagements
Date: 2024-05-14T16:04:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2024-05-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2024/05/upcoming-speaking-engagements-36.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m giving a webinar via Zoom on Wednesday, May 22, at 11:00 AM ET. The topic is “ Should the USG Establish a Publicly Funded AI Option? “ The list is maintained on this page. [...]
