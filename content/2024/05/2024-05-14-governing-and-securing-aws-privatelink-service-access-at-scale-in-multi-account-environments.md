Title: Governing and securing AWS PrivateLink service access at scale in multi-account environments
Date: 2024-05-14T13:43:24+00:00
Author: Anandprasanna Gaitonde
Category: AWS Security
Tags: Advanced (300);Amazon EventBridge;AWS CloudFormation;AWS Config;AWS Lambda;AWS PrivateLink;Security, Identity, & Compliance;Technical How-to;AWS Privatelink;Networking;Security;Security Blog;service control policy
Slug: 2024-05-14-governing-and-securing-aws-privatelink-service-access-at-scale-in-multi-account-environments

[Source](https://aws.amazon.com/blogs/security/governing-and-securing-aws-privatelink-service-access-at-scale-in-multi-account-environments/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) customers have been adopting the approach of using AWS PrivateLink to have secure communication to AWS services, their own internal services, and third-party services in the AWS Cloud. As these environments scale, the number of PrivateLink connections outbound to external services and inbound to internal services increase and are spread out [...]
