Title: Over 50,000 Tinyproxy servers vulnerable to critical RCE flaw
Date: 2024-05-07T13:07:41-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-07-over-50000-tinyproxy-servers-vulnerable-to-critical-rce-flaw

[Source](https://www.bleepingcomputer.com/news/security/over-50-000-tinyproxy-servers-vulnerable-to-critical-rce-flaw/){:target="_blank" rel="noopener"}

> Nearly 52,000 internet-exposed Tinyproxy instances are vulnerable to CVE-2023-49606, a recently disclosed critical remote code execution (RCE) flaw. [...]
