Title: What do Europeans, Americans and Australians have in common? Scammed $50M by fake e-stores
Date: 2024-05-08T23:22:08+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-05-08-what-do-europeans-americans-and-australians-have-in-common-scammed-50m-by-fake-e-stores

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/08/bogusbazaar_fraud_china/){:target="_blank" rel="noopener"}

> BogusBazaar ripped off shoppers and scraped card details, but not in China A crime ring dubbed BogusBazaar has scammed 850,000 people out of tens of millions of dollars via a network of dodgy shopping websites.... [...]
