Title: US woman allegedly aided North Korean IT workers infiltrate 300 firms
Date: 2024-05-16T15:17:13-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-16-us-woman-allegedly-aided-north-korean-it-workers-infiltrate-300-firms

[Source](https://www.bleepingcomputer.com/news/security/five-arizona-ukraine-charged-for-cyber-schemes-infiltrating-over-300-companies-to-benefit-north-koreas-weapons-program/){:target="_blank" rel="noopener"}

> ​The U.S. Justice Department charged five individuals today, a U.S. Citizen woman, a Ukrainian man, and three foreign nationals, for their involvement in cyber schemes that generated revenue for North Korea's nuclear weapons program. [...]
