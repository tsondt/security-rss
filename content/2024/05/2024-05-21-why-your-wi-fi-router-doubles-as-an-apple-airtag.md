Title: Why Your Wi-Fi Router Doubles as an Apple AirTag
Date: 2024-05-21T16:21:20+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Security Tools;The Coming Storm;apple;BSSID;David Levin;Erik Rye;IEEE;Starlink;University of Maryland;Wi-Fi location
Slug: 2024-05-21-why-your-wi-fi-router-doubles-as-an-apple-airtag

[Source](https://krebsonsecurity.com/2024/05/why-your-wi-fi-router-doubles-as-an-apple-airtag/){:target="_blank" rel="noopener"}

> Image: Shutterstock. Apple and the satellite-based broadband service Starlink each recently took steps to address new research into the potential security and privacy implications of how their services geo-locate devices. Researchers from the University of Maryland say they relied on publicly available data from Apple to track the location of billions of devices globally — including non-Apple devices like Starlink systems — and found they could use this data to monitor the destruction of Gaza, as well as the movements and in many cases identities of Russian and Ukrainian troops. At issue is the way that Apple collects and publicly [...]
