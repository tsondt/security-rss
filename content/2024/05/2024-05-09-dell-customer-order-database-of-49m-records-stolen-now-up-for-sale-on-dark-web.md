Title: Dell customer order database of '49M records' stolen, now up for sale on dark web
Date: 2024-05-09T17:55:21+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-05-09-dell-customer-order-database-of-49m-records-stolen-now-up-for-sale-on-dark-web

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/09/dell_data_stolen/){:target="_blank" rel="noopener"}

> IT giant tries to downplay leak as just names, addresses, info about kit Dell has confirmed information about its customers and their orders has been stolen from one of its portals. Though the thief claimed to have swiped 49 million records, which are now up for sale on the dark web, the IT giant declined to say how many people may be affected.... [...]
