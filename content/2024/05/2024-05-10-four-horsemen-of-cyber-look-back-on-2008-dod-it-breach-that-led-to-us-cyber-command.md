Title: 'Four horsemen of cyber' look back on 2008 DoD IT breach that led to US Cyber Command
Date: 2024-05-10T13:00:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-10-four-horsemen-of-cyber-look-back-on-2008-dod-it-breach-that-led-to-us-cyber-command

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/10/dod_usb_attack/){:target="_blank" rel="noopener"}

> 'This was a no sh*tter' RSAC A malware-laced USB stick, inserted into a military laptop at a base in Afghanistan in 2008, led to what has been called the worst military breach in US history, and to the creation of the US Cyber Command.... [...]
