Title: Veeam warns of critical Backup Enterprise Manager auth bypass bug
Date: 2024-05-21T18:24:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-21-veeam-warns-of-critical-backup-enterprise-manager-auth-bypass-bug

[Source](https://www.bleepingcomputer.com/news/security/veeam-warns-of-critical-backup-enterprise-manager-auth-bypass-bug/){:target="_blank" rel="noopener"}

> ​Veeam warned customers today to patch a critical security vulnerability that allows unauthenticated attackers to sign into any account via the Veeam Backup Enterprise Manager (VBEM). [...]
