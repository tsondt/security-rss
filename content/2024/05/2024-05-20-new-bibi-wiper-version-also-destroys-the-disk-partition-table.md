Title: New BiBi Wiper version also destroys the disk partition table
Date: 2024-05-20T12:06:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-20-new-bibi-wiper-version-also-destroys-the-disk-partition-table

[Source](https://www.bleepingcomputer.com/news/security/new-bibi-wiper-version-also-destroys-the-disk-partition-table/){:target="_blank" rel="noopener"}

> A new version of the BiBi Wiper malware is now deleting the disk partition table to make data restoration harder, extending the downtime for targeted victims. [...]
