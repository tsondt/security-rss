Title: Zscaler takes "test environment" offline after rumors of a breach
Date: 2024-05-08T19:30:25-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-05-08-zscaler-takes-test-environment-offline-after-rumors-of-a-breach

[Source](https://www.bleepingcomputer.com/news/security/zscaler-takes-test-environment-offline-after-rumors-of-a-breach/){:target="_blank" rel="noopener"}

> Zscaler says that they discovered an exposed "test environment" that was taken offline for analysis after rumors circulated that a threat actor was selling access to the company's systems. [...]
