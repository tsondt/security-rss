Title: Auction house Christie’s confirms criminals stole some client data
Date: 2024-05-28T13:30:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-28-auction-house-christies-confirms-criminals-stole-some-client-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/28/christies_confirms_cybercriminals_stole_client/){:target="_blank" rel="noopener"}

> Centuries-old institution dodges questions on how it happened as ransomware gang claims credit International auctioning giant Christie's has confirmed data was stolen during an online attack after a top-three ransomware group claimed credit.... [...]
