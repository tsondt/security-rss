Title: Critical vulnerabilities in BIG-IP appliances leave big networks open to intrusion
Date: 2024-05-08T21:35:08+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;big-ip;f5;network intrusion;vulnerabilities
Slug: 2024-05-08-critical-vulnerabilities-in-big-ip-appliances-leave-big-networks-open-to-intrusion

[Source](https://arstechnica.com/?p=2022973){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Researchers on Wednesday reported critical vulnerabilities in a widely used networking appliance that leaves some of the world’s biggest networks open to intrusion. The vulnerabilities reside in BIG-IP Next Central Manager, a component in the latest generation of the BIG-IP line of appliances, which organizations use to manage traffic going into and out of their networks. Seattle-based F5, which sells the product, says its gear is used in 48 of the top 50 corporations as tracked by Fortune. F5 describes the Next Central Manager as a “single, centralized point of control” for managing entire fleets of [...]
