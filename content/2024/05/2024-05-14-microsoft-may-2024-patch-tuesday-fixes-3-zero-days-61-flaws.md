Title: Microsoft May 2024 Patch Tuesday fixes 3 zero-days, 61 flaws
Date: 2024-05-14T13:49:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-05-14-microsoft-may-2024-patch-tuesday-fixes-3-zero-days-61-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-may-2024-patch-tuesday-fixes-3-zero-days-61-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's May 2024 Patch Tuesday, which includes security updates for 61 flaws and three actively exploited or publicly disclosed zero days. [...]
