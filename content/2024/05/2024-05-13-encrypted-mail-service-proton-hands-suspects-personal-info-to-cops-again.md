Title: Encrypted mail service Proton hands suspect's personal info to cops again
Date: 2024-05-13T02:21:18+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-05-13-encrypted-mail-service-proton-hands-suspects-personal-info-to-cops-again

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/13/infosec_in_brief/){:target="_blank" rel="noopener"}

> Plus: Google patches another Chrome security hole, and more Infosec in brief Encrypted email service Proton Mail is in hot water again from some quarters, and for the same thing that earned it flack before: Handing user data over to law enforcement.... [...]
