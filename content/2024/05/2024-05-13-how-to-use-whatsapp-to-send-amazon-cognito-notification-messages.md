Title: How to use WhatsApp to send Amazon Cognito notification messages
Date: 2024-05-13T13:29:24+00:00
Author: Nideesh K T
Category: AWS Security
Tags: Advanced (300);Amazon Cognito;Security, Identity, & Compliance;authentication;AWS KMS;AWS Lambda;AWS Secrets Manager;Security Blog
Slug: 2024-05-13-how-to-use-whatsapp-to-send-amazon-cognito-notification-messages

[Source](https://aws.amazon.com/blogs/security/how-to-use-whatsapp-to-send-amazon-cognito-notification-messages/){:target="_blank" rel="noopener"}

> While traditional channels like email and SMS remain important, businesses are increasingly exploring alternative messaging services to reach their customers more effectively. In recent years, WhatsApp has emerged as a simple and effective way to engage with users. According to statista, as of 2024, WhatsApp is the most popular mobile messenger app worldwide and has [...]
