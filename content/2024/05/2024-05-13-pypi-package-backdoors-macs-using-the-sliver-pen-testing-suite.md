Title: PyPi package backdoors Macs using the Sliver pen-testing suite
Date: 2024-05-13T17:50:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple
Slug: 2024-05-13-pypi-package-backdoors-macs-using-the-sliver-pen-testing-suite

[Source](https://www.bleepingcomputer.com/news/security/pypi-package-backdoors-macs-using-the-sliver-pen-testing-suite/){:target="_blank" rel="noopener"}

> A new package mimicked the popular 'requests' library on the Python Package Index (PyPI) to target macOS devices with the Sliver C2 adversary framework, used for gaining initial access to corporate networks. [...]
