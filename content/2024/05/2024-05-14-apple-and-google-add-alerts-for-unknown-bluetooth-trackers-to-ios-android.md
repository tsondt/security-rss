Title: Apple and Google add alerts for unknown Bluetooth trackers to iOS, Android
Date: 2024-05-14T11:07:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Apple;Google;Mobile
Slug: 2024-05-14-apple-and-google-add-alerts-for-unknown-bluetooth-trackers-to-ios-android

[Source](https://www.bleepingcomputer.com/news/security/apple-and-google-add-alerts-for-unknown-bluetooth-trackers-to-ios-android/){:target="_blank" rel="noopener"}

> On Monday, Apple and Google jointly announced a new privacy feature that warns Android and iOS users when an unknown Bluetooth tracking device travels with them. [...]
