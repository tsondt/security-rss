Title: Advancing the art of AI-driven security with Google Cloud
Date: 2024-05-06T13:00:00+00:00
Author: Umesh Shankar
Category: GCP Security
Tags: Security & Identity
Slug: 2024-05-06-advancing-the-art-of-ai-driven-security-with-google-cloud

[Source](https://cloud.google.com/blog/products/identity-security/advancing-the-art-of-ai-driven-security-with-google-cloud-at-rsa/){:target="_blank" rel="noopener"}

> The advent of generative AI has unlocked new opportunities to empower defenders and security professionals. We have already seen how AI can transform malware analysis at scale as we work to deliver better outcomes for defenders. In fact, using Gemini 1.5 Pro, we were recently able to reverse engineer and analyze the decompiled code of the WannaCry malware in a single pass — and identify the killswitch — in only 34 seconds. Our vision for AI is to accelerate your ability to protect and defend against threats by shifting from manual, time-intensive efforts to assisted and, ultimately, semi-autonomous security — [...]
