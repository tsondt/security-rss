Title: GhostStripe attack haunts self-driving cars by making them ignore road signs
Date: 2024-05-10T14:04:14+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-05-10-ghoststripe-attack-haunts-self-driving-cars-by-making-them-ignore-road-signs

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/10/baidu_apollo_hack/){:target="_blank" rel="noopener"}

> Cameras tested are specced for Baidu's Apollo Six boffins mostly hailing from Singapore-based universities say they can prove it's possible to interfere with autonomous vehicles by exploiting the machines' reliance on camera-based computer vision and cause them to not recognize road signs.... [...]
