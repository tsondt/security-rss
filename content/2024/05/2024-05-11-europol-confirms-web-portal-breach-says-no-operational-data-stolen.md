Title: Europol confirms web portal breach, says no operational data stolen
Date: 2024-05-11T08:36:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-11-europol-confirms-web-portal-breach-says-no-operational-data-stolen

[Source](https://www.bleepingcomputer.com/news/security/europol-confirms-web-portal-breach-says-no-operational-data-stolen/){:target="_blank" rel="noopener"}

> ​Europol, the European Union's law enforcement agency, confirmed that its Europol Platform for Experts (EPE) portal was breached and is now investigating the incident after a threat actor claimed they stole For Official Use Only (FOUO) documents containing classified data. [...]
