Title: EU probes Meta over its provisions for protecting children
Date: 2024-05-16T15:45:12+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2024-05-16-eu-probes-meta-over-its-provisions-for-protecting-children

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/16/eu_investigates_meta_over_its/){:target="_blank" rel="noopener"}

> Has social media biz done enough to comply with Digital Services Act? Maybe not The European Commission has opened formal proceedings to assess whether Meta, the provider of Facebook and Instagram, may have breached the Digital Services Act (DSA) in areas linked to the protection of minors.... [...]
