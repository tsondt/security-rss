Title: REvil ransomware scum sentenced to almost 14 years inside, ordered to pay $16 million
Date: 2024-05-02T06:31:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-02-revil-ransomware-scum-sentenced-to-almost-14-years-inside-ordered-to-pay-16-million

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/02/revil_ransomware_prison/){:target="_blank" rel="noopener"}

> After extorting $700 million from thousands of victims A Ukrainian man has been sentenced to almost 14 years in prison and ordered to pay more than $16 million in restitution for his role in infecting thousands of victims with REvil ransomware.... [...]
