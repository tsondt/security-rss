Title: 70% of CISOs worry their org is at risk of a material cyber attack
Date: 2024-05-23T13:30:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-23-70-of-cisos-worry-their-org-is-at-risk-of-a-material-cyber-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/23/cisco_survey_2024/){:target="_blank" rel="noopener"}

> Wait, why do you want this job again? Chief information security officers around the globe "are nervously looking over the horizon," according to a survey of 1,600 CISOs that found more than two thirds (70 percent) worry their organization is at risk of a material cyber attack over the next 12 months.... [...]
