Title: Qantas app glitch sees boarding passes fly to other accounts
Date: 2024-05-01T15:03:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-01-qantas-app-glitch-sees-boarding-passes-fly-to-other-accounts

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/01/qanta_app_glitch/){:target="_blank" rel="noopener"}

> Issue now resolved and isn't thought to be the work of criminals Aussie airline Qantas says its app is now stable following a data breach that saw boarding passes take off from passengers' accounts.... [...]
