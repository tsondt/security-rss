Title: Stifling Beijing in cyberspace is now British intelligence’s number-one mission
Date: 2024-05-16T14:45:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-16-stifling-beijing-in-cyberspace-is-now-british-intelligences-number-one-mission

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/16/the_uks_alarm_over_china/){:target="_blank" rel="noopener"}

> Annual conference of cyber intel unit shows UK's alarm over China blaring louder than ever Regular attendees of CYBERUK, the annual conference hosted by British intelligence unit the National Cyber Security Centre (NCSC), will know that in addition to the expected conference panels, there is usually an interwoven theme to proceedings.... [...]
