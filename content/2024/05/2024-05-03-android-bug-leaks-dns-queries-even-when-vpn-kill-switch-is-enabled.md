Title: Android bug leaks DNS queries even when VPN kill switch is enabled
Date: 2024-05-03T17:02:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2024-05-03-android-bug-leaks-dns-queries-even-when-vpn-kill-switch-is-enabled

[Source](https://www.bleepingcomputer.com/news/security/android-bug-leaks-dns-queries-even-when-vpn-kill-switch-is-enabled/){:target="_blank" rel="noopener"}

> A Mullvad VPN user has discovered that Android devices leak DNS queries when switching VPN servers even though the "Always-on VPN" feature was enabled with the "Block connections without VPN" option. [...]
