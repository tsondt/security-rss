Title: Casino cyberattacks put a bullseye on Scattered Spider – and the FBI is closing in
Date: 2024-05-23T20:16:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-23-casino-cyberattacks-put-a-bullseye-on-scattered-spider-and-the-fbi-is-closing-in

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/23/mandiant_cto_scattered_spider/){:target="_blank" rel="noopener"}

> Mandiant CTO chats to The Reg about the looming fate of this ransomware crew Interview The cyberattacks against Las Vegas casinos over the summer put a big target on the backs of prime suspects Scattered Spider, according to Mandiant CTO Charles Carmakal.... [...]
