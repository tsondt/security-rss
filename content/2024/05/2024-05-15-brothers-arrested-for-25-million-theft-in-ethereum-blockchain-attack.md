Title: Brothers arrested for $25 million theft in Ethereum blockchain attack
Date: 2024-05-15T14:36:46-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-05-15-brothers-arrested-for-25-million-theft-in-ethereum-blockchain-attack

[Source](https://www.bleepingcomputer.com/news/security/brothers-arrested-for-25-million-theft-in-ethereum-blockchain-attack/){:target="_blank" rel="noopener"}

> ​The U.S. Department of Justice has indicted two brothers for allegedly manipulating the Ethereum blockchain and stealing $25 million worth of cryptocurrency within approximately 12 seconds in a "first-of-its-kind" scheme. [...]
