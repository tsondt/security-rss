Title: Gawd, after that week, we wonder what's next for China and the Western world
Date: 2024-05-18T12:35:12+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-05-18-gawd-after-that-week-we-wonder-whats-next-for-china-and-the-western-world

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/18/register_kettle_china/){:target="_blank" rel="noopener"}

> For starters: Crypto, import tariffs, and Microsoft shipping out staff Kettle It's been a fairly troubling week in terms of the relationship between China and the Western world.... [...]
