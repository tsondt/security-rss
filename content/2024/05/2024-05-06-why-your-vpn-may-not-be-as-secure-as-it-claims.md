Title: Why Your VPN May Not Be As Secure As It Claims
Date: 2024-05-06T14:24:47+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;The Coming Storm;Web Fraud 2.0;Bill Woodcock;Dani Cronce;DHCP option 121;DHCP starvation attack;Dynamic Host Control Protocol;John Kristoff;Leviathan Security;Lizzie Moratti;Packet Clearing House
Slug: 2024-05-06-why-your-vpn-may-not-be-as-secure-as-it-claims

[Source](https://krebsonsecurity.com/2024/05/why-your-vpn-may-not-be-as-secure-as-it-claims/){:target="_blank" rel="noopener"}

> Virtual private networking (VPN) companies market their services as a way to prevent anyone from snooping on your Internet usage. But new research suggests this is a dangerous assumption when connecting to a VPN via an untrusted network, because attackers on the same network could force a target’s traffic off of the protection provided by their VPN without triggering any alerts to the user. Image: Shutterstock. When a device initially tries to connect to a network, it broadcasts a message to the entire local network stating that it is requesting an Internet address. Normally, the only system on the network [...]
