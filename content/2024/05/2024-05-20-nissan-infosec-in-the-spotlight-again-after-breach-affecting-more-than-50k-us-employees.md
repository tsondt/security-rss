Title: Nissan infosec in the spotlight again after breach affecting more than 50K US employees
Date: 2024-05-20T02:28:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-05-20-nissan-infosec-in-the-spotlight-again-after-breach-affecting-more-than-50k-us-employees

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/20/in_brief_security/){:target="_blank" rel="noopener"}

> PLUS: Connected automakers put on notice; Cisco Talos develops macOS fuzzing technique; Last week's critical vulns Infosec in brief Nissan has admitted to another data loss – this time involving the theft of personal information belonging to more than 50,000 Nissan employees.... [...]
