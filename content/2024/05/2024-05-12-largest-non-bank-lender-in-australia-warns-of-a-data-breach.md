Title: Largest non-bank lender in Australia warns of a data breach
Date: 2024-05-12T10:16:32-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-12-largest-non-bank-lender-in-australia-warns-of-a-data-breach

[Source](https://www.bleepingcomputer.com/news/security/largest-non-bank-lender-in-australia-warns-of-a-data-breach/){:target="_blank" rel="noopener"}

> Firstmac Limited is warning customers that it suffered a data breach a day after the new Embargo cyber-extortion group leaked over 500GB of data allegedly stolen from the firm. [...]
