Title: Widely used modems in industrial IoT devices open to SMS attack
Date: 2024-05-10T04:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-10-widely-used-modems-in-industrial-iot-devices-open-to-sms-attack

[Source](https://www.bleepingcomputer.com/news/security/widely-used-modems-in-industrial-iot-devices-open-to-sms-attack/){:target="_blank" rel="noopener"}

> Security flaws in Telit Cinterion cellular modems, widely used in sectors including industrial, healthcare, and telecommunications, could allow remote attackers to execute arbitrary code via SMS. [...]
