Title: SEC: Financial orgs have 30 days to send data breach notifications
Date: 2024-05-17T12:13:25-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-05-17-sec-financial-orgs-have-30-days-to-send-data-breach-notifications

[Source](https://www.bleepingcomputer.com/news/security/sec-financial-orgs-have-30-days-to-send-data-breach-notifications/){:target="_blank" rel="noopener"}

> The Securities and Exchange Commission (SEC) has adopted amendments to Regulation S-P that require certain financial institutions to disclose data breach incidents to impacted individuals within 30 days of discovery. [...]
