Title: One year on, universities org admits MOVEit attack hit data of 800K people
Date: 2024-05-08T14:00:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-08-one-year-on-universities-org-admits-moveit-attack-hit-data-of-800k-people

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/08/georgia_state_education_moveit/){:target="_blank" rel="noopener"}

> Nearly 95M people in total snagged by flaw in file transfer tool Just short of a year after the initial incident, the state of Georgia's higher education government agency has confirmed that it was the victim of an attack on its systems affecting the data of 800,000 people.... [...]
