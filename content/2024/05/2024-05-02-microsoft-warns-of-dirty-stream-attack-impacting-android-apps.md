Title: Microsoft warns of "Dirty Stream" attack impacting Android apps
Date: 2024-05-02T12:02:45-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Microsoft;Mobile
Slug: 2024-05-02-microsoft-warns-of-dirty-stream-attack-impacting-android-apps

[Source](https://www.bleepingcomputer.com/news/security/microsoft-warns-of-dirty-stream-attack-impacting-android-apps/){:target="_blank" rel="noopener"}

> Microsoft has highlighted a novel attack dubbed "Dirty Stream," which could allow malicious Android apps to overwrite files in another application's home directory, potentially leading to arbitrary code execution and secrets theft. [...]
