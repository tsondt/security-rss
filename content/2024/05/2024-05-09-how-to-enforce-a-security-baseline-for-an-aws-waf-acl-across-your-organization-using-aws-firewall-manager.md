Title: How to enforce a security baseline for an AWS WAF ACL across your organization using AWS Firewall Manager
Date: 2024-05-09T13:36:42+00:00
Author: Omner Barajas
Category: AWS Security
Tags: Advanced (300);AWS Firewall Manager;AWS WAF;Security, Identity, & Compliance;Technical How-to;Security;Security Blog
Slug: 2024-05-09-how-to-enforce-a-security-baseline-for-an-aws-waf-acl-across-your-organization-using-aws-firewall-manager

[Source](https://aws.amazon.com/blogs/security/how-to-enforce-a-security-baseline-for-an-aws-waf-acl-across-your-organization-using-aws-firewall-manager/){:target="_blank" rel="noopener"}

> Most organizations prioritize protecting their web applications that are exposed to the internet. Using the AWS WAF service, you can create rules to control bot traffic, help prevent account takeover fraud, and block common threat patterns such as SQL injection or cross-site scripting (XSS). Further, for those customers managing multi-account environments, it is possible to [...]
