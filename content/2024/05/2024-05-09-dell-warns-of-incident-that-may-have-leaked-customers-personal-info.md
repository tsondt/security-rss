Title: Dell warns of “incident” that may have leaked customers’ personal info
Date: 2024-05-09T18:40:08+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;breaches;dell;personally identifiable information;PII;scams
Slug: 2024-05-09-dell-warns-of-incident-that-may-have-leaked-customers-personal-info

[Source](https://arstechnica.com/?p=2023185){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty ) For years, Dell customers have been on the receiving end of scam calls from people claiming to be part of the computer maker’s support team. The scammers call from a valid Dell phone number, know the customer's name and address, and use information that should be known only to Dell and the customer, including the service tag number, computer model, and serial number associated with a past purchase. Then the callers attempt to scam the customer into making a payment, installing questionable software, or taking some other potentially harmful action. Recently, according to numerous social media [...]
