Title: Introducing Google Security Operations: Intel-driven, AI-powered SecOps
Date: 2024-05-06T13:00:00+00:00
Author: Chris Corde
Category: GCP Security
Tags: Security & Identity
Slug: 2024-05-06-introducing-google-security-operations-intel-driven-ai-powered-secops

[Source](https://cloud.google.com/blog/products/identity-security/introducing-google-security-operations-intel-driven-ai-powered-secops-at-rsa/){:target="_blank" rel="noopener"}

> In the generative AI-era, security teams are looking for a fully-operational, high-performing security operations solution that can drive productivity while empowering defenders to detect and mitigate new threats. Today at the RSA Conference in San Francisco, we’re announcing AI innovations across the Google Cloud Security portfolio, including Google Threat Intelligence, and the latest release of Google Security Operations. Today’s update is designed help to reduce the do-it-yourself complexity of SecOps and enhance the productivity of your entire Security Operations Center. Introducing Google Security Operations Turn intelligence into action At Next ‘24, we shared how Applied Threat Intelligence can help teams [...]
