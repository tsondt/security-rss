Title: UK data watchdog wants six figures from N Ireland cops after 2023 data leak
Date: 2024-05-23T08:30:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-23-uk-data-watchdog-wants-six-figures-from-n-ireland-cops-after-2023-data-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/23/uks_ico_wants_six_figures/){:target="_blank" rel="noopener"}

> Massive discount applied to save cop shop’s helicopter budget Following a data leak that brought "tangible fear of threat to life", the UK's data protection watchdog says it intends to fine the Police Service of Northern Ireland (PSNI) £750,000 ($955,798).... [...]
