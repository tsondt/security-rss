Title: Norway recommends replacing SSL VPN to prevent breaches
Date: 2024-05-16T15:07:45-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-16-norway-recommends-replacing-ssl-vpn-to-prevent-breaches

[Source](https://www.bleepingcomputer.com/news/security/norway-recommends-replacing-ssl-vpn-to-prevent-breaches/){:target="_blank" rel="noopener"}

> The Norwegian National Cyber Security Centre (NCSC) recommends replacing SSLVPN/WebVPN solutions with alternatives due to the repeated exploitation of related vulnerabilities in edge network devices to breach corporate networks. [...]
