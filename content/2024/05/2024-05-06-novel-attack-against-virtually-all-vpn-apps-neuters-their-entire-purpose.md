Title: Novel attack against virtually all VPN apps neuters their entire purpose
Date: 2024-05-06T20:35:29+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;encryption;IP addresses;virtual private networks;vpns
Slug: 2024-05-06-novel-attack-against-virtually-all-vpn-apps-neuters-their-entire-purpose

[Source](https://arstechnica.com/?p=2022250){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Researchers have devised an attack against nearly all virtual private network applications that forces them to send and receive some or all traffic outside of the encrypted tunnel designed to protect it from snooping or tampering. TunnelVision, as the researchers have named their attack, largely negates the entire purpose and selling point of VPNs, which is to encapsulate incoming and outgoing Internet traffic in an encrypted tunnel and to cloak the user’s IP address. The researchers believe it affects all VPN applications when they’re connected to a hostile network and that there are no ways to [...]
