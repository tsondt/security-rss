Title: REvil hacker behind Kaseya ransomware attack gets 13 years in prison
Date: 2024-05-02T10:44:23-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-05-02-revil-hacker-behind-kaseya-ransomware-attack-gets-13-years-in-prison

[Source](https://www.bleepingcomputer.com/news/security/revil-hacker-behind-kaseya-ransomware-attack-gets-13-years-in-prison/){:target="_blank" rel="noopener"}

> Yaroslav Vasinskyi, a Ukrainian national, was sentenced to 13 years and seven months in prison and ordered to pay $16 million in restitution for his involvement in the REvil ransomware operation. [...]
