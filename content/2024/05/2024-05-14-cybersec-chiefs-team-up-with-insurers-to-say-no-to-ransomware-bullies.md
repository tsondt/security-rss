Title: Cybersec chiefs team up with insurers to say 'no' to ransomware bullies
Date: 2024-05-14T16:15:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-14-cybersec-chiefs-team-up-with-insurers-to-say-no-to-ransomware-bullies

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/14/uk_ncsc_partners_with_insurance/){:target="_blank" rel="noopener"}

> Guidebook aims to undermine the criminal business model The latest effort to reduce the number of ransom payments sent to cybercriminals in the UK involves the country's National Cyber Security Centre (NCSC) locking arms with insurance associations.... [...]
