Title: Federal frenzy to patch gaping GitLab account takeover hole
Date: 2024-05-02T14:15:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-02-federal-frenzy-to-patch-gaping-gitlab-account-takeover-hole

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/02/critical_gitlab_vulnerability/){:target="_blank" rel="noopener"}

> Warning comes exactly a year after the vulnerability was introduced The US Cybersecurity and Infrastructure Security Agency (CISA) is forcing all federal agencies to patch a critical vulnerability in GitLab's Community and Enterprise editions, confirming it is very much under "active exploit."... [...]
