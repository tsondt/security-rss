Title: Lattice-Based Cryptosystems and Quantum Cryptanalysis
Date: 2024-05-28T11:09:28+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptography;essays;quantum computing;quantum cryptography
Slug: 2024-05-28-lattice-based-cryptosystems-and-quantum-cryptanalysis

[Source](https://www.schneier.com/blog/archives/2024/05/lattice-based-cryptosystems-and-quantum-cryptanalysis.html){:target="_blank" rel="noopener"}

> Quantum computers are probably coming, though we don’t know when—and when they arrive, they will, most likely, be able to break our standard public-key cryptography algorithms. In anticipation of this possibility, cryptographers have been working on quantum-resistant public-key algorithms. The National Institute for Standards and Technology (NIST) has been hosting a competition since 2017, and there already are several proposed standards. Most of these are based on lattice problems. The mathematics of lattice cryptography revolve around combining sets of vectors—that’s the lattice—in a multi-dimensional space. These lattices are filled with multi-dimensional periodicities. The hard problem that’s used in cryptography is [...]
