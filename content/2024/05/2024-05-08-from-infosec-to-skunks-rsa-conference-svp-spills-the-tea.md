Title: From infosec to skunks, RSA Conference SVP spills the tea
Date: 2024-05-08T04:03:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-08-from-infosec-to-skunks-rsa-conference-svp-spills-the-tea

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/08/rsa_conference_svp_interview/){:target="_blank" rel="noopener"}

> Keynotes, physical security, playlists... the buck stops with Linda Gray Martin Interview The 33rd RSA Conference is underway this week, and no one feels that more acutely than the cybersecurity event's SVP Linda Gray Martin.... [...]
