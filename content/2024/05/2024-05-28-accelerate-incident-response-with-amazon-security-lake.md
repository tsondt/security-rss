Title: Accelerate incident response with Amazon Security Lake
Date: 2024-05-28T15:54:05+00:00
Author: Jerry Chen
Category: AWS Security
Tags: Advanced (300);Amazon Security Lake;Security, Identity, & Compliance;Technical How-to;Incident response;Security;Security Blog
Slug: 2024-05-28-accelerate-incident-response-with-amazon-security-lake

[Source](https://aws.amazon.com/blogs/security/accelerate-incident-response-with-amazon-security-lake/){:target="_blank" rel="noopener"}

> This blog post is the first of a two-part series that will demonstrate the value of Amazon Security Lake and how you can use it and other resources to accelerate your incident response (IR) capabilities. Security Lake is a purpose-built data lake that centrally stores your security logs in a common, industry-standard format. In part [...]
