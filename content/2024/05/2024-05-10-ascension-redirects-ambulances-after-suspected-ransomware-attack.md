Title: Ascension redirects ambulances after suspected ransomware attack
Date: 2024-05-10T14:51:56-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-05-10-ascension-redirects-ambulances-after-suspected-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/healthcare-giant-ascension-redirects-ambulances-after-suspected-Black-Basta-ransomware-attack/){:target="_blank" rel="noopener"}

> Ascension, a major U.S. healthcare network, is diverting ambulances from several hospitals due to a suspected ransomware attack that has been causing clinical operation disruptions and system outages since Wednesday. [...]
