Title: How's Uncle Sam getting on with Biden's AI exec order? Pretty good, we're told
Date: 2024-05-27T16:56:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-05-27-hows-uncle-sam-getting-on-with-bidens-ai-exec-order-pretty-good-were-told

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/27/government_agencies_slowly_getting_onboard/){:target="_blank" rel="noopener"}

> Former Pentagon deputy CIO Rob Carey tells us guardrails should steer Feds away from bad ML Interview President Biden's October executive order encouraging the safe use of AI included a ton of requirements for federal government agencies that are developing and deploying machine learning technologies.... [...]
