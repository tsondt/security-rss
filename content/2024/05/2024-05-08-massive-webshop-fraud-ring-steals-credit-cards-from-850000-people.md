Title: Massive webshop fraud ring steals credit cards from 850,000 people
Date: 2024-05-08T10:53:01-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-05-08-massive-webshop-fraud-ring-steals-credit-cards-from-850000-people

[Source](https://www.bleepingcomputer.com/news/security/massive-webshop-fraud-ring-steals-credit-cards-from-850-000-people/){:target="_blank" rel="noopener"}

> A massive network of 75,000 fake online shops called 'BogusBazaar' tricked over 850,000 people in the US and Europe into making purchases, allowing the criminals to steal credit card information and attempt to process an estimated $50 million in fake orders. [...]
