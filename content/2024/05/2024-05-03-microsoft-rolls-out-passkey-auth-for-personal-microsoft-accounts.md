Title: Microsoft rolls out passkey auth for personal Microsoft accounts
Date: 2024-05-03T11:17:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-05-03-microsoft-rolls-out-passkey-auth-for-personal-microsoft-accounts

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-rolls-out-passkey-auth-for-personal-microsoft-accounts/){:target="_blank" rel="noopener"}

> Microsoft announced that Windows users can now log into their Microsoft consumer accounts using a passkey, allowing users to authenticate using password-less methods such as Windows Hello, FIDO2 security keys, biometric data (facial scans or fingerprints), or device PINs. [...]
