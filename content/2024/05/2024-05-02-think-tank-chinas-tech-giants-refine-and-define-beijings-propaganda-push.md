Title: Think tank: China's tech giants refine and define Beijing's propaganda push
Date: 2024-05-02T06:57:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-05-02-think-tank-chinas-tech-giants-refine-and-define-beijings-propaganda-push

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/02/china_big_data_proganda/){:target="_blank" rel="noopener"}

> Taking down TikTok won't stop the CCP's attempt to control global narratives Chinese tech companies that serve as important links in the world's digital supply chains are helping Beijing to execute and refine its propaganda strategy, according to an Australian think tank.... [...]
