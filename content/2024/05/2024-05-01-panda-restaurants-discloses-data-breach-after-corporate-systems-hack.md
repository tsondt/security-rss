Title: Panda Restaurants discloses data breach after corporate systems hack
Date: 2024-05-01T13:35:03-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-01-panda-restaurants-discloses-data-breach-after-corporate-systems-hack

[Source](https://www.bleepingcomputer.com/news/security/panda-restaurants-discloses-a-data-breach-after-corporate-systems-hack/){:target="_blank" rel="noopener"}

> Panda Restaurant Group, the parent company of Panda Express, Panda Inn, and Hibachi-San, disclosed a data breach after attackers compromised its corporate systems in March and stole the personal information of an undisclosed number of individuals. [...]
