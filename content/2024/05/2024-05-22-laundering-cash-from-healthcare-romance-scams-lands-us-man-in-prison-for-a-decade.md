Title: Laundering cash from healthcare, romance scams lands US man in prison for a decade
Date: 2024-05-22T18:00:10+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-05-22-laundering-cash-from-healthcare-romance-scams-lands-us-man-in-prison-for-a-decade

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/22/health_care_and_romance_frauds/){:target="_blank" rel="noopener"}

> $4.5M slushed through accounts from state healthcare and lonely people Georgia resident Malachi Mullings received a decade-long sentence for laundering money scored in scams against healthcare providers, private companies, and individuals to the tune of $4.5 million.... [...]
