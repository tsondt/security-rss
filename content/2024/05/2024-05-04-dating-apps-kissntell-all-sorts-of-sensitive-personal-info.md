Title: Dating apps kiss'n'tell all sorts of sensitive personal info
Date: 2024-05-04T18:00:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-04-dating-apps-kissntell-all-sorts-of-sensitive-personal-info

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/04/dating_apps_privacy_mozilla/){:target="_blank" rel="noopener"}

> Privacy Not Included label slapped on 22 of 25 top lonely-hearts corners Interview Dating apps ask people to disclose all kinds of personal information in the hope of them finding love, or at least a hook-up.... [...]
