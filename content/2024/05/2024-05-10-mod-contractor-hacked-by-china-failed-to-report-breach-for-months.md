Title: MoD contractor hacked by China failed to report breach for months
Date: 2024-05-10T15:00:23+00:00
Author: Anna Isaac and Dan Sabbagh
Category: The Guardian
Tags: Hacking;Ministry of Defence;Data and computer security;Technology;Business;UK news;Cybercrime;Cyberwar;Espionage;Internet;World news
Slug: 2024-05-10-mod-contractor-hacked-by-china-failed-to-report-breach-for-months

[Source](https://www.theguardian.com/technology/article/2024/may/10/mod-contractor-hacked-china-failed-report-breach-months){:target="_blank" rel="noopener"}

> Exclusive: Defence ministry was told in recent days that staff details accessed but sources say SSCL knew in February The IT company targeted in a Chinese hack that accessed the data of hundreds of thousands of Ministry of Defence staff failed to report the breach for months, the Guardian can reveal. The UK defence secretary, Grant Shapps, told MPs on Tuesday that Shared Services Connected Ltd (SSCL) had been breached by a malign actor and “state involvement” could not be ruled out. Continue reading... [...]
