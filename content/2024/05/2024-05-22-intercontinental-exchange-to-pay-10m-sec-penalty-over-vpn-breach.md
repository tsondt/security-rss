Title: Intercontinental Exchange to pay $10M SEC penalty over VPN breach
Date: 2024-05-22T13:20:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-22-intercontinental-exchange-to-pay-10m-sec-penalty-over-vpn-breach

[Source](https://www.bleepingcomputer.com/news/security/intercontinental-exchange-to-pay-10m-sec-penalty-over-vpn-breach/){:target="_blank" rel="noopener"}

> The Intercontinental Exchange (ICE) will pay a $10 million penalty to settle charges brought by the U.S. Securities and Exchange Commission (SEC) after failing to ensure its subsidiaries promptly reported an April 2021 VPN security breach. [...]
