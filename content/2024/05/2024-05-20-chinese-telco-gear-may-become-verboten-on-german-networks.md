Title: Chinese telco gear may become <i>verboten</i> on German networks
Date: 2024-05-20T06:28:14+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-05-20-chinese-telco-gear-may-become-verboten-on-german-networks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/20/huawei_germany_ban/){:target="_blank" rel="noopener"}

> Industry reportedly pressuring digital ministry not to cut the cord Germany may soon remove Huawei and ZTE equipment from its 5G networks, according to media reports.... [...]
