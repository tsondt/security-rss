Title: CISA's early-warning system helped critical orgs close 852 ransomware holes
Date: 2024-05-07T19:58:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-07-cisas-early-warning-system-helped-critical-orgs-close-852-ransomware-holes

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/07/cisas_ransomware_warnings/){:target="_blank" rel="noopener"}

> In the first year alone, that's saved us all a lot of money and woe Interview As ransomware gangs step up their attacks against healthcare, schools, and other US critical infrastructure, CISA is ramping up a program to help these organizations fix flaws exploited by extortionists in the first place.... [...]
