Title: Iran most likely to launch destructive cyber-attack against US – ex-Air Force intel analyst
Date: 2024-05-10T21:01:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-10-iran-most-likely-to-launch-destructive-cyber-attack-against-us-ex-air-force-intel-analyst

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/10/iran_intel_analysis/){:target="_blank" rel="noopener"}

> But China's the most technologically advanced Interview China remains the biggest cyber threat to the US government, America's critical infrastructure, and its private-sector networks, the nation's intelligence community has assessed.... [...]
