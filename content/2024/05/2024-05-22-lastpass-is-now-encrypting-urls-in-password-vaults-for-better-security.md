Title: LastPass is now encrypting URLs in password vaults for better security
Date: 2024-05-22T13:04:02-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2024-05-22-lastpass-is-now-encrypting-urls-in-password-vaults-for-better-security

[Source](https://www.bleepingcomputer.com/news/security/lastpass-is-now-encrypting-urls-in-password-vaults-for-better-security/){:target="_blank" rel="noopener"}

> LastPass announced it will start encrypting URLs stored in user vaults for enhanced privacy and protection against data breaches and unauthorized access. [...]
