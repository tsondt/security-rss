Title: Cooler Master confirms customer info stolen in data breach
Date: 2024-05-30T11:01:52-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-05-30-cooler-master-confirms-customer-info-stolen-in-data-breach

[Source](https://www.bleepingcomputer.com/news/security/cooler-master-confirms-customer-info-stolen-in-data-breach/){:target="_blank" rel="noopener"}

> Computer hardware manufacturer Cooler Master has confirmed that it suffered a data breach on May 19, allowing a threat actor to steal customer data. [...]
