Title: Confused by the SEC's IT security breach reporting rules? Read this
Date: 2024-05-22T16:30:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-22-confused-by-the-secs-it-security-breach-reporting-rules-read-this

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/22/sec_cybersecurity_disclosure_clarification/){:target="_blank" rel="noopener"}

> 'Clarification' weighs in on material vs voluntary disclosures The US Securities and Exchange Commission (SEC) wants to clarify guidelines for public companies regarding the disclosure of ransomware and other cybersecurity incidents.... [...]
