Title: How Criminals Are Using Generative AI
Date: 2024-05-09T16:05:57+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;cybercrime;LLM
Slug: 2024-05-09-how-criminals-are-using-generative-ai

[Source](https://www.schneier.com/blog/archives/2024/05/how-criminals-are-using-generative-ai.html){:target="_blank" rel="noopener"}

> There’s a new report on how criminals are using generative AI tools: Key Takeaways: Adoption rates of AI technologies among criminals lag behind the rates of their industry counterparts because of the evolving nature of cybercrime. Compared to last year, criminals seem to have abandoned any attempt at training real criminal large language models (LLMs). Instead, they are jailbreaking existing ones. We are finally seeing the emergence of actual criminal deepfake services, with some bypassing user verification used in financial services. [...]
