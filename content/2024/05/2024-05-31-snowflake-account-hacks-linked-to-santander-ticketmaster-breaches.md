Title: Snowflake account hacks linked to Santander, Ticketmaster breaches
Date: 2024-05-31T13:31:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-31-snowflake-account-hacks-linked-to-santander-ticketmaster-breaches

[Source](https://www.bleepingcomputer.com/news/security/snowflake-account-hacks-linked-to-santander-ticketmaster-breaches/){:target="_blank" rel="noopener"}

> A threat actor claiming recent Santander and Ticketmaster breaches says they stole data after hacking into an employee's account at cloud storage company Snowflake. However, Snowflake disputes these claims, saying recent breaches were caused by poorly secured customer accounts. [...]
