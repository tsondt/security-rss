Title: Microsoft, Google do a victory lap around passkeys
Date: 2024-05-02T23:03:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-02-microsoft-google-do-a-victory-lap-around-passkeys

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/02/microsoft_google_passkeys/){:target="_blank" rel="noopener"}

> Windows giant extends passwordless tech to everyone else Microsoft today said it will now let us common folk — not just commercial subscribers — sign into their Microsoft accounts and apps using passkeys with their face, fingerprint, or device PIN.... [...]
