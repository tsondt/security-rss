Title: Western Sydney University data breach exposed student data
Date: 2024-05-21T15:39:43-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Education
Slug: 2024-05-21-western-sydney-university-data-breach-exposed-student-data

[Source](https://www.bleepingcomputer.com/news/security/western-sydney-university-data-breach-exposed-student-data/){:target="_blank" rel="noopener"}

> Western Sydney University (WSU) has notified students and academic staff about a data breach after threat actors breached its Microsoft 365 and Sharepoint environment. [...]
