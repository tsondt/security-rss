Title: 'China-aligned' spyware slingers operating since 2018 unmasked at last
Date: 2024-05-23T03:47:12+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-05-23-china-aligned-spyware-slingers-operating-since-2018-unmasked-at-last

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/23/china_hacking_group/){:target="_blank" rel="noopener"}

> Unfading Sea Haze adept at staying under the radar Bitdefender says it has tracked down and exposed an online gang that has been operating since 2018 nearly without a trace – and likely working for Chinese interests.... [...]
