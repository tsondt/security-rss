Title: Google to push ahead with Chrome's ad-blocker extension overhaul in earnest
Date: 2024-05-31T11:15:15+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-05-31-google-to-push-ahead-with-chromes-ad-blocker-extension-overhaul-in-earnest

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/31/google_prepares_for_chrome_extension/){:target="_blank" rel="noopener"}

> Starting Monday, users will gradually be warned the end is near On Monday, some people using Beta, Dev, and Canary builds of Google Chrome will be presented with a warning when they access their browser's extension management page – located at chrome://extensions.... [...]
