Title: French hospital CHC-SV refuses to pay LockBit extortion demand
Date: 2024-05-01T12:38:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-05-01-french-hospital-chc-sv-refuses-to-pay-lockbit-extortion-demand

[Source](https://www.bleepingcomputer.com/news/security/french-hospital-chc-sv-refuses-to-pay-lockbit-extortion-demand/){:target="_blank" rel="noopener"}

> The Hôpital de Cannes - Simone Veil (CHC-SV) in France announced it received a ransom demand from the Lockbit 3.0 ransomware gang, saying they refuse to pay the ransom. [...]
