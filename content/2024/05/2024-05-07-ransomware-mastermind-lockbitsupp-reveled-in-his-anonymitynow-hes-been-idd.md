Title: Ransomware mastermind LockBitSupp reveled in his anonymity—now he’s been ID’d
Date: 2024-05-07T19:34:00+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;lockbit;operation cronos;ransomware
Slug: 2024-05-07-ransomware-mastermind-lockbitsupp-reveled-in-his-anonymitynow-hes-been-idd

[Source](https://arstechnica.com/?p=2022656){:target="_blank" rel="noopener"}

> Enlarge / Dmitry Yuryevich Khoroshev, aka LockBitSupp (credit: UK National Crime Agency) Since at least 2019, a shadowy figure hiding behind several pseudonyms has publicly gloated for extorting millions of dollars from thousands of victims he and his associates had hacked. Now, for the first time, “LockBitSupp” has been unmasked by an international law enforcement team, and a $10 million bounty has been placed for his arrest. In an indictment unsealed Tuesday, US federal prosecutors unmasked the flamboyant persona as Dmitry Yuryevich Khoroshev, a 31-year-old Russian national. Prosecutors said that during his five years at the helm of LockBit—one of [...]
