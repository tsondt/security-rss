Title: Hacker free-for-all fights for control of home and office routers everywhere
Date: 2024-05-02T00:20:12+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;botnets;malware;routers
Slug: 2024-05-02-hacker-free-for-all-fights-for-control-of-home-and-office-routers-everywhere

[Source](https://arstechnica.com/?p=2021233){:target="_blank" rel="noopener"}

> Enlarge (credit: Aurich Lawson / Ars Technica ) Cybercriminals and spies working for nation-states are surreptitiously coexisting inside compromised name-brand routers as they use the devices to disguise attacks motivated both by financial gain and strategic espionage, researchers said. In some cases, the coexistence is peaceful, as financially motivated hackers provide spies with access to already compromised routers in exchange for a fee, researchers from security firm Trend Micro reported Wednesday. In other cases, hackers working in nation-state-backed advanced persistent threat groups take control of devices previously hacked by the cybercrime groups. Sometimes the devices are independently compromised multiple times [...]
