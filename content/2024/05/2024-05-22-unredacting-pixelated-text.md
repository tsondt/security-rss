Title: Unredacting Pixelated Text
Date: 2024-05-22T11:03:26+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;redaction;steganography
Slug: 2024-05-22-unredacting-pixelated-text

[Source](https://www.schneier.com/blog/archives/2024/05/unredacting-pixelated-text.html){:target="_blank" rel="noopener"}

> Experiments in unredacting text that has been pixelated. [...]
