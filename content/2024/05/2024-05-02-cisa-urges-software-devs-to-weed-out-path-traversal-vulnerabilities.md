Title: CISA urges software devs to weed out path traversal vulnerabilities
Date: 2024-05-02T15:38:31-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-05-02-cisa-urges-software-devs-to-weed-out-path-traversal-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/cisa-urges-software-devs-to-weed-out-path-traversal-vulnerabilities/){:target="_blank" rel="noopener"}

> ​CISA and the FBI urged software companies today to review their products and eliminate path traversal security vulnerabilities before shipping. [...]
