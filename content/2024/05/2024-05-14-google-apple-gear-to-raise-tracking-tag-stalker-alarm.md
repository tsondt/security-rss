Title: Google, Apple gear to raise tracking tag stalker alarm
Date: 2024-05-14T13:30:15+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-05-14-google-apple-gear-to-raise-tracking-tag-stalker-alarm

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/14/android_apple_devices_anti_stalking/){:target="_blank" rel="noopener"}

> After years of people being victimized, it's about time Google and Apple are rolling out an anti-stalking feature for Android 6.0+ and iOS 17.5 that will issue an alert if some scumbag is using a gadget like an AirTag or similar to clandestinely track the user.... [...]
