Title: Crooks plant backdoor in software used by courtrooms around the world
Date: 2024-05-23T22:46:51+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;audio/video;backdoors;cybersecurity;supply chain attacks
Slug: 2024-05-23-crooks-plant-backdoor-in-software-used-by-courtrooms-around-the-world

[Source](https://arstechnica.com/?p=2026911){:target="_blank" rel="noopener"}

> Enlarge (credit: JAVS) A software maker serving more than 10,000 courtrooms throughout the world hosted an application update containing a hidden backdoor that maintained persistent communication with a malicious website, researchers reported Thursday, in the latest episode of a supply-chain attack. The software, known as the JAVS Viewer 8, is a component of the JAVS Suite 8, an application package courtrooms use to record, play back, and manage audio and video from proceedings. Its maker, Louisville, Kentucky-based Justice AV Solutions, says its products are used in more than 10,000 courtrooms throughout the US and 11 other countries. The company has [...]
