Title: Lawmakers advance bill to tighten White House grip on AI model exports
Date: 2024-05-23T00:16:09+00:00
Author: Tobias Mann
Category: The Register
Tags: 
Slug: 2024-05-23-lawmakers-advance-bill-to-tighten-white-house-grip-on-ai-model-exports

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/23/us_lawmakers_advance_bill_to/){:target="_blank" rel="noopener"}

> Vague ML definitions subject to change – yeah, great The House Foreign Affairs Committee voted Wednesday to advance a law bill expanding the White House's authority to police exports of AI systems – including models said to pose a national security threat to the United States.... [...]
