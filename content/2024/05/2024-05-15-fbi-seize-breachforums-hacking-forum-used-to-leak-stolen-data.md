Title: FBI seize BreachForums hacking forum used to leak stolen data
Date: 2024-05-15T10:44:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-05-15-fbi-seize-breachforums-hacking-forum-used-to-leak-stolen-data

[Source](https://www.bleepingcomputer.com/news/security/fbi-seize-breachforums-hacking-forum-used-to-leak-stolen-data/){:target="_blank" rel="noopener"}

> The FBI has seized the notorious BreachForums hacking forum that leaked and sold stolen corporate data to other cybercriminals. [...]
