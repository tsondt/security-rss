Title: Researchers call out QNAP for dragging its heels on patch development
Date: 2024-05-20T14:00:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-05-20-researchers-call-out-qnap-for-dragging-its-heels-on-patch-development

[Source](https://go.theregister.com/feed/www.theregister.com/2024/05/20/qnap_watchtowr/){:target="_blank" rel="noopener"}

> WatchTowr publishes report claiming vendor failed to issue fixes after four months Infosec boffins say they were forced to go public after QNAP failed to fix various vulnerabilities that were reported to it months ago.... [...]
