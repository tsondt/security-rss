Title: Moldovan charged for operating botnet used to push ransomware
Date: 2024-04-17T14:53:30-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-17-moldovan-charged-for-operating-botnet-used-to-push-ransomware

[Source](https://www.bleepingcomputer.com/news/security/moldovan-charged-for-operating-botnet-used-to-push-ransomware/){:target="_blank" rel="noopener"}

> The U.S. Justice Department charged Moldovan national Alexander Lefterov, the owner and operator of a large-scale botnet that infected thousands of computers across the United States. [...]
