Title: Why the US government’s overreliance on Microsoft is a big problem
Date: 2024-04-16T13:55:34+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;cybersecurity;microsoft;syndication
Slug: 2024-04-16-why-the-us-governments-overreliance-on-microsoft-is-a-big-problem

[Source](https://arstechnica.com/?p=2017405){:target="_blank" rel="noopener"}

> Enlarge (credit: Joan Cros via Getty ) When Microsoft revealed in January that foreign government hackers had once again breached its systems, the news prompted another round of recriminations about the security posture of the world’s largest tech company. Despite the angst among policymakers, security experts, and competitors, Microsoft faced no consequences for its latest embarrassing failure. The United States government kept buying and using Microsoft products, and senior officials refused to publicly rebuke the tech giant. It was another reminder of how insulated Microsoft has become from virtually any government accountability, even as the Biden administration vows to make [...]
