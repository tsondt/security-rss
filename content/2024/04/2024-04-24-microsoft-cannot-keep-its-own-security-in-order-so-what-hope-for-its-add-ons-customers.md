Title: Microsoft cannot keep its own security in order, so what hope for its add-ons customers?
Date: 2024-04-24T17:15:11+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-04-24-microsoft-cannot-keep-its-own-security-in-order-so-what-hope-for-its-add-ons-customers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/24/microsoft_security_addons/){:target="_blank" rel="noopener"}

> Secure-by-default... if your pockets are deep enough Microsoft has come under fire for charging for security add-ons despite the company's own patchy record when it comes to vulnerabilities and breaches.... [...]
