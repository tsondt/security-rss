Title: TLS inspection configuration for encrypted egress traffic and AWS Network Firewall
Date: 2024-04-01T18:52:20+00:00
Author: Brandon Carroll
Category: AWS Security
Tags: AWS Network Firewall;Best Practices;Intermediate (200);Security, Identity, & Compliance;Security;Security Blog
Slug: 2024-04-01-tls-inspection-configuration-for-encrypted-egress-traffic-and-aws-network-firewall

[Source](https://aws.amazon.com/blogs/security/tls-inspection-configuration-for-encrypted-egress-traffic-and-aws-network-firewall/){:target="_blank" rel="noopener"}

> In the evolving landscape of network security, safeguarding data as it exits your virtual environment is as crucial as protecting incoming traffic. In a previous post, we highlighted the significance of ingress TLS inspection in enhancing security within Amazon Web Services (AWS) environments. Building on that foundation, I focus on egress TLS inspection in this [...]
