Title: Cisco warns of large-scale brute-force attacks against VPN services
Date: 2024-04-16T12:11:01-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-16-cisco-warns-of-large-scale-brute-force-attacks-against-vpn-services

[Source](https://www.bleepingcomputer.com/news/security/cisco-warns-of-large-scale-brute-force-attacks-against-vpn-services/){:target="_blank" rel="noopener"}

> Cisco warns about a large-scale credential brute-forcing campaign targeting VPN and SSH services on Cisco, CheckPoint, Fortinet, SonicWall, and Ubiquiti devices worldwide. [...]
