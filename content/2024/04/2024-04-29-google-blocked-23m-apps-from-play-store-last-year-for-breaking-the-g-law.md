Title: Google blocked 2.3M apps from Play Store last year for breaking the G law
Date: 2024-04-29T22:20:16+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-04-29-google-blocked-23m-apps-from-play-store-last-year-for-breaking-the-g-law

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/29/google_rejected_apps/){:target="_blank" rel="noopener"}

> Third of a million developer accounts kiboshed, too Google says it stopped 2.28 million Android apps from being published in its official Play Store last year because they violated security rules.... [...]
