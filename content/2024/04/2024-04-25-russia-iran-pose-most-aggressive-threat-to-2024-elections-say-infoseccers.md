Title: Russia, Iran pose most aggressive threat to 2024 elections, say infoseccers
Date: 2024-04-25T13:34:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-25-russia-iran-pose-most-aggressive-threat-to-2024-elections-say-infoseccers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/25/mandiant_russia_and_iran_pose/){:target="_blank" rel="noopener"}

> Google security crew reveal ‘the four Ds’ to be on the watch for It may come as a surprise to absolutely nobody that experts say, in revealing the most prevalent and likely tactics to meddle with elections this year, that state-sponsored cybercriminals pose the biggest threat.... [...]
