Title: Expanded Confidential Computing portfolio and introducing Confidential Accelerators for AI workloads
Date: 2024-04-10T16:00:00+00:00
Author: Nelly Porter
Category: GCP Security
Tags: Google Cloud Next;Security & Identity
Slug: 2024-04-10-expanded-confidential-computing-portfolio-and-introducing-confidential-accelerators-for-ai-workloads

[Source](https://cloud.google.com/blog/products/identity-security/expanding-confidential-computing-for-ai-workloads-next24/){:target="_blank" rel="noopener"}

> Confidential Computing can help organizations process sensitive data in the cloud with strong guarantees around confidentiality. We’ve continued to make strides in maturing this important technology, and are collaborating with industry leaders including Intel, AMD, and NVIDIA on advancing Confidential Computing solutions. Today at Google Cloud Next, we are excited to announce advancements in our Confidential Computing solutions that expand hardware options, add support for data migrations, and further broaden the partnerships that have helped establish Confidential Computing as a vital solution for data security and confidentiality. Confidential VMs with Intel TDX and built-in accelerator with AMX We are excited [...]
