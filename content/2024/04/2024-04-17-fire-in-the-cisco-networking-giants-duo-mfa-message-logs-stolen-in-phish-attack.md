Title: Fire in the Cisco! Networking giant's Duo MFA message logs stolen in phish attack
Date: 2024-04-17T00:06:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-17-fire-in-the-cisco-networking-giants-duo-mfa-message-logs-stolen-in-phish-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/17/cisco_duo_vpn/){:target="_blank" rel="noopener"}

> Also warns of brute force attacks targeting its own VPNs, Check Point, Fortinet, SonicWall and more Cisco is fighting fires on a couple cybersecurity fronts this week involving its Duo multi-factor authentication (MFA) service and its remote-access VPN services.... [...]
