Title: WhatsApp, Threads, more banished from Apple App Store in China
Date: 2024-04-19T14:30:09+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-04-19-whatsapp-threads-more-banished-from-apple-app-store-in-china

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/19/whatsapp_threads_ban_china/){:target="_blank" rel="noopener"}

> Still available in Hong Kong and Macau, for now Apple has removed four apps from its China-regional app store, including Meta's WhatsApp and Threads, after it was ordered to do so by Beijing for security reasons.... [...]
