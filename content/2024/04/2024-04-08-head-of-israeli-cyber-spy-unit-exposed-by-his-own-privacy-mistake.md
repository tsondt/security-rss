Title: Head of Israeli cyber spy unit exposed ... by his own privacy mistake
Date: 2024-04-08T06:28:13+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-04-08-head-of-israeli-cyber-spy-unit-exposed-by-his-own-privacy-mistake

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/08/infosec_news_roundup/){:target="_blank" rel="noopener"}

> Plus: Another local government hobbled by ransomware; Huge rise in infostealing malware; and critical vulns Infosec in brief Protecting your privacy online is hard. So hard, in fact, that even a top Israeli spy who managed to stay incognito for 20 years has found himself exposed after one basic error.... [...]
