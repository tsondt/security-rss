Title: Smuggling Gold by Disguising it as Machine Parts
Date: 2024-04-12T11:01:18+00:00
Author: B. Schneier
Category: Bruce Schneier
Tags: Uncategorized;crime;smuggling
Slug: 2024-04-12-smuggling-gold-by-disguising-it-as-machine-parts

[Source](https://www.schneier.com/blog/archives/2024/04/smuggling-gold-by-disguising-it-as-machine-parts.html){:target="_blank" rel="noopener"}

> Someone got caught trying to smuggle 322 pounds of gold (that’s about 1/4 of a cubic foot) out of Hong Kong. It was disguised as machine parts: On March 27, customs officials x-rayed two air compressors and discovered that they contained gold that had been “concealed in the integral parts” of the compressors. Those gold parts had also been painted silver to match the other components in an attempt to throw customs off the trail. [...]
