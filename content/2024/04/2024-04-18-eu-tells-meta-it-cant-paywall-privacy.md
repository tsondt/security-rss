Title: EU tells Meta it can't paywall privacy
Date: 2024-04-18T12:19:19+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2024-04-18-eu-tells-meta-it-cant-paywall-privacy

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/18/eu_meta_subscription_privacy/){:target="_blank" rel="noopener"}

> Platforms should not confront users with 'binary choice' over personal data use The EU's Data Protection Board (EDPB) has told large online platforms they should not offer users a binary choice between paying for a service and consenting to their personal data being used to provide targeted advertising.... [...]
