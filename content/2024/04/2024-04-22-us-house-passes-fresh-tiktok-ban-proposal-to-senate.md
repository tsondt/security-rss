Title: US House passes fresh TikTok ban proposal to Senate
Date: 2024-04-22T13:00:14+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2024-04-22-us-house-passes-fresh-tiktok-ban-proposal-to-senate

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/22/us_house_of_representatives_tiktok_bill/){:target="_blank" rel="noopener"}

> Sadly no push to end stupid TikTok dances, but ByteDance would have year to offload app stateside Fresh US legislation to force the sale of TikTok locally was passed in Washington over the weekend after an earlier version stalled in the Senate.... [...]
