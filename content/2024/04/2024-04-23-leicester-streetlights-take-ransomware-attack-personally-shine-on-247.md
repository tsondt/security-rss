Title: Leicester streetlights take ransomware attack personally, shine on 24/7
Date: 2024-04-23T11:05:30+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-23-leicester-streetlights-take-ransomware-attack-personally-shine-on-247

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/23/leicester_streetlights_ransomware/){:target="_blank" rel="noopener"}

> City council says it lost control after shutting down systems It's become somewhat cliché in cybersecurity reporting to speculate whether an organization will have the resources to "keep the lights on" after an attack. But the opposite turns out to be true with Leicester City Council following its March ransomware incident.... [...]
