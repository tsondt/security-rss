Title: CISA says Sisense hack impacts critical infrastructure orgs
Date: 2024-04-11T10:55:04-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-11-cisa-says-sisense-hack-impacts-critical-infrastructure-orgs

[Source](https://www.bleepingcomputer.com/news/security/cisa-says-sisense-hack-impacts-critical-infrastructure-orgs/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity and Infrastructure Security Agency (CISA) is investigating the recent breach of data analytics company Sisense, an incident that also impacted critical infrastructure organizations. [...]
