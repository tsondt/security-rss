Title: Old Windows print spooler bug is latest target of Russia's Fancy Bear gang
Date: 2024-04-23T01:15:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-23-old-windows-print-spooler-bug-is-latest-target-of-russias-fancy-bear-gang

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/23/russia_fancy_bear_goose_egg/){:target="_blank" rel="noopener"}

> Putin's pals use 'GooseEgg' malware to launch attacks you can defeat with patches or deletion Russian spies are exploiting a years-old Windows print spooler vulnerability and using a custom tool called GooseEgg to elevate privileges and steal credentials across compromised networks, according to Microsoft Threat Intelligence.... [...]
