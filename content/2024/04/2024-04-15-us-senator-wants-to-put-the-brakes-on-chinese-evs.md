Title: US senator wants to put the brakes on Chinese EVs
Date: 2024-04-15T13:00:14+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2024-04-15-us-senator-wants-to-put-the-brakes-on-chinese-evs

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/15/china_ev_ban/){:target="_blank" rel="noopener"}

> Fears of low-cost invasion and data spies spark call for ban Electric vehicles may become a new front in America's tech war with China after a US senator called for Washington DC to block Chinese-made EVs to protect domestic industries and national security.... [...]
