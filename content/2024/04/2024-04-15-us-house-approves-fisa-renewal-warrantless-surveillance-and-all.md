Title: US House approves FISA renewal – warrantless surveillance and all
Date: 2024-04-15T01:58:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-04-15-us-house-approves-fisa-renewal-warrantless-surveillance-and-all

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/15/security_in_brief/){:target="_blank" rel="noopener"}

> PLUS: Chinese chipmaker Nexperia attacked; A Microsoft-signed backdoor; CISA starts scanning your malware; and more Infosec in brief US Congress nearly killed a reauthorization of FISA Section 702 last week over concerns that it would continue to allow warrantless surveillance of Americans, but an amendment to require a warrant failed to pass.... [...]
