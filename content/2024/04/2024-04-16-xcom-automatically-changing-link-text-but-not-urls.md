Title: X.com Automatically Changing Link Text but Not URLs
Date: 2024-04-16T11:00:58+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;domain names;phishing;Twitter
Slug: 2024-04-16-xcom-automatically-changing-link-text-but-not-urls

[Source](https://www.schneier.com/blog/archives/2024/04/x-com-automatically-changing-link-names-but-not-links.html){:target="_blank" rel="noopener"}

> Brian Krebs reported that X (formerly known as Twitter) started automatically changing twitter.com links to x.com links. The problem is: (1) it changed any domain name that ended with “twitter.com,” and (2) it only changed the link’s appearance (anchortext), not the underlying URL. So if you were a clever phisher and registered fedetwitter.com, people would see the link as fedex.com, but it would send people to fedetwitter.com. Thankfully, the problem has been fixed. [...]
