Title: SurveyLama data breach exposes info of 4.4 million users
Date: 2024-04-03T18:28:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-03-surveylama-data-breach-exposes-info-of-44-million-users

[Source](https://www.bleepingcomputer.com/news/security/surveylama-data-breach-exposes-info-of-44-million-users/){:target="_blank" rel="noopener"}

> Data breach alerting service Have I Been Pwned (HIBP) warns that SurveyLama suffered a data breach in February 2024, which exposed the sensitive data of 4.4 million users. [...]
