Title: Prolific phishing-made-easy emporium LabHost knocked offline in cyber-cop op
Date: 2024-04-18T10:15:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-18-prolific-phishing-made-easy-emporium-labhost-knocked-offline-in-cyber-cop-op

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/18/police_lab_host/){:target="_blank" rel="noopener"}

> Police emit Spotify Wrapped-style videos to let crims know they're being hunted Feature Cops have brought down a dark-web souk that provided cyber criminals with convincing copies of trusted brands' websites for use in phishing campaigns.... [...]
