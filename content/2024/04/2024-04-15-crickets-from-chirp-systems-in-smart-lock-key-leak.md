Title: Crickets from Chirp Systems in Smart Lock Key Leak
Date: 2024-04-15T14:51:17+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Security Tools;August.com;Chirp Systems;Matt Brown;ProPublica;RealPage Inc.;U.S. Cybersecurity & Infrastructure Security Agency
Slug: 2024-04-15-crickets-from-chirp-systems-in-smart-lock-key-leak

[Source](https://krebsonsecurity.com/2024/04/crickets-from-chirp-systems-in-smart-lock-key-leak/){:target="_blank" rel="noopener"}

> The U.S. government is warning that “smart locks” securing entry to an estimated 50,000 dwellings nationwide contain hard-coded credentials that can be used to remotely open any of the locks. The lock’s maker Chirp Systems remains unresponsive, even though it was first notified about the critical weakness in March 2021. Meanwhile, Chirp’s parent company, RealPage, Inc., is being sued by multiple U.S. states for allegedly colluding with landlords to illegally raise rents. On March 7, 2024, the U.S. Cybersecurity & Infrastructure Security Agency (CISA) warned about a remotely exploitable vulnerability with “low attack complexity” in Chirp Systems smart locks. “Chirp [...]
