Title: 96% of US hospital websites share visitor info with Meta, Google, data brokers
Date: 2024-04-11T15:00:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-11-96-of-us-hospital-websites-share-visitor-info-with-meta-google-data-brokers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/11/hospital_website_data_sharing/){:target="_blank" rel="noopener"}

> Could have been worse – last time researchers checked it was 98.6% Hospitals – despite being places where people implicitly expect to have their personal details kept private – frequently use tracking technologies on their websites to share user information with Google, Meta, data brokers, and other third parties, according to research published today.... [...]
