Title: Backdoor in XZ Utils That Almost Happened
Date: 2024-04-11T11:01:51+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;backdoors;cybersecurity;economics of security;hacking;Linux;malware;open source;social engineering;SSH
Slug: 2024-04-11-backdoor-in-xz-utils-that-almost-happened

[Source](https://www.schneier.com/blog/archives/2024/04/backdoor-in-xz-utils-that-almost-happened.html){:target="_blank" rel="noopener"}

> Last week, the internet dodged a major nation-state attack that would have had catastrophic cybersecurity repercussions worldwide. It’s a catastrophe that didn’t happen, so it won’t get much attention—but it should. There’s an important moral to the story of the attack and its discovery : The security of the global internet depends on countless obscure pieces of software written and maintained by even more obscure unpaid, distractible, and sometimes vulnerable volunteers. It’s an untenable situation, and one that is being exploited by malicious actors. Yet precious little is being done to remedy it. Programmers dislike doing extra work. If they [...]
