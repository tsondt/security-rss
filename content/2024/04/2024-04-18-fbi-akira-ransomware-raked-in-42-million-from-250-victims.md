Title: FBI: Akira ransomware raked in $42 million from 250+ victims
Date: 2024-04-18T14:11:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-18-fbi-akira-ransomware-raked-in-42-million-from-250-victims

[Source](https://www.bleepingcomputer.com/news/security/fbi-akira-ransomware-raked-in-42-million-from-250-plus-victims/){:target="_blank" rel="noopener"}

> According to a joint advisory from the FBI, CISA, Europol's European Cybercrime Centre (EC3), and the Netherlands' National Cyber Security Centre (NCSC-NL), the Akira ransomware operation has breached the networks of over 250 organizations and raked in roughly $42 million in ransom payments. [...]
