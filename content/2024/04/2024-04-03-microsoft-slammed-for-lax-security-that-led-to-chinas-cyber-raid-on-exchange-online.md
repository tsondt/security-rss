Title: Microsoft slammed for lax security that led to China's cyber-raid on Exchange Online
Date: 2024-04-03T02:15:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-04-03-microsoft-slammed-for-lax-security-that-led-to-chinas-cyber-raid-on-exchange-online

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/03/cisa_microsoft_exchange_online_china_report/){:target="_blank" rel="noopener"}

> CISA calls for 'fundamental, security-focused reforms' to happen ASAP, delaying work on other software A review of the June 2023 attack on Microsoft's Exchange Online hosted email service – which saw accounts used by senior US officials compromised by a China-linked group called "Storm-0558" – has found that the incident would have been preventable save for Microsoft's lax infosec culture and sub-par cloud security precautions.... [...]
