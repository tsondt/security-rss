Title: UnitedHealth admits IT security breach could 'cover substantial proportion of people in America'
Date: 2024-04-23T12:30:15+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2024-04-23-unitedhealth-admits-it-security-breach-could-cover-substantial-proportion-of-people-in-america

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/23/unitedhealth_admits_breach_substantial/){:target="_blank" rel="noopener"}

> That said, good ol' American healthcare system so elaborately costly, some are forced to avoid altogether UnitedHealth Group, the parent of ransomware-struck Change Healthcare, delivered some very unwelcome news for customers today as it continues to recover from the massively expensive side and disruptive digital break-in.... [...]
