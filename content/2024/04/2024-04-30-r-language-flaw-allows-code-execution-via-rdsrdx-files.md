Title: R language flaw allows code execution via RDS/RDX files
Date: 2024-04-30T14:46:53-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-30-r-language-flaw-allows-code-execution-via-rdsrdx-files

[Source](https://www.bleepingcomputer.com/news/security/r-language-flaw-allows-code-execution-via-rds-rdx-files/){:target="_blank" rel="noopener"}

> A new vulnerability has been discovered in the R programming language that allows arbitrary code execution upon deserializing specially crafted RDS and RDX files. [...]
