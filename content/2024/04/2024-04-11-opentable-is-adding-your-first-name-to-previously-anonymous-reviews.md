Title: OpenTable is adding your first name to previously anonymous reviews
Date: 2024-04-11T18:15:31-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Technology;Security
Slug: 2024-04-11-opentable-is-adding-your-first-name-to-previously-anonymous-reviews

[Source](https://www.bleepingcomputer.com/news/technology/opentable-is-adding-your-first-name-to-previously-anonymous-reviews/){:target="_blank" rel="noopener"}

> Restaurant reservation platform OpenTable says that all reviews on the platform will no longer be fully anonymous starting May 22nd and will now show members' profile pictures and first names. [...]
