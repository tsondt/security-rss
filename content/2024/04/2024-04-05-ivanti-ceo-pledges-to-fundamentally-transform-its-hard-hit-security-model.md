Title: Ivanti CEO pledges to “fundamentally transform” its hard-hit security model
Date: 2024-04-05T17:05:01+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;Security;ivanti;ivanti connect secure;jeff abbott;pulse connect secure;VPN;vpns
Slug: 2024-04-05-ivanti-ceo-pledges-to-fundamentally-transform-its-hard-hit-security-model

[Source](https://arstechnica.com/?p=2015100){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Ivanti, the remote-access company whose remote-access products have been battered by severe exploits in recent months, has pledged a "new era," one that "fundamentally transforms the Ivanti security operating model" backed by "a significant investment" and full board support. CEO Jeff Abbott's open letter promises to revamp "core engineering, security, and vulnerability management," make all products "secure by design," formalize cyber-defense agency partnerships, and "sharing information and learning with our customers." Among the details is the company's promise to improve search abilities in Ivanti's security resources and documentation portal, "powered by AI," and an "Interactive Voice [...]
