Title: Cisco Duo warns third-party data breach exposed SMS MFA logs
Date: 2024-04-15T10:52:39-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-15-cisco-duo-warns-third-party-data-breach-exposed-sms-mfa-logs

[Source](https://www.bleepingcomputer.com/news/security/cisco-duo-warns-third-party-data-breach-exposed-sms-mfa-logs/){:target="_blank" rel="noopener"}

> Cisco Duo's security team warns that hackers stole some customers' VoIP and SMS logs for multi-factor authentication (MFA) messages in a cyberattack on their telephony provider. [...]
