Title: Meet clickjacking's slicker cousin, 'gesture jacking,' aka 'cross window forgery'
Date: 2024-04-03T06:33:07+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-04-03-meet-clickjackings-slicker-cousin-gesture-jacking-aka-cross-window-forgery

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/03/clickjacking_heir_gesture_jacking/){:target="_blank" rel="noopener"}

> Web devs advised to do their part to limit UI redress attacks Web browsers still struggle to prevent clickjacking, an attack technique first noted in 2008 that repurposes web page interface elements to deceive visitors.... [...]
