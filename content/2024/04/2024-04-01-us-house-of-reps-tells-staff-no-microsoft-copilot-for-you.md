Title: US House of Reps tells staff: No Microsoft Copilot for you!
Date: 2024-04-01T22:34:08+00:00
Author: Tobias Mann
Category: The Register
Tags: 
Slug: 2024-04-01-us-house-of-reps-tells-staff-no-microsoft-copilot-for-you

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/01/us_house_copilot_ban/){:target="_blank" rel="noopener"}

> At least not until Redmond's government edition is ready to roll Staff working at the US House Of Representatives have been barred from using Microsoft's Copilot chatbot and AI productivity tools, pending the launch of a version tailored to the needs of government users.... [...]
