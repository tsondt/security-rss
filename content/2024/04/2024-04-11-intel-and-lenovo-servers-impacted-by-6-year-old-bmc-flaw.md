Title: Intel and Lenovo servers impacted by 6-year-old BMC flaw
Date: 2024-04-11T12:50:21-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-11-intel-and-lenovo-servers-impacted-by-6-year-old-bmc-flaw

[Source](https://www.bleepingcomputer.com/news/security/intel-and-lenovo-servers-impacted-by-6-year-old-bmc-flaw/){:target="_blank" rel="noopener"}

> An almost 6-year-old vulnerability in the Lighttpd web server used in Baseboard Management Controllers has been overlooked by many device vendors, including Intel and Lenovo. [...]
