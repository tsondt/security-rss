Title: Nearly 3M people hit in Harvard Pilgrim healthcare data theft
Date: 2024-04-01T14:45:44+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-04-01-nearly-3m-people-hit-in-harvard-pilgrim-healthcare-data-theft

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/01/in_brief_security/){:target="_blank" rel="noopener"}

> Also, TheMoon botnet back for EoL SOHO routers, Sellafield to be prosecuted for 'infosec failures', plus critical vulns Infosec in brief Nearly a year on from the discovery of a massive data theft at healthcare biz Harvard Pilgrim, and the number of victims has now risen to nearly 2.9 million people in all US states.... [...]
