Title: Rubrik files to go public following alliance with Microsoft
Date: 2024-04-02T13:30:09+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2024-04-02-rubrik-files-to-go-public-following-alliance-with-microsoft

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/02/rubrik_files_for_ipo/){:target="_blank" rel="noopener"}

> Cloud cyber resilience model could raise $700M despite $278M losses Cloud security provider Rubrik has filed for an IPO on the New York Stock Exchange following a flurry of similar flotations.... [...]
