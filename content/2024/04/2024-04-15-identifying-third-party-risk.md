Title: Identifying third-party risk
Date: 2024-04-15T08:03:01+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2024-04-15-identifying-third-party-risk

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/15/identifying_thirdparty_risk/){:target="_blank" rel="noopener"}

> The prima facie case for real-time threat intelligence Webinar Cybercriminals are always on the hunt for new ways to breach your privacy, and busy supply chains often look like a good way to get in under the wire.... [...]
