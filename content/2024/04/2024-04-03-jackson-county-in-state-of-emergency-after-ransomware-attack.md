Title: Jackson County in state of emergency after ransomware attack
Date: 2024-04-03T17:10:56-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-03-jackson-county-in-state-of-emergency-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/jackson-county-in-state-of-emergency-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Jackson County, Missouri, is in a state of emergency after a ransomware attack took down some county services on Tuesday. [...]
