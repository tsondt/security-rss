Title: Rarest, strangest, form of Windows saved techie from moment of security madness
Date: 2024-04-22T07:29:08+00:00
Author: Matthew JC Powell
Category: The Register
Tags: 
Slug: 2024-04-22-rarest-strangest-form-of-windows-saved-techie-from-moment-of-security-madness

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/22/who_me/){:target="_blank" rel="noopener"}

> For once, Redmond's finest saved the day - by being rubbish in unexpectedly useful ways Who, Me? It's Monday once again, dear reader, and you know what that means: another dive into the Who, Me? confessional, to share stories of IT gone wrong that Reg readers managed to pretend had gone right.... [...]
