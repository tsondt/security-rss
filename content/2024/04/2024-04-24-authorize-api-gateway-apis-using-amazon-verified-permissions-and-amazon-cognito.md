Title: Authorize API Gateway APIs using Amazon Verified Permissions and Amazon Cognito
Date: 2024-04-24T18:02:43+00:00
Author: Kevin Hakanson
Category: AWS Security
Tags: Amazon API Gateway;Amazon Cognito;Amazon Verified Permissions;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2024-04-24-authorize-api-gateway-apis-using-amazon-verified-permissions-and-amazon-cognito

[Source](https://aws.amazon.com/blogs/security/authorize-api-gateway-apis-using-amazon-verified-permissions-and-amazon-cognito/){:target="_blank" rel="noopener"}

> Externalizing authorization logic for application APIs can yield multiple benefits for Amazon Web Services (AWS) customers. These benefits can include freeing up development teams to focus on application logic, simplifying application and resource access audits, and improving application security by using continual authorization. Amazon Verified Permissions is a scalable permissions management and fine-grained authorization service [...]
