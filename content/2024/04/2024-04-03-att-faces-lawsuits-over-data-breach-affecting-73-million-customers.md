Title: AT&T faces lawsuits over data breach affecting 73 million customers
Date: 2024-04-03T12:28:10-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-04-03-att-faces-lawsuits-over-data-breach-affecting-73-million-customers

[Source](https://www.bleepingcomputer.com/news/security/atandt-faces-lawsuits-over-data-breach-affecting-73-million-customers/){:target="_blank" rel="noopener"}

> AT&T is facing multiple class-action lawsuits following the company's admission to a massive data breach that exposed the sensitive data of 73 million current and former customers. [...]
