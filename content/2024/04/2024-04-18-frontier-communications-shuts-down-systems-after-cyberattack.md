Title: Frontier Communications shuts down systems after cyberattack
Date: 2024-04-18T17:02:24-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-18-frontier-communications-shuts-down-systems-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/frontier-communications-shuts-down-systems-after-cyberattack/){:target="_blank" rel="noopener"}

> ​American telecom provider Frontier Communications is restoring systems after a cybercrime group breached some of its IT systems in a recent cyberattack. [...]
