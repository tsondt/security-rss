Title: Poland launches inquiry into previous government’s spyware use
Date: 2024-04-01T04:00:03+00:00
Author: Shaun Walker in Warsaw
Category: The Guardian
Tags: Poland;Surveillance;Data and computer security;Europe;World news
Slug: 2024-04-01-poland-launches-inquiry-into-previous-governments-spyware-use

[Source](https://www.theguardian.com/world/2024/apr/01/poland-launches-inquiry-into-previous-governments-spyware-use){:target="_blank" rel="noopener"}

> Victims of Pegasus hacking will be notified and criminal proceedings could be brought against former officials Poland has launched an investigation into its previous government’s use of the controversial spyware Pegasus, with a parliamentary inquiry under way and the possibility of criminal charges being brought against former government officials in future. Adam Bodnar, Poland’s new justice minister, told the Guardian that in coming months the government would notify people who were targeted with Pegasus. Under Polish law, they would then have the possibility of seeking financial compensation, and becoming party to potential criminal proceedings. Continue reading... [...]
