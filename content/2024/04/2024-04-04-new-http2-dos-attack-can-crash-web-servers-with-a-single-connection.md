Title: New HTTP/2 DoS attack can crash web servers with a single connection
Date: 2024-04-04T11:28:55-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-04-new-http2-dos-attack-can-crash-web-servers-with-a-single-connection

[Source](https://www.bleepingcomputer.com/news/security/new-http-2-dos-attack-can-crash-web-servers-with-a-single-connection/){:target="_blank" rel="noopener"}

> Newly discovered HTTP/2 protocol vulnerabilities called "CONTINUATION Flood" can lead to denial of service (DoS) attacks, crashing web servers with a single TCP connection in some implementations. [...]
