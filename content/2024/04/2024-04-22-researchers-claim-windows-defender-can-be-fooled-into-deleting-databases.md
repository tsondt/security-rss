Title: Researchers claim Windows Defender can be fooled into deleting databases
Date: 2024-04-22T04:29:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-04-22-researchers-claim-windows-defender-can-be-fooled-into-deleting-databases

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/22/edr_attack_remote_data_deletion/){:target="_blank" rel="noopener"}

> Two rounds of reports and patches may not have completely closed this hole BLACK HAT ASIA Researchers at US/Israeli infosec outfit SafeBreach last Friday discussed flaws in Microsoft and Kaspersky security products that can potentially allow the remote deletion of files. And, they asserted, the hole could remain exploitable – even after both vendors claim to have patched the problem.... [...]
