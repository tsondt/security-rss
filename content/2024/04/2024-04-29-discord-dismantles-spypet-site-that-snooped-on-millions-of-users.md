Title: Discord dismantles Spy.pet site that snooped on millions of users
Date: 2024-04-29T02:29:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-04-29-discord-dismantles-spypet-site-that-snooped-on-millions-of-users

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/29/infosec_in_brief/){:target="_blank" rel="noopener"}

> ALSO: Infostealer spotted hiding in CDN cache, antivirus update hijacked to deliver virus, and some critical vulns Updated - Infosec in brief They say sunlight is the best disinfectant, and that appears to have been true in the case of Discord data harvesting site Spy.pet – as it was recently and swiftly dismantled after its existence and purpose became known.... [...]
