Title: Alleged cryptojacking scheme consumed $3.5M of stolen computing to make just $1M
Date: 2024-04-15T19:46:03+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;AWS;azure;cloud computing;cryptojacking
Slug: 2024-04-15-alleged-cryptojacking-scheme-consumed-35m-of-stolen-computing-to-make-just-1m

[Source](https://arstechnica.com/?p=2017285){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Federal prosecutors indicted a Nebraska man on charges he perpetrated a cryptojacking scheme that defrauded two cloud providers—one based in Seattle and the other in Redmond, Washington—out of $3.5 million. The indictment, filed in US District Court for the Eastern District of New York and unsealed on Monday, charges Charles O. Parks III—45 of Omaha, Nebraska—with wire fraud, money laundering, and engaging in unlawful monetary transactions in connection with the scheme. Parks has yet to enter a plea and is scheduled to make an initial appearance in federal court in Omaha on Tuesday. Parks was arrested [...]
