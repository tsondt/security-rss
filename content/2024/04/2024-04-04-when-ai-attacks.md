Title: When AI attacks
Date: 2024-04-04T08:56:05+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2024-04-04-when-ai-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/04/when_ai_attacks/){:target="_blank" rel="noopener"}

> Watch this webinar for a hair raising journey into the darkest depths of GenAI enabled cyber crime Sponsored Post Artificial intelligence (AI) offers enormous commercial potential but also substantial risks to data security if it is harnessed by cyber criminals intent on stealing or corrupting sensitive information for their own gain.... [...]
