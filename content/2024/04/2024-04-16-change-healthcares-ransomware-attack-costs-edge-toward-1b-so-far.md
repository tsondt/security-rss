Title: Change Healthcare’s ransomware attack costs edge toward $1B so far
Date: 2024-04-16T12:50:33+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-16-change-healthcares-ransomware-attack-costs-edge-toward-1b-so-far

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/16/change_healthcares_ransomware_attack_has/){:target="_blank" rel="noopener"}

> First glimpse at attack financials reveals huge pain UnitedHealth, parent company of ransomware-besieged Change Healthcare, says the total costs of tending to the February cyberattack for the first calendar quarter of 2024 currently stands at $872 million.... [...]
