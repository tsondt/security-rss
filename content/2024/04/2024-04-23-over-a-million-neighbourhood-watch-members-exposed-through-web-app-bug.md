Title: Over a million Neighbourhood Watch members exposed through web app bug
Date: 2024-04-23T08:30:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-23-over-a-million-neighbourhood-watch-members-exposed-through-web-app-bug

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/23/neighbourhood_watch_privacy_bug/){:target="_blank" rel="noopener"}

> Unverified users could scoop up data on high-value individuals without any form of verification process Neighbourhood Watch (NW) groups across the UK can now rest easy knowing the developers behind a communications platform fixed a web app bug that leaked their data en masse.... [...]
