Title: OWASP discloses data breach caused by wiki misconfiguration
Date: 2024-04-01T15:25:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-01-owasp-discloses-data-breach-caused-by-wiki-misconfiguration

[Source](https://www.bleepingcomputer.com/news/security/owasp-discloses-data-breach-caused-by-wiki-misconfiguration/){:target="_blank" rel="noopener"}

> The OWASP Foundation has disclosed a data breach after some members' resumes were exposed online due to a misconfiguration of its old Wiki web server. [...]
