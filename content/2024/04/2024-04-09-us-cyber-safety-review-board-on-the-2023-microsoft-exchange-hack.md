Title: US Cyber Safety Review Board on the 2023 Microsoft Exchange Hack
Date: 2024-04-09T13:56:55+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;China;email;hacking;Microsoft;privacy
Slug: 2024-04-09-us-cyber-safety-review-board-on-the-2023-microsoft-exchange-hack

[Source](https://www.schneier.com/blog/archives/2024/04/us-cyber-safety-review-board-on-the-2023-microsoft-exchange-hack.html){:target="_blank" rel="noopener"}

> US Cyber Safety Review Board released a report on the summer 2023 hack of Microsoft Exchange by China. It was a serious attack by the Chinese government that accessed the emails of senior U.S. government officials. From the executive summary: The Board finds that this intrusion was preventable and should never have occurred. The Board also concludes that Microsoft’s security culture was inadequate and requires an overhaul, particularly in light of the company’s centrality in the technology ecosystem and the level of trust customers place in the company to protect their data and operations. The Board reaches this conclusion based [...]
