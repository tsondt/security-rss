Title: UnitedHealth CEO: 'Decision to pay ransom was mine'
Date: 2024-04-30T19:51:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-30-unitedhealth-ceo-decision-to-pay-ransom-was-mine

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/30/unitedhealth_ceo_ransom/){:target="_blank" rel="noopener"}

> Congress to hear how Citrix MFA snafu led to massive data theft, $870M+ loss UnitedHealth CEO Andrew Witty will tell US lawmakers Wednesday the cybercriminals who hit Change Healthcare with ransomware used stolen credentials to remotely access a Citrix portal that didn't have multi-factor authentication enabled.... [...]
