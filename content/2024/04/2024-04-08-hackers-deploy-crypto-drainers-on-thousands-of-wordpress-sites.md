Title: Hackers deploy crypto drainers on thousands of WordPress sites
Date: 2024-04-08T14:22:30-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-04-08-hackers-deploy-crypto-drainers-on-thousands-of-wordpress-sites

[Source](https://www.bleepingcomputer.com/news/security/hackers-deploy-crypto-drainers-on-thousands-of-wordpress-sites/){:target="_blank" rel="noopener"}

> Almost 2,000 hacked WordPress sites now display fake NFT and discount pop-ups to trick visitors into connecting their wallets to crypto drainers that automatically steal funds. [...]
