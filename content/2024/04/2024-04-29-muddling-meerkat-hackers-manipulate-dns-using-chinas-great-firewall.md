Title: Muddling Meerkat hackers manipulate DNS using China’s Great Firewall
Date: 2024-04-29T16:24:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-29-muddling-meerkat-hackers-manipulate-dns-using-chinas-great-firewall

[Source](https://www.bleepingcomputer.com/news/security/muddling-meerkat-hackers-manipulate-dns-using-chinas-great-firewall/){:target="_blank" rel="noopener"}

> A new cluster of activity tracked as "Muddling Meerkat" is believed to be linked to a Chinese state-sponsored threat actor's manipulation of DNS to probe networks globally since October 2019, with a spike in activity observed in September 2023. [...]
