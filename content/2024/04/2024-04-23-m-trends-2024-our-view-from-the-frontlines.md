Title: M-Trends 2024: Our View from the Frontlines
Date: 2024-04-23T12:00:00+00:00
Author: Jurgen Kutscher
Category: GCP Security
Tags: Security & Identity;Threat Intelligence
Slug: 2024-04-23-m-trends-2024-our-view-from-the-frontlines

[Source](https://cloud.google.com/blog/topics/threat-intelligence/m-trends-2024/){:target="_blank" rel="noopener"}

> Attackers are taking greater strides to evade detection. This is one of the running themes in our latest release: M-Trends 2024. This edition of our annual report continues our tradition of providing relevant attacker and defender metrics, and insights into the latest attacker tactics, techniques and procedures, along with guidance and best practices on how organizations and defenders should be responding to threats. This year’s M-Trends report covers Mandiant Consulting investigations of targeted attack activity conducted between January 1, 2023 and December 31, 2023. During that time, many of our observations demonstrate a more concerted effort by attackers to evade [...]
