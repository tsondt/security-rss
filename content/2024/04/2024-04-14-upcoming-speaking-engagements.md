Title: Upcoming Speaking Engagements
Date: 2024-04-14T16:02:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2024-04-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2024/04/upcoming-speaking-engagements-35.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking twice at RSA Conference 2024 in San Francisco. I’ll be on a panel on software liability on May 6, 2024 at 8:30 AM, and I’m giving a keynote on AI and democracy on May 7, 2024 at 2:25 PM. The list is maintained on this page. [...]
