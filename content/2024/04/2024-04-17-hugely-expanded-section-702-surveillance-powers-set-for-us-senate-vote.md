Title: Hugely expanded Section 702 surveillance powers set for US Senate vote
Date: 2024-04-17T23:44:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-17-hugely-expanded-section-702-surveillance-powers-set-for-us-senate-vote

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/17/fisa_section_702/){:target="_blank" rel="noopener"}

> Opponents warn almost anyone could be asked to share info with Uncle Sam On Thursday the US Senate is expected to reauthorize the contentious warrantless surveillance powers conferred by Section 702 of the Foreign Intelligence Surveillance Act (FISA), and may even strengthen them with language that, according to US Senator Ron Wyden (D-OR), "will force a huge range of companies and individuals to spy for the government."... [...]
