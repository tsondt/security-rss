Title: UK businesses shockingly unaware of how to handle security threats
Date: 2024-04-09T12:41:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-09-uk-businesses-shockingly-unaware-of-how-to-handle-security-threats

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/09/uk_biz_response_to_cybercrime/){:target="_blank" rel="noopener"}

> Many decide to make no changes after detecting a breach UK businesses' response to security breaches has "astounded" experts following the release of the government's official cybercrime stats for 2024.... [...]
