Title: New XZ backdoor scanner detects implant in any Linux binary
Date: 2024-04-02T10:33:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Linux
Slug: 2024-04-02-new-xz-backdoor-scanner-detects-implant-in-any-linux-binary

[Source](https://www.bleepingcomputer.com/news/security/new-xz-backdoor-scanner-detects-implant-in-any-linux-binary/){:target="_blank" rel="noopener"}

> Firmware security firm Binarly has released a free online scanner to detect Linux executables impacted by the XZ Utils supply chain attack, tracked as CVE-2024-3094. [...]
