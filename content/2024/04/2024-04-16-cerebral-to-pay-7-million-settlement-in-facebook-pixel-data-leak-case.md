Title: Cerebral to pay $7 million settlement in Facebook pixel data leak case
Date: 2024-04-16T17:37:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-04-16-cerebral-to-pay-7-million-settlement-in-facebook-pixel-data-leak-case

[Source](https://www.bleepingcomputer.com/news/security/cerebral-to-pay-7-million-settlement-in-facebook-pixel-data-leak-case/){:target="_blank" rel="noopener"}

> The U.S. Federal Trade Commission has reached a settlement with telehealth firm Cerebral in which the company will pay $7,000,000 over allegations of mishandling people's sensitive health data. [...]
