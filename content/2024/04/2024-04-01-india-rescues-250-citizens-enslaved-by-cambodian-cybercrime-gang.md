Title: India rescues 250 citizens enslaved by Cambodian cybercrime gang
Date: 2024-04-01T21:04:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-01-india-rescues-250-citizens-enslaved-by-cambodian-cybercrime-gang

[Source](https://www.bleepingcomputer.com/news/security/india-rescues-250-citizens-enslaved-by-cambodian-cybercrime-gang/){:target="_blank" rel="noopener"}

> The Indian government says it rescued and repatriated 250 citizens who sought jobs in Cambodia, only to be forced into conducting cybercrime once they arrived. [...]
