Title: Introducing Shadow API detection for your Google Cloud environments
Date: 2024-04-10T16:00:00+00:00
Author: Shelly Hershkovitz
Category: GCP Security
Tags: Security & Identity;Google Cloud Next;API Management
Slug: 2024-04-10-introducing-shadow-api-detection-for-your-google-cloud-environments

[Source](https://cloud.google.com/blog/products/api-management/track-down-shadow-apis-with-apigee/){:target="_blank" rel="noopener"}

> Enterprises operate a large and growing number of APIs — more than 200 on average — each a potential front door to sensitive data. Even more challenging can be figuring out which of these APIs are not actively managed “shadow APIs”. Born from well-intended development initiatives and legacy systems, shadow APIs operate without proper oversight or governance, and could be the source of damaging security incidents. Today at Google Cloud Next, we are excited to announce shadow API detection in preview in Advanced API Security, part of our Apigee API Management solution. Securing your APIs with Apigee API Management Apigee [...]
