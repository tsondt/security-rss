Title: UK flooded with forged stamps despite using barcodes — to prevent just that
Date: 2024-04-13T05:05:00-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2024-04-13-uk-flooded-with-forged-stamps-despite-using-barcodes-to-prevent-just-that

[Source](https://www.bleepingcomputer.com/news/security/uk-flooded-with-forged-stamps-despite-using-barcodes-to-prevent-just-that/){:target="_blank" rel="noopener"}

> Royal Mail, the British postal and courier service began switching all snail mail stamps to barcoded stamps last year. The purpose of the barcode was to enhance security, deter stamp reuse, and possibly prevent forgeries—which it has failed to do. [...]
