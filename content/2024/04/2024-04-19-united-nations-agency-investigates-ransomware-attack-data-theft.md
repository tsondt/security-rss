Title: United Nations agency investigates ransomware attack, data theft
Date: 2024-04-19T14:03:31-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-19-united-nations-agency-investigates-ransomware-attack-data-theft

[Source](https://www.bleepingcomputer.com/news/security/united-nations-agency-investigates-ransomware-attack-claimed-by-8Base-gang/){:target="_blank" rel="noopener"}

> ​The United Nations Development Programme (UNDP) is investigating a cyberattack after threat actors breached its IT systems to steal human resources data. [...]
