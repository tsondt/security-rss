Title: PuTTY SSH client flaw allows recovery of cryptographic private keys
Date: 2024-04-16T11:01:57-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-16-putty-ssh-client-flaw-allows-recovery-of-cryptographic-private-keys

[Source](https://www.bleepingcomputer.com/news/security/putty-ssh-client-flaw-allows-recovery-of-cryptographic-private-keys/){:target="_blank" rel="noopener"}

> A vulnerability tracked as CVE-2024-31497 in PuTTY 0.68 through 0.80 could potentially allow attackers with access to 60 cryptographic signatures to recover the private key used for their generation. [...]
