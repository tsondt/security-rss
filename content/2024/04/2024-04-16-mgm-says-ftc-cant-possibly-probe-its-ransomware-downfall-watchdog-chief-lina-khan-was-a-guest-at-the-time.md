Title: MGM says FTC can't possibly probe its ransomware downfall – watchdog chief Lina Khan was a guest at the time
Date: 2024-04-16T20:32:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-16-mgm-says-ftc-cant-possibly-probe-its-ransomware-downfall-watchdog-chief-lina-khan-was-a-guest-at-the-time

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/16/mgm_ftc_lawsuit/){:target="_blank" rel="noopener"}

> What a twist! MGM Resorts wants the FTC to halt a probe into last year's ransomware infection at the mega casino chain – because the watchdog's boss Lina Khan was a guest at one of its hotels during the cyberattack, apparently.... [...]
