Title: US State Department investigates alleged theft of government data
Date: 2024-04-03T14:55:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Government
Slug: 2024-04-03-us-state-department-investigates-alleged-theft-of-government-data

[Source](https://www.bleepingcomputer.com/news/security/us-state-department-investigates-alleged-theft-of-government-data/){:target="_blank" rel="noopener"}

> The U.S. Department of State is investigating claims of a cyber incident after a threat actor leaked documents allegedly stolen from a government contractor. [...]
