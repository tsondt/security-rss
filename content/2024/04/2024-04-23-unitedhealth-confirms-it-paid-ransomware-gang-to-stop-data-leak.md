Title: UnitedHealth confirms it paid ransomware gang to stop data leak
Date: 2024-04-23T10:28:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-04-23-unitedhealth-confirms-it-paid-ransomware-gang-to-stop-data-leak

[Source](https://www.bleepingcomputer.com/news/security/unitedhealth-confirms-it-paid-ransomware-gang-to-stop-data-leak/){:target="_blank" rel="noopener"}

> The UnitedHealth Group has confirmed that it paid a ransom to cybercriminals to protect sensitive data stolen during the Optum ransomware attack in late February. [...]
