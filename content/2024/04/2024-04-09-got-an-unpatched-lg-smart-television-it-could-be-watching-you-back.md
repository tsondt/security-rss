Title: Got an unpatched LG 'smart' television? It could be watching you back
Date: 2024-04-09T18:00:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-09-got-an-unpatched-lg-smart-television-it-could-be-watching-you-back

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/09/lg_tv_critical_bugs/){:target="_blank" rel="noopener"}

> Four fatal flaws allow TV takeover A handful of bugs in LG smart TVs running WebOS could allow an attacker to bypass authorization and gain root access on the device.... [...]
