Title: Change Healthcare faces another ransomware threat—and it looks credible
Date: 2024-04-13T18:25:32+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Security
Slug: 2024-04-13-change-healthcare-faces-another-ransomware-threatand-it-looks-credible

[Source](https://arstechnica.com/?p=2017095){:target="_blank" rel="noopener"}

> Enlarge (credit: iStock / Getty Images Plus ) For months, Change Healthcare has faced an immensely messy ransomware debacle that has left hundreds of pharmacies and medical practices across the United States unable to process claims. Now, thanks to an apparent dispute within the ransomware criminal ecosystem, it may have just become far messier still. In March, the ransomware group AlphV, which had claimed credit for encrypting Change Healthcare’s network and threatened to leak reams of the company’s sensitive health care data, received a $22 million payment —evidence, publicly captured on bitcoin’s blockchain, that Change Healthcare had very likely caved [...]
