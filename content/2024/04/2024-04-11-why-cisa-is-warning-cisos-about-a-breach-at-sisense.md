Title: Why CISA is Warning CISOs About a Breach at Sisense
Date: 2024-04-11T20:48:06+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;The Coming Storm;Nicholas Weaver;Sangram Dash;Sisense breach;U.S. Cybersecurity and Infrastructure Security Agency
Slug: 2024-04-11-why-cisa-is-warning-cisos-about-a-breach-at-sisense

[Source](https://krebsonsecurity.com/2024/04/why-cisa-is-warning-cisos-about-a-breach-at-sisense/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity and Infrastructure Security Agency (CISA) said today it is investigating a breach at business intelligence company Sisense, whose products are designed to allow companies to view the status of multiple third-party online services in a single dashboard. CISA urged all Sisense customers to reset any credentials and secrets that may have been shared with the company, which is the same advice Sisense gave to its customers Wednesday evening. New York City based Sisense has more than a thousand customers across a range of industry verticals, including financial services, telecommunications, healthcare and higher education. On April 10, Sisense [...]
