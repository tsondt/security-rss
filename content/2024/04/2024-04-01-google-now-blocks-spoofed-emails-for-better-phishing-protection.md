Title: Google now blocks spoofed emails for better phishing protection
Date: 2024-04-01T16:29:31-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Google;Security
Slug: 2024-04-01-google-now-blocks-spoofed-emails-for-better-phishing-protection

[Source](https://www.bleepingcomputer.com/news/google/google-now-blocks-spoofed-emails-for-better-phishing-protection/){:target="_blank" rel="noopener"}

> Google has started automatically blocking emails sent by bulk senders who don't meet stricter spam thresholds and authenticate their messages as required by new guidelines to strengthen defenses against spam and phishing attacks. [...]
