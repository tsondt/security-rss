Title: Yacht retailer MarineMax discloses data breach after cyberattack
Date: 2024-04-01T14:37:49-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-01-yacht-retailer-marinemax-discloses-data-breach-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/yacht-retailer-marinemax-discloses-data-breach-after-cyberattack/){:target="_blank" rel="noopener"}

> MarineMax, self-described as one of the world's largest recreational boat and yacht retailers, says attackers stole employee and customer data after breaching its systems in a March cyberattack. [...]
