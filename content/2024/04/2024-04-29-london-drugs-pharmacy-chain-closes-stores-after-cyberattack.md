Title: London Drugs pharmacy chain closes stores after cyberattack
Date: 2024-04-29T13:13:50-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-29-london-drugs-pharmacy-chain-closes-stores-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/london-drugs-pharmacy-chain-closes-stores-after-cyberattack/){:target="_blank" rel="noopener"}

> ​Canadian pharmacy chain London Drugs has closed all its retail stores to contain what it described as a "cybersecurity incident." [...]
