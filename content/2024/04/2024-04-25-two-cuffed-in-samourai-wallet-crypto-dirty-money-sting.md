Title: Two cuffed in Samourai Wallet crypto dirty money sting
Date: 2024-04-25T17:15:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-25-two-cuffed-in-samourai-wallet-crypto-dirty-money-sting

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/25/samourai_wallet_laundering_charges/){:target="_blank" rel="noopener"}

> Suspects in Portugal and the US said to have laundered over $100M Two men alleged to be co-founders of cryptocurrency biz Samourai Wallet face serious charges and potentially decades in US prison over claims they owned a product that facilitated the laundering of over $100 million in criminal cash.... [...]
