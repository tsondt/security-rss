Title: LastPass users targeted in phishing attacks good enough to trick even the savvy
Date: 2024-04-18T18:42:06+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;credential phishing;lastpass;multi factor authentication
Slug: 2024-04-18-lastpass-users-targeted-in-phishing-attacks-good-enough-to-trick-even-the-savvy

[Source](https://arstechnica.com/?p=2018339){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Password-manager LastPass users were recently targeted by a convincing phishing campaign that used a combination of email, SMS, and voice calls to trick targets into divulging their master passwords, company officials said. The attackers used an advanced phishing-as-a-service kit discovered in February by researchers from mobile security firm Lookout. Dubbed CryptoChameleon for its focus on cryptocurrency accounts, the kit provides all the resources needed to trick even relatively savvy people into believing the communications are legitimate. Elements include high-quality URLs, a counterfeit single sign-on page for the service the target is using, and everything needed to [...]
