Title: Nearly 1M medical records feared stolen from City of Hope cancer centers
Date: 2024-04-03T23:33:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-03-nearly-1m-medical-records-feared-stolen-from-city-of-hope-cancer-centers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/03/city_of_hope_data_theft/){:target="_blank" rel="noopener"}

> Is there no cure for this cyber-plague? Nearly one million individuals' personal details, financial account information, and medical records may have been stolen from City of Hope systems in the United States.... [...]
