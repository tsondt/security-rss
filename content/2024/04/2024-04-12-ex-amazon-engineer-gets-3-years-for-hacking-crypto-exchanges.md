Title: Ex-Amazon engineer gets 3 years for hacking crypto exchanges
Date: 2024-04-12T13:54:57-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-12-ex-amazon-engineer-gets-3-years-for-hacking-crypto-exchanges

[Source](https://www.bleepingcomputer.com/news/security/ex-amazon-engineer-gets-3-years-for-hacking-crypto-exchanges/){:target="_blank" rel="noopener"}

> Former Amazon security engineer Shakeeb Ahmed was sentenced to three years in prison for hacking two cryptocurrency exchanges in July 2022 and stealing over $12 million. [...]
