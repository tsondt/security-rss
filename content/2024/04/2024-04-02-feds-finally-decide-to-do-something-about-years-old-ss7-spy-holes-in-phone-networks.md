Title: Feds finally decide to do something about years-old SS7 spy holes in phone networks
Date: 2024-04-02T23:17:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-02-feds-finally-decide-to-do-something-about-years-old-ss7-spy-holes-in-phone-networks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/02/fcc_ss7_security/){:target="_blank" rel="noopener"}

> And Diameter, too, for good measure The FCC appears to finally be stepping up efforts to secure decades-old flaws in American telephone networks that are allegedly being used by foreign governments and surveillance outfits to remotely spy on and monitor wireless devices.... [...]
