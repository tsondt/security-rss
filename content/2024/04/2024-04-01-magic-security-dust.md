Title: Magic Security Dust
Date: 2024-04-01T14:19:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized
Slug: 2024-04-01-magic-security-dust

[Source](https://www.schneier.com/blog/archives/2024/04/magic-security-dust.html){:target="_blank" rel="noopener"}

> Adam Shostack is selling magic security dust. It’s about time someone is commercializing this essential technology. [...]
