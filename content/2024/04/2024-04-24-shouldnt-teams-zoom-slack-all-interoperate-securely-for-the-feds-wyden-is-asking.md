Title: Shouldn't Teams, Zoom, Slack all interoperate securely for the Feds? Wyden is asking
Date: 2024-04-24T19:43:28+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-24-shouldnt-teams-zoom-slack-all-interoperate-securely-for-the-feds-wyden-is-asking

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/24/wyden_government_interoperability/){:target="_blank" rel="noopener"}

> Doctorow: 'The most amazing part is that this isn't already the way it's done' Collaboration software used by federal government agencies — this includes apps from Microsoft, Zoom, Slack, and Google — will be required to work together and be securely end-to-end encrypted, if legislation proposed by US Senator Ron Wyden (D-OR) passes.... [...]
