Title: UK's Investigatory Powers Bill to become law despite tech world opposition
Date: 2024-04-26T12:00:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-26-uks-investigatory-powers-bill-to-become-law-despite-tech-world-opposition

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/26/investigatory_powers_bill/){:target="_blank" rel="noopener"}

> Only minor changes from original proposals that kicked up privacy storm The UK's contentious Investigatory Powers (Amendment) Bill (IPB) 2024 has officially received the King's nod of approval and will become law.... [...]
