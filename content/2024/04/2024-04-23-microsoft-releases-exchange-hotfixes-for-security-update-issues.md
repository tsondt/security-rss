Title: Microsoft releases Exchange hotfixes for security update issues
Date: 2024-04-23T15:50:44-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-04-23-microsoft-releases-exchange-hotfixes-for-security-update-issues

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-releases-exchange-hotfixes-for-security-update-issues/){:target="_blank" rel="noopener"}

> ​Microsoft has released hotfix updates to address multiple known issues impacting Exchange servers after installing the March 2024 security updates. [...]
