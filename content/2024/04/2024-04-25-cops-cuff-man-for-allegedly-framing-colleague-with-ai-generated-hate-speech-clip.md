Title: Cops cuff man for allegedly framing colleague with AI-generated hate speech clip
Date: 2024-04-25T21:43:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-04-25-cops-cuff-man-for-allegedly-framing-colleague-with-ai-generated-hate-speech-clip

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/25/ai_voice_arrest/){:target="_blank" rel="noopener"}

> Athletics boss accused of deep-faking Baltimore school principal Baltimore police have arrested Dazhon Leslie Darien, the former athletic director of Pikesville High School (PHS), for allegedly impersonating the school's principal using AI software to make it seem as if he made racist and antisemitic remarks.... [...]
