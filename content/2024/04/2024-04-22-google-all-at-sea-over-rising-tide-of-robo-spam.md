Title: Google all at sea over rising tide of robo-spam
Date: 2024-04-22T08:30:15+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2024-04-22-google-all-at-sea-over-rising-tide-of-robo-spam

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/22/opinion_google_spam/){:target="_blank" rel="noopener"}

> What if it's not AI but the algorithm to blame? Opinion It was a bold claim by the richest and most famous tech founder: bold, precise and wrong. Laughably so. Twenty years ago, Bill Gates promised to rid the world of spam by 2006. How's that worked out for you?... [...]
