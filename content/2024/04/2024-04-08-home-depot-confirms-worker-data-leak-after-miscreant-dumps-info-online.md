Title: Home Depot confirms worker data leak after miscreant dumps info online
Date: 2024-04-08T18:01:48+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-08-home-depot-confirms-worker-data-leak-after-miscreant-dumps-info-online

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/08/home_depot_data_theft/){:target="_blank" rel="noopener"}

> SaaS slip up leads to scumbags seeking sinecure Home Depot has confirmed that a third-party company accidentally exposed some of its employees' personal details after a criminal copy-pasted the data online.... [...]
