Title: Chrome Enterprise Premium promises extra security – for a fee
Date: 2024-04-10T06:26:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-04-10-chrome-enterprise-premium-promises-extra-security-for-a-fee

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/10/chrome_enterprise_premium_security/){:target="_blank" rel="noopener"}

> Paying for browsers is no longer a memory from the 1990s Cloud Next Hoping to upsell freeloading corporate users of its Chrome browser, Google has announced Chrome Enterprise Premium – which comes with a dash of AI security sauce for just $6 per user per month.... [...]
