Title: Misconfigured cloud server leaked clues of North Korean animation scam
Date: 2024-04-23T05:26:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-04-23-misconfigured-cloud-server-leaked-clues-of-north-korean-animation-scam

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/23/north_korea_animators/){:target="_blank" rel="noopener"}

> Outsourcers outsourced work for the BBC, Amazon, and HBO Max to the hermit kingdom A misconfigured cloud server that used a North Korean IP address has led to the discovery that film production studios including the BBC, Amazon, and HBO Max could be inadvertently using workers from the hermit kingdom for animation projects.... [...]
