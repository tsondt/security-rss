Title: UnitedHealth: Change Healthcare cyberattack caused $872 million loss
Date: 2024-04-16T10:24:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-04-16-unitedhealth-change-healthcare-cyberattack-caused-872-million-loss

[Source](https://www.bleepingcomputer.com/news/security/unitedhealth-change-healthcare-cyberattack-caused-872-million-loss/){:target="_blank" rel="noopener"}

> UnitedHealth Group reported an $872 million impact on its Q1 earnings due to the ransomware attack disrupting the U.S. healthcare system since February. [...]
