Title: Russian FSB Counterintelligence Chief Gets 9 Years in Cybercrime Bribery Scheme
Date: 2024-04-22T20:07:56+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Web Fraud 2.0;Alexander Kovalev;Artem Zaitsev;Federal Security Service (FSB);Ferum Shop;Get-Net LLC;Grigory Tsaregorodtsev;Sky-Fraud;Trump's-Dumps
Slug: 2024-04-22-russian-fsb-counterintelligence-chief-gets-9-years-in-cybercrime-bribery-scheme

[Source](https://krebsonsecurity.com/2024/04/russian-fsb-counterintelligence-chief-gets-9-years-in-cybercrime-bribery-scheme/){:target="_blank" rel="noopener"}

> The head of counterintelligence for a division of the Russian Federal Security Service (FSB) was sentenced last week to nine years in a penal colony for accepting a USD $1.7 million bribe to ignore the activities of a prolific Russian cybercrime group that hacked thousands of e-commerce websites. The protection scheme was exposed in 2022 when Russian authorities arrested six members of the group, which sold millions of stolen payment cards at flashy online shops like Trump’s Dumps. A now-defunct carding shop that sold stolen credit cards and invoked 45’s likeness and name. As reported by The Record, a Russian [...]
