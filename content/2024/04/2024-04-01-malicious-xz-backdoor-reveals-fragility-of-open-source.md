Title: Malicious xz backdoor reveals fragility of open source
Date: 2024-04-01T21:16:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-04-01-malicious-xz-backdoor-reveals-fragility-of-open-source

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/01/xz_backdoor_open_source/){:target="_blank" rel="noopener"}

> This time, we got lucky. It mostly affected bleeding-edge distros. But that's not a defense strategy Analysis The discovery last week of a backdoor in a widely used open source compression library called xz could have been a security disaster had it not been caught by luck and atypical curiosity about latency from a Microsoft engineer.... [...]
