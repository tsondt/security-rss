Title: An MP who gives colleagues’ numbers to blackmailers. Isn’t William Wragg just right for this Westminster? | Marina Hyde
Date: 2024-04-05T12:21:03+00:00
Author: Marina Hyde
Category: The Guardian
Tags: Conservatives;Politics;UK news;Data and computer security
Slug: 2024-04-05-an-mp-who-gives-colleagues-numbers-to-blackmailers-isnt-william-wragg-just-right-for-this-westminster-marina-hyde

[Source](https://www.theguardian.com/commentisfree/2024/apr/05/mp-blackmail-william-wragg-tory-apology-rishi-sunak){:target="_blank" rel="noopener"}

> The ‘senior Tory’ has issued a self-flagellating apology, but we should see him as a child of this political age Where to start with Westminster’s latest scandal, which – without wishing to speculate on spoilers – I suggest you formally label as “developing”? Blowing his own cover in it is William Wragg, MP for Hazel Grove in Greater Manchester, and chair of the public administration and constitutional affairs committee. Aged 36, William is described as a “senior Tory” on the basis of something or other – possibly his predilection for calling for other politicians to resign on moral grounds. Mind [...]
