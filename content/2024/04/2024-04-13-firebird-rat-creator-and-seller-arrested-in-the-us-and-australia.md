Title: Firebird RAT creator and seller arrested in the U.S. and Australia
Date: 2024-04-13T10:17:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-04-13-firebird-rat-creator-and-seller-arrested-in-the-us-and-australia

[Source](https://www.bleepingcomputer.com/news/security/firebird-rat-creator-and-seller-arrested-in-the-us-and-australia/){:target="_blank" rel="noopener"}

> A joint police operation between the Australian Federal Police (AFP) and the FBI has led to the arrest and charging of two individuals who are believed to be behind the development and distribution of the "Firebird" remote access trojan (RAT), later rebranded as "Hive." [...]
