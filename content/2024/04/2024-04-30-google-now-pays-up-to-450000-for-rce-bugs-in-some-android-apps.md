Title: Google now pays up to $450,000 for RCE bugs in some Android apps
Date: 2024-04-30T14:33:51-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-30-google-now-pays-up-to-450000-for-rce-bugs-in-some-android-apps

[Source](https://www.bleepingcomputer.com/news/security/google-now-pays-up-to-450-000-for-rce-bugs-in-some-android-apps/){:target="_blank" rel="noopener"}

> Google has increased rewards for reporting remote code execution vulnerabilities within select Android apps by ten times, from $30,000 to $300,000, with the maximum reward reaching $450,000 for exceptional quality reports. [...]
