Title: Palo Alto Networks warns of PAN-OS firewall zero-day used in attacks
Date: 2024-04-12T09:28:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-12-palo-alto-networks-warns-of-pan-os-firewall-zero-day-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/palo-alto-networks-warns-of-pan-os-firewall-zero-day-used-in-attacks/){:target="_blank" rel="noopener"}

> Today, Palo Alto Networks warns that an unpatched critical command injection vulnerability in its PAN-OS firewall is being actively exploited in attacks. [...]
