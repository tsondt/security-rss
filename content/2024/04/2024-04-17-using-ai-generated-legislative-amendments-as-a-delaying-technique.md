Title: Using AI-Generated Legislative Amendments as a Delaying Technique
Date: 2024-04-17T11:08:32+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;A Hacker's Mind;artificial intelligence;laws;LLM;noncomputer hacks
Slug: 2024-04-17-using-ai-generated-legislative-amendments-as-a-delaying-technique

[Source](https://www.schneier.com/blog/archives/2024/04/using-ai-generated-legislative-amendments-as-a-delaying-technique.html){:target="_blank" rel="noopener"}

> Canadian legislators proposed 19,600 amendments —almost certainly AI-generated—to a bill in an attempt to delay its adoption. I wrote about many different legislative delaying tactics in A Hacker’s Mind, but this is a new one. [...]
