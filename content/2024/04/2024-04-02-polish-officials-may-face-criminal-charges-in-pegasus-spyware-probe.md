Title: Polish officials may face criminal charges in Pegasus spyware probe
Date: 2024-04-02T12:00:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-02-polish-officials-may-face-criminal-charges-in-pegasus-spyware-probe

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/02/polish_pegasus_inquiry/){:target="_blank" rel="noopener"}

> Victims of the powerful surveillance tool will soon find out the truth Former Polish government officials may face criminal charges following an investigation into their use of the notorious spyware Pegasus to surveil political opponents and others.... [...]
