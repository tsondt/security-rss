Title: X fixes URL blunder that could enable convincing social media phishing campaigns
Date: 2024-04-10T10:37:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-10-x-fixes-url-blunder-that-could-enable-convincing-social-media-phishing-campaigns

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/10/x_fixes_url_blunder/){:target="_blank" rel="noopener"}

> Poorly implemented rule allowed miscreants to deceive users with trusted URLs Elon Musk's X has apparently fixed an embarrassing issue implemented earlier in the week that royally bungled URLs on the social media platform formerly known as Twitter.... [...]
