Title: Critical Forminator plugin flaw impacts over 300k WordPress sites
Date: 2024-04-20T11:19:38-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-20-critical-forminator-plugin-flaw-impacts-over-300k-wordpress-sites

[Source](https://www.bleepingcomputer.com/news/security/critical-forminator-plugin-flaw-impacts-over-300k-wordpress-sites/){:target="_blank" rel="noopener"}

> The Forminator WordPress plugin used in over 500,000 sites is vulnerable to a flaw that allows malicious actors to perform unrestricted file uploads to the server. [...]
