Title: Framework’s software and firmware have been a mess, but it’s working on them
Date: 2024-04-15T11:00:44+00:00
Author: Andrew Cunningham
Category: Ars Technica
Tags: Biz & IT;Features;Tech;AMD;BIOS;framework laptop;framework laptop 13;framework laptop 16;Intel
Slug: 2024-04-15-frameworks-software-and-firmware-have-been-a-mess-but-its-working-on-them

[Source](https://arstechnica.com/?p=2012352){:target="_blank" rel="noopener"}

> Enlarge / The Framework Laptop 13. (credit: Andrew Cunningham) Since Framework showed off its first prototypes in February 2021, we've generally been fans of the company's modular, repairable, upgradeable laptops. Not that the company's hardware releases to date have been perfect—each Framework Laptop 13 model has had quirks and flaws that range from minor to quite significant, and the Laptop 16's upsides struggle to balance its downsides. But the hardware mostly does a good job of functioning as a regular laptop while being much more tinkerer-friendly than your typical MacBook, XPS, or ThinkPad. But even as it builds new upgrades [...]
