Title: How to generate security findings to help your security team with incident response simulations
Date: 2024-04-01T16:00:03+00:00
Author: Jonathan Nguyen
Category: AWS Security
Tags: Amazon GuardDuty;Amazon Inspector;AWS Security Hub;Intermediate (200);Security, Identity, & Compliance;Technical How-to;GuardDuty;Incident response;Security;Security Blog;Security Hub;SIEM
Slug: 2024-04-01-how-to-generate-security-findings-to-help-your-security-team-with-incident-response-simulations

[Source](https://aws.amazon.com/blogs/security/how-to-generate-security-findings-to-help-your-security-team-with-incident-response-simulations/){:target="_blank" rel="noopener"}

> Continually reviewing your organization’s incident response capabilities can be challenging without a mechanism to create security findings with actual Amazon Web Services (AWS) resources within your AWS estate. As prescribed within the AWS Security Incident Response whitepaper, it’s important to periodically review your incident response capabilities to make sure your security team is continually maturing [...]
