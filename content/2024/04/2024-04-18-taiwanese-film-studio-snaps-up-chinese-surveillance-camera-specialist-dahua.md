Title: Taiwanese film studio snaps up Chinese surveillance camera specialist Dahua
Date: 2024-04-18T03:30:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-04-18-taiwanese-film-studio-snaps-up-chinese-surveillance-camera-specialist-dahua

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/18/taiwanese_company_buys_dahua/){:target="_blank" rel="noopener"}

> Stymied by sanctions, it had to go... but where? Chinese surveillance camera manufacturer Zhejiang Dahua Technology, which has found itself on the USA’s entity list of banned orgs, has fully sold off its stateside subsidiary for $15 million to Taiwan's Central Motion Picture Corporation, according to the firm's annual report released on Monday.... [...]
