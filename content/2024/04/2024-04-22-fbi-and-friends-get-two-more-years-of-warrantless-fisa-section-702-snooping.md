Title: FBI and friends get two more years of warrantless FISA Section 702 snooping
Date: 2024-04-22T21:09:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-22-fbi-and-friends-get-two-more-years-of-warrantless-fisa-section-702-snooping

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/22/fisa_section_702_renewed/){:target="_blank" rel="noopener"}

> Senate kills reform amendments, Biden swiftly signs bill into law US lawmakers on Saturday reauthorized a contentious warrantless surveillance tool for another two years — and added a whole bunch of people and organizations to the list of those who can be compelled to spy for Uncle Sam.... [...]
