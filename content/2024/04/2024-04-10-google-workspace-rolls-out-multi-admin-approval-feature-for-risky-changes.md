Title: Google Workspace rolls out multi-admin approval feature for risky changes
Date: 2024-04-10T15:13:41-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2024-04-10-google-workspace-rolls-out-multi-admin-approval-feature-for-risky-changes

[Source](https://www.bleepingcomputer.com/news/security/google-workspace-rolls-out-multi-admin-approval-feature-for-risky-changes/){:target="_blank" rel="noopener"}

> Google is rolling out a new Workspace feature that requires multiple admins to approve high-risk setting changes to prevent unauthorized or accidental modifications that could reduce security. [...]
