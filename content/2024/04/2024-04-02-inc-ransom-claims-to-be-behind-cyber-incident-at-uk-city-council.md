Title: INC Ransom claims to be behind 'cyber incident' at UK city council
Date: 2024-04-02T11:15:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-02-inc-ransom-claims-to-be-behind-cyber-incident-at-uk-city-council

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/02/inc_ransom_leicester_council/){:target="_blank" rel="noopener"}

> This follows attack on NHS services in Scotland last week The cyber skids at INC Ransom are claiming responsbility for the ongoing cybersecurity incident at Leicester City Council, according to a post caught by eagle-eyed infosec watchers.... [...]
