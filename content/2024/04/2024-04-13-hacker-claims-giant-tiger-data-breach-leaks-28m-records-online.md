Title: Hacker claims Giant Tiger data breach, leaks 2.8M records online
Date: 2024-04-13T10:00:16-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2024-04-13-hacker-claims-giant-tiger-data-breach-leaks-28m-records-online

[Source](https://www.bleepingcomputer.com/news/security/hacker-claims-giant-tiger-data-breach-leaks-28m-records-online/){:target="_blank" rel="noopener"}

> Canadian retail chain Giant Tiger disclosed a data breach in March 2024. A threat actor has now publicly claimed responsibility for the data breach and leaked 2.8 million records on a hacker forum that they claim are of Giant Tiger customers. [...]
