Title: Google rejected 2.28 million risky Android apps from Play store in 2023
Date: 2024-04-29T12:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2024-04-29-google-rejected-228-million-risky-android-apps-from-play-store-in-2023

[Source](https://www.bleepingcomputer.com/news/security/google-rejected-228-million-risky-android-apps-from-play-store-in-2023/){:target="_blank" rel="noopener"}

> Google blocked 2.28 million Android apps from being published on Google Play after finding various policy violations that could threaten user's security. [...]
