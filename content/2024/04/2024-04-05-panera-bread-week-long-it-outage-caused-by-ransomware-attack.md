Title: Panera Bread week-long IT outage caused by ransomware attack
Date: 2024-04-05T09:52:22-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-04-05-panera-bread-week-long-it-outage-caused-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/panera-bread-week-long-it-outage-caused-by-ransomware-attack/){:target="_blank" rel="noopener"}

> Panera Bread's recent week-long outage was caused by a ransomware attack, according to people familiar with the matter and emails seen by BleepingComputer. [...]
