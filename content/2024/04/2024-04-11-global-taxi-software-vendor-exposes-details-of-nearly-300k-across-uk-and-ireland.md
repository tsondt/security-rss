Title: Global taxi software vendor exposes details of nearly 300K across UK and Ireland
Date: 2024-04-11T09:30:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-11-global-taxi-software-vendor-exposes-details-of-nearly-300k-across-uk-and-ireland

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/11/icabbi_database_exposure/){:target="_blank" rel="noopener"}

> High-profile individuals including MPs said to be caught up in leak Exclusive Taxi software biz iCabbi recently fixed an issue that exposed the personal information of nearly 300,000 individuals via an unprotected database.... [...]
