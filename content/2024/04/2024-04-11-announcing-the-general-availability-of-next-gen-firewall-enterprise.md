Title: Announcing the general availability of Next Gen Firewall Enterprise
Date: 2024-04-11T16:00:00+00:00
Author: Muninder Sambi
Category: GCP Security
Tags: Security & Identity;Google Cloud Next;Networking
Slug: 2024-04-11-announcing-the-general-availability-of-next-gen-firewall-enterprise

[Source](https://cloud.google.com/blog/products/identity-security/announcing-next-gen-firewall-enterprise-now-in-ga-next24/){:target="_blank" rel="noopener"}

> In today's ever-evolving threat landscape, organizations require robust network security solutions to protect their critical assets in the cloud. Google Cloud is committed to providing superior cloud-first security controls, and today at Google Cloud Next, we're thrilled to announce the general availability of Google Cloud NGFW Enterprise, our next-generation cloud firewall offering. Cloud NGFW Enterprise (formerly Cloud Firewall Plus) is an evolution of our fully-distributed cloud-first firewall service that delivers comprehensive Zero Trust network protection for your Google Cloud workloads. It can provide advanced Intrusion Prevention Service (IPS) capabilities, powered by Palo Alto Networks technology, to identify and block malicious [...]
