Title: Flaws in Chinese keyboard apps leave 750 million users open to snooping, researchers claim
Date: 2024-04-26T05:33:17+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-04-26-flaws-in-chinese-keyboard-apps-leave-750-million-users-open-to-snooping-researchers-claim

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/26/pinyin_keyboard_security_risks/){:target="_blank" rel="noopener"}

> Huawei is OK, but Xiaomi, OPPO, and Samsung are in strife. And Honor isn't living its name Many Chinese keyboard apps, some from major handset manufacturers, can leak keystrokes to determined snoopers, leaving perhaps three quarters of a billion people at risk according to research from the University of Toronto’s Citizen Lab.... [...]
