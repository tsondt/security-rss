Title: Microsoft still unsure how hackers stole MSA key in 2023 Exchange attack
Date: 2024-04-03T20:21:10-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-04-03-microsoft-still-unsure-how-hackers-stole-msa-key-in-2023-exchange-attack

[Source](https://www.bleepingcomputer.com/news/security/microsoft-still-unsure-how-hackers-stole-msa-key-in-2023-exchange-attack/){:target="_blank" rel="noopener"}

> The U.S. Department of Homeland Security's Cyber Safety Review Board (CSRB) has released a scathing report on how Microsoft handled its 2023 Exchange Online attack, warning that the company needs to do better at securing data and be more truthful about how threat actors stole an Azure signing key. [...]
