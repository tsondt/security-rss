Title: Second time lucky for Thoma Bravo, which scoops up Darktrace for $5.3B
Date: 2024-04-26T16:00:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-26-second-time-lucky-for-thoma-bravo-which-scoops-up-darktrace-for-53b

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/26/thoma_bravo_darktrace/){:target="_blank" rel="noopener"}

> Analysts brand deal a 'nail in the coffin' for UK tech investment Private equity investor Thoma Bravo has successfully completed a second acquisition attempt of UK-based cybersecurity company Darktrace in a $5.3 billion deal.... [...]
