Title: Millions of IPs remain infected by USB worm years after its creators left it for dead
Date: 2024-04-25T18:49:07+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;infection;plugx;sinkhole;USB;worm
Slug: 2024-04-25-millions-of-ips-remain-infected-by-usb-worm-years-after-its-creators-left-it-for-dead

[Source](https://arstechnica.com/?p=2020055){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) A now-abandoned USB worm that backdoors connected devices has continued to self-replicate for years since its creators lost control of it and remains active on thousands, possibly millions, of machines, researchers said Thursday. The worm—which first came to light in a 2023 post published by security firm Sophos—became active in 2019 when a variant of malware known as PlugX added functionality that allowed it to infect USB drives automatically. In turn, those drives would infect any new machine they connected to, a capability that allowed the malware to spread without requiring any end-user interaction. Researchers who [...]
