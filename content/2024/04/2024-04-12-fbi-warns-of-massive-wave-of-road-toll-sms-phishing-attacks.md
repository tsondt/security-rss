Title: FBI warns of massive wave of road toll SMS phishing attacks
Date: 2024-04-12T14:56:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-12-fbi-warns-of-massive-wave-of-road-toll-sms-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-massive-wave-of-road-toll-sms-phishing-attacks/){:target="_blank" rel="noopener"}

> On Friday, the Federal Bureau of Investigation warned of a massive ongoing wave of SMS phishing attacks targeting Americans with lures regarding unpaid road toll fees. [...]
