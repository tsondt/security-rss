Title: Google cools on cookie phase-out while regulators chew on plans
Date: 2024-04-24T14:31:28+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-04-24-google-cools-on-cookie-phase-out-while-regulators-chew-on-plans

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/24/google_delays_cookie_cull/){:target="_blank" rel="noopener"}

> Privacy Sandbox slips into 2025 after challenges from UK authorities Google's plan to phase out third-party cookies in Chrome is being postponed to 2025 amid wrangling with the UK's Competition and Markets Authority (CMA) and Information Commissioner's Office (ICO).... [...]
