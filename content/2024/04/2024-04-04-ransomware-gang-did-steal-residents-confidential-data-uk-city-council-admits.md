Title: Ransomware gang <em>did</em> steal residents' confidential data, UK city council admits
Date: 2024-04-04T10:49:40+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-04-ransomware-gang-did-steal-residents-confidential-data-uk-city-council-admits

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/04/ransomware_gang_did_in_fact/){:target="_blank" rel="noopener"}

> INC Ransom emerges as a growing threat as some ex-LockBit/ALPHV affiliates get new gigs Leicester City Council is finally admitting its "cyber incident" was carried out by a ransomware gang and that data was stolen, hours after the criminals forced its hand.... [...]
