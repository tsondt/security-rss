Title: Management company settles for $18.4M after nuclear weapons plant staff fudged their timesheets
Date: 2024-04-24T15:00:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-24-management-company-settles-for-184m-after-nuclear-weapons-plant-staff-fudged-their-timesheets

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/24/management_company_settles_for_184m/){:target="_blank" rel="noopener"}

> The firm 'fessed up to staff misconduct and avoided criminal liability A company contracted to manage an Amarillo, Texas nuclear weapons facility has to pay US government $18.4 million in a settlement over allegations that its atomic technicians fudged their timesheets to collect more money from Uncle Sam.... [...]
