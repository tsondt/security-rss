Title: European Commission starts formal probe of Meta over election misinformation
Date: 2024-04-30T12:30:11+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2024-04-30-european-commission-starts-formal-probe-of-meta-over-election-misinformation

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/30/european_commission_launches_proceedings_meta_misinformation/){:target="_blank" rel="noopener"}

> Europe takes action after Facebook parent withdraws monitoring tool The European Commission has launched formal proceedings against Meta, alleging failure to properly monitor distribution by "foreign actors" of political misinformation before June's European elections.... [...]
