Title: Surveillance by the New Microsoft Outlook App
Date: 2024-04-04T11:07:39+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;data collection;geolocation;Microsoft;privacy;surveillance
Slug: 2024-04-04-surveillance-by-the-new-microsoft-outlook-app

[Source](https://www.schneier.com/blog/archives/2024/04/surveillance-by-the-new-microsoft-outlook-app.html){:target="_blank" rel="noopener"}

> The ProtonMail people are accusing Microsoft’s new Outlook for Windows app of conducting extensive surveillance on its users. It shares data with advertisers, a lot of data: The window informs users that Microsoft and those 801 third parties use their data for a number of purposes, including to: Store and/or access information on the user’s device Develop and improve products Personalize ads and content Measure ads and content Derive audience insights Obtain precise geolocation data Identify users through device scanning Commentary. [...]
