Title: Over 92,000 exposed D-Link NAS devices have a backdoor account
Date: 2024-04-06T10:16:32-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-06-over-92000-exposed-d-link-nas-devices-have-a-backdoor-account

[Source](https://www.bleepingcomputer.com/news/security/over-92-000-exposed-d-link-nas-devices-have-a-backdoor-account/){:target="_blank" rel="noopener"}

> A threat researcher has disclosed a new arbitrary command injection and hardcoded backdoor flaw in multiple end-of-life D-Link Network Attached Storage (NAS) device models. [...]
