Title: Academics probe Apple's privacy settings and get lost and confused
Date: 2024-04-05T05:34:12+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-04-05-academics-probe-apples-privacy-settings-and-get-lost-and-confused

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/05/apple_apps_privacy_study/){:target="_blank" rel="noopener"}

> Just disabling Siri requires visits to five submenus A study has concluded that Apple's privacy practices aren't particularly effective, because default apps on the iPhone and Mac have limited privacy settings and confusing configuration options.... [...]
