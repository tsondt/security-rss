Title: CISA in a flap as Chirp smart door locks can be trivially unlocked remotely
Date: 2024-04-15T22:35:33+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-04-15-cisa-in-a-flap-as-chirp-smart-door-locks-can-be-trivially-unlocked-remotely

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/15/critical_vulnerability_chirp_lock/){:target="_blank" rel="noopener"}

> Hard-coded credentials last thing you want in home security app Some smart locks controlled by Chirp Systems' software can be remotely unlocked by strangers thanks to a critical security vulnerability.... [...]
