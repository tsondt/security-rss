Title: Maybe the Phone System Surveillance Vulnerabilities Will Be Fixed
Date: 2024-04-05T11:00:42+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;infrastructure;phones;protocols;vulnerabilities
Slug: 2024-04-05-maybe-the-phone-system-surveillance-vulnerabilities-will-be-fixed

[Source](https://www.schneier.com/blog/archives/2024/04/maybe-the-phone-system-surveillance-vulnerabilities-will-be-fixed.html){:target="_blank" rel="noopener"}

> It seems that the FCC might be fixing the vulnerabilities in SS7 and the Diameter protocol: On March 27 the commission asked telecommunications providers to weigh in and detail what they are doing to prevent SS7 and Diameter vulnerabilities from being misused to track consumers’ locations. The FCC has also asked carriers to detail any exploits of the protocols since 2018. The regulator wants to know the date(s) of the incident(s), what happened, which vulnerabilities were exploited and with which techniques, where the location tracking occurred, and ­ if known ­ the attacker’s identity. This time frame is significant because [...]
