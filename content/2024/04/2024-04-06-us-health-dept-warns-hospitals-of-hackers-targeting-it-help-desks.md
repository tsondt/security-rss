Title: US Health Dept warns hospitals of hackers targeting IT help desks
Date: 2024-04-06T11:09:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-04-06-us-health-dept-warns-hospitals-of-hackers-targeting-it-help-desks

[Source](https://www.bleepingcomputer.com/news/security/us-health-dept-warns-hospitals-of-hackers-targeting-it-help-desks/){:target="_blank" rel="noopener"}

> The U.S. Department of Health and Human Services (HHS) warns that hackers are now using social engineering tactics to target IT help desks across the Healthcare and Public Health (HPH) sector. [...]
