Title: Europol now latest cops to beg Big Tech to ditch E2EE
Date: 2024-04-22T16:30:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-04-22-europol-now-latest-cops-to-beg-big-tech-to-ditch-e2ee

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/22/europol_becomes_latest_cop_shop/){:target="_blank" rel="noopener"}

> Don't bore us, get to the chorus: You need less privacy so we can protect the children Yet another international cop shop has come out swinging against end-to-end encryption - this time it's Europol which is urging an end to implementation of the tech for fear police investigations will be hampered by protected DMs.... [...]
