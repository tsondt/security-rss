Title: Powering Google Cloud with Gemini
Date: 2024-04-09T12:00:00+00:00
Author: Brad Calder
Category: GCP Security
Tags: Application Development;Databases;API Management;Security & Identity;AI & Machine Learning
Slug: 2024-04-09-powering-google-cloud-with-gemini

[Source](https://cloud.google.com/blog/products/ai-machine-learning/gemini-for-google-cloud-is-here/){:target="_blank" rel="noopener"}

> We are excited to share with you that Gemini for Google Cloud is here. Generative AI’s capabilities have grown tremendously over the last year, and we have infused it throughout our product portfolio. Gemini for Google Cloud is a new generation of AI assistants for developers, Google Cloud services, and applications. These assist users in working and coding more effectively, gaining deeper data insights, navigating security challenges, and more. Using Google's Gemini family of models, Gemini for Google Cloud lets teams accomplish more in the cloud. Gemini for Google Cloud offers enterprise-ready, AI capabilities that deliver robust security, compliance, and [...]
