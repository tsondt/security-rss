Title: AT&amp;T, Verizon, Sprint, T-Mobile US fined $200M for selling off people's location info
Date: 2024-04-29T23:20:15+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-04-29-att-verizon-sprint-t-mobile-us-fined-200m-for-selling-off-peoples-location-info

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/29/fcc_telecom_fines/){:target="_blank" rel="noopener"}

> Carriers claim real culprits are getting away with it - the data brokers The FCC on Monday fined four major US telcos almost $200 million for "illegally" selling subscribers' location information to data brokers.... [...]
