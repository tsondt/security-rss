Title: Critical Rust flaw enables Windows command injection attacks
Date: 2024-04-09T16:20:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-09-critical-rust-flaw-enables-windows-command-injection-attacks

[Source](https://www.bleepingcomputer.com/news/security/critical-rust-flaw-enables-windows-command-injection-attacks/){:target="_blank" rel="noopener"}

> Threat actors can exploit a security vulnerability in the Rust standard library to target Windows systems in command injection attacks. [...]
