Title: Microsoft breach allowed Russian spies to steal emails from US government
Date: 2024-04-12T14:37:12+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2024-04-12-microsoft-breach-allowed-russian-spies-to-steal-emails-from-us-government

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/12/microsoft_cisa_order/){:target="_blank" rel="noopener"}

> Affected federal agencies must comb through mails, reset API keys and passwords The US Cybersecurity and Infrastructure Security Agency (CISA) warns that Russian spies who gained access to Microsoft's email system were able to steal sensitive data, including authentication details and that immediate remedial action is required by affected agencies.... [...]
