Title: Cisco creates architecture to improve security and sell you new switches
Date: 2024-04-18T07:01:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-04-18-cisco-creates-architecture-to-improve-security-and-sell-you-new-switches

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/18/cisco_hypershield/){:target="_blank" rel="noopener"}

> Hypershield detects bad behavior and automagically reconfigures networks to snuff out threats Cisco has developed a product called Hypershield that it thinks represents a new way to do network security.... [...]
