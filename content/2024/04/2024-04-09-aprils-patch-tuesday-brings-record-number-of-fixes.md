Title: April’s Patch Tuesday Brings Record Number of Fixes
Date: 2024-04-09T20:28:17+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;.NET Framework;Adobe After Effects;Azure;Azure AI;Ben McCarthy;Bitlocker;Bridge;Commerce;CVE-2023-24932;CVE-2024-20670;CVE-2024-21412;CVE-2024-26234;CVE-2024-29063;CVE-2024-29988;DNS Server;Experience Manager;Illustrator;Immersive Labs;InDesign;Media Encoder;Office;photoshop;Satnam Narang;SQL Server;Trend Micro's Zero Day Initiative;Visual Studio;Windows Defender;Windows Secure Boot
Slug: 2024-04-09-aprils-patch-tuesday-brings-record-number-of-fixes

[Source](https://krebsonsecurity.com/2024/04/aprils-patch-tuesday-brings-record-number-of-fixes/){:target="_blank" rel="noopener"}

> If only Patch Tuesdays came around infrequently — like total solar eclipse rare — instead of just creeping up on us each month like The Man in the Moon. Although to be fair, it would be tough for Microsoft to eclipse the number of vulnerabilities fixed in this month’s patch batch — a record 147 flaws in Windows and related software. Yes, you read that right. Microsoft today released updates to address 147 security holes in Windows, Office, Azure,.NET Framework, Visual Studio, SQL Server, DNS Server, Windows Defender, Bitlocker, and Windows Secure Boot. “This is the largest release from Microsoft [...]
