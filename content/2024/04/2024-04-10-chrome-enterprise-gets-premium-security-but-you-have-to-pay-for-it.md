Title: Chrome Enterprise gets Premium security but you have to pay for it
Date: 2024-04-10T15:52:30-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-04-10-chrome-enterprise-gets-premium-security-but-you-have-to-pay-for-it

[Source](https://www.bleepingcomputer.com/news/security/chrome-enterprise-gets-premium-security-but-you-have-to-pay-for-it/){:target="_blank" rel="noopener"}

> Google has announced a new version of its browser for organizations, Chrome Enterprise Premium, which comes with extended security controls for a monthly fee per user. [...]
