Title: French issue <em>alerte rouge</em> after local governments knocked offline by cyber attack
Date: 2024-04-12T05:30:05+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-12-french-issue-alerte-rouge-after-local-governments-knocked-offline-by-cyber-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/12/french_municipalities_cyberattack/){:target="_blank" rel="noopener"}

> Embarrassing, as its officials are in the US to discuss Olympics cyber threats Several French municipal governments' services have been knocked offline following a "large-scale cyber attack" on their shared servers.... [...]
