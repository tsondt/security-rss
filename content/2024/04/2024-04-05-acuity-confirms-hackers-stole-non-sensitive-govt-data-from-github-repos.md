Title: Acuity confirms hackers stole non-sensitive govt data from GitHub repos
Date: 2024-04-05T11:32:46-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-05-acuity-confirms-hackers-stole-non-sensitive-govt-data-from-github-repos

[Source](https://www.bleepingcomputer.com/news/security/acuity-confirms-hackers-stole-non-sensitive-govt-data-from-github-repos/){:target="_blank" rel="noopener"}

> Acuity, a federal contractor that works with U.S. government agencies, has confirmed that hackers breached its GitHub repositories and stole documents containing old and non-sensitive data. [...]
