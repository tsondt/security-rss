Title: Microsoft warns deepfake election subversion is disturbingly easy
Date: 2024-04-02T15:00:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-02-microsoft-warns-deepfake-election-subversion-is-disturbingly-easy

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/02/microsoft_election_ai_fakes/){:target="_blank" rel="noopener"}

> Simple stuff like slapping on a logo fools more folks and travels further As hundreds of millions of voters around the globe prepare to elect their leaders this year, there's no question that trolls will try to sway the outcomes using AI, according to Clint Watts, general manager of Microsoft's Threat Analysis Center.... [...]
