Title: Sacramento airport goes no-fly after AT&amp;T internet cable snipped
Date: 2024-04-19T20:30:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-04-19-sacramento-airport-goes-no-fly-after-att-internet-cable-snipped

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/19/sacramento_airport_outage/){:target="_blank" rel="noopener"}

> Police say this appears to be a 'deliberate act.' Sacramento International Airport (SMF) suffered hours of flight delays yesterday after what appears to be an intentional cutting of an AT&T internet cable serving the facility.... [...]
