Title: Account compromise of “unprecedented scale” uses everyday home devices
Date: 2024-04-29T19:35:32+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;credential stuffing;okta
Slug: 2024-04-29-account-compromise-of-unprecedented-scale-uses-everyday-home-devices

[Source](https://arstechnica.com/?p=2020513){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Authentication service Okta is warning about the “unprecedented scale” of an ongoing campaign that routes fraudulent login requests through the mobile devices and browsers of everyday users in an attempt to conceal the malicious behavior. The attack, Okta said, uses other means to camouflage the login attempts as well, including the TOR network and so-called proxy services from providers such as NSOCKS, Luminati, and DataImpulse, which can also harness users’ devices without their knowledge. In some cases, the affected mobile devices are running malicious apps. In other cases, users have enrolled their devices in proxy services [...]
