Title: T-Mobile, Verizon workers get texts offering $300 for SIM swaps
Date: 2024-04-16T19:01:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-16-t-mobile-verizon-workers-get-texts-offering-300-for-sim-swaps

[Source](https://www.bleepingcomputer.com/news/security/t-mobile-verizon-workers-get-texts-offering-300-for-sim-swaps/){:target="_blank" rel="noopener"}

> Criminals are now texting T-Mobile and Verizon employees on their personal and work phones, trying to tempt them with cash to perform SIM swaps. [...]
