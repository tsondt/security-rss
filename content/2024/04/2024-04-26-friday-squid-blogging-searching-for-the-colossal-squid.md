Title: Friday Squid Blogging: Searching for the Colossal Squid
Date: 2024-04-26T21:07:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-04-26-friday-squid-blogging-searching-for-the-colossal-squid

[Source](https://www.schneier.com/blog/archives/2024/04/friday-squid-blogging-searching-for-the-colossal-squid.html){:target="_blank" rel="noopener"}

> A cruise ship is searching for the colossal squid. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
