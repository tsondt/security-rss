Title: How to automate up to 90% of IT offboarding tasks
Date: 2024-04-11T10:02:04-04:00
Author: Sponsored by Nudge Security
Category: BleepingComputer
Tags: Security
Slug: 2024-04-11-how-to-automate-up-to-90-of-it-offboarding-tasks

[Source](https://www.bleepingcomputer.com/news/security/how-to-automate-up-to-90-percent-of-it-offboarding-tasks/){:target="_blank" rel="noopener"}

> Employee offboarding isn't anybody's favorite task—but it's a critical IT process that needs to be executed diligently and efficiently. Learn more from Nudge Security on automating offboarding of users in a secure manner. [...]
