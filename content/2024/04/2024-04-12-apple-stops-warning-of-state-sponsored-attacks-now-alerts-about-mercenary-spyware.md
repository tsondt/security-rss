Title: Apple stops warning of 'state-sponsored' attacks, now alerts about 'mercenary spyware'
Date: 2024-04-12T04:46:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-04-12-apple-stops-warning-of-state-sponsored-attacks-now-alerts-about-mercenary-spyware

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/12/apple_mercenary_spyware/){:target="_blank" rel="noopener"}

> Report claims India's government, which is accused of using Pegasus at home, was displeased Apple has made a significant change to the wording of its threat notifications, opting not to attribute attacks to a specific source or perpetrator, but categorizing them broadly as "mercenary spyware."... [...]
