Title: 6 Prompts You Don't Want Employees Putting in Microsoft Copilot
Date: 2024-04-03T10:02:04-04:00
Author: Sponsored by Varonis
Category: BleepingComputer
Tags: Security
Slug: 2024-04-03-6-prompts-you-dont-want-employees-putting-in-microsoft-copilot

[Source](https://www.bleepingcomputer.com/news/security/6-prompts-you-dont-want-employees-putting-in-microsoft-copilot/){:target="_blank" rel="noopener"}

> Microsoft Copilot is a powerful asset for companies, but with it comes an increased risk of data exposure. In this article, Varonis demonstrates prompt-hacking examples that can expose sensitive data. [...]
