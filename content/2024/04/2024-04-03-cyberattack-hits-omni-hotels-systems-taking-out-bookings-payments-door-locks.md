Title: Cyberattack hits Omni Hotels systems, taking out bookings, payments, door locks
Date: 2024-04-03T19:28:15+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-03-cyberattack-hits-omni-hotels-systems-taking-out-bookings-payments-door-locks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/03/omni_hotels_it_outage/){:target="_blank" rel="noopener"}

> As WhatsApp, Facebook Messenger, other Meta bits plus Apple stuff fall offline today Updated Omni Hotels & Resorts' computer systems have been offline since Friday due to what the American luxury hospitality chain called a "disruption."... [...]
