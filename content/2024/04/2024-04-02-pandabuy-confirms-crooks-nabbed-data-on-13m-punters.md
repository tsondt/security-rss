Title: Pandabuy confirms crooks nabbed data on 1.3M punters
Date: 2024-04-02T16:30:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-02-pandabuy-confirms-crooks-nabbed-data-on-13m-punters

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/02/pandabuy_admits_to_data_breach/){:target="_blank" rel="noopener"}

> Nothing says 'sorry' like 10 percent off shipping for a month Ecommerce platform Pandabuy has apologized after two cybercriminals were spotted hawking personal data belonging to 1.3 million of its customers.... [...]
