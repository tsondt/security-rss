Title: Watchdog reveals lingering Google Privacy Sandbox worries
Date: 2024-04-29T10:15:14+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-04-29-watchdog-reveals-lingering-google-privacy-sandbox-worries

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/29/uk_cma_google/){:target="_blank" rel="noopener"}

> Ad tech rewrite to replace web cookies still not to regulatory taste The UK Competition and Markets Authority (CMA) still has privacy and competition concerns about Google's Privacy Sandbox advertising toolkit, which explains why the ad giant recently again delayed its plan to drop third-party cookies in Chrome until 2025.... [...]
