Title: Change Healthcare hacked using stolen Citrix account with no MFA
Date: 2024-04-30T10:13:30-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-04-30-change-healthcare-hacked-using-stolen-citrix-account-with-no-mfa

[Source](https://www.bleepingcomputer.com/news/security/change-healthcare-hacked-using-stolen-citrix-account-with-no-mfa/){:target="_blank" rel="noopener"}

> UnitedHealth confirms that Change Healthcare's network was breached by the BlackCat ransomware gang, who used stolen credentials to log into the company's Citrix remote access service, which did not have multi-factor authentication enabled. [...]
