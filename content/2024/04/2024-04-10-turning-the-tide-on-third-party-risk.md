Title: Turning the tide on third-party risk
Date: 2024-04-10T08:39:13+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2024-04-10-turning-the-tide-on-third-party-risk

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/10/turning_the_tide_on_thirdparty/){:target="_blank" rel="noopener"}

> Using threat intelligence to mitigate against security breaches Webinar There are some unhappy projections out there about the prevalence of third-party security breaches.... [...]
