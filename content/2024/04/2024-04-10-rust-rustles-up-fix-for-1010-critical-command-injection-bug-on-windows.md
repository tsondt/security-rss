Title: Rust rustles up fix for 10/10 critical command injection bug on Windows
Date: 2024-04-10T13:15:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-10-rust-rustles-up-fix-for-1010-critical-command-injection-bug-on-windows

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/10/rust_critical_vulnerability_windows/){:target="_blank" rel="noopener"}

> BatBadBut hits Erlang, Go, Python, Ruby as well Programmers are being urged to update their Rust versions after the security experts working on the language addressed a critical vulnerability that could lead to malicious command injections on Windows machines.... [...]
