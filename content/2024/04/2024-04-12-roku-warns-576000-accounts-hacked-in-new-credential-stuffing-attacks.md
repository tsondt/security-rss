Title: Roku warns 576,000 accounts hacked in new credential stuffing attacks
Date: 2024-04-12T11:05:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-12-roku-warns-576000-accounts-hacked-in-new-credential-stuffing-attacks

[Source](https://www.bleepingcomputer.com/news/security/roku-warns-576-000-accounts-hacked-in-new-credential-stuffing-attacks/){:target="_blank" rel="noopener"}

> Roku warns that 576,000 accounts were hacked in new credential stuffing attacks after disclosing another incident that compromised 15,000 accounts in early March. [...]
