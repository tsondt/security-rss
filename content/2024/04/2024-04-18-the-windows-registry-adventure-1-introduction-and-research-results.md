Title: The Windows Registry Adventure #1: Introduction and research results
Date: 2024-04-18T09:45:00-07:00
Author: Google Project Zero (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2024-04-18-the-windows-registry-adventure-1-introduction-and-research-results

[Source](https://googleprojectzero.blogspot.com/2024/04/the-windows-registry-adventure-1.html){:target="_blank" rel="noopener"}

> Posted by Mateusz Jurczyk, Google Project Zero In the 20-month period between May 2022 and December 2023, I thoroughly audited the Windows Registry in search of local privilege escalation bugs. It all started unexpectedly: I was in the process of developing a coverage-based Windows kernel fuzzer based on the Bochs x86 emulator (one of my favorite tools for security research: see Bochspwn, Bochspwn Reloaded, and my earlier font fuzzing infrastructure ), and needed some binary formats to test it on. My first pick were PE files: they are very popular in the Windows environment, which makes it easy to create [...]
