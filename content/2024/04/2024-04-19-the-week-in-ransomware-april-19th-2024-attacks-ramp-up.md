Title: The Week in Ransomware - April 19th 2024 -  Attacks Ramp Up
Date: 2024-04-19T19:36:54-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-04-19-the-week-in-ransomware-april-19th-2024-attacks-ramp-up

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-april-19th-2024-attacks-ramp-up/){:target="_blank" rel="noopener"}

> While ransomware attacks decreased after the LockBit and BlackCat disruptions, they have once again started to ramp up with other operations filling the void. [...]
