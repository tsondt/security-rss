Title: Met police failed to act on Commons ‘honeytrap’ sexting reports last year
Date: 2024-04-11T18:51:41+00:00
Author: Aletha Adu
Category: The Guardian
Tags: Metropolitan police;Conservatives;UK news;House of Commons;Data and computer security;Politics
Slug: 2024-04-11-met-police-failed-to-act-on-commons-honeytrap-sexting-reports-last-year

[Source](https://www.theguardian.com/uk-news/2024/apr/11/met-police-failed-to-act-on-commons-honeytrap-sexting-reports-last-year){:target="_blank" rel="noopener"}

> The force has said there was nothing to suggest at the time that the messages were part of a wider pattern The Metropolitan police first had reports of unsolicited messages targeting a number of MPs, staff and journalists in Westminster last year, but officers failed to notify politicians. After the former Conservative MP William Wragg said he was manipulated into giving the personal phone numbers of colleagues to a man he met on a dating app, Scotland Yard said it was working with other police forces, alongside its own investigation, amid concerns many other MPs could have been targeted. Continue [...]
