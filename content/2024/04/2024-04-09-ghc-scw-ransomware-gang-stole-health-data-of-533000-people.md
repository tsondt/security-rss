Title: GHC-SCW: Ransomware gang stole health data of 533,000 people
Date: 2024-04-09T14:02:45-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-04-09-ghc-scw-ransomware-gang-stole-health-data-of-533000-people

[Source](https://www.bleepingcomputer.com/news/security/ghc-scw-ransomware-gang-stole-health-data-of-533-000-people/){:target="_blank" rel="noopener"}

> Non-profit healthcare service provider Group Health Cooperative of South Central Wisconsin (GHC-SCW) has disclosed that a ransomware gang breached its network in January and stole documents containing the personal and medical information of over 500,000 individuals. [...]
