Title: SIM swap crooks solicit T-Mobile US, Verizon staff via text to do their dirty work
Date: 2024-04-16T15:30:12+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-04-16-sim-swap-crooks-solicit-t-mobile-us-verizon-staff-via-text-to-do-their-dirty-work

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/16/sim_swap_scam_tmobile/){:target="_blank" rel="noopener"}

> No breach responsible for employee contact info getting out, says T-Mo T-Mobile US employees say they are being sent text messages that offer them cash to perform illegal SIM swaps for supposed criminals.... [...]
