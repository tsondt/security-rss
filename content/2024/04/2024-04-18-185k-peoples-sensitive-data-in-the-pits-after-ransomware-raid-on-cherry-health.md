Title: 185K people's sensitive data in the pits after ransomware raid on Cherry Health
Date: 2024-04-18T14:00:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-18-185k-peoples-sensitive-data-in-the-pits-after-ransomware-raid-on-cherry-health

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/18/ransomware_cherry_health/){:target="_blank" rel="noopener"}

> Extent of information seized will be a concern for those affected Ransomware strikes at yet another US healthcare organization led to the theft of sensitive data belonging to just shy of 185,000 people.... [...]
