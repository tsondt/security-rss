Title: AT&amp;T admits massive 70M+ mid-March customer data dump is real though old
Date: 2024-04-01T12:34:50+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-04-01-att-admits-massive-70m-mid-march-customer-data-dump-is-real-though-old

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/01/att_admits_massive_70m_midmarch/){:target="_blank" rel="noopener"}

> Still claims the personal info wasn't stolen from its systems AT&T confirmed over the weekend that more than 73 million records of its current and former customers dumped on the dark web in mid-March do indeed describe its subscribers, though it still denies the data came direct from its systems.... [...]
