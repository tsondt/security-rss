Title: Integrate Kubernetes policy-as-code solutions into Security Hub
Date: 2024-04-18T15:58:32+00:00
Author: Joaquin Manuel Rinaudo
Category: AWS Security
Tags: Amazon Elastic Kubernetes Service;AWS Security Hub;Best Practices;Security, Identity, & Compliance;Amazon EKS;EKS;Security Blog;Security Hub
Slug: 2024-04-18-integrate-kubernetes-policy-as-code-solutions-into-security-hub

[Source](https://aws.amazon.com/blogs/security/integrate-kubernetes-policy-as-code-solutions-into-security-hub/){:target="_blank" rel="noopener"}

> Using Kubernetes policy-as-code (PaC) solutions, administrators and security professionals can enforce organization policies to Kubernetes resources. There are several publicly available PAC solutions that are available for Kubernetes, such as Gatekeeper, Polaris, and Kyverno. PaC solutions usually implement two features: Use Kubernetes admission controllers to validate or modify objects before they’re created to help enforce configuration best [...]
