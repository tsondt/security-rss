Title: Ex-White House CIO tells The Reg: TikTok ban may be diplomatic disaster
Date: 2024-04-01T13:15:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-04-01-ex-white-house-cio-tells-the-reg-tiktok-ban-may-be-diplomatic-disaster

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/01/tiktok_ban_white_house/){:target="_blank" rel="noopener"}

> Theresa Payton on why US needs a national privacy law Interview Congress is mulling legislation that will require TikTok's Chinese parent ByteDance to cut ties with the video-sharing mega-app, or the social network will be banned in the USA.... [...]
