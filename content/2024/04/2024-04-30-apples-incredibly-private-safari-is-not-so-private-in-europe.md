Title: Apple's 'incredibly private' Safari is not so private in Europe
Date: 2024-04-30T07:24:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-04-30-apples-incredibly-private-safari-is-not-so-private-in-europe

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/30/apple_safari_europe_tracking/){:target="_blank" rel="noopener"}

> Infosec eggheads find iGiant left EU iOS 17 users open to being tracked around the web Apple's grudging accommodation of European antitrust rules by allowing third-party app stores on iPhones has left users of its Safari browser exposed to potential web activity tracking.... [...]
