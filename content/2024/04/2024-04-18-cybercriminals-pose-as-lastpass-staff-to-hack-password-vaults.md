Title: Cybercriminals pose as LastPass staff to hack password vaults
Date: 2024-04-18T10:56:41-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-18-cybercriminals-pose-as-lastpass-staff-to-hack-password-vaults

[Source](https://www.bleepingcomputer.com/news/security/cybercriminals-pose-as-lastpass-staff-to-hack-password-vaults/){:target="_blank" rel="noopener"}

> LastPass is warning of a malicious campaign targeting its users with the CryptoChameleon phishing kit that is associated with cryptocurrency theft. [...]
