Title: Japanese government rejects Yahoo<i>!</i> infosec improvement plan
Date: 2024-04-17T05:44:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-04-17-japanese-government-rejects-yahoo-infosec-improvement-plan

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/17/japan_rejects_line_yahoo_security_plan/){:target="_blank" rel="noopener"}

> Just doesn't believe it will sort out the mess that saw data leak from LINE messaging app Japan's government has considered the proposed security improvements developed by Yahoo !, found them wanting, and ordered the onetime web giant to take new measures.... [...]
