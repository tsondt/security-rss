Title: Omni Hotels experiencing nationwide IT outage since Friday
Date: 2024-04-02T15:59:56-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-02-omni-hotels-experiencing-nationwide-it-outage-since-friday

[Source](https://www.bleepingcomputer.com/news/security/omni-hotels-experiencing-nationwide-it-outage-since-friday/){:target="_blank" rel="noopener"}

> Omni Hotels & Resorts has been experiencing a chain-wide outage that brought down its IT systems on Friday, impacting reservation, hotel room door lock, and point-of-sale (POS) systems. [...]
