Title: Tory MP Luke Evans reveals he was targeted in Westminster sexting scandal
Date: 2024-04-05T19:02:30+00:00
Author: Eleni Courea and Charlie Moloney
Category: The Guardian
Tags: Conservatives;Politics;Data and computer security;UK news;House of Commons
Slug: 2024-04-05-tory-mp-luke-evans-reveals-he-was-targeted-in-westminster-sexting-scandal

[Source](https://www.theguardian.com/politics/2024/apr/05/conservative-tory-mp-william-wragg-to-keep-whip-investigation){:target="_blank" rel="noopener"}

> Evans says he was first to alert authorities after receiving messages in what is suspected to be part of wider attempt to target MPs A Conservative MP has revealed that he was targeted in the Westminster sexting scandal and was the MP that first alerted the authorities. Luke Evans said he was messaged in what is suspected to be part of a wider attempt to target MPs. Continue reading... [...]
