Title: Friday Squid Blogging: Squid Trackers
Date: 2024-04-19T21:05:43+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;privacy;squid;tracking
Slug: 2024-04-19-friday-squid-blogging-squid-trackers

[Source](https://www.schneier.com/blog/archives/2024/04/friday-squid-blogging-squid-trackers.html){:target="_blank" rel="noopener"}

> A new bioadhesive makes it easier to attach trackers to squid. Note: the article does not discuss squid privacy rights. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
