Title: Apple's GoFetch silicon security fail was down to an obsession with speed
Date: 2024-04-02T07:30:08+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2024-04-02-apples-gofetch-silicon-security-fail-was-down-to-an-obsession-with-speed

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/02/apple_gofetch_opinion/){:target="_blank" rel="noopener"}

> Ye cannae change the laws of physics, but you can change your mind Opinion Apple is good at security. It's good at processors. Thus GoFetch, a major security flaw in its processor architecture, is a double whammy.... [...]
