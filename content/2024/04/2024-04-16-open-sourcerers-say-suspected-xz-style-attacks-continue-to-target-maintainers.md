Title: Open sourcerers say suspected xz-style attacks continue to target maintainers
Date: 2024-04-16T14:07:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-16-open-sourcerers-say-suspected-xz-style-attacks-continue-to-target-maintainers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/16/xz_style_attacks_continue/){:target="_blank" rel="noopener"}

> Social engineering patterns spotted across range of popular projects Open source groups are warning the community about a wave of ongoing attacks targeting project maintainers similar to those that led to the recent attempted backdooring of a core Linux library.... [...]
