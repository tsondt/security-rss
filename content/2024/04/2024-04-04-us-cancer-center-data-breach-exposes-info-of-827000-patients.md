Title: US cancer center data breach exposes info of 827,000 patients
Date: 2024-04-04T12:57:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-04-04-us-cancer-center-data-breach-exposes-info-of-827000-patients

[Source](https://www.bleepingcomputer.com/news/security/us-cancer-center-data-breach-exposes-info-of-827-000-patients/){:target="_blank" rel="noopener"}

> Cancer treatment and research center City of Hope is warning that a data breach exposed the sensitive information of over 820,000 patients. [...]
