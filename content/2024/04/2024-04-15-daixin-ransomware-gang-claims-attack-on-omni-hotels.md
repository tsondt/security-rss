Title: Daixin ransomware gang claims attack on Omni Hotels
Date: 2024-04-15T11:01:54-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-15-daixin-ransomware-gang-claims-attack-on-omni-hotels

[Source](https://www.bleepingcomputer.com/news/security/daixin-ransomware-gang-claims-attack-on-omni-hotels/){:target="_blank" rel="noopener"}

> The Daixin Team ransomware gang claimed a recent cyberattack on Omni Hotels & Resorts and is now threatening to publish customers' sensitive information if a ransom is not paid. [...]
