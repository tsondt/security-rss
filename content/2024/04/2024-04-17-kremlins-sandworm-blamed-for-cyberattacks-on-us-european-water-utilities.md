Title: Kremlin's Sandworm blamed for cyberattacks on US, European water utilities
Date: 2024-04-17T19:56:05+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-17-kremlins-sandworm-blamed-for-cyberattacks-on-us-european-water-utilities

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/17/russia_sandworm_cyberattacks_water/){:target="_blank" rel="noopener"}

> Water tank overflowed during one system malfunction, says Mandiant The Russian military's notorious Sandworm crew was likely behind cyberattacks on US and European water plants that, in at least one case, caused a tank to overflow.... [...]
