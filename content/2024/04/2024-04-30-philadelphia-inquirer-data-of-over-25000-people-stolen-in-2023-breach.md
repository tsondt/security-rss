Title: Philadelphia Inquirer: Data of over 25,000 people stolen in 2023 breach
Date: 2024-04-30T16:12:01-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-30-philadelphia-inquirer-data-of-over-25000-people-stolen-in-2023-breach

[Source](https://www.bleepingcomputer.com/news/security/philadelphia-inquirer-data-of-over-25-000-people-stolen-in-2023-breach/){:target="_blank" rel="noopener"}

> Daily newspaper Philadelphia Inquirer revealed that attackers behind a May 2023 security breach have stolen the personal and financial information of 25,549 individuals. [...]
