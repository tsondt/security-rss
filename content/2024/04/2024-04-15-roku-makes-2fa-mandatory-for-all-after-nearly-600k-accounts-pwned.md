Title: Roku makes 2FA mandatory for all after nearly 600K accounts pwned
Date: 2024-04-15T15:32:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-15-roku-makes-2fa-mandatory-for-all-after-nearly-600k-accounts-pwned

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/15/roku_2fa_for_everyone/){:target="_blank" rel="noopener"}

> Streamer says access came via credential stuffing Streaming giant Roku is making 2FA mandatory after attackers accessed around 591,000 customer accounts earlier this year.... [...]
