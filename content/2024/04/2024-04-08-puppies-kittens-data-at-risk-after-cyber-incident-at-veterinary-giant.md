Title: Puppies, kittens, data at risk after 'cyber incident' at veterinary giant
Date: 2024-04-08T14:30:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-08-puppies-kittens-data-at-risk-after-cyber-incident-at-veterinary-giant

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/08/cyber_incident_strikes_veterinary_services/){:target="_blank" rel="noopener"}

> IT systems pulled offline for chance to paws and reflect First, they came for hospitals, then it was charities and cancer centers. Now, cyber scumbags are coming for the puppies and kittens.... [...]
