Title: Boosting data cyber-resilience for your Cloud Storage data with object retention lock
Date: 2024-04-04T16:00:00+00:00
Author: Karthik Gangidi
Category: GCP Security
Tags: Security & Identity;Storage & Data Transfer
Slug: 2024-04-04-boosting-data-cyber-resilience-for-your-cloud-storage-data-with-object-retention-lock

[Source](https://cloud.google.com/blog/products/storage-data-transfer/introducing-cloud-storage-object-retention-lock/){:target="_blank" rel="noopener"}

> Data retention is crucial for customers especially in regulated industries such as financial services, healthcare, and government. Customers can use write once, read many (WORM) storage to meet their data retention needs, keeping their data immutable, and comply with industry regulations set forth by governing bodies such as FINRA, SEC, and CFTC. WORM storage can also provide an extra layer of security to organizations dealing with sensitive data, by preventing any data modifications or deletions and reducing the risk of accidental data loss, data breaches, and unauthorized alterations. We are now making it easier for our customers to configure WORM [...]
