Title: Singapore infosec boss warns China/West tech split will be bad for interoperability
Date: 2024-04-18T05:32:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-04-18-singapore-infosec-boss-warns-chinawest-tech-split-will-be-bad-for-interoperability

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/18/blackhat_koh_splinternet/){:target="_blank" rel="noopener"}

> When you decide not to trust a big chunk of the supply chain, tech (and trade) get harder One of the biggest challenges Singapore faces is the potential for a split between tech stacks developed and used by China and the West, according to the island nation's Cyber Security Administration (CSA) chief executive David Koh.... [...]
