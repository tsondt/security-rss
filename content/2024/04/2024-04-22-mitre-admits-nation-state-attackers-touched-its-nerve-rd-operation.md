Title: MITRE admits 'nation state' attackers touched its NERVE R&amp;D operation
Date: 2024-04-22T01:57:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-04-22-mitre-admits-nation-state-attackers-touched-its-nerve-rd-operation

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/22/in_brief_security/){:target="_blank" rel="noopener"}

> PLUS: Akira ransomware resurgent; Telehealth outfit fined for data-sharing; This week's nastiest vulns Infosec In Brief In a cautionary tale that no one is immune from attack, the security org MITRE has admitted that it got pwned.... [...]
