Title: US government excoriates Microsoft for 'avoidable errors' but keeps paying for its products
Date: 2024-04-05T14:30:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-05-us-government-excoriates-microsoft-for-avoidable-errors-but-keeps-paying-for-its-products

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/05/microsoft_government_contracts/){:target="_blank" rel="noopener"}

> In what other sphere does a bad supplier not feel pain for its foulups? Analysis You might think that when a government supplier fails in one of its key duties it would find itself shunned or at least feel financial pain.... [...]
