Title: New Spectre v2 attack impacts Linux systems on Intel CPUs
Date: 2024-04-10T13:19:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware;Linux
Slug: 2024-04-10-new-spectre-v2-attack-impacts-linux-systems-on-intel-cpus

[Source](https://www.bleepingcomputer.com/news/security/new-spectre-v2-attack-impacts-linux-systems-on-intel-cpus/){:target="_blank" rel="noopener"}

> Researchers have demonstrated the "first native Spectre v2 exploit" for a new speculative execution side-channel flaw that impacts Linux systems running on many modern Intel processors. [...]
