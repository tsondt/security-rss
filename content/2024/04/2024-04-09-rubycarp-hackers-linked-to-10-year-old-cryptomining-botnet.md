Title: RUBYCARP hackers linked to 10-year-old cryptomining botnet
Date: 2024-04-09T11:30:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-09-rubycarp-hackers-linked-to-10-year-old-cryptomining-botnet

[Source](https://www.bleepingcomputer.com/news/security/rubycarp-hackers-linked-to-10-year-old-cryptomining-botnet/){:target="_blank" rel="noopener"}

> A Romanian botnet group named 'RUBYCARP' is leveraging known vulnerabilities and performing brute force attacks to breach corporate networks and compromise servers for financial gain. [...]
