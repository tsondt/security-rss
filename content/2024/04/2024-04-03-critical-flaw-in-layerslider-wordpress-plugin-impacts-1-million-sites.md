Title: Critical flaw in LayerSlider WordPress plugin impacts 1 million sites
Date: 2024-04-03T14:21:38-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-03-critical-flaw-in-layerslider-wordpress-plugin-impacts-1-million-sites

[Source](https://www.bleepingcomputer.com/news/security/critical-flaw-in-layerslider-wordpress-plugin-impacts-1-million-sites/){:target="_blank" rel="noopener"}

> A premium WordPress plugin named LayerSlider, used in over one million sites, is vulnerable to unauthenticated SQL injection, requiring admins to prioritize applying security updates for the plugin. [...]
