Title: xz Utils Backdoor
Date: 2024-04-02T18:50:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;backdoors;cybersecurity;hacking;malware;open source;social engineering
Slug: 2024-04-02-xz-utils-backdoor

[Source](https://www.schneier.com/blog/archives/2024/04/xz-utils-backdoor.html){:target="_blank" rel="noopener"}

> The cybersecurity world got really lucky last week. An intentionally placed backdoor in xz Utils, an open-source compression utility, was pretty much accidentally discovered by a Microsoft engineer—weeks before it would have been incorporated into both Debian and Red Hat Linux. From ArsTehnica : Malicious code added to xz Utils versions 5.6.0 and 5.6.1 modified the way the software functions. The backdoor manipulated sshd, the executable file used to make remote SSH connections. Anyone in possession of a predetermined encryption key could stash any code of their choice in an SSH login certificate, upload it, and execute it on the [...]
