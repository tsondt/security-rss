Title: 2023 ISO 27001 certificate available in Spanish and French, and 2023 ISO 22301 certificate available in Spanish
Date: 2024-04-18T18:48:06+00:00
Author: Atulsing Patil
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS CSA STAR;AWS ISO;AWS ISO Certificates;AWS ISO22301;AWS ISO27001;AWS ISO27017;AWS ISO27018;AWS ISO9001;Compliance;Compliance reports;Security Blog
Slug: 2024-04-18-2023-iso-27001-certificate-available-in-spanish-and-french-and-2023-iso-22301-certificate-available-in-spanish

[Source](https://aws.amazon.com/blogs/security/2023-iso-27001-certificate-available-in-spanish-and-french-and-2023-iso-22301-certificate-available-in-spanish/){:target="_blank" rel="noopener"}

> French » Spanish » Amazon Web Services (AWS) is pleased to announce that a translated version of our 2023 ISO 27001 and 2023 ISO 22301 certifications are now available: The 2023 ISO 27001 certificate is available in Spanish and French. The 2023 ISO 22301 certificate is available in Spanish. Translated certificates are available to customers [...]
