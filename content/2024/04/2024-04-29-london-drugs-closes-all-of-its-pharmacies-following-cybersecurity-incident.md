Title: London Drugs closes all of its pharmacies following 'cybersecurity incident'
Date: 2024-04-29T18:21:35+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-29-london-drugs-closes-all-of-its-pharmacies-following-cybersecurity-incident

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/29/canada_london_drugs/){:target="_blank" rel="noopener"}

> Canadian stores shuttered 'until further notice' Updated Canadian pharmacy chain London Drugs closed all of its stores over the weekend until further notice following a "cybersecurity incident."... [...]
