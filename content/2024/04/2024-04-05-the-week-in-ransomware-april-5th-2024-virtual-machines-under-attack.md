Title: The Week in Ransomware - April 5th 2024 - Virtual Machines under Attack
Date: 2024-04-05T17:59:20-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-04-05-the-week-in-ransomware-april-5th-2024-virtual-machines-under-attack

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-april-5th-2024-virtual-machines-under-attack/){:target="_blank" rel="noopener"}

> Ransomware attacks targeting VMware ESXi and other virtual machine platforms are wreaking havoc among the enterprise, causing widespread disruption and loss of services. [...]
