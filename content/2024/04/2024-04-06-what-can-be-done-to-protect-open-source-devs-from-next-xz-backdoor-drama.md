Title: What can be done to protect open source devs from next xz backdoor drama?
Date: 2024-04-06T16:12:14+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-04-06-what-can-be-done-to-protect-open-source-devs-from-next-xz-backdoor-drama

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/06/register_kettle_xz/){:target="_blank" rel="noopener"}

> What happened, how it was found, and what your vultures have made of it all Kettle It's been about a week since the shock discovery of a hidden and truly sophisticated backdoor in the xz software library that ordinarily is used by countless systems.... [...]
