Title: Cyber-bastard jailed for stealing psychotherapy files, blackmailing patients
Date: 2024-04-30T23:26:41+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-30-cyber-bastard-jailed-for-stealing-psychotherapy-files-blackmailing-patients

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/30/finnish_psychotherapy_center_crook_sentenced/){:target="_blank" rel="noopener"}

> Vastaamo villain more than doubled reported crime in Nordic nation A cyber-thief who snatched tens of thousands of patients' sensitive records from a psychotherapy clinic before blackmailing them and then leaking their files online has been caged for six years and three months.... [...]
