Title: Crypto miner arrested for skipping on $3.5 million in cloud server bills
Date: 2024-04-15T14:10:41-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency;Legal
Slug: 2024-04-15-crypto-miner-arrested-for-skipping-on-35-million-in-cloud-server-bills

[Source](https://www.bleepingcomputer.com/news/security/crypto-miner-arrested-for-skipping-on-35-million-in-cloud-server-bills/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice has announced the arrest and indictment of Charles O. Parks III, known as "CP3O," for allegedly renting large numbers of cloud servers to conduct crypto mining and then skipping out on paying the bills. [...]
