Title: FIN7 targets American automaker’s IT staff in phishing attacks
Date: 2024-04-17T16:40:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-17-fin7-targets-american-automakers-it-staff-in-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/fin7-targets-american-automakers-it-staff-in-phishing-attacks/){:target="_blank" rel="noopener"}

> The financially motivated threat actor FIN7 targeted a large U.S. car maker with spear-phishing emails for employees in the IT department to infect systems with the Anunak backdoor. [...]
