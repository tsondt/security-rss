Title: Four trends to top the CISO’s packed agenda
Date: 2024-04-26T07:34:12+00:00
Author: Guy Matthews
Category: The Register
Tags: 
Slug: 2024-04-26-four-trends-to-top-the-cisos-packed-agenda

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/26/four_trends_to_top_the/){:target="_blank" rel="noopener"}

> Check out the SANS CISO Primer for tips on hardening your organisation’s security posture in 2024 Sponsored Post Ever get nostalgic for the good old days of cybersecurity protection? When attacks were for the most part amateurish and infrequent, and perhaps more in the nature of an occasional nuisance rather than a daily existential threat?... [...]
