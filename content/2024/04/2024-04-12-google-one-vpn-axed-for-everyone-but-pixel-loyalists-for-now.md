Title: Google One VPN axed for everyone but Pixel loyalists ... for now
Date: 2024-04-12T20:21:06+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-04-12-google-one-vpn-axed-for-everyone-but-pixel-loyalists-for-now

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/12/vpn_google_shuttered/){:target="_blank" rel="noopener"}

> Another one bytes the dust In an incredibly rare move, Google is killing off one of its online services – this time, VPN for Google One.... [...]
