Title: Fake Lawsuit Threat Exposes Privnote Phishing Sites
Date: 2024-04-04T14:12:16+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Latest Warnings;Russia's War on Ukraine;Web Fraud 2.0;Alexandr Ermakov;Andrey Sokol;fory66399;Hkleaks;MetaMask;Privnote;privnote.com;Taylor Monahan;Tornote.io
Slug: 2024-04-04-fake-lawsuit-threat-exposes-privnote-phishing-sites

[Source](https://krebsonsecurity.com/2024/04/fake-lawsuit-threat-exposes-privnote-phishing-sites/){:target="_blank" rel="noopener"}

> A cybercrook who has been setting up websites that mimic the self-destructing message service privnote.com accidentally exposed the breadth of their operations recently when they threatened to sue a software company. The disclosure revealed a profitable network of phishing sites that behave and look like the real Privnote, except that any messages containing cryptocurrency addresses will be automatically altered to include a different payment address controlled by the scammers. The real Privnote, at privnote.com. Launched in 2008, privnote.com employs technology that encrypts each message so that even Privnote itself cannot read its contents. And it doesn’t send or receive messages. [...]
