Title: Hackers infect users of antivirus service that delivered updates over HTTP
Date: 2024-04-23T21:03:01+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;antivirus;backdoors;encryption;HTTP;HTTPS;man in the middle
Slug: 2024-04-23-hackers-infect-users-of-antivirus-service-that-delivered-updates-over-http

[Source](https://arstechnica.com/?p=2019398){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Hackers abused an antivirus service for five years in order to infect end users with malware. The attack worked because the service delivered updates over HTTP, a protocol vulnerable to attacks that corrupt or tamper with data as it travels over the Internet. The unknown hackers, who may have ties to the North Korean government, pulled off this feat by performing a man-in-the-middle (MiitM) attack that replaced the genuine update with a file that installed an advanced backdoor instead, said researchers from security firm Avast today. eScan, an AV service headquartered in India, has delivered updates [...]
