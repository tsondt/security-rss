Title: No 10 tells MPs to be cautious about unsolicited messages after attempted ‘honeytrap’
Date: 2024-04-08T14:42:22+00:00
Author: Kiran Stacey Political correspondent
Category: The Guardian
Tags: Conservatives;Data and computer security;UK news;Politics;Technology;WhatsApp;Grindr
Slug: 2024-04-08-no-10-tells-mps-to-be-cautious-about-unsolicited-messages-after-attempted-honeytrap

[Source](https://www.theguardian.com/politics/2024/apr/08/no-10-mps-cautious-about-unsolicited-messages-after-attempted-honeytrap){:target="_blank" rel="noopener"}

> Message comes as pressure builds on Tories to take disciplinary action against MP William Wragg UK politics – latest updates Downing Street has urged MPs to be cautious when responding to unsolicited messages, after the “spear-phishing” attack that targeted more than a dozen MPs, staff and journalists working in Westminster. Number 10 issued the warning on Monday morning, days after two police forces launched an investigation into what is being described as an attempted “ honeytrap ”. Continue reading... [...]
