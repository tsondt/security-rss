Title: Ransomware feared as IT 'issues' force Octapharma Plasma to close 150+ centers
Date: 2024-04-18T22:27:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-18-ransomware-feared-as-it-issues-force-octapharma-plasma-to-close-150-centers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/18/ransomware_octapharma_plasma/){:target="_blank" rel="noopener"}

> Source blames BlackSuit infection – as separately ISP Frontier confirms cyberattack Octapharma Plasma has blamed IT "network issues" for the ongoing closure of its 150-plus centers across the US. It's feared a ransomware infection may be the root cause of the medical firm's ailment.... [...]
