Title: FBI warns of fake verification schemes targeting dating app users
Date: 2024-04-29T12:59:11-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-29-fbi-warns-of-fake-verification-schemes-targeting-dating-app-users

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-fake-verification-schemes-targeting-dating-app-users/){:target="_blank" rel="noopener"}

> The FBI is warning of fake verification schemes promoted by fraudsters on online dating platforms that lead to costly recurring subscription charges. [...]
