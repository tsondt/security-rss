Title: Indian bank’s IT is so shabby it’s been banned from opening new accounts
Date: 2024-04-25T06:29:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-04-25-indian-banks-it-is-so-shabby-its-been-banned-from-opening-new-accounts

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/25/rbi_india_kotak_mahindra_bank/){:target="_blank" rel="noopener"}

> After two years of warnings, and outages, regulators ran out of patience with Kotak Mahindra Bank India’s central bank has banned Kotak Mahindra Bank from signing up new customers for accounts or credit cards through its online presence and app.... [...]
