Title: Microsoft will limit Exchange Online bulk emails to fight spam
Date: 2024-04-15T15:11:16-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-04-15-microsoft-will-limit-exchange-online-bulk-emails-to-fight-spam

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-will-limit-exchange-online-bulk-emails-to-fight-spam/){:target="_blank" rel="noopener"}

> Microsoft has announced plans to fight spam by imposing a daily Exchange Online bulk email limit of 2,000 external recipients starting January 2025. [...]
