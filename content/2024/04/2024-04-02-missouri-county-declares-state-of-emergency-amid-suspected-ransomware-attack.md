Title: Missouri county declares state of emergency amid suspected ransomware attack
Date: 2024-04-02T23:59:50+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;hacking;jackson county;ransomware
Slug: 2024-04-02-missouri-county-declares-state-of-emergency-amid-suspected-ransomware-attack

[Source](https://arstechnica.com/?p=2014470){:target="_blank" rel="noopener"}

> Enlarge / Downtown Kansas City, Missouri, which is part of Jackson County. (credit: Eric Rogers ) Jackson County, Missouri, has declared a state of emergency and closed key offices indefinitely as it responds to what officials believe is a ransomware attack that has made some of its IT systems inoperable. "Jackson County has identified significant disruptions within its IT systems, potentially attributable to a ransomware attack," officials wrote Tuesday. "Early indications suggest operational inconsistencies across its digital infrastructure and certain systems have been rendered inoperative while others continue to function as normal." The systems confirmed inoperable include tax and online [...]
