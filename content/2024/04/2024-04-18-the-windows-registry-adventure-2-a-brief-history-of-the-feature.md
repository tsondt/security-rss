Title: The Windows Registry Adventure #2: A brief history of the feature
Date: 2024-04-18T09:46:00-07:00
Author: Google Project Zero (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2024-04-18-the-windows-registry-adventure-2-a-brief-history-of-the-feature

[Source](https://googleprojectzero.blogspot.com/2024/04/the-windows-registry-adventure-2.html){:target="_blank" rel="noopener"}

> Posted by Mateusz Jurczyk, Google Project Zero Before diving into the low-level security aspects of the registry, it is important to understand its role in the operating system and a bit of history behind it. In essence, the registry is a hierarchical database made of named "keys" and "values", used by Windows and applications to store a variety of settings and configuration data. It is represented by a tree structure, in which keys may have one or more sub-keys, and every subkey is associated with exactly one parent key. Furthermore, every key may also contain one or more values, which [...]
