Title: US imposes visa bans on 13 spyware makers and their families
Date: 2024-04-23T11:38:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-23-us-imposes-visa-bans-on-13-spyware-makers-and-their-families

[Source](https://www.bleepingcomputer.com/news/security/us-imposes-visa-bans-on-13-spyware-makers-and-their-families/){:target="_blank" rel="noopener"}

> ​The Department of State has started imposing visa restrictions on mercenary spyware makers and peddlers, prohibiting their entry into the United States, as announced earlier in February. [...]
