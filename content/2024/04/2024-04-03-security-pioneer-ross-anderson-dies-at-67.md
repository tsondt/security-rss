Title: Security pioneer Ross Anderson dies at 67
Date: 2024-04-03T12:48:51+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-03-security-pioneer-ross-anderson-dies-at-67

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/03/ross_anderson_obit/){:target="_blank" rel="noopener"}

> A man with a list of accolades long enough for several lifetimes, friends remember his brilliance Obituary Venerable computer scientist and information security expert Ross Anderson has died at the age of 67.... [...]
