Title: Omni Hotels confirms cyberattack behind ongoing IT outage
Date: 2024-04-03T18:17:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-03-omni-hotels-confirms-cyberattack-behind-ongoing-it-outage

[Source](https://www.bleepingcomputer.com/news/security/omni-hotels-confirms-cyberattack-behind-ongoing-it-outage/){:target="_blank" rel="noopener"}

> Omni Hotels & Resorts has confirmed a cyberattack caused a nationwide IT outage that is still affecting its locations. [...]
