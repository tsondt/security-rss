Title: LabHost phishing service with 40,000 domains disrupted, 37 arrested
Date: 2024-04-18T05:52:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-04-18-labhost-phishing-service-with-40000-domains-disrupted-37-arrested

[Source](https://www.bleepingcomputer.com/news/security/labhost-phishing-service-with-40-000-domains-disrupted-37-arrested/){:target="_blank" rel="noopener"}

> The LabHost phishing-as-a-service (PhaaS) platform has been disrupted in a year-long global law enforcement operation that compromised the infrastructure and arrested 37 suspects, among them the original developer. [...]
