Title: Korean researcher details scheme abusing Apple's third-party pickup policy
Date: 2024-04-18T16:00:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-04-18-korean-researcher-details-scheme-abusing-apples-third-party-pickup-policy

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/18/blackhat_apple_korea/){:target="_blank" rel="noopener"}

> Criminals make lucrative use of stolen credit cards Black Hat Asia Speaking at Black Hat Asia on Thursday, a Korean researcher revealed how the discovery of one phishing website led to uncovering an operation whose activities leveraged second-hand shops and included using Apple’s "someone-else pickup" method to cash in.... [...]
