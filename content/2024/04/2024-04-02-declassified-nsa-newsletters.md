Title: Declassified NSA Newsletters
Date: 2024-04-02T17:05:15+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Applied Cryptography;FOIA;history of cryptography;history of security;NSA
Slug: 2024-04-02-declassified-nsa-newsletters

[Source](https://www.schneier.com/blog/archives/2024/04/declassified-nsa-newsletters.html){:target="_blank" rel="noopener"}

> Through a 2010 FOIA request (yes, it took that long), we have copies of the NSA’s KRYPTOS Society Newsletter, “ Tales of the Krypt,” from 1994 to 2003. There are many interesting things in the 800 pages of newsletter. There are many redactions. And a 1994 review of Applied Cryptography by redacted : Applied Cryptography, for those who don’t read the internet news, is a book written by Bruce Schneier last year. According to the jacket, Schneier is a data security expert with a master’s degree in computer science. According to his followers, he is a hero who has finally [...]
