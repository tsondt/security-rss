Title: Caliptra: Building trust, one chip at a time
Date: 2024-04-23T20:00:00+00:00
Author: Amber Huffman
Category: GCP Security
Tags: Security & Identity;Systems
Slug: 2024-04-23-caliptra-building-trust-one-chip-at-a-time

[Source](https://cloud.google.com/blog/topics/systems/google-security-innovation-at-the-ocp-regional-summit/){:target="_blank" rel="noopener"}

> At Google, we build sustainable, secure, and scalable hardware and software to enable services that support billions of users. We have embraced open innovation as a core tenet to deliver these experiences. Our society’s AI-driven future includes many types of system-on-chips (SoCs) acting in concert with each other — from CPUs to GPUs to TPUs to NICs to SSDs and more. To deliver secure solutions at scale, there must be trust and transparency for the firmware that runs on all of these chips. Welcoming Caliptra 1.0 Google partnered with AMD, Microsoft, and NVIDIA to develop Caliptra, a standard at the [...]
