Title: LastPass: Hackers targeted employee in failed deepfake CEO call
Date: 2024-04-11T18:00:39-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-11-lastpass-hackers-targeted-employee-in-failed-deepfake-ceo-call

[Source](https://www.bleepingcomputer.com/news/security/lastpass-hackers-targeted-employee-in-failed-deepfake-ceo-call/){:target="_blank" rel="noopener"}

> LastPass revealed this week that threat actors targeted one of its employees in a voice phishing attack, using deepfake audio to impersonate Karim Toubba, the company's Chief Executive Officer. [...]
