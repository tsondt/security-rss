Title: DPRK hacking groups breach South Korean defense contractors
Date: 2024-04-23T12:56:50-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-23-dprk-hacking-groups-breach-south-korean-defense-contractors

[Source](https://www.bleepingcomputer.com/news/security/dprk-hacking-groups-breach-south-korean-defense-contractors/){:target="_blank" rel="noopener"}

> The National Police Agency in South Korea issued an urgent warning today about North Korean hacking groups targeting defense industry entities to steal valuable technology information. [...]
