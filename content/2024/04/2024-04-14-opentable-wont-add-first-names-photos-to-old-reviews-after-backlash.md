Title: OpenTable won't add first names, photos to old reviews after backlash
Date: 2024-04-14T18:28:01-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-04-14-opentable-wont-add-first-names-photos-to-old-reviews-after-backlash

[Source](https://www.bleepingcomputer.com/news/security/opentable-wont-add-first-names-photos-to-old-reviews-after-backlash/){:target="_blank" rel="noopener"}

> OpenTable has reversed its decision to show members' first names and profile pictures in past anonymous reviews after receiving backlash from members who felt it was a breach of privacy. [...]
