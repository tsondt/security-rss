Title: HelloKitty ransomware rebrands, releases CD Projekt and Cisco data
Date: 2024-04-19T15:20:45-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-19-hellokitty-ransomware-rebrands-releases-cd-projekt-and-cisco-data

[Source](https://www.bleepingcomputer.com/news/security/hellokitty-ransomware-rebrands-releases-cd-projekt-and-cisco-data/){:target="_blank" rel="noopener"}

> An operator of the HelloKitty ransomware operation announced they changed the name to 'HelloGookie,' releasing passwords for previously leaked CD Projekt source code, Cisco network information, and decryption keys from old attacks.. [...]
