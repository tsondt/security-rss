Title: The curious case of faster AWS KMS symmetric key rotation
Date: 2024-04-12T19:44:38+00:00
Author: Jeremy Stieglitz
Category: AWS Security
Tags: Advanced (300);Announcements;AWS Key Management Service;Best Practices;Security, Identity, & Compliance;AWS Key Management Service (KMS);Compliance;Identity;Security;Security Blog
Slug: 2024-04-12-the-curious-case-of-faster-aws-kms-symmetric-key-rotation

[Source](https://aws.amazon.com/blogs/security/the-curious-case-of-faster-aws-kms-symmetric-key-rotation/){:target="_blank" rel="noopener"}

> Today, AWS Key Management Service (AWS KMS) is introducing faster options for automatic symmetric key rotation. We’re also introducing rotate on-demand, rotation visibility improvements, and a new limit on the price of all symmetric keys that have had two or more rotations (including existing keys). In this post, I discuss all those capabilities and changes. [...]
