Title: Google bakes new cookie strategy that will leave crooks with a bad taste
Date: 2024-04-03T12:08:17+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2024-04-03-google-bakes-new-cookie-strategy-that-will-leave-crooks-with-a-bad-taste

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/03/google_cookie_strategy/){:target="_blank" rel="noopener"}

> Device Bound Session Credentials said to render cookie theft useless Google reckons that cookie theft is a problem for users, and is seeking to address it with a mechanism to tie authentication data to a specific device, rendering any stolen cookies useless.... [...]
