Title: Attackers are pummeling networks around the world with millions of login attempts
Date: 2024-04-16T21:31:25+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;credential compromise;SSH;Talos;vpns
Slug: 2024-04-16-attackers-are-pummeling-networks-around-the-world-with-millions-of-login-attempts

[Source](https://arstechnica.com/?p=2017646){:target="_blank" rel="noopener"}

> Enlarge (credit: Matejmo | Getty Images) Cisco’s Talos security team is warning of a large-scale credential compromise campaign that’s indiscriminately assailing networks with login attempts aimed at gaining unauthorized access to VPN, SSH, and web application accounts. The login attempts use both generic usernames and valid usernames targeted at specific organizations. Cisco included a list of more than 2,000 usernames and almost 100 passwords used in the attacks, along with nearly 4,000 IP addresses sending the login traffic. The IP addresses appear to originate from TOR exit nodes and other anonymizing tunnels and proxies. The attacks appear to be indiscriminate [...]
