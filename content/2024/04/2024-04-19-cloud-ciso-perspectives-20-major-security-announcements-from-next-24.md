Title: Cloud CISO Perspectives: 20 major security announcements from Next ‘24
Date: 2024-04-19T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2024-04-19-cloud-ciso-perspectives-20-major-security-announcements-from-next-24

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-20-major-security-announcements-from-next-24/){:target="_blank" rel="noopener"}

> Welcome to the first Cloud CISO Perspectives for April 2024. In this update, we'll give a list of some of the major announcements of security products and security enhancements to Google Cloud. There's an even longer list here. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI Security & CISO, Google Cloud aside_block <ListValue: [StructValue([('title', 'Get vital CISO Insights with Google Cloud'), ('body', <wagtail.rich_text.RichText object at 0x3e1a96f4da00>), ('btn_text', [...]
