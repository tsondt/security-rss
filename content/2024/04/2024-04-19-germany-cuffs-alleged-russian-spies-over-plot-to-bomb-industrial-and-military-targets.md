Title: Germany cuffs alleged Russian spies over plot to bomb industrial and military targets
Date: 2024-04-19T10:15:09+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-04-19-germany-cuffs-alleged-russian-spies-over-plot-to-bomb-industrial-and-military-targets

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/19/germany_arrests_alleged_russian_spies/){:target="_blank" rel="noopener"}

> Apparently an attempt to damage Ukraine's war effort Bavarian state police have arrested two German-Russian citizens on suspicion of being Russian spies and planning to bomb industrial and military facilities that participate in efforts to assist Ukraine defend itself against Vladimir Putin’s illegal invasion.... [...]
