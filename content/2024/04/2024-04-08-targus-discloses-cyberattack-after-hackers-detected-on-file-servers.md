Title: Targus discloses cyberattack after hackers detected on file servers
Date: 2024-04-08T21:41:08-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-04-08-targus-discloses-cyberattack-after-hackers-detected-on-file-servers

[Source](https://www.bleepingcomputer.com/news/security/targus-discloses-cyberattack-after-hackers-detected-on-file-servers/){:target="_blank" rel="noopener"}

> Laptop and tablet accessories maker Targus disclosed that it suffered a cyberattack disrupting operations after a threat actor gained access to the company's file servers. [...]
