Title: Apple: Mercenary spyware attacks target iPhone users in 92 countries
Date: 2024-04-11T10:16:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple;Mobile
Slug: 2024-04-11-apple-mercenary-spyware-attacks-target-iphone-users-in-92-countries

[Source](https://www.bleepingcomputer.com/news/security/apple-mercenary-spyware-attacks-target-iphone-users-in-92-countries/){:target="_blank" rel="noopener"}

> Apple has been notifying iPhone users in 92 countries about a "mercenary spyware attack" attempting to remotely compromise their device. [...]
