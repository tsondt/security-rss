Title: Feds probe massive alleged classified US govt data theft and leak
Date: 2024-04-04T18:20:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-04-feds-probe-massive-alleged-classified-us-govt-data-theft-and-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/04/feds_data_dump/){:target="_blank" rel="noopener"}

> State Dept keeps schtum 'for security reasons' Updated Uncle Sam is investigating claims that some miscreant stole and leaked classified information from the Pentagon and other national security agencies.... [...]
