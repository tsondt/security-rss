Title: Russian Sandworm hackers targeted 20 critical orgs in Ukraine
Date: 2024-04-22T08:30:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-22-russian-sandworm-hackers-targeted-20-critical-orgs-in-ukraine

[Source](https://www.bleepingcomputer.com/news/security/russian-sandworm-hackers-targeted-20-critical-orgs-in-ukraine/){:target="_blank" rel="noopener"}

> Russian hacker group Sandworm aimed to disrupt operations at around 20 critical infrastructure facilities in Ukraine, according to a report from the Ukrainian Computer Emergency Response Team (CERT-UA). [...]
