Title: Man Who Mass-Extorted Psychotherapy Patients Gets Six Years
Date: 2024-04-30T13:34:32+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Antti Kurittu;ColdFusion botnet;hack the planet;julius zeekill kivimaki;Lizard Squad;ransom_man;Vastaamo Psychotherapy Center;Ville Tapio
Slug: 2024-04-30-man-who-mass-extorted-psychotherapy-patients-gets-six-years

[Source](https://krebsonsecurity.com/2024/04/man-who-mass-extorted-psychotherapy-patients-gets-six-years/){:target="_blank" rel="noopener"}

> A 26-year-old Finnish man was sentenced to more than six years in prison today after being convicted of hacking into an online psychotherapy clinic, leaking tens of thousands of patient therapy records, and attempting to extort the clinic and patients. On October 21, 2020, the Vastaamo Psychotherapy Center in Finland became the target of blackmail when a tormentor identified as “ransom_man” demanded payment of 40 bitcoins (~450,000 euros at the time) in return for a promise not to publish highly sensitive therapy session notes Vastaamo had exposed online. Ransom_man announced on the dark web that he would start publishing 100 [...]
