Title: Home Depot confirms third-party data breach exposed employee info
Date: 2024-04-07T13:40:56-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-04-07-home-depot-confirms-third-party-data-breach-exposed-employee-info

[Source](https://www.bleepingcomputer.com/news/security/home-depot-confirms-third-party-data-breach-exposed-employee-info/){:target="_blank" rel="noopener"}

> Home Depot has confirmed that it suffered a data breach after one of its SaaS vendors mistakenly exposed a small sample of limited employee data, which could potentially be used in targeted phishing attacks. [...]
