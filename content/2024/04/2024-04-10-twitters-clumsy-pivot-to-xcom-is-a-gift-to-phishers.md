Title: Twitter’s Clumsy Pivot to X.com Is a Gift to Phishers
Date: 2024-04-10T14:28:17+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Other;DomainTools.com;Mastodon;Matthew Garrett;phishing;Sean McNee;Twitter.com;X.com
Slug: 2024-04-10-twitters-clumsy-pivot-to-xcom-is-a-gift-to-phishers

[Source](https://krebsonsecurity.com/2024/04/twitters-clumsy-pivot-to-x-com-is-a-gift-to-phishers/){:target="_blank" rel="noopener"}

> On April 9, Twitter/X began automatically modifying links that mention “twitter.com” to read “x.com” instead. But over the past 48 hours, dozens of new domain names have been registered that demonstrate how this change could be used to craft convincing phishing links — such as fedetwitter[.]com, which until very recently rendered as fedex.com in tweets. The message displayed when one visits goodrtwitter.com, which Twitter/X displayed as goodrx.com in tweets and messages. A search at DomainTools.com shows at least 60 domain names have been registered over the past two days for domains ending in “twitter.com,” although research so far shows the [...]
