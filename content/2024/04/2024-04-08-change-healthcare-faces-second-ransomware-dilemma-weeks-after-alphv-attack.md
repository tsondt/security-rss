Title: Change Healthcare faces second ransomware dilemma weeks after ALPHV attack
Date: 2024-04-08T13:00:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-08-change-healthcare-faces-second-ransomware-dilemma-weeks-after-alphv-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/08/change_healthcare_ransomware/){:target="_blank" rel="noopener"}

> Theories abound over who's truly responsible Change Healthcare is allegedly being extorted by a second ransomware gang, mere weeks after recovering from an ALPHV attack.... [...]
