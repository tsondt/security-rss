Title: House passes bill banning Uncle Sam from snooping on citizens via data brokers
Date: 2024-04-18T17:29:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-18-house-passes-bill-banning-uncle-sam-from-snooping-on-citizens-via-data-brokers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/18/congress_data_broker_surveillance/){:target="_blank" rel="noopener"}

> Vote met strong opposition from Biden's office A draft law to restrict the US government's ability to procure data on citizens through data brokers will progress to the Senate after being passed in the House of Representatives.... [...]
