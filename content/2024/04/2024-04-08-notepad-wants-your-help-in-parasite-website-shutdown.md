Title: Notepad++ wants your help in "parasite website" shutdown
Date: 2024-04-08T05:51:17-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2024-04-08-notepad-wants-your-help-in-parasite-website-shutdown

[Source](https://www.bleepingcomputer.com/news/security/notepad-plus-plus-wants-your-help-in-parasite-website-shutdown/){:target="_blank" rel="noopener"}

> The Notepad++ project is seeking the public's help in taking down a copycat website that closely impersonates Notepad++ but is not affiliated with the project. There is some concern that it could pose security threats—for example, if it starts pushing malicious releases or spam someday either deliberately or as a result of a hijack. [...]
