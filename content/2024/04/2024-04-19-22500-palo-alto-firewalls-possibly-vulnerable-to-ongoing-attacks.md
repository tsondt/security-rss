Title: 22,500 Palo Alto firewalls "possibly vulnerable" to ongoing attacks
Date: 2024-04-19T11:27:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-19-22500-palo-alto-firewalls-possibly-vulnerable-to-ongoing-attacks

[Source](https://www.bleepingcomputer.com/news/security/22-500-palo-alto-firewalls-possibly-vulnerable-to-ongoing-attacks/){:target="_blank" rel="noopener"}

> Approximately 22,500 exposed Palo Alto GlobalProtect firewall devices are likely vulnerable to the CVE-2024-3400 flaw, a critical command injection vulnerability that has been actively exploited in attacks since at least March 26, 2024. [...]
