Title: FTC: Americans lost $1.1 billion to impersonation scams in 2023
Date: 2024-04-01T12:03:06-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-04-01-ftc-americans-lost-11-billion-to-impersonation-scams-in-2023

[Source](https://www.bleepingcomputer.com/news/security/ftc-americans-lost-11-billion-to-impersonation-scams-in-2023/){:target="_blank" rel="noopener"}

> Impersonation scams in the U.S. exceeded $1.1 billion in losses last year, according to statistics collected by the Federal Trade Commission (FTC), a figure that is three times higher than in 2020. [...]
