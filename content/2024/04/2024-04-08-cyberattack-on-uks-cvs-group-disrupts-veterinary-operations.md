Title: Cyberattack on UK’s CVS Group disrupts veterinary operations
Date: 2024-04-08T10:45:32-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-04-08-cyberattack-on-uks-cvs-group-disrupts-veterinary-operations

[Source](https://www.bleepingcomputer.com/news/security/cyberattack-on-uks-cvs-group-disrupts-veterinary-operations/){:target="_blank" rel="noopener"}

> UK veterinary services provider CVS Group has announced that it suffered a cyberattack that disrupted IT services at its practices across the country. [...]
