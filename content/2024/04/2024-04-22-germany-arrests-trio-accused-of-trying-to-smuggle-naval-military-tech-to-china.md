Title: Germany arrests trio accused of trying to smuggle naval military tech to China
Date: 2024-04-22T15:30:08+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-04-22-germany-arrests-trio-accused-of-trying-to-smuggle-naval-military-tech-to-china

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/22/germany_arrests_espionage_suspects/){:target="_blank" rel="noopener"}

> Prosecutors believe one frikkin' laser did make its way to Beijing Germany has arrested three citizens who allegedly tried to transfer military technology to China, a violation of the country's export rules.... [...]
