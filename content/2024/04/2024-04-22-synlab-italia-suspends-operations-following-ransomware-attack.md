Title: Synlab Italia suspends operations following ransomware attack
Date: 2024-04-22T11:27:52-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-04-22-synlab-italia-suspends-operations-following-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/synlab-italia-suspends-operations-following-ransomware-attack/){:target="_blank" rel="noopener"}

> Synlab Italia has suspended all its medical diagnostic and testing services after a ransomware attack forced its IT systems to be taken offline. [...]
