Title: UK e-visa rollout starts today for millions: no more physical immigration cards
Date: 2024-04-17T01:48:43-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2024-04-17-uk-e-visa-rollout-starts-today-for-millions-no-more-physical-immigration-cards

[Source](https://www.bleepingcomputer.com/news/security/uk-e-visa-rollout-starts-today-for-millions-no-more-physical-immigration-cards/){:target="_blank" rel="noopener"}

> Starting today, millions living in the UK will receive email invitations to sign up for an e-visa account that will replace their physical immigration documents like Biometric Residence Permits (BRPs). The move is, according to the Home Office, "a key step in creating a modernised and digital border." [...]
