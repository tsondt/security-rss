Title: Shopping platform PandaBuy data leak impacts 1.3 million users
Date: 2024-04-01T11:00:28-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-01-shopping-platform-pandabuy-data-leak-impacts-13-million-users

[Source](https://www.bleepingcomputer.com/news/security/shopping-platform-pandabuy-data-leak-impacts-13-million-users/){:target="_blank" rel="noopener"}

> Data belonging to more than 1.3 million customers of the PandaBuy online shopping platform has been leaked, allegedly after two threat actors exploited multiple vulnerabilities to breach systems. [...]
