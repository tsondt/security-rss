Title: Thousands of LG TVs are vulnerable to takeover—here’s how to ensure yours isn’t one
Date: 2024-04-09T19:12:47+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;hacking;Internet of things;LG;TVs
Slug: 2024-04-09-thousands-of-lg-tvs-are-vulnerable-to-takeoverheres-how-to-ensure-yours-isnt-one

[Source](https://arstechnica.com/?p=2015865){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) As many as 91,000 LG TVs face the risk of being commandeered unless they receive a just-released security update patching four critical vulnerabilities discovered late last year. The vulnerabilities are found in four LG TV models that collectively comprise slightly more than 88,000 units around the world, according to results returned by the Shodan search engine for Internet-connected devices. The vast majority of those units are located in South Korea, followed by Hong Kong, the US, Sweden, and Finland. The models are: LG43UM7000PLA running webOS 4.9.7 - 5.30.40 OLED55CXPUA running webOS 5.5.0 - 04.50.51 OLED48C1PUB running [...]
