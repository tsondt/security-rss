Title: Ransomware gang starts leaking alleged stolen Change Healthcare data
Date: 2024-04-15T17:54:20-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-04-15-ransomware-gang-starts-leaking-alleged-stolen-change-healthcare-data

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-starts-leaking-alleged-stolen-change-healthcare-data/){:target="_blank" rel="noopener"}

> The RansomHub extortion gang has begun leaking what they claim is corporate and patient data stolen from United Health subsidiary Change Healthcare in what has been a long and convoluted extortion process for the company. [...]
