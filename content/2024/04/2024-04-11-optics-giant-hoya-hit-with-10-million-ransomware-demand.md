Title: Optics giant Hoya hit with $10 million ransomware demand
Date: 2024-04-11T14:15:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-11-optics-giant-hoya-hit-with-10-million-ransomware-demand

[Source](https://www.bleepingcomputer.com/news/security/optics-giant-hoya-hit-with-10-million-ransomware-demand/){:target="_blank" rel="noopener"}

> A recent cyberattack on Hoya Corporation was conducted by the 'Hunters International' ransomware operation, which demanded a $10 million ransom for a file decryptor and not to release files stolen during the attack. [...]
