Title: Hackers hijack OpenMetadata apps in Kubernetes cryptomining attacks
Date: 2024-04-17T17:01:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-17-hackers-hijack-openmetadata-apps-in-kubernetes-cryptomining-attacks

[Source](https://www.bleepingcomputer.com/news/security/hackers-hijack-openmetadata-apps-in-kubernetes-cryptomining-attacks/){:target="_blank" rel="noopener"}

> In an ongoing Kubernetes cryptomining campaign, attackers target OpenMetadata workloads using critical remote code execution and authentication vulnerabilities. [...]
