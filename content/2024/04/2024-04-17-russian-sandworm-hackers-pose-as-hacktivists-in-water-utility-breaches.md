Title: Russian Sandworm hackers pose as hacktivists in water utility breaches
Date: 2024-04-17T13:08:59-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-17-russian-sandworm-hackers-pose-as-hacktivists-in-water-utility-breaches

[Source](https://www.bleepingcomputer.com/news/security/russian-sandworm-hackers-pose-as-hacktivists-in-water-utility-breaches/){:target="_blank" rel="noopener"}

> The Sandworm hacking group associated with Russian military intelligence has been hiding attacks and operations behind multiple online personas posing as hacktivist groups. [...]
