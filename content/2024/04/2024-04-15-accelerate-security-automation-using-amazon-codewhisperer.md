Title: Accelerate security automation using Amazon CodeWhisperer
Date: 2024-04-15T13:32:09+00:00
Author: Brendan Jenkins
Category: AWS Security
Tags: Advanced (300);Amazon CodeWhisperer;Amazon EventBridge;AWS Lambda;AWS Security Hub;Security, Identity, & Compliance;Technical How-to;Devops;Security;Security Blog
Slug: 2024-04-15-accelerate-security-automation-using-amazon-codewhisperer

[Source](https://aws.amazon.com/blogs/security/accelerate-security-automation-using-amazon-codewhisperer/){:target="_blank" rel="noopener"}

> In an ever-changing security landscape, teams must be able to quickly remediate security risks. Many organizations look for ways to automate the remediation of security findings that are currently handled manually. Amazon CodeWhisperer is an artificial intelligence (AI) coding companion that generates real-time, single-line or full-function code suggestions in your integrated development environment (IDE) to [...]
