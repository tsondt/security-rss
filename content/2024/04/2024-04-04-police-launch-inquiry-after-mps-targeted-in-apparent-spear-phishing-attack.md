Title: Police launch inquiry after MPs targeted in apparent ‘spear-phishing’ attack
Date: 2024-04-04T17:52:26+00:00
Author: Ben Quinn and Eleni Courea
Category: The Guardian
Tags: Police;Politics;Data and computer security
Slug: 2024-04-04-police-launch-inquiry-after-mps-targeted-in-apparent-spear-phishing-attack

[Source](https://www.theguardian.com/uk-news/2024/apr/04/police-launch-inquiry-after-mps-targeted-in-apparent-spear-phishing-attack){:target="_blank" rel="noopener"}

> At least a dozen people sent suspicious messages, with senior figures suggesting foreign state could be culprit A police investigation has been launched after MPs were apparently targeted in a “spear-phishing” attack, in what security experts believe could be an attempt to compromise parliament. A police force said it had started an inquiry after receiving a complaint from an MP who was sent a number of unsolicited messages last month. Continue reading... [...]
