Title: Reusing passwords: The hidden cost of convenience
Date: 2024-04-10T10:02:04-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2024-04-10-reusing-passwords-the-hidden-cost-of-convenience

[Source](https://www.bleepingcomputer.com/news/security/reusing-passwords-the-hidden-cost-of-convenience/){:target="_blank" rel="noopener"}

> Password reuse might seem like a small problem — but it can have far-reaching consequences for an organization's cybersecurity. Learn more from Specops Software about what IT teams can do to combat the problem. [...]
