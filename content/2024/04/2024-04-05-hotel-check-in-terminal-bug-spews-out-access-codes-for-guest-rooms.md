Title: Hotel check-in terminal bug spews out access codes for guest rooms
Date: 2024-04-05T12:30:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-05-hotel-check-in-terminal-bug-spews-out-access-codes-for-guest-rooms

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/05/hotel_checkin_terminal_bug/){:target="_blank" rel="noopener"}

> Attacks could be completed in seconds, compromising customer safety A self-service check-in terminal used in a German Ibis budget hotel was found leaking hotel room keycodes, and the researcher behind the discovery claims the issue could potentially affect hotels around Europe.... [...]
