Title: Poll Vaulting: Cyber Threats to Global Elections
Date: 2024-04-25T10:00:00+00:00
Author: Mandiant
Category: GCP Security
Tags: Security & Identity;Threat Intelligence
Slug: 2024-04-25-poll-vaulting-cyber-threats-to-global-elections

[Source](https://cloud.google.com/blog/topics/threat-intelligence/cyber-threats-global-elections/){:target="_blank" rel="noopener"}

> Written by: Kelli Vanderlee, Jamie Collier Executive Summary The election cybersecurity landscape globally is characterized by a diversity of targets, tactics, and threats. Elections attract threat activity from a variety of threat actors including: state-sponsored actors, cyber criminals, hacktivists, insiders, and information operations as-a-service entities. Mandiant assesses with high confidence that state-sponsored actors pose the most serious cybersecurity risk to elections. Operations targeting election-related infrastructure can combine cyber intrusion activity, disruptive and destructive capabilities, and information operations, which include elements of public-facing advertisement and amplification of threat activity claims. Successful targeting does not automatically translate to high impact. Many threat [...]
