Title: Health care giant comes clean about recent hack and paid ransom
Date: 2024-04-30T20:44:58+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;Change Healthcare;prescriptions;ransomware
Slug: 2024-04-30-health-care-giant-comes-clean-about-recent-hack-and-paid-ransom

[Source](https://arstechnica.com/?p=2020827){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Change Healthcare, the health care services provider that recently experienced a ransomware attack that hamstrung the US prescription market for two weeks, was hacked through a compromised account that failed to use multifactor authentication, the company CEO told members of Congress. The February 21 attack by a ransomware group using the names ALPHV or BlackCat took down a nationwide network Change Healthcare administers to allow healthcare providers to manage customer payments and insurance claims. With no easy way for pharmacies to calculate what costs were covered by insurance companies, payment processors, providers, and patients experienced long [...]
