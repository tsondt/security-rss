Title: Senior Tory ‘mortified’ after reportedly passing MPs’ data to dating app contact
Date: 2024-04-04T22:52:51+00:00
Author: Nadeem Badshah
Category: The Guardian
Tags: Conservatives;Data and computer security;Politics;UK news
Slug: 2024-04-04-senior-tory-mortified-after-reportedly-passing-mps-data-to-dating-app-contact

[Source](https://www.theguardian.com/politics/2024/apr/04/senior-tory-mortified-after-reportedly-passing-mps-data-to-dating-app-contact){:target="_blank" rel="noopener"}

> William Wragg says he was pressed for colleagues’ details after sharing compromising photos of himself A senior Conservative MP has reportedly admitted to giving out the personal phone numbers of colleagues to a person he met on a dating app. William Wragg told the Times that he gave the information after he had sent intimate pictures of himself, saying he was “scared” and “mortified”. Continue reading... [...]
