Title: What we know about the xz Utils backdoor that almost infected the world
Date: 2024-04-01T06:55:22+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;backdoors;llinux;supply chain attacks
Slug: 2024-04-01-what-we-know-about-the-xz-utils-backdoor-that-almost-infected-the-world

[Source](https://arstechnica.com/?p=2013894){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) On Friday, a lone Microsoft developer rocked the world when he revealed a backdoor had been intentionally planted in xz Utils, an open source data compression utility available on almost all installations of Linux and other Unix-like operating systems. The person or people behind this project likely spent years on it. They were likely very close to seeing the backdoor update merged into Debian and Red Hat, the two biggest distributions of Linux, when an eagle-eyed software developer spotted something fishy. "This might be the best executed supply chain attack we've seen described in the open, [...]
