Title: FCC Fines Major U.S. Wireless Carriers for Selling Customer Location Data
Date: 2024-04-29T20:56:42+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;AT&T;LocationSmart;Securus Technologies;Sprint;T-Mobile;U.S. Federal Communications Commission;Verizon
Slug: 2024-04-29-fcc-fines-major-us-wireless-carriers-for-selling-customer-location-data

[Source](https://krebsonsecurity.com/2024/04/fcc-fines-major-u-s-wireless-carriers-for-selling-customer-location-data/){:target="_blank" rel="noopener"}

> The U.S. Federal Communications Commission (FCC) today levied fines totaling nearly $200 million against the four major carriers — including AT&T, Sprint, T-Mobile and Verizon — for illegally sharing access to customers’ location information without consent. The fines mark the culmination of a more than four-year investigation into the actions of the major carriers. In February 2020, the FCC put all four wireless providers on notice that their practices of sharing access to customer location data were likely violating the law. The FCC said it found the carriers each sold access to its customers’ location information to ‘aggregators,’ who then [...]
