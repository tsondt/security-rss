Title: Whale Song Code
Date: 2024-04-29T11:07:42+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;espionage;history of cryptography;military
Slug: 2024-04-29-whale-song-code

[Source](https://www.schneier.com/blog/archives/2024/04/whale-song-code.html){:target="_blank" rel="noopener"}

> During the Cold War, the US Navy tried to make a secret code out of whale song. The basic plan was to develop coded messages from recordings of whales, dolphins, sea lions, and seals. The submarine would broadcast the noises and a computer—the Combo Signal Recognizer (CSR)—would detect the specific patterns and decode them on the other end. In theory, this idea was relatively simple. As work progressed, the Navy found a number of complicated problems to overcome, the bulk of which centered on the authenticity of the code itself. The message structure couldn’t just substitute the moaning of a [...]
