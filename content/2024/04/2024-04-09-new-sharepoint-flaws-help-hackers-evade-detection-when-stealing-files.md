Title: New SharePoint flaws help hackers evade detection when stealing files
Date: 2024-04-09T09:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-04-09-new-sharepoint-flaws-help-hackers-evade-detection-when-stealing-files

[Source](https://www.bleepingcomputer.com/news/security/new-sharepoint-flaws-help-hackers-evade-detection-when-stealing-files/){:target="_blank" rel="noopener"}

> Researchers have discovered two techniques that could enable attackers to bypass audit logs or generate less severe entries when downloading files from SharePoint. [...]
