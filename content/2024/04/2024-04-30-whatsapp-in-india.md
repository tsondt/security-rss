Title: WhatsApp in India
Date: 2024-04-30T11:00:28+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;encryption;India;Meta;privacy;WhatsApp
Slug: 2024-04-30-whatsapp-in-india

[Source](https://www.schneier.com/blog/archives/2024/04/whatsapp-in-india.html){:target="_blank" rel="noopener"}

> Meta has threatened to pull WhatsApp out of India if the courts try to force it to break its end-to-end encryption. [...]
