Title: No more 12345: devices with weak passwords to be banned in UK
Date: 2024-04-28T23:01:17+00:00
Author: Guardian staff and agency
Category: The Guardian
Tags: Data and computer security;Cybercrime;Technology;UK news
Slug: 2024-04-28-no-more-12345-devices-with-weak-passwords-to-be-banned-in-uk

[Source](https://www.theguardian.com/technology/2024/apr/29/devices-with-weak-passwords-to-be-banned-uk){:target="_blank" rel="noopener"}

> Makers of phones, TVs and smart doorbells legally required to protect devices against access by cybercriminals Tech that comes with weak passwords such as “admin” or “12345” will be banned in the UK under new laws dictating that all smart devices must meet minimum security standards. Measures to protect consumers from hacking and cyber-attacks come into effect on Monday, the Department for Science, Innovation and Technology said. Continue reading... [...]
