Title: Detecting and remediating inactive user accounts with Amazon Cognito
Date: 2024-04-09T19:51:59+00:00
Author: Harun Abdi
Category: AWS Security
Tags: Advanced (300);Amazon Cognito;AWS Identity and Access Management (IAM);Security, Identity, & Compliance;Technical How-to;Amazon DynamoDB;AWS Lambda;Identity;Security Blog
Slug: 2024-04-09-detecting-and-remediating-inactive-user-accounts-with-amazon-cognito

[Source](https://aws.amazon.com/blogs/security/detecting-and-remediating-inactive-user-accounts-with-amazon-cognito/){:target="_blank" rel="noopener"}

> For businesses, particularly those in highly regulated industries, managing user accounts isn’t just a matter of security but also a compliance necessity. In sectors such as finance, healthcare, and government, where regulations often mandate strict control over user access, disabling stale user accounts is a key compliance activity. In this post, we show you a [...]
