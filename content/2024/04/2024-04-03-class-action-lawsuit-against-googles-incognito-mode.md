Title: Class-Action Lawsuit against Google’s Incognito Mode
Date: 2024-04-03T11:01:51+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;browsers;courts;data collection;Google
Slug: 2024-04-03-class-action-lawsuit-against-googles-incognito-mode

[Source](https://www.schneier.com/blog/archives/2024/04/class-action-lawsuit-against-googles-incognito-mode.html){:target="_blank" rel="noopener"}

> The lawsuit has been settled : Google has agreed to delete “billions of data records” the company collected while users browsed the web using Incognito mode, according to documents filed in federal court in San Francisco on Monday. The agreement, part of a settlement in a class action lawsuit filed in 2020, caps off years of disclosures about Google’s practices that shed light on how much data the tech giant siphons from its users­—even when they’re in private-browsing mode. Under the terms of the settlement, Google must further update the Incognito mode “splash page” that appears anytime you open an [...]
