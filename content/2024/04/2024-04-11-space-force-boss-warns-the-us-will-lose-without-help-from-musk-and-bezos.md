Title: Space Force boss warns 'the US will lose' without help from Musk and Bezos
Date: 2024-04-11T23:30:13+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-04-11-space-force-boss-warns-the-us-will-lose-without-help-from-musk-and-bezos

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/11/space_force_us_industry/){:target="_blank" rel="noopener"}

> China, Russia have muscled up, and whoever wins up there wins down here The commander of the US Space Force (USSF) has warned that America risks losing its dominant position in space, and therefore on Earth too.... [...]
