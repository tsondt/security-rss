Title: NSA guy who tried and failed to spy for Russia gets 262 months in the slammer
Date: 2024-04-30T17:01:14+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-04-30-nsa-guy-who-tried-and-failed-to-spy-for-russia-gets-262-months-in-the-slammer

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/30/nsa_employee_guilty_sentence/){:target="_blank" rel="noopener"}

> Tried to sell top secret docs for the low, low price of $85K A former NSA employee has been sentenced to 262 months in prison for attempting to freelance as a Russian spy.... [...]
