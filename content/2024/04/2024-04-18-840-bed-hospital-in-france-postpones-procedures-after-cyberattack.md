Title: 840-bed hospital in France postpones procedures after cyberattack
Date: 2024-04-18T14:29:33-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-04-18-840-bed-hospital-in-france-postpones-procedures-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/chc-sv-hospital-in-france-postpones-procedures-after-cyberattack/){:target="_blank" rel="noopener"}

> The Hospital Simone Veil in Cannes (CHC-SV) has announced that it was targeted by a cyberattack on Tuesday morning, severely impacting its operations and forcing staff to go back to pen and paper. [...]
