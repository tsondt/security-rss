Title: Alleged cryptojacker accused of stealing $3.5M from cloud to mine under $1M in crypto
Date: 2024-04-16T16:31:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-16-alleged-cryptojacker-accused-of-stealing-35m-from-cloud-to-mine-under-1m-in-crypto

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/16/alleged_multimilliondollar_billdodging_cryptojacker_faces/){:target="_blank" rel="noopener"}

> No prizes for guessing the victims A Nebraska man will appear in court today to face charges related to allegations that he defrauded cloud service providers of more than $3.5 million in a long-running cryptojacking scheme.... [...]
