Title: Hackable Intel and Lenovo hardware that went undetected for 5 years won’t ever be fixed
Date: 2024-04-11T18:53:03+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;baseboard management controllers;servers;supply chain
Slug: 2024-04-11-hackable-intel-and-lenovo-hardware-that-went-undetected-for-5-years-wont-ever-be-fixed

[Source](https://arstechnica.com/?p=2016577){:target="_blank" rel="noopener"}

> Enlarge (credit: Intel) Hardware sold for years by the likes of Intel and Lenovo contains a remotely exploitable vulnerability that will never be fixed. The cause: a supply chain snafu involving an open source software package and hardware from multiple manufacturers that directly or indirectly incorporated it into their products. Researchers from security firm Binarly have confirmed that the lapse has resulted in Intel, Lenovo, and Supermicro shipping server hardware that contains a vulnerability that can be exploited to reveal security-critical information. The researchers, however, went on to warn that any hardware that incorporates certain generations of baseboard management controllers [...]
