Title: Six banks share customer info to help Singapore fight money laundering
Date: 2024-04-02T00:59:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-04-02-six-banks-share-customer-info-to-help-singapore-fight-money-laundering

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/02/asia_tech_news_roundup/){:target="_blank" rel="noopener"}

> Plus: Google Cloud ANZ boss departs; Japan revives airliner ambitions; China-linked attackers target Asian entities Asia in brief Singapore's Monetary Authority on Monday launched an application, intuitively named "COllaborative Sharing of Money Laundering/TF Information & Cases" (COSMIC for short, obviously) to target money laundering and terrorism financing.... [...]
