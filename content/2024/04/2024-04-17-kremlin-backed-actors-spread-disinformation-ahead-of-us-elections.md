Title: Kremlin-backed actors spread disinformation ahead of US elections
Date: 2024-04-17T21:55:30+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;disinformation;election influence;election interferance;elections
Slug: 2024-04-17-kremlin-backed-actors-spread-disinformation-ahead-of-us-elections

[Source](https://arstechnica.com/?p=2018090){:target="_blank" rel="noopener"}

> Enlarge (credit: da-kuk/Getty ) Kremlin-backed actors have stepped up efforts to interfere with the US presidential election by planting disinformation and false narratives on social media and fake news sites, analysts with Microsoft reported Wednesday. The analysts have identified several unique influence-peddling groups affiliated with the Russian government seeking to influence the election outcome, with the objective in large part to reduce US support of Ukraine and sow domestic infighting. These groups have so far been less active during the current election cycle than they were during previous ones, likely because of a less contested primary season. Stoking divisions Over [...]
