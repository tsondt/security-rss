Title: Ross Anderson
Date: 2024-04-01T00:21:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptanalysis;cryptography;cybersecurity;economics of security;security conferences;security engineering
Slug: 2024-04-01-ross-anderson

[Source](https://www.schneier.com/blog/archives/2024/03/ross-anderson.html){:target="_blank" rel="noopener"}

> Ross Anderson unexpectedly passed away Thursday night in, I believe, his home in Cambridge. I can’t remember when I first met Ross. Of course it was before 2008, when we created the Security and Human Behavior workshop. It was well before 2001, when we created the Workshop on Economics and Information Security. (Okay, he created both—I helped.) It was before 1998, when we wrote about the problems with key escrow systems. I was one of the people he brought to the Newton Institute for the six-month cryptography residency program he ran (I mistakenly didn’t stay the whole time)—that was in [...]
