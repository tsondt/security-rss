Title: New Lattice Cryptanalytic Technique
Date: 2024-04-15T11:04:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptanalysis;cryptography;quantum cryptography
Slug: 2024-04-15-new-lattice-cryptanalytic-technique

[Source](https://www.schneier.com/blog/archives/2024/04/new-lattice-cryptanalytic-technique.html){:target="_blank" rel="noopener"}

> A new paper presents a polynomial-time quantum algorithm for solving certain hard lattice problems. This could be a big deal for post-quantum cryptographic algorithms, since many of them base their security on hard lattice problems. A few things to note. One, this paper has not yet been peer reviewed. As this comment points out: “We had already some cases where efficient quantum algorithms for lattice problems were discovered, but they turned out not being correct or only worked for simple special cases.” Two, this is a quantum algorithm, which means that it has not been tested. There is a wide [...]
