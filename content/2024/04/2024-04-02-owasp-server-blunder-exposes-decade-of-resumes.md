Title: OWASP server blunder exposes decade of resumes
Date: 2024-04-02T18:30:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-02-owasp-server-blunder-exposes-decade-of-resumes

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/02/owasp_discloses_data_breach/){:target="_blank" rel="noopener"}

> Irony alerts: Open Web Application Security Project Foundation suffers lapse A misconfigured MediaWiki web server allowed digital snoops to access members' resumes containing their personal details at the Open Web Application Security Project (OWASP) Foundation.... [...]
