Title: UK lays down fresh legislation banning crummy default device passwords
Date: 2024-04-29T11:45:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-29-uk-lays-down-fresh-legislation-banning-crummy-default-device-passwords

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/29/uk_lays_password_legislation/){:target="_blank" rel="noopener"}

> New laws mean vendors need to make clear how long you'll get updates too Smart device manufacturers will have to play by new rules in the UK as of today, with laws coming into force to make it more difficult for cybercriminals to break into hardware such as phones and tablets.... [...]
