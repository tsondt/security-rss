Title: New Ivanti RCE flaw may impact 16,000 exposed VPN gateways
Date: 2024-04-05T13:40:01-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-05-new-ivanti-rce-flaw-may-impact-16000-exposed-vpn-gateways

[Source](https://www.bleepingcomputer.com/news/security/new-ivanti-rce-flaw-may-impact-16-000-exposed-vpn-gateways/){:target="_blank" rel="noopener"}

> Approximately 16,500 Ivanti Connect Secure and Poly Secure gateways exposed on the internet are likely vulnerable to a remote code execution (RCE) flaw the vendor addressed earlier this week. [...]
