Title: Happy 20th birthday Gmail, you're mostly grown up – now fix the spam
Date: 2024-04-02T09:27:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-04-02-happy-20th-birthday-gmail-youre-mostly-grown-up-now-fix-the-spam

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/02/gmail_turns_20/){:target="_blank" rel="noopener"}

> Senders of more than 5K messages a day are in the crosshairs It was 20 years ago on Monday that Google unleashed Gmail on the world, and the chocolate factory is celebrating with new rules that just might, hopefully, cut down on the amount of spam users receive.... [...]
