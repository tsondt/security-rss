Title: ‘The Manipulaters’ Improve Phishing, Still Fail at Opsec
Date: 2024-04-03T13:16:25+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Ne'er-Do-Well News;Web Fraud 2.0;bluebtcus@gmail.com;DomainTools.com;FudCo;Fudpage;Fudsender;Fudtools;HeartSender;Saim Raza;The Manipulaters;We Code Solutions
Slug: 2024-04-03-the-manipulaters-improve-phishing-still-fail-at-opsec

[Source](https://krebsonsecurity.com/2024/04/the-manipulaters-improve-phishing-still-fail-at-opsec/){:target="_blank" rel="noopener"}

> Roughly nine years ago, KrebsOnSecurity profiled a Pakistan-based cybercrime group called “ The Manipulaters,” a sprawling web hosting network of phishing and spam delivery platforms. In January 2024, The Manipulaters pleaded with this author to unpublish previous stories about their work, claiming the group had turned over a new leaf and gone legitimate. But new research suggests that while they have improved the quality of their products and services, these nitwits still fail spectacularly at hiding their illegal activities. In May 2015, KrebsOnSecurity published a brief writeup about the brazen Manipulaters team, noting that they openly operated hundreds of web [...]
