Title: Change the law on computer evidence with an amendment to data protection bill | Letter
Date: 2024-04-22T17:25:20+00:00
Author: Guardian Staff
Category: The Guardian
Tags: Post Office Horizon scandal;Post Office;Law;Data and computer security
Slug: 2024-04-22-change-the-law-on-computer-evidence-with-an-amendment-to-data-protection-bill-letter

[Source](https://www.theguardian.com/uk-news/2024/apr/22/change-the-law-on-computer-evidence-with-an-amendment-to-data-protection-bill){:target="_blank" rel="noopener"}

> A group of software experts and barristers who have been supporting the subpostmasters affected by the Post Office Horizon miscarriages of justice call for changes to the bill going through the House of Lords It is now clear that the Post Office was advised by its lawyers to delay disclosing some evidence that would help subpostmasters ( Post Office was urged by external lawyers to ‘suppress’ key document, inquiry hears, 18 April ). Failure to disclose vital evidence about the defects in the Horizon IT system led to appalling injustice. We suggest that the data protection and digital information bill [...]
