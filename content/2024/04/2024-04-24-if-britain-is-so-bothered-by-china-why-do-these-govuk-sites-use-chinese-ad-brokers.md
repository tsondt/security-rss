Title: If Britain is so bothered by China, why do these .gov.uk sites use Chinese ad brokers?
Date: 2024-04-24T07:29:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-04-24-if-britain-is-so-bothered-by-china-why-do-these-govuk-sites-use-chinese-ad-brokers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/24/ads_on_gov_uk_websites/){:target="_blank" rel="noopener"}

> One wonders why are there adverts on public-sector portals at all Exclusive At least 18 public-sector websites in the UK and US send visitor data in some form to various web advertising brokers – including an ad-tech biz in China involved in past privacy controversies, a security firm claims.... [...]
