Title: Microsoft April 2024 Patch Tuesday fixes 150 security flaws, 67 RCEs
Date: 2024-04-09T13:34:48-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-04-09-microsoft-april-2024-patch-tuesday-fixes-150-security-flaws-67-rces

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-april-2024-patch-tuesday-fixes-150-security-flaws-67-rces/){:target="_blank" rel="noopener"}

> Today is Microsoft's April 2024 Patch Tuesday, which includes security updates for 150 flaws and sixty-seven remote code execution bugs. [...]
