Title: MITRE says state hackers breached its network via Ivanti zero-days
Date: 2024-04-19T15:02:09-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-19-mitre-says-state-hackers-breached-its-network-via-ivanti-zero-days

[Source](https://www.bleepingcomputer.com/news/security/mitre-says-state-hackers-breached-its-network-via-ivanti-zero-days/){:target="_blank" rel="noopener"}

> The MITRE Corporation says a state-backed hacking group breached its systems in January 2024 by chaining two Ivanti VPN zero-days. [...]
