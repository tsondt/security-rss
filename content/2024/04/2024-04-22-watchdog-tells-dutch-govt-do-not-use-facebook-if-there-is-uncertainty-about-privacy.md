Title: Watchdog tells Dutch govt: 'Do not use Facebook if there is uncertainty about privacy'
Date: 2024-04-22T14:00:15+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-04-22-watchdog-tells-dutch-govt-do-not-use-facebook-if-there-is-uncertainty-about-privacy

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/22/meta_facing_dutch_government_departure/){:target="_blank" rel="noopener"}

> Meta insists it's just misunderstood and it's safe to talk to citizens over FB The Dutch Data Protection Authority (AP) has warned that government organizations should not use Facebook to communicate with the country's citizens unless they can guarantee the privacy of data.... [...]
