Title: World's second-largest eyeglass lens-maker blinded by infosec incident
Date: 2024-04-05T01:45:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-04-05-worlds-second-largest-eyeglass-lens-maker-blinded-by-infosec-incident

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/05/hoya_infosec_incident/){:target="_blank" rel="noopener"}

> Japan's Hoya also makes components for chips, displays, and hard disks, and has spent four days groping for a fix If ever there was an incident that brings the need for good infosec into sharp focus, this is the one: Japan's Hoya – a maker of eyeglass and contact lenses, plus kit used to make semiconductor manufacturing, flat panel displays, and hard disk drives – has halted some production and sales activity after experiencing an attack on its IT systems.... [...]
