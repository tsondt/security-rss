Title: Microsoft blamed for “a cascade of security failures” in Exchange breach report
Date: 2024-04-03T18:51:54+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;Security;audit logging;china;CISA;crash dump;csrb;microsoft;Microsoft Azure;Microsoft Exchange;nation-state actors;storm-0558
Slug: 2024-04-03-microsoft-blamed-for-a-cascade-of-security-failures-in-exchange-breach-report

[Source](https://arstechnica.com/?p=2014535){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) A federal Cyber Safety Review Board has issued its report on what led to last summer's capture of hundreds of thousands of emails by Chinese hackers from cloud customers, including federal agencies. It cites "a cascade of security failures at Microsoft" and finds that "Microsoft's security culture was inadequate" and needs to adjust to a "new normal" of cloud provider targeting. The report, mandated by President Biden in the wake of the far-reaching intrusion, details the steps that Microsoft took before, during, and after the breach and in each case finds critical failure. The breach was [...]
