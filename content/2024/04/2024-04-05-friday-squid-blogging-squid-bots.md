Title: Friday Squid Blogging: SqUID Bots
Date: 2024-04-05T21:02:11+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;robotics;squid
Slug: 2024-04-05-friday-squid-blogging-squid-bots

[Source](https://www.schneier.com/blog/archives/2024/04/friday-squid-blogging-squid-bots.html){:target="_blank" rel="noopener"}

> They’re AI warehouse robots. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
