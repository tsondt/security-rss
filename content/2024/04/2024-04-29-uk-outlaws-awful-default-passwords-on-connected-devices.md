Title: UK outlaws awful default passwords on connected devices
Date: 2024-04-29T19:45:16+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;Policy;Security;Tech;connected devices;DDoS;iot;iot passwords;passwords;smart home
Slug: 2024-04-29-uk-outlaws-awful-default-passwords-on-connected-devices

[Source](https://arstechnica.com/?p=2020491){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) If you build a gadget that connects to the Internet and sell it in the United Kingdom, you can no longer make the default password "password." In fact, you're not supposed to have default passwords at all. A new version of the 2022 Product Security and Telecommunications Infrastructure Act (PTSI) is now in effect, covering just about everything that a consumer can buy that connects to the web. Under the guidelines, even the tiniest Wi-Fi board must either have a randomized password or else generate a password upon initialization (through a smartphone app or other means). [...]
