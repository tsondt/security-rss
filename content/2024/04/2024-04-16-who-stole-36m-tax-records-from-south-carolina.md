Title: Who Stole 3.6M Tax Records from South Carolina?
Date: 2024-04-16T11:26:55+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Breadcrumbs;Data Breaches;Ne'er-Do-Well News;Tax Refund Fraud;Aleksandr Ermakov;Associated Press;Embargo;Home Depot breach;Jeffrey Collins;Mark Keel;Mazafaka;Mikhail Shefel;Nikki Haley;rescator;Shtazi;target breach;tax refund fraud;tax return fraud;The Post and Courier;U.S. Internal Revenue Service;Verified
Slug: 2024-04-16-who-stole-36m-tax-records-from-south-carolina

[Source](https://krebsonsecurity.com/2024/04/who-stole-3-6m-tax-records-from-south-carolina/){:target="_blank" rel="noopener"}

> For nearly a dozen years, residents of South Carolina have been kept in the dark by state and federal investigators over who was responsible for hacking into the state’s revenue department in 2012 and stealing tax and bank account information for 3.6 million people. The answer may no longer be a mystery: KrebsOnSecurity found compelling clues suggesting the intrusion was carried out by the same Russian hacking crew that stole of millions of payment card records from big box retailers like Home Depot and Target in the years that followed. Questions about who stole tax and financial data on roughly [...]
