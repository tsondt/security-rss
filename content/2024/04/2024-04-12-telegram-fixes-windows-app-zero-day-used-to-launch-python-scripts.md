Title: Telegram fixes Windows app zero-day used to launch Python scripts
Date: 2024-04-12T14:46:27-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-04-12-telegram-fixes-windows-app-zero-day-used-to-launch-python-scripts

[Source](https://www.bleepingcomputer.com/news/security/telegram-fixes-windows-app-zero-day-used-to-launch-python-scripts/){:target="_blank" rel="noopener"}

> Telegram fixed a zero-day vulnerability in its Windows desktop application that could be used to bypass security warnings and automatically launch Python scripts. [...]
