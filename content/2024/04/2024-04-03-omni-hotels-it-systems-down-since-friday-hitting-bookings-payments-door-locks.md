Title: Omni Hotels IT systems down since Friday, hitting bookings, payments, door locks
Date: 2024-04-03T19:28:15+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-03-omni-hotels-it-systems-down-since-friday-hitting-bookings-payments-door-locks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/03/omni_hotels_it_outage/){:target="_blank" rel="noopener"}

> As WhatsApp, Facebook Messenger, other Meta bits fall offline today Updated Luxury resort chain Omni Hotels & Resorts has had its computer systems knocked offline since Friday in what it has described as a "disruption," though sounds a lot like the MGM Resorts ransomware infection over the summer.... [...]
