Title: Over 90,000 LG Smart TVs may be exposed to remote attacks
Date: 2024-04-09T09:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-09-over-90000-lg-smart-tvs-may-be-exposed-to-remote-attacks

[Source](https://www.bleepingcomputer.com/news/security/over-90-000-lg-smart-tvs-may-be-exposed-to-remote-attacks/){:target="_blank" rel="noopener"}

> Security researchers at Bitdefender have discovered four vulnerabilities impacting multiple versions of WebOS, the operating system used in LG smart TVs. [...]
