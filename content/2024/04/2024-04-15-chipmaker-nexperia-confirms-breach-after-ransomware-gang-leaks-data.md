Title: Chipmaker Nexperia confirms breach after ransomware gang leaks data
Date: 2024-04-15T12:00:01-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-15-chipmaker-nexperia-confirms-breach-after-ransomware-gang-leaks-data

[Source](https://www.bleepingcomputer.com/news/security/chipmaker-nexperia-confirms-breach-after-ransomware-gang-leaks-data/){:target="_blank" rel="noopener"}

> Dutch chipmaker Nexperia confirmed late last week that hackers breached its network in March 2024 after a ransomware gang leaked samples of allegedly stolen data. [...]
