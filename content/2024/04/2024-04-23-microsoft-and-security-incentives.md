Title: Microsoft and Security Incentives
Date: 2024-04-23T11:09:31+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;incentives;Microsoft;national security policy
Slug: 2024-04-23-microsoft-and-security-incentives

[Source](https://www.schneier.com/blog/archives/2024/04/microsoft-and-security-incentives.html){:target="_blank" rel="noopener"}

> Former senior White House cyber policy director A. J. Grotto talks about the economic incentives for companies to improve their security—in particular, Microsoft: Grotto told us Microsoft had to be “dragged kicking and screaming” to provide logging capabilities to the government by default, and given the fact the mega-corp banked around $20 billion in revenue from security services last year, the concession was minimal at best. [...] “The government needs to focus on encouraging and catalyzing competition,” Grotto said. He believes it also needs to publicly scrutinize Microsoft and make sure everyone knows when it messes up. “At the end [...]
