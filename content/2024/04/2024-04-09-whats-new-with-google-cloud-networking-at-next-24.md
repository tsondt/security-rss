Title: What’s new with Google Cloud Networking at Next ’24
Date: 2024-04-09T12:00:00+00:00
Author: Muninder Sambi
Category: GCP Security
Tags: Security & Identity;Google Cloud Next;Networking
Slug: 2024-04-09-whats-new-with-google-cloud-networking-at-next-24

[Source](https://cloud.google.com/blog/products/networking/whats-new-for-networking-at-next24/){:target="_blank" rel="noopener"}

> Cross-Cloud Network has transformed how organizations connect and secure workloads across hybrid and multi-cloud networks. It simplifies complexity, strengthens security posture, and helps deliver faster business outcomes. Built on Google Cloud’s planet-scale network, Cross-Cloud Network enables you to deliver rich experiences, streamline operational efficiency, and lower TCO. AI adoption and growth are driving a major inflection point in networking, requiring higher performance, scale, intelligence, and security. Foundation models such as large language models (LLMs) fuel a step-function improvement in performing a number of network operations such as design, secure, optimize and troubleshoot. With Cross-Cloud Network, you can train and inference [...]
