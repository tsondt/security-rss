Title: Introducing Chrome Enterprise Premium: The future of endpoint security
Date: 2024-04-09T12:00:00+00:00
Author: Parisa Tabriz
Category: GCP Security
Tags: Google Cloud Next;Security & Identity
Slug: 2024-04-09-introducing-chrome-enterprise-premium-the-future-of-endpoint-security

[Source](https://cloud.google.com/blog/products/identity-security/introducing-chrome-enterprise-premium/){:target="_blank" rel="noopener"}

> Today at Google Cloud Next, we are announcing a new frontline of defense for organizations: Chrome Enterprise Premium, an offering that can help simplify and strengthen endpoint security. With Chrome Enterprise Premium, the latest evolution of the world’s most trusted enterprise browser, hundreds of millions of enterprise users can be further protected. The new endpoint Browsers are more than just a portal to the Internet: They are the new endpoint where almost every high-value activity and interaction in the enterprise takes place. Authentication, access, communication and collaboration, administration, and even coding are all browser-based activities in the modern enterprise. Endpoint [...]
