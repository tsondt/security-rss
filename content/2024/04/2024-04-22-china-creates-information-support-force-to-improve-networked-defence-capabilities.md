Title: China creates 'Information Support Force' to improve networked defence capabilities
Date: 2024-04-22T03:15:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-04-22-china-creates-information-support-force-to-improve-networked-defence-capabilities

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/22/china_information_support_force/){:target="_blank" rel="noopener"}

> A day after FBI boss warns Beijing is poised to strike against US infrastructure China last week reorganized its military to create an Information Support Force aimed at ensuring it can fight and win networked wars.... [...]
