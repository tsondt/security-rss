Title: Long Article on GM Spying on Its Cars’ Drivers
Date: 2024-04-26T11:01:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;insurance;privacy;surveillance
Slug: 2024-04-26-long-article-on-gm-spying-on-its-cars-drivers

[Source](https://www.schneier.com/blog/archives/2024/04/long-article-on-gm-spying-on-its-cars-drivers.html){:target="_blank" rel="noopener"}

> Kashmir Hill has a really good article on how GM tricked its drivers into letting it spy on them—and then sold that data to insurance companies. [...]
