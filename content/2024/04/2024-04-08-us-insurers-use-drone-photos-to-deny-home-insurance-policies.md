Title: US insurers use drone photos to deny home insurance policies
Date: 2024-04-08T20:30:08+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-04-08-us-insurers-use-drone-photos-to-deny-home-insurance-policies

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/08/us_insurers_drones/){:target="_blank" rel="noopener"}

> Of course, it helps if you don't live in a potential disaster zone US insurance companies are reportedly relying on aerial photos from drones to deny claims.... [...]
