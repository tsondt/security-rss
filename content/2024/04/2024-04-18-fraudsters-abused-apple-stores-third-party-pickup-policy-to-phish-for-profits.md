Title: Fraudsters abused Apple Stores' third-party pickup policy to phish for profits
Date: 2024-04-18T16:00:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-04-18-fraudsters-abused-apple-stores-third-party-pickup-policy-to-phish-for-profits

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/18/blackhat_apple_korea/){:target="_blank" rel="noopener"}

> Scam prevalent across Korea and Japan actually had some winners Black Hat Asia Speaking at the Black Hat Asia conference on Thursday, a Korean researcher revealed how the discovery of a phishing operation led to the exposure of a criminal operation that used stolen credit cards and second-hand stores to make money by abusing Apple Stores’ practice of letting third parties pick up purchases.... [...]
