Title: New SteganoAmor attacks use steganography to target 320 orgs globally
Date: 2024-04-15T16:31:28-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-15-new-steganoamor-attacks-use-steganography-to-target-320-orgs-globally

[Source](https://www.bleepingcomputer.com/news/security/new-steganoamor-attacks-use-steganography-to-target-320-orgs-globally/){:target="_blank" rel="noopener"}

> A new campaign conducted by the TA558 hacking group is concealing malicious code inside images using steganography to deliver various malware tools onto targeted systems. [...]
