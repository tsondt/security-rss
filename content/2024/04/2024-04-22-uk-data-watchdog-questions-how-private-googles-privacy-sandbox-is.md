Title: UK data watchdog questions how private Google's Privacy Sandbox is
Date: 2024-04-22T11:13:15+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-04-22-uk-data-watchdog-questions-how-private-googles-privacy-sandbox-is

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/22/ico_google_privacy_sandbox/){:target="_blank" rel="noopener"}

> Leaked draft report says stated goals still come up short Google's Privacy Sandbox, which aspires to provide privacy-preserving ad targeting and analytics, still isn't sufficiently private.... [...]
