Title: AT&T: Data breach affects 73 million or 51 million customers. No, we won’t explain.
Date: 2024-04-10T22:28:02+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;AT&T;Data breaches;Identity theft
Slug: 2024-04-10-att-data-breach-affects-73-million-or-51-million-customers-no-we-wont-explain

[Source](https://arstechnica.com/?p=2016342){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) AT&T is notifying millions of current or former customers that their account data has been compromised and published last month on the dark web. Just how many millions, the company isn't saying. In a mandatory filing with the Maine Attorney General’s office, the telecommunications company said 51.2 million account holders were affected. On its corporate website, AT&T put the number at 73 million. In either event, compromised data included one or more of the following: full names, email addresses, mailing addresses, phone numbers, social security numbers, dates of birth, AT&T account numbers, and AT&T passcodes. Personal [...]
