Title: Cybercriminals threaten to leak all 5 million records from stolen database of high-risk individuals
Date: 2024-04-19T11:28:46+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-19-cybercriminals-threaten-to-leak-all-5-million-records-from-stolen-database-of-high-risk-individuals

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/19/cybercriminals_threaten_to_leak_all/){:target="_blank" rel="noopener"}

> It’s the second time the World-Check list has fallen into the wrong hands The World-Check database used by businesses to verify the trustworthiness of users has fallen into the hands of cybercriminals.... [...]
