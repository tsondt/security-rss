Title: Dan Solove on Privacy Regulation
Date: 2024-04-24T11:05:29+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;GDPR;privacy
Slug: 2024-04-24-dan-solove-on-privacy-regulation

[Source](https://www.schneier.com/blog/archives/2024/04/dan-solove-on-privacy-regulation.html){:target="_blank" rel="noopener"}

> Law professor Dan Solove has a new article on privacy regulation. In his email to me, he writes: “I’ve been pondering privacy consent for more than a decade, and I think I finally made a breakthrough with this article.” His mini-abstract: In this Article I argue that most of the time, privacy consent is fictitious. Instead of futile efforts to try to turn privacy consent from fiction to fact, the better approach is to lean into the fictions. The law can’t stop privacy consent from being a fairy tale, but the law can ensure that the story ends well. I [...]
