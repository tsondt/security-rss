Title: Friday Squid Blogging: The Awfulness of Squid Fishing Boats
Date: 2024-04-12T21:08:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-04-12-friday-squid-blogging-the-awfulness-of-squid-fishing-boats

[Source](https://www.schneier.com/blog/archives/2024/04/friday-squid-blogging-the-awfulness-of-squid-fishing-boats.html){:target="_blank" rel="noopener"}

> It’s a pretty awful story. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
