Title: DuckDuckGo launches a premium Privacy Pro VPN service
Date: 2024-04-11T08:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2024-04-11-duckduckgo-launches-a-premium-privacy-pro-vpn-service

[Source](https://www.bleepingcomputer.com/news/security/duckduckgo-launches-a-premium-privacy-pro-vpn-service/){:target="_blank" rel="noopener"}

> DuckDuckGo has launched a new paid-for 3-in-1 subscription service called 'Privacy Pro,' which includes a virtual private network (VPN), a personal data removal service, and an identity theft restoration solution. [...]
