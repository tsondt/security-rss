Title: Delinea Secret Server customers should apply latest patches
Date: 2024-04-15T14:00:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-15-delinea-secret-server-customers-should-apply-latest-patches

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/15/delinea_secret_server_patch/){:target="_blank" rel="noopener"}

> Attackers could nab an org's most sensitive keys if left unaddressed Updated Customers of Delinea's Secret Server are being urged to upgrade their installations "immediately" after a researcher claimed a critical vulnerability could allow attackers to gain admin-level access.... [...]
