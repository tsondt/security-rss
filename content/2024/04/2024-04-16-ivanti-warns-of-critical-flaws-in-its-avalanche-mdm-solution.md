Title: Ivanti warns of critical flaws in its Avalanche MDM solution
Date: 2024-04-16T15:52:14-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-16-ivanti-warns-of-critical-flaws-in-its-avalanche-mdm-solution

[Source](https://www.bleepingcomputer.com/news/security/ivanti-warns-of-critical-flaws-in-its-avalanche-mdm-solution/){:target="_blank" rel="noopener"}

> Ivanti has released security updates to fix 27 vulnerabilities in its Avalanche mobile device management (MDM) solution, two of them critical heap overflows that can be exploited for remote command execution. [...]
