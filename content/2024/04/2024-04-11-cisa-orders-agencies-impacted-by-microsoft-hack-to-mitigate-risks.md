Title: CISA orders agencies impacted by Microsoft hack to mitigate risks
Date: 2024-04-11T13:47:19-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-04-11-cisa-orders-agencies-impacted-by-microsoft-hack-to-mitigate-risks

[Source](https://www.bleepingcomputer.com/news/security/cisa-orders-agencies-impacted-by-microsoft-hack-to-mitigate-risks/){:target="_blank" rel="noopener"}

> CISA has issued a new emergency directive ordering U.S. federal agencies to address risks resulting from the breach of multiple Microsoft corporate email accounts by the Russian APT29 hacking group. [...]
