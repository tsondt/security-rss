Title: Has the ever-present cyber danger just got worse?
Date: 2024-04-22T10:59:13+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2024-04-22-has-the-ever-present-cyber-danger-just-got-worse

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/22/has_the_everpresent_cyber_danger/){:target="_blank" rel="noopener"}

> Facing down the triple threat of ransomware, data breaches and criminal extortion Sponsored On the face of it, there really isn't much of an upside for the current UK government after MPs described its response to attacks by cyber-espionage group APT31 as 'feeble, derisory and sadly insufficient.'... [...]
