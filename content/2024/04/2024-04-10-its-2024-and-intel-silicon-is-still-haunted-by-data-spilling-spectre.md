Title: It's 2024 and Intel silicon is still haunted by data-spilling Spectre
Date: 2024-04-10T20:22:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-10-its-2024-and-intel-silicon-is-still-haunted-by-data-spilling-spectre

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/10/intel_cpus_native_spectre_attacks/){:target="_blank" rel="noopener"}

> Go, go InSpectre Gadget Intel CPU cores remain vulnerable to Spectre data-leaking attacks, say academics at VU Amsterdam.... [...]
