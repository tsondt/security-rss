Title: France willing to buy key Atos assets to keep them French
Date: 2024-04-29T13:00:12+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2024-04-29-france-willing-to-buy-key-atos-assets-to-keep-them-french

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/29/france_buy_atos_assets/){:target="_blank" rel="noopener"}

> Finance minister says government has interests in IT giant's 'sovereign activities' The French government has tabled an offer to buy key assets of ailing IT giant Atos after the company late last week almost doubled its estimate of the cash it will need to stay afloat in the near future.... [...]
