Title: Other Attempts to Take Over Open Source Projects
Date: 2024-04-18T11:06:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;backdoors;open source;social engineering
Slug: 2024-04-18-other-attempts-to-take-over-open-source-projects

[Source](https://www.schneier.com/blog/archives/2024/04/other-attempts-to-take-over-open-source-projects.html){:target="_blank" rel="noopener"}

> After the XZ Utils discovery, people have been examining other open-source projects. Surprising no one, the incident is not unique: The OpenJS Foundation Cross Project Council received a suspicious series of emails with similar messages, bearing different names and overlapping GitHub-associated emails. These emails implored OpenJS to take action to update one of its popular JavaScript projects to “address any critical vulnerabilities,” yet cited no specifics. The email author(s) wanted OpenJS to designate them as a new maintainer of the project despite having little prior involvement. This approach bears strong resemblance to the manner in which “Jia Tan” positioned themselves [...]
