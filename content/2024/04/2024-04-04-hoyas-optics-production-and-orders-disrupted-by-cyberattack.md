Title: Hoya’s optics production and orders disrupted by cyberattack
Date: 2024-04-04T13:22:40-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-04-04-hoyas-optics-production-and-orders-disrupted-by-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/hoyas-optics-production-and-orders-disrupted-by-cyberattack/){:target="_blank" rel="noopener"}

> Hoya Corporation, one of the largest global manufacturers of optical products, says a "system failure" caused servers at some of its production plants and business divisions to go offline on Saturday. [...]
