Title: Kaiser Permanente handed over 13.4M people's data to Microsoft, Google, others
Date: 2024-04-26T18:14:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-04-26-kaiser-permanente-handed-over-134m-peoples-data-to-microsoft-google-others

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/26/kaiser_patient_data/){:target="_blank" rel="noopener"}

> Ouch! Millions of Kaiser Permanente patients' data was likely handed over to Google, Microsoft Bing, X/Twitter, and other third-parties, according to the American healthcare giant.... [...]
