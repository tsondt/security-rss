Title: William Wragg resigns from two Commons roles after divulging MPs’ phone numbers
Date: 2024-04-08T22:37:24+00:00
Author: Nadeem Badshah
Category: The Guardian
Tags: House of Commons;Conservatives;Data and computer security;WhatsApp;Politics
Slug: 2024-04-08-william-wragg-resigns-from-two-commons-roles-after-divulging-mps-phone-numbers

[Source](https://www.theguardian.com/politics/2024/apr/08/william-wragg-resigns-from-two-commons-roles-after-divulging-mps-phone-numbers){:target="_blank" rel="noopener"}

> Tory MP resigns committee roles after apology for role in parliamentary sexting scandal The Conservative MP who divulged colleagues’ personal phone numbers to someone he met on a dating app as part of a parliamentary sexting scandal has stepped down from two Commons roles, it has been reported. William Wragg has resigned as chair of the Commons’ public administration and constitutional affairs committee and also quit his post as the vice-chair of the 1922 Committee of Conservative backbenchers after admitting to giving the information to a man he met, according to reports. Continue reading... [...]
