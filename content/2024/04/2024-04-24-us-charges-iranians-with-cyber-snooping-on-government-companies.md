Title: US charges Iranians with cyber snooping on government, companies
Date: 2024-04-24T14:01:56+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-24-us-charges-iranians-with-cyber-snooping-on-government-companies

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/24/iranians_charged_cyber_espionage/){:target="_blank" rel="noopener"}

> Their holiday options are now far more restricted The US has charged and sanctioned four Iranian nationals for their alleged roles in various attacks on US companies and government departments, all of whom are claimed to have worked for fake companies linked to Iran's military.... [...]
