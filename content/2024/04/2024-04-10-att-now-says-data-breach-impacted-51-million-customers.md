Title: AT&T now says data breach impacted 51 million customers
Date: 2024-04-10T10:18:41-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-10-att-now-says-data-breach-impacted-51-million-customers

[Source](https://www.bleepingcomputer.com/news/security/att-now-says-data-breach-impacted-51-million-customers/){:target="_blank" rel="noopener"}

> AT&T is notifying 51 million former and current customers, warning them of a data breach that exposed their personal information on a hacking forum. However, the company has still not disclosed how the data was obtained. [...]
