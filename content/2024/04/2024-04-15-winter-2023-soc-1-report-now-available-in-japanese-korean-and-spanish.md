Title: Winter 2023 SOC 1 report now available in Japanese, Korean, and Spanish
Date: 2024-04-15T16:22:51+00:00
Author: Brownell Combs
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS SOC Reports;Compliance;Security Blog;SOC
Slug: 2024-04-15-winter-2023-soc-1-report-now-available-in-japanese-korean-and-spanish

[Source](https://aws.amazon.com/blogs/security/winter-2023-soc-1-report-now-available-in-japanese-korean-and-spanish/){:target="_blank" rel="noopener"}

> Japanese | Korean | Spanish We continue to listen to our customers, regulators, and stakeholders to understand their needs regarding audit, assurance, certification, and attestation programs at Amazon Web Services (AWS). We are pleased to announce that for the first time an AWS System and Organization Controls (SOC) 1 report is now available in Japanese [...]
