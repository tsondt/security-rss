Title: 15 must-attend security sessions at Next '24
Date: 2024-04-01T16:00:00+00:00
Author: Ruchika Mishra
Category: GCP Security
Tags: Security & Identity
Slug: 2024-04-01-15-must-attend-security-sessions-at-next-24

[Source](https://cloud.google.com/blog/products/identity-security/15-must-attend-security-sessions-at-next24/){:target="_blank" rel="noopener"}

> Google Cloud Next, our global exhibition of inspiration, innovation, and education, is coming to the Mandalay Bay Convention Center in Las Vegas starting Tuesday, April 9. We’re featuring a packed Security Professionals track, including more than 40 breakout sessions, demos, and lightning talks where you can discover new ways to secure your cloud workloads, get the latest in threat intelligence, and learn how AI is evolving security practices. Learn from an exciting lineup of speakers, including experts from Google Cloud and leaders from Charles Schwab, L’Oreal, Snap, Fiserv, Spotify, and Electronic Arts. Register here and get ready to defend your [...]
