Title: New Chrome feature aims to stop hackers from using stolen cookies
Date: 2024-04-02T14:08:42-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2024-04-02-new-chrome-feature-aims-to-stop-hackers-from-using-stolen-cookies

[Source](https://www.bleepingcomputer.com/news/security/new-chrome-feature-aims-to-stop-hackers-from-using-stolen-cookies/){:target="_blank" rel="noopener"}

> Google announced a new Chrome security feature called 'Device Bound Session Credentials' that ties cookies to a specific device, blocking hackers from stealing and using them to hijack users' accounts. [...]
