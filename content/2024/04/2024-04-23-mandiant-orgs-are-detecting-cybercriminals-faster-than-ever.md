Title: Mandiant: Orgs are detecting cybercriminals faster than ever
Date: 2024-04-23T13:05:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-04-23-mandiant-orgs-are-detecting-cybercriminals-faster-than-ever

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/23/mandiant_orgs_are_detecting_cybercrims/){:target="_blank" rel="noopener"}

> The 'big victory for the good guys' shouldn't be celebrated too much, though The average time taken by global organizations to detect cyberattacks has dropped to its lowest-ever level of ten days, Mandiant revealed today.... [...]
