Title: US govt sanctions Iranians linked to government cyberattacks
Date: 2024-04-23T14:40:25-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Government
Slug: 2024-04-23-us-govt-sanctions-iranians-linked-to-government-cyberattacks

[Source](https://www.bleepingcomputer.com/news/security/us-govt-sanctions-iranians-linked-to-government-cyberattacks/){:target="_blank" rel="noopener"}

> The Treasury Department's Office of Foreign Assets Control (OFAC) has sanctioned four Iranian nationals for their involvement in cyberattacks against the U.S. government, defense contractors, and private companies. [...]
