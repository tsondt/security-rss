Title: Hosting firm's VMware ESXi servers hit by new SEXi ransomware
Date: 2024-04-03T17:58:18-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-04-03-hosting-firms-vmware-esxi-servers-hit-by-new-sexi-ransomware

[Source](https://www.bleepingcomputer.com/news/security/hosting-firms-vmware-esxi-servers-hit-by-new-sexi-ransomware/){:target="_blank" rel="noopener"}

> Chilean data center and hosting provider IxMetro Powerhost has suffered a cyberattack at the hands of a new ransomware gang known as SEXi, which encrypted the company's VMware ESXi servers and backups. [...]
