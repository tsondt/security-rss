Title: Australia’s spies and cops want ‘accountable encryption’ - aka access to backdoors
Date: 2024-04-25T00:29:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-04-25-australias-spies-and-cops-want-accountable-encryption-aka-access-to-backdoors

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/25/asio_afp_accountable_encryption/){:target="_blank" rel="noopener"}

> And warn that AI is already being used by extremists to plot attacks The director general of Australia’s lead intelligence agency and the commissioner of its Federal Police yesterday both called for social networks to offer more assistance to help their investigators work on cases involving terrorism, child exploitation, and racist nationalism.... [...]
