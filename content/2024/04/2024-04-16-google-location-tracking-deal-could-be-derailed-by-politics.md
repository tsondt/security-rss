Title: Google location tracking deal could be derailed by politics
Date: 2024-04-16T10:45:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-04-16-google-location-tracking-deal-could-be-derailed-by-politics

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/16/google_location_tracking_deal_could/){:target="_blank" rel="noopener"}

> $62 million settlement plan challenged over payments to progressive nonprofits Google's plan to pay $62 million to settle allegations that it tracked people even when their Location History setting was switched off may have to be renegotiated based on several objections.... [...]
