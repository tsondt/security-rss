Title: In Memoriam: Ross Anderson, 1956–2024
Date: 2024-04-10T11:08:10+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptanalysis;cryptography;cybersecurity;economics of security;security conferences;security engineering
Slug: 2024-04-10-in-memoriam-ross-anderson-19562024

[Source](https://www.schneier.com/blog/archives/2024/04/in-memoriam-ross-anderson-1956-2024.html){:target="_blank" rel="noopener"}

> Last week, I posted a short memorial of Ross Anderson. The Communications of the ACM asked me to expand it. Here’s the longer version. EDITED TO ADD (4/11): Two weeks before he passed away, Ross gave an 80-minute interview where he told his life story. [...]
