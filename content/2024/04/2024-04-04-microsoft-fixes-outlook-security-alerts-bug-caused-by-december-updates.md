Title: Microsoft fixes Outlook security alerts bug caused by December updates
Date: 2024-04-04T15:14:30-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-04-04-microsoft-fixes-outlook-security-alerts-bug-caused-by-december-updates

[Source](https://www.bleepingcomputer.com/news/security/microsoft-fixes-outlook-security-alerts-bug-caused-by-december-updates/){:target="_blank" rel="noopener"}

> Microsoft has fixed an issue that triggers erroneous Outlook security alerts when opening.ICS calendar files after installing the December 2023 Outlook Desktop security updates [...]
