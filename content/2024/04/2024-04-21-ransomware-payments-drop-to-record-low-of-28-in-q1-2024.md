Title: Ransomware payments drop to record low of 28% in Q1 2024
Date: 2024-04-21T10:21:42-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-04-21-ransomware-payments-drop-to-record-low-of-28-in-q1-2024

[Source](https://www.bleepingcomputer.com/news/security/ransomware-payments-drop-to-record-low-of-28-percent-in-q1-2024/){:target="_blank" rel="noopener"}

> Ransomware actors have had a rough start this year, as stats from cybersecurity firm Coveware show that the trend of victims declining to pay the cybercriminals continues and has now reached a new record low of 28%. [...]
