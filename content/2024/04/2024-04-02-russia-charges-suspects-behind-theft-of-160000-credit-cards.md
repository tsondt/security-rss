Title: Russia charges suspects behind theft of 160,000 credit cards
Date: 2024-04-02T11:37:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-04-02-russia-charges-suspects-behind-theft-of-160000-credit-cards

[Source](https://www.bleepingcomputer.com/news/security/russia-charges-suspects-behind-theft-of-160-000-credit-cards/){:target="_blank" rel="noopener"}

> Russia's Prosecutor General's Office has announced the indictment of six suspected "hacking group" members for using malware to steal credit card and payment information from foreign online stores. [...]
