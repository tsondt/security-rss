Title: Ring dinged for $5.6M after, among other claims, rogue insider spied on 'pretty girls'
Date: 2024-04-25T21:03:10+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-04-25-ring-dinged-for-56m-after-among-other-claims-rogue-insider-spied-on-pretty-girls

[Source](https://go.theregister.com/feed/www.theregister.com/2024/04/25/ring_ftc_settlement/){:target="_blank" rel="noopener"}

> Cash to go out as refunds to punters The FTC today announced it would be sending refunds totaling $5.6 million to Ring customers, paid from the Amazon subsidiary's coffers.... [...]
