Title: The Rise of Large-Language-Model Optimization
Date: 2024-04-25T11:02:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;essays;search engines
Slug: 2024-04-25-the-rise-of-large-language-model-optimization

[Source](https://www.schneier.com/blog/archives/2024/04/the-rise-of-large.html){:target="_blank" rel="noopener"}

> The web has become so interwoven with everyday life that it is easy to forget what an extraordinary accomplishment and treasure it is. In just a few decades, much of human knowledge has been collectively written up and made available to anyone with an internet connection. But all of this is coming to an end. The advent of AI threatens to destroy the complex online ecosystem that allows writers, artists, and other creators to reach human audiences. To understand why, you must understand publishing. Its core task is to connect writers to an audience. Publishers work as gatekeepers, filtering candidates [...]
