Title: New Blast-RADIUS attack breaks 30-year-old protocol used in networks everywhere
Date: 2024-07-09T19:02:38+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security
Slug: 2024-07-09-new-blast-radius-attack-breaks-30-year-old-protocol-used-in-networks-everywhere

[Source](https://arstechnica.com/?p=2035809){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) One of the most widely used network protocols is vulnerable to a newly discovered attack that can allow adversaries to gain control over a range of environments, including industrial controllers, telecommunications services, ISPs, and all manner of enterprise networks. Short for Remote Authentication Dial-In User Service, RADIUS harkens back to the days of dial-in Internet and network access through public switched telephone networks. It has remained the de facto standard for lightweight authentication ever since and is supported in virtually all switches, routers, access points, and VPN concentrators shipped in the past two decades. Despite its [...]
