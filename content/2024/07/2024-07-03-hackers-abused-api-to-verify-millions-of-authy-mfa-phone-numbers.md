Title: Hackers abused API to verify millions of Authy MFA phone numbers
Date: 2024-07-03T12:43:12-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-07-03-hackers-abused-api-to-verify-millions-of-authy-mfa-phone-numbers

[Source](https://www.bleepingcomputer.com/news/security/hackers-abused-api-to-verify-millions-of-authy-mfa-phone-numbers/){:target="_blank" rel="noopener"}

> Twilio has confirmed that an unsecured API endpoint allowed threat actors to verify the phone numbers of millions of Authy multi-factor authentication users, potentially making them vulnerable to SMS phishing and SIM swapping attacks. [...]
