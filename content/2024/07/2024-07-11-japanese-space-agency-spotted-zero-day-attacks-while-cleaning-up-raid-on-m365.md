Title: Japanese space agency spotted zero-day attacks while cleaning up raid on M365
Date: 2024-07-11T05:31:58+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-07-11-japanese-space-agency-spotted-zero-day-attacks-while-cleaning-up-raid-on-m365

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/11/jaxa_m365_zeroday_attacks/){:target="_blank" rel="noopener"}

> Multiple malware assault saw personal data accessed, rocket science remained safe The Japanese Space Exploration Agency (JAXA) discovered it was under attack using zero-day exploits while working with Microsoft to probe a 2023 cyberattack on its systems.... [...]
