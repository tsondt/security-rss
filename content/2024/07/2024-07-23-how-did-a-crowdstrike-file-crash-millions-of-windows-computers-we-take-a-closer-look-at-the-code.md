Title: How did a CrowdStrike file crash millions of Windows computers? We take a closer look at the code
Date: 2024-07-23T20:52:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-07-23-how-did-a-crowdstrike-file-crash-millions-of-windows-computers-we-take-a-closer-look-at-the-code

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/23/crowdstrike_failure_shows_need_for/){:target="_blank" rel="noopener"}

> Maybe next time some staged rollouts? A bit of QA too? Analysis Last week, at 0409 UTC on July 19, 2024, antivirus maker CrowdStrike released an update to its widely used Falcon platform that caused Microsoft Windows machines around the world to crash.... [...]
