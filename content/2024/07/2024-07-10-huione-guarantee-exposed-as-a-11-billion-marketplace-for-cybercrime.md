Title: Huione Guarantee exposed as a $11 billion marketplace for cybercrime
Date: 2024-07-10T16:40:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-07-10-huione-guarantee-exposed-as-a-11-billion-marketplace-for-cybercrime

[Source](https://www.bleepingcomputer.com/news/security/huione-guarantee-exposed-as-a-11-billion-marketplace-for-cybercrime/){:target="_blank" rel="noopener"}

> The seemingly legitimate online marketplace Huione Guarantee is being used as a platform for laundering money from online scams, especially "pig butchering" investment fraud, researchers say. [...]
