Title: CrowdStrike update crashes Windows systems, causes outages worldwide
Date: 2024-07-19T07:43:59-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-07-19-crowdstrike-update-crashes-windows-systems-causes-outages-worldwide

[Source](https://www.bleepingcomputer.com/news/security/crowdstrike-update-crashes-windows-systems-causes-outages-worldwide/){:target="_blank" rel="noopener"}

> A faulty component in the latest CrowdStrike Falcon update is crashing Windows systems, impacting various organizations and services across the world, including airports, TV stations, and hospitals. [...]
