Title: Major outages at CrowdStrike, Microsoft leave the world with BSODs and confusion
Date: 2024-07-19T13:22:09+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;Security;airlines;Crowdstrike;cybersecurity;falcon sensor;global outage;microsoft;Microsoft Azure
Slug: 2024-07-19-major-outages-at-crowdstrike-microsoft-leave-the-world-with-bsods-and-confusion

[Source](https://arstechnica.com/?p=2038106){:target="_blank" rel="noopener"}

> Enlarge / A passenger sits on the floor as long queues form at the check-in counters at Ninoy Aquino International Airport, on July 19, 2024 in Manila, Philippines. (credit: Ezra Acayan/Getty Images) Millions of people outside the IT industry are learning what CrowdStrike is today, and that's a real bad thing. Meanwhile, Microsoft is also catching blame for global network outages, and between the two, it's unclear as of Friday morning just who caused what. After cybersecurity firm CrowdStrike shipped an update to its Falcon Sensor software that protects mission-critical systems, blue screens of death (BSODs) started taking down Windows-based [...]
