Title: IAM so lost: A guide to identity in Google Cloud
Date: 2024-07-10T16:00:00+00:00
Author: Michele Chubirka
Category: GCP Security
Tags: Security & Identity
Slug: 2024-07-10-iam-so-lost-a-guide-to-identity-in-google-cloud

[Source](https://cloud.google.com/blog/products/identity-security/scaling-the-iam-mountain-an-in-depth-guide-to-identity-in-google-cloud/){:target="_blank" rel="noopener"}

> Identity and access management (IAM) can seem like a gentle hill at first. It’s not too difficult to manage, assigning logins and passwords, and granting access control based on a person’s job in an organization. However, that gentle hill can shift as you start to configure IAM. Suddenly, it can feel like a steep mountain slope: Terminology and concepts overlap and slip beneath your feet, which is a serious problem because security boundaries in the cloud are based on understanding crucial identity concepts. To help you regain solid footing, let’s start by demystifying two foundational IAM access control principles: the [...]
