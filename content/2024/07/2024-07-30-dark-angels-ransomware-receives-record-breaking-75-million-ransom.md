Title: Dark Angels ransomware receives record-breaking $75 million ransom
Date: 2024-07-30T16:22:00-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-07-30-dark-angels-ransomware-receives-record-breaking-75-million-ransom

[Source](https://www.bleepingcomputer.com/news/security/dark-angels-ransomware-receives-record-breaking-75-million-ransom/){:target="_blank" rel="noopener"}

> A Fortune 50 company paid a record-breaking $75 million ransom payment to the Dark Angels ransomware gang, according to a report by Zscaler ThreatLabz. [...]
