Title: Cisco SSM On-Prem bug lets hackers change any user's password
Date: 2024-07-17T13:31:09-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-17-cisco-ssm-on-prem-bug-lets-hackers-change-any-users-password

[Source](https://www.bleepingcomputer.com/news/security/cisco-ssm-on-prem-bug-lets-hackers-change-any-users-password/){:target="_blank" rel="noopener"}

> Cisco has fixed a maximum severity vulnerability that allows attackers to change any user's password on vulnerable Cisco Smart Software Manager On-Prem (Cisco SSM On-Prem) license servers, including administrators. [...]
