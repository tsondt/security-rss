Title: North Korea likely behind takedown of Indian crypto exchange WazirX
Date: 2024-07-19T05:59:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-07-19-north-korea-likely-behind-takedown-of-indian-crypto-exchange-wazirx

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/19/wasirx_pauses_trade/){:target="_blank" rel="noopener"}

> Firm halts trades after seeing $230 million disappear Indian crypto exchange WazirX has revealed it lost virtual assets valued at over $230 million after a cyber attack that has since been linked to North Korea.... [...]
