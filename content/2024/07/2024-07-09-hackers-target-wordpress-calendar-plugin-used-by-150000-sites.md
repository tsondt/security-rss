Title: Hackers target WordPress calendar plugin used by 150,000 sites
Date: 2024-07-09T13:21:10-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-07-09-hackers-target-wordpress-calendar-plugin-used-by-150000-sites

[Source](https://www.bleepingcomputer.com/news/security/hackers-target-wordpress-calendar-plugin-used-by-150-000-sites/){:target="_blank" rel="noopener"}

> Hackers are trying to exploit a vulnerability in the Modern Events Calendar WordPress plugin that is present on more than 150,000 websites to upload arbitrary files to a vulnerable site and execute code remotely. [...]
