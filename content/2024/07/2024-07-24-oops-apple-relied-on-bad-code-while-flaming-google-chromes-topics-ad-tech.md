Title: Oops. Apple relied on bad code while flaming Google Chrome's Topics ad tech
Date: 2024-07-24T20:44:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-07-24-oops-apple-relied-on-bad-code-while-flaming-google-chromes-topics-ad-tech

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/24/apple_google_topics/){:target="_blank" rel="noopener"}

> Yes, you can be fingerprinted and tracked via Privacy Sandbox – tho the risk isn't as high as feared Apple last week celebrated a slew of privacy changes coming to its Safari browser and took the time to bash rival Google for its Topics system that serves online ads based on your Chrome history.... [...]
