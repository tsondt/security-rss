Title: RADIUS networking protocol blasted into submission through MD5-based flaw
Date: 2024-07-10T03:15:37+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-10-radius-networking-protocol-blasted-into-submission-through-md5-based-flaw

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/10/radius_critical_vulnerability/){:target="_blank" rel="noopener"}

> If someone can do a little MITM'ing and hash cracking, they can log in with no valid password needed Cybersecurity experts at universities and Big Tech have disclosed a vulnerability in a common client-server networking protocol that allows snoops to potentially bypass user authentication via man-in-the-middle (MITM) attacks.... [...]
