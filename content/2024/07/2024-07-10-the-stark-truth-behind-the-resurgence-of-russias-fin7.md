Title: The Stark Truth Behind the Resurgence of Russia’s Fin7
Date: 2024-07-10T16:22:59+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Ne'er-Do-Well News;Ransomware;Russia's War on Ukraine;Web Fraud 2.0;7-zip;Advanced IP Scanner;AIMP;AnyDesk;AutoDesk;Bastion Secure;Bitwarden;Blackberry;Combi Security;eSentire;Fin7;Malwarebytes;microsoft;Node.js;Notepad++;pgAdmin;ProDaft;ProtectedPDFViewer;PuTTY;Python;Rest Proxy;Silent Push;spearphishing;Stark Industries Solutions;Sublime Text;typosquatting;Zach Edwards
Slug: 2024-07-10-the-stark-truth-behind-the-resurgence-of-russias-fin7

[Source](https://krebsonsecurity.com/2024/07/the-stark-truth-behind-the-resurgence-of-russias-fin7/){:target="_blank" rel="noopener"}

> The Russia-based cybercrime group dubbed “ Fin7,” known for phishing and malware attacks that have cost victim organizations an estimated $3 billion in losses since 2013, was declared dead last year by U.S. authorities. But experts say Fin7 has roared back to life in 2024 — setting up thousands of websites mimicking a range of media and technology companies — with the help of Stark Industries Solutions, a sprawling hosting provider that is a persistent source of cyberattacks against enemies of Russia. In May 2023, the U.S. attorney for Washington state declared “Fin7 is an entity no more,” after prosecutors [...]
