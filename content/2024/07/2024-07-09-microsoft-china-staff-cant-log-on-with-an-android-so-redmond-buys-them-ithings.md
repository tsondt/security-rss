Title: Microsoft China staff can't log on with an Android, so Redmond buys them iThings
Date: 2024-07-09T06:32:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-07-09-microsoft-china-staff-cant-log-on-with-an-android-so-redmond-buys-them-ithings

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/09/microsoft_china_apple_google_authentication/){:target="_blank" rel="noopener"}

> Google's absence creates software distribution issues not even mighty Microsoft can handle Microsoft China will provide staff with Apple devices so they can log on to the software giant's systems.... [...]
