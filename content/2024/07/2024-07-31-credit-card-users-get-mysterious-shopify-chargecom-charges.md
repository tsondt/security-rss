Title: Credit card users get mysterious shopify-charge.com charges
Date: 2024-07-31T17:19:44-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-07-31-credit-card-users-get-mysterious-shopify-chargecom-charges

[Source](https://www.bleepingcomputer.com/news/security/credit-card-users-get-mysterious-shopify-chargecom-charges/){:target="_blank" rel="noopener"}

> People worldwide report seeing mysterious $1 or $0 charges from Shopify-charge.com appearing on their credit card bills, even when they did not attempt to purchase anything. [...]
