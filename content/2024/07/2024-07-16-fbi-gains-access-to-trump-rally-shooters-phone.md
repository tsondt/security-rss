Title: FBI gains access to Trump rally shooter's phone
Date: 2024-07-16T03:16:30+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-07-16-fbi-gains-access-to-trump-rally-shooters-phone

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/16/fbi_access_trump_shooting_phone/){:target="_blank" rel="noopener"}

> Hasn't said how it did it, but has form cracking devices The FBI on Monday revealed it has gained access to a phone it says was used by Thomas Matthew Crooks – the man who shot at and wounded former US president Donald Trump on July 13 in an apparent failed assassination attempt.... [...]
