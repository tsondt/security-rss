Title: Merged Exabeam and LogRhythm cut jobs, face lawsuit
Date: 2024-07-17T23:27:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-07-17-merged-exabeam-and-logrhythm-cut-jobs-face-lawsuit

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/17/exabeam_logrhythm_merger_job_cuts_lawsuit/){:target="_blank" rel="noopener"}

> Unconfirmed reports suggest 30 percent reduction in headcount Exabeam and LogRhythm – a pair of cyber security firms – finalized their merger on Wednesday, an occasion The Register understands was marked by swift job cuts and shareholder action to investigate the transaction.... [...]
