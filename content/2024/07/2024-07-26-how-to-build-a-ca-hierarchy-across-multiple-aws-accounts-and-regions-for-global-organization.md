Title: How to build a CA hierarchy across multiple AWS accounts and Regions for global organization
Date: 2024-07-26T16:08:43+00:00
Author: Jiaqing Xue
Category: AWS Security
Tags: AWS Private Certificate Authority;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2024-07-26-how-to-build-a-ca-hierarchy-across-multiple-aws-accounts-and-regions-for-global-organization

[Source](https://aws.amazon.com/blogs/security/how-to-build-a-ca-hierarchy-across-multiple-aws-accounts-and-regions-for-global-organization/){:target="_blank" rel="noopener"}

> Building a certificate authority (CA) hierarchy using AWS Private Certificate Authority has been made simple in Amazon Web Services (AWS); however, the CA tree will often reside in one AWS Region in one account. Many AWS customers run their businesses in multiple Regions using multiple AWS accounts and have described the process of creating a [...]
