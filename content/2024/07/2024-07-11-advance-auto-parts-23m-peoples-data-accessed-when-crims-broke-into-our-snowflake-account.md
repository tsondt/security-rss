Title: Advance Auto Parts: 2.3M people's data accessed when crims broke into our Snowflake account
Date: 2024-07-11T13:15:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-11-advance-auto-parts-23m-peoples-data-accessed-when-crims-broke-into-our-snowflake-account

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/11/advance_auto_parts_confirms_23/){:target="_blank" rel="noopener"}

> Letters from CISO Ethan Steiger suggest the data related to job applications Advance Auto Parts' CISO just revealed for the first time the number of individuals affected when criminals broke into its Snowflake instance – a hefty 2.3 million.... [...]
