Title: 'Error' in Microsoft's DDoS defenses amplified 8-hour Azure outage
Date: 2024-07-31T12:58:06+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-07-31-error-in-microsofts-ddos-defenses-amplified-8-hour-azure-outage

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/31/microsoft_ddos_azure/){:target="_blank" rel="noopener"}

> A playbook full of strategies and someone fumbles the implementation Do you have problems configuring Microsoft's Defender? You might not be alone: Microsoft admitted that whatever it's using for its defensive implementation exacerbated yesterday's Azure instability.... [...]
