Title: DigiCert gives unlucky folks 24 hours to replace doomed certificates after code blunder
Date: 2024-07-31T01:31:46+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-07-31-digicert-gives-unlucky-folks-24-hours-to-replace-doomed-certificates-after-code-blunder

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/31/digicert_certificates_revoked/){:target="_blank" rel="noopener"}

> For the want of an underscore DigiCert has given unlucky customers 24 hours to replace their SSL/TLS security certificates it previously issued them – due to a five-year-old blunder in its backend software.... [...]
