Title: Patch management still seemingly abysmal because no one wants the job
Date: 2024-07-25T07:27:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-07-25-patch-management-still-seemingly-abysmal-because-no-one-wants-the-job

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/25/patch_management_study/){:target="_blank" rel="noopener"}

> Are your security and ops teams fighting to pass the buck? Comment Patching: The bane of every IT professional's existence. It's a thankless, laborious job that no one wants to do, goes unappreciated when it interrupts work, and yet it's more critical than ever in this modern threat landscape.... [...]
