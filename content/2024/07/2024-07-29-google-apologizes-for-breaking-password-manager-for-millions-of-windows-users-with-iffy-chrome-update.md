Title: Google apologizes for breaking password manager for millions of Windows users with iffy Chrome update
Date: 2024-07-29T13:01:07+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-07-29-google-apologizes-for-breaking-password-manager-for-millions-of-windows-users-with-iffy-chrome-update

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/29/google_password_manager_outage/){:target="_blank" rel="noopener"}

> Happy Sysadmin Day Google celebrated Sysadmin Day last week by apologizing for breaking its password manager for millions of Windows users – just as many Windows admins were still hard at work mitigating the impact of the faulty CrowdStrike update.... [...]
