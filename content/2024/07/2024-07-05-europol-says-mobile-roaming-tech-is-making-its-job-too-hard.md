Title: Europol says mobile roaming tech is making its job too hard
Date: 2024-07-05T08:26:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-05-europol-says-mobile-roaming-tech-is-making-its-job-too-hard

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/05/europol_home_routing_complaint/){:target="_blank" rel="noopener"}

> Privacy measures apparently helping criminals evade capture Top Eurocops are appealing for help from lawmakers to undermine a privacy-enhancing technology (PET) they say is hampering criminal investigations – and it's not end-to-end encryption this time. Not exactly.... [...]
