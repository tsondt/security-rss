Title: Brett Solomon on Digital Rights
Date: 2024-07-19T16:02:42+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;history of security;human rights
Slug: 2024-07-19-brett-solomon-on-digital-rights

[Source](https://www.schneier.com/blog/archives/2024/07/brett-solomon-on-digital-rights.html){:target="_blank" rel="noopener"}

> Brett Solomon is retiring from AccessNow after fifteen years as its Executive Director. He’s written a blog post about what he’s learned and what comes next. [...]
