Title: Europol nukes nearly 600 IP addresses in Cobalt Strike crackdown
Date: 2024-07-04T08:30:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-04-europol-nukes-nearly-600-ip-addresses-in-cobalt-strike-crackdown

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/04/europol_cobalt_strike_crackdown/){:target="_blank" rel="noopener"}

> Private sector helped out with week-long operation – but didn't touch China Europol just announced that a week-long operation at the end of June dropped nearly 600 IP addresses that supported illegal copies of Cobalt Strike.... [...]
