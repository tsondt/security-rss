Title: Not-so-OpenAI allegedly never bothered to report 2023 data breach
Date: 2024-07-08T01:45:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-07-08-not-so-openai-allegedly-never-bothered-to-report-2023-data-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/08/infosec_in_brief/){:target="_blank" rel="noopener"}

> Also: F1 authority breached; Prudential victim count skyrockets; a new ransomware actor appears; and more security in brief It's been a week of bad cyber security revelations for OpenAI, after news emerged that the startup failed to report a 2023 breach of its systems to anybody outside the organization, and that its ChatGPT app for macOS was coded without any regard for user privacy.... [...]
