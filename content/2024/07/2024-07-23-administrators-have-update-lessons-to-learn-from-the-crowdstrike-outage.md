Title: Administrators have update lessons to learn from the CrowdStrike outage
Date: 2024-07-23T17:27:09+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-07-23-administrators-have-update-lessons-to-learn-from-the-crowdstrike-outage

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/23/crowdstrike_lessons_to_learn/){:target="_blank" rel="noopener"}

> How could this happen to us? We were supposed to be two versions behind? If administrators have learned anything from the CrowdStrike chaos, it's to understand exactly what delayed updates mean – or don't mean – in the anti-malware world.... [...]
