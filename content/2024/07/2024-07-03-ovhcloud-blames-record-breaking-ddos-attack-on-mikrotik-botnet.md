Title: OVHcloud blames record-breaking DDoS attack on MikroTik botnet
Date: 2024-07-03T14:07:29-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-07-03-ovhcloud-blames-record-breaking-ddos-attack-on-mikrotik-botnet

[Source](https://www.bleepingcomputer.com/news/security/ovhcloud-blames-record-breaking-ddos-attack-on-mikrotik-botnet/){:target="_blank" rel="noopener"}

> OVHcloud, a global cloud services provider and one of the largest of its kind in Europe, says it mitigated a record-breaking distributed denial of service (DDoS) attack earlier this year that reached an unprecedented packet rate of 840 million packets per second (Mpps). [...]
