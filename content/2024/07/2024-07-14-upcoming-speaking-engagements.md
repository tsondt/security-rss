Title: Upcoming Speaking Engagements
Date: 2024-07-14T16:05:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2024-07-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2024/07/upcoming-speaking-engagements-38.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking—along with John Bruce, the CEO and Co-founder of Inrupt—at the 18th Annual CDOIQ Symposium in Cambridge, Massachusetts, USA. The symposium runs from July 16 through 18, 2024, and my session is on Tuesday, July 16 at 3:15 PM. The symposium will also be livestreamed through the Whova platform. I’m speaking on “Reimagining Democracy in the Age of AI” at the Bozeman Library in Bozeman, Montana, USA, July 18, 2024. The event will also be available via Zoom. I’m speaking at the TEDxBillings Democracy Event [...]
