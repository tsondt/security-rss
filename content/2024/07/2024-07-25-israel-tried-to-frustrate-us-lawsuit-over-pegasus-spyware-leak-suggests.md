Title: Israel tried to frustrate US lawsuit over Pegasus spyware, leak suggests
Date: 2024-07-25T16:00:25+00:00
Author: Harry Davies, and Stephanie Kirchgaessner in Washington
Category: The Guardian
Tags: Israel;WhatsApp;Espionage;Technology;US news;Surveillance;Data and computer security
Slug: 2024-07-25-israel-tried-to-frustrate-us-lawsuit-over-pegasus-spyware-leak-suggests

[Source](https://www.theguardian.com/news/article/2024/jul/25/israel-tried-to-frustrate-us-lawsuit-over-pegasus-spyware-leak-suggests){:target="_blank" rel="noopener"}

> Officials seized documents from NSO Group to try to stop handover of information about notorious hacking tool, files suggest The Israeli government took extraordinary measures to frustrate a high-stakes US lawsuit that threatened to reveal closely guarded secrets about one of the world’s most notorious hacking tools, leaked files suggest. Israeli officials seized documents about Pegasus spyware from its manufacturer, NSO Group, in an effort to prevent the company from being able to comply with demands made by WhatsApp in a US court to hand over information about the invasive technology. Continue reading... [...]
