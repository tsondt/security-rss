Title: Google's plan to drop third-party cookies in Chrome crumbles
Date: 2024-07-23T00:03:53+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-07-23-googles-plan-to-drop-third-party-cookies-in-chrome-crumbles

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/23/google_cookies_third_party_continue/){:target="_blank" rel="noopener"}

> Ad giant promises to protect privacy, as critics say surveillance continues Google no longer intends to drop support for third-party cookies – the online identifiers used by the ad industry to track people and target them with ads based on their online activities.... [...]
