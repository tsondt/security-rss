Title: Ransomware infection cuts off blood supply to 250+ hospitals
Date: 2024-07-31T23:33:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-07-31-ransomware-infection-cuts-off-blood-supply-to-250-hospitals

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/31/ransomware_blood_supply_hospital/){:target="_blank" rel="noopener"}

> Scumbags go for the jugular A ransomware attack against blood-donation nonprofit OneBlood, which services more than 250 American hospitals, has "significantly reduced" the org's ability to take, test, and distribute blood.... [...]
