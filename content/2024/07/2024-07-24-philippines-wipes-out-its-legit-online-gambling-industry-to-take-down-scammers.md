Title: Philippines wipes out its legit online gambling industry to take down scammers
Date: 2024-07-24T00:30:15+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-07-24-philippines-wipes-out-its-legit-online-gambling-industry-to-take-down-scammers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/24/phillipines_bans_online_gambling_operators/){:target="_blank" rel="noopener"}

> President apologizes in advance for job losses The Philippines has decided to dismantle the worst of its offshored industries: the bits that run gambling and scam operations.... [...]
