Title: Evolve Bank &amp; Trust confirms LockBit stole 7.6 million people's data
Date: 2024-07-09T13:52:32+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-09-evolve-bank-trust-confirms-lockbit-stole-76-million-peoples-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/09/evolve_lockbit_attack/){:target="_blank" rel="noopener"}

> Making cyberattack among the largest ever recorded in finance industry Evolve Bank & Trust says the data of more than 7.6 million customers was stolen during the LockBit break-in in late May, per a fresh filing with Maine's attorney general.... [...]
