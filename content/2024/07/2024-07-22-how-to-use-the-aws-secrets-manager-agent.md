Title: How to use the AWS Secrets Manager Agent
Date: 2024-07-22T17:45:53+00:00
Author: Eduardo Patrocinio
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;AWS Secrets Manager;Security Blog
Slug: 2024-07-22-how-to-use-the-aws-secrets-manager-agent

[Source](https://aws.amazon.com/blogs/security/how-to-use-the-aws-secrets-manager-agent/){:target="_blank" rel="noopener"}

> AWS Secrets Manager is a service that helps you manage, retrieve, and rotate database credentials, application credentials, API keys, and other secrets throughout their lifecycles. You can use Secrets Manager to replace hard-coded credentials in application source code with a runtime call to the Secrets Manager service to retrieve credentials dynamically when you need them. [...]
