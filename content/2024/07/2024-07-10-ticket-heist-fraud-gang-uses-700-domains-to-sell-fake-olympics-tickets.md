Title: Ticket Heist fraud gang uses 700 domains to sell fake Olympics tickets
Date: 2024-07-10T06:33:31-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-07-10-ticket-heist-fraud-gang-uses-700-domains-to-sell-fake-olympics-tickets

[Source](https://www.bleepingcomputer.com/news/security/ticket-heist-fraud-gang-uses-700-domains-to-sell-fake-olympics-tickets/){:target="_blank" rel="noopener"}

> A large-scale fraud campaign with over 700 domain names is likely targeting Russian-speaking users looking to purchase tickets for the Summer Olympics in Paris. [...]
