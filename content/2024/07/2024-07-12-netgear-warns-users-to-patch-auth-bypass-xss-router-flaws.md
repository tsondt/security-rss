Title: Netgear warns users to patch auth bypass, XSS router flaws
Date: 2024-07-12T11:34:50-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-12-netgear-warns-users-to-patch-auth-bypass-xss-router-flaws

[Source](https://www.bleepingcomputer.com/news/security/netgear-warns-users-to-patch-authentication-bypass-xss-router-flaws/){:target="_blank" rel="noopener"}

> Netgear warned customers to update their devices to the latest available firmware, which patches stored cross-site scripting (XSS) and authentication bypass vulnerabilities in several WiFi 6 router models. [...]
