Title: Progress discloses second critical flaw in Telerik Report Server in as many months
Date: 2024-07-26T13:32:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-26-progress-discloses-second-critical-flaw-in-telerik-report-server-in-as-many-months

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/26/critical_vulnerability_progress_telerik/){:target="_blank" rel="noopener"}

> These are the kinds of bugs APTs thrive on, just ask the Feds Progress Software's latest security advisory warns customers about the second critical vulnerability targeting its Telerik Report Server in as many months.... [...]
