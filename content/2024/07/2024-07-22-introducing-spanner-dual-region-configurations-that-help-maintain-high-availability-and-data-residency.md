Title: Introducing Spanner dual-region configurations that help maintain high availability and data residency
Date: 2024-07-22T16:00:00+00:00
Author: Nitin Sagar
Category: GCP Security
Tags: Security & Identity;Databases
Slug: 2024-07-22-introducing-spanner-dual-region-configurations-that-help-maintain-high-availability-and-data-residency

[Source](https://cloud.google.com/blog/products/databases/spanner-dual-region-configurations-for-data-residency/){:target="_blank" rel="noopener"}

> Spanner is Google’s fully managed, globally distributed database with high throughput and virtually unlimited scale. Spanner processes over 4 billion queries per second at peak. With features such as automatic sharding, zero downtime, and strong consistency, Spanner powers demanding, global workloads — not only across customers in financial services, gaming, retail, and other industries but also Google internally — that are both relational and non-relational. Previously, to maintain data residency in countries with only two Google Cloud regions, you were restricted to regional Spanner configurations with 99.99% availability. That’s because Spanner multi-region configurations require three regions, one of which would [...]
