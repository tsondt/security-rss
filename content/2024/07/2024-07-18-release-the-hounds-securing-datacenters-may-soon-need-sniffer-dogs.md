Title: Release the hounds! Securing datacenters may soon need sniffer dogs
Date: 2024-07-18T00:54:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-07-18-release-the-hounds-securing-datacenters-may-soon-need-sniffer-dogs

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/18/sniffer_dogs_datacenter_defence_implants/){:target="_blank" rel="noopener"}

> Nothing else can detect attackers with implants designed to foil physical security Sniffer dogs may soon become a useful means of improving physical security in datacenters, as increasing numbers of people are adopting implants like NFC chips that have the potential to enable novel attacks on access control tools.... [...]
