Title: CrowdStrike Windows patchpocalypse could take weeks to fix, IT admins fear
Date: 2024-07-19T17:54:07+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-07-19-crowdstrike-windows-patchpocalypse-could-take-weeks-to-fix-it-admins-fear

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/19/crowdstrike_windows_kettle/){:target="_blank" rel="noopener"}

> Our vultures gather to review this very freaky Friday Kettle If you're an IT administrator with Windows boxes on your network, Friday can't have been a lot of fun. What's likely millions of systems were or still are stuck in blue-screen boot loop hell, mostly requiring manual intervention to fix.... [...]
