Title: Australian charged for ‘Evil Twin’ WiFi attack on plane
Date: 2024-07-01T14:28:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-07-01-australian-charged-for-evil-twin-wifi-attack-on-plane

[Source](https://www.bleepingcomputer.com/news/security/australian-charged-for-evil-twin-wifi-attack-on-plane/){:target="_blank" rel="noopener"}

> An Australian man was charged by Australia's Federal Police (AFP) for allegedly conducting an 'evil twin' WiFi attack on various domestic flights and airports in Perth, Melbourne, and Adelaide to steal other people's email or social media credentials. [...]
