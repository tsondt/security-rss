Title: Europol takes down 593 Cobalt Strike servers used by cybercriminals
Date: 2024-07-03T10:46:32-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-03-europol-takes-down-593-cobalt-strike-servers-used-by-cybercriminals

[Source](https://www.bleepingcomputer.com/news/security/europol-takes-down-593-cobalt-strike-servers-used-by-cybercriminals/){:target="_blank" rel="noopener"}

> Europol coordinated a joint law enforcement action known as Operation Morpheus, which led to the takedown of almost 600 Cobalt Strike servers used by cybercriminals to infiltrate victims' networks. [...]
