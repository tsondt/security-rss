Title: CISA broke into a US federal agency, and no one noticed for a full 5 months
Date: 2024-07-12T18:01:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-12-cisa-broke-into-a-us-federal-agency-and-no-one-noticed-for-a-full-5-months

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/12/cisa_broke_into_fed_agency/){:target="_blank" rel="noopener"}

> Red team exercise revealed a score of security fails The US Cybersecurity and Infrastructure Security Agency (CISA) says a red team exercise at a certain unnamed federal agency in 2023 revealed a string of security failings that exposed its most critical assets.... [...]
