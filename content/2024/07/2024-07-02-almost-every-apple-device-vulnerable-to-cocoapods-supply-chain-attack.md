Title: 'Almost every Apple device' vulnerable to CocoaPods supply chain attack
Date: 2024-07-02T07:32:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-07-02-almost-every-apple-device-vulnerable-to-cocoapods-supply-chain-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/02/cocoapods_vulns_supply_chain_potential/){:target="_blank" rel="noopener"}

> Dependency manager used in millions of apps leaves a bitter taste CocoaPods, an open-source dependency manager used in over three million applications coded in Swift and Objective-C, left thousands of packages exposed and ready for takeover for nearly a decade – thereby creating opportunities for supply chain attacks on iOS and macOS apps, according to security researchers.... [...]
