Title: CISA director: US is 'not afraid' to shout about Big Tech's security failings
Date: 2024-07-01T09:35:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-01-cisa-director-us-is-not-afraid-to-shout-about-big-techs-security-failings

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/01/cisa_big_tech_security/){:target="_blank" rel="noopener"}

> Jen Easterly hopes CSRB's Microsoft report won't impede future private sector collaboration CISA director Jen Easterly says the US Cybersecurity Safety Review Board (CSRB) "is not afraid to say when something is amiss" in response to questions about the future of private sector collaboration following the board's scathing report on Microsoft.... [...]
