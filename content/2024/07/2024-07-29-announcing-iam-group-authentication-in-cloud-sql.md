Title: Announcing IAM group authentication in Cloud SQL
Date: 2024-07-29T16:00:00+00:00
Author: Neha Bhatnagar
Category: GCP Security
Tags: Cloud SQL;Security & Identity;Databases
Slug: 2024-07-29-announcing-iam-group-authentication-in-cloud-sql

[Source](https://cloud.google.com/blog/products/databases/announcing-iam-group-authentication-in-cloud-sql/){:target="_blank" rel="noopener"}

> Managing and auditing data access can be very complex at scale, in particular, for a fleet of databases with a myriad of users. Today, we’re launching Identity and Access Management (IAM) group authentication in Cloud SQL for PostgreSQL and Cloud SQL for MySQL, simplifying user management and database authentication. Now, developers, database administrators, and security admins can benefit from simplified database access, better security controls, and more efficient user management at scale. Cloud SQL IAM group authentication allows customers to use Google Cloud Identity groups to manage access to Cloud SQL instances and databases. IAM group authentication extends existing IAM [...]
