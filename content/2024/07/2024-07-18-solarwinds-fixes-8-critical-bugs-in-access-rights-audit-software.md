Title: SolarWinds fixes 8 critical bugs in access rights audit software
Date: 2024-07-18T11:51:04-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-18-solarwinds-fixes-8-critical-bugs-in-access-rights-audit-software

[Source](https://www.bleepingcomputer.com/news/security/solarwinds-fixes-8-critical-bugs-in-access-rights-audit-software/){:target="_blank" rel="noopener"}

> SolarWinds has fixed eight critical vulnerabilities in its Access Rights Manager (ARM) software, six of which allowed attackers to gain remote code execution (RCE) on vulnerable devices. [...]
