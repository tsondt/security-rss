Title: At the Olympics, AI is watching you
Date: 2024-07-26T16:24:28+00:00
Author: WIRED
Category: Ars Technica
Tags: AI;Biz & IT;Security;2024 olympics;AI surveillance;syndication
Slug: 2024-07-26-at-the-olympics-ai-is-watching-you

[Source](https://arstechnica.com/?p=2039514){:target="_blank" rel="noopener"}

> Enlarge / Police observe the Eiffel Tower from Trocadero ahead of the Paris 2024 Olympic Games on July 22, 2024. (credit: Hector Vivas/Getty Images ) On the eve of the Olympics opening ceremony, Paris is a city swamped in security. Forty thousand barriers divide the French capital. Packs of police officers wearing stab vests patrol pretty, cobbled streets. The river Seine is out of bounds to anyone who has not already been vetted and issued a personal QR code. Khaki-clad soldiers, present since the 2015 terrorist attacks, linger near a canal-side boulangerie, wearing berets and clutching large guns to their [...]
