Title: Over 400,000 Life360 user phone numbers leaked via unsecured API
Date: 2024-07-17T12:32:43-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-17-over-400000-life360-user-phone-numbers-leaked-via-unsecured-api

[Source](https://www.bleepingcomputer.com/news/security/over-400-000-life360-user-phone-numbers-leaked-via-unsecured-android-api/){:target="_blank" rel="noopener"}

> A threat actor has leaked a database containing the personal information of 442,519 Life360 customers collected by abusing a flaw in the login API. [...]
