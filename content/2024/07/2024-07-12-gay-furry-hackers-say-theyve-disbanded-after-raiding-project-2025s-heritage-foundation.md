Title: 'Gay furry hackers' say they've disbanded after raiding Project 2025's Heritage Foundation
Date: 2024-07-12T00:22:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-07-12-gay-furry-hackers-say-theyve-disbanded-after-raiding-project-2025s-heritage-foundation

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/12/gay_furry_hackers_2025/){:target="_blank" rel="noopener"}

> Ultra-conservative org funnily enough not ready to turn the other cheek After claiming to break into a database belonging to The Heritage Foundation, and then leaking 2GB of files belonging to the ultra-conservative think tank, the hacktivist crew SiegedSec claims to have disbanded.... [...]
