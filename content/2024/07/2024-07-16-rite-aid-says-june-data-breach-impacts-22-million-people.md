Title: Rite Aid says June data breach impacts 2.2 million people
Date: 2024-07-16T10:54:23-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-16-rite-aid-says-june-data-breach-impacts-22-million-people

[Source](https://www.bleepingcomputer.com/news/security/rite-aid-says-june-data-breach-impacts-22-million-people/){:target="_blank" rel="noopener"}

> Rite Aid, the third-largest drugstore chain in the United States, says that 2.2 million customers' personal information was stolen last month in what it described as a "data security incident." [...]
