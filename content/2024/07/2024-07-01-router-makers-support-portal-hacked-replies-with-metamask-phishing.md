Title: Router maker's support portal hacked, replies with MetaMask phishing
Date: 2024-07-01T03:58:12-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2024-07-01-router-makers-support-portal-hacked-replies-with-metamask-phishing

[Source](https://www.bleepingcomputer.com/news/security/router-makers-support-portal-hacked-replies-with-metamask-phishing/){:target="_blank" rel="noopener"}

> BleepingComputer has verified that the helpdesk portal of a router manufacturer is currently sending MetaMask phishing emails in response to newly filed support tickets, in what appears to be a compromise. [...]
