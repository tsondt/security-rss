Title: Rite Aid says breach exposes sensitive details of 2.2 million customers
Date: 2024-07-16T22:09:58+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;Data breaches;personal information;rite aid
Slug: 2024-07-16-rite-aid-says-breach-exposes-sensitive-details-of-22-million-customers

[Source](https://arstechnica.com/?p=2037497){:target="_blank" rel="noopener"}

> Enlarge / Rite Aid logo displayed at one of its stores. (credit: Getty Images) Rite Aid, the third biggest US drug store chain, said that more than 2.2 million of its customers have been swept into a data breach that stole personal information, including driver's license numbers, addresses, and dates of birth. The company said in mandatory filings with the attorneys general of states including Maine, Massachusetts, Vermont, and Oregon that the stolen data was associated with purchases or attempted purchases of retail products made between June 6, 2017, and July 30, 2018. The data provided included the purchaser's name, [...]
