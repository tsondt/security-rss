Title: Selfie-based authentication raises eyebrows among infosec experts
Date: 2024-07-08T05:30:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-07-08-selfie-based-authentication-raises-eyebrows-among-infosec-experts

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/08/selfie_authentication_security/){:target="_blank" rel="noopener"}

> Vietnam now requires it for some purchases. It may be a fraud risk in Singapore. Or ML could be making it safe The use of selfies to verify identity online is an emerging trend in some parts of the world since the pandemic forced more business to go digital. Some banks – and even governments – have begun requiring live images over Zoom or similar in order to participate in the modern economy. The question must be asked, though: is it cyber smart?... [...]
