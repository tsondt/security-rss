Title: Strategies for achieving least privilege at scale – Part 2
Date: 2024-07-09T18:13:54+00:00
Author: Joshua Du Lac
Category: AWS Security
Tags: AWS IAM Access Analyzer;AWS Identity and Access Management (IAM);AWS Organizations;Best Practices;Intermediate (200);Management & Governance;Security, Identity, & Compliance;AWS IAM;Security Blog
Slug: 2024-07-09-strategies-for-achieving-least-privilege-at-scale-part-2

[Source](https://aws.amazon.com/blogs/security/strategies-for-achieving-least-privilege-at-scale-part-2/){:target="_blank" rel="noopener"}

> In this post, we continue with our recommendations for achieving least privilege at scale with AWS Identity and Access Management (IAM). In Part 1 of this two-part series, we described the first five of nine strategies for implementing least privilege in IAM at scale. We also looked at a few mental models that can assist [...]
