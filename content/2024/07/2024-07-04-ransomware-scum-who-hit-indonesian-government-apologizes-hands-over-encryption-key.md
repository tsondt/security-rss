Title: Ransomware scum who hit Indonesian government apologizes, hands over encryption key
Date: 2024-07-04T05:47:38+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-07-04-ransomware-scum-who-hit-indonesian-government-apologizes-hands-over-encryption-key

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/04/hackers_of_indonesian_government_apologize/){:target="_blank" rel="noopener"}

> Brain Cipher was never getting the $8 million it demanded anyway Brain Cipher, the group responsible for hacking into Indonesia's Temporary National Data Center (PDNS) and disrupting the country's services, has seemingly apologized for its actions and released an encryption key to the government.... [...]
