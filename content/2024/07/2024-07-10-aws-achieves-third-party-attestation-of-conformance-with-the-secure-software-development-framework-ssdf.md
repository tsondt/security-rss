Title: AWS achieves third-party attestation of conformance with the Secure Software Development Framework (SSDF)
Date: 2024-07-10T20:11:40+00:00
Author: Hayley Kleeman Jung
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Auditing;AWS security;Compliance;NIST;Security;Security Assurance;Security Blog
Slug: 2024-07-10-aws-achieves-third-party-attestation-of-conformance-with-the-secure-software-development-framework-ssdf

[Source](https://aws.amazon.com/blogs/security/aws-achieves-third-party-attestation-of-conformance-with-the-secure-software-development-framework-ssdf/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce the successful attestation of our conformance with the National Institute of Standards and Technology (NIST) Secure Software Development Framework (SSDF), Special Publication 800-218. This achievement underscores our ongoing commitment to the security and integrity of our software supply chain. Executive Order (EO) 14028, Improving the Nation’s Cybersecurity [...]
