Title: Beijing's attack gang Volt Typhoon was a false flag inside job conspiracy: China
Date: 2024-07-19T05:09:48+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-07-19-beijings-attack-gang-volt-typhoon-was-a-false-flag-inside-job-conspiracy-china

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/19/volt_typhoon_china_theory/){:target="_blank" rel="noopener"}

> Run by the NSA, the FBI, and Five Eyes nations, who fooled infosec researchers, apparently China has wildly claimed the Volt Typhoon gang, which Five Eyes nations accuse of being a Beijing-backed attacker that targets critical infrastructure, was in fact made up by the US intelligence community.... [...]
