Title: Cyber Public Health: A new approach to cybersecurity
Date: 2024-07-19T16:00:00+00:00
Author: Taylor Lehmann
Category: GCP Security
Tags: Healthcare & Life Sciences;Public Sector;Security & Identity
Slug: 2024-07-19-cyber-public-health-a-new-approach-to-cybersecurity

[Source](https://cloud.google.com/blog/products/identity-security/cyber-public-health-a-new-approach-to-cybersecurity/){:target="_blank" rel="noopener"}

> At Google, we believe the approach to cloud infrastructure should be informed, in part, by understanding the relative “health” of the Internet. Defining and measuring these vital statistics can help proactively and systemically identify and address conditions that make the internet unhealthy, unsafe and insecure. Crucially, they can be used to help craft a holistic view of the internet that applies the principles and science of public health to cybersecurity — an emerging field known as Cyber Public Health (CPH). We're excited to announce our support for the practice of CPH, which can help us understand if our individual efforts [...]
