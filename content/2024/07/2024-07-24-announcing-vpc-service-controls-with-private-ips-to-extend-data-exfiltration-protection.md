Title: Announcing VPC Service Controls with private IPs to extend data exfiltration protection
Date: 2024-07-24T16:00:00+00:00
Author: Sandesh D’Souza
Category: GCP Security
Tags: Networking;Customers;Security & Identity
Slug: 2024-07-24-announcing-vpc-service-controls-with-private-ips-to-extend-data-exfiltration-protection

[Source](https://cloud.google.com/blog/products/identity-security/announcing-vpc-service-controls-with-private-ips-to-extend-data-exfiltration-protection/){:target="_blank" rel="noopener"}

> Google Cloud’s VPC Service Controls can help organizations mitigate the risk of data exfiltration from their Google Cloud managed services. VPC Service Controls (VPC-SC) creates isolation perimeters around cloud resources and networks in Google Cloud, helping you limit access to your sensitive data. Today, we are excited to introduce support for private IP addresses within VPC Service Controls. This new capability permits traffic from specific internal networks to access protected resources. Extending VPC-SC to secure resources in private IP address space VPC-SC helps prevent data exfiltration to unauthorized Cloud organizations, folders, projects, and resources with defined perimeters accessible only by [...]
