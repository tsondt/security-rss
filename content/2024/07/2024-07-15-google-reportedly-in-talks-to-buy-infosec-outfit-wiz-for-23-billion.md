Title: Google reportedly in talks to buy infosec outfit Wiz for $23 billion
Date: 2024-07-15T04:39:35+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-07-15-google-reportedly-in-talks-to-buy-infosec-outfit-wiz-for-23-billion

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/15/google_wiz_acquisition/){:target="_blank" rel="noopener"}

> The security industry has never had a clear leader – could it be the Chocolate Factory? Ask any techie to name who leads the market for OSes, databases, networks or ERP and the answers are clear: Microsoft, Oracle, Cisco, and SAP.... [...]
