Title: Elexon's Insight into UK electricity felled by expired certificate
Date: 2024-07-09T14:01:13+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-07-09-elexons-insight-into-uk-electricity-felled-by-expired-certificate

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/09/elexons_insight_expired_cert/){:target="_blank" rel="noopener"}

> Understanding the power needs of the UK begins with knowing when renewals are due Certificate Watch Demonstrating that Microsoft is not alone in its inability to keep track of certificates is UK power market biz Elexon.... [...]
