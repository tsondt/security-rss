Title: Hacking Scientific Citations
Date: 2024-07-15T17:13:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic;fraud;hacking
Slug: 2024-07-15-hacking-scientific-citations

[Source](https://www.schneier.com/blog/archives/2024/07/hacking-scientific-citations.html){:target="_blank" rel="noopener"}

> Some scholars are inflating their reference counts by sneaking them into metadata: Citations of scientific work abide by a standardized referencing system: Each reference explicitly mentions at least the title, authors’ names, publication year, journal or conference name, and page numbers of the cited publication. These details are stored as metadata, not visible in the article’s text directly, but assigned to a digital object identifier, or DOI—a unique identifier for each scientific publication. References in a scientific publication allow authors to justify methodological choices or present the results of past studies, highlighting the iterative and collaborative nature of science. However, [...]
