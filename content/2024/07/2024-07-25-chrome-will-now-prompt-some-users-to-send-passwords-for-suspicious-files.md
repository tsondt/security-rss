Title: Chrome will now prompt some users to send passwords for suspicious files
Date: 2024-07-25T20:12:59+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Google;Security;chrome;google;passwords;safe browsing
Slug: 2024-07-25-chrome-will-now-prompt-some-users-to-send-passwords-for-suspicious-files

[Source](https://arstechnica.com/?p=2039375){:target="_blank" rel="noopener"}

> (credit: Chrome ) Google is redesigning Chrome malware detections to include password-protected executable files that users can upload for deep scanning, a change the browser maker says will allow it to detect more malicious threats. Google has long allowed users to switch on the Enhanced Mode of its Safe Browsing, a Chrome feature that warns users when they’re downloading a file that’s believed to be unsafe, either because of suspicious characteristics or because it’s in a list of known malware. With Enhanced Mode turned on, Google will prompt users to upload suspicious files that aren’t allowed or blocked by its [...]
