Title: Snake Mimics a Spider
Date: 2024-07-22T11:06:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;natural security
Slug: 2024-07-22-snake-mimics-a-spider

[Source](https://www.schneier.com/blog/archives/2024/07/snake-mimics-a-spider.html){:target="_blank" rel="noopener"}

> This is a fantastic video. It’s an Iranian spider-tailed horned viper ( Pseudocerastes urarachnoides ). Its tail looks like a spider, which the snake uses to fool passing birds looking for a meal. [...]
