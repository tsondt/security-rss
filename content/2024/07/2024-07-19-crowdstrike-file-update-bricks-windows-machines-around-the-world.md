Title: CrowdStrike file update bricks Windows machines around the world
Date: 2024-07-19T06:46:32+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-07-19-crowdstrike-file-update-bricks-windows-machines-around-the-world

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/19/crowdstrike_falcon_sensor_bsod_incident/){:target="_blank" rel="noopener"}

> Falcon Sensor putting hosts into deathloop - but there's a workaround Updated An update to a product from infosec vendor CrowdStrike is bricking computers running Windows globally.... [...]
