Title: Security biz KnowBe4 hired fake North Korean techie, who got straight to work ... on evil
Date: 2024-07-24T04:57:14+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-07-24-security-biz-knowbe4-hired-fake-north-korean-techie-who-got-straight-to-work-on-evil

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/24/knowbe4_north_korean/){:target="_blank" rel="noopener"}

> If it can happen to folks that run social engineering defence training, what hope for the rest of us? Cybersecurity awareness and training provider KnowBe4 hired a North Korean fake IT worker for a software engineering role on its AI team, and only realized its mistake once the guy started using his company-provided computer for evil.... [...]
