Title: Neiman Marcus data breach: 31 million email addresses found exposed
Date: 2024-07-08T16:38:16-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-08-neiman-marcus-data-breach-31-million-email-addresses-found-exposed

[Source](https://www.bleepingcomputer.com/news/security/neiman-marcus-data-breach-31-million-email-addresses-found-exposed/){:target="_blank" rel="noopener"}

> A May 2024 data breach disclosed by American luxury retailer and department store chain Neiman Marcus last month has exposed more than 31 million customer email addresses, according to Have I Been Pwned founder Troy Hunt, who analyzed the stolen data. [...]
