Title: Proofpoint phishing palaver plagues millions with 'perfectly spoofed' emails from IBM, Nike, Disney, others
Date: 2024-07-30T06:27:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-07-30-proofpoint-phishing-palaver-plagues-millions-with-perfectly-spoofed-emails-from-ibm-nike-disney-others

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/30/scammers_spoofed_emails/){:target="_blank" rel="noopener"}

> They DKIM here, they DKIM there A huge phishing campaign exploited a security blind-spot in Proofpoint's email filtering systems to send an average of three million "perfectly spoofed" messages a day purporting to be from Disney, IBM, Nike, Best Buy, and Coca-Cola – all of which are Proofpoint customers.... [...]
