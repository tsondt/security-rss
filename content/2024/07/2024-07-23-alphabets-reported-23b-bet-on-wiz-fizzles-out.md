Title: Alphabet's reported $23B bet on Wiz fizzles out
Date: 2024-07-23T14:32:12+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-07-23-alphabets-reported-23b-bet-on-wiz-fizzles-out

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/23/alphabet_wiz_deal_scuppered/){:target="_blank" rel="noopener"}

> Cybersecurity outfit to go its own way to IPO and $1B ARR On the day of Alphabet's Q2 earnings call, cybersecurity firm Wiz has walked from a $23 billion takeover bid by Google's parent company.... [...]
