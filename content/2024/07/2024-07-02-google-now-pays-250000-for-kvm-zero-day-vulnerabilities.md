Title: Google now pays $250,000 for KVM zero-day vulnerabilities
Date: 2024-07-02T14:06:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2024-07-02-google-now-pays-250000-for-kvm-zero-day-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/google-now-pays-250-000-for-kvm-zero-day-vulnerabilities/){:target="_blank" rel="noopener"}

> Google has launched kvmCTF, a new vulnerability reward program (VRP) first announced in October 2023 to improve the security of the Kernel-based Virtual Machine (KVM) hypervisor that comes with $250,000 bounties for full VM escape exploits. [...]
