Title: Cloudflare blames recent outage on BGP hijacking incident
Date: 2024-07-05T14:41:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-07-05-cloudflare-blames-recent-outage-on-bgp-hijacking-incident

[Source](https://www.bleepingcomputer.com/news/security/cloudflare-blames-recent-outage-on-bgp-hijacking-incident/){:target="_blank" rel="noopener"}

> Internet giant Cloudflare reports that its DNS resolver service, 1.1.1.1, was recently unreachable or degraded for some of its customers because of a combination of Border Gateway Protocol (BGP) hijacking and a route leak. [...]
