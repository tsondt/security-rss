Title: A decade after collapsing, crypto exchange Mt Gox repays some investors
Date: 2024-07-08T00:44:52+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-07-08-a-decade-after-collapsing-crypto-exchange-mt-gox-repays-some-investors

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/08/asia_tech_news_roundup/){:target="_blank" rel="noopener"}

> Plus: Samsung strike; India likely upping chip subsidies; Asian nations link payment schemes Asia In Brief Mt Gox, the Japanese crypto exchange that dominated trading for a brief time in the early 2010s before collapsing amid the disappearance of nearly half a billion dollars worth of the digicash, likely as a result of its own shoddy software, has said it will start to repay some investors – in Bitcoin.... [...]
