Title: More than 83K certs from nearly 7K DigiCert customers must be swapped out now
Date: 2024-07-31T21:15:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-07-31-more-than-83k-certs-from-nearly-7k-digicert-customers-must-be-swapped-out-now

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/31/digicert_certificates_extension/){:target="_blank" rel="noopener"}

> Small stay of execution in 'exceptional circumstances' promised as lawsuits start to fly As the DigiCert drama continues, we now have a better idea of the size and scope of the problem – with the organization's infosec boss admitting the SSL/TLS certificate revocation sweep will affect tens of thousands of its customers, some of which have warned that the short notice may have real-world safety implications and disrupt critical services.... [...]
