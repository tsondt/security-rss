Title: Advance Auto Parts data breach impacts 2.3 million people
Date: 2024-07-11T10:17:21-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-07-11-advance-auto-parts-data-breach-impacts-23-million-people

[Source](https://www.bleepingcomputer.com/news/security/advance-auto-parts-data-breach-impacts-23-million-people/){:target="_blank" rel="noopener"}

> Advance Auto Parts is sending data breach notifications to over 2.3 million people whose personal data was stolen in recent Snowflake data theft attacks. [...]
