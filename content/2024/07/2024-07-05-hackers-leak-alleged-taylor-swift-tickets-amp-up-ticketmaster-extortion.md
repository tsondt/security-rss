Title: Hackers leak alleged Taylor Swift tickets, amp up Ticketmaster extortion
Date: 2024-07-05T13:05:30-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-07-05-hackers-leak-alleged-taylor-swift-tickets-amp-up-ticketmaster-extortion

[Source](https://www.bleepingcomputer.com/news/security/hackers-leak-alleged-taylor-swift-tickets-amp-up-ticketmaster-extortion/){:target="_blank" rel="noopener"}

> Hackers have leaked what they claim is Ticketmaster barcode data for 166,000 Taylor Swift Eras Tour tickets, warning that more events would be leaked if a $2 million extortion demand is not paid. [...]
