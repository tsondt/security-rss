Title: Navigating Europe’s digital identity crossroads
Date: 2024-07-08T08:54:08+00:00
Author: Contributed by Signicat
Category: The Register
Tags: 
Slug: 2024-07-08-navigating-europes-digital-identity-crossroads

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/08/navigating_europes_digital_identity_crossroads/){:target="_blank" rel="noopener"}

> How to get ready for the future of digital identity in the European Union from eIDAS 1.0 to eIDAS 2.0 and beyond Partner Content : Opening a bank account, making or receiving a payment, instructing an accountant or booking a doctor's appointment. These everyday tasks depend on identity, either proving who you are or verifying who you're dealing with.... [...]
