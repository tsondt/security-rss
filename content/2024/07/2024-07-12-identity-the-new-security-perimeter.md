Title: Identity: the new security perimeter
Date: 2024-07-12T14:25:09+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2024-07-12-identity-the-new-security-perimeter

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/12/identity_the_new_security_perimeter/){:target="_blank" rel="noopener"}

> What to do when your MFA is mercilessly attacked by hackers Webinar Threat actors are always looking for that easy way in by testing weak spots, and user identities are one of their favourite targets.... [...]
