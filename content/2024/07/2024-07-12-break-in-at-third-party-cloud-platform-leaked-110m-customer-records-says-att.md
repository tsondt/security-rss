Title: Break-in at 'third-party cloud platform' leaked 110M customer records, says AT&amp;T
Date: 2024-07-12T14:09:27+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-07-12-break-in-at-third-party-cloud-platform-leaked-110m-customer-records-says-att

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/12/att_admits_110_million_ppl_data_lost/){:target="_blank" rel="noopener"}

> Snowflake? Snowflake AT&T has admitted that cyberattackers grabbed a load of its data for the second time this year, and if you think the first haul was big you haven't seen anything: This one includes data on "nearly all" AT&T wireless customers - and those served by mobile virtual network operators (MVNOs) running on AT&T's network.... [...]
