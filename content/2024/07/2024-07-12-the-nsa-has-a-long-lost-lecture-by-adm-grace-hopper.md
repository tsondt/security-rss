Title: The NSA Has a Long-Lost Lecture by Adm. Grace Hopper
Date: 2024-07-12T11:04:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;history of security;NSA;video
Slug: 2024-07-12-the-nsa-has-a-long-lost-lecture-by-adm-grace-hopper

[Source](https://www.schneier.com/blog/archives/2024/07/the-nsa-has-a-long-lost-lecture-by-adm-grace-hopper.html){:target="_blank" rel="noopener"}

> The NSA has a video recording of a 1982 lecture by Adm. Grace Hopper titled “Future Possibilities: Data, Hardware, Software, and People.” The agency is (so far) refusing to release it. Basically, the recording is in an obscure video format. People at the NSA can’t easily watch it, so they can’t redact it. So they won’t do anything. With digital obsolescence threatening many early technological formats, the dilemma surrounding Admiral Hopper’s lecture underscores the critical need for and challenge of digital preservation. This challenge transcends the confines of NSA’s operational scope. It is our shared obligation to safeguard such pivotal [...]
