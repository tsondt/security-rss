Title: Scammers double-scam victims by offering to help recover from scams
Date: 2024-07-09T05:58:15+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-07-09-scammers-double-scam-victims-by-offering-to-help-recover-from-scams

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/09/australia_rescam_warning/){:target="_blank" rel="noopener"}

> Scum keep databases of the people they've already skimmed Australia's Competition and Consumer Commission has warned that scammers are targeting scam victims with fake offers to help them recover from scams.... [...]
