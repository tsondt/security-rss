Title: Indonesian government didn't have backups of ransomwared data, because DR was only an option
Date: 2024-07-01T04:56:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-07-01-indonesian-government-didnt-have-backups-of-ransomwared-data-because-dr-was-only-an-option

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/01/indonesian_president_orders_datacenter_audit/){:target="_blank" rel="noopener"}

> President has ordered a datacenter audit and made backups mandatory Indonesia’s president Joko Widodo has ordered an audit of government datacenters after it was revealed that most of the data they store is not backed up.... [...]
