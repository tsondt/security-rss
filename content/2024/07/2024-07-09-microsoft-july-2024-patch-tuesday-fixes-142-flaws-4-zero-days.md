Title: Microsoft July 2024 Patch Tuesday fixes 142 flaws, 4 zero-days
Date: 2024-07-09T13:52:01-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-07-09-microsoft-july-2024-patch-tuesday-fixes-142-flaws-4-zero-days

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-july-2024-patch-tuesday-fixes-142-flaws-4-zero-days/){:target="_blank" rel="noopener"}

> Today is Microsoft's July 2024 Patch Tuesday, which includes security updates for 142 flaws, including two actively exploited and two publicly disclosed zero-days. [...]
