Title: Phish-Friendly Domain Registry “.top” Put on Notice
Date: 2024-07-23T19:41:51+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;The Coming Storm;.top;Anti-Phishing Working Group;Dave Piscitello;Interisle Consulting Group;Internet Corporation for Assigned Names and Numbers;Jiangsu Bangning Science & Technology Co. Ltd
Slug: 2024-07-23-phish-friendly-domain-registry-top-put-on-notice

[Source](https://krebsonsecurity.com/2024/07/phish-friendly-domain-registry-top-put-on-notice/){:target="_blank" rel="noopener"}

> The Chinese company in charge of handing out domain names ending in “.top ” has been given until mid-August 2024 to show that it has put in place systems for managing phishing reports and suspending abusive domains, or else forfeit its license to sell domains. The warning comes amid the release of new findings that.top was the most common suffix in phishing websites over the past year, second only to domains ending in “.com.” Image: Shutterstock. On July 16, the Internet Corporation for Assigned Names and Numbers (ICANN) sent a letter to the owners of the.top domain registry. ICANN has [...]
