Title: Secure Boot useless on hundreds of PCs from major vendors after key leak
Date: 2024-07-29T01:58:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-07-29-secure-boot-useless-on-hundreds-of-pcs-from-major-vendors-after-key-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/29/infosec_roundup/){:target="_blank" rel="noopener"}

> Plus: More stalkerware exposure; a $16M TracFone fine; Ransomware victims don't use MFA, and more Infosec in brief Protecting computers' BIOS and the boot process is essential for modern security – but knowing it's important isn't the same as actually taking steps to do it.... [...]
