Title: Modern SecOps Masterclass: Now Available on Coursera
Date: 2024-07-18T16:00:00+00:00
Author: Anton Chuvakin
Category: GCP Security
Tags: Training and Certifications;Security & Identity
Slug: 2024-07-18-modern-secops-masterclass-now-available-on-coursera

[Source](https://cloud.google.com/blog/products/identity-security/modern-secops-masterclass-now-available-on-coursera/){:target="_blank" rel="noopener"}

> Security practitioners constantly need to rethink and refine their approaches to defending their organization. Staying ahead requires innovation, continuous improvement, and a mindset shift away from siloed operations into building end-to-end solutions against threats. Today, Google Cloud is excited to announce the launch of the Modern SecOps (MSO) course, a six-week, platform-agnostic education program designed to equip security professionals with the latest skills and knowledge to help modernize their security operations, based on our Autonomic Security Operations framework and Continuous Detection, Continuous Response (CD/CR) methodology. Introducing Modern Security Operations Course The Modern Security Operations course provides a comprehensive curriculum that [...]
