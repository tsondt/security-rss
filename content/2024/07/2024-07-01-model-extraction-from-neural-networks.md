Title: Model Extraction from Neural Networks
Date: 2024-07-01T11:05:25+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;cryptography
Slug: 2024-07-01-model-extraction-from-neural-networks

[Source](https://www.schneier.com/blog/archives/2024/07/model-extraction-from-neural-networks.html){:target="_blank" rel="noopener"}

> A new paper, “Polynomial Time Cryptanalytic Extraction of Neural Network Models,” by Adi Shamir and others, uses ideas from differential cryptanalysis to extract the weights inside a neural network using specific queries and their results. This is much more theoretical than practical, but it’s a really interesting result. Abstract: Billions of dollars and countless GPU hours are currently spent on training Deep Neural Networks (DNNs) for a variety of tasks. Thus, it is essential to determine the difficulty of extracting all the parameters of such neural networks when given access to their black-box implementations. Many versions of this problem have [...]
