Title: Banks in Singapore to phase out one-time passwords in 3 months
Date: 2024-07-14T10:18:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-07-14-banks-in-singapore-to-phase-out-one-time-passwords-in-3-months

[Source](https://www.bleepingcomputer.com/news/security/banks-in-singapore-to-phase-out-one-time-passwords-in-3-months/){:target="_blank" rel="noopener"}

> The Monetary Authority of Singapore (MAS) has announced a new requirement impacting all major retail banks in the country to phase out the use of one-time passwords (OTPs) within the next three months. [...]
