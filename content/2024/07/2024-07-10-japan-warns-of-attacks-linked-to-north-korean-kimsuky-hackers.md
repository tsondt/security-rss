Title: Japan warns of attacks linked to North Korean Kimsuky hackers
Date: 2024-07-10T13:10:32-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-07-10-japan-warns-of-attacks-linked-to-north-korean-kimsuky-hackers

[Source](https://www.bleepingcomputer.com/news/security/japan-warns-of-attacks-linked-to-north-korean-kimsuky-hackers/){:target="_blank" rel="noopener"}

> Japan's Computer Emergency Response Team Coordination Center (JPCERT/CC) is warning that Japanese organizations are being targeted in attacks by the North Korean 'Kimsuky' threat actors. [...]
