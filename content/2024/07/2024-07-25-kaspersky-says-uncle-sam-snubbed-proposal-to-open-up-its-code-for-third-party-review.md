Title: Kaspersky says Uncle Sam snubbed proposal to open up its code for third-party review
Date: 2024-07-25T12:01:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-07-25-kaspersky-says-uncle-sam-snubbed-proposal-to-open-up-its-code-for-third-party-review

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/25/kaspersky_us_review_snub/){:target="_blank" rel="noopener"}

> Those national security threat claims? 'No evidence,' VP tells The Reg Exclusive Despite the Feds' determination to ban Kaspersky's security software in the US, the Russian business continues to push its proposal to open up its data and products to independent third-party review – and prove to Uncle Sam that its code hasn't been and won't be compromised by Kremlin spies.... [...]
