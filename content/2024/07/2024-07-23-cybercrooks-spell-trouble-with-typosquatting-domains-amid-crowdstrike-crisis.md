Title: Cybercrooks spell trouble with typosquatting domains amid CrowdStrike crisis
Date: 2024-07-23T15:15:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-23-cybercrooks-spell-trouble-with-typosquatting-domains-amid-crowdstrike-crisis

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/23/typosquatting_crowdstrike_crisis/){:target="_blank" rel="noopener"}

> Latest trend follows various malware campaigns that began just hours after IT calamity Thousands of typosquatting domains are now registered to exploit the desperation of IT admins still struggling to recover from last week's CrowdStrike outage, researchers say.... [...]
