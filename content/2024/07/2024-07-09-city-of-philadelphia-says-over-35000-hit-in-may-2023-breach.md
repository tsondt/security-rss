Title: City of Philadelphia says over 35,000 hit in May 2023 breach
Date: 2024-07-09T12:55:52-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-09-city-of-philadelphia-says-over-35000-hit-in-may-2023-breach

[Source](https://www.bleepingcomputer.com/news/security/city-of-philadelphia-says-over-35-000-hit-in-may-2023-breach/){:target="_blank" rel="noopener"}

> The City of Philadelphia revealed that a May 2024 disclosed in October impacted more than 35,000 individuals' personal and protected health information. [...]
