Title: Hackers leak 39,000 print-at-home Ticketmaster tickets for 154 events
Date: 2024-07-08T17:39:54-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-07-08-hackers-leak-39000-print-at-home-ticketmaster-tickets-for-154-events

[Source](https://www.bleepingcomputer.com/news/security/hackers-leak-39-000-print-at-home-ticketmaster-tickets-for-154-events/){:target="_blank" rel="noopener"}

> In an ongoing extortion campaign against Ticketmaster, threat actors have leaked almost 39,000 print-at-home tickets for 150 upcoming concerts and events, including Pearl Jam, Phish, Tate McCrae, and Foo Fighters. [...]
