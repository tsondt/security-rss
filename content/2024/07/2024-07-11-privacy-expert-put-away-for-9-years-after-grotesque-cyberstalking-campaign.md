Title: Privacy expert put away for 9 years after 'grotesque' cyberstalking campaign
Date: 2024-07-11T10:29:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-07-11-privacy-expert-put-away-for-9-years-after-grotesque-cyberstalking-campaign

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/11/cyberstalker_jailed/){:target="_blank" rel="noopener"}

> Scumbag targeted many victims – and those who tried to help them A scumbag who used to work as a privacy consultant has been put behind bars for nine years for a "grotesque" cyberstalking campaign against more than a dozen victims.... [...]
