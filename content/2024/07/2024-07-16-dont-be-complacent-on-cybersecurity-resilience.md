Title: Don’t be complacent on cybersecurity resilience
Date: 2024-07-16T14:21:13+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2024-07-16-dont-be-complacent-on-cybersecurity-resilience

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/16/dont_be_complacent_on_cybersecurity/){:target="_blank" rel="noopener"}

> Read the 2024 Cisco Cybersecurity Readiness Index for tips on how best to prepare Sponsored Post Protecting sensitive data and mission-critical applications, systems and services from the unwanted attention of hackers and cyber criminals is never easy.... [...]
