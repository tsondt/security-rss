Title: DNS hijacks target crypto platforms registered with Squarespace
Date: 2024-07-12T14:28:28-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-07-12-dns-hijacks-target-crypto-platforms-registered-with-squarespace

[Source](https://www.bleepingcomputer.com/news/security/dns-hijacks-target-crypto-platforms-registered-with-squarespace/){:target="_blank" rel="noopener"}

> A wave of coordinated DNS hijacking attacks targets decentralized finance (DeFi) cryptocurrency domains using the Squarespace registrar, redirecting visitors to phishing sites hosting wallet drainers. [...]
