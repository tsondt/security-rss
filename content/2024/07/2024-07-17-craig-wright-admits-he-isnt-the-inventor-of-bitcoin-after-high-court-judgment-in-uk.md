Title: Craig Wright admits he isn't the inventor of Bitcoin after High Court judgment in UK
Date: 2024-07-17T07:33:05+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-07-17-craig-wright-admits-he-isnt-the-inventor-of-bitcoin-after-high-court-judgment-in-uk

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/17/craig_wright_isnt_bitcoins_satoshi_nakamoto/){:target="_blank" rel="noopener"}

> Aussie definitely not Satoshi Nakamoto, faces £6M legal bill and possible perjury trial Australian Craig Wright has finally admitted he is not the inventor of Bitcoin after losing several cases in the High Court of England and Wales, whose judge has suggested he be investigated for perjury.... [...]
