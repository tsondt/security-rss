Title: Affirm says cardholders impacted by Evolve Bank data breach
Date: 2024-07-02T11:57:39-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-07-02-affirm-says-cardholders-impacted-by-evolve-bank-data-breach

[Source](https://www.bleepingcomputer.com/news/security/affirm-says-cardholders-impacted-by-evolve-bank-data-breach/){:target="_blank" rel="noopener"}

> Buy now, pay later loan company Affirm is warning that holders of its payment cards had their personal information exposed due to a data breach at its third-party issuer, Evolve Bank & Trust (Evolve). [...]
