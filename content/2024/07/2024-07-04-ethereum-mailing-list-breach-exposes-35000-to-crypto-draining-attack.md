Title: Ethereum mailing list breach exposes 35,000 to crypto draining attack
Date: 2024-07-04T12:17:29-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-07-04-ethereum-mailing-list-breach-exposes-35000-to-crypto-draining-attack

[Source](https://www.bleepingcomputer.com/news/security/ethereum-mailing-list-breach-exposes-35-000-to-crypto-draining-attack/){:target="_blank" rel="noopener"}

> A threat actor compromised Ethereum's mailing list provider and sent to over 35,000 addresses a phishing email with a link to a malicious site running a crypto drainer. [...]
