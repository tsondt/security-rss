Title: Proton launches free, privacy-focused Google Docs alternative
Date: 2024-07-03T06:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Software;Cloud;Security
Slug: 2024-07-03-proton-launches-free-privacy-focused-google-docs-alternative

[Source](https://www.bleepingcomputer.com/news/software/proton-launches-free-privacy-focused-google-docs-alternative/){:target="_blank" rel="noopener"}

> Proton has launched 'Docs in Proton Drive,' a free and open-source end-to-end encrypted web-based document editing and collaboration tool. [...]
