Title: Kaspersky challenges US government to put up or shut up about Kremlin ties
Date: 2024-07-18T16:29:05+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-07-18-kaspersky-challenges-us-government-to-put-up-or-shut-up-about-kremlin-ties

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/18/kaspersky_us_government/){:target="_blank" rel="noopener"}

> Stick an independent probe in our software, you won't find any Putin.DLL backdoor Kaspersky has hit back after the US government banned its products – by proposing an independent verification that its software is above board and not backdoored by the Kremlin.... [...]
