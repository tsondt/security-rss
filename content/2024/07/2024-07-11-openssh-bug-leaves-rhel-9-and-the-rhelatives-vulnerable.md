Title: OpenSSH bug leaves RHEL 9 and the RHELatives vulnerable
Date: 2024-07-11T19:13:08+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2024-07-11-openssh-bug-leaves-rhel-9-and-the-rhelatives-vulnerable

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/11/openssh_bug_in_rhel_9/){:target="_blank" rel="noopener"}

> Newly discovered flaw affects OpenSSH 8.7 and 8.8 daemon The founder of Openwall has discovered a new signal handler race condition in the core sshd daemon used in RHEL 9.x and its various offshoots.... [...]
