Title: Researchers: Weak Security Defaults Enabled Squarespace Domains Hijacks
Date: 2024-07-15T15:24:46+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Latest Warnings;Web Fraud 2.0;Google Domains;MetaMask;Paradigm;Squarespace;Taylor Monahan
Slug: 2024-07-15-researchers-weak-security-defaults-enabled-squarespace-domains-hijacks

[Source](https://krebsonsecurity.com/2024/07/researchers-weak-security-defaults-enabled-squarespace-domains-hijacks/){:target="_blank" rel="noopener"}

> At least a dozen organizations with domain names at domain registrar Squarespace saw their websites hijacked last week. Squarespace bought all assets of Google Domains a year ago, but many customers still haven’t set up their new accounts. Experts say malicious hackers learned they could commandeer any migrated Squarespace accounts that hadn’t yet been registered, merely by supplying an email address tied to an existing domain. Until this past weekend, Squarespace’s website had an option to log in via email. The Squarespace domain hijacks, which took place between July 9 and July 12, appear to have mostly targeted cryptocurrency businesses, [...]
