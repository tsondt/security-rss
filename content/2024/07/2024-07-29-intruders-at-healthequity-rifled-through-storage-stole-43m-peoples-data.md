Title: Intruders at HealthEquity rifled through storage, stole 4.3M people's data
Date: 2024-07-29T13:45:41+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-29-intruders-at-healthequity-rifled-through-storage-stole-43m-peoples-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/29/healthequity_says_data_breach_affects/){:target="_blank" rel="noopener"}

> No mention of malware or ransomware – somewhat of a rarity these days HealthEquity, a US fintech firm for the healthcare sector, admits that a "data security event" it discovered at the end of June hit the data of a substantial 4.3 million individuals. Stolen details include addresses, telephone numbers and payment data.... [...]
