Title: Affirm fears customer info pilfered during ransomware raid at Evolve Bank
Date: 2024-07-02T13:16:04+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-02-affirm-fears-customer-info-pilfered-during-ransomware-raid-at-evolve-bank

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/02/affirm_evolve_ransomware_breach/){:target="_blank" rel="noopener"}

> Number of partners acknowledging data theft continues to rise The number of financial institutions caught up in the ransomware attack on Evolve Bank & Trust continues to rise as fintech businesses Wise and Affirm both confirm they have been materially affected.... [...]
