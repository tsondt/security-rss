Title: CrowdStrike update blunder may cost world billions – and insurance ain't covering it all
Date: 2024-07-26T00:35:11+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-07-26-crowdstrike-update-blunder-may-cost-world-billions-and-insurance-aint-covering-it-all

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/26/crowdstrike_insurance_money/){:target="_blank" rel="noopener"}

> We offer this formula instead: RND(100.0)*(10^9) The cost of CrowdStrike's apocalyptic Falcon update that brought down millions of Windows computers last week may be in the billions of dollars, and insurance isn't covering most of that.... [...]
