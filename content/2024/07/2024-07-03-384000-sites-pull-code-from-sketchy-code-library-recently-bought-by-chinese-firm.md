Title: 384,000 sites pull code from sketchy code library recently bought by Chinese firm
Date: 2024-07-03T19:36:04+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security
Slug: 2024-07-03-384000-sites-pull-code-from-sketchy-code-library-recently-bought-by-chinese-firm

[Source](https://arstechnica.com/?p=2035216){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) More than 384,000 websites are linking to a site that was caught last week performing a supply-chain attack that redirected visitors to malicious sites, researchers said. For years, the JavaScript code, hosted at polyfill[.]com, was a legitimate open source project that allowed older browsers to handle advanced functions that weren’t natively supported. By linking to cdn.polyfill[.]io, websites could ensure that devices using legacy browsers could render content in newer formats. The free service was popular among websites because all they had to do was embed the link in their sites. The code hosted on the polyfill [...]
