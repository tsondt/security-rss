Title: FBI, cyber-cops zap ~1K Russian AI disinfo Twitter bots
Date: 2024-07-09T23:35:28+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-07-09-fbi-cyber-cops-zap-1k-russian-ai-disinfo-twitter-bots

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/09/russian_ai_bot_farm/){:target="_blank" rel="noopener"}

> RT News snarks back after it's accused of building social nyet-work for Kremlin The FBI and cybersecurity agencies in Canada and the Netherlands say they have taken down an almost 1,000-strong Twitter bot farm set up by Russian state-run RT News that used generative AI to spread disinformation to Americans and others.... [...]
