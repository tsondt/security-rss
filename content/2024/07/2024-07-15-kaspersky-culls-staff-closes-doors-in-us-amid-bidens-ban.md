Title: Kaspersky culls staff, closes doors in US amid Biden's ban
Date: 2024-07-15T21:32:15+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-07-15-kaspersky-culls-staff-closes-doors-in-us-amid-bidens-ban

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/15/kasperky_us_operations/){:target="_blank" rel="noopener"}

> After all we've done for you, America, sniffs antivirus lab Kaspersky has confirmed it will shutter its American operations and cut US-based jobs following President Biden's ban on the Russian business last month.... [...]
