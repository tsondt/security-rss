Title: Fujitsu confirms customer data exposed in March cyberattack
Date: 2024-07-09T15:34:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-07-09-fujitsu-confirms-customer-data-exposed-in-march-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/fujitsu-confirms-customer-data-exposed-in-march-cyberattack/){:target="_blank" rel="noopener"}

> Fujitsu confirms that information related to some individuals and customers' business has been compromised during the data breach detected earlier this year. [...]
