Title: Microsoft forgets about SwiftKey's support site
Date: 2024-07-08T14:12:11+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-07-08-microsoft-forgets-about-swiftkeys-support-site

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/08/microsoft_swiftkeys_cert_expires/){:target="_blank" rel="noopener"}

> Injecting Copilot branding will not make TLS certificates auto-renew Another Microsoft certificate has expired, leaving SwiftKey users that are seeking support faced with an alarming certificate error.... [...]
