Title: Critical Windows licensing bugs – plus two others under attack – top Patch Tuesday
Date: 2024-07-10T00:59:17+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-07-10-critical-windows-licensing-bugs-plus-two-others-under-attack-top-patch-tuesday

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/10/july_2024_patch_tuesday/){:target="_blank" rel="noopener"}

> Citrix, SAP also deserve your attention – because miscreants are already thinking about Exploit Wednesday Patch Tuesday Clear your Microsoft system administrator's diary: The bundle of fixes in Redmond's July Patch Tuesday is a doozy, with at least two bugs under active exploitation.... [...]
