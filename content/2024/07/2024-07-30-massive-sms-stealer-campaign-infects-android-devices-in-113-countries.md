Title: Massive SMS stealer campaign infects Android devices in 113 countries
Date: 2024-07-30T17:29:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2024-07-30-massive-sms-stealer-campaign-infects-android-devices-in-113-countries

[Source](https://www.bleepingcomputer.com/news/security/massive-sms-stealer-campaign-infects-android-devices-in-113-countries/){:target="_blank" rel="noopener"}

> A malicious campaign targeting Android devices worldwide utilizes thousands of Telegram bots to infect devices with SMS-stealing malware and steal one-time 2FA passwords (OTPs) for over 600 services. [...]
