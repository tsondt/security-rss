Title: Cloud CISO Perspectives: How to think about security budgets
Date: 2024-07-15T16:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2024-07-15-cloud-ciso-perspectives-how-to-think-about-security-budgets

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-how-to-think-about-security-budgets/){:target="_blank" rel="noopener"}

> Welcome to the first Cloud CISO Perspectives for July 2024. Today I’m addressing the important and complex issue of budgets for security teams, a topic I’ve tackled on my personal blog that I’m presenting here for the first time. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI Security & CISO, Google Cloud aside_block <ListValue: [StructValue([('title', 'Get vital CISO Insights with Google Cloud'), ('body', <wagtail.rich_text.RichText object at 0x3e4b1bb71d90>), [...]
