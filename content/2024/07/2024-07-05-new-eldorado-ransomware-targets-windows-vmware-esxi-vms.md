Title: New Eldorado ransomware targets Windows, VMware ESXi VMs
Date: 2024-07-05T11:56:01-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-07-05-new-eldorado-ransomware-targets-windows-vmware-esxi-vms

[Source](https://www.bleepingcomputer.com/news/security/new-eldorado-ransomware-targets-windows-vmware-esxi-vms/){:target="_blank" rel="noopener"}

> A new ransomware-as-a-service (RaaS) called Eldorado emerged in March and comes with locker variants for VMware ESXi and Windows. [...]
