Title: 'LockBit of phishing' EvilProxy used in more than a million attacks every month
Date: 2024-07-30T14:33:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-07-30-lockbit-of-phishing-evilproxy-used-in-more-than-a-million-attacks-every-month

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/30/evilproxy_phishing_kit_analysis/){:target="_blank" rel="noopener"}

> Leaves a trail of ransomware infections, data theft, business email compromise in its wake Insight The developers of EvilProxy – a phishing kit dubbed the "LockBit of phishing" – have produced guides on using legitimate Cloudflare services to disguise malicious traffic. This adds to the ever-growing arsenal of tools offering criminals who lack actual technical expertise to get into the digital thievery biz.... [...]
