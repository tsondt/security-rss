Title: World leading silver producer Fresnillo discloses cyberattack
Date: 2024-07-31T12:32:49-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-31-world-leading-silver-producer-fresnillo-discloses-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/world-leading-silver-producer-fresnillo-discloses-cyberattack/){:target="_blank" rel="noopener"}

> ​Fresnillo PLC, the world's largest silver producer and a top global producer of gold, copper, and zinc, said attackers gained access to data stored on its systems during a recent cyberattack. [...]
