Title: CISA and FBI: DDoS attacks won’t impact US election integrity
Date: 2024-07-31T13:50:52-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Government
Slug: 2024-07-31-cisa-and-fbi-ddos-attacks-wont-impact-us-election-integrity

[Source](https://www.bleepingcomputer.com/news/security/cisa-and-fbi-ddos-attacks-wont-impact-us-election-integrity/){:target="_blank" rel="noopener"}

> ​CISA and the FBI said today that Distributed Denial of Service (DDoS) attacks targeting election infrastructure will, at most, hinder public access to information but will have no impact on the integrity or security of the 2024 U.S. general election processes. [...]
