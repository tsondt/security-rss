Title: Microsoft links Scattered Spider hackers to Qilin ransomware attacks
Date: 2024-07-16T09:40:58-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-07-16-microsoft-links-scattered-spider-hackers-to-qilin-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-links-scattered-spider-hackers-to-qilin-ransomware-attacks/){:target="_blank" rel="noopener"}

> Microsoft says the Scattered Spider cybercrime gang has added Qilin ransomware to its arsenal and is now using it in attacks. [...]
