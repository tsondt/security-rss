Title: Reverse-Engineering Ticketmaster’s Barcode System
Date: 2024-07-09T16:27:46+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;hacking;reverse engineering
Slug: 2024-07-09-reverse-engineering-ticketmasters-barcode-system

[Source](https://www.schneier.com/blog/archives/2024/07/reverse-engineering-ticketmasters-barcode-system.html){:target="_blank" rel="noopener"}

> Interesting : By reverse-engineering how Ticketmaster and AXS actually make their electronic tickets, scalpers have essentially figured out how to regenerate specific, genuine tickets that they have legally purchased from scratch onto infrastructure that they control. In doing so, they are removing the anti-scalping restrictions put on the tickets by Ticketmaster and AXS. [...]
