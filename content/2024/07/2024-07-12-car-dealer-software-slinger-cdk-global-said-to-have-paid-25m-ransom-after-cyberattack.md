Title: Car dealer software slinger CDK Global said to have paid $25M ransom after cyberattack
Date: 2024-07-12T23:53:31+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-07-12-car-dealer-software-slinger-cdk-global-said-to-have-paid-25m-ransom-after-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/12/cdk_ransom_payout/){:target="_blank" rel="noopener"}

> 15K dealerships take estimated $600M+ hit CDK Global reportedly paid a $25 million ransom in Bitcoin after its servers were knocked offline by crippling ransomware.... [...]
