Title: Rite Aid confirms data breach after June ransomware attack
Date: 2024-07-12T14:49:07-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-12-rite-aid-confirms-data-breach-after-june-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/rite-aid-confirms-data-breach-after-june-ransomware-attack/){:target="_blank" rel="noopener"}

> Pharmacy giant Rite Aid confirmed a data breach after suffering a cyberattack in June, which was claimed by the RansomHub ransomware operation. [...]
