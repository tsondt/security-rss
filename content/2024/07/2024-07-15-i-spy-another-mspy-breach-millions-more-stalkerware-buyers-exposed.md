Title: I spy another mSpy breach: Millions more stalkerware buyers exposed
Date: 2024-07-15T02:01:14+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-07-15-i-spy-another-mspy-breach-millions-more-stalkerware-buyers-exposed

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/15/infosec_roundup/){:target="_blank" rel="noopener"}

> Also: Velops routers love plaintext; everything is a dark pattern; Internet Explorer rises from the grave, and more Infosec in brief Commercial spyware maker mSpy has been breached – again – and millions of purchasers can be identified from the spilled records.... [...]
