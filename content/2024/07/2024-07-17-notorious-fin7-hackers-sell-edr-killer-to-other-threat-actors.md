Title: Notorious FIN7 hackers sell EDR killer to other threat actors
Date: 2024-07-17T17:11:09-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-07-17-notorious-fin7-hackers-sell-edr-killer-to-other-threat-actors

[Source](https://www.bleepingcomputer.com/news/security/notorious-fin7-hackers-sell-edr-killer-to-other-threat-actors/){:target="_blank" rel="noopener"}

> The notorious FIN7 hacking group has been spotted selling its custom "AvNeutralizer" tool, used to evade detection by killing enterprise endpoint protection software on corporate networks. [...]
