Title: Global cops power down world's 'most prolific' DDoS dealership
Date: 2024-07-22T20:15:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-22-global-cops-power-down-worlds-most-prolific-ddos-dealership

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/22/ddos_for_hire_shutdown/){:target="_blank" rel="noopener"}

> One arrest was made weeks ago but no word on the suspect's identity yet A DDoS-for-hire site described by the UK's National Crime Agency (NCA) as the world's most prolific operator in the field is out-of-action following a law enforcement sting dubbed Operation Power Off.... [...]
