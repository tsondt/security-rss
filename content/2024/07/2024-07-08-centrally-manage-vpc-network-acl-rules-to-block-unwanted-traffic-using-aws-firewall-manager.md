Title: Centrally manage VPC network ACL rules to block unwanted traffic using AWS Firewall Manager
Date: 2024-07-08T12:54:24+00:00
Author: Bryan Van Hook
Category: AWS Security
Tags: Advanced (300);AWS Firewall Manager;Security, Identity, & Compliance;Technical How-to;content delivery network;Network and content delivery;Networking;Security;Security Blog
Slug: 2024-07-08-centrally-manage-vpc-network-acl-rules-to-block-unwanted-traffic-using-aws-firewall-manager

[Source](https://aws.amazon.com/blogs/security/centrally-manage-vpc-network-acl-rules-to-block-unwanted-traffic-using-aws-firewall-manager/){:target="_blank" rel="noopener"}

> Amazon Virtual Private Cloud (Amazon VPC) provides two options for controlling network traffic: network access control lists (ACLs) and security groups. A network ACL defines inbound and outbound rules that allow or deny traffic based on protocol, IP address range, and port range. Security groups determine which inbound and outbound traffic is allowed on a [...]
