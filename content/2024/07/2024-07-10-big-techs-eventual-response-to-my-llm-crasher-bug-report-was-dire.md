Title: Big Tech's eventual response to my LLM-crasher bug report was dire
Date: 2024-07-10T07:25:06+00:00
Author: Mark Pesce
Category: The Register
Tags: 
Slug: 2024-07-10-big-techs-eventual-response-to-my-llm-crasher-bug-report-was-dire

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/10/vendors_response_to_my_llmcrasher/){:target="_blank" rel="noopener"}

> Fixes have been made, it appears, but disclosure or discussion is invisible Column Found a bug? It turns out that reporting it with a story in The Register works remarkably well... mostly. After publication of my "Kryptonite" article about a prompt that crashes many AI chatbots, I began to get a steady stream of emails from readers – many times the total of all reader emails I'd received in the previous decade.... [...]
