Title: Uncle Sam opens probe into CrowdStrike turbulence at Delta Air Lines
Date: 2024-07-24T17:02:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-24-uncle-sam-opens-probe-into-crowdstrike-turbulence-at-delta-air-lines

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/24/transport_department_delta_probe/){:target="_blank" rel="noopener"}

> Concerns abound over why it has taken so long to recover compared to competitors The US Department of Transportation (DoT) is investigating Delta Air Lines over its handling of the global IT outage caused by CrowdStrike's content update.... [...]
