Title: Data pilfered from Pentagon IT supplier Leidos
Date: 2024-07-24T13:31:08+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2024-07-24-data-pilfered-from-pentagon-it-supplier-leidos

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/24/leidos_data_leak/){:target="_blank" rel="noopener"}

> With numerous US government agency customers, any leak could be serious Updated Internal documents stolen from Leidos Holdings, an IT services provider contracted with the Department of Defense and other US government agencies, have been leaked on the dark web.... [...]
