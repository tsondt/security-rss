Title: Upcoming Book on AI and Democracy
Date: 2024-07-02T18:11:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;books;democracy;Schneier news
Slug: 2024-07-02-upcoming-book-on-ai-and-democracy

[Source](https://www.schneier.com/blog/archives/2024/07/upcoming-book-on-ai-and-democracy.html){:target="_blank" rel="noopener"}

> If you’ve been reading my blog, you’ve noticed that I have written a lot about AI and democracy, mostly with my co-author Nathan Sanders. I am pleased to announce that we’re writing a book on the topic. This isn’t a book about deep fakes, or misinformation. This is a book about what happens when AI writes laws, adjudicates disputes, audits bureaucratic actions, assists in political strategy, and advises citizens on what candidates and issues to support. It’s a book that tries to look into what an AI-assisted democratic system might look like, and then at how to best ensure that [...]
