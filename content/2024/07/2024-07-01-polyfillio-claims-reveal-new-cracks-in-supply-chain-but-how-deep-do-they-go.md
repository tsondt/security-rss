Title: Polyfill.io claims reveal new cracks in supply chain, but how deep do they go?
Date: 2024-07-01T10:32:08+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2024-07-01-polyfillio-claims-reveal-new-cracks-in-supply-chain-but-how-deep-do-they-go

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/01/polyfill_io_supply_chain/){:target="_blank" rel="noopener"}

> There will always be bad actors in the system. We can always learn from the drama they create Opinion Libraries. Hushed temples to the civilizing power of knowledge, or launchpads of global destruction? Yep, another word tech has borrowed and debased. Code libraries are essential for adding just the right standard tested functionality to a project. They're also a natural home for supply chain attacks that materialize malware in the heart of the enterprise like shock troops of Klingons arriving by transporter beam.... [...]
