Title: China's APT40 gang is ready to attack vulns within hours or days of public release
Date: 2024-07-09T02:33:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-07-09-chinas-apt40-gang-is-ready-to-attack-vulns-within-hours-or-days-of-public-release

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/09/apt_40_tradecraft_advisory/){:target="_blank" rel="noopener"}

> Lax patching and vulnerable small biz kit make life easy for Beijing's secret-stealers Law enforcement agencies from eight nations, led by Australia, have issued an advisory that details the tradecraft used by China-aligned threat actor APT40 – aka Kryptonite Panda, GINGHAM TYPHOON, Leviathan and Bronze Mohawk – and found it prioritizes developing exploits for newly found vulnerabilities and can target them within hours.... [...]
