Title: Post-CrowdStrike, Microsoft to discourage use of kernel drivers by security tools
Date: 2024-07-29T06:30:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-07-29-post-crowdstrike-microsoft-to-discourage-use-of-kernel-drivers-by-security-tools

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/29/microsoft_crowdstrike_kernel_mode/){:target="_blank" rel="noopener"}

> Now there's an idea – parsing config data in user mode Updated Microsoft has vowed to reduce cybersecurity vendors' reliance on kernel-mode code, which was at the heart of the CrowdStrike super-snafu this month.... [...]
