Title: Signal downplays encryption key flaw, fixes it after X drama
Date: 2024-07-11T16:49:49-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-07-11-signal-downplays-encryption-key-flaw-fixes-it-after-x-drama

[Source](https://www.bleepingcomputer.com/news/security/signal-downplays-encryption-key-flaw-fixes-it-after-x-drama/){:target="_blank" rel="noopener"}

> Signal is finally tightening its desktop client's security by changing how it stores plain text encryption keys for the data store after downplaying the issue since 2018. [...]
