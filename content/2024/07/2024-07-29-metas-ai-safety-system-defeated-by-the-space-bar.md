Title: Meta's AI safety system defeated by the space bar
Date: 2024-07-29T21:01:25+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-07-29-metas-ai-safety-system-defeated-by-the-space-bar

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/29/meta_ai_safety/){:target="_blank" rel="noopener"}

> 'Ignore previous instructions' thwarts Prompt-Guard model if you just add some good ol' ASCII code 32 Meta's machine-learning model for detecting prompt injection attacks – special prompts to make neural networks behave inappropriately – is itself vulnerable to, you guessed it, prompt injection attacks.... [...]
