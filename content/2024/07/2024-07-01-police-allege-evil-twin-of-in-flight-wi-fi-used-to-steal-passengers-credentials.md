Title: Police allege ‘evil twin’ of in-flight Wi-Fi used to steal passenger’s credentials
Date: 2024-07-01T05:45:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-07-01-police-allege-evil-twin-of-in-flight-wi-fi-used-to-steal-passengers-credentials

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/01/australia_evil_twin_wifi_airline_attack/){:target="_blank" rel="noopener"}

> Fasten your seat belts, secure your tray table, and try not to give away your passwords Australia’s Federal Police (AFP) has charged a man with running a fake Wi-Fi network on at least one commercial flight and using it to harvest fliers’ credentials for email and social media services.... [...]
