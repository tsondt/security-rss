Title: 2017 ODNI Memo on Kaspersky Labs
Date: 2024-07-23T11:08:40+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;FOIA;Kaspersky
Slug: 2024-07-23-2017-odni-memo-on-kaspersky-labs

[Source](https://www.schneier.com/blog/archives/2024/07/2017-odni-memo-on-kaspersky-labs.html){:target="_blank" rel="noopener"}

> It’s heavily redacted, but still interesting. Many more ODNI documents here. [...]
