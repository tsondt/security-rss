Title: AWS revalidates its AAA Pinakes rating for Spanish financial entities
Date: 2024-07-29T18:56:07+00:00
Author: Daniel Fuertes
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Security Blog
Slug: 2024-07-29-aws-revalidates-its-aaa-pinakes-rating-for-spanish-financial-entities

[Source](https://aws.amazon.com/blogs/security/aws-revalidates-its-aaa-pinakes-rating-for-spanish-financial-entities/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce that we have revalidated our AAA rating for the Pinakes qualification system. The scope of this requalification covers 171 services in 31 global AWS Regions. Pinakes is a security rating framework developed by the Spanish banking association Centro de Cooperación Interbancaria (CCI) to facilitate the management and [...]
