Title: New Research in Detecting AI-Generated Videos
Date: 2024-07-29T11:02:51+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence;deepfake;videos
Slug: 2024-07-29-new-research-in-detecting-ai-generated-videos

[Source](https://www.schneier.com/blog/archives/2024/07/new-research-in-detecting-ai-generated-videos.html){:target="_blank" rel="noopener"}

> The latest in what will be a continuing arms race between creating and detecting videos: The new tool the research project is unleashing on deepfakes, called “MISLnet”, evolved from years of data derived from detecting fake images and video with tools that spot changes made to digital video or images. These may include the addition or movement of pixels between frames, manipulation of the speed of the clip, or the removal of frames. Such tools work because a digital camera’s algorithmic processing creates relationships between pixel color values. Those relationships between values are very different in user-generated or images edited [...]
