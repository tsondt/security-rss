Title: Google Advanced Protection Program gets passkeys for high-risk users
Date: 2024-07-10T06:00:00-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2024-07-10-google-advanced-protection-program-gets-passkeys-for-high-risk-users

[Source](https://www.bleepingcomputer.com/news/security/google-advanced-protection-program-gets-passkeys-for-high-risk-users/){:target="_blank" rel="noopener"}

> Google announced today that passkeys are now available for high-risk users when enrolling in the Advanced Protection Program, which provides the strongest level of account security. [...]
