Title: New Blast-RADIUS attack bypasses widely-used RADIUS authentication
Date: 2024-07-09T15:44:05-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-09-new-blast-radius-attack-bypasses-widely-used-radius-authentication

[Source](https://www.bleepingcomputer.com/news/security/new-blast-radius-attack-bypasses-widely-used-radius-authentication/){:target="_blank" rel="noopener"}

> Blast-RADIUS, an authentication bypass in the widely used RADIUS/UDP protocol, enables threat actors to breach networks and devices in man-in-the-middle MD5 collision attacks. [...]
