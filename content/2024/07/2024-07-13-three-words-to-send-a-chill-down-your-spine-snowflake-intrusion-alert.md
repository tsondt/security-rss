Title: Three words to send a chill down your spine: Snowflake. Intrusion. Alert
Date: 2024-07-13T15:04:12+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-07-13-three-words-to-send-a-chill-down-your-spine-snowflake-intrusion-alert

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/13/snowflake_kettle/){:target="_blank" rel="noopener"}

> And can AI save us from the scourge of malware? In theory, why not, but in practice... Color us skeptical Kettle For this week's Kettle episode, in which our journos as usual get together for an end-of-week chat about the news, it's security, security, security.... [...]
