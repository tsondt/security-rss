Title: Dallas County: Data of 200,000 exposed in 2023 ransomware attack
Date: 2024-07-11T13:15:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-07-11-dallas-county-data-of-200000-exposed-in-2023-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/dallas-county-data-of-200-000-exposed-in-2023-ransomware-attack/){:target="_blank" rel="noopener"}

> Dallas County is notifying over 200,000 people that the Play ransomware attack, which occurred in October 2023, exposed their personal data to cybercriminals. [...]
