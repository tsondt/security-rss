Title: Crooks Steal Phone, SMS Records for Nearly All AT&T Customers
Date: 2024-07-12T18:12:20+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Latest Warnings;Advance Auto Parts;Allstate;Anheuser-Busch;AT&T breach;fbi;Los Angeles Unified;Mitsubishi;Neiman Marcus;Progressive;Pure Storage;Santander Bank;Snowflake hack;State Farm;Techcrunch;Ticketmaster;U.S. Securities and Exchange Commission;wired
Slug: 2024-07-12-crooks-steal-phone-sms-records-for-nearly-all-att-customers

[Source](https://krebsonsecurity.com/2024/07/hackers-steal-phone-sms-records-for-nearly-all-att-customers/){:target="_blank" rel="noopener"}

> AT&T Corp. disclosed today that a new data breach has exposed phone call and text message records for roughly 110 million people — nearly all of its customers. AT&T said it delayed disclosing the incident in response to “national security and public safety concerns,” noting that some of the records included data that could be used to determine where a call was made or text message sent. AT&T also acknowledged the customer records were exposed in a cloud database that was protected only by a username and password (no multi-factor authentication needed). In a regulatory filing with the U.S. Securities [...]
