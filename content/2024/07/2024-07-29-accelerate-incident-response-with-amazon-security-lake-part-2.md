Title: Accelerate incident response with Amazon Security Lake – Part 2
Date: 2024-07-29T16:12:02+00:00
Author: Frank Phillis
Category: AWS Security
Tags: Advanced (300);Amazon Security Lake;Security, Identity, & Compliance;Technical How-to;Incident response;Security;Security Blog
Slug: 2024-07-29-accelerate-incident-response-with-amazon-security-lake-part-2

[Source](https://aws.amazon.com/blogs/security/accelerate-incident-response-with-amazon-security-lake-part-2/){:target="_blank" rel="noopener"}

> This blog post is the second of a two-part series where we show you how to respond to a specific incident by using Amazon Security Lake as the primary data source to accelerate incident response workflow. The workflow is described in the Unintended Data Access in Amazon S3 incident response playbook, published in the AWS [...]
