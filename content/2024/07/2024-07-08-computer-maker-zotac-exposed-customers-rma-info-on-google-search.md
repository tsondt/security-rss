Title: Computer maker Zotac exposed customers' RMA info on Google Search
Date: 2024-07-08T18:03:41-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-07-08-computer-maker-zotac-exposed-customers-rma-info-on-google-search

[Source](https://www.bleepingcomputer.com/news/security/computer-maker-zotac-exposed-customers-rma-info-on-google-search/){:target="_blank" rel="noopener"}

> Computer hardware maker Zotac has exposed return merchandise authorization (RMA) requests and related documents online for an unknown period, exposing sensitive customer information. [...]
