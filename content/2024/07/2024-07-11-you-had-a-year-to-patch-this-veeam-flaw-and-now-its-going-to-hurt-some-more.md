Title: You had a year to patch this Veeam flaw – and now it's going to hurt some more
Date: 2024-07-11T07:28:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-07-11-you-had-a-year-to-patch-this-veeam-flaw-and-now-its-going-to-hurt-some-more

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/11/estate_ransomware_veeam_bug/){:target="_blank" rel="noopener"}

> LockBit variant targets backup software - which you may remember is supposed to help you recover from ransomware Yet another new ransomware gang, this one dubbed EstateRansomware, is now exploiting a Veeam vulnerability that was patched more than a year ago to deploy file-encrypting malware, a LockBit variant, and extort payments from victims.... [...]
