Title: CrowdStrike meets Murphy's Law: Anything that can go wrong will
Date: 2024-07-26T18:36:12+00:00
Author: Steven J. Vaughan-Nichols
Category: The Register
Tags: 
Slug: 2024-07-26-crowdstrike-meets-murphys-law-anything-that-can-go-wrong-will

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/26/crowdstrike_meets_murphys_law/){:target="_blank" rel="noopener"}

> And boy, did last Friday's Windows fiasco ever prove that yet again Opinion CrowdStrike's recent Windows debacle will surely earn a prominent place in the annals of epic tech failures. On July 19, the cybersecurity giant accomplished what legions of hackers could only dream of – bringing millions of Windows systems worldwide to their knees with a single botched update.... [...]
