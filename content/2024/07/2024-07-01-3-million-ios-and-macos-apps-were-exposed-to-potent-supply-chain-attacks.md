Title: 3 million iOS and macOS apps were exposed to potent supply-chain attacks
Date: 2024-07-01T23:43:41+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Apple;Biz & IT;Security;apps;cocoapods;iOS;MacOS;open source
Slug: 2024-07-01-3-million-ios-and-macos-apps-were-exposed-to-potent-supply-chain-attacks

[Source](https://arstechnica.com/?p=2034866){:target="_blank" rel="noopener"}

> Enlarge (credit: Aurich Lawson) Vulnerabilities that went undetected for a decade left thousands of macOS and iOS apps susceptible to supply-chain attacks. Hackers could have added malicious code compromising the security of millions or billions of people who installed them, researchers said Monday. The vulnerabilities, which were fixed last October, resided in a “trunk” server used to manage CocoaPods, a repository for open source Swift and Objective-C projects that roughly 3 million macOS and iOS apps depend on. When developers make changes to one of their “pods”—CocoaPods lingo for individual code packages—dependent apps typically incorporate them automatically through app updates, [...]
