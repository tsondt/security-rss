Title: Public Surveillance of Bars
Date: 2024-07-02T11:06:37+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;privacy;surveillance
Slug: 2024-07-02-public-surveillance-of-bars

[Source](https://www.schneier.com/blog/archives/2024/07/public-surveillance-of-bars.html){:target="_blank" rel="noopener"}

> This article about an app that lets people remotely view bars to see if they’re crowded or not is filled with commentary—on both sides—about privacy and openness. [...]
