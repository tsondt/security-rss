Title: Crooks Bypassed Google’s Email Verification to Create Workspace Accounts, Access 3rd-Party Services
Date: 2024-07-26T21:31:54+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Web Fraud 2.0;Anu Yamunan;Dropbox;google;Google Workspace;sign in with Google;Slack;Squarespace
Slug: 2024-07-26-crooks-bypassed-googles-email-verification-to-create-workspace-accounts-access-3rd-party-services

[Source](https://krebsonsecurity.com/2024/07/crooks-bypassed-googles-email-verification-to-create-workspace-accounts-access-3rd-party-services/){:target="_blank" rel="noopener"}

> Google says it recently fixed an authentication weakness that allowed crooks to circumvent the email verification required to create a Google Workspace account, and leverage that to impersonate a domain holder at third-party services that allow logins through Google’s “Sign in with Google” feature. Last week, KrebsOnSecurity heard from a reader who said they received a notice that their email address had been used to create a potentially malicious Workspace account that Google had blocked. “In the last few weeks, we identified a small-scale abuse campaign whereby bad actors circumvented the email verification step in our account creation flow for [...]
