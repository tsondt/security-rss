Title: How to deploy an Amazon OpenSearch cluster to ingest logs from Amazon Security Lake
Date: 2024-07-30T16:02:19+00:00
Author: Kevin Low
Category: AWS Security
Tags: Advanced (300);Amazon Security Lake;Customer Solutions;Security, Identity, & Compliance;Technical How-to;AWS security;Cloud security;Incident response;OpenSearch;Security Blog;threat detection
Slug: 2024-07-30-how-to-deploy-an-amazon-opensearch-cluster-to-ingest-logs-from-amazon-security-lake

[Source](https://aws.amazon.com/blogs/security/how-to-deploy-an-amazon-opensearch-cluster-to-ingest-logs-from-amazon-security-lake/){:target="_blank" rel="noopener"}

> Many customers use Amazon Security Lake to automatically centralize security data from Amazon Web Services (AWS) environments, software as a service (SaaS) providers, on-premises workloads, and cloud sources into a purpose-built data lake in their AWS accounts. With Security Lake, customers can choose between native AWS security analytics tools and partner security information and event [...]
