Title: Patelco shuts down banking systems following ransomware attack
Date: 2024-07-02T13:47:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-07-02-patelco-shuts-down-banking-systems-following-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/patelco-shuts-down-banking-systems-following-ransomware-attack/){:target="_blank" rel="noopener"}

> Patelco Credit Union has disclosed it experienced a ransomware attack that led to the proactive shutdown of several of its customer-facing banking systems to contain the incident's impact. [...]
