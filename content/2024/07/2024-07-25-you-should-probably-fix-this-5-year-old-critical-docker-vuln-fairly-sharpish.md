Title: You should probably fix this 5-year-old critical Docker vuln fairly sharpish
Date: 2024-07-25T13:46:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-25-you-should-probably-fix-this-5-year-old-critical-docker-vuln-fairly-sharpish

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/25/5yo_docker_vulnerability/){:target="_blank" rel="noopener"}

> For some unknown reason, initial patch was omitted from later versions Docker is warning users to rev their Docker Engine into patch mode after it realized a near-maximum severity vulnerability had been sticking around for five years.... [...]
