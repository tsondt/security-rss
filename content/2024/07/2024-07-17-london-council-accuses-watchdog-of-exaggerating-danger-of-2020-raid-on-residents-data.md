Title: London council accuses watchdog of 'exaggerating' danger of 2020 raid on residents' data
Date: 2024-07-17T11:45:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-17-london-council-accuses-watchdog-of-exaggerating-danger-of-2020-raid-on-residents-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/17/londons_hackney_council_accuses_the/){:target="_blank" rel="noopener"}

> You escaped a big fat fine! Take the win and run, won’t you? London's inner city district of Hackney says the UK's data protection watchdog has misunderstood and "exaggerated" details surrounding a ransomware attack on its systems in 2020.... [...]
