Title: CloudSorcerer hackers abuse cloud services to steal Russian govt data
Date: 2024-07-08T11:11:46-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-07-08-cloudsorcerer-hackers-abuse-cloud-services-to-steal-russian-govt-data

[Source](https://www.bleepingcomputer.com/news/security/cloudsorcerer-hackers-abuse-cloud-services-to-steal-russian-govt-data/){:target="_blank" rel="noopener"}

> A new advanced persistent threat (APT) group named CloudSorcerer abuses public cloud services to steal data from Russian government organizations in cyberespionage attacks. [...]
