Title: Robot Dog Internet Jammer
Date: 2024-07-24T15:25:10+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;denial of service;DHS;Internet of Things;law enforcement;robotics
Slug: 2024-07-24-robot-dog-internet-jammer

[Source](https://www.schneier.com/blog/archives/2024/07/robot-dog-internet-jammer.html){:target="_blank" rel="noopener"}

> Supposedly the DHS has these : The robot, called “NEO,” is a modified version of the “Quadruped Unmanned Ground Vehicle” (Q-UGV) sold to law enforcement by a company called Ghost Robotics. Benjamine Huffman, the director of DHS’s Federal Law Enforcement Training Centers (FLETC), told police at the 2024 Border Security Expo in Texas that DHS is increasingly worried about criminals setting “booby traps” with internet of things and smart home devices, and that NEO allows DHS to remotely disable the home networks of a home or building law enforcement is raiding. The Border Security Expo is open only to law [...]
