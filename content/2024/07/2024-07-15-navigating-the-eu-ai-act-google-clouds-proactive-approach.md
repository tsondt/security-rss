Title: Navigating the EU AI Act: Google Cloud's proactive approach
Date: 2024-07-15T16:00:00+00:00
Author: Jeanette Manfra
Category: GCP Security
Tags: Security & Identity
Slug: 2024-07-15-navigating-the-eu-ai-act-google-clouds-proactive-approach

[Source](https://cloud.google.com/blog/products/identity-security/navigating-the-eu-ai-act-google-clouds-proactive-approach/){:target="_blank" rel="noopener"}

> Governance of AI has reached a critical milestone in the European Union: The AI Act has been published in the EU’s Official Journal, and will enter into force on August 1. The AI Act is a legal framework that establishes obligations for AI systems based on their potential risks and levels of impact. It will phase in over the next 36 months and include bans on certain practices, general-purpose AI rules, and obligations for high-risk systems. Critically, the Act’s yet-to-be-developed AI Code of Practice will establish compliance requirements for a subset of general purpose AI. At Google, we believe in [...]
