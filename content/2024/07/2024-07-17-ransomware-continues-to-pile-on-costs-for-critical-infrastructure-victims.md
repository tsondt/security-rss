Title: Ransomware continues to pile on costs for critical infrastructure victims
Date: 2024-07-17T15:01:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-17-ransomware-continues-to-pile-on-costs-for-critical-infrastructure-victims

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/17/ransomware_continues_to_pile_on/){:target="_blank" rel="noopener"}

> Millions more spent without any improvement in recovery times Costs associated with ransomware attacks on critical national infrastructure (CNI) organizations skyrocketed in the past year.... [...]
