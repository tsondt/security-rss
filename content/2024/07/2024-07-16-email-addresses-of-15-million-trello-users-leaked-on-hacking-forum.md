Title: Email addresses of 15 million Trello users leaked on hacking forum
Date: 2024-07-16T13:57:37-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-07-16-email-addresses-of-15-million-trello-users-leaked-on-hacking-forum

[Source](https://www.bleepingcomputer.com/news/security/email-addresses-of-15-million-trello-users-leaked-on-hacking-forum/){:target="_blank" rel="noopener"}

> A threat actor has released over 15 million email addresses associated with Trello accounts that were collected using an unsecured API in January. [...]
