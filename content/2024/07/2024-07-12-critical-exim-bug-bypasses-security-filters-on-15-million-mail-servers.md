Title: Critical Exim bug bypasses security filters on 1.5 million mail servers
Date: 2024-07-12T16:48:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-12-critical-exim-bug-bypasses-security-filters-on-15-million-mail-servers

[Source](https://www.bleepingcomputer.com/news/security/critical-exim-bug-bypasses-security-filters-on-15-million-mail-servers/){:target="_blank" rel="noopener"}

> Censys warns that over 1.5 million Exim mail transfer agent (MTA) instances are unpatched against a critical vulnerability that lets threat actors bypass security filters. [...]
