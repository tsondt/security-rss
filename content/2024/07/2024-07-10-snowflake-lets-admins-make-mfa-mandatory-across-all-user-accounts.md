Title: Snowflake lets admins make MFA mandatory across all user accounts
Date: 2024-07-10T16:45:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-10-snowflake-lets-admins-make-mfa-mandatory-across-all-user-accounts

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/10/snowflake_mandatory_mfa/){:target="_blank" rel="noopener"}

> Company announces intent following Ticketmaster, Santander break-ins A month after incident response giant Mandiant suggested the litany of data thefts linked to Snowflake account intrusions had the common component of lacking multi-factor authentication (MFA) controls, the cloud storage and data analytics company is offering a mandatory MFA option to admins.... [...]
