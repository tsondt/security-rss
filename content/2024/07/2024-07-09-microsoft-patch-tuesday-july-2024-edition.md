Title: Microsoft Patch Tuesday, July 2024 Edition
Date: 2024-07-09T19:50:33+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;AskWoody.com;Automox;CVE-2024-38021;CVE-2024-38074;CVE-2024-38076;CVE-2024-38077;CVE-2024-38080;CVE-2024-38112;Forta;Immersive Labs;Jason Kikta;Kevin Breen;Michael Gorelik;Morphisec;MSHTML;sans internet storm center;Satnam Narang;SQL Server 2014;Tyler Reguly;Windows Layer Two Bridge Network
Slug: 2024-07-09-microsoft-patch-tuesday-july-2024-edition

[Source](https://krebsonsecurity.com/2024/07/microsoft-patch-tuesday-july-2024-edition/){:target="_blank" rel="noopener"}

> Microsoft Corp. today issued software updates to plug at least 139 security holes in various flavors of Windows and other Microsoft products. Redmond says attackers are already exploiting at least two of the vulnerabilities in active attacks against Windows users. The first Microsoft zero-day this month is CVE-2024-38080, a bug in the Windows Hyper-V component that affects Windows 11 and Windows Server 2022 systems. CVE-2024-38080 allows an attacker to increase their account privileges on a Windows machine. Although Microsoft says this flaw is being exploited, it has offered scant details about its exploitation. The other zero-day is CVE-2024-38112, which is [...]
