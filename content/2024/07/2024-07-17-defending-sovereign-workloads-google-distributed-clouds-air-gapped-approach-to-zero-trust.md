Title: Defending sovereign workloads: Google Distributed Cloud's air-gapped approach to Zero Trust
Date: 2024-07-17T10:00:00+00:00
Author: Jason Byrd
Category: GCP Security
Tags: Public Sector;Security & Identity
Slug: 2024-07-17-defending-sovereign-workloads-google-distributed-clouds-air-gapped-approach-to-zero-trust

[Source](https://cloud.google.com/blog/products/identity-security/google-distributed-clouds-air-gapped-approach-to-zero-trust/){:target="_blank" rel="noopener"}

> Public sector and government organizations require cloud solutions that can drive innovation and also adhere to strict sovereignty and security requirements. For years, security experts have warned of the risks of government overreliance on singular security controls. To help address public sector and government requirements, we offer Google Distributed Cloud (GDC), a comprehensive suite of solutions. This includes GDC air-gapped, a disconnected private cloud environment for managing classified, restricted, and top-secret data that can help organizations meet public sector regulatory and security standards. Why regulated industry and public sector need Zero Trust for air-gapped clouds As cyber threats become increasingly [...]
