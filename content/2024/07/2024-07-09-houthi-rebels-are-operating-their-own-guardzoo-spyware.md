Title: Houthi rebels are operating their own GuardZoo spyware
Date: 2024-07-09T10:56:53+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-07-09-houthi-rebels-are-operating-their-own-guardzoo-spyware

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/09/houthi_rebels_malware/){:target="_blank" rel="noopener"}

> Fairly 'low budget', unsophisticated malware, say researchers, but it can collect the same data as Pegasus Interview When it comes to surveillance malware, sophisticated spyware with complex capabilities tends to hog the limelight – for example NSO Group's Pegasus, which is sold to established governments. But it's actually less polished kit that you've never heard of, like GuardZoo – developed and used by Houthi rebels in Yemen – that dominates the space.... [...]
