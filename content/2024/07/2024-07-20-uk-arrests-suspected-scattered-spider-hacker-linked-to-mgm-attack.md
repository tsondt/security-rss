Title: UK arrests suspected Scattered Spider hacker linked to MGM attack
Date: 2024-07-20T15:05:35-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-07-20-uk-arrests-suspected-scattered-spider-hacker-linked-to-mgm-attack

[Source](https://www.bleepingcomputer.com/news/security/uk-arrests-suspected-scattered-spider-hacker-linked-to-mgm-attack/){:target="_blank" rel="noopener"}

> UK police have arrested a 17-year-old boy suspected of being involved in the 2023 MGM Resorts ransomware attack and a member of the Scattered Spider hacking collective. [...]
