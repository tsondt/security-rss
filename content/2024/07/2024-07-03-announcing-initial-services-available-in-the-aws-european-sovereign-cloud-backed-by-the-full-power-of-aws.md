Title: Announcing initial services available in the AWS European Sovereign Cloud, backed by the full power of AWS
Date: 2024-07-03T20:32:37+00:00
Author: Max Peterson
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS Digital Sovereignty Pledge;data sovereignty;Digital Sovereignty;Europe;European Union;Security Blog;Sovereign-by-design
Slug: 2024-07-03-announcing-initial-services-available-in-the-aws-european-sovereign-cloud-backed-by-the-full-power-of-aws

[Source](https://aws.amazon.com/blogs/security/announcing-initial-services-available-in-the-aws-european-sovereign-cloud-backed-by-the-full-power-of-aws/){:target="_blank" rel="noopener"}

> English | French | German | Italian | Spanish Last month, we shared that we are investing €7.8 billion in the AWS European Sovereign Cloud, a new independent cloud for Europe, which is set to launch by the end of 2025. We are building the AWS European Sovereign Cloud designed to offer public sector organizations [...]
