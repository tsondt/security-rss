Title: Critical Cisco bug lets hackers add root users on SEG devices
Date: 2024-07-18T08:48:31-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-18-critical-cisco-bug-lets-hackers-add-root-users-on-seg-devices

[Source](https://www.bleepingcomputer.com/news/security/critical-cisco-bug-lets-hackers-add-root-users-on-seg-devices/){:target="_blank" rel="noopener"}

> Cisco has fixed a critical severity vulnerability that lets attackers add new users with root privileges and permanently crash Security Email Gateway (SEG) appliances using emails with malicious attachments. [...]
