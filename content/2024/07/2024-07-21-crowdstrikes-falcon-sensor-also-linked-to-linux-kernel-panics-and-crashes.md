Title: CrowdStrike's Falcon Sensor also linked to Linux kernel panics and crashes
Date: 2024-07-21T23:51:18+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-07-21-crowdstrikes-falcon-sensor-also-linked-to-linux-kernel-panics-and-crashes

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/21/crowdstrike_linux_crashes_restoration_tools/){:target="_blank" rel="noopener"}

> Rapid restore tool being tested as Microsoft estimates 8.5 million machines went down CrowdStrike's now-infamous Falcon Sensor software, which last week led to widespread outages of Windows-powered computers, has also caused crashes of Linux machines.... [...]
