Title: Iran's MuddyWater phishes Israeli orgs with custom BugSleep backdoor
Date: 2024-07-17T00:00:51+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-07-17-irans-muddywater-phishes-israeli-orgs-with-custom-bugsleep-backdoor

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/17/irans_muddywater_phishes_israeli_orgs/){:target="_blank" rel="noopener"}

> India, Turkey, also being targeted by campaign that relies on corporate email compromise MuddyWater, an Iranian government-backed cyber espionage crew, has upgraded its malware with a custom backdoor, which it's used to target Israeli organizations.... [...]
