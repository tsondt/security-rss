Title: Fraud ring pushes 600+ fake web shops via Facebook ads
Date: 2024-07-31T10:14:55-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-07-31-fraud-ring-pushes-600-fake-web-shops-via-facebook-ads

[Source](https://www.bleepingcomputer.com/news/security/fraud-ring-pushes-600-plus-fake-web-shops-via-facebook-ads/){:target="_blank" rel="noopener"}

> A malicious fraud campaign dubbed "ERIAKOS" promotes more than 600 fake web shops through Facebook advertisements to steal visitors' personal and financial information. [...]
