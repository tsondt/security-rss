Title: White House urged to double check Microsoft isn't funneling AI to China via G42 deal
Date: 2024-07-12T20:22:09+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-07-12-white-house-urged-to-double-check-microsoft-isnt-funneling-ai-to-china-via-g42-deal

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/12/g42_uae_us_house/){:target="_blank" rel="noopener"}

> Windows maker insisted everything will be locked down and secure – which given its reputation, uh-oh! Two House committee chairs have sent a public letter to the White House asking it to look into a deal between AI R&D outfit G42 and Microsoft.... [...]
