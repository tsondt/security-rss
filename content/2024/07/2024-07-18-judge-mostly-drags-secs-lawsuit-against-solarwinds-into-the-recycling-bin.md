Title: Judge mostly drags SEC's lawsuit against SolarWinds into the recycling bin
Date: 2024-07-18T21:06:49+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-07-18-judge-mostly-drags-secs-lawsuit-against-solarwinds-into-the-recycling-bin

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/18/sec_solarwinds_lawsuit/){:target="_blank" rel="noopener"}

> Russia-invaded software biz 'grateful for the support we have received' A judge has mostly thrown out a lawsuit brought by America's financial watchdog that accused SolarWinds and its chief infosec officer of misleading investors about its computer security practices and the backdooring of its Orion product.... [...]
