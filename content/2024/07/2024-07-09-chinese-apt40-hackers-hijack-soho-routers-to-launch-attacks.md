Title: Chinese APT40 hackers hijack SOHO routers to launch attacks
Date: 2024-07-09T11:11:21-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-07-09-chinese-apt40-hackers-hijack-soho-routers-to-launch-attacks

[Source](https://www.bleepingcomputer.com/news/security/chinese-apt40-hackers-hijack-soho-routers-to-launch-attacks/){:target="_blank" rel="noopener"}

> An advisory by CISA and multiple international cybersecurity agencies highlights the tactics, techniques, and procedures (TTPs) of APT40 (aka "Kryptonite Panda"), a state-sponsored Chinese cyber-espionage actor. [...]
