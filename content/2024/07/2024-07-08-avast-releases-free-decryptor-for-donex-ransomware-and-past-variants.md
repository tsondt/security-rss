Title: Avast releases free decryptor for DoNex ransomware and past variants
Date: 2024-07-08T14:51:28-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2024-07-08-avast-releases-free-decryptor-for-donex-ransomware-and-past-variants

[Source](https://www.bleepingcomputer.com/news/security/avast-releases-free-decryptor-for-donex-ransomware-and-past-variants/){:target="_blank" rel="noopener"}

> Antivirus company Avast have discovered a weakness in the cryptographic scheme of the DoNex ransomware family and released a decryptor so victims can recover their files for free. [...]
