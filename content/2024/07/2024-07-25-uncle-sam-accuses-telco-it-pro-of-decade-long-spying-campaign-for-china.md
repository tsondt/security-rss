Title: Uncle Sam accuses telco IT pro of decade-long spying campaign for China
Date: 2024-07-25T17:15:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-25-uncle-sam-accuses-telco-it-pro-of-decade-long-spying-campaign-for-china

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/25/us_it_pro_spying_charge/){:target="_blank" rel="noopener"}

> Beijing has a long history of recruiting US residents to carry out various espionage activities The US is looking to prosecute a Chinese immigrant over claims he has been drip-feeding information of interest to Beijing since at least 2012.... [...]
