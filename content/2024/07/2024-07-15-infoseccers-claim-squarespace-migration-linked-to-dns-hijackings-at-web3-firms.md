Title: Infoseccers claim Squarespace migration linked to DNS hijackings at Web3 firms
Date: 2024-07-15T13:45:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-15-infoseccers-claim-squarespace-migration-linked-to-dns-hijackings-at-web3-firms

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/15/squarespace_fingered_for_dns_hijackings/){:target="_blank" rel="noopener"}

> Company keeps quiet amid high-profile compromises Security researchers are claiming a spate of DNS hijackings at web3 businesses is linked to Squarespace's acquisition of Google Domains last year.... [...]
