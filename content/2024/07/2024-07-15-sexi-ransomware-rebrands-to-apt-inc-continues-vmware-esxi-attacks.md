Title: SEXi ransomware rebrands to APT INC, continues VMware ESXi attacks
Date: 2024-07-15T10:27:54-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-07-15-sexi-ransomware-rebrands-to-apt-inc-continues-vmware-esxi-attacks

[Source](https://www.bleepingcomputer.com/news/security/sexi-ransomware-rebrands-to-apt-inc-continues-vmware-esxi-attacks/){:target="_blank" rel="noopener"}

> The SEXi ransomware operation, known for targeting VMware ESXi servers, has rebranded under the name APT INC and has targeted numerous organizations in recent attacks. [...]
