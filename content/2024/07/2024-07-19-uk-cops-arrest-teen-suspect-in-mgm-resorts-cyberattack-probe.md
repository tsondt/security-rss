Title: UK cops arrest teen suspect in MGM Resorts cyberattack probe
Date: 2024-07-19T21:51:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-07-19-uk-cops-arrest-teen-suspect-in-mgm-resorts-cyberattack-probe

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/19/uk_mgm_suspect_arrested/){:target="_blank" rel="noopener"}

> 17-year-old cuffed as FBI says it will 'relentlessly pursue' miscreants around the globe Cops in the UK have arrested a suspected member of the notorious Scattered Spider crime gang, which is accused of crippling MGM Resorts in Las Vegas with ransomware last summer.... [...]
