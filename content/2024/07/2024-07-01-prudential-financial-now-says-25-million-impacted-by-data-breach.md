Title: Prudential Financial now says 2.5 million impacted by data breach
Date: 2024-07-01T17:14:51-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-01-prudential-financial-now-says-25-million-impacted-by-data-breach

[Source](https://www.bleepingcomputer.com/news/security/prudential-financial-now-says-25-million-impacted-by-data-breach/){:target="_blank" rel="noopener"}

> Prudential Financial, a global financial services company, has revealed that over 2.5 million people had their personal information compromised in a February data breach. [...]
