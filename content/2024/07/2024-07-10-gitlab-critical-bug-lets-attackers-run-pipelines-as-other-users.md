Title: GitLab: Critical bug lets attackers run pipelines as other users
Date: 2024-07-10T16:08:01-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-10-gitlab-critical-bug-lets-attackers-run-pipelines-as-other-users

[Source](https://www.bleepingcomputer.com/news/security/gitlab-warns-of-critical-bug-that-lets-attackers-run-pipelines-as-an-arbitrary-user/){:target="_blank" rel="noopener"}

> GitLab warned today that a critical vulnerability in its product's GitLab Community and Enterprise editions allows attackers to run pipeline jobs as any other user. [...]
