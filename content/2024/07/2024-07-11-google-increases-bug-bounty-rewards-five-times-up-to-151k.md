Title: Google increases bug bounty rewards five times, up to $151K
Date: 2024-07-11T14:17:56-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2024-07-11-google-increases-bug-bounty-rewards-five-times-up-to-151k

[Source](https://www.bleepingcomputer.com/news/security/google-increases-bug-bounty-rewards-five-times-up-to-151k/){:target="_blank" rel="noopener"}

> Google has announced a fivefold increase in payouts for bugs found in its systems and applications reported through its Vulnerability Reward Program, with a new maximum bounty of $151,515 for a single security flaw. [...]
