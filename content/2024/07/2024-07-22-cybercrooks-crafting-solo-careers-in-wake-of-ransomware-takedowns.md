Title: Cybercrooks crafting solo careers in wake of ransomware takedowns
Date: 2024-07-22T16:33:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-22-cybercrooks-crafting-solo-careers-in-wake-of-ransomware-takedowns

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/22/europol_says_ransomware_takedowns_make/){:target="_blank" rel="noopener"}

> More baddies go it alone as trust in big gangs withers, claims Europol A fresh report from Europol suggests that the recent disruption of ransomware-as-a-service (RaaS) groups is fragmenting the threat landscape, making it more difficult to track.... [...]
