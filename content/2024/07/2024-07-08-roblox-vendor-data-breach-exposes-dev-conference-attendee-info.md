Title: Roblox vendor data breach exposes dev conference attendee info
Date: 2024-07-08T09:56:05-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2024-07-08-roblox-vendor-data-breach-exposes-dev-conference-attendee-info

[Source](https://www.bleepingcomputer.com/news/security/roblox-vendor-data-breach-exposes-dev-conference-attendee-info/){:target="_blank" rel="noopener"}

> Roblox announced late last week that it suffered a data breach impacting attendees of the 2022, 2023, and 2024 Roblox Developer Conference attendees. [...]
