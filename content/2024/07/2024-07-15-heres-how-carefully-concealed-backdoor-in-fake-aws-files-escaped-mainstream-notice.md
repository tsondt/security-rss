Title: Here’s how carefully concealed backdoor in fake AWS files escaped mainstream notice
Date: 2024-07-15T20:18:50+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;backdoors;open source;steganigraphy
Slug: 2024-07-15-heres-how-carefully-concealed-backdoor-in-fake-aws-files-escaped-mainstream-notice

[Source](https://arstechnica.com/?p=2037194){:target="_blank" rel="noopener"}

> Enlarge (credit: BeeBright / Getty Images / iStockphoto ) Researchers have determined that two fake AWS packages downloaded hundreds of times from the open source NPM JavaScript repository contained carefully concealed code that backdoored developers' computers when executed. The packages— img-aws-s3-object-multipart-copy and legacyaws-s3-object-multipart-copy —were attempts to appear as aws-s3-object-multipart-copy, a legitimate JavaScript library for copying files using Amazon’s S3 cloud service. The fake files included all the code found in the legitimate library but added an additional JavaScript file named loadformat.js. That file provided what appeared to be benign code and three JPG images that were processed during package installation. [...]
