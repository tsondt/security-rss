Title: ARRL finally confirms ransomware gang stole data in cyberattack
Date: 2024-07-11T17:32:34-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-11-arrl-finally-confirms-ransomware-gang-stole-data-in-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/arrl-finally-confirms-ransomware-gang-stole-data-in-cyberattack/){:target="_blank" rel="noopener"}

> The American Radio Relay League (ARRL) finally confirmed that some of its employees' data was stolen in a May ransomware attack initially described as a "serious incident." [...]
