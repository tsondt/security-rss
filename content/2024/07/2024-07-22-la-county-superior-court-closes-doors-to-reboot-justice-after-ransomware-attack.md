Title: LA County Superior Court closes doors to reboot justice after ransomware attack
Date: 2024-07-22T17:15:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-07-22-la-county-superior-court-closes-doors-to-reboot-justice-after-ransomware-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/22/ransomware_la_county_superior_court/){:target="_blank" rel="noopener"}

> Some rest for the wicked? Los Angeles County Superior Court, the largest trial court in America, closed all 36 of its courthouses today following an "unprecedented" ransomware attack on Friday.... [...]
