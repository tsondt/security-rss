Title: Cyber-crime super-crew Scattered Spider falls in love with RansomHub and Qilin
Date: 2024-07-16T18:05:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-16-cyber-crime-super-crew-scattered-spider-falls-in-love-with-ransomhub-and-qilin

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/16/scattered_spider_ransom/){:target="_blank" rel="noopener"}

> Extortionists left hanging after rivals crawled into the woodwork The Scattered Spider cybercrime group is now using RansomHub and Qilin ransomware variants in its attacks, illustrating a possible power shift among hacking groups.... [...]
