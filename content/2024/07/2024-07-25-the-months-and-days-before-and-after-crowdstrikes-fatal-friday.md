Title: The months and days before and after CrowdStrike's fatal Friday
Date: 2024-07-25T00:17:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-07-25-the-months-and-days-before-and-after-crowdstrikes-fatal-friday

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/25/crowdstrike_timeline/){:target="_blank" rel="noopener"}

> 'In the short term, they're going to have to do a lot of groveling' Analysis The great irony of the CrowdStrike fiasco is that a cybersecurity company caused the exact sort of massive global outage it was supposed to prevent. And it all started with an effort to make life more difficult for criminals and their malware, with an update to its endpoint detection and response tool Falcon.... [...]
