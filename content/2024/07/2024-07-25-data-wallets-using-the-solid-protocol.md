Title: Data Wallets Using the Solid Protocol
Date: 2024-07-25T11:05:10+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;data privacy;data protection;Inrupt;security standards
Slug: 2024-07-25-data-wallets-using-the-solid-protocol

[Source](https://www.schneier.com/blog/archives/2024/07/data-wallets-using-the-solid-protocol.html){:target="_blank" rel="noopener"}

> I am the Chief of Security Architecture at Inrupt, Inc., the company that is commercializing Tim Berners-Lee’s Solid open W3C standard for distributed data ownership. This week, we announced a digital wallet based on the Solid architecture. Details are here, but basically a digital wallet is a repository for personal data and documents. Right now, there are hundreds of different wallets, but no standard. We think designing a wallet around Solid makes sense for lots of reasons. A wallet is more than a data store—data in wallets is for using and sharing. That requires interoperability, which is what you get [...]
