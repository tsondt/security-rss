Title: Microsoft tells yet more customers their emails have been stolen
Date: 2024-07-01T03:35:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-07-01-microsoft-tells-yet-more-customers-their-emails-have-been-stolen

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/01/infosec_in_brief/){:target="_blank" rel="noopener"}

> Plus: US auto dealers still offline; Conti coders sanction; Rabbit R1 hardcoded API keys; and more security in brief It took a while, but Microsoft has told customers that the Russian criminals who compromised its systems earlier this year made off with even more emails than it first admitted.... [...]
