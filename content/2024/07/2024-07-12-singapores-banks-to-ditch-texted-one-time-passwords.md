Title: Singapore's banks to ditch texted one-time passwords
Date: 2024-07-12T03:30:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-07-12-singapores-banks-to-ditch-texted-one-time-passwords

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/12/singapore_banks_fight_phishing/){:target="_blank" rel="noopener"}

> Accessibility be damned, preventing phishing is the priority After around two decades of allowing one-time passwords (OTPs) delivered by text message to assist log ins to bank accounts in Singapore, the city-state will abandon the authentication technique.... [...]
