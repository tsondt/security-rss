Title: Microsoft finally fixes Outlook alerts bug caused by December updates
Date: 2024-07-16T08:17:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-07-16-microsoft-finally-fixes-outlook-alerts-bug-caused-by-december-updates

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-finally-fixes-outlook-alerts-bug-caused-by-december-updates/){:target="_blank" rel="noopener"}

> Microsoft has finally fixed a known Outlook issue, confirmed in February, which was triggering incorrect security alerts after installing the December security updates for Outlook Desktop. [...]
