Title: 97% of CrowdStrike systems are back online; Microsoft suggests Windows changes
Date: 2024-07-26T17:46:16+00:00
Author: Andrew Cunningham
Category: Ars Technica
Tags: Biz & IT;Security;BSOD;Crowdstrike;windows 10;windows 11
Slug: 2024-07-26-97-of-crowdstrike-systems-are-back-online-microsoft-suggests-windows-changes

[Source](https://arstechnica.com/?p=2039500){:target="_blank" rel="noopener"}

> Enlarge / A bad update to CrowdStrike's Falcon security software crashed millions of Windows PCs last week. (credit: CrowdStrike) CrowdStrike CEO George Kurtz said Thursday that 97 percent of all Windows systems running its Falcon sensor software were back online, a week after an update-related outage to the corporate security software delayed flights and took down emergency response systems, among many other disruptions. The update, which caused Windows PCs to throw the dreaded Blue Screen of Death and reboot, affected about 8.5 million systems by Microsoft's count, leaving roughly 250,000 that still need to be brought back online. Microsoft VP [...]
