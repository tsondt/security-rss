Title: Europol says Home Routing mobile encryption feature aids criminals
Date: 2024-07-07T11:23:35-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-07-07-europol-says-home-routing-mobile-encryption-feature-aids-criminals

[Source](https://www.bleepingcomputer.com/news/security/europol-says-home-routing-mobile-encryption-feature-aids-criminals/){:target="_blank" rel="noopener"}

> Europol is proposing solutions to avoid challenges posed by privacy-enhancing technologies in Home Routing that hinder law enforcement's ability to intercept communications during criminal investigations. [...]
