Title: FYI: Data from deleted GitHub repos may not actually be deleted
Date: 2024-07-25T19:51:32+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-07-25-fyi-data-from-deleted-github-repos-may-not-actually-be-deleted

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/25/data_from_deleted_github_repos/){:target="_blank" rel="noopener"}

> And the forking Microsoft-owned code warehouse doesn't see this as much of a problem Researchers at Truffle Security have found, or arguably rediscovered, that data from deleted GitHub repositories (public or private) and from deleted copies (forks) of repositories isn't necessarily deleted.... [...]
