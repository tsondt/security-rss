Title: Nasty regreSSHion bug in OpenSSH puts roughly 700K Linux boxes at risk
Date: 2024-07-01T14:01:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-01-nasty-regresshion-bug-in-openssh-puts-roughly-700k-linux-boxes-at-risk

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/01/regresshion_openssh/){:target="_blank" rel="noopener"}

> Full system takeovers on the cards, for those with enough patience to pull it off Glibc-based Linux systems are vulnerable to a new bug (CVE-2024-6387) in OpenSSH's server (sshd) and should upgrade to the latest version.... [...]
