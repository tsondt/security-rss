Title: Cloudflare Reports that Almost 7% of All Internet Traffic Is Malicious
Date: 2024-07-17T16:03:20+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybercrime;denial of service;Internet
Slug: 2024-07-17-cloudflare-reports-that-almost-7-of-all-internet-traffic-is-malicious

[Source](https://www.schneier.com/blog/archives/2024/07/cloudflare-reports-that-almost-7-of-all-internet-traffic-is-malicious.html){:target="_blank" rel="noopener"}

> 6.8%, to be precise. From ZDNet : However, Distributed Denial of Service (DDoS) attacks continue to be cybercriminals’ weapon of choice, making up over 37% of all mitigated traffic. The scale of these attacks is staggering. In the first quarter of 2024 alone, Cloudflare blocked 4.5 million unique DDoS attacks. That total is nearly a third of all the DDoS attacks they mitigated the previous year. But it’s not just about the sheer volume of DDoS attacks. The sophistication of these attacks is increasing, too. Last August, Cloudflare mitigated a massive HTTP/2 Rapid Reset DDoS attack that peaked at 201 [...]
