Title: Australians affected by MediSecure hack may never know their personal details have been compromised
Date: 2024-07-19T02:41:17+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Cybercrime;Health;Australia news;Data and computer security
Slug: 2024-07-19-australians-affected-by-medisecure-hack-may-never-know-their-personal-details-have-been-compromised

[Source](https://www.theguardian.com/technology/article/2024/jul/19/australia-medisecure-hack-details-leaked-passwords-update){:target="_blank" rel="noopener"}

> Prime minister says ‘very significant cyber event’ still under investigation by federal police and he is ‘not aware’ if he is among the 12.9m victims Follow our Australia news live blog for latest updates Get our morning and afternoon news emails, free app or daily news podcast The 12.9 million Australians caught up in the hack on electronic prescriptions provider MediSecure may never be told their personal information has been compromised, with the prime minister saying on Friday he wasn’t aware if he was one of the victims. On Thursday evening, the administrators for MediSecure – which went into administration [...]
