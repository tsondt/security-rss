Title: ViperSoftX variant spotted abusing .NET runtime to disguise data theft
Date: 2024-07-10T06:26:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-07-10-vipersoftx-variant-spotted-abusing-net-runtime-to-disguise-data-theft

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/10/vipersoftx_malware_dot_net/){:target="_blank" rel="noopener"}

> Freeware AutoIt also used to hide entire PowerShell environments in scripts A rapidly-changing infostealer malware known as ViperSoftX has evolved to become more dangerous, according to security researchers at threat detection vendor Trellix.... [...]
