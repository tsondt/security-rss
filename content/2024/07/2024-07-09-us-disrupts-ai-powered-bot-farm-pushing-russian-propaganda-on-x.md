Title: US disrupts AI-powered bot farm pushing Russian propaganda on X
Date: 2024-07-09T17:16:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-09-us-disrupts-ai-powered-bot-farm-pushing-russian-propaganda-on-x

[Source](https://www.bleepingcomputer.com/news/security/us-disrupts-ai-powered-bot-farm-pushing-russian-propaganda-on-x/){:target="_blank" rel="noopener"}

> Almost a thousand Twitter accounts controlled by a large bot farm pushing Russian propaganda and domains used to register the bots were taken down in a joint international law enforcement operation led by the U.S. Justice Department. [...]
