Title: Friday Squid Blogging: Peru Trying to Protect its Squid Fisheries
Date: 2024-07-19T21:03:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-07-19-friday-squid-blogging-peru-trying-to-protect-its-squid-fisheries

[Source](https://www.schneier.com/blog/archives/2024/07/friday-squid-blogging-peru-trying-to-protect-its-squid-fisheries.html){:target="_blank" rel="noopener"}

> Peru is trying to protect its territorial waters from Chinese squid-fishing boats. Blog moderation policy. [...]
