Title: Russians plead guilty to involvement in LockBit ransomware attacks
Date: 2024-07-19T07:31:03-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-19-russians-plead-guilty-to-involvement-in-lockbit-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/russians-plead-guilty-to-involvement-in-lockbit-ransomware-attacks/){:target="_blank" rel="noopener"}

> Two Russian individuals admitted to participating in many LockBit ransomware attacks, which targeted victims worldwide and across the United States. [...]
