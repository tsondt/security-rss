Title: Cloud CISO Perspectives: Why water security can’t wait
Date: 2024-07-31T16:00:00+00:00
Author: Sandra Joyce
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2024-07-31-cloud-ciso-perspectives-why-water-security-cant-wait

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-why-water-security-cant-wait/){:target="_blank" rel="noopener"}

> Welcome to the second Cloud CISO Perspectives for July 2024. Today, guest columnist Sandra Joyce discusses the complex response needed to secure drinking water systems from cybersecurity risks. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI Security & CISO, Google Cloud aside_block <ListValue: [StructValue([('title', 'Get vital CISO Insights with Google Cloud'), ('body', <wagtail.rich_text.RichText object at 0x3edd33721d30>), ('btn_text', 'Visit the hub'), ('href', 'https://cloud.google.com/solutions/security/leaders?utm_source=cloud_sfdc&utm_medium=email&utm_campaign=FY23-Q2-global-PROD418-email-oi-dgcsm-CISOPerspectivesNewsletter&utm_content=ciso-hub&utm_term=-'), ('image', <GAEImage: GCAT-replacement-logo-A>)])]> Safeguarding the [...]
