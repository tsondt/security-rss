Title: Avast secretly gave DoNex ransomware decryptors to victims before crims vanished
Date: 2024-07-08T12:44:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-08-avast-secretly-gave-donex-ransomware-decryptors-to-victims-before-crims-vanished

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/08/avast_secretly_gave_donex_ransomware/){:target="_blank" rel="noopener"}

> Good riddance to another pesky tribe of miscreants Updated Researchers at Avast have provided decryptors to DoNex ransomware victims on the down-low since March after discovering a flaw in the crims' cryptography, the company confirmed today.... [...]
