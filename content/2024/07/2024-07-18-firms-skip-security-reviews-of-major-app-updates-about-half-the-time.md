Title: Firms skip security reviews of major app updates about half the time
Date: 2024-07-18T07:28:07+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-07-18-firms-skip-security-reviews-of-major-app-updates-about-half-the-time

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/18/security_review_failure/){:target="_blank" rel="noopener"}

> Complicated, costly, time-consuming – pick three Cyber security workers only review major updates to software applications only 54 percent of the time, according to a poll of tech managers.... [...]
