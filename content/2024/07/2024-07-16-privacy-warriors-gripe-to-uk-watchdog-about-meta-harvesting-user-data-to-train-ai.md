Title: Privacy warriors gripe to UK watchdog about Meta harvesting user data to train AI
Date: 2024-07-16T11:25:59+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2024-07-16-privacy-warriors-gripe-to-uk-watchdog-about-meta-harvesting-user-data-to-train-ai

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/16/campaign_group_complains_to_uk/){:target="_blank" rel="noopener"}

> Move follows Instagram and Facebook giant's decision to reverse direction in EU after protests A UK data rights campaign group has launched a complaint with the data law regulator against Meta’s change of privacy policy which allows it to scrape user data to develop AI models.... [...]
