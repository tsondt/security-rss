Title: The Not-So-Secret Network Access Broker x999xx
Date: 2024-07-03T16:41:34+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Breadcrumbs;Ne'er-Do-Well News;Cobalt Strike;Cobaltforce;Constella Intelligence;dashin2008@yahoo.com;Flashpoint;Intel 471;kirtsov@telecom.ozersk.ru;LockBit;Maksim Georgievich Kirtsov;maksya@icloud.com;maxnmalias-1@yahoo.com;Mikhail Matveev;Operation Endgame;osint.industries;Ozersk Technological Institute National Research Nuclear University;Recorded Future;U.S. Department of Justice;Wazawaka;x999xx;Кирцов Максим Георгиевич
Slug: 2024-07-03-the-not-so-secret-network-access-broker-x999xx

[Source](https://krebsonsecurity.com/2024/07/the-not-so-secret-network-access-broker-x999xx/){:target="_blank" rel="noopener"}

> Most accomplished cybercriminals go out of their way to separate their real names from their hacker handles. But among certain old-school Russian hackers it is not uncommon to find major players who have done little to prevent people from figuring out who they are in real life. A case study in this phenomenon is “ x999xx,” the nickname chosen by a venerated Russian hacker who specializes in providing the initial network access to various ransomware groups. x999xx is a well-known “access broker” who frequently sells access to hacked corporate networks — usually in the form of remote access credentials — [...]
