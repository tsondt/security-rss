Title: Russia takes aim at Sitting Ducks domains, bags 30,000+
Date: 2024-07-31T20:50:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-07-31-russia-takes-aim-at-sitting-ducks-domains-bags-30000

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/31/domains_with_delegated_name_service/){:target="_blank" rel="noopener"}

> Eight-year-old domain hijacking technique still claiming victims Dozens of Russia-affiliated criminals are right now trying to wrest control of web domains by exploiting weak DNS services.... [...]
