Title: Secure Boot is completely broken on 200+ models from 5 big device makers
Date: 2024-07-25T18:00:10+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Features;Security;cryptography;key compromise;rootkits;secure boot;supply chain;uefi;unified extensible firmware interface
Slug: 2024-07-25-secure-boot-is-completely-broken-on-200-models-from-5-big-device-makers

[Source](https://arstechnica.com/?p=2039140){:target="_blank" rel="noopener"}

> Enlarge (credit: sasha85ru | Getty Imates) In 2012, an industry-wide coalition of hardware and software makers adopted Secure Boot to protect against a long-looming security threat. The threat was the specter of malware that could infect the BIOS, the firmware that loaded the operating system each time a computer booted up. From there, it could remain immune to detection and removal and could load even before the OS and security apps did. The threat of such BIOS-dwelling malware was largely theoretical and fueled in large part by the creation of ICLord Bioskit by a Chinese researcher in 2007. ICLord was [...]
