Title: OneBlood's virtual machines encrypted in ransomware attack
Date: 2024-07-31T14:16:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-07-31-onebloods-virtual-machines-encrypted-in-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/onebloods-virtual-machines-encrypted-in-ransomware-attack/){:target="_blank" rel="noopener"}

> OneBlood, a large not-for-profit blood center that serves hospitals and patients in the United States, is dealing with an IT systems outage caused by a ransomware attack. [...]
