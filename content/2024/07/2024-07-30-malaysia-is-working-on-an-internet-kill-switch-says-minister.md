Title: Malaysia is working on an internet 'kill switch', says minister
Date: 2024-07-30T02:29:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-07-30-malaysia-is-working-on-an-internet-kill-switch-says-minister

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/30/malaysia_internet_killswitch/){:target="_blank" rel="noopener"}

> Follows requirement for social media and messaging platforms to get a license Legislation for an internet "kill switch" will reach Malaysia’s Parliament in October, according to the country's minister for Law and Institutional Reform.... [...]
