Title: CrowdStrike blames a test software bug for that giant global mess it made
Date: 2024-07-24T05:17:01+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-07-24-crowdstrike-blames-a-test-software-bug-for-that-giant-global-mess-it-made

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/24/crowdstrike_validator_failure/){:target="_blank" rel="noopener"}

> Something called 'Content Validator' did not validate the content, and the rest is history CrowdStrike has blamed a bug in its own test software for the mass-crash-event it caused last week.... [...]
