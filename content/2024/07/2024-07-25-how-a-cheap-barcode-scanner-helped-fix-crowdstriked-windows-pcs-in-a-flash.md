Title: How a cheap barcode scanner helped fix CrowdStrike'd Windows PCs in a flash
Date: 2024-07-25T02:29:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-07-25-how-a-cheap-barcode-scanner-helped-fix-crowdstriked-windows-pcs-in-a-flash

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/25/crowdstrike_remediation_with_barcode_scanner/){:target="_blank" rel="noopener"}

> This one weird trick saved countless hours and stress – no, really Not long after Windows PCs and servers at the Australian limb of audit and tax advisory Grant Thornton started BSODing last Friday, senior systems engineer Rob Woltz remembered a small but important fact: When PCs boot, they consider barcode scanners no differently to keyboards.... [...]
