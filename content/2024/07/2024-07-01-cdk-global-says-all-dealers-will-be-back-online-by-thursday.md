Title: CDK Global says all dealers will be back online by Thursday
Date: 2024-07-01T15:22:48-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-01-cdk-global-says-all-dealers-will-be-back-online-by-thursday

[Source](https://www.bleepingcomputer.com/news/security/cdk-global-says-all-dealers-will-be-back-online-by-thursday/){:target="_blank" rel="noopener"}

> CDK Global says that its dealer management system (DMS), impacted by a massive IT outage following a June 18th ransomware attack, will be back online by Thursday for all car dealerships. [...]
