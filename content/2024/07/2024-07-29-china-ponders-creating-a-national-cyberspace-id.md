Title: China ponders creating a national 'cyberspace ID'
Date: 2024-07-29T05:28:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-07-29-china-ponders-creating-a-national-cyberspace-id

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/29/china_cyberspace_id_proposal/){:target="_blank" rel="noopener"}

> Because clearly it's better for Beijing to know who you are than for every ISP and social service to keep its own records Beijing may soon issue "cyberspace IDs" to its citizens, after floating a proposal for the scheme last Friday.... [...]
