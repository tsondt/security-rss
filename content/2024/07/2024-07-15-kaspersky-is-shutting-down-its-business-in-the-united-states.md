Title: Kaspersky is shutting down its business in the United States
Date: 2024-07-15T15:49:03-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-15-kaspersky-is-shutting-down-its-business-in-the-united-states

[Source](https://www.bleepingcomputer.com/news/security/kaspersky-is-shutting-down-its-business-in-the-united-states/){:target="_blank" rel="noopener"}

> Russian cybersecurity company and antivirus software provider Kaspersky Lab will start shutting down operations in the United States on July 20. [...]
