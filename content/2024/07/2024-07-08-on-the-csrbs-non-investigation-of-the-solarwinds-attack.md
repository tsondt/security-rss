Title: On the CSRB’s Non-Investigation of the SolarWinds Attack
Date: 2024-07-08T17:59:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;cyberespionage;DHS;Microsoft;national security policy;Russia
Slug: 2024-07-08-on-the-csrbs-non-investigation-of-the-solarwinds-attack

[Source](https://www.schneier.com/blog/archives/2024/07/on-the-csrbs-non-investigation-of-the-solarwinds-attack.html){:target="_blank" rel="noopener"}

> ProPublica has a long investigative article on how the Cyber Safety Review Board failed to investigate the SolarWinds attack, and specifically Microsoft’s culpability, even though they were directed by President Biden to do so. [...]
