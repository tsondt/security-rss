Title: Announcing expanded Sensitive Data Protection for Cloud Storage
Date: 2024-07-02T16:00:00+00:00
Author: Scott Ellis
Category: GCP Security
Tags: Storage & Data Transfer;Security & Identity
Slug: 2024-07-02-announcing-expanded-sensitive-data-protection-for-cloud-storage

[Source](https://cloud.google.com/blog/products/identity-security/announcing-expanded-sensitive-data-protection-for-cloud-storage/){:target="_blank" rel="noopener"}

> Organizations rely on data-driven insights to power their business, but maximizing the potential of data comes with the responsibility to handle it securely. This can be a challenge when data growth can easily outpace the ability to manually inspect it, and data sprawl can lead to sensitive data appearing in unexpected places. To help, we are excited to announce that our Sensitive Data Protection (SDP) discovery service now supports Cloud Storage, joining BigQuery, BigLake, and Cloud SQL. Cloud Storage is Google Cloud’s enterprise-ready, fully-managed service for storing unstructured data. With this addition, SDP discovery now supports the most common services [...]
