Title: Criminal Gang Physically Assaulting People for Their Cryptocurrency
Date: 2024-07-18T15:33:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;bitcoin;crime;cryptocurrency;extortion;law enforcement;physical security;torture
Slug: 2024-07-18-criminal-gang-physically-assaulting-people-for-their-cryptocurrency

[Source](https://www.schneier.com/blog/archives/2024/07/criminal-gang-physically-assaulting-people-for-their-cryptocurrency.html){:target="_blank" rel="noopener"}

> This is pretty horrific :...a group of men behind a violent crime spree designed to compel victims to hand over access to their cryptocurrency savings. That announcement and the criminal complaint laying out charges against St. Felix focused largely on a single theft of cryptocurrency from an elderly North Carolina couple, whose home St. Felix and one of his accomplices broke into before physically assaulting the two victims—­both in their seventies—­and forcing them to transfer more than $150,000 in Bitcoin and Ether to the thieves’ crypto wallets. I think cryptocurrencies are more susceptible to this kind of real-world attack because [...]
