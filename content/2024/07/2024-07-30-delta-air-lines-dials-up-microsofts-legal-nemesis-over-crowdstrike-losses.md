Title: Delta Air Lines dials up Microsoft's legal nemesis over CrowdStrike losses
Date: 2024-07-30T19:00:14+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-07-30-delta-air-lines-dials-up-microsofts-legal-nemesis-over-crowdstrike-losses

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/30/crowdstrike_delta_microsoft_lawsuit/){:target="_blank" rel="noopener"}

> Oh, Boies, here we go again Delta Air Lines lost hundreds of millions of dollars due to the CrowdStrike outage earlier this month – and it has hired a high-powered law firm to claw some of those lost funds back, potentially from the Falcon maker and Microsoft itself.... [...]
