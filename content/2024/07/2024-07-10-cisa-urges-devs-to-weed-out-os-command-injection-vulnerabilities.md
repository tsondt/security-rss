Title: CISA urges devs to weed out OS command injection vulnerabilities
Date: 2024-07-10T14:02:49-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-10-cisa-urges-devs-to-weed-out-os-command-injection-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/cisa-urges-devs-to-weed-out-os-command-injection-vulnerabilities/){:target="_blank" rel="noopener"}

> ​CISA and the FBI urged software companies on Wednesday to review their products and eliminate path OS command injection vulnerabilities before shipping. [...]
