Title: Latest Intel CPUs impacted by new Indirector side-channel attack
Date: 2024-07-01T10:24:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2024-07-01-latest-intel-cpus-impacted-by-new-indirector-side-channel-attack

[Source](https://www.bleepingcomputer.com/news/security/latest-intel-cpus-impacted-by-new-indirector-side-channel-attack/){:target="_blank" rel="noopener"}

> Modern Intel processors, including chips from the Raptor Lake and the Alder Lake generations are susceptible to a new type of a high-precision Branch Target Injection (BTI) attack dubbed 'Indirector,' which could be used to steal sensitive information from the CPU. [...]
