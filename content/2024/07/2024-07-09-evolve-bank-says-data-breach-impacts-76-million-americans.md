Title: Evolve Bank says data breach impacts 7.6 million Americans
Date: 2024-07-09T10:01:49-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-07-09-evolve-bank-says-data-breach-impacts-76-million-americans

[Source](https://www.bleepingcomputer.com/news/security/evolve-bank-says-data-breach-impacts-76-million-americans/){:target="_blank" rel="noopener"}

> Evolve Bank & Trust (Evolve) is sending notices of a data breach to 7.6 million Americans whose data was stolen during a recent LockBit ransomware attack. [...]
