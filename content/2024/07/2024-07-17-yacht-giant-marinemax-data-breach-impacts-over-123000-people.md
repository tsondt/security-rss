Title: Yacht giant MarineMax data breach impacts over 123,000 people
Date: 2024-07-17T10:37:39-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-17-yacht-giant-marinemax-data-breach-impacts-over-123000-people

[Source](https://www.bleepingcomputer.com/news/security/yacht-giant-marinemax-data-breach-impacts-over-123-000-people/){:target="_blank" rel="noopener"}

> MarineMax, self-described as the world's largest recreational boat and yacht retailer, is notifying over 123,000 customers whose personal information was stolen in a March security breach claimed by the Rhysida ransomware gang. [...]
