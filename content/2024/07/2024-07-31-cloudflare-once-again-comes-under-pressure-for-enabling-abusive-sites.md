Title: Cloudflare once again comes under pressure for enabling abusive sites
Date: 2024-07-31T23:22:54+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;abuse;cloudflare;moderation
Slug: 2024-07-31-cloudflare-once-again-comes-under-pressure-for-enabling-abusive-sites

[Source](https://arstechnica.com/?p=2040424){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) A familiar debate is once again surrounding Cloudflare, the content delivery network that provides a free service that protects websites from being taken down in denial-of-service attacks by masking their hosts: Is Cloudflare a bastion of free speech or an enabler of spam, malware delivery, harassment and the very DDoS attacks it claims to block? The controversy isn't new for Cloudflare, a network operator that has often taken a hands-off approach to moderating the enormous amount of traffic flowing through its infrastructure. With Cloudflare helping deliver 16 percent of global Internet traffic, processing 57 million web [...]
