Title: Global Microsoft Meltdown Tied to Bad Crowdstrike Update
Date: 2024-07-19T14:24:27+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Time to Patch;blue screen of death;CrowdStrike;George Kurtz;microsoft
Slug: 2024-07-19-global-microsoft-meltdown-tied-to-bad-crowdstrike-update

[Source](https://krebsonsecurity.com/2024/07/global-microsoft-meltdown-tied-to-bad-crowstrike-update/){:target="_blank" rel="noopener"}

> A faulty software update from cybersecurity vendor Crowdstrike crippled countless Microsoft Windows computers across the globe today, disrupting everything from airline travel and financial institutions to hospitals and businesses online. Crowdstrike said a fix has been deployed, but experts say the recovery from this outage could take some time, as Crowdstrike’s solution needs to be applied manually on a per-machine basis. A photo taken at San Jose International Airport today shows the dreaded Microsoft “Blue Screen of Death” across the board. Credit: Twitter.com/adamdubya1990 Earlier today, an errant update shipped by Crowdstrike began causing Windows machines running the software to display [...]
