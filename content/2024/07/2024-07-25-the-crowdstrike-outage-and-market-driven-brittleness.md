Title: The CrowdStrike Outage and Market-Driven Brittleness
Date: 2024-07-25T18:37:40+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;economics of security;externalities;incentives;risk assessment
Slug: 2024-07-25-the-crowdstrike-outage-and-market-driven-brittleness

[Source](https://www.schneier.com/blog/archives/2024/07/the-crowdstrike-outage-and-market-driven-brittleness.html){:target="_blank" rel="noopener"}

> Friday’s massive internet outage, caused by a mid-sized tech company called CrowdStrike, disrupted major airlines, hospitals, and banks. Nearly 7,000 flights were canceled. It took down 911 systems and factories, courthouses, and television stations. Tallying the total cost will take time. The outage affected more than 8.5 million Windows computers, and the cost will surely be in the billions of dollars ­easily matching the most costly previous cyberattacks, such as NotPetya. The catastrophe is yet another reminder of how brittle global internet infrastructure is. It’s complex, deeply interconnected, and filled with single points of failure. As we experienced last week, [...]
