Title: Exchange Online adds Inbound DANE with DNSSEC for security boost
Date: 2024-07-17T15:02:24-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-07-17-exchange-online-adds-inbound-dane-with-dnssec-for-security-boost

[Source](https://www.bleepingcomputer.com/news/microsoft/exchange-online-adds-inbound-dane-with-dnssec-for-security-boost/){:target="_blank" rel="noopener"}

> Microsoft is rolling out inbound SMTP DANE with DNSSEC for Exchange Online in public preview, a new capability to boost email integrity and security. [...]
