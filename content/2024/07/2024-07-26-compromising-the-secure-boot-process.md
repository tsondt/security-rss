Title: Compromising the Secure Boot Process
Date: 2024-07-26T16:21:42+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptography;encryption;keys;passwords;supply chain;vulnerabilities
Slug: 2024-07-26-compromising-the-secure-boot-process

[Source](https://www.schneier.com/blog/archives/2024/07/compromising-the-secure-boot-process.html){:target="_blank" rel="noopener"}

> This isn’t good : On Thursday, researchers from security firm Binarly revealed that Secure Boot is completely compromised on more than 200 device models sold by Acer, Dell, Gigabyte, Intel, and Supermicro. The cause: a cryptographic key underpinning Secure Boot on those models that was compromised in 2022. In a public GitHub repository committed in December of that year, someone working for multiple US-based device manufacturers published what’s known as a platform key, the cryptographic key that forms the root-of-trust anchor between the hardware device and the firmware that runs on it. The repository was located at https://github.com/raywu-aaeon/Ryzen2000_4000.git, and it’s [...]
