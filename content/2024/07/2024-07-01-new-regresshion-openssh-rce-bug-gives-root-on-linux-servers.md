Title: New regreSSHion OpenSSH RCE bug gives root on Linux servers
Date: 2024-07-01T09:37:48-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Linux
Slug: 2024-07-01-new-regresshion-openssh-rce-bug-gives-root-on-linux-servers

[Source](https://www.bleepingcomputer.com/news/security/new-regresshion-openssh-rce-bug-gives-root-on-linux-servers/){:target="_blank" rel="noopener"}

> A new OpenSSH unauthenticated remote code execution (RCE) vulnerability dubbed "regreSSHion" gives root privileges on glibc-based Linux systems. [...]
