Title: The president ordered a board to probe a massive Russian cyberattack. It never did.
Date: 2024-07-08T17:52:09+00:00
Author: ProPublica
Category: Ars Technica
Tags: Biz & IT;Security;cyberattack;microsoft;russian hacking;solarwinds;syndication
Slug: 2024-07-08-the-president-ordered-a-board-to-probe-a-massive-russian-cyberattack-it-never-did

[Source](https://arstechnica.com/?p=2035563){:target="_blank" rel="noopener"}

> Enlarge (credit: Avishek Das/SOPA Images/LightRocket via Getty Images ) This story was originally published by ProPublica. Investigating how the world’s largest software provider handles the security of its own ubiquitous products. After Russian intelligence launched one of the most devastating cyber espionage attacks in history against US government agencies, the Biden administration set up a new board and tasked it to figure out what happened—and tell the public. Read 63 remaining paragraphs | Comments [...]
