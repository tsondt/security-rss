Title: Juniper Networks flings out emergency patches for perfect 10 router vuln
Date: 2024-07-01T11:32:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-01-juniper-networks-flings-out-emergency-patches-for-perfect-10-router-vuln

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/01/emergency_patches_available_for_juniper/){:target="_blank" rel="noopener"}

> Get 'em while they're hot A critical vulnerability affecting Juniper Networks routers forced the vendor to issue emergency patches last week, and users are advised to apply them as soon as possible.... [...]
