Title: How to build user authentication into your gen AI app-accessing database
Date: 2024-07-19T16:00:00+00:00
Author: Jessica Chan
Category: GCP Security
Tags: Security & Identity;Developers & Practitioners;AI & Machine Learning
Slug: 2024-07-19-how-to-build-user-authentication-into-your-gen-ai-app-accessing-database

[Source](https://cloud.google.com/blog/products/ai-machine-learning/build-user-authentication-into-your-genai-app-accessing-database/){:target="_blank" rel="noopener"}

> Generative AI agents introduce immense potential to transform enterprise workspaces. Enterprises from almost every industry are exploring the possibilities of generative AI, adopting AI agents for purposes ranging from internal productivity to customer-facing support. However, while these AI agents can efficiently interact with data already in your databases to provide summaries, answer complex questions, and generate insightful content, concerns persist around safeguarding sensitive user data when integrating this technology. The data privacy dilemma Before discussing data privacy, we should define an important concept: RAG (Retrieval-Augmented Generation) is a machine-learning technique to optimize the accuracy and reliability of generative AI models. [...]
