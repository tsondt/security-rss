Title: Kaspersky offers free security software for six months in U.S. goodbye
Date: 2024-07-16T18:25:19-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-07-16-kaspersky-offers-free-security-software-for-six-months-in-us-goodbye

[Source](https://www.bleepingcomputer.com/news/security/kaspersky-offers-free-security-software-for-six-months-in-us-goodbye/){:target="_blank" rel="noopener"}

> Kaspersky is offering free security products for six months and tips for staying safe as a parting gift to consumers in the United States. [...]
