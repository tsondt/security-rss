Title: North Korean chap charged for attacks on US hospitals, military, NASA – and even China
Date: 2024-07-26T02:58:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-07-26-north-korean-chap-charged-for-attacks-on-us-hospitals-military-nasa-and-even-china

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/26/andariel_indictment_north_korea/){:target="_blank" rel="noopener"}

> Microsoft, Mandiant, weigh in with info about methods used by Andariel gang alleged to have made many, many, heists The US Department of Justice on Thursday charged a North Korean national over a series of ransomware attacks on stateside hospitals and healthcare providers, US defense companies, NASA, and even a Chinese target.... [...]
