Title: Massive AT&T data breach exposes call logs of 109 million customers
Date: 2024-07-12T09:37:32-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-07-12-massive-att-data-breach-exposes-call-logs-of-109-million-customers

[Source](https://www.bleepingcomputer.com/news/security/massive-atandt-data-breach-exposes-call-logs-of-109-million-customers/){:target="_blank" rel="noopener"}

> AT&T is warning of a massive data breach where threat actors stole the call logs for approximately 109 million customers, or nearly all of its mobile customers, from an online database on the company's Snowflake account. [...]
