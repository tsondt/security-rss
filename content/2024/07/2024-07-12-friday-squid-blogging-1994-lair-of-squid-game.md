Title: Friday Squid Blogging: 1994 Lair of Squid Game
Date: 2024-07-12T21:01:51+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-07-12-friday-squid-blogging-1994-lair-of-squid-game

[Source](https://www.schneier.com/blog/archives/2024/07/friday-squid-blogging-1994-lair-of-squid-game.html){:target="_blank" rel="noopener"}

> I didn’t know : In 1994, Hewlett-Packard released a miracle machine: the HP 200LX pocket-size PC. In the depths of the device, among the MS-DOS productivity apps built into its fixed memory, there lurked a first-person maze game called Lair of Squid. [...] In Lair of Squid, you’re trapped in an underwater labyrinth, seeking a way out while avoiding squid roaming the corridors. A collision with any cephalopod results in death. To progress through each stage and ascend to the surface, you locate the exit and provide a hidden, scrambled code word. The password is initially displayed as asterisks, with [...]
