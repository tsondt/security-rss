Title: AWS renews TISAX certification (Information with Very High Protection Needs (AL3)) across 19 regions
Date: 2024-07-15T12:52:37+00:00
Author: Janice Leung
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Certification;Compliance;Security Blog;TISAX
Slug: 2024-07-15-aws-renews-tisax-certification-information-with-very-high-protection-needs-al3-across-19-regions

[Source](https://aws.amazon.com/blogs/security/aws-achieves-tisax-certification-information-with-very-high-protection-needs-al3-2/){:target="_blank" rel="noopener"}

> We’re excited to announce the successful completion of the Trusted Information Security Assessment Exchange (TISAX) assessment on June 11, 2024 for 19 AWS Regions. These Regions renewed the Information with Very High Protection Needs (AL3) label for the control domains Information Handling and Data Protection. This alignment with TISAX requirements demonstrates our continued commitment to [...]
