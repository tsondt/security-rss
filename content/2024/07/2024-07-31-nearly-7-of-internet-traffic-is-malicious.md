Title: Nearly 7% of Internet Traffic Is Malicious
Date: 2024-07-31T15:55:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;denial of service;malware;spam;vulnerabilities
Slug: 2024-07-31-nearly-7-of-internet-traffic-is-malicious

[Source](https://www.schneier.com/blog/archives/2024/07/nearly-7-of-internet-traffic-is-malicious.html){:target="_blank" rel="noopener"}

> Cloudflare reports on the state of applications security. It claims that 6.8% of Internet traffic is malicious. And that CVEs are exploited as quickly as 22 minutes after proof-of-concepts are published. News articles. [...]
