Title: Traeger security bugs bad news for grillers with neighborly beef
Date: 2024-07-03T16:24:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-03-traeger-security-bugs-bad-news-for-grillers-with-neighborly-beef

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/03/traeger_security_bugs/){:target="_blank" rel="noopener"}

> Never risk it when it comes to brisket – make sure those updates are applied Keen meatheads better hope they haven't angered any cybersecurity folk before allowing their Traeger grills to update because a new high-severity vulnerability could be used for all kinds of high jinks.... [...]
