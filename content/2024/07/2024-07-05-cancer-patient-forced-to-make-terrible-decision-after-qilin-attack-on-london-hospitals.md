Title: Cancer patient forced to make terrible decision after Qilin attack on London hospitals
Date: 2024-07-05T17:00:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-05-cancer-patient-forced-to-make-terrible-decision-after-qilin-attack-on-london-hospitals

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/05/qilin_impacts_patient/){:target="_blank" rel="noopener"}

> Skin-sparing mastectomy and breast reconstruction scrapped as result of ransomware at supplier Exclusive The latest figures suggest that around 1,500 medical procedures have been canceled across some of London's biggest hospitals in the four weeks since Qilin's ransomware attack hit pathology services provider Synnovis. But perhaps no single person was affected as severely as Johanna Groothuizen.... [...]
