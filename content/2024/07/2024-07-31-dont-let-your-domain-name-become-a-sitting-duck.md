Title: Don’t Let Your Domain Name Become a “Sitting Duck”
Date: 2024-07-31T12:06:45+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Time to Patch;Web Fraud 2.0;APWG;Dave Mitchell;Digicert;Digital Ocean;DNSMadeEasy;Eclypsium;Hostinger;Infoblox;Matthew Bryant;sitting duck domains;Steve Job
Slug: 2024-07-31-dont-let-your-domain-name-become-a-sitting-duck

[Source](https://krebsonsecurity.com/2024/07/dont-let-your-domain-name-become-a-sitting-duck/){:target="_blank" rel="noopener"}

> More than a million domain names — including many registered by Fortune 100 firms and brand protection companies — are vulnerable to takeover by cybercriminals thanks to authentication weaknesses at a number of large web hosting providers and domain registrars, new research finds. Image: Shutterstock. Your Web browser knows how to find a site like example.com thanks to the global Domain Name System (DNS), which serves as a kind of phone book for the Internet by translating human-friendly website names (example.com) into numeric Internet addresses. When someone registers a domain name, the registrar will typically provide two sets of DNS [...]
