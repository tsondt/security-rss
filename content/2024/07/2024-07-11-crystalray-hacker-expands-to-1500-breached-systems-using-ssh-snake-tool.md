Title: CRYSTALRAY hacker expands to 1,500 breached systems using SSH-Snake tool
Date: 2024-07-11T11:09:55-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-07-11-crystalray-hacker-expands-to-1500-breached-systems-using-ssh-snake-tool

[Source](https://www.bleepingcomputer.com/news/security/crystalray-hacker-expands-to-1-500-breached-systems-using-ssh-snake-tool/){:target="_blank" rel="noopener"}

> A new threat actor known as CRYSTALRAY has significantly broadened its targeting scope with new tactics and exploits, now counting over 1,500 victims whose credentials were stolen and cryptominers deployed. [...]
