Title: Formula 1 governing body discloses data breach after email hacks
Date: 2024-07-03T11:53:41-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-03-formula-1-governing-body-discloses-data-breach-after-email-hacks

[Source](https://www.bleepingcomputer.com/news/security/formula-1-governing-body-discloses-data-breach-after-email-hacks/){:target="_blank" rel="noopener"}

> FIA (Fédération Internationale de l'Automobile), the auto racing governing body since the 1950s, says attackers gained access to personal data after compromising several FIA email accounts in a phishing attack. [...]
