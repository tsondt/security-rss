Title: School gets an F for using facial recognition on kids in canteen
Date: 2024-07-24T08:32:09+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2024-07-24-school-gets-an-f-for-using-facial-recognition-on-kids-in-canteen

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/24/essex_school_facial_recognition/){:target="_blank" rel="noopener"}

> Watchdog reprimand follows similar cases in 2021 The UK's data protection watchdog has reprimanded a school in Essex for using facial recognition for canteen payments, nearly three years after other schools were warned about doing the same.... [...]
