Title: MediSecure: Ransomware gang stole data of 12.9 million people
Date: 2024-07-19T13:05:30-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-19-medisecure-ransomware-gang-stole-data-of-129-million-people

[Source](https://www.bleepingcomputer.com/news/security/medisecure-ransomware-gang-stole-data-of-129-million-people/){:target="_blank" rel="noopener"}

> MediSecure, an Australian prescription delivery service provider, revealed that roughly 12.9 million people had their personal and health information stolen in an April ransomware attack. [...]
