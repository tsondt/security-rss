Title: HealthEquity data breach exposes protected health information
Date: 2024-07-03T15:34:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-07-03-healthequity-data-breach-exposes-protected-health-information

[Source](https://www.bleepingcomputer.com/news/security/healthequity-data-breach-exposes-protected-health-information/){:target="_blank" rel="noopener"}

> Healthcare fintech firm HealthEquity is warning that it suffered a data breach after a partner's account was compromised and used to access the Company's systems to steal protected health information. [...]
