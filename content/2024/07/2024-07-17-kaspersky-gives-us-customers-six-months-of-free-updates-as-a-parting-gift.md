Title: Kaspersky gives US customers six months of free updates as a parting gift
Date: 2024-07-17T18:20:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-07-17-kaspersky-gives-us-customers-six-months-of-free-updates-as-a-parting-gift

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/17/kaspersky_goodbye_gift/){:target="_blank" rel="noopener"}

> So long, farewell, do svidaniya, goodbye Updated Embattled Russian infosec shop Kaspersky is giving US customers six months of security updates for free as a parting gift as Uncle Sam kicks the antivirus maker out of the American market.... [...]
