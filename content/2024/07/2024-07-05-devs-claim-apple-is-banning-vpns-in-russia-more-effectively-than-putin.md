Title: Devs claim Apple is banning VPNs in Russia 'more effectively' than Putin
Date: 2024-07-05T21:27:10+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-07-05-devs-claim-apple-is-banning-vpns-in-russia-more-effectively-than-putin

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/05/kremlin_internet_censor_vpn/){:target="_blank" rel="noopener"}

> Mozilla shows guts with its extensions – but that's the way the Cook, he crumbles Updated At least two VPNs are no longer available for Russian iPhone users, seemingly after the Kremlin's internet regulatory agency Roskomnadzor demanded Apple take them down.... [...]
