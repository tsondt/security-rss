Title: Call, text logs for 110M AT&amp;T customers stolen from compromised cloud storage
Date: 2024-07-12T14:09:27+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-07-12-call-text-logs-for-110m-att-customers-stolen-from-compromised-cloud-storage

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/12/att_110_million_call_text_logs/){:target="_blank" rel="noopener"}

> Snowflake? Snowflake AT&T has admitted that cyberattackers grabbed a load of its data for the second time this year, and if you think the first haul was big, you haven't seen anything: This latest one includes data on "nearly all" AT&T wireless customers - and those served by mobile virtual network operators (MVNOs) running on AT&T's network.... [...]
