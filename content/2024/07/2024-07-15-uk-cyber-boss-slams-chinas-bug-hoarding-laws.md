Title: UK cyber-boss slams China's bug-hoarding laws
Date: 2024-07-15T00:03:38+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-07-15-uk-cyber-boss-slams-chinas-bug-hoarding-laws

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/15/asia_technology_news_weekly_roundup/){:target="_blank" rel="noopener"}

> Plus: Japanese scientists ID ancient supernova; AWS dismisses China trouble rumor; and more ASIA IN BRIEF The interim CEO of the UK's National Cyber Security Centre (NCSC) has criticized China's approach to bug reporting.... [...]
