Title: Shopify denies it was hacked, links stolen data to third-party app
Date: 2024-07-07T10:09:18-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-07-07-shopify-denies-it-was-hacked-links-stolen-data-to-third-party-app

[Source](https://www.bleepingcomputer.com/news/security/shopify-denies-it-was-hacked-links-stolen-data-to-third-party-app/){:target="_blank" rel="noopener"}

> E-commerce platform Shopify denies it suffered a data breach after a threat actor began selling customer data they claim was stolen from the company's network. [...]
