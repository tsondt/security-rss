Title: US border cops really must get a warrant in NY before searching your phones, devices
Date: 2024-07-29T20:17:50+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-07-29-us-border-cops-really-must-get-a-warrant-in-ny-before-searching-your-phones-devices

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/29/us_border_cops_warrant/){:target="_blank" rel="noopener"}

> Do we really want to bother SCOTUS with this, friends? Surely they're way too busy to take a look US border agents must obtain a warrant, in New York at least, to search anyone's phone and other electronic device when traveling in or out of the country, another federal judge has ruled.... [...]
