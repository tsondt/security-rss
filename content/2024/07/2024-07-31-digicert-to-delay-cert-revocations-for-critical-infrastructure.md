Title: DigiCert to delay cert revocations for critical infrastructure
Date: 2024-07-31T16:18:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-07-31-digicert-to-delay-cert-revocations-for-critical-infrastructure

[Source](https://www.bleepingcomputer.com/news/security/digicert-to-delay-cert-revocations-for-critical-infrastructure/){:target="_blank" rel="noopener"}

> DigiCert urges critical infrastructure operators to request a delay if they cannot reissue their certificates, as required by an ongoing certificate mass-revocation process announced on Tuesday. [...]
