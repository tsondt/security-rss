Title: Providing Security Updates to Automobile Software
Date: 2024-07-30T11:07:05+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;books;cars;cybersecurity;software
Slug: 2024-07-30-providing-security-updates-to-automobile-software

[Source](https://www.schneier.com/blog/archives/2024/07/providing-security-updates-to-automobile-software.html){:target="_blank" rel="noopener"}

> Auto manufacturers are just starting to realize the problems of supporting the software in older models: Today’s phones are able to receive updates six to eight years after their purchase date. Samsung and Google provide Android OS updates and security updates for seven years. Apple halts servicing products seven years after they stop selling them. That might not cut it in the auto world, where the average age of cars on US roads is only going up. A recent report found that cars and trucks just reached a new record average age of 12.6 years, up two months from 2023. [...]
