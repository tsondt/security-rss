Title: Apple Is Alerting iPhone Users of Spyware Attacks
Date: 2024-07-11T15:09:32+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;cybersecurity;iPhone;spyware
Slug: 2024-07-11-apple-is-alerting-iphone-users-of-spyware-attacks

[Source](https://www.schneier.com/blog/archives/2024/07/apple-is-alerting-iphone-users-of-spyware-attacks.html){:target="_blank" rel="noopener"}

> Not a lot of details : Apple has issued a new round of threat notifications to iPhone users across 98 countries, warning them of potential mercenary spyware attacks. It’s the second such alert campaign from the company this year, following a similar notification sent to users in 92 nations in April. [...]
