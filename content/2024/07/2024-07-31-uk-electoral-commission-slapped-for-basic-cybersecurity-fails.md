Title: UK Electoral Commission slapped for basic cybersecurity fails
Date: 2024-07-31T08:30:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-07-31-uk-electoral-commission-slapped-for-basic-cybersecurity-fails

[Source](https://go.theregister.com/feed/www.theregister.com/2024/07/31/uk_electoral_commission_ico/){:target="_blank" rel="noopener"}

> It took 13 months to notice 40 million voters' data was compromised The UK's Electoral Commission has received a formal slap on the wrist for a litany of security failings that led to the theft of personal data belonging to around 40 million voters.... [...]
