Title: Ransomware fiends boast they've stolen 1.4TB from US pharmacy network
Date: 2024-11-13T19:10:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-13-ransomware-fiends-boast-theyve-stolen-14tb-from-us-pharmacy-network

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/13/embargo_ransomware_breach_aap/){:target="_blank" rel="noopener"}

> American Associated Pharmacies yet to officially confirm infection American Associated Pharmacies (AAP) is the latest US healthcare organization to have had its data stolen and encrypted by cyber-crooks, it is feared.... [...]
