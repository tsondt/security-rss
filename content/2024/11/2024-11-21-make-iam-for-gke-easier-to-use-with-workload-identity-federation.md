Title: Make IAM for GKE easier to use with Workload Identity Federation
Date: 2024-11-21T17:00:00+00:00
Author: Shaun Liu
Category: GCP Security
Tags: Containers & Kubernetes;Security & Identity
Slug: 2024-11-21-make-iam-for-gke-easier-to-use-with-workload-identity-federation

[Source](https://cloud.google.com/blog/products/identity-security/make-iam-for-gke-easier-to-use-with-workload-identity-federation/){:target="_blank" rel="noopener"}

> At Google Cloud, we work to continually improve our platform’s security capabilities to deliver the most trusted cloud. As part of this goal, we’re helping our users move away from less secure authentication methods such as long-lived, unauditable, service account keys towards more secure alternatives when authenticating to Google Cloud APIs and services. In the context of Kubernetes workloads, there have been three ways users can do this: Export credentials and mount as a secret in the Pod at runtime. This is done using service account keys but could be a security risk if the keys are not managed correctly. [...]
