Title: Mandatory MFA is coming to Google Cloud. Here’s what you need to know
Date: 2024-11-04T17:00:00+00:00
Author: Mayank Upadhyay
Category: GCP Security
Tags: Security & Identity
Slug: 2024-11-04-mandatory-mfa-is-coming-to-google-cloud-heres-what-you-need-to-know

[Source](https://cloud.google.com/blog/products/identity-security/mandatory-mfa-is-coming-to-google-cloud-heres-what-you-need-to-know/){:target="_blank" rel="noopener"}

> At Google Cloud, we’re committed to providing the strongest security for our customers. As pioneers in bringing multi-factor authentication (MFA) to millions of Google users worldwide, we've seen firsthand how it strengthens security without sacrificing a smooth and convenient online experience. That’s why we will soon require MFA for all Google Cloud users who currently sign in with just a password. We will be implementing mandatory MFA for Google Cloud in a phased approach that will roll out to all users worldwide during 2025. To ensure a smooth transition, Google Cloud will provide advance notification to enterprises and users along [...]
