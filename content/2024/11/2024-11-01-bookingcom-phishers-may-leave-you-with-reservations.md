Title: Booking.com Phishers May Leave You With Reservations
Date: 2024-11-01T21:12:38+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;The Coming Storm;Web Fraud 2.0;booking.com;phishing;secureworks;spear phishing
Slug: 2024-11-01-bookingcom-phishers-may-leave-you-with-reservations

[Source](https://krebsonsecurity.com/2024/11/booking-com-phishers-may-leave-you-with-reservations/){:target="_blank" rel="noopener"}

> A number of cybercriminal innovations are making it easier for scammers to cash in on your upcoming travel plans. This story examines a recent spear-phishing campaign that ensued when a California hotel had its booking.com credentials stolen. We’ll also explore an array of cybercrime services aimed at phishers who target hotels that rely on the world’s most visited travel website. According to the market share website statista.com, booking.com is by far the Internet’s busiest travel service, with nearly 550 million visits in September. KrebsOnSecurity last week heard from a reader whose close friend received a targeted phishing message within the [...]
