Title: Important changes to CloudTrail events for AWS IAM Identity Center
Date: 2024-11-19T18:20:01+00:00
Author: Arthur Mnev
Category: AWS Security
Tags: Advanced (300);Announcements;Best Practices;Security, Identity, & Compliance;Amazon CloudTrail;AWS CloudTrail;AWS IAM Identity Center;CloudTrail;IAM Identity Center;Identity;Security;Security Blog
Slug: 2024-11-19-important-changes-to-cloudtrail-events-for-aws-iam-identity-center

[Source](https://aws.amazon.com/blogs/security/modifications-to-aws-cloudtrail-event-data-of-iam-identity-center/){:target="_blank" rel="noopener"}

> AWS IAM Identity Center is streamlining its AWS CloudTrail events by including only essential fields that are necessary for workflows like audit and incident response. This change simplifies user identification in CloudTrail, addressing customer feedback. It also enhances correlation between IAM Identity Center users and external directory services, such as Okta Universal Directory or Microsoft [...]
