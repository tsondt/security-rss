Title: Fake Bitwarden ads on Facebook push info-stealing Chrome extension
Date: 2024-11-18T12:05:41-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-11-18-fake-bitwarden-ads-on-facebook-push-info-stealing-chrome-extension

[Source](https://www.bleepingcomputer.com/news/security/fake-bitwarden-ads-on-facebook-push-info-stealing-chrome-extension/){:target="_blank" rel="noopener"}

> Fake Bitwarden password manager advertisements on Facebook are pushing a malicious Google Chrome extension that collects and steals sensitive user data from the browser. [...]
