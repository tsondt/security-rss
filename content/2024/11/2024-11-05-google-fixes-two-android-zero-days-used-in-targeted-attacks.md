Title: Google fixes two Android zero-days used in targeted attacks
Date: 2024-11-05T09:30:58-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2024-11-05-google-fixes-two-android-zero-days-used-in-targeted-attacks

[Source](https://www.bleepingcomputer.com/news/security/google-fixes-two-android-zero-days-used-in-targeted-attacks/){:target="_blank" rel="noopener"}

> Google fixed two actively exploited Android zero-day flaws as part of its November security updates, addressing a total of 51 vulnerabilities. [...]
