Title: The Scale of Geoblocking by Nation
Date: 2024-11-22T12:06:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;Cuba;national security policy;privacy;surveillance
Slug: 2024-11-22-the-scale-of-geoblocking-by-nation

[Source](https://www.schneier.com/blog/archives/2024/11/the-scale-of-geoblocking-by-nation.html){:target="_blank" rel="noopener"}

> Interesting analysis : We introduce and explore a little-known threat to digital equality and freedom­websites geoblocking users in response to political risks from sanctions. U.S. policy prioritizes internet freedom and access to information in repressive regimes. Clarifying distinctions between free and paid websites, allowing trunk cables to repressive states, enforcing transparency in geoblocking, and removing ambiguity about sanctions compliance are concrete steps the U.S. can take to ensure it does not undermine its own aims. The paper: “ Digital Discrimination of Users in Sanctioned States: The Case of the Cuba Embargo “: Abstract : We present one of the first [...]
