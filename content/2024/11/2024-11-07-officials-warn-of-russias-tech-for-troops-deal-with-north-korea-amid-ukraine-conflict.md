Title: Officials warn of Russia's tech-for-troops deal with North Korea amid Ukraine conflict
Date: 2024-11-07T02:30:14+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-11-07-officials-warn-of-russias-tech-for-troops-deal-with-north-korea-amid-ukraine-conflict

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/07/russia_tech_transfer_north_korea/){:target="_blank" rel="noopener"}

> 10,000 of Kim Jong Un's soldiers believed to be headed for front line The EU has joined US and South Korean officials in expressing concern over a Russian transfer of technology to North Korea in return for military assistance against Ukraine.... [...]
