Title: Asda security chief replaced, retailer sheds jobs during Walmart tech divorce
Date: 2024-11-14T09:30:12+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2024-11-14-asda-security-chief-replaced-retailer-sheds-jobs-during-walmart-tech-divorce

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/14/senior_tech_departure_asda/){:target="_blank" rel="noopener"}

> British grocer's workers called back to office as clock ticks for contractors The head of tech security at Asda, the UK's third-largest food retailer, has left amid an ongoing tech divorce from US grocery giant Walmart.... [...]
