Title: Implement effective data authorization mechanisms to secure your data used in generative AI applications
Date: 2024-11-05T16:23:38+00:00
Author: Riggs Goodman III
Category: AWS Security
Tags: Advanced (300);Amazon Bedrock;Best Practices;Generative AI;Security, Identity, & Compliance;Security Blog
Slug: 2024-11-05-implement-effective-data-authorization-mechanisms-to-secure-your-data-used-in-generative-ai-applications

[Source](https://aws.amazon.com/blogs/security/implement-effective-data-authorization-mechanisms-to-secure-your-data-used-in-generative-ai-applications/){:target="_blank" rel="noopener"}

> Data security and data authorization, as distinct from user authorization, is a critical component of business workload architectures. Its importance has grown with the evolution of artificial intelligence (AI) technology, with generative AI introducing new opportunities to use internal data sources with large language models (LLMs) and multimodal foundation models (FMs) to augment model outputs. [...]
