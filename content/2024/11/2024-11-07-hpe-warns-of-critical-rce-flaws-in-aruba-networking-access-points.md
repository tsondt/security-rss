Title: HPE warns of critical RCE flaws in Aruba Networking access points
Date: 2024-11-07T10:47:53-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-11-07-hpe-warns-of-critical-rce-flaws-in-aruba-networking-access-points

[Source](https://www.bleepingcomputer.com/news/security/hpe-warns-of-critical-rce-flaws-in-aruba-networking-access-points/){:target="_blank" rel="noopener"}

> Hewlett Packard Enterprise (HPE) released updates for Instant AOS-8 and AOS-10 software to address two critical vulnerabilities in Aruba Networking Access Points. [...]
