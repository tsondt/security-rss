Title: Friday Squid Blogging: Squid-A-Rama in Des Moines
Date: 2024-11-08T22:04:01+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-11-08-friday-squid-blogging-squid-a-rama-in-des-moines

[Source](https://www.schneier.com/blog/archives/2024/11/friday-squid-blogging-squid-a-rama-in-des-moines.html){:target="_blank" rel="noopener"}

> Squid-A-Rama will be in Des Moines at the end of the month. Visitors will be able to dissect squid, explore fascinating facts about the species, and witness a live squid release conducted by local divers. How are they doing a live squid release? Simple: this is Des Moines, Washington; not Des Moines, Iowa. Blog moderation policy. [...]
