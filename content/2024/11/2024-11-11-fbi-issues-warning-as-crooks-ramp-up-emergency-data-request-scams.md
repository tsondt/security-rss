Title: FBI issues warning as crooks ramp up emergency data request scams
Date: 2024-11-11T16:23:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-11-fbi-issues-warning-as-crooks-ramp-up-emergency-data-request-scams

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/11/fraudulent_edr_emails/){:target="_blank" rel="noopener"}

> Just because it's.gov doesn't mean that email is trustworthy Cybercrooks abusing emergency data requests in the US isn't new, but the FBI says it's becoming a more pronounced issue as the year draws to a close.... [...]
