Title: From Naptime to Big Sleep: Using Large Language Models To Catch Vulnerabilities In Real-World Code
Date: 2024-11-01T08:12:00.003000-07:00
Author: Google Project Zero (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2024-11-01-from-naptime-to-big-sleep-using-large-language-models-to-catch-vulnerabilities-in-real-world-code

[Source](https://googleprojectzero.blogspot.com/2024/10/from-naptime-to-big-sleep.html){:target="_blank" rel="noopener"}

> Posted by the Big Sleep team Introduction In our previous post, Project Naptime: Evaluating Offensive Security Capabilities of Large Language Models, we introduced our framework for large-language-model-assisted vulnerability research and demonstrated its potential by improving the state-of-the-art performance on Meta's CyberSecEval2 benchmarks. Since then, Naptime has evolved into Big Sleep, a collaboration between Google Project Zero and Google DeepMind. Today, we're excited to share the first real-world vulnerability discovered by the Big Sleep agent : an exploitable stack buffer underflow in SQLite, a widely used open source database engine. We discovered the vulnerability and reported it to the developers in [...]
