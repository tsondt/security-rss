Title: Google blocked 1,000-plus pro-China fake news websites from its search results
Date: 2024-11-25T06:29:12+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-11-25-google-blocked-1000-plus-pro-china-fake-news-websites-from-its-search-results

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/25/google_beijing_propaganda/){:target="_blank" rel="noopener"}

> Beijing's propaganda buddies aren't just using social media Google’s Threat Intelligence Group has blocked a network China-related firms from its search results for operating fake news services and websites.... [...]
