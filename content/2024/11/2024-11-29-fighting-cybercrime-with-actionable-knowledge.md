Title: Fighting cybercrime with actionable knowledge
Date: 2024-11-29T09:19:11+00:00
Author: Guy Matthews
Category: The Register
Tags: 
Slug: 2024-11-29-fighting-cybercrime-with-actionable-knowledge

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/29/fighting_cybercrime_with_actionable_knowledge/){:target="_blank" rel="noopener"}

> A reason to celebrate SANS and its 35 years of cyber security training Sponsored Post Cybercrime never sleeps. As threats continue to evolve and attack surfaces become broader and harder to defend, it has never been more important for the good guys to keep their skills sharp and their knowledge up to date.... [...]
