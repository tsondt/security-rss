Title: US senators propose law to require bare minimum security standards
Date: 2024-11-26T16:00:15+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-26-us-senators-propose-law-to-require-bare-minimum-security-standards

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/26/us_senators_healthcare_cybersecurity/){:target="_blank" rel="noopener"}

> In case anyone forgot about Change Healthcare American hospitals and healthcare organizations would be required to adopt multi-factor authentication (MFA) and other minimum cybersecurity standards under new legislation proposed by a bipartisan group of US senators.... [...]
