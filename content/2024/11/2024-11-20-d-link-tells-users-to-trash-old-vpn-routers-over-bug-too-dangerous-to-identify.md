Title: D-Link tells users to trash old VPN routers over bug too dangerous to identify
Date: 2024-11-20T14:32:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-20-d-link-tells-users-to-trash-old-vpn-routers-over-bug-too-dangerous-to-identify

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/20/dlink_rip_replace_router/){:target="_blank" rel="noopener"}

> Vendor offers 20% discount on new model, but not patches Owners of older models of D-Link VPN routers are being told to retire and replace their devices following the disclosure of a serious remote code execution (RCE) vulnerability.... [...]
