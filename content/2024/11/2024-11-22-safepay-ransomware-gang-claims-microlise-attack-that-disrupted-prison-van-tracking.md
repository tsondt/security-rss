Title: SafePay ransomware gang claims Microlise attack that disrupted prison van tracking
Date: 2024-11-22T08:34:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-22-safepay-ransomware-gang-claims-microlise-attack-that-disrupted-prison-van-tracking

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/22/safepay_microlise/){:target="_blank" rel="noopener"}

> Fledgling band of crooks says it stole 1.2 TB of data The new SafePay ransomware gang has claimed responsibility for the attack on UK telematics biz Microlise, giving the company less than 24 hours to pay its extortion demands before leaking data.... [...]
