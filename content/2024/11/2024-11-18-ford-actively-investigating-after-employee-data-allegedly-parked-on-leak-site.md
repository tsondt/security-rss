Title: Ford 'actively investigating' after employee data allegedly parked on leak site
Date: 2024-11-18T23:58:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-18-ford-actively-investigating-after-employee-data-allegedly-parked-on-leak-site

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/18/ford_actively_investigating_breach/){:target="_blank" rel="noopener"}

> Plus: Maxar Space Systems confirms employee info stolen in digital intrusion Ford Motor Company says it is looking into allegations of a data breach after attackers claimed to have stolen an internal database containing 44,000 customer records and dumped the info on a cyber crime souk for anyone to "enjoy."... [...]
