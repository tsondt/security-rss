Title: Bitfinex burglar bags 5 years behind bars for Bitcoin heist
Date: 2024-11-15T14:09:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-15-bitfinex-burglar-bags-5-years-behind-bars-for-bitcoin-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/15/bitfinix_intruder_sentenced/){:target="_blank" rel="noopener"}

> A nervous wait for rapper wife who also faces a stint in the clink The US is sending the main figure behind the 2016 intrusion at crypto exchange Bitfinex to prison for five years after he stole close to 120,000 Bitcoin.... [...]
