Title: Britain Putin up stronger AI defences to counter growing cyber threats
Date: 2024-11-26T06:29:13+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-11-26-britain-putin-up-stronger-ai-defences-to-counter-growing-cyber-threats

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/26/uk_ai_security/){:target="_blank" rel="noopener"}

> 'Be in no doubt: the UK and others in this room are watching Russia' The government of the United Kingdom on Monday announced the formation of a Laboratory for AI Security Research (LASR) to make the nation more resilient to AI-powered cyber threats from Russia.... [...]
