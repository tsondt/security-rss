Title: Mega US healthcare payments network restores system 9 months after ransomware attack
Date: 2024-11-20T18:01:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-20-mega-us-healthcare-payments-network-restores-system-9-months-after-ransomware-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/20/change_healthcares_clearinghouse_services/){:target="_blank" rel="noopener"}

> Change Healthcare’s $2 billion recovery is still a work in progress Still reeling from its February ransomware attack, Change Healthcare confirms its clearinghouse services are back up and running, almost exactly nine months since the digital disruption began.... [...]
