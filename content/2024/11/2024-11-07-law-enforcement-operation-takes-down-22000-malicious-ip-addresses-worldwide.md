Title: Law enforcement operation takes down 22,000 malicious IP addresses worldwide
Date: 2024-11-07T23:12:23+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;cybercrime;Interpol;police
Slug: 2024-11-07-law-enforcement-operation-takes-down-22000-malicious-ip-addresses-worldwide

[Source](https://arstechnica.com/information-technology/2024/11/law-enforcement-operation-takes-down-22000-malicious-ip-addresses-worldwide/){:target="_blank" rel="noopener"}

> An international coalition of police agencies has taken a major whack at criminals accused of running a host of online scams, including phishing, the stealing of account credentials and other sensitive data, and the spreading of ransomware, Interpol said recently. The operation, which ran from the beginning of April through the end of August, resulted in the arrest of 41 people and the takedown of 1,037 servers and other infrastructure running on 22,000 IP addresses. Synergia II, as the operation was named, was the work of multiple law enforcement agencies across the world, as well as three cybersecurity organizations. A [...]
