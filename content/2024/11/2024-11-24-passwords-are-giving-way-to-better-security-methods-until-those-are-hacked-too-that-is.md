Title: Passwords are giving way to better security methods – until those are hacked too, that is
Date: 2024-11-24T13:00:12+00:00
Author: Gene Marks
Category: The Guardian
Tags: US small business;Data and computer security;Microsoft;Apple;Google;Samsung;Business;Technology;US news
Slug: 2024-11-24-passwords-are-giving-way-to-better-security-methods-until-those-are-hacked-too-that-is

[Source](https://www.theguardian.com/business/2024/nov/24/small-business-data-security-methods){:target="_blank" rel="noopener"}

> It’s a war that will never end. But for small-business owners, it’s all about managing risk while reaping rewards We humans are simply too dumb to use passwords. A recent study from password manager NordPass found that “secret” was the most commonly used password in 2024. That was followed by “123456” and “password”. So let’s all give praise that the password is dying. Yes, we know that we should be using 20-letter passwords with weird symbols and numbers, but our minds can’t cope. We use the same password for many accounts, be it for a newsletter subscription or our life [...]
