Title: City of Columbus: Data of 500,000 stolen in July ransomware attack
Date: 2024-11-04T09:52:01-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-11-04-city-of-columbus-data-of-500000-stolen-in-july-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/city-of-columbus-data-of-500-000-stolen-in-july-ransomware-attack/){:target="_blank" rel="noopener"}

> ​The City of Columbus, Ohio, notified 500,000 individuals that a ransomware gang stole their personal and financial information in a July 2024 cyberattack. [...]
