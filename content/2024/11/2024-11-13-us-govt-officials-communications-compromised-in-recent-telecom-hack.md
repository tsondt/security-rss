Title: US govt officials’ communications compromised in recent telecom hack
Date: 2024-11-13T17:34:15-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-11-13-us-govt-officials-communications-compromised-in-recent-telecom-hack

[Source](https://www.bleepingcomputer.com/news/security/chinese-hackers-compromised-us-government-officials-private-communications-in-recent-telecom-breach/){:target="_blank" rel="noopener"}

> CISA and the FBI confirmed that Chinese hackers compromised the "private communications" of a "limited number" of government officials after breaching multiple U.S. broadband providers. [...]
