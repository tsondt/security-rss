Title: Signal introduces convenient "call links" for private group chats
Date: 2024-11-12T12:26:17-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Software;Security
Slug: 2024-11-12-signal-introduces-convenient-call-links-for-private-group-chats

[Source](https://www.bleepingcomputer.com/news/software/signal-introduces-convenient-call-links-for-private-group-chats/){:target="_blank" rel="noopener"}

> The Signal messenger application has announced a set of new features aimed at making private group chats more convenient and easier for people to join. [...]
