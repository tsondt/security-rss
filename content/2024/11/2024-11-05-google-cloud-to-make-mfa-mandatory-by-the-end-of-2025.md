Title: Google Cloud to make MFA mandatory by the end of 2025
Date: 2024-11-05T15:07:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2024-11-05-google-cloud-to-make-mfa-mandatory-by-the-end-of-2025

[Source](https://www.bleepingcomputer.com/news/security/google-cloud-to-make-mfa-mandatory-by-the-end-of-2025/){:target="_blank" rel="noopener"}

> Google has announced that multi-factor authentication (MFA) will be mandatory on all Cloud accounts by the end of 2025 to enhance security. [...]
