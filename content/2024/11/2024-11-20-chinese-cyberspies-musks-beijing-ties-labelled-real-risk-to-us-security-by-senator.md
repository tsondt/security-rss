Title: Chinese cyberspies, Musk’s Beijing ties, labelled ‘real risk’ to US security by senator
Date: 2024-11-20T23:50:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-20-chinese-cyberspies-musks-beijing-ties-labelled-real-risk-to-us-security-by-senator

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/20/musk_chinese_cyberspies/){:target="_blank" rel="noopener"}

> Meet Liminal Panda, which prowls telecom networks in South Asia and Africa A senior US senator has warned that American tech companies’ activities in China represent a national security risk, in a hearing that saw infosec biz CrowdStrike testify it has identified another cyber-espionage crew it believes is backed by Beijing.... [...]
