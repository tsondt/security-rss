Title: Criminals open DocuSign's Envelope API to make BEC special delivery
Date: 2024-11-05T18:34:13+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-11-05-criminals-open-docusigns-envelope-api-to-make-bec-special-delivery

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/05/docusigns_envelope_bec/){:target="_blank" rel="noopener"}

> Why? Because that's where the money is Business email compromise scammers are trying to up their success rate by using a DocuSign API.... [...]
