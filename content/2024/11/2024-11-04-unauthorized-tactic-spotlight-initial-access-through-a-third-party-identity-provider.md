Title: Unauthorized tactic spotlight: Initial access through a third-party identity provider
Date: 2024-11-04T14:00:19+00:00
Author: Steve de Vera
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Incident response;Security;Security Blog;threat detection
Slug: 2024-11-04-unauthorized-tactic-spotlight-initial-access-through-a-third-party-identity-provider

[Source](https://aws.amazon.com/blogs/security/unauthorized-tactic-spotlight-initial-access-through-a-third-party-identity-provider/){:target="_blank" rel="noopener"}

> Security is a shared responsibility between Amazon Web Services (AWS) and you, the customer. As a customer, the services you choose, how you connect them, and how you run your solutions can impact your security posture. To help customers fulfill their responsibilities and find the right balance for their business, under the shared responsibility model, [...]
