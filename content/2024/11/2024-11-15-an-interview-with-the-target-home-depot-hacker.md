Title: An Interview With the Target & Home Depot Hacker
Date: 2024-11-15T04:45:32+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Ne'er-Do-Well News;Pharma Wars;Aleksandr Ermakov;chronopay;Dmitri Golubov;Helkern;Home Depot breach;Hydra Market;MikeMike;Mikhail Lenin;Mikhail Shefel;pavel vrublevsky;Peter Vrublevsky;Sprut;Sugar ransomware;target breach
Slug: 2024-11-15-an-interview-with-the-target-home-depot-hacker

[Source](https://krebsonsecurity.com/2024/11/an-interview-with-the-target-home-depot-hacker/){:target="_blank" rel="noopener"}

> In December 2023, KrebsOnSecurity revealed the real-life identity of Rescator, the nickname used by a Russian cybercriminal who sold more than 100 million payment cards stolen from Target and Home Depot between 2013 and 2014. Moscow resident Mikhail Shefel, who confirmed using the Rescator identity in a recent interview, also admitted reaching out because he is broke and seeking publicity for several new money making schemes. Mikhail “Mike” Shefel’s former Facebook profile. Shefel has since legally changed his last name to Lenin. Mr. Shefel, who recently changed his legal surname to Lenin, was the star of last year’s story, Ten [...]
