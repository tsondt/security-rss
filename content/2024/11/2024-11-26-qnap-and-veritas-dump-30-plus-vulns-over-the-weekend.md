Title: QNAP and Veritas dump 30-plus vulns over the weekend
Date: 2024-11-26T10:29:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-26-qnap-and-veritas-dump-30-plus-vulns-over-the-weekend

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/26/qnap_veritas_vulnerabilities/){:target="_blank" rel="noopener"}

> Just what you want to find when you start a new week Updated Taiwanese NAS maker QNAP addressed 24 vulnerabilities across various products over the weekend.... [...]
