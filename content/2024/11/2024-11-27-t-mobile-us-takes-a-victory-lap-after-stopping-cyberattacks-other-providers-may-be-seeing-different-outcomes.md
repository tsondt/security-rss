Title: T-Mobile US takes a victory lap after stopping cyberattacks: 'Other providers may be seeing different outcomes'
Date: 2024-11-27T20:59:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-27-t-mobile-us-takes-a-victory-lap-after-stopping-cyberattacks-other-providers-may-be-seeing-different-outcomes

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/27/tmobile_cyberattack_victory_lap/){:target="_blank" rel="noopener"}

> Funny what putting more effort and resources into IT security can do Attackers - possibly China's Salt Typhoon cyber-espionage crew - compromised an unnamed wireline provider's network and used this access to try to break into T-Mobile US systems multiple times over the past few weeks, according to its Chief Security Officer Jeff Simon.... [...]
