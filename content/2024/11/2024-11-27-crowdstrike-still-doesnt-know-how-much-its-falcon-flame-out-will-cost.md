Title: CrowdStrike still doesn't know how much its Falcon flame-out will cost
Date: 2024-11-27T07:27:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-11-27-crowdstrike-still-doesnt-know-how-much-its-falcon-flame-out-will-cost

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/27/crowdstrike_q3_2025/){:target="_blank" rel="noopener"}

> Thinks customers may have forgiven it after revenue hits a record CrowdStrike can't yet confidently predict the financial impact of the failed update to its Falcon software that crashed millions of computers around the world last July, but is confident its third quarter results show customers can't find a better security product.... [...]
