Title: China's Volt Typhoon crew and its botnet surge back with a vengeance
Date: 2024-11-13T00:58:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-13-chinas-volt-typhoon-crew-and-its-botnet-surge-back-with-a-vengeance

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/13/china_volt_typhoon_back/){:target="_blank" rel="noopener"}

> Ohm, for flux sake China's Volt Typhoon crew and its botnet are back, compromising old Cisco routers once again to break into critical infrastructure networks and kick off cyberattacks, according to security researchers.... [...]
