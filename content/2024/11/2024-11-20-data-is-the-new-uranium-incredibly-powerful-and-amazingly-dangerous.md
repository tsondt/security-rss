Title: Data is the new uranium – incredibly powerful and amazingly dangerous
Date: 2024-11-20T07:15:09+00:00
Author: Mark Pesce
Category: The Register
Tags: 
Slug: 2024-11-20-data-is-the-new-uranium-incredibly-powerful-and-amazingly-dangerous

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/20/data_is_the_new_uranium/){:target="_blank" rel="noopener"}

> CISOs are quietly wishing they had less data, because the cost of management sometimes exceeds its value Column I recently got to play a 'fly on the wall' at a roundtable of chief information security officers. Beyond the expected griping and moaning about funding shortfalls and always-too-gullible users, I began to hear a new note: data has become a problem.... [...]
