Title: DARPA-backed voting system for soldiers abroad savaged
Date: 2024-11-21T19:27:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-11-21-darpa-backed-voting-system-for-soldiers-abroad-savaged

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/21/darpabacked_voting_system_for_soldiers/){:target="_blank" rel="noopener"}

> VotingWorks, developer of the system, disputes critics' claims An electronic voting project backed by DARPA – Uncle Sam's boffinry nerve center – to improve the process of absentee voting for American military personnel stationed abroad has been slammed by security researchers.... [...]
