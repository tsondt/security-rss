Title: Hacker in Snowflake Extortions May Be a U.S. Soldier
Date: 2024-11-27T00:45:48+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;DDoS-for-Hire;Ne'er-Do-Well News;Ransomware;The Coming Storm;AT&T;Boxfan;buttholio;Connor Riley Moucka;cyb3rph4nt0m;John Erin Binns;Judische;Kiberphant0m;Naver;Proman557;Reverseshell;Shi-Bot;Snowflake;South Korea;telekomterrorist;Vars_Secc;Verizon;Waifu
Slug: 2024-11-27-hacker-in-snowflake-extortions-may-be-a-us-soldier

[Source](https://krebsonsecurity.com/2024/11/hacker-in-snowflake-extortions-may-be-a-u-s-soldier/){:target="_blank" rel="noopener"}

> Two men have been arrested for allegedly stealing data from and extorting dozens of companies that used the cloud data storage company Snowflake, but a third suspect — a prolific hacker known as Kiberphant0m — remains at large and continues to publicly extort victims. However, this person’s identity may not remain a secret for long: A careful review of Kiberphant0m’s daily chats across multiple cybercrime personas suggests they are a U.S. Army soldier who is or was recently stationed in South Korea. Kiberphant0m’s identities on cybercrime forums and on Telegram and Discord chat channels have been selling data stolen from [...]
