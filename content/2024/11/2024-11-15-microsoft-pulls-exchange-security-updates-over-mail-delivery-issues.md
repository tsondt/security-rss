Title: Microsoft pulls Exchange security updates over mail delivery issues
Date: 2024-11-15T10:23:42-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-11-15-microsoft-pulls-exchange-security-updates-over-mail-delivery-issues

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-pulls-exchange-security-updates-over-mail-delivery-issues/){:target="_blank" rel="noopener"}

> Microsoft has pulled the November 2024 Exchange security updates released during this month's Patch Tuesday because of email delivery issues on servers using custom mail flow rules. [...]
