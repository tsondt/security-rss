Title: Financial institutions told to get their house in order before the next CrowdStrike strikes
Date: 2024-11-02T09:30:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-02-financial-institutions-told-to-get-their-house-in-order-before-the-next-crowdstrike-strikes

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/02/fca_it_resilience/){:target="_blank" rel="noopener"}

> Calls for improvements will soon turn into demands when new rules come into force The UK's finance regulator is urging all institutions under its remit to better prepare for IT meltdowns like that of CrowdStrike in July.... [...]
