Title: iPhones now auto-restart to block access to encrypted data after long idle times
Date: 2024-11-11T19:19:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Apple
Slug: 2024-11-11-iphones-now-auto-restart-to-block-access-to-encrypted-data-after-long-idle-times

[Source](https://www.bleepingcomputer.com/news/security/iphones-now-auto-restart-to-block-access-to-encrypted-data-after-long-idle-times/){:target="_blank" rel="noopener"}

> Apple has added a new security feature with the iOS 18.1 update released last month to ensure that iPhones automatically reboot after long idle periods to re-encrypt data and make it harder to extract. [...]
