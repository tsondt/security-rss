Title: LA housing authority confirms breach claimed by Cactus ransomware
Date: 2024-11-01T16:30:15-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-11-01-la-housing-authority-confirms-breach-claimed-by-cactus-ransomware

[Source](https://www.bleepingcomputer.com/news/security/la-housing-authority-confirms-breach-claimed-by-cactus-ransomware/){:target="_blank" rel="noopener"}

> The Housing Authority of the City of Los Angeles (HACLA), one of the largest public housing authorities in the United States, confirmed that a cyberattack hit its IT network after recent breach claims from the Cactus ransomware gang. [...]
