Title: A Kansas pig butchering: CEO who defrauded bank, church, friends gets 24 years
Date: 2024-11-05T20:30:10+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-11-05-a-kansas-pig-butchering-ceo-who-defrauded-bank-church-friends-gets-24-years

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/05/fbi_pig_kansas_cryptocurrency/){:target="_blank" rel="noopener"}

> FBI recovers just $8M after scam crashes Heartland Tri-State Bank The FBI has recovered $8 million in funds from a cryptocurrency scam that netted $47 million and devastated the Kansas city of Elkhart.... [...]
