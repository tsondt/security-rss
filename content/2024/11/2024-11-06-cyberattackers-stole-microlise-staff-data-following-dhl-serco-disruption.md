Title: Cyberattackers stole Microlise staff data following DHL, Serco disruption
Date: 2024-11-06T12:06:28+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-06-cyberattackers-stole-microlise-staff-data-following-dhl-serco-disruption

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/06/microlise_cyberattack/){:target="_blank" rel="noopener"}

> Experts say incident has 'all the hallmarks of ransomware' Telematics tech biz Microlise says an attack that hit its network likely did not expose customer data, although staff aren't so lucky.... [...]
