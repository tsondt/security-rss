Title: T-Mobile US 'monitoring' China's 'industry-wide attack' amid fresh security breach fears
Date: 2024-11-18T20:43:22+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-18-t-mobile-us-monitoring-chinas-industry-wide-attack-amid-fresh-security-breach-fears

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/18/tmobile_us_attack_salt_typhoon/){:target="_blank" rel="noopener"}

> Un-carrier said to be among those hit by Salt Typhoon, including AT&T, Verizon T-Mobile US said it is "monitoring" an "industry-wide" cyber-espionage campaign against American networks – amid fears Chinese government-backed spies compromised the un-carrier among with various other telecommunications providers.... [...]
