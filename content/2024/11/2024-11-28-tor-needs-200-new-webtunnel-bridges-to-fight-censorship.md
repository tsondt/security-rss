Title: Tor needs 200 new WebTunnel bridges to fight censorship
Date: 2024-11-28T11:50:22-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-11-28-tor-needs-200-new-webtunnel-bridges-to-fight-censorship

[Source](https://www.bleepingcomputer.com/news/security/tor-needs-200-new-webtunnel-bridges-to-fight-censorship/){:target="_blank" rel="noopener"}

> The Tor Project has put out an urgent call to the privacy community asking volunteers to help deploy 200 new WebTunnel bridges by the end of the year to fight government censorship. [...]
