Title: Cloud CISO Perspectives: To end ransomware scourge, start with more reporting — not blocking cyber-insurance
Date: 2024-11-25T17:00:00+00:00
Author: Monica Shokrai
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2024-11-25-cloud-ciso-perspectives-to-end-ransomware-scourge-start-with-more-reporting-not-blocking-cyber-insurance

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-ransomware-cyber-insurance-reporting/){:target="_blank" rel="noopener"}

> Welcome to the second Cloud CISO Perspectives for November 2024. Today, Monica Shokrai, head of business risk and insurance, Google Cloud, and Kimberly Goody, cybercrime analysis lead, Google Threat Intelligence Group, explore the role cyber-insurance can play in combating the scourge of ransomware. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI Security & CISO, Google Cloud aside_block <ListValue: [StructValue([('title', 'Get vital board insights with Google Cloud'), ('body', [...]
