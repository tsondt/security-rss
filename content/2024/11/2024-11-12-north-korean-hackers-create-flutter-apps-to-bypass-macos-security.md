Title: North Korean hackers create Flutter apps to bypass macOS security
Date: 2024-11-12T08:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple
Slug: 2024-11-12-north-korean-hackers-create-flutter-apps-to-bypass-macos-security

[Source](https://www.bleepingcomputer.com/news/security/north-korean-hackers-create-flutter-apps-to-bypass-macos-security/){:target="_blank" rel="noopener"}

> North Korean threat actors target Apple macOS systems using trojanized Notepad apps and minesweeper games created with Flutter, which are signed and notarized by legitimate Apple developer IDs. [...]
