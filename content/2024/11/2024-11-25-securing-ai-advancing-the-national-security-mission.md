Title: Securing AI: Advancing the national security mission
Date: 2024-11-25T21:00:00+00:00
Author: Ron Bushar
Category: GCP Security
Tags: Security & Identity;Public Sector
Slug: 2024-11-25-securing-ai-advancing-the-national-security-mission

[Source](https://cloud.google.com/blog/topics/public-sector/securing-ai-advancing-the-national-security-mission/){:target="_blank" rel="noopener"}

> Artificial intelligence is not just a technological advancement; it's a national security priority. In this new era, AI is both a powerful technology that can bolster any organization’s cybersecurity capabilities and also a critical part of the technology infrastructure that we need to defend and protect. Google recently commissioned IDC to conduct a study that surveyed 161 federal CAIOs, government AI leaders and other decision makers to understand how agency leaders are leading in this new AI era. The study found that internal cybersecurity protection is currently the top AI use case for federal agencies according to 60% of those [...]
