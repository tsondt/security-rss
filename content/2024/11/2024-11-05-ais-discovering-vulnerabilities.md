Title: AIs Discovering Vulnerabilities
Date: 2024-11-05T12:08:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;vulnerabilities;zero-day
Slug: 2024-11-05-ais-discovering-vulnerabilities

[Source](https://www.schneier.com/blog/archives/2024/11/ais-discovering-vulnerabilities.html){:target="_blank" rel="noopener"}

> I’ve been writing about the possibility of AIs automatically discovering code vulnerabilities since at least 2018. This is an ongoing area of research: AIs doing source code scanning, AIs finding zero-days in the wild, and everything in between. The AIs aren’t very good at it yet, but they’re getting better. Here’s some anecdotal data from this summer: Since July 2024, ZeroPath is taking a novel approach combining deep program analysis with adversarial AI agents for validation. Our methodology has uncovered numerous critical vulnerabilities in production systems, including several that traditional Static Application Security Testing (SAST) tools were ill-equipped to find. [...]
