Title: New Google Pixel AI feature analyzes phone conversations for scams
Date: 2024-11-13T13:05:51-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Google;Artificial Intelligence;Mobile;Security
Slug: 2024-11-13-new-google-pixel-ai-feature-analyzes-phone-conversations-for-scams

[Source](https://www.bleepingcomputer.com/news/google/new-google-pixel-ai-feature-analyzes-phone-conversations-for-scams/){:target="_blank" rel="noopener"}

> Google is adding a new AI-powered scam protection feature that monitors phone call conversations on Google Pixel devices to detect patterns that warn when the caller may be a scammer. [...]
