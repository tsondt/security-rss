Title: Put your usernames and passwords in your will, advises Japan's government
Date: 2024-11-21T06:14:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-11-21-put-your-usernames-and-passwords-in-your-will-advises-japans-government

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/21/japan_digital_end_of_life/){:target="_blank" rel="noopener"}

> Digital end of life planning saves your loved ones from a little extra anguish Japan's National Consumer Affairs Center on Wednesday suggested citizens start "digital end of life planning" and offered tips on how to do it.... [...]
