Title: Here's what we know about the suspected Snowflake data extortionists
Date: 2024-11-12T21:10:15+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-12-heres-what-we-know-about-the-suspected-snowflake-data-extortionists

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/12/snowflake_hackers_indictment/){:target="_blank" rel="noopener"}

> A Canadian and an American living in Turkey 'walk into' cloud storage environments... Two men allegedly compromised what's believed to be multiple organizations' Snowflake-hosted cloud environments, stole sensitive data within, and extorted at least $2.5 million from at least three victims.... [...]
