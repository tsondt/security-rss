Title: QNAP firmware update leaves NAS owners locked out of their boxes
Date: 2024-11-26T18:10:39+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;Tech;firmware updates;NAS;network attached storage;QNAP
Slug: 2024-11-26-qnap-firmware-update-leaves-nas-owners-locked-out-of-their-boxes

[Source](https://arstechnica.com/gadgets/2024/11/qnap-firmware-update-leaves-nas-owners-locked-out-of-their-boxes/){:target="_blank" rel="noopener"}

> A recent firmware pushed to QNAP network attached storage (NAS) devices left a number of owners unable to access their storage systems. The company has pulled back the firmware and issued a fixed version, but the company's response has left some users feeling less confident in the boxes into which they put all their digital stuff. As seen on a QNAP community thread, and as announced by QNAP itself, the QNAP operating system, QTS, received update 5.2.2.2950, build 20241114, at some point around November 19. After QNAP "received feedbacks from some users reporting issues with device functionality after installation," the [...]
