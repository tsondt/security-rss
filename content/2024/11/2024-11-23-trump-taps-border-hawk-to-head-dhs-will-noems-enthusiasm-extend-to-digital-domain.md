Title: Trump taps border hawk to head DHS. Will Noem's 'enthusiasm' extend to digital domain?
Date: 2024-11-23T17:39:15+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-23-trump-taps-border-hawk-to-head-dhs-will-noems-enthusiasm-extend-to-digital-domain

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/23/trump_noem_homeland_security/){:target="_blank" rel="noopener"}

> Meanwhile, CISA chief Jen Easterly will step down prior to inauguration Analysis President-elect Donald Trump has announced several unorthodox nominations for his cabinet over the last two weeks, including South Dakota Governor Kristi Noem, whom he tapped to serve as Homeland Security Secretary.... [...]
