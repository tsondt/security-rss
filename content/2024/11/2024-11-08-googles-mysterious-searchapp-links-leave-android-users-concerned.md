Title: Google's mysterious 'search.app' links leave Android users concerned
Date: 2024-11-08T07:56:50-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2024-11-08-googles-mysterious-searchapp-links-leave-android-users-concerned

[Source](https://www.bleepingcomputer.com/news/security/googles-mysterious-searchapp-links-leave-android-users-concerned/){:target="_blank" rel="noopener"}

> The most recent update to the Google Android app has startled users as they notice the mysterious "search.app" links being generated when sharing content and links from the Google app externally. [...]
