Title: Good Essay on the History of Bad Password Policies
Date: 2024-11-15T12:05:02+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;hashes;history of security;passwords
Slug: 2024-11-15-good-essay-on-the-history-of-bad-password-policies

[Source](https://www.schneier.com/blog/archives/2024/11/good-essay-on-the-history-of-bad-password-policies.html){:target="_blank" rel="noopener"}

> Stuart Schechter makes some good points on the history of bad password policies: Morris and Thompson’s work brought much-needed data to highlight a problem that lots of people suspected was bad, but that had not been studied scientifically. Their work was a big step forward, if not for two mistakes that would impede future progress in improving passwords for decades. First, was Morris and Thompson’s confidence that their solution, a password policy, would fix the underlying problem of weak passwords. They incorrectly assumed that if they prevented the specific categories of weakness that they had noted, that the result would [...]
