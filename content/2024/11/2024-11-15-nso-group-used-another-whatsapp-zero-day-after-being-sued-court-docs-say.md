Title: NSO Group used another WhatsApp zero-day after being sued, court docs say
Date: 2024-11-15T17:04:18-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-11-15-nso-group-used-another-whatsapp-zero-day-after-being-sued-court-docs-say

[Source](https://www.bleepingcomputer.com/news/security/nso-group-used-another-whatsapp-zero-day-after-being-sued-court-docs-say/){:target="_blank" rel="noopener"}

> Israeli surveillance firm NSO Group reportedly used multiple zero-day exploits, including an unknown one named "Erised," that leveraged WhatsApp vulnerabilities to deploy Pegasus spyware in zero-click attacks, even after getting sued. [...]
