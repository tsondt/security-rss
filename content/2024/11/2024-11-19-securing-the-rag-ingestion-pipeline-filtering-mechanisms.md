Title: Securing the RAG ingestion pipeline: Filtering mechanisms
Date: 2024-11-19T21:51:24+00:00
Author: Laura Verghote
Category: AWS Security
Tags: Advanced (300);Amazon Bedrock;Artificial Intelligence;Best Practices;Generative AI;Security, Identity, & Compliance;artificial intelligence;Security Blog
Slug: 2024-11-19-securing-the-rag-ingestion-pipeline-filtering-mechanisms

[Source](https://aws.amazon.com/blogs/security/securing-the-rag-ingestion-pipeline-filtering-mechanisms/){:target="_blank" rel="noopener"}

> Retrieval-Augmented Generative (RAG) applications enhance the responses retrieved from large language models (LLMs) by integrating external data such as downloaded files, web scrapings, and user-contributed data pools. This integration improves the models’ performance by adding relevant context to the prompt. While RAG applications are a powerful way to dynamically add additional context to an LLM’s prompt [...]
