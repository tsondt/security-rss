Title: GitHub projects targeted with malicious commits to frame researcher
Date: 2024-11-16T10:30:29-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2024-11-16-github-projects-targeted-with-malicious-commits-to-frame-researcher

[Source](https://www.bleepingcomputer.com/news/security/github-projects-targeted-with-malicious-commits-to-frame-researcher/){:target="_blank" rel="noopener"}

> GitHub projects have been targeted with malicious commits and pull requests, in an attempt to inject backdoors into these projects. Most recently, the GitHub repository of Exo Labs, an AI and machine learning startup, was targeted in the attack, which has left many wondering about the attacker's true intentions. [...]
