Title: NHS major 'cyber incident' forces hospitals to use pen and paper
Date: 2024-11-28T12:31:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-28-nhs-major-cyber-incident-forces-hospitals-to-use-pen-and-paper

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/28/wirral_nhs_cyber_incident/){:target="_blank" rel="noopener"}

> Systems are isolated and pulled offline, while scheduled procedures are canceled The ongoing cyber security incident affecting a North West England NHS group has forced sites to fall back on pen-and-paper operations.... [...]
