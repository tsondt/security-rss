Title: Washington courts' systems offline following weekend cyberattack
Date: 2024-11-06T12:28:10-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-11-06-washington-courts-systems-offline-following-weekend-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/washington-courts-systems-offline-following-weekend-cyberattack/){:target="_blank" rel="noopener"}

> ​​Court systems across Washington state have been down since Sunday when officials said "unauthorized activity" was detected on their networks. [...]
