Title: Friday Squid Blogging: Female Gonatus Onyx Squid Carrying Her Eggs
Date: 2024-11-15T22:07:02+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-11-15-friday-squid-blogging-female-gonatus-onyx-squid-carrying-her-eggs

[Source](https://www.schneier.com/blog/archives/2024/11/friday-squid-blogging-female-gonatus-onyx-squid-carrying-her-eggs.html){:target="_blank" rel="noopener"}

> Fantastic video of a female Gonatus onyx squid swimming while carrying her egg sack. An earlier related post. Blog moderation policy. [...]
