Title: Imagine a land in which Big Tech can't send you down online rabbit holes or use algorithms to overcharge you
Date: 2024-11-25T05:29:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-11-25-imagine-a-land-in-which-big-tech-cant-send-you-down-online-rabbit-holes-or-use-algorithms-to-overcharge-you

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/25/china_algorithm_transparency/){:target="_blank" rel="noopener"}

> China is trying to become that land, with a government crackdown on the things that make the internet no fun Internet echo chambers and nasty e-commerce tricks that analyze your behavior to milk you for more cash are set to be banned – in China.... [...]
