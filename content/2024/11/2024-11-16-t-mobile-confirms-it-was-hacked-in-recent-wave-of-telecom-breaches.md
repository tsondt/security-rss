Title: T-Mobile confirms it was hacked in recent wave of telecom breaches
Date: 2024-11-16T12:47:00-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-11-16-t-mobile-confirms-it-was-hacked-in-recent-wave-of-telecom-breaches

[Source](https://www.bleepingcomputer.com/news/security/t-mobile-confirms-it-was-hacked-in-recent-wave-of-telecom-breaches/){:target="_blank" rel="noopener"}

> T-Mobile confirms it was hacked in the wave of recently reported telecom breaches conducted by Chinese threat actors to gain access to private communications, call records, and law enforcement information requests. [...]
