Title: Rust haters, unite! Fil-C aims to Make C Great Again
Date: 2024-11-16T10:12:14+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-11-16-rust-haters-unite-fil-c-aims-to-make-c-great-again

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/16/rusthaters_unite_filc/){:target="_blank" rel="noopener"}

> It's memory-safe, with a few caveats Developers looking to continue working in the C and C++ programming languages amid the global push to promote memory-safe programming now have another option that doesn't involve learning Rust.... [...]
