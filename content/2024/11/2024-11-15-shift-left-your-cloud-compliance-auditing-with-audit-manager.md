Title: Shift-left your cloud compliance auditing with Audit Manager
Date: 2024-11-15T17:00:00+00:00
Author: Elise Bailey
Category: GCP Security
Tags: Security & Identity
Slug: 2024-11-15-shift-left-your-cloud-compliance-auditing-with-audit-manager

[Source](https://cloud.google.com/blog/products/identity-security/shift-left-your-cloud-compliance-auditing-with-audit-manager/){:target="_blank" rel="noopener"}

> Cloud compliance can present significant regulatory and technical challenges for organizations. These complexities often include delineating compliance responsibilities and accountabilities between the customer and cloud provider. At Google Cloud, we understand these challenges faced by our customers’ cloud engineering, compliance, and audit teams, and want to help make them easier to manage. That's why we’re pleased to announce that our Audit Manager service, which can digitize and help streamline the compliance auditing process, is now generally available. Understanding compliance across layers in Google Cloud. aside_block <ListValue: [StructValue([('title', '$300 in free credit to try Google Cloud security products'), ('body', <wagtail.rich_text.RichText object [...]
