Title: Wire cutters: how the world’s vital undersea data cables are being targeted
Date: 2024-11-22T15:58:17+00:00
Author: Dan Milmo Global technology editor
Category: The Guardian
Tags: Telecoms;Data and computer security;Espionage;Technology;World news
Slug: 2024-11-22-wire-cutters-how-the-worlds-vital-undersea-data-cables-are-being-targeted

[Source](https://www.theguardian.com/world/2024/nov/22/wire-cutters-how-the-worlds-vital-undersea-data-cables-are-being-targeted){:target="_blank" rel="noopener"}

> Carrying 99% of the world’s international telecommunications, the vulnerable lines are drawing nefarious interest The lead-clad telegraphic cable seemed to weigh tons, according to Lt Cameron Winslow of the US navy, and the weather wasn’t helping their attempts to lift it up from the seabed and sever it. “The rough water knocked the heavy boats together, breaking and almost crushing in their planking,” he wrote. Eventually, Winslow’s men managed to cut the cable with hacksaws and disrupt the enemy’s communications by slicing off a 46-metre (150ft) section. Continue reading... [...]
