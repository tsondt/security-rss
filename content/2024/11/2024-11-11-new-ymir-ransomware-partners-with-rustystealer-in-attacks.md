Title: New Ymir ransomware partners with RustyStealer in attacks
Date: 2024-11-11T17:46:14-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-11-11-new-ymir-ransomware-partners-with-rustystealer-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/new-ymir-ransomware-partners-with-rustystealer-in-attacks/){:target="_blank" rel="noopener"}

> A new ransomware family called 'Ymir' has been spotted in the wild, being introduced onto systems that were previously compromised by the RustyStealer info-stealer malware. [...]
