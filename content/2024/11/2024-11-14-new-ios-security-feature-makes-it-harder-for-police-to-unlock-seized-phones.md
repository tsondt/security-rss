Title: New iOS Security Feature Makes It Harder for Police to Unlock Seized Phones
Date: 2024-11-14T12:05:26+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;cybersecurity;iPhone
Slug: 2024-11-14-new-ios-security-feature-makes-it-harder-for-police-to-unlock-seized-phones

[Source](https://www.schneier.com/blog/archives/2024/11/new-ios-security-feature-makes-it-harder-for-police-to-unlock-seized-phones.html){:target="_blank" rel="noopener"}

> Everybody is reporting about a new security iPhone security feature with iOS 18: if the phone hasn’t been used for a few days, it automatically goes into its “Before First Unlock” state and has to be rebooted. This is a really good security feature. But various police departments don’t like it, because it makes it harder for them to unlock suspects’ phones. [...]
