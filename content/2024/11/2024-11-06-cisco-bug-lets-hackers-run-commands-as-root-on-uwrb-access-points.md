Title: Cisco bug lets hackers run commands as root on UWRB access points
Date: 2024-11-06T14:34:13-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-11-06-cisco-bug-lets-hackers-run-commands-as-root-on-uwrb-access-points

[Source](https://www.bleepingcomputer.com/news/security/cisco-bug-lets-hackers-run-commands-as-root-on-uwrb-access-points/){:target="_blank" rel="noopener"}

> Cisco has fixed a maximum severity vulnerability that allows attackers to run commands with root privileges on vulnerable Ultra-Reliable Wireless Backhaul (URWB) access points that provide connectivity for industrial wireless automation. [...]
