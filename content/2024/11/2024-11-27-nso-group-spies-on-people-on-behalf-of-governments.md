Title: NSO Group Spies on People on Behalf of Governments
Date: 2024-11-27T12:05:16+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberespionage;espionage;hacking;Israel;spyware
Slug: 2024-11-27-nso-group-spies-on-people-on-behalf-of-governments

[Source](https://www.schneier.com/blog/archives/2024/11/nso-group-spies-on-people-on-behalf-of-governments.html){:target="_blank" rel="noopener"}

> The Israeli company NSO Group sells Pegasus spyware to countries around the world (including countries like Saudi Arabia, UAE, India, Mexico, Morocco and Rwanda). We assumed that those countries use the spyware themselves. Now we’ve learned that that’s not true: that NSO Group employees operate the spyware on behalf of their customers. Legal documents released in ongoing US litigation between NSO Group and WhatsApp have revealed for the first time that the Israeli cyberweapons maker ­ and not its government customers ­ is the party that “installs and extracts” information from mobile phones targeted by the company’s hacking software. [...]
