Title: Threat modeling your generative AI workload to evaluate security risk
Date: 2024-11-18T14:00:32+00:00
Author: Danny Cortegaca
Category: AWS Security
Tags: Generative AI;Intermediate (200);Security, Identity, & Compliance;Application security;Security;Security Blog
Slug: 2024-11-18-threat-modeling-your-generative-ai-workload-to-evaluate-security-risk

[Source](https://aws.amazon.com/blogs/security/threat-modeling-your-generative-ai-workload-to-evaluate-security-risk/){:target="_blank" rel="noopener"}

> As generative AI models become increasingly integrated into business applications, it’s crucial to evaluate the potential security risks they introduce. At AWS re:Invent 2023, we presented on this topic, helping hundreds of customers maintain high-velocity decision-making for adopting new technologies securely. Customers who attended this session were able to better understand our recommended approach for [...]
