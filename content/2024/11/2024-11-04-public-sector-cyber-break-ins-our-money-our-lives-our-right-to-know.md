Title: Public sector cyber break-ins: Our money, our lives, our right to know
Date: 2024-11-04T10:27:08+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2024-11-04-public-sector-cyber-break-ins-our-money-our-lives-our-right-to-know

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/04/public_sector_breakins_opinion/){:target="_blank" rel="noopener"}

> Is that a walrus in your server logs, or aren't you pleased to see me? Opinion At the start of September, Transport for London was hit by a major cyber attack. TfL is the public body that moves many of London's human bodies to and from work and play in the capital, and as the attack didn't hit power, signaling, or communications systems, most of the effects went unnoticed by commuters. The organization downplayed the damage done to back office ticketing, billing, and other systems. Everything was in hand.... [...]
