Title: ChatGPT allows access to underlying sandbox OS, “playbook” data
Date: 2024-11-14T11:08:43-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Artificial Intelligence;Security
Slug: 2024-11-14-chatgpt-allows-access-to-underlying-sandbox-os-playbook-data

[Source](https://www.bleepingcomputer.com/news/artificial-intelligence/chatgpt-allows-access-to-underlying-sandbox-os-playbook-data/){:target="_blank" rel="noopener"}

> OpenAI's containerized ChatGPT environment is open to limited yet extensive access to core instructions while allowing arbitrary file uploads and command execution within the isolated sandbox. [...]
