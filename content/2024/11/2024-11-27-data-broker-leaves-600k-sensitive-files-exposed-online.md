Title: Data broker leaves 600K+ sensitive files exposed online
Date: 2024-11-27T18:00:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-27-data-broker-leaves-600k-sensitive-files-exposed-online

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/27/600k_sensitive_files_exposed/){:target="_blank" rel="noopener"}

> Researcher spotted open database before criminals... we hope Exclusive More than 600,000 sensitive files containing thousands of people's criminal histories, background checks, vehicle and property records were exposed to the internet in a non-password protected database belonging to data brokerage SL Data Services, according to a security researcher.... [...]
