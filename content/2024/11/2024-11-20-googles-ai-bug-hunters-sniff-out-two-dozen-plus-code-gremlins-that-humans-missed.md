Title: Google's AI bug hunters sniff out two dozen-plus code gremlins that humans missed
Date: 2024-11-20T17:01:27+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-11-20-googles-ai-bug-hunters-sniff-out-two-dozen-plus-code-gremlins-that-humans-missed

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/20/google_ossfuzz/){:target="_blank" rel="noopener"}

> OSS-Fuzz is making a strong argument for LLMs in security research Google's OSS-Fuzz project, which uses large language models (LLMs) to help find bugs in code repositories, has now helped identify 26 vulnerabilities, including a critical flaw in the widely used OpenSSL library.... [...]
