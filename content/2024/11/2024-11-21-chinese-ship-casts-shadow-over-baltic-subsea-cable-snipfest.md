Title: Chinese ship casts shadow over Baltic subsea cable snipfest
Date: 2024-11-21T17:20:09+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2024-11-21-chinese-ship-casts-shadow-over-baltic-subsea-cable-snipfest

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/21/chinese_ship_baltic_cable/){:target="_blank" rel="noopener"}

> Danish military confirms it is monitoring as Swedish police investigate. Cloudflare says impact was 'minimal' The Danish military has confirmed it is tracking a Chinese ship that is under investigation after two optical fiber internet cables under the Baltic Sea were damaged.... [...]
