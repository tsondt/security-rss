Title: AI Industry is Trying to Subvert the Definition of “Open Source AI”
Date: 2024-11-08T12:03:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;machine learning;open source;privacy
Slug: 2024-11-08-ai-industry-is-trying-to-subvert-the-definition-of-open-source-ai

[Source](https://www.schneier.com/blog/archives/2024/11/ai-industry-is-trying-to-subvert-the-definition-of-open-source-ai.html){:target="_blank" rel="noopener"}

> The Open Source Initiative has published (news article here ) its definition of “open source AI,” and it’s terrible. It allows for secret training data and mechanisms. It allows for development to be done in secret. Since for a neural network, the training data is the source code—it’s how the model gets programmed—the definition makes no sense. And it’s confusing; most “open source” AI models—like LLAMA—are open source in name only. But the OSI seems to have been co-opted by industry players that want both corporate secrecy and the “open source” label. (Here’s one rebuttal to the definition.) This is [...]
