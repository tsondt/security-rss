Title: Admins can give thanks this November for dollops of Microsoft patches
Date: 2024-11-13T01:29:13+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-11-13-admins-can-give-thanks-this-november-for-dollops-of-microsoft-patches

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/13/november_patch_tuesday/){:target="_blank" rel="noopener"}

> Don't be a turkey – get these fixed Patch Tuesday Patch Tuesday has swung around again, and Microsoft has released fixes for 89 CVE-listed security flaws in its products – including two under active attack – and reissued three more.... [...]
