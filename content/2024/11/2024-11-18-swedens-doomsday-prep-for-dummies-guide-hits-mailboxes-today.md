Title: Sweden's 'Doomsday Prep for Dummies' guide hits mailboxes today
Date: 2024-11-18T16:03:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-18-swedens-doomsday-prep-for-dummies-guide-hits-mailboxes-today

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/18/sweden_updates_war_guide/){:target="_blank" rel="noopener"}

> First in six years is nearly three times the size of the older, pre-NATO version Residents of Sweden are to receive a handy new guide this week that details how to prepare for various types of crisis situations or wartime should geopolitical events threaten the country.... [...]
