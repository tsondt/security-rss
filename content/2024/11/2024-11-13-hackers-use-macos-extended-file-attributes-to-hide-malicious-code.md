Title: Hackers use macOS extended file attributes to hide malicious code
Date: 2024-11-13T17:53:24-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-11-13-hackers-use-macos-extended-file-attributes-to-hide-malicious-code

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-macos-extended-file-attributes-to-hide-malicious-code/){:target="_blank" rel="noopener"}

> Hackers are using a novel technique that abuses extended attributes for macOS files to deliver a new trojan that researchers call RustyAttr. [...]
