Title: Solving the painful password problem with better policies
Date: 2024-11-04T10:01:11-05:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2024-11-04-solving-the-painful-password-problem-with-better-policies

[Source](https://www.bleepingcomputer.com/news/security/solving-the-painful-password-problem-with-better-policies/){:target="_blank" rel="noopener"}

> Weak and reused credentials continue to plague users and organizations. Learn from Specops software about why passwords are so easy to hack and how organizations can fortify their security efforts. [...]
