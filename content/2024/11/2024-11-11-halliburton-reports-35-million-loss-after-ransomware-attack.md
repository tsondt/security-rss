Title: Halliburton reports $35 million loss after ransomware attack
Date: 2024-11-11T10:21:59-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-11-11-halliburton-reports-35-million-loss-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/halliburton-reports-35-million-loss-after-ransomware-attack/){:target="_blank" rel="noopener"}

> Halliburton has revealed that an August ransomware attack has led to $35 million in losses after the breach caused the company to shut down IT systems and disconnect customers. [...]
