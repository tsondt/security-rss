Title: Nokia says hackers leaked third-party app source code
Date: 2024-11-07T13:24:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-11-07-nokia-says-hackers-leaked-third-party-app-source-code

[Source](https://www.bleepingcomputer.com/news/security/nokia-says-hackers-leaked-third-party-app-source-code/){:target="_blank" rel="noopener"}

> Nokia's investigation of recent claims of a data breach found that the source code leaked on a hacker forum belongs to a third party and company and customer data has not been impacted. [...]
