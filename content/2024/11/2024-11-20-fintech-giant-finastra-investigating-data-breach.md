Title: Fintech Giant Finastra Investigating Data Breach
Date: 2024-11-20T01:12:15+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;Latest Warnings;Ne'er-Do-Well News;The Coming Storm;abyss0;BreachForums;Finastra;ke-la.com
Slug: 2024-11-20-fintech-giant-finastra-investigating-data-breach

[Source](https://krebsonsecurity.com/2024/11/fintech-giant-finastra-investigating-data-breach/){:target="_blank" rel="noopener"}

> The financial technology firm Finastra is investigating the alleged large-scale theft of information from its internal file transfer platform, KrebsOnSecurity has learned. Finastra, which provides software and services to 45 of the world’s top 50 banks, notified customers of the security incident after a cybercriminal began selling more than 400 gigabytes of data purportedly stolen from the company. London-based Finastra has offices in 42 countries and reported $1.9 billion in revenues last year. The company employs more than 7,000 people and serves approximately 8,100 financial institutions around the world. A major part of Finastra’s day-to-day business involves processing huge volumes [...]
