Title: Ransom gang claims attack on NHS Alder Hey Children's Hospital
Date: 2024-11-29T12:24:46+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-29-ransom-gang-claims-attack-on-nhs-alder-hey-childrens-hospital

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/29/inc_ransom_alder_hey_childrens_hospital/){:target="_blank" rel="noopener"}

> Second alleged intrusion on English NHS org systems this week Yet another of the UK's National Health Service (NHS) systems appears to be under attack, with a ransomware gang threatening to leak stolen data it says is from one of England's top children's hospitals.... [...]
