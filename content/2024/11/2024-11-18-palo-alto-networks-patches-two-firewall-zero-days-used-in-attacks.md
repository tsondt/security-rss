Title: Palo Alto Networks patches two firewall zero-days used in attacks
Date: 2024-11-18T15:50:32-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-11-18-palo-alto-networks-patches-two-firewall-zero-days-used-in-attacks

[Source](https://www.bleepingcomputer.com/news/security/palo-alto-networks-patches-two-firewall-zero-days-used-in-attacks/){:target="_blank" rel="noopener"}

> Palo Alto Networks has finally released security updates for an actively exploited zero-day vulnerability in its Next-Generation Firewalls (NGFW). [...]
