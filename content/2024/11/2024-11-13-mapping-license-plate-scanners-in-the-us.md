Title: Mapping License Plate Scanners in the US
Date: 2024-11-13T12:06:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;privacy;scanners;surveillance
Slug: 2024-11-13-mapping-license-plate-scanners-in-the-us

[Source](https://www.schneier.com/blog/archives/2024/11/mapping-license-plate-scanners-in-the-us.html){:target="_blank" rel="noopener"}

> DeFlock is a crowd-sourced project to map license plate scanners. It only records the fixed scanners, of course. The mobile scanners on cars are not mapped. The post Mapping License Plate Scanners in the US appeared first on Schneier on Security. [...]
