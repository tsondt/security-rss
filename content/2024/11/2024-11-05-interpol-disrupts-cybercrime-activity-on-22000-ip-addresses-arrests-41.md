Title: Interpol disrupts cybercrime activity on 22,000 IP addresses, arrests 41
Date: 2024-11-05T13:55:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-11-05-interpol-disrupts-cybercrime-activity-on-22000-ip-addresses-arrests-41

[Source](https://www.bleepingcomputer.com/news/security/interpol-disrupts-cybercrime-activity-on-22-000-ip-addresses-arrests-41/){:target="_blank" rel="noopener"}

> Interpol announced it arrested 41 individuals and taken down 1,037 servers and infrastructure running on 22,000 IP addresses facilitating cybercrime in an international law enforcement action titled Operation Synergia II. [...]
