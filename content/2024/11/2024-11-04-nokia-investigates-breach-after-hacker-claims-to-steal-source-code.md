Title: Nokia investigates breach after hacker claims to steal source code
Date: 2024-11-04T18:47:32-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-11-04-nokia-investigates-breach-after-hacker-claims-to-steal-source-code

[Source](https://www.bleepingcomputer.com/news/security/nokia-investigates-breach-after-hacker-claims-to-steal-source-code/){:target="_blank" rel="noopener"}

> Nokia is investigating whether a third-party vendor was breached after a hacker claimed to be selling the company's stolen source code. [...]
