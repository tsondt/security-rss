Title: Alleged Snowflake attacker gets busted by Canadians – politely, we assume
Date: 2024-11-11T03:28:13+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-11-11-alleged-snowflake-attacker-gets-busted-by-canadians-politely-we-assume

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/11/infosec_in_brief/){:target="_blank" rel="noopener"}

> Also: Crypto hacks will continue; CoD hacker gets thousands banned, and more in brief One of the suspected masterminds behind the widespread Snowflake breach has been arrested in Canada – but the saga isn't over, eh.... [...]
