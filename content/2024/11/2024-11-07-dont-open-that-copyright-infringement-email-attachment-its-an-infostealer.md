Title: Don't open that 'copyright infringement' email attachment – it's an infostealer
Date: 2024-11-07T22:18:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-07-dont-open-that-copyright-infringement-email-attachment-its-an-infostealer

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/07/fake_copyright_email_malware/){:target="_blank" rel="noopener"}

> Curiosity gives crims access to wallets and passwords Organizations should be on the lookout for bogus copyright infringement emails as they might be the latest ploy by cybercriminals to steal their data.... [...]
