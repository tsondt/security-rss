Title: Microsoft Patch Tuesday, November 2024 Edition
Date: 2024-11-12T21:59:46+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Security Tools;Time to Patch;CVE-2024-43451;CVE-2024-43602;CVE-2024-49019;CVE-2024-49039;CVE-2024-49040;Google TAG;Microsoft Patch Tuesday November 2024;Satnam Narang;Tenable
Slug: 2024-11-12-microsoft-patch-tuesday-november-2024-edition

[Source](https://krebsonsecurity.com/2024/11/microsoft-patch-tuesday-november-2024-edition/){:target="_blank" rel="noopener"}

> Microsoft today released updates to plug at least 89 security holes in its Windows operating systems and other software. November’s patch batch includes fixes for two zero-day vulnerabilities that are already being exploited by attackers, as well as two other flaws that were publicly disclosed prior to today. The zero-day flaw tracked as CVE-2024-49039 is a bug in the Windows Task Scheduler that allows an attacker to increase their privileges on a Windows machine. Microsoft credits Google’s Threat Analysis Group with reporting the flaw. The second bug fixed this month that is already seeing in-the-wild exploitation is CVE-2024-43451, a spoofing [...]
