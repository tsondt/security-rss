Title: Feds Charge Five Men in ‘Scattered Spider’ Roundup
Date: 2024-11-21T20:13:08+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;SIM Swapping;Ahmed Hossam Eldin Elbadawy;Evans Onyeaka Osiebo;Joel Martin Evans;Joeleoli;Kingbob;lastpass;Mailchimp;Namecheap;Noah Michael Urban;ogusers;Okta;Oktapus;Scattered Spider;Sosa;T-Mobile;Twilio;Tylerb
Slug: 2024-11-21-feds-charge-five-men-in-scattered-spider-roundup

[Source](https://krebsonsecurity.com/2024/11/feds-charge-five-men-in-scattered-spider-roundup/){:target="_blank" rel="noopener"}

> Federal prosecutors in Los Angeles this week unsealed criminal charges against five men alleged to be members of a hacking group responsible for dozens of cyber intrusions at major U.S. technology companies between 2021 and 2023, including LastPass, MailChimp, Okta, T-Mobile and Twilio. A visual depiction of the attacks by the SMS phishing group known as Scattered Spider, and Oktapus. Image: Amitai Cohen twitter.com/amitaico. The five men, aged 20 to 25, are allegedly members of a hacking conspiracy dubbed “ Scattered Spider ” and “ Oktapus,” which specialized in SMS-based phishing attacks that tricked employees at tech firms into entering [...]
