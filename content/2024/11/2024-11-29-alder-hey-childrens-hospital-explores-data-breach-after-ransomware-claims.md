Title: Alder Hey children’s hospital explores ‘data breach’ after ransomware claims
Date: 2024-11-29T16:36:50+00:00
Author: Dan Milmo and Andrew Gregory
Category: The Guardian
Tags: Data and computer security;Hospitals;Liverpool;NHS;UK news;Technology;Hacking
Slug: 2024-11-29-alder-hey-childrens-hospital-explores-data-breach-after-ransomware-claims

[Source](https://www.theguardian.com/technology/2024/nov/29/alder-hey-childrens-hospital-explores-data-breach-after-ransomware-claims){:target="_blank" rel="noopener"}

> Screenshots purporting to be from systems of Liverpool NHS health facility have been posted on dark web A ransomware gang claims to have stolen data from the Alder Hey children’s hospital in Liverpool, allegedly including patient records. The INC Ransom group said it had published screenshots of data on the dark web that contained the personal information of patients, donations from benefactors and procurement information. Continue reading... [...]
