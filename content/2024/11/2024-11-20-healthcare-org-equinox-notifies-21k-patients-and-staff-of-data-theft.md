Title: Healthcare org Equinox notifies 21K patients and staff of data theft
Date: 2024-11-20T00:30:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-20-healthcare-org-equinox-notifies-21k-patients-and-staff-of-data-theft

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/20/equinox_patients_employees_data/){:target="_blank" rel="noopener"}

> Ransomware scum LockBit claims it did the dirty deed Equinox, a New York State health and human services organization, has begun notifying over 21 thousand clients and staff that cyber criminals stole their health, financial, and personal information in a "data security incident" nearly seven months ago.... [...]
