Title: Brave on iOS adds new "Shred" button to wipe site-specific data
Date: 2024-11-18T17:40:09-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile;Software
Slug: 2024-11-18-brave-on-ios-adds-new-shred-button-to-wipe-site-specific-data

[Source](https://www.bleepingcomputer.com/news/security/brave-on-ios-adds-new-shred-button-to-wipe-site-specific-data/){:target="_blank" rel="noopener"}

> Brave Browser 1.71 for iOS introduces a new privacy-focused feature called "Shred," which allows users to easily delete site-specific mobile browsing data. [...]
