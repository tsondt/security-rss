Title: The workplace has become a surveillance state
Date: 2024-11-27T08:31:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-11-27-the-workplace-has-become-a-surveillance-state

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/27/workplace_surveillance/){:target="_blank" rel="noopener"}

> Cracked Labs report explores the use of motion sensors and wireless networking kit to monitor offices Office buildings have become like web browsers – they're full of tracking technology, a trend documented in a report out this week by Cracked Labs.... [...]
