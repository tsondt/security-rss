Title: China has utterly pwned 'thousands and thousands' of devices at US telcos
Date: 2024-11-25T13:15:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-11-25-china-has-utterly-pwned-thousands-and-thousands-of-devices-at-us-telcos

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/25/salt_typhoon_mark_warner_warning/){:target="_blank" rel="noopener"}

> Senate Intelligence Committee chair says his 'hair is on fire' as execs front the White House The Biden administration on Friday hosted telco execs to chat about China's recent attacks on the sector, amid revelations that US networks may need mass rebuilds to recover.... [...]
