Title: Volunteer DEF CON hackers dive into America's leaky water infrastructure
Date: 2024-11-24T15:27:12+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-11-24-volunteer-def-con-hackers-dive-into-americas-leaky-water-infrastructure

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/24/water_defcon_hacker/){:target="_blank" rel="noopener"}

> Six sites targeted for security clean-up, just 49,994 to go A plan for hackers to help secure America's critical infrastructure has kicked off with six US water companies signing up to let coders kick the tires of their computer systems and fix any vulnerabilities.... [...]
