Title: New Rockstar 2FA phishing service targets Microsoft 365 accounts
Date: 2024-11-29T14:01:50-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-11-29-new-rockstar-2fa-phishing-service-targets-microsoft-365-accounts

[Source](https://www.bleepingcomputer.com/news/security/new-rockstar-2fa-phishing-service-targets-microsoft-365-accounts/){:target="_blank" rel="noopener"}

> A new phishing-as-a-service (PhaaS) platform named 'Rockstar 2FA' has emerged, facilitating large-scale adversary-in-the-middle (AiTM) attacks to steal Microsoft 365 credentials. [...]
