Title: What Graykey Can and Can’t Unlock
Date: 2024-11-26T12:01:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cell phones;forensics;hacking;iPhone;law enforcement
Slug: 2024-11-26-what-graykey-can-and-cant-unlock

[Source](https://www.schneier.com/blog/archives/2024/11/what-graykey-can-and-cant-unlock.html){:target="_blank" rel="noopener"}

> This is from 404 Media : The Graykey, a phone unlocking and forensics tool that is used by law enforcement around the world, is only able to retrieve partial data from all modern iPhones that run iOS 18 or iOS 18.0.1, which are two recently released versions of Apple’s mobile operating system, according to documents describing the tool’s capabilities in granular detail obtained by 404 Media. The documents do not appear to contain information about what Graykey can access from the public release of iOS 18.1, which was released on October 28. More information : Meanwhile, Graykey’s performance with Android [...]
