Title: Canada orders TikTok to shut down over national risk concerns
Date: 2024-11-07T11:23:34-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-11-07-canada-orders-tiktok-to-shut-down-over-national-risk-concerns

[Source](https://www.bleepingcomputer.com/news/security/canada-orders-tiktok-to-shut-down-over-national-risk-concerns/){:target="_blank" rel="noopener"}

> The Canadian government has ordered the dissolution of TikTok Technology Canada following a multi-step review that provided information and evidence of the social media company posing a national risk. [...]
