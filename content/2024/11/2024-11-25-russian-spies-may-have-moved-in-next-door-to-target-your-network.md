Title: Russian spies may have moved in next door to target your network
Date: 2024-11-25T01:30:15+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-11-25-russian-spies-may-have-moved-in-next-door-to-target-your-network

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/25/infosec_news_in_brief/){:target="_blank" rel="noopener"}

> Plus: Microsoft seizes phishing domains; Helldown finds new targets; Illegal streaming with Jupyter, and more Infosec in brief Not to make you paranoid, but that business across the street could, under certain conditions, serve as a launching point for Russian cyber spies to compromise your network.... [...]
