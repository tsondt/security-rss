Title: Sophos Versus the Chinese Hackers
Date: 2024-11-04T12:02:25+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;China;hacking
Slug: 2024-11-04-sophos-versus-the-chinese-hackers

[Source](https://www.schneier.com/blog/archives/2024/11/sophos-versus-the-chinese-hackers.html){:target="_blank" rel="noopener"}

> Really interesting story of Sophos’s five-year war against Chinese hackers. [...]
