Title: Security plugin flaw in millions of WordPress sites gives admin access
Date: 2024-11-17T10:19:29-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-11-17-security-plugin-flaw-in-millions-of-wordpress-sites-gives-admin-access

[Source](https://www.bleepingcomputer.com/news/security/security-plugin-flaw-in-millions-of-wordpress-sites-gives-admin-access/){:target="_blank" rel="noopener"}

> A critical authentication bypass vulnerability has been discovered impacting the WordPress plugin 'Really Simple Security' (formerly 'Really Simple SSL'), including both free and Pro versions. [...]
