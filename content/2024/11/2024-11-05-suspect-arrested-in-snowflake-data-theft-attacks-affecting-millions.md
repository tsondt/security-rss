Title: Suspect arrested in Snowflake data-theft attacks affecting millions
Date: 2024-11-05T22:03:42+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;Data breaches;infostealers
Slug: 2024-11-05-suspect-arrested-in-snowflake-data-theft-attacks-affecting-millions

[Source](https://arstechnica.com/security/2024/11/suspect-arrested-in-snowflake-data-theft-attacks-affecting-millions/){:target="_blank" rel="noopener"}

> Canadian authorities have arrested a man on suspicion he breached hundreds of accounts belonging to users of cloud storage provider Snowflake and used that access to steal personal data belonging to millions of people, authorities said Tuesday. “Following a request by the United States, Alexander Moucka (aka Connor Moucka) was arrested on a provisional arrest warrant on Wednesday, October 30, 2024,” an official with the Canada Department of Justice wrote in an email Tuesday. “He appeared in court later that afternoon, and his case was adjourned to Tuesday, November 5, 2024. As extradition requests are considered confidential state-to-state communications, we [...]
