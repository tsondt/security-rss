Title: Scumbag puts 'stolen' Nokia source code, SSH and RSA keys, more up for sale
Date: 2024-11-06T00:01:45+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-11-06-scumbag-puts-stolen-nokia-source-code-ssh-and-rsa-keys-more-up-for-sale

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/06/nokia_data_theft/){:target="_blank" rel="noopener"}

> Data pinched from pwned outside supplier, thief claims IntelBroker, a notorious peddler of stolen data, claims to have pilfered source code, private keys, and other sensitive materials belonging to Nokia.... [...]
