Title: ChatGPT-4o can be used for autonomous voice-based scams
Date: 2024-11-03T10:12:22-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Artificial Intelligence
Slug: 2024-11-03-chatgpt-4o-can-be-used-for-autonomous-voice-based-scams

[Source](https://www.bleepingcomputer.com/news/security/chatgpt-4o-can-be-used-for-autonomous-voice-based-scams/){:target="_blank" rel="noopener"}

> Researchers have shown that it's possible to abuse OpenAI's real-time voice API for ChatGPT-4o, an advanced LLM chatbot, to conduct financial scams with low to moderate success rates. [...]
