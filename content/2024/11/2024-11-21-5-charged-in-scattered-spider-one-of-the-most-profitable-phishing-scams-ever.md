Title: 5 charged in “Scattered Spider,” one of the most profitable phishing scams ever
Date: 2024-11-21T13:00:21+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;courts;department of justice;phishing
Slug: 2024-11-21-5-charged-in-scattered-spider-one-of-the-most-profitable-phishing-scams-ever

[Source](https://arstechnica.com/information-technology/2024/11/prosecutors-charge-5-in-phishing-scams-that-stole-millions-of-dollars/){:target="_blank" rel="noopener"}

> Federal prosecutors have charged five men with running an extensive phishing scheme that allegedly allowed them to compromise hundreds of companies nationwide, gain non-public information, and steal millions of dollars in cryptocurrency. The charges, detailed in court documents unsealed Wednesday, pertain to a crime group security researchers have dubbed Scattered Spider. Members were behind a massive breach on MGM last year that cost the casino and resort company $100 million. MGM preemptively shut down large parts of its internal networks after discovering the breach, causing slot machines and keycards for thousands of hotel rooms to stop working and slowing electronic [...]
