Title: Russian suspected Phobos ransomware admin extradited to US over $16M extortion
Date: 2024-11-19T21:55:07+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-11-19-russian-suspected-phobos-ransomware-admin-extradited-to-us-over-16m-extortion

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/19/suspected_phobos_admin/){:target="_blank" rel="noopener"}

> This malware is FREE for EVERY crook ($300 decryption keys sold separately) A Russian citizen has been extradited from South Korea to the United States to face charges related to his alleged role in the Phobos ransomware operation.... [...]
