Title: Microsoft November 2024 Patch Tuesday fixes 4 zero-days, 89 flaws
Date: 2024-11-12T14:00:36-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-11-12-microsoft-november-2024-patch-tuesday-fixes-4-zero-days-89-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-november-2024-patch-tuesday-fixes-4-zero-days-89-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's November 2024 Patch Tuesday, which includes security updates for 89 flaws, including four zero-days, two of which are actively exploited. [...]
