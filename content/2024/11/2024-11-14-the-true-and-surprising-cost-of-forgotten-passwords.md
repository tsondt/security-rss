Title: The true (and surprising) cost of forgotten passwords
Date: 2024-11-14T10:01:11-05:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2024-11-14-the-true-and-surprising-cost-of-forgotten-passwords

[Source](https://www.bleepingcomputer.com/news/security/the-true-and-surprising-cost-of-forgotten-passwords/){:target="_blank" rel="noopener"}

> Password resets are more expensive for your organization than you may realize. Learn more from Specops Software on why password resets are so expensive and how a self-service password reset solution can save you money. [...]
