Title: Cisco says DevHub site leak won’t enable future breaches
Date: 2024-11-04T04:14:37-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-11-04-cisco-says-devhub-site-leak-wont-enable-future-breaches

[Source](https://www.bleepingcomputer.com/news/security/cisco-says-devhub-site-leak-wont-enable-future-breaches/){:target="_blank" rel="noopener"}

> ​Cisco says that non-public files recently downloaded by a threat actor from a misconfigured public-facing DevHub portal don't contain information that could be exploited in future breaches of the company's systems. [...]
