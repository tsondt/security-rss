Title: Hacker gets 10 years in prison for extorting US healthcare provider
Date: 2024-11-14T11:58:44-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;Healthcare;Security
Slug: 2024-11-14-hacker-gets-10-years-in-prison-for-extorting-us-healthcare-provider

[Source](https://www.bleepingcomputer.com/news/legal/hacker-gets-10-years-in-prison-for-extorting-us-healthcare-provider/){:target="_blank" rel="noopener"}

> Robert Purbeck, a 45-year-old man from Idaho, has been sentenced to ten years in prison for hacking at least 19 organizations in the United States, stealing the personal data of more than 132,000 people, and multiple extortion attempts. [...]
