Title: Bloke behind Helix Bitcoin launderette jailed for three years, hands over $400M
Date: 2024-11-16T00:58:06+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-11-16-bloke-behind-helix-bitcoin-launderette-jailed-for-three-years-hands-over-400m

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/16/helix_bitcoin_mixer/){:target="_blank" rel="noopener"}

> Digital money laundering pays, until it doesn't An Ohio man, who operated the Grams dark-web search engine and the Helix cryptocurrency money-laundering service associated with it, has been sentenced to three years in prison.... [...]
