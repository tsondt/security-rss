Title: Why the long name? Okta discloses auth bypass bug affecting 52-character usernames
Date: 2024-11-04T11:28:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-04-why-the-long-name-okta-discloses-auth-bypass-bug-affecting-52-character-usernames

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/04/why_the_long_name_okta/){:target="_blank" rel="noopener"}

> Mondays are for checking months of logs, apparently, if MFA's not enabled In potentially bad news for those with long names and/or employers with verbose domain names, Okta spotted a security hole that could have allowed crims to pass Okta AD/LDAP Delegated Authentication (DelAuth) using only a username.... [...]
