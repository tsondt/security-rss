Title: Winos4.0 abuses gaming apps to infect, control Windows machines
Date: 2024-11-08T02:30:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-08-winos40-abuses-gaming-apps-to-infect-control-windows-machines

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/08/winos40_targets_windows/){:target="_blank" rel="noopener"}

> 'Multiple' malware samples likely targeting education orgs Criminals are using game-related applications to infect Windows systems with a malicious software framework called Winos4.0 that gives the attackers full control over compromised machines.... [...]
