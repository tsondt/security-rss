Title: Phishing emails increasingly use SVG attachments to evade detection
Date: 2024-11-17T11:25:36-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-11-17-phishing-emails-increasingly-use-svg-attachments-to-evade-detection

[Source](https://www.bleepingcomputer.com/news/security/phishing-emails-increasingly-use-svg-attachments-to-evade-detection/){:target="_blank" rel="noopener"}

> Threat actors increasingly use Scalable Vector Graphics (SVG) attachments to display phishing forms or deploy malware while evading detection. [...]
