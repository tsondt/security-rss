Title: RansomHub claims to net data hat-trick against Bologna FC
Date: 2024-11-30T09:31:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-30-ransomhub-claims-to-net-data-hat-trick-against-bologna-fc

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/30/bologna_fc_ransomhub/){:target="_blank" rel="noopener"}

> Crooks say they have stolen sensitive files on managers and players Italian professional football club Bologna FC is allegedly a recent victim of the RansomHub cybercrime gang, according to the group's dark web postings.... [...]
