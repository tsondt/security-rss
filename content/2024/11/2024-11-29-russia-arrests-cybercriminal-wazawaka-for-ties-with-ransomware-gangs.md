Title: Russia arrests cybercriminal Wazawaka for ties with ransomware gangs
Date: 2024-11-29T12:50:55-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-11-29-russia-arrests-cybercriminal-wazawaka-for-ties-with-ransomware-gangs

[Source](https://www.bleepingcomputer.com/news/security/russia-arrests-cybercriminal-wazawaka-for-ties-with-ransomware-gangs/){:target="_blank" rel="noopener"}

> Russian law enforcement has arrested and indicted notorious ransomware affiliate Mikhail Pavlovich Matveev (also known as Wazawaka, Uhodiransomwar, m1x, and Boriselcin) for developing malware and his involvement in several hacking groups. [...]
