Title: China's Volt Typhoon reportedly breached Singtel in 'test-run' for US telecom attacks
Date: 2024-11-06T02:30:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-06-chinas-volt-typhoon-reportedly-breached-singtel-in-test-run-for-us-telecom-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/06/chinas_volt_typhoon_breached_singtel/){:target="_blank" rel="noopener"}

> Alleged intrusion spotted in June updated Chinese government cyberspies Volt Typhoon reportedly breached Singapore Telecommunications over the summer as part of their ongoing attacks against critical infrastructure operators.... [...]
