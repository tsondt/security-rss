Title: Critical Veeam RCE bug now used in Frag ransomware attacks
Date: 2024-11-08T15:23:10-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-11-08-critical-veeam-rce-bug-now-used-in-frag-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/critical-veeam-rce-bug-now-used-in-frag-ransomware-attacks/){:target="_blank" rel="noopener"}

> After being used in Akira and Fog ransomware attacks, a critical Veeam Backup & Replication (VBR) security flaw was also recently exploited to deploy Frag ransomware. [...]
