Title: Windows 10 KB5046613 update released with fixes for printer bugs
Date: 2024-11-12T14:37:08-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-11-12-windows-10-kb5046613-update-released-with-fixes-for-printer-bugs

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-10-kb5046613-update-released-with-fixes-for-printer-bugs/){:target="_blank" rel="noopener"}

> Microsoft has released the KB5046613 cumulative update for Windows 10 22H2 and Windows 10 21H2, which includes ten changes and fixes, including the new Microsoft account manager on the Start menu and fixes for multi-function printer issues. [...]
