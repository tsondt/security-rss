Title: Will passkeys ever replace passwords? Can they?
Date: 2024-11-17T18:30:07+00:00
Author: Bruce Davie
Category: The Register
Tags: 
Slug: 2024-11-17-will-passkeys-ever-replace-passwords-can-they

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/17/passkeys_passwords/){:target="_blank" rel="noopener"}

> Here's why they really should Systems Approach I have been playing around with passkeys, or as they are formally known, discoverable credentials.... [...]
