Title: Operation Synergia II sees Interpol swoop on global cyber crims
Date: 2024-11-06T15:25:10+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-11-06-operation-synergia-ii-sees-interpol-swoop-on-global-cyber-crims

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/06/operation_synergia_ii_interpol/){:target="_blank" rel="noopener"}

> 22,000 IP addresses taken down, 59 servers seized, 41 arrests in 95 countries Interpol is reporting a big win after a massive combined operation against online criminals made 41 arrests and seized hardware thought to be used for nefarious purposes.... [...]
