Title: Man accused of hilariously bad opsec as alleged cybercrime spree detailed
Date: 2024-11-26T20:33:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-26-man-accused-of-hilariously-bad-opsec-as-alleged-cybercrime-spree-detailed

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/26/kansas_city_cybercrime_charges/){:target="_blank" rel="noopener"}

> Complaint claims he trespassed, gave himself discounts, and sorted CCTV access... A Kansas City man who stands accused of having a disregard for basic opsec made his first court appearance on Friday over a series of alleged cybercrimes.... [...]
