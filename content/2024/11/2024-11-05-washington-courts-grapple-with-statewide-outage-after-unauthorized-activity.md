Title: Washington courts grapple with statewide outage after 'unauthorized activity'
Date: 2024-11-05T14:29:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-05-washington-courts-grapple-with-statewide-outage-after-unauthorized-activity

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/05/washington_courts_outage/){:target="_blank" rel="noopener"}

> Justice still being served, but many systems are down A statewide IT outage attributed to "unauthorized activity" is affecting the availability of services provided by all courts in Washington.... [...]
