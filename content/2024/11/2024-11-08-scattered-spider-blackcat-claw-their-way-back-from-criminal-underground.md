Title: Scattered Spider, BlackCat claw their way back from criminal underground
Date: 2024-11-08T14:57:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-08-scattered-spider-blackcat-claw-their-way-back-from-criminal-underground

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/08/scattered_spider_blackcat_return/){:target="_blank" rel="noopener"}

> We all know by now that monsters never die, right? Two high-profile criminal gangs, Scattered Spider and BlackCat/ALPHV, seemed to disappear into the darkness like their namesakes following a series of splashy digital heists last year, after which there were arrests and website seizures.... [...]
