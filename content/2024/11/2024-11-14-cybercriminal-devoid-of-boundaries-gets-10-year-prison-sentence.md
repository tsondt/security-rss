Title: Cybercriminal devoid of boundaries gets 10-year prison sentence
Date: 2024-11-14T20:27:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-14-cybercriminal-devoid-of-boundaries-gets-10-year-prison-sentence

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/14/cybercriminal_devoid_of_boundaries_gets/){:target="_blank" rel="noopener"}

> Serial extortionist of medical facilities stooped to cavernous lows in search of small payouts A rampant cybercrook and repeat attacker of medical facilities in the US is being sentenced to a decade in prison, around seven years after the first of his many crimes.... [...]
