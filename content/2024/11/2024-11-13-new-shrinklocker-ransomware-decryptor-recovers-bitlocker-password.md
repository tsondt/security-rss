Title: New ShrinkLocker ransomware decryptor recovers BitLocker password
Date: 2024-11-13T09:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-11-13-new-shrinklocker-ransomware-decryptor-recovers-bitlocker-password

[Source](https://www.bleepingcomputer.com/news/security/new-shrinklocker-ransomware-decryptor-recovers-bitlocker-password/){:target="_blank" rel="noopener"}

> Bitdefender has released a decryptor for the 'ShrinkLocker' ransomware strain, which uses Windows' built-in BitLocker drive encryption tool to lock victim's files. [...]
