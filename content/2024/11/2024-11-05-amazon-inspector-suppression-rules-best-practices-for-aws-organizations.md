Title: Amazon Inspector suppression rules best practices for AWS Organizations
Date: 2024-11-05T19:57:57+00:00
Author: Mojgan Toth
Category: AWS Security
Tags: Advanced (300);Amazon Inspector;AWS Organizations;Security, Identity, & Compliance;Security Blog
Slug: 2024-11-05-amazon-inspector-suppression-rules-best-practices-for-aws-organizations

[Source](https://aws.amazon.com/blogs/security/amazon-inspector-suppression-rules-best-practices-for-aws-organizations/){:target="_blank" rel="noopener"}

> Vulnerability management is a vital part of network, application, and infrastructure security, and its goal is to protect an organization from inadvertent access and exposure of sensitive data and infrastructure. As part of vulnerability management, organizations typically perform a risk assessment to determine which vulnerabilities pose the greatest risk, evaluate their impact on business goals [...]
