Title: Hack Nintendo's alarm clock to show cat pics? Let's-a-go!
Date: 2024-11-01T08:32:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-01-hack-nintendos-alarm-clock-to-show-cat-pics-lets-a-go

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/01/hack_nintendos_alarmo/){:target="_blank" rel="noopener"}

> How 'Gary' defeated Bowser broke into the interactive alarm clock A hacker who uses the handle GaryOderNichts has found a way to break into Nintendo's recently launched Alarmo clock, and run code on the device.... [...]
