Title: Ongoing typosquatting campaign impersonates hundreds of popular npm packages
Date: 2024-11-05T16:28:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-05-ongoing-typosquatting-campaign-impersonates-hundreds-of-popular-npm-packages

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/05/typosquatting_npm_campaign/){:target="_blank" rel="noopener"}

> Puppeteer or Pupeter? One of them will snoop around on your machine and steal your credentials An ongoing typosquatting campaign is targeting developers via hundreds of popular JavaScript libraries, whose weekly downloads number in the tens of millions, to infect systems with info-stealing and snooping malware.... [...]
