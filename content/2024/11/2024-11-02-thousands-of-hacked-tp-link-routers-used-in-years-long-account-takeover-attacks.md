Title: Thousands of hacked TP-Link routers used in years-long account takeover attacks
Date: 2024-11-02T00:13:20+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;botnets;microsoft;password spraying;tp-link
Slug: 2024-11-02-thousands-of-hacked-tp-link-routers-used-in-years-long-account-takeover-attacks

[Source](https://arstechnica.com/information-technology/2024/11/microsoft-warns-of-8000-strong-botnet-used-in-password-spraying-attacks/){:target="_blank" rel="noopener"}

> Hackers working on behalf of the Chinese government are using a botnet of thousands of routers, cameras, and other Internet-connected devices to perform highly evasive password spray attacks against users of Microsoft’s Azure cloud service, the company warned Thursday. The malicious network, made up almost entirely of TP-Link routers, was first documented in October 2023 by a researcher who named it Botnet-7777. The geographically dispersed collection of more than 16,000 compromised devices at its peak got its name because it exposes its malicious malware on port 7777. Account compromise at scale In July and again in August of this year, [...]
