Title: Leaked info of 122 million linked to B2B data aggregator breach
Date: 2024-11-13T16:41:52-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-11-13-leaked-info-of-122-million-linked-to-b2b-data-aggregator-breach

[Source](https://www.bleepingcomputer.com/news/security/leaked-info-of-122-million-linked-to-b2b-data-aggregator-breach/){:target="_blank" rel="noopener"}

> The business contact information for 122 million people circulating since February 2024 is now confirmed to have been stolen from a B2B demand generation platform. [...]
