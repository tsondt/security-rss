Title: Dark web crypto laundering kingpin sentenced to 12.5 years in prison
Date: 2024-11-11T12:38:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-11-dark-web-crypto-laundering-kingpin-sentenced-to-125-years-in-prison

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/11/bitcoin_fog_sentencing/){:target="_blank" rel="noopener"}

> Prosecutors hand Russo-Swede a half-billion bill The operator of the longest-running money laundering machine in dark web history, Bitcoin Fog, has been sentenced to 12 years and six months in US prison.... [...]
