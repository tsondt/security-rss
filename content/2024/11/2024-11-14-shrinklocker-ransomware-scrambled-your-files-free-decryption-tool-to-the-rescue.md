Title: ShrinkLocker ransomware scrambled your files? Free decryption tool to the rescue
Date: 2024-11-14T00:14:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-14-shrinklocker-ransomware-scrambled-your-files-free-decryption-tool-to-the-rescue

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/14/shrinklocker_ransomware_decryptor/){:target="_blank" rel="noopener"}

> Plus: CISA's ScubaGear dives deep to fix M365 misconfigs Bitdefender has released a free decryption tool that can unlock data encrypted by the ShrinkLocker ransomware.... [...]
