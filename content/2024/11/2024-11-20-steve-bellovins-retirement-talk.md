Title: Steve Bellovin’s Retirement Talk
Date: 2024-11-20T16:22:59+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;history of security
Slug: 2024-11-20-steve-bellovins-retirement-talk

[Source](https://www.schneier.com/blog/archives/2024/11/steve-bellovins-retirement-talk.html){:target="_blank" rel="noopener"}

> Steve Bellovin is retiring. Here’s his retirement talk, reflecting on his career and what the cybersecurity field needs next. [...]
