Title: Keyboard robbers steal 171K customers' data from AnnieMac mortgage house
Date: 2024-11-15T19:22:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-15-keyboard-robbers-steal-171k-customers-data-from-anniemac-mortgage-house

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/15/anniemac_data_breach/){:target="_blank" rel="noopener"}

> Names and social security numbers of folks looking for the biggest loan of their lives exposed A major US mortgage lender has told customers looking to make the biggest financial transaction of their lives that an intruder broke into its systems and saw data belonging to 171,000 of them.... [...]
