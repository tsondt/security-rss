Title: Updated whitepaper: Architecting for PCI DSS Segmentation and Scoping on AWS
Date: 2024-11-15T14:55:05+00:00
Author: Abdul Javid
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;PCI;Security Blog;Whitepaper
Slug: 2024-11-15-updated-whitepaper-architecting-for-pci-dss-segmentation-and-scoping-on-aws

[Source](https://aws.amazon.com/blogs/security/updated-whitepaper-architecting-for-pci-dss-segmentation-and-scoping-on-aws/){:target="_blank" rel="noopener"}

> Our mission at AWS Security Assurance Services is to assist with Payment Card Industry Data Security Standard (PCI DSS) compliance for Amazon Web Services (AWS) customers. We work closely with AWS customers to answer their questions about compliance on the AWS Cloud, finding and implementing solutions, and optimizing their controls and assessments. We’ve compiled the [...]
