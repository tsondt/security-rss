Title: Bitfinex hacker gets 5 years in prison for 120,000 bitcoin heist
Date: 2024-11-15T11:36:42-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency;Legal
Slug: 2024-11-15-bitfinex-hacker-gets-5-years-in-prison-for-120000-bitcoin-heist

[Source](https://www.bleepingcomputer.com/news/security/bitfinex-hacker-gets-5-years-in-prison-for-120-000-bitcoin-heist/){:target="_blank" rel="noopener"}

> A hacker responsible for stealing 119,754 Bitcoin in a 2016 hack on the Bitfinex cryptocurrency exchange was sentenced to five years in prison by U.S. authorities. [...]
