Title: DocuSign's Envelopes API abused to send realistic fake invoices
Date: 2024-11-04T15:18:47-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-11-04-docusigns-envelopes-api-abused-to-send-realistic-fake-invoices

[Source](https://www.bleepingcomputer.com/news/security/docusigns-envelopes-api-abused-to-send-realistic-fake-invoices/){:target="_blank" rel="noopener"}

> Threat actors are abusing DocuSign's Envelopes API to create and mass-distribute fake invoices that appear genuine, impersonating well-known brands like Norton and PayPal. [...]
