Title: Fake AI video generators infect Windows, macOS with infostealers
Date: 2024-11-16T15:14:21-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-11-16-fake-ai-video-generators-infect-windows-macos-with-infostealers

[Source](https://www.bleepingcomputer.com/news/security/fake-ai-video-generators-infect-windows-macos-with-infostealers/){:target="_blank" rel="noopener"}

> Fake AI image and video generators infect Windows and macOS with the Lumma Stealer and AMOS information-stealing malware, used to steal credentials and cryptocurrency wallets from infected devices. [...]
