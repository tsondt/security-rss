Title: The only thing worse than being fired is scammers fooling you into thinking you're fired
Date: 2024-11-28T07:31:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-28-the-only-thing-worse-than-being-fired-is-scammers-fooling-you-into-thinking-youre-fired

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/28/fired_phishing_campaign_cloudflare/){:target="_blank" rel="noopener"}

> Scumbags play on victims' worst fears in phishing campaign referencing UK Employment Tribunal A current phishing campaign scares recipients into believing they've been sacked, when in reality they've been hacked – and infected with infostealers and other malware that means a payday for the crooks behind the scam.... [...]
