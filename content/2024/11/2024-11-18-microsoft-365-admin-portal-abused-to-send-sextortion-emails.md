Title: Microsoft 365 Admin portal abused to send sextortion emails
Date: 2024-11-18T07:08:15-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-11-18-microsoft-365-admin-portal-abused-to-send-sextortion-emails

[Source](https://www.bleepingcomputer.com/news/security/microsoft-365-admin-portal-abused-to-send-sextortion-emails/){:target="_blank" rel="noopener"}

> The Microsoft 365 Admin Portal is being abused to send sextortion emails, making the emails appear trustworthy and bypassing email security platforms. [...]
