Title: Microsoft Exchange adds warning to emails abusing spoofing flaw
Date: 2024-11-12T16:45:53-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-11-12-microsoft-exchange-adds-warning-to-emails-abusing-spoofing-flaw

[Source](https://www.bleepingcomputer.com/news/security/unpatched-microsoft-exchange-server-flaw-enables-spoofing-attacks/){:target="_blank" rel="noopener"}

> Microsoft has disclosed a high-severity Exchange Server vulnerability that allows attackers to forge legitimate senders on incoming emails and make malicious messages a lot more effective. [...]
