Title: Suspect behind Snowflake data-theft attacks arrested in Canada
Date: 2024-11-05T10:20:09-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-11-05-suspect-behind-snowflake-data-theft-attacks-arrested-in-canada

[Source](https://www.bleepingcomputer.com/news/security/suspect-behind-snowflake-data-theft-attacks-arrested-in-canada/){:target="_blank" rel="noopener"}

> Canadian authorities have arrested a man suspected of having stolen the data of hundreds of millions after targeting over 165 organizations, all of them customers of cloud storage company Snowflake. [...]
