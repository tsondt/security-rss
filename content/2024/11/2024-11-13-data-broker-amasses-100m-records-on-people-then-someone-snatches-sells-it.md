Title: Data broker amasses 100M+ records on people – then someone snatches, sells it
Date: 2024-11-13T21:44:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-13-data-broker-amasses-100m-records-on-people-then-someone-snatches-sells-it

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/13/demandscience_data/){:target="_blank" rel="noopener"}

> We call this lead degeneration What's claimed to be more than 183 million records of people's contact details and employment info has been stolen or otherwise obtained from a data broker and put up for sale by a miscreant.... [...]
