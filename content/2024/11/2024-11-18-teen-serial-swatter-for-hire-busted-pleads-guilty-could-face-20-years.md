Title: Teen serial swatter-for-hire busted, pleads guilty, could face 20 years
Date: 2024-11-18T00:31:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-11-18-teen-serial-swatter-for-hire-busted-pleads-guilty-could-face-20-years

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/18/teenage_serial_swatterforhire_busted/){:target="_blank" rel="noopener"}

> PLUS: Cost of Halliburton hack disclosed; Time to dump old D-Link NAS; More UN cybercrime convention concerns; and more Infosec in brief A teenager has pleaded guilty to calling in more than 375 fake threats to law enforcement, and now faces years in prison.... [...]
