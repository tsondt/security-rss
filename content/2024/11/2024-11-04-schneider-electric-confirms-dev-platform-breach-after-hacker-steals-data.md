Title: Schneider Electric confirms dev platform breach after hacker steals data
Date: 2024-11-04T14:22:36-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-11-04-schneider-electric-confirms-dev-platform-breach-after-hacker-steals-data

[Source](https://www.bleepingcomputer.com/news/security/schneider-electric-confirms-dev-platform-breach-after-hacker-steals-data/){:target="_blank" rel="noopener"}

> Schneider Electric has confirmed a developer platform was breached after a threat actor claimed to steal 40GB of data from the company's JIRA server. [...]
