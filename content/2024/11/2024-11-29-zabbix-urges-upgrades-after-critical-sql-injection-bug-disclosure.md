Title: Zabbix urges upgrades after critical SQL injection bug disclosure
Date: 2024-11-29T17:44:05+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-29-zabbix-urges-upgrades-after-critical-sql-injection-bug-disclosure

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/29/zabbix_urges_upgrades_after_critical/){:target="_blank" rel="noopener"}

> US agencies blasted 'unforgivable' SQLi flaws earlier this year Open-source enterprise network and application monitoring provider Zabbix is warning customers of a new critical vulnerability that could lead to full system compromise.... [...]
