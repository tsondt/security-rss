Title: Here's what happens if you don't layer network security – or remove unused web shells
Date: 2024-11-22T01:13:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-22-heres-what-happens-if-you-dont-layer-network-security-or-remove-unused-web-shells

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/22/cisa_red_team_exercise/){:target="_blank" rel="noopener"}

> TL;DR: Attackers will break in and pwn you, as a US government red team demonstrated The US Cybersecurity and Infrastructure Agency often breaks into critical organizations' networks – with their permission, of course – to simulate real-world cyber attacks and thereby help improve their security. In one of those recent exercises conducted at a critical infrastructure provider, the Agency exploited a web shell left behind from an earlier bug bounty program, scooped up a bunch of credentials and security keys, moved through the network and ultimately pwned the org's domain and several sensitive business system targets.... [...]
