Title: Why Italy Sells So Much Spyware
Date: 2024-11-19T12:05:31+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Italy;privacy;spyware
Slug: 2024-11-19-why-italy-sells-so-much-spyware

[Source](https://www.schneier.com/blog/archives/2024/11/why-italy-sells-so-much-spyware.html){:target="_blank" rel="noopener"}

> Interesting analysis : Although much attention is given to sophisticated, zero-click spyware developed by companies like Israel’s NSO Group, the Italian spyware marketplace has been able to operate relatively under the radar by specializing in cheaper tools. According to an Italian Ministry of Justice document, as of December 2022 law enforcement in the country could rent spyware for €150 a day, regardless of which vendor they used, and without the large acquisition costs which would normally be prohibitive. As a result, thousands of spyware operations have been carried out by Italian authorities in recent years, according to a report from [...]
