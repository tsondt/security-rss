Title: First-ever UEFI bootkit for Linux in the works, experts say
Date: 2024-11-27T15:32:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-27-first-ever-uefi-bootkit-for-linux-in-the-works-experts-say

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/27/firstever_uefi_bootkit_for_linux/){:target="_blank" rel="noopener"}

> Bootkitty doesn’t bite... yet Security researchers say they've stumbled upon the first-ever UEFI bootkit targeting Linux, illustrating a key moment in the evolution of such tools.... [...]
