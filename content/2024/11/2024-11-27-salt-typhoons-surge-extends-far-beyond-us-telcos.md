Title: Salt Typhoon's surge extends far beyond US telcos
Date: 2024-11-27T23:44:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-27-salt-typhoons-surge-extends-far-beyond-us-telcos

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/27/salt_typhoons_us_telcos/){:target="_blank" rel="noopener"}

> Plus, a brand-new backdoor, GhostSpider, is linked to the cyber spy crew's operations The reach of the China-linked Salt Typhoon gang extends beyond telecommunications giants in the United States, and its arsenal includes several backdoors – including a brand-new malware dubbed GhostSpider – according to Trend Micro researchers.... [...]
