Title: NatWest blocks bevy of apps in clampdown on unmonitorable comms
Date: 2024-11-14T10:53:32+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-14-natwest-blocks-bevy-of-apps-in-clampdown-on-unmonitorable-comms

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/14/natwest_blocks_bevy_of_apps/){:target="_blank" rel="noopener"}

> From guidance to firm action... no more WhatsApp, Meta's Messenger, Signal, Telegram and more The full list of messaging apps officially blocked by Brit banking and insurance giant NatWest Group is more extensive than WhatsApp, Meta's Messenger, and Skype – as first reported.... [...]
