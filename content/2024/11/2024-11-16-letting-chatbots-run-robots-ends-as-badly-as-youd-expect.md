Title: Letting chatbots run robots ends as badly as you'd expect
Date: 2024-11-16T00:03:24+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-11-16-letting-chatbots-run-robots-ends-as-badly-as-youd-expect

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/16/chatbots_run_robots/){:target="_blank" rel="noopener"}

> LLM-controlled droids easily jailbroken to perform mayhem, researchers warn Science fiction author Isaac Asimov proposed three laws of robotics, and you'd never know it from the behavior of today's robots or those making them.... [...]
