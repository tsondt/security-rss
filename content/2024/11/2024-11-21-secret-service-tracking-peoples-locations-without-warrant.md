Title: Secret Service Tracking People’s Locations without Warrant
Date: 2024-11-21T12:03:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;geolocation;privacy;Secret Service;surveillance;tracking
Slug: 2024-11-21-secret-service-tracking-peoples-locations-without-warrant

[Source](https://www.schneier.com/blog/archives/2024/11/secret-service-tracking-peoples-locations-without-warrant.html){:target="_blank" rel="noopener"}

> This feels important : The Secret Service has used a technology called Locate X which uses location data harvested from ordinary apps installed on phones. Because users agreed to an opaque terms of service page, the Secret Service believes it doesn’t need a warrant. [...]
