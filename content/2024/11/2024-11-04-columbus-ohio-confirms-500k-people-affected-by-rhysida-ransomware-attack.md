Title: Columbus, Ohio, confirms 500K people affected by Rhysida ransomware attack
Date: 2024-11-04T17:01:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-04-columbus-ohio-confirms-500k-people-affected-by-rhysida-ransomware-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/04/columbus_rhysida_ransomware/){:target="_blank" rel="noopener"}

> Victims were placed in serious danger following highly sensitive data dump The City of Columbus, Ohio, has confirmed half a million people's data was accessed and potentially stolen when Rhysida's ransomware raided its systems over the summer.... [...]
