Title: 'Cybersecurity issue' at Food Lion parent blamed for US grocery mayhem
Date: 2024-11-12T19:30:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-12-cybersecurity-issue-at-food-lion-parent-blamed-for-us-grocery-mayhem

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/12/ahold_delhaize_cybersecurity_issue_blamed/){:target="_blank" rel="noopener"}

> Stores still open, but customers report delayed deliveries, invoicing issues, and more at Stop & Shop and others Retail giant Ahold Delhaize, which owns Food Lion and Stop & Shop, among others, is confirming outages at several of its US grocery stores are being caused by an ongoing "cybersecurity issue."... [...]
