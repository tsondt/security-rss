Title: Telco engineer who spied on US employer for Beijing gets four years in the clink
Date: 2024-11-27T05:30:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-11-27-telco-engineer-who-spied-on-us-employer-for-beijing-gets-four-years-in-the-clink

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/27/telco_spy_beijing_jailed/){:target="_blank" rel="noopener"}

> Provides insight to how China gets inside US systems, perhaps at Verizon and Infosys A 59 year-old Florida telco engineer was sentenced to 48 months in prison after he served as a spy for China and provided Beijing with details like his employer’s cybersecurity, according to the US Department of Justice.... [...]
