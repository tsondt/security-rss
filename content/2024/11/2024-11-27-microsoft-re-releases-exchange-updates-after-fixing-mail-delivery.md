Title: Microsoft re-releases Exchange updates after fixing mail delivery
Date: 2024-11-27T17:34:12-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-11-27-microsoft-re-releases-exchange-updates-after-fixing-mail-delivery

[Source](https://www.bleepingcomputer.com/news/security/microsoft-re-releases-exchange-updates-after-fixing-mail-delivery/){:target="_blank" rel="noopener"}

> ​Microsoft has re-released the November 2024 security updates for Exchange Server after pulling them earlier this month due to email delivery issues on servers using custom mail flow rules. [...]
