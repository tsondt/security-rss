Title: Amazon confirms employee data breach after vendor hack
Date: 2024-11-11T14:10:58-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-11-11-amazon-confirms-employee-data-breach-after-vendor-hack

[Source](https://www.bleepingcomputer.com/news/security/amazon-confirms-employee-data-breach-after-vendor-hack/){:target="_blank" rel="noopener"}

> Amazon confirmed a data breach involving employee information after data allegedly stolen during the May 2023 MOVEit attacks was leaked on a hacking forum. [...]
