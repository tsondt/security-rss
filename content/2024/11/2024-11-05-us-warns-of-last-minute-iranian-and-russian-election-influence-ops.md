Title: US warns of last-minute Iranian and Russian election influence ops
Date: 2024-11-05T11:23:13-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government
Slug: 2024-11-05-us-warns-of-last-minute-iranian-and-russian-election-influence-ops

[Source](https://www.bleepingcomputer.com/news/security/us-warns-of-last-minute-iranian-and-russian-election-influence-ops/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity & Infrastructure Security Agency is warning about last-minute influence operations conducted by Iranian and Russian actors to undermine the public trust in the integrity and fairness of the upcoming presidential election. [...]
