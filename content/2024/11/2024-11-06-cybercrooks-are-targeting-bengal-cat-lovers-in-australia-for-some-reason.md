Title: Cybercrooks are targeting Bengal cat lovers in Australia for some reason
Date: 2024-11-06T21:47:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-06-cybercrooks-are-targeting-bengal-cat-lovers-in-australia-for-some-reason

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/06/bengal_cat_australia/){:target="_blank" rel="noopener"}

> In case today’s news cycle wasn’t shocking enough, here’s a gem from Sophos Fresh from a series of serious reports detailing its five-year battle with Chinese cyberattackers, Sophos has dropped a curious story about users of a popular infostealer-cum-RAT targeting a niche group of victims.... [...]
