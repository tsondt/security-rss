Title: DDoS site Dstat.cc seized and two suspects arrested in Germany
Date: 2024-11-01T10:50:11-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-11-01-ddos-site-dstatcc-seized-and-two-suspects-arrested-in-germany

[Source](https://www.bleepingcomputer.com/news/security/ddos-site-dstatcc-seized-and-two-suspects-arrested-in-germany/){:target="_blank" rel="noopener"}

> The Dstat.cc DDoS review platform has been seized by law enforcement, and two suspects have been arrested after the service helped fuel distributed denial-of-service attacks for years. [...]
