Title: Secure root user access for member accounts in AWS Organizations
Date: 2024-11-22T14:17:18+00:00
Author: Jonathan VanKim
Category: AWS Security
Tags: AWS Identity and Access Management (IAM);AWS Organizations;Best Practices;Intermediate (200);Security, Identity, & Compliance;Technical How-to;AWS IAM;Security Blog
Slug: 2024-11-22-secure-root-user-access-for-member-accounts-in-aws-organizations

[Source](https://aws.amazon.com/blogs/security/secure-root-user-access-for-member-accounts-in-aws-organizations/){:target="_blank" rel="noopener"}

> AWS Identity and Access Management (IAM) now supports centralized management of root access for member accounts in AWS Organizations. With this capability, you can remove unnecessary root user credentials for your member accounts and automate some routine tasks that previously required root user credentials, such as restoring access to Amazon Simple Storage Service (Amazon S3) [...]
