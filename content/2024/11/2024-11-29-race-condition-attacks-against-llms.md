Title: Race Condition Attacks against LLMs
Date: 2024-11-29T12:01:44+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;cyberattack;LLM
Slug: 2024-11-29-race-condition-attacks-against-llms

[Source](https://www.schneier.com/blog/archives/2024/11/race-condition-attacks-against-llms.html){:target="_blank" rel="noopener"}

> These are two attacks against the system components surrounding LLMs: We propose that LLM Flowbreaking, following jailbreaking and prompt injection, joins as the third on the growing list of LLM attack types. Flowbreaking is less about whether prompt or response guardrails can be bypassed, and more about whether user inputs and generated model outputs can adversely affect these other components in the broader implemented system. [...] When confronted with a sensitive topic, Microsoft 365 Copilot and ChatGPT answer questions that their first-line guardrails are supposed to stop. After a few lines of text they halt—seemingly having “second thoughts”—before retracting the [...]
