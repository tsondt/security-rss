Title: IoT Devices in Password-Spraying Botnet
Date: 2024-11-06T12:02:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;botnets;China;Internet of Things;passwords
Slug: 2024-11-06-iot-devices-in-password-spraying-botnet

[Source](https://www.schneier.com/blog/archives/2024/11/iot-devices-in-password-spraying-botnet.html){:target="_blank" rel="noopener"}

> Microsoft is warning Azure cloud users that a Chinese controlled botnet is engaging in “highly evasive” password spraying. Not sure about the “highly evasive” part; the techniques seem basically what you get in a distributed password-guessing attack: “Any threat actor using the CovertNetwork-1658 infrastructure could conduct password spraying campaigns at a larger scale and greatly increase the likelihood of successful credential compromise and initial access to multiple organizations in a short amount of time,” Microsoft officials wrote. “This scale, combined with quick operational turnover of compromised credentials between CovertNetwork-1658 and Chinese threat actors, allows for the potential of account compromises [...]
