Title: Bologna FC confirms data breach after RansomHub ransomware attack
Date: 2024-11-29T12:19:47-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-11-29-bologna-fc-confirms-data-breach-after-ransomhub-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/bologna-fc-confirms-data-breach-after-ransomhub-ransomware-attack/){:target="_blank" rel="noopener"}

> Bologna Football Club 1909 has confirmed it suffered a ransomware attack after its stolen data was leaked online by the RansomHub extortion group. [...]
