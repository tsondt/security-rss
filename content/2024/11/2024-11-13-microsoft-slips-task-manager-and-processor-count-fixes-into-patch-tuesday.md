Title: Microsoft slips Task Manager and processor count fixes into Patch Tuesday
Date: 2024-11-13T17:35:12+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-11-13-microsoft-slips-task-manager-and-processor-count-fixes-into-patch-tuesday

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/13/microsoft_task_manager_patch_tuesday/){:target="_blank" rel="noopener"}

> Sore about cores no more Microsoft has resolved two issues vexing Windows 11 24H2 and Windows Server 2025 users among the many security updates that emerged on Patch Tuesday.... [...]
