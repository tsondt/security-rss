Title: Windows infected with backdoored Linux VMs in new phishing attacks
Date: 2024-11-04T10:53:06-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Linux
Slug: 2024-11-04-windows-infected-with-backdoored-linux-vms-in-new-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/windows-infected-with-backdoored-linux-vms-in-new-phishing-attacks/){:target="_blank" rel="noopener"}

> A new phishing campaign dubbed 'CRON#TRAP' infects Windows with a Linux virtual machine that contains a built-in backdoor to give stealthy access to corporate networks. [...]
