Title: Germany drafts law to protect researchers who find security flaws
Date: 2024-11-06T10:17:30-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-11-06-germany-drafts-law-to-protect-researchers-who-find-security-flaws

[Source](https://www.bleepingcomputer.com/news/security/germany-drafts-law-to-protect-researchers-who-find-security-flaws/){:target="_blank" rel="noopener"}

> The Federal Ministry of Justice in Germany has drafted a law to provide legal protection to security researchers who discover and responsibly report security vulnerabilities to vendors. [...]
