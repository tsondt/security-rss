Title: New Windows Server 2012 zero-day gets free, unofficial patches
Date: 2024-11-29T12:00:18-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-11-29-new-windows-server-2012-zero-day-gets-free-unofficial-patches

[Source](https://www.bleepingcomputer.com/news/security/new-windows-server-2012-zero-day-gets-free-unofficial-patches/){:target="_blank" rel="noopener"}

> Free unofficial security patches have been released through the 0patch platform to address a zero-day vulnerability introduced over two years ago in the Windows Mark of the Web (MotW) security mechanism. [...]
