Title: Microsoft Power Pages misconfigurations exposing sensitive data
Date: 2024-11-15T06:32:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-15-microsoft-power-pages-misconfigurations-exposing-sensitive-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/15/microsoft_power_pages_misconfigurations/){:target="_blank" rel="noopener"}

> NHS supplier that leaked employee info fell victim to fiddly access controls that can leave databases dangling online Private businesses and public-sector organizations are unwittingly exposing millions of people's sensitive information to the public internet because they misconfigure Microsoft’s Power Pages website creation program.... [...]
