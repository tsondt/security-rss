Title: Bing Wallpaper app, now in Windows Store, accused of cookie shenanigans
Date: 2024-11-26T14:30:15+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-11-26-bing-wallpaper-app-now-in-windows-store-accused-of-cookie-shenanigans

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/26/bing_wallpaper_app/){:target="_blank" rel="noopener"}

> Microsoft free tool snooping on users? Surely not! If you've been tempted to download the Bing Wallpaper app to spice up your Windows 11 desktop backgrounds, you may want to think twice.... [...]
