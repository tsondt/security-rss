Title: Schneider Electric ransomware crew demands $125k paid in baguettes
Date: 2024-11-05T21:51:00+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-05-schneider-electric-ransomware-crew-demands-125k-paid-in-baguettes

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/05/schneider_electric_cybersecurity_incident/){:target="_blank" rel="noopener"}

> Hellcat crew claimed to have gained access via the company's Atlassian Jira system Schneider Electric confirmed that it is investigating a breach as a ransomware group Hellcat claims to have stolen more than 40 GB of compressed data — and demanded the French multinational energy management company pay $125,000 in baguettes or else see its sensitive customer and operational information leaked.... [...]
