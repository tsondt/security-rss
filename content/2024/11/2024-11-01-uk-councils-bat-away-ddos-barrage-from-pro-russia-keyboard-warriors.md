Title: UK councils bat away DDoS barrage from pro-Russia keyboard warriors
Date: 2024-11-01T10:58:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-01-uk-councils-bat-away-ddos-barrage-from-pro-russia-keyboard-warriors

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/01/uk_councils_russia_ddos/){:target="_blank" rel="noopener"}

> Local authority websites downed in response to renewed support for Ukraine Multiple UK councils had their websites either knocked offline or were inaccessible to residents this week after pro-Russia cyber nuisances added them to a daily target list.... [...]
