Title: US space tech giant Maxar discloses employee data breach
Date: 2024-11-18T15:59:42-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-11-18-us-space-tech-giant-maxar-discloses-employee-data-breach

[Source](https://www.bleepingcomputer.com/news/security/us-space-tech-giant-maxar-discloses-employee-data-breach/){:target="_blank" rel="noopener"}

> Hackers breached U.S. satellite maker Maxar Space Systems and accessed personal data belonging to its employees, the company informs in a notification to impacted individuals. [...]
