Title: Security? We've heard of it: How Microsoft plans to better defend Windows
Date: 2024-11-25T19:15:11+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-11-25-security-weve-heard-of-it-how-microsoft-plans-to-better-defend-windows

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/25/microsoft_talks_up_beefier_windows/){:target="_blank" rel="noopener"}

> Did we say CrowdStrike? We meant, er, The July Incident... Ignite The sound of cyber security professionals spraying their screens with coffee could be heard this week as Microsoft claimed, "security is our top priority," as it talked up its Secure Future Initiative (SFI) once again and explained how Windows could be secured.... [...]
