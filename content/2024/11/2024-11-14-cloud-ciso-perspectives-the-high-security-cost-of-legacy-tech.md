Title: Cloud CISO Perspectives: The high security cost of legacy tech
Date: 2024-11-14T17:00:00+00:00
Author: Andy Wen
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2024-11-14-cloud-ciso-perspectives-the-high-security-cost-of-legacy-tech

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-the-high-security-cost-of-legacy-tech/){:target="_blank" rel="noopener"}

> Welcome to the first Cloud CISO Perspectives for November 2024. Today I’m joined by Andy Wen, Google Cloud’s senior director of product management for Google Workspace, to discuss a new Google survey into the high security costs of legacy tech. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI Security & CISO, Google Cloud aside_block <ListValue: [StructValue([('title', 'Get vital board insights with Google Cloud'), ('body', <wagtail.rich_text.RichText object at [...]
