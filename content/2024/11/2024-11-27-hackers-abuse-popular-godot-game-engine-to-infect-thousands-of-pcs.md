Title: Hackers abuse popular Godot game engine to infect thousands of PCs
Date: 2024-11-27T16:17:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-11-27-hackers-abuse-popular-godot-game-engine-to-infect-thousands-of-pcs

[Source](https://www.bleepingcomputer.com/news/security/new-godloader-malware-infects-thousands-of-gamers-using-godot-scripts/){:target="_blank" rel="noopener"}

> ​Hackers have used new GodLoader malware exploiting the capabilities of the widely used Godot game engine to evade detection and infect over 17,000 systems in just three months. [...]
