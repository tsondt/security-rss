Title: Cisco scores a perfect CVSS 10 with critical flaw in its wireless system
Date: 2024-11-07T11:48:53+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-11-07-cisco-scores-a-perfect-cvss-10-with-critical-flaw-in-its-wireless-system

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/07/cisco_uiws_flaw/){:target="_blank" rel="noopener"}

> Ultra-Reliable Wireless Backhaul doesn't live up to its name Cisco is issuing a critical alert notice about a flaw that makes its so-called Ultra-Reliable Wireless Backhaul systems easy to subvert.... [...]
