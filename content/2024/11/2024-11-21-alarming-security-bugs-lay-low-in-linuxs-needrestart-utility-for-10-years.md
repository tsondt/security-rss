Title: 'Alarming' security bugs lay low in Linux's needrestart utility for 10 years
Date: 2024-11-21T15:03:05+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-21-alarming-security-bugs-lay-low-in-linuxs-needrestart-utility-for-10-years

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/21/qualys_needrestart_linux_vulnerabilities/){:target="_blank" rel="noopener"}

> Update now: Qualys says flaws give root to local users, 'easily exploitable', default in Ubuntu Server Researchers at Qualys refuse to release exploit code for five bugs in the Linux world's needrestart utility that allow unprivileged local attackers to gain root access without any user interaction.... [...]
