Title: Supply chain management vendor Blue Yonder succumbs to ransomware
Date: 2024-11-26T01:27:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-11-26-supply-chain-management-vendor-blue-yonder-succumbs-to-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/26/blue_yonder_ransomware/){:target="_blank" rel="noopener"}

> And it looks like major UK retailers that rely on it are feeling the pinch US-based supply chain SaaS vendor Blue Yonder has revealed a service disruption caused by ransomware, and its customers are reportedly struggling to get goods onto shelves as a result.... [...]
