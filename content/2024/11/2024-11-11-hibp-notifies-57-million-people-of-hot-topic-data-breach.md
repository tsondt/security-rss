Title: HIBP notifies 57 million people of Hot Topic data breach
Date: 2024-11-11T16:23:49-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-11-11-hibp-notifies-57-million-people-of-hot-topic-data-breach

[Source](https://www.bleepingcomputer.com/news/security/hibp-notifies-57-million-people-of-hot-topic-data-breach/){:target="_blank" rel="noopener"}

> Have I Been Pwned warns that an alleged data breach exposed the personal information of 56,904,909 accounts for Hot Topic, Box Lunch, and Torrid customers. [...]
