Title: UK hospital network postpones procedures after cyberattack
Date: 2024-11-28T05:08:58-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-11-28-uk-hospital-network-postpones-procedures-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/uk-hospital-network-postpones-procedures-after-cyberattack/){:target="_blank" rel="noopener"}

> Major UK healthcare provider Wirral University Teaching Hospital (WUTH), part of the NHS Foundation Trust, has suffered a cyberattack that caused a systems outage leading to postponing appointments and scheduled procedures. [...]
