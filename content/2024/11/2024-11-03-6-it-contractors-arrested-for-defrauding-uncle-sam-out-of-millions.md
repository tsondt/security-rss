Title: 6 IT contractors arrested for defrauding Uncle Sam out of millions
Date: 2024-11-03T18:30:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-11-03-6-it-contractors-arrested-for-defrauding-uncle-sam-out-of-millions

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/03/6_it_contractors_arrested_for/){:target="_blank" rel="noopener"}

> Also, ecommerce fraud ring disrupted, another Operation Power Off victory, Sino SOHO botnet spotted, and more in brief The US Department of Justice has charged six people with two separate schemes to defraud Uncle Sam out of millions of dollars connected to IT product and services contracts.... [...]
