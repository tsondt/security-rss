Title: LastPass warns of fake support centers trying to steal customer data
Date: 2024-11-01T13:51:12-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-11-01-lastpass-warns-of-fake-support-centers-trying-to-steal-customer-data

[Source](https://www.bleepingcomputer.com/news/security/lastpass-warns-of-fake-support-centers-trying-to-steal-customer-data/){:target="_blank" rel="noopener"}

> LastPass is warning about an ongoing campaign where scammers are writing reviews for its Chrome extension to promote a fake customer support phone number. However, this phone number is part of a much larger campaign to trick callers into giving scammers remote access to their computers, as discovered by BleepingComputer. [...]
