Title: Five Scattered Spider suspects indicted for phishing spree and crypto heists
Date: 2024-11-21T01:29:13+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-11-21-five-scattered-spider-suspects-indicted-for-phishing-spree-and-crypto-heists

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/21/scattered_spider_suspects/){:target="_blank" rel="noopener"}

> DoJ also shutters allleged crimeware and credit card mart PopeyeTools The US Department of Justice has issued an indictment that names five people accused of stealing millions in cryptocurrency – and we are told they are suspected members of cyber-gang Scattered Spider.... [...]
