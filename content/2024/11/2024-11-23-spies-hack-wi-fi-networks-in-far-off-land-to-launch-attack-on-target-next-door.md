Title: Spies hack Wi-Fi networks in far-off land to launch attack on target next door
Date: 2024-11-23T02:03:27+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;advanced persistent threats;hacking;tradecraft
Slug: 2024-11-23-spies-hack-wi-fi-networks-in-far-off-land-to-launch-attack-on-target-next-door

[Source](https://arstechnica.com/security/2024/11/spies-hack-wi-fi-networks-in-far-off-land-to-launch-attack-on-target-next-door/){:target="_blank" rel="noopener"}

> One of 2024's coolest hacking tales occurred two years ago, but it wasn't revealed to the public until Friday at the Cyberwarcon conference in Arlington, Virginia. Hackers with ties to Fancy Bear—the spy agency operated by Russia’s GRU —broke into the network of a high-value target after first compromising a Wi-Fi-enabled device in a nearby building and using it to exploit compromised accounts on the target’s Wi-Fi network. The attack, from a group security firm Volexity calls GruesomeLarch, shows the boundless lengths well-resourced hackers will go to hack high-value targets, presumably only after earlier hack attempts haven’t worked. When the [...]
