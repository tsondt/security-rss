Title: Secure by Design: AWS enhances centralized security controls as MFA requirements expand
Date: 2024-11-15T17:58:22+00:00
Author: Arynn Crow
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS IAM;MFA;multi-factor authentication;Security;Security Blog
Slug: 2024-11-15-secure-by-design-aws-enhances-centralized-security-controls-as-mfa-requirements-expand

[Source](https://aws.amazon.com/blogs/security/secure-by-design-aws-enhances-centralized-security-controls-as-mfa-requirements-expand/){:target="_blank" rel="noopener"}

> At Amazon Web Services (AWS), we’ve built our services with secure by design principles from day one, including features that set a high bar for our customers’ default security posture. Strong authentication is a foundational component in overall account security, and the use of multi-factor authentication (MFA) is one of the simplest and most effective [...]
