Title: Now Online Safety Act is law, UK has 'priorities' – but still won't explain 'spy clause'
Date: 2024-11-21T10:38:11+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2024-11-21-now-online-safety-act-is-law-uk-has-priorities-but-still-wont-explain-spy-clause

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/21/online_safety_act/){:target="_blank" rel="noopener"}

> Draft doc struggles to describe how theoretically encryption-busting powers might be used The UK government has set out plans detailing how it will use the new law it has created to control online platforms and social media – with one telling exception.... [...]
