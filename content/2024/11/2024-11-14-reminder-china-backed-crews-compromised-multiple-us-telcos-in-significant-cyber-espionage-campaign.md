Title: Reminder: China-backed crews compromised 'multiple' US telcos in 'significant cyber espionage campaign'
Date: 2024-11-14T01:54:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-11-14-reminder-china-backed-crews-compromised-multiple-us-telcos-in-significant-cyber-espionage-campaign

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/14/salt_typhoon_hacked_multiple_telecom/){:target="_blank" rel="noopener"}

> Feds don't name Salt Typhoon, but describe Beijing band's alleged deeds The US government has confirmed there was "a broad and significant cyber espionage campaign" conducted by China-linked snoops against "multiple" American telecommunications providers' networks.... [...]
