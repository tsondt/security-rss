Title: US charges Phobos ransomware admin after South Korea extradition
Date: 2024-11-18T14:47:16-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-11-18-us-charges-phobos-ransomware-admin-after-south-korea-extradition

[Source](https://www.bleepingcomputer.com/news/security/us-charges-phobos-ransomware-admin-after-south-korea-extradition/){:target="_blank" rel="noopener"}

> Evgenii Ptitsyn, a Russian national and suspected administrator of the Phobos ransomware operation, was extradited from South Korea and is facing cybercrime charges in the United States. [...]
