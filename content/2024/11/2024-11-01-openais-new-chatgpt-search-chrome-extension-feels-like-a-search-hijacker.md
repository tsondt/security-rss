Title: OpenAI's new ChatGPT Search Chrome extension feels like a search hijacker
Date: 2024-11-01T16:25:37-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Artificial Intelligence
Slug: 2024-11-01-openais-new-chatgpt-search-chrome-extension-feels-like-a-search-hijacker

[Source](https://www.bleepingcomputer.com/news/security/openais-new-chatgpt-search-chrome-extension-feels-like-a-search-hijacker/){:target="_blank" rel="noopener"}

> OpenAI's new "ChatGPT search" Chrome extension feels like nothing more than a typical search hijacker, changing Chrome's settings so your address bar searches go through ChatGPT Search instead. [...]
