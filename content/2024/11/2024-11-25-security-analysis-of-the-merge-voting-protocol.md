Title: Security Analysis of the MERGE Voting Protocol
Date: 2024-11-25T12:09:05+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;DARPA;protocols;voting
Slug: 2024-11-25-security-analysis-of-the-merge-voting-protocol

[Source](https://www.schneier.com/blog/archives/2024/11/security-analysis-of-the-merge-voting-protocol.html){:target="_blank" rel="noopener"}

> Interesting analysis: An Internet Voting System Fatally Flawed in Creative New Ways. Abstract: The recently published “MERGE” protocol is designed to be used in the prototype CAC-vote system. The voting kiosk and protocol transmit votes over the internet and then transmit voter-verifiable paper ballots through the mail. In the MERGE protocol, the votes transmitted over the internet are used to tabulate the results and determine the winners, but audits and recounts use the paper ballots that arrive in time. The enunciated motivation for the protocol is to allow (electronic) votes from overseas military voters to be included in preliminary results [...]
