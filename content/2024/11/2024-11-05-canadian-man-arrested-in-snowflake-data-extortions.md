Title: Canadian Man Arrested in Snowflake Data Extortions
Date: 2024-11-05T17:10:04+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Ne'er-Do-Well News;SIM Swapping;Advance Auto Parts;Alexander Antonin Moucka;AT&T breach;Atomwaffen Division;Austin Larsen;Bharat Sanchar Nigam Ltd;Connor Riley Moucka;Court;IntelSecrets;IRDev;John Erin Binns;Judische;Kiberphant0m;Leak Society;Lending Tree;Mandiant;RapeLash;Satori;Snowflake;Ticketmaster;UNC5537;Verizon;Waifu
Slug: 2024-11-05-canadian-man-arrested-in-snowflake-data-extortions

[Source](https://krebsonsecurity.com/2024/11/canadian-man-arrested-in-snowflake-data-extortions/){:target="_blank" rel="noopener"}

> A 26-year-old man in Ontario, Canada has been arrested for allegedly stealing data from and extorting more than 160 companies that used the cloud data service Snowflake. Image: https://www.pomerium.com/blog/the-real-lessons-from-the-snowflake-breach On October 30, Canadian authorities arrested Alexander Moucka, a.k.a. Connor Riley Moucka of Kitchener, Ontario, on a provisional arrest warrant from the United States. Bloomberg first reported Moucka’s alleged ties to the Snowflake hacks on Monday. At the end of 2023, malicious hackers learned that many large companies had uploaded huge volumes of sensitive customer data to Snowflake accounts that were protected with little more than a username and password (no [...]
