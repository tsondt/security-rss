Title: Andrew Tate's site ransacked, subscriber data stolen
Date: 2024-11-22T22:38:07+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-11-22-andrew-tates-site-ransacked-subscriber-data-stolen

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/22/andrew_tate_raid/){:target="_blank" rel="noopener"}

> He'll just have to take this one on the chin The website of self-proclaimed misogynist and alleged sex trafficker and rapist Andrew Tate has been compromised and data on its paying subscribers stolen.... [...]
