Title: Malicious PyPI package with 37,000 downloads steals AWS keys
Date: 2024-11-09T10:17:27-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-11-09-malicious-pypi-package-with-37000-downloads-steals-aws-keys

[Source](https://www.bleepingcomputer.com/news/security/malicious-pypi-package-with-37-000-downloads-steals-aws-keys/){:target="_blank" rel="noopener"}

> A malicious Python package named 'fabrice' has been present in the Python Package Index (PyPI) since 2021, stealing Amazon Web Services credentials from unsuspecting developers. [...]
