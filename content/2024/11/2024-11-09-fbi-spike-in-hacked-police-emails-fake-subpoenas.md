Title: FBI: Spike in Hacked Police Emails, Fake Subpoenas
Date: 2024-11-09T19:20:26+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;The Coming Storm;Web Fraud 2.0;emergency data request;fake EDR;fbi;Kodex;Matt Donahue;pwnstar
Slug: 2024-11-09-fbi-spike-in-hacked-police-emails-fake-subpoenas

[Source](https://krebsonsecurity.com/2024/11/fbi-spike-in-hacked-police-emails-fake-subpoenas/){:target="_blank" rel="noopener"}

> The Federal Bureau of Investigation (FBI) is urging police departments and governments worldwide to beef up security around their email systems, citing a recent increase in cybercriminal services that use hacked police email accounts to send unauthorized subpoenas and customer data requests to U.S.-based technology companies. In an alert (PDF) published this week, the FBI said it has seen un uptick in postings on criminal forums regarding the process of emergency data requests (EDRs) and the sale of email credentials stolen from police departments and government agencies. “Cybercriminals are likely gaining access to compromised US and foreign government email addresses [...]
