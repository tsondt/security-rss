Title: Scammers target UK senior citizens with Winter Fuel Payment texts
Date: 2024-11-09T16:08:08-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2024-11-09-scammers-target-uk-senior-citizens-with-winter-fuel-payment-texts

[Source](https://www.bleepingcomputer.com/news/security/scammers-target-uk-senior-citizens-with-winter-fuel-payment-texts/){:target="_blank" rel="noopener"}

> As the winter season kicks in, scammers are not missing the chance to target senior British residents with bogus "winter heating allowance" and "cost of living support" scam texts. [...]
