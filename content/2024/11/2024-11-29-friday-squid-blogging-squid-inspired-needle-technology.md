Title: Friday Squid Blogging: Squid-Inspired Needle Technology
Date: 2024-11-29T22:03:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;medicine;squid
Slug: 2024-11-29-friday-squid-blogging-squid-inspired-needle-technology

[Source](https://www.schneier.com/blog/archives/2024/11/friday-squid-blogging-squid-inspired-needle-technology.html){:target="_blank" rel="noopener"}

> Interesting research : Using jet propulsion inspired by squid, researchers demonstrate a microjet system that delivers medications directly into tissues, matching the effectiveness of traditional needles. Blog moderation policy. [...]
