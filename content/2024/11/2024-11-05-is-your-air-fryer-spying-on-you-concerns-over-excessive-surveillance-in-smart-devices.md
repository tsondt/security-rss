Title: Is your air fryer spying on you? Concerns over ‘excessive’ surveillance in smart devices
Date: 2024-11-05T00:00:04+00:00
Author: Robert Booth  UK technology editor
Category: The Guardian
Tags: Smart homes;Data protection;Privacy;Smart speakers;Smartphones;Smartwatches;Technology;Mobile phones;Data and computer security;Surveillance;World news;Telecoms;Consumer affairs;Money;GDPR;UK news
Slug: 2024-11-05-is-your-air-fryer-spying-on-you-concerns-over-excessive-surveillance-in-smart-devices

[Source](https://www.theguardian.com/technology/2024/nov/05/air-fryer-excessive-surveillance-smart-devices-which-watches-speakers-trackers){:target="_blank" rel="noopener"}

> UK consumer group Which? finds some everyday items including watches and speakers are ‘stuffed with trackers’ Air fryers that gather your personal data and audio speakers “stuffed with trackers” are among examples of smart devices engaged in “excessive” surveillance, according to the consumer group Which? The organisation tested three air fryers, increasingly a staple of British kitchens, each of which requested permission to record audio on the user’s phone through a connected app. Continue reading... [...]
