Title: Fraud network uses 4,700 fake shopping sites to steal credit cards
Date: 2024-11-14T17:45:30-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-11-14-fraud-network-uses-4700-fake-shopping-sites-to-steal-credit-cards

[Source](https://www.bleepingcomputer.com/news/security/fraud-network-uses-4-700-fake-shopping-sites-to-steal-credit-cards/){:target="_blank" rel="noopener"}

> A financially motivated Chinese threat actor dubbed "SilkSpecter" is using thousands of fake online stores to steal the payment card details of online shoppers in the U.S. and Europe. [...]
