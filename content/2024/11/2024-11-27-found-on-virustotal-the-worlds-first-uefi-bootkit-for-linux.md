Title: Found on VirusTotal: The world’s first UEFI bootkit for Linux
Date: 2024-11-27T19:21:05+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;bootkit;malware;uefi
Slug: 2024-11-27-found-on-virustotal-the-worlds-first-uefi-bootkit-for-linux

[Source](https://arstechnica.com/security/2024/11/found-in-the-wild-the-worlds-first-unkillable-uefi-bootkit-for-linux/){:target="_blank" rel="noopener"}

> UPDATE: November 28, 3:20 PM California time. The headline of this post has been changed. This update is adding the following further details: this threat is not a UEFI firmware implant or rootkit, it's a UEFI bootkit attacking the bootloader. The Bootkitty sample analyzed by ESET was not unkillable. Below is the article with inaccurate details removed. Researchers at security firm ESET said Wednesday that they found the first UEFI bootkit for Linux. The discovery may portend that UEFI bootkits that have targeted Windows systems in recent years may soon target Linux too. Bootkitty—the name unknown threat actors gave to [...]
