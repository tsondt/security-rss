Title: Hackers now use ZIP file concatenation to evade detection
Date: 2024-11-10T10:13:25-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-11-10-hackers-now-use-zip-file-concatenation-to-evade-detection

[Source](https://www.bleepingcomputer.com/news/security/hackers-now-use-zip-file-concatenation-to-evade-detection/){:target="_blank" rel="noopener"}

> Hackers are targeting Windows machines using the ZIP file concatenation technique to deliver malicious payloads in compressed archives without security solutions detecting them. [...]
