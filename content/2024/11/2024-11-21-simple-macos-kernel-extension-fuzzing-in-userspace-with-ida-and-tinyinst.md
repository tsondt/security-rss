Title: Simple macOS kernel extension fuzzing in userspace with IDA and TinyInst
Date: 2024-11-21T09:53:00-08:00
Author: Google Project Zero (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2024-11-21-simple-macos-kernel-extension-fuzzing-in-userspace-with-ida-and-tinyinst

[Source](https://googleprojectzero.blogspot.com/2024/11/simple-macos-kernel-extension-fuzzing.html){:target="_blank" rel="noopener"}

> Posted by Ivan Fratric, Google Project Zero Recently, one of the projects I was involved in had to do with video decoding on Apple platforms, specifically AV1 decoding. On Apple devices that support AV1 video format (starting from Apple A17 iOS / M3 macOS), decoding is done in hardware. However, despite this, during decoding, a large part of the AV1 format parsing happens in software, inside the kernel, more specifically inside the AppleAVD kernel extension (or at least, that used to be the case in macOS 14/ iOS 17). As fuzzing is one of the techniques we employ regularly, the [...]
