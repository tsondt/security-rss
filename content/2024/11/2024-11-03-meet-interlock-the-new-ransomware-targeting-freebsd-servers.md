Title: Meet Interlock — The new ransomware targeting FreeBSD servers
Date: 2024-11-03T16:09:18-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-11-03-meet-interlock-the-new-ransomware-targeting-freebsd-servers

[Source](https://www.bleepingcomputer.com/news/security/meet-interlock-the-new-ransomware-targeting-freebsd-servers/){:target="_blank" rel="noopener"}

> A relatively new ransomware operation named Interlock attacks organizations worldwide, taking the unusual approach of creating an encryptor to target FreeBSD servers. [...]
