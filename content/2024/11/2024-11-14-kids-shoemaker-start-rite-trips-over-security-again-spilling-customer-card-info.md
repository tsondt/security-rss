Title: Kids' shoemaker Start-Rite trips over security again, spilling customer card info
Date: 2024-11-14T11:57:46+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-14-kids-shoemaker-start-rite-trips-over-security-again-spilling-customer-card-info

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/14/smartrite_breach/){:target="_blank" rel="noopener"}

> Full details exposed, putting shoppers at serious risk of fraud Updated Children's shoemaker Start-Rite is dealing with a nasty "security incident" involving customer payment card details, its second significant lapse during the past eight years.... [...]
