Title: Helpline for Yakuza victims fears it leaked their personal info
Date: 2024-11-22T05:24:07+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-11-22-helpline-for-yakuza-victims-fears-it-leaked-their-personal-info

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/22/helpline_for_yakuza_victims_may/){:target="_blank" rel="noopener"}

> Organized crime types tend not to be kind to those who go against them, so this is nasty A local Japanese government agency dedicated to preventing organized crime has apologized after experiencing an incident it fears may have led to a leak of personal information describing 2,500 people who reached out to it for consultation.... [...]
