Title: D-Link won’t fix critical bug in 60,000 exposed EoL modems
Date: 2024-11-12T15:31:48-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2024-11-12-d-link-wont-fix-critical-bug-in-60000-exposed-eol-modems

[Source](https://www.bleepingcomputer.com/news/security/d-link-wont-fix-critical-bug-in-60-000-exposed-eol-modems/){:target="_blank" rel="noopener"}

> Tens of thousands of exposed D-Link routers that have reached their end-of-life are vulnerable to a critical security issue that allows an unauthenticated remote attacker to change any user's password and take complete control of the device. [...]
