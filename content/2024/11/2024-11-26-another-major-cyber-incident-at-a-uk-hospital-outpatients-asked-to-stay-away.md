Title: Another 'major cyber incident' at a UK hospital, outpatients asked to stay away
Date: 2024-11-26T11:36:39+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-11-26-another-major-cyber-incident-at-a-uk-hospital-outpatients-asked-to-stay-away

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/26/third_major_cyber_incident_declared/){:target="_blank" rel="noopener"}

> Third time this year an NHS unit's IT systems have come under attack A UK hospital is declaring a "major incident," cancelling all outpatient appointments due to "cybersecurity reasons."... [...]
