Title: Federated access to Amazon Athena using AWS IAM Identity Center
Date: 2024-11-27T21:14:16+00:00
Author: Ajay Rawat
Category: AWS Security
Tags: Amazon Athena;AWS IAM Identity Center;Intermediate (200);Security, Identity, & Compliance;Technical How-to;AWS Identity and Access Management (IAM);Security;Security Blog
Slug: 2024-11-27-federated-access-to-amazon-athena-using-aws-iam-identity-center

[Source](https://aws.amazon.com/blogs/security/federated-access-to-amazon-athena-using-aws-iam-identity-center/){:target="_blank" rel="noopener"}

> Managing Amazon Athena through identity federation allows you to manage authentication and authorization procedures centrally. Athena is a serverless, interactive analytics service that provides a simplified and flexible way to analyze petabytes of data. In this blog post, we show you how you can use the Athena JDBC driver (which includes a browser Security Assertion [...]
