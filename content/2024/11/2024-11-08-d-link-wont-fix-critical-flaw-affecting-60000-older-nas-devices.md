Title: D-Link won’t fix critical flaw affecting 60,000 older NAS devices
Date: 2024-11-08T14:21:09-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-11-08-d-link-wont-fix-critical-flaw-affecting-60000-older-nas-devices

[Source](https://www.bleepingcomputer.com/news/security/d-link-wont-fix-critical-flaw-affecting-60-000-older-nas-devices/){:target="_blank" rel="noopener"}

> More than 60,000 D-Link network-attached storage devices that have reached end-of-life are vulnerable to a command injection vulnerability with a publicly available exploit. [...]
