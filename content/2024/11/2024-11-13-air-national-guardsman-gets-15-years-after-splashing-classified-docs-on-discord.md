Title: Air National Guardsman gets 15 years after splashing classified docs on Discord
Date: 2024-11-13T00:01:21+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-11-13-air-national-guardsman-gets-15-years-after-splashing-classified-docs-on-discord

[Source](https://go.theregister.com/feed/www.theregister.com/2024/11/13/teixeira_prison_discord/){:target="_blank" rel="noopener"}

> Jack Teixeira, 22, talked of 'culling the weak minded' – hmm! A former Air National Guard member who stole classified American military secrets, and showed them to his gaming buddies on Discord, has been sentenced to 15 years in prison.... [...]
