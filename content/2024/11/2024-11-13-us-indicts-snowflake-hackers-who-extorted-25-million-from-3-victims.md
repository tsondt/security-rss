Title: US indicts Snowflake hackers who extorted $2.5 million from 3 victims
Date: 2024-11-13T13:52:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-11-13-us-indicts-snowflake-hackers-who-extorted-25-million-from-3-victims

[Source](https://www.bleepingcomputer.com/news/security/us-indicts-snowflake-hackers-who-extorted-25-million-from-3-victims/){:target="_blank" rel="noopener"}

> The Department of Justice has unsealed the indictment against two suspected Snowflake hackers, who breached more than 165 organizations using the services of the Snowflake cloud storage company. [...]
