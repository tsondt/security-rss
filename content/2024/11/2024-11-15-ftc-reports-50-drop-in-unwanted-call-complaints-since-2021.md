Title: FTC reports 50% drop in unwanted call complaints since 2021
Date: 2024-11-15T13:50:46-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-11-15-ftc-reports-50-drop-in-unwanted-call-complaints-since-2021

[Source](https://www.bleepingcomputer.com/news/security/ftc-reports-50-percent-drop-in-unwanted-call-complaints-since-2021/){:target="_blank" rel="noopener"}

> On Friday, the U.S. Federal Trade Commission (FTC) reported that the number of consumer complaints about unwanted telemarketing phone calls has dropped over 50% since 2021, continuing a trend that started three years ago. [...]
