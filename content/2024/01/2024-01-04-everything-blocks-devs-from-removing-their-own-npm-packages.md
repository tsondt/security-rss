Title: 'everything' blocks devs from removing their own npm packages
Date: 2024-01-04T04:55:00-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2024-01-04-everything-blocks-devs-from-removing-their-own-npm-packages

[Source](https://www.bleepingcomputer.com/news/security/everything-blocks-devs-from-removing-their-own-npm-packages/){:target="_blank" rel="noopener"}

> Over the holidays, the npm package registry was flooded with more than 3,000 packages, including one called "everything," and others named a variation of the word. These 3,000+ packages make it impossible for all npm authors to unpublish their packages from the registry. [...]
