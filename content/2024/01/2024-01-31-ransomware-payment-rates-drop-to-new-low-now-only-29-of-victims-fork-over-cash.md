Title: Ransomware payment rates drop to new low – now 'only 29% of victims' fork over cash
Date: 2024-01-31T19:15:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-01-31-ransomware-payment-rates-drop-to-new-low-now-only-29-of-victims-fork-over-cash

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/31/ransomware_payment_rates_drop/){:target="_blank" rel="noopener"}

> It's almost like years of false assurances have made people realize payments are pointless Trusting a ransomware crew to honor a deal isn't the greatest idea, and the world seems to be waking up to that. It's claimed that number of victims who chose to pay dropped to a new low of 29 percent in the last quarter of 2023.... [...]
