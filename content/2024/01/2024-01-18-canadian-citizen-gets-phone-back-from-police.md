Title: Canadian Citizen Gets Phone Back from Police
Date: 2024-01-18T12:02:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cell phones;courts;law enforcement;passwords;phones
Slug: 2024-01-18-canadian-citizen-gets-phone-back-from-police

[Source](https://www.schneier.com/blog/archives/2024/01/canadian-citizen-gets-phone-back-from-police.html){:target="_blank" rel="noopener"}

> After 175 million failed password guesses, a judge rules that the Canadian police must return a suspect’s phone. [Judge] Carter said the investigation can continue without the phones, and he noted that Ottawa police have made a formal request to obtain more data from Google. “This strikes me as a potentially more fruitful avenue of investigation than using brute force to enter the phones,” he said. [...]
