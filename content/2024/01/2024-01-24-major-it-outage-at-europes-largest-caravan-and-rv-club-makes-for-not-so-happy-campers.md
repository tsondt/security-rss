Title: Major IT outage at Europe's largest caravan and RV club makes for not-so-happy campers
Date: 2024-01-24T17:30:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-24-major-it-outage-at-europes-largest-caravan-and-rv-club-makes-for-not-so-happy-campers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/24/major_it_outage_at_caravan/){:target="_blank" rel="noopener"}

> 1 million members still searching for answers as IT issues floor primary digital services The UK's Caravan and Motorhome Club (CAMC) is battling a suspected cyberattack with members reporting widespread IT outages for the past five days.... [...]
