Title: New Images of Colossus Released
Date: 2024-01-30T20:08:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptography;history of cryptography
Slug: 2024-01-30-new-images-of-colossus-released

[Source](https://www.schneier.com/blog/archives/2024/01/new-images-of-colossus-released.html){:target="_blank" rel="noopener"}

> GCHQ has released new images of the WWII Colossus code-breaking computer, celebrating the machine’s eightieth anniversary (birthday?). News article. [...]
