Title: Digital afterlife – how to deal with social media accounts when someone dies
Date: 2024-01-20T19:00:07+00:00
Author: Josh Taylor
Category: The Guardian
Tags: Death and dying;Social media;Privacy;Google;Facebook;Australia news;Data and computer security;Law;Digital media;Social networking
Slug: 2024-01-20-digital-afterlife-how-to-deal-with-social-media-accounts-when-someone-dies

[Source](https://www.theguardian.com/society/2024/jan/21/digital-afterlife-how-to-deal-with-social-media-accounts-when-someone-dies){:target="_blank" rel="noopener"}

> Deciding what to do with a dead friend or relative’s online presence is complicated and time-consuming but there are shortcuts Find more essential summer reading Get our morning and afternoon news emails, free app or daily news podcast Gavin Blomeley was lucky his mother was incredibly organised before she died. She left a note that included the passcode to her phone and access to all her online passwords. “I can’t even begin to imagine how difficult this could have gotten not having these passwords or knowing this note with all of her passwords existed,” Blomeley says. Sign up for Guardian [...]
