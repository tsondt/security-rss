Title: Atos confirms talks with Airbus over cybersecurity wing sale
Date: 2024-01-03T15:45:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-03-atos-confirms-talks-with-airbus-over-cybersecurity-wing-sale

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/03/atos_confirms_airbus_talks/){:target="_blank" rel="noopener"}

> IT service company's latest move to clear its maturing debts French IT services provider Atos has entered talks with Airbus to sell its tech security division in an effort to ease its financial burdens.... [...]
