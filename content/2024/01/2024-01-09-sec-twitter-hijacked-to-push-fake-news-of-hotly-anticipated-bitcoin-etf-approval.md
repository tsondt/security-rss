Title: SEC Twitter hijacked to push fake news of hotly anticipated Bitcoin ETF approval
Date: 2024-01-09T21:48:07+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: 2024-01-09-sec-twitter-hijacked-to-push-fake-news-of-hotly-anticipated-bitcoin-etf-approval

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/09/sec_bitcoin_etf_hacked/){:target="_blank" rel="noopener"}

> Buy the hype, sell the, wait, what do we do now?! The SEC today said its Twitter account was hijacked to wrongly claim it had approved hotly anticipated Bitcoin ETFs, causing the cryptocurrency to spike and then slip in price.... [...]
