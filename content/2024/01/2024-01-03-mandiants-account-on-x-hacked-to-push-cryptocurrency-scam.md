Title: Mandiant’s account on X hacked to push cryptocurrency scam
Date: 2024-01-03T17:50:16-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-01-03-mandiants-account-on-x-hacked-to-push-cryptocurrency-scam

[Source](https://www.bleepingcomputer.com/news/security/mandiants-account-on-x-hacked-to-push-cryptocurrency-scam/){:target="_blank" rel="noopener"}

> The Twitter account of American cybersecurity firm and Google subsidiary Mandiant was hijacked earlier today to impersonate the Phantom crypto wallet and share a cryptocurrency scam. [...]
