Title: Capital Health attack claimed by LockBit ransomware, risk of data leak
Date: 2024-01-08T10:53:33-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-01-08-capital-health-attack-claimed-by-lockbit-ransomware-risk-of-data-leak

[Source](https://www.bleepingcomputer.com/news/security/capital-health-attack-claimed-by-lockbit-ransomware-risk-of-data-leak/){:target="_blank" rel="noopener"}

> The Lockbit ransomware operation has claimed responsibility for a November 2023 cyberattack on the Capital Health hospital network and threatens to leak stolen data and negotiation chats by tomorrow. [...]
