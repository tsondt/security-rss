Title: Ambient light sensors can reveal your device activity. How big a threat is it?
Date: 2024-01-23T16:20:25+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;ambient light sensors;Phones;privacy;Smart devices
Slug: 2024-01-23-ambient-light-sensors-can-reveal-your-device-activity-how-big-a-threat-is-it

[Source](https://arstechnica.com/?p=1998032){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) An overwhelming majority of handheld devices these days have ambient light sensors built into them. A large percentage of TVs and monitors do, too, and that proportion is growing. The sensors allow devices to automatically adjust the screen brightness based on how light or dark the surroundings are. That, in turn, reduces eye strain and improves power consumption. New research reveals that embedded ambient light sensors can, under certain conditions, allow website operators, app makers, and others to pry into user actions that until now have been presumed to be private. A proof-of-concept attack coming out [...]
