Title: AWS completes the first cloud audit by the Ingelheim Kreis Initiative Joint Audits group for the pharmaceutical and life sciences sector
Date: 2024-01-22T17:06:51+00:00
Author: Janice Leung
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;audit;Auditing;AWS security;GxP;Healthcare;Security Blog
Slug: 2024-01-22-aws-completes-the-first-cloud-audit-by-the-ingelheim-kreis-initiative-joint-audits-group-for-the-pharmaceutical-and-life-sciences-sector

[Source](https://aws.amazon.com/blogs/security/aws-completes-the-first-cloud-audit-by-the-ingelheim-kreis-initiative-joint-audits-group-for-the-pharmaceutical-and-life-sciences-sector/){:target="_blank" rel="noopener"}

> We’re excited to announce that Amazon Web Services (AWS) has completed the first cloud service provider (CSP) audit by the Ingelheim Kreis (IK) Initiative Joint Audits group. The audit group represents quality and compliance professionals from some of our largest pharmaceutical and life sciences customers who collectively perform audits on their key suppliers. As customers [...]
