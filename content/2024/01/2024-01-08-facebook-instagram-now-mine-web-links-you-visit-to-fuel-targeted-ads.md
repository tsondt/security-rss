Title: Facebook, Instagram now mine web links you visit to fuel targeted ads
Date: 2024-01-08T07:27:12+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-01-08-facebook-instagram-now-mine-web-links-you-visit-to-fuel-targeted-ads

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/08/security_in_brief/){:target="_blank" rel="noopener"}

> Also: Twitter hijackings, BEC arrest, and critical vulnerabilities Infosec in brief We gather everyone's still easing themselves into the New Year. Deleting screens of unread emails, putting on a brave face in meetings, and slowly getting up to speed. While you're recovering from the Christmas break, Meta has been busy introducing fresh ways to monetize your web surfing habits while dressing it up as a user experience improvement.... [...]
