Title: Combination of cheap .cloud domains and fake Shark Tank news fuel unhealthy wellness scams
Date: 2024-01-17T06:29:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-01-17-combination-of-cheap-cloud-domains-and-fake-shark-tank-news-fuel-unhealthy-wellness-scams

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/17/netcraft_health_scams_analysis/){:target="_blank" rel="noopener"}

> .SBS gTLD once owned by Australian broadcaster is another source of strife Scammers are buying up cheap domain names to host sites that sell dodgy health products using fake articles, according to cybercrime disruption outfit Netcraft.... [...]
