Title: HPE: Russian hackers breached its security team’s email accounts
Date: 2024-01-24T16:50:29-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-01-24-hpe-russian-hackers-breached-its-security-teams-email-accounts

[Source](https://www.bleepingcomputer.com/news/security/hpe-russian-hackers-breached-its-security-teams-email-accounts/){:target="_blank" rel="noopener"}

> Hewlett Packard Enterprise (HPE) disclosed today that suspected Russian hackers known as Midnight Blizzard gained access to the company's Microsoft Office 365 email environment to steal data from its cybersecurity team and other departments. [...]
