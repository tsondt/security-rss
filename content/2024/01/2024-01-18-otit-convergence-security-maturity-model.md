Title: OT/IT convergence security maturity model
Date: 2024-01-18T18:51:22+00:00
Author: James Hobbs
Category: AWS Security
Tags: Advanced (300);Best Practices;Security, Identity, & Compliance;Security Blog
Slug: 2024-01-18-otit-convergence-security-maturity-model

[Source](https://aws.amazon.com/blogs/security/ot-it-convergence-security-maturity-model/){:target="_blank" rel="noopener"}

> For decades, we’ve watched energy companies attempt to bring off-the-shelf information technology (IT) systems into operations technology (OT) environments. These attempts have had varying degrees of success. While converging OT and IT brings new efficiencies, it also brings new risks. There are many moving parts to convergence, and there are several questions that you must [...]
