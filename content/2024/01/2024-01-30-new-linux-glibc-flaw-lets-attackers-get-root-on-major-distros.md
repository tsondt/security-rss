Title: New Linux glibc flaw lets attackers get root on major distros
Date: 2024-01-30T18:06:35-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Linux
Slug: 2024-01-30-new-linux-glibc-flaw-lets-attackers-get-root-on-major-distros

[Source](https://www.bleepingcomputer.com/news/security/new-linux-glibc-flaw-lets-attackers-get-root-on-major-distros/){:target="_blank" rel="noopener"}

> ​Unprivileged attackers can get root access on multiple major Linux distributions in default configurations by exploiting a newly disclosed local privilege escalation (LPE) vulnerability in the GNU C Library (glibc). [...]
