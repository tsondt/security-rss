Title: AWS named as a Leader in 2023 ISG Provider Lens report for Multi Public Cloud Services – Sovereign Cloud Infrastructure Services (EU)
Date: 2024-01-09T21:59:21+00:00
Author: Marta Taggart
Category: AWS Security
Tags: Announcements;Europe;Foundational (100);Security, Identity, & Compliance;AWS Digital Sovereignty Pledge;Digital Sovereignty;report;Security Blog;Sovereign-by-design
Slug: 2024-01-09-aws-named-as-a-leader-in-2023-isg-provider-lens-report-for-multi-public-cloud-services-sovereign-cloud-infrastructure-services-eu

[Source](https://aws.amazon.com/blogs/security/aws-named-as-a-leader-in-2023-isg-provider-lens-report-for-multi-public-cloud-services-sovereign-cloud-infrastructure-services-eu/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is named as a Leader in the 2023 ISG Provider Lens Quadrant Report for Multi Public Cloud Services – Sovereign Cloud Infrastructure Services (EU), published on January 8, 2024. This first-ever Information Services Group (ISG) report evaluates providers of sovereign cloud infrastructure services in the multi public cloud environment and examines [...]
