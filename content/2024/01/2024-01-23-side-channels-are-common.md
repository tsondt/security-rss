Title: Side Channels Are Common
Date: 2024-01-23T12:09:42+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;sensors;side-channel attacks
Slug: 2024-01-23-side-channels-are-common

[Source](https://www.schneier.com/blog/archives/2024/01/side-channels-are-common.html){:target="_blank" rel="noopener"}

> Really interesting research: “ Lend Me Your Ear: Passive Remote Physical Side Channels on PCs.” Abstract: We show that built-in sensors in commodity PCs, such as microphones, inadvertently capture electromagnetic side-channel leakage from ongoing computation. Moreover, this information is often conveyed by supposedly-benign channels such as audio recordings and common Voice-over-IP applications, even after lossy compression. Thus, we show, it is possible to conduct physical side-channel attacks on computation by remote and purely passive analysis of commonly-shared channels. These attacks require neither physical proximity (which could be mitigated by distance and shielding), nor the ability to run code on the [...]
