Title: Cisco warns of critical RCE flaw in communications software
Date: 2024-01-25T09:41:11-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-25-cisco-warns-of-critical-rce-flaw-in-communications-software

[Source](https://www.bleepingcomputer.com/news/security/cisco-warns-of-critical-rce-flaw-in-communications-software/){:target="_blank" rel="noopener"}

> Cisco is warning that several of its Unified Communications Manager (CM) and Contact Center Solutions products are vulnerable to a critical severity remote code execution security issue. [...]
