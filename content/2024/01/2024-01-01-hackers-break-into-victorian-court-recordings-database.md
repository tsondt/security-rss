Title: Hackers break into Victorian court recordings database
Date: 2024-01-01T23:28:00+00:00
Author: Australian Associated Press
Category: The Guardian
Tags: Victoria;Cybercrime;Data and computer security
Slug: 2024-01-01-hackers-break-into-victorian-court-recordings-database

[Source](https://www.theguardian.com/australia-news/2024/jan/02/victoria-court-recording-hack-details){:target="_blank" rel="noopener"}

> Court Service Victoria says it will notify those captured on recordings of hearings of the breach Victoria’s court system has been hit by a cyber-attack, with hackers accessing several weeks of recorded hearings. Court Services Victoria (CSV) was first made aware of the attack on 21 December but it is believed the audio-visual technology network was first compromised on 1 November. Continue reading... [...]
