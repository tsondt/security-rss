Title: ShinyHunters chief phisherman gets 3 years, must cough up $5M
Date: 2024-01-10T15:30:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-10-shinyhunters-chief-phisherman-gets-3-years-must-cough-up-5m

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/10/shinyhunters_kingpin_prison/){:target="_blank" rel="noopener"}

> Sebastien Raoult developed various credential-harvesting websites over more than 2 years A key member of the ShinyHunters cybercrime group is facing three years in the slammer and being forced to return $5 million in criminal proceeds.... [...]
