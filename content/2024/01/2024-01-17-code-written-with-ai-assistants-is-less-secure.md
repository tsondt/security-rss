Title: Code Written with AI Assistants Is Less Secure
Date: 2024-01-17T12:14:03+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence;programming;security analysis
Slug: 2024-01-17-code-written-with-ai-assistants-is-less-secure

[Source](https://www.schneier.com/blog/archives/2024/01/code-written-with-ai-assistants-is-less-secure.html){:target="_blank" rel="noopener"}

> Interesting research: “ Do Users Write More Insecure Code with AI Assistants? “: Abstract: We conduct the first large-scale user study examining how users interact with an AI Code assistant to solve a variety of security related tasks across different programming languages. Overall, we find that participants who had access to an AI assistant based on OpenAI’s codex-davinci-002 model wrote significantly less secure code than those without access. Additionally, participants with access to an AI assistant were more likely to believe they wrote secure code than those without access to the AI assistant. Furthermore, we find that participants who trusted [...]
