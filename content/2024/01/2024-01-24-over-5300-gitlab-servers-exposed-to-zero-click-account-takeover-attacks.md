Title: Over 5,300 GitLab servers exposed to zero-click account takeover attacks
Date: 2024-01-24T12:55:29-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-24-over-5300-gitlab-servers-exposed-to-zero-click-account-takeover-attacks

[Source](https://www.bleepingcomputer.com/news/security/over-5-300-gitlab-servers-exposed-to-zero-click-account-takeover-attacks/){:target="_blank" rel="noopener"}

> Over 5,300 internet-exposed GitLab instances are vulnerable to CVE-2023-7028, a zero-click account takeover flaw GitLab warned about earlier this month. [...]
