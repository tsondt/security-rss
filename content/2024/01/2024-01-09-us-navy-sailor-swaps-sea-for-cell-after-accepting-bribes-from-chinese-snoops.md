Title: US Navy sailor swaps sea for cell after accepting bribes from Chinese snoops
Date: 2024-01-09T16:30:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-09-us-navy-sailor-swaps-sea-for-cell-after-accepting-bribes-from-chinese-snoops

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/09/us_navy_petty_officer_china_bribes/){:target="_blank" rel="noopener"}

> Petty officer Wenheng Zhao admitted to taking as many as 14 payoffs in return for non-public military information A US Naval sailor will face more than two years behind bars after pleading guilty to taking bribes from Chinese spies in exchange for sensitive military information.... [...]
