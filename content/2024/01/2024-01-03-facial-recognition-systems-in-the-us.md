Title: Facial Recognition Systems in the US
Date: 2024-01-03T12:07:44+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;face recognition;identification;privacy;surveillance
Slug: 2024-01-03-facial-recognition-systems-in-the-us

[Source](https://www.schneier.com/blog/archives/2024/01/facial-recognition-systems-in-the-us.html){:target="_blank" rel="noopener"}

> A helpful summary of which US retail stores are using facial recognition, thinking about using it, or currently not planning on using it. (This, of course, can all change without notice.) Three years ago, I wrote that campaigns to ban facial recognition are too narrow. The problem here is identification, correlation, and then discrimination. There’s no difference whether the identification technology is facial recognition, the MAC address of our phones, gait recognition, license plate recognition, or anything else. Facial recognition is just the easiest technology right now. [...]
