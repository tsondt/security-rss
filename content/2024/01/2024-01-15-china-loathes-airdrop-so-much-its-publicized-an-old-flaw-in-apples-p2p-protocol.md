Title: China loathes AirDrop so much it's publicized an old flaw in Apple's P2P protocol
Date: 2024-01-15T02:58:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-01-15-china-loathes-airdrop-so-much-its-publicized-an-old-flaw-in-apples-p2p-protocol

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/15/china_airdrop_anonymity_warning/){:target="_blank" rel="noopener"}

> Infosec academic suggests Beijing's warning that iThing owners aren't anonymous deserves attention outside the great firewall too In June 2023 China made a typically bombastic announcement: operators of short-distance ad hoc networks must ensure they run according to proper socialist principles, and ensure all users divulge their real-world identities.... [...]
