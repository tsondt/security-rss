Title: ICO fines spam slinging financial services biz
Date: 2024-01-22T11:00:05+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2024-01-22-ico-fines-spam-slinging-financial-services-biz

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/22/ico_fines_spam_slinging_financial/){:target="_blank" rel="noopener"}

> It's all very well offering 'Free Debt Help,' but recipients were unwilling, says watchdog... A financial services company that illegally dispatched tens of thousands of spam messages promising to help the recipients magically wipe away their debts is itself now a debtor to the UK’s data regulator.... [...]
