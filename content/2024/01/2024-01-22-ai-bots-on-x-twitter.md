Title: AI Bots on X (Twitter)
Date: 2024-01-22T12:09:55+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;chatbots;identification;Twitter
Slug: 2024-01-22-ai-bots-on-x-twitter

[Source](https://www.schneier.com/blog/archives/2024/01/ai-bots-on-x-twitter.html){:target="_blank" rel="noopener"}

> You can find them by searching for OpenAI chatbot warning messages, like: “I’m sorry, I cannot provide a response as it goes against OpenAI’s use case policy.” I hadn’t thought about this before: identifying bots by searching for distinctive bot phrases. [...]
