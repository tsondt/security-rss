Title: Atlassian warns of critical RCE flaw in older Confluence versions
Date: 2024-01-16T10:17:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-16-atlassian-warns-of-critical-rce-flaw-in-older-confluence-versions

[Source](https://www.bleepingcomputer.com/news/security/atlassian-warns-of-critical-rce-flaw-in-older-confluence-versions/){:target="_blank" rel="noopener"}

> Atlassian Confluence Data Center and Confluence Server are vulnerable to a critical remote code execution (RCE) vulnerability that impacts versions released before December 5, 2023, including out-of-support releases. [...]
