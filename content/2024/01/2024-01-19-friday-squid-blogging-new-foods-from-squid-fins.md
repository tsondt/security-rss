Title: Friday Squid Blogging: New Foods from Squid Fins
Date: 2024-01-19T22:07:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-01-19-friday-squid-blogging-new-foods-from-squid-fins

[Source](https://www.schneier.com/blog/archives/2024/01/friday-squid-blogging-new-foods-from-squid-fins.html){:target="_blank" rel="noopener"}

> We only eat about half of a squid, ignoring the fins. A group of researchers is working to change that. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
