Title: Europcar denies data breach of 50 million users, says data is fake
Date: 2024-01-31T14:25:10-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-01-31-europcar-denies-data-breach-of-50-million-users-says-data-is-fake

[Source](https://www.bleepingcomputer.com/news/security/europcar-denies-data-breach-of-50-million-users-says-data-is-fake/){:target="_blank" rel="noopener"}

> Car rental company Europcar says it has not suffered a data breach and that shared customer data is fake after a threat actor claimed to be selling the personal info of 50 million customers. [...]
