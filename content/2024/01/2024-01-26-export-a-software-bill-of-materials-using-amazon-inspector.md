Title: Export a Software Bill of Materials using Amazon Inspector
Date: 2024-01-26T19:21:36+00:00
Author: Varun Sharma
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;Amazon Inspector;Security Blog
Slug: 2024-01-26-export-a-software-bill-of-materials-using-amazon-inspector

[Source](https://aws.amazon.com/blogs/security/export-a-software-bill-of-materials-using-amazon-inspector/){:target="_blank" rel="noopener"}

> Amazon Inspector is an automated vulnerability management service that continually scans Amazon Web Services (AWS) workloads for software vulnerabilities and unintended network exposure. Amazon Inspector has expanded capability that allows customers to export a consolidated Software Bill of Materials (SBOM) for supported Amazon Inspector monitored resources, excluding Windows EC2 instances. Customers have asked us to [...]
