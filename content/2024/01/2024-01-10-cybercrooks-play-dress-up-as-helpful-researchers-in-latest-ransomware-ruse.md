Title: Cybercrooks play dress-up as 'helpful' researchers in latest ransomware ruse
Date: 2024-01-10T17:01:43+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-10-cybercrooks-play-dress-up-as-helpful-researchers-in-latest-ransomware-ruse

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/10/phony_ransomware_researchers/){:target="_blank" rel="noopener"}

> Posing as cyber samaritans, scumbags are kicking folks when they're down Ransomware victims already reeling from potential biz disruption and the cost of resolving the matter are now being subjected to follow-on extortion attempts by criminals posing as helpful security researchers.... [...]
