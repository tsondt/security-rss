Title: Insurance website's buggy API leaked Office 365 password and a giant email trove
Date: 2024-01-18T01:58:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-01-18-insurance-websites-buggy-api-leaked-office-365-password-and-a-giant-email-trove

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/18/ttibi_office_buggy/){:target="_blank" rel="noopener"}

> Pen-tester accessed more than 650,000 sensitive messages, and still can, at Indian outfit using Toyota SaaS Toyota Tsusho Insurance Broker India (TTIBI), an Indo-Japanese joint insurance venture, operated a misconfigured server that exposed more than 650,000 Microsoft-hosted email messages to customers, a security researcher has found.... [...]
