Title: TikTok Editorial Analysis
Date: 2024-01-02T12:04:42+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;China;filtering;propaganda;social media
Slug: 2024-01-02-tiktok-editorial-analysis

[Source](https://www.schneier.com/blog/archives/2024/01/tiktok-editorial-analysis.html){:target="_blank" rel="noopener"}

> TikTok seems to be skewing things in the interests of the Chinese Communist Party. (This is a serious analysis, and the methodology looks sound.) Conclusion: Substantial Differences in Hashtag Ratios Raise Concerns about TikTok’s Impartiality Given the research above, we assess a strong possibility that content on TikTok is either amplified or suppressed based on its alignment with the interests of the Chinese Government. Future research should aim towards a more comprehensive analysis to determine the potential influence of TikTok on popular public narratives. This research should determine if and how TikTok might be utilized for furthering national/regional or international [...]
