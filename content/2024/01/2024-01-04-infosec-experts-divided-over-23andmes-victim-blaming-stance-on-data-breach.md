Title: Infosec experts divided over 23andMe's 'victim-blaming' stance on data breach
Date: 2024-01-04T18:30:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-04-infosec-experts-divided-over-23andmes-victim-blaming-stance-on-data-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/04/23andme_victim_blaming_breach/){:target="_blank" rel="noopener"}

> Users apparently at fault after reusing credentials the company didn't check were already compromised 23andMe users' godawful password practices were supposedly to blame for the biotech company's October data disaster, according to its legal reps.... [...]
