Title: Three Chinese balloons float near Taiwanese airbase
Date: 2024-01-04T10:15:11+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-01-04-three-chinese-balloons-float-near-taiwanese-airbase

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/04/three_chinese_balloons_float_near/){:target="_blank" rel="noopener"}

> Also: Remember that balloon over the US last February? It might have used a US internet provider Four Chinese balloons have reportedly floated over the Taiwan Strait, three of them crossing over the island's land mass and near its Ching-Chuan-Kang air base before disappearing, according to the Taiwan's defense ministry.... [...]
