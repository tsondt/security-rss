Title: Paraguay warns of Black Hunt ransomware attacks after Tigo Business breach
Date: 2024-01-09T11:28:08-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-01-09-paraguay-warns-of-black-hunt-ransomware-attacks-after-tigo-business-breach

[Source](https://www.bleepingcomputer.com/news/security/paraguay-warns-of-black-hunt-ransomware-attacks-after-tigo-business-breach/){:target="_blank" rel="noopener"}

> The Paraguay military is warning of Black Hunt ransomware attacks after Tigo Business suffered a cyberattack last week impacting cloud and hosting services in the company's business division. [...]
