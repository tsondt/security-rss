Title: Data breach at healthcare tech firm impacts 4.5 million patients
Date: 2024-01-03T11:23:06-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-01-03-data-breach-at-healthcare-tech-firm-impacts-45-million-patients

[Source](https://www.bleepingcomputer.com/news/security/data-breach-at-healthcare-tech-firm-impacts-45-million-patients/){:target="_blank" rel="noopener"}

> HealthEC LLC, a provider of health management solutions, suffered a data breach that impacts close to 4.5 million individuals who received care through one of the company's customers. [...]
