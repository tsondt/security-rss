Title: Water services giant Veolia North America hit by ransomware attack
Date: 2024-01-23T16:52:56-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-23-water-services-giant-veolia-north-america-hit-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/water-services-giant-veolia-north-america-hit-by-ransomware-attack/){:target="_blank" rel="noopener"}

> Veolia North America, a subsidiary of transnational conglomerate Veolia, disclosed a ransomware attack that impacted systems part of its Municipal Water division and disrupted its bill payment systems. [...]
