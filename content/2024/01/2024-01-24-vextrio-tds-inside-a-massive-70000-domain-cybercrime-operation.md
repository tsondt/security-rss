Title: VexTrio TDS: Inside a massive 70,000-domain cybercrime operation
Date: 2024-01-24T14:46:40-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-24-vextrio-tds-inside-a-massive-70000-domain-cybercrime-operation

[Source](https://www.bleepingcomputer.com/news/security/vextrio-tds-inside-a-massive-70-000-domain-cybercrime-operation/){:target="_blank" rel="noopener"}

> A previously unknown traffic distribution system (TDS) named 'VexTrio' has been active since at least 2017, aiding 60 affiliates in their cybercrime operations through a massive network of 70,000 sites. [...]
