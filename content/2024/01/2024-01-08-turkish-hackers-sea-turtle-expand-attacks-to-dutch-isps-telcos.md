Title: Turkish hackers Sea Turtle expand attacks to Dutch ISPs, telcos
Date: 2024-01-08T15:38:33-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-08-turkish-hackers-sea-turtle-expand-attacks-to-dutch-isps-telcos

[Source](https://www.bleepingcomputer.com/news/security/turkish-hackers-sea-turtle-expand-attacks-to-dutch-isps-telcos/){:target="_blank" rel="noopener"}

> The Turkish state-backed cyber espionage group tracked as Sea Turtle has been carrying out multiple spying campaigns in the Netherlands, focusing on telcos, media, internet service providers (ISPs), and Kurdish websites. [...]
