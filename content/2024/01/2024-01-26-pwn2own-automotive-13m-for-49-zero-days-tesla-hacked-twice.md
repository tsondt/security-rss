Title: Pwn2Own Automotive: $1.3M for 49 zero-days, Tesla hacked twice
Date: 2024-01-26T07:32:23-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-26-pwn2own-automotive-13m-for-49-zero-days-tesla-hacked-twice

[Source](https://www.bleepingcomputer.com/news/security/pwn2own-automotive-13m-for-49-zero-days-tesla-hacked-twice/){:target="_blank" rel="noopener"}

> The first edition of Pwn2Own Automotive has ended with competitors earning $1,323,750 for hacking Tesla twice and demoing 49 zero-day bugs in multiple electric car systems between January 24 and January 26. [...]
