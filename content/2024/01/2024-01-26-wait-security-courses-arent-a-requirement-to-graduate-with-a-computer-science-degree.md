Title: Wait, security courses aren't a requirement to graduate with a computer science degree?
Date: 2024-01-26T21:28:07+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-26-wait-security-courses-arent-a-requirement-to-graduate-with-a-computer-science-degree

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/26/security_courses_requirements/){:target="_blank" rel="noopener"}

> And software makers seem to be OK with this, apparently Comment There's a line in the latest plea from CISA – the US government's cybersecurity agency – to software developers to do a better job of writing secure code that may make you spit out your coffee.... [...]
