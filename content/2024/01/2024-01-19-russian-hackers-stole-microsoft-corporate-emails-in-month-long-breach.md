Title: Russian hackers stole Microsoft corporate emails in month-long breach
Date: 2024-01-19T19:02:12-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-01-19-russian-hackers-stole-microsoft-corporate-emails-in-month-long-breach

[Source](https://www.bleepingcomputer.com/news/security/russian-hackers-stole-microsoft-corporate-emails-in-month-long-breach/){:target="_blank" rel="noopener"}

> Microsoft disclosed Friday night that some of its corporate email accounts were breached and data stolen by the Russian state-sponsored hacking group Midnight Blizzard. [...]
