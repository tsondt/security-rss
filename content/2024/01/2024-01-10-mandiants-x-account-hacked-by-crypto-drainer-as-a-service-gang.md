Title: Mandiant's X account hacked by crypto Drainer-as-a-Service gang
Date: 2024-01-10T17:21:44-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-01-10-mandiants-x-account-hacked-by-crypto-drainer-as-a-service-gang

[Source](https://www.bleepingcomputer.com/news/security/mandiants-x-account-hacked-by-crypto-drainer-as-a-service-gang/){:target="_blank" rel="noopener"}

> Cybersecurity firm and Google subsidiary Mandiant says its Twitter/X account was hijacked last week by a Drainer-as-a-Service (DaaS) gang in what it described as "likely a brute force password attack." [...]
