Title: Watch out for "I can't believe he is gone" Facebook phishing posts
Date: 2024-01-21T11:19:38-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-01-21-watch-out-for-i-cant-believe-he-is-gone-facebook-phishing-posts

[Source](https://www.bleepingcomputer.com/news/security/watch-out-for-i-cant-believe-he-is-gone-facebook-phishing-posts/){:target="_blank" rel="noopener"}

> A widespread Facebook phishing campaign stating, "I can't believe he is gone. I'm gonna miss him so much," leads unsuspecting users to a website that steals your Facebook credentials. [...]
