Title: New year, new updates for security holes in Windows, Adobe, Android and more
Date: 2024-01-09T22:26:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-09-new-year-new-updates-for-security-holes-in-windows-adobe-android-and-more

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/09/january_patch_tuesday/){:target="_blank" rel="noopener"}

> Nothing under exploit... The calm before the storm? Patch Tuesday Microsoft rang in the New Year with a relatively calm Patch Tuesday: Just 49 Windows security updates including fixes for two critical-rated bugs, plus four high-severity Chrome flaws in Microsoft Edge.... [...]
