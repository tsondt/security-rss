Title: Over 150k WordPress sites at takeover risk via vulnerable plugin
Date: 2024-01-11T16:54:43-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-11-over-150k-wordpress-sites-at-takeover-risk-via-vulnerable-plugin

[Source](https://www.bleepingcomputer.com/news/security/over-150k-wordpress-sites-at-takeover-risk-via-vulnerable-plugin/){:target="_blank" rel="noopener"}

> Two vulnerabilities impacting the POST SMTP Mailer WordPress plugin, an email delivery tool used by 300,000 websites, could help attackers take complete control of a site authentication. [...]
