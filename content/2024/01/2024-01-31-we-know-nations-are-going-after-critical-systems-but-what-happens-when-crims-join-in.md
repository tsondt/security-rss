Title: We know nations are going after critical systems, but what happens when crims join in?
Date: 2024-01-31T17:15:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-31-we-know-nations-are-going-after-critical-systems-but-what-happens-when-crims-join-in

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/31/critical_infrastructure_hacking/){:target="_blank" rel="noopener"}

> This isn't going to end well Volt Typhoon, the Chinese government-backed cyberspies whose infrastructure was at least partially disrupted by Uncle Sam, has been homing in on other US energy, satellite and telecommunications systems, according to Robert Lee, CEO of security shop Dragos.... [...]
