Title: Vast botnet hijacks smart TVs for prime-time cybercrime
Date: 2024-01-18T10:15:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-18-vast-botnet-hijacks-smart-tvs-for-prime-time-cybercrime

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/18/bigpanzi_botnet_smart_tvs/){:target="_blank" rel="noopener"}

> 8-year-old op responsible for DDoS attacks and commandeering broadcasts to push war material Security researchers have pinned a DDoS botnet that's infected potentially millions of smart TVs and set-top boxes to an eight-year-old cybercrime syndicate called Bigpanzi.... [...]
