Title: Microsoft kills off Windows app installation from the web, again
Date: 2024-01-04T00:02:44+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-01-04-microsoft-kills-off-windows-app-installation-from-the-web-again

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/04/microsoft_windows_app_installation/){:target="_blank" rel="noopener"}

> Unpleasant Christmas package lets malware down the chimney Microsoft has disabled a protocol that allowed the installation of Windows apps after finding that miscreants were abusing the mechanism to install malware.... [...]
