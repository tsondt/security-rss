Title: Ransomware victims targeted by fake hack-back offers
Date: 2024-01-09T16:09:01-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-09-ransomware-victims-targeted-by-fake-hack-back-offers

[Source](https://www.bleepingcomputer.com/news/security/ransomware-victims-targeted-by-fake-hack-back-offers/){:target="_blank" rel="noopener"}

> Some organizations victimized by the Royal and Akira ransomware gangs have been targeted by a threat actor posing as a security researcher who promised to hack back the original attacker and delete stolen victim data. [...]
