Title: China claims it cracked Apple's AirDrop to find numbers, email addresses
Date: 2024-01-09T16:46:01-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Government;Technology
Slug: 2024-01-09-china-claims-it-cracked-apples-airdrop-to-find-numbers-email-addresses

[Source](https://www.bleepingcomputer.com/news/security/china-claims-it-cracked-apples-airdrop-to-find-numbers-email-addresses/){:target="_blank" rel="noopener"}

> A Chinese state-backed research institute claims to have discovered how to decrypt device logs for Apple's AirDrop feature, allowing the government to identify phone numbers or email addresses of those who shared content. [...]
