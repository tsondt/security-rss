Title: Canadian Man Stuck in Triangle of E-Commerce Fraud
Date: 2024-01-19T15:34:53+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;The Coming Storm;Web Fraud 2.0;Adavio;Amazon.ca;Duncan's First Nation;RCMP;Royal Canadian Mounted Police;Timothy Barker;triangulation fraud;Walmart
Slug: 2024-01-19-canadian-man-stuck-in-triangle-of-e-commerce-fraud

[Source](https://krebsonsecurity.com/2024/01/canadian-man-stuck-in-triangle-of-e-commerce-fraud/){:target="_blank" rel="noopener"}

> A Canadian man who says he’s been falsely charged with orchestrating a complex e-commerce scam is seeking to clear his name. His case appears to involve “triangulation fraud,” which occurs when a consumer purchases something online — from a seller on Amazon or eBay, for example — but the seller doesn’t actually own the item for sale. Instead, the seller purchases the item from an online retailer using stolen payment card data. In this scam, the unwitting buyer pays the scammer and receives what they ordered, and very often the only party left to dispute the transaction is the owner [...]
