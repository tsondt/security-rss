Title: Vans, North Face owner says ransomware breach affects 35 million people
Date: 2024-01-19T09:35:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-19-vans-north-face-owner-says-ransomware-breach-affects-35-million-people

[Source](https://www.bleepingcomputer.com/news/security/vans-north-face-owner-says-ransomware-breach-affects-35-million-people/){:target="_blank" rel="noopener"}

> VF Corporation, the company behind brands like Vans, Timberland, The North Face, Dickies, and Supreme, said that more than 35 million customers had their personal information stolen in a December ransomware attack. [...]
