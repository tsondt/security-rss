Title: Here’s Some Bitcoin: Oh, and You’ve Been Served!
Date: 2024-01-10T13:39:37+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;SIM Swapping;The Coming Storm;Coinbase;Ethan Mora;Judge Helena M. March-Kuchta;mark rasch;Nick Bax;Ryan Dellone
Slug: 2024-01-10-heres-some-bitcoin-oh-and-youve-been-served

[Source](https://krebsonsecurity.com/2024/01/heres-some-bitcoin-oh-and-youve-been-served/){:target="_blank" rel="noopener"}

> A California man who lost $100,000 in a 2021 SIM-swapping attack is suing the unknown holder of a cryptocurrency wallet that harbors his stolen funds. The case is thought to be the first in which a federal court has recognized the use of information included in a bitcoin transaction — such as a link to a civil claim filed in federal court — as reasonably likely to provide notice of the lawsuit to the defendant. Experts say the development could make it easier for victims of crypto heists to recover stolen funds through the courts without having to wait years [...]
