Title: Fake 401K year-end statements used to steal corporate credentials
Date: 2024-01-10T13:33:36-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-10-fake-401k-year-end-statements-used-to-steal-corporate-credentials

[Source](https://www.bleepingcomputer.com/news/security/fake-401k-year-end-statements-used-to-steal-corporate-credentials/){:target="_blank" rel="noopener"}

> Threat actors are using communication about personal pension accounts (the 401(k) plans in the U.S.), salary adjustments, and performance reports to steal company employees' credentials. [...]
