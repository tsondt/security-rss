Title: Microsoft reveals how hackers breached its Exchange Online accounts
Date: 2024-01-26T10:23:52-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-26-microsoft-reveals-how-hackers-breached-its-exchange-online-accounts

[Source](https://www.bleepingcomputer.com/news/security/microsoft-reveals-how-hackers-breached-its-exchange-online-accounts/){:target="_blank" rel="noopener"}

> Microsoft confirmed that the Russian Foreign Intelligence Service hacking group, which hacked into its executives' email accounts in November 2023, also breached other organizations as part of this malicious campaign. [...]
