Title: Have I Been Pwned adds 71 million emails from Naz.API stolen account list
Date: 2024-01-17T17:06:05-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-01-17-have-i-been-pwned-adds-71-million-emails-from-nazapi-stolen-account-list

[Source](https://www.bleepingcomputer.com/news/security/have-i-been-pwned-adds-71-million-emails-from-nazapi-stolen-account-list/){:target="_blank" rel="noopener"}

> Have I Been Pwned has added almost 71 million email addresses associated with stolen accounts in the Naz.API dataset to its data breach notification service. [...]
