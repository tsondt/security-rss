Title: FTC secures first databroker settlement banning sale of sensitive location data
Date: 2024-01-15T15:34:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-01-15-ftc-secures-first-databroker-settlement-banning-sale-of-sensitive-location-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/15/infosec_in_brief/){:target="_blank" rel="noopener"}

> Also, iOS spyware abused Apple's own ECC, breach victim says it can't figure out what hackers took, and some critical vulns Infosec in brief The US Federal Trade Commission has secured its first data broker settlement agreement, prohibiting X-Mode Social from sharing or selling sensitive location data.... [...]
