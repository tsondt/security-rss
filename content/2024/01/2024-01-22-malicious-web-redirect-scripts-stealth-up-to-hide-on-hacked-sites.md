Title: Malicious web redirect scripts stealth up to hide on hacked sites
Date: 2024-01-22T15:15:10-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-22-malicious-web-redirect-scripts-stealth-up-to-hide-on-hacked-sites

[Source](https://www.bleepingcomputer.com/news/security/malicious-web-redirect-scripts-stealth-up-to-hide-on-hacked-sites/){:target="_blank" rel="noopener"}

> Security researchers looking at more than 10,000 scripts used by the Parrot traffic direction system (TDS) noticed an evolution marked by optimizations that make malicious code stealthier against security mechanisms. [...]
