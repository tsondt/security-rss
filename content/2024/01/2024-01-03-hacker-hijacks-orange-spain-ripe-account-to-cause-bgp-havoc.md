Title: Hacker hijacks Orange Spain RIPE account to cause BGP havoc
Date: 2024-01-03T14:44:22-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-01-03-hacker-hijacks-orange-spain-ripe-account-to-cause-bgp-havoc

[Source](https://www.bleepingcomputer.com/news/security/hacker-hijacks-orange-spain-ripe-account-to-cause-bgp-havoc/){:target="_blank" rel="noopener"}

> Orange Spain suffered an internet outage today after a hacker breached the company's RIPE account to misconfigure BGP routing and an RPKI configuration. [...]
