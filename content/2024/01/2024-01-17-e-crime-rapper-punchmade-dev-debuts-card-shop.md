Title: E-Crime Rapper ‘Punchmade Dev’ Debuts Card Shop
Date: 2024-01-17T17:00:40+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Web Fraud 2.0;Constella Intelligence;Devon Turner;DevTakeFlight;DevTakeFlightBeats Inc.;DomainTools.com;NBA 2K17;OBN Dev;OBN Group LLC;Punchmade Dev;Punchmade LLC
Slug: 2024-01-17-e-crime-rapper-punchmade-dev-debuts-card-shop

[Source](https://krebsonsecurity.com/2024/01/e-crime-rapper-punchmade-dev-debuts-card-shop/){:target="_blank" rel="noopener"}

> The rapper and social media personality Punchmade Dev is perhaps best known for his flashy videos singing the praises of a cybercrime lifestyle. With memorable hits such as “Internet Swiping” and “Million Dollar Criminal” earning millions of views, Punchmade has leveraged his considerable following to peddle tutorials on how to commit financial crimes online. But until recently, there wasn’t much to support a conclusion that Punchmade was actually doing the cybercrime things he promotes in his songs. Images from Punchmade Dev’s Twitter/X account show him displaying bags of cash and wearing a functional diamond-crusted payment card skimmer. Punchmade Dev’s most [...]
