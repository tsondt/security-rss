Title: IDC study: Customers cite 407% ROI with Chronicle Security Operations
Date: 2024-01-24T17:00:00+00:00
Author: Ahnna Schini
Category: GCP Security
Tags: Security & Identity
Slug: 2024-01-24-idc-study-customers-cite-407-roi-with-chronicle-security-operations

[Source](https://cloud.google.com/blog/products/identity-security/idc-study-customers-cite-407-percent-roi-with-chronicle-security-operations/){:target="_blank" rel="noopener"}

> How effective is security technology at keeping your organization safe? While quantifying the investment return on security technology can be tricky, understanding the ROI of security products is crucial information for organizations to make informed decisions about resource allocation, strategy adjustments, and to get buy-in from financial approvers. With this in mind, Google Cloud commissioned IDC to conduct an in-depth analysis on the business value of Google Chronicle Security Operations. Based on interviews with customers from around the world, IDC determined that Google Cloud’s SecOps platform delivers ROI of 407% over three years, with a payback period under 7 months. [...]
