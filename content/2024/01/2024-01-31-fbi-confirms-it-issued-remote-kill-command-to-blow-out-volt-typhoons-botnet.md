Title: FBI confirms it issued remote kill command to blow out Volt Typhoon's botnet
Date: 2024-01-31T19:24:51+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-31-fbi-confirms-it-issued-remote-kill-command-to-blow-out-volt-typhoons-botnet

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/31/volt_typhoon_botnet/){:target="_blank" rel="noopener"}

> Remotely disinfects Cisco and Netgear routers to block Chinese critters China's Volt Typhoon attackers used "hundreds" of outdated Cisco and NetGear routers infected with malware in an attempt to break into US critical infrastructure facilities, according to the Justice Department.... [...]
