Title: Global fintech firm EquiLend offline after recent cyberattack
Date: 2024-01-24T11:36:35-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-24-global-fintech-firm-equilend-offline-after-recent-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/global-fintech-firm-equilend-offline-after-recent-cyberattack/){:target="_blank" rel="noopener"}

> New York-based global financial technology firm EquiLend says its operations have been disrupted after some systems were taken offline in a Monday cyberattack. [...]
