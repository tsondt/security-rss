Title: Speaking to the CIA’s Creative Writing Group
Date: 2024-01-19T12:21:13+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;books;CIA
Slug: 2024-01-19-speaking-to-the-cias-creative-writing-group

[Source](https://www.schneier.com/blog/archives/2024/01/speaking-to-the-cias-creative-writing-group.html){:target="_blank" rel="noopener"}

> This is a fascinating story. Last spring, a friend of a friend visited my office and invited me to Langley to speak to Invisible Ink, the CIA’s creative writing group. I asked Vivian (not her real name) what she wanted me to talk about. She said that the topic of the talk was entirely up to me. I asked what level the writers in the group were. She said the group had writers of all levels. I asked what the speaking fee was. She said that as far as she knew, there was no speaking fee. What I want to [...]
