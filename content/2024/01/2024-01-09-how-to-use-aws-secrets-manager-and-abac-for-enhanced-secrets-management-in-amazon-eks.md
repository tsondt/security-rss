Title: How to use AWS Secrets Manager and ABAC for enhanced secrets management in Amazon EKS
Date: 2024-01-09T18:05:45+00:00
Author: Nima Fotouhi
Category: AWS Security
Tags: Amazon Elastic Kubernetes Service;AWS Secrets Manager;Containers;Intermediate (200);Security, Identity, & Compliance;Amazon EKS;Security;Security Blog
Slug: 2024-01-09-how-to-use-aws-secrets-manager-and-abac-for-enhanced-secrets-management-in-amazon-eks

[Source](https://aws.amazon.com/blogs/security/how-to-use-aws-secrets-manager-and-abac-for-enhanced-secrets-management-in-amazon-eks/){:target="_blank" rel="noopener"}

> In this post, we show you how to apply attribute-based access control (ABAC) while you store and manage your Amazon Elastic Kubernetes Services (Amazon EKS) workload secrets in AWS Secrets Manager, and then retrieve them by integrating Secrets Manager with Amazon EKS using External Secrets Operator to define more fine-grained and dynamic AWS Identity and [...]
