Title: Five ripped off IT giant with $7M+ in bogus work expenses, prosecutors claim
Date: 2024-01-19T21:21:23+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-19-five-ripped-off-it-giant-with-7m-in-bogus-work-expenses-prosecutors-claim

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/19/5_fake_expenses_claim/){:target="_blank" rel="noopener"}

> Account manager and pals blew it on hotels, cruise, fancy meals and more allegedly Five people have been accused of pulling off a "brazen" scam that involved submitting more than $7 million in fake work expense claims to an IT consultancy to bankroll hotel stays, a cruise, visits to strip clubs, and more.... [...]
