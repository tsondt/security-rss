Title: Ivanti releases patches for VPN zero-days, discloses two more high-severity vulns
Date: 2024-01-31T15:45:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-31-ivanti-releases-patches-for-vpn-zero-days-discloses-two-more-high-severity-vulns

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/31/ivanti_patches_zero_days/){:target="_blank" rel="noopener"}

> Many versions still without fixes while sophisticated attackers bypass mitigations Ivanti has finally released the first round of patches for vulnerability-stricken Connect Secure and Policy Secure gateways, but in doing so has also found two additional zero-days, one of which is under active exploitation.... [...]
