Title: X users fed up with constant stream of malicious crypto ads
Date: 2024-01-06T10:09:18-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-01-06-x-users-fed-up-with-constant-stream-of-malicious-crypto-ads

[Source](https://www.bleepingcomputer.com/news/security/x-users-fed-up-with-constant-stream-of-malicious-crypto-ads/){:target="_blank" rel="noopener"}

> Cybercriminals are abusing X advertisements to promote websites that lead to crypto drainers, fake airdrops, and other scams. [...]
