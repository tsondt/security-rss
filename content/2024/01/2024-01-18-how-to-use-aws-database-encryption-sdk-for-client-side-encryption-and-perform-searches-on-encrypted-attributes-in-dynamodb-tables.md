Title: How to use AWS Database Encryption SDK for client-side encryption and perform searches on encrypted attributes in DynamoDB tables
Date: 2024-01-18T21:36:01+00:00
Author: Samit Kumbhani
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;Amazon DynamoDB;AWS Encryption SDK;DynamoDB;DynamoDB table;Security;Security Blog
Slug: 2024-01-18-how-to-use-aws-database-encryption-sdk-for-client-side-encryption-and-perform-searches-on-encrypted-attributes-in-dynamodb-tables

[Source](https://aws.amazon.com/blogs/security/how-to-use-aws-database-encryption-sdk-for-client-side-encryption-and-perform-searches-on-encrypted-attributes-in-dynamodb-tables/){:target="_blank" rel="noopener"}

> Today’s applications collect a lot of data from customers. The data often includes personally identifiable information (PII), that must be protected in compliance with data privacy laws such as the General Data Protection Regulation (GDPR) and the California Consumer Privacy Act (CCPA). Modern business applications require fast and reliable access to customer data, and Amazon [...]
