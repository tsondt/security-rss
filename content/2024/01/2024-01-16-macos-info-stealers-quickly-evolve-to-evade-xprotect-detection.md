Title: MacOS info-stealers quickly evolve to evade XProtect detection
Date: 2024-01-16T16:29:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-16-macos-info-stealers-quickly-evolve-to-evade-xprotect-detection

[Source](https://www.bleepingcomputer.com/news/security/macos-info-stealers-quickly-evolve-to-evade-xprotect-detection/){:target="_blank" rel="noopener"}

> Multiple information stealers for the macOS platform have demonstrated the capability to evade detection even when security companies follow and report about new variants frequently. [...]
