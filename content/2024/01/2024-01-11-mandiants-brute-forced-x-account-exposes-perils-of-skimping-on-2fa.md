Title: Mandiant's brute-forced X account exposes perils of skimping on 2FA
Date: 2024-01-11T17:00:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-11-mandiants-brute-forced-x-account-exposes-perils-of-skimping-on-2fa

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/11/mandiant_x_account_brute_forced/){:target="_blank" rel="noopener"}

> Speculation builds over whether a nearly year-old policy change was to blame Google-owned security house Mandiant's investigation into how its X account was taken over to push cryptocurrency scams concludes the "likely" cause was a successful brute-force password attack.... [...]
