Title: Fortra warns of new critical GoAnywhere MFT auth bypass, patch now
Date: 2024-01-23T10:41:20-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-23-fortra-warns-of-new-critical-goanywhere-mft-auth-bypass-patch-now

[Source](https://www.bleepingcomputer.com/news/security/fortra-warns-of-new-critical-goanywhere-mft-auth-bypass-patch-now/){:target="_blank" rel="noopener"}

> Fortra is warning of a new authentication bypass vulnerability impacting GoAnywhere MFT (Managed File Transfer) versions before 7.4.1 that allows an attacker to create a new admin user. [...]
