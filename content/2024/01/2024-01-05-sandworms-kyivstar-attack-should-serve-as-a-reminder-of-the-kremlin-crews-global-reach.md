Title: Sandworm's Kyivstar attack should serve as a reminder of the Kremlin crew's 'global reach'
Date: 2024-01-05T07:30:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-05-sandworms-kyivstar-attack-should-serve-as-a-reminder-of-the-kremlin-crews-global-reach

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/05/sandworm_kyivstar_hack/){:target="_blank" rel="noopener"}

> 'Almost everything' wiped in the telecom attack, says Ukraine's top cyber spy Russia's Sandworm crew appear to have been responsible for knocking out mobile and internet services to about 24 million users in Ukraine last month with an attack on telco giant Kyivstar.... [...]
