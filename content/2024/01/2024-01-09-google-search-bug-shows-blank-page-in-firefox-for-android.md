Title: Google Search bug shows blank page in Firefox for Android
Date: 2024-01-09T09:48:59-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2024-01-09-google-search-bug-shows-blank-page-in-firefox-for-android

[Source](https://www.bleepingcomputer.com/news/security/google-search-bug-shows-blank-page-in-firefox-for-android/){:target="_blank" rel="noopener"}

> Users of the Firefox browser for Android have been reporting that they are seeing a blank page when trying to load the main Google Search site. [...]
