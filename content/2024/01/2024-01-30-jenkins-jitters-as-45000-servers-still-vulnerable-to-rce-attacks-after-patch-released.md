Title: Jenkins jitters as 45,000 servers still vulnerable to RCE attacks after patch released
Date: 2024-01-30T17:45:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-30-jenkins-jitters-as-45000-servers-still-vulnerable-to-rce-attacks-after-patch-released

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/30/jenkins_rce_flaw_patch/){:target="_blank" rel="noopener"}

> Multiple publicly available exploits have since been published for the critical flaw The number of public-facing installs of Jenkins servers vulnerable to a recently disclosed critical vulnerability is in the tens of thousands.... [...]
