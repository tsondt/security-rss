Title: Tietoevry ransomware attack causes outages for Swedish firms, cities
Date: 2024-01-21T15:13:25-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-01-21-tietoevry-ransomware-attack-causes-outages-for-swedish-firms-cities

[Source](https://www.bleepingcomputer.com/news/security/tietoevry-ransomware-attack-causes-outages-for-swedish-firms-cities/){:target="_blank" rel="noopener"}

> Finnish IT services and enterprise cloud hosting provider Tietoevry has suffered a ransomware attack impacting cloud hosting customers in one of its data centers in Sweden, with the attack reportedly conducted by the Akira ransomware gang. [...]
