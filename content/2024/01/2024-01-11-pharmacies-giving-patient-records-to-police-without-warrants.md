Title: Pharmacies Giving Patient Records to Police without Warrants
Date: 2024-01-11T12:09:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;data privacy;healthcare;law enforcement;medicine;police;privacy
Slug: 2024-01-11-pharmacies-giving-patient-records-to-police-without-warrants

[Source](https://www.schneier.com/blog/archives/2024/01/pharmacies-giving-patient-records-to-police-without-warrants.html){:target="_blank" rel="noopener"}

> Add pharmacies to the list of industries that are giving private data to the police without a warrant. [...]
