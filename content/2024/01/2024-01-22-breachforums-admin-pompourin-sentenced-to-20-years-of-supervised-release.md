Title: BreachForums admin 'Pompourin' sentenced to 20 years of supervised release
Date: 2024-01-22T02:29:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-01-22-breachforums-admin-pompourin-sentenced-to-20-years-of-supervised-release

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/22/infosec_news_roundup/){:target="_blank" rel="noopener"}

> Also: Another UEFI flaw found; Kaspersky discovers iOS log files actually work; and a few critical vulnerabilities Infosec in brief Conor Brian Fitzpatrick – aka "Pompourin," a former administrator of notorious leak site BreachForums – has been sentenced to 20 years of supervised release.... [...]
