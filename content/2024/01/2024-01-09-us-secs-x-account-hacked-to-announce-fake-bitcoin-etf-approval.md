Title: US SEC’s X account hacked to announce fake Bitcoin ETF approval
Date: 2024-01-09T17:30:23-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Government
Slug: 2024-01-09-us-secs-x-account-hacked-to-announce-fake-bitcoin-etf-approval

[Source](https://www.bleepingcomputer.com/news/security/us-secs-x-account-hacked-to-announce-fake-bitcoin-etf-approval/){:target="_blank" rel="noopener"}

> The X account for the U.S. Securities and Exchange Commission was hacked today to issue a fake announcement on the approval of Bitcoin ETFs on security exchanges. [...]
