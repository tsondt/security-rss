Title: BreachForums admin jailed again for using a VPN, unmonitored PC
Date: 2024-01-05T15:03:46-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-01-05-breachforums-admin-jailed-again-for-using-a-vpn-unmonitored-pc

[Source](https://www.bleepingcomputer.com/news/security/breachforums-admin-jailed-again-for-using-a-vpn-unmonitored-pc/){:target="_blank" rel="noopener"}

> The administrator behind the notorious BreachForums hacking forum has been arrested again for breaking pretrial release conditions, including using an unmonitored computer and a VPN. [...]
