Title: Zelle Is Using My Name and Voice without My Consent
Date: 2024-01-19T20:05:56+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;audio;marketing;Schneier news
Slug: 2024-01-19-zelle-is-using-my-name-and-voice-without-my-consent

[Source](https://www.schneier.com/blog/archives/2024/01/zelle-is-using-my-name-and-voice-without-my-consent.html){:target="_blank" rel="noopener"}

> Okay, so this is weird. Zelle has been using my name, and my voice, in audio podcast ads—without my permission. At least, I think it is without my permission. It’s possible that I gave some sort of blanket permission when speaking at an event. It’s not likely, but it is possible. I wrote to Zelle about it. Or, at least, I wrote to a company called Early Warning that owns Zelle about it. They asked me where the ads appeared. This seems odd to me. Podcast distribution networks drop ads in podcasts depending on the listener—like personalized ads on webpages—so [...]
