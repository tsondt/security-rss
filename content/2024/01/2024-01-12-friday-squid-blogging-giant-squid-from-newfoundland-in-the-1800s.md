Title: Friday Squid Blogging: Giant Squid from Newfoundland in the 1800s
Date: 2024-01-12T22:06:25+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;photos;squid
Slug: 2024-01-12-friday-squid-blogging-giant-squid-from-newfoundland-in-the-1800s

[Source](https://www.schneier.com/blog/archives/2024/01/friday-squid-blogging-giant-squid-from-newfoundland-in-the-1800s.html){:target="_blank" rel="noopener"}

> Interesting article, with photographs. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
