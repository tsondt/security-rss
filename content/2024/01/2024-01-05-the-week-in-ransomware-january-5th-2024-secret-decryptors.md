Title: The Week in Ransomware - January 5th 2024 - Secret decryptors
Date: 2024-01-05T17:16:57-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-01-05-the-week-in-ransomware-january-5th-2024-secret-decryptors

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-january-5th-2024-secret-decryptors/){:target="_blank" rel="noopener"}

> With it being the first week of the New Year and some still away on vacation, it has been slow with ransomware news, attacks, and new information. [...]
