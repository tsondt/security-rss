Title: Microsoft shares script to update Windows 10 WinRE with BitLocker fixes
Date: 2024-01-11T13:32:30-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-01-11-microsoft-shares-script-to-update-windows-10-winre-with-bitlocker-fixes

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-shares-script-to-update-windows-10-winre-with-bitlocker-fixes/){:target="_blank" rel="noopener"}

> Microsoft has released a PowerShell script to automate updating the Windows Recovery Environment (WinRE) partition in order to fix CVE-2024-20666, a vulnerability that allowed for BitLocker encryption bypass. [...]
