Title: US govt wants BreachForums admin sentenced to 15 years in prison
Date: 2024-01-18T11:08:46-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-01-18-us-govt-wants-breachforums-admin-sentenced-to-15-years-in-prison

[Source](https://www.bleepingcomputer.com/news/security/us-govt-wants-breachforums-admin-sentenced-to-15-years-in-prison/){:target="_blank" rel="noopener"}

> The United States government has recommended that Conor Brian Fitzpatrick, the creator and lead administrator of the now-defunct BreachForums hacking forums, receive a sentence of 15 years in prison. [...]
