Title: Latest Adblock update causes massive YouTube performance hit
Date: 2024-01-15T11:16:59-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2024-01-15-latest-adblock-update-causes-massive-youtube-performance-hit

[Source](https://www.bleepingcomputer.com/news/security/latest-adblock-update-causes-massive-youtube-performance-hit/){:target="_blank" rel="noopener"}

> Adblock and Adblock Plus users report performance issues on YouTube, initially blamed on Google but later determined to be an issue in the popular ad-blocking extension. [...]
