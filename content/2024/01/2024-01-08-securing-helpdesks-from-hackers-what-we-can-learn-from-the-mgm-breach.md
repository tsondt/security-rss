Title: Securing helpdesks from hackers: What we can learn from the MGM breach
Date: 2024-01-08T10:02:01-05:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2024-01-08-securing-helpdesks-from-hackers-what-we-can-learn-from-the-mgm-breach

[Source](https://www.bleepingcomputer.com/news/security/securing-helpdesks-from-hackers-what-we-can-learn-from-the-mgm-breach/){:target="_blank" rel="noopener"}

> In the wake of the MGM Resorts service desk hack, it's clear that organizations need to rethink their approach to securing their help desks. Learn more from Specops Software on how to prevent such incidents. [...]
