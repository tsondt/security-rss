Title: Web3 security firm CertiK's X account hacked to push crypto drainer
Date: 2024-01-05T12:20:43-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-01-05-web3-security-firm-certiks-x-account-hacked-to-push-crypto-drainer

[Source](https://www.bleepingcomputer.com/news/security/web3-security-firm-certiks-x-account-hacked-to-push-crypto-drainer/){:target="_blank" rel="noopener"}

> The Twitter/X account of blockchain security firm CertiK was hijacked today to redirect the company's more than 343,000 followers to a malicious website pushing a cryptocurrency wallet drainer. [...]
