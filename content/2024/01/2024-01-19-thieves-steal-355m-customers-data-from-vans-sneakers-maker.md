Title: Thieves steal 35.5M customers’ data from Vans sneakers maker
Date: 2024-01-19T13:56:52+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-19-thieves-steal-355m-customers-data-from-vans-sneakers-maker

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/19/vf_corp_ransomware_impact/){:target="_blank" rel="noopener"}

> But what kind of info was actually compromised? None of your business VF Corporation, parent company of clothes and footwear brands including Vans and North Face, says 35.5 million customers were impacted in some way when criminals broke into their systems in December.... [...]
