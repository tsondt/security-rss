Title: US shorts China's Volt Typhoon crew targeting America's criticals
Date: 2024-01-30T18:15:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-30-us-shorts-chinas-volt-typhoon-crew-targeting-americas-criticals

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/30/fbi_china_volt/){:target="_blank" rel="noopener"}

> Invaders inveigle infrastructure The US Justice Department and FBI may have scored a win over Chinese state-sponsored snoops trying to break into American critical infrastructure.... [...]
