Title: New Balada Injector campaign infects 6,700 WordPress sites
Date: 2024-01-11T12:44:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-11-new-balada-injector-campaign-infects-6700-wordpress-sites

[Source](https://www.bleepingcomputer.com/news/security/new-balada-injector-campaign-infects-6-700-wordpress-sites/){:target="_blank" rel="noopener"}

> A new Balada Injector campaign launched in mid-December has infected over 6,700 WordPress websites using a vulnerable version of the Popup Builder campaign. [...]
