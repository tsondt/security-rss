Title: Xerox says subsidiary XBS U.S. breached after ransomware gang leaks data
Date: 2024-01-02T12:29:40-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-02-xerox-says-subsidiary-xbs-us-breached-after-ransomware-gang-leaks-data

[Source](https://www.bleepingcomputer.com/news/security/xerox-says-subsidiary-xbs-us-breached-after-ransomware-gang-leaks-data/){:target="_blank" rel="noopener"}

> The U.S. division of Xerox Business Solutions (XBS) has been compromised by hackers, and a limited amount of personal information might have been exposed, according to an announcement by the parent company, Xerox Corporation. [...]
