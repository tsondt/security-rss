Title: Secret multimillion-dollar cryptojacker snared by Ukrainian police
Date: 2024-01-12T17:22:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-12-secret-multimillion-dollar-cryptojacker-snared-by-ukrainian-police

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/12/secret_multimilliondollar_cryptojacker_snared_by/){:target="_blank" rel="noopener"}

> Criminal scored $2M in crypto proceeds but ends up in ‘cuffs following property raid The criminal thought to be behind a multimillion-dollar cryptojacking scheme is in custody following a Europol-led investigation.... [...]
