Title: Ransomware payment ban: Wrong idea at the wrong time
Date: 2024-01-06T13:24:12+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-06-ransomware-payment-ban-wrong-idea-at-the-wrong-time

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/06/ransomware_payment_ban_wrong_idea/){:target="_blank" rel="noopener"}

> Won't stop the chaos, may lead to attacks with more dire consequences Opinion A general ban on ransomware payments, as was floated by some this week, sounds like a good idea. Eliminate extortion as a source of criminal income, and the attacks are undoubtedly going to drop.... [...]
