Title: Hackers can infect network-connected wrenches to install ransomware
Date: 2024-01-09T14:00:34+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;hack;ransomware
Slug: 2024-01-09-hackers-can-infect-network-connected-wrenches-to-install-ransomware

[Source](https://arstechnica.com/?p=1994532){:target="_blank" rel="noopener"}

> Enlarge / The Rexroth Nutrunner, a line of torque wrench sold by Bosch Rexroth. (credit: Bosch Rexroth) Researchers have unearthed nearly two dozen vulnerabilities that could allow hackers to sabotage or disable a popular line of network-connected wrenches that factories around the world use to assemble sensitive instruments and devices. The vulnerabilities, reported Tuesday by researchers from security firm Nozomi, reside in the Bosch Rexroth Handheld Nutrunner NXA015S-36V-B. The cordless device, which wirelessly connects to the local network of organizations that use it, allows engineers to tighten bolts and other mechanical fastenings to precise torque levels that are critical for [...]
