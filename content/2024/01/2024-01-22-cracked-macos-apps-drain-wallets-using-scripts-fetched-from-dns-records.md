Title: Cracked macOS apps drain wallets using scripts fetched from DNS records
Date: 2024-01-22T17:27:24-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple;CryptoCurrency
Slug: 2024-01-22-cracked-macos-apps-drain-wallets-using-scripts-fetched-from-dns-records

[Source](https://www.bleepingcomputer.com/news/security/cracked-macos-apps-drain-wallets-using-scripts-fetched-from-dns-records/){:target="_blank" rel="noopener"}

> Hackers are using a stealthy method to deliver to macOS users information-stealing malware through DNS records that hide malicious scripts. [...]
