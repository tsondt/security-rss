Title: Poisoning AI Models
Date: 2024-01-24T12:06:20+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence;LLM;machine learning;threat models
Slug: 2024-01-24-poisoning-ai-models

[Source](https://www.schneier.com/blog/archives/2024/01/poisoning-ai-models.html){:target="_blank" rel="noopener"}

> New research into poisoning AI models : The researchers first trained the AI models using supervised learning and then used additional “safety training” methods, including more supervised learning, reinforcement learning, and adversarial training. After this, they checked if the AI still had hidden behaviors. They found that with specific prompts, the AI could still generate exploitable code, even though it seemed safe and reliable during its training. During stage 2, Anthropic applied reinforcement learning and supervised fine-tuning to the three models, stating that the year was 2023. The result is that when the prompt indicated “2023,” the model wrote secure [...]
