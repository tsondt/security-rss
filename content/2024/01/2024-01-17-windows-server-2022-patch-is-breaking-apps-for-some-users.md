Title: Windows Server 2022 patch is breaking apps for some users
Date: 2024-01-17T11:45:13+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-01-17-windows-server-2022-patch-is-breaking-apps-for-some-users

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/17/windows_server_2022_patch_breaks/){:target="_blank" rel="noopener"}

> Uninstall the update or edit the Windows registry to restore order The latest Windows Server 2022 patch has broken the Chrome browser, and short of uninstalling the update, a registry hack is the only way to restore service for affected users.... [...]
