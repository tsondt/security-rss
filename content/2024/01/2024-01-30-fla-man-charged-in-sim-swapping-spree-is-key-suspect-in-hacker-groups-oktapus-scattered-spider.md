Title: Fla. Man Charged in SIM-Swapping Spree is Key Suspect in Hacker Groups Oktapus, Scattered Spider
Date: 2024-01-30T19:07:18+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Ne'er-Do-Well News;Web Fraud 2.0;0ktapus;Elijah;grails;Group-IB;King Bob;lastpass;lastpass breach;Mailchimp;Noah Michael Urban;Okta;Plex;Scattered Spider;signal;SIM swapping;Sosa;Twilio
Slug: 2024-01-30-fla-man-charged-in-sim-swapping-spree-is-key-suspect-in-hacker-groups-oktapus-scattered-spider

[Source](https://krebsonsecurity.com/2024/01/fla-man-charged-in-sim-swapping-spree-is-key-suspect-in-hacker-groups-oktapus-scattered-spider/){:target="_blank" rel="noopener"}

> On Jan. 9, 2024, U.S. authorities arrested a 19-year-old Florida man charged with wire fraud, aggravated identity theft, and conspiring with others to use SIM-swapping to steal cryptocurrency. Sources close to the investigation tell KrebsOnSecurity the accused was a key member of a criminal hacking group blamed for a string of cyber intrusions at major U.S. technology companies during the summer of 2022. A graphic depicting how 0ktapus leveraged one victim to attack another. Image credit: Amitai Cohen of Wiz. Prosecutors say Noah Michael Urban of Palm Coast, Fla., stole at least $800,000 from at least five victims between August [...]
