Title: Vastaamo hacker traced via ‘untraceable’ Monero transactions, police says
Date: 2024-01-30T14:44:52-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-01-30-vastaamo-hacker-traced-via-untraceable-monero-transactions-police-says

[Source](https://www.bleepingcomputer.com/news/security/vastaamo-hacker-traced-via-untraceable-monero-transactions-police-says/){:target="_blank" rel="noopener"}

> Julius Aleksanteri Kivimäki, the suspect believed to be behind an attack against one of Finland's largest psychotherapy clinics, Vastaamo, was allegedly identified by tracing what has been believed to be untraceable Monero transactions. [...]
