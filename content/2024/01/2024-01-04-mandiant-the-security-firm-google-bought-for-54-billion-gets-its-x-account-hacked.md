Title: Mandiant, the security firm Google bought for $5.4 billion, gets its X account hacked
Date: 2024-01-04T01:32:14+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;account hijack;google;Mandiant;Twitter
Slug: 2024-01-04-mandiant-the-security-firm-google-bought-for-54-billion-gets-its-x-account-hacked

[Source](https://arstechnica.com/?p=1993618){:target="_blank" rel="noopener"}

> Enlarge Google-owned security firm Mandiant spent several hours trying to regain control of its account on X (formerly known as Twitter) on Wednesday after an unknown scammer hijacked it and used it to spread a link that attempted to steal cryptocurrency from people who clicked on it. “We are aware of the incident impacting the Mandiant X account and are working to resolve the issue,” company officials wrote in a statement. “We've since regained control over the account and are currently working on restoring it.” The statement didn’t answer questions asking if the company had determined how the account was [...]
