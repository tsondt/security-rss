Title: Online museum collections down after cyberattack on service provider
Date: 2024-01-02T14:01:37-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-02-online-museum-collections-down-after-cyberattack-on-service-provider

[Source](https://www.bleepingcomputer.com/news/security/online-museum-collections-down-after-cyberattack-on-service-provider/){:target="_blank" rel="noopener"}

> Museum software solutions provider Gallery Systems has disclosed that its ongoing IT outages were caused by a ransomware attack last week. [...]
