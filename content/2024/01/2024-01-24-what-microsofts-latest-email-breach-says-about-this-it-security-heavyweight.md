Title: What Microsoft's latest email breach says about this IT security heavyweight
Date: 2024-01-24T11:02:25+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-24-what-microsofts-latest-email-breach-says-about-this-it-security-heavyweight

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/24/microsoft_latest_breach_cozy_bear/){:target="_blank" rel="noopener"}

> Senator Wyden tells The Reg this latest security lapse is 'inexcusable' Comment For most organizations – especially security vendors – disclosing a corporate email breach, in which executives' internal messages and attachments were stolen, would noticeably ding their stock prices.... [...]
