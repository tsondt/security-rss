Title: US agencies warn made-in-China drones might help Beijing snoop on the world
Date: 2024-01-19T02:45:40+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-01-19-us-agencies-warn-made-in-china-drones-might-help-beijing-snoop-on-the-world

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/19/drone_cisa_fbi/){:target="_blank" rel="noopener"}

> It’s a bird, it’s a plane... it’s a flying menace out to endanger national security Two US government agencies, the Cybersecurity and Infrastructure Security Agency (CISA) and Federal Bureau of Investigation (FBI), warned on Wednesday that drones made in China could be used to gather information on critical infrastructure.... [...]
