Title: CISA boss swatted: 'While my own experience was certainly harrowing, it was unfortunately not unique'
Date: 2024-01-23T18:30:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-23-cisa-boss-swatted-while-my-own-experience-was-certainly-harrowing-it-was-unfortunately-not-unique

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/23/cisa_easterly_swatted/){:target="_blank" rel="noopener"}

> Election officials, judges, politicians, and gamers are in swatters' crosshairs CISA Director Jen Easterly has confirmed she was the subject of a swatting attempt on December 30 after a bogus report of a shooting at her home.... [...]
