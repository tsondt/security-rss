Title: Framework discloses data breach after accountant gets phished
Date: 2024-01-11T17:01:30-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-11-framework-discloses-data-breach-after-accountant-gets-phished

[Source](https://www.bleepingcomputer.com/news/security/framework-discloses-data-breach-after-accountant-gets-phished/){:target="_blank" rel="noopener"}

> Framework Computer disclosed a data breach exposing the personal information of an undisclosed number of customers after Keating Consulting Group, its accounting service provider, fell victim to a phishing attack. [...]
