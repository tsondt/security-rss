Title: Haier hits Home Assistant plugin dev with takedown notice
Date: 2024-01-18T12:37:32-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-18-haier-hits-home-assistant-plugin-dev-with-takedown-notice

[Source](https://www.bleepingcomputer.com/news/security/haier-hits-home-assistant-plugin-dev-with-takedown-notice/){:target="_blank" rel="noopener"}

> Appliances giant Haier reportedly issued a takedown notice to a software developer for creating Home Assistant integration plugins for the company's home appliances and releasing them on GitHub. [...]
