Title: The Story of the Mirai Botnet
Date: 2024-01-16T12:21:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;botnets;history of security
Slug: 2024-01-16-the-story-of-the-mirai-botnet

[Source](https://www.schneier.com/blog/archives/2024/01/the-story-of-the-mirai-botnet.html){:target="_blank" rel="noopener"}

> Over at Wired, Andy Greenberg has an excellent story about the creators of the 2016 Mirai botnet. [...]
