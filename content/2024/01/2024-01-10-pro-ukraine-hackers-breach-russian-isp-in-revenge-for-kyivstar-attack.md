Title: Pro-Ukraine hackers breach Russian ISP in revenge for KyivStar attack
Date: 2024-01-10T14:43:52-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-10-pro-ukraine-hackers-breach-russian-isp-in-revenge-for-kyivstar-attack

[Source](https://www.bleepingcomputer.com/news/security/pro-ukraine-hackers-breach-russian-isp-in-revenge-for-kyivstar-attack/){:target="_blank" rel="noopener"}

> A pro-Ukraine hacktivist group named 'Blackjack' has claimed a cyberattack against Russian provider of internet services M9com as a direct response to the attack against Kyivstar mobile operator. [...]
