Title: US charged 19 suspects linked to xDedic cybercrime marketplace
Date: 2024-01-05T16:16:02-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-05-us-charged-19-suspects-linked-to-xdedic-cybercrime-marketplace

[Source](https://www.bleepingcomputer.com/news/security/us-charged-19-suspects-linked-to-xdedic-cybercrime-marketplace/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice announced the end of a transnational investigation into the dark web xDedic cybercrime marketplace, charging 19 suspects for their involvement in running and using the market's services. [...]
