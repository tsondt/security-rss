Title: Bitwarden adds passkey support to log into web password vaults
Date: 2024-01-11T14:21:12-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-11-bitwarden-adds-passkey-support-to-log-into-web-password-vaults

[Source](https://www.bleepingcomputer.com/news/security/bitwarden-adds-passkey-support-to-log-into-web-password-vaults/){:target="_blank" rel="noopener"}

> The open-source Bitwarden password manager has announced that all users can now log in to their web vaults using a passkey instead of the standard username and password pairs. [...]
