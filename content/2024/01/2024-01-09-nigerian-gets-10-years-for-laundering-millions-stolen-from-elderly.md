Title: Nigerian gets 10 years for laundering millions stolen from elderly
Date: 2024-01-09T18:31:02-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-09-nigerian-gets-10-years-for-laundering-millions-stolen-from-elderly

[Source](https://www.bleepingcomputer.com/news/security/nigerian-gets-10-years-for-laundering-millions-stolen-from-elderly/){:target="_blank" rel="noopener"}

> A Nigerian man was sentenced on Monday to 10 years and one month in prison for conspiring to launder millions stolen from elderly victims in internet fraud schemes. [...]
