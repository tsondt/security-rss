Title: Akira ransomware gang says it stole passport scans from Lush in 110 GB data heist
Date: 2024-01-26T12:25:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-26-akira-ransomware-gang-says-it-stole-passport-scans-from-lush-in-110-gb-data-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/26/akira_lush_ransomware/){:target="_blank" rel="noopener"}

> Cosmetics brand goes from Jackson Pollocking your bathwater to cleaning up serious a digital mess The Akira ransomware gang is claiming responsiblity for the "cybersecurity incident" at British bath bomb merchant.... [...]
