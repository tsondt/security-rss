Title: SEC confirms X account was hacked in SIM swapping attack
Date: 2024-01-22T18:04:22-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-01-22-sec-confirms-x-account-was-hacked-in-sim-swapping-attack

[Source](https://www.bleepingcomputer.com/news/security/sec-confirms-x-account-was-hacked-in-sim-swapping-attack/){:target="_blank" rel="noopener"}

> The U.S. Securities and Exchange Commission confirmed today that its X account was hacked through a SIM-swapping attack on the cell phone number associated with the account. [...]
