Title: In major gaffe, hacked Microsoft test account was assigned admin privileges
Date: 2024-01-27T00:29:15+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;microsoft;network breaches;system privileges
Slug: 2024-01-27-in-major-gaffe-hacked-microsoft-test-account-was-assigned-admin-privileges

[Source](https://arstechnica.com/?p=1999478){:target="_blank" rel="noopener"}

> Enlarge The hackers who recently broke into Microsoft’s network and monitored top executives’ email for two months did so by gaining access to an aging test account with administrative privileges, a major gaffe on the company's part, a researcher said. The new detail was provided in vaguely worded language included in a post Microsoft published on Thursday. It expanded on a disclosure Microsoft published late last Friday. Russia-state hackers, Microsoft said, used a technique known as password spraying to exploit a weak credential for logging in to a “legacy non-production test tenant account” that wasn’t protected by multifactor authentication. From [...]
