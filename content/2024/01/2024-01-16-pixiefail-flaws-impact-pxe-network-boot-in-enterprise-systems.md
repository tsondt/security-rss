Title: PixieFail flaws impact PXE network boot in enterprise systems
Date: 2024-01-16T12:19:19-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-16-pixiefail-flaws-impact-pxe-network-boot-in-enterprise-systems

[Source](https://www.bleepingcomputer.com/news/security/pixiefail-flaws-impact-pxe-network-boot-in-enterprise-systems/){:target="_blank" rel="noopener"}

> A set of nine vulnerabilities, collectively called 'PixieFail,' impact the IPv6 network protocol stack of Tianocore's EDK II, the open-source reference implementation of the UEFI specification widely used in enterprise computers and servers. [...]
