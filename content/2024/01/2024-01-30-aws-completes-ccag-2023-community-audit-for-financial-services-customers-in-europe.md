Title: AWS completes CCAG 2023 community audit for financial services customers in Europe
Date: 2024-01-30T05:02:41+00:00
Author: Manuel Mazarredo
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;audit;CCAG;Compliance;Security;Security Blog
Slug: 2024-01-30-aws-completes-ccag-2023-community-audit-for-financial-services-customers-in-europe

[Source](https://aws.amazon.com/blogs/security/aws-completes-ccag-2023-community-audit-for-financial-services-customers-in-europe/){:target="_blank" rel="noopener"}

> We’re excited to announce that Amazon Web Services (AWS) has completed its fifth annual Collaborative Cloud Audit Group (CCAG) pooled audit with European financial services institutions under regulatory supervision. At AWS, security is the highest priority. As customers embrace the scalability and flexibility of AWS, we’re helping them evolve security and compliance into key business [...]
