Title: Data regulator fines HelloFresh £140K for sending 80M+ spams
Date: 2024-01-12T11:27:15+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2024-01-12-data-regulator-fines-hellofresh-140k-for-sending-80m-spams

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/12/data_regulator_fines_hellofresh_140k/){:target="_blank" rel="noopener"}

> Messaging menace used text and email to bombard people Food delivery company HelloFresh is nursing a £140,000 ($178k) fine by Britain’s data privacy watchdog after a probe found it had dispatched upwards of a staggering 79 million spam email and one million texts in just seven months.... [...]
