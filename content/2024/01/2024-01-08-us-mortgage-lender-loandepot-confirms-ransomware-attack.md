Title: US mortgage lender loanDepot confirms ransomware attack
Date: 2024-01-08T12:39:58-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-08-us-mortgage-lender-loandepot-confirms-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/us-mortgage-lender-loandepot-confirms-ransomware-attack/){:target="_blank" rel="noopener"}

> ​Leading U.S. mortgage lender loanDepot confirmed today that a cyber incident disclosed over the weekend was a ransomware attack that led to data encryption. [...]
