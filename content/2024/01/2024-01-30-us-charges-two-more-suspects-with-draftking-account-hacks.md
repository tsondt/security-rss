Title: US charges two more suspects with DraftKing account hacks
Date: 2024-01-30T16:28:42-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-30-us-charges-two-more-suspects-with-draftking-account-hacks

[Source](https://www.bleepingcomputer.com/news/security/us-charges-two-more-suspects-with-draftking-account-hacks/){:target="_blank" rel="noopener"}

> ​The U.S. Department of Justice arrested and charged two more suspects for their involvement in the hacking of almost 68,000 DraftKings accounts in a November 2022 credential stuffing attack. [...]
