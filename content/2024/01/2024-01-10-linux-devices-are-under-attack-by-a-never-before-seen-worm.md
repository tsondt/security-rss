Title: Linux devices are under attack by a never-before-seen worm
Date: 2024-01-10T16:12:40+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;botnet;botnets;Internet of things;mirai
Slug: 2024-01-10-linux-devices-are-under-attack-by-a-never-before-seen-worm

[Source](https://arstechnica.com/?p=1994946){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) For the past year, previously unknown self-replicating malware has been compromising Linux devices around the world and installing cryptomining malware that takes unusual steps to conceal its inner workings, researchers said. The worm is a customized version of Mirai, the botnet malware that infects Linux-based servers, routers, web cameras, and other so-called Internet of Things devices. Mirai came to light in 2016 when it was used to deliver record-setting distributed denial-of-service attacks that paralyzed key parts of the Internet that year. The creators soon released the underlying source code, a move that allowed a wide array [...]
