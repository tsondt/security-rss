Title: Patch now: Critical VMware, Atlassian flaws found
Date: 2024-01-16T18:09:48+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-16-patch-now-critical-vmware-atlassian-flaws-found

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/16/patch_vmware_atlassian/){:target="_blank" rel="noopener"}

> You didn't have anything else to do this Tuesday, right? VMware and Atlassian today disclosed critical vulnerabilities and, while neither appear to have been exploited by miscreants yet, admins should patch now to avoid disappointment.... [...]
