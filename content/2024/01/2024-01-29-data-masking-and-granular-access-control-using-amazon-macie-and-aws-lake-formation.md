Title: Data masking and granular access control using Amazon Macie and AWS Lake Formation
Date: 2024-01-29T15:44:28+00:00
Author: Iris Ferreira
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;Amazon Macie;AWS Lake Formation;Security Blog
Slug: 2024-01-29-data-masking-and-granular-access-control-using-amazon-macie-and-aws-lake-formation

[Source](https://aws.amazon.com/blogs/security/data-masking-and-granular-access-control-using-amazon-macie-and-aws-lake-formation/){:target="_blank" rel="noopener"}

> Companies have been collecting user data to offer new products, recommend options more relevant to the user’s profile, or, in the case of financial institutions, to be able to facilitate access to higher credit lines or lower interest rates. However, personal data is sensitive as its use enables identification of the person using a specific [...]
