Title: Secure network operations for hybrid working
Date: 2024-01-12T14:42:14+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2024-01-12-secure-network-operations-for-hybrid-working

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/12/secure_network_operations_for_hybrid/){:target="_blank" rel="noopener"}

> How to have zero trust connectivity and optimize the remote user experience Webinar Remote working has rapidly become the norm for many organizations and isn't ever going away. But it still needs to be secure if it's to be a success.... [...]
