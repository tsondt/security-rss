Title: Slug slimes aerospace biz AerCap with ransomware, brags about 1TB theft
Date: 2024-01-22T20:45:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-22-slug-slimes-aerospace-biz-aercap-with-ransomware-brags-about-1tb-theft

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/22/ransomware_aercap_loandepot/){:target="_blank" rel="noopener"}

> Loanbase admits massive loss of customer data to thieves, too AerCap, the world's largest aircraft leasing company, has reported a ransomware infection that occurred earlier this month, but claims it hasn't yet suffered any financial losses yet and all its systems are under control.... [...]
