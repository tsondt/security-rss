Title: AMD, Apple, Qualcomm GPUs leak AI data in LeftoverLocals attacks
Date: 2024-01-17T10:32:06-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2024-01-17-amd-apple-qualcomm-gpus-leak-ai-data-in-leftoverlocals-attacks

[Source](https://www.bleepingcomputer.com/news/security/amd-apple-qualcomm-gpus-leak-ai-data-in-leftoverlocals-attacks/){:target="_blank" rel="noopener"}

> A new vulnerability dubbed 'LeftoverLocals' affecting graphics processing units from AMD, Apple, Qualcomm, and Imagination Technologies allows retrieving data from the local memory space. [...]
