Title: Voice Cloning with Very Short Samples
Date: 2024-01-15T12:09:20+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;cloning;voice recognition
Slug: 2024-01-15-voice-cloning-with-very-short-samples

[Source](https://www.schneier.com/blog/archives/2024/01/voice-cloning-with-very-short-samples.html){:target="_blank" rel="noopener"}

> New research demonstrates voice cloning, in multiple languages, using samples ranging from one to twelve seconds. Research paper. [...]
