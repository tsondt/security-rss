Title: Nokia walks the walk about its RAN to play on Uncle Sam’s China fears
Date: 2024-01-17T02:59:14+00:00
Author: Tobias Mann
Category: The Register
Tags: 
Slug: 2024-01-17-nokia-walks-the-walk-about-its-ran-to-play-on-uncle-sams-china-fears

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/17/nokia_us_sales_list/){:target="_blank" rel="noopener"}

> It pays not to be Huawei, and the US military can be lucrative, too Comment A vendor establishing a business unit dedicated to government sales is not new or unusual. But Finnish telecommunications giant Nokia’s decision to do so in the USA this week tells a bigger story about Washington’s paranoia regarding the security of critical communications infrastructure security.... [...]
