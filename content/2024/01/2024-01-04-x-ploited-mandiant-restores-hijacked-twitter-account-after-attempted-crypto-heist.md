Title: X-ploited: Mandiant restores hijacked Twitter account after attempted crypto heist
Date: 2024-01-04T20:00:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-04-x-ploited-mandiant-restores-hijacked-twitter-account-after-attempted-crypto-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/04/mandiant_restores_hijacked_x_account/){:target="_blank" rel="noopener"}

> Miscreants mock Google-owned security house: 'Change password please' Miscreants took over security giant Mandiant's Twitter account for several hours on Wednesday in an attempt to steal cryptocurrency, then trolled the Google-owned security shop, telling its admins to change the password.... [...]
