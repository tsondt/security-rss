Title: Researcher uncovers one of the biggest password dumps in recent history
Date: 2024-01-18T00:04:26+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;breaches;data stealers;passwords
Slug: 2024-01-18-researcher-uncovers-one-of-the-biggest-password-dumps-in-recent-history

[Source](https://arstechnica.com/?p=1996879){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Nearly 71 million unique credentials stolen for logging into websites such as Facebook, Roblox, eBay, and Yahoo have been circulating on the Internet for at least four months, a researcher said Wednesday. Troy Hunt, operator of the Have I Been Pwned? breach notification service, said the massive amount of data was posted to a well-known underground market that brokers sales of compromised credentials. Hunt said he often pays little attention to dumps like these because they simply compile and repackage previously published passwords taken in earlier campaigns. Post appearing on breach site advertising the availability of [...]
