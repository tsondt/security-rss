Title: FTC bans data broker from selling Americans’ location data
Date: 2024-01-09T15:32:28-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-09-ftc-bans-data-broker-from-selling-americans-location-data

[Source](https://www.bleepingcomputer.com/news/security/ftc-bans-data-broker-from-selling-americans-location-data/){:target="_blank" rel="noopener"}

> Today, the U.S. Federal Trade Commission (FTC) banned data broker Outlogic, formerly X-Mode Social, from selling Americans' raw location data that could be used for tracking purposes. [...]
