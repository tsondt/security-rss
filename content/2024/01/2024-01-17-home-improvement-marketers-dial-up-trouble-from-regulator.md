Title: Home improvement marketers dial up trouble from regulator
Date: 2024-01-17T09:30:07+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2024-01-17-home-improvement-marketers-dial-up-trouble-from-regulator

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/17/ico_cold_call_fines/){:target="_blank" rel="noopener"}

> ICO slaps penalties on two businesses that collectively made more than 3 million cold calls Another week and yet another couple of pesky cold callers face fines from the UK's data privacy watchdog for "bombarding" unsuspecting households with marketing messages about home improvements.... [...]
