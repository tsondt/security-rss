Title: Court charges dev with hacking after cybersecurity issue disclosure
Date: 2024-01-20T11:17:34-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-01-20-court-charges-dev-with-hacking-after-cybersecurity-issue-disclosure

[Source](https://www.bleepingcomputer.com/news/security/court-charges-dev-with-hacking-after-cybersecurity-issue-disclosure/){:target="_blank" rel="noopener"}

> A German court has charged a programmer investigating an IT problem with hacking and fined them €3,000 ($3,265) for what it deemed was unauthorized access to external computer systems and spying on data. [...]
