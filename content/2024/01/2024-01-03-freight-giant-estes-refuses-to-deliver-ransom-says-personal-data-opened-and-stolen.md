Title: Freight giant Estes refuses to deliver ransom, says personal data opened and stolen
Date: 2024-01-03T21:30:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-03-freight-giant-estes-refuses-to-deliver-ransom-says-personal-data-opened-and-stolen

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/03/estes_ransomware/){:target="_blank" rel="noopener"}

> Pay up, or just decline to submit One of America's biggest private freight shippers, Estes Express Lines, has told more than 20,000 customers that criminals stole their personal information.... [...]
