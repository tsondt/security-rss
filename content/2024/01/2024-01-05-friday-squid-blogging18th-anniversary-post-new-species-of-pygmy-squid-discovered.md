Title: Friday Squid Blogging—18th Anniversary Post: New Species of Pygmy Squid Discovered
Date: 2024-01-05T22:05:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-01-05-friday-squid-blogging18th-anniversary-post-new-species-of-pygmy-squid-discovered

[Source](https://www.schneier.com/blog/archives/2024/01/friday-squid-blogging-new-species-of-pygmy-squid-discovered.html){:target="_blank" rel="noopener"}

> They’re Ryukyuan pygmy squid ( Idiosepius kijimuna ) and Hannan’s pygmy squid ( Kodama jujutsu ). The second one represents an entire new genus. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. And, yes, this is the eighteenth anniversary of Friday Squid Blogging. The first squid post is from January 6, 2006, and I have been posting them weekly since then. Never did I believe there would be so much to write about squid—but the links never seem to end. Read my blog posting guidelines here. [...]
