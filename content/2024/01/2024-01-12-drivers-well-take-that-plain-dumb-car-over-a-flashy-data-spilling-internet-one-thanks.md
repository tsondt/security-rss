Title: Drivers: We'll take that plain dumb car over a flashy data-spilling internet one, thanks
Date: 2024-01-12T07:25:13+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-12-drivers-well-take-that-plain-dumb-car-over-a-flashy-data-spilling-internet-one-thanks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/12/smart_cars_data_privacy/){:target="_blank" rel="noopener"}

> Now that's a smart move CES Despite all the buzz around internet-connected smart cars at this year's CES in Las Vegas, most folks don't want vehicle manufacturers sharing their personal data with third parties – and even say they'd consider buying an older or dumber car to protect their privacy and security.... [...]
