Title: CISA warns agencies of fourth flaw used in Triangulation spyware attacks
Date: 2024-01-09T14:32:37-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-09-cisa-warns-agencies-of-fourth-flaw-used-in-triangulation-spyware-attacks

[Source](https://www.bleepingcomputer.com/news/security/cisa-warns-agencies-of-fourth-flaw-used-in-triangulation-spyware-attacks/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity and Infrastructure Security Agency has added to its to the Known Exploited Vulnerabilities catalog six vulnerabilities that impact products from Adobe, Apache, D-Link, and Joomla. [...]
