Title: Number of orgs compromised via Ivanti VPN zero-days grows as Mandiant weighs in
Date: 2024-01-13T02:20:15+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-13-number-of-orgs-compromised-via-ivanti-vpn-zero-days-grows-as-mandiant-weighs-in

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/13/ivanti_zeroday_mandiant_analysis/){:target="_blank" rel="noopener"}

> Snoops had no fewer than five custom bits of malware to hand to backdoor networks Two zero-day bugs in Ivanti products were likely under attack by cyberspies as early as December, according to Mandiant's threat intel team.... [...]
