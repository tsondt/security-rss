Title: Meet Ika & Sal: The Bulletproof Hosting Duo from Hell
Date: 2024-01-08T17:57:55+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;ad1@safe-mail.net;Alexander Valerievich Grichishkin;Andrey Skvortsov;Cherepovets;Constella Intelligence;duanesburg central school district;grichishkin@gmail.com;Icami$;Icamis;icamis@4host.info;Ika;JabberZeuS;nameservers.ru;rescator;Sal;Salomon;Spamdot;Spamit;tech@safe-mail.net
Slug: 2024-01-08-meet-ika-sal-the-bulletproof-hosting-duo-from-hell

[Source](https://krebsonsecurity.com/2024/01/meet-ika-sal-the-bulletproof-hosting-duo-from-hell/){:target="_blank" rel="noopener"}

> In 2020, the United States brought charges against four men accused of building a bulletproof hosting empire that once dominated the Russian cybercrime industry and supported multiple organized cybercrime groups. All four pleaded guilty to conspiracy and racketeering charges. But there is a fascinating and untold backstory behind the two Russian men involved, who co-ran the world’s top spam forum and worked closely with Russia’s most dangerous cybercriminals. From January 2005 to April 2013, there were two primary administrators of the cybercrime forum Spamdot (a.k.a Spamit), an invite-only community for Russian-speaking people in the businesses of sending spam and building [...]
