Title: Announcing general availability of Custom Org Policy to help tailor resource guardrails with confidence
Date: 2024-01-25T17:00:00+00:00
Author: Alok Jain
Category: GCP Security
Tags: Security & Identity
Slug: 2024-01-25-announcing-general-availability-of-custom-org-policy-to-help-tailor-resource-guardrails-with-confidence

[Source](https://cloud.google.com/blog/products/identity-security/announcing-custom-org-policy-to-help-tailor-resource-guardrails-with-confidence/){:target="_blank" rel="noopener"}

> Google Cloud’s Organization Policy Service can help you control resource configurations and establish guardrails in your cloud environment. And with custom organization policies, a powerful new extension to Organization (Org) Policy Service, you can now create granular resource policies to help address your cloud governance requirements. This new capability, now generally available, also comes with a dry-run mode that lets you safely roll out new policies without impacting your production environment. Org Policy Service can help you establish security guardrails that only allow compliant resource configurations in your cloud organization. With Org Policy Service, you could select from the library [...]
