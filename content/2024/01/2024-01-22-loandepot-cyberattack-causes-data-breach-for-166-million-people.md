Title: loanDepot cyberattack causes data breach for 16.6 million people
Date: 2024-01-22T10:59:24-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-22-loandepot-cyberattack-causes-data-breach-for-166-million-people

[Source](https://www.bleepingcomputer.com/news/security/loandepot-cyberattack-causes-data-breach-for-166-million-people/){:target="_blank" rel="noopener"}

> Mortgage lender loanDepot says that approximately 16.6 million people had their personal information stolen in a ransomware attack disclosed earlier this month. [...]
