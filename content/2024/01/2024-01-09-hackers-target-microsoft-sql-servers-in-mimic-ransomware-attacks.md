Title: Hackers target Microsoft SQL servers in Mimic ransomware attacks
Date: 2024-01-09T13:50:49-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-09-hackers-target-microsoft-sql-servers-in-mimic-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/hackers-target-microsoft-sql-servers-in-mimic-ransomware-attacks/){:target="_blank" rel="noopener"}

> A group of financially motivated Turkish hackers targets Microsoft SQL (MSSQL) servers worldwide to encrypt the victims' files with Mimic (N3ww4v3) ransomware. [...]
