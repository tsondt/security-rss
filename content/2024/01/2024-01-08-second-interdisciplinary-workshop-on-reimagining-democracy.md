Title: Second Interdisciplinary Workshop on Reimagining Democracy
Date: 2024-01-08T12:03:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;conferences;Schneier news;voting
Slug: 2024-01-08-second-interdisciplinary-workshop-on-reimagining-democracy

[Source](https://www.schneier.com/blog/archives/2024/01/second-interdisciplinary-workshop-on-reimagining-democracy.html){:target="_blank" rel="noopener"}

> Last month, I convened the Second Interdisciplinary Workshop on Reimagining Democracy ( IWORD 2023 ) at the Harvard Kennedy School Ash Center. As with IWORD 2022, the goal was to bring together a diverse set of thinkers and practitioners to talk about how democracy might be reimagined for the twenty-first century. My thinking is very broad here. Modern democracy was invented in the mid-eighteenth century, using mid-eighteenth-century technology. Were democracy to be invented from scratch today, with today’s technologies, it would look very different. Representation would look different. Adjudication would look different. Resource allocation and reallocation would look different. Everything [...]
