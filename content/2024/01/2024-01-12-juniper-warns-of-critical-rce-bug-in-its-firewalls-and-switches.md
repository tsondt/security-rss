Title: Juniper warns of critical RCE bug in its firewalls and switches
Date: 2024-01-12T12:36:05-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-12-juniper-warns-of-critical-rce-bug-in-its-firewalls-and-switches

[Source](https://www.bleepingcomputer.com/news/security/juniper-warns-of-critical-rce-bug-in-its-firewalls-and-switches/){:target="_blank" rel="noopener"}

> Juniper Networks has released security updates to fix a critical pre-auth remote code execution (RCE) vulnerability in its SRX Series firewalls and EX Series switches. [...]
