Title: And that's a wrap for Babuk Tortilla ransomware as free decryptor released
Date: 2024-01-09T13:18:16+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-09-and-thats-a-wrap-for-babuk-tortilla-ransomware-as-free-decryptor-released

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/09/babuk_tortilla_decryptor_arrests/){:target="_blank" rel="noopener"}

> Experts' job made 'straightforward' by crooks failing to update encryption schema after three years Security researchers have put out an updated decryptor for the Babuk ransomware family, providing a free solution for victims of the Tortilla variant.... [...]
