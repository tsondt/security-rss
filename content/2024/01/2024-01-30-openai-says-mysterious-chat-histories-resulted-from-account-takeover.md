Title: OpenAI says mysterious chat histories resulted from account takeover
Date: 2024-01-30T20:15:28+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: AI;Biz & IT;Security;ChatGPT;openai;privacy
Slug: 2024-01-30-openai-says-mysterious-chat-histories-resulted-from-account-takeover

[Source](https://arstechnica.com/?p=1999872){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) OpenAI officials say that the ChatGPT histories a user reported result from his ChatGPT account being compromised. The unauthorized logins came from Sri Lanka, an Open AI representative said. The user said he logs into his account from Brooklyn, New York. “From what we discovered, we consider it an account take over in that it’s consistent with activity we see where someone is contributing to a ‘pool’ of identities that an external community or proxy server uses to distribute free access,” the representative wrote. “The investigation observed that conversations were created recently from Sri Lanka. These [...]
