Title: Russian hackers wiped thousands of systems in KyivStar attack
Date: 2024-01-04T14:39:09-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-04-russian-hackers-wiped-thousands-of-systems-in-kyivstar-attack

[Source](https://www.bleepingcomputer.com/news/security/russian-hackers-wiped-thousands-of-systems-in-kyivstar-attack/){:target="_blank" rel="noopener"}

> The Russian hackers behind a December breach of Kyivstar, Ukraine's largest telecommunications service provider, have wiped all systems on the telecom operator's core network. [...]
