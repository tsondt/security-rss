Title: How Transfer Family can help you build a secure, compliant managed file transfer solution
Date: 2024-01-03T14:54:06+00:00
Author: John Jamail
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2024-01-03-how-transfer-family-can-help-you-build-a-secure-compliant-managed-file-transfer-solution

[Source](https://aws.amazon.com/blogs/security/how-transfer-family-can-help-you-build-a-secure-compliant-managed-file-transfer-solution/){:target="_blank" rel="noopener"}

> Building and maintaining a secure, compliant managed file transfer (MFT) solution to securely send and receive files inside and outside of your organization can be challenging. Working with a competent, vigilant, and diligent MFT vendor to help you protect the security of your file transfers can help you address this challenge. In this blog post, [...]
