Title: Fidelity National Financial: Hackers stole data of 1.3 million people
Date: 2024-01-10T15:32:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-10-fidelity-national-financial-hackers-stole-data-of-13-million-people

[Source](https://www.bleepingcomputer.com/news/security/fidelity-national-financial-hackers-stole-data-of-13-million-people/){:target="_blank" rel="noopener"}

> Fidelity National Financial (FNF) has confirmed that a November cyberattack (claimed by the BlackCat ransomware gang) has exposed the data of 1.3 million customers. [...]
