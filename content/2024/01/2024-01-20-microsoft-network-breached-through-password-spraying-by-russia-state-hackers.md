Title: Microsoft network breached through password-spraying by Russia-state hackers
Date: 2024-01-20T01:41:43+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;kremlin;microsoft;russian hacking
Slug: 2024-01-20-microsoft-network-breached-through-password-spraying-by-russia-state-hackers

[Source](https://arstechnica.com/?p=1997633){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Russia-state hackers exploited a weak password to compromise Microsoft’s corporate network and accessed emails and documents that belonged to senior executives and employees working in security and legal teams, Microsoft said late Friday. The attack, which Microsoft attributed to a Kremlin-backed hacking group it tracks as Midnight Blizzard, is at least the second time in as many years that failures to follow basic security hygiene have resulted in a breach that has the potential to harm customers. One paragraph in Friday’s disclosure, filed with the Securities and Exchange Commission, was gobsmacking: Beginning in late November 2023, [...]
