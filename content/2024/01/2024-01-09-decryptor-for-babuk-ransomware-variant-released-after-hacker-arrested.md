Title: Decryptor for Babuk ransomware variant released after hacker arrested
Date: 2024-01-09T11:46:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-09-decryptor-for-babuk-ransomware-variant-released-after-hacker-arrested

[Source](https://www.bleepingcomputer.com/news/security/decryptor-for-babuk-ransomware-variant-released-after-hacker-arrested/){:target="_blank" rel="noopener"}

> Researchers from Cisco Talos working with the Dutch police obtained a decryption tool for the Tortilla variant of Babuk ransomware and shared intelligence that led to the arrest of the ransomware's operator. [...]
