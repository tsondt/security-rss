Title: Building a security-first mindset: three key themes from AWS re:Invent 2023
Date: 2024-01-17T14:55:42+00:00
Author: Clarke Rodgers
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Thought Leadership;Amazon Detective;Amazon Inspector;Amazon Security Lake;AWS Config;AWS Wickr;Compliance;cryptography;Cybersecurity awareness;data privacy;Security;Security Blog
Slug: 2024-01-17-building-a-security-first-mindset-three-key-themes-from-aws-reinvent-2023

[Source](https://aws.amazon.com/blogs/security/building-a-security-first-mindset-three-key-themes-from-aws-reinvent-2023/){:target="_blank" rel="noopener"}

> AWS re:Invent drew 52,000 attendees from across the globe to Las Vegas, Nevada, November 27 to December 1, 2023. Now in its 12th year, the conference featured 5 keynotes, 17 innovation talks, and over 2,250 sessions and hands-on labs offering immersive learning and networking opportunities. Amazon CSO Stephen Schmidt With dozens of service and feature [...]
