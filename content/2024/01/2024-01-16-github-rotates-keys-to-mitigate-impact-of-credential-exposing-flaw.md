Title: GitHub rotates keys to mitigate impact of credential-exposing flaw
Date: 2024-01-16T17:19:42-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-16-github-rotates-keys-to-mitigate-impact-of-credential-exposing-flaw

[Source](https://www.bleepingcomputer.com/news/security/github-rotates-keys-to-mitigate-impact-of-credential-exposing-flaw/){:target="_blank" rel="noopener"}

> GitHub rotated keys potentially exposed by a vulnerability patched in December that could let attackers access credentials within production containers via environment variables. [...]
