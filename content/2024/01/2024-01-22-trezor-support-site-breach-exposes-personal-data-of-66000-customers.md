Title: Trezor support site breach exposes personal data of 66,000 customers
Date: 2024-01-22T09:16:44-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency;Gaming
Slug: 2024-01-22-trezor-support-site-breach-exposes-personal-data-of-66000-customers

[Source](https://www.bleepingcomputer.com/news/security/trezor-support-site-breach-exposes-personal-data-of-66-000-customers/){:target="_blank" rel="noopener"}

> Trezor issued an alert following a security breach on January 17, 2024, when unauthorized access was gained to their third-party support ticketing portal. [...]
