Title: The law enforcement operations targeting cybercrime in 2023
Date: 2024-01-01T11:05:10-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Technology
Slug: 2024-01-01-the-law-enforcement-operations-targeting-cybercrime-in-2023

[Source](https://www.bleepingcomputer.com/news/security/the-law-enforcement-operations-targeting-cybercrime-in-2023/){:target="_blank" rel="noopener"}

> In 2023, we saw numerous law enforcement operations targeting cybercrime operations, including cryptocurrency scams, phishing attacks, credential theft, malware development, and ransomware attacks. [...]
