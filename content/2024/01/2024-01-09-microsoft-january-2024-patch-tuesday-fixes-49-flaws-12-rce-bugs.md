Title: Microsoft January 2024 Patch Tuesday fixes 49 flaws, 12 RCE bugs
Date: 2024-01-09T14:05:17-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-01-09-microsoft-january-2024-patch-tuesday-fixes-49-flaws-12-rce-bugs

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-january-2024-patch-tuesday-fixes-49-flaws-12-rce-bugs/){:target="_blank" rel="noopener"}

> Today is Microsoft's January 2024 Patch Tuesday, which includes security updates for a total of 49 flaws and 12 remote code execution vulnerabilities. [...]
