Title: Cisco says critical Unity Connection bug lets attackers get root
Date: 2024-01-10T15:42:51-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-10-cisco-says-critical-unity-connection-bug-lets-attackers-get-root

[Source](https://www.bleepingcomputer.com/news/security/cisco-says-critical-unity-connection-bug-lets-attackers-get-root/){:target="_blank" rel="noopener"}

> Cisco has patched a critical Unity Connection security flaw that can let unauthenticated attackers remotely gain root privileges on unpatched devices. [...]
