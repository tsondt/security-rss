Title: Uncle Sam tells hospitals: Meet security standards or no federal dollars for you
Date: 2024-01-10T20:32:14+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-10-uncle-sam-tells-hospitals-meet-security-standards-or-no-federal-dollars-for-you

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/10/us_hospitals_security_rules/){:target="_blank" rel="noopener"}

> Expect new rules in upcoming weeks US hospitals will be required to meet basic cybersecurity standards before receiving federal funding, according to rules the White House is expected to propose in the next few weeks.... [...]
