Title: FTC bans one more data broker from selling your location info
Date: 2024-01-19T12:13:07-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-01-19-ftc-bans-one-more-data-broker-from-selling-your-location-info

[Source](https://www.bleepingcomputer.com/news/security/ftc-bans-one-more-data-broker-from-selling-your-location-info/){:target="_blank" rel="noopener"}

> The U.S. Federal Trade Commission (FTC) continues to target data brokers, this time in a settlement with InMarket Media, which bans the company from selling Americans' precise location data. [...]
