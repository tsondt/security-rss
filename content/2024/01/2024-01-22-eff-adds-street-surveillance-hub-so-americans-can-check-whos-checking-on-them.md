Title: EFF adds Street Surveillance Hub so Americans can check who's checking on them
Date: 2024-01-22T16:30:08+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-01-22-eff-adds-street-surveillance-hub-so-americans-can-check-whos-checking-on-them

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/22/eff_privacy_atlas/){:target="_blank" rel="noopener"}

> 'The federal government has almost entirely abdicated its responsibility' For a country that prides itself on being free, America does seem to have an awful lot of spying going on, as the new Street Surveillance Hub from the Electronic Frontier Foundation shows.... [...]
