Title: Researchers link 3AM ransomware to Conti, Royal cybercrime gangs
Date: 2024-01-20T10:09:18-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-01-20-researchers-link-3am-ransomware-to-conti-royal-cybercrime-gangs

[Source](https://www.bleepingcomputer.com/news/security/researchers-link-3am-ransomware-to-conti-royal-cybercrime-gangs/){:target="_blank" rel="noopener"}

> Security researchers analyzing the activity of the recently emerged 3AM ransomware operation uncovered close connections with infamous groups, such as the Conti syndicate and the Royal ransomware gang. [...]
