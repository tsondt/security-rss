Title: CISA pushes federal agencies to patch Citrix RCE within a week
Date: 2024-01-17T13:31:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-17-cisa-pushes-federal-agencies-to-patch-citrix-rce-within-a-week

[Source](https://www.bleepingcomputer.com/news/security/cisa-pushes-federal-agencies-to-patch-citrix-rce-within-a-week/){:target="_blank" rel="noopener"}

> Today, CISA ordered U.S. federal agencies to secure their systems against three recently patched Citrix NetScaler and Google Chrome zero-days actively exploited in attacks. [...]
