Title: eBay to cough up $3M after cyber-stalking couple who dared criticize the souk
Date: 2024-01-11T21:55:16+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-11-ebay-to-cough-up-3m-after-cyber-stalking-couple-who-dared-criticize-the-souk

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/11/ebay_3m_penalty_stalking/){:target="_blank" rel="noopener"}

> Staff sent live cockroaches, porno – and more – in harassment campaign to silence pair eBay will pay $3 million to settle criminal charges that its security team stalked and harassed a Massachusetts couple in retaliation for their website's critical coverage of the online tat bazaar.... [...]
