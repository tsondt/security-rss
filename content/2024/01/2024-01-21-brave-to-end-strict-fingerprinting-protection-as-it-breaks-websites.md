Title: Brave to end 'Strict' fingerprinting protection as it breaks websites
Date: 2024-01-21T10:19:38-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2024-01-21-brave-to-end-strict-fingerprinting-protection-as-it-breaks-websites

[Source](https://www.bleepingcomputer.com/news/security/brave-to-end-strict-fingerprinting-protection-as-it-breaks-websites/){:target="_blank" rel="noopener"}

> Brave Software has announced plans to deprecate the 'Strict' fingerprinting protection mode in its privacy-focused Brave Browser because it causes many sites to function incorrectly. [...]
