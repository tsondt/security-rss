Title: Fidelity National now says 1.3M customers had data stolen by cyber-crooks
Date: 2024-01-10T23:16:27+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-10-fidelity-national-now-says-13m-customers-had-data-stolen-by-cyber-crooks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/10/fidelity_data_disclosure/){:target="_blank" rel="noopener"}

> It's still not calling it ransomware Fidelity National Financial now says criminals got hold of data belonging to 1.3 million customers after breaking into its IT network in November.... [...]
