Title: Ransomware attacks hospitalizing security pros, as one admits suicidal feelings
Date: 2024-01-18T17:00:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-18-ransomware-attacks-hospitalizing-security-pros-as-one-admits-suicidal-feelings

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/18/ransomware_attacks_hospitalize_security_pros/){:target="_blank" rel="noopener"}

> Untold harms of holding the corporate perimeter revealed in extensive series of interviews Ransomware attacks are being linked to a litany of psychological and physical illnesses reported by infosec professionals, and in some cases blamed for hospitalizations.... [...]
