Title: CISA: Vendors must secure SOHO routers against Volt Typhoon attacks
Date: 2024-01-31T11:14:48-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-31-cisa-vendors-must-secure-soho-routers-against-volt-typhoon-attacks

[Source](https://www.bleepingcomputer.com/news/security/cisa-vendors-must-secure-soho-routers-against-volt-typhoon-attacks/){:target="_blank" rel="noopener"}

> CISA has urged manufacturers of small office/home office (SOHO) routers to ensure their devices' security against ongoing attacks attempting to hijack them, especially those coordinated by Chinese state-backed hacking group Volt Typhoon (Bronze Silhouette). [...]
