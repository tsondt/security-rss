Title: Formal ban on ransomware payments? Asking orgs nicely to not cough up ain't working
Date: 2024-01-03T08:30:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-03-formal-ban-on-ransomware-payments-asking-orgs-nicely-to-not-cough-up-aint-working

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/03/ban_ransomware_payments/){:target="_blank" rel="noopener"}

> With the average demand hitting $1.5 million, something's gotta change Emsisoft has called for a complete ban on ransom payments following another record-breaking year of digital extortion.... [...]
