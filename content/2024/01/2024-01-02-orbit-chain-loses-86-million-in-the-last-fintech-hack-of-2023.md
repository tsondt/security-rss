Title: Orbit Chain loses $86 million in the last fintech hack of 2023
Date: 2024-01-02T14:46:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-01-02-orbit-chain-loses-86-million-in-the-last-fintech-hack-of-2023

[Source](https://www.bleepingcomputer.com/news/security/orbit-chain-loses-86-million-in-the-last-fintech-hack-of-2023/){:target="_blank" rel="noopener"}

> Orbit Chain has experienced a security breach that has resulted in a loss of $86 million in cryptocurrency, particularly Ether, Dai, Tether, and USD Coin. [...]
