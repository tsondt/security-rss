Title: After injecting cancer hospital with ransomware, crims threaten to swat patients
Date: 2024-01-05T21:54:33+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-05-after-injecting-cancer-hospital-with-ransomware-crims-threaten-to-swat-patients

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/05/swatting_extorion_tactics/){:target="_blank" rel="noopener"}

> Remember the good old days when ransomware crooks vowed not to infect medical centers? Extortionists are now threatening to swat hospital patients — calling in bomb threats or other bogus reports to the police so heavily armed cops show up at victims' homes — if the medical centers don't pay the crooks' ransom demands.... [...]
