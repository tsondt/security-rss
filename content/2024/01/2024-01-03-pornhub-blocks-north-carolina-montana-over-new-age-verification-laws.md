Title: PornHub blocks North Carolina, Montana over new age verification laws
Date: 2024-01-03T12:55:05-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Legal;Technology
Slug: 2024-01-03-pornhub-blocks-north-carolina-montana-over-new-age-verification-laws

[Source](https://www.bleepingcomputer.com/news/security/pornhub-blocks-north-carolina-montana-over-new-age-verification-laws/){:target="_blank" rel="noopener"}

> Adult media giant Aylo has blocked access to many of its websites, including PornHub, to visitors from Montana and North Carolina as new age verifications laws go into effect. [...]
