Title: Thousands of Juniper Networks devices vulnerable to critical RCE bug
Date: 2024-01-15T19:34:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-15-thousands-of-juniper-networks-devices-vulnerable-to-critical-rce-bug

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/15/juniper_networks_rce_flaw/){:target="_blank" rel="noopener"}

> Yet more support for the argument to adopt memory-safe languages More than 11,500 Juniper Networks devices are exposed to a new remote code execution (RCE) vulnerability, and infosec researchers are pressing admins to urgently apply the patches.... [...]
