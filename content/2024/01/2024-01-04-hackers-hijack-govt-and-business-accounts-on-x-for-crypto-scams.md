Title: Hackers hijack govt and business accounts on X for crypto scams
Date: 2024-01-04T13:40:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-04-hackers-hijack-govt-and-business-accounts-on-x-for-crypto-scams

[Source](https://www.bleepingcomputer.com/news/security/hackers-hijack-govt-and-business-accounts-on-x-for-crypto-scams/){:target="_blank" rel="noopener"}

> Hackers are increasingly targeting verified accounts on X (formerly Twitter) belonging to government and business profiles and marked with 'gold' and 'grey' checkmarks to promote cryptocurrency scams, phishing sites, and sites with crypto drainers. [...]
