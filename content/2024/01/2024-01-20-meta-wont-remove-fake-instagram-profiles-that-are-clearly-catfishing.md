Title: Meta won't remove fake Instagram profiles that are clearly catfishing
Date: 2024-01-20T07:19:42-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2024-01-20-meta-wont-remove-fake-instagram-profiles-that-are-clearly-catfishing

[Source](https://www.bleepingcomputer.com/news/security/meta-wont-remove-fake-instagram-profiles-that-are-clearly-catfishing/){:target="_blank" rel="noopener"}

> Meta seems to be falling short of effectively tackling fake Instagram profiles even when there are sufficient signs to indicate that a profile is misusing someone else's photos and identity. [...]
