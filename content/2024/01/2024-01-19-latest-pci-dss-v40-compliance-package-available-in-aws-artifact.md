Title: Latest PCI DSS v4.0 compliance package available in AWS Artifact
Date: 2024-01-19T19:03:41+00:00
Author: Nivetha Chandran
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Compliance;Compliance reports;PCI;PCI DSS;Security Blog
Slug: 2024-01-19-latest-pci-dss-v40-compliance-package-available-in-aws-artifact

[Source](https://aws.amazon.com/blogs/security/latest-pci-dss-v4-0-compliance-package-available-in-aws-artifact/){:target="_blank" rel="noopener"}

> Amazon Web Services is pleased to announce that eight additional AWS services have been added to the scope of our Payment Card Industry Data Security Standard (PCI DSS) v4.0 certification: AWS AppFabric Amazon Bedrock AWS Clean Rooms AWS HealthImaging AWS IoT Device Defender AWS IoT TwinMaker AWS Resilience Hub AWS User Notifications Coalfire, a third-party [...]
