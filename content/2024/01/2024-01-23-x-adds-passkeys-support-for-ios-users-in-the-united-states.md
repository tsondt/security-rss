Title: X adds passkeys support for iOS users in the United States
Date: 2024-01-23T15:19:51-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-23-x-adds-passkeys-support-for-ios-users-in-the-united-states

[Source](https://www.bleepingcomputer.com/news/security/x-adds-passkeys-support-for-ios-users-in-the-united-states/){:target="_blank" rel="noopener"}

> X, formerly Twitter, announced today that iOS users in the United States can now log into their accounts using passkeys. [...]
