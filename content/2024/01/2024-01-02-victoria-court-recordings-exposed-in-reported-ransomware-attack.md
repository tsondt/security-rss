Title: Victoria court recordings exposed in reported ransomware attack
Date: 2024-01-02T10:47:45-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-01-02-victoria-court-recordings-exposed-in-reported-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/victoria-court-recordings-exposed-in-reported-ransomware-attack/){:target="_blank" rel="noopener"}

> Australia's Court Services Victoria (CSV) is warning that video recordings of court hearings were exposed after suffering a reported Qilin ransomware attack. [...]
