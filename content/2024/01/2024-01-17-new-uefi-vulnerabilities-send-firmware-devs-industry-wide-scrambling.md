Title: New UEFI vulnerabilities send firmware devs industry wide scrambling
Date: 2024-01-17T11:00:12+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;exploits;pixiefail;uefi;vulnerabilities
Slug: 2024-01-17-new-uefi-vulnerabilities-send-firmware-devs-industry-wide-scrambling

[Source](https://arstechnica.com/?p=1996543){:target="_blank" rel="noopener"}

> Enlarge (credit: Nadezhda Kozhedub) UEFI firmware from five of the leading suppliers contains vulnerabilities that allow attackers with a toehold in a user's network to infect connected devices with malware that runs at the firmware level. The vulnerabilities, which collectively have been dubbed PixieFail by the researchers who discovered them, pose a threat mostly to public and private data centers and possibly other enterprise settings. People with even minimal access to such a network—say a paying customer, a low-level employee, or an attacker who has already gained limited entry—can exploit the vulnerabilities to infect connected devices with a malicious UEFI. [...]
