Title: TeamViewer abused to breach networks in new ransomware attacks
Date: 2024-01-18T16:07:14-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-18-teamviewer-abused-to-breach-networks-in-new-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/teamviewer-abused-to-breach-networks-in-new-ransomware-attacks/){:target="_blank" rel="noopener"}

> Ransomware actors are again using TeamViewer to gain initial access to organization endpoints and attempt to deploy encryptors based on the leaked LockBit ransomware builder. [...]
