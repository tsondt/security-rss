Title: Copy that? Xerox confirms 'security incident' at subsidiary
Date: 2024-01-03T13:15:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-03-copy-that-xerox-confirms-security-incident-at-subsidiary

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/03/xerox_inks_confirmation_of_security/){:target="_blank" rel="noopener"}

> Company’s removal from ransomware gang’s leak blog could mean negotiations underway Xerox has officially confirmed that a cyber baddie broke into the systems of its US subsidiary - a week after INC Ransom claimed to have exfiltrated data from the copier and print giant.... [...]
