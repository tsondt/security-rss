Title: On IoT Devices and Software Liability
Date: 2024-01-12T12:03:48+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;cyberattack;data protection;Internet of Things;software liability
Slug: 2024-01-12-on-iot-devices-and-software-liability

[Source](https://www.schneier.com/blog/archives/2024/01/on-iot-devices-and-software-liability.html){:target="_blank" rel="noopener"}

> New law journal article : Smart Device Manufacturer Liability and Redress for Third-Party Cyberattack Victims Abstract: Smart devices are used to facilitate cyberattacks against both their users and third parties. While users are generally able to seek redress following a cyberattack via data protection legislation, there is no equivalent pathway available to third-party victims who suffer harm at the hands of a cyberattacker. Given how these cyberattacks are usually conducted by exploiting a publicly known and yet un-remediated bug in the smart device’s code, this lacuna is unreasonable. This paper scrutinises recent judgments from both the Supreme Court of the [...]
