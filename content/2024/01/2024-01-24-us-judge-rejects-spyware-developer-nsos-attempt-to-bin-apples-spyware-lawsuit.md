Title: US judge rejects spyware developer NSO's attempt to bin Apple's spyware lawsuit
Date: 2024-01-24T23:31:10+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-24-us-judge-rejects-spyware-developer-nsos-attempt-to-bin-apples-spyware-lawsuit

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/24/us_judge_rejects_pegasus_spyware/){:target="_blank" rel="noopener"}

> Judge says anti-hacking laws fits Pegasus case "to a T" A US court has rejected spyware vendor NSO Group's motion to dismiss a lawsuit filed by Apple that alleges the developer violated computer fraud and other laws by infecting customers' iDevices with its surveillance software.... [...]
