Title: Atlassian Confluence Server RCE attacks underway from 600+ IPs
Date: 2024-01-22T23:37:11+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-22-atlassian-confluence-server-rce-attacks-underway-from-600-ips

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/22/atlassian_confluence_server_rce/){:target="_blank" rel="noopener"}

> If you're still running a vulnerable instance then 'assume a breach' More than 600 IP addresses are launching thousands of exploit attempts against CVE-2023-22527 – a critical bug in out–of-date versions of Atlassian Confluence Data Center and Server – according to non-profit security org Shadowserver.... [...]
