Title: LastPass now requires 12-character master passwords for better security
Date: 2024-01-03T12:11:02-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-03-lastpass-now-requires-12-character-master-passwords-for-better-security

[Source](https://www.bleepingcomputer.com/news/security/lastpass-now-requires-12-character-master-passwords-for-better-security/){:target="_blank" rel="noopener"}

> LastPass notified customers today that they are now required to use complex master passwords with a minimum of 12 characters to increase their accounts' security. [...]
