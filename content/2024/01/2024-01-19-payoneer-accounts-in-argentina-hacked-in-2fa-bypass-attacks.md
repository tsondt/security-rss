Title: Payoneer accounts in Argentina hacked in 2FA bypass attacks
Date: 2024-01-19T15:28:20-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-19-payoneer-accounts-in-argentina-hacked-in-2fa-bypass-attacks

[Source](https://www.bleepingcomputer.com/news/security/payoneer-accounts-in-argentina-hacked-in-2fa-bypass-attacks/){:target="_blank" rel="noopener"}

> Numerous Payoneer users in Argentina report waking up to find that their 2FA-protected accounts were hacked and funds stolen after receiving SMS OTP codes while they were sleeping. [...]
