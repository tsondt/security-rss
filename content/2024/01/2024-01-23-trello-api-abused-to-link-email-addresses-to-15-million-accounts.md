Title: Trello API abused to link email addresses to 15 million accounts
Date: 2024-01-23T16:31:49-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-01-23-trello-api-abused-to-link-email-addresses-to-15-million-accounts

[Source](https://www.bleepingcomputer.com/news/security/trello-api-abused-to-link-email-addresses-to-15-million-accounts/){:target="_blank" rel="noopener"}

> An exposed Trello API allows linking private email addresses with Trello accounts, enabling the creation of millions of data profiles containing both public and private information. [...]
