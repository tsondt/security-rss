Title: Johnson Controls says ransomware attack cost $27 million, data stolen
Date: 2024-01-31T09:55:37-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-31-johnson-controls-says-ransomware-attack-cost-27-million-data-stolen

[Source](https://www.bleepingcomputer.com/news/security/johnson-controls-says-ransomware-attack-cost-27-million-data-stolen/){:target="_blank" rel="noopener"}

> Johnson Controls International has confirmed that a September 2023 ransomware attack cost the company $27 million in expenses and led to a data breach after hackers stole corporate data. [...]
