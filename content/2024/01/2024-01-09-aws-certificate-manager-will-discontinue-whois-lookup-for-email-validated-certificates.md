Title: AWS Certificate Manager will discontinue WHOIS lookup for email-validated certificates
Date: 2024-01-09T20:31:55+00:00
Author: Lerna Ekmekcioglu
Category: AWS Security
Tags: Announcements;AWS Certificate Manager;Foundational (100);Security, Identity, & Compliance;ACM;Security Blog;SSL;TLS;Transport Layer Security
Slug: 2024-01-09-aws-certificate-manager-will-discontinue-whois-lookup-for-email-validated-certificates

[Source](https://aws.amazon.com/blogs/security/aws-certificate-manager-will-discontinue-whois-lookup-for-email-validated-certificates/){:target="_blank" rel="noopener"}

> AWS Certificate Manager (ACM) is a managed service that you can use to provision, manage, and deploy public and private TLS certificates for use with Amazon Web Services (AWS) and your internal connected resources. Today, we’re announcing that ACM will be discontinuing the use of WHOIS lookup for validating domain ownership when you request email-validated [...]
