Title: Microsoft Executives Hacked
Date: 2024-01-29T12:03:42+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;disclosure;hacking;Microsoft;Russia
Slug: 2024-01-29-microsoft-executives-hacked

[Source](https://www.schneier.com/blog/archives/2024/01/microsoft-executives-hacked.html){:target="_blank" rel="noopener"}

> Microsoft is reporting that a Russian intelligence agency—the same one responsible for SolarWinds—accessed the email system of the company’s executives. Beginning in late November 2023, the threat actor used a password spray attack to compromise a legacy non-production test tenant account and gain a foothold, and then used the account’s permissions to access a very small percentage of Microsoft corporate email accounts, including members of our senior leadership team and employees in our cybersecurity, legal, and other functions, and exfiltrated some emails and attached documents. The investigation indicates they were initially targeting email accounts for information related to Midnight Blizzard [...]
