Title: iShutdown scripts can help detect iOS spyware on your iPhone
Date: 2024-01-17T13:03:47-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple
Slug: 2024-01-17-ishutdown-scripts-can-help-detect-ios-spyware-on-your-iphone

[Source](https://www.bleepingcomputer.com/news/security/ishutdown-scripts-can-help-detect-ios-spyware-on-your-iphone/){:target="_blank" rel="noopener"}

> Security researchers found that infections with high-profile spyware Pegasus, Reign, and Predator could be discovered on compromised Apple mobile devices by checking Shutdown.log, a system log file that stores reboot events. [...]
