Title: KyberSlash attacks put quantum encryption projects at risk
Date: 2024-01-07T10:05:10-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-07-kyberslash-attacks-put-quantum-encryption-projects-at-risk

[Source](https://www.bleepingcomputer.com/news/security/kyberslash-attacks-put-quantum-encryption-projects-at-risk/){:target="_blank" rel="noopener"}

> Multiple implementations of the Kyber key encapsulation mechanism for quantum-safe encryption, are vulnerable to a set of flaws collectively referred to as KyberSlash, which could allow the recovery of secret keys. [...]
