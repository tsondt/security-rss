Title: FTC offers $25,000 prize for detecting AI-enabled voice cloning
Date: 2024-01-04T09:30:36-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Artificial Intelligence
Slug: 2024-01-04-ftc-offers-25000-prize-for-detecting-ai-enabled-voice-cloning

[Source](https://www.bleepingcomputer.com/news/security/ftc-offers-25-000-prize-for-detecting-ai-enabled-voice-cloning/){:target="_blank" rel="noopener"}

> The U.S. Federal Trade Commission (FTC) has started accepting submissions for its Voice Cloning Challenge, a public competition with a $25,000 top prize for ideas that protect consumers from the danger of AI-enabled voice cloning for fraudulent activity. [...]
