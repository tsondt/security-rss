Title: Netgear, Hyundai latest X accounts hacked to push crypto drainers
Date: 2024-01-08T16:06:03-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-08-netgear-hyundai-latest-x-accounts-hacked-to-push-crypto-drainers

[Source](https://www.bleepingcomputer.com/news/security/netgear-hyundai-latest-x-accounts-hacked-to-push-crypto-drainers/){:target="_blank" rel="noopener"}

> The official Netgear and Hyundai MEA Twitter/X accounts (together with over 160,000 followers) are the latest hijacked to push scams designed to infect potential victims with cryptocurrency wallet drainer malware. [...]
