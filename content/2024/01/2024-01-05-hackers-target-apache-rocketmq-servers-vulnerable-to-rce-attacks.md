Title: Hackers target Apache RocketMQ servers vulnerable to RCE attacks
Date: 2024-01-05T12:32:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-05-hackers-target-apache-rocketmq-servers-vulnerable-to-rce-attacks

[Source](https://www.bleepingcomputer.com/news/security/hackers-target-apache-rocketmq-servers-vulnerable-to-rce-attacks/){:target="_blank" rel="noopener"}

> Security researchers are detecting hundreds of IP addresses on a daily basis that scan or attempt to exploit Apache RocketMQ services vulnerable to a remote command execution flaw identified as CVE-2023-33246 and CVE-2023-37582. [...]
