Title: AWS re:Invent 2023: Security, identity, and compliance recap
Date: 2024-01-26T14:43:17+00:00
Author: Nisha Amthul
Category: AWS Security
Tags: Announcements;AWS re:Invent;Events;Foundational (100);Security, Identity, & Compliance;Thought Leadership;Live Events;re:Invent 2023;Security Blog
Slug: 2024-01-26-aws-reinvent-2023-security-identity-and-compliance-recap

[Source](https://aws.amazon.com/blogs/security/aws-reinvent-2023-security-identity-and-compliance-recap/){:target="_blank" rel="noopener"}

> In this post, we share the key announcements related to security, identity, and compliance at AWS re:Invent 2023, and offer details on how you can learn more through on-demand video of sessions and relevant blog posts. AWS re:Invent returned to Las Vegas in November 2023. The conference featured over 2,250 sessions and hands-on labs, with [...]
