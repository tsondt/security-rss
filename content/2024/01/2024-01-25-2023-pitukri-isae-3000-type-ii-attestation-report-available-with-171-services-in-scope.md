Title: 2023 PiTuKri ISAE 3000 Type II attestation report available with 171 services in scope
Date: 2024-01-25T19:51:24+00:00
Author: Tariro Dongo
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS Compliance;AWS PiTuKri;AWS security;cybersecurity;Finland;Finnish Public Sector;PiTuKri Compliance;Security Blog
Slug: 2024-01-25-2023-pitukri-isae-3000-type-ii-attestation-report-available-with-171-services-in-scope

[Source](https://aws.amazon.com/blogs/security/2023-pitukri-isae-3000-type-ii-attestation-report-available-with-171-services-in-scope/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce the issuance of the Criteria to Assess the Information Security of Cloud Services (PiTuKri) International Standard on Assurance Engagements (ISAE) 3000 Type II attestation report. The scope of the report covers a total of 171 services and 29 global AWS Regions. The Finnish Transport and Communications Agency [...]
