Title: Steam drops support for Windows 7 and 8.1 to boost security
Date: 2024-01-02T15:39:55-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-01-02-steam-drops-support-for-windows-7-and-81-to-boost-security

[Source](https://www.bleepingcomputer.com/news/security/steam-drops-support-for-windows-7-and-81-to-boost-security/){:target="_blank" rel="noopener"}

> Steam is no longer supported on Windows 7, Windows 8, and Windows 8.1 as of January 1, with the company recommending users upgrade to a newer operating system. [...]
