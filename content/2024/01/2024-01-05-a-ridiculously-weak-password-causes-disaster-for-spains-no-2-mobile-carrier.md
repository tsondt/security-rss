Title: A “ridiculously weak“ password causes disaster for Spain’s No. 2 mobile carrier
Date: 2024-01-05T00:01:59+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;BGP;boder gateway protocol
Slug: 2024-01-05-a-ridiculously-weak-password-causes-disaster-for-spains-no-2-mobile-carrier

[Source](https://arstechnica.com/?p=1993801){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Orange España, Spain’s second-biggest mobile operator, suffered a major outage on Wednesday after an unknown party obtained a “ridiculously weak” password and used it to access an account for managing the global routing table that controls which networks deliver the company's Internet traffic, researchers said. The hijacking began around 9:28 Coordinated Universal Time (about 2:28 Pacific time) when the party logged into Orange’s RIPE NCC account using the password “ripeadmin” (minus the quotation marks). The RIPE Network Coordination Center is one of five Regional Internet Registries, which are responsible for managing and allocating IP addresses to [...]
