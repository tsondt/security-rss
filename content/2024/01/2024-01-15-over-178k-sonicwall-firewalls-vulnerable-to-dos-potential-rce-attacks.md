Title: Over 178K SonicWall firewalls vulnerable to DoS, potential RCE attacks
Date: 2024-01-15T13:28:20-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-15-over-178k-sonicwall-firewalls-vulnerable-to-dos-potential-rce-attacks

[Source](https://www.bleepingcomputer.com/news/security/over-178k-sonicwall-firewalls-vulnerable-to-dos-potential-rce-attacks/){:target="_blank" rel="noopener"}

> Security researchers have found over 178,000 SonicWall next-generation firewalls (NGFW) with the management interface exposed online are vulnerable to denial-of-service (DoS) and potential remote code execution (RCE) attacks. [...]
