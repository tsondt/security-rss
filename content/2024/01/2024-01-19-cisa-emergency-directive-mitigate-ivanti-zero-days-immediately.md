Title: CISA emergency directive: Mitigate Ivanti zero-days immediately
Date: 2024-01-19T14:25:11-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-19-cisa-emergency-directive-mitigate-ivanti-zero-days-immediately

[Source](https://www.bleepingcomputer.com/news/security/cisa-emergency-directive-mitigate-ivanti-zero-days-immediately/){:target="_blank" rel="noopener"}

> CISA issued this year's first emergency directive ordering Federal Civilian Executive Branch (FCEB) agencies to immediately mitigate two Ivanti Connect Secure and Ivanti Policy Secure zero-day flaws in response to widespread and active exploitation by multiple threat actors. [...]
