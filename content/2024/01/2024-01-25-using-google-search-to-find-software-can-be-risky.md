Title: Using Google Search to Find Software Can Be Risky
Date: 2024-01-25T18:38:43+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Web Fraud 2.0;domaintools;google;malvertising;Sentinel One;Tom Hegel
Slug: 2024-01-25-using-google-search-to-find-software-can-be-risky

[Source](https://krebsonsecurity.com/2024/01/using-google-search-to-find-software-can-be-risky/){:target="_blank" rel="noopener"}

> Google continues to struggle with cybercriminals running malicious ads on its search platform to trick people into downloading booby-trapped copies of popular free software applications. The malicious ads, which appear above organic search results and often precede links to legitimate sources of the same software, can make searching for software on Google a dicey affair. Google says keeping users safe is a top priority, and that the company has a team of thousands working around the clock to create and enforce their abuse policies. And by most accounts, the threat from bad ads leading to backdoored software has subsided significantly [...]
