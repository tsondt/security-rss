Title: Ivanti warns critical EPM bug lets hackers hijack enrolled devices
Date: 2024-01-04T16:37:54-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-04-ivanti-warns-critical-epm-bug-lets-hackers-hijack-enrolled-devices

[Source](https://www.bleepingcomputer.com/news/security/ivanti-warns-critical-epm-bug-lets-hackers-hijack-enrolled-devices/){:target="_blank" rel="noopener"}

> Ivanti fixed a critical remote code execution (RCE) vulnerability in its Endpoint Management software (EPM) that can let unauthenticated attackers hijack enrolled devices or the core server. [...]
