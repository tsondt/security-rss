Title: Microsoft sheds some light on Russian email heist – and how to learn from Redmond's mistakes
Date: 2024-01-27T00:32:44+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-27-microsoft-sheds-some-light-on-russian-email-heist-and-how-to-learn-from-redmonds-mistakes

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/27/microsoft_cozy_bear_mfa/){:target="_blank" rel="noopener"}

> Step one, actually turn on MFA Microsoft, a week after disclosing that Kremlin-backed spies broke into its network and stole internal emails and files from its executives and staff, has now confirmed the compromised corporate account used in the genesis of the heist didn't even have multi-factor authentication (MFA) enabled.... [...]
