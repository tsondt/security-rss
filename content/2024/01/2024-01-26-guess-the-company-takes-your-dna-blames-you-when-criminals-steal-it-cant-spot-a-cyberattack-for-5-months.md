Title: Guess the company: Takes your DNA, blames you when criminals steal it, can’t spot a cyberattack for 5 months
Date: 2024-01-26T16:00:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-26-guess-the-company-takes-your-dna-blames-you-when-criminals-steal-it-cant-spot-a-cyberattack-for-5-months

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/26/23_and_me_breach_filing/){:target="_blank" rel="noopener"}

> Breach filings show Reddit post led to the discovery rather than any sophisticated cyber defenses Biotech and DNA-collection biz 23andMe, the one that blamed its own customers for the October mega-breach, just admitted it failed to detect any malicious activity for the entire five months attackers were breaking into user accounts.... [...]
