Title: How to build a unified authorization layer for identity providers with Amazon Verified Permissions
Date: 2024-01-22T21:02:09+00:00
Author: Akash Kumar
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;Amazon API Gateway;Amazon Verified Permissions;AWS Lambda;Security;Security Blog
Slug: 2024-01-22-how-to-build-a-unified-authorization-layer-for-identity-providers-with-amazon-verified-permissions

[Source](https://aws.amazon.com/blogs/security/how-to-build-a-unified-authorization-layer-for-identity-providers-with-amazon-verified-permissions/){:target="_blank" rel="noopener"}

> Enterprises often have an identity provider (IdP) for their employees and another for their customers. Using multiple IdPs allows you to apply different access controls and policies for employees and for customers. However, managing multiple identity systems can be complex. A unified authorization layer can ease administration by centralizing access policies for APIs regardless of [...]
