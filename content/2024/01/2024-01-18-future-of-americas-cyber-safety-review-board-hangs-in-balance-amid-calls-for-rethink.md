Title: Future of America's Cyber Safety Review Board hangs in balance amid calls for rethink
Date: 2024-01-18T18:30:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-18-future-of-americas-cyber-safety-review-board-hangs-in-balance-amid-calls-for-rethink

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/18/cyber_safety_review_board_rethink/){:target="_blank" rel="noopener"}

> Politics-busting, uber-transparent incident reviews require independence, less internal conflict As the US mulls legislation that would see the Cyber Safety Review Board (CSRB) become a permanent fixture in the government's cyber defense armory, experts are calling for substantial changes in the way it's organized.... [...]
