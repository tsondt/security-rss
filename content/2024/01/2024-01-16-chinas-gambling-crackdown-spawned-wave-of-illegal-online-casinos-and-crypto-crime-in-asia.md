Title: China’s gambling crackdown spawned wave of illegal online casinos and crypto-crime in Asia
Date: 2024-01-16T03:30:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-01-16-chinas-gambling-crackdown-spawned-wave-of-illegal-online-casinos-and-crypto-crime-in-asia

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/16/un_asia_tech_crime_report/){:target="_blank" rel="noopener"}

> ‘Inaccessible and autonomous armed group territories’ host crooks who use tech to launder cash, run slave scam gangs, and more Global crime networks have set up shop in autonomous territories run by armed gangs across Southeast Asia, and are using them to host physical and online casinos that, in concert with crypto exchanges, have led to an explosion of money laundering, cyberfraud, and cybercrime across the region and beyond.... [...]
