Title: Nearly 4-year-old Cisco vuln linked to recent Akira ransomware attacks
Date: 2024-01-31T17:45:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-31-nearly-4-year-old-cisco-vuln-linked-to-recent-akira-ransomware-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/31/cisco_vuln_akira_attacks/){:target="_blank" rel="noopener"}

> Evidence mounts of an exploit gatekept within Russia's borders Security researchers believe the Akira ransomware group could be exploiting a nearly four-year-old Cisco vulnerability and using it as an entry point into organizations' systems.... [...]
