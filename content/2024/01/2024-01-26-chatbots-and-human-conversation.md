Title: Chatbots and Human Conversation
Date: 2024-01-26T12:09:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;chatbots;Internet and society;LLM;trust
Slug: 2024-01-26-chatbots-and-human-conversation

[Source](https://www.schneier.com/blog/archives/2024/01/chatbots-and-human-conversation.html){:target="_blank" rel="noopener"}

> For most of history, communicating with a computer has not been like communicating with a person. In their earliest years, computers required carefully constructed instructions, delivered through punch cards; then came a command-line interface, followed by menus and options and text boxes. If you wanted results, you needed to learn the computer’s language. This is beginning to change. Large language models—the technology undergirding modern chatbots—allow users to interact with computers through natural conversation, an innovation that introduces some baggage from human-to-human exchanges. Early on in our respective explorations of ChatGPT, the two of us found ourselves typing a word that [...]
