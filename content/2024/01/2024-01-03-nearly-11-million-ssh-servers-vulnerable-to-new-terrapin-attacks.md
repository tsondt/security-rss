Title: Nearly 11 million SSH servers vulnerable to new Terrapin attacks
Date: 2024-01-03T10:06:55-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-03-nearly-11-million-ssh-servers-vulnerable-to-new-terrapin-attacks

[Source](https://www.bleepingcomputer.com/news/security/nearly-11-million-ssh-servers-vulnerable-to-new-terrapin-attacks/){:target="_blank" rel="noopener"}

> Almost 11 million internet-exposed SSH servers are vulnerable to the Terrapin attack that threatens the integrity of some SSH connections. [...]
