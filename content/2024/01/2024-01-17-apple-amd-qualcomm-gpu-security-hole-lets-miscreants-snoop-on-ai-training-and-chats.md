Title: Apple, AMD, Qualcomm GPU security hole lets miscreants snoop on AI training and chats
Date: 2024-01-17T23:21:41+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-17-apple-amd-qualcomm-gpu-security-hole-lets-miscreants-snoop-on-ai-training-and-chats

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/17/leftoverlocals_gpu_flaw/){:target="_blank" rel="noopener"}

> So much for isolation A design flaw in GPU drivers made by Apple, Qualcomm, AMD, and likely Imagination can be exploited by miscreants on a shared system to snoop on fellow users.... [...]
