Title: Subway's data torpedoed by LockBit, ransomware gang claims
Date: 2024-01-22T14:00:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-22-subways-data-torpedoed-by-lockbit-ransomware-gang-claims

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/22/subways_data_toasted_by_lockbit/){:target="_blank" rel="noopener"}

> Fast food chain could face a footlong recovery process if allegations are true The LockBit ransomware gang is claiming an attack on submarine sandwich slinger Subway, alleging it has made off with a platter of data.... [...]
