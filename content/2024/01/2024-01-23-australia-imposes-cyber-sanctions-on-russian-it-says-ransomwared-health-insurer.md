Title: Australia imposes cyber sanctions on Russian it says ransomwared health insurer
Date: 2024-01-23T03:01:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-01-23-australia-imposes-cyber-sanctions-on-russian-it-says-ransomwared-health-insurer

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/23/australia_medibank_private_attacker_named/){:target="_blank" rel="noopener"}

> 'Aleksandr Ermakov' isn't allowed down under after being linked to ten-million-record leak Australia's government has used the "significant cyber incidents" sanctions regime it introduced in 2021 for the first time, against a Russian named Aleksandr Gennadievich Ermakov whom authorities have deemed responsible for the 2022 attack on health insurer Medibank Private.... [...]
