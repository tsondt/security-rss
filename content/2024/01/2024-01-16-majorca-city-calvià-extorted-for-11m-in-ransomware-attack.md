Title: Majorca city Calvià extorted for $11M in ransomware attack
Date: 2024-01-16T13:45:09-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-16-majorca-city-calvià-extorted-for-11m-in-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/majorca-city-calvi-extorted-for-11m-in-ransomware-attack/){:target="_blank" rel="noopener"}

> The Calvià City Council in Majorca announced it was targeted by a ransomware attack on Saturday, which impacted municipal services. [...]
