Title: Court hearings become ransomware concern after justice system breach
Date: 2024-01-02T16:15:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-02-court-hearings-become-ransomware-concern-after-justice-system-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/02/victoria_court_system_breach/){:target="_blank" rel="noopener"}

> From legal proceedings to potential YouTube fodder The court system of Victoria, Australia, was subject to a suspected ransomware attack in which audiovisual recordings of court hearings may have been accessed.... [...]
