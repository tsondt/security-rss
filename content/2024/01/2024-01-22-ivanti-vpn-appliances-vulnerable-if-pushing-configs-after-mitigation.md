Title: Ivanti: VPN appliances vulnerable if pushing configs after mitigation
Date: 2024-01-22T13:24:13-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-22-ivanti-vpn-appliances-vulnerable-if-pushing-configs-after-mitigation

[Source](https://www.bleepingcomputer.com/news/security/ivanti-vpn-appliances-vulnerable-if-pushing-configs-after-mitigation/){:target="_blank" rel="noopener"}

> Ivanti warned admins to stop pushing new device configurations to appliances after applying mitigations because this will leave them vulnerable to ongoing attacks exploiting two zero-day vulnerabilities. [...]
