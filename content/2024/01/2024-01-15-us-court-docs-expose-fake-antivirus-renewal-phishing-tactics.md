Title: US court docs expose fake antivirus renewal phishing tactics
Date: 2024-01-15T16:16:32-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-01-15-us-court-docs-expose-fake-antivirus-renewal-phishing-tactics

[Source](https://www.bleepingcomputer.com/news/security/us-court-docs-expose-fake-antivirus-renewal-phishing-tactics/){:target="_blank" rel="noopener"}

> In a seizure warrant application, the U.S. Secret Service sheds light on how threat actors stole $34,000 using fake antivirus renewal subscription emails. [...]
