Title: As lawmakers mull outlawing poor security, what can they really do to tackle online gangs?
Date: 2024-01-04T11:45:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-04-as-lawmakers-mull-outlawing-poor-security-what-can-they-really-do-to-tackle-online-gangs

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/04/feds_stole_the_ransomware_limelight/){:target="_blank" rel="noopener"}

> Headline-grabbing takedowns are nice, but long-term solutions require short-term sacrifices Comment In some ways, the ransomware landscape in 2023 remained unchanged from the way it looked in previous years. Vendor reports continue to show a rise in attacks, major organizations are still getting hit, and the inherent issues that enable it as a business model remain unaddressed.... [...]
