Title: EquiLend drags systems offline after admitting attacker broke in
Date: 2024-01-25T14:00:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-25-equilend-drags-systems-offline-after-admitting-attacker-broke-in

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/25/cybersecurity_incident_forces_equilend_to/){:target="_blank" rel="noopener"}

> Securities lender processes trillions of dollars worth of Wall Street transactions every day US securities lender EquiLend has pulled a number of its systems offline after a security "incident" in which an attacker gained "unauthorized access".... [...]
