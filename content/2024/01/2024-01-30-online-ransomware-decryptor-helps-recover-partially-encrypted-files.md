Title: Online ransomware decryptor helps recover partially encrypted files
Date: 2024-01-30T17:00:34-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-30-online-ransomware-decryptor-helps-recover-partially-encrypted-files

[Source](https://www.bleepingcomputer.com/news/security/online-ransomware-decryptor-helps-recover-partially-encrypted-files/){:target="_blank" rel="noopener"}

> CyberArk has created an online version of 'White Phoenix,' an open-source ransomware decryptor targeting operations using intermittent encryption. [...]
