Title: Cloud CISO Perspectives: How new SEC rules can help business leaders
Date: 2024-01-22T17:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2024-01-22-cloud-ciso-perspectives-how-new-sec-rules-can-help-business-leaders

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-how-new-SEC-rules-can-help-business-leaders/){:target="_blank" rel="noopener"}

> Welcome to the first Cloud CISO Perspectives of the year, and the first of our two newsletters for January. Today I’ll be discussing some of the important changes to the Cybersecurity Risk Management, Strategy, Governance, and Incident Disclosure rules as outlined by the U.S. Securities and Exchange Commission. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. aside_block <ListValue: [StructValue([('title', 'Board of Directors Insights Hub'), ('body', <wagtail.rich_text.RichText object at 0x3e41b5075400>), ('btn_text', 'Visit [...]
