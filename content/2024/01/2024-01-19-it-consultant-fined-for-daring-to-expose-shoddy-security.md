Title: IT consultant fined for daring to expose shoddy security
Date: 2024-01-19T06:44:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-01-19-it-consultant-fined-for-daring-to-expose-shoddy-security

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/19/germany_fine_security/){:target="_blank" rel="noopener"}

> Spotting a plaintext password and using it in research without authorization deemed a crime A security researcher in Germany has been fined €3,000 ($3,300, £2,600) for finding and reporting an e-commerce database vulnerability that was exposing almost 700,000 customer records.... [...]
