Title: Mortgage firm loanDepot cyberattack impacts IT systems, payment portal
Date: 2024-01-07T15:37:30-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-01-07-mortgage-firm-loandepot-cyberattack-impacts-it-systems-payment-portal

[Source](https://www.bleepingcomputer.com/news/security/mortgage-firm-loandepot-cyberattack-impacts-it-systems-payment-portal/){:target="_blank" rel="noopener"}

> U.S. mortgage lender loanDepot has suffered a cyberattack that caused the company to take IT systems offline, preventing online payments against loans. [...]
