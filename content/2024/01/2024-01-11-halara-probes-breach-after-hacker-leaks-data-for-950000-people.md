Title: Halara probes breach after hacker leaks data for 950,000 people
Date: 2024-01-11T15:28:30-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-01-11-halara-probes-breach-after-hacker-leaks-data-for-950000-people

[Source](https://www.bleepingcomputer.com/news/security/halara-probes-breach-after-hacker-leaks-data-for-950-000-people/){:target="_blank" rel="noopener"}

> Popular athleisure clothing brand Halara is investigating a data breach after the alleged data of almost 950,000 customers was leaked on a hacking forum. [...]
