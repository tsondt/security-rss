Title: How to customize access tokens in Amazon Cognito user pools
Date: 2024-01-11T20:46:59+00:00
Author: Edward Sun
Category: AWS Security
Tags: Amazon Cognito;Best Practices;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2024-01-11-how-to-customize-access-tokens-in-amazon-cognito-user-pools

[Source](https://aws.amazon.com/blogs/security/how-to-customize-access-tokens-in-amazon-cognito-user-pools/){:target="_blank" rel="noopener"}

> With Amazon Cognito, you can implement customer identity and access management (CIAM) into your web and mobile applications. You can add user authentication and access control to your applications in minutes. In this post, I introduce you to the new access token customization feature for Amazon Cognito user pools and show you how to use [...]
