Title: NSA Buying Bulk Surveillance Data on Americans without a Warrant
Date: 2024-01-30T12:12:30+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;data collection;data privacy;metadata;NSA;privacy;surveillance
Slug: 2024-01-30-nsa-buying-bulk-surveillance-data-on-americans-without-a-warrant

[Source](https://www.schneier.com/blog/archives/2024/01/nsa-buying-bulk-surveillance-data-on-americans-without-a-warrant.html){:target="_blank" rel="noopener"}

> It finally admitted to buying bulk data on Americans from data brokers, in response to a query by Senator Weyden. This is almost certainly illegal, although the NSA maintains that it is legal until it’s told otherwise. Some news articles. [...]
