Title: US, UK, Australia sanction REvil hacker behind Medibank data breach
Date: 2024-01-23T08:40:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-01-23-us-uk-australia-sanction-revil-hacker-behind-medibank-data-breach

[Source](https://www.bleepingcomputer.com/news/security/us-uk-australia-sanction-revil-hacker-behind-medibank-data-breach/){:target="_blank" rel="noopener"}

> The Australian government has announced sanctions for Aleksandr Gennadievich Ermakov, a Russian national considered responsible for the 2022 Medibank hack and a member of the REvil ransomware group. [...]
