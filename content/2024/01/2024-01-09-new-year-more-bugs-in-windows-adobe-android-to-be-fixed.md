Title: New year, more bugs in Windows, Adobe, Android to be fixed
Date: 2024-01-09T22:26:08+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-09-new-year-more-bugs-in-windows-adobe-android-to-be-fixed

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/09/january_patch_tuesday/){:target="_blank" rel="noopener"}

> Nothing under exploit... Is this the calm before the storm? Patch Tuesday Microsoft rang in the New Year with a relatively calm Patch Tuesday: Just 49 Windows security updates including fixes for two critical-rated bugs, plus four high-severity Chrome flaws in Microsoft Edge.... [...]
