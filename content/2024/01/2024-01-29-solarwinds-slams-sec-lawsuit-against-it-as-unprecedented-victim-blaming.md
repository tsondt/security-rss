Title: SolarWinds slams SEC lawsuit against it as 'unprecedented' victim blaming
Date: 2024-01-29T20:52:28+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-29-solarwinds-slams-sec-lawsuit-against-it-as-unprecedented-victim-blaming

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/29/solarwinds_sec_lawsuit/){:target="_blank" rel="noopener"}

> 18,000 customers, including the Pentagon and Microsoft, may have other thoughts SolarWinds – whose network monitoring software was backdoored by Russian spies so that the biz's customers could be spied upon – has accused America's financial watchdog of seeking to "revictimise the victim" after the agency sued it over the 2020 attack.... [...]
