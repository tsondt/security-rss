Title: Tesla hacked, 24 zero-days demoed at Pwn2Own Automotive 2024
Date: 2024-01-24T08:36:18-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-24-tesla-hacked-24-zero-days-demoed-at-pwn2own-automotive-2024

[Source](https://www.bleepingcomputer.com/news/security/tesla-hacked-24-zero-days-demoed-at-pwn2own-automotive-2024/){:target="_blank" rel="noopener"}

> Security researchers hacked a Tesla Modem and collected awards of $722,500 on the first day of Pwn2Own Automotive 2024 for three bug collisions and 24 unique zero-day exploits. [...]
