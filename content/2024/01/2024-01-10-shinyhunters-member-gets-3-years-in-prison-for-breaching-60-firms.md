Title: ShinyHunters member gets 3 years in prison for breaching 60 firms
Date: 2024-01-10T09:46:03-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-01-10-shinyhunters-member-gets-3-years-in-prison-for-breaching-60-firms

[Source](https://www.bleepingcomputer.com/news/security/shinyhunters-member-gets-3-years-in-prison-for-breaching-60-firms/){:target="_blank" rel="noopener"}

> The U.S. District Court in Seattle sentenced ShinyHunters member Sebastien Raoult to three years in prison and ordered a restitution of $5,000,000. [...]
