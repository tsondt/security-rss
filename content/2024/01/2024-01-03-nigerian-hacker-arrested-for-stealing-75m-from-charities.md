Title: Nigerian hacker arrested for stealing $7.5M from charities
Date: 2024-01-03T14:34:28-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-03-nigerian-hacker-arrested-for-stealing-75m-from-charities

[Source](https://www.bleepingcomputer.com/news/security/nigerian-hacker-arrested-for-stealing-75m-from-charities/){:target="_blank" rel="noopener"}

> A Nigerian national was arrested in Ghana and is facing charges related to business email compromise (BEC) attacks that caused a charitable organization in the United States to lose more than $7.5 million. [...]
