Title: Jason’s Deli says customer data exposed in credential stuffing attack
Date: 2024-01-23T11:44:05-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-23-jasons-deli-says-customer-data-exposed-in-credential-stuffing-attack

[Source](https://www.bleepingcomputer.com/news/security/jasons-deli-says-customer-data-exposed-in-credential-stuffing-attack/){:target="_blank" rel="noopener"}

> Jason's Deli is warning of a data breach in notifications sent to customers of its online platform stating that their personal data was exposed in credential stuffing attacks. [...]
