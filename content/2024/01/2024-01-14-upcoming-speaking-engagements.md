Title: Upcoming Speaking Engagements
Date: 2024-01-14T17:01:46+00:00
Author: B. Schneier
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2024-01-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2024/01/upcoming-speaking-engagements-33.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking at the International PolCampaigns Expo (IPE24) in Cape Town, South Africa, January 25-26, 2024. The list is maintained on this page. [...]
