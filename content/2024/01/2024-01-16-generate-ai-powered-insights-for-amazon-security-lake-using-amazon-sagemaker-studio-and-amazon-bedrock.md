Title: Generate AI powered insights for Amazon Security Lake using Amazon SageMaker Studio and Amazon Bedrock
Date: 2024-01-16T22:00:29+00:00
Author: Jonathan Nguyen
Category: AWS Security
Tags: Amazon Bedrock;Amazon SageMaker Studio;Amazon Security Lake;Customer Solutions;Expert (400);Generative AI;Security, Identity, & Compliance;Technical How-to;artificial intelligence;SageMaker;Security Blog
Slug: 2024-01-16-generate-ai-powered-insights-for-amazon-security-lake-using-amazon-sagemaker-studio-and-amazon-bedrock

[Source](https://aws.amazon.com/blogs/security/generate-ai-powered-insights-for-amazon-security-lake-using-amazon-sagemaker-studio-and-amazon-bedrock/){:target="_blank" rel="noopener"}

> In part 1, we discussed how to use Amazon SageMaker Studio to analyze time-series data in Amazon Security Lake to identify critical areas and prioritize efforts to help increase your security posture. Security Lake provides additional visibility into your environment by consolidating and normalizing security data from both AWS and non-AWS sources. Security teams can [...]
