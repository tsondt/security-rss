Title: Google Groups is ending support for Usenet to combat spam
Date: 2024-01-02T12:03:21-05:00
Author: Mayank Parmar
Category: BleepingComputer
Tags: Google;Security
Slug: 2024-01-02-google-groups-is-ending-support-for-usenet-to-combat-spam

[Source](https://www.bleepingcomputer.com/news/google/google-groups-is-ending-support-for-usenet-to-combat-spam/){:target="_blank" rel="noopener"}

> Google has officially announced it's ceasing support for Usenet groups on its Google Groups platform, a move partly attributed to the platform's increasing struggle with spam content. [...]
