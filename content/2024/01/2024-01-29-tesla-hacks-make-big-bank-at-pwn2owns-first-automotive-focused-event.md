Title: Tesla hacks make big bank at Pwn2Own's first automotive-focused event
Date: 2024-01-29T01:29:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-01-29-tesla-hacks-make-big-bank-at-pwn2owns-first-automotive-focused-event

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/29/infosec_news_roundup_in_brief/){:target="_blank" rel="noopener"}

> ALSO: SEC admits to X account negligence; New macOS malware family appears; and some critical vulns Infosec in brief Trend Micro's Zero Day Initiative (ZDI) held its first-ever automotive-focused Pwn2Own event in Tokyo last week, and awarded over $1.3 million to the discoverers of 49 vehicle-related zero day vulnerabilities.... [...]
