Title: UK water giant admits attackers broke into system as gang holds it to ransom
Date: 2024-01-23T11:48:50+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-23-uk-water-giant-admits-attackers-broke-into-system-as-gang-holds-it-to-ransom

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/23/southern_water_confirms_cyberattack/){:target="_blank" rel="noopener"}

> Comes mere months after Western intelligence agencies warned of attacks on water providers Southern Water confirmed this morning that criminals broke into its IT systems, making off with a "limited amount of data."... [...]
