Title: CFPB’s Proposed Data Rules
Date: 2024-01-31T12:04:33+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;banking;data privacy;privacy;regulation
Slug: 2024-01-31-cfpbs-proposed-data-rules

[Source](https://www.schneier.com/blog/archives/2024/01/cfpbs-proposed-data-rules.html){:target="_blank" rel="noopener"}

> In October, the Consumer Financial Protection Bureau (CFPB) proposed a set of rules that if implemented would transform how financial institutions handle personal data about their customers. The rules put control of that data back in the hands of ordinary Americans, while at the same time undermining the data broker economy and increasing customer choice and competition. Beyond these economic effects, the rules have important data security benefits. The CFPB’s rules align with a key security idea: the decoupling principle. By separating which companies see what parts of our data, and in what contexts, we can gain control over data [...]
