Title: Kansas State University cyberattack disrupts IT network and services
Date: 2024-01-18T13:44:47-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Education
Slug: 2024-01-18-kansas-state-university-cyberattack-disrupts-it-network-and-services

[Source](https://www.bleepingcomputer.com/news/security/kansas-state-university-cyberattack-disrupts-it-network-and-services/){:target="_blank" rel="noopener"}

> Kansas State University (K-State) announced it is managing a cybersecurity incident that has disrupted certain network systems, including VPN, K-State Today emails, and video services on Canvas and Mediasite. [...]
