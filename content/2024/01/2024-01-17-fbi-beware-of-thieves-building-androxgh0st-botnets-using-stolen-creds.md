Title: FBI: Beware of thieves building Androxgh0st botnets using stolen creds
Date: 2024-01-17T01:29:09+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-17-fbi-beware-of-thieves-building-androxgh0st-botnets-using-stolen-creds

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/17/fbi_botnet_warning/){:target="_blank" rel="noopener"}

> Infecting networks via years-old CVEs that should have been patched by now Crooks are exploiting years-old vulnerabilities to deploy Androxgh0st malware and build a cloud-credential stealing botnet, according to the FBI and the Cybersecurity and Infrastructure Security Agency (CISA).... [...]
