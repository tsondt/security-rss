Title: Facial Scanning by Burger King in Brazil
Date: 2024-01-10T12:05:55+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;biometrics;Brazil;face recognition;marketing
Slug: 2024-01-10-facial-scanning-by-burger-king-in-brazil

[Source](https://www.schneier.com/blog/archives/2024/01/facial-scanning-by-burger-king-in-brazil.html){:target="_blank" rel="noopener"}

> In 2000, I wrote : “If McDonald’s offered three free Big Macs for a DNA sample, there would be lines around the block.” Burger King in Brazil is almost there, offering discounts in exchange for a facial scan. From a marketing video: “At the end of the year, it’s Friday every day, and the hangover kicks in,” a vaguely robotic voice says as images of cheeseburgers glitch in and out over fake computer code. “BK presents Hangover Whopper, a technology that scans your hangover level and offers a discount on the ideal combo to help combat it.” The stunt runs [...]
