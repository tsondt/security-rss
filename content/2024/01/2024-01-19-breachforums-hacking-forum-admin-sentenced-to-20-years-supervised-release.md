Title: BreachForums hacking forum admin sentenced to 20 years supervised release
Date: 2024-01-19T18:20:04-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-01-19-breachforums-hacking-forum-admin-sentenced-to-20-years-supervised-release

[Source](https://www.bleepingcomputer.com/news/security/breachforums-hacking-forum-admin-sentenced-to-20-years-supervised-release/){:target="_blank" rel="noopener"}

> Conor Brian Fitzpatrick was sentenced to 20 years of supervised release today in the Eastern District of Virginia for operating the notorious BreachForums hacking forum, known for the sale and leaking of personal data for hundreds of millions of people worldwide. [...]
