Title: Ukraine: Hack wiped 2 petabytes of data from Russian research center
Date: 2024-01-26T11:59:06-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-26-ukraine-hack-wiped-2-petabytes-of-data-from-russian-research-center

[Source](https://www.bleepingcomputer.com/news/security/ukraine-hack-wiped-2-petabytes-of-data-from-russian-research-center/){:target="_blank" rel="noopener"}

> The Main Intelligence Directorate of Ukraine's Ministry of Defense claims that pro-Ukrainian hacktivists breached the Russian Center for Space Hydrometeorology, aka "planeta" (планета), and wiped 2 petabytes of data. [...]
