Title: Twilio will ditch its Authy desktop 2FA app in August, goes mobile only
Date: 2024-01-08T13:07:47-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2024-01-08-twilio-will-ditch-its-authy-desktop-2fa-app-in-august-goes-mobile-only

[Source](https://www.bleepingcomputer.com/news/security/twilio-will-ditch-its-authy-desktop-2fa-app-in-august-goes-mobile-only/){:target="_blank" rel="noopener"}

> The Authy desktop apps for Windows, macOS, and Linux will be discontinued in August 2024, with the company recommending users switch to a mobile version of the two-factor authentication (2FA) app. [...]
