Title: What's worse than paying an extortion bot that auto-pwned your database?
Date: 2024-01-17T15:00:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-17-whats-worse-than-paying-an-extortion-bot-that-auto-pwned-your-database

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/17/extortion_bot_is_autopwning_postgresql/){:target="_blank" rel="noopener"}

> Paying one that lied to you and only saved the first 20 rows of each table Publicly exposed PostgreSQL and MySQL databases with weak passwords are being autonomously wiped out by a malicious extortion bot – one that marks who pays up and who is not getting their data back.... [...]
