Title: British Library: Finances remain healthy as ransomware recovery continues
Date: 2024-01-08T13:15:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-08-british-library-finances-remain-healthy-as-ransomware-recovery-continues

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/08/british_library_finances_remain_healthy/){:target="_blank" rel="noopener"}

> Authors continue to lose out on owed payments as rebuild of digital services drags on The British Library is denying reports suggesting the recovery costs for its 2023 ransomware attack may reach highs of nearly $9 million as work to restore services remains ongoing.... [...]
