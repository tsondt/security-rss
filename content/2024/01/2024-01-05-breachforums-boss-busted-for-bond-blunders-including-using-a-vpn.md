Title: BreachForums boss busted for bond blunders – including using a VPN
Date: 2024-01-05T14:35:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-05-breachforums-boss-busted-for-bond-blunders-including-using-a-vpn

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/05/breachforums_admin_arrested_again/){:target="_blank" rel="noopener"}

> Fitzpatrick faces potentially decades in prison later this month, so may as well get some foreign Netflix in beforehand The cybercriminal behind BreachForums was this week arrested for violating the terms of his pretrial release and will now be held in custody until his sentencing hearing.... [...]
