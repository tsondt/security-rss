Title: More than 178,000 SonicWall firewalls are exposed to old denial of service bugs
Date: 2024-01-16T17:02:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-16-more-than-178000-sonicwall-firewalls-are-exposed-to-old-denial-of-service-bugs

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/16/more_than_178000_sonicwall_firewalls/){:target="_blank" rel="noopener"}

> Majority of public-facing devices still unpatched against critical vulns from as far back as 2022 More than 178,000 SonicWall firewalls are still vulnerable to years-old vulnerabilities, an infosec reseacher claims.... [...]
