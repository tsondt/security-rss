Title: While we fire the boss, can you lock him out of the network?
Date: 2024-01-12T08:31:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-01-12-while-we-fire-the-boss-can-you-lock-him-out-of-the-network

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/12/on_call/){:target="_blank" rel="noopener"}

> And he would have got away with it, too, if it weren’t for this one tiny backdoor On Call Welcome once more, dear reader, to On Call, The Register 's weekly reader-contributed column detailing the delights and dangers of working in tech support.... [...]
