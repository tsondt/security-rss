Title: JPMorgan exec claims bank repels 45 billion cyberattack attempts per day
Date: 2024-01-18T19:04:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-01-18-jpmorgan-exec-claims-bank-repels-45-billion-cyberattack-attempts-per-day

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/18/jpmorgan_exec_attacks/){:target="_blank" rel="noopener"}

> Assets boss also reckons she has more engineers than Amazon The largest bank in the United States repels 45 billion – yes, with a B – cyberattack attempts per day, one of its leaders claimed at the World Economic Forum in Davos.... [...]
