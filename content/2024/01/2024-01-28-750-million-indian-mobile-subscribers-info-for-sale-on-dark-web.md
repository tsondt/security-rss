Title: 750 million Indian mobile subscribers' info for sale on dark web
Date: 2024-01-28T23:29:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-01-28-750-million-indian-mobile-subscribers-info-for-sale-on-dark-web

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/28/asia_tech_news_roundup/){:target="_blank" rel="noopener"}

> ALSO: Samsung turns to Baidu for Galaxy AI in China; Terraform Labs files for bankruptcy; India's supercomputing ambitions Asia In Brief Indian infosec firm CloudSEK last week claimed it found records describing 750 million Indian mobile network subscribers on the dark web, with two crime gangs offering the trove of data for just $3,000.... [...]
