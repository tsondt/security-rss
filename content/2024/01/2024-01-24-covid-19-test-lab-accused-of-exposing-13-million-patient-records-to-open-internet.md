Title: COVID-19 test lab accused of exposing 1.3 million patient records to open internet
Date: 2024-01-24T07:28:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-01-24-covid-19-test-lab-accused-of-exposing-13-million-patient-records-to-open-internet

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/24/dutch_covid_testing_firm_ignored_warnings/){:target="_blank" rel="noopener"}

> Now that's a Dutch crunch A password-less database containing an estimated 1.3 million sets of Dutch COVID-19 testing records was left exposed to the open internet, and it's not clear if anyone is taking responsibility.... [...]
