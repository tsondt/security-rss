Title: Apple AirDrop leaks user data like a sieve. Chinese authorities say they’re scooping it up.
Date: 2024-01-12T17:52:41+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;AirDrop;apple;cryptographic hashes;privacy
Slug: 2024-01-12-apple-airdrop-leaks-user-data-like-a-sieve-chinese-authorities-say-theyre-scooping-it-up

[Source](https://arstechnica.com/?p=1995574){:target="_blank" rel="noopener"}

> Enlarge (credit: Aurich Lawson | Getty Images) Chinese authorities recently said they're using an advanced encryption attack to de-anonymize users of AirDrop in an effort to crack down on citizens who use the Apple file-sharing feature to mass-distribute content that's outlawed in that country. According to a 2022 report from The New York Times, activists have used AirDrop to distribute scathing critiques of the Communist Party of China to nearby iPhone users in subway trains and stations and other public venues. A document one protester sent in October of that year called General Secretary Xi Jinping a “despotic traitor.” A [...]
