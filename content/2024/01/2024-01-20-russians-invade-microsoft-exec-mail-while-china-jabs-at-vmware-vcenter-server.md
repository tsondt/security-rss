Title: Russians invade Microsoft exec mail while China jabs at VMware vCenter Server
Date: 2024-01-20T00:08:58+00:00
Author: Jessica Lyons Hardcastle
Category: The Register
Tags: 
Slug: 2024-01-20-russians-invade-microsoft-exec-mail-while-china-jabs-at-vmware-vcenter-server

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/20/chinese_russia_vmware_microsoft/){:target="_blank" rel="noopener"}

> Plus: Uncle Sam says Ivanti exploits 'consistent with PRC' snoops A VMware security vulnerability has been exploited by Chinese cyberspies since late 2021, according to Mandiant, in what has been a busy week for nation-state espionage news.... [...]
