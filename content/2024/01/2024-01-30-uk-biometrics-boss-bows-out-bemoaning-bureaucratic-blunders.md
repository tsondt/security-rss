Title: UK biometrics boss bows out, bemoaning bureaucratic blunders
Date: 2024-01-30T09:30:41+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-30-uk-biometrics-boss-bows-out-bemoaning-bureaucratic-blunders

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/30/surveillance_commissioner_final_report/){:target="_blank" rel="noopener"}

> Questionable institutional change and myriad IT issues pervade the governance landscape The farewell report written by the UK's biometrics and surveillance commissioner highlights a litany of failings in the Home Office's approach to governing the technology.... [...]
