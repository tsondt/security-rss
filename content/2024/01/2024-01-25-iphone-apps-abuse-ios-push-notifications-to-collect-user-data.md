Title: iPhone apps abuse iOS push notifications to collect user data
Date: 2024-01-25T13:28:01-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple;Mobile
Slug: 2024-01-25-iphone-apps-abuse-ios-push-notifications-to-collect-user-data

[Source](https://www.bleepingcomputer.com/news/security/iphone-apps-abuse-ios-push-notifications-to-collect-user-data/){:target="_blank" rel="noopener"}

> Numerous iOS apps are using background processes triggered by push notifications to collect user data about devices, potentially allowing the creation of fingerprinting profiles used for tracking. [...]
