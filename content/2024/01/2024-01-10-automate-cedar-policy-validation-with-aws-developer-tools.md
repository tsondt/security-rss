Title: Automate Cedar policy validation with AWS developer tools
Date: 2024-01-10T17:08:18+00:00
Author: Pontus Palmenäs
Category: AWS Security
Tags: Advanced (300);Amazon Verified Permissions;AWS CodeBuild;AWS CodePipeline;Security, Identity, & Compliance;Technical How-to;DevSecOps;Open source;Security Blog
Slug: 2024-01-10-automate-cedar-policy-validation-with-aws-developer-tools

[Source](https://aws.amazon.com/blogs/security/automate-cedar-policy-validation-with-aws-developer-tools/){:target="_blank" rel="noopener"}

> Cedar is an open-source language that you can use to write policies and make authorization decisions based on those policies. AWS security services including AWS Verified Access and Amazon Verified Permissions use Cedar to define policies. Cedar supports schema declaration for the structure of entity types in those policies and policy validation with that schema. [...]
