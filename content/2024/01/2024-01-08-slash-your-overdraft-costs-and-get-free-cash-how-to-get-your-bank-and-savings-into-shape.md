Title: Slash your overdraft costs and get ‘free’ cash: how to get your bank and savings into shape
Date: 2024-01-08T08:00:03+00:00
Author: Miles Brignall Samuel Gibbs, Sandra Haurant, Rupert Jones, Sarah Marsh and Hilary Osborne
Category: The Guardian
Tags: Banks and building societies;Current accounts;Consumer affairs;Money;Savings;Isas;UK news;Data and computer security;Savings rates
Slug: 2024-01-08-slash-your-overdraft-costs-and-get-free-cash-how-to-get-your-bank-and-savings-into-shape

[Source](https://www.theguardian.com/money/2024/jan/08/overdraft-costs-free-cash-bank-savings-current-accounts-isa-savings-best-buy){:target="_blank" rel="noopener"}

> Switch to a better current account, revive your Isa and pick a savings best buy to make the most of your money Authorised overdraft costs can vary dramatically. Many banks have overdraft calculators on their websites, so log on and compare what your bank charges versus what you would be charged if you took your custom elsewhere. But can you switch your current account if you are overdrawn? The answer is yes, says the Current Account Switch Service (Cass). However, you will need to agree any overdraft you require with your new bank. Alternatively, they may be able to provide [...]
