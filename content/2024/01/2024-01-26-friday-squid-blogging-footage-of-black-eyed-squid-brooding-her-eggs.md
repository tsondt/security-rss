Title: Friday Squid Blogging: Footage of Black-Eyed Squid Brooding Her Eggs
Date: 2024-01-26T22:10:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid;video
Slug: 2024-01-26-friday-squid-blogging-footage-of-black-eyed-squid-brooding-her-eggs

[Source](https://www.schneier.com/blog/archives/2024/01/friday-squid-blogging-footage-of-black-eyed-squid-brooding-her-eggs.html){:target="_blank" rel="noopener"}

> Amazing footage of a black-eyed squid ( Gonatus onyx ) carrying thousands of eggs. They tend to hang out about 6,200 feet below sea level. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
