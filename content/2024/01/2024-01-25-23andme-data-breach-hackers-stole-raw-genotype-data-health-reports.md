Title: 23andMe data breach: Hackers stole raw genotype data, health reports
Date: 2024-01-25T17:05:51-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-25-23andme-data-breach-hackers-stole-raw-genotype-data-health-reports

[Source](https://www.bleepingcomputer.com/news/security/23andme-data-breach-hackers-stole-raw-genotype-data-health-reports/){:target="_blank" rel="noopener"}

> Genetic testing provider 23andMe confirmed that hackers stole health reports and raw genotype data of customers affected by a credential stuffing attack that went unnoticed for five months, from April 29 to September 27. [...]
