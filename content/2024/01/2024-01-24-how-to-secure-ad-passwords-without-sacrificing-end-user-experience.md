Title: How to secure AD passwords without sacrificing end-user experience
Date: 2024-01-24T10:02:04-05:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2024-01-24-how-to-secure-ad-passwords-without-sacrificing-end-user-experience

[Source](https://www.bleepingcomputer.com/news/security/how-to-secure-ad-passwords-without-sacrificing-end-user-experience/){:target="_blank" rel="noopener"}

> To increase password security, regulatory bodies recommend longer and unique passwords. Despite this, many still stick to using the same easy-to-guess passwords for the sake of convenience. Learn more from Specops Software on an alternative approach that supports security and end-user experience at the same time. [...]
