Title: The Week in Ransomware - January 12th 2024 - Targeting homeowners' data
Date: 2024-01-12T17:06:44-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-01-12-the-week-in-ransomware-january-12th-2024-targeting-homeowners-data

[Source](https://www.bleepingcomputer.com/news/security/the-week-in-ransomware-january-12th-2024-targeting-homeowners-data/){:target="_blank" rel="noopener"}

> Mortgage lenders and related companies are becoming popular targets of ransomware gangs, with four companies in this sector recently attacked. [...]
