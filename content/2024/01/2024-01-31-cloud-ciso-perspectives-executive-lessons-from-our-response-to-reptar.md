Title: Cloud CISO Perspectives: Executive lessons from our response to Reptar
Date: 2024-01-31T17:00:00+00:00
Author: Yousif Hussin
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2024-01-31-cloud-ciso-perspectives-executive-lessons-from-our-response-to-reptar

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-executive-lessons-from-our-response-to-reptar/){:target="_blank" rel="noopener"}

> Welcome to the second Cloud CISO Perspectives for January 2024. In this edition, I’m turning the mic over to Yousif Hussin, who as a member of Google’s Vulnerability Coordination Center led our response to the recent serious zero-day CPU vulnerability Reptar. He talks about what important lessons executives can learn from how Google responds to critical zero-day vulnerabilities. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI Security [...]
