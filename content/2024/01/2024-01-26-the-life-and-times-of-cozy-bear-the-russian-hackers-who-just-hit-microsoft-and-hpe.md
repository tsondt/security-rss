Title: The life and times of Cozy Bear, the Russian hackers who just hit Microsoft and HPE
Date: 2024-01-26T13:15:08+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;breaches;cozy bear;hacking;hewlett packarge enterprise;microsoft
Slug: 2024-01-26-the-life-and-times-of-cozy-bear-the-russian-hackers-who-just-hit-microsoft-and-hpe

[Source](https://arstechnica.com/?p=1999178){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Hewlett Packard Enterprise (HPE) said Wednesday that Kremlin-backed actors hacked into the email accounts of its security personnel and other employees last May—and maintained surreptitious access until December. The disclosure was the second revelation of a major corporate network breach by the hacking group in five days. The hacking group that hit HPE is the same one that Microsoft said Friday broke into its corporate network in November and monitored email accounts of senior executives and security team members until being driven out earlier this month. Microsoft tracks the group as Midnight Blizzard. (Under the company’s [...]
