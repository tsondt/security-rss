Title: Zeppelin ransomware source code sold for $500 on hacking forum
Date: 2024-01-04T11:16:43-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-04-zeppelin-ransomware-source-code-sold-for-500-on-hacking-forum

[Source](https://www.bleepingcomputer.com/news/security/zeppelin-ransomware-source-code-sold-for-500-on-hacking-forum/){:target="_blank" rel="noopener"}

> A threat actor announced on a cybercrime forum that they sold the source code and a cracked version of the Zeppelin ransomware builder for just $500. [...]
