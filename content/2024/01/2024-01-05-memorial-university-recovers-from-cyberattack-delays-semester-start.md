Title: Memorial University recovers from cyberattack, delays semester start
Date: 2024-01-05T10:31:27-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Education
Slug: 2024-01-05-memorial-university-recovers-from-cyberattack-delays-semester-start

[Source](https://www.bleepingcomputer.com/news/security/memorial-university-recovers-from-cyberattack-delays-semester-start/){:target="_blank" rel="noopener"}

> The Memorial University of Newfoundland (MUN) continues to deal with the effects of a cyberattack that occurred in late December and postponed the start of classes in one campus. [...]
