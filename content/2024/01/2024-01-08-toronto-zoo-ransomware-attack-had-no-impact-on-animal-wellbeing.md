Title: Toronto Zoo: Ransomware attack had no impact on animal wellbeing
Date: 2024-01-08T17:16:43-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-01-08-toronto-zoo-ransomware-attack-had-no-impact-on-animal-wellbeing

[Source](https://www.bleepingcomputer.com/news/security/toronto-zoo-ransomware-attack-had-no-impact-on-animal-wellbeing/){:target="_blank" rel="noopener"}

> Toronto Zoo, the largest zoo in Canada, says that a ransomware attack that hit its systems on early Friday had no impact on the animals, its website, or its day-to-day operations. [...]
