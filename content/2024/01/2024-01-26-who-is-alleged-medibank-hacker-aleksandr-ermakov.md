Title: Who is Alleged Medibank Hacker Aleksandr Ermakov?
Date: 2024-01-26T18:12:09+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Breadcrumbs;Ne'er-Do-Well News;ae.ermak@yandex.ru;Aleksandr Ermakov;GustaveDore;JimJones;Medibank breach;Mikhail Borisovich Shefel;Patrick Gray;rEvil;Risky Business podcast;Shtazi
Slug: 2024-01-26-who-is-alleged-medibank-hacker-aleksandr-ermakov

[Source](https://krebsonsecurity.com/2024/01/who-is-alleged-medibank-hacker-aleksandr-ermakov/){:target="_blank" rel="noopener"}

> Authorities in Australia, the United Kingdom and the United States this week levied financial sanctions against a Russian man accused of stealing data on nearly 10 million customers of the Australian health insurance giant Medibank. 33-year-old Aleksandr Ermakov allegedly stole and leaked the Medibank data while working with one of Russia’s most destructive ransomware groups, but little more is shared about the accused. Here’s a closer look at the activities of Mr. Ermakov’s alleged hacker handles. Aleksandr Ermakov, 33, of Russia. Image: Australian Department of Foreign Affairs and Trade. The allegations against Ermakov mark the first time Australia has sanctioned [...]
