Title: Crypto wallet founder loses $125,000 to fake airdrop website
Date: 2024-01-05T07:17:43-05:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2024-01-05-crypto-wallet-founder-loses-125000-to-fake-airdrop-website

[Source](https://www.bleepingcomputer.com/news/security/crypto-wallet-founder-loses-125-000-to-fake-airdrop-website/){:target="_blank" rel="noopener"}

> A crypto wallet service co-founder shares with the world his agony after losing $125,000 to a crypto scam. The startup CEO, who at the time believed he was on a legitimate cryptocurrency airdrop website, realized after his loss that the domain he'd went on was setup for the purposes of phishing unsuspecting users. [...]
