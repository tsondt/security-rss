Title: Hackers target WordPress database plugin active on 1 million sites
Date: 2024-01-25T09:15:49-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-25-hackers-target-wordpress-database-plugin-active-on-1-million-sites

[Source](https://www.bleepingcomputer.com/news/security/hackers-target-wordpress-database-plugin-active-on-1-million-sites/){:target="_blank" rel="noopener"}

> Malicious activity targeting a critical severity flaw in the 'Better Search Replace' WordPress plugin has been detected, with researchers observing thousands of attempts in the past 24 hours. [...]
