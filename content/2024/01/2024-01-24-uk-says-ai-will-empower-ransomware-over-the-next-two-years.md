Title: UK says AI will empower ransomware over the next two years
Date: 2024-01-24T11:56:13-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-24-uk-says-ai-will-empower-ransomware-over-the-next-two-years

[Source](https://www.bleepingcomputer.com/news/security/uk-says-ai-will-empower-ransomware-over-the-next-two-years/){:target="_blank" rel="noopener"}

> The United Kingdom's National Cyber Security Centre (NCSC) warns that artificial intelligence (AI) tools will have an adverse near-term impact on cybersecurity, helping escalate the threat of ransomware. [...]
