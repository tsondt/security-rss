Title: Kasseika ransomware uses antivirus driver to kill other antiviruses
Date: 2024-01-23T14:58:56-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-23-kasseika-ransomware-uses-antivirus-driver-to-kill-other-antiviruses

[Source](https://www.bleepingcomputer.com/news/security/kasseika-ransomware-uses-antivirus-driver-to-kill-other-antiviruses/){:target="_blank" rel="noopener"}

> A recently uncovered ransomware operation named 'Kasseika' has joined the club of threat actors that employs Bring Your Own Vulnerable Driver (BYOVD) tactics to disable antivirus software before encrypting files. [...]
