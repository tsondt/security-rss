Title: Crypto-crook Sam Bankman-Fried spared a second trial
Date: 2024-01-02T07:30:06+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-01-02-crypto-crook-sam-bankman-fried-spared-a-second-trial

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/02/sam_bankman_fried_spared_a/){:target="_blank" rel="noopener"}

> Eighth charge related to campaign contributions would just take too dang long US prosecutors do not plan to proceed with a second trial of convicted and imprisoned crypto-villain Sam Bankman-Fried (SBF), according to a Southern District of New York court letter filed on December 29.... [...]
