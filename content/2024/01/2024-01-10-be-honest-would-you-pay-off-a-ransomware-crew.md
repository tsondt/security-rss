Title: Be honest. Would you pay off a ransomware crew?
Date: 2024-01-10T19:56:53+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: 2024-01-10-be-honest-would-you-pay-off-a-ransomware-crew

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/10/ransomware_kettle/){:target="_blank" rel="noopener"}

> Today us vultures are debating bans on ransom payments, deplorable tactics by extortionists, and more Kettle Believe us, we wish there was a simple solution that could stop ransomware dead in its tracks for good.... [...]
