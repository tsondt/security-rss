Title: Hacker spins up 1 million virtual servers to illegally mine crypto
Date: 2024-01-13T10:09:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud;CryptoCurrency;Legal
Slug: 2024-01-13-hacker-spins-up-1-million-virtual-servers-to-illegally-mine-crypto

[Source](https://www.bleepingcomputer.com/news/security/hacker-spins-up-1-million-virtual-servers-to-illegally-mine-crypto/){:target="_blank" rel="noopener"}

> A 29-year-old man in Ukraine was arrested this week for using hacked accounts to create 1 million virtual servers used to mine $2 million in cryptocurrency. [...]
