Title: Quantum Computing Skeptics
Date: 2024-01-25T12:04:15+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;quantum computing
Slug: 2024-01-25-quantum-computing-skeptics

[Source](https://www.schneier.com/blog/archives/2024/01/quantum-computing-skeptics.html){:target="_blank" rel="noopener"}

> Interesting article. I am also skeptical that we are going to see useful quantum computers anytime soon. Since at least 2019, I have been saying that this is hard. And that we don’t know if it’s “land a person on the surface of the moon” hard, or “land a person on the surface of the sun” hard. They’re both hard, but very different. [...]
