Title: Docker hosts hacked in ongoing website traffic theft scheme
Date: 2024-01-18T06:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-18-docker-hosts-hacked-in-ongoing-website-traffic-theft-scheme

[Source](https://www.bleepingcomputer.com/news/security/docker-hosts-hacked-in-ongoing-website-traffic-theft-scheme/){:target="_blank" rel="noopener"}

> A new campaign targeting vulnerable Docker services deploys an XMRig miner and the 9hits viewer app on compromised hosts, allowing a dual monetization strategy. [...]
