Title: HPE joins the 'our executive email was hacked by Russia' club
Date: 2024-01-25T02:02:34+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-01-25-hpe-joins-the-our-executive-email-was-hacked-by-russia-club

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/25/hpe_russia_email_attack/){:target="_blank" rel="noopener"}

> Moscow-backed Cozy Bear may have had access to the green rectangular email cloud for six months HPE has become the latest tech giant to admit it has been compromised by Russian operatives.... [...]
