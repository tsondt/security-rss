Title: Accused PII seller faces jail for running underground fraud op
Date: 2024-01-23T16:00:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-01-23-accused-pii-seller-faces-jail-for-running-underground-fraud-op

[Source](https://go.theregister.com/feed/www.theregister.com/2024/01/23/serial_data_peddler_faces_prison/){:target="_blank" rel="noopener"}

> More than 5,000 victims claimed over a 3-year period but filing reckons accused didn't even use a VPN A Baltimore man faces a potential maximum 20-year prison sentence after being charged for his alleged role in running an online service that sold personal data which was later used for financial fraud.... [...]
