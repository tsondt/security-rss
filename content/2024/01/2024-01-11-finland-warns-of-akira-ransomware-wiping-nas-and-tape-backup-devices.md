Title: Finland warns of Akira ransomware wiping NAS and tape backup devices
Date: 2024-01-11T10:01:46-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-01-11-finland-warns-of-akira-ransomware-wiping-nas-and-tape-backup-devices

[Source](https://www.bleepingcomputer.com/news/security/finland-warns-of-akira-ransomware-wiping-nas-and-tape-backup-devices/){:target="_blank" rel="noopener"}

> The Finish National Cybersecurity Center (NCSC-FI) is informing of increased Akira ransomware activity in December, targeting companies in the country and wiping backups. [...]
