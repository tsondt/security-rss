Title: AI will increase the number and impact of cyberattacks, intel officers say
Date: 2024-01-25T13:44:28+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: AI;Biz & IT;Science;Artificial Intelligence;cybercrime;hacking;ransomware
Slug: 2024-01-25-ai-will-increase-the-number-and-impact-of-cyberattacks-intel-officers-say

[Source](https://arstechnica.com/?p=1998885){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Threats from malicious cyberactivity are likely to increase as nation-states, financially motivated criminals, and novices increasingly incorporate artificial intelligence into their routines, the UK’s top intelligence agency said. The assessment, from the UK’s Government Communications Headquarters, predicted ransomware will be the biggest threat to get a boost from AI over the next two years. AI will lower barriers to entry, a change that will bring a surge of new entrants into the criminal enterprise. More experienced threat actors—such as nation-states, the commercial firms that serve them, and financially motivated crime groups—will likely also benefit, as AI [...]
