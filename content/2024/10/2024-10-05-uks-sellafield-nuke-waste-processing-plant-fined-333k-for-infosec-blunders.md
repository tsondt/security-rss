Title: UK's Sellafield nuke waste processing plant fined £333K for infosec blunders
Date: 2024-10-05T06:07:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-05-uks-sellafield-nuke-waste-processing-plant-fined-333k-for-infosec-blunders

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/05/sellafield_nuclear_site_fined/){:target="_blank" rel="noopener"}

> Radioactive hazards and cyber failings... what could possibly go wrong? The outfit that runs Britain's Sellafield nuclear waste processing and decommissioning site has been fined £332,500 ($440,000) by the nation's Office for Nuclear Regulation (ONR) for its shoddy cybersecurity practices between 2019 and 2023.... [...]
