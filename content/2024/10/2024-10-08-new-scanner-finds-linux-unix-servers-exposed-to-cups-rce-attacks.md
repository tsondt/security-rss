Title: New scanner finds Linux, UNIX servers exposed to CUPS RCE attacks
Date: 2024-10-08T17:48:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Software;Security
Slug: 2024-10-08-new-scanner-finds-linux-unix-servers-exposed-to-cups-rce-attacks

[Source](https://www.bleepingcomputer.com/news/software/new-scanner-finds-linux-unix-servers-exposed-to-cups-rce-attacks/){:target="_blank" rel="noopener"}

> An automated scanner has been released to help security professionals scan environments for devices vulnerable to the Common Unix Printing System (CUPS) RCE flaw tracked as CVE-2024-47176. [...]
