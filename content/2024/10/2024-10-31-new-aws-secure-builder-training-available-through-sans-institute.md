Title: New AWS Secure Builder training available through SANS Institute
Date: 2024-10-31T23:01:29+00:00
Author: Mecca Nnacheta
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Cybersecurity awareness;Security;Security Blog;Training
Slug: 2024-10-31-new-aws-secure-builder-training-available-through-sans-institute

[Source](https://aws.amazon.com/blogs/security/new-aws-secure-builder-training-available-through-sans-institute/){:target="_blank" rel="noopener"}

> Education is critical to effective security. As organizations migrate, modernize, and build with Amazon Web Services (AWS), engineering and development teams need specific skills and knowledge to embed security into workloads. Lack of support for these skills can increase the likelihood of security incidents. AWS has partnered with SANS Institute to create SEC480: AWS Secure Builder—a [...]
