Title: ESET partner breached to send data wipers to Israeli orgs
Date: 2024-10-18T14:25:53-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-10-18-eset-partner-breached-to-send-data-wipers-to-israeli-orgs

[Source](https://www.bleepingcomputer.com/news/security/eset-partner-breached-to-send-data-wipers-to-israeli-orgs/){:target="_blank" rel="noopener"}

> Hackers breached ESET's exclusive partner in Israel to send phishing emails to Israeli businesses that pushed data wipers disguised as antivirus software for destructive attacks. [...]
