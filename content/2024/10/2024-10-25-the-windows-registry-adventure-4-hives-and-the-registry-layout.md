Title: The Windows Registry Adventure #4: Hives and the registry layout
Date: 2024-10-25T10:30:00.001000-07:00
Author: Google Project Zero (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2024-10-25-the-windows-registry-adventure-4-hives-and-the-registry-layout

[Source](https://googleprojectzero.blogspot.com/2024/10/the-windows-registry-adventure-4-hives.html){:target="_blank" rel="noopener"}

> Posted by Mateusz Jurczyk, Google Project Zero To a normal user or even a Win32 application developer, the registry layout may seem simple: there are five root keys that we know from Regedit (abbreviated as HKCR, HKLM, HKCU, HKU and HKCC), and each of them contains a nested tree structure that serves a specific role in the system. But as one tries to dig deeper and understand how the registry really works internally, things may get confusing really fast. What are hives ? How do they map or relate to the top-level keys? Why are some HKEY root keys pointing [...]
