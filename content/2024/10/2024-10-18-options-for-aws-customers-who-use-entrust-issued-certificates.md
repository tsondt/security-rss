Title: Options for AWS customers who use Entrust-issued certificates
Date: 2024-10-18T12:48:47+00:00
Author: Zach Miller
Category: AWS Security
Tags: AWS Certificate Manager;Foundational (100);Security, Identity, & Compliance;ACM;certificate management;certificates;Security Blog;TLS
Slug: 2024-10-18-options-for-aws-customers-who-use-entrust-issued-certificates

[Source](https://aws.amazon.com/blogs/security/options-for-aws-customers-who-use-entrust-issued-certificates/){:target="_blank" rel="noopener"}

> Multiple popular browsers have announced that they will no longer trust public certificates issued by Entrust later this year. Certificates that are issued by Entrust on dates up to and including November 11, 2024 will continue to be trusted until they expire, according to current information from browser makers. Certificates issued by Entrust after that date [...]
