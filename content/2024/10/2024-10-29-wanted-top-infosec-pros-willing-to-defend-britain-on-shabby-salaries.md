Title: Wanted. Top infosec pros willing to defend Britain on shabby salaries
Date: 2024-10-29T06:26:09+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-10-29-wanted-top-infosec-pros-willing-to-defend-britain-on-shabby-salaries

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/29/gchq_needs_advanced_cybersecurity_professionals/){:target="_blank" rel="noopener"}

> GCHQ job ads seek top talent with bottom-end pay packets While the wages paid by governments seldom match those available in the private sector, it appears that the UK's intelligence, security and cyber agency is a long way short of being competitive in its quest for talent.... [...]
