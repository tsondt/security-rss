Title: Are Automatic License Plate Scanners Constitutional?
Date: 2024-10-23T18:16:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cars;courts;privacy;scanners;searches;surveillance
Slug: 2024-10-23-are-automatic-license-plate-scanners-constitutional

[Source](https://www.schneier.com/blog/archives/2024/10/are-automatic-license-plate-scanners-constitutional.html){:target="_blank" rel="noopener"}

> An advocacy groups is filing a Fourth Amendment challenge against automatic license plate readers. “The City of Norfolk, Virginia, has installed a network of cameras that make it functionally impossible for people to drive anywhere without having their movements tracked, photographed, and stored in an AI-assisted database that enables the warrantless surveillance of their every move. This civil rights lawsuit seeks to end this dragnet surveillance program,” the lawsuit notes. “In Norfolk, no one can escape the government’s 172 unblinking eyes,” it continues, referring to the 172 Flock cameras currently operational in Norfolk. The Fourth Amendment protects against unreasonable searches [...]
