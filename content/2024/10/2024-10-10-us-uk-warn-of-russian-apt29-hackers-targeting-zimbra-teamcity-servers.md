Title: US, UK warn of Russian APT29 hackers targeting Zimbra, TeamCity servers
Date: 2024-10-10T14:49:36-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-10-10-us-uk-warn-of-russian-apt29-hackers-targeting-zimbra-teamcity-servers

[Source](https://www.bleepingcomputer.com/news/security/us-uk-warn-of-russian-apt29-hackers-targeting-zimbra-teamcity-servers/){:target="_blank" rel="noopener"}

> U.S. and U.K. cyber agencies warned today that APT29 hackers linked to Russia's Foreign Intelligence Service (SVR) target vulnerable Zimbra and JetBrains TeamCity servers "at a mass scale." [...]
