Title: Tower PC case used as 'creative cavity' by drug importer
Date: 2024-10-31T08:25:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-10-31-tower-pc-case-used-as-creative-cavity-by-drug-importer

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/31/australia_pc_drug_arrest/){:target="_blank" rel="noopener"}

> Motherboard missing, leaving space for a million hits of meth Australian police have arrested a man after finding he imported what appear to be tower PC cases that were full of illicit drugs.... [...]
