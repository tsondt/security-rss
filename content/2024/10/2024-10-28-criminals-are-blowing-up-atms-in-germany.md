Title: Criminals Are Blowing up ATMs in Germany
Date: 2024-10-28T16:12:43+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;ATMs;banking;bombs;theft
Slug: 2024-10-28-criminals-are-blowing-up-atms-in-germany

[Source](https://www.schneier.com/blog/archives/2024/10/criminals-are-blowing-up-atms-in-germany.html){:target="_blank" rel="noopener"}

> It’s low tech, but effective. Why Germany? It has more ATMs than other European countries, and—if I read the article right—they have more money in them. [...]
