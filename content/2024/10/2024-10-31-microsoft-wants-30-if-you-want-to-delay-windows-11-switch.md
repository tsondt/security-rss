Title: Microsoft wants $30 if you want to delay Windows 11 switch
Date: 2024-10-31T14:07:17-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-10-31-microsoft-wants-30-if-you-want-to-delay-windows-11-switch

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-wants-30-if-you-want-to-delay-windows-11-switch/){:target="_blank" rel="noopener"}

> ​Microsoft announced today that Windows 10 home users can delay the switch to Windows 11 for one more year if they're willing to pay $30 for Extended Security Updates (ESU). [...]
