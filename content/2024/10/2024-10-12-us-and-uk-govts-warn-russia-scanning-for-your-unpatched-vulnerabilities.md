Title: US and UK govts warn: Russia scanning for your unpatched vulnerabilities
Date: 2024-10-12T03:05:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-10-12-us-and-uk-govts-warn-russia-scanning-for-your-unpatched-vulnerabilities

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/12/russia_is_targeting_you_for/){:target="_blank" rel="noopener"}

> Also, phishing's easier over the phone, and your F5 cookies might be unencrypted, and more in brief If you need an excuse to improve your patching habits, a joint advisory from the US and UK governments about a massive, ongoing Russian campaign exploiting known vulnerabilities should do the trick.... [...]
