Title: Fired Disney staffer accused of hacking menu to add profanity, wingdings, removes allergen info
Date: 2024-10-30T15:12:39+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-10-30-fired-disney-staffer-accused-of-hacking-menu-to-add-profanity-wingdings-removes-allergen-info

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/30/fired_disney_employee_hacks_menu/){:target="_blank" rel="noopener"}

> If you're gonna come at the mouse, you need to be better at hiding your tracks A disgruntled ex-Disney employee has been arrested and charged with hacking his former employer's systems to alter restaurant menus with potentially deadly consequences.... [...]
