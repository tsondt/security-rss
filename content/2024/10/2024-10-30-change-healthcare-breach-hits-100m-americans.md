Title: Change Healthcare Breach Hits 100M Americans
Date: 2024-10-30T13:34:08+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;Latest Warnings;The Coming Storm;ALPHV;Anthem Inc.;BlackCat;Equifax;Experian;HIPAA Journal;IDX;RansomHub;Sen. Mark Warner;Sen. Ron Wyden;TransUnion;U.S. Department of Health and Human Resources;United Health Group
Slug: 2024-10-30-change-healthcare-breach-hits-100m-americans

[Source](https://krebsonsecurity.com/2024/10/change-healthcare-breach-hits-100m-americans/){:target="_blank" rel="noopener"}

> Change Healthcare says it has notified approximately 100 million Americans that their personal, financial and healthcare records may have been stolen in a February 2024 ransomware attack that caused the largest ever known data breach of protected health information. Image: Tamer Tuncay, Shutterstock.com. A ransomware attack at Change Healthcare in the third week of February quickly spawned disruptions across the U.S. healthcare system that reverberated for months, thanks to the company’s central role in processing payments and prescriptions on behalf of thousands of organizations. In April, Change estimated the breach would affect a “substantial proportion of people in America.” On [...]
