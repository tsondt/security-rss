Title: Happy birthday, Putin – you've been pwned
Date: 2024-10-08T06:30:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-08-happy-birthday-putin-youve-been-pwned

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/08/russia_state_news_shutdown/){:target="_blank" rel="noopener"}

> Pro-Ukraine hackers claim credit for Russian state broadcasting shutdown Ukrainian hackers shut down Russian state news agency VGTRK's online broadcasting and streaming services on Monday – president Vladimir Putin's 72nd birthday – as Kremlin officials vowed to bring those responsible for the "unprecedented" cyber attack to justice.... [...]
