Title: LiteSpeed Cache WordPress plugin bug lets hackers get admin access
Date: 2024-10-31T12:19:31-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-10-31-litespeed-cache-wordpress-plugin-bug-lets-hackers-get-admin-access

[Source](https://www.bleepingcomputer.com/news/security/litespeed-cache-wordpress-plugin-bug-lets-hackers-get-admin-access/){:target="_blank" rel="noopener"}

> The free version of the popular WordPress plugin LiteSpeed Cache has fixed a dangerous privilege elevation flaw on its latest release that could allow unauthenticated site visitors to gain admin rights. [...]
