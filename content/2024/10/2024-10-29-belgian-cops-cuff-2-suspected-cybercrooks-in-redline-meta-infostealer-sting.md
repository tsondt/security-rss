Title: Belgian cops cuff 2 suspected cybercrooks in Redline, Meta infostealer sting
Date: 2024-10-29T16:35:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-29-belgian-cops-cuff-2-suspected-cybercrooks-in-redline-meta-infostealer-sting

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/29/belgian_cops_arrest_two_suspected/){:target="_blank" rel="noopener"}

> US also charges an alleged Redline dev, no mention of an arrest International law enforcement officials have arrested two individuals and charged another in connection with the use and distribution of the Redline and Meta infostealer malware strains.... [...]
