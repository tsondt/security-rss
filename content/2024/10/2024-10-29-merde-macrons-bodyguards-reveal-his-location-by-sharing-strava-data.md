Title: Merde! Macron's bodyguards reveal his location by sharing Strava data
Date: 2024-10-29T10:32:08+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-10-29-merde-macrons-bodyguards-reveal-his-location-by-sharing-strava-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/29/macron_location_strava/){:target="_blank" rel="noopener"}

> It's not just the French president, Biden and Putin also reportedly trackable The French equivalent of the US Secret Service may have been letting their guard down, as an investigation showed they are easily trackable via the fitness app Strava.... [...]
