Title: Brazilian police claim they've cuffed serial cybercrook behind FBI and Airbus attacks
Date: 2024-10-17T14:00:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-17-brazilian-police-claim-theyve-cuffed-serial-cybercrook-behind-fbi-and-airbus-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/17/brazil_usdod_arrest/){:target="_blank" rel="noopener"}

> Early stage opsec failures lead to landmark arrest of suspected serial data thief Brazilian police are being cagey with the details about the arrest of a person suspected to be responsible for various high-profile data thefts.... [...]
