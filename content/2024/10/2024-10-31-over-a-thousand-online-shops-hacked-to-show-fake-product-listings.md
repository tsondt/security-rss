Title: Over a thousand online shops hacked to show fake product listings
Date: 2024-10-31T09:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-10-31-over-a-thousand-online-shops-hacked-to-show-fake-product-listings

[Source](https://www.bleepingcomputer.com/news/security/over-a-thousand-online-shops-hacked-to-show-fake-product-listings/){:target="_blank" rel="noopener"}

> A phishing campaign dubbed 'Phish n' Ships' has been underway since at least 2019, infecting over a thousand legitimate online stores to promote fake product listings for hard-to-find items. [...]
