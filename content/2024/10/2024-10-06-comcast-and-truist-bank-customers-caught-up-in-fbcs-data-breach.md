Title: Comcast and Truist Bank customers caught up in FBCS data breach
Date: 2024-10-06T11:12:23-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-10-06-comcast-and-truist-bank-customers-caught-up-in-fbcs-data-breach

[Source](https://www.bleepingcomputer.com/news/security/comcast-and-truist-bank-customers-caught-up-in-fbcs-data-breach/){:target="_blank" rel="noopener"}

> Comcast Cable Communications and Truist Bank have disclosed they were impacted by a data breach at FBCS, and are now informing their respective customers that their data has been compromised. [...]
