Title: Critical default credential bug in Kubernetes Image Builder allows SSH root access
Date: 2024-10-16T21:58:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-16-critical-default-credential-bug-in-kubernetes-image-builder-allows-ssh-root-access

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/16/critical_kubernetes_image_builder_bug/){:target="_blank" rel="noopener"}

> It's called leaving the door wide open – especially in Proxmox A critical bug in Kubernetes Image Builder could allow unauthorized SSH access to virtual machines (VMs) thanks to default credentials being enabled during the image build process.... [...]
