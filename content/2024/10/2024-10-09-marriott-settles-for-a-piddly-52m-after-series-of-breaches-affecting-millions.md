Title: Marriott settles for a piddly $52M after series of breaches affecting millions
Date: 2024-10-09T21:08:19+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-09-marriott-settles-for-a-piddly-52m-after-series-of-breaches-affecting-millions

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/09/marriott_settlements_data_breaches/){:target="_blank" rel="noopener"}

> Intruders stayed for free on the network between 2014 and 2020 Marriott has agreed to pay a $52 million penalty and develop a comprehensive infosec program following a series of major data breaches between 2014 and 2020 that affected more than 344 million people worldwide.... [...]
