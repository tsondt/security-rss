Title: Akira ransomware is encrypting victims again following pure extortion fling
Date: 2024-10-22T15:31:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-22-akira-ransomware-is-encrypting-victims-again-following-pure-extortion-fling

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/22/akira_encrypting_again/){:target="_blank" rel="noopener"}

> Crooks revert to old ways for greater efficiency Experts believe the Akira ransomware operation is up to its old tricks again, encrypting victims' files after a break from the typical double extortion tactics.... [...]
