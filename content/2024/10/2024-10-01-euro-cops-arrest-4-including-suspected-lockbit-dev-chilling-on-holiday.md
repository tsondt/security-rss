Title: Euro cops arrest 4 including suspected LockBit dev chilling on holiday
Date: 2024-10-01T17:35:00+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-01-euro-cops-arrest-4-including-suspected-lockbit-dev-chilling-on-holiday

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/01/euro_cops_arrest_four_mystery/){:target="_blank" rel="noopener"}

> And what looks like proof stolen data was never deleted even after ransom paid Building on the success of what's known around here as LockBit Leak Week in February, the authorities say they've arrested a further four individuals with ties to the now-scuppered LockBit ransomware empire.... [...]
