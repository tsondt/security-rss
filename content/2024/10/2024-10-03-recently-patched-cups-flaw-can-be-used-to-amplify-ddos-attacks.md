Title: Recently patched CUPS flaw can be used to amplify DDoS attacks
Date: 2024-10-03T18:33:38-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-10-03-recently-patched-cups-flaw-can-be-used-to-amplify-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/security/recently-patched-cups-flaw-can-be-used-to-amplify-ddos-attacks/){:target="_blank" rel="noopener"}

> A recently disclosed vulnerability in the Common Unix Printing System (CUPS) open-source printing system can be exploited by threat actors to launch distributed denial-of-service (DDoS) attacks with a 600x amplification factor. [...]
