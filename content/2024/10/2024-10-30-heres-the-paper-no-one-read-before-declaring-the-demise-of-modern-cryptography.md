Title: Here’s the paper no one read before declaring the demise of modern cryptography
Date: 2024-10-30T11:00:32+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Features;Security;encryption;post quantum cryptography;quantum computing
Slug: 2024-10-30-heres-the-paper-no-one-read-before-declaring-the-demise-of-modern-cryptography

[Source](https://arstechnica.com/information-technology/2024/10/the-sad-bizarre-tale-of-hype-fueling-fears-that-modern-cryptography-is-dead/){:target="_blank" rel="noopener"}

> There’s little doubt that some of the most important pillars of modern cryptography will tumble spectacularly once quantum computing, now in its infancy, matures sufficiently. Some experts say that could be in the next couple decades. Others say it could take longer. No one knows. The uncertainty leaves a giant vacuum that can be filled with alarmist pronouncements that the world is close to seeing the downfall of cryptography as we know it. The false pronouncements can take on a life of their own as they’re repeated by marketers looking to peddle post-quantum cryptography snake oil and journalists tricked into [...]
