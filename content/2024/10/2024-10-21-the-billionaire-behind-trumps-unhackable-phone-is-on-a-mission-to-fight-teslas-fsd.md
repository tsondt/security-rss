Title: The billionaire behind Trump's 'unhackable' phone is on a mission to fight Tesla's FSD
Date: 2024-10-21T19:30:13+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-10-21-the-billionaire-behind-trumps-unhackable-phone-is-on-a-mission-to-fight-teslas-fsd

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/21/odowd_tesla_trump/){:target="_blank" rel="noopener"}

> Dan O'Dowd tells El Reg about the OS secrets and ongoing clash with Musk Interview This month, presidential hopeful Donald Trump got a tool in his arsenal: some allegedly "unhackable" communications kit. The Register has talked to the man behind the operating system, who also ran for the US Senate on a campaign to get self-driving Teslas off the road – and is on something of a crusade about the matter.... [...]
