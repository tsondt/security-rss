Title: Sudanese Brothers Arrested in ‘AnonSudan’ Takedown
Date: 2024-10-17T14:17:07+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;DDoS-for-Hire;Ne'er-Do-Well News;Web Fraud 2.0;Ahmed Salah Yousif Omer;Alaa Salah Yusuuf Omer;Amazon;AnonSudan;Anonymous Sudan;fbi;Godzilla botnet;InfraShutdown;Skynet;U.S. Department of Justice
Slug: 2024-10-17-sudanese-brothers-arrested-in-anonsudan-takedown

[Source](https://krebsonsecurity.com/2024/10/sudanese-brothers-arrested-in-anonsudan-takedown/){:target="_blank" rel="noopener"}

> The U.S. government on Wednesday announced the arrest and charging of two Sudanese brothers accused of running Anonymous Sudan (a.k.a. AnonSudan ), a cybercrime business known for launching powerful distributed denial-of-service (DDoS) attacks against a range of targets, including dozens of hospitals, news websites and cloud providers. The younger brother is facing charges that could land him life in prison for allegedly seeking to kill people with his attacks. Image: FBI Active since at least January 2023, AnonSudan has been described in media reports as a “hacktivist” group motivated by ideological causes. But in a criminal complaint, the FBI said [...]
