Title: Rejoice! The charade of having to change our passwords every few months is coming to an end | Kate O'Flaherty
Date: 2024-10-09T14:16:30+00:00
Author: Kate O'Flaherty
Category: The Guardian
Tags: Data and computer security;Cybercrime;Technology;Internet;World news
Slug: 2024-10-09-rejoice-the-charade-of-having-to-change-our-passwords-every-few-months-is-coming-to-an-end-kate-oflaherty

[Source](https://www.theguardian.com/commentisfree/2024/oct/09/charade-change-passwords-cyberattacks){:target="_blank" rel="noopener"}

> The US government is finally admitting there’s no need – instead, to fend off cyber-attacks we need passwords that are long but memorable Over the past decade or so, people have accumulated a vast array of logins for dozens of sites and apps, as more of our work and home lives moves on to the internet. That’s why it has never made sense that so many IT departments have belligerently insisted on maintaining a major hurdle to password management. Namely, the need to change passwords regularly. It’s a familiar scenario. You arrive at the office and need to log on [...]
