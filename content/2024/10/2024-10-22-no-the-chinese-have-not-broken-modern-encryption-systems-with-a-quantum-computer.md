Title: No, The Chinese Have Not Broken Modern Encryption Systems with a Quantum Computer
Date: 2024-10-22T11:03:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;China;encryption;quantum computing
Slug: 2024-10-22-no-the-chinese-have-not-broken-modern-encryption-systems-with-a-quantum-computer

[Source](https://www.schneier.com/blog/archives/2024/10/no-the-chinese-have-not-broken-modern-encryption-systems-with-a-quantum-computer.html){:target="_blank" rel="noopener"}

> The headline is pretty scary: “ China’s Quantum Computer Scientists Crack Military-Grade Encryption.” No, it’s not true. This debunking saved me the trouble of writing one. It all seems to have come from this news article, which wasn’t bad but was taken widely out of proportion. Cryptography is safe, and will be for a long time [...]
