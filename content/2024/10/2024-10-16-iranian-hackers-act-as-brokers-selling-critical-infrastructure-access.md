Title: Iranian hackers act as brokers selling critical infrastructure access
Date: 2024-10-16T19:16:17-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-10-16-iranian-hackers-act-as-brokers-selling-critical-infrastructure-access

[Source](https://www.bleepingcomputer.com/news/security/iranian-hackers-act-as-brokers-selling-critical-infrastructure-access/){:target="_blank" rel="noopener"}

> Iranian hackers are breaching critical infrastructure organizations to collect credentials and network data that can be sold on cybercriminal forums to enable cyberattacks from other threat actors. [...]
