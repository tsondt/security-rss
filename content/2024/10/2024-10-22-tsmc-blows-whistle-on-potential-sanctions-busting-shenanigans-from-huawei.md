Title: TSMC blows whistle on potential sanctions-busting shenanigans from Huawei
Date: 2024-10-22T17:45:05+00:00
Author: Gavin Bonshor
Category: The Register
Tags: 
Slug: 2024-10-22-tsmc-blows-whistle-on-potential-sanctions-busting-shenanigans-from-huawei

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/22/tsmc_huawei_sanctions_report/){:target="_blank" rel="noopener"}

> Chip giant tells Uncle Sam someone could be making orders on the sly TSMC has reportedly tipped off US officials to a potential attempt by Huawei to circumvent export controls and obtain AI chips manufactured by the Taiwanese company.... [...]
