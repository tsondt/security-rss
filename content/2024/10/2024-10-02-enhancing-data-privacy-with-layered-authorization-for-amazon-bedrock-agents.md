Title: Enhancing data privacy with layered authorization for Amazon Bedrock Agents
Date: 2024-10-02T14:23:18+00:00
Author: Jeremy Ware
Category: AWS Security
Tags: Advanced (300);Amazon Bedrock;Best Practices;Generative AI;Security, Identity, & Compliance;Amazon Verified Permissions;authentication;authorization;Security;Security Blog
Slug: 2024-10-02-enhancing-data-privacy-with-layered-authorization-for-amazon-bedrock-agents

[Source](https://aws.amazon.com/blogs/security/enhancing-data-privacy-with-layered-authorization-for-amazon-bedrock-agents/){:target="_blank" rel="noopener"}

> Customers are finding several advantages to using generative AI within their applications. However, using generative AI adds new considerations when reviewing the threat model of an application, whether you’re using it to improve the customer experience for operational efficiency, to generate more tailored or specific results, or for other reasons. Generative AI models are inherently [...]
