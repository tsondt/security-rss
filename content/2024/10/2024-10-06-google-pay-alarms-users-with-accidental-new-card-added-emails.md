Title: Google Pay alarms users with accidental ‘new card’ added emails
Date: 2024-10-06T09:00:00-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2024-10-06-google-pay-alarms-users-with-accidental-new-card-added-emails

[Source](https://www.bleepingcomputer.com/news/security/google-pay-alarms-users-with-accidental-new-card-added-emails/){:target="_blank" rel="noopener"}

> Google Pay alarmed users this week after erroneously sending out "new card" added email notifications. Google has acknowledged that the email was "accidental" and that no user information was compromised. [...]
