Title: Evil Corp hit with new sanctions, BitPaymer ransomware charges
Date: 2024-10-01T12:30:16-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-10-01-evil-corp-hit-with-new-sanctions-bitpaymer-ransomware-charges

[Source](https://www.bleepingcomputer.com/news/security/evil-corp-hit-with-new-sanctions-bitpaymer-ransomware-charges/){:target="_blank" rel="noopener"}

> The Evil Corp cybercrime syndicate has been hit with new sanctions by the United States, United Kingdom, and Australia. The US also indicted one of its members for conducting BitPaymer ransomware attacks. [...]
