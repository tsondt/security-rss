Title: Troubled US insurance giant hit by extortion after data leak
Date: 2024-10-17T23:30:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-10-17-troubled-us-insurance-giant-hit-by-extortion-after-data-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/17/us_insurance_giant_with_a/){:target="_blank" rel="noopener"}

> Globe Life claims blackmailers shared stolen into with short sellers US insurance provider Globe Life, already grappling with legal troubles, now faces a fresh headache: an extortion attempt involving stolen customer data.... [...]
