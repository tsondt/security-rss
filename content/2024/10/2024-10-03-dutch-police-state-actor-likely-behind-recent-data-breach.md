Title: Dutch Police: ‘State actor’ likely behind recent data breach
Date: 2024-10-03T14:56:35-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-10-03-dutch-police-state-actor-likely-behind-recent-data-breach

[Source](https://www.bleepingcomputer.com/news/security/dutch-police-state-actor-likely-behind-recent-data-breach/){:target="_blank" rel="noopener"}

> The national Dutch police (Politie) says that a state actor was likely behind the data breach it detected last week. [...]
