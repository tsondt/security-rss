Title: Five Eyes nations tell tech startups to take infosec seriously. Again
Date: 2024-10-29T08:29:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-10-29-five-eyes-nations-tell-tech-startups-to-take-infosec-seriously-again

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/29/five_eyes_secure_innovation_campaign/){:target="_blank" rel="noopener"}

> Only took 'em a year to dish up some scary travel advice, and a Secure Innovation... Placemat? Cyber security agencies from the Five Eyes nations have delivered on a promise to offer tech startups more guidance on how to stay secure.... [...]
