Title: JPMorgan Chase sues scammers following viral 'infinite money glitch'
Date: 2024-10-28T20:45:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-28-jpmorgan-chase-sues-scammers-following-viral-infinite-money-glitch

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/28/jpmorgan_chase_sues_scammers/){:target="_blank" rel="noopener"}

> ATMs paid customers thousands... and now the bank wants its money back JPMorgan Chase has begun suing fraudsters who allegedly stole thousands of dollars from the bank's ATMs after a check fraud glitch went viral on social media.... [...]
