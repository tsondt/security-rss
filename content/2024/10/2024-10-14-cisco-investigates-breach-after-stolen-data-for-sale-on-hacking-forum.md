Title: Cisco investigates breach after stolen data for sale on hacking forum
Date: 2024-10-14T22:25:02-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-10-14-cisco-investigates-breach-after-stolen-data-for-sale-on-hacking-forum

[Source](https://www.bleepingcomputer.com/news/security/cisco-investigates-breach-after-stolen-data-for-sale-on-hacking-forum/){:target="_blank" rel="noopener"}

> Cisco has confirmed to BleepingComputer that it is investigating recent claims that it suffered a breach after a threat actor began selling allegedly stolen data on a hacking forum. [...]
