Title: Russia arrests US-sanctioned Cryptex founder, 95 other linked suspects
Date: 2024-10-04T14:56:09-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-10-04-russia-arrests-us-sanctioned-cryptex-founder-95-other-linked-suspects

[Source](https://www.bleepingcomputer.com/news/security/russia-arrests-us-sanctioned-cryptex-founder-95-other-linked-suspects/){:target="_blank" rel="noopener"}

> ​Russian law enforcement detained almost 100 suspects linked to the Cryptex cryptocurrency exchange, the UAPS anonymous payment service, and 33 other online services and platforms used to make illegal payments and sell stolen credentials. [...]
