Title: Strengthening security in the era of generative AI: Must-attend sessions at re:Invent 2024
Date: 2024-10-10T16:00:06+00:00
Author: Anna Montalat
Category: AWS Security
Tags: Announcements;AWS re:Invent;Foundational (100);Security, Identity, & Compliance;Thought Leadership;AWS Re:Invent;Live Events;Security Blog
Slug: 2024-10-10-strengthening-security-in-the-era-of-generative-ai-must-attend-sessions-at-reinvent-2024

[Source](https://aws.amazon.com/blogs/security/strengthening-security-in-the-era-of-generative-ai-must-attend-sessions-at-reinvent-2024/){:target="_blank" rel="noopener"}

> / Generative AI is transforming industries in new and exciting ways every single day. At Amazon Web Services (AWS), security is our top priority, and we see security as a foundational enabler for organizations looking to innovate. As you prepare for AWS re:Invent 2024, make sure that these essential sessions are on your schedule to [...]
