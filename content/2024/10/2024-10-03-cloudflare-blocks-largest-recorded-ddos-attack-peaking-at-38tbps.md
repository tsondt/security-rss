Title: Cloudflare blocks largest recorded DDoS attack peaking at 3.8Tbps
Date: 2024-10-03T12:11:26-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-10-03-cloudflare-blocks-largest-recorded-ddos-attack-peaking-at-38tbps

[Source](https://www.bleepingcomputer.com/news/security/cloudflare-blocks-largest-recorded-ddos-attack-peaking-at-38tbps/){:target="_blank" rel="noopener"}

> During a distributed denial-of-service campaign targeting organizations in the financial services, internet, and telecommunications sectors, volumetric attacks peaked at 3.8 terabits per second, the largest publicly recorded to date. The assault consisted of a "month-long" barrage of more than 100 hyper-volumetric DDoS attacks flood. [...]
