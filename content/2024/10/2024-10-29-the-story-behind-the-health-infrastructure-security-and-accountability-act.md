Title: The story behind the Health Infrastructure Security and Accountability Act
Date: 2024-10-29T16:00:08+00:00
Author: Jay Mar Tang
Category: The Register
Tags: 
Slug: 2024-10-29-the-story-behind-the-health-infrastructure-security-and-accountability-act

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/29/hold_the_story_behind_the/){:target="_blank" rel="noopener"}

> Health care breaches lead to legislation Partner Content Breaches breed regulation; which hopefully in turn breeds meaningful change.... [...]
