Title: Undercover North Korean IT workers now steal data, extort employers
Date: 2024-10-17T14:01:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-10-17-undercover-north-korean-it-workers-now-steal-data-extort-employers

[Source](https://www.bleepingcomputer.com/news/security/undercover-north-korean-it-workers-now-steal-data-extort-employers/){:target="_blank" rel="noopener"}

> North Korean IT professionals who trick Western companies into hiring them are stealing data from the organization's network and asking for a ransom to not leak it. [...]
