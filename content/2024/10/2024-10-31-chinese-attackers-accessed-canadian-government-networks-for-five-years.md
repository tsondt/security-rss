Title: Chinese attackers accessed Canadian government networks – for five years
Date: 2024-10-31T05:34:23+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-10-31-chinese-attackers-accessed-canadian-government-networks-for-five-years

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/31/canada_cybersec_threats/){:target="_blank" rel="noopener"}

> India makes it onto list of likely threats for the first time A report by Canada's Communications Security Establishment (CSE) revealed that state-backed actors have collected valuable information from government networks for five years.... [...]
