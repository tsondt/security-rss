Title: Visit CyberThreat 2024 to hone your cybersecurity skills
Date: 2024-10-04T08:02:06+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2024-10-04-visit-cyberthreat-2024-to-hone-your-cybersecurity-skills

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/04/visit_cyberthreat_2024_to_hone/){:target="_blank" rel="noopener"}

> Get together with the European cybersecurity community at a two-day conference in London this December Sponsored Post This year's CyberThreat returns to London to provide a place for cybersecurity professionals to share experiences, new tools and techniques to help organisations stay ahead of the latest cyber threats.... [...]
