Title: Sophos to snatch Secureworks in $859M buyout: Why fight when you can just buy?
Date: 2024-10-21T21:30:15+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-10-21-sophos-to-snatch-secureworks-in-859m-buyout-why-fight-when-you-can-just-buy

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/21/sophos_buys_secureworks_thoma_bravo/){:target="_blank" rel="noopener"}

> Private equity giant Thoma Bravo adds another trophy to its growing collection British security biz Sophos has announced a plan to gobble up competitor Secureworks in an $859 million deal that will make Dell happy.... [...]
