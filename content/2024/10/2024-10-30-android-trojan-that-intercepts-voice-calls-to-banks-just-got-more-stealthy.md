Title: Android Trojan that intercepts voice calls to banks just got more stealthy
Date: 2024-10-30T19:59:03+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;android;bank fraud;malware;trojans
Slug: 2024-10-30-android-trojan-that-intercepts-voice-calls-to-banks-just-got-more-stealthy

[Source](https://arstechnica.com/information-technology/2024/10/android-trojan-that-intercepts-voice-calls-to-banks-just-got-more-stealthy/){:target="_blank" rel="noopener"}

> Researchers have found new versions of a sophisticated Android financial-fraud Trojan that’s notable for its ability to intercept calls a victim tries to place to customer-support personnel of their banks. FakeCall first came to public attention in 2022, when researchers from security firm Kaspersky reported that the malicious app wasn’t your average banking Trojan. Besides containing the usual capabilities for stealing account credentials, FakeCall could reroute voice calls to numbers controlled by the attackers. A strategic evolution The malware, available on websites masquerading as Google Play, could also simulate incoming calls from bank employees. The intention of the novel feature [...]
