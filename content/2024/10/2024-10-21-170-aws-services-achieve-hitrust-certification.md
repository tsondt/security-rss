Title: 170 AWS services achieve HITRUST certification
Date: 2024-10-21T21:55:44+00:00
Author: Mark Weech
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Auditing;AWS HITRUST;AWS HITRUST Inheritance;Compliance reports;Security Assurance;Security Blog
Slug: 2024-10-21-170-aws-services-achieve-hitrust-certification

[Source](https://aws.amazon.com/blogs/security/170-aws-services-achieve-hitrust-certification/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is excited to announce that 170 AWS services have achieved HITRUST certification for the 2024 assessment cycle, including the following 12 services that were certified for the first time: AWS AppFabric AWS Application Migration Service Amazon Bedrock AWS Clean Rooms Amazon DataZone AWS Entity Resolution AWS HealthImaging AWS IoT Device Defender [...]
