Title: Windows Themes zero-day bug exposes users to NTLM credential theft
Date: 2024-10-30T21:30:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-30-windows-themes-zero-day-bug-exposes-users-to-ntlm-credential-theft

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/30/zeroday_windows_themes/){:target="_blank" rel="noopener"}

> Plus a free micropatch until Redmond fixes the flaw There's a Windows Themes spoofing zero-day bug on the loose that allows attackers to steal people's NTLM credentials.... [...]
