Title: Average North American CISO pay now $565K, mainly thanks to one weird trick
Date: 2024-10-03T14:01:08+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-10-03-average-north-american-ciso-pay-now-565k-mainly-thanks-to-one-weird-trick

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/03/ciso_salary_survey/){:target="_blank" rel="noopener"}

> Best way to boost your package is to leave, or pretend to A survey of nearly 700 CISOs in the US and Canada has found their pay has risen over the past year to an average of $565,000 and a median of $403,000, with the top 10 percent of execs pulling in over $1 million.... [...]
