Title: How to build a Security Guardians program to distribute security ownership
Date: 2024-10-21T13:00:04+00:00
Author: Mitch Beaumont
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security;Security, Identity, & Compliance;Thought Leadership;Application security;AWS security;Security Blog
Slug: 2024-10-21-how-to-build-a-security-guardians-program-to-distribute-security-ownership

[Source](https://aws.amazon.com/blogs/security/how-to-build-your-own-security-guardians-program/){:target="_blank" rel="noopener"}

> Welcome to the second post in our series on Security Guardians, a mechanism to distribute security ownership at Amazon Web Services (AWS) that trains, develops, and empowers builder teams to make security decisions about the software that they create. In the previous post, you learned the importance of building a culture of security ownership to [...]
