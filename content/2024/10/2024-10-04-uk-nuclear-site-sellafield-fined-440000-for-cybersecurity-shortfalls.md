Title: UK nuclear site Sellafield fined $440,000 for cybersecurity shortfalls
Date: 2024-10-04T08:57:35-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-10-04-uk-nuclear-site-sellafield-fined-440000-for-cybersecurity-shortfalls

[Source](https://www.bleepingcomputer.com/news/security/uk-nuclear-site-sellafield-fined-440-000-for-cybersecurity-shortfalls/){:target="_blank" rel="noopener"}

> Nuclear waste processing facility Sellafield has been fined £332,500 ($440k) by the Office for Nuclear Regulation (ONR) for failing to adhere to cybersecurity standards and putting sensitive nuclear information at risk over four years, from 2019 to 2023. [...]
