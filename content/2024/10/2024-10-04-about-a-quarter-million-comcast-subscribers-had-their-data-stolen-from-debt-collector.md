Title: About a quarter million Comcast subscribers had their data stolen from debt collector
Date: 2024-10-04T20:13:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-04-about-a-quarter-million-comcast-subscribers-had-their-data-stolen-from-debt-collector

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/04/comcast_fcbs_ransomware_theft/){:target="_blank" rel="noopener"}

> Cable giant says ransomware involved, FBCS keeps schtum Comcast says data on 237,703 of its customers was in fact stolen in a cyberattack on a debt collector it was using, contrary to previous assurances it was given that it was unaffected by that intrusion.... [...]
