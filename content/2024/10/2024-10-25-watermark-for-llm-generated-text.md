Title: Watermark for LLM-Generated Text
Date: 2024-10-25T13:56:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence;cryptography;Google;identification;LLM
Slug: 2024-10-25-watermark-for-llm-generated-text

[Source](https://www.schneier.com/blog/archives/2024/10/watermark-for-llm-generated-text.html){:target="_blank" rel="noopener"}

> Researchers at Google have developed a watermark for LLM-generated text. The basics are pretty obvious: the LLM chooses between tokens partly based on a cryptographic key, and someone with knowledge of the key can detect those choices. What makes this hard is (1) how much text is required for the watermark to work, and (2) how robust the watermark is to post-generation editing. Google’s version looks pretty good: it’s detectable in text as small as 200 tokens. [...]
