Title: Trump campaign arms up with 'unhackable' phones after Iranian intrusion
Date: 2024-10-14T14:28:05+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-10-14-trump-campaign-arms-up-with-unhackable-phones-after-iranian-intrusion

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/14/trump_unhackable_phones/){:target="_blank" rel="noopener"}

> Florida man gets his hands on 'the best ever' With less than a month to go before American voters head to the polls to choose their next president, the Trump campaign has been investing in secure tech to make sure it doesn't get hacked again.... [...]
