Title: Announcing new Confidential Computing updates for even more hardware security options
Date: 2024-10-02T16:00:00+00:00
Author: Sam Lugani
Category: GCP Security
Tags: Security & Identity
Slug: 2024-10-02-announcing-new-confidential-computing-updates-for-even-more-hardware-security-options

[Source](https://cloud.google.com/blog/products/identity-security/new-confidential-computing-updates-for-more-hardware-security-options/){:target="_blank" rel="noopener"}

> Google Cloud is committed to ensuring that your data remains safe, secure, and firmly under your control. This begins with fortifying the very foundation of your compute infrastructure — your Compute Engine virtual machines (VMs) — with the power of Confidential Computing. Confidential Computing protects data while it’s being used and processed with a hardware-based Trusted Execution Environment (TEE). TEEs are secure and isolated environments that prevent unauthorized access or modification of applications and data while they are in use. At Google, we have been early adopters and investors in Confidential Computing products and solutions. For more than four years, [...]
