Title: Microsoft warns it lost some customer's security logs for a month
Date: 2024-10-17T18:17:29-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-10-17-microsoft-warns-it-lost-some-customers-security-logs-for-a-month

[Source](https://www.bleepingcomputer.com/news/security/microsoft-warns-it-lost-some-customers-security-logs-for-a-month/){:target="_blank" rel="noopener"}

> Microsoft is warning enterprise customers that, for almost a month, a bug caused critical logs to be partially lost, putting at risk companies that rely on this data to detect unauthorized activity. [...]
