Title: Embattled users worn down by privacy options? Let them eat code
Date: 2024-10-07T08:30:14+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2024-10-07-embattled-users-worn-down-by-privacy-options-let-them-eat-code

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/07/cookie_opinion/){:target="_blank" rel="noopener"}

> Struggle ye not with cookies, lest ye become a cookie monster Opinion The people are defeated. Worn out, deflated, and apathetic about the barrage of banners and pop-ups about cookies and permissions.... [...]
