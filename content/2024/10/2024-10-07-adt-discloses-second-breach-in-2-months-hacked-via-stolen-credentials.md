Title: ADT discloses second breach in 2 months, hacked via stolen credentials
Date: 2024-10-07T18:12:37-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-10-07-adt-discloses-second-breach-in-2-months-hacked-via-stolen-credentials

[Source](https://www.bleepingcomputer.com/news/security/adt-discloses-second-breach-in-2-months-hacked-via-stolen-credentials/){:target="_blank" rel="noopener"}

> Home and small business security company ADT disclosed it suffered a breach after threat actors gained access to its systems using stolen credentials and exfiltrated employee account data. [...]
