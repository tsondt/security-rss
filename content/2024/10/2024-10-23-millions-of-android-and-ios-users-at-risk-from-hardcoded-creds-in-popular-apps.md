Title: Millions of Android and iOS users at risk from hardcoded creds in popular apps
Date: 2024-10-23T00:31:09+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-10-23-millions-of-android-and-ios-users-at-risk-from-hardcoded-creds-in-popular-apps

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/23/android_ios_security/){:target="_blank" rel="noopener"}

> Azure Blob Storage, AWS, and Twilio keys all up for grabs An analysis of widely used mobile apps offered on Google Play and the Apple App Store has found hardcoded and unencrypted cloud service credentials, exposing millions of users to major security problems.... [...]
