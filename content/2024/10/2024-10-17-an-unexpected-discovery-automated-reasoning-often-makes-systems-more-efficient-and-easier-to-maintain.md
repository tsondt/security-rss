Title: An unexpected discovery: Automated reasoning often makes systems more efficient and easier to maintain
Date: 2024-10-17T13:00:02+00:00
Author: Byron Cook
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Thought Leadership;provable security;Security Blog
Slug: 2024-10-17-an-unexpected-discovery-automated-reasoning-often-makes-systems-more-efficient-and-easier-to-maintain

[Source](https://aws.amazon.com/blogs/security/an-unexpected-discovery-automated-reasoning-often-makes-systems-more-efficient-and-easier-to-maintain/){:target="_blank" rel="noopener"}

> During a recent visit to the Defense Advanced Research Projects Agency (DARPA), I mentioned a trend that piqued their interest: Over the last 10 years of applying automated reasoning at Amazon Web Services (AWS), we’ve found that formally verified code is often more performant than the unverified code it replaces. The reason is that the [...]
