Title: Smart TVs are spying on everyone
Date: 2024-10-09T22:15:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-10-09-smart-tvs-are-spying-on-everyone

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/09/smart_tv_spy_on_viewers/){:target="_blank" rel="noopener"}

> Regulators know this is a nightmare and have done little to stop it. Privacy advocacy group wants that to change Smart TVs are watching their viewers and harvesting their data to benefit brokers using the same ad technology that denies privacy on the internet.... [...]
