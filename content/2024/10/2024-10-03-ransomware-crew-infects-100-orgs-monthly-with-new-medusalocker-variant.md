Title: Ransomware crew infects 100+ orgs monthly with new MedusaLocker variant
Date: 2024-10-03T10:00:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-03-ransomware-crew-infects-100-orgs-monthly-with-new-medusalocker-variant

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/03/ransomware_spree_infects_100_orgs/){:target="_blank" rel="noopener"}

> Crooks 'like a sysadmin, with a malicious slant' Exclusive An extortionist armed with a new variant of MedusaLocker ransomware has infected more than 100 organizations a month since at least 2022, according to Cisco Talos, which recently discovered a "substantial" Windows credential data dump that sheds light on the criminal and their victims.... [...]
