Title: 'Satanic' data thief claims to have slipped into 350M Hot Topic shoppers info
Date: 2024-10-23T20:30:15+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-10-23-satanic-data-thief-claims-to-have-slipped-into-350m-hot-topic-shoppers-info

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/23/satanic_data_thief/){:target="_blank" rel="noopener"}

> We know where you got your skinny jeans - big deal A data thief calling themselves Satanic claims to have purloined the records of around 350 million customers of fashion retailer Hot Topic.... [...]
