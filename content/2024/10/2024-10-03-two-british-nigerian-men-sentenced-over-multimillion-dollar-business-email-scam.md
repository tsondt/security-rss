Title: Two British-Nigerian men sentenced over multimillion-dollar business email scam
Date: 2024-10-03T12:30:18+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-03-two-british-nigerian-men-sentenced-over-multimillion-dollar-business-email-scam

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/03/british_nigerian_scammers_sentenced/){:target="_blank" rel="noopener"}

> Fraudsters targeted local government, colleges, and construction firms in Texas and North Carolina Two British-Nigerian men were sentenced for serious business email compromise schemes in the US this week, netting them millions of dollars from local government entities, construction companies, and colleges.... [...]
