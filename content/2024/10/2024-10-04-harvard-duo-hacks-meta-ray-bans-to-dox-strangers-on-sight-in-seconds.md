Title: Harvard duo hacks Meta Ray-Bans to dox strangers on sight in seconds
Date: 2024-10-04T06:32:05+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-10-04-harvard-duo-hacks-meta-ray-bans-to-dox-strangers-on-sight-in-seconds

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/04/harvard_engineer_meta_smart_glasses/){:target="_blank" rel="noopener"}

> 'You can build this in a few days – even as a very naïve developer' A pair of inventive Harvard undergraduates have created what they believe could be one of the most intrusive devices ever built – a wake-up call, they tell The Register, for the world to take privacy seriously in the AI era.... [...]
