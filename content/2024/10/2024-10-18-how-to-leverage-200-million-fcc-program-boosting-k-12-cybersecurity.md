Title: How to leverage $200 million FCC program boosting K-12 cybersecurity
Date: 2024-10-18T10:01:11-04:00
Author: Sponsored by Cynet
Category: BleepingComputer
Tags: Security
Slug: 2024-10-18-how-to-leverage-200-million-fcc-program-boosting-k-12-cybersecurity

[Source](https://www.bleepingcomputer.com/news/security/how-to-leverage-200-million-fcc-program-boosting-k-12-cybersecurity/){:target="_blank" rel="noopener"}

> In 2024, the Federal Communications Commission (FCC) launched the K-12 Cybersecurity Pilot Program, a groundbreaking initiative backed by $200 million in funding. Learn more from Cynet about how schools and libraries can apply to this program. [...]
