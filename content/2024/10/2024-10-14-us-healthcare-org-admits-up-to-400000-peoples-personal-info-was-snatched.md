Title: US healthcare org admits up to 400,000 people's personal info was snatched
Date: 2024-10-14T22:03:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-14-us-healthcare-org-admits-up-to-400000-peoples-personal-info-was-snatched

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/14/gryphon_healthcare_breach/){:target="_blank" rel="noopener"}

> It waited till just before Columbus Day weekend to make mandated filing, but don't worry, we saw it A Houston-based services provider to healthcare organizations says a crook may have grabbed up to 400,000 people's information after the miscreant accessed the systems of one of its customers.... [...]
