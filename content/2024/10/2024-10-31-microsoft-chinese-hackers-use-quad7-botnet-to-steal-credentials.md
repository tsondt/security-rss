Title: Microsoft: Chinese hackers use Quad7 botnet to steal credentials
Date: 2024-10-31T16:03:43-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-10-31-microsoft-chinese-hackers-use-quad7-botnet-to-steal-credentials

[Source](https://www.bleepingcomputer.com/news/security/microsoft-chinese-hackers-use-quad7-botnet-to-steal-credentials/){:target="_blank" rel="noopener"}

> Microsoft warns that Chinese threat actors use the Quad7 botnet, compromised of hacked SOHO routers, to steal credentials in password-spray attacks. [...]
