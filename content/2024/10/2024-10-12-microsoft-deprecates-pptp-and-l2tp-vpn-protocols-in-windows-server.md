Title: Microsoft deprecates PPTP and L2TP VPN protocols in Windows Server
Date: 2024-10-12T11:25:36-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-10-12-microsoft-deprecates-pptp-and-l2tp-vpn-protocols-in-windows-server

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-deprecates-pptp-and-l2tp-vpn-protocols-in-windows-server/){:target="_blank" rel="noopener"}

> Microsoft has officially deprecated the Point-to-Point Tunneling Protocol (PPTP) and Layer 2 Tunneling Protocol (L2TP) in future versions of Windows Server, recommending admins switch to different protocols that offer increased security. [...]
