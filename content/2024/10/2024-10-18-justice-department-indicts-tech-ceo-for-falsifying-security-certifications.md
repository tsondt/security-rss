Title: Justice Department Indicts Tech CEO for Falsifying Security Certifications
Date: 2024-10-18T13:58:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;auditing;business of security;certifications;fraud
Slug: 2024-10-18-justice-department-indicts-tech-ceo-for-falsifying-security-certifications

[Source](https://www.schneier.com/blog/archives/2024/10/justice-department-indicts-tech-ceo-for-falsifying-security-certifications.html){:target="_blank" rel="noopener"}

> The Wall Street Journal is reporting that the CEO of a still unnamed company has been indicted for creating a fake auditing company to falsify security certifications in order to win government business. [...]
