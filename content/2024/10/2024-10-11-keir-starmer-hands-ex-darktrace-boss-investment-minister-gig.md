Title: Keir Starmer hands ex-Darktrace boss investment minister gig
Date: 2024-10-11T11:13:42+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-11-keir-starmer-hands-ex-darktrace-boss-investment-minister-gig

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/11/darktrace_investment_minister/){:target="_blank" rel="noopener"}

> What's harder? Convincing people to invest in a beleaguered security business or a tiny island everybody hates? Keir Starmer's decision to appoint Poppy Gustafsson as the UK's new investment minister is being resoundingly praised despite the former Darktrace boss spending years failing to fully rebuild investor confidence in the embattled company.... [...]
