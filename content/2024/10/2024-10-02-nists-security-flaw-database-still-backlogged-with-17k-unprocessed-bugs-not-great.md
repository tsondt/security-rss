Title: NIST's security flaw database still backlogged with 17K+ unprocessed bugs. Not great
Date: 2024-10-02T12:31:05+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-02-nists-security-flaw-database-still-backlogged-with-17k-unprocessed-bugs-not-great

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/02/cve_pileup_nvd_missed_deadline/){:target="_blank" rel="noopener"}

> Logjam 'hurting infosec processes world over' one expert tells us as US body blows its own Sept deadline NIST has made some progress clearing its backlog of security vulnerability reports to process – though it's not quite on target as hoped.... [...]
