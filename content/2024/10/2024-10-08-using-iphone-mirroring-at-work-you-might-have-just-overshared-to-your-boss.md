Title: Using iPhone Mirroring at work? You might have just overshared to your boss
Date: 2024-10-08T18:30:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-08-using-iphone-mirroring-at-work-you-might-have-just-overshared-to-your-boss

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/08/iphone_mirroring_at_work/){:target="_blank" rel="noopener"}

> What does IT glimpse but a dating app on your wee little screen If you're using iPhone Mirroring at work: It's time to stop, lest you give your employer's IT department the capability to snoop through the list of apps you have on your phone — dating apps, those tracking medical conditions or sexual history, or any other NSFW apps that you might want to keep to yourself.... [...]
