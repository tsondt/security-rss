Title: DrayTek fixed critical flaws in over 700,000 exposed routers
Date: 2024-10-02T09:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2024-10-02-draytek-fixed-critical-flaws-in-over-700000-exposed-routers

[Source](https://www.bleepingcomputer.com/news/security/draytek-fixed-critical-flaws-in-over-700-000-exposed-routers/){:target="_blank" rel="noopener"}

> DrayTek has released security updates for multiple router models to address 14 vulnerabilities of varying severity, including a remote code execution flaw that received the maximum CVSS score of 10. [...]
