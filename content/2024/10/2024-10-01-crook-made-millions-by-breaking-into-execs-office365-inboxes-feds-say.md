Title: Crook made millions by breaking into execs’ Office365 inboxes, feds say
Date: 2024-10-01T19:40:59+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;insider trading;password resets;passwords
Slug: 2024-10-01-crook-made-millions-by-breaking-into-execs-office365-inboxes-feds-say

[Source](https://arstechnica.com/?p=2053721){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Federal prosecutors have charged a man for an alleged “hack-to-trade” scheme that earned him millions of dollars by breaking into the Office365 accounts of executives at publicly traded companies and obtaining quarterly financial reports before they were released publicly. The action, taken by the office of the US Attorney for the district of New Jersey, accuses UK national Robert B. Westbrook of earning roughly $3.75 million in 2019 and 2020 from stock trades that capitalized on the illicitly obtained information. After accessing it, prosecutors said, he executed stock trades. The advance notice allowed him to act [...]
