Title: Intel, AMD CPUs on Linux impacted by newly disclosed Spectre bypass
Date: 2024-10-18T10:48:13-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2024-10-18-intel-amd-cpus-on-linux-impacted-by-newly-disclosed-spectre-bypass

[Source](https://www.bleepingcomputer.com/news/security/intel-amd-cpus-on-linux-impacted-by-newly-disclosed-spectre-bypass/){:target="_blank" rel="noopener"}

> The latest generations of Intel processors, including Xeon chips, and AMD's older Zen 1, Zen 1+, and Zen 2 microarchitectures on Linux are vulnerable to new speculative execution attacks that bypass existing 'Spectre' mitigations. [...]
