Title: Location tracking of phones is out of control. Here’s how to fight back.
Date: 2024-10-23T23:03:15+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Apple;Biz & IT;Google;Security;location;Phones;privacy;surveillance
Slug: 2024-10-23-location-tracking-of-phones-is-out-of-control-heres-how-to-fight-back

[Source](https://arstechnica.com/information-technology/2024/10/phone-tracking-tool-lets-government-agencies-follow-your-every-move/){:target="_blank" rel="noopener"}

> You likely have never heard of Babel Street or Location X, but chances are good that they know a lot about you and anyone else you know who keeps a phone nearby around the clock. Reston, Virginia-located Babel Street is the little-known firm behind Location X, a service with the capability to track the locations of hundreds of millions of phone users over sustained periods of time. Ostensibly, Babel Street limits the use of the service to personnel and contractors of US government law enforcement agencies, including state entities. Despite the restriction, an individual working on behalf of a company [...]
