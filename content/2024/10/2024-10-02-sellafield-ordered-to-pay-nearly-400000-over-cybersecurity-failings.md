Title: Sellafield ordered to pay nearly £400,000 over cybersecurity failings
Date: 2024-10-02T16:18:38+00:00
Author: Anna Isaac and Alex Lawson
Category: The Guardian
Tags: Nuclear power;Nuclear waste;Business;Energy;Environment;Waste;UK news;Cybercrime;Cyberwar;Data and computer security;Law;Energy industry
Slug: 2024-10-02-sellafield-ordered-to-pay-nearly-400000-over-cybersecurity-failings

[Source](https://www.theguardian.com/business/2024/oct/02/sellafield-ordered-to-pay-nearly-400000-over-cybersecurity-failings){:target="_blank" rel="noopener"}

> Nuclear waste dump in Cumbria pleaded guilty to leaving data that could threaten national security exposed for four years, says regulator Sellafield will have to pay almost £400,000 after it pleaded guilty to criminal charges over years of cybersecurity failings at Britain’s most hazardous nuclear site. The vast nuclear waste dump in Cumbria left information that could threaten national security exposed for four years, according to the industry regulator, which brought the charges. It was also found that 75% of its computer servers were vulnerable to cyber-attack. Continue reading... [...]
