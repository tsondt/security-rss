Title: Hackers blackmail Globe Life after stealing customer data
Date: 2024-10-17T10:32:25-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-10-17-hackers-blackmail-globe-life-after-stealing-customer-data

[Source](https://www.bleepingcomputer.com/news/security/hackers-blackmail-globe-life-after-stealing-customer-data/){:target="_blank" rel="noopener"}

> Insurance giant Globe Life says an unknown threat actor attempted to extort money in exchange for not publishing data stolen from the company's systems earlier this year. [...]
