Title: Hacking ChatGPT by Planting False Memories into Its Data
Date: 2024-10-01T11:07:34+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;LLM;vulnerabilities
Slug: 2024-10-01-hacking-chatgpt-by-planting-false-memories-into-its-data

[Source](https://www.schneier.com/blog/archives/2024/10/hacking-chatgpt-by-planting-false-memories-into-its-data.html){:target="_blank" rel="noopener"}

> This vulnerability hacks a feature that allows ChatGPT to have long-term memory, where it uses information from past conversations to inform future conversations with that same user. A researcher found that he could use that feature to plant “false memories” into that context window that could subvert the model. A month later, the researcher submitted a new disclosure statement. This time, he included a PoC that caused the ChatGPT app for macOS to send a verbatim copy of all user input and ChatGPT output to a server of his choice. All a target needed to do was instruct the LLM [...]
