Title: Tracking World Leaders Using Strava
Date: 2024-10-31T15:16:25+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;data privacy;tracking
Slug: 2024-10-31-tracking-world-leaders-using-strava

[Source](https://www.schneier.com/blog/archives/2024/10/tracking-world-leaders-using-strava.html){:target="_blank" rel="noopener"}

> Way back in 2018, people noticed that you could find secret military bases using data published by the Strava fitness app. Soldiers and other military personal were using them to track their runs, and you could look at the public data and find places where there should be no people running. Six years later, the problem remains. Le Monde has reported that the same Strava data can be used to track the movements of world leaders. They don’t wear the tracking device, but many of their bodyguards do. [...]
