Title: Summer 2024 SOC 1 report now available in Japanese, Korean, and Spanish
Date: 2024-10-04T17:05:50+00:00
Author: Brownell Combs
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS SOC Reports;Compliance;Security Blog;SOC
Slug: 2024-10-04-summer-2024-soc-1-report-now-available-in-japanese-korean-and-spanish

[Source](https://aws.amazon.com/blogs/security/summer-2024-soc-1-report-now-available-in-japanese-korean-and-spanish/){:target="_blank" rel="noopener"}

> Japanese | Korean | Spanish At Amazon Web Services (AWS), we continue to listen to our customers, regulators, and stakeholders to understand their needs regarding audit, assurance, certification, and attestation programs. We are pleased to announce that the AWS System and Organization Controls (SOC) 1 report is now available in Japanese, Korean, and Spanish. This [...]
