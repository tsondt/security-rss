Title: Feds reach for sliver of crypto-cash nicked by North Korea's notorious Lazarus Group
Date: 2024-10-08T00:27:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-08-feds-reach-for-sliver-of-crypto-cash-nicked-by-north-koreas-notorious-lazarus-group

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/08/us_lazarus_group_crypto_seizure/){:target="_blank" rel="noopener"}

> A couple million will do for a start... but Kim's crews are suspected of stealing much more The US government is attempting to claw back more than $2.67 million stolen by North Korea's Lazarus Group, filing two lawsuits to force the forfeiture of millions in Tether and Bitcoin.... [...]
