Title: Google warns uBlock Origin and other extensions may be disabled soon
Date: 2024-10-13T18:16:27-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Google;Security;Software
Slug: 2024-10-13-google-warns-ublock-origin-and-other-extensions-may-be-disabled-soon

[Source](https://www.bleepingcomputer.com/news/google/google-warns-ublock-origin-and-other-extensions-may-be-disabled-soon/){:target="_blank" rel="noopener"}

> Google's Chrome Web Store is now warning that the uBlock Origin ad blocker and other extensions may soon be blocked as part of the company's deprecation of the Manifest V2 extension specification. [...]
