Title: California AI Safety Bill Vetoed
Date: 2024-10-02T11:01:30+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;EU;laws
Slug: 2024-10-02-california-ai-safety-bill-vetoed

[Source](https://www.schneier.com/blog/archives/2024/10/california-ai-safety-bill-vetoed.html){:target="_blank" rel="noopener"}

> Governor Newsom has vetoed the state’s AI safety bill. I have mixed feelings about the bill. There’s a lot to like about it, and I want governments to regulate in this space. But, for now, it’s all EU. (Related, the Council of Europe treaty on AI is ready for signature. It’ll be legally binding when signed, and it’s a big deal.) [...]
