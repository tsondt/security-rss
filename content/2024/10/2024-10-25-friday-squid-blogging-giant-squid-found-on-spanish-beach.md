Title: Friday Squid Blogging: Giant Squid Found on Spanish Beach
Date: 2024-10-25T21:01:14+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-10-25-friday-squid-blogging-giant-squid-found-on-spanish-beach

[Source](https://www.schneier.com/blog/archives/2024/10/friday-squid-blogging-giant-squid-found-on-spanish-beach.html){:target="_blank" rel="noopener"}

> A giant squid has washed up on a beach in Northern Spain. Blog moderation policy. [...]
