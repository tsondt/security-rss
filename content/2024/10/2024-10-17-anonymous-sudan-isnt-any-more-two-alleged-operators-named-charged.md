Title: Anonymous Sudan isn't any more: Two alleged operators named, charged
Date: 2024-10-17T07:27:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-10-17-anonymous-sudan-isnt-any-more-two-alleged-operators-named-charged

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/17/anonymous_sudan_arrests_charges/){:target="_blank" rel="noopener"}

> Gang said to have developed its evilware on GitHub – then DDoSed GitHub Hacktivist gang Anonymous Sudan appears to have lost its anonymity after the US Attorney's Office on Wednesday unsealed an indictment identifying two of its alleged operators.... [...]
