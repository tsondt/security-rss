Title: Google Scholar has a 'verified email' for Sir Isaac Newton
Date: 2024-10-19T09:00:00-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2024-10-19-google-scholar-has-a-verified-email-for-sir-isaac-newton

[Source](https://www.bleepingcomputer.com/news/security/google-scholar-has-a-verified-email-for-sir-isaac-newton/){:target="_blank" rel="noopener"}

> It's true: Google Scholar profile of the renowned former physicist and polymath, Sir Isaac Newton bears a "verified email" note. According to Google Scholar, Isaac Newton is a "Professor of Physics, MIT" with a "Verified email at mit.edu." [...]
