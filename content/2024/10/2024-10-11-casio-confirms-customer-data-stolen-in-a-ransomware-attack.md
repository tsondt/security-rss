Title: Casio confirms customer data stolen in a ransomware attack
Date: 2024-10-11T10:46:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-10-11-casio-confirms-customer-data-stolen-in-a-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/casio-confirms-customer-data-stolen-in-a-ransomware-attack/){:target="_blank" rel="noopener"}

> Casio now confirms it suffered a ransomware attack earlier this month, warning that the personal and confidential data of employees, job candidates, and some customers was also stolen. [...]
