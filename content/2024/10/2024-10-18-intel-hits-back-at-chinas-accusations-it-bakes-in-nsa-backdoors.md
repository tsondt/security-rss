Title: Intel hits back at China's accusations it bakes in NSA backdoors
Date: 2024-10-18T05:32:13+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-10-18-intel-hits-back-at-chinas-accusations-it-bakes-in-nsa-backdoors

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/18/intel_china_security_allegations/){:target="_blank" rel="noopener"}

> Chipzilla says it obeys the law wherever it is, which is nice Intel has responded to Chinese claims that its chips include security backdoors at the direction of America's NSA.... [...]
