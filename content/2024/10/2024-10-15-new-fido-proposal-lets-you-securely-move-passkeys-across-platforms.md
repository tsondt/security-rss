Title: New FIDO proposal lets you securely move passkeys across platforms
Date: 2024-10-15T11:18:59-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Technology
Slug: 2024-10-15-new-fido-proposal-lets-you-securely-move-passkeys-across-platforms

[Source](https://www.bleepingcomputer.com/news/security/new-fido-proposal-lets-you-securely-move-passkeys-across-platforms/){:target="_blank" rel="noopener"}

> The Fast IDentity Online (FIDO) Alliance has published a working draft of a new specification that aims to enable the secure transfer of passkeys between different providers. [...]
