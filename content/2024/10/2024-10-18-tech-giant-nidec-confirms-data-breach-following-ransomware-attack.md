Title: Tech giant Nidec confirms data breach following ransomware attack
Date: 2024-10-18T12:37:35-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-10-18-tech-giant-nidec-confirms-data-breach-following-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/tech-giant-nidec-confirms-data-breach-following-ransomware-attack/){:target="_blank" rel="noopener"}

> Nidec Corporation is informing that hackers behind a ransomware attack is suffered earlier this year stole data and leaked it on the dark web. [...]
