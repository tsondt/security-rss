Title: CISA: Hackers abuse F5 BIG-IP cookies to map internal servers
Date: 2024-10-11T12:27:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-10-11-cisa-hackers-abuse-f5-big-ip-cookies-to-map-internal-servers

[Source](https://www.bleepingcomputer.com/news/security/cisa-hackers-abuse-f5-big-ip-cookies-to-map-internal-servers/){:target="_blank" rel="noopener"}

> CISA is warning that threat actors have been observed abusing unencrypted persistent F5 BIG-IP cookies to identify and target other internal devices on the targeted network. [...]
