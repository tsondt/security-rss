Title: RAC duo busted for stealing and selling crash victims' data
Date: 2024-10-11T11:45:16+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2024-10-11-rac-duo-busted-for-stealing-and-selling-crash-victims-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/11/rac_worker_convictions/){:target="_blank" rel="noopener"}

> Roadside assistance biz praised for deploying security monitoring software and reporting workers to cops Two former workers at roadside assistance provider RAC were this week given suspended sentences after illegally copying and selling tens of thousands of lines of personal data on people involved in accidents.... [...]
