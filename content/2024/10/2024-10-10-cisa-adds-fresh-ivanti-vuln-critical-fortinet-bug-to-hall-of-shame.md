Title: CISA adds fresh Ivanti vuln, critical Fortinet bug to hall of shame
Date: 2024-10-10T13:34:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-10-cisa-adds-fresh-ivanti-vuln-critical-fortinet-bug-to-hall-of-shame

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/10/cisa_ivanti_fortinet_vulns/){:target="_blank" rel="noopener"}

> Usual three-week window to address significant risks to federal agencies applies The US Cybersecurity and Infrastructure Security Agency (CISA) says vulnerabilities in Fortinet and Ivanti products are now being exploited, earning them places in its Known Exploited Vulnerabilities (KEV) catalog.... [...]
