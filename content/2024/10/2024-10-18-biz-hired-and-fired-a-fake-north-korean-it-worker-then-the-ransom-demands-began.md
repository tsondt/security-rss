Title: Biz hired, and fired, a fake North Korean IT worker – then the ransom demands began
Date: 2024-10-18T04:28:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-18-biz-hired-and-fired-a-fake-north-korean-it-worker-then-the-ransom-demands-began

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/18/ransom_fake_it_worker_scam/){:target="_blank" rel="noopener"}

> 'My webcam isn't working today' is the new 'The dog ate my network' It's a pattern cropping up more and more frequently: a company fills an IT contractor post, not realizing it's mistakenly hired a North Korean operative. The phony worker almost immediately begins exfiltrating sensitive data, before being fired for poor performance. Then the six-figure ransom demands – accompanied by proof of the stolen files – start appearing.... [...]
