Title: ‘Pig butchering’ trading apps found on Google Play, App Store
Date: 2024-10-03T15:36:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2024-10-03-pig-butchering-trading-apps-found-on-google-play-app-store

[Source](https://www.bleepingcomputer.com/news/security/pig-butchering-trading-apps-found-on-google-play-app-store/){:target="_blank" rel="noopener"}

> Fake trading apps on Google Play and Apple's App Store lure victims into "pig butchering" scams that have a global reach. [...]
