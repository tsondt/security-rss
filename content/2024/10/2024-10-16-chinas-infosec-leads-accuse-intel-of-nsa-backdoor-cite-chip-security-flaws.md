Title: China’s infosec leads accuse Intel of NSA backdoor, cite chip security flaws
Date: 2024-10-16T18:30:15+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-16-chinas-infosec-leads-accuse-intel-of-nsa-backdoor-cite-chip-security-flaws

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/16/china_intel_chip_security/){:target="_blank" rel="noopener"}

> Uncle Sam having a secret way into US tech? Say it ain't so A Chinese industry group has accused Intel of backdooring its CPUs, in addition to other questionable security practices while calling for an investigation into the chipmaker, claiming its products pose "serious risks to national security."... [...]
