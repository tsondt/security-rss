Title: Effective Fuzzing: A Dav1d Case Study
Date: 2024-10-03T10:01:00-07:00
Author: Unknown (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2024-10-03-effective-fuzzing-a-dav1d-case-study

[Source](https://googleprojectzero.blogspot.com/2024/10/effective-fuzzing-dav1d-case-study.html){:target="_blank" rel="noopener"}

> Guest post by Nick Galloway, Senior Security Engineer, 20% time on Project Zero Late in 2023, while working on a 20% project with Project Zero, I found an integer overflow in the dav1d AV1 video decoder. That integer overflow leads to an out-of-bounds write to memory. Dav1d 1.4.0 patched this, and it was assigned CVE-2024-1580. After the disclosure, I received some questions about how this issue was discovered, since dav1d is already being fuzzed by at least oss-fuzz. This blog post explains what happened. It’s a useful case study in how to construct fuzzers to exercise as much code as [...]
