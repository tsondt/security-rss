Title: Two simple give-me-control security bugs found in Optigo network switches used in critical manufacturing
Date: 2024-10-02T20:39:50+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-10-02-two-simple-give-me-control-security-bugs-found-in-optigo-network-switches-used-in-critical-manufacturing

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/02/cisa_optigo_switch_flaws/){:target="_blank" rel="noopener"}

> Poor use of PHP include() strikes again Two trivial but critical security holes have been found in Optigo's Spectra Aggregation Switch, and so far no patch is available.... [...]
