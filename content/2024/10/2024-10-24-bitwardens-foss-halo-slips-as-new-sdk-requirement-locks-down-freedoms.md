Title: Bitwarden's FOSS halo slips as new SDK requirement locks down freedoms
Date: 2024-10-24T11:36:14+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2024-10-24-bitwardens-foss-halo-slips-as-new-sdk-requirement-locks-down-freedoms

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/24/bitwarden_foss_doubts/){:target="_blank" rel="noopener"}

> Arguments continue but change suggests it's not Free Software anymore The Bitwarden online credentials storage service is changing its build requirements – which some commentators feel mean it's no longer FOSS.... [...]
