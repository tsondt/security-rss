Title: Beijing claims it's found 'underwater lighthouses' that its foes use for espionage
Date: 2024-10-30T08:31:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-10-30-beijing-claims-its-found-underwater-lighthouses-that-its-foes-use-for-espionage

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/30/china_seabed_surveillance_device_claims/){:target="_blank" rel="noopener"}

> Release the Kraken! China has accused unnamed foreign entities of using devices hidden in the seabed and bobbing on the waves to learn its maritime secrets.... [...]
