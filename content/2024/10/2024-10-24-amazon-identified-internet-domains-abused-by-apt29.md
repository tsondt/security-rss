Title: Amazon identified internet domains abused by APT29
Date: 2024-10-24T16:49:00+00:00
Author: CJ Moses
Category: AWS Security
Tags: Security, Identity, & Compliance;Security Blog
Slug: 2024-10-24-amazon-identified-internet-domains-abused-by-apt29

[Source](https://aws.amazon.com/blogs/security/amazon-identified-internet-domains-abused-by-apt29/){:target="_blank" rel="noopener"}

> APT29 aka Midnight Blizzard recently attempted to phish thousands of people. Building on work by CERT-UA, Amazon recently identified internet domains abused by APT29, a group widely attributed to Russia’s Foreign Intelligence Service (SVR). In this instance, their targets were associated with government agencies, enterprises, and militaries, and the phishing campaign was apparently aimed at [...]
