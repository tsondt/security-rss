Title: The run-up to my prostate examination | Brief letters
Date: 2024-10-11T16:10:07+00:00
Author: Guardian Staff
Category: The Guardian
Tags: Doctors;Race;Mobile phones;Crime;Data and computer security;UK news
Slug: 2024-10-11-the-run-up-to-my-prostate-examination-brief-letters

[Source](https://www.theguardian.com/society/2024/oct/11/the-run-up-to-my-prostate-examination){:target="_blank" rel="noopener"}

> At the doctor’s | Phone safety | Passwords | Heinz advert Lucy Mangan, writing about her doctor joking about her kidneys ( Digested week, 4 October ), reminded me of having an examination for an enlarged prostate. As I lay on the couch waiting for the procedure, my doctor said: “In accordance with current NHS guidelines, I have to take a run-up.” It made me feel less discomfited. David Noonan Earley, Berkshire • En route for Colombia in 2018, I threaded a chain through the case of my new iPhone. I kept the chain looped round my wrist to reduce [...]
