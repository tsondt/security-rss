Title: Chinese believed to have targeted Trump’s and Vance’s phones in US telecommunications breach
Date: 2024-10-25T19:58:01+00:00
Author: Léonie Chao-Fong in Washington DC
Category: The Guardian
Tags: Donald Trump;JD Vance;Data and computer security;Verizon Communications;AT&T;Hacking;US politics;US news;US elections 2024
Slug: 2024-10-25-chinese-believed-to-have-targeted-trumps-and-vances-phones-in-us-telecommunications-breach

[Source](https://www.theguardian.com/us-news/2024/oct/25/china-hack-trump-vance){:target="_blank" rel="noopener"}

> Trump campaign immediately blamed Biden White House and Kamala Harris for Chinese government-linked hack Chinese government-linked hackers are believed to have targeted phones used by Donald Trump and his running mate, JD Vance, as part of a larger breach of US telecommunications networks, according to a New York Times report. The Trump campaign was informed this week that the phone numbers of the Republican presidential and vice-presidential nominee were among those targeted during a breach of the Verizon network, the paper said, citing sources. Continue reading... [...]
