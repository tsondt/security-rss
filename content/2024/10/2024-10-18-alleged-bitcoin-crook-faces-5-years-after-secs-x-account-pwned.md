Title: Alleged Bitcoin crook faces 5 years after SEC's X account pwned
Date: 2024-10-18T12:30:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-18-alleged-bitcoin-crook-faces-5-years-after-secs-x-account-pwned

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/18/sec_bitcoin_arrest/){:target="_blank" rel="noopener"}

> SIM swappers strike again, warping cryptocurrency prices An Alabama man faces five years in prison for allegedly attempting to manipulate the price of Bitcoin by pwning the US Securities and Exchange Commission's X account earlier this year.... [...]
