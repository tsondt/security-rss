Title: AWS Cloud Development Kit flaw exposed accounts to full takeover
Date: 2024-10-24T22:33:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-24-aws-cloud-development-kit-flaw-exposed-accounts-to-full-takeover

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/24/aws_cloud_development_kit_flaw/){:target="_blank" rel="noopener"}

> Remember Bucket Monopoly? Yeah, it gets worse Amazon Web Services has fixed a flaw in its open source Cloud Development Kit that, under the right conditions, could allow an attacker to hijack a user's account completely.... [...]
