Title: More Details on Israel Sabotaging Hezbollah Pagers and Walkie-Talkies
Date: 2024-10-15T11:06:44+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;bombs;Internet of Things;Israel;sabotage
Slug: 2024-10-15-more-details-on-israel-sabotaging-hezbollah-pagers-and-walkie-talkies

[Source](https://www.schneier.com/blog/archives/2024/10/more-details-on-israel-sabotaging-hezbollah-pagers-and-walkie-talkies.html){:target="_blank" rel="noopener"}

> The Washington Post has a long and detailed story about the operation that’s well worth reading (alternate version here ). The sales pitch came from a marketing official trusted by Hezbollah with links to Apollo. The marketing official, a woman whose identity and nationality officials declined to reveal, was a former Middle East sales representative for the Taiwanese firm who had established her own company and acquired a license to sell a line of pagers that bore the Apollo brand. Sometime in 2023, she offered Hezbollah a deal on one of the products her firm sold: the rugged and reliable [...]
