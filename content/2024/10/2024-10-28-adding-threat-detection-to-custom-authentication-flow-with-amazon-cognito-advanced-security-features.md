Title: Adding threat detection to custom authentication flow with Amazon Cognito advanced security features
Date: 2024-10-28T22:06:21+00:00
Author: Vishal Jakharia
Category: AWS Security
Tags: Advanced (300);Amazon Cognito;Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2024-10-28-adding-threat-detection-to-custom-authentication-flow-with-amazon-cognito-advanced-security-features

[Source](https://aws.amazon.com/blogs/security/adding-threat-detection-to-custom-authentication-flow-with-amazon-cognito-advanced-security-features/){:target="_blank" rel="noopener"}

> Recently, passwordless authentication has gained popularity compared to traditional password-based authentication methods. Application owners can add user management to their applications while offloading most of the security heavy-lifting to Amazon Cognito. You can use Amazon Cognito to customize user authentication flow by implementing passwordless authentication. Amazon Cognito enhances the security posture of your applications because [...]
