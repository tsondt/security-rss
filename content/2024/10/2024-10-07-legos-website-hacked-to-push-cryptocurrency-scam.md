Title: LEGO's website hacked to push cryptocurrency scam
Date: 2024-10-07T17:48:33-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-10-07-legos-website-hacked-to-push-cryptocurrency-scam

[Source](https://www.bleepingcomputer.com/news/security/legos-website-hacked-to-push-cryptocurrency-scam/){:target="_blank" rel="noopener"}

> On Friday night, cryptocurrency scammers briefly hacked the LEGO website to promote a fake Lego token that could be purchased with Ethereum. [...]
