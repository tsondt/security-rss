Title: IBM acquires Indian SaaS startup Prescinto to shine a light on renewable energy assets
Date: 2024-10-16T05:25:08+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-10-16-ibm-acquires-indian-saas-startup-prescinto-to-shine-a-light-on-renewable-energy-assets

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/16/ibm_aquires_prescinto/){:target="_blank" rel="noopener"}

> Also: Crypto-hub Binance helps Delhi police shut down solar power scam IBM announced on Tuesday it has acquired Prescinto – a Bangalore-based provider of asset performance management software for renewable energy.... [...]
