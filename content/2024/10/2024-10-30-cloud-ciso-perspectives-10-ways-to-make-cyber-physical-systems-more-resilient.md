Title: Cloud CISO Perspectives: 10 ways to make cyber-physical systems more resilient
Date: 2024-10-30T16:00:00+00:00
Author: Anton Chuvakin
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2024-10-30-cloud-ciso-perspectives-10-ways-to-make-cyber-physical-systems-more-resilient

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-10-ways-to-make-cyber-physical-systems-more-resilient/){:target="_blank" rel="noopener"}

> Welcome to the second Cloud CISO Perspectives for October 2024. Today, Anton Chuvakin, senior security consultant for our Office of the CISO, offers 10 leading indicators to improve cyber-physical systems, guided by our analysis of the White House’s new PCAST report. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI Security & CISO, Google Cloud aside_block <ListValue: [StructValue([('title', 'Get vital CISO Insights with Google Cloud'), ('body', <wagtail.rich_text.RichText object [...]
