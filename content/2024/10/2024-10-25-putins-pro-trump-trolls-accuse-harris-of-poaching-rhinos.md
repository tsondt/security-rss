Title: Putin's pro-Trump trolls accuse Harris of poaching rhinos
Date: 2024-10-25T01:30:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-25-putins-pro-trump-trolls-accuse-harris-of-poaching-rhinos

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/25/russia_china_iran_election_disinfo/){:target="_blank" rel="noopener"}

> Plus: Iran's IRGC probes election-related websites in swing states Russian, Iranian, and Chinese trolls are all ramping up their US election disinformation efforts ahead of November 5, but – aside from undermining faith in the democratic process and confidence in the election result – with very different objectives, according to Microsoft.... [...]
