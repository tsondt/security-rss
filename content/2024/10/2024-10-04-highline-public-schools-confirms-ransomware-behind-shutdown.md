Title: Highline Public Schools confirms ransomware behind shutdown
Date: 2024-10-04T16:32:53-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-10-04-highline-public-schools-confirms-ransomware-behind-shutdown

[Source](https://www.bleepingcomputer.com/news/security/highline-public-schools-confirms-ransomware-attack-was-behind-september-shut-down/){:target="_blank" rel="noopener"}

> On Thursday, K-12 school district Highline Public Schools confirmed that a ransomware attack forced it to shut down all schools in early September. [...]
