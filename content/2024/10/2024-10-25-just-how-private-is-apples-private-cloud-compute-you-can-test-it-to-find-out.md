Title: Just how private is Apple's Private Cloud Compute? You can test it to find out
Date: 2024-10-25T15:04:14+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-10-25-just-how-private-is-apples-private-cloud-compute-you-can-test-it-to-find-out

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/25/apple_private_cloud_compute/){:target="_blank" rel="noopener"}

> Also updates bug bounty program with $1M payout In June, Apple used its Worldwide Developer Conference to announce the creation of the Private Cloud Compute platform to run its AI Intelligence applications, and now it's asking people to stress test the system for security holes.... [...]
