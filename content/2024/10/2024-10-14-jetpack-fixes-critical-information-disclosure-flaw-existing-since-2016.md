Title: Jetpack fixes critical information disclosure flaw existing since 2016
Date: 2024-10-14T15:30:30-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-10-14-jetpack-fixes-critical-information-disclosure-flaw-existing-since-2016

[Source](https://www.bleepingcomputer.com/news/security/jetpack-fixes-critical-information-disclosure-flaw-existing-since-2016/){:target="_blank" rel="noopener"}

> WordPress plugin Jetpack released a critical security update earlier today, addressing a vulnerability that allowed a logged-in user to access forms submitted by other visitors to the site. [...]
