Title: Largest Recorded DDoS Attack is 3.8 Tbps
Date: 2024-10-07T11:02:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;denial of service
Slug: 2024-10-07-largest-recorded-ddos-attack-is-38-tbps

[Source](https://www.schneier.com/blog/archives/2024/10/largest-recorded-ddos-attack-is-3-8-tbps.html){:target="_blank" rel="noopener"}

> CLoudflare just blocked the current record DDoS attack: 3.8 terabits per second. (Lots of good information on the attack, and DDoS in general, at the link.) News article. [...]
