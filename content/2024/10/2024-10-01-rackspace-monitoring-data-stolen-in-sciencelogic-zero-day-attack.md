Title: Rackspace monitoring data stolen in ScienceLogic zero-day attack
Date: 2024-10-01T15:30:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-10-01-rackspace-monitoring-data-stolen-in-sciencelogic-zero-day-attack

[Source](https://www.bleepingcomputer.com/news/security/rackspace-monitoring-data-stolen-in-sciencelogic-zero-day-attack/){:target="_blank" rel="noopener"}

> Cloud hosting provider Rackspace suffered a data breach exposing "limited" customer monitoring data after threat actors exploited a zero-day vulnerability in a third-party tool used by the ScienceLogic SL1 platform. [...]
