Title: Underground ransomware claims attack on Casio, leaks stolen data
Date: 2024-10-10T12:00:47-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-10-10-underground-ransomware-claims-attack-on-casio-leaks-stolen-data

[Source](https://www.bleepingcomputer.com/news/security/underground-ransomware-claims-attack-on-casio-leaks-stolen-data/){:target="_blank" rel="noopener"}

> The Underground ransomware gang has claimed responsibility for an October 5 attack on Japanese tech giant Casio, which caused system disruptions and impacted some of the firm's services. [...]
