Title: Two never-before-seen tools, from same group, infect air-gapped devices
Date: 2024-10-09T12:26:26+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;air gaps;espionage;malware
Slug: 2024-10-09-two-never-before-seen-tools-from-same-group-infect-air-gapped-devices

[Source](https://arstechnica.com/security/2024/10/two-never-before-seen-tools-from-same-group-infect-air-gapped-devices/){:target="_blank" rel="noopener"}

> Researchers have unearthed two sophisticated toolsets that a nation-state hacking group—possibly from Russia—used to steal sensitive data stored on air-gapped devices, meaning those that are deliberately isolated from the Internet or other networks to safeguard them from malware. One of the custom tool collections was used starting in 2019 against a South Asian embassy in Belarus. A largely different toolset created by the same threat group infected a European Union government organization three years later. Researchers from ESET, the security firm that discovered the toolkits, said some of the components in both were identical to those fellow security firm Kaspersky [...]
