Title: Interbank confirms data breach following failed extortion, data leak
Date: 2024-10-30T18:22:27-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-10-30-interbank-confirms-data-breach-following-failed-extortion-data-leak

[Source](https://www.bleepingcomputer.com/news/security/interbank-confirms-data-breach-following-failed-extortion-data-leak/){:target="_blank" rel="noopener"}

> ​Interbank, one of Peru's leading financial institutions, has confirmed a data breach after a threat actor who hacked into its systems leaked stolen data online. [...]
