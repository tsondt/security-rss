Title: Recent Dr.Web cyberattack claimed by pro-Ukrainian hacktivists
Date: 2024-10-09T11:53:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-10-09-recent-drweb-cyberattack-claimed-by-pro-ukrainian-hacktivists

[Source](https://www.bleepingcomputer.com/news/security/recent-drweb-breach-claimed-by-dumpforums-pro-ukrainian-hacktivists/){:target="_blank" rel="noopener"}

> A group of pro-Ukrainian hacktivists has claimed responsibility for the September breach of Russian security company Doctor Web (Dr.Web). [...]
