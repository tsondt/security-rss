Title: China's top messaging app WeChat banned from Hong Kong government computers
Date: 2024-10-24T05:11:37+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-10-24-chinas-top-messaging-app-wechat-banned-from-hong-kong-government-computers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/24/hong_kong_wechat_ban/){:target="_blank" rel="noopener"}

> Google and WhatsApp also binned, which is far easier to explain than canning a local hero Hong Kong’s government has updated infosec guidelines to restrict the use of Chinese messaging app WeChat, alongside Meta and Google products like WhatsApp and Google Drive, on computers it operates.... [...]
