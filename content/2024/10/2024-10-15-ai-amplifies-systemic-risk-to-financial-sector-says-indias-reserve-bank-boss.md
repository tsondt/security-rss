Title: AI amplifies systemic risk to financial sector, says India's Reserve Bank boss
Date: 2024-10-15T03:42:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-10-15-ai-amplifies-systemic-risk-to-financial-sector-says-indias-reserve-bank-boss

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/15/india_rbi_ai_risks/){:target="_blank" rel="noopener"}

> Who also worries misinformation on social media could threaten liquidity The governor of India's Reserve Bank, Shri Shaktikanta Das, yesterday warned that AI – and the platforms that provide it – could worsen systemic risk to the nation's financial system.... [...]
