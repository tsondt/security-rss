Title: A Single Cloud Compromise Can Feed an Army of AI Sex Bots
Date: 2024-10-03T13:05:52+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Web Fraud 2.0;Anthropic;AWS;Bedrock;Chub AI;GitHub;Ian Ahl;Lore;NSFL;Permiso Security;Sysdig
Slug: 2024-10-03-a-single-cloud-compromise-can-feed-an-army-of-ai-sex-bots

[Source](https://krebsonsecurity.com/2024/10/a-single-cloud-compromise-can-feed-an-army-of-ai-sex-bots/){:target="_blank" rel="noopener"}

> Organizations that get relieved of credentials to their cloud environments can quickly find themselves part of a disturbing new trend: Cybercriminals using stolen cloud credentials to operate and resell sexualized AI-powered chat services. Researchers say these illicit chat bots, which use custom jailbreaks to bypass content filtering, often veer into darker role-playing scenarios, including child sexual exploitation and rape. Image: Shutterstock. Researchers at security firm Permiso Security say attacks against generative artificial intelligence (AI) infrastructure like Bedrock from Amazon Web Services (AWS) have increased markedly over the last six months, particularly when someone in the organization accidentally exposes their cloud [...]
