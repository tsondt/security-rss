Title: Samsung phone users under attack, Google warns
Date: 2024-10-24T00:16:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-24-samsung-phone-users-under-attack-google-warns

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/24/samsung_phone_eop_attacks/){:target="_blank" rel="noopener"}

> Don't ignore this nasty zero day exploit says TAG A nasty bug in Samsung's mobile chips is being exploited by miscreants as part of an exploit chain to escalate privileges and then remotely execute arbitrary code, according to Google security researchers.... [...]
