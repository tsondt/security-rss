Title: Brazen crims selling stolen credit cards on Meta's Threads
Date: 2024-10-28T15:45:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-28-brazen-crims-selling-stolen-credit-cards-on-metas-threads

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/28/crims_selling_credit_cards_threads/){:target="_blank" rel="noopener"}

> The platform 'continues to take action' against illegal posts, we're told Exclusive Brazen crooks are selling people's pilfered financial information on Meta's Threads, in some cases posting full credit card details, plus stolen credentials, alongside images of the cards themselves.... [...]
