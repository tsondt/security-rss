Title: Critical Kubernetes Image Builder flaw gives SSH root access to VMs
Date: 2024-10-16T12:58:11-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud
Slug: 2024-10-16-critical-kubernetes-image-builder-flaw-gives-ssh-root-access-to-vms

[Source](https://www.bleepingcomputer.com/news/security/critical-kubernetes-image-builder-flaw-gives-ssh-root-access-to-vms/){:target="_blank" rel="noopener"}

> A critical vulnerability in Kubernetes could allow unauthorized SSH access to a virtual machine running an image created with the Kubernetes Image Builder project. [...]
