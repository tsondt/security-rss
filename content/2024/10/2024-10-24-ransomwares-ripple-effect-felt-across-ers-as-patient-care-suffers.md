Title: Ransomware's ripple effect felt across ERs as patient care suffers
Date: 2024-10-24T10:37:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-24-ransomwares-ripple-effect-felt-across-ers-as-patient-care-suffers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/24/ransomware_ripple_effect_hospitals/){:target="_blank" rel="noopener"}

> 389 US healthcare orgs infected this year alone Ransomware infected 389 US healthcare organizations this fiscal year, putting patients' lives at risk and costing facilities up to $900,000 a day in downtime alone, according to Microsoft.... [...]
