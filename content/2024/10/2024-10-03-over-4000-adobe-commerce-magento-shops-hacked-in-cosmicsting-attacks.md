Title: Over 4,000 Adobe Commerce, Magento shops hacked in CosmicSting attacks
Date: 2024-10-03T13:19:37-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-10-03-over-4000-adobe-commerce-magento-shops-hacked-in-cosmicsting-attacks

[Source](https://www.bleepingcomputer.com/news/security/over-4-000-adobe-commerce-magento-shops-hacked-in-cosmicsting-attacks/){:target="_blank" rel="noopener"}

> Approximately 5% of all Adobe Commerce and Magento online stores, or 4,275 in absolute numbers, have been hacked in "CosmicSting" attacks. [...]
