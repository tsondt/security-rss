Title: BianLian ransomware claims attack on Boston Children's Health Physicians
Date: 2024-10-17T11:37:48-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-10-17-bianlian-ransomware-claims-attack-on-boston-childrens-health-physicians

[Source](https://www.bleepingcomputer.com/news/security/bianlian-ransomware-claims-attack-on-boston-childrens-health-physicians/){:target="_blank" rel="noopener"}

> The BianLian ransomware group has claimed the cyberattack on Boston Children's Health Physicians (BCHP) and threatens to leak stolen files unless a ransom is paid. [...]
