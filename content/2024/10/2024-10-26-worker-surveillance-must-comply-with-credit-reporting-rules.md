Title: Worker surveillance must comply with credit reporting rules
Date: 2024-10-26T05:30:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-10-26-worker-surveillance-must-comply-with-credit-reporting-rules

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/26/worker_surveillance_credit_reporting_privacy_requirement/){:target="_blank" rel="noopener"}

> US Consumer Financial Protection Bureau demands transparency, accountability from sellers of employee metrics The US Consumer Financial Protection Bureau on Thursday published guidance advising businesses that third-party reports about workers must comply with the consent and transparency requirements set forth in the Fair Credit Reporting Act.... [...]
