Title: Invisible text that AI chatbots understand and humans can’t? Yep, it’s a thing.
Date: 2024-10-14T19:06:27+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: AI;Biz & IT;Features;Security;large language models;LLMs;steganography;Unicode
Slug: 2024-10-14-invisible-text-that-ai-chatbots-understand-and-humans-cant-yep-its-a-thing

[Source](https://arstechnica.com/security/2024/10/ai-chatbots-can-read-and-write-invisible-text-creating-an-ideal-covert-channel/){:target="_blank" rel="noopener"}

> What if there was a way to sneak malicious instructions into Claude, Copilot, or other top-name AI chatbots and get confidential data out of them by using characters large language models can recognize and their human users can’t? As it turns out, there was—and in some cases still is. The invisible characters, the result of a quirk in the Unicode text encoding standard, create an ideal covert channel that can make it easier for attackers to conceal malicious payloads fed into an LLM. The hidden text can similarly obfuscate the exfiltration of passwords, financial information, or other secrets out of [...]
