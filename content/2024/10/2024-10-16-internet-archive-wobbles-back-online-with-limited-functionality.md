Title: Internet Archive wobbles back online, with limited functionality
Date: 2024-10-16T07:28:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-10-16-internet-archive-wobbles-back-online-with-limited-functionality

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/16/internet_archive_recovery/){:target="_blank" rel="noopener"}

> DDoS detectives deduce Mirai used to do the deed, using home entertainment boxes in Korea, China, and Brazil The Internet Archive has come back online, in slightly degraded mode, after repelling an October 9 DDoS attack and then succumbing to a raid on users' data.... [...]
