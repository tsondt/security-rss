Title: Voice-enabled AI agents can automate everything, even your phone scams
Date: 2024-10-24T06:30:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-10-24-voice-enabled-ai-agents-can-automate-everything-even-your-phone-scams

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/24/openai_realtime_api_phone_scam/){:target="_blank" rel="noopener"}

> All for the low, low price of a mere dollar Scammers, rejoice. OpenAI's real-time voice API can be used to build AI agents capable of conducting successful phone call scams for less than a dollar.... [...]
