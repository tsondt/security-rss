Title: Cisco confirms 'ongoing investigation' after crims brag about selling tons of data
Date: 2024-10-15T22:30:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-15-cisco-confirms-ongoing-investigation-after-crims-brag-about-selling-tons-of-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/15/cisco_confirm_ongoing_investigation/){:target="_blank" rel="noopener"}

> IntelBroker claims the breach impacts Microsoft, SAP, AT&T, Verizon, T-Mobile US, and more Cisco has confirmed it is investigating claims of stealing — and now selling — data belonging to the networking giant.... [...]
