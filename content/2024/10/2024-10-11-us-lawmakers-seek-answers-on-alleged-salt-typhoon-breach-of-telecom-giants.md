Title: US lawmakers seek answers on alleged Salt Typhoon breach of telecom giants
Date: 2024-10-11T21:30:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-11-us-lawmakers-seek-answers-on-alleged-salt-typhoon-breach-of-telecom-giants

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/11/us_lawmakers_salt_typhoon/){:target="_blank" rel="noopener"}

> Cyberspies abusing a backdoor? Groundbreaking Lawmakers are demanding answers about earlier news reports that China's Salt Typhoon cyberspies breached US telecommunications companies Verizon, AT&T, and Lumen Technologies, and hacked their wiretapping systems. They also urge federal regulators to hold these companies accountable for their infosec practices - or lack thereof.... [...]
