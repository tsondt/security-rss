Title: How to perform a proof of concept for automated discovery using Amazon Macie
Date: 2024-10-01T20:00:55+00:00
Author: Jason Stone
Category: AWS Security
Tags: Amazon Macie;Foundational (100);Security, Identity, & Compliance;Technical How-to;Data protection;Data security investigations;PCI;PHI;PII;Security Blog;Sensitive Data Discovery
Slug: 2024-10-01-how-to-perform-a-proof-of-concept-for-automated-discovery-using-amazon-macie

[Source](https://aws.amazon.com/blogs/security/proof-of-concept-for-automated-discovery-using-amazon-macie/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) customers of various sizes across different industries are pursuing initiatives to better classify and protect the data they store in Amazon Simple Storage Service (Amazon S3). Amazon Macie helps customers identify, discover, monitor, and protect sensitive data stored in Amazon S3. However, it’s important that customers evaluate and test the capabilities [...]
