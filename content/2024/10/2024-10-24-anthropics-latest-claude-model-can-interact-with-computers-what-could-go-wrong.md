Title: Anthropic's latest Claude model can interact with computers – what could go wrong?
Date: 2024-10-24T04:30:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-10-24-anthropics-latest-claude-model-can-interact-with-computers-what-could-go-wrong

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/24/anthropic_claude_model_can_use_computers/){:target="_blank" rel="noopener"}

> For starters, it could launch a prompt injection attack on itself... The latest version of AI startup Anthropic's Claude 3.5 Sonnet model can use computers – and the developer makes it sound like that's a good thing.... [...]
