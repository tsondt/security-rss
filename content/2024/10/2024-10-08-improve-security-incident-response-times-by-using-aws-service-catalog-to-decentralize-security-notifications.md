Title: Improve security incident response times by using AWS Service Catalog to decentralize security notifications
Date: 2024-10-08T13:08:09+00:00
Author: Cheng Wang
Category: AWS Security
Tags: Advanced (300);Amazon Simple Notification Service (SNS);AWS Identity and Access Management (IAM);AWS Organizations;AWS Security Hub;AWS Service Catalog;Security & Governance;Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2024-10-08-improve-security-incident-response-times-by-using-aws-service-catalog-to-decentralize-security-notifications

[Source](https://aws.amazon.com/blogs/security/improve-security-incident-response-times-by-using-aws-service-catalog-to-decentralize-security-notifications/){:target="_blank" rel="noopener"}

> Many organizations continuously receive security-related findings that highlight resources that aren’t configured according to the organization’s security policies. The findings can come from threat detection services like Amazon GuardDuty, or from cloud security posture management (CSPM) services like AWS Security Hub, or other sources. An important question to ask is: How, and how soon, are [...]
