Title: How should CISOs respond to the rise of GenAI?
Date: 2024-10-10T07:24:43+00:00
Author: Mohan Veloo, APCJ Field CTO
Category: The Register
Tags: 
Slug: 2024-10-10-how-should-cisos-respond-to-the-rise-of-genai

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/10/how_should_cisos_respond_to/){:target="_blank" rel="noopener"}

> Apply comprehensive security with access control, secure coding, infrastructure protection and AI governance Partner Content As generative AI (GenAI) becomes increasingly integrated into the corporate world, it is transforming everyday operations across various industries.... [...]
