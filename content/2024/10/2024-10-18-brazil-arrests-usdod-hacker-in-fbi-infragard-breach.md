Title: Brazil Arrests ‘USDoD,’ Hacker in FBI Infragard Breach
Date: 2024-10-18T12:33:51+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Ne'er-Do-Well News;CrowdStrike;Equation Corp;fbi;Hackread;InfraGard;Intel 471;National Public Data;NetSec;RaidForums;Tecmundo;TV Globo;USDoD
Slug: 2024-10-18-brazil-arrests-usdod-hacker-in-fbi-infragard-breach

[Source](https://krebsonsecurity.com/2024/10/brazil-arrests-usdod-hacker-in-fbi-infragard-breach/){:target="_blank" rel="noopener"}

> Brazilian authorities reportedly have arrested a 33-year-old man on suspicion of being “ USDoD,” a prolific cybercriminal who rose to infamy in 2022 after infiltrating the FBI’s InfraGard program and leaking contact information for 80,000 members. More recently, USDoD was behind a breach at the consumer data broker National Public Data that led to the leak of Social Security numbers and other personal information for a significant portion of the U.S. population. USDoD’s InfraGard sales thread on Breached. The Brazilian news outlet TV Globo first reported the news of USDoD’s arrest, saying the Federal Police arrested a 33-year-old man from [...]
