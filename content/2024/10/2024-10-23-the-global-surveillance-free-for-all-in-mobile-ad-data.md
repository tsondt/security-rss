Title: The Global Surveillance Free-for-All in Mobile Ad Data
Date: 2024-10-23T11:30:18+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;The Coming Storm;AccuWeather;Adx;Android Advertising ID;App Tracking Transparency;apple;Associated Press;bid request;BR24;Daniel's Law;Electronic Frontier Foundation;Eva Galperin;Fog Reveal;GasBuddy;google;Grindr;Identifier for Advertisers;Judge Andrew F. Wilkinson Judicial Security Act;Justin Sherman;Justyna Maloney;Macy's;MAID;mobile advertising ID;MyFitnessPal;netzpolitik.org;Scott Maloney;Sen. Ron Wyden;SilentPush;Tangles;Troutman Pepper;U.S. Securities and Exchange Commission;WebLoc;Zach Edwards
Slug: 2024-10-23-the-global-surveillance-free-for-all-in-mobile-ad-data

[Source](https://krebsonsecurity.com/2024/10/the-global-surveillance-free-for-all-in-mobile-ad-data/){:target="_blank" rel="noopener"}

> Not long ago, the ability to digitally track someone’s daily movements just by knowing their home address, employer, or place of worship was considered a dangerous power that should remain only within the purview of nation states. But a new lawsuit in a likely constitutional battle over a New Jersey privacy law shows that anyone can now access this capability, thanks to a proliferation of commercial services that hoover up the digital exhaust emitted by widely-used mobile apps and websites. Image: Shutterstock, Arthimides. Delaware-based Atlas Data Privacy Corp. helps its users remove their personal information from the clutches of consumer [...]
