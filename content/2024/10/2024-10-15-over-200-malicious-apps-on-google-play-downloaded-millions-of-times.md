Title: Over 200 malicious apps on Google Play downloaded millions of times
Date: 2024-10-15T10:26:27-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2024-10-15-over-200-malicious-apps-on-google-play-downloaded-millions-of-times

[Source](https://www.bleepingcomputer.com/news/security/over-200-malicious-apps-on-google-play-downloaded-millions-of-times/){:target="_blank" rel="noopener"}

> Google Play, the official store for Android, distributed over a period of one year more than 200 malicious applications, which cumulatively counted nearly eight million downloads. [...]
