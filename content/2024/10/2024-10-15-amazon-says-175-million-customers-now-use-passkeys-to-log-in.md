Title: Amazon says 175 million customers now use passkeys to log in
Date: 2024-10-15T16:52:14-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-10-15-amazon-says-175-million-customers-now-use-passkeys-to-log-in

[Source](https://www.bleepingcomputer.com/news/security/amazon-says-175-million-customers-now-use-passkeys-to-log-in/){:target="_blank" rel="noopener"}

> Amazon has seen massive adoption of passkeys since the company quietly rolled them out a year ago, announcing today that over 175 million customers use the security feature. [...]
