Title: Delta officially launches lawyers at $500M CrowdStrike problem
Date: 2024-10-28T14:17:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-28-delta-officially-launches-lawyers-at-500m-crowdstrike-problem

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/28/delta_airlines_crowdstrike_lawsuit/){:target="_blank" rel="noopener"}

> Legal action comes months after alleging negligence by Falcon vendor Delta Air Lines is suing CrowdStrike in a bid to recover the circa $500 million in estimated lost revenue months after the cybersecurity company "caused" an infamous global IT outage.... [...]
