Title: Evil Corp's deep ties with Russia and NATO member attacks exposed
Date: 2024-10-01T15:35:16+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-01-evil-corps-deep-ties-with-russia-and-nato-member-attacks-exposed

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/01/evil_corp_russia_relationship/){:target="_blank" rel="noopener"}

> Ransomware criminals believed to have taken orders from intel services The relationship between infamous cybercrime outfit Evil Corp and the Russian state is thought to be extraordinarily close, so close that intelligence officials allegedly ordered the criminals to carry out cyberattacks on NATO members.... [...]
