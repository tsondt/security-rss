Title: US lawmakers push DoJ to prosecute tax prep firms for leaking taxpayer data to big tech
Date: 2024-10-22T22:31:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-10-22-us-lawmakers-push-doj-to-prosecute-tax-prep-firms-for-leaking-taxpayer-data-to-big-tech

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/22/democrats_demand_doj_prosecute_tax/){:target="_blank" rel="noopener"}

> TaxSlayer, H&R Block, TaxAct, and Ramsey Solutions accused of sharing info with Meta and Google A quartet of lawmakers have penned a letter to the Department of Justice asking it to prosecute tax preparation companies for sharing customer data, including tax return information, with Meta and Google.... [...]
