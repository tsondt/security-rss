Title: You can now sign Microsoft Windows artifacts with keys protected by Cloud HSM
Date: 2024-10-04T16:00:00+00:00
Author: Lanre Ogunmola
Category: GCP Security
Tags: Security & Identity
Slug: 2024-10-04-you-can-now-sign-microsoft-windows-artifacts-with-keys-protected-by-cloud-hsm

[Source](https://cloud.google.com/blog/products/identity-security/you-can-now-sign-microsoft-windows-artifacts-with-keys-protected-by-cloud-hsm/){:target="_blank" rel="noopener"}

> To build trust in the software world, developers need to be able to digitally sign their code and attest that the software their customers are downloading is legitimate and hasn’t been maliciously altered. Keys used to sign code are the cryptographic equivalent of crown jewels for many organizations, and protecting them is of utmost importance. Google Cloud’s Cloud Key Management System (KMS) provides capabilities for securely generating, managing, and controlling access to cryptographic keys. Cloud KMS offers a user-friendly interface that allows you to create, store, and perform cryptographic operations such as code signing with keys in our tamper-resistant Cloud [...]
