Title: Dutch cops pwn the Redline and Meta infostealers, leak 'VIP' aliases
Date: 2024-10-28T12:01:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-28-dutch-cops-pwn-the-redline-and-meta-infostealers-leak-vip-aliases

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/28/dutch_cops_pwn_the_redline/){:target="_blank" rel="noopener"}

> Legal proceedings underway with more details to follow Dutch police (Politie) say they've dismantled the servers powering the Redline and Meta infostealers – two key tools in a modern cyber crook's arsenal.... [...]
