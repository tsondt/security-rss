Title: Crooks stole personal info of 77k Fidelity Investments customers
Date: 2024-10-10T21:30:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-10-crooks-stole-personal-info-of-77k-fidelity-investments-customers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/10/fidelity_investment_data_breach/){:target="_blank" rel="noopener"}

> But hey, no worries, the firm claims no evidence of data misuse Fidelity Investments has notified 77,099 people that their personal information was stolen in an August data breach.... [...]
