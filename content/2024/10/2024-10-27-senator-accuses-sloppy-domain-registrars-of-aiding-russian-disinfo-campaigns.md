Title: Senator accuses sloppy domain registrars of aiding Russian disinfo campaigns
Date: 2024-10-27T15:44:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-10-27-senator-accuses-sloppy-domain-registrars-of-aiding-russian-disinfo-campaigns

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/27/senator_domain_registrars_russia_disinfo/){:target="_blank" rel="noopener"}

> Also, Change Healthcare sets a record, cybercrime cop suspect indicted, a new Mallox decryptor, and more in brief Senate intelligence committee chair Mark Warner (D-VA) is demanding to know why, in the wake of the bust-up of a massive online Russian disinformation operation, the names of six US-based domain registrars seem to keep popping up as, at best, negligent facilitators of election meddling.... [...]
