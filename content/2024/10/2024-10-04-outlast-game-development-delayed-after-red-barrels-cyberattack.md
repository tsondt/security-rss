Title: Outlast game development delayed after Red Barrels cyberattack
Date: 2024-10-04T11:56:47-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2024-10-04-outlast-game-development-delayed-after-red-barrels-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/outlast-game-development-delayed-after-red-barrels-cyberattack/){:target="_blank" rel="noopener"}

> Canadian video game developer Red Barrels is warning that the development of its Outlast games will likely be delayed after the company suffered a cyberattack impacting its internal IT systems and data. [...]
