Title: LottieFiles supply chain attack exposes users to malicious crypto wallet drainer
Date: 2024-10-31T11:55:17+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-31-lottiefiles-supply-chain-attack-exposes-users-to-malicious-crypto-wallet-drainer

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/31/lottiefiles_supply_chain_attack/){:target="_blank" rel="noopener"}

> A scary few Halloween hours for team behind hugely popular web plugin LottieFiles is overcoming something of a Halloween fright after battling to regain control of a compromised developer account that was used to exploit users' crypto wallets.... [...]
