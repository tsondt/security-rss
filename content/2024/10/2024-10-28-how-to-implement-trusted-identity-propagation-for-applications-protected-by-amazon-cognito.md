Title: How to implement trusted identity propagation for applications protected by Amazon Cognito
Date: 2024-10-28T17:43:24+00:00
Author: Joseph de Clerck
Category: AWS Security
Tags: Amazon Cognito;AWS IAM Identity Center;Expert (400);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2024-10-28-how-to-implement-trusted-identity-propagation-for-applications-protected-by-amazon-cognito

[Source](https://aws.amazon.com/blogs/security/how-to-implement-trusted-identity-propagation-for-applications-protected-by-amazon-cognito/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) recently released AWS IAM Identity Center trusted identity propagation to create identity-enhanced IAM role sessions when requesting access to AWS services as well as to trusted token issuers. These two features can help customers build custom applications on top of AWS, which requires fine-grained access to data analytics-focused AWS services such [...]
