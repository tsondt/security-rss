Title: Cheating at Conkers
Date: 2024-10-16T11:03:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cheating;games
Slug: 2024-10-16-cheating-at-conkers

[Source](https://www.schneier.com/blog/archives/2024/10/cheating-at-conkers.html){:target="_blank" rel="noopener"}

> The men’s world conkers champion is accused of cheating with a steel chestnut. [...]
