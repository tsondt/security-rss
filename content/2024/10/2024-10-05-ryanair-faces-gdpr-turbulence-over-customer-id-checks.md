Title: Ryanair faces GDPR turbulence over customer ID checks
Date: 2024-10-05T09:31:10+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-10-05-ryanair-faces-gdpr-turbulence-over-customer-id-checks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/05/irish_dpc_ryanair_probe/){:target="_blank" rel="noopener"}

> Irish data watchdog opens probe after 'numerous complaints' Ireland's Data Protection Commission (DPC) has launched an inquiry into Ryanair's Customer Verification Process for travelers booking flights through third-party websites or online travel agents (OTA).... [...]
