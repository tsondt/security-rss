Title: VMware fixes critical RCE, make-me-root bugs in vCenter - for the second time
Date: 2024-10-22T17:02:27+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-22-vmware-fixes-critical-rce-make-me-root-bugs-in-vcenter-for-the-second-time

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/22/vmware_rce_vcenter_bugs/){:target="_blank" rel="noopener"}

> If the first patches don't work, try, try again VMware has pushed a second patch for a critical, heap-overflow bug in the vCenter Server that could allow a remote attacker to fully compromise vulnerable systems after the first software update, issued last month, didn't work.... [...]
