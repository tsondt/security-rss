Title: AI and the SEC Whistleblower Program
Date: 2024-10-21T11:09:33+00:00
Author: B. Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;finance;LLM;regulation;whistleblowers
Slug: 2024-10-21-ai-and-the-sec-whistleblower-program

[Source](https://www.schneier.com/blog/archives/2024/10/ai-and-the-sec-whistleblower-program.html){:target="_blank" rel="noopener"}

> Tax farming is the practice of licensing tax collection to private contractors. Used heavily in ancient Rome, it’s largely fallen out of practice because of the obvious conflict of interest between the state and the contractor. Because tax farmers are primarily interested in short-term revenue, they have no problem abusing taxpayers and making things worse for them in the long term. Today, the U.S. Securities and Exchange Commission (SEC) is engaged in a modern-day version of tax farming. And the potential for abuse will grow when the farmers start using artificial intelligence. In 2009, after Bernie Madoff’s $65 billion Ponzi [...]
