Title: Russian spies use remote desktop protocol files in unusual mass phishing drive
Date: 2024-10-30T12:40:56+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-30-russian-spies-use-remote-desktop-protocol-files-in-unusual-mass-phishing-drive

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/30/russia_wrangles_rdp_files_in/){:target="_blank" rel="noopener"}

> The prolific Midnight Blizzard crew cast a much wider net in search of scrummy intel Microsoft says a mass phishing campaign by Russia's foreign intelligence services (SVR) is now in its second week, and the spies are using a novel info-gathering technique.... [...]
