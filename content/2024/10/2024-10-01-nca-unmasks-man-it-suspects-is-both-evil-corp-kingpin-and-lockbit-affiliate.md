Title: NCA unmasks man it suspects is both 'Evil Corp kingpin' and LockBit affiliate
Date: 2024-10-01T14:08:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-01-nca-unmasks-man-it-suspects-is-both-evil-corp-kingpin-and-lockbit-affiliate

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/01/nca_names_alleged_evil_corp_kingpin/){:target="_blank" rel="noopener"}

> Aleksandr Ryzhenkov alleged to have extorted around $100M from victims, built 60 LockBit attacks The latest installment of the National Crime Agency's (NCA) series of ransomware revelations from February's LockBit Leak Week emerges today as the agency identifies a man it not only believes is a member of the long-running Evil Corp crime group but also a LockBit affiliate.... [...]
