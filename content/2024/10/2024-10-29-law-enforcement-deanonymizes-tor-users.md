Title: Law Enforcement Deanonymizes Tor Users
Date: 2024-10-29T11:02:15+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;de-anonymization;law enforcement;Tor
Slug: 2024-10-29-law-enforcement-deanonymizes-tor-users

[Source](https://www.schneier.com/blog/archives/2024/10/law-enforcement-deanonymizes-tor-users.html){:target="_blank" rel="noopener"}

> The German police have successfully deanonymized at least four Tor users. It appears they watch known Tor relays and known suspects, and use timing analysis to figure out who is using what relay. Tor has written about this. Hacker News thread. [...]
