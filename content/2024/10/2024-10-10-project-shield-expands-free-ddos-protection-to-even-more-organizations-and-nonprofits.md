Title: Project Shield expands free DDoS protection to even more organizations and nonprofits
Date: 2024-10-10T16:00:00+00:00
Author: Marc Howard
Category: GCP Security
Tags: Security & Identity
Slug: 2024-10-10-project-shield-expands-free-ddos-protection-to-even-more-organizations-and-nonprofits

[Source](https://cloud.google.com/blog/products/identity-security/project-shield-expands-free-ddos-protection/){:target="_blank" rel="noopener"}

> Project Shield has helped news, human rights, and elections-related organizations defend against distributed denial of service (DDoS) attacks since 2013 as part of Google's commitment to keep online content universally accessible. The solution has helped keep election resources online, supported news reporting during pivotal geopolitical events, and helped governments during times of urgency. We are pleased to announce that Project Shield is expanding our eligibility criteria to support and protect organizations representing marginalized groups and non-profit organizations supporting the arts and sciences. These types of organizations are often subject to attacks and censorship attempts, and DDoS represents a common component [...]
