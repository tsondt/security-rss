Title: MoneyGram: No evidence ransomware is behind recent cyberattack
Date: 2024-10-05T10:16:26-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-10-05-moneygram-no-evidence-ransomware-is-behind-recent-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/moneygram-no-evidence-ransomware-is-behind-recent-cyberattack/){:target="_blank" rel="noopener"}

> MoneyGram says there is no evidence that ransomware is behind a recent cyberattack that led to a five-day outage in September. [...]
