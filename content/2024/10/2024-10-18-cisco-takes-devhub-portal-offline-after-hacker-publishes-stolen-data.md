Title: Cisco takes DevHub portal offline after hacker publishes stolen data
Date: 2024-10-18T18:21:16-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-10-18-cisco-takes-devhub-portal-offline-after-hacker-publishes-stolen-data

[Source](https://www.bleepingcomputer.com/news/security/cisco-takes-devhub-portal-offline-after-hacker-publishes-stolen-data/){:target="_blank" rel="noopener"}

> Cisco confirmed today that it took its public DevHub portal offline after a threat actor leaked "non-public" data, but it continues to state that there is no evidence that its systems were breached. [...]
