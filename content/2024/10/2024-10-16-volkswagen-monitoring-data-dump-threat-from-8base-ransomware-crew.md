Title: Volkswagen monitoring data dump threat from 8Base ransomware crew
Date: 2024-10-16T21:30:12+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-10-16-volkswagen-monitoring-data-dump-threat-from-8base-ransomware-crew

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/16/volkswagen_ransomware_data_loss/){:target="_blank" rel="noopener"}

> The German car giant appears to be unconcerned The 8Base ransomware crew claims to have stolen a huge data dump of Volkswagen files and is threatening to publish them, but the German car giant appears to be unconcerned.... [...]
