Title: Fraudsters imprisoned for scamming Apple out of 6,000 iPhones
Date: 2024-10-03T12:27:47-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Apple
Slug: 2024-10-03-fraudsters-imprisoned-for-scamming-apple-out-of-6000-iphones

[Source](https://www.bleepingcomputer.com/news/security/fraudsters-imprisoned-for-scamming-apple-out-of-6-000-iphones/){:target="_blank" rel="noopener"}

> Two Chinese nationals were sentenced to prison for scamming Apple out of more than $2.5 million after exchanging over 6,000 counterfeit iPhones for authentic ones. [...]
