Title: Neo-Nazis head to encrypted SimpleX Chat app, bail on Telegram
Date: 2024-10-05T11:00:41+00:00
Author: David Gilbert, wired.com
Category: Ars Technica
Tags: Biz & IT;Policy;hate speech;Neo-nazis;privacy;SimpleX Chat;syndication;telegram
Slug: 2024-10-05-neo-nazis-head-to-encrypted-simplex-chat-app-bail-on-telegram

[Source](https://arstechnica.com/tech-policy/2024/10/neo-nazis-head-to-encrypted-simplex-chat-app-bail-on-telegram/){:target="_blank" rel="noopener"}

> Dozens of neo-Nazis are fleeing Telegram and moving to a relatively unknown secret chat app that has received funding from Twitter founder Jack Dorsey. In a report from the Institute for Strategic Dialogue published on Friday morning, researchers found that in the wake of the arrest of Telegram founder Pavel Durov and charges against leaders of the so-called Terrorgram Collective, dozens of extremist groups have moved to the app SimpleX Chat in recent weeks over fears that Telegram’s privacy policies expose them to being arrested. The Terrorgram Collective is a neo-Nazi propaganda network that calls for acolytes to target government [...]
