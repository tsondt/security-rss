Title: Men accused of DDoSing some of the world’s biggest tech companies
Date: 2024-10-16T22:40:56+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security
Slug: 2024-10-16-men-accused-of-ddosing-some-of-the-worlds-biggest-tech-companies

[Source](https://arstechnica.com/information-technology/2024/10/us-prosecutors-take-down-operation-accused-of-35000-ddoses-over-14-months/){:target="_blank" rel="noopener"}

> Federal authorities have charged two Sudanese nationals with running an operation that performed tens of thousands of distributed denial of service (DDoS) attacks against some of the world’s biggest technology companies, as well as critical infrastructure and government agencies. The service, branded as Anonymous Sudan, directed powerful and sustained DDoSes against Big Tech companies, including Microsoft, OpenAI, Riot Games, PayPal, Steam, Hulu, Netflix, Reddit, GitHub, and Cloudflare. Other targets included CNN.com, Cedars-Sinai Medical Center in Los Angeles, the US departments of Justice, Defense and State, the FBI, and government websites for the state of Alabama. Other attacks targeted sites or [...]
