Title: Big brands among thousands infected by payment-card-stealing CosmicSting crooks
Date: 2024-10-04T03:42:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-04-big-brands-among-thousands-infected-by-payment-card-stealing-cosmicsting-crooks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/04/cisco_ray_ban_whirpool_cosmicsting_hack/){:target="_blank" rel="noopener"}

> Gangs hit 5% of all Adobe Commerce, Magento-powered stores, Sansec says Updated Ray-Ban, National Geographic, Whirlpool, and Segway are among thousands of brands whose web stores were reportedly compromised by criminals exploiting the CosmicSting flaw in hope of stealing shoppers' payment card info as they order stuff online.... [...]
