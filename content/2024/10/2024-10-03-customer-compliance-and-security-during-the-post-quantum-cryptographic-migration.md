Title: Customer compliance and security during the post-quantum cryptographic migration
Date: 2024-10-03T16:18:14+00:00
Author: Panos Kampanakis
Category: AWS Security
Tags: Best Practices;Intermediate (200);Security, Identity, & Compliance;Technical How-to;cryptography;post-quantum cryptography;Security Blog
Slug: 2024-10-03-customer-compliance-and-security-during-the-post-quantum-cryptographic-migration

[Source](https://aws.amazon.com/blogs/security/customer-compliance-and-security-during-the-post-quantum-cryptographic-migration/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) prioritizes the security, privacy, and performance of its services. AWS is responsible for the security of the cloud and the services it offers, and customers own the security of the hosts, applications, and services they deploy in the cloud. AWS has also been introducing quantum-resistant key exchange in common transport protocols used [...]
