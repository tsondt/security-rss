Title: Penn State pays DoJ $1.25M to settle cybersecurity compliance case
Date: 2024-10-23T23:29:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-10-23-penn-state-pays-doj-125m-to-settle-cybersecurity-compliance-case

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/23/penn_state_university_doj_settlement/){:target="_blank" rel="noopener"}

> Fight On, State? Not this time Pennsylvania State University has agreed to pay the Justice Department $1.25 million to settle claims of misrepresenting its cybersecurity compliance to the federal government and leaving sensitive data improperly secured.... [...]
