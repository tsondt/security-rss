Title: Indian Fishermen Are Catching Less Squid
Date: 2024-10-11T21:04:10+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-10-11-indian-fishermen-are-catching-less-squid

[Source](https://www.schneier.com/blog/archives/2024/10/indian-fishermen-are-catching-less-squid.html){:target="_blank" rel="noopener"}

> Fishermen in Tamil Nadu are reporting smaller catches of squid. Blog moderation policy. [...]
