Title: Why your password policy should include a custom dictionary
Date: 2024-10-03T10:02:04-04:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2024-10-03-why-your-password-policy-should-include-a-custom-dictionary

[Source](https://www.bleepingcomputer.com/news/security/why-your-password-policy-should-include-a-custom-dictionary/){:target="_blank" rel="noopener"}

> Utilizing a custom dictionaries helps strengthen your password policies. Learn more from Specops Software about how to build custom dictionaries in your Windows Active Directory password policy. [...]
