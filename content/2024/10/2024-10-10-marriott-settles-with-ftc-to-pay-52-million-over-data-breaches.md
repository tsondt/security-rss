Title: Marriott settles with FTC, to pay $52 million over data breaches
Date: 2024-10-10T14:57:38-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;Security
Slug: 2024-10-10-marriott-settles-with-ftc-to-pay-52-million-over-data-breaches

[Source](https://www.bleepingcomputer.com/news/legal/marriott-settles-with-ftc-to-pay-52-million-over-data-breaches/){:target="_blank" rel="noopener"}

> Marriott International and its subsidiary Starwood Hotels will pay $52 million and create a comprehensive information security program as part of settlements for data breaches that impacted over 344 million customers. [...]
