Title: Australian e-tailer digiDirect customers' info allegedly stolen and dumped online
Date: 2024-10-01T00:26:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-01-australian-e-tailer-digidirect-customers-info-allegedly-stolen-and-dumped-online

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/01/australian_digidirect_info_leak/){:target="_blank" rel="noopener"}

> Full names, contact details, and company info – all the fixings for a phishing holiday Data allegedly belonging to more than 304,000 customers of Australian camera and tech e-tailer digiDirect has been leaked to an online cyber crime forum.... [...]
