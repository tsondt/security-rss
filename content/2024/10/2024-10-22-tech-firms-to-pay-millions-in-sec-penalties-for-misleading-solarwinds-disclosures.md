Title: Tech firms to pay millions in SEC penalties for misleading SolarWinds disclosures
Date: 2024-10-22T16:31:05+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-10-22-tech-firms-to-pay-millions-in-sec-penalties-for-misleading-solarwinds-disclosures

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/22/sec_fines_four_tech_firms/){:target="_blank" rel="noopener"}

> Unisys, Avaya, Check Point, and Mimecast settled with the agency without admitting or denying wrongdoing Four high-profile tech companies reached an agreement with the Securities and Exchange Commission to pay millions of dollars in penalties for misleading investors about their exposure to the 2020 SolarWinds hack.... [...]
