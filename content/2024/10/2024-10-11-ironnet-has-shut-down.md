Title: IronNet Has Shut Down
Date: 2024-10-11T11:08:28+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;fraud;NSA
Slug: 2024-10-11-ironnet-has-shut-down

[Source](https://www.schneier.com/blog/archives/2024/10/ironnet-has-shut-down.html){:target="_blank" rel="noopener"}

> After retiring in 2014 from an uncharacteristically long tenure running the NSA (and US CyberCommand), Keith Alexander founded a cybersecurity company called IronNet. At the time, he claimed that it was based on IP he developed on his own time while still in the military. That always troubled me. Whatever ideas he had, they were developed on public time using public resources: he shouldn’t have been able to leave military service with them in his back pocket. In any case, it was never clear what those ideas were. IronNet never seemed to have any special technology going for it. Near [...]
