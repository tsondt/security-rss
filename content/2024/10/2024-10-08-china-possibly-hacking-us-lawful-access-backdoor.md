Title: China Possibly Hacking US “Lawful Access” Backdoor
Date: 2024-10-08T11:00:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;backdoors;CALEA;China;cyberattack;hacking
Slug: 2024-10-08-china-possibly-hacking-us-lawful-access-backdoor

[Source](https://www.schneier.com/blog/archives/2024/10/china-possibly-hacking-us-lawful-access-backdoor.html){:target="_blank" rel="noopener"}

> The Wall Street Journal is reporting that Chinese hackers (Salt Typhoon) penetrated the networks of US broadband providers, and might have accessed the backdoors that the federal government uses to execute court-authorized wiretap requests. Those backdoors have been mandated by law—CALEA—since 1994. It’s a weird story. The first line of the article is: “A cyberattack tied to the Chinese government penetrated the networks of a swath of U.S. broadband providers.” This implies that the attack wasn’t against the broadband providers directly, but against one of the intermediary companies that sit between the government CALEA requests and the broadband providers. For [...]
