Title: How AWS uses active defense to help protect customers from security threats
Date: 2024-10-10T13:00:09+00:00
Author: Chris Betz
Category: AWS Security
Tags: Foundational (100);Security, Identity, & Compliance;Thought Leadership;Security;Security Blog;Threat Intelligence
Slug: 2024-10-10-how-aws-uses-active-defense-to-help-protect-customers-from-security-threats

[Source](https://aws.amazon.com/blogs/security/how-aws-uses-active-defense-to-help-protect-customers-from-security-threats/){:target="_blank" rel="noopener"}

> AWS is deeply committed to earning and maintaining the trust of customers who rely on us to run their workloads. Security has always been our top priority, which includes designing our own services with security in mind at the outset, and taking proactive measures to mitigate potential threats so that customers can focus on their [...]
