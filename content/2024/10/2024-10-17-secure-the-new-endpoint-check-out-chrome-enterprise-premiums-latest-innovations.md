Title: Secure the new endpoint: Check out Chrome Enterprise Premium's latest innovations
Date: 2024-10-17T07:00:00+00:00
Author: Tanisha Rai
Category: GCP Security
Tags: Chrome Enterprise;Security & Identity
Slug: 2024-10-17-secure-the-new-endpoint-check-out-chrome-enterprise-premiums-latest-innovations

[Source](https://cloud.google.com/blog/products/identity-security/check-out-chrome-enterprise-premiums-latest-innovations/){:target="_blank" rel="noopener"}

> The modern workplace revolves around the browser. It's where employees access critical applications, handle sensitive data, and collaborate with colleagues. This makes the browser a critical point for enforcing security. Chrome Enterprise, the most trusted enterprise browser, recently introduced powerful new capabilities for Chrome Enterprise Premium designed to further enhance security, threat detection, and usability. Let’s check them out. Enhanced data loss prevention with watermarking Watermarking, now generally available, can help deter data leaks by overlaying a customizable, semi-transparent watermark on sensitive web pages. When a user visits a page that triggers a Data Loss Prevention (DLP) rule, the watermark [...]
