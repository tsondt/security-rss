Title: Auto-Identification Smart Glasses
Date: 2024-10-09T11:05:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;doxing;identification;LLM
Slug: 2024-10-09-auto-identification-smart-glasses

[Source](https://www.schneier.com/blog/archives/2024/10/auto-identification-smart-glasses.html){:target="_blank" rel="noopener"}

> Two students have created a demo of a smart-glasses app that performs automatic facial recognition and then information lookups. Kind of obvious, but the sort of creepy demo that gets attention. News article. [...]
