Title: USDoD hacker behind National Public Data breach arrested in Brazil
Date: 2024-10-16T17:47:48-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-10-16-usdod-hacker-behind-national-public-data-breach-arrested-in-brazil

[Source](https://www.bleepingcomputer.com/news/security/usdod-hacker-behind-national-public-data-breach-arrested-in-brazil/){:target="_blank" rel="noopener"}

> A notorious hacker named USDoD, who is linked to the National Public Data and InfraGard breaches, has been arrested by Brazil's Polícia Federal in "Operation Data Breach". [...]
