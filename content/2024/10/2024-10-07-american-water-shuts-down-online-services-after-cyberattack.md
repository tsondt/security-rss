Title: American Water shuts down online services after cyberattack
Date: 2024-10-07T13:29:24-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-10-07-american-water-shuts-down-online-services-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/american-water-shuts-down-online-services-after-cyberattack/){:target="_blank" rel="noopener"}

> American Water, the largest publicly traded U.S. water and wastewater utility company, was forced to shut down some of its systems after a Thursday cyberattack. [...]
