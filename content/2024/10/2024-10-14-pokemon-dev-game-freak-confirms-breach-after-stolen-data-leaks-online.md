Title: Pokemon dev Game Freak confirms breach after stolen data leaks online
Date: 2024-10-14T11:43:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2024-10-14-pokemon-dev-game-freak-confirms-breach-after-stolen-data-leaks-online

[Source](https://www.bleepingcomputer.com/news/security/pokemon-dev-game-freak-confirms-breach-after-stolen-data-leaks-online/){:target="_blank" rel="noopener"}

> Japanese video game developer Game Freak has confirmed it suffered a cyberattack in August after source code and game designs for unpublished games were leaked online. [...]
