Title: More on My AI and Democracy Book
Date: 2024-10-11T19:00:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;books;democracy;Schneier news
Slug: 2024-10-11-more-on-my-ai-and-democracy-book

[Source](https://www.schneier.com/blog/archives/2024/10/more-on-my-ai-and-democracy-book.html){:target="_blank" rel="noopener"}

> In July, I wrote about my new book project on AI and democracy, to be published by MIT Press in fall 2025. My co-author and collaborator Nathan Sanders and I are hard at work writing. At this point, we would like feedback on titles. Here are four possibilities: Rewiring the Republic: How AI Will Transform our Politics, Government, and Citizenship The Thinking State: How AI Can Improve Democracy Better Run: How AI Can Make our Politics, Government, Citizenship More Efficient, Effective and Fair AI and the New Future of Democracy: Changes in Politics, Government, and Citizenship What we want out [...]
