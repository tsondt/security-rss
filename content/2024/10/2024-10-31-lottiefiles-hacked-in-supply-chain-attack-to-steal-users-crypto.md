Title: LottieFiles hacked in supply chain attack to steal users’ crypto
Date: 2024-10-31T05:02:35-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-10-31-lottiefiles-hacked-in-supply-chain-attack-to-steal-users-crypto

[Source](https://www.bleepingcomputer.com/news/security/lottiefiles-hacked-in-supply-chain-attack-to-steal-users-crypto/){:target="_blank" rel="noopener"}

> The popular LottieFiles Lotti-Player project was compromised in a supply chain attack to inject a crypto drainer into websites that steals visitors' cryptocurrency. [...]
