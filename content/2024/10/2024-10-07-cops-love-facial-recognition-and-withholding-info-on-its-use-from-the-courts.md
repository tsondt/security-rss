Title: Cops love facial recognition, and withholding info on its use from the courts
Date: 2024-10-07T19:45:13+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-10-07-cops-love-facial-recognition-and-withholding-info-on-its-use-from-the-courts

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/07/cops_love_facial_recognition_and/){:target="_blank" rel="noopener"}

> Withholding exculpatory evidence from suspects isn't a great look when the tech is already questionable Police around the United States are routinely using facial recognition technology to help identify suspects, but those departments rarely disclose they've done so - even to suspects and their lawyers.... [...]
