Title: Arc browser launches bug bounty program after fixing RCE bug
Date: 2024-10-01T18:33:38-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2024-10-01-arc-browser-launches-bug-bounty-program-after-fixing-rce-bug

[Source](https://www.bleepingcomputer.com/news/security/arc-browser-launches-bug-bounty-program-after-fixing-rce-bug/){:target="_blank" rel="noopener"}

> The Browser Company has introduced an Arc Bug Bounty Program to encourage security researchers to report vulnerabilities to the project and receive rewards. [...]
