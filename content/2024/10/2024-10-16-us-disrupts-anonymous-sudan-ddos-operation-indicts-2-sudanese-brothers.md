Title: US disrupts Anonymous Sudan DDoS operation, indicts 2 Sudanese brothers
Date: 2024-10-16T14:36:34-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-10-16-us-disrupts-anonymous-sudan-ddos-operation-indicts-2-sudanese-brothers

[Source](https://www.bleepingcomputer.com/news/security/us-disrupts-anonymous-sudan-ddos-operation-indicts-2-sudanese-brothers/){:target="_blank" rel="noopener"}

> The United States Department of Justice unsealed an indictment today against two Sudanese brothers suspected of being the operators of Anonymous Sudan, a notorious and dangerous hacktivist group known for conducting over 35,000 DDoS attacks in a year. [...]
