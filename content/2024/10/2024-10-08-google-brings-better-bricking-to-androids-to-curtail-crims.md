Title: Google brings better bricking to Androids, to curtail crims
Date: 2024-10-08T02:59:13+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-10-08-google-brings-better-bricking-to-androids-to-curtail-crims

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/08/google_android_security/){:target="_blank" rel="noopener"}

> Improved security features teased in May now appearing around the world Google has apparently started a global rollout of three features in Android designed to make life a lot harder for thieves to profit from purloined phones.... [...]
