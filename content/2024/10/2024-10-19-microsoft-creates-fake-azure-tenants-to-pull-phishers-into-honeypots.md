Title: Microsoft creates fake Azure tenants to pull phishers into honeypots
Date: 2024-10-19T10:32:01-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-10-19-microsoft-creates-fake-azure-tenants-to-pull-phishers-into-honeypots

[Source](https://www.bleepingcomputer.com/news/security/microsoft-creates-fake-azure-tenants-to-pull-phishers-into-honeypots/){:target="_blank" rel="noopener"}

> Microsoft is using deceptive tactics against phishing actors by spawning realistic-looking honeypot tenants with access to Azure and lure cybercriminals in to collect intelligence about them. [...]
