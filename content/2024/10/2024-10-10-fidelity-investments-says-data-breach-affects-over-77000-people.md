Title: Fidelity Investments says data breach affects over 77,000 people
Date: 2024-10-10T12:50:39-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-10-10-fidelity-investments-says-data-breach-affects-over-77000-people

[Source](https://www.bleepingcomputer.com/news/security/fidelity-investments-says-data-breach-affects-over-77-000-people/){:target="_blank" rel="noopener"}

> Fidelity Investments, a Boston-based multinational financial services company, disclosed that the personal information of over 77,000 customers was exposed after its systems were breached in August. [...]
