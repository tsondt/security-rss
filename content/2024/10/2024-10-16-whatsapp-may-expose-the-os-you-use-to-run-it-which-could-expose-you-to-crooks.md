Title: WhatsApp may expose the OS you use to run it – which could expose you to crooks
Date: 2024-10-16T04:26:10+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-10-16-whatsapp-may-expose-the-os-you-use-to-run-it-which-could-expose-you-to-crooks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/16/whatsapp_privacy_concerns/){:target="_blank" rel="noopener"}

> Meta knows messaging service creates persistent user IDs that have different qualities on each device Updated An analysis of Meta's WhatsApp messaging software reveals that it may expose which operating system a user is running, and their device setup information – including the number of linked devices.... [...]
