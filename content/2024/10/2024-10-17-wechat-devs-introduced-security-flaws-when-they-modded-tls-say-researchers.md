Title: WeChat devs introduced security flaws when they modded TLS, say researchers
Date: 2024-10-17T08:31:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-17-wechat-devs-introduced-security-flaws-when-they-modded-tls-say-researchers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/17/wechat_devs_modded_tls_introducing/){:target="_blank" rel="noopener"}

> No attacks possible, but enough issues to cause concern Messaging giant WeChat uses a network protocol that the app's developers modified – and by doing so introduced security weaknesses, researchers claim.... [...]
