Title: Ransomware attack forces UMC Health System to divert some patients
Date: 2024-10-01T13:29:09-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-10-01-ransomware-attack-forces-umc-health-system-to-divert-some-patients

[Source](https://www.bleepingcomputer.com/news/security/ransomware-attack-forces-umc-health-system-to-divert-some-patients/){:target="_blank" rel="noopener"}

> Texas healthcare provider UMC Health System was forced to divert some patients to other locations after a ransomware attack impacted its operations. [...]
