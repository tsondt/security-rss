Title: Lamborghini Carjackers Lured by $243M Cyberheist
Date: 2024-10-09T17:36:27+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Ne'er-Do-Well News;SIM Swapping;Web Fraud 2.0;Angel "Chi Chi" Borrero;AnyDesk;Gemini;Jeandiel "Box" Serrano;Malone "Greavys" Lam;Pantic;Reynaldo "Rey" Diaz;Swag;The Com;Veer Chetal;Wiz;ZachXBT
Slug: 2024-10-09-lamborghini-carjackers-lured-by-243m-cyberheist

[Source](https://krebsonsecurity.com/2024/10/lamborghini-carjackers-lured-by-243m-cyberheist/){:target="_blank" rel="noopener"}

> The parents of a 19-year-old Connecticut honors student accused of taking part in a $243 million cryptocurrency heist in August were carjacked a week later — while out house-hunting in a brand new Lamborghini. Prosecutors say the couple was beaten and briefly kidnapped by six young men who traveled from Florida as part of a botched plan to hold the parents for ransom. Image: ABC7NY. youtube.com/watch?v=xoiaGzwrunY Late in the afternoon of Aug. 25, 2024 in Danbury, Ct., a married couple in their 50s pulled up to a gated community in a new Lamborghini Urus (investigators say the sports car still [...]
