Title: Internet Archive user info stolen in cyberattack, succumbs to DDoS
Date: 2024-10-10T01:33:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-10-10-internet-archive-user-info-stolen-in-cyberattack-succumbs-to-ddos

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/10/internet_archive_ddos_data_theft/){:target="_blank" rel="noopener"}

> 31M folks' usernames, email addresses, salted-encrypted passwords now out there The Internet Archive had a bad day on the infosec front, after being DDoSed and having had its user account data stolen in a security breach.... [...]
