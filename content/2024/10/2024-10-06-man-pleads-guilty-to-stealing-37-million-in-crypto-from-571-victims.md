Title: Man pleads guilty to stealing $37 million in crypto from 571 victims
Date: 2024-10-06T10:17:27-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;CryptoCurrency;Security
Slug: 2024-10-06-man-pleads-guilty-to-stealing-37-million-in-crypto-from-571-victims

[Source](https://www.bleepingcomputer.com/news/legal/man-pleads-guilty-to-stealing-37-million-in-crypto-from-571-victims/){:target="_blank" rel="noopener"}

> A 21-year-old man from Indiana named Evan Frederick Light pleaded guilty to stealing $37,704,560 worth of cryptocurrency from 571 victims in a 2022 cyberattack. [...]
