Title: Police arrest four suspects linked to LockBit ransomware gang
Date: 2024-10-01T11:36:31-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-10-01-police-arrest-four-suspects-linked-to-lockbit-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/police-arrest-four-suspects-linked-to-lockbit-ransomware-gang/){:target="_blank" rel="noopener"}

> Law enforcement authorities from 12 countries arrested four suspects linked to the LockBit ransomware gang, including a developer, a bulletproof hosting service administrator, and two people connected to LockBit activity. [...]
