Title: New Mamba 2FA bypass service targets Microsoft 365 accounts
Date: 2024-10-08T16:27:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-10-08-new-mamba-2fa-bypass-service-targets-microsoft-365-accounts

[Source](https://www.bleepingcomputer.com/news/security/new-mamba-2fa-bypass-service-targets-microsoft-365-accounts/){:target="_blank" rel="noopener"}

> An emerging phishing-as-a-service (PhaaS) platform called Mamba 2FA has been observed targeting Microsoft 365 accounts in AiTM attacks using well-crafted login pages. [...]
