Title: EDRSilencer red team tool used in attacks to bypass security
Date: 2024-10-15T14:47:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-10-15-edrsilencer-red-team-tool-used-in-attacks-to-bypass-security

[Source](https://www.bleepingcomputer.com/news/security/edrsilencer-red-team-tool-used-in-attacks-to-bypass-security/){:target="_blank" rel="noopener"}

> A tool for red-team operations called EDRSilencer has been observed in malicious incidents attempting to identify security tools and mute their alerts to management consoles. [...]
