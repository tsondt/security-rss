Title: Healthcare attacks spread beyond US – just ask India's Star Health
Date: 2024-10-11T02:57:43+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-10-11-healthcare-attacks-spread-beyond-us-just-ask-indias-star-health

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/11/star_health_breach/){:target="_blank" rel="noopener"}

> Acknowledges bulk customer data leak weeks after Telegram channels dangled it online Leading Indian health insurance provider Star Health has admitted to being the victim of a cyber attack after criminals claimed they had posted records of 30-milion-plus clients online.... [...]
