Title: How to use the Amazon Detective API to investigate GuardDuty security findings and enrich data in Security Hub
Date: 2024-10-22T20:38:10+00:00
Author: Nicholas Jaeger
Category: AWS Security
Tags: Advanced (300);Amazon Detective;Amazon GuardDuty;AWS Security Hub;Customer Solutions;Security, Identity, & Compliance;Security Blog
Slug: 2024-10-22-how-to-use-the-amazon-detective-api-to-investigate-guardduty-security-findings-and-enrich-data-in-security-hub

[Source](https://aws.amazon.com/blogs/security/how-to-use-the-amazon-detective-api-to-investigate-guardduty-security-findings-and-enrich-data-in-security-hub/){:target="_blank" rel="noopener"}

> Understanding risk and identifying the root cause of an issue in a timely manner is critical to businesses. Amazon Web Services (AWS) offers multiple security services that you can use together to perform more timely investigations and improve the mean time to remediate issues. In this blog post, you will learn how to integrate Amazon [...]
