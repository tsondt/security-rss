Title: FBI created a cryptocurrency so it could watch it being abused
Date: 2024-10-11T05:28:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-10-11-fbi-created-a-cryptocurrency-so-it-could-watch-it-being-abused

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/11/fbi_nexfundai_crypto_fraud_sting/){:target="_blank" rel="noopener"}

> It worked – alleged pump and dump schemers arrested in UK, US and Portugal this week The FBI created its own cryptocurrency so it could watch suspected fraudsters use it – an idea that worked so well it produced arrests in three countries.... [...]
