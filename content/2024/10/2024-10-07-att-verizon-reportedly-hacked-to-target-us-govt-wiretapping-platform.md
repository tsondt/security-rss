Title: AT&T, Verizon reportedly hacked to target US govt wiretapping platform
Date: 2024-10-07T10:51:04-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-10-07-att-verizon-reportedly-hacked-to-target-us-govt-wiretapping-platform

[Source](https://www.bleepingcomputer.com/news/security/atandt-verizon-reportedly-hacked-to-target-us-govt-wiretapping-platform/){:target="_blank" rel="noopener"}

> Multiple U.S. broadband providers, including Verizon, AT&T, and Lumen Technologies, have been breached by a Chinese hacking group tracked as Salt Typhoon, the Wall Street Journal reports. [...]
