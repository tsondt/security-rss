Title: How to protect your site from DDoS attacks with the power of Google Cloud networking and network security
Date: 2024-10-07T16:00:00+00:00
Author: Max Saltonstall
Category: GCP Security
Tags: Developers & Practitioners;Application Modernization;Infrastructure;Startups;Security & Identity
Slug: 2024-10-07-how-to-protect-your-site-from-ddos-attacks-with-the-power-of-google-cloud-networking-and-network-security

[Source](https://cloud.google.com/blog/products/identity-security/how-to-protect-your-site-from-ddos-attacks-with-cloud-networking/){:target="_blank" rel="noopener"}

> Google Cloud constantly innovates and invests significantly in our capabilities to stop cyberattacks such as distributed denial-of-service attacks from taking down websites, apps, and services. It’s an essential part of protecting our customers. Our Project Shield offering, which uses Google's Cloud networking and our Global Front End infrastructure to help defend against attacks, including stopping one of the world's largest distributed denial-of-service (DDoS) attacks to date, uses elements of our Cloud Armor, Cloud CDN, and Load Balancing services. It combines them into a robust defense platform that can help keep key public-interest websites online in the face of constant attacks. [...]
