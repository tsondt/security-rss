Title: Patch Tuesday, October 2024 Edition
Date: 2024-10-08T22:21:19+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;Security Tools;Time to Patch;.NET;adobe;Adobe Framemaker;Adobe Substance 3D Painter;Animate;apple;Azure;Commerce;CVE-2024-43572;CVE-2024-43573;Dimension;Elastic Security Labs;GrimResource;Immersive Labs;InCopy;InDesign;Lightroom;macOS 15;MSHTML;Nikolas Cemerikic;Office;OpenSSH for Windows; Power BI; Windows Hyper-V; Windows Mobile Broadband;Satnam Narang;Sequoia;Substance 3D Stager;Tenable;Visual Studio
Slug: 2024-10-08-patch-tuesday-october-2024-edition

[Source](https://krebsonsecurity.com/2024/10/patch-tuesday-october-2024-edition/){:target="_blank" rel="noopener"}

> Microsoft today released security updates to fix at least 117 security holes in Windows computers and other software, including two vulnerabilities that are already seeing active attacks. Also, Adobe plugged 52 security holes across a range of products, and Apple has addressed a bug in its new macOS 15 “ Sequoia ” update that broke many cybersecurity tools. One of the zero-day flaws — CVE-2024-43573 — stems from a security weakness in MSHTML, the proprietary engine of Microsoft’s Internet Explorer web browser. If that sounds familiar it’s because this is the fourth MSHTML vulnerability found to be exploited in the [...]
