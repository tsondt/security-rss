Title: qBittorrent fixes flaw exposing users to MitM attacks for 14 years
Date: 2024-10-31T11:11:14-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2024-10-31-qbittorrent-fixes-flaw-exposing-users-to-mitm-attacks-for-14-years

[Source](https://www.bleepingcomputer.com/news/security/qbittorrent-fixes-flaw-exposing-users-to-mitm-attacks-for-14-years/){:target="_blank" rel="noopener"}

> qBittorrent has addressed a remote code execution flaw caused by the failure to validate SSL/TLS certificates in the application's DownloadManager, a component that manages downloads throughout the app. [...]
