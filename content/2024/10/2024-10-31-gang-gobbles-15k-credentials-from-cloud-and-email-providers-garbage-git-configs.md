Title: Gang gobbles 15K credentials from cloud and email providers' garbage Git configs
Date: 2024-10-31T23:59:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-31-gang-gobbles-15k-credentials-from-cloud-and-email-providers-garbage-git-configs

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/31/emeraldwhale_credential_theft/){:target="_blank" rel="noopener"}

> Emeraldwhale gang looked sharp – until it made a common S3 bucket mistake A criminal operation dubbed Emeraldwhale has been discovered after it dumped more than 15,000 credentials belonging to cloud service and email providers in an open AWS S3 bucket, according to security researchers.... [...]
