Title: Chinese cyberspies reportedly breached Verizon, AT&amp;T, Lumen
Date: 2024-10-07T17:17:54+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-07-chinese-cyberspies-reportedly-breached-verizon-att-lumen

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/07/verizon_att_lumen_salt_typhoon/){:target="_blank" rel="noopener"}

> Salt Typhoon may have accessed court-ordered wiretaps and US internet traffic Verizon, AT&T, and Lumen Technologies were among the US broadband providers whose networks were reportedly hacked by Chinese cyberspies, possibly compromising the wiretapping systems used for court-ordered surveillance.... [...]
