Title: China’s Spamouflage cranks up trolling of US Senator Rubio as election day looms
Date: 2024-10-21T22:30:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-21-chinas-spamouflage-cranks-up-trolling-of-us-senator-rubio-as-election-day-looms

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/21/china_spamouflage_trolls_marc_rubio/){:target="_blank" rel="noopener"}

> Note to Xi: Marco and Ted Cruz aren't the same person China's Spamouflage disinformation crew has been targeting US Senator Marco Rubio (R-Florida) with its fake news campaigns over the past couple of months, trolling the Republican lawmaker's official X account and posting negative stories about Rubio on Reddit and Medium.... [...]
