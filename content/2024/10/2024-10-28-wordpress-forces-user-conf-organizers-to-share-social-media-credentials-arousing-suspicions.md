Title: WordPress forces user conf organizers to share social media credentials, arousing suspicions
Date: 2024-10-28T06:27:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-10-28-wordpress-forces-user-conf-organizers-to-share-social-media-credentials-arousing-suspicions

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/28/wordcamp_password_sharing_requirement/){:target="_blank" rel="noopener"}

> One told to take down posts that said nice things about WP Engine Organisers of WordCamps, community-organized events for WordPress users, have been ordered to take down some social media posts and share their login credentials for social networks.... [...]
