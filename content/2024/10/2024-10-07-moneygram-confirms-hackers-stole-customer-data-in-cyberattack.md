Title: MoneyGram confirms hackers stole customer data in cyberattack
Date: 2024-10-07T18:52:45-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-10-07-moneygram-confirms-hackers-stole-customer-data-in-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/moneygram-confirms-hackers-stole-customer-data-in-cyberattack/){:target="_blank" rel="noopener"}

> MoneyGram has confirmed that hackers stole customers' personal information and transaction data in a September cyberattack that caused a five-day outage. [...]
