Title: FBI arrest Alabama man suspected of hacking SEC's X account
Date: 2024-10-17T14:21:35-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-10-17-fbi-arrest-alabama-man-suspected-of-hacking-secs-x-account

[Source](https://www.bleepingcomputer.com/news/security/fbi-arrest-alabama-man-suspected-of-hacking-secs-x-account/){:target="_blank" rel="noopener"}

> An Alabama man was arrested today by the FBI for his suspected role in hacking the SEC's X account to make a fake announcement that Bitcoin ETFs were approved. [...]
