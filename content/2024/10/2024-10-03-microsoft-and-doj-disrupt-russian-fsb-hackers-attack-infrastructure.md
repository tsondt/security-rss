Title: Microsoft and DOJ disrupt Russian FSB hackers' attack infrastructure
Date: 2024-10-03T13:58:09-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-10-03-microsoft-and-doj-disrupt-russian-fsb-hackers-attack-infrastructure

[Source](https://www.bleepingcomputer.com/news/security/microsoft-and-doj-seize-spear-phishing-domains-used-by-star-blizzard-russian-hackers/){:target="_blank" rel="noopener"}

> Microsoft and the Justice Department have seized over 100 domains used by the Russian ColdRiver hacking group to target United States government employees and nonprofit organizations from Russia and worldwide in spear-phishing attacks. [...]
