Title: Google removes Kaspersky's antivirus software from Play Store
Date: 2024-10-04T12:03:35-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Google
Slug: 2024-10-04-google-removes-kasperskys-antivirus-software-from-play-store

[Source](https://www.bleepingcomputer.com/news/security/google-removes-kasperskys-antivirus-software-from-play-store-disables-developer-accounts/){:target="_blank" rel="noopener"}

> Over the weekend, Google removed Kaspersky's Android security apps from the Google Play store and disabled the Russian company's developer accounts. [...]
