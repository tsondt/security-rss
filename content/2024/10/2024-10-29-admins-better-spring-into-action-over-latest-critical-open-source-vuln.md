Title: Admins better Spring into action over latest critical open source vuln
Date: 2024-10-29T14:33:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-29-admins-better-spring-into-action-over-latest-critical-open-source-vuln

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/29/admins_spring_into_action_over/){:target="_blank" rel="noopener"}

> Patch up: The Spring framework dominates the Java ecosystem If you're running an application built using the Spring development framework, now is a good time to check it's fully updated – a new, critical-severity vulnerability has just been disclosed.... [...]
