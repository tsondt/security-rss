Title: Exploring digital sovereignty: learning opportunities at re:Invent 2024
Date: 2024-10-24T13:41:22+00:00
Author: Marta Taggart
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS Re:Invent;Data protection;Digital Sovereignty;Live Events;Security Blog
Slug: 2024-10-24-exploring-digital-sovereignty-learning-opportunities-at-reinvent-2024

[Source](https://aws.amazon.com/blogs/security/exploring-digital-sovereignty-learning-opportunities-at-reinvent-2024/){:target="_blank" rel="noopener"}

> AWS re:Invent 2024, a learning conference hosted by Amazon Web Services (AWS) for the global cloud computing community, will take place December 2–6, 2024, in Las Vegas, Nevada, across multiple venues. At re:Invent, you can join cloud enthusiasts from around the world to hear the latest cloud industry innovations, meet with AWS experts, and build [...]
