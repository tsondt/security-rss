Title: American Water rinsed in cyber attack, turns off app
Date: 2024-10-07T21:30:12+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-10-07-american-water-rinsed-in-cyber-attack-turns-off-app

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/07/american_water_cyberattack/){:target="_blank" rel="noopener"}

> It's still safe to drink, top provider tells us Updated American Water, which supplies over 14 million people in the US and numerous military bases, has stopped issuing bills and has taken its MyWater app offline while it investigates a cyber attack on its systems.... [...]
