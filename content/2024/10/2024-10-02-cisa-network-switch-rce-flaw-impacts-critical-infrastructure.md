Title: CISA: Network switch RCE flaw impacts critical infrastructure
Date: 2024-10-02T11:02:40-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-10-02-cisa-network-switch-rce-flaw-impacts-critical-infrastructure

[Source](https://www.bleepingcomputer.com/news/security/cisa-network-switch-rce-flaw-impacts-critical-infrastructure/){:target="_blank" rel="noopener"}

> U.S. cybersecurity agency CISA is warning about two critical vulnerabilities that allow authentication bypass and remote code execution in Optigo Networks ONS-S8 Aggregation Switch products used in critical infrastructure. [...]
