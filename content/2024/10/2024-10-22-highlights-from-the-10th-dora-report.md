Title: Highlights from the 10th DORA report
Date: 2024-10-22T16:00:00+00:00
Author: Derek DeBellis
Category: GCP Security
Tags: Security & Identity;Developers & Practitioners;DevOps & SRE
Slug: 2024-10-22-highlights-from-the-10th-dora-report

[Source](https://cloud.google.com/blog/products/devops-sre/announcing-the-2024-dora-report/){:target="_blank" rel="noopener"}

> The DORA research program has been investigating the capabilities, practices, and measures of high-performing technology-driven teams and organizations for more than a decade. It has published reports based on data collected from annual surveys of professionals working in technical roles, including software developers, managers, and senior executives. Today, we’re pleased to announce the publication of the 2024 Accelerate State of DevOps Report, marking a decade of DORA’s investigation into high-performing technology teams and organizations. DORA’s four key metrics, introduced in 2013, have become the industry standard for measuring software delivery performance. Each year, we seek to gain a comprehensive understanding [...]
