Title: Dutch cops reveal takedown of 'world's largest dark web market'
Date: 2024-10-10T06:30:14+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-10-10-dutch-cops-reveal-takedown-of-worlds-largest-dark-web-market

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/10/cannabia_bohemia_darkweb_market_investigation/){:target="_blank" rel="noopener"}

> Two arrested after allegedly trying to make off with their ill-gotten gains The alleged administrators of the infamous Bohemia and Cannabia dark web marketplaces have been arrested after apparently shuttering the sites and trying to flee with their earnings.... [...]
