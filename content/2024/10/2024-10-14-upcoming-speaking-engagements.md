Title: Upcoming Speaking Engagements
Date: 2024-10-14T16:49:08+00:00
Author: B. Schneier
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2024-10-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2024/10/upcoming-speaking-engagements-41.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking at SOSS Fusion 2024 in Atlanta, Georgia, USA. The event will be held on October 22 and 23, 2024, and my talk is at 9:15 AM ET on October 22, 2024. The list is maintained on this page. [...]
