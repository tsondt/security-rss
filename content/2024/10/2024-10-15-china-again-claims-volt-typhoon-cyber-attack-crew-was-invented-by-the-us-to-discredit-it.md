Title: China again claims Volt Typhoon cyber-attack crew was invented by the US to discredit it
Date: 2024-10-15T01:15:08+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-10-15-china-again-claims-volt-typhoon-cyber-attack-crew-was-invented-by-the-us-to-discredit-it

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/15/china_volt_typhoon_false_flag/){:target="_blank" rel="noopener"}

> Enough with the racist-sounding 'dragons' and 'pandas', Beijing complains – then points the finger at koalas Chinese authorities have published another set of allegations that assert the Volt Typhoon cyber-crew is an invention of the US and its allies, and not a crew run by Beijing.... [...]
