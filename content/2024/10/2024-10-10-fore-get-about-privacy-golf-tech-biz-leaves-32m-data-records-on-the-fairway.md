Title: Fore-get about privacy, golf tech biz leaves 32M data records on the fairway
Date: 2024-10-10T14:14:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-10-fore-get-about-privacy-golf-tech-biz-leaves-32m-data-records-on-the-fairway

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/10/trackman_unprotected_database/){:target="_blank" rel="noopener"}

> Researcher spots 110 TB of sensitive info sitting in unprotected database Nearly 32 million records belonging to users of tech from Trackman were left exposed to the internet, sitting in a non-password protected database, for an undetermined amount of time, according to researcher Jeremiah Fowler.... [...]
