Title: Crypto-apocalypse soon? Chinese researchers find a potential quantum attack on classical encryption
Date: 2024-10-14T06:30:09+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-10-14-crypto-apocalypse-soon-chinese-researchers-find-a-potential-quantum-attack-on-classical-encryption

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/14/china_quantum_attack/){:target="_blank" rel="noopener"}

> With an off-the-shelf D-Wave machine, but only against very short keys Chinese researchers claim they have found a way to use D-Wave's quantum annealing systems to develop a promising attack on classical encryption.... [...]
