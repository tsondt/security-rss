Title: Internet Archive hacked, data breach impacts 31 million users
Date: 2024-10-09T18:22:20-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-10-09-internet-archive-hacked-data-breach-impacts-31-million-users

[Source](https://www.bleepingcomputer.com/news/security/internet-archive-hacked-data-breach-impacts-31-million-users/){:target="_blank" rel="noopener"}

> Internet Archive's "The Wayback Machine" has suffered a data breach after a threat actor compromised the website and stole a user authentication database containing 31 million unique records. [...]
