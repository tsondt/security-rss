Title: US contractor pays $300k to settle accusation it didn't properly look after Medicare users' data
Date: 2024-10-16T23:15:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-16-us-contractor-pays-300k-to-settle-accusation-it-didnt-properly-look-after-medicare-users-data

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/16/us_contractor_pays_300k_in/){:target="_blank" rel="noopener"}

> Resolves allegations it improperly stored screenshots containing PII that were later snaffled A US government contractor will settle claims it violated cyber security rules prior to a breach that compromised Medicare beneficiaries' personal data.... [...]
