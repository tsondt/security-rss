Title: Microsoft says more ransomware stopped before reaching encryption
Date: 2024-10-15T16:45:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-15-microsoft-says-more-ransomware-stopped-before-reaching-encryption

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/15/microsoft_ransomware_attacks/){:target="_blank" rel="noopener"}

> Volume of attacks still surging though, according to Digital Defense Report Microsoft says ransomware attacks are up 2.75 times compared to last year, but claims defenses are actually working better than ever.... [...]
