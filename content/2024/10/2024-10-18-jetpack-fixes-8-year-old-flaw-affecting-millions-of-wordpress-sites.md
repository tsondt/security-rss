Title: Jetpack fixes 8-year-old flaw affecting millions of WordPress sites
Date: 2024-10-18T22:30:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-10-18-jetpack-fixes-8-year-old-flaw-affecting-millions-of-wordpress-sites

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/18/jetpack_patches_wordpress_vulnerability/){:target="_blank" rel="noopener"}

> Also, new EU cyber reporting rules are live, exploiters hit the gas pedal, free PDNS for UK schools, and more in brief A critical security update for the near-ubiquitous WordPress plugin Jetpack was released last week. Site administrators should ensure the latest version is installed to keep their sites secure.... [...]
