Title: How to use interface VPC endpoints to meet your security objectives
Date: 2024-10-22T13:02:19+00:00
Author: Joaquin Manuel Rinaudo
Category: AWS Security
Tags: Best Practices;Intermediate (200);Technical How-to;Amazon VPC;AWS Privatelink;Compliance;Identity;Security;Security Blog;VPC endpoint
Slug: 2024-10-22-how-to-use-interface-vpc-endpoints-to-meet-your-security-objectives

[Source](https://aws.amazon.com/blogs/security/how-to-use-interface-vpc-endpoints-to-meet-your-security-objectives/){:target="_blank" rel="noopener"}

> October 28, 2024: We updated the text and figure for security objective 1 to show Amazon Route 53 Resolver DNS Firewall. Amazon Virtual Private Cloud (Amazon VPC) endpoints—powered by AWS PrivateLink—enable customers to establish private connectivity to supported AWS services, enterprise services, and third-party services by using private IP addresses. There are three types of [...]
