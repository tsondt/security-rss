Title: Brits hate how big tech handles their data, but can't be bothered to do much about it
Date: 2024-10-03T09:15:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-03-brits-hate-how-big-tech-handles-their-data-but-cant-be-bothered-to-do-much-about-it

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/03/dsit_web_tracking_survey/){:target="_blank" rel="noopener"}

> Managing the endless stream of cookie banners leaves little energy for anything else Fewer than one in five Brits report being happy with the way their personal data is handled by big tech companies, yet the furthest many will go is to reject optional cookies on the web.... [...]
