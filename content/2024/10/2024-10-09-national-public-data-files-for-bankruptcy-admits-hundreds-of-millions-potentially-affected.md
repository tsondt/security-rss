Title: National Public Data files for bankruptcy, admits 'hundreds of millions' potentially affected
Date: 2024-10-09T19:30:15+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-10-09-national-public-data-files-for-bankruptcy-admits-hundreds-of-millions-potentially-affected

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/09/national_public_data_bankrupt/){:target="_blank" rel="noopener"}

> One-man-band faces a mountain of lawsuits but has few assets The Florida business behind data brokerage National Public Data has filed for bankruptcy, admitting "hundreds of millions" of people were potentially affected in one of the largest information leaks of the year.... [...]
