Title: 'Patch yesterday': Zimbra mail servers under siege through RCE vuln
Date: 2024-10-02T10:50:45+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-02-patch-yesterday-zimbra-mail-servers-under-siege-through-rce-vuln

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/02/mass_exploitation_of_zimbra_rce/){:target="_blank" rel="noopener"}

> Attacks began the day after public disclosure "Patch yesterday" is the advice from infosec researchers as the latest critical vulnerability affecting Zimbra mail servers is now being mass-exploited.... [...]
