Title: Join Ars in DC for infrastructure, cocktails, and spycraft on October 29
Date: 2024-10-10T14:26:47+00:00
Author: Lee Hutchinson
Category: Ars Technica
Tags: Biz & IT;AI;event;ibm;ibm event;Infrastructure
Slug: 2024-10-10-join-ars-in-dc-for-infrastructure-cocktails-and-spycraft-on-october-29

[Source](https://arstechnica.com/information-technology/2024/10/join-ars-in-dc-for-infrastructure-cocktails-and-spycraft-on-october-29/){:target="_blank" rel="noopener"}

> After a great event last month in San Jose, Ars is switching coasts for October and descending in force on our nation's capital. If you're on the East Coast and want to come hang out with Ars EIC Ken Fisher and me while we talk to some neat speakers and learn some stuff, then read on! Continuing our partnership with IBM, Ars presents "AI in DC: Privacy, Compliance, and Making Infrastructure Smarter." Our tone this time around will be a little more policy-oriented than our San Jose event. We intend to have three panel discussions, with the overall topics looking [...]
