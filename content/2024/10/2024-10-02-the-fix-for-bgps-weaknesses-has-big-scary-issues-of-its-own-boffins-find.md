Title: The fix for BGP's weaknesses has big, scary, issues of its own, boffins find
Date: 2024-10-02T06:31:07+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-10-02-the-fix-for-bgps-weaknesses-has-big-scary-issues-of-its-own-boffins-find

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/02/rpki_immaturity_study/){:target="_blank" rel="noopener"}

> Bother, given the White House has bet big on RPKI – just like we all rely on immature internet infrastructure that usually works The Resource Public Key Infrastructure (RPKI) protocol has "software vulnerabilities, inconsistent specifications, and operational challenges" according to a pre-press paper from a trio of German researchers.... [...]
