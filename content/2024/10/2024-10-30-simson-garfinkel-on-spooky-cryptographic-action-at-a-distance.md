Title: Simson Garfinkel on Spooky Cryptographic Action at a Distance
Date: 2024-10-30T14:48:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptography;quantum cryptography
Slug: 2024-10-30-simson-garfinkel-on-spooky-cryptographic-action-at-a-distance

[Source](https://www.schneier.com/blog/archives/2024/10/simson-garfinkel-on-spooky-cryptographic-action-at-a-distance.html){:target="_blank" rel="noopener"}

> Excellent read. One example: Consider the case of basic public key cryptography, in which a person’s public and private key are created together in a single operation. These two keys are entangled, not with quantum physics, but with math. When I create a virtual machine server in the Amazon cloud, I am prompted for an RSA public key that will be used to control access to the machine. Typically, I create the public and private keypair on my laptop and upload the public key to Amazon, which bakes my public key into the server’s administrator account. My laptop and that [...]
