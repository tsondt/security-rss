Title: Uncle Sam puts $10M bounty on Russian troll farm Rybar
Date: 2024-10-18T01:00:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-18-uncle-sam-puts-10m-bounty-on-russian-troll-farm-rybar

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/18/us_rybar_bounty/){:target="_blank" rel="noopener"}

> Propaganda op focuses on anti-West narratives to meddle with elections The US has placed a $10 million bounty on Russian media network Rybar and a number of its key staffers following alleged attempts to sway the upcoming US presidential election.... [...]
