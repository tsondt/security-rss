Title: Sophos reveals 5-year battle with Chinese hackers attacking network devices
Date: 2024-10-31T18:16:52-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-10-31-sophos-reveals-5-year-battle-with-chinese-hackers-attacking-network-devices

[Source](https://www.bleepingcomputer.com/news/security/sophos-reveals-5-year-battle-with-chinese-hackers-attacking-network-devices/){:target="_blank" rel="noopener"}

> Sophos disclosed today a series of reports dubbed "Pacific Rim" that detail how the cybersecurity company has been sparring with Chinese threat actors for over 5 years as they increasingly targeted networking devices worldwide, including those from Sophos. [...]
