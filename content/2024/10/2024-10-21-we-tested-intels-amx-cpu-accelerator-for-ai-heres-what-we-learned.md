Title: We tested Intel’s AMX CPU accelerator for AI. Here’s what we learned
Date: 2024-10-21T16:00:00+00:00
Author: Zhimin Yao
Category: GCP Security
Tags: Security & Identity
Slug: 2024-10-21-we-tested-intels-amx-cpu-accelerator-for-ai-heres-what-we-learned

[Source](https://cloud.google.com/blog/products/identity-security/we-tested-intels-amx-cpu-accelerator-for-ai-heres-what-we-learned/){:target="_blank" rel="noopener"}

> At Google Cloud, we believe that cloud computing will increasingly shift to private, encrypted services where users can be confident that their software and data are not being exposed to unauthorized actors. In support of this goal, our aim is to make confidential computing technology ubiquitous across our Cloud without compromising usability, performance, and scale. The latest C3 Confidential VMs we’ve developed with Intel have a built-in, on by default accelerator: Intel AMX. Intel AMX improves the performance of deep-learning training and inference on the CPU, and is ideal for workloads including natural-language processing, recommendation systems, and image recognition. These [...]
