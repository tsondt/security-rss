Title: 700K+ DrayTek routers are sitting ducks on the internet, open to remote hijacking
Date: 2024-10-02T21:33:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-02-700k-draytek-routers-are-sitting-ducks-on-the-internet-open-to-remote-hijacking

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/02/draytek_routers_bugs/){:target="_blank" rel="noopener"}

> With 14 serious security flaws found, what a gift for spies and crooks Fourteen newly found bugs in DrayTek Vigor routers — including one critical remote-code-execution flaw that received a perfect 10 out of 10 CVSS severity rating — could be abused by crooks looking to seize control of the equipment to then steal sensitive data, deploy ransomware, and launch denial-of-service attacks.... [...]
