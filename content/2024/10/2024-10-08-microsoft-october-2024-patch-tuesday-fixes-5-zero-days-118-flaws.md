Title: Microsoft October 2024 Patch Tuesday fixes 5 zero-days, 118 flaws
Date: 2024-10-08T14:16:37-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-10-08-microsoft-october-2024-patch-tuesday-fixes-5-zero-days-118-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-october-2024-patch-tuesday-fixes-5-zero-days-118-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's October 2024 Patch Tuesday, which includes security updates for 118 flaws, including five publicly disclosed zero-days, two of which are actively exploited. [...]
