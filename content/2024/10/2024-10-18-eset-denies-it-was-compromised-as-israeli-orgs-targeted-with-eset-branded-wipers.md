Title: ESET denies it was compromised as Israeli orgs targeted with 'ESET-branded' wipers
Date: 2024-10-18T11:00:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-18-eset-denies-it-was-compromised-as-israeli-orgs-targeted-with-eset-branded-wipers

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/18/eset_denies_israel_branch_breach/){:target="_blank" rel="noopener"}

> Says 'limited' incident isolated to 'partner company' ESET denies being compromised after an infosec researcher highlighted a wiper campaign that appeared to victims as if it was launched using the Slovak security shop's infrastructure.... [...]
