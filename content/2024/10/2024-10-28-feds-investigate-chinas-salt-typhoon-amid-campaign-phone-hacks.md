Title: Feds investigate China's Salt Typhoon amid campaign phone hacks
Date: 2024-10-28T20:00:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-28-feds-investigate-chinas-salt-typhoon-amid-campaign-phone-hacks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/28/feds_investigate_chinas_salt_typhoon/){:target="_blank" rel="noopener"}

> 'They're taunting us,' investigator says and it looks like it's working The feds are investigating Chinese government-linked cyberspies breaking into the infrastructure of US telecom companies, as reports suggest Salt Typhoon - the same crew believed to be behind those hacks - has also been targeting phones belonging to people affiliated with US Democratic presidential candidate Kamala Harris, along with Republican candidate Donald Trump and his running mate, JD Vance.... [...]
