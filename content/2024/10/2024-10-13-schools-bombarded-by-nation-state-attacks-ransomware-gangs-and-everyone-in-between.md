Title: Schools bombarded by nation-state attacks, ransomware gangs, and everyone in between
Date: 2024-10-13T13:00:05+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-13-schools-bombarded-by-nation-state-attacks-ransomware-gangs-and-everyone-in-between

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/13/schools_nationstate_attacks_ransomware/){:target="_blank" rel="noopener"}

> Reading, writing, and cyber mayhem, amirite? If we were to draw an infosec Venn diagram, with one circle representing "sensitive info that attackers would want to steal" and the other "limited resources plus difficult-to-secure IT environments," education would sit in the overlap.... [...]
