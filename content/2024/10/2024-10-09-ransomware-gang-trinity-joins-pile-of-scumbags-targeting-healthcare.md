Title: Ransomware gang Trinity joins pile of scumbags targeting healthcare
Date: 2024-10-09T13:45:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-10-09-ransomware-gang-trinity-joins-pile-of-scumbags-targeting-healthcare

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/09/trinity_ransomware_targets_healthcare_orgs/){:target="_blank" rel="noopener"}

> As if hospitals and clinics didn't have enough to worry about At least one US healthcare provider has been infected by Trinity, an emerging cybercrime gang with eponymous ransomware that uses double extortion and other "sophisticated" tactics that make it a "significant threat," according to the feds.... [...]
