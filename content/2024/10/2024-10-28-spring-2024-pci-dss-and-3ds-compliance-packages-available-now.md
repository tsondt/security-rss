Title: Spring 2024 PCI DSS and 3DS compliance packages available now
Date: 2024-10-28T20:45:49+00:00
Author: Ramone Weyerhaeuser
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Compliance;Compliance reports;PCI;PCI DSS;Security Blog
Slug: 2024-10-28-spring-2024-pci-dss-and-3ds-compliance-packages-available-now

[Source](https://aws.amazon.com/blogs/security/spring-2024-pci-dss-and-3ds-compliance-packages-available-now/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce that three new AWS services have been added to the scope of our Payment Card Industry Data Security Standard (PCI DSS) and Payment Card Industry Three Domain Secure (PCI 3DS) certifications: Amazon DataZone Amazon DevOps Guru Amazon Managed Grafana You can see the full list of services [...]
