Title: INC ransomware rebrands to Lynx – same code, new name, still up to no good
Date: 2024-10-11T23:00:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-10-11-inc-ransomware-rebrands-to-lynx-same-code-new-name-still-up-to-no-good

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/11/inc_ransomware_lynx/){:target="_blank" rel="noopener"}

> Researchers point to evidence that scumbags visited the strategy boutique Researchers at Palo Alto's Unit 42 believe the INC ransomware crew is no more and recently rebranded itself as Lynx over a three-month period.... [...]
