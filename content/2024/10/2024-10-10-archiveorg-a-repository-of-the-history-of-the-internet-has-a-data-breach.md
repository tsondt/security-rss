Title: Archive.org, a repository of the history of the Internet, has a data breach
Date: 2024-10-10T00:12:56+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security
Slug: 2024-10-10-archiveorg-a-repository-of-the-history-of-the-internet-has-a-data-breach

[Source](https://arstechnica.com/information-technology/2024/10/archive-org-a-repository-storing-the-entire-history-of-the-internet-has-been-hacked/){:target="_blank" rel="noopener"}

> Archive.org, one of the only entities to attempt to preserve the entire history of the World Wide Web and much of the broader Internet, was recently compromised in a hack that revealed data on roughly 31 million users. A little after 2 pm California time, social media sites became awash with screenshots showing what the archive.org homepage displayed. It read: Read full article Comments [...]
