Title: GitLab warns of critical arbitrary branch pipeline execution flaw
Date: 2024-10-10T11:12:51-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-10-10-gitlab-warns-of-critical-arbitrary-branch-pipeline-execution-flaw

[Source](https://www.bleepingcomputer.com/news/security/gitlab-warns-of-critical-arbitrary-branch-pipeline-execution-flaw/){:target="_blank" rel="noopener"}

> GitLab has released security updates to address multiple flaws in Community Edition (CE) and Enterprise Edition (EE), including a critical arbitrary branch pipeline execution flaw. [...]
