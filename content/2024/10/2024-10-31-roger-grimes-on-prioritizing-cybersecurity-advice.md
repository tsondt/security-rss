Title: Roger Grimes on Prioritizing Cybersecurity Advice
Date: 2024-10-31T15:43:16+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cybersecurity;patching;two-factor authentication
Slug: 2024-10-31-roger-grimes-on-prioritizing-cybersecurity-advice

[Source](https://www.schneier.com/blog/archives/2024/10/roger-grimes-on-prioritizing-cybersecurity-advice.html){:target="_blank" rel="noopener"}

> This is a good point : Part of the problem is that we are constantly handed lists...list of required controls...list of things we are being asked to fix or improve...lists of new projects...lists of threats, and so on, that are not ranked for risks. For example, we are often given a cybersecurity guideline (e.g., PCI-DSS, HIPAA, SOX, NIST, etc.) with hundreds of recommendations. They are all great recommendations, which if followed, will reduce risk in your environment. What they do not tell you is which of the recommended things will have the most impact on best reducing risk in your [...]
