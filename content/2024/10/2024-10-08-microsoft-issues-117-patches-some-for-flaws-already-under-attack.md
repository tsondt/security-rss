Title: Microsoft issues 117 patches – some for flaws already under attack
Date: 2024-10-08T23:30:11+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-10-08-microsoft-issues-117-patches-some-for-flaws-already-under-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/10/08/patch_tuesday_october_2024/){:target="_blank" rel="noopener"}

> Plus: SAP re-patches a failed patch for critical-rated flaw Patch Tuesday It's the second Tuesday of the month, which means Patch Tuesday, bringing with it fixes for numerous flaws, bugs and vulnerabilities in major software. And this one is a doozy.... [...]
