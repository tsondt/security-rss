Title: Crypto scammers circle back, pose as lawyers, steal an extra $10M in truly devious plan
Date: 2024-06-25T18:28:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-25-crypto-scammers-circle-back-pose-as-lawyers-steal-an-extra-10m-in-truly-devious-plan

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/25/predators_steal_additional_10m/){:target="_blank" rel="noopener"}

> Business is more lucrative than you might think The FBI says in just 12 months, scumbags stole circa $10 million from victims of crypto scams after posing as helpful lawyers offering to recover their lost tokens.... [...]
