Title: Toronto District School Board hit by a ransomware attack
Date: 2024-06-13T14:43:23-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Education
Slug: 2024-06-13-toronto-district-school-board-hit-by-a-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/toronto-district-school-board-hit-by-a-ransomware-attack/){:target="_blank" rel="noopener"}

> The Toronto District School Board (TDSB) is warning that it suffered a ransomware attack on its software testing environment and is now investigating whether any personal information was exposed. [...]
