Title: Gitloker attacks abuse GitHub notifications to push malicious oAuth apps
Date: 2024-06-10T18:24:16-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-06-10-gitloker-attacks-abuse-github-notifications-to-push-malicious-oauth-apps

[Source](https://www.bleepingcomputer.com/news/security/gitloker-attacks-abuse-github-notifications-to-push-malicious-oauth-apps/){:target="_blank" rel="noopener"}

> Threat actors impersonate GitHub's security and recruitment teams in phishing attacks to hijack repositories using malicious OAuth apps in an ongoing extortion campaign wiping compromised repos. [...]
