Title: 23andMe data breach under investigation in UK and Canada
Date: 2024-06-10T11:00:28-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-06-10-23andme-data-breach-under-investigation-in-uk-and-canada

[Source](https://www.bleepingcomputer.com/news/security/23andme-data-breach-under-investigation-in-uk-and-canada/){:target="_blank" rel="noopener"}

> Privacy authorities in Canada and the United Kingdom have launched a joint investigation to assess the scope of sensitive customer information exposed in last year's 23andMe data breach. [...]
