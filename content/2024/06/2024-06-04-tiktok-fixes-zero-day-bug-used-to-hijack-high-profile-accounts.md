Title: TikTok fixes zero-day bug used to hijack high-profile accounts
Date: 2024-06-04T17:57:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-06-04-tiktok-fixes-zero-day-bug-used-to-hijack-high-profile-accounts

[Source](https://www.bleepingcomputer.com/news/security/tiktok-fixes-zero-day-bug-used-to-hijack-high-profile-accounts/){:target="_blank" rel="noopener"}

> Over the past week, attackers have hijacked high-profile TikTok accounts belonging to multiple companies and celebrities, exploiting a zero-day vulnerability in the social media's direct messages feature. [...]
