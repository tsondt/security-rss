Title: VMware by Broadcom warns of two critical vCenter flaws, plus a nasty sudo bug
Date: 2024-06-18T06:08:23+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-06-18-vmware-by-broadcom-warns-of-two-critical-vcenter-flaws-plus-a-nasty-sudo-bug

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/18/vmware_criticial_vcenter_flaws/){:target="_blank" rel="noopener"}

> Specially crafted network packet could allow remote code execution and access to VM fleets VMware by Broadcom has revealed a pair of critical-rated flaws in vCenter Server – the tool used to manage virtual machines and hosts in its flagship Cloud Foundation and vSphere suites.... [...]
