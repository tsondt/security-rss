Title: Pure Storage pwned, claims data plundered by crims who broke into Snowflake workspace
Date: 2024-06-11T18:01:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-11-pure-storage-pwned-claims-data-plundered-by-crims-who-broke-into-snowflake-workspace

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/11/pure_storage_snowflake_breach/){:target="_blank" rel="noopener"}

> Secure storage company hasn't spilled details on how they got in Pure Storage is the latest company to confirm it's a victim of mounting Snowflake-related data breaches.... [...]
