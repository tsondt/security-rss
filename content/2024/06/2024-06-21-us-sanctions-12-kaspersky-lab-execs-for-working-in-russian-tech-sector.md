Title: US sanctions 12 Kaspersky Lab execs for working in Russian tech sector
Date: 2024-06-21T13:32:05-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-21-us-sanctions-12-kaspersky-lab-execs-for-working-in-russian-tech-sector

[Source](https://www.bleepingcomputer.com/news/security/us-sanctions-12-kaspersky-lab-execs-for-working-in-russian-tech-sector/){:target="_blank" rel="noopener"}

> The Treasury Department's Office of Foreign Assets Control (OFAC) has sanctioned twelve Kaspersky Lab executives for operating in the technology sector of Russia. [...]
