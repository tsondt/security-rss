Title: High-severity vulnerabilities affect a wide range of Asus router models
Date: 2024-06-17T18:39:41+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;ASUS;routers;vulnerabilities
Slug: 2024-06-17-high-severity-vulnerabilities-affect-a-wide-range-of-asus-router-models

[Source](https://arstechnica.com/?p=2031993){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Hardware manufacturer Asus has released updates patching multiple critical vulnerabilities that allow hackers to remotely take control of a range of router models with no authentication or interaction required of end users. The most critical vulnerability, tracked as CVE-2024-3080 is an authentication bypass flaw that can allow remote attackers to log into a device without authentication. The vulnerability, according to the Taiwan Computer Emergency Response Team / Coordination Center (TWCERT/CC), carries a severity rating of 9.8 out of 10. Asus said the vulnerability affects the following routers: Model name Support Site link XT8 and XT8_V2 https://www.asus.com/uk/supportonly/asus%20zenwifi%20ax%20(xt8)/helpdesk_bios/ [...]
