Title: Ratel RAT targets outdated Android phones in ransomware attacks
Date: 2024-06-22T10:19:38-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile
Slug: 2024-06-22-ratel-rat-targets-outdated-android-phones-in-ransomware-attacks

[Source](https://www.bleepingcomputer.com/news/security/ratel-rat-targets-outdated-android-phones-in-ransomware-attacks/){:target="_blank" rel="noopener"}

> An open-source Android malware named 'Ratel RAT' is widely deployed by multiple cybercriminals to attack outdated devices, some aiming to lock them down with a ransomware module that demands payment on Telegram. [...]
