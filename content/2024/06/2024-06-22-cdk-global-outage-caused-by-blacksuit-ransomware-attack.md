Title: CDK Global outage caused by BlackSuit ransomware attack
Date: 2024-06-22T15:08:43-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-22-cdk-global-outage-caused-by-blacksuit-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/cdk-global-outage-caused-by-blacksuit-ransomware-attack/){:target="_blank" rel="noopener"}

> The BlackSuit ransomware gang is behind CDK Global's massive IT outage and disruption to car dealerships across North America, according to multiple sources familiar with the matter. [...]
