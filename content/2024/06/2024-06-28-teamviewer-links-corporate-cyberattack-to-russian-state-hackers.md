Title: TeamViewer links corporate cyberattack to Russian state hackers
Date: 2024-06-28T10:42:24-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-28-teamviewer-links-corporate-cyberattack-to-russian-state-hackers

[Source](https://www.bleepingcomputer.com/news/security/teamviewer-links-corporate-cyberattack-to-russian-state-hackers/){:target="_blank" rel="noopener"}

> RMM software developer TeamViewer says a Russian state-sponsored hacking group known as Midnight Blizzard is believed to be behind a breach of their corporate network this week. [...]
