Title: Fiend touts stolen Neiman Marcus customer info for $150K
Date: 2024-06-25T20:27:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-25-fiend-touts-stolen-neiman-marcus-customer-info-for-150k

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/25/neiman_marcus_snowflake_victim/){:target="_blank" rel="noopener"}

> Flash clobber chain fashionably late to Snowflake fiasco party Customer information said to have been stolen from Neiman Marcus's Snowflake instance has been put up for sale on the dark web for $150,000.... [...]
