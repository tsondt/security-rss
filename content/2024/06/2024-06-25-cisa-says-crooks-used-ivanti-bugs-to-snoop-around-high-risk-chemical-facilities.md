Title: CISA says crooks used Ivanti bugs to snoop around high-risk chemical facilities
Date: 2024-06-25T13:45:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-25-cisa-says-crooks-used-ivanti-bugs-to-snoop-around-high-risk-chemical-facilities

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/25/cisa_ivanti_chemical_facilities/){:target="_blank" rel="noopener"}

> Crafty crims broke in but encryption stopped any nastiness US cybersecurity agency CISA is urging high-risk chemical facilities to secure their online accounts after someone broke into its Chemical Security Assessment Tool (CSAT) portal.... [...]
