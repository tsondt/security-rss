Title: Major London hospitals disrupted by Synnovis ransomware attack
Date: 2024-06-04T12:05:21-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-06-04-major-london-hospitals-disrupted-by-synnovis-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/major-london-hospitals-disrupted-by-synnovis-ransomware-attack/){:target="_blank" rel="noopener"}

> A ransomware attack affecting pathology and diagnostic services provider Synnovis has impacted healthcare services at multiple major NHS hospitals in London. [...]
