Title: Qilin ransomware gang linked to attack on London hospitals
Date: 2024-06-05T13:57:10-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-06-05-qilin-ransomware-gang-linked-to-attack-on-london-hospitals

[Source](https://www.bleepingcomputer.com/news/security/qilin-ransomware-gang-linked-to-attack-on-london-hospitals/){:target="_blank" rel="noopener"}

> A ransomware attack that hit pathology services provider Synnovis on Monday and impacted several major NHS hospitals in London has now been linked to the Qilin ransomware operation. [...]
