Title: Join the latest Google Cloud Security Talks on the intersection of AI and cybersecurity
Date: 2024-06-12T16:00:00+00:00
Author: Ruchika Mishra
Category: GCP Security
Tags: Security & Identity
Slug: 2024-06-12-join-the-latest-google-cloud-security-talks-on-the-intersection-of-ai-and-cybersecurity

[Source](https://cloud.google.com/blog/products/identity-security/join-the-latest-google-cloud-security-talks-on-the-intersection-of-ai-and-cybersecurity/){:target="_blank" rel="noopener"}

> As AI continues to change the way we work, security professionals are thinking about how to apply generative AI to help them in their jobs, and how to safeguard the AI systems their organizations are beginning to use. They’re also envisioning new ways to use threat intelligence, sharpen their risk assessments, and increase visibility into threats and risks across their operations. We’re bringing together experts from Google Cloud and the broader Google security community on June 26 to share insights, best practices, and actionable strategies to strengthen your security posture. The one-day virtual gathering kicks off with a keynote session [...]
