Title: How you can build a FedRAMP High-compliant network with Assured Workloads
Date: 2024-06-10T16:00:00+00:00
Author: Neha Chhabra
Category: GCP Security
Tags: Public Sector;Security & Identity
Slug: 2024-06-10-how-you-can-build-a-fedramp-high-compliant-network-with-assured-workloads

[Source](https://cloud.google.com/blog/products/identity-security/how-you-can-build-a-fedramp-high-compliant-network-with-assured-workloads/){:target="_blank" rel="noopener"}

> Google Cloud recently achieved a major U.S. government compliance milestone with more than 130 services, including 12 additional Cloud Networking services, approved for FedRAMP High authorization — the strictest standard for protecting the most sensitive unclassified data. To help our customers securely deploy a network architecture that aligns with FedRAMP High, we have outlined several of our recommended best practices. Google Cloud's Assured Workloads enables public sector customers to run regulated FedRAMP High workloads on Google's public cloud infrastructure. By enforcing U.S. data location and personnel access controls, Assured Workloads simplifies the process of maintaining security and compliance while allowing [...]
