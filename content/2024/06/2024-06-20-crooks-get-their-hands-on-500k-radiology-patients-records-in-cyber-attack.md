Title: Crooks get their hands on 500K+ radiology patients' records in cyber-attack
Date: 2024-06-20T21:43:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-20-crooks-get-their-hands-on-500k-radiology-patients-records-in-cyber-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/20/radiology_information_loss/){:target="_blank" rel="noopener"}

> Two ransomware gangs bragged of massive theft of personal info and medical files Consulting Radiologists has notified almost 512,000 patients that digital intruders accessed their personal and medical information during a February cyberattack.... [...]
