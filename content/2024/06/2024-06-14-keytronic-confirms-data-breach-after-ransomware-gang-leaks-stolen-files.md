Title: Keytronic confirms data breach after ransomware gang leaks stolen files
Date: 2024-06-14T17:20:52-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-14-keytronic-confirms-data-breach-after-ransomware-gang-leaks-stolen-files

[Source](https://www.bleepingcomputer.com/news/security/keytronic-confirms-data-breach-after-ransomware-gang-leaks-stolen-files/){:target="_blank" rel="noopener"}

> PCBA manufacturing giant Keytronic is warning it suffered a data breach after the Black Basta ransomware gang leaked 530GB of the company's stolen data two weeks ago. [...]
