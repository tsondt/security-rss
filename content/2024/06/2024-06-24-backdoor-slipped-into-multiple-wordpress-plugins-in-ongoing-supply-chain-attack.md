Title: Backdoor slipped into multiple WordPress plugins in ongoing supply-chain attack
Date: 2024-06-24T21:00:43+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;backdoors;plugins;supply chain attacks;wordpress
Slug: 2024-06-24-backdoor-slipped-into-multiple-wordpress-plugins-in-ongoing-supply-chain-attack

[Source](https://arstechnica.com/?p=2033226){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) WordPress plugins running on as many as 36,000 websites have been backdoored in a supply-chain attack with unknown origins, security researchers said on Monday. So far, five plugins are known to be affected in the campaign, which was active as recently as Monday morning, researchers from security firm Wordfence reported. Over the past week, unknown threat actors have added malicious functions to updates available for the plugins on WordPress.org, the official site for the open source WordPress CMS software. When installed, the updates automatically create an attacker-controlled administrative account that provides full control over the [...]
