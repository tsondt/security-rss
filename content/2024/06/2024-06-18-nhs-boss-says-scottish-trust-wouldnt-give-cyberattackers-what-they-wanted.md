Title: NHS boss says Scottish trust wouldn't give cyberattackers what they wanted
Date: 2024-06-18T11:29:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-18-nhs-boss-says-scottish-trust-wouldnt-give-cyberattackers-what-they-wanted

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/18/nhs_dumfries_and_galloway_letter/){:target="_blank" rel="noopener"}

> CEO of Dumfries and Galloway admits circa 150K people should assume their details leaked The chief exec at NHS Dumfries and Galloway will write to thousands of folks in the Scottish region whose data was stolen by criminals, admitting the lot of it was published after the trust did not give in to the miscreants' demands.... [...]
