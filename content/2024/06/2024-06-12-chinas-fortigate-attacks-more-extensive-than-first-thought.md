Title: China's FortiGate attacks more extensive than first thought
Date: 2024-06-12T14:00:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-12-chinas-fortigate-attacks-more-extensive-than-first-thought

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/12/chinas_targeting_of_fortigate_systems/){:target="_blank" rel="noopener"}

> Dutch intelligence says at least 20,000 firewalls pwned in just a few months The Netherlands' cybersecurity agency (NCSC) says the previously reported attack on the country's Ministry of Defense (MoD) was far more extensive than previously thought.... [...]
