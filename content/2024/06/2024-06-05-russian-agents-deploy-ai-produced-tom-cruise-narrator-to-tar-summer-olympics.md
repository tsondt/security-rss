Title: Russian agents deploy AI-produced Tom Cruise narrator to tar Summer Olympics
Date: 2024-06-05T21:41:41+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;disinformation;influence operations;Olympics;russia
Slug: 2024-06-05-russian-agents-deploy-ai-produced-tom-cruise-narrator-to-tar-summer-olympics

[Source](https://arstechnica.com/?p=2029315){:target="_blank" rel="noopener"}

> Enlarge / A visual from the fake documentary "Olympics Has Fallen" produced by Russia-affiliated influence actor Storm-1679. (credit: Microsoft) Last year, a feature-length documentary purportedly produced by Netflix began circulating on Telegram. Titled “Olympics have Fallen” and narrated by a voice with a striking similarity to that of actor Tom Cruise, it sharply criticized the leadership of the International Olympic Committee. The slickly produced film, claiming five-star reviews from the New York Times, Washington Post, and BBC, was quickly amplified on social media. Among those seemingly endorsing the documentary were celebrities on the platform Cameo. A recently published report by [...]
