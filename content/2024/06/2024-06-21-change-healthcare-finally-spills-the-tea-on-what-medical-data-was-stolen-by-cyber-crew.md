Title: Change Healthcare finally spills the tea on what medical data was stolen by cyber-crew
Date: 2024-06-21T21:33:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-21-change-healthcare-finally-spills-the-tea-on-what-medical-data-was-stolen-by-cyber-crew

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/21/change_healthcare_patients/){:target="_blank" rel="noopener"}

> 'Substantial proportion' of America to get a note from next month Change Healthcare is formally notifying some of its pharmacy and hospital customers that their patients' data was stolen from it by ransomware criminals back in February – and for the first time has concretely disclosed the types of information swiped during that IT intrusion.... [...]
