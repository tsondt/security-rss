Title: Advance Auto Parts confirms data breach exposed employee information
Date: 2024-06-19T15:45:57-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-19-advance-auto-parts-confirms-data-breach-exposed-employee-information

[Source](https://www.bleepingcomputer.com/news/security/advance-auto-parts-confirms-data-breach-exposed-employee-information/){:target="_blank" rel="noopener"}

> Advance Auto Parts has confirmed it suffered a data breach after a threat actor attempted to sell stolen data on a hacking forum earlier this month. [...]
