Title: Nigerian faces up to 102 years in the slammer for $1.5M phishing scam
Date: 2024-06-14T20:15:15+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-06-14-nigerian-faces-up-to-102-years-in-the-slammer-for-15m-phishing-scam

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/14/phishing_scam_conviction/){:target="_blank" rel="noopener"}

> Crook and his alleged co-conspirators said to have used Discord to coordinate The US Department of Justice has convicted a Nigerian national of participating in a business email compromise (BEC) scam worth $1.5 million.... [...]
