Title: Russia takes gold for disinformation as Olympics approach
Date: 2024-06-03T14:45:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-03-russia-takes-gold-for-disinformation-as-olympics-approach

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/03/russias_cyberattacks_against_2024_olympics/){:target="_blank" rel="noopener"}

> Featuring Tom Cruise deepfakes and multiple made-up terrorism threats Still throwing toys out the pram over its relationship with international sport, Russia is engaged in a multi-pronged disinformation campaign against the Olympic Games and host nation France that's intensifying as the opening ceremony approaches.... [...]
