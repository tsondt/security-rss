Title: Infosys McCamish says LockBit stole data of 6 million people
Date: 2024-06-28T14:08:59-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-28-infosys-mccamish-says-lockbit-stole-data-of-6-million-people

[Source](https://www.bleepingcomputer.com/news/security/infosys-mccamish-says-lockbit-stole-data-of-6-million-people/){:target="_blank" rel="noopener"}

> Infosys McCamish Systems (IMS) disclosed that the LockBit ransomware attack it suffered earlier this year impacted sensitive information of more than six million individuals. [...]
