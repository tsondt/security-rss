Title: Uncle Sam ends financial support to orgs hurt by Change Healthcare attack
Date: 2024-06-18T13:15:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-18-uncle-sam-ends-financial-support-to-orgs-hurt-by-change-healthcare-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/18/support_ends_change_healthcare/){:target="_blank" rel="noopener"}

> Billions of dollars made available but worst appears to be over The US government is winding down its financial support for healthcare providers originally introduced following the ransomware attack at Change Healthcare in February.... [...]
