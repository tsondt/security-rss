Title: New York Times warns freelancers of GitHub repo data breach
Date: 2024-06-13T15:52:26-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-06-13-new-york-times-warns-freelancers-of-github-repo-data-breach

[Source](https://www.bleepingcomputer.com/news/security/new-york-times-warns-freelancers-of-github-repo-data-breach/){:target="_blank" rel="noopener"}

> The New York Times notified an undisclosed number of contributors that some of their sensitive personal information was stolen and leaked after its GitHub repositories were breached in January 2024. [...]
