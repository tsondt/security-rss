Title: Australian mining company discloses breach after BianLian leaks data
Date: 2024-06-04T18:10:59-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-04-australian-mining-company-discloses-breach-after-bianlian-leaks-data

[Source](https://www.bleepingcomputer.com/news/security/australian-mining-company-discloses-breach-after-bianlian-leaks-data/){:target="_blank" rel="noopener"}

> Northern Minerals issued an announcement earlier today warning that it suffered a cybersecurity breach resulting in some of its stolen data being published on the dark web. [...]
