Title: CISA looked at C/C++ projects and found a lot of C/C++ code. Wanna redo any of it in Rust?
Date: 2024-06-28T20:55:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-06-28-cisa-looked-at-cc-projects-and-found-a-lot-of-cc-code-wanna-redo-any-of-it-in-rust

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/28/cisa_open_source/){:target="_blank" rel="noopener"}

> So, so many lines of memory-unsafe routines in crucial open source, and unsafe dependencies The US government's Cybersecurity and Infrastructure Security Agency (CISA) has analyzed 172 critical open source projects and found that more than half contain code written in languages like C and C++ that are not naturally memory safe.... [...]
