Title: Google Chrome change that weakens ad blockers begins June 3rd
Date: 2024-06-01T10:14:07-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Google;Security;Software
Slug: 2024-06-01-google-chrome-change-that-weakens-ad-blockers-begins-june-3rd

[Source](https://www.bleepingcomputer.com/news/google/google-chrome-change-that-weakens-ad-blockers-begins-june-3rd/){:target="_blank" rel="noopener"}

> Google is continuing with its plan to phase out Manifest V2 extensions in Chrome starting in early June 2024, weakening the abilities of ad blockers. [...]
