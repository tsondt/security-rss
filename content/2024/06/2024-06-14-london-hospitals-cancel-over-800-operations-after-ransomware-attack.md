Title: London hospitals cancel over 800 operations after ransomware attack
Date: 2024-06-14T14:05:51-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-06-14-london-hospitals-cancel-over-800-operations-after-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/london-hospitals-cancel-over-800-operations-after-ransomware-attack/){:target="_blank" rel="noopener"}

> NHS England revealed today that multiple London hospitals impacted by last week's Synnovis ransomware attack were forced to cancel hundreds of planned operations and appointments. [...]
