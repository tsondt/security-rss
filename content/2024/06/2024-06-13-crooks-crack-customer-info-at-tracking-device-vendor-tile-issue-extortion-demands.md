Title: Crooks crack customer info at tracking device vendor Tile, issue 'extortion' demands
Date: 2024-06-13T01:15:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-13-crooks-crack-customer-info-at-tracking-device-vendor-tile-issue-extortion-demands

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/13/tile_life360_extortion/){:target="_blank" rel="noopener"}

> Who tracks the trackers? Life360, purveyor of "Tile" Bluetooth tracking devices and developer of associated apps, has revealed it is dealing with a "criminal extortion attempt" after unknown miscreants contacted it with an allegation they had customer data in their possession.... [...]
