Title: Microsoft makes Windows Recall opt-in, secures data with Windows Hello
Date: 2024-06-07T12:37:51-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-06-07-microsoft-makes-windows-recall-opt-in-secures-data-with-windows-hello

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-makes-windows-recall-opt-in-secures-data-with-windows-hello/){:target="_blank" rel="noopener"}

> Following massive customer pushback after it announced the new AI-powered Recall for Copilot+ PCs last month, Microsoft says it will update the feature to be more secure and require customers to opt in to enable it. [...]
