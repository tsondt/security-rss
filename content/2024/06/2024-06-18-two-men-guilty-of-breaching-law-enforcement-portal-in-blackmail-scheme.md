Title: Two men guilty of breaching law enforcement portal in blackmail scheme
Date: 2024-06-18T10:48:33-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-06-18-two-men-guilty-of-breaching-law-enforcement-portal-in-blackmail-scheme

[Source](https://www.bleepingcomputer.com/news/security/two-men-guilty-of-breaching-law-enforcement-portal-in-blackmail-scheme/){:target="_blank" rel="noopener"}

> Two men have pleaded guilty to hacking into a federal law enforcement database to steal personal information of those they were extorting. [...]
