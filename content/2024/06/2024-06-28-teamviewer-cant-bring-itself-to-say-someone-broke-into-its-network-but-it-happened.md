Title: TeamViewer can't bring itself to say someone broke into its network – but it happened
Date: 2024-06-28T00:37:41+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-28-teamviewer-cant-bring-itself-to-say-someone-broke-into-its-network-but-it-happened

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/28/teamviewer_network_breach/){:target="_blank" rel="noopener"}

> Claims customer data, prod environment not affected as NCC sounds alarm Updated TeamViewer on Thursday said its security team just "detected an irregularity" within one of its networks – which is a fancy way of saying someone broke in.... [...]
