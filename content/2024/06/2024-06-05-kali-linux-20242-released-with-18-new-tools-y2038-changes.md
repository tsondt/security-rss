Title: Kali Linux 2024.2 released with 18 new tools, Y2038 changes
Date: 2024-06-05T13:22:04-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Linux;Security
Slug: 2024-06-05-kali-linux-20242-released-with-18-new-tools-y2038-changes

[Source](https://www.bleepingcomputer.com/news/linux/kali-linux-20242-released-with-18-new-tools-y2038-changes/){:target="_blank" rel="noopener"}

> Kali Linux has released version 2024.2, the first version of 2024, with eighteen new tools and fixes for the Y2038 bug. [...]
