Title: 4 cuffed following probe into holiday scheme for cybercrooks
Date: 2024-06-05T12:06:01+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-05-4-cuffed-following-probe-into-holiday-scheme-for-cybercrooks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/05/four_arrested_in_scheme_to/){:target="_blank" rel="noopener"}

> Public officials allegedly bribed to allow extradition-dodging travel Four arrests were made this week as part of an international probe into two overlapping corruption schemes that allowed cybercrims on INTERPOL watch lists to travel freely without flagging any alerts.... [...]
