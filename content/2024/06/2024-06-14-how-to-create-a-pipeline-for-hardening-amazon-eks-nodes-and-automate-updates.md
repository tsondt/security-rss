Title: How to create a pipeline for hardening Amazon EKS nodes and automate updates
Date: 2024-06-14T16:20:50+00:00
Author: Nima Fotouhi
Category: AWS Security
Tags: Amazon Elastic Kubernetes Service;Containers;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Amazon EKS;EC2 image builder;hardening;Security;Security Blog
Slug: 2024-06-14-how-to-create-a-pipeline-for-hardening-amazon-eks-nodes-and-automate-updates

[Source](https://aws.amazon.com/blogs/security/how-to-create-a-pipeline-for-hardening-amazon-eks-nodes-and-automate-updates/){:target="_blank" rel="noopener"}

> Amazon Elastic Kubernetes Service (Amazon EKS) offers a powerful, Kubernetes-certified service to build, secure, operate, and maintain Kubernetes clusters on Amazon Web Services (AWS). It integrates seamlessly with key AWS services such as Amazon CloudWatch, Amazon EC2 Auto Scaling, and AWS Identity and Access Management (IAM), enhancing the monitoring, scaling, and load balancing of containerized [...]
