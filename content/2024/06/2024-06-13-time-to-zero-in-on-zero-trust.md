Title: Time to zero in on Zero Trust?
Date: 2024-06-13T03:12:12+00:00
Author: David Gordon
Category: The Register
Tags: 
Slug: 2024-06-13-time-to-zero-in-on-zero-trust

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/13/time_to_zero_in_on/){:target="_blank" rel="noopener"}

> Recently discovered vulnerabilities in VPN services should push ASEAN organizations to rethink their perimeter security approach Sponsored Post Companies the ASEAN region have long relied on a virtual private network (VPN) to help encrypt their Internet traffic and protect users' online identities.... [...]
