Title: If you're using Polyfill.io code on your site – like 100,000+ are – remove it immediately
Date: 2024-06-25T23:48:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-25-if-youre-using-polyfillio-code-on-your-site-like-100000-are-remove-it-immediately

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/25/polyfillio_china_crisis/){:target="_blank" rel="noopener"}

> Scripts turn sus after mysterious CDN swallows domain The polyfill.io domain is being used to infect more than 100,000 websites with malicious code after what's said to be a Chinese organization bought the domain earlier this year, researchers have said.... [...]
