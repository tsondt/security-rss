Title: Russia's cyber spies still threatening French national security, democracy
Date: 2024-06-20T12:27:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-20-russias-cyber-spies-still-threatening-french-national-security-democracy

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/20/russias_cyber_attacks_france_report/){:target="_blank" rel="noopener"}

> Publishing right before a major election is apparently just a coincidence A fresh report into the Nobelium offensive cyber crew published by France's computer emergency response team (CERT-FR) highlights the group's latest tricks as the country prepares for a major election and to host this year's Olympic and Paralympic Games.... [...]
