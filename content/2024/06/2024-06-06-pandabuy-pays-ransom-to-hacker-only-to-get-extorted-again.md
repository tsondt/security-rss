Title: PandaBuy pays ransom to hacker only to get extorted again
Date: 2024-06-06T11:18:01-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-06-pandabuy-pays-ransom-to-hacker-only-to-get-extorted-again

[Source](https://www.bleepingcomputer.com/news/security/pandabuy-pays-ransom-to-hacker-only-to-get-extorted-again/){:target="_blank" rel="noopener"}

> Chinese shopping platform Pandabuy told BleepingComputer it previously paid a a ransom demand to prevent stolen data from being leaked, only for the same threat actor to extort the company again this week. [...]
