Title: Linux version of RansomHub ransomware targets VMware ESXi VMs
Date: 2024-06-20T15:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-20-linux-version-of-ransomhub-ransomware-targets-vmware-esxi-vms

[Source](https://www.bleepingcomputer.com/news/security/linux-version-of-ransomhub-ransomware-targets-vmware-esxi-vms/){:target="_blank" rel="noopener"}

> The RansomHub ransomware operation is using a Linux encryptor designed specifically to encrypt VMware ESXi environments in corporate attacks. [...]
