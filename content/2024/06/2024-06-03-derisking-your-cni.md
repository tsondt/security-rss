Title: Derisking your CNI
Date: 2024-06-03T09:00:06+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2024-06-03-derisking-your-cni

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/03/derisking_your_cni/){:target="_blank" rel="noopener"}

> How to strengthen cyber risk management for cyber physical systems (CPS) Webinar Can organizations ever scale back on the relentless task of identifying, prioritizing, and remediating vulnerabilities, and misconfigurations across their industrial and critical infrastructure environments?... [...]
