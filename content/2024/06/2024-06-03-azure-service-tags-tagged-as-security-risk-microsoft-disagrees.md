Title: Azure Service Tags tagged as security risk, Microsoft disagrees
Date: 2024-06-03T14:55:29-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-06-03-azure-service-tags-tagged-as-security-risk-microsoft-disagrees

[Source](https://www.bleepingcomputer.com/news/microsoft/azure-service-tags-tagged-as-security-risk-microsoft-disagrees/){:target="_blank" rel="noopener"}

> ​Security researchers at Tenable discovered what they describe as a high-severity vulnerability in Azure Service Tags that could allow attackers to access customers' private data. [...]
