Title: Scattered Spider hackers switch focus to cloud apps for data theft
Date: 2024-06-14T11:04:59-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-14-scattered-spider-hackers-switch-focus-to-cloud-apps-for-data-theft

[Source](https://www.bleepingcomputer.com/news/security/scattered-spider-hackers-switch-focus-to-cloud-apps-for-data-theft/){:target="_blank" rel="noopener"}

> The Scattered Spider gang has started to steal data from software-as-a-service (SaaS) applications and establish persistence through creating new virtual machines. [...]
