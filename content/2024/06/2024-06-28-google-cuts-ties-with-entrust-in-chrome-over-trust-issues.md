Title: Google cuts ties with Entrust in Chrome over trust issues
Date: 2024-06-28T14:29:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-28-google-cuts-ties-with-entrust-in-chrome-over-trust-issues

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/28/google_axes_entrust_over_six/){:target="_blank" rel="noopener"}

> Move comes weeks after Mozilla blasted certificate authority for failings Google is severing its trust in Entrust after what it describes as a protracted period of failures around compliance and general improvements.... [...]
