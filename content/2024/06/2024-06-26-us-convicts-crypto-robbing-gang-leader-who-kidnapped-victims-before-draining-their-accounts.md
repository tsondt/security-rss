Title: US convicts crypto-robbing gang leader who kidnapped victims before draining their accounts
Date: 2024-06-26T18:46:05+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-26-us-convicts-crypto-robbing-gang-leader-who-kidnapped-victims-before-draining-their-accounts

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/26/us_convicts_vicious_cryptorobbing_gang/){:target="_blank" rel="noopener"}

> Said to have zip tied elderly crypto investors, held them at gunpoint, and threatened to kill them The US has convicted the 24-year-old leader of an international robbery crew that kidnapped and terrorized wealthy victims during home invasions that were carried out to steal cryptocurrency tokens.... [...]
