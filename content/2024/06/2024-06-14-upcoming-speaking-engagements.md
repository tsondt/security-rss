Title: Upcoming Speaking Engagements
Date: 2024-06-14T15:59:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2024-06-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2024/06/upcoming-speaking-engagements-37.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m appearing on a panel on Society and Democracy at ACM Collective Intelligence in Boston, Massachusetts. The conference runs from June 26 through 29, 2024, and my panel is at 9:00 AM on Friday, June 28. I’m speaking on “Reimagining Democracy in the Age of AI” at the Bozeman Library in Bozeman, Montana, USA, July 18, 2024. The event will also be available via Zoom. I’m speaking at the TEDxBillings Democracy Event in Billings, Montana, USA, on July 19, 2024. The list is maintained on this [...]
