Title: Fake IT support sites push malicious PowerShell scripts as Windows fixes
Date: 2024-06-30T10:21:42-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-30-fake-it-support-sites-push-malicious-powershell-scripts-as-windows-fixes

[Source](https://www.bleepingcomputer.com/news/security/fake-it-support-sites-push-malicious-powershell-scripts-as-windows-fixes/){:target="_blank" rel="noopener"}

> Fake IT support sites promote malicious PowerShell "fixes" for common Windows errors, like the 0x80070643 error, to infect devices with information-stealing malware. [...]
