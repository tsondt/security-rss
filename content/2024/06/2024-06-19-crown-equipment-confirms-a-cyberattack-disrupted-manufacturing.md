Title: Crown Equipment confirms a cyberattack disrupted manufacturing
Date: 2024-06-19T18:30:35-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-19-crown-equipment-confirms-a-cyberattack-disrupted-manufacturing

[Source](https://www.bleepingcomputer.com/news/security/crown-equipment-confirms-a-cyberattack-disrupted-manufacturing/){:target="_blank" rel="noopener"}

> Forklift manufacturer Crown Equipment confirmed today that it suffered a cyberattack earlier this month that disrupted manufacturing at its plants. [...]
