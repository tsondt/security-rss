Title: FBI recovers 7,000 LockBit keys, urges ransomware victims to reach out
Date: 2024-06-05T18:48:52-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-06-05-fbi-recovers-7000-lockbit-keys-urges-ransomware-victims-to-reach-out

[Source](https://www.bleepingcomputer.com/news/security/fbi-recovers-7-000-lockbit-keys-urges-ransomware-victims-to-reach-out/){:target="_blank" rel="noopener"}

> The FBI urges past victims of LockBit ransomware attacks to come forward after revealing that it has obtained over 7,000 LockBit decryption keys that they can use to recover encrypted data for free. [...]
