Title: Change Healthcare lists the medical data stolen in ransomware attack
Date: 2024-06-21T12:10:25-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-21-change-healthcare-lists-the-medical-data-stolen-in-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/change-healthcare-lists-the-medical-data-stolen-in-ransomware-attack/){:target="_blank" rel="noopener"}

> UnitedHealth has confirmed for the first time what types of medical and patient data were stolen in the massive Change Healthcare ransomware attack, stating that data breach notifications will be mailed in July. [...]
