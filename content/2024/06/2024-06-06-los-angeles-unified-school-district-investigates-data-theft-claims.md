Title: Los Angeles Unified School District investigates data theft claims
Date: 2024-06-06T18:44:23-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-06-06-los-angeles-unified-school-district-investigates-data-theft-claims

[Source](https://www.bleepingcomputer.com/news/security/los-angeles-unified-school-district-investigates-data-theft-claims/){:target="_blank" rel="noopener"}

> Los Angeles Unified School District (LAUSD) officials are investigating a threat actor's claims that they're selling stolen databases containing records belonging to millions of students and thousands of teachers. [...]
