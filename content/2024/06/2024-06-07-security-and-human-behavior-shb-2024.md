Title: Security and Human Behavior (SHB) 2024
Date: 2024-06-07T20:55:28+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;conferences;cybersecurity;security conferences
Slug: 2024-06-07-security-and-human-behavior-shb-2024

[Source](https://www.schneier.com/blog/archives/2024/06/security-and-human-behavior-shb-2024.html){:target="_blank" rel="noopener"}

> This week, I hosted the seventeenth Workshop on Security and Human Behavior at the Harvard Kennedy School. This is the first workshop since our co-founder, Ross Anderson, died unexpectedly. SHB is a small, annual, invitational workshop of people studying various aspects of the human side of security. The fifty or so attendees include psychologists, economists, computer security researchers, criminologists, sociologists, political scientists, designers, lawyers, philosophers, anthropologists, geographers, neuroscientists, business school professors, and a smattering of others. It’s not just an interdisciplinary event; most of the people here are individually interdisciplinary. Our goal is always to maximize discussion and interaction. We [...]
