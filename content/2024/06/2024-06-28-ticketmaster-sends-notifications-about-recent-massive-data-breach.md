Title: Ticketmaster sends notifications about recent massive data breach
Date: 2024-06-28T12:45:36-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-28-ticketmaster-sends-notifications-about-recent-massive-data-breach

[Source](https://www.bleepingcomputer.com/news/security/ticketmaster-sends-notifications-about-recent-massive-data-breach/){:target="_blank" rel="noopener"}

> Ticketmaster has started to notify customers who were impacted by a data breach after hackers stole the company's Snowflake database, containing the data of millions of people. [...]
