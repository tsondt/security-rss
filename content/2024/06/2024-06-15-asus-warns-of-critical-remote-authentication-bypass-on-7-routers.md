Title: ASUS warns of critical remote authentication bypass on 7 routers
Date: 2024-06-15T11:17:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2024-06-15-asus-warns-of-critical-remote-authentication-bypass-on-7-routers

[Source](https://www.bleepingcomputer.com/news/security/asus-warns-of-critical-remote-authentication-bypass-on-7-routers/){:target="_blank" rel="noopener"}

> ASUS has released a new firmware update that addresses a vulnerability impacting seven router models that allow remote attackers to log in to devices. [...]
