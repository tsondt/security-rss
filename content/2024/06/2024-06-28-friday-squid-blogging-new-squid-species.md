Title: Friday Squid Blogging: New Squid Species
Date: 2024-06-28T21:01:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;squid;video
Slug: 2024-06-28-friday-squid-blogging-new-squid-species

[Source](https://www.schneier.com/blog/archives/2024/06/friday-squid-blogging-new-squid-species-3.html){:target="_blank" rel="noopener"}

> A new squid species—of the Gonatidae family—was discovered. The video shows her holding a brood of very large eggs. Research paper. [...]
