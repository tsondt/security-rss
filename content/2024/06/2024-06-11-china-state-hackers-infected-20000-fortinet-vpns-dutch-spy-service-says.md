Title: China state hackers infected 20,000 Fortinet VPNs, Dutch spy service says
Date: 2024-06-11T22:56:04+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;exploits;fortigate;Fortinet;vpns;vulnerabilities
Slug: 2024-06-11-china-state-hackers-infected-20000-fortinet-vpns-dutch-spy-service-says

[Source](https://arstechnica.com/?p=2030948){:target="_blank" rel="noopener"}

> Enlarge Hackers working for the Chinese government gained access to more than 20,000 VPN appliances sold by Fortinet using a critical vulnerability that the company failed to disclose for two weeks after fixing it, Netherlands government officials said. The vulnerability, tracked as CVE-2022-42475, is a heap-based buffer overflow that allows hackers to remotely execute malicious code. It carries a severity rating of 9.8 out of 10. A maker of network security software, Fortinet silently fixed the vulnerability on November 28, 2022, but failed to mention the threat until December 12 of that year, when the company said it became aware [...]
