Title: Paul Nakasone Joins OpenAI’s Board of Directors
Date: 2024-06-24T11:04:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;NSA
Slug: 2024-06-24-paul-nakasone-joins-openais-board-of-directors

[Source](https://www.schneier.com/blog/archives/2024/06/paul-nakasone-joins-openais-board-of-directors.html){:target="_blank" rel="noopener"}

> Former NSA Director Paul Nakasone has joined the board of OpenAI. [...]
