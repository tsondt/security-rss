Title: Access AWS services programmatically using trusted identity propagation
Date: 2024-06-27T15:57:34+00:00
Author: Roberto Migli
Category: AWS Security
Tags: Advanced (300);AWS IAM Identity Center;Security, Identity, & Compliance;Technical How-to;Security;Security Blog
Slug: 2024-06-27-access-aws-services-programmatically-using-trusted-identity-propagation

[Source](https://aws.amazon.com/blogs/security/access-aws-services-programmatically-using-trusted-identity-propagation/){:target="_blank" rel="noopener"}

> With the introduction of trusted identity propagation, applications can now propagate a user’s workforce identity from their identity provider (IdP) to applications running in Amazon Web Services (AWS) and to storage services backing those applications, such as Amazon Simple Storage Service (Amazon S3) or AWS Glue. Since access to applications and data can now be [...]
