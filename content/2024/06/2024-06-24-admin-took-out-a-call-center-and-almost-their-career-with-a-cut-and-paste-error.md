Title: Admin took out a call center – and almost their career – with a cut and paste error
Date: 2024-06-24T07:29:08+00:00
Author: Matthew JC Powell
Category: The Register
Tags: 
Slug: 2024-06-24-admin-took-out-a-call-center-and-almost-their-career-with-a-cut-and-paste-error

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/24/who_me/){:target="_blank" rel="noopener"}

> Have you heard the one about the techie who forgot what was on the clipboard? Who, me? Brace yourselves, gentle readers, for it is once again Monday, and the work week has commenced. Thankfully, The Reg is here with another dose of Who, Me? in which readers share tales of times they had a day worse than the one you're having. We hope it helps.... [...]
