Title: AWS completes Police-Assured Secure Facilities (PASF) audit in the Europe (London) Region
Date: 2024-06-11T19:28:35+00:00
Author: Vishal Pabari
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Compliance;EU (London) Region;Security Blog;United Kingdom
Slug: 2024-06-11-aws-completes-police-assured-secure-facilities-pasf-audit-in-the-europe-london-region

[Source](https://aws.amazon.com/blogs/security/aws-completes-police-assured-secure-facilities-pasf-audit-in-the-europe-london-region/){:target="_blank" rel="noopener"}

> We’re excited to announce that our Europe (London) Region has renewed our accreditation for United Kingdom (UK) Police-Assured Secure Facilities (PASF) for Official-Sensitive data. Since 2017, the Amazon Web Services (AWS) Europe (London) Region has been assured under the PASF program. This demonstrates our continuous commitment to adhere to the heightened expectations of customers with [...]
