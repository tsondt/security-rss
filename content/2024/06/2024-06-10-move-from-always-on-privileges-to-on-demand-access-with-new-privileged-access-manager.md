Title: Move from always-on privileges to on-demand access with new Privileged Access Manager
Date: 2024-06-10T17:00:00+00:00
Author: Balaji Ramani
Category: GCP Security
Tags: Security & Identity
Slug: 2024-06-10-move-from-always-on-privileges-to-on-demand-access-with-new-privileged-access-manager

[Source](https://cloud.google.com/blog/products/identity-security/move-from-always-on-privileges-to-on-demand-access-with-privileged-access-manager/){:target="_blank" rel="noopener"}

> We are continually enhancing Google Cloud’s Identity and Access Management (IAM) capabilities to help our customers strengthen their security posture. To help mitigate the risks associated with excessive privileges and misuses of elevated access, we are excited to announce Google Cloud’s built-in Privileged Access Manager (PAM). Now available in preview, PAM helps you achieve the principle of least privilege by ensuring your principals or other high-privilege users have an easy way to obtain precisely the access they need, only when required, and for no longer than required. PAM helps mitigate risks by allowing you to shift always-on standing privileges to [...]
