Title: Risk of installing dodgy extensions from Chrome store way worse than Google's letting on, study suggests
Date: 2024-06-23T10:36:10+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-06-23-risk-of-installing-dodgy-extensions-from-chrome-store-way-worse-than-googles-letting-on-study-suggests

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/23/google_chrome_web_store_vetting/){:target="_blank" rel="noopener"}

> All depends on how you count it – Chocolate Factory claims 1% fail rate Google this week offered reassurance that its vetting of Chrome extensions catches most malicious code, even as it acknowledged that "as with any software, extensions can also introduce risk."... [...]
