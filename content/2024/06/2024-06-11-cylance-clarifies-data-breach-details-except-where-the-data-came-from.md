Title: Cylance clarifies data breach details, except where the data came from
Date: 2024-06-11T16:30:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-11-cylance-clarifies-data-breach-details-except-where-the-data-came-from

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/11/cylance_clarifies_data_breach_details/){:target="_blank" rel="noopener"}

> Customers, partners, operations remain uncompromised, BlackBerry says BlackBerry-owned cybersecurity shop Cylance says the data allegedly belonging to it and being sold on a crime forum doesn't endanger customers, yet it won't say where the information was stored originally.... [...]
