Title: Stanford Internet Observatory wilts under legal pressure during election year
Date: 2024-06-14T21:38:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-06-14-stanford-internet-observatory-wilts-under-legal-pressure-during-election-year

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/14/stanford_internet_observatory/){:target="_blank" rel="noopener"}

> Because who needs disinformation research at times like these The Stanford Internet Observatory (SIO), which for the past five years has been studying and reporting on social media disinformation, is being reimagined with new management and fewer staff following the recent departure of research director Renee DiResta.... [...]
