Title: Check-in terminals used by thousands of hotels leak guest info
Date: 2024-06-05T16:43:37-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-05-check-in-terminals-used-by-thousands-of-hotels-leak-guest-info

[Source](https://www.bleepingcomputer.com/news/security/check-in-terminals-used-by-thousands-of-hotels-leak-guest-info/){:target="_blank" rel="noopener"}

> Ariane Systems self check-in systems installed at thousands of hotels worldwide are vulnerable to a kiosk mode bypass flaw that could allow access to guests' personal information and the keys for other rooms. [...]
