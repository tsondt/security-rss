Title: Advance Auto Parts stolen data for sale after Snowflake attack
Date: 2024-06-05T17:56:20-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-06-05-advance-auto-parts-stolen-data-for-sale-after-snowflake-attack

[Source](https://www.bleepingcomputer.com/news/security/advance-auto-parts-stolen-data-for-sale-after-snowflake-attack/){:target="_blank" rel="noopener"}

> Threat actors claim to be selling 3TB of data from Advance Auto Parts, a leading automotive aftermarket parts provider, stolen after breaching the company's Snowflake account. [...]
