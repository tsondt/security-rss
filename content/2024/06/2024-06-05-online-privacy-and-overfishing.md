Title: Online Privacy and Overfishing
Date: 2024-06-05T11:00:58+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;Microsoft;privacy
Slug: 2024-06-05-online-privacy-and-overfishing

[Source](https://www.schneier.com/blog/archives/2024/06/online-privacy-and-overfishing.html){:target="_blank" rel="noopener"}

> Microsoft recently caught state-backed hackers using its generative AI tools to help with their attacks. In the security community, the immediate questions weren’t about how hackers were using the tools (that was utterly predictable), but about how Microsoft figured it out. The natural conclusion was that Microsoft was spying on its AI users, looking for harmful hackers at work. Some pushed back at characterizing Microsoft’s actions as “spying.” Of course cloud service providers monitor what users are doing. And because we expect Microsoft to be doing something like this, it’s not fair to call it spying. We see this argument [...]
