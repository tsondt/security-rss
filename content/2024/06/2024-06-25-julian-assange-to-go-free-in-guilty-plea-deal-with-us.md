Title: Julian Assange to go free in guilty plea deal with US
Date: 2024-06-25T00:19:35+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-06-25-julian-assange-to-go-free-in-guilty-plea-deal-with-us

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/25/julian_assange_freed_plea_deal/){:target="_blank" rel="noopener"}

> WikiLeaks boss already out of Blighty and, if all goes to plan, ultimately off to home in Australia WikiLeaks founder Julian Assange has been freed from prison in the UK after agreeing to plead guilty to just one count of conspiracy to obtain and disclose national defense information, brought against him by the United States. Uncle Sam previously filed more than a dozen counts.... [...]
