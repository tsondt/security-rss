Title: LastPass says 12-hour outage caused by bad Chrome extension update
Date: 2024-06-07T18:02:48-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-07-lastpass-says-12-hour-outage-caused-by-bad-chrome-extension-update

[Source](https://www.bleepingcomputer.com/news/security/lastpass-says-12-hour-outage-caused-by-bad-chrome-extension-update/){:target="_blank" rel="noopener"}

> LastPass says its almost 12-hour outage yesterday was caused by a bad update to its Google Chrome extension. [...]
