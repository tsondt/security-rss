Title: New Fog ransomware targets US education sector via breached VPNs
Date: 2024-06-06T14:29:10-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Education
Slug: 2024-06-06-new-fog-ransomware-targets-us-education-sector-via-breached-vpns

[Source](https://www.bleepingcomputer.com/news/security/new-fog-ransomware-targets-us-education-sector-via-breached-vpns/){:target="_blank" rel="noopener"}

> A new ransomware operation named 'Fog' launched in early May 2024, using compromised VPN credentials to breach the networks of educational organizations in the U.S. [...]
