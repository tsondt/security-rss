Title: That didn't take long: Replacement for SORBS spam blacklist arises ... sort of
Date: 2024-06-17T01:59:12+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-06-17-that-didnt-take-long-replacement-for-sorbs-spam-blacklist-arises-sort-of

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/17/infosec_news_in_brief/){:target="_blank" rel="noopener"}

> Also: Online adoption cyberstalker nabbed; Tesla trade secrets thief pleads guilty; and a critical ASUS Wi-Fi vuln Infosec in brief A popular spam blocklist service that went offline earlier this month has advised users it is down permanently – but at least one potential candidate is stepping up to try to fill the threat intelligence void.... [...]
