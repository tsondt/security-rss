Title: 'Mirai-like' botnet observed attacking EOL Zyxel NAS devices
Date: 2024-06-24T14:39:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-24-mirai-like-botnet-observed-attacking-eol-zyxel-nas-devices

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/24/mirailike_botnet_zyxel_nas/){:target="_blank" rel="noopener"}

> Seems like as good a time as any to upgrade older hardware There are early indications of active attacks targeting end-of-life Zyxel NAS boxes just a few weeks after details of three critical vulnerabilities were made public.... [...]
