Title: Ukrainian cops collar Kyiv programmer believed to be Conti, LockBit linchpin
Date: 2024-06-13T16:27:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-13-ukrainian-cops-collar-kyiv-programmer-believed-to-be-conti-lockbit-linchpin

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/13/conti_lockbit_ukraine_arrest/){:target="_blank" rel="noopener"}

> 28-year-old accused of major ransomware attacks across Europe An alleged cog in the Conti and LockBit ransomware machines is now in handcuffs after Ukrainian police raided his home this week.... [...]
