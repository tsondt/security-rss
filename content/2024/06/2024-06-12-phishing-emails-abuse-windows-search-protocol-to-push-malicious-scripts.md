Title: Phishing emails abuse Windows search protocol to push malicious scripts
Date: 2024-06-12T18:30:29-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-12-phishing-emails-abuse-windows-search-protocol-to-push-malicious-scripts

[Source](https://www.bleepingcomputer.com/news/security/phishing-emails-abuse-windows-search-protocol-to-push-malicious-scripts/){:target="_blank" rel="noopener"}

> A new phishing campaign uses HTML attachments that abuse the Windows search protocol (search-ms URI) to push batch files hosted on remote servers that deliver malware. [...]
