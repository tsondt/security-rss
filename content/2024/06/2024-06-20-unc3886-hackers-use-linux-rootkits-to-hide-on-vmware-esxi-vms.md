Title: UNC3886 hackers use Linux rootkits to hide on VMware ESXi VMs
Date: 2024-06-20T13:46:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Linux
Slug: 2024-06-20-unc3886-hackers-use-linux-rootkits-to-hide-on-vmware-esxi-vms

[Source](https://www.bleepingcomputer.com/news/security/unc3886-hackers-use-linux-rootkits-to-hide-on-vmware-esxi-vms/){:target="_blank" rel="noopener"}

> A suspected Chinese threat actor tracked as UNC3886 uses publicly available open-source rootkits named 'Reptile' and 'Medusa' to remain hidden on VMware ESXi virtual machines, allowing them to conduct credential theft, command execution, and lateral movement. [...]
