Title: Polyfill.io owner punches back at 'malicious defamation' amid domain shutdown
Date: 2024-06-28T03:45:46+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-28-polyfillio-owner-punches-back-at-malicious-defamation-amid-domain-shutdown

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/28/polyfillio_cloudflare_malware/){:target="_blank" rel="noopener"}

> No supply-chain attacks to see over here! Updated After having its website shut down, the polyfill.io owner is fighting back against claims it smuggled suspicious code onto websites all across the internet.... [...]
