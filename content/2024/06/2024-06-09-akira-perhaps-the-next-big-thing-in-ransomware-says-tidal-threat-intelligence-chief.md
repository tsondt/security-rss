Title: Akira: Perhaps the next big thing in ransomware, says Tidal threat intelligence chief
Date: 2024-06-09T12:10:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-06-09-akira-perhaps-the-next-big-thing-in-ransomware-says-tidal-threat-intelligence-chief

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/09/akira_the_next_big_thing/){:target="_blank" rel="noopener"}

> Scott Small tells us gang's 'intent and capability' should get the attention of CSOs Interview It might not be as big a name as BlackCat or LockBit, but the Akira ransomware is every bit as dangerous, says one cybersecurity researcher – and it's poised to make a big impact.... [...]
