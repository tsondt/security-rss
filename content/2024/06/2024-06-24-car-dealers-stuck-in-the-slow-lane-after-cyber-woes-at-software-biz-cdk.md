Title: Car dealers stuck in the slow lane after cyber woes at software biz CDK
Date: 2024-06-24T18:02:13+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-24-car-dealers-stuck-in-the-slow-lane-after-cyber-woes-at-software-biz-cdk

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/24/the_number_of_cdk_customers/){:target="_blank" rel="noopener"}

> More customers self-reporting to SEC as disruption carries into second week The number of US companies filing Form 8-Ks with the Securities and Exchange Commission (SEC) and referencing embattled car dealership software biz CDK is mounting.... [...]
