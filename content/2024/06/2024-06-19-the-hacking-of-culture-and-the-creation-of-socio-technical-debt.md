Title: The Hacking of Culture and the Creation of Socio-Technical Debt
Date: 2024-06-19T11:09:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;data collection;hacking;noncomputer hacks
Slug: 2024-06-19-the-hacking-of-culture-and-the-creation-of-socio-technical-debt

[Source](https://www.schneier.com/blog/archives/2024/06/the-hacking-of-culture-and-the-creation-of-socio-technical-debt.html){:target="_blank" rel="noopener"}

> Culture is increasingly mediated through algorithms. These algorithms have splintered the organization of culture, a result of states and tech companies vying for influence over mass audiences. One byproduct of this splintering is a shift from imperfect but broad cultural narratives to a proliferation of niche groups, who are defined by ideology or aesthetics instead of nationality or geography. This change reflects a material shift in the relationship between collective identity and power, and illustrates how states no longer have exclusive domain over either. Today, both power and culture are increasingly corporate. Blending Stewart Brand and Jean-Jacques Rousseau, McKenzie Wark [...]
