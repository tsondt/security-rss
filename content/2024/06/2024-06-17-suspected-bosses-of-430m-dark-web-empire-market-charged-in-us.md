Title: Suspected bosses of $430M dark-web Empire Market charged in US
Date: 2024-06-17T20:13:02+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-17-suspected-bosses-of-430m-dark-web-empire-market-charged-in-us

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/17/empire_market_arrests/){:target="_blank" rel="noopener"}

> Cybercrime super-souk's Dopenugget and Zero Angel may face life behind bars if convicted The two alleged administrators of Empire Market, a dark-web bazaar that peddled drugs, malware, digital fraud, and other illegal stuff, have been detained on charges related to owning and operating the illicit souk.... [...]
