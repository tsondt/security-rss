Title: Google Chrome to let Isolated Web App access sensitive USB devices
Date: 2024-06-30T17:17:23-04:00
Author: Mayank Parmar
Category: BleepingComputer
Tags: Google;Security;Software
Slug: 2024-06-30-google-chrome-to-let-isolated-web-app-access-sensitive-usb-devices

[Source](https://www.bleepingcomputer.com/news/google/google-chrome-to-let-isolated-web-app-access-sensitive-usb-devices/){:target="_blank" rel="noopener"}

> Google is working on a new Unrestricted WebUSB feature, which allows trusted isolated web apps to bypass security restrictions in the WebUSB API. [...]
