Title: Since joining NATO, Sweden claims Russia has been borking Nordic satellites
Date: 2024-06-21T06:57:09+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-06-21-since-joining-nato-sweden-claims-russia-has-been-borking-nordic-satellites

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/21/sweden_russia_jamming/){:target="_blank" rel="noopener"}

> If Putin likes jammin', we hope NATO likes jammin' too Sweden says its satellites have been impacted by "harmful interference" from Russia ever since the Nordic nation joined the North Atlantic Treaty Organization (NATO) last March.... [...]
