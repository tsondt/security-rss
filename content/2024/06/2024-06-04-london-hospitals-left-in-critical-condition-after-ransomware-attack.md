Title: London hospitals left in critical condition after ransomware attack
Date: 2024-06-04T15:43:24+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-04-london-hospitals-left-in-critical-condition-after-ransomware-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/04/suspected_cyberattack_hits_major_london/){:target="_blank" rel="noopener"}

> Pathology lab provider targeted, affecting blood transfusions and surgeries Hospitals in London are struggling to deliver pathology services after a ransomware attack at a service partner downed some key systems.... [...]
