Title: Student's flimsy bin bags blamed for latest NHS data breach
Date: 2024-06-13T11:30:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-13-students-flimsy-bin-bags-blamed-for-latest-nhs-data-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/13/nhs_bin_bag_data_breach/){:target="_blank" rel="noopener"}

> Confidential patient information found by member of the public A data protection gaffe affecting the UK's NHS is being pinned on a medical student who placed too much trust in their bin bags.... [...]
