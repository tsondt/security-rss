Title: Qilin: We knew our Synnovis attack would cause a healthcare crisis at London hospitals
Date: 2024-06-20T10:29:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-20-qilin-we-knew-our-synnovis-attack-would-cause-a-healthcare-crisis-at-london-hospitals

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/20/qilin_our_plan_was_to/){:target="_blank" rel="noopener"}

> Cybercriminals claim they used a zero-day to breach pathology provider’s systems Interview The ransomware gang responsible for a healthcare crisis at London hospitals says it has no regrets about its cyberattack, which was entirely deliberate, it told The Register in an interview.... [...]
