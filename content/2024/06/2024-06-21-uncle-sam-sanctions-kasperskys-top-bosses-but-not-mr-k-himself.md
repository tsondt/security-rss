Title: Uncle Sam sanctions Kaspersky's top bosses – but not Mr K himself
Date: 2024-06-21T20:23:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-21-uncle-sam-sanctions-kasperskys-top-bosses-but-not-mr-k-himself

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/21/kaspersky_sanctions_ceo/){:target="_blank" rel="noopener"}

> Here's America's list of the supposedly dirty dozen Uncle Sam took another swing at Kaspersky Lab today and sanctioned a dozen C-suite and senior-level executives at the antivirus maker, but spared CEO and co-founder Eugene Kaspersky.... [...]
