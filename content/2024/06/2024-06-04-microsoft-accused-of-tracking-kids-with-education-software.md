Title: Microsoft accused of tracking kids with education software
Date: 2024-06-04T14:00:06+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2024-06-04-microsoft-accused-of-tracking-kids-with-education-software

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/04/noyb_microsoft_complaint/){:target="_blank" rel="noopener"}

> Privacy group seeks clarification of whether EU data protection law has been breached A privacy campaign group with a strong record in legal upheavals has asked the Austrian data protection authority to investigate Microsoft 365 Education to clarify if it breaches transparency provisions under GDPR.... [...]
