Title: Former IT employee gets 2.5 years for wiping 180 virtual servers
Date: 2024-06-14T11:51:50-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-06-14-former-it-employee-gets-25-years-for-wiping-180-virtual-servers

[Source](https://www.bleepingcomputer.com/news/security/former-it-employee-gets-25-years-for-wiping-180-virtual-servers/){:target="_blank" rel="noopener"}

> A former quality assurance employee of National Computer Systems (NCS) was sentenced to two years and eight months in prison for reportedly deleting 180 virtual servers after being fired. [...]
