Title: Yahoo<i>!</i> Japan to waive $189 million ad revenue after detecting fraudulent clicks
Date: 2024-06-26T05:16:14+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-06-26-yahoo-japan-to-waive-189-million-ad-revenue-after-detecting-fraudulent-clicks

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/26/yahoo_japan_ad_fraud/){:target="_blank" rel="noopener"}

> Admits it's not sure some clicks came from humans, points to bettter quality as sign not all is rotten Yahoo ! Japan will waive $189 million charged to advertisers after deciding they were fraudulently charged, the portal's corporate parent revealed on Tuesday.... [...]
