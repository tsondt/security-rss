Title: CHERI Alliance formed to promote memory security tech ... but where's Arm?
Date: 2024-06-18T15:04:12+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2024-06-18-cheri-alliance-formed-to-promote-memory-security-tech-but-wheres-arm

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/18/cheri_alliance_formed_to_promote/){:target="_blank" rel="noopener"}

> Academic-industry project takes next step as key promoter chip designer licks its wounds A group of technology organizations has formed the CHERI Alliance CIC (Community Interest Company) to promote industry adoption of the security technology focused on memory access.... [...]
