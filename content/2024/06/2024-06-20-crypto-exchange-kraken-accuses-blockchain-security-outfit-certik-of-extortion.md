Title: Crypto exchange Kraken accuses blockchain security outfit CertiK of extortion
Date: 2024-06-20T17:35:13+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-06-20-crypto-exchange-kraken-accuses-blockchain-security-outfit-certik-of-extortion

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/20/kraken_certik_crypto_dispute/){:target="_blank" rel="noopener"}

> Researchers allegedly stole $3M using the vulnerability, then asked how much it was really worth Kraken, one of the largest cryptocurrency exchanges in the world, has accused a trio of security researchers of discovering a critical bug, expoliting it to steal millions in digital cash, then using stolen funds to extort the exchange for more.... [...]
