Title: Microsoft India’s X account hijacked in Roaring Kitty crypto scam
Date: 2024-06-03T18:30:49-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-06-03-microsoft-indias-x-account-hijacked-in-roaring-kitty-crypto-scam

[Source](https://www.bleepingcomputer.com/news/security/microsoft-indias-x-account-hijacked-in-roaring-kitty-crypto-scam-to-push-wallet-drainers/){:target="_blank" rel="noopener"}

> The official Microsoft India account on Twitter, with over 211,000 followers, was hijacked by cryptocurrency scammers to impersonate Roaring Kitty, the handle used by notorious meme stock trader Keith Gill. [...]
