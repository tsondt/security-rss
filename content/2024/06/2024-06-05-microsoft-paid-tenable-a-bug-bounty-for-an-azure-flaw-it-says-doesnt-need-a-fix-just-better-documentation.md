Title: Microsoft paid Tenable a bug bounty for an Azure flaw it says doesn't need a fix, just better documentation
Date: 2024-06-05T06:44:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-05-microsoft-paid-tenable-a-bug-bounty-for-an-azure-flaw-it-says-doesnt-need-a-fix-just-better-documentation

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/05/tenable_azure_flaw/){:target="_blank" rel="noopener"}

> Let customers interfere with other tenants? That's our cloud working by design, Redmond seems to say A vulnerability — or just Azure working as intended, depending on who you ask — in Microsoft's cloud potentially allows miscreants to wave away firewall rules and access other people's private web resources.... [...]
