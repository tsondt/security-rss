Title: CDK Global cyberattack impacts thousands of US car dealerships
Date: 2024-06-19T13:58:53-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-19-cdk-global-cyberattack-impacts-thousands-of-us-car-dealerships

[Source](https://www.bleepingcomputer.com/news/security/cdk-global-cyberattack-impacts-thousands-of-us-car-dealerships/){:target="_blank" rel="noopener"}

> Car dealership software-as-a-service provider CDK Global was hit by a massive cyberattack, causing the company to shut down its systems and leaving clients unable to operate their business normally. [...]
