Title: James Bamford on Section 702 Extension
Date: 2024-06-28T11:04:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;FISA;national security policy;NSA;privacy;surveillance
Slug: 2024-06-28-james-bamford-on-section-702-extension

[Source](https://www.schneier.com/blog/archives/2024/06/james-bamford-on-section-702-extension.html){:target="_blank" rel="noopener"}

> Longtime NSA-watcher James Bamford has a long article on the reauthorization of Section 702 of the Foreign Intelligence Surveillance Act (FISA). [...]
