Title: Why attack surfaces are expanding
Date: 2024-06-21T14:58:10+00:00
Author: Annaliese Ingrams
Category: The Register
Tags: 
Slug: 2024-06-21-why-attack-surfaces-are-expanding

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/21/why_attack_surfaces_are_expanding/){:target="_blank" rel="noopener"}

> Insights from Cloudflare Webinar In the ever-evolving world of cybersecurity, understanding why attack surfaces are expanding is more critical than ever.... [...]
