Title: FBI encourages LockBit victims to step right up for free decryption keys
Date: 2024-06-06T19:45:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-06-06-fbi-encourages-lockbit-victims-to-step-right-up-for-free-decryption-keys

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/06/lockbit_fbi_decryption_keys/){:target="_blank" rel="noopener"}

> The bad news? Gang wasn't deleting victim data after payments LockBit victims who are still trying to clean up their encrypted files are in luck: the FBI has a big set of decryption keys it would love to let you try.... [...]
