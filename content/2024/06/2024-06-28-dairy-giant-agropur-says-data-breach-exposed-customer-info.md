Title: Dairy giant Agropur says data breach exposed customer info
Date: 2024-06-28T12:53:47-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-28-dairy-giant-agropur-says-data-breach-exposed-customer-info

[Source](https://www.bleepingcomputer.com/news/security/dairy-giant-agropur-says-data-breach-exposed-customer-info/){:target="_blank" rel="noopener"}

> Agropur, one of the largest dairy cooperatives in North America, is notifying customers of a data breach after some of its shared online directories were exposed. [...]
