Title: AWS adds passkeys support, warns root users must enable MFA
Date: 2024-06-12T15:38:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Technology
Slug: 2024-06-12-aws-adds-passkeys-support-warns-root-users-must-enable-mfa

[Source](https://www.bleepingcomputer.com/news/security/aws-adds-passkeys-support-warns-root-users-must-enable-mfa/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) has introduced FIDO2 passkeys as a new method for multi-factor authentication (MFA) to enhance account security and usability. [...]
