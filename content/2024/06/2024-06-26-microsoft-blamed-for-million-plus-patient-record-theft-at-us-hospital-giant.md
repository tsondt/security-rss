Title: Microsoft blamed for million-plus patient record theft at US hospital giant
Date: 2024-06-26T00:44:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-06-26-microsoft-blamed-for-million-plus-patient-record-theft-at-us-hospital-giant

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/26/geisinger_nuance_microsoft_worker/){:target="_blank" rel="noopener"}

> Probe: Worker at speech-recog outfit Nuance wasn't locked out after firing Updated American healthcare provider Geisinger fears highly personal data on more than a million of its patients has been stolen – and claimed a former employee at a Microsoft subsidiary is the likely culprit.... [...]
