Title: Panera warns of employee data breach after March ransomware attack
Date: 2024-06-13T14:32:14-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-06-13-panera-warns-of-employee-data-breach-after-march-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/panera-warns-of-employee-data-breach-after-march-ransomware-attack/){:target="_blank" rel="noopener"}

> U.S. food chain giant Panera Bread is notifying employees of a data breach after unknown threat actors stole their sensitive personal information in a March ransomware attack. [...]
