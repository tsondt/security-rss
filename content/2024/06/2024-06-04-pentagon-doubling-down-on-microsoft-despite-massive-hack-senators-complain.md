Title: Pentagon 'doubling down' on Microsoft despite 'massive hack,' senators complain
Date: 2024-06-04T18:42:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-04-pentagon-doubling-down-on-microsoft-despite-massive-hack-senators-complain

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/04/pentagon_doubling_down_on_microsoft/){:target="_blank" rel="noopener"}

> Meanwhile Mr Smith goes to Washington to testify before Congress The Pentagon is "doubling down" on its investment in Microsoft products despite the serious failings at the IT giant that put America's national security at risk, say two US senators.... [...]
