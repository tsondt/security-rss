Title: Christie's starts notifying clients of RansomHub data breach
Date: 2024-06-07T15:04:33-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-06-07-christies-starts-notifying-clients-of-ransomhub-data-breach

[Source](https://www.bleepingcomputer.com/news/security/christies-starts-notifying-clients-of-ransomhub-data-breach/){:target="_blank" rel="noopener"}

> British auction house Christie's is notifying individuals whose data was stolen by the RansomHub ransomware gang in a recent network breach. [...]
