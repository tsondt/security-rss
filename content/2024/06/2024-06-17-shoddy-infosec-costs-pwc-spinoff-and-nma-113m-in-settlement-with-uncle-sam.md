Title: Shoddy infosec costs PwC spinoff and NMA $11.3M in settlement with Uncle Sam
Date: 2024-06-17T23:47:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-17-shoddy-infosec-costs-pwc-spinoff-and-nma-113m-in-settlement-with-uncle-sam

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/17/guidehouse_nma_fined/){:target="_blank" rel="noopener"}

> Pen-testing tools didn't work – and personal info of folks hit by pandemic started appearing in search engines Two consulting firms, Guidehouse and Nan McKay and Associates, have agreed to pay a total of $11.3 million to resolve allegations of cybersecurity failings over their roll-out of COVID-19 assistance.... [...]
