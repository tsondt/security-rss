Title: Rogue uni IT director pleads guilty after fraudulently buying $2.1M of tech
Date: 2024-06-18T23:46:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-18-rogue-uni-it-director-pleads-guilty-after-fraudulently-buying-21m-of-tech

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/18/rogue_it_director_pleads_guilty/){:target="_blank" rel="noopener"}

> Two decades in the clink would be quite an education A now-former IT director has pleaded guilty to defrauding the university at which he was employed – and a computer equipment supplier – for $2.1 million over five years.... [...]
