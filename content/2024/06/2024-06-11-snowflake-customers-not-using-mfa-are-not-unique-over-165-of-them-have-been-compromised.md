Title: Snowflake customers not using MFA are not unique – over 165 of them have been compromised
Date: 2024-06-11T03:27:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-11-snowflake-customers-not-using-mfa-are-not-unique-over-165-of-them-have-been-compromised

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/11/crims_targeting_snowflake_customers/){:target="_blank" rel="noopener"}

> Mandiant warns criminal gang UNC5537, which may be friendly with Scattered Spider, is on the rampage An unknown financially motivated crime crew has swiped a "significant volume of records" from Snowflake customers' databases using stolen credentials, according to Mandiant.... [...]
