Title: Microsoft answered Congress' questions on security. Now the White House needs to act
Date: 2024-06-15T01:20:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-15-microsoft-answered-congress-questions-on-security-now-the-white-house-needs-to-act

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/15/microsoft_brad_smith_congress/){:target="_blank" rel="noopener"}

> Business as usual needs a real change Feature Microsoft president Brad Smith struck a conciliatory tone regarding his IT giant's repeated computer security failings during a congressional hearing on Thursday – while also claiming the Windows maker is above the rule of law, at least in China.... [...]
