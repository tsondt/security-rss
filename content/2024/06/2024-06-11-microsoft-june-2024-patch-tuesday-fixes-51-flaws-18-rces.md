Title: Microsoft June 2024 Patch Tuesday fixes 51 flaws, 18 RCEs
Date: 2024-06-11T13:31:20-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-06-11-microsoft-june-2024-patch-tuesday-fixes-51-flaws-18-rces

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-june-2024-patch-tuesday-fixes-51-flaws-18-rces/){:target="_blank" rel="noopener"}

> Today is Microsoft's June 2024 Patch Tuesday, which includes security updates for 51 flaws, eighteen remote code execution flaws, and one publicly disclosed zero-day vulnerability. [...]
