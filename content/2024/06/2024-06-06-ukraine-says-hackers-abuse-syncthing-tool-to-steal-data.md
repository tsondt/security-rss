Title: Ukraine says hackers abuse SyncThing tool to steal data
Date: 2024-06-06T16:43:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-06-ukraine-says-hackers-abuse-syncthing-tool-to-steal-data

[Source](https://www.bleepingcomputer.com/news/security/ukraine-says-hackers-abuse-syncthing-tool-to-steal-data/){:target="_blank" rel="noopener"}

> The Computer Emergency Response Team of Ukraine (CERT-UA) reports about a new campaign dubbed "SickSync," launched by the UAC-0020 (Vermin) hacking group in attacks on the Ukrainian defense forces. [...]
