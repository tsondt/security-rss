Title: Cylance confirms data breach linked to 'third-party' platform
Date: 2024-06-10T13:42:52-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-06-10-cylance-confirms-data-breach-linked-to-third-party-platform

[Source](https://www.bleepingcomputer.com/news/security/cylance-confirms-data-breach-linked-to-third-party-platform/){:target="_blank" rel="noopener"}

> Cybersecurity company Cylance confirmed the legitimacy of data being sold on a hacking forum, stating that it is old data stolen from a "third-party platform." [...]
