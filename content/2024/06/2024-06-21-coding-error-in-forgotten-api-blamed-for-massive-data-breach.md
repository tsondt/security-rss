Title: Coding error in forgotten API blamed for massive data breach
Date: 2024-06-21T05:38:24+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-06-21-coding-error-in-forgotten-api-blamed-for-massive-data-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/21/optus_data_breach_faulty_api/){:target="_blank" rel="noopener"}

> Australian telco Optus allegedly left redundant website with poor access controls online for years The data breach at Australian telco Optus, which saw over nine million customers' personal information exposed, has been blamed on a coding error that broke API access controls, and was left in place for years.... [...]
