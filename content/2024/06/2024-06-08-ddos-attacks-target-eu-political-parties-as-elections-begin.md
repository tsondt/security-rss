Title: DDoS attacks target EU political parties as elections begin
Date: 2024-06-08T10:12:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Artificial Intelligence
Slug: 2024-06-08-ddos-attacks-target-eu-political-parties-as-elections-begin

[Source](https://www.bleepingcomputer.com/news/security/ddos-attacks-target-eu-political-parties-as-elections-begin/){:target="_blank" rel="noopener"}

> Hacktivists are conducting DDoS attacks on European political parties that represent and promote strategies opposing their interests, according to a report by Cloudflare. [...]
