Title: Truist Bank confirms breach after stolen data shows up on hacking forum
Date: 2024-06-13T19:17:27-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-06-13-truist-bank-confirms-breach-after-stolen-data-shows-up-on-hacking-forum

[Source](https://www.bleepingcomputer.com/news/security/truist-bank-confirms-data-breach-after-stolen-data-shows-up-on-hacking-forum/){:target="_blank" rel="noopener"}

> Leading U.S. commercial bank Truist confirmed its systems were breached in an October 2023 cyberattack after a threat actor posted some of the company's data for sale on a hacking forum. [...]
