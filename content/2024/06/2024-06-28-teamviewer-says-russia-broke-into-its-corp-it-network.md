Title: TeamViewer says Russia broke into its corp IT network
Date: 2024-06-28T19:00:44+00:00
Author: Chris Williams
Category: The Register
Tags: 
Slug: 2024-06-28-teamviewer-says-russia-broke-into-its-corp-it-network

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/28/teamviewer_russia/){:target="_blank" rel="noopener"}

> Same APT29 crew that hit Microsoft and SolarWinds. How close were we to a mega backdoor situation? TeamViewer says it was Russian intelligence that broke into its systems this week.... [...]
