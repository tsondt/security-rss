Title: UK and US cops band together to tackle Qilin's ransomware shakedowns
Date: 2024-06-25T12:01:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-25-uk-and-us-cops-band-together-to-tackle-qilins-ransomware-shakedowns

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/25/nca_fbi_qilin_ransomware/){:target="_blank" rel="noopener"}

> Attacking the NHS is a very bad move UK and US cops have reportedly joined forces to find and fight Qilin, the ransomware gang wreaking havoc on the global healthcare industry.... [...]
