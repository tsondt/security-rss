Title: Feds put $5M bounty on 'CryptoQueen' Ruja Ignatova
Date: 2024-06-26T21:58:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-26-feds-put-5m-bounty-on-cryptoqueen-ruja-ignatova

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/26/fbi_ruja_ignatova/){:target="_blank" rel="noopener"}

> OneCoin co-founder allegedly bilked investors out of $4B Uncle Sam has put a $5 million bounty on any information leading to the arrest or conviction of self-titled "CryptoQueen" Ruja Ignatova, who is wanted in the US for apparently bilking victims out of more than $4 billion in what the Feds describe as the "one of the largest global fraud schemes in history."... [...]
