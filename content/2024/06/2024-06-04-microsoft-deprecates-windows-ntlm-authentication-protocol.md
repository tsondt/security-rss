Title: Microsoft deprecates Windows NTLM authentication protocol
Date: 2024-06-04T11:38:03-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-06-04-microsoft-deprecates-windows-ntlm-authentication-protocol

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-deprecates-windows-ntlm-authentication-protocol/){:target="_blank" rel="noopener"}

> Microsoft has officially deprecated NTLM authentication on Windows and Windows servers, stating that developers should transition to Kerberos or Negotiation authentication to prevent problems in the future. [...]
