Title: 'Skeleton Key' attack unlocks the worst of AI, says Microsoft
Date: 2024-06-28T06:38:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-06-28-skeleton-key-attack-unlocks-the-worst-of-ai-says-microsoft

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/28/microsoft_skeleton_key_ai_attack/){:target="_blank" rel="noopener"}

> Simple jailbreak prompt can bypass safety guardrails on major models Microsoft on Thursday published details about Skeleton Key – a technique that bypasses the guardrails used by makers of AI models to prevent their generative chatbots from creating harmful content.... [...]
