Title: Command senior chief busted for secretly setting up Wi-Fi on US Navy combat ship
Date: 2024-06-04T20:04:06+00:00
Author: Matthew Connatser
Category: The Register
Tags: 
Slug: 2024-06-04-command-senior-chief-busted-for-secretly-setting-up-wi-fi-on-us-navy-combat-ship

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/04/us_navy_ship_chief_demoted/){:target="_blank" rel="noopener"}

> In the Navy, no, you cannot have an unauthorized WLAN. In the Navy, no, that's not a good plan The US Navy has cracked down on an illicit Wi-Fi network installed on a combat ship by demoting the senior enlisted leader who ordered it to be set up.... [...]
