Title: Dark-web kingpin puts 'stolen' internal AMD databases, source code up for sale
Date: 2024-06-18T23:01:39+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-06-18-dark-web-kingpin-puts-stolen-internal-amd-databases-source-code-up-for-sale

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/18/amd_intelbroker_breachforums/){:target="_blank" rel="noopener"}

> Chip designer really gonna need to channel some Zen right now AMD's IT team is no doubt going through its logs today after cyber-crooks put up for sale what is claimed to be internal data stolen from the US microprocessor designer.... [...]
