Title: Tor Browser 13.5 brings Android enhancements, better bridge management
Date: 2024-06-21T10:26:46-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2024-06-21-tor-browser-135-brings-android-enhancements-better-bridge-management

[Source](https://www.bleepingcomputer.com/news/security/tor-browser-135-brings-android-enhancements-better-bridge-management/){:target="_blank" rel="noopener"}

> The Tor Project has released Tor Browser 13.5, bringing several improvements and enhancements for Android and desktop versions. [...]
