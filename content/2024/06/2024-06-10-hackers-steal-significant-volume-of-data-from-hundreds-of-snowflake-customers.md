Title: Hackers steal “significant volume” of data from hundreds of Snowflake customers
Date: 2024-06-10T22:08:42+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;Data breaches;multi factor authentication;snowflake
Slug: 2024-06-10-hackers-steal-significant-volume-of-data-from-hundreds-of-snowflake-customers

[Source](https://arstechnica.com/?p=2030619){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) As many as 165 customers of cloud storage provider Snowflake have been compromised by a group that obtained login credentials through information-stealing malware, researchers said Monday. On Friday, Lending Tree subsidiary QuoteWizard confirmed it was among the customers notified by Snowflake that it was affected in the incident. Lending Tree spokesperson Megan Greuling said the company is in the process of determining whether data stored on Snowflake has been stolen. “That investigation is ongoing,” she wrote in an email. “As of this time, it does not appear that consumer financial account information was impacted, nor information [...]
