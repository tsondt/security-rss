Title: Mozilla Firefox can now secure access to passwords with device credentials
Date: 2024-06-14T16:19:27-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Software
Slug: 2024-06-14-mozilla-firefox-can-now-secure-access-to-passwords-with-device-credentials

[Source](https://www.bleepingcomputer.com/news/security/mozilla-firefox-can-now-secure-access-to-passwords-with-device-credentials/){:target="_blank" rel="noopener"}

> Mozilla Firefox finally allows you to further protect local access to stored credentials in the browser's password manager using your device's login, including a password, fingerprint, pin, or other biometrics [...]
