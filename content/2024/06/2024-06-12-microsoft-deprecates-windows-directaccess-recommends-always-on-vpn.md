Title: Microsoft deprecates Windows DirectAccess, recommends Always On VPN
Date: 2024-06-12T11:05:11-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-06-12-microsoft-deprecates-windows-directaccess-recommends-always-on-vpn

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-deprecates-windows-directaccess-recommends-always-on-vpn/){:target="_blank" rel="noopener"}

> Microsoft has announced that the DirectAccess remote access solution is now deprecated and will be removed in a future release of Windows, recommending companies migrate to the 'Always On VPN' for enhanced security and continued support. [...]
