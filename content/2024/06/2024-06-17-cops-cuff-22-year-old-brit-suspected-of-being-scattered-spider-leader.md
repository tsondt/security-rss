Title: Cops cuff 22-year-old Brit suspected of being Scattered Spider leader
Date: 2024-06-17T13:00:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-17-cops-cuff-22-year-old-brit-suspected-of-being-scattered-spider-leader

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/17/scattered_spider_arrest/){:target="_blank" rel="noopener"}

> Spanish plod make arrest at airport before he jetted off to Italy Spanish police arrested a person they allege to be the leader of the notorious cybercrime gang Scattered Spider as he boarded a private flight to Naples.... [...]
