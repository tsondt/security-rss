Title: UK's Total Fitness exposed nearly 500K images of members, staff through unprotected database
Date: 2024-06-17T10:35:55+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-17-uks-total-fitness-exposed-nearly-500k-images-of-members-staff-through-unprotected-database

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/17/uks_total_fitness_exposed_nearly/){:target="_blank" rel="noopener"}

> Health club chain headed for the spa on choose-a-password day Exclusive A cybersecurity researcher claims UK health club and gym chain Total Fitness bungled its data protection responsibilities by failing to lock down a database chock-full of members' personal data.... [...]
