Title: Pure Storage confirms data breach after Snowflake account hack
Date: 2024-06-11T08:48:05-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-06-11-pure-storage-confirms-data-breach-after-snowflake-account-hack

[Source](https://www.bleepingcomputer.com/news/security/pure-storage-confirms-data-breach-after-snowflake-account-hack/){:target="_blank" rel="noopener"}

> Pure Storage, a leading provider of cloud storage systems and services, confirmed on Monday that attackers breached its Snowflake workspace and gained access to what the company describes as telemetry information [...]
