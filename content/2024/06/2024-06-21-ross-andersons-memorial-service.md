Title: Ross Anderson’s Memorial Service
Date: 2024-06-21T11:04:12+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptography;security engineering
Slug: 2024-06-21-ross-andersons-memorial-service

[Source](https://www.schneier.com/blog/archives/2024/06/ross-andersons-memorial-service.html){:target="_blank" rel="noopener"}

> The memorial service for Ross Anderson will be held on Saturday, at 2:00 PM BST. People can attend remotely on Zoom. (The passcode is “L3954FrrEF”.) [...]
