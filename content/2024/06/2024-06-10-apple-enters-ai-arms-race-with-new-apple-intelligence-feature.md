Title: Apple enters AI arms race with new Apple Intelligence feature
Date: 2024-06-10T17:57:02-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Apple;Security
Slug: 2024-06-10-apple-enters-ai-arms-race-with-new-apple-intelligence-feature

[Source](https://www.bleepingcomputer.com/news/apple/apple-enters-ai-arms-race-with-new-apple-intelligence-feature/){:target="_blank" rel="noopener"}

> Apple unveiled its new 'Apple Intelligence' feature today at its 2024 Worldwide Developer Conference, finally unveiling its generative AI strategy that will power new personalized experiences on Apple devices. [...]
