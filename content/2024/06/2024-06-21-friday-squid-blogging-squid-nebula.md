Title: Friday Squid Blogging: Squid Nebula
Date: 2024-06-21T21:06:45+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-06-21-friday-squid-blogging-squid-nebula

[Source](https://www.schneier.com/blog/archives/2024/06/friday-squid-blogging-squid-nebula-2.html){:target="_blank" rel="noopener"}

> Beautiful astronomical photo. [...]
