Title: Single point of software failure could hamstring 15K car dealerships for days
Date: 2024-06-20T16:03:50+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;Cars;Security;car dealers;car dealerships;cars;cdk global;crm;ransomware;SaaS;venture capital;VPN
Slug: 2024-06-20-single-point-of-software-failure-could-hamstring-15k-car-dealerships-for-days

[Source](https://arstechnica.com/?p=2032502){:target="_blank" rel="noopener"}

> Enlarge / Ford Mustang Mach E electric vehicles are offered for sale at a dealership on June 5, 2024, in Chicago, Illinois. (credit: Scott Olson / Getty Images) CDK Global touts itself as an all-in-one software-as-a-service solution that is "trusted by nearly 15,000 dealer locations." One connection, over an always-on VPN to CDK's data centers, gives a dealership customer relationship management (CRM) software, financing, inventory, and more back-office tools. That all-in-one nature explains why people trying to buy cars, and especially those trying to sell them, have had a rough couple of days. CDK's services have been down, due to [...]
