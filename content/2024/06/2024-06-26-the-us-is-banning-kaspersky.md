Title: The US Is Banning Kaspersky
Date: 2024-06-26T11:06:26+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;antivirus;cyberespionage;espionage;national security policy
Slug: 2024-06-26-the-us-is-banning-kaspersky

[Source](https://www.schneier.com/blog/archives/2024/06/the-us-is-banning-kaspersky.html){:target="_blank" rel="noopener"}

> This move has been coming for a long time. The Biden administration on Thursday said it’s banning the company from selling its products to new US-based customers starting on July 20, with the company only allowed to provide software updates to existing customers through September 29. The ban—­the first such action under authorities given to the Commerce Department in 2019­—follows years of warnings from the US intelligence community about Kaspersky being a national security threat because Moscow could allegedly commandeer its all-seeing antivirus software to spy on its customers. [...]
