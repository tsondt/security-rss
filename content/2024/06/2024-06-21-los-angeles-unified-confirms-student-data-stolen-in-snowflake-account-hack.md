Title: Los Angeles Unified confirms student data stolen in Snowflake account hack
Date: 2024-06-21T17:09:33-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-21-los-angeles-unified-confirms-student-data-stolen-in-snowflake-account-hack

[Source](https://www.bleepingcomputer.com/news/security/los-angeles-unified-confirms-student-data-stolen-in-snowflake-account-hack/){:target="_blank" rel="noopener"}

> The Los Angeles Unified School District has confirmed a data breach after threat actors stole student and employee data by breaching the company's Snowflake account. [...]
