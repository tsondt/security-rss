Title: Chinese hacking groups team up in cyber espionage campaign
Date: 2024-06-05T16:06:11-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-05-chinese-hacking-groups-team-up-in-cyber-espionage-campaign

[Source](https://www.bleepingcomputer.com/news/security/chinese-hacking-groups-team-up-in-cyber-espionage-campaign/){:target="_blank" rel="noopener"}

> Chinese state-sponsored actors have been targeting a government agency since at least March 2023 in a cyberespionage campaign that researchers track as Crimson Palace [...]
