Title: Application Security at re:Inforce 2024
Date: 2024-06-04T19:04:06+00:00
Author: Daniel Begimher
Category: AWS Security
Tags: Announcements;AWS re:Inforce;Events;Foundational (100);Security, Identity, & Compliance;Application security;Live Events;re:Inforce 2024;Security Blog
Slug: 2024-06-04-application-security-at-reinforce-2024

[Source](https://aws.amazon.com/blogs/security/application-security-at-reinforce-2024/){:target="_blank" rel="noopener"}

> Join us in Philadelphia, Pennsylvania, on June 10–12, 2024, for AWS re:Inforce, a security learning conference where you can enhance your skills and confidence in cloud security, compliance, identity, and privacy. As an attendee, you will have access to hundreds of technical and non-technical sessions, an Expo featuring Amazon Web Services (AWS) experts and AWS [...]
