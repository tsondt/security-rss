Title: New Gitloker attacks wipe GitHub repos in extortion scheme
Date: 2024-06-06T13:53:35-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-06-06-new-gitloker-attacks-wipe-github-repos-in-extortion-scheme

[Source](https://www.bleepingcomputer.com/news/security/new-gitloker-attacks-wipe-github-repos-in-extortion-scheme/){:target="_blank" rel="noopener"}

> Attackers are targeting GitHub repositories, wiping their contents, and asking the victims to reach out on Telegram for more information. [...]
