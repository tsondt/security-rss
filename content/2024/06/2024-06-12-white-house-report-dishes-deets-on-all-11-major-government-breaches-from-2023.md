Title: White House report dishes deets on all 11 major government breaches from 2023
Date: 2024-06-12T16:15:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-12-white-house-report-dishes-deets-on-all-11-major-government-breaches-from-2023

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/12/white_house_report/){:target="_blank" rel="noopener"}

> The MOVEit breach and ransomware weren’t kind to the Feds last year The number of cybersecurity incidents reported by US federal agencies rose 9.9 percent year-on-year (YoY) in 2023 to a total of 32,211, per a new White House report, which also spilled the details on the most serious incidents suffered across the government.... [...]
