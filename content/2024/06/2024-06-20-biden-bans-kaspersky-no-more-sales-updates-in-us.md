Title: Biden bans Kaspersky: No more sales, updates in US
Date: 2024-06-20T21:07:31+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-20-biden-bans-kaspersky-no-more-sales-updates-in-us

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/20/us_bans_kaspersky_software/){:target="_blank" rel="noopener"}

> Blockade begins July 20 on national security grounds as antivirus slinger vows to fight back The Biden administration today banned the sale of Kaspersky Lab products and services in the United States, declaring the Russian biz a national security risk.... [...]
