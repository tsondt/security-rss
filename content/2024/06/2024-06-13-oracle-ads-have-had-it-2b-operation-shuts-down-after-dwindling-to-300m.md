Title: Oracle Ads have had it: $2B operation shuts down after dwindling to $300M
Date: 2024-06-13T19:55:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-06-13-oracle-ads-have-had-it-2b-operation-shuts-down-after-dwindling-to-300m

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/13/oracle_online_ads/){:target="_blank" rel="noopener"}

> In this slightly more private era, your data ain't as profitable as it once was Analysis Oracle Advertising is shutting down, CEO Safra Catz said during the database goliath's fiscal 2024 Q4 earnings call with Wall Street this week.... [...]
