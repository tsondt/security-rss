Title: LLMs Acting Deceptively
Date: 2024-06-11T11:02:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;artificial intelligence;deception;LLM
Slug: 2024-06-11-llms-acting-deceptively

[Source](https://www.schneier.com/blog/archives/2024/06/llms-acting-deceptively.html){:target="_blank" rel="noopener"}

> New research: “ Deception abilities emerged in large language models “: Abstract: Large language models (LLMs) are currently at the forefront of intertwining AI systems with human communication and everyday life. Thus, aligning them with human values is of great importance. However, given the steady increase in reasoning abilities, future LLMs are under suspicion of becoming able to deceive human operators and utilizing this ability to bypass monitoring efforts. As a prerequisite to this, LLMs need to possess a conceptual understanding of deception strategies. This study reveals that such strategies emerged in state-of-the-art LLMs, but were nonexistent in earlier LLMs. [...]
