Title: AI platform Hugging Face says hackers stole auth tokens from Spaces
Date: 2024-06-02T16:56:26-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-02-ai-platform-hugging-face-says-hackers-stole-auth-tokens-from-spaces

[Source](https://www.bleepingcomputer.com/news/security/ai-platform-hugging-face-says-hackers-stole-auth-tokens-from-spaces/){:target="_blank" rel="noopener"}

> AI platform Hugging Face says that its Spaces platform was breached, allowing hackers to access authentication secrets for its members. [...]
