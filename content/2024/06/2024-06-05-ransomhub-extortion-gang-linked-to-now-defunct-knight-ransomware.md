Title: RansomHub extortion gang linked to now-defunct Knight ransomware
Date: 2024-06-05T08:39:15-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-05-ransomhub-extortion-gang-linked-to-now-defunct-knight-ransomware

[Source](https://www.bleepingcomputer.com/news/security/ransomhub-extortion-gang-linked-to-now-defunct-knight-ransomware/){:target="_blank" rel="noopener"}

> Security researchers analyzing the relatively new RansomHub ransomware-as-a-service believe that it has evoloved from the currently defunct Knight ransomware project. [...]
