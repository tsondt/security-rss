Title: Malicious VSCode extensions with millions of installs discovered
Date: 2024-06-09T10:22:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-06-09-malicious-vscode-extensions-with-millions-of-installs-discovered

[Source](https://www.bleepingcomputer.com/news/security/malicious-visual-studio-code-extensions-with-millions-of-installs-discovered/){:target="_blank" rel="noopener"}

> A group of Israeli researchers explored the security of the Visual Studio Code marketplace and managed to "infect" over 100 organizations by trojanizing a copy of the popular 'Dracula Official theme to include risky code. Further research into the VSCode Marketplace found thousands of extensions with millions of installs. [...]
