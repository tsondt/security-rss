Title: FCC takes some action against notorious BGP
Date: 2024-06-07T22:29:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-07-fcc-takes-some-action-against-notorious-bgp

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/07/fcc_bgp_proposal/){:target="_blank" rel="noopener"}

> How's your RPKI-based security plan coming along? Feds want to know US broadband providers will soon have to provide proof to Uncle Sam that they are taking steps to prevent Border Gateway Protocol (BGP) hijacking and locking down internet routing in general.... [...]
