Title: Cox fixed an API auth bypass exposing millions of modems to attacks
Date: 2024-06-03T17:10:11-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-06-03-cox-fixed-an-api-auth-bypass-exposing-millions-of-modems-to-attacks

[Source](https://www.bleepingcomputer.com/news/security/cox-fixed-an-api-auth-bypass-exposing-millions-of-modems-to-attacks/){:target="_blank" rel="noopener"}

> ​Cox Communications has fixed an authorization bypass vulnerability that enabled remote attackers to abuse exposed backend APIs to reset millions of modems' settings and steal customers' sensitive personal information. [...]
