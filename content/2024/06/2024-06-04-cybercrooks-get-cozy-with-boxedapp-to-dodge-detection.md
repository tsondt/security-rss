Title: Cybercrooks get cozy with BoxedApp to dodge detection
Date: 2024-06-04T12:00:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-04-cybercrooks-get-cozy-with-boxedapp-to-dodge-detection

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/04/cybercriminals_abusing_boxedapp/){:target="_blank" rel="noopener"}

> Some of the biggest names in the game are hopping on the trend Malware miscreants are increasingly showing a penchant for abusing legitimate, commercial packer apps to evade detection.... [...]
