Title: US bans sales of Kaspersky antivirus software over Russia ties
Date: 2024-06-21T02:42:50+00:00
Author: Agence France-Presse
Category: The Guardian
Tags: US foreign policy;US news;Data and computer security;Computing;Russia;Technology;Malware
Slug: 2024-06-21-us-bans-sales-of-kaspersky-antivirus-software-over-russia-ties

[Source](https://www.theguardian.com/us-news/article/2024/jun/21/us-kaspersky-antivirus-software-ban-russia-ties){:target="_blank" rel="noopener"}

> Washington says Moscow’s influence over company poses significant risk, as Kaspersky argues its activities do not threaten US security Joe Biden’s administration has banned Russia-based cybersecurity firm Kaspersky from providing its popular antivirus products in the US over national security concerns. “Kaspersky will generally no longer be able to, among other activities, sell its software within the United States or provide updates to software already in use,” said a commerce department statement. The announcement came after a lengthy investigation found Kaspersky’s “continued operations in the United States presented a national security risk due to the Russian government’s offensive cyber capabilities [...]
