Title: Uncle Sam seeks to claw back $5M+ stolen from trade union through spoofed email
Date: 2024-06-06T13:30:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-06-uncle-sam-seeks-to-claw-back-5m-stolen-from-trade-union-through-spoofed-email

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/06/union_bec_scam/){:target="_blank" rel="noopener"}

> Funds are currently seized after being sent to offshore accounts The US Justice Department is seeking permission to recover more than $5 million worth of funds stolen from a trade union by business email compromise (BEC) scammers.... [...]
