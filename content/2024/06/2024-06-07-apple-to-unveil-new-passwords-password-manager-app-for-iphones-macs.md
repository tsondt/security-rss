Title: Apple to unveil new 'Passwords' password manager app for iPhones, Macs
Date: 2024-06-07T15:50:40-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Apple
Slug: 2024-06-07-apple-to-unveil-new-passwords-password-manager-app-for-iphones-macs

[Source](https://www.bleepingcomputer.com/news/security/apple-to-unveil-new-passwords-password-manager-app-for-iphones-macs/){:target="_blank" rel="noopener"}

> Apple will reportedly unveil a standalone password manager named 'Passwords' as part of iOS 18, iPadOS 18, and macOS 15 during the upcoming Apple Worldwide Developers Conference. [...]
