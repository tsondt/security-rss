Title: Ukraine busts SIM farms targeting soldiers with spyware
Date: 2024-06-14T13:22:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-14-ukraine-busts-sim-farms-targeting-soldiers-with-spyware

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/14/ukraine_sim_farm_bust/){:target="_blank" rel="noopener"}

> Russia recruits local residents to support battlefield goals Infrastructure that enabled two pro-Russia Ukraine residents to break into soldiers' devices and deploy spyware has been dismantled by the Security Service of Ukraine (SSU).... [...]
