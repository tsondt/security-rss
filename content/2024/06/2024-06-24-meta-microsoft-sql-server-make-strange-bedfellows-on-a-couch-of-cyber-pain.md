Title: Meta, Microsoft SQL Server make strange bedfellows on a couch of cyber-pain
Date: 2024-06-24T08:30:05+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2024-06-24-meta-microsoft-sql-server-make-strange-bedfellows-on-a-couch-of-cyber-pain

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/24/meta_ms_sql_column/){:target="_blank" rel="noopener"}

> Yanks get food poisoning far more often than Brits. Is American IT just as sickening? Opinion When two stories from opposite ends of the IT universe boil down to the same thing, sound the klaxons. At the uber-fashionable AI end of tech, Meta has grudgingly complied with a ruling not to feed European social media crap into its training data. Meanwhile, in the industrial slums, 20 percent of running Microsoft SQL Server instances are now past the end of support.... [...]
