Title: New Cloud KMS Autokey can help encrypt your resources quickly and efficiently
Date: 2024-06-26T16:00:00+00:00
Author: Luis Urena
Category: GCP Security
Tags: Security & Identity
Slug: 2024-06-26-new-cloud-kms-autokey-can-help-encrypt-your-resources-quickly-and-efficiently

[Source](https://cloud.google.com/blog/products/identity-security/cloud-kms-autokey-can-help-you-encrypt-resources-quickly-and-efficiently/){:target="_blank" rel="noopener"}

> Encryption is a fundamental control for data security, sovereignty, and privacy in the cloud. While Google Cloud provides default encryption for customer data at rest, many organizations want greater control over their encryption keys that control access to their data. Customer-Managed Encryption Keys (CMEK) can help you by providing flexibility in cryptographic key creation, rotation, usage logging, and storage.While CMEK provides the additional control that many organizations need, using it requires manual processes that require time and effort to ensure that the desired controls and configurations are implemented. To help make CMEK configuration more efficient, today, we're excited to announce [...]
