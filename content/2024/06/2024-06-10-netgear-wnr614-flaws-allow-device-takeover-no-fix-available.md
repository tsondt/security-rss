Title: Netgear WNR614 flaws allow device takeover, no fix available
Date: 2024-06-10T17:38:36-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2024-06-10-netgear-wnr614-flaws-allow-device-takeover-no-fix-available

[Source](https://www.bleepingcomputer.com/news/security/netgear-wnr614-flaws-allow-device-takeover-no-fix-available/){:target="_blank" rel="noopener"}

> Researchers found half a dozen vulnerabilities of varying severity impacting Netgear WNR614 N300, a budget-friendly router that proved popular among home users and small businesses. [...]
