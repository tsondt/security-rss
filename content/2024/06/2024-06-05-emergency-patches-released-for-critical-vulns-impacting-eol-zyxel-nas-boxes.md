Title: Emergency patches released for critical vulns impacting EOL Zyxel NAS boxes
Date: 2024-06-05T17:30:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-05-emergency-patches-released-for-critical-vulns-impacting-eol-zyxel-nas-boxes

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/05/zyxel_emergency_patches_nas/){:target="_blank" rel="noopener"}

> That backdoor's not meant to be there? Zyxel just released security fixes for two of its obsolete network-attached storage (NAS) devices after an intern at a security vendor reported critical flaws months ago.... [...]
