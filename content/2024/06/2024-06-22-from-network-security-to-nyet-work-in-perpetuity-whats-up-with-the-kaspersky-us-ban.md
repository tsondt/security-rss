Title: From network security to nyet work in perpetuity: What's up with the Kaspersky US ban?
Date: 2024-06-22T08:16:12+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2024-06-22-from-network-security-to-nyet-work-in-perpetuity-whats-up-with-the-kaspersky-us-ban

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/22/kaspersky_kettle/){:target="_blank" rel="noopener"}

> It's been a long time coming. Now our journos speak their brains Kettle The US government on Thursday banned Kaspersky Lab from selling its antivirus and other products in America from late July, and from issuing updates and malware signatures from October.... [...]
