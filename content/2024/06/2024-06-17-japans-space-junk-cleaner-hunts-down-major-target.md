Title: Japan's space junk cleaner hunts down major target
Date: 2024-06-17T00:44:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-06-17-japans-space-junk-cleaner-hunts-down-major-target

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/17/asia_tech_news_roundup/){:target="_blank" rel="noopener"}

> PLUS: Australia to age limit social media; Hong Kong's robo-dogs; India's new tech minister The space junk cleaning mission launched by Japan's Aerospace Exploration Agency (JAXA) has successfully hunted down one of its targets.... [...]
