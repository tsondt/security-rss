Title: How to safeguard your SSH environment with Identity-Aware Proxy and Security Command Center
Date: 2024-06-13T16:00:00+00:00
Author: Joakim Nydrén
Category: GCP Security
Tags: Security & Identity
Slug: 2024-06-13-how-to-safeguard-your-ssh-environment-with-identity-aware-proxy-and-security-command-center

[Source](https://cloud.google.com/blog/products/identity-security/how-to-safeguard-your-ssh-environment-with-identity-aware-proxy-and-security-command-center/){:target="_blank" rel="noopener"}

> Identifying and securing internet-facing resources is an essential part of cloud security risk management. Google Cloud offers tools to help you understand risks and add strong access controls to your organization and projects where they are needed. These tools and controls can help mitigate the risks such as the recent critical security vulnerability ( CVE-2024-3094 ) discovered in the XZ Utils compression software. This vulnerability affects sshd, a popular tool for secure remote access, and could allow attackers to execute remote code on servers without requiring authentication. While Google Cloud Compute Engine customers who rely on our supported public images [...]
