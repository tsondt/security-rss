Title: 'New York Times source code' leaks online via 4chan
Date: 2024-06-07T23:39:24+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-07-new-york-times-source-code-leaks-online-via-4chan

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/07/4chan_nyt_code/){:target="_blank" rel="noopener"}

> Breaking breaking-news news A 4chan user claims to have leaked 270GB of internal New York Times data, including source code, via the notorious image board.... [...]
