Title: Levi's and more affected in pants-dropping week of data breaches
Date: 2024-06-24T10:34:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-24-levis-and-more-affected-in-pants-dropping-week-of-data-breaches

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/24/security_breaches_of_the_week/){:target="_blank" rel="noopener"}

> A busy few days for security teams There were data breaches galore in the US last week with various major incidents reported to state attorneys general, some in good time, some not.... [...]
