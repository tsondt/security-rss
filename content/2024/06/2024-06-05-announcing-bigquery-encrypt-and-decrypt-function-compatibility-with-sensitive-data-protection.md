Title: Announcing BigQuery Encrypt and Decrypt function compatibility with Sensitive Data Protection
Date: 2024-06-05T16:00:00+00:00
Author: Parth Desai
Category: GCP Security
Tags: Data Analytics;Security & Identity
Slug: 2024-06-05-announcing-bigquery-encrypt-and-decrypt-function-compatibility-with-sensitive-data-protection

[Source](https://cloud.google.com/blog/products/identity-security/using-bigquery-encrypt-and-decrypt-with-sensitive-data-protection/){:target="_blank" rel="noopener"}

> Organizations collect vast amounts of data to create innovative solutions, perform ground breaking research, or optimize their designs. With this comes the responsibility to ensure data is adequately protected to meet regulatory, compliance, contractual or internal security requirements. For organizations that want to move their data warehouses from on-premises to cloud-first systems, such as BigQuery, protecting sensitive data from unauthorized access or accidental exposure is crucial. Using encryption-based tokenization is a vital tool to create an additional layer of defense and fine-grained data control. In addition to storage-level encryption, whether using Google-managed or customer-managed keys, BigQuery now has seamless integration [...]
