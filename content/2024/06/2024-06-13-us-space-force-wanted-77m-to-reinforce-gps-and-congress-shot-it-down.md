Title: US Space Force wanted $77M to reinforce GPS – and Congress shot it down
Date: 2024-06-13T22:42:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-06-13-us-space-force-wanted-77m-to-reinforce-gps-and-congress-shot-it-down

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/13/space_force_gps/){:target="_blank" rel="noopener"}

> Can't we do this another way, like without these mini-sats costing $1B over 5 years, House reps wonder A plan by America's Space Force to harden GPS against spoofing attacks may be going nowhere: A request by the service branch for $77 million of public cash to finish the work is struggling to get approval from Congress.... [...]
