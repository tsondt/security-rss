Title: Scathing report on Medibank cyberattack highlights unenforced MFA
Date: 2024-06-18T13:25:44-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-18-scathing-report-on-medibank-cyberattack-highlights-unenforced-mfa

[Source](https://www.bleepingcomputer.com/news/security/scathing-report-on-medibank-cyberattack-highlights-unenforced-mfa/){:target="_blank" rel="noopener"}

> A scathing report by Australia's Information Commissioner details how misconfigurations and missed alerts allowed a hacker to breach Medibank and steal data from over 9 million people. [...]
