Title: Juniper releases out-of-cycle fix for max severity auth bypass flaw
Date: 2024-06-30T11:14:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-30-juniper-releases-out-of-cycle-fix-for-max-severity-auth-bypass-flaw

[Source](https://www.bleepingcomputer.com/news/security/juniper-releases-out-of-cycle-fix-for-max-severity-auth-bypass-flaw/){:target="_blank" rel="noopener"}

> Juniper Networks has released an emergency update to address a maximum severity vulnerability that leads to authentication bypass in Session Smart Router (SSR), Session Smart Conductor, and WAN Assurance Router products. [...]
