Title: Russian hacktivists vow mass attacks against EU elections
Date: 2024-06-07T10:29:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-07-russian-hacktivists-vow-mass-attacks-against-eu-elections

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/07/russian_hacktivists_eu_elections/){:target="_blank" rel="noopener"}

> But do they get to wear 'I DDoSed' stickers? A Russian hacktivist crew has threatened to attack European internet infrastructure as four days of EU elections begin on Thursday.... [...]
