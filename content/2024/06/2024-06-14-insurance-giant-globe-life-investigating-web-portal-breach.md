Title: Insurance giant Globe Life investigating web portal breach
Date: 2024-06-14T08:39:36-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-06-14-insurance-giant-globe-life-investigating-web-portal-breach

[Source](https://www.bleepingcomputer.com/news/security/insurance-giant-globe-life-investigating-web-portal-breach/){:target="_blank" rel="noopener"}

> American financial services holding company Globe Life says attackers may have accessed consumer and policyholder data after breaching one of its web portals. [...]
