Title: Friday Squid Blogging: Squid Cartoon
Date: 2024-06-14T21:06:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-06-14-friday-squid-blogging-squid-cartoon

[Source](https://www.schneier.com/blog/archives/2024/06/friday-squid-blogging-squid-cartoon-2.html){:target="_blank" rel="noopener"}

> Squid humor. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
