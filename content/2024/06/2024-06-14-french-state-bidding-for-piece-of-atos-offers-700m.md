Title: French state bidding for piece of Atos, offers €700M
Date: 2024-06-14T11:33:10+00:00
Author: Dan Robinson
Category: The Register
Tags: 
Slug: 2024-06-14-french-state-bidding-for-piece-of-atos-offers-700m

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/14/french_state_still_wants_atos_stake/){:target="_blank" rel="noopener"}

> Big data + security division could be owed by the government and its people The French government has confirmed an offer of €700 million ($748 million) for key assets of ailing IT services giant Atos, following the company’s acceptance of a restructuring deal earlier this week.... [...]
