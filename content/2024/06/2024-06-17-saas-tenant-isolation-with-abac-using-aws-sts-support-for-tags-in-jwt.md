Title: SaaS tenant isolation with ABAC using AWS STS support for tags in JWT
Date: 2024-06-17T18:01:58+00:00
Author: Manuel Heinkel
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;AWS STS;JWT;SaaS;Security Blog;Tags
Slug: 2024-06-17-saas-tenant-isolation-with-abac-using-aws-sts-support-for-tags-in-jwt

[Source](https://aws.amazon.com/blogs/security/saas-tenant-isolation-with-abac-using-aws-sts-support-for-tags-in-jwt/){:target="_blank" rel="noopener"}

> As independent software vendors (ISVs) shift to a multi-tenant software-as-a-service (SaaS) model, they commonly adopt a shared infrastructure model to achieve cost and operational efficiency. The more ISVs move into a multi-tenant model, the more concern they may have about the potential for one tenant to access the resources of another tenant. SaaS systems include [...]
