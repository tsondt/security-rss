Title: Alleged Scattered Spider sim-swapper arrested in Spain
Date: 2024-06-17T10:16:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;Security
Slug: 2024-06-17-alleged-scattered-spider-sim-swapper-arrested-in-spain

[Source](https://www.bleepingcomputer.com/news/legal/alleged-scattered-spider-sim-swapper-arrested-in-spain/){:target="_blank" rel="noopener"}

> A 22-year-old British national allegedly linked to the Scattered Spider hacking group and responsible for attacks on 45 U.S. companies has been arrested in Palma de Mallorca, Spain. [...]
