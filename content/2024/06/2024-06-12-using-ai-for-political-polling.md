Title: Using AI for Political Polling
Date: 2024-06-12T11:02:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;chatbots;democracy;LLM
Slug: 2024-06-12-using-ai-for-political-polling

[Source](https://www.schneier.com/blog/archives/2024/06/using-ai-for-political-polling.html){:target="_blank" rel="noopener"}

> Public polling is a critical function of modern political campaigns and movements, but it isn’t what it once was. Recent US election cycles have produced copious postmortems explaining both the successes and the flaws of public polling. There are two main reasons polling fails. First, nonresponse has skyrocketed. It’s radically harder to reach people than it used to be. Few people fill out surveys that come in the mail anymore. Few people answer their phone when a stranger calls. Pew Research reported that 36% of the people they called in 1997 would talk to them, but only 6% by 2018. [...]
