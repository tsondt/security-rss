Title: Cisco fixes WebEx flaw that allowed government, military meetings to be spied on
Date: 2024-06-07T15:04:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-07-cisco-fixes-webex-flaw-that-allowed-government-military-meetings-to-be-spied-on

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/07/cisco_fixes_webex_flaw_which/){:target="_blank" rel="noopener"}

> Researchers were able to glean data from 10,000 meetings held by top Dutch gov officials Cisco squashed some bugs this week that allowed anyone to view WebEx meeting information and join them, potentially opening up security and privacy concerns for highly sensitive meets.... [...]
