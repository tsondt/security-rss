Title: How to navigate NIS2 and secure your vulnerabilities
Date: 2024-06-07T15:07:13+00:00
Author: Elizabeth Coles
Category: The Register
Tags: 
Slug: 2024-06-07-how-to-navigate-nis2-and-secure-your-vulnerabilities

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/07/how_to_navigate_nis2_and/){:target="_blank" rel="noopener"}

> Meeting the challenges of managing risk for cyber-physical systems Webinar The risk of cyber attack hangs over every digital environment but cyber physical systems (CPS) tend to be more vulnerable - after all, they weren't usually designed with security in mind.... [...]
