Title: Amtrak confirms crooks are breaking into user accounts, derailing email addresses
Date: 2024-06-19T13:00:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-19-amtrak-confirms-crooks-are-breaking-into-user-accounts-derailing-email-addresses

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/19/amtrak_has_had_another_breach/){:target="_blank" rel="noopener"}

> Rail company goes full steam ahead with notification letters to Rewards customers about spilled card details and more US rail company Amtrak is writing to users of its Guest Rewards program to inform them that their data is potentially at risk following a derailment of their account security.... [...]
