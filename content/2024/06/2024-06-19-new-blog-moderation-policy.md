Title: New Blog Moderation Policy
Date: 2024-06-19T20:26:22+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2024-06-19-new-blog-moderation-policy

[Source](https://www.schneier.com/blog/archives/2024/06/new-blog-moderation-policy.html){:target="_blank" rel="noopener"}

> There has been a lot of toxicity in the comments section of this blog. Recently, we’re having to delete more and more comments. Not just spam and off-topic comments, but also sniping and personal attacks. It’s gotten so bad that I need to do something. My options are limited because I’m just one person, and this website is free, ad-free, and anonymous. I pay for a part-time moderator out of pocket; he isn’t able to constantly monitor comments. And I’m unwilling to require verified accounts. So starting now, we will be pre-screening comments and letting through only those that 1) [...]
