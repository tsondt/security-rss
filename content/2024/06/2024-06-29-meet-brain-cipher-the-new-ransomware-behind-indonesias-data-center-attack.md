Title: Meet Brain Cipher — The new ransomware behind Indonesia's data center attack
Date: 2024-06-29T10:14:28-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-29-meet-brain-cipher-the-new-ransomware-behind-indonesias-data-center-attack

[Source](https://www.bleepingcomputer.com/news/security/meet-brain-cipher-the-new-ransomware-behind-indonesia-data-center-attack/){:target="_blank" rel="noopener"}

> The new Brain Cipher ransomware operation has begun targeting organizations worldwide, gaining media attention for a recent attack on Indonesia's temporary National Data Center. [...]
