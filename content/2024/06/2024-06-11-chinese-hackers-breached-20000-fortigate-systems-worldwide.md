Title: Chinese hackers breached 20,000 FortiGate systems worldwide
Date: 2024-06-11T12:22:12-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-06-11-chinese-hackers-breached-20000-fortigate-systems-worldwide

[Source](https://www.bleepingcomputer.com/news/security/chinese-hackers-breached-20-000-fortigate-systems-worldwide/){:target="_blank" rel="noopener"}

> The Dutch Military Intelligence and Security Service (MIVD) warned today that the impact of a Chinese cyber-espionage campaign unveiled earlier this year is "much larger than previously known." [...]
