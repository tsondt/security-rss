Title: WA man set up fake free wifi at Australian airports and on flights to steal people’s data, police allege
Date: 2024-06-28T01:59:08+00:00
Author: Stephanie Convery
Category: The Guardian
Tags: Cybercrime;Technology;Australia news;Western Australia;Wifi;Data and computer security;Privacy
Slug: 2024-06-28-wa-man-set-up-fake-free-wifi-at-australian-airports-and-on-flights-to-steal-peoples-data-police-allege

[Source](https://www.theguardian.com/technology/article/2024/jun/28/wa-man-fake-free-wifi-airports-data-theft-ntwnfb){:target="_blank" rel="noopener"}

> Investigation launched after airline reported a suspicious network popped up during a domestic flight in April Follow our Australia news live blog for latest updates Get our morning and afternoon news emails, free app or daily news podcast A man has been charged after he allegedly set up fake free wifi networks at Australian airports and on domestic flights to steal personal data from unsuspecting members of the public. The 42-year-old Western Australian man is facing nine cybercrime charges and was due to appear in Perth magistrates court on Friday. Sign up for Guardian Australia’s free morning and afternoon email [...]
