Title: Patch Tuesday, June 2024 “Recall” Edition
Date: 2024-06-11T22:57:35+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Time to Patch;Adam Barnett;Copilot;CVE-2004-30080;CVE-2024-30078;Immersive Labs;Kevin Beaumont;Kevin Breen;Mastodon;Microsoft Message Queuing;Microsoft Patch Tuesday June 2024;Patrick Gray;Rapid7;Recall;Risky Business podcast;windows;Windows WiFi Driver
Slug: 2024-06-11-patch-tuesday-june-2024-recall-edition

[Source](https://krebsonsecurity.com/2024/06/patch-tuesday-june-2024-recall-edition/){:target="_blank" rel="noopener"}

> Microsoft today released updates to fix more than 50 security vulnerabilities in Windows and related software, a relatively light Patch Tuesday this month for Windows users. The software giant also responded to a torrent of negative feedback on a new feature of Redmond’s flagship operating system that constantly takes screenshots of whatever users are doing on their computers, saying the feature would no longer be enabled by default. Last month, Microsoft debuted Copilot+ PCs, an AI-enabled version of Windows. Copilot+ ships with a feature nobody asked for that Redmond has aptly dubbed Recall, which constantly takes screenshots of what the [...]
