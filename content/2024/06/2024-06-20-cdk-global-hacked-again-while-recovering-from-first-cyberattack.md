Title: CDK Global hacked again while recovering from first cyberattack
Date: 2024-06-20T11:32:08-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-20-cdk-global-hacked-again-while-recovering-from-first-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/cdk-global-hacked-again-while-recovering-from-first-cyberattack/){:target="_blank" rel="noopener"}

> Car dealership SaaS platform CDK Global suffered an additional breach Wednesday night as it was starting to restore systems shut down in an previous cyberattack. [...]
