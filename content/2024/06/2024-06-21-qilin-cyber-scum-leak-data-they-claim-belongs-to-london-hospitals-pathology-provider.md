Title: Qilin cyber scum leak data they claim belongs to London hospitals’ pathology provider
Date: 2024-06-21T11:15:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-21-qilin-cyber-scum-leak-data-they-claim-belongs-to-london-hospitals-pathology-provider

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/21/qilin_cyber_scum_leak_the/){:target="_blank" rel="noopener"}

> At least they didn’t get paid their $50M ransom demand The ransomware gang responsible for the chaos at London hospitals kept true to its word and released a trove of data that it claims belongs to pathology services provider Synnovis.... [...]
