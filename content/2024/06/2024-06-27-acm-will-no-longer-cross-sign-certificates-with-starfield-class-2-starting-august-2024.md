Title: ACM will no longer cross sign certificates with Starfield Class 2 starting August 2024
Date: 2024-06-27T20:02:55+00:00
Author: Chandan Kundapur
Category: AWS Security
Tags: Announcements;AWS Certificate Manager;Intermediate (200);Security, Identity, & Compliance;ACM;Security Blog
Slug: 2024-06-27-acm-will-no-longer-cross-sign-certificates-with-starfield-class-2-starting-august-2024

[Source](https://aws.amazon.com/blogs/security/acm-will-no-longer-cross-sign-certificates-with-starfield-class-2-starting-august-2024/){:target="_blank" rel="noopener"}

> AWS Certificate Manager (ACM) is a managed service that you can use to provision, manage, and deploy public and private TLS certificates for use with Elastic Load Balancing (ELB), Amazon CloudFront, Amazon API Gateway, and other integrated AWS services. Starting August 2024, public certificates issued from ACM will terminate at the Starfield Services G2 (G2) root [...]
