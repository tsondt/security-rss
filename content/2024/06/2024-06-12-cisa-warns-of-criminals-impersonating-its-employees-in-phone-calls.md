Title: CISA warns of criminals impersonating its employees in phone calls
Date: 2024-06-12T13:58:16-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-06-12-cisa-warns-of-criminals-impersonating-its-employees-in-phone-calls

[Source](https://www.bleepingcomputer.com/news/security/cisa-warns-of-criminals-impersonating-its-employees-in-phone-calls/){:target="_blank" rel="noopener"}

> Today, the Cybersecurity and Infrastructure Security Agency (CISA) warned that criminals are impersonating its employees in phone calls and attempting to deceive potential victims into transferring money. [...]
