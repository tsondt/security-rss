Title: 7,000 LockBit decryption keys now in the hands of the FBI, offering victims hope
Date: 2024-06-06T19:13:13+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;encryption;lockbit;ransomware
Slug: 2024-06-06-7000-lockbit-decryption-keys-now-in-the-hands-of-the-fbi-offering-victims-hope

[Source](https://arstechnica.com/?p=2029593){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images ) The FBI is urging victims of one of the most prolific ransomware groups to come forward after agents recovered thousands of decryption keys that may allow the recovery of data that has remained inaccessible for months or years. The revelation, made Wednesday by a top FBI official, comes three months after an international roster of law enforcement agencies seized servers and other infrastructure used by LockBit, a ransomware syndicate that authorities say has extorted more than $1 billion from 7,000 victims around the world. Authorities said at the time that they took control of 1,000 [...]
