Title: America's best chance for nationwide privacy law could do more harm than good
Date: 2024-06-25T00:02:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-06-25-americas-best-chance-for-nationwide-privacy-law-could-do-more-harm-than-good

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/25/american_privacy_rights_actsa/){:target="_blank" rel="noopener"}

> 'Congress has effectively gutted it as part of a backroom deal' Analysis Introduced in April, the American Privacy Rights Act (APRA) was - in the words of its drafters - "the best opportunity we’ve had in decades to establish a national data privacy and security standard that gives people the right to control their personal information."... [...]
