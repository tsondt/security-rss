Title: Fake Google Chrome errors trick you into running malicious PowerShell scripts
Date: 2024-06-17T18:31:29-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-17-fake-google-chrome-errors-trick-you-into-running-malicious-powershell-scripts

[Source](https://www.bleepingcomputer.com/news/security/fake-google-chrome-errors-trick-you-into-running-malicious-powershell-scripts/){:target="_blank" rel="noopener"}

> A new malware distribution campaign uses fake Google Chrome, Word, and OneDrive errors to trick users into running malicious PowerShell "fixes" that install malware. [...]
