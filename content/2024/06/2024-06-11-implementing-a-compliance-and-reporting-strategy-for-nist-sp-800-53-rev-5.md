Title: Implementing a compliance and reporting strategy for NIST SP 800-53 Rev. 5
Date: 2024-06-11T17:06:18+00:00
Author: Josh Moss
Category: AWS Security
Tags: Intermediate (200);Security, Identity, & Compliance;Technical How-to;Automation;Compliance;FedRAMP;NIST;Security;Security Blog
Slug: 2024-06-11-implementing-a-compliance-and-reporting-strategy-for-nist-sp-800-53-rev-5

[Source](https://aws.amazon.com/blogs/security/implementing-a-compliance-and-reporting-strategy-for-nist-sp-800-53-rev-5/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) provides tools that simplify automation and monitoring for compliance with security standards, such as the NIST SP 800-53 Rev. 5 Operational Best Practices. Organizations can set preventative and proactive controls to help ensure that noncompliant resources aren’t deployed. Detective and responsive controls notify stakeholders of misconfigurations immediately and automate fixes, thus [...]
