Title: T-Mobile denies it was hacked, links leaked data to vendor breach
Date: 2024-06-19T20:43:09-04:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2024-06-19-t-mobile-denies-it-was-hacked-links-leaked-data-to-vendor-breach

[Source](https://www.bleepingcomputer.com/news/security/t-mobile-denies-it-was-hacked-links-leaked-data-to-vendor-breach/){:target="_blank" rel="noopener"}

> T-Mobile has denied it was breached or that source code was stolen after a threat actor claimed to be selling stolen data from the telecommunications company. [...]
