Title: Proton is taking its privacy-first apps to a nonprofit foundation model
Date: 2024-06-17T16:40:48+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;Tech;andy yen;encryption;privacy;proton;proton foundation;proton mail;Tim Berners-Lee
Slug: 2024-06-17-proton-is-taking-its-privacy-first-apps-to-a-nonprofit-foundation-model

[Source](https://arstechnica.com/?p=2031934){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Proton, the secure-minded email and productivity suite, is becoming a nonprofit foundation, but it doesn't want you to think about it in the way you think about other notable privacy and web foundations. "We believe that if we want to bring about large-scale change, Proton can’t be billionaire-subsidized (like Signal), Google-subsidized (like Mozilla), government-subsidized (like Tor), donation-subsidized (like Wikipedia), or even speculation-subsidized (like the plethora of crypto “foundations”)," Proton CEO Andy Yen wrote in a blog post announcing the transition. "Instead, Proton must have a profitable and healthy business at its core." The announcement comes exactly [...]
