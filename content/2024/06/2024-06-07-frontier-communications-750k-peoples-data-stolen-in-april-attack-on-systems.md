Title: Frontier Communications: 750k people's data stolen in April attack on systems
Date: 2024-06-07T18:09:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-07-frontier-communications-750k-peoples-data-stolen-in-april-attack-on-systems

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/07/frontier_communications_filing_cyberattack/){:target="_blank" rel="noopener"}

> Company says just names and SSNs affected, watering down RansomHub’s claims Frontier Communications has confirmed more than 750,000 individuals were affected in an April cyberattack on its systems, according to a regulatory filing.... [...]
