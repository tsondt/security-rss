Title: Microsoft hits snooze again on security certificate renewal
Date: 2024-06-28T13:26:06+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-06-28-microsoft-hits-snooze-again-on-security-certificate-renewal

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/28/microsoft_security_certificate_expires/){:target="_blank" rel="noopener"}

> Seeing weird warnings in Microsoft 365 and Office Online? That'll be why Microsoft has expiration issues with its TLS certificates, resulting in unwanted security warnings.... [...]
