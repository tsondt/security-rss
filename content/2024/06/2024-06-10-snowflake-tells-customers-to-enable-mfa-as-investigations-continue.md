Title: Snowflake tells customers to enable MFA as investigations continue
Date: 2024-06-10T12:30:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-06-10-snowflake-tells-customers-to-enable-mfa-as-investigations-continue

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/10/security_in_brief/){:target="_blank" rel="noopener"}

> Also, industry begs Uncle Sam for infosec reg harmony, dueling container-compromise campaigns, and crit vulns infosec in brief Cloud data analytics platform Snowflake said it is going to begin forcing customers to implement multi-factor authentication to prevent more intrusions.... [...]
