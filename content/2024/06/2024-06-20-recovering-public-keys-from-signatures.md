Title: Recovering Public Keys from Signatures
Date: 2024-06-20T11:10:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;anonymity;cryptanalysis;keys;signatures
Slug: 2024-06-20-recovering-public-keys-from-signatures

[Source](https://www.schneier.com/blog/archives/2024/06/recovering-public-keys-from-signatures.html){:target="_blank" rel="noopener"}

> Interesting summary of various ways to derive the public key from digitally signed files. Normally, with a signature scheme, you have the public key and want to know whether a given signature is valid. But what if we instead have a message and a signature, assume the signature is valid, and want to know which public key signed it? A rather delightful property if you want to attack anonymity in some proposed “everybody just uses cryptographic signatures for everything” scheme. [...]
