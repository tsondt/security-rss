Title: FBI warns of fake remote work ads used for cryptocurrency fraud
Date: 2024-06-04T16:33:02-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2024-06-04-fbi-warns-of-fake-remote-work-ads-used-for-cryptocurrency-fraud

[Source](https://www.bleepingcomputer.com/news/security/fbi-warns-of-fake-remote-work-ads-used-for-cryptocurrency-fraud/){:target="_blank" rel="noopener"}

> Today, the FBI issued a warning about scammers using fake remote job ads to steal cryptocurrency from job seekers across the United States while posing as recruiters for legitimate companies. [...]
