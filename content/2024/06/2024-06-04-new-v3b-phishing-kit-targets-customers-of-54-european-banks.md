Title: New V3B phishing kit targets customers of 54 European banks
Date: 2024-06-04T14:53:42-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-04-new-v3b-phishing-kit-targets-customers-of-54-european-banks

[Source](https://www.bleepingcomputer.com/news/security/new-v3b-phishing-kit-targets-customers-of-54-european-banks/){:target="_blank" rel="noopener"}

> Cybercriminals are promoting a new phishing kit named 'V3B' on Telegram, which currently targets customers of 54 major financial institutes in Ireland, the Netherlands, Finland, Austria, Germany, France, Belgium, Greece, Luxembourg, and Italy. [...]
