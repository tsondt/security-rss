Title: New ARM 'TIKTAG' attack impacts Google Chrome, Linux systems
Date: 2024-06-16T10:16:08-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2024-06-16-new-arm-tiktag-attack-impacts-google-chrome-linux-systems

[Source](https://www.bleepingcomputer.com/news/security/new-arm-tiktag-attack-impacts-google-chrome-linux-systems/){:target="_blank" rel="noopener"}

> A new speculative execution attack named "TIKTAG" targets ARM's Memory Tagging Extension (MTE) to leak data with over a 95% chance of success, allowing hackers to bypass the security feature. [...]
