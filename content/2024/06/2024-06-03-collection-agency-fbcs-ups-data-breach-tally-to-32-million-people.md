Title: Collection agency FBCS ups data breach tally to 3.2 million people
Date: 2024-06-03T19:11:22-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-03-collection-agency-fbcs-ups-data-breach-tally-to-32-million-people

[Source](https://www.bleepingcomputer.com/news/security/collection-agency-fbcs-ups-data-breach-tally-to-32-million-people/){:target="_blank" rel="noopener"}

> Debt collection agency Financial Business and Consumer Solutions (FBCS) now says over 3.2 million people have been impacted by a data breach that occurred in February. [...]
