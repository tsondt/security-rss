Title: Japanese vid-sharing site Niconico needs rebuild after cyberattack
Date: 2024-06-11T02:00:29+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-06-11-japanese-vid-sharing-site-niconico-needs-rebuild-after-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/11/niconico_ebiten_kadokawa_cyberattack_outages/){:target="_blank" rel="noopener"}

> Offline for four days and counting, as are parent company and e-commerce brand Japanese media conglomerate Kadokawa and several of its properties have been offline for four days after a major cyber attack.... [...]
