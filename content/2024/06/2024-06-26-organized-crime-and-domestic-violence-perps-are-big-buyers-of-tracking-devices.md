Title: Organized crime and domestic violence perps are big buyers of tracking devices
Date: 2024-06-26T03:45:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-06-26-organized-crime-and-domestic-violence-perps-are-big-buyers-of-tracking-devices

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/26/criminals_use_gps_bluetooth_trackers/){:target="_blank" rel="noopener"}

> Australian study finds GPS trackers – and sometimes AirTags – are in demand for the wrong reasons Tracking devices are in demand from organized crime groups and known perpetrators of domestic violence, according to an Australian study.... [...]
