Title: Seeing Like a Data Structure
Date: 2024-06-03T11:06:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Internet and society
Slug: 2024-06-03-seeing-like-a-data-structure

[Source](https://www.schneier.com/blog/archives/2024/06/seeing-like-a-data-structure.html){:target="_blank" rel="noopener"}

> Technology was once simply a tool—and a small one at that—used to amplify human intent and capacity. That was the story of the industrial revolution: we could control nature and build large, complex human societies, and the more we employed and mastered technology, the better things got. We don’t live in that world anymore. Not only has technology become entangled with the structure of society, but we also can no longer see the world around us without it. The separation is gone, and the control we thought we once had has revealed itself as a mirage. We’re in a transitional [...]
