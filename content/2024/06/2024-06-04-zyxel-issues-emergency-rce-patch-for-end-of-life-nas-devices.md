Title: Zyxel issues emergency RCE patch for end-of-life NAS devices
Date: 2024-06-04T13:28:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-04-zyxel-issues-emergency-rce-patch-for-end-of-life-nas-devices

[Source](https://www.bleepingcomputer.com/news/security/zyxel-issues-emergency-rce-patch-for-end-of-life-nas-devices/){:target="_blank" rel="noopener"}

> Zyxel Networks has released an emergency security update to address three critical vulnerabilities impacting older NAS devices that have reached end-of-life. [...]
