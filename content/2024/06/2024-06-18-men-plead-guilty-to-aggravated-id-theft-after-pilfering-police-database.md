Title: Men plead guilty to aggravated ID theft after pilfering police database
Date: 2024-06-18T20:30:12+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;doxxing;extortion;Identity theft
Slug: 2024-06-18-men-plead-guilty-to-aggravated-id-theft-after-pilfering-police-database

[Source](https://arstechnica.com/?p=2032310){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Two men have pleaded guilty to charges of computer intrusion and aggravated identity theft tied to their theft of records from a law enforcement database for use in doxxing and extorting multiple individuals. Sagar Steven Singh, 20, and Nicholas Ceraolo, 26, admitted to being members of ViLE, a group that specializes in obtaining personal information of individuals and using it to extort or harass them. Members use various methods to collect social security numbers, cell phone numbers, and other personal data and post it, or threaten to post it, to a website administered by the group. [...]
