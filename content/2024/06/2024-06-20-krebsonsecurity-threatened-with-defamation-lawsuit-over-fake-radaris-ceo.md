Title: KrebsOnSecurity Threatened with Defamation Lawsuit Over Fake Radaris CEO
Date: 2024-06-20T19:16:01+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;617-794-0001;Aleksej Gubarev;Andtop Company;Atlas Data Privacy Corp.;Barsky.com;Better Business Bureau;Bitseller Expert Limited;Boston Law Group;BuzzFeed;Constella Intelligence;difive.com;Dmitry Lubarsky;DomainTools.com;Gary Norden;gn@difive.com;Humanbook;Igor Lubarsky;Pavel Kaydash;Radaris;Radaris.com;Steele Dossier;trustoria.com;Val Gurvits;Webzilla;XBT Holding
Slug: 2024-06-20-krebsonsecurity-threatened-with-defamation-lawsuit-over-fake-radaris-ceo

[Source](https://krebsonsecurity.com/2024/06/krebsonsecurity-threatened-with-defamation-lawsuit-over-fake-radaris-ceo/){:target="_blank" rel="noopener"}

> On March 8, 2024, KrebsOnSecurity published a deep dive on the consumer data broker Radaris, showing how the original owners are two men in Massachusetts who operated multiple Russian language dating services and affiliate programs, in addition to a dizzying array of people-search websites. The subjects of that piece are threatening to sue KrebsOnSecurity for defamation unless the story is retracted. Meanwhile, their attorney has admitted that the person Radaris named as the CEO from its inception is a fabricated identity. Radaris is just one cog in a sprawling network of people-search properties online that sell highly detailed background reports [...]
