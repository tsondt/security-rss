Title: Life360 says hacker tried to extort them after Tile data breach
Date: 2024-06-12T12:45:55-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-06-12-life360-says-hacker-tried-to-extort-them-after-tile-data-breach

[Source](https://www.bleepingcomputer.com/news/security/life360-says-hacker-tried-to-extort-them-after-tile-data-breach/){:target="_blank" rel="noopener"}

> Safety and location services company Life360 says it was the target of an extortion attempt after a threat actor breached and stole sensitive information from a Tile customer support platform. [...]
