Title: Rethinking Democracy for the Age of AI
Date: 2024-06-18T11:04:08+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;democracy;essays
Slug: 2024-06-18-rethinking-democracy-for-the-age-of-ai

[Source](https://www.schneier.com/blog/archives/2024/06/rethinking-democracy-for-the-age-of-ai.html){:target="_blank" rel="noopener"}

> There is a lot written about technology’s threats to democracy. Polarization. Artificial intelligence. The concentration of wealth and power. I have a more general story: The political and economic systems of governance that were created in the mid-18th century are poorly suited for the 21st century. They don’t align incentives well. And they are being hacked too effectively. At the same time, the cost of these hacked systems has never been greater, across all human history. We have become too powerful as a species. And our systems cannot keep up with fast-changing disruptive technologies. We need to create new systems [...]
