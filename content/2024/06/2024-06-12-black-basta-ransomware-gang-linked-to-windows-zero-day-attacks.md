Title: Black Basta ransomware gang linked to Windows zero-day attacks
Date: 2024-06-12T06:00:00-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-12-black-basta-ransomware-gang-linked-to-windows-zero-day-attacks

[Source](https://www.bleepingcomputer.com/news/security/black-basta-ransomware-gang-linked-to-windows-zero-day-attacks/){:target="_blank" rel="noopener"}

> The Cardinal cybercrime group (Storm-1811, UNC4394), who are the main operators of the Black Basta ransomware, is suspected of exploiting a Windows privilege escalation vulnerability, CVE-2024-26169, before a fix was made available. [...]
