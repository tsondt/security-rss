Title: Microsoft Research chief scientist has no issue with Windows Recall
Date: 2024-06-06T07:26:08+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-06-06-microsoft-research-chief-scientist-has-no-issue-with-windows-recall

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/06/microsoft_research_recall/){:target="_blank" rel="noopener"}

> As tool emerges to probe OS feature's SQLite-based store of user activities Asked to explore the data privacy issues arising from Microsoft Recall, the Windows maker's poorly received self-surveillance tool, Jaime Teevan, chief scientist and technical fellow at Microsoft Research, brushed aside concerns.... [...]
