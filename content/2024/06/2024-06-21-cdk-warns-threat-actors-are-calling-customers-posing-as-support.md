Title: CDK warns: threat actors are calling customers, posing as support
Date: 2024-06-21T07:00:37-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2024-06-21-cdk-warns-threat-actors-are-calling-customers-posing-as-support

[Source](https://www.bleepingcomputer.com/news/security/cdk-warns-threat-actors-are-calling-customers-posing-as-support/){:target="_blank" rel="noopener"}

> CDK Global has cautioned customers about unscrupulous actors calling them and posing as CDK agents or affiliates to gain unauthorized systems access. The warning follows ongoing cyberattacks that have hit CDK, a software-as-a-service (SaaS) platform that thousands of US car dealerships rely upon. [...]
