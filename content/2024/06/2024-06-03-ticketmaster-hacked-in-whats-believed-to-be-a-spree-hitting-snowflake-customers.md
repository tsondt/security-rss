Title: Ticketmaster hacked in what’s believed to be a spree hitting Snowflake customers
Date: 2024-06-03T22:23:45+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;Data breaches;infostealers;snowflake
Slug: 2024-06-03-ticketmaster-hacked-in-whats-believed-to-be-a-spree-hitting-snowflake-customers

[Source](https://arstechnica.com/?p=2028710){:target="_blank" rel="noopener"}

> Enlarge (credit: Getty Images) Cloud storage provider Snowflake said that accounts belonging to multiple customers have been hacked after threat actors obtained credentials through info-stealing malware or by purchasing them on online crime forums. Ticketmaster parent Live Nation—which disclosed Friday that hackers gained access to data it stored through an unnamed third-party provider— told TechCrunch the provider was Snowflake. The live-event ticket broker said it identified the hack on May 20, and a week later, a “criminal threat actor offered what it alleged to be Company user data for sale via the dark web.” Ticketmaster is one of six Snowflake [...]
