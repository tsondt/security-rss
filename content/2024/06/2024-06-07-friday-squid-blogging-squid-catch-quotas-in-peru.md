Title: Friday Squid Blogging: Squid Catch Quotas in Peru
Date: 2024-06-07T21:05:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2024-06-07-friday-squid-blogging-squid-catch-quotas-in-peru

[Source](https://www.schneier.com/blog/archives/2024/06/friday-squid-blogging-squid-catch-quotas-in-peru.html){:target="_blank" rel="noopener"}

> Peru has set a lower squid quota for 2024. The article says “giant squid,” but that seems wrong. We don’t eat those. As usual, you can also use this squid post to talk about the security stories in the news that I haven’t covered. Read my blog posting guidelines here. [...]
