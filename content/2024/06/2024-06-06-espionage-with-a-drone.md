Title: Espionage with a Drone
Date: 2024-06-06T15:51:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;drones;espionage;national security policy;photos
Slug: 2024-06-06-espionage-with-a-drone

[Source](https://www.schneier.com/blog/archives/2024/06/espionage-with-a-drone.html){:target="_blank" rel="noopener"}

> The US is using a World War II law that bans aircraft photography of military installations to charge someone with doing the same thing with a drone. [...]
