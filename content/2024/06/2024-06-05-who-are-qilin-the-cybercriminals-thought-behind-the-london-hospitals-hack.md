Title: Who are Qilin, the cybercriminals thought behind the London hospitals hack?
Date: 2024-06-05T11:10:35+00:00
Author: Alex Hern Global technology editor
Category: The Guardian
Tags: Cybercrime;Russia;Internet;London;NHS;Data and computer security;Technology;UK news
Slug: 2024-06-05-who-are-qilin-the-cybercriminals-thought-behind-the-london-hospitals-hack

[Source](https://www.theguardian.com/technology/article/2024/jun/05/who-are-qilin-the-cybercriminals-thought-behind-the-london-hospitals-hack){:target="_blank" rel="noopener"}

> Russian-speaking ransomware gang lets hackers use its tools in exchange for cut of proceeds A Russian-speaking ransomware criminal gang called Qilin is thought to be behind the cyber-attack on NHS medical services provider Synnovis, that halted tests and operations at hospital trusts to a halt and affected GPs across London. Although the location of the group is unknown, if it is based in Russia, it will be difficult for British law enforcement to directly target it. The Russian state has long had a ban on extraditing criminals overseas, and since it launched a full-scale invasion of Ukraine, it has largely [...]
