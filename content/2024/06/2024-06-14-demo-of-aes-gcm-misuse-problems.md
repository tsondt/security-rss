Title: Demo of AES GCM Misuse Problems
Date: 2024-06-14T11:05:35+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;AES;algorithms;cryptanalysis;encryption
Slug: 2024-06-14-demo-of-aes-gcm-misuse-problems

[Source](https://www.schneier.com/blog/archives/2024/06/demo-of-aes-gcm-misuse-problems.html){:target="_blank" rel="noopener"}

> This is really neat demo of the security problems arising from reusing nonces with a symmetric cipher in GCM mode. [...]
