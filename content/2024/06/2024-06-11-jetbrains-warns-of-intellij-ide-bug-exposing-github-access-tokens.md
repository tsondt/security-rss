Title: JetBrains warns of IntelliJ IDE bug exposing GitHub access tokens
Date: 2024-06-11T14:59:38-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2024-06-11-jetbrains-warns-of-intellij-ide-bug-exposing-github-access-tokens

[Source](https://www.bleepingcomputer.com/news/security/jetbrains-warns-of-intellij-ide-bug-exposing-github-access-tokens/){:target="_blank" rel="noopener"}

> JetBrains warned customers to patch a critical vulnerability that impacts users of its IntelliJ integrated development environment (IDE) apps and exposes GitHub access tokens. [...]
