Title: Biden bans Kaspersky antivirus software in US over security concerns
Date: 2024-06-20T18:38:58-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Government;Software
Slug: 2024-06-20-biden-bans-kaspersky-antivirus-software-in-us-over-security-concerns

[Source](https://www.bleepingcomputer.com/news/security/biden-bans-kaspersky-antivirus-software-in-us-over-security-concerns/){:target="_blank" rel="noopener"}

> Today, the Biden administration has announced an upcoming ban of Kaspersky antivirus software and the pushing of software updates to US companies and consumers, giving customers until September 29, 2024, to find alternative security software. [...]
