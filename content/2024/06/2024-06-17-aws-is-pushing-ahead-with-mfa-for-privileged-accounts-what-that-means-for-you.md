Title: AWS is pushing ahead with MFA for privileged accounts. What that means for you ...
Date: 2024-06-17T11:18:30+00:00
Author: Jessica Lyons and Chris Williams
Category: The Register
Tags: 
Slug: 2024-06-17-aws-is-pushing-ahead-with-mfa-for-privileged-accounts-what-that-means-for-you

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/17/aws_mfa_roll_out/){:target="_blank" rel="noopener"}

> The clock is ticking – why not try a passkey? Heads up: Amazon Web Services is pushing ahead with making multi-factor authentication (MFA) mandatory for certain users, and we love to see it.... [...]
