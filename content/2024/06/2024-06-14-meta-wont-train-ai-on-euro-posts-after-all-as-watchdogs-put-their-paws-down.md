Title: Meta won't train AI on Euro posts after all, as watchdogs put their paws down
Date: 2024-06-14T20:46:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-14-meta-wont-train-ai-on-euro-posts-after-all-as-watchdogs-put-their-paws-down

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/14/meta_eu_privacy/){:target="_blank" rel="noopener"}

> Facebook parent calls step forward for privacy a 'step backwards' Meta has caved to European regulators, and agreed to pause its plans to train AI models on EU users' Facebook and Instagram users' posts — a move that the social media giant said will delay its plans to launch Meta AI in the economic zone.... [...]
