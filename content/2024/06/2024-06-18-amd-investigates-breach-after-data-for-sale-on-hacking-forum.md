Title: AMD investigates breach after data for sale on hacking forum
Date: 2024-06-18T17:26:26-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-18-amd-investigates-breach-after-data-for-sale-on-hacking-forum

[Source](https://www.bleepingcomputer.com/news/security/amd-investigates-breach-after-data-for-sale-on-hacking-forum/){:target="_blank" rel="noopener"}

> AMD is investigating whether it suffered a cyberattack after a threat actor put allegedly stolen data up for sale on a hacking forum, claiming it contains AMD employee information, financial documents, and confidential information. [...]
