Title: Snowflake breach snowballs as more victims, perps, come forward
Date: 2024-06-24T02:14:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2024-06-24-snowflake-breach-snowballs-as-more-victims-perps-come-forward

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/24/snowflake_breach_accelerating_into_snowball/){:target="_blank" rel="noopener"}

> Also: The leaked Apple internal tools that weren't; TV pirate pirates convicted; and some critical vulns, too Infosec in brief The descending ball of trouble over at Snowflake keeps growing larger, with more victims – and even one of the alleged intruders – coming forward last week.... [...]
