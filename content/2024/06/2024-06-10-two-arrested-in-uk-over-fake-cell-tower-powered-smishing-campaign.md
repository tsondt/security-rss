Title: Two arrested in UK over 'fake cell tower-powered' smishing campaign
Date: 2024-06-10T11:31:00+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-10-two-arrested-in-uk-over-fake-cell-tower-powered-smishing-campaign

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/10/two_arrested_in_uk_over/){:target="_blank" rel="noopener"}

> Thousands of dodgy SMS messages bypassed network filters in UK-first case, it is claimed British police have arrested two individuals following an investigation into a suspected illegal homebrew phone mast used in SMS-based phishing campaigns.... [...]
