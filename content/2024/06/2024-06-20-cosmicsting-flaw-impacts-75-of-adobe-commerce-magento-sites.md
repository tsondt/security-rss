Title: CosmicSting flaw impacts 75% of Adobe Commerce, Magento sites
Date: 2024-06-20T16:02:04-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-20-cosmicsting-flaw-impacts-75-of-adobe-commerce-magento-sites

[Source](https://www.bleepingcomputer.com/news/security/cosmicsting-flaw-impacts-75-percent-of-adobe-commerce-magento-sites/){:target="_blank" rel="noopener"}

> A vulnerability dubbed "CosmicSting" impacting Adobe Commerce and Magento websites remains largely unpatched nine days after the security update has been made available, leaving millions of sites open to catastrophic attacks. [...]
