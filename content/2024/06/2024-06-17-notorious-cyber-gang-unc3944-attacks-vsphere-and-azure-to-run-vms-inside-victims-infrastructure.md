Title: Notorious cyber gang UNC3944 attacks vSphere and Azure to run VMs inside victims' infrastructure
Date: 2024-06-17T06:34:14+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-06-17-notorious-cyber-gang-unc3944-attacks-vsphere-and-azure-to-run-vms-inside-victims-infrastructure

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/17/unc3944_scattered_spider_tactics_change/){:target="_blank" rel="noopener"}

> Who needs ransomware when you can scare techies into coughing up their credentials? Notorious cyber gang UNC3944 – the crew suspected of involvement in the recent attacks on Snowflake and MGM Entertainment, and plenty more besides – has changed its tactics and is now targeting SaaS applications... [...]
