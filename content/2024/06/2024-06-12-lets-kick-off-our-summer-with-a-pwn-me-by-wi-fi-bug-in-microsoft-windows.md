Title: Let's kick off our summer with a pwn-me-by-Wi-Fi bug in Microsoft Windows
Date: 2024-06-12T00:29:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-12-lets-kick-off-our-summer-with-a-pwn-me-by-wi-fi-bug-in-microsoft-windows

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/12/june_patch_tuesday/){:target="_blank" rel="noopener"}

> Redmond splats dozens of bugs as does Adobe while Arm drivers and PHP under active attack Patch Tuesday Microsoft kicked off our summer season with a relatively light June Patch Tuesday, releasing updates for 49 CVE-tagged security flaws in its products – including one bug deemed critical, a fairly terrifying one in wireless networking, and one listed as publicly disclosed.... [...]
