Title: Security Analysis of the EU’s Digital Wallet
Date: 2024-06-27T11:06:32+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;credentials;cryptanalysis;cryptography;EU;identification
Slug: 2024-06-27-security-analysis-of-the-eus-digital-wallet

[Source](https://www.schneier.com/blog/archives/2024/06/security-analysis-of-the-eus-digital-wallet.html){:target="_blank" rel="noopener"}

> A group of cryptographers have analyzed the eiDAS 2.0 regulation (electronic identification and trust services) that defines the new EU Digital Identity Wallet. [...]
