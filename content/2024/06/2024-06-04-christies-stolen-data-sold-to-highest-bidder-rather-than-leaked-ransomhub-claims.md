Title: Christie's stolen data sold to highest bidder rather than leaked, RansomHub claims
Date: 2024-06-04T14:32:51+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-04-christies-stolen-data-sold-to-highest-bidder-rather-than-leaked-ransomhub-claims

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/04/christies_stolen_data_auctioned_off/){:target="_blank" rel="noopener"}

> Experts say auctioning the auctioneer’s data is unlikely to have been genuinely successful The cybercrims who claimed the attack on Christie's fancy themselves as auctioneers as well, after they allegedly sold off the company's data to the highest bidder instead of leaking everything on the dark web.... [...]
