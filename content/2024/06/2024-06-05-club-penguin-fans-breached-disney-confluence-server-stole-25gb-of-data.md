Title: Club Penguin fans breached Disney Confluence server, stole 2.5GB of data
Date: 2024-06-05T16:15:41-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-05-club-penguin-fans-breached-disney-confluence-server-stole-25gb-of-data

[Source](https://www.bleepingcomputer.com/news/security/club-penguin-fans-breached-disney-confluence-server-stole-25gb-of-data/){:target="_blank" rel="noopener"}

> Club Penguin fans hacked a Disney Confluence server to steal information about their favorite game but wound up walking away with 2.5 GB of internal corporate data, BleepingComputer has learned. [...]
