Title: Two cuffed over suspected smishing campaign using 'text message blaster'
Date: 2024-06-10T11:31:00+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-10-two-cuffed-over-suspected-smishing-campaign-using-text-message-blaster

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/10/two_arrested_uk_smishing/){:target="_blank" rel="noopener"}

> Thousands of dodgy SMSes bypassed network filters in UK-first case, it is claimed British police have arrested two individuals following an investigation into an SMS-based phishing campaign using some kind of homebrew hardware.... [...]
