Title: Blackbaud has to cough up a few million dollars more over 2020 ransomware attack
Date: 2024-06-17T17:45:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-17-blackbaud-has-to-cough-up-a-few-million-dollars-more-over-2020-ransomware-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/17/blackbaud_breach_california_settlement/){:target="_blank" rel="noopener"}

> Four years on and it's still paying for what California attorney general calls 'unacceptable' practice Months after escaping without a fine from the US Federal Trade Commission (FTC), the luck of cloud software biz Blackbaud ran out when it came to reaching a settlement with California's attorney general.... [...]
