Title: PHP fixes critical RCE flaw impacting all versions for Windows
Date: 2024-06-07T10:32:19-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-06-07-php-fixes-critical-rce-flaw-impacting-all-versions-for-windows

[Source](https://www.bleepingcomputer.com/news/security/php-fixes-critical-rce-flaw-impacting-all-versions-for-windows/){:target="_blank" rel="noopener"}

> A new PHP for Windows remote code execution (RCE) vulnerability has been disclosed, impacting all releases since version 5.x, potentially impacting a massive number of servers worldwide. [...]
