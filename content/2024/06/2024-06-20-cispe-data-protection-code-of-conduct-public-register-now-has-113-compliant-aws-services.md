Title: CISPE Data Protection Code of Conduct Public Register now has 113 compliant AWS services
Date: 2024-06-20T13:19:50+00:00
Author: Gokhan Akyuz
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;CISPE;Data protection;EU Data Protection;GDPR;Security Blog
Slug: 2024-06-20-cispe-data-protection-code-of-conduct-public-register-now-has-113-compliant-aws-services

[Source](https://aws.amazon.com/blogs/security/cispe-data-protection-code-of-conduct-public-register-now-has-113-compliant-aws-services/){:target="_blank" rel="noopener"}

> We continue to expand the scope of our assurance programs at Amazon Web Services (AWS) and are pleased to announce that 113 services are now certified as compliant with the Cloud Infrastructure Services Providers in Europe (CISPE) Data Protection Code of Conduct. This alignment with the CISPE requirements demonstrates our ongoing commitment to adhere to the heightened expectations for [...]
