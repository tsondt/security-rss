Title: Google named a Leader in the Cybersecurity Incident Response Services Forrester Wave, Q2 2024
Date: 2024-06-11T16:00:00+00:00
Author: Jennifer Guzzetta
Category: GCP Security
Tags: Security & Identity
Slug: 2024-06-11-google-named-a-leader-in-the-cybersecurity-incident-response-services-forrester-wave-q2-2024

[Source](https://cloud.google.com/blog/products/identity-security/google-named-a-leader-in-the-cybersecurity-incident-response-services-forrester-wave-report/){:target="_blank" rel="noopener"}

> Today, we’re pleased to share that Google was named a Leader in The Forrester WaveTM: Cybersecurity Incident Response Services Report, Q2 2024. Forrester identified 14 top vendors in the cybersecurity incident response services space, assessing them on their current offerings, strategy, and market presence. Mandiant, part of Google Cloud, received the highest possible score in 17 of the overall 25 pre-defined criteria areas, including but not limited to incident preparation, recovery, ecosystem collaboration, innovation, and supporting services and offerings. “A Google-boosted Mandiant maintains comprehensive, innovative end-to-end IR services...the combined forces are fueling innovation that allows the firm to support more [...]
