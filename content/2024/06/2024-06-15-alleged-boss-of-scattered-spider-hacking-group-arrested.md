Title: Alleged Boss of ‘Scattered Spider’ Hacking Group Arrested
Date: 2024-06-15T23:40:20+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Data Breaches;Ne'er-Do-Well News;SIM Swapping;Web Fraud 2.0;0ktapus;Caesars;DoorDash;fbi;Group-IB;King Bob;lastpass;Mailchimp;MGM;Murcia Today;Noah Michael Urban;Okta;Scattered Spider;signal;SIM swapping;Sosa;The Com;Tyler Buchanan;VX-Underground
Slug: 2024-06-15-alleged-boss-of-scattered-spider-hacking-group-arrested

[Source](https://krebsonsecurity.com/2024/06/alleged-boss-of-scattered-spider-hacking-group-arrested/){:target="_blank" rel="noopener"}

> A 22-year-old man from the United Kingdom arrested this week in Spain is allegedly the ringleader of Scattered Spider, a cybercrime group suspected of hacking into Twilio, LastPass, DoorDash, Mailchimp, and nearly 130 other organizations over the past two years. The Spanish daily Murcia Today reports the suspect was wanted by the FBI and arrested in Palma de Mallorca as he tried to board a flight to Italy. A still frame from a video released by the Spanish national police shows Tylerb in custody at the airport. “He stands accused of hacking into corporate accounts and stealing critical information, which [...]
