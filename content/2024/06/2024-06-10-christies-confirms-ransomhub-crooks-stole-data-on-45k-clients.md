Title: Christie's confirms RansomHub crooks stole data on 45K clients
Date: 2024-06-10T17:00:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-10-christies-confirms-ransomhub-crooks-stole-data-on-45k-clients

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/10/christies_clients_data_stolen/){:target="_blank" rel="noopener"}

> A far cry from the half-million claim that crims originally boasted Auction house to the wealthy Christie's says 45,798 people were affected by its recent cyberattack and resulting data theft.... [...]
