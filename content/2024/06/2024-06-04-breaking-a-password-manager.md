Title: Breaking a Password Manager
Date: 2024-06-04T11:08:16+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;bitcoin;cryptocurrency;passwords
Slug: 2024-06-04-breaking-a-password-manager

[Source](https://www.schneier.com/blog/archives/2024/06/breaking-a-password-manager.html){:target="_blank" rel="noopener"}

> Interesting story of breaking the security of the RoboForm password manager in order to recover a cryptocurrency wallet password. Grand and Bruno spent months reverse engineering the version of the RoboForm program that they thought Michael had used in 2013 and found that the pseudo-random number generator used to generate passwords in that version­and subsequent versions until 2015­did indeed have a significant flaw that made the random number generator not so random. The RoboForm program unwisely tied the random passwords it generated to the date and time on the user’s computer­it determined the computer’s date and time, and then generated [...]
