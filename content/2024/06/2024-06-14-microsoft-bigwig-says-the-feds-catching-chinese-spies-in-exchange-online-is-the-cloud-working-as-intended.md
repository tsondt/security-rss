Title: Microsoft bigwig says the Feds catching Chinese spies in Exchange Online is the cloud working as intended
Date: 2024-06-14T00:40:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-14-microsoft-bigwig-says-the-feds-catching-chinese-spies-in-exchange-online-is-the-cloud-working-as-intended

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/14/brad_smith_microsoft_hearing/){:target="_blank" rel="noopener"}

> 'It's not our job to find the culprits – That's what we're paying you for' lawmaker scolds Brad Smith Lawmakers on Thursday grilled Microsoft president Brad Smith about the Windows giant's businesses dealing in China — and the super-corp's repeated security failings — at a time when Beijing-backed spies are accused of breaking into Microsoft-hosted email accounts of American government officials.... [...]
