Title: Crooks threaten to leak 3B personal records 'stolen from background check firm'
Date: 2024-06-03T19:36:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-03-crooks-threaten-to-leak-3b-personal-records-stolen-from-background-check-firm

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/03/usdod_data_dump/){:target="_blank" rel="noopener"}

> Turns out opting out actually works? Billions of records detailing people's personal information may soon be dumped online after being allegedly obtained from a Florida firm that handles background checks and other requests for folks' private info.... [...]
