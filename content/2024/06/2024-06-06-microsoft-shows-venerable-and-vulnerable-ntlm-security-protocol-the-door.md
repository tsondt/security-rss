Title: Microsoft shows venerable and vulnerable NTLM security protocol the door
Date: 2024-06-06T12:00:05+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-06-06-microsoft-shows-venerable-and-vulnerable-ntlm-security-protocol-the-door

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/06/microsoft_deprecates_ntlm/){:target="_blank" rel="noopener"}

> Time to get moving if you still rely on this deprecated feature Microsoft has finally decided to add the venerable NTLM authentication protocol to the Deprecated Features list.... [...]
