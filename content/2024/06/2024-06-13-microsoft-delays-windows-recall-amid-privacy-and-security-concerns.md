Title: Microsoft delays Windows Recall amid privacy and security concerns
Date: 2024-06-13T22:11:39-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2024-06-13-microsoft-delays-windows-recall-amid-privacy-and-security-concerns

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-delays-windows-recall-amid-privacy-and-security-concerns/){:target="_blank" rel="noopener"}

> Microsoft is delaying the release of its AI-powered Windows Recall feature to test and secure it further before releasing it in a public preview on Copilot+ PCs. [...]
