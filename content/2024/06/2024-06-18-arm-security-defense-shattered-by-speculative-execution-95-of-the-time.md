Title: Arm security defense shattered by speculative execution 95% of the time
Date: 2024-06-18T01:11:09+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-06-18-arm-security-defense-shattered-by-speculative-execution-95-of-the-time

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/18/arm_memory_tag_extensions_leak/){:target="_blank" rel="noopener"}

> 'TikTag' security folks find anti-exploit mechanism rather fragile In 2018, chip designer Arm introduced a hardware security feature called Memory Tagging Extensions (MTE) as a defense against memory safety bugs. But it may not be as effective as first hoped.... [...]
