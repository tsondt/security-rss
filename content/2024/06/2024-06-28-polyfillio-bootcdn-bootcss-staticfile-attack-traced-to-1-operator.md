Title: Polyfill.io, BootCDN, Bootcss, Staticfile attack traced to 1 operator
Date: 2024-06-28T09:00:41-04:00
Author: Ax Sharma
Category: BleepingComputer
Tags: Security
Slug: 2024-06-28-polyfillio-bootcdn-bootcss-staticfile-attack-traced-to-1-operator

[Source](https://www.bleepingcomputer.com/news/security/polyfillio-bootcdn-bootcss-staticfile-attack-traced-to-1-operator/){:target="_blank" rel="noopener"}

> The recent large scale supply chain attack conducted via multiple CDNs, namely Polyfill.io, BootCDN, Bootcss, and Staticfile that affected up to tens of millions of websites has been traced to a common operator. Researchers discovered a public GitHub repository with leaked API keys helping them draw a conclusion. [...]
