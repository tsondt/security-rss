Title: Linux version of TargetCompany ransomware focuses on VMware ESXi
Date: 2024-06-05T19:17:28-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-05-linux-version-of-targetcompany-ransomware-focuses-on-vmware-esxi

[Source](https://www.bleepingcomputer.com/news/security/linux-version-of-targetcompany-ransomware-focuses-on-vmware-esxi/){:target="_blank" rel="noopener"}

> Researchers observed a new Linux variant of the TargetCompany ransomware family that targets VMware ESXi environments using a custom shell script to deliver and execute payloads. [...]
