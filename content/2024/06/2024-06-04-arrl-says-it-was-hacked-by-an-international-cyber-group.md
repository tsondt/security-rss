Title: ARRL says it was hacked by an "international cyber group"
Date: 2024-06-04T16:19:27-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-04-arrl-says-it-was-hacked-by-an-international-cyber-group

[Source](https://www.bleepingcomputer.com/news/security/american-radio-relay-league-says-it-was-hacked-by-an-international-cyber-group/){:target="_blank" rel="noopener"}

> American Radio Relay League (ARRL) has shared more information about a May cyberattack that took its Logbook of the World offline and caused some members to become frustrated over the lack of information. [...]
