Title: What is RansomHub? Looks like a Knight ransomware reboot
Date: 2024-06-05T20:13:05+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-05-what-is-ransomhub-looks-like-a-knight-ransomware-reboot

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/05/ransomhub_knight_reboot/){:target="_blank" rel="noopener"}

> Malware code potentially sold off, tweaked, back at it infecting victims RansomHub, a newish cyber-crime operation that has claimed to be behind the theft of data from Christie's auction house and others, is "very likely" some kind of rebrand of the Knight ransomware gang, according to threat hunters.... [...]
