Title: Batten down the hatches, it's time to patch some more MOVEit bugs
Date: 2024-06-26T13:32:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-26-batten-down-the-hatches-its-time-to-patch-some-more-moveit-bugs

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/26/batten_down_the_hatches_its/){:target="_blank" rel="noopener"}

> Exploit attempts for ‘devastating’ vulnerabilities already underway Thought last year's MOVEit hellscape was well and truly behind you? Unlucky, buster. We're back for round two after Progress Software lifted the lid on fresh vulnerabilities affecting MOVEit Transfer and Gateway.... [...]
