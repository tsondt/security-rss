Title: Driving forward in Android drivers
Date: 2024-06-13T11:03:00-07:00
Author: Google Project Zero (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2024-06-13-driving-forward-in-android-drivers

[Source](https://googleprojectzero.blogspot.com/2024/06/driving-forward-in-android-drivers.html){:target="_blank" rel="noopener"}

> Posted by Seth Jenkins, Google Project Zero Introduction Android's open-source ecosystem has led to an incredible diversity of manufacturers and vendors developing software that runs on a broad variety of hardware. This hardware requires supporting drivers, meaning that many different codebases carry the potential to compromise a significant segment of Android phones. There are recent public examples of third-party drivers containing serious vulnerabilities that are exploited on Android. While there exists a well-established body of public ( and In-the-Wild ) security research on Android GPU drivers, other chipset components may not be as frequently audited so this research sought to [...]
