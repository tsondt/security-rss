Title: AWS HITRUST Shared Responsibility Matrix v1.4.3 for HITRUST CSF v11.3 now available
Date: 2024-06-18T13:17:20+00:00
Author: Mark Weech
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Auditing;AWS HITRUST;AWS HITRUST Inheritance;Compliance reports;Security Assurance;Security Blog
Slug: 2024-06-18-aws-hitrust-shared-responsibility-matrix-v143-for-hitrust-csf-v113-now-available

[Source](https://aws.amazon.com/blogs/security/aws-hitrust-shared-responsibility-matrix-v1-4-3-for-hitrust-csf-v11-3-now-available/){:target="_blank" rel="noopener"}

> The latest version of the AWS HITRUST Shared Responsibility Matrix (SRM)—SRM version 1.4.3—is now available. To request a copy, choose SRM version 1.4.3 from the HITRUST website. SRM version 1.4.3 adds support for the HITRUST Common Security Framework (CSF) v11.3 assessments in addition to continued support for previous versions of HITRUST CSF assessments v9.1–v11.2. As [...]
