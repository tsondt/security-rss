Title: New Warmcookie Windows backdoor pushed via fake job offers
Date: 2024-06-11T11:17:18-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-11-new-warmcookie-windows-backdoor-pushed-via-fake-job-offers

[Source](https://www.bleepingcomputer.com/news/security/new-warmcookie-windows-backdoor-pushed-via-fake-job-offers/){:target="_blank" rel="noopener"}

> A never-before-seen Windows malware named 'Warmcookie' is distributed through fake job offer phishing campaigns to breach corporate networks. [...]
