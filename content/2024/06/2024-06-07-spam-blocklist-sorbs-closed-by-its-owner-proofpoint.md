Title: Spam blocklist SORBS closed by its owner, Proofpoint
Date: 2024-06-07T06:27:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-06-07-spam-blocklist-sorbs-closed-by-its-owner-proofpoint

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/07/sorbs_closed/){:target="_blank" rel="noopener"}

> Spammers will probably bid to buy it, so community is trying to find a better home for decades-old service Exclusive The Spam and Open Relay Blocking System (SORBS) – a longstanding source of info on known sources of spam widely used to create blocklists – has been shuttered by its owner, cyber security software vendor Proofpoint.... [...]
