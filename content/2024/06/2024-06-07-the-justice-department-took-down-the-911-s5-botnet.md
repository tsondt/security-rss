Title: The Justice Department Took Down the 911 S5 Botnet
Date: 2024-06-07T11:04:46+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;botnets;courts;COVID-19;crime;cybercrime;malware
Slug: 2024-06-07-the-justice-department-took-down-the-911-s5-botnet

[Source](https://www.schneier.com/blog/archives/2024/06/the-justice-department-took-down-the-911-s5-botnet.html){:target="_blank" rel="noopener"}

> The US Justice Department has dismantled an enormous botnet: According to an indictment unsealed on May 24, from 2014 through July 2022, Wang and others are alleged to have created and disseminated malware to compromise and amass a network of millions of residential Windows computers worldwide. These devices were associated with more than 19 million unique IP addresses, including 613,841 IP addresses located in the United States. Wang then generated millions of dollars by offering cybercriminals access to these infected IP addresses for a fee. [...] This operation was a coordinated multiagency effort led by law enforcement in the United [...]
