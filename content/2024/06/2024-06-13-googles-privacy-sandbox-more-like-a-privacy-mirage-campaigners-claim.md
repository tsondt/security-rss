Title: Google's Privacy Sandbox more like a privacy mirage, campaigners claim
Date: 2024-06-13T13:30:09+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2024-06-13-googles-privacy-sandbox-more-like-a-privacy-mirage-campaigners-claim

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/13/noyb_gdpr_privacy_sandbox/){:target="_blank" rel="noopener"}

> Chocolate Factory accused of misleading Chrome browser users Updated Privacy campaigner noyb has filed a GDPR complaint regarding Google's Privacy Sandbox, alleging that turning on a "Privacy Feature" in the Chrome browser resulted in unwanted tracking by the US megacorp.... [...]
