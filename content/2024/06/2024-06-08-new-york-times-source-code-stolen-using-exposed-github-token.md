Title: New York Times source code stolen using exposed GitHub token
Date: 2024-06-08T13:10:46-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-08-new-york-times-source-code-stolen-using-exposed-github-token

[Source](https://www.bleepingcomputer.com/news/security/new-york-times-source-code-stolen-using-exposed-github-token/){:target="_blank" rel="noopener"}

> Internal source code and data belonging to The New York Times was leaked on the 4chan message board after being stolen from the company's GitHub repositories in January 2024, The Times confirmed to BleepingComputer. [...]
