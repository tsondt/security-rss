Title: Car dealer software bigshot CDK pulls systems offline twice amid 'cyber incident'
Date: 2024-06-20T20:50:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-20-car-dealer-software-bigshot-cdk-pulls-systems-offline-twice-amid-cyber-incident

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/20/cdk_global_offline/){:target="_blank" rel="noopener"}

> Downtime set to crash into next week The vendor behind the software on which nearly 15,000 car dealerships across the US rely says an ongoing "cyber incident" has forced it to pull systems offline for a second time in as many days.... [...]
