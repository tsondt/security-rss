Title: Phoenix UEFI flaw puts long list of Intel chips in hot seat
Date: 2024-06-21T16:27:05+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-21-phoenix-uefi-flaw-puts-long-list-of-intel-chips-in-hot-seat

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/21/uefi_vulnerability_intel_chips/){:target="_blank" rel="noopener"}

> Researchers discuss it in same breath as BlackLotus and MosaicRegressor A new vulnerability in UEFI firmware is threatening the security of a wide range of Intel chip families in a similar fashion to BlackLotus and others like it.... [...]
