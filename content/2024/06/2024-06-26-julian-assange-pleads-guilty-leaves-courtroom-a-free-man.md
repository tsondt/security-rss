Title: Julian Assange pleads guilty, leaves courtroom a free man
Date: 2024-06-26T07:02:45+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2024-06-26-julian-assange-pleads-guilty-leaves-courtroom-a-free-man

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/26/assange_pleads_guilty_sentenced_freed/){:target="_blank" rel="noopener"}

> Now, about that bill for the private jet that's taking him home to Australia... Julian Assange is a free man.... [...]
