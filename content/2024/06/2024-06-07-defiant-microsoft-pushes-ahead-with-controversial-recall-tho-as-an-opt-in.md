Title: Defiant Microsoft pushes ahead with controversial Recall – tho as an opt-in
Date: 2024-06-07T19:40:54+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-06-07-defiant-microsoft-pushes-ahead-with-controversial-recall-tho-as-an-opt-in

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/07/microsoft_recall_changes/){:target="_blank" rel="noopener"}

> Windows maker acknowledges 'clear signal' from everyone, then mostly ignores it Microsoft is not giving up on its controversial Windows Recall, though says it will give customers an option to opt in instead of having it on by default, and will beef up the security of any data the software stores.... [...]
