Title: Police arrest Conti and LockBit ransomware crypter specialist
Date: 2024-06-12T09:42:11-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2024-06-12-police-arrest-conti-and-lockbit-ransomware-crypter-specialist

[Source](https://www.bleepingcomputer.com/news/security/police-arrest-conti-and-lockbit-ransomware-crypter-specialist/){:target="_blank" rel="noopener"}

> The Ukraine cyber police have arrested a 28-year-old Russian man in Kyiv for working with Conti and LockBit ransomware operations to make their malware undetectable by antivirus software and conducting at least one attack himself. [...]
