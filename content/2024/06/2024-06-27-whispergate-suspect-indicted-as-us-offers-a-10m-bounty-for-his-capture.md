Title: WhisperGate suspect indicted as US offers a $10M bounty for his capture
Date: 2024-06-27T00:33:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-27-whispergate-suspect-indicted-as-us-offers-a-10m-bounty-for-his-capture

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/27/whispergate_malware_suspect_bounty/){:target="_blank" rel="noopener"}

> Russian national accused of attacks in lead-up to the Ukraine war The US Department of Justice has indicted a 22-year-old Russian for allegedly attacking Ukrainian government computers and destroying critical infrastructure systems in the so-called “WhisperGate” wiper attack that preceded Russia's illegal invasion of the European nation.... [...]
