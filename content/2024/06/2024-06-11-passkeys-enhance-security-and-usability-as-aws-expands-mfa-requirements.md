Title: Passkeys enhance security and usability as AWS expands MFA requirements
Date: 2024-06-11T14:30:19+00:00
Author: Arynn Crow
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS IAM;MFA;multi-factor authentication;Security;Security Blog
Slug: 2024-06-11-passkeys-enhance-security-and-usability-as-aws-expands-mfa-requirements

[Source](https://aws.amazon.com/blogs/security/passkeys-enhance-security-and-usability-as-aws-expands-mfa-requirements/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is designed to be the most secure place for customers to run their workloads. From day one, we pioneered secure by design and secure by default practices in the cloud. Today, we’re taking another step to enhance our customers’ options for strong authentication by launching support for FIDO2 passkeys as a [...]
