Title: Kaspersky releases free tool that scans Linux for known threats
Date: 2024-06-01T11:17:34-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Software;Linux;Security
Slug: 2024-06-01-kaspersky-releases-free-tool-that-scans-linux-for-known-threats

[Source](https://www.bleepingcomputer.com/news/software/kaspersky-releases-free-tool-that-scans-linux-for-known-threats/){:target="_blank" rel="noopener"}

> Kaspersky has released a new virus removal tool named KVRT for the Linux platform, allowing users to scan their systems and remove malware and other known threats for free. [...]
