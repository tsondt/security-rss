Title: London hospitals declare emergency following ransomware attack
Date: 2024-06-04T21:16:20+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;hospitals;ransomware
Slug: 2024-06-04-london-hospitals-declare-emergency-following-ransomware-attack

[Source](https://arstechnica.com/?p=2029003){:target="_blank" rel="noopener"}

> Enlarge A ransomware attack that crippled a London-based medical testing and diagnostics provider has led several major hospitals in the city to declare a critical incident emergency and cancel non-emergency surgeries and pathology appointments, it was widely reported Tuesday. The attack was detected Monday against Synnovis, a supplier of blood tests, swabs, bowel tests, and other hospital services in six London boroughs. The company said it has "affected all Synnovis IT systems, resulting in interruptions to many of our pathology services." The company gave no estimate of when its systems would be restored and provided no details about the attack [...]
