Title: Ascension hacked after employee downloaded malicious file
Date: 2024-06-13T17:52:51-04:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-06-13-ascension-hacked-after-employee-downloaded-malicious-file

[Source](https://www.bleepingcomputer.com/news/security/ascension-hacked-after-employee-downloaded-malicious-file/){:target="_blank" rel="noopener"}

> Ascension, one of the largest U.S. healthcare systems, revealed that a May 2024 ransomware attack was caused by an employee who downloaded a malicious file onto a company device. [...]
