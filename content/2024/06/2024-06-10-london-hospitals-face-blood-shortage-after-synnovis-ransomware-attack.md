Title: London hospitals face blood shortage after Synnovis ransomware attack
Date: 2024-06-10T11:43:11-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2024-06-10-london-hospitals-face-blood-shortage-after-synnovis-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/london-hospitals-face-blood-shortage-after-synnovis-ransomware-attack/){:target="_blank" rel="noopener"}

> England's NHS Blood and Transplant (NHSBT) has issued an urgent call to O Positive and O Negative blood donors to book appointments and donate after last week's cyberattack on pathology provider Synnovis impacted multiple hospitals in London. [...]
