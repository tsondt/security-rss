Title: New phishing toolkit uses PWAs to steal login credentials
Date: 2024-06-12T13:35:26-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-12-new-phishing-toolkit-uses-pwas-to-steal-login-credentials

[Source](https://www.bleepingcomputer.com/news/security/new-phishing-toolkit-uses-pwas-to-steal-login-credentials/){:target="_blank" rel="noopener"}

> A new phishing kit has been released that allows red teamers and cybercriminals to create progressive web Apps (PWAs) that display convincing corporate login forms to steal credentials. [...]
