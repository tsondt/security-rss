Title: 361 million stolen accounts leaked on Telegram added to HIBP
Date: 2024-06-03T15:47:47-04:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2024-06-03-361-million-stolen-accounts-leaked-on-telegram-added-to-hibp

[Source](https://www.bleepingcomputer.com/news/security/361-million-stolen-accounts-leaked-on-telegram-added-to-hibp/){:target="_blank" rel="noopener"}

> A massive trove of 361 million email addresses from credentials stolen by password-stealing malware, in credential stuffing attacks, and from data breaches was added to the Have I Been Pwned data breach notification service, allowing anyone to check if their accounts have been compromised. [...]
