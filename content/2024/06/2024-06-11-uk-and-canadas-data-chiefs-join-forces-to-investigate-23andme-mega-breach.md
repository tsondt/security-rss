Title: UK and Canada's data chiefs join forces to investigate 23andMe mega-breach
Date: 2024-06-11T13:30:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2024-06-11-uk-and-canadas-data-chiefs-join-forces-to-investigate-23andme-mega-breach

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/11/uk_and_canada_23andme_probe/){:target="_blank" rel="noopener"}

> Three-pronged approach aims to uncover any malpractice at the Silicon Valley biotech biz The data protection watchdogs of the UK and Canada are teaming up to hunt down the facts behind last year's 23andMe data breach.... [...]
