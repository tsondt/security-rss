Title: Hudson Rock yanks report fingering Snowflake employee creds snafu for mega-leak
Date: 2024-06-04T02:25:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-04-hudson-rock-yanks-report-fingering-snowflake-employee-creds-snafu-for-mega-leak

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/04/snowflake_report_pulled/){:target="_blank" rel="noopener"}

> Cloud storage giant lawyers up against infosec house Analysis Hudson Rock, citing legal pressure from Snowflake, has removed its online report that claimed miscreants broke into the cloud storage and analytics giant's underlying systems and stole data from potentially hundreds of customers including Ticketmaster and Santander Bank.... [...]
