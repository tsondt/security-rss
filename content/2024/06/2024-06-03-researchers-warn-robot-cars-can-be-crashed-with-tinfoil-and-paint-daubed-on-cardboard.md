Title: Researchers warn robot cars can be crashed with tinfoil and paint daubed on cardboard
Date: 2024-06-03T05:48:18+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-06-03-researchers-warn-robot-cars-can-be-crashed-with-tinfoil-and-paint-daubed-on-cardboard

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/03/baidu_robotaxi_attack/){:target="_blank" rel="noopener"}

> Use Baidu's platform to show how the fusion of Lidar, radar, and cameras can be fooled by stuff from your kids' craft box A team of researchers from prominent universities – including SUNY Buffalo, Iowa State, UNC Charlotte, and Purdue – were able to turn an autonomous vehicle (AV) operated on the open sourced Apollo driving platform from Chinese web giant Baidu into a deadly weapon by tricking its multi-sensor fusion system, and suggest the attack could be applied to other self-driving cars.... [...]
