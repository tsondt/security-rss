Title: Uber ex-CSO Joe Sullivan: We need security leaders running to work, not giving up
Date: 2024-06-08T14:40:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2024-06-08-uber-ex-cso-joe-sullivan-we-need-security-leaders-running-to-work-not-giving-up

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/08/uber_cso_joe_sullivan/){:target="_blank" rel="noopener"}

> Lessons learned from the infosec chief convicted and punished for covering up theft of data from taxi app maker Interview Joe Sullivan – the now-former Uber chief security officer who was found guilty of covering-up a theft of data from Uber in 2016 – remembers sitting down and thinking through the worst-case scenarios he faced following that guilty verdict in 2022.... [...]
