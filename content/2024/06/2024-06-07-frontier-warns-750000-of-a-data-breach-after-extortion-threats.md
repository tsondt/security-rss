Title: Frontier warns 750,000 of a data breach after extortion threats
Date: 2024-06-07T14:45:26-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-07-frontier-warns-750000-of-a-data-breach-after-extortion-threats

[Source](https://www.bleepingcomputer.com/news/security/frontier-warns-750-000-of-a-data-breach-after-extorted-by-ransomhub/){:target="_blank" rel="noopener"}

> Frontier Communications is warning 750,000 customers that their information was exposed in a data breach after an April cyberattack claimed by the RansomHub ransomware operation. [...]
