Title: ONNX phishing service targets Microsoft 365 accounts at financial firms
Date: 2024-06-18T16:28:20-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2024-06-18-onnx-phishing-service-targets-microsoft-365-accounts-at-financial-firms

[Source](https://www.bleepingcomputer.com/news/security/onnx-phishing-service-targets-microsoft-365-accounts-at-financial-firms/){:target="_blank" rel="noopener"}

> A new phishing-as-a-service (PhaaS) platform called ONNX Store is targeting Microsoft 365 accounts for employees at financial firms using QR codes in PDF attachments. [...]
