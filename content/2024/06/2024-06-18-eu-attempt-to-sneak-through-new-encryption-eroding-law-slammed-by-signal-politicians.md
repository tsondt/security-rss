Title: EU attempt to sneak through new encryption-eroding law slammed by Signal, politicians
Date: 2024-06-18T22:22:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2024-06-18-eu-attempt-to-sneak-through-new-encryption-eroding-law-slammed-by-signal-politicians

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/18/signal_eu_upload_moderation/){:target="_blank" rel="noopener"}

> If you call 'client-side scanning' something like 'upload moderation,' it still undermines privacy, security On Thursday, the EU Council is scheduled to vote on a legislative proposal that would attempt to protect children online by disallowing confidential communication.... [...]
