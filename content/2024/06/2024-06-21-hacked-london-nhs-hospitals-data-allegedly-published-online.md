Title: Hacked London NHS hospitals data allegedly published online
Date: 2024-06-21T11:02:58+00:00
Author: Geneva Abdul and Dan Milmo
Category: The Guardian
Tags: NHS;Cybercrime;Health;Internet;Society;Technology;UK news;England;Hospitals;Data and computer security;Data protection;London
Slug: 2024-06-21-hacked-london-nhs-hospitals-data-allegedly-published-online

[Source](https://www.theguardian.com/society/article/2024/jun/21/hacked-london-nhs-hospitals-data-allegedly-published-online){:target="_blank" rel="noopener"}

> Cyber-attack earlier this month led to cancellation of almost 1,600 operations and outpatient appointments Data from a ransomware attack has allegedly been published online weeks after the attack halted operations and tests in major London hospitals, NHS England has said. A Russian group carried out the cyber-attack on Synnovis, a private pathology firm that analyses blood tests for Guy’s and St Thomas’ NHS foundation trust (GSTT) and King’s College trust, on 3 June, forcing hospitals in the capital to cancel almost 1,600 operations and outpatient appointments. Continue reading... [...]
