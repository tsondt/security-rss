Title: How Confidential Accelerators can boost AI workload security
Date: 2024-06-17T16:00:00+00:00
Author: Sam Lugani
Category: GCP Security
Tags: Security & Identity
Slug: 2024-06-17-how-confidential-accelerators-can-boost-ai-workload-security

[Source](https://cloud.google.com/blog/products/identity-security/how-confidential-accelerators-can-boost-ai-workload-security/){:target="_blank" rel="noopener"}

> As artificial intelligence and machine learning workloads become more popular, it's important to secure them with specialized data security measures. Confidential Computing can help protect sensitive data used in ML training to maintain the privacy of user prompts and AI/ML models during inference and enable secure collaboration during model creation. At Google Cloud Next, we announced two new Confidential Computing offerings specifically designed to protect the privacy and confidentiality of AI/ML workloads: Confidential VMs powered by NVIDIA H100 Tensor Core GPUs with HGX protected PCIe, and Confidential VMs with Intel Advanced Matrix Extensions (Intel AMX) support. A3 Confidential VMs: Securing [...]
