Title: Microsoft: New Outlook security changes coming to personal accounts
Date: 2024-06-15T10:12:24-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2024-06-15-microsoft-new-outlook-security-changes-coming-to-personal-accounts

[Source](https://www.bleepingcomputer.com/news/security/microsoft-new-outlook-security-changes-coming-to-personal-accounts/){:target="_blank" rel="noopener"}

> Microsoft has announced new cybersecurity enhancements for Outlook personal email accounts as part of its 'Secure Future Initiative,' including the deprecation of basic authentication (username + password) by September 16, 2024. [...]
