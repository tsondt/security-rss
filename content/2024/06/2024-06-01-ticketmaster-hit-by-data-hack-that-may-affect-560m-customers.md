Title: Ticketmaster hit by data hack that may affect 560m customers
Date: 2024-06-01T13:52:17+00:00
Author: Sammy Gecsoyler and agency
Category: The Guardian
Tags: Cybercrime;Hacking;Technology;Data and computer security;US news;Securities and Exchange Commission;Business
Slug: 2024-06-01-ticketmaster-hit-by-data-hack-that-may-affect-560m-customers

[Source](https://www.theguardian.com/technology/article/2024/jun/01/live-nation-investigating-data-breach-of-its-us-ticketmaster-unit){:target="_blank" rel="noopener"}

> Cybercrime group ShinyHunters reportedly demanding £400,000 ransom to prevent data being sold Ticketmaster has been targeted in a cyber-attack, with hackers allegedly offering to sell customer data on the dark web, its parent company, Live Nation, has confirmed. The ShinyHunters hacking group is reportedly demanding about £400,000 in a ransom payment to prevent the data being sold. Continue reading... [...]
