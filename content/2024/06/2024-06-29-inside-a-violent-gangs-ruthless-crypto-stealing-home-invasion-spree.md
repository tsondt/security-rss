Title: Inside a violent gang’s ruthless crypto-stealing home invasion spree
Date: 2024-06-29T10:55:08+00:00
Author: WIRED
Category: Ars Technica
Tags: Biz & IT;Security;cryptocurrency;extortion;syndication
Slug: 2024-06-29-inside-a-violent-gangs-ruthless-crypto-stealing-home-invasion-spree

[Source](https://arstechnica.com/?p=2034420){:target="_blank" rel="noopener"}

> Enlarge (credit: Malte Mueller / Getty ) Cryptocurrency has always made a ripe target for theft —and not just hacking, but the old-fashioned, up-close-and-personal kind, too. Given that it can be irreversibly transferred in seconds with little more than a password, it's perhaps no surprise that thieves have occasionally sought to steal crypto in home-invasion burglaries and even kidnappings. But rarely do those thieves leave a trail of violence in their wake as disturbing as that of one recent, ruthless, and particularly prolific gang of crypto extortionists. The United States Justice Department earlier this week announced the conviction of Remy [...]
