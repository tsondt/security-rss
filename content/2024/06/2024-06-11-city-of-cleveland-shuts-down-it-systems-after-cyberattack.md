Title: City of Cleveland shuts down IT systems after cyberattack
Date: 2024-06-11T12:54:44-04:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government
Slug: 2024-06-11-city-of-cleveland-shuts-down-it-systems-after-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/city-of-cleveland-shuts-down-it-systems-after-cyberattack/){:target="_blank" rel="noopener"}

> The City of Cleveland, Ohio, is currently dealing with a cyberattack that has forced it to take citizen-facing services offline, including the public offices and facilities at Erieview and the City Hall. [...]
