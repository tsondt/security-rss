Title: US lawmakers wave red flags over Chinese drone dominance
Date: 2024-06-27T13:44:10+00:00
Author: Laura Dobberstein
Category: The Register
Tags: 
Slug: 2024-06-27-us-lawmakers-wave-red-flags-over-chinese-drone-dominance

[Source](https://go.theregister.com/feed/www.theregister.com/2024/06/27/congress_china_drones/){:target="_blank" rel="noopener"}

> Congressman warns tech is getting the 'Huawei Playbook' treatment US Congress members warned against Chinese dominance of the drone industry on Wednesday, elevating the threat posed by Beijing's control of the technology as similar to that of semiconductors and ships.... [...]
