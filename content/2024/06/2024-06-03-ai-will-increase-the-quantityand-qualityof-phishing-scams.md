Title: AI Will Increase the Quantity—and Quality—of Phishing Scams
Date: 2024-06-03T11:04:18+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;artificial intelligence;LLM;phishing
Slug: 2024-06-03-ai-will-increase-the-quantityand-qualityof-phishing-scams

[Source](https://www.schneier.com/blog/archives/2024/06/ai-will-increase-the-quantity-and-quality-of-phishing-scams.html){:target="_blank" rel="noopener"}

> A piece I coauthored with Fredrik Heiding and Arun Vishwanath in the Harvard Business Review : Summary. Gen AI tools are rapidly making these emails more advanced, harder to spot, and significantly more dangerous. Recent research showed that 60% of participants fell victim to artificial intelligence (AI)-automated phishing, which is comparable to the success rates of non-AI-phishing messages created by human experts. Companies need to: 1) understand the asymmetrical capabilities of AI-enhanced phishing, 2) determine the company or division’s phishing threat severity level, and 3) confirm their current phishing awareness routines. Here’s the full text. [...]
