Title: Chinese hackers targeted sanctions office in Treasury attack
Date: 2025-01-02T13:09:03-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-02-chinese-hackers-targeted-sanctions-office-in-treasury-attack

[Source](https://www.bleepingcomputer.com/news/security/chinese-hackers-targeted-sanctions-office-in-treasury-attack/){:target="_blank" rel="noopener"}

> ​Chinese state-backed hackers have reportedly breached the Office of Foreign Assets Control (OFAC), a Treasury Department office that administers and enforces trade and economic sanctions programs. [...]
