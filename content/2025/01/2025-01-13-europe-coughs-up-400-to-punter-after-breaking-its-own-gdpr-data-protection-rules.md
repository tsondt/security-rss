Title: Europe coughs up €400 to punter after breaking its own GDPR data protection rules
Date: 2025-01-13T05:27:10+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2025-01-13-europe-coughs-up-400-to-punter-after-breaking-its-own-gdpr-data-protection-rules

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/13/data_broker_hacked/){:target="_blank" rel="noopener"}

> PLUS: Data broker leak reveals extent of info trading; Hot new ransomware gang might be all AI, no bark; and more Infosec in brief Gravy Analytics, a vendor of location intelligence info for marketers which reached a settlement with US authorities last year over its alleged unlawful sale of location, has reportedly been hacked – potentially exposing millions of smartphone users.... [...]
