Title: Boffins carve up C so code can be converted to Rust
Date: 2025-01-03T12:33:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2025-01-03-boffins-carve-up-c-so-code-can-be-converted-to-rust

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/03/mini_c_microsoft_inria/){:target="_blank" rel="noopener"}

> Mini-C is a subset of C that can be automatically turned to Rust without much fuss Computer scientists affiliated with France's Inria and Microsoft have devised a way to automatically turn a subset of C code into safe Rust code, in an effort to meet the growing demand for memory safety.... [...]
