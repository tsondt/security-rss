Title: FCC orders telecoms to secure their networks after Salt Tyhpoon hacks
Date: 2025-01-17T11:05:29-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-17-fcc-orders-telecoms-to-secure-their-networks-after-salt-tyhpoon-hacks

[Source](https://www.bleepingcomputer.com/news/security/fcc-orders-telecoms-to-secure-their-networks-after-salt-tyhpoon-hacks/){:target="_blank" rel="noopener"}

> The Federal Communications Commission (FCC) has ordered U.S. telecommunications carriers to secure their networks following last year's Salt Typhoon security breaches. [...]
