Title: Baguette bandits strike again with ransomware and a side of mockery
Date: 2025-01-28T22:15:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-28-baguette-bandits-strike-again-with-ransomware-and-a-side-of-mockery

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/28/baguettes_bandits_strike_again/){:target="_blank" rel="noopener"}

> Big-game hunting to the extreme Hellcat, the ransomware crew that infected Schneider Electric and demanded $125,000 in baguettes, has aggressively targeted government, education, energy, and other critical industries since it emerged around mid-2024.... [...]
