Title: DEF CON's hacker-in-chief faces fortune in medical bills after paralyzing neck injury
Date: 2025-01-07T14:45:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-07-def-cons-hacker-in-chief-faces-fortune-in-medical-bills-after-paralyzing-neck-injury

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/07/def_con_security_chief_injured/){:target="_blank" rel="noopener"}

> Marc Rogers is 'lucky to be alive' Marc Rogers, DEF CON's head of security, faces tens of thousands of dollars in medical bills following an accident that left him with a broken neck and temporary quadriplegia.... [...]
