Title: Court rules FISA Section 702 surveillance of US resident was unconstitutional
Date: 2025-01-24T04:31:55+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-01-24-court-rules-fisa-section-702-surveillance-of-us-resident-was-unconstitutional

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/24/section_702_court/){:target="_blank" rel="noopener"}

> 'Public interest alone does not justify warrantless querying' says judge It was revealed this week a court in New York made a landmark ruling that sided against the warrantless state surveillance of people's private communications in America.... [...]
