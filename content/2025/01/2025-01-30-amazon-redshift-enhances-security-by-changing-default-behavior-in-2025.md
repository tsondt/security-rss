Title: Amazon Redshift enhances security by changing default behavior in 2025
Date: 2025-01-30T18:19:00+00:00
Author: Yanzhu Ji
Category: AWS Security
Tags: Amazon Redshift;Announcements;Intermediate (200);Security, Identity, & Compliance;Redshift;Security Blog
Slug: 2025-01-30-amazon-redshift-enhances-security-by-changing-default-behavior-in-2025

[Source](https://aws.amazon.com/blogs/security/amazon-redshift-enhances-security-by-changing-default-behavior-in-2025/){:target="_blank" rel="noopener"}

> Today, I’m thrilled to announce that Amazon Redshift, a widely used, fully managed, petabyte-scale data warehouse, is taking a significant step forward in strengthening the default security posture of our customers’ data warehouses. Some default security settings for newly created provisioned clusters, Amazon Redshift Serverless workgroups, and clusters restored from snapshots have changed. These changes [...]
