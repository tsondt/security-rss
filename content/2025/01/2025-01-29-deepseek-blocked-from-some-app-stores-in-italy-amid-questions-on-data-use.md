Title: DeepSeek blocked from some app stores in Italy amid questions on data use
Date: 2025-01-29T18:20:57+00:00
Author: Robert Booth, Jacob Krupa and Angela Giuffrida in Rome
Category: The Guardian
Tags: DeepSeek;Artificial intelligence (AI);Computing;Technology;China;Italy;Ireland;Apple;Google;Alphabet;Asia Pacific;Europe;World news;Data and computer security;Data protection
Slug: 2025-01-29-deepseek-blocked-from-some-app-stores-in-italy-amid-questions-on-data-use

[Source](https://www.theguardian.com/technology/2025/jan/29/deepseek-blocked-some-app-stores-italy-questions-data-use){:target="_blank" rel="noopener"}

> Italian and Irish regulators want answers on how data harvested by chatbot could be used by Chinese government The Chinese AI platform DeepSeek has become unavailable for download from some app stores in Italy as regulators in Rome and in Ireland demanded answers from the company about its handling of citizens’ data. Amid growing concern on Wednesday about how data harvested by the new chatbot could be used by the Chinese government, the app disappeared from the Apple and Google app stores in Italy with customers seeing messages that said it was “currently not available in the country or area [...]
