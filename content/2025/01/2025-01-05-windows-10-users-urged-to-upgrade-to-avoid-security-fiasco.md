Title: Windows 10 users urged to upgrade to avoid "security fiasco"
Date: 2025-01-05T15:48:04-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2025-01-05-windows-10-users-urged-to-upgrade-to-avoid-security-fiasco

[Source](https://www.bleepingcomputer.com/news/microsoft/windows-10-users-urged-to-upgrade-to-avoid-security-fiasco/){:target="_blank" rel="noopener"}

> ​Cybersecurity firm ESET is urging Windows 10 users to upgrade to Windows 11 or Linux to avoid a "security fiasco" as the 10-year-old operating system nears the end of support in October 2025. [...]
