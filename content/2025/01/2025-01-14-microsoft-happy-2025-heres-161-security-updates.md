Title: Microsoft: Happy 2025. Here’s 161 Security Updates
Date: 2025-01-14T22:50:00+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Latest Warnings;The Coming Storm;Time to Patch;Adam Barnett;Bitlocker;Bob Hopkins;CVE-2024-49142;CVE-2025-21186;CVE-2025-21210;CVE-2025-21298;CVE-2025-21311;CVE-2025-21333;CVE-2025-21334;CVE-2025-21335;CVE-2025-21366;CVE-2025-21395;Kev Breen;Microsoft Access;Microsoft Patch Tuesday January 2025;Rapid7;Satnam Narang;unpatched.ai;Windows 11;Windows Hyper-V;Windows NTLMv1
Slug: 2025-01-14-microsoft-happy-2025-heres-161-security-updates

[Source](https://krebsonsecurity.com/2025/01/microsoft-happy-2025-heres-161-security-updates/){:target="_blank" rel="noopener"}

> Microsoft today unleashed updates to plug a whopping 161 security vulnerabilities in Windows and related software, including three “zero-day” weaknesses that are already under active attack. Redmond’s inaugural Patch Tuesday of 2025 bundles more fixes than the company has shipped in one go since 2017. Rapid7 ‘s Adam Barnett says January marks the fourth consecutive month where Microsoft has published zero-day vulnerabilities on Patch Tuesday without evaluating any of them as critical severity at time of publication. Today also saw the publication of nine critical remote code execution (RCE) vulnerabilities. The Microsoft flaws already seeing active attacks include CVE-2025-21333, CVE-2025-21334 [...]
