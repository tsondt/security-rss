Title: US sanctions Chinese company linked to Flax Typhoon hackers
Date: 2025-01-03T11:19:53-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-03-us-sanctions-chinese-company-linked-to-flax-typhoon-hackers

[Source](https://www.bleepingcomputer.com/news/security/us-sanctions-chinese-company-linked-to-flax-typhoon-hackers/){:target="_blank" rel="noopener"}

> ​The U.S. Treasury Department has sanctioned Beijing-based cybersecurity company Integrity Tech (also known as Yongxin Zhicheng) for its involvement in cyberattacks attributed to the Chinese state-sponsored Flax Typhoon hacking group. [...]
