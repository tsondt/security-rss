Title: MirrorFace hackers targeting Japanese govt, politicians since 2019
Date: 2025-01-09T12:20:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-09-mirrorface-hackers-targeting-japanese-govt-politicians-since-2019

[Source](https://www.bleepingcomputer.com/news/security/mirrorface-hackers-targeting-japanese-govt-politicians-since-2019/){:target="_blank" rel="noopener"}

> The National Police Agency (NPA) and the Cabinet Cyber Security Center in Japan have linked a cyber-espionage campaign targeting the country to the Chinese state-backed "MirrorFace" hacking group. [...]
