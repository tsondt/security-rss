Title: Look for the label: White House rolls out 'Cyber Trust Mark' for smart devices
Date: 2025-01-09T21:45:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2025-01-09-look-for-the-label-white-house-rolls-out-cyber-trust-mark-for-smart-devices

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/09/white_house_smart_device_security_label/){:target="_blank" rel="noopener"}

> Beware the IoT that doesn’t get a security tag The White House this week introduced a voluntary cybersecurity labeling program for technology products so that consumers can have some assurance their smart devices aren't spying on them.... [...]
