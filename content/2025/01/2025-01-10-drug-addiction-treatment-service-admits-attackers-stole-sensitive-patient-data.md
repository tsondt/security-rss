Title: Drug addiction treatment service admits attackers stole sensitive patient data
Date: 2025-01-10T15:37:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-10-drug-addiction-treatment-service-admits-attackers-stole-sensitive-patient-data

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/10/baymark_data_breach/){:target="_blank" rel="noopener"}

> Details of afflictions and care plastered online BayMark Health Services, one of the biggest drug addiction treatment facilities in the US, says it is notifying some patients this week that their sensitive personal information was stolen.... [...]
