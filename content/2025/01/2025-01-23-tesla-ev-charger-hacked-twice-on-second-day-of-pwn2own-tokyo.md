Title: Tesla EV charger hacked twice on second day of Pwn2Own Tokyo
Date: 2025-01-23T10:24:11-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-23-tesla-ev-charger-hacked-twice-on-second-day-of-pwn2own-tokyo

[Source](https://www.bleepingcomputer.com/news/security/tesla-ev-charger-hacked-twice-on-second-day-of-pwn2own-tokyo/){:target="_blank" rel="noopener"}

> ​Security researchers hacked Tesla's Wall Connector electric vehicle charger twice on the second day of the Pwn2Own Automotive 2025 hacking contest. [...]
