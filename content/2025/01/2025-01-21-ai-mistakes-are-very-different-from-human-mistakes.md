Title: AI Mistakes Are Very Different from Human Mistakes
Date: 2025-01-21T12:02:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;AI;LLM
Slug: 2025-01-21-ai-mistakes-are-very-different-from-human-mistakes

[Source](https://www.schneier.com/blog/archives/2025/01/ai-mistakes-are-very-different-from-human-mistakes.html){:target="_blank" rel="noopener"}

> Humans make mistakes all the time. All of us do, every day, in tasks both new and routine. Some of our mistakes are minor and some are catastrophic. Mistakes can break trust with our friends, lose the confidence of our bosses, and sometimes be the difference between life and death. Over the millennia, we have created security systems to deal with the sorts of mistakes humans commonly make. These days, casinos rotate their dealers regularly, because they make mistakes if they do the same task for too long. Hospital personnel write on limbs before surgery so that doctors operate on [...]
