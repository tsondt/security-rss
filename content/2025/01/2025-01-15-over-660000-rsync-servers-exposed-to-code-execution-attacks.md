Title: Over 660,000 Rsync servers exposed to code execution attacks
Date: 2025-01-15T12:00:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Linux
Slug: 2025-01-15-over-660000-rsync-servers-exposed-to-code-execution-attacks

[Source](https://www.bleepingcomputer.com/news/security/over-660-000-rsync-servers-exposed-to-code-execution-attacks/){:target="_blank" rel="noopener"}

> Over 660,000 exposed Rsync servers are potentially vulnerable to six new vulnerabilities, including a critical-severity heap-buffer overflow flaw that allows remote code execution on servers. [...]
