Title: Ransomware crew abuses AWS native encryption, sets data-destruct timer for 7 days
Date: 2025-01-13T14:00:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-13-ransomware-crew-abuses-aws-native-encryption-sets-data-destruct-timer-for-7-days

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/13/ransomware_crew_abuses_compromised_aws/){:target="_blank" rel="noopener"}

> 'Codefinger' crims on the hunt for compromised keys A new ransomware crew dubbed Codefinger targets AWS S3 buckets and uses the cloud giant's own server-side encryption with customer provided keys (SSE-C) to lock up victims' data before demanding a ransom payment for the symmetric AES-256 keys required to decrypt it.... [...]
