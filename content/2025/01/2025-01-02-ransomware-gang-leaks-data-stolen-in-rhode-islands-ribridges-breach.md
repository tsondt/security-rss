Title: Ransomware gang leaks data stolen in Rhode Island's RIBridges Breach
Date: 2025-01-02T17:51:01-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2025-01-02-ransomware-gang-leaks-data-stolen-in-rhode-islands-ribridges-breach

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-leaks-data-stolen-in-rhode-islands-ribridges-breach/){:target="_blank" rel="noopener"}

> The Brain Cipher ransomware gang has begun to leak documents stolen in an attack on Rhode Island's "RIBridges" social services platform. [...]
