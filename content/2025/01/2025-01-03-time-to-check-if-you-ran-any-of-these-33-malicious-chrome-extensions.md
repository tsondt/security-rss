Title: Time to check if you ran any of these 33 malicious Chrome extensions
Date: 2025-01-03T12:15:47+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;browsers;chrome;extensions;privacy
Slug: 2025-01-03-time-to-check-if-you-ran-any-of-these-33-malicious-chrome-extensions

[Source](https://arstechnica.com/security/2025/01/dozens-of-backdoored-chrome-extensions-discovered-on-2-6-million-devices/){:target="_blank" rel="noopener"}

> As many of us celebrated the year-end holidays, a small group of researchers worked overtime tracking a startling discovery: At least 33 browser extensions hosted in Google’s Chrome Web Store, some for as long as 18 months, were surreptitiously siphoning sensitive data from roughly 2.6 million devices. The compromises came to light with the discovery by data loss prevention service Cyberhaven that a Chrome extension used by 400,000 of its customers had been updated with code that stole their sensitive data. ’Twas the night before Christmas The malicious extension, available as version 24.10.4, was available for 31 hours, from December [...]
