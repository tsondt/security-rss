Title: Upcoming Speaking Engagements
Date: 2025-01-14T17:05:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2025-01-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2025/01/upcoming-speaking-engagements-42.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking on “AI: Trust & Power” at Capricon 45 in Chicago, Illinois, USA, at 11:30 AM on February 7, 2025. I’m also signing books there on Saturday, February 8, starting at 1:45 PM. I’m speaking at Boskone 62 in Boston, Massachusetts, USA, which runs from February 14-16, 2025. I’m speaking at the Rossfest Symposium in Cambridge, UK, on March 25, 2025. The list is maintained on this page. [...]
