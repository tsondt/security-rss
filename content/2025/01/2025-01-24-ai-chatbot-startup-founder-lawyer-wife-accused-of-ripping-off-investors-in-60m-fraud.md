Title: AI chatbot startup founder, lawyer wife accused of ripping off investors in $60M fraud
Date: 2025-01-24T23:26:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2025-01-24-ai-chatbot-startup-founder-lawyer-wife-accused-of-ripping-off-investors-in-60m-fraud

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/24/ai_startup_founder_wife_indicted/){:target="_blank" rel="noopener"}

> GameOn? It's looking more like game over for that biz The co-founder and former CEO of AI startup GameOn is in a pickle. After exiting the top job last year under a cloud, he's now in court – along with his wife – for allegedly bilking his company and its investors out of more than $60 million.... [...]
