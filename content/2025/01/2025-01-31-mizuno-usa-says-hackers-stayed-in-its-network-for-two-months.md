Title: Mizuno USA says hackers stayed in its network for two months
Date: 2025-01-31T10:12:06-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-31-mizuno-usa-says-hackers-stayed-in-its-network-for-two-months

[Source](https://www.bleepingcomputer.com/news/security/mizuno-usa-says-hackers-stayed-in-its-network-for-two-months/){:target="_blank" rel="noopener"}

> ​Mizuno USA, a subsidiary of Mizuno Corporation, one of the world's largest sporting goods manufacturers, confirmed in data breach notification letters that unknown attackers stole files from its network between August and October 2024. [...]
