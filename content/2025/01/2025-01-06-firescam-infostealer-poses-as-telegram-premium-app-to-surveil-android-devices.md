Title: FireScam infostealer poses as Telegram Premium app to surveil Android devices
Date: 2025-01-06T16:31:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-06-firescam-infostealer-poses-as-telegram-premium-app-to-surveil-android-devices

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/06/firescam_android_malware/){:target="_blank" rel="noopener"}

> Once installed, it helps itself to your data like it's a free buffet Android malware dubbed FireScam tricks people into thinking they are downloading a Telegram Premium application that stealthily monitors victims' notifications, text messages, and app activity, while stealing sensitive information via Firebase services.... [...]
