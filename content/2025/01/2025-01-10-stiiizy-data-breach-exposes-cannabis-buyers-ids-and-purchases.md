Title: STIIIZY data breach exposes cannabis buyers’ IDs and purchases
Date: 2025-01-10T10:19:50-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2025-01-10-stiiizy-data-breach-exposes-cannabis-buyers-ids-and-purchases

[Source](https://www.bleepingcomputer.com/news/security/stiiizy-data-breach-exposes-cannabis-buyers-ids-and-purchases/){:target="_blank" rel="noopener"}

> Popular cannabis brand STIIIZY disclosed a data breach this week after hackers breached its point-of-sale (POS) vendor to steal customer information, including government IDs and purchase information. [...]
