Title: 2024 C5 Type 2 attestation report available with 179 services in scope
Date: 2025-01-27T17:10:35+00:00
Author: Tea Jioshvili
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Security Blog
Slug: 2025-01-27-2024-c5-type-2-attestation-report-available-with-179-services-in-scope

[Source](https://aws.amazon.com/blogs/security/2024-c5-type-2-attestation-report-available-with-179-services-in-scope/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce a successful completion of the 2024 Cloud Computing Compliance Controls Catalogue (C5) attestation cycle with 179 services in scope. This alignment with C5 requirements demonstrates our ongoing commitment to adhere to the heightened expectations for cloud service providers. AWS customers in Germany and across Europe can run [...]
