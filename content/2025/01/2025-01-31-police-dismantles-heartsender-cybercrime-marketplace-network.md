Title: Police dismantles HeartSender cybercrime marketplace network
Date: 2025-01-31T06:56:57-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-31-police-dismantles-heartsender-cybercrime-marketplace-network

[Source](https://www.bleepingcomputer.com/news/security/police-dismantles-heartsender-cybercrime-marketplace-network/){:target="_blank" rel="noopener"}

> ​Law enforcement authorities in the United States and the Netherlands have seized 39 domains and associated servers used by the HeartSender phishing gang operating out of Pakistan. [...]
