Title: Phishing False Alarm
Date: 2025-01-15T12:00:58+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;false positives;phishing
Slug: 2025-01-15-phishing-false-alarm

[Source](https://www.schneier.com/blog/archives/2025/01/phishing-false-alarm.html){:target="_blank" rel="noopener"}

> A very security-conscious company was hit with a (presumed) massive state-actor phishing attack with gift cards, and everyone rallied to combat it—until it turned out it was company management sending the gift cards. [...]
