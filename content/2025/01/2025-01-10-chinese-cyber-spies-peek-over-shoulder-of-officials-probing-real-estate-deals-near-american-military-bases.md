Title: Chinese cyber-spies peek over shoulder of officials probing real-estate deals near American military bases
Date: 2025-01-10T21:45:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-10-chinese-cyber-spies-peek-over-shoulder-of-officials-probing-real-estate-deals-near-american-military-bases

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/10/china_treasury_foreign_investment/){:target="_blank" rel="noopener"}

> Gee, wonder why Beijing is so keen on the – checks notes – Committee on Foreign Investment in the US Chinese cyber-spies who broke into the US Treasury Department also stole documents from officials investigating real-estate sales near American military bases, it's reported.... [...]
