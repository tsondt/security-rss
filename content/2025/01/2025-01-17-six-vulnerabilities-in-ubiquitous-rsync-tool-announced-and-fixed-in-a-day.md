Title: Six vulnerabilities in ubiquitous rsync tool announced and fixed in a day
Date: 2025-01-17T15:49:09+00:00
Author: Liam Proven
Category: The Register
Tags: 
Slug: 2025-01-17-six-vulnerabilities-in-ubiquitous-rsync-tool-announced-and-fixed-in-a-day

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/17/rsync_vulnerabilities/){:target="_blank" rel="noopener"}

> Turns out tool does both file transfers and security fixes fast Don't panic. Yes, there were a bunch of CVEs, affecting potentially hundreds of thousands of users, found in rsync in early December – and made public on Tuesday – but a fixed version came out the same day, and was further tweaked for better compatibility the following day.... [...]
