Title: Russia's Star Blizzard phishing crew caught targeting WhatsApp accounts
Date: 2025-01-16T19:15:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-16-russias-star-blizzard-phishing-crew-caught-targeting-whatsapp-accounts

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/16/russia_star_blizzard_whatsapp/){:target="_blank" rel="noopener"}

> FSB cyberspies venture into a new app for espionage, Microsoft says Star Blizzard, a prolific phishing crew backed by the Russian Federal Security Service (FSB), conducted a new campaign aiming to compromise WhatsApp accounts and gain access to their messages and data, according to Microsoft.... [...]
