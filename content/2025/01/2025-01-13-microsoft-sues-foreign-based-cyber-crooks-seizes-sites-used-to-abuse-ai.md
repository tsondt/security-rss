Title: Microsoft sues 'foreign-based' cyber-crooks, seizes sites used to abuse AI
Date: 2025-01-13T19:00:15+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-13-microsoft-sues-foreign-based-cyber-crooks-seizes-sites-used-to-abuse-ai

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/13/microsoft_sues_foreignbased_crims_seizes/){:target="_blank" rel="noopener"}

> Scumbags stole API keys, then started a hacking-as-a-service biz, it is claimed Microsoft has sued a group of unnamed cybercriminals who developed tools to bypass safety guardrails in its generative AI tools. The tools were used to create harmful content, and access to the tools were sold as a service to other miscreants.... [...]
