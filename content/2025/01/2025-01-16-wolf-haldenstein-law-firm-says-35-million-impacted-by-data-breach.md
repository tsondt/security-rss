Title: Wolf Haldenstein law firm says 3.5 million impacted by data breach
Date: 2025-01-16T11:26:41-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2025-01-16-wolf-haldenstein-law-firm-says-35-million-impacted-by-data-breach

[Source](https://www.bleepingcomputer.com/news/security/wolf-haldenstein-law-firm-says-35-million-impacted-by-data-breach/){:target="_blank" rel="noopener"}

> Wolf Haldenstein Adler Freeman & Herz LLP ("Wolf Haldenstein") reports it has suffered a data breach that exposed the personal information of nearly 3.5 million individuals to hackers. [...]
