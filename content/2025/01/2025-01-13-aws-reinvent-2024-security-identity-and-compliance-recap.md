Title: AWS re:Invent 2024: Security, identity, and compliance recap
Date: 2025-01-13T18:04:17+00:00
Author: Marshall Jones
Category: AWS Security
Tags: Announcements;AWS re:Invent;Foundational (100);Security, Identity, & Compliance;AWS Re:Invent;Live Events;Security Blog
Slug: 2025-01-13-aws-reinvent-2024-security-identity-and-compliance-recap

[Source](https://aws.amazon.com/blogs/security/aws-reinvent-2024-security-identity-and-compliance-recap/){:target="_blank" rel="noopener"}

> AWS re:Invent 2024 was held in Las Vegas December 2–6, with over 54,000 attendees participating in more than 2,300 sessions and hands-on labs. The conference was a hub of innovation and learning hosted by AWS for the global cloud computing community. In this blog post, we cover on-demand sessions and major security, identity, and compliance announcements that [...]
