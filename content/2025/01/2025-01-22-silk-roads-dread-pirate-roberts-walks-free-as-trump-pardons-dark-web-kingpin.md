Title: Silk Road's Dread Pirate Roberts walks free as Trump pardons dark web kingpin
Date: 2025-01-22T15:30:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-22-silk-roads-dread-pirate-roberts-walks-free-as-trump-pardons-dark-web-kingpin

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/22/silk_road_founder_freed/){:target="_blank" rel="noopener"}

> Ross Ulbricht's family are now appealing for donations to support his reintegration into society Silk Road founder Ross Ulbricht is now a free man after US President Donald Trump made good on his promise to issue a federal pardon upon taking office.... [...]
