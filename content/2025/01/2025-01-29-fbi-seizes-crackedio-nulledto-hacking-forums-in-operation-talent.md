Title: FBI seizes Cracked.io, Nulled.to hacking forums in Operation Talent
Date: 2025-01-29T12:30:09-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-29-fbi-seizes-crackedio-nulledto-hacking-forums-in-operation-talent

[Source](https://www.bleepingcomputer.com/news/security/fbi-seizes-crackedio-nulledto-hacking-forums-in-operation-talent/){:target="_blank" rel="noopener"}

> The FBI has seized the domains for the infamous Cracked.io and Nulled.to hacking forums, which are known for their focus on cybercrime, password theft, cracking, and credential stuffing attacks. [...]
