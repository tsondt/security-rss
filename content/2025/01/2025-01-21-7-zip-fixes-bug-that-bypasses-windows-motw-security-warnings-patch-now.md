Title: 7-Zip fixes bug that bypasses Windows MoTW security warnings, patch now
Date: 2025-01-21T11:05:10-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-21-7-zip-fixes-bug-that-bypasses-windows-motw-security-warnings-patch-now

[Source](https://www.bleepingcomputer.com/news/security/7-zip-fixes-bug-that-bypasses-the-windows-motw-security-mechanism-patch-now/){:target="_blank" rel="noopener"}

> ​A high-severity vulnerability in the 7-Zip file archiver allows attackers to bypass the Mark of the Web (MotW) Windows security feature and execute code on users' computers when extracting malicious files from nested archives. [...]
