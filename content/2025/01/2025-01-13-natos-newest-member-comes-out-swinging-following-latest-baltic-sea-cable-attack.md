Title: NATO's newest member comes out swinging following latest Baltic Sea cable attack
Date: 2025-01-13T16:47:22+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2025-01-13-natos-newest-member-comes-out-swinging-following-latest-baltic-sea-cable-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/13/sweden_baltic_cable_attack/){:target="_blank" rel="noopener"}

> 'Sweden has changed,' PM warns as trio of warships join defense efforts Sweden has committed to sending naval forces into the Baltic Sea following yet another suspected Russian attack on underwater cables in the region.... [...]
