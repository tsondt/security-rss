Title: Google OAuth flaw lets attackers gain access to abandoned accounts
Date: 2025-01-14T12:28:20-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2025-01-14-google-oauth-flaw-lets-attackers-gain-access-to-abandoned-accounts

[Source](https://www.bleepingcomputer.com/news/security/google-oauth-flaw-lets-attackers-gain-access-to-abandoned-accounts/){:target="_blank" rel="noopener"}

> A weakness in Google's OAuth "Sign in with Google" feature could enable attackers that register domains of defunct startups to access sensitive data of former employee accounts linked to various software-as-a-service (SaaS) platforms. [...]
