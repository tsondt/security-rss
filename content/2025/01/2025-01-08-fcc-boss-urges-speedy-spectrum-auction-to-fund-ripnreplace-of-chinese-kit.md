Title: FCC boss urges speedy spectrum auction to fund 'Rip'n'Replace' of Chinese kit
Date: 2025-01-08T00:12:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-08-fcc-boss-urges-speedy-spectrum-auction-to-fund-ripnreplace-of-chinese-kit

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/08/fcc_chief_urges_spectrum_auction/){:target="_blank" rel="noopener"}

> Telcos would effectively fund grants paid to protect national security The outgoing boss of the FCC, Jessica Rosenworcel, has called on her colleagues to "quickly" adopt rules allowing the US regulator to stage a radio spectrum auction, the proceeds of which would fund the removal from American networks of equipment made by Chinese vendors Huawei and ZTE.... [...]
