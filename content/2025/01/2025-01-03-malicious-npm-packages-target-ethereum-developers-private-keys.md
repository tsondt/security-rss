Title: Malicious npm packages target Ethereum developers' private keys
Date: 2025-01-03T10:53:34-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-03-malicious-npm-packages-target-ethereum-developers-private-keys

[Source](https://www.bleepingcomputer.com/news/security/malicious-npm-packages-target-ethereum-developers-private-keys/){:target="_blank" rel="noopener"}

> Twenty malicious packages impersonating the Hardhat development environment used by Ethereum developers are targeting private keys and other sensitive data. [...]
