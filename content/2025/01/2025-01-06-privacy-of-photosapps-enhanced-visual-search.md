Title: Privacy of Photos.app’s Enhanced Visual Search
Date: 2025-01-06T12:06:52+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;searches
Slug: 2025-01-06-privacy-of-photosapps-enhanced-visual-search

[Source](https://www.schneier.com/blog/archives/2025/01/privacy-of-photos-apps-enhanced-visual-search.html){:target="_blank" rel="noopener"}

> Initial speculation about a new Apple feature. [...]
