Title: Azure, Microsoft 365 MFA outage locks out users across regions
Date: 2025-01-13T17:55:09+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-01-13-azure-microsoft-365-mfa-outage-locks-out-users-across-regions

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/13/azure_m365_outage/){:target="_blank" rel="noopener"}

> It's fixed, mostly, after Europeans had a manic Monday Microsoft's multi-factor authentication (MFA) for Azure and Microsoft 365 (M365) was offline for four hours during Monday's busy start for European subscribers.... [...]
