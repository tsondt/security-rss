Title: Raspberry Pi hands out prizes to all in the RP2350 Hacking Challenge
Date: 2025-01-16T15:15:07+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2025-01-16-raspberry-pi-hands-out-prizes-to-all-in-the-rp2350-hacking-challenge

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/16/raspberry_pi_awards_prizes_for/){:target="_blank" rel="noopener"}

> Power-induced glitches, lasers, and electromagnetic fields are all tools of the trade Raspberry Pi has given out prizes for extracting a secret value from the one-time-programmable (OTP) memory of the Raspberry Pi RP2350 microcontroller – awarding a pile of cash to all four entrants.... [...]
