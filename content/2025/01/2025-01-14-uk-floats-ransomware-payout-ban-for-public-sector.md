Title: UK floats ransomware payout ban for public sector
Date: 2025-01-14T11:04:22+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-14-uk-floats-ransomware-payout-ban-for-public-sector

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/14/uk_ransomware_payout_ban/){:target="_blank" rel="noopener"}

> Stronger proposals may also see private sector applying for a payment 'license' A total ban on ransomware payments across the public sector might actually happen after the UK government opened a consultation on how to combat the trend of criminals locking up whole systems and taxpayers footing the bill.... [...]
