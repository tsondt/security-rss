Title: Criminal IP Teams Up with OnTheHub for Digital Education Cybersecurity
Date: 2025-01-21T10:02:12-05:00
Author: Sponsored by Criminal IP
Category: BleepingComputer
Tags: Security
Slug: 2025-01-21-criminal-ip-teams-up-with-onthehub-for-digital-education-cybersecurity

[Source](https://www.bleepingcomputer.com/news/security/criminal-ip-teams-up-with-onthehub-for-digital-education-cybersecurity/){:target="_blank" rel="noopener"}

> AI SPERA announced today that it has partnered with education platform OnTheHub to provide its integrated cybersecurity solution, Criminal IP, to students and educational institutions. [...]
