Title: Microsoft tests Edge Scareware Blocker to block tech support scams
Date: 2025-01-28T09:30:52-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2025-01-28-microsoft-tests-edge-scareware-blocker-to-block-tech-support-scams

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-tests-edge-scareware-blocker-to-block-tech-support-scams/){:target="_blank" rel="noopener"}

> Microsoft has started testing a new "scareware blocker" feature for the Edge web browser on Windows PCs, which uses machine learning (ML) to detect tech support scams. [...]
