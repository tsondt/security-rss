Title: UN aviation agency investigating 'potential' security breach
Date: 2025-01-07T10:59:23-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-07-un-aviation-agency-investigating-potential-security-breach

[Source](https://www.bleepingcomputer.com/news/security/un-aviation-agency-investigating-potential-security-breach/){:target="_blank" rel="noopener"}

> ​On Monday, the United Nations' International Civil Aviation Organization (ICAO) announced it was investigating what it described as a "reported security incident." [...]
