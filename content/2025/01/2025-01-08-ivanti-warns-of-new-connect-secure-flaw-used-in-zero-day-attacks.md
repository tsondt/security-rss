Title: Ivanti warns of new Connect Secure flaw used in zero-day attacks
Date: 2025-01-08T15:43:34-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2025-01-08-ivanti-warns-of-new-connect-secure-flaw-used-in-zero-day-attacks

[Source](https://www.bleepingcomputer.com/news/security/ivanti-warns-of-new-connect-secure-flaw-used-in-zero-day-attacks/){:target="_blank" rel="noopener"}

> Ivanti is warning that a new Connect Secure remote code execution vulnerability tracked as CVE-2025-0282 was exploited in zero-day attacks to install malware on appliances. [...]
