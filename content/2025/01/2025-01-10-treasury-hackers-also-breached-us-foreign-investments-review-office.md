Title: Treasury hackers also breached US foreign investments review office
Date: 2025-01-10T12:02:49-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-10-treasury-hackers-also-breached-us-foreign-investments-review-office

[Source](https://www.bleepingcomputer.com/news/security/treasury-hackers-also-breached-us-foreign-investments-review-office/){:target="_blank" rel="noopener"}

> Chinese hackers, part of the state-backed Silk Typhoon threat group, have reportedly breached the Committee on Foreign Investment in the United States (CFIUS), which reviews foreign investments to determine national security risks. [...]
