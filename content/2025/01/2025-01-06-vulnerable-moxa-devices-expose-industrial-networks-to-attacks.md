Title: Vulnerable Moxa devices expose industrial networks to attacks
Date: 2025-01-06T12:15:46-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2025-01-06-vulnerable-moxa-devices-expose-industrial-networks-to-attacks

[Source](https://www.bleepingcomputer.com/news/security/vulnerable-moxa-devices-expose-industrial-networks-to-attacks/){:target="_blank" rel="noopener"}

> Industrial networking and communications provider Moxa is warning of a high-severity and a critical vulnerability that impact various models of its cellular routers, secure routers, and network security appliances. [...]
