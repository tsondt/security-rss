Title: PowerSchool theft latest: Decades of Canadian student records, data from 40-plus US states feared stolen
Date: 2025-01-22T01:02:31+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-01-22-powerschool-theft-latest-decades-of-canadian-student-records-data-from-40-plus-us-states-feared-stolen

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/22/powerschool_canada_lawsuits/){:target="_blank" rel="noopener"}

> Lawsuits pile up after database accessed by miscreants Updated Canada's largest school board has revealed that student records dating back to 1985 may have been accessed by miscreants who compromised software provider PowerSchool.... [...]
