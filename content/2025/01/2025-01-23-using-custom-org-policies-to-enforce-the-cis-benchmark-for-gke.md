Title: Using custom Org Policies to enforce the CIS benchmark for GKE
Date: 2025-01-23T17:00:00+00:00
Author: Edi Wiraya
Category: GCP Security
Tags: Security & Identity
Slug: 2025-01-23-using-custom-org-policies-to-enforce-the-cis-benchmark-for-gke

[Source](https://cloud.google.com/blog/products/identity-security/how-to-use-custom-org-policies-to-enforce-cis-benchmark-for-gke/){:target="_blank" rel="noopener"}

> As the adoption of container workloads increases, so does the need to establish and maintain a consistent, strong Kubernetes security posture. Failing to do so can have significant consequences for the risk posture of an organization. Nearly 50% of organizations experienced revenue or customer loss due to container and Kubernetes security incidents, according to the 2024 State of Kubernetes Security Report. Org policies are your friend to help you achieve pervasive security across your cloud infrastructure. In particular, you can use custom Organization Policies to enforce many of the CIS Benchmarks proactively, ensuring that you've established proper guardrails for Google [...]
