Title: CDNs: Great for speeding up the internet, bad for location privacy
Date: 2025-01-27T11:45:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2025-01-27-cdns-great-for-speeding-up-the-internet-bad-for-location-privacy

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/27/cloudflare_cdn_location_data/){:target="_blank" rel="noopener"}

> Also, Subaru web portal spills user deets, Tornado Cash sanctions overturned, a Stark ransomware attack, and more Infosec in brief Using a custom-built tool, a 15-year-old hacker exploited Cloudflare's content delivery network to approximate the locations of users of apps like Signal, Discord, and others.... [...]
