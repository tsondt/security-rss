Title: Encryption backdoor debate 'done and dusted,' former White House tech advisor says
Date: 2025-01-04T14:30:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-04-encryption-backdoor-debate-done-and-dusted-former-white-house-tech-advisor-says

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/04/encryption_backdoor_debate/){:target="_blank" rel="noopener"}

> When the FBI urges E2EE, you know it's serious business interview In the wake of the Salt Typhoon hacks, which lawmakers and privacy advocates alike have called the worst telecoms breach in America's history, the US government agencies have reversed course on encryption.... [...]
