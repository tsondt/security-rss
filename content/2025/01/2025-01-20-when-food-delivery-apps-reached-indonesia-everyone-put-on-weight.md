Title: When food delivery apps reached Indonesia, everyone put on weight
Date: 2025-01-20T03:30:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2025-01-20-when-food-delivery-apps-reached-indonesia-everyone-put-on-weight

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/20/asia_tech_news_roundup/){:target="_blank" rel="noopener"}

> PLUS: Salt Typhoon and IT worker scammers sanctioned; Alibaba Cloud’s K8s go global; Amazon acquires Indian BNPL company Asia In Brief When food delivery “superapps” started operations in Indonesia, users started putting on weight – and that’s not an entirely bad thing.... [...]
