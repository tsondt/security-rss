Title: Even modest makeup can thwart facial recognition
Date: 2025-01-15T18:45:11+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2025-01-15-even-modest-makeup-can-thwart-facial-recognition

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/15/make_up_thwart_facial_recognition/){:target="_blank" rel="noopener"}

> You may not need to go full Juggalo for the sake of privacy Researchers at cyber-defense contractor PeopleTec have found that facial-recognition algorithms' focus on specific areas of the face opens the door to subtler surveillance avoidance strategies.... [...]
