Title: Patch now: Cisco fixes critical 9.9-rated, make-me-admin bug in Meeting Management
Date: 2025-01-23T21:00:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-23-patch-now-cisco-fixes-critical-99-rated-make-me-admin-bug-in-meeting-management

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/23/cisco_fixes_critical_bug/){:target="_blank" rel="noopener"}

> No in-the-wild exploits... yet Cisco has pushed a patch for a critical, 9.9-rated vulnerability in its Meeting Management tool that could allow a remote, authenticated attacker with low privileges to escalate to administrator on affected devices.... [...]
