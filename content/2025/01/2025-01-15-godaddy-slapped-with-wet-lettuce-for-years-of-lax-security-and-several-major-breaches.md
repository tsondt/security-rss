Title: GoDaddy slapped with wet lettuce for years of lax security and 'several major breaches'
Date: 2025-01-15T23:47:18+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-15-godaddy-slapped-with-wet-lettuce-for-years-of-lax-security-and-several-major-breaches

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/15/godaddy_ftc_order/){:target="_blank" rel="noopener"}

> Watchdog alleged it had no SIEM or MFA, orders rapid adoption of basic infosec tools GoDaddy has failed to protect its web-hosting platform with even basic infosec tools and practices since 2018, according to the FTC, but the internet giant won’t face any immediate consequences for its many alleged acts of omission.... [...]
