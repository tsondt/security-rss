Title: Ransomware scum make it personal for <i>Reg</i> readers by impersonating tech support
Date: 2025-01-22T09:29:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-22-ransomware-scum-make-it-personal-for-reg-readers-by-impersonating-tech-support

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/22/ransomware_crews_abuse_microsoft_teams/){:target="_blank" rel="noopener"}

> That invitation to a Teams call on which IT promises to mop up a spamstorm may not be what it seems Two ransomware campaigns are abusing Microsoft Teams to infect organizations and steal data, and the crooks may have ties to Black Basta and FIN7, according to Sophos.... [...]
