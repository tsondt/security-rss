Title: It's not just Big Tech: The UK's Online Safety Act applies across the board
Date: 2025-01-14T12:15:10+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2025-01-14-its-not-just-big-tech-the-uks-online-safety-act-applies-across-the-board

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/14/online_safety_act/){:target="_blank" rel="noopener"}

> That niche forum running for 20 years – get ready, there's work to do Analysis A little more than two months out from its first legal deadline, the UK’s Online Safety Act is causing concern among smaller online forums caught within its reach. The legislation, which came into law in the autumn of 2023, applies to search services and services that allow users to post content online or to interact with each other.... [...]
