Title: Ransomware attack disrupts New York blood donation giant
Date: 2025-01-30T12:53:13-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2025-01-30-ransomware-attack-disrupts-new-york-blood-donation-giant

[Source](https://www.bleepingcomputer.com/news/security/ransomware-attack-disrupts-new-york-blood-donation-giant/){:target="_blank" rel="noopener"}

> ​The New York Blood Center (NYBC), one of the world's largest independent blood collection and distribution organizations, says a Sunday ransomware attack forced it to reschedule some appointments. [...]
