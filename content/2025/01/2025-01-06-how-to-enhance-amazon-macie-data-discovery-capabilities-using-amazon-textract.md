Title: How to enhance Amazon Macie data discovery capabilities using Amazon Textract
Date: 2025-01-06T17:02:27+00:00
Author: ZhiWei Huang
Category: AWS Security
Tags: Amazon Macie;Amazon Textract;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2025-01-06-how-to-enhance-amazon-macie-data-discovery-capabilities-using-amazon-textract

[Source](https://aws.amazon.com/blogs/security/how-to-enhance-amazon-macie-data-discovery-capabilities-using-amazon-textract/){:target="_blank" rel="noopener"}

> Amazon Macie is a managed service that uses machine learning (ML) and deterministic pattern matching to help discover sensitive data that’s stored in Amazon Simple Storage Service (Amazon S3) buckets. Macie can detect sensitive data in many different formats, including commonly used compression and archive formats. However, Macie doesn’t support the discovery of sensitive data [...]
