Title: Hackers game out infowar against China with the US Navy
Date: 2025-01-20T18:54:09+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-01-20-hackers-game-out-infowar-against-china-with-the-us-navy

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/20/china_taiwan_wargames/){:target="_blank" rel="noopener"}

> Taipei invites infosec bods to come and play on its home turf Picture this: It's 2030 and China's furious with Taiwan after the island applies to the UN to be recognized as an independent state. After deciding on a full military invasion, China attempts to first cripple its rebellious neighbor's critical infrastructure.... [...]
