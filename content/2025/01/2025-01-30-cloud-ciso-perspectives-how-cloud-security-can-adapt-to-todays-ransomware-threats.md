Title: Cloud CISO Perspectives: How cloud security can adapt to today’s ransomware threats
Date: 2025-01-30T17:00:00+00:00
Author: Iain Mulholland
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2025-01-30-cloud-ciso-perspectives-how-cloud-security-can-adapt-to-todays-ransomware-threats

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-how-cloud-security-can-adapt-ransomware-threats/){:target="_blank" rel="noopener"}

> Welcome to the second Cloud CISO Perspectives for January 2025. Iain Mulholland, senior director, Security Engineering, shares insights on the state of ransomware in the cloud from our new Threat Horizons Report. The research and intelligence in the report should prove helpful to all cloud providers and security professionals. Similarly, the recommended risk mitigations will work well with Google Cloud, but are generally applicable to all clouds. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, [...]
