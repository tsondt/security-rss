Title: Backdoor found in two healthcare patient monitors, linked to IP in China
Date: 2025-01-30T18:31:23-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2025-01-30-backdoor-found-in-two-healthcare-patient-monitors-linked-to-ip-in-china

[Source](https://www.bleepingcomputer.com/news/security/backdoor-found-in-two-healthcare-patient-monitors-linked-to-ip-in-china/){:target="_blank" rel="noopener"}

> The US Cybersecurity and Infrastructure Security Agency (CISA) is warning that Contec CMS8000 devices, a widely used healthcare patient monitoring device, include a backdoor that quietly sends patient data to a remote IP address and downloads and executes files on the device. [...]
