Title: Who is DDoSing you? Rivals, probably, or cheesed-off users
Date: 2025-01-23T10:19:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-23-who-is-ddosing-you-rivals-probably-or-cheesed-off-users

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/23/who_is_ddosing_you_competitors/){:target="_blank" rel="noopener"}

> Plus: 'Largest-ever' duff traffic tsunami clocks in at 5.6 Tbps In addition to Chinese spies invading organizations' networks and ransomware crews locking up sensitive files, botnets blasting distributed denial of service (DDoS) attacks can still cause a world of hurt — and website downtime — and it's quite likely your competitors are to blame.... [...]
