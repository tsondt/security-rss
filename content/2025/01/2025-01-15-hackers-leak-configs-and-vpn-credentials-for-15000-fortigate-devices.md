Title: Hackers leak configs and VPN credentials for 15,000 FortiGate devices
Date: 2025-01-15T21:57:23-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2025-01-15-hackers-leak-configs-and-vpn-credentials-for-15000-fortigate-devices

[Source](https://www.bleepingcomputer.com/news/security/hackers-leak-configs-and-vpn-credentials-for-15-000-fortigate-devices/){:target="_blank" rel="noopener"}

> A new hacking group has leaked the configuration files, IP addresses, and VPN credentials for over 15,000 FortiGate devices for free on the dark web, exposing a great deal of sensitive technical information to other cybercriminals. [...]
