Title: Hackers get $886,250 for 49 zero-days at Pwn2Own Automotive 2025
Date: 2025-01-24T08:00:37-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-24-hackers-get-886250-for-49-zero-days-at-pwn2own-automotive-2025

[Source](https://www.bleepingcomputer.com/news/security/hackers-get-886-250-for-49-zero-days-at-pwn2own-automotive-2025/){:target="_blank" rel="noopener"}

> ​The Pwn2Own Automotive 2025 hacking contest has ended with security researchers collecting $886,250 after exploiting 49 zero-days. [...]
