Title: Apple chips can be hacked to leak secrets from Gmail, iCloud, and more
Date: 2025-01-28T20:56:32+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Apple;Biz & IT;Security;a-series chips;apple;m-series chips;side channels;speculative execution
Slug: 2025-01-28-apple-chips-can-be-hacked-to-leak-secrets-from-gmail-icloud-and-more

[Source](https://arstechnica.com/security/2025/01/newly-discovered-flaws-in-apple-chips-leak-secrets-in-safari-and-chrome/){:target="_blank" rel="noopener"}

> Apple-designed chips powering Macs, iPhones, and iPads contain two newly discovered vulnerabilities that leak credit card information, locations, and other sensitive data from the Chrome and Safari browsers as they visit sites such as iCloud Calendar, Google Maps, and Proton Mail. The vulnerabilities, affecting the CPUs in later generations of Apple A- and M-series chip sets, open them to side channel attacks, a class of exploit that infers secrets by measuring manifestations such as timing, sound, and power consumption. Both side channels are the result of the chips’ use of speculative execution, a performance optimization that improves speed by predicting [...]
