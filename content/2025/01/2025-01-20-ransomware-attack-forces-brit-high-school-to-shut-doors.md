Title: Ransomware attack forces Brit high school to shut doors
Date: 2025-01-20T12:03:01+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-20-ransomware-attack-forces-brit-high-school-to-shut-doors

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/20/blacon_high_school_ransomware/){:target="_blank" rel="noopener"}

> Students have work to complete at home in the meantime A UK high school will have to close for at least two days, today and tomorrow, after becoming the latest public-sector victim of ransomware criminals.... [...]
