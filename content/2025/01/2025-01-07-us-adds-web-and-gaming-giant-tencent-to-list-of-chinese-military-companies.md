Title: US adds web and gaming giant Tencent to list of Chinese military companies
Date: 2025-01-07T06:58:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2025-01-07-us-adds-web-and-gaming-giant-tencent-to-list-of-chinese-military-companies

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/07/tencent_catl_chinese_military_company_list/){:target="_blank" rel="noopener"}

> This could be the start of a saga to rival TikTok’s troubles, and embroil Tesla and Microsoft The US Department of Defense has added Chinese messaging and gaming Tencent to its list of “Chinese military company”, a designation that won’t necessarily result in a ban but is nonetheless unpleasant.... [...]
