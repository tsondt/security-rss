Title: Snyk appears to deploy 'malicious' packages targeting Cursor for unknown reason
Date: 2025-01-14T13:13:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-14-snyk-appears-to-deploy-malicious-packages-targeting-cursor-for-unknown-reason

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/14/snyk_npm_deployment_removed/){:target="_blank" rel="noopener"}

> Packages removed, vendor said to have apologized to AI code editor as onlookers say it could have been a test Updated Developer security company Snyk is at the center of allegations concerning the possible targeting or testing of Cursor, an AI code editor company, using "malicious" packages uploaded to NPM.... [...]
