Title: US healthcare provider data breach impacts 1 million patients
Date: 2025-01-31T08:18:29-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2025-01-31-us-healthcare-provider-data-breach-impacts-1-million-patients

[Source](https://www.bleepingcomputer.com/news/security/data-breach-at-us-healthcare-provider-chc-impacts-1-million-patients/){:target="_blank" rel="noopener"}

> Community Health Center (CHC), a leading Connecticut healthcare provider, is notifying over 1 million patients that their personal and health information was stolen in an October breach. [...]
