Title: Zyxel warns of bad signature update causing firewall boot loops
Date: 2025-01-24T15:39:00-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Technology
Slug: 2025-01-24-zyxel-warns-of-bad-signature-update-causing-firewall-boot-loops

[Source](https://www.bleepingcomputer.com/news/security/zyxel-warns-of-bad-signature-update-causing-firewall-boot-loops/){:target="_blank" rel="noopener"}

> Zyxel is warning that a bad security signature update is causing critical errors for USG FLEX or ATP Series firewalls, including putting the device into a boot loop. [...]
