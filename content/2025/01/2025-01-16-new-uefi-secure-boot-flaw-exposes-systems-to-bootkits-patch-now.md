Title: New UEFI Secure Boot flaw exposes systems to bootkits, patch now
Date: 2025-01-16T10:05:59-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2025-01-16-new-uefi-secure-boot-flaw-exposes-systems-to-bootkits-patch-now

[Source](https://www.bleepingcomputer.com/news/security/new-uefi-secure-boot-flaw-exposes-systems-to-bootkits-patch-now/){:target="_blank" rel="noopener"}

> A new UEFI Secure Boot bypass vulnerability tracked as CVE-2024-7344 that affects a Microsoft-signed application could be exploited to deploy bootkits even if Secure Boot protection is active. [...]
