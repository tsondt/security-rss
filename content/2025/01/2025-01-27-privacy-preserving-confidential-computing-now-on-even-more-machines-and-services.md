Title: Privacy-preserving Confidential Computing now on even more machines and services
Date: 2025-01-27T17:00:00+00:00
Author: Rene Kolga
Category: GCP Security
Tags: Security & Identity
Slug: 2025-01-27-privacy-preserving-confidential-computing-now-on-even-more-machines-and-services

[Source](https://cloud.google.com/blog/products/identity-security/privacy-preserving-confidential-computing-now-on-even-more-machines/){:target="_blank" rel="noopener"}

> Organizations are increasingly using Confidential Computing to help protect their sensitive data in use as part of their data protection efforts. Today, we are excited to highlight new Confidential Computing capabilities that make it easier for organizations of all sizes to adopt this important privacy-preserving technology. 1. Confidential GKE Nodes on the general-purpose C3D machine series for GKE Standard mode, generally available Confidential GKE Nodes enforce data encryption in-use in your Google Kubernetes Engine (GKE) nodes and workloads. Confidential GKE Nodes are built on top of Compute Engine Confidential VMs using AMD Secure Encryption Virtualization (AMD SEV), which encrypts the [...]
