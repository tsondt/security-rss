Title: Friday Squid Blogging: Cotton-and-Squid-Bone Sponge
Date: 2025-01-10T22:06:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;squid
Slug: 2025-01-10-friday-squid-blogging-cotton-and-squid-bone-sponge

[Source](https://www.schneier.com/blog/archives/2025/01/friday-squid-blogging-cotton-and-squid-bone-sponge.html){:target="_blank" rel="noopener"}

> News : A sponge made of cotton and squid bone that has absorbed about 99.9% of microplastics in water samples in China could provide an elusive answer to ubiquitous microplastic pollution in water across the globe, a new report suggests. [...] The study tested the material in an irrigation ditch, a lake, seawater and a pond, where it removed up to 99.9% of plastic. It addressed 95%-98% of plastic after five cycles, which the authors say is remarkable reusability. The sponge is made from chitin extracted from squid bone and cotton cellulose, materials that are often used to address pollution. [...]
