Title: Chinese hackers also breached Charter and Windstream networks
Date: 2025-01-06T10:27:53-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-06-chinese-hackers-also-breached-charter-and-windstream-networks

[Source](https://www.bleepingcomputer.com/news/security/charter-and-windstream-among-nine-us-telecoms-hacked-by-china/){:target="_blank" rel="noopener"}

> ​More U.S. companies have been added to the list of telecommunications firms hacked in a wave of breaches by a Chinese state-backed threat group tracked as Salt Typhoon. [...]
