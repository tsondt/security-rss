Title: Engineering giant Smiths Group discloses security breach
Date: 2025-01-28T12:28:15-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-28-engineering-giant-smiths-group-discloses-security-breach

[Source](https://www.bleepingcomputer.com/news/security/engineering-giant-smiths-group-discloses-security-breach/){:target="_blank" rel="noopener"}

> London-based engineering giant Smiths Group disclosed a security breach after unknown attackers gained access to the company's systems. [...]
