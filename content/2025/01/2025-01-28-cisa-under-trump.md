Title: CISA Under Trump
Date: 2025-01-28T12:09:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cyberattack;cybersecurity;ransomware
Slug: 2025-01-28-cisa-under-trump

[Source](https://www.schneier.com/blog/archives/2025/01/cisa-under-trump.html){:target="_blank" rel="noopener"}

> Jen Easterly is out as the Director of CISA. Read her final interview : There’s a lot of unfinished business. We have made an impact through our ransomware vulnerability warning pilot and our pre-ransomware notification initiative, and I’m really proud of that, because we work on preventing somebody from having their worst day. But ransomware is still a problem. We have been laser-focused on PRC cyber actors. That will continue to be a huge problem. I’m really proud of where we are, but there’s much, much more work to be done. There are things that I think we can continue [...]
