Title: Microsoft Teams phishing attack alerts coming to everyone next month
Date: 2025-01-27T12:43:27-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2025-01-27-microsoft-teams-phishing-attack-alerts-coming-to-everyone-next-month

[Source](https://www.bleepingcomputer.com/news/security/microsoft-teams-phishing-attack-alerts-coming-to-everyone-next-month/){:target="_blank" rel="noopener"}

> Microsoft reminded Microsoft 365 admins that its new brand impersonation protection feature for Teams Chat will be available for all customers by mid-February 2025. [...]
