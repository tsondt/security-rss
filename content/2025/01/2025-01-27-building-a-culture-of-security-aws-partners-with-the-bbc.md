Title: Building a culture of security: AWS partners with the BBC
Date: 2025-01-27T19:55:15+00:00
Author: Carter Spriggs
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Thought Leadership;Security Blog
Slug: 2025-01-27-building-a-culture-of-security-aws-partners-with-the-bbc

[Source](https://aws.amazon.com/blogs/security/building-a-culture-of-security-aws-partners-with-the-bbc/){:target="_blank" rel="noopener"}

> Cybersecurity isn’t just about technology—it’s about people. That’s why Amazon Web Services (AWS) partnered with the BBC to explore the human side of cybersecurity in our latest article, The Human Side of Cybersecurity: Building a Culture of Security, available on the BBC website. In the piece, we spotlight the AWS Security Guardians program and how [...]
