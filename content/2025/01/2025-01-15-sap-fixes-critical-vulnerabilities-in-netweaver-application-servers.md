Title: SAP fixes critical vulnerabilities in NetWeaver application servers
Date: 2025-01-15T17:02:15-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-15-sap-fixes-critical-vulnerabilities-in-netweaver-application-servers

[Source](https://www.bleepingcomputer.com/news/security/sap-fixes-critical-vulnerabilities-in-netweaver-application-servers/){:target="_blank" rel="noopener"}

> SAP has fixed two critical vulnerabilities affecting NetWeaver web application server that could be exploited to escalate privileges and access restricted information. [...]
