Title: FBI: North Korean IT workers steal source code to extort employers
Date: 2025-01-23T15:55:12-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-23-fbi-north-korean-it-workers-steal-source-code-to-extort-employers

[Source](https://www.bleepingcomputer.com/news/security/fbi-north-korean-it-workers-steal-source-code-to-extort-employers/){:target="_blank" rel="noopener"}

> The FBI warned today that North Korean IT workers are abusing their access to steal source code and extort U.S. companies that have been tricked into hiring them. [...]
