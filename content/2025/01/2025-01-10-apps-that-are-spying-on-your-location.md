Title: Apps That Are Spying on Your Location
Date: 2025-01-10T16:27:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;adware;cyberespionage;data collection;geolocation;hacking
Slug: 2025-01-10-apps-that-are-spying-on-your-location

[Source](https://www.schneier.com/blog/archives/2025/01/apps-that-are-spying-on-your-location.html){:target="_blank" rel="noopener"}

> 404 Media is reporting on all the apps that are spying on your location, based on a hack of the location data company Gravy Analytics: The thousands of apps, included in hacked files from location data company Gravy Analytics, include everything from games like Candy Crush to dating apps like Tinder, to pregnancy tracking and religious prayer apps across both Android and iOS. Because much of the collection is occurring through the advertising ecosystem­—not code developed by the app creators themselves—­this data collection is likely happening both without users’ and even app developers’ knowledge. [...]
