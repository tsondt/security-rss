Title: PayPal to pay $2 million settlement over 2022 data breach
Date: 2025-01-25T10:15:25-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2025-01-25-paypal-to-pay-2-million-settlement-over-2022-data-breach

[Source](https://www.bleepingcomputer.com/news/security/paypal-to-pay-2-million-settlement-over-2022-data-breach/){:target="_blank" rel="noopener"}

> New York State has announced a $2,000,000 settlement with PayPal over charges it failed to comply with the state's cybersecurity regulations, leading to a 2022 data breach. [...]
