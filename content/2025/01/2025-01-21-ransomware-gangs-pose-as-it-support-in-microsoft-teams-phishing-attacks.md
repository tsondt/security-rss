Title: Ransomware gangs pose as IT support in Microsoft Teams phishing attacks
Date: 2025-01-21T10:59:29-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-21-ransomware-gangs-pose-as-it-support-in-microsoft-teams-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gangs-pose-as-it-support-in-microsoft-teams-phishing-attacks/){:target="_blank" rel="noopener"}

> Ransomware gangs are increasingly adopting email bombing followed by posing as tech support in Microsoft Teams calls to trick employees into allowing remote control and install malware that provides access to the company network. [...]
