Title: TalkTalk investigates breach after data for sale on hacking forum
Date: 2025-01-25T16:23:24-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2025-01-25-talktalk-investigates-breach-after-data-for-sale-on-hacking-forum

[Source](https://www.bleepingcomputer.com/news/security/talktalk-investigates-breach-after-data-for-sale-on-hacking-forum/){:target="_blank" rel="noopener"}

> UK telecommunications company TalkTalk is investigating a third-party supplier data breach after a threat actor began selling alleged customer data on a hacking forum. [...]
