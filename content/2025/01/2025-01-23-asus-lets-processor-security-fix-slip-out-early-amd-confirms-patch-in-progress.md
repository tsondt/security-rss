Title: Asus lets processor security fix slip out early, AMD confirms patch in progress
Date: 2025-01-23T07:19:08+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-01-23-asus-lets-processor-security-fix-slip-out-early-amd-confirms-patch-in-progress

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/23/asus_amd_processor_fix/){:target="_blank" rel="noopener"}

> Answers on a postcard to what 'Microcode Signature Verification Vulnerability' might mean AMD has confirmed at least some of its microprocessors suffer a microcode-related security vulnerability, the existence of which accidentally emerged this month after a fix for the flaw appeared in a beta BIOS update from PC maker Asus.... [...]
