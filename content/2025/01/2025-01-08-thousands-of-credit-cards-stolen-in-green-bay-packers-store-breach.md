Title: Thousands of credit cards stolen in Green Bay Packers store breach
Date: 2025-01-08T10:05:50-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-08-thousands-of-credit-cards-stolen-in-green-bay-packers-store-breach

[Source](https://www.bleepingcomputer.com/news/security/thousands-of-credit-cards-stolen-in-green-bay-packers-store-breach/){:target="_blank" rel="noopener"}

> ​American football team Green Bay Packers says cybercriminals stole the credit card data of over 8,500 customers after hacking its official Pro Shop online retail store in a September breach. [...]
