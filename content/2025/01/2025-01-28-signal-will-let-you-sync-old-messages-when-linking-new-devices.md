Title: Signal will let you sync old messages when linking new devices
Date: 2025-01-28T11:27:02-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile;Software
Slug: 2025-01-28-signal-will-let-you-sync-old-messages-when-linking-new-devices

[Source](https://www.bleepingcomputer.com/news/security/signal-will-let-you-sync-old-messages-when-linking-new-devices/){:target="_blank" rel="noopener"}

> Signal is finally adding a new feature that allows users to synchronize their old message history from their primary iOS or Android devices to newly linked devices like desktops and iPads. [...]
