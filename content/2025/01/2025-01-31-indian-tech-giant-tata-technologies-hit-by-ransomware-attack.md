Title: Indian tech giant Tata Technologies hit by ransomware attack
Date: 2025-01-31T11:02:22-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-31-indian-tech-giant-tata-technologies-hit-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/indian-tech-giant-tata-technologies-hit-by-ransomware-attack/){:target="_blank" rel="noopener"}

> Tata Technologies Ltd. had to suspend some of its IT services following a ransomware attack that impacted the company network. [...]
