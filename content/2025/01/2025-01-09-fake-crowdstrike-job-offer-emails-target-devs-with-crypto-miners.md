Title: Fake CrowdStrike job offer emails target devs with crypto miners
Date: 2025-01-09T16:30:07-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-09-fake-crowdstrike-job-offer-emails-target-devs-with-crypto-miners

[Source](https://www.bleepingcomputer.com/news/security/fake-crowdstrike-job-offer-emails-target-devs-with-crypto-miners/){:target="_blank" rel="noopener"}

> CrowdStrike is warning that a phishing campaign is impersonating the cybersecurity company in fake job offer emails to trick targets into infecting themselves with a Monero cryptocurrency miner (XMRig). [...]
