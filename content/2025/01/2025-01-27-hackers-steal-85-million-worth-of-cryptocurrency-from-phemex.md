Title: Hackers steal $85 million worth of cryptocurrency from Phemex
Date: 2025-01-27T13:03:02-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2025-01-27-hackers-steal-85-million-worth-of-cryptocurrency-from-phemex

[Source](https://www.bleepingcomputer.com/news/security/hackers-steal-85-million-worth-of-cryptocurrency-from-phemex/){:target="_blank" rel="noopener"}

> The Phemex crypto exchange suffered a massive security breach on Thursday where threat actors stole over $85 million worth of cryptocurrency. [...]
