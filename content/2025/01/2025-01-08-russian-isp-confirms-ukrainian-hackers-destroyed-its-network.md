Title: Russian ISP confirms Ukrainian hackers "destroyed" its network
Date: 2025-01-08T14:26:04-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-08-russian-isp-confirms-ukrainian-hackers-destroyed-its-network

[Source](https://www.bleepingcomputer.com/news/security/russian-isp-confirms-ukrainian-hackers-destroyed-its-network/){:target="_blank" rel="noopener"}

> Russian internet service provider Nodex confirmed on Tuesday that its network was "destroyed" in a cyberattack claimed by Ukrainian hacktivists part of the Ukrainian Cyber Alliance [...]
