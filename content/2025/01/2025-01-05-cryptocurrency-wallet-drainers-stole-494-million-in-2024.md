Title: Cryptocurrency wallet drainers stole $494 million in 2024
Date: 2025-01-05T10:11:21-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2025-01-05-cryptocurrency-wallet-drainers-stole-494-million-in-2024

[Source](https://www.bleepingcomputer.com/news/security/cryptocurrency-wallet-drainers-stole-494-million-in-2024/){:target="_blank" rel="noopener"}

> Scammers stole $494 million worth of cryptocurrency in wallet drainer attacks last year that targeted more than 300,000 wallet addresses. [...]
