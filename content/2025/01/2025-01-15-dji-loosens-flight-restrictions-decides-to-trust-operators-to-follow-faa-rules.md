Title: DJI loosens flight restrictions, decides to trust operators to follow FAA rules
Date: 2025-01-15T22:30:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2025-01-15-dji-loosens-flight-restrictions-decides-to-trust-operators-to-follow-faa-rules

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/15/dji_ditch_geofencing/){:target="_blank" rel="noopener"}

> Right after one of its drones crashed into an aircraft fighting California wildfires? Great timing Drone maker DJI has decided to scale back its geofencing restrictions, meaning its software won't automatically stop operators from flying into areas flagged as no-fly zones.... [...]
