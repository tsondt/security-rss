Title: Over 3 million mail servers without encryption exposed to sniffing attacks
Date: 2025-01-02T10:54:53-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-02-over-3-million-mail-servers-without-encryption-exposed-to-sniffing-attacks

[Source](https://www.bleepingcomputer.com/news/security/over-3-million-mail-servers-without-encryption-exposed-to-sniffing-attacks/){:target="_blank" rel="noopener"}

> Over three million POP3 and IMAP mail servers without TLS encryption are currently exposed on the Internet and vulnerable to network sniffing attacks. [...]
