Title: Hackers use FastHTTP in new high-speed Microsoft 365 password attacks
Date: 2025-01-14T10:57:07-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-14-hackers-use-fasthttp-in-new-high-speed-microsoft-365-password-attacks

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-fasthttp-in-new-high-speed-microsoft-365-password-attacks/){:target="_blank" rel="noopener"}

> Threat actors are utilizing the FastHTTP Go library to launch high-speed brute-force password attacks targeting Microsoft 365 accounts globally. [...]
