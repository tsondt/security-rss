Title: One of Salt Typhoon's favorite flaws still wide open on 91% of at-risk Exchange Servers
Date: 2025-01-23T23:30:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-23-one-of-salt-typhoons-favorite-flaws-still-wide-open-on-91-of-at-risk-exchange-servers

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/23/proxylogon_flaw_salt_typhoons_open/){:target="_blank" rel="noopener"}

> But we mean, you've had nearly four years to patch One of the critical security flaws exploited by China's Salt Typhoon to breach US telecom and government networks has had a patch available for nearly four years - yet despite repeated warnings from law enforcement and private-sector security firms, nearly all public-facing Microsoft Exchange Server instances with this vulnerability remain unpatched.... [...]
