Title: Microsoft sues service for creating illicit content with its AI platform
Date: 2025-01-10T23:10:57+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: AI;Biz & IT;Policy;Security;Artificial Intelligence;generative ai;microsoft
Slug: 2025-01-10-microsoft-sues-service-for-creating-illicit-content-with-its-ai-platform

[Source](https://arstechnica.com/security/2025/01/microsoft-sues-service-for-creating-illicit-content-with-its-ai-platform/){:target="_blank" rel="noopener"}

> Microsoft is accusing three individuals of running a "hacking-as-a-service" scheme that was designed to allow the creation of harmful and illicit content using the company’s platform for AI-generated content. The foreign-based defendants developed tools specifically designed to bypass safety guardrails Microsoft has erected to prevent the creation of harmful content through its generative AI services, said Steven Masada, the assistant general counsel for Microsoft’s Digital Crimes Unit. They then compromised the legitimate accounts of paying customers. They combined those two things to create a fee-based platform people could use. A sophisticated scheme Microsoft is also suing seven individuals it says [...]
