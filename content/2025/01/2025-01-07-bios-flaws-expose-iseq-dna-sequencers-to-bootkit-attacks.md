Title: BIOS flaws expose iSeq DNA sequencers to bootkit attacks
Date: 2025-01-07T14:02:41-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2025-01-07-bios-flaws-expose-iseq-dna-sequencers-to-bootkit-attacks

[Source](https://www.bleepingcomputer.com/news/security/bios-flaws-expose-iseq-dna-sequencers-to-bootkit-attacks/){:target="_blank" rel="noopener"}

> BIOS/UEFI vulnerabilities in the iSeq 100 DNA sequencer from U.S. biotechnology company Illumina could let attackers disable devices used for detecting illnesses and developing vaccines. [...]
