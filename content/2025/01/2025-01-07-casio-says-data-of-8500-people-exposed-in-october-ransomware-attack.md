Title: Casio says data of 8,500 people exposed in October ransomware attack
Date: 2025-01-07T16:56:52-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-07-casio-says-data-of-8500-people-exposed-in-october-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/casio-says-data-of-8-500-people-exposed-in-october-ransomware-attack/){:target="_blank" rel="noopener"}

> Japanese electronics manufacturer Casio says that the October 2024 ransomware incident exposed the personal data of approximately 8,500 people. [...]
