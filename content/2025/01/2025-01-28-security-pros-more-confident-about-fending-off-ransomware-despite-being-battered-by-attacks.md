Title: Security pros more confident about fending off ransomware, despite being battered by attacks
Date: 2025-01-28T14:02:44+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-28-security-pros-more-confident-about-fending-off-ransomware-despite-being-battered-by-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/28/research_security_pros_gain_ransomware/){:target="_blank" rel="noopener"}

> Data leak, shmata leak. It will all work out, right? IT and security pros say they are more confident in their ability to manage ransomware attacks after nearly nine in ten (88 percent) were forced to contain efforts by criminals to breach their defenses in the past year.... [...]
