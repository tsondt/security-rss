Title: Laravel admin package Voyager vulnerable to one-click RCE flaw
Date: 2025-01-29T14:27:19-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-29-laravel-admin-package-voyager-vulnerable-to-one-click-rce-flaw

[Source](https://www.bleepingcomputer.com/news/security/laravel-admin-package-voyager-vulnerable-to-one-click-rce-flaw/){:target="_blank" rel="noopener"}

> Three vulnerabilities discovered in the open-source PHP package Voyager for managing Laravel applications could be used for remote code execution attacks. [...]
