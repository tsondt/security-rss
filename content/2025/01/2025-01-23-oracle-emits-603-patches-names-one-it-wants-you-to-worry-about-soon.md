Title: Oracle emits 603 patches, names one it wants you to worry about soon
Date: 2025-01-23T01:06:44+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-01-23-oracle-emits-603-patches-names-one-it-wants-you-to-worry-about-soon

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/23/oracle_patch_linux/){:target="_blank" rel="noopener"}

> Old flaws that keep causing trouble haunt Big Red Oracle has delivered its regular quarterly collection of patches: 603 in total, 318 for its own products, and another 285 for Linux code it ships.... [...]
