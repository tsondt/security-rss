Title: Telegram captcha tricks you into running malicious PowerShell scripts
Date: 2025-01-22T15:35:44-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-22-telegram-captcha-tricks-you-into-running-malicious-powershell-scripts

[Source](https://www.bleepingcomputer.com/news/security/telegram-captcha-tricks-you-into-running-malicious-powershell-scripts/){:target="_blank" rel="noopener"}

> Threat actors on X are exploiting the news around Ross Ulbricht to direct unsuspecting users to a Telegram channel that tricks them into executing PowerShell code that infects them with malware. [...]
