Title: Trump 'waved a white flag to Chinese hackers' as Homeland Security axed cyber advisory boards
Date: 2025-01-22T21:30:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-22-trump-waved-a-white-flag-to-chinese-hackers-as-homeland-security-axed-cyber-advisory-boards

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/22/dhs_axes_cyber_advisory_boards/){:target="_blank" rel="noopener"}

> And: America 'has never been less secure,' retired rear admiral tells Congress The Trump administration gutted key cybersecurity advisory boards in its first days, as expert witnesses warned Congress of potentially destructive cyberattacks by China.... [...]
