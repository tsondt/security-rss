Title: Biz tax rises, inflation and high interest. Why fewer UK tech firms started in 2024
Date: 2025-01-23T09:30:08+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2025-01-23-biz-tax-rises-inflation-and-high-interest-why-fewer-uk-tech-firms-started-in-2024

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/23/uk_startup_incorporation_drop/){:target="_blank" rel="noopener"}

> And the government thinks that AI and taking shackles off big tech will help? God help Britain For the first time since the start of the pandemic, the number of tech firms incorporated in the UK has declined, with a shrinking economy, as well as high inflation and interest rates causing a slump in business confidence.... [...]
