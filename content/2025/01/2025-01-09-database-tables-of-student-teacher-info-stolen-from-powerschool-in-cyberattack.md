Title: Database tables of student, teacher info stolen from PowerSchool in cyberattack
Date: 2025-01-09T00:44:13+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-01-09-database-tables-of-student-teacher-info-stolen-from-powerschool-in-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/09/powerschool_school_data/){:target="_blank" rel="noopener"}

> Class act: Biz only serves 60M people across America, no biggie A leading education software maker has admitted its IT environment was compromised in a cyberattack, with students and teachers' personal data – including some Social Security Numbers and medical info – stolen.... [...]
