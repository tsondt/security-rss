Title: Medical billing firm Medusind discloses breach affecting 360,000 people
Date: 2025-01-08T12:28:01-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2025-01-08-medical-billing-firm-medusind-discloses-breach-affecting-360000-people

[Source](https://www.bleepingcomputer.com/news/security/medical-billing-firm-medusind-discloses-breach-affecting-360-000-people/){:target="_blank" rel="noopener"}

> ​Medusind, a leading billing provider for healthcare organizations, is notifying hundreds of thousands of individuals of a data breach that exposed their personal and health information more than a year ago, in December 2023. [...]
