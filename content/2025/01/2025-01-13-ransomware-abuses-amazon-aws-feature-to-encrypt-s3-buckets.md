Title: Ransomware abuses Amazon AWS feature to encrypt S3 buckets
Date: 2025-01-13T10:27:22-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud
Slug: 2025-01-13-ransomware-abuses-amazon-aws-feature-to-encrypt-s3-buckets

[Source](https://www.bleepingcomputer.com/news/security/ransomware-abuses-amazon-aws-feature-to-encrypt-s3-buckets/){:target="_blank" rel="noopener"}

> A new ransomware campaign encrypts Amazon S3 buckets using AWS's Server-Side Encryption with Customer Provided Keys (SSE-C) known only to the threat actor, demanding ransoms to receive the decryption key. [...]
