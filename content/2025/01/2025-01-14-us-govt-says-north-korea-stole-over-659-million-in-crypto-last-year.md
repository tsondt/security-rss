Title: US govt says North Korea stole over $659 million in crypto last year
Date: 2025-01-14T15:01:02-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2025-01-14-us-govt-says-north-korea-stole-over-659-million-in-crypto-last-year

[Source](https://www.bleepingcomputer.com/news/security/us-govt-says-north-korea-stole-over-659-million-in-crypto-last-year/){:target="_blank" rel="noopener"}

> ​North Korean state-backed hacking groups have stolen over $659 million worth of cryptocurrency in multiple crypto-heists, according to a joint statement issued by the United States, South Korea, and Japan on Tuesday. [...]
