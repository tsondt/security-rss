Title: MFA Failures - The Worst is Yet to Come
Date: 2025-01-16T10:01:11-05:00
Author: Sponsored by Token
Category: BleepingComputer
Tags: Security
Slug: 2025-01-16-mfa-failures-the-worst-is-yet-to-come

[Source](https://www.bleepingcomputer.com/news/security/mfa-failures-the-worst-is-yet-to-come/){:target="_blank" rel="noopener"}

> This article delves into the rising tide of MFA failures, the alarming role of generative AI in amplifying these attacks, the growing user discontent weakening our defenses, and the glaring vulnerabilities being frequently exploited. The storm is building, and the worst is yet to come. [...]
