Title: Preventing unintended encryption of Amazon S3 objects
Date: 2025-01-16T02:43:53+00:00
Author: Steve de Vera
Category: AWS Security
Tags: Advanced (300);Best Practices;Security, Identity, & Compliance;Incident response;ransomware;S3;Security;Security Blog;threat detection
Slug: 2025-01-16-preventing-unintended-encryption-of-amazon-s3-objects

[Source](https://aws.amazon.com/blogs/security/preventing-unintended-encryption-of-amazon-s3-objects/){:target="_blank" rel="noopener"}

> At Amazon Web Services (AWS), the security of our customers’ data is our top priority, and it always will be. Recently, the AWS Customer Incident Response Team (CIRT) and our automated security monitoring systems identified an increase in unusual encryption activity associated with Amazon Simple Storage Service (Amazon S3) buckets. Working with customers, our security [...]
