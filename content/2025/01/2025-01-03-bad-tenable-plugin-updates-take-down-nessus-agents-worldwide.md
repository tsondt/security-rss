Title: Bad Tenable plugin updates take down Nessus agents worldwide
Date: 2025-01-03T17:57:59-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-03-bad-tenable-plugin-updates-take-down-nessus-agents-worldwide

[Source](https://www.bleepingcomputer.com/news/security/bad-tenable-plugin-updates-take-down-nessus-agents-worldwide/){:target="_blank" rel="noopener"}

> Tenable says customers must manually upgrade their software to revive Nessus vulnerability scanner agents taken offline on December 31st due to buggy differential plugin updates. [...]
