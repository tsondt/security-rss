Title: Friday Squid Blogging: Beaked Whales Feed on Squid
Date: 2025-01-24T22:01:29+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2025-01-24-friday-squid-blogging-beaked-whales-feed-on-squid

[Source](https://www.schneier.com/blog/archives/2025/01/friday-squid-blogging-beaked-whales-feed-on-squid.html){:target="_blank" rel="noopener"}

> A Travers’ beaked whale ( Mesoplodon traversii ) washed ashore in New Zealand, and scientists conlcuded that “the prevalence of squid remains [in its stomachs] suggests that these deep-sea cephalopods form a significant part of the whale’s diet, similar to other beaked whale species.” Blog moderation policy. [...]
