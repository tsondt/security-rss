Title: A Day in the Life of a Prolific Voice Phishing Crew
Date: 2025-01-07T23:41:53+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;The Coming Storm;Web Fraud 2.0;800-275-2273;Allison Nixon;Aristotle;autodoxers;Coinbase;Crypto Chameleon;Discord;domaintools;Lookout;Mark Cuban;Okta;Perm;Shark Tank;Star Fraud;Stotle;telegram;Trezor;Unit 221B;voice phishing
Slug: 2025-01-07-a-day-in-the-life-of-a-prolific-voice-phishing-crew

[Source](https://krebsonsecurity.com/2025/01/a-day-in-the-life-of-a-prolific-voice-phishing-crew/){:target="_blank" rel="noopener"}

> Besieged by scammers seeking to phish user accounts over the telephone, Apple and Google frequently caution that they will never reach out unbidden to users this way. However, new details about the internal operations of a prolific voice phishing gang show the group routinely abuses legitimate services at Apple and Google to force a variety of outbound communications to their users, including emails, automated phone calls and system-level messages sent to all signed-in devices. Image: Shutterstock, iHaMoo. KrebsOnSecurity recently told the saga of a cryptocurrency investor named Tony who was robbed of more than $4.7 million in an elaborate voice [...]
