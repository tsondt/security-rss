Title: US charges operators of cryptomixers linked to ransomware gangs
Date: 2025-01-10T12:59:25-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-10-us-charges-operators-of-cryptomixers-linked-to-ransomware-gangs

[Source](https://www.bleepingcomputer.com/news/security/us-charges-operators-of-cryptomixers-linked-to-ransomware-gangs/){:target="_blank" rel="noopener"}

> The U.S. Department of Justice indicted three operators of sanctioned Blender.io and Sinbad.io crypto mixer services used by ransomware gangs and North Korean hackers to launder ransoms and stolen cryptocurrency. [...]
