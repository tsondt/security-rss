Title: Datacus extractus: Harry Potter publisher breached without resorting to magic
Date: 2025-01-20T05:27:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2025-01-20-datacus-extractus-harry-potter-publisher-breached-without-resorting-to-magic

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/20/harry_potter_publisher_breach/){:target="_blank" rel="noopener"}

> PLUS: Allstate sued for allegedly tracking drivers; Dutch DDoS; More fake jobs from Pyongyang; and more Infosec in brief Hogwarts doesn’t teach an incantation that could have saved Harry Potter publisher Scholastic from feeling the power of an online magician who made off with millions of customer records - except perhaps the wizardry of multifactor authentication.... [...]
