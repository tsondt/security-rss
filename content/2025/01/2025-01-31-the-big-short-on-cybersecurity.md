Title: The Big Short on Cybersecurity
Date: 2025-01-31T14:30:15+00:00
Author: Stephen Tutterow, Sales Engineer Team Lead at Pentera
Category: The Register
Tags: 
Slug: 2025-01-31-the-big-short-on-cybersecurity

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/31/the_big_short_on_cybersecurity/){:target="_blank" rel="noopener"}

> How to communicate risk to executives Partner Content Have you ever watched ? It's one of my all-time favorite movies, not just for the story but for how it handles complexity.... [...]
