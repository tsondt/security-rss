Title: Trump admin's purge of US cyber advisory boards was 'foolish,' says ex-Navy admiral
Date: 2025-01-30T18:15:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-30-trump-admins-purge-of-us-cyber-advisory-boards-was-foolish-says-ex-navy-admiral

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/30/gutting_us_cyber_advisory_boards/){:target="_blank" rel="noopener"}

> ‘No one was kicked off the NTSB in the middle of investigating a crash’ interview Gutting the Cyber Safety Review Board as it was investigating how China's Salt Typhoon breached American government and telecommunications networks was "foolish" and "bad for national security," according to retired US Navy Rear Admiral Mark Montgomery.... [...]
