Title: Microsoft patches Windows to eliminate Secure Boot bypass threat
Date: 2025-01-16T13:24:17+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;firmware;secure boot;uefi;Windows
Slug: 2025-01-16-microsoft-patches-windows-to-eliminate-secure-boot-bypass-threat

[Source](https://arstechnica.com/security/2025/01/microsoft-patches-windows-to-eliminate-secure-boot-bypass-threat/){:target="_blank" rel="noopener"}

> For the past seven months—and likely longer—an industry-wide standard that protects Windows devices from firmware infections could be bypassed using a simple technique. On Tuesday, Microsoft finally patched the vulnerability. The status of Linux systems is still unclear. Tracked as CVE-2024-7344, the vulnerability made it possible for attackers who had already gained privileged access to a device to run malicious firmware during bootup. These types of attacks can be particularly pernicious because infections hide inside the firmware that runs at an early stage, before even Windows or Linux has loaded. This strategic position allows the malware to evade defenses installed [...]
