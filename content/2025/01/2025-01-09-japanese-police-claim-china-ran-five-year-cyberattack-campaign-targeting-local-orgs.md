Title: Japanese police claim China ran five-year cyberattack campaign targeting local orgs
Date: 2025-01-09T03:56:11+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2025-01-09-japanese-police-claim-china-ran-five-year-cyberattack-campaign-targeting-local-orgs

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/09/japan_mirrorface_china_attack/){:target="_blank" rel="noopener"}

> ‘MirrorFace’ group found ways to run malware in the Windows sandbox, which may be worrying Japan’s National Police Agency and Center of Incident Readiness and Strategy for Cybersecurity have confirmed third party reports of attacks on local orgs by publishing details of a years-long series of attacks attributed to a China-backed source.... [...]
