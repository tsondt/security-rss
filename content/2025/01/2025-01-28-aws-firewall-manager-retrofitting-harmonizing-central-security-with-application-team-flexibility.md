Title: AWS Firewall Manager retrofitting: Harmonizing central security with application team flexibility
Date: 2025-01-28T17:00:18+00:00
Author: Ian Olson
Category: AWS Security
Tags: Advanced (300);AWS Firewall Manager;AWS WAF;Security, Identity, & Compliance;Technical How-to;AWS Web Application Firewall;Security Blog;WAF
Slug: 2025-01-28-aws-firewall-manager-retrofitting-harmonizing-central-security-with-application-team-flexibility

[Source](https://aws.amazon.com/blogs/security/aws-firewall-manager-retrofitting-harmonizing-central-security-with-application-team-flexibility/){:target="_blank" rel="noopener"}

> AWS Firewall Manager is a powerful tool that organizations can use to define common AWS WAF rules with centralized security policies. These policies specify which accounts and resources are in scope. Firewall Manager creates a web access control list (web ACL) that adheres to the organization’s policy requirements and associates it with the in-scope resources. [...]
