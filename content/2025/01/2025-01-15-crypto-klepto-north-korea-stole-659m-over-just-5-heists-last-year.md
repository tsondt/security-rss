Title: Crypto klepto North Korea stole $659M over just 5 heists last year
Date: 2025-01-15T14:45:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-15-crypto-klepto-north-korea-stole-659m-over-just-5-heists-last-year

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/15/north_korea_crypto_heists/){:target="_blank" rel="noopener"}

> US, Japan, South Korea vow to intensify counter efforts North Korean blockchain bandits stole more than half a billion dollars in cryptocurrency in 2024 alone, the US, Japan, and South Korea say.... [...]
