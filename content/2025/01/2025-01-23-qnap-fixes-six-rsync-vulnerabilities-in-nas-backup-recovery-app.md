Title: QNAP fixes six Rsync vulnerabilities in NAS backup, recovery app
Date: 2025-01-23T13:30:26-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-23-qnap-fixes-six-rsync-vulnerabilities-in-nas-backup-recovery-app

[Source](https://www.bleepingcomputer.com/news/security/qnap-fixes-six-rsync-vulnerabilities-in-hbs-nas-backup-recovery-app/){:target="_blank" rel="noopener"}

> QNAP has fixed six rsync vulnerabilities that could let attackers gain remote code execution on unpatched Network Attached Storage (NAS) devices. [...]
