Title: W3 Total Cache plugin flaw exposes 1 million WordPress sites to attacks
Date: 2025-01-16T15:36:43-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-16-w3-total-cache-plugin-flaw-exposes-1-million-wordpress-sites-to-attacks

[Source](https://www.bleepingcomputer.com/news/security/w3-total-cache-plugin-flaw-exposes-1-million-wordpress-sites-to-attacks/){:target="_blank" rel="noopener"}

> A severe flaw in the W3 Total Cache plugin installed on more than one million WordPress sites could give attackers access to various information, including metadata on cloud-based apps. [...]
