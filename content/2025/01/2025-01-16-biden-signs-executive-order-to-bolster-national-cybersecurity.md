Title: Biden signs executive order to bolster national cybersecurity
Date: 2025-01-16T12:58:14-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-16-biden-signs-executive-order-to-bolster-national-cybersecurity

[Source](https://www.bleepingcomputer.com/news/security/biden-signs-executive-order-to-bolster-national-cybersecurity/){:target="_blank" rel="noopener"}

> Days before leaving office, President Joe Biden signed an executive order to shore up the United States' cybersecurity by making it easier to sanction hacking groups targeting federal agencies and the nation's critical infrastructure. [...]
