Title: Backdoor infecting VPNs used “magic packets” for stealth and security
Date: 2025-01-23T23:42:29+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;backdoors;juniper;magic packets;vpns
Slug: 2025-01-23-backdoor-infecting-vpns-used-magic-packets-for-stealth-and-security

[Source](https://arstechnica.com/security/2025/01/backdoor-infecting-vpns-used-magic-packets-for-stealth-and-security/){:target="_blank" rel="noopener"}

> When threat actors use backdoor malware to gain access to a network, they want to make sure all their hard work can’t be leveraged by competing groups or detected by defenders. One countermeasure is to equip the backdoor with a passive agent that remains dormant until it receives what’s known in the business as a “magic packet.” On Thursday, researchers revealed that a never-before-seen backdoor that quietly took hold of dozens of enterprise VPNs running Juniper Network’s Junos OS has been doing just that. J-Magic, the tracking name for the backdoor, goes one step further to prevent unauthorized access. After [...]
