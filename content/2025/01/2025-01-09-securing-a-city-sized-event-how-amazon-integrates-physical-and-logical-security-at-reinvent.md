Title: Securing a city-sized event: How Amazon integrates physical and logical security at re:Invent
Date: 2025-01-09T17:00:16+00:00
Author: Steve Schmidt
Category: AWS Security
Tags: AWS re:Invent;Best Practices;Intermediate (200);Security, Identity, & Compliance;Thought Leadership;AWS Re:Invent;Security Blog
Slug: 2025-01-09-securing-a-city-sized-event-how-amazon-integrates-physical-and-logical-security-at-reinvent

[Source](https://aws.amazon.com/blogs/security/securing-a-city-sized-event-how-amazon-integrates-physical-and-logical-security-at-reinvent/){:target="_blank" rel="noopener"}

> Securing an event of the magnitude of AWS re:Invent—the Amazon Web Services annual conference in Las Vegas—is no small feat. The most recent event, in December, operated on the scale of a small city, spanning seven venues over twelve miles and nearly seven million square feet across the bustling Las Vegas Strip. Keeping all 60,000 [...]
