Title: Sweden seizes cargo ship after another undersea cable hit in suspected sabotage
Date: 2025-01-27T13:25:09+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2025-01-27-sweden-seizes-cargo-ship-after-another-undersea-cable-hit-in-suspected-sabotage

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/27/sweden_seizes_ship/){:target="_blank" rel="noopener"}

> NATO increasing patrols in the Baltic as region awaits navy drones Swedish authorities have "seized" a vessel – believed to be the cargo ship Vezhen – "suspected of carrying out sabotage" after a cable running between Sweden and Latvia in the Baltic Sea was damaged on the morning of January 26.... [...]
