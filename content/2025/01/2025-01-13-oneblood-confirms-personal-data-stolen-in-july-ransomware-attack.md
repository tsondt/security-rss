Title: OneBlood confirms personal data stolen in July ransomware attack
Date: 2025-01-13T17:36:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2025-01-13-oneblood-confirms-personal-data-stolen-in-july-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/oneblood-confirms-personal-data-stolen-in-july-ransomware-attack/){:target="_blank" rel="noopener"}

> Blood-donation not-for-profit OneBlood confirms that donors' personal information was stolen in a ransomware attack last summer. [...]
