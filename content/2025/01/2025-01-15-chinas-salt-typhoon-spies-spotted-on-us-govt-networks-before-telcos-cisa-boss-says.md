Title: China's Salt Typhoon spies spotted on US govt networks before telcos, CISA boss says
Date: 2025-01-15T20:30:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-15-chinas-salt-typhoon-spies-spotted-on-us-govt-networks-before-telcos-cisa-boss-says

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/15/salt_typhoon_us_govt_networks/){:target="_blank" rel="noopener"}

> We are only seeing 'the tip of the iceberg,' Easterly warns Beijing's Salt Typhoon cyberspies had been seen in US government networks before telcos discovered the same foreign intruders in their own systems, according to CISA boss Jen Easterly.... [...]
