Title: Infosec was literally the last item in Trump's policy plan, yet major changes are likely on his watch
Date: 2025-01-22T13:15:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-22-infosec-was-literally-the-last-item-in-trumps-policy-plan-yet-major-changes-are-likely-on-his-watch

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/22/trump_cyber_policy/){:target="_blank" rel="noopener"}

> Everyone agrees defense matters. How to do it is up for debate Feature The Trump administration came to office this week without a detailed information security policy, but analysis of cabinet nominees’ public remarks and expert comments suggest it will make significant changes in the field.... [...]
