Title: Nuclei flaw bypasses template signature checks to execute commands
Date: 2025-01-04T17:59:25-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2025-01-04-nuclei-flaw-bypasses-template-signature-checks-to-execute-commands

[Source](https://www.bleepingcomputer.com/news/security/nuclei-flaw-bypasses-template-signature-checks-to-execute-commands/){:target="_blank" rel="noopener"}

> A now-fixed vulnerability in the open-source vulnerability scanner Nuclei could potentially allow attackers to bypass signature verification while sneaking malicious code into templates that execute on local systems. [...]
