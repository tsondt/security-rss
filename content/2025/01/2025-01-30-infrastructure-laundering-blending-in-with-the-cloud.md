Title: Infrastructure Laundering: Blending in with the Cloud
Date: 2025-01-30T17:10:08+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;Time to Patch;Web Fraud 2.0;ACB Group;Amazon AWS;Anjie CDN;Crowell & Moring LLP;Fangneng CDN;Funnull;infrastructure laundering;Microsoft Azure;NETSCOUT;NoName057(16);polyfill;Richard Hummel;Silent Push;Suncity Group;U.S. Department of Commerce;Zach Edwards
Slug: 2025-01-30-infrastructure-laundering-blending-in-with-the-cloud

[Source](https://krebsonsecurity.com/2025/01/infrastructure-laundering-blending-in-with-the-cloud/){:target="_blank" rel="noopener"}

> Image: Shutterstock, ArtHead. In an effort to blend in and make their malicious traffic tougher to block, hosting firms catering to cybercriminals in China and Russia increasingly are funneling their operations through major U.S. cloud providers. Research published this week on one such outfit — a sprawling network tied to Chinese organized crime gangs and aptly named “ Funnull ” — highlights a persistent whac-a-mole problem facing cloud services. In October 2024, the security firm Silent Push published a lengthy analysis of how Amazon AWS and Microsoft Azure were providing services to Funnull, a two-year-old Chinese content delivery network that [...]
