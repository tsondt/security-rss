Title: New AWS Skill Builder course available: Securing Generative AI on AWS
Date: 2025-01-08T22:10:18+00:00
Author: Anna McAbee
Category: AWS Security
Tags: Announcements;Generative AI;Intermediate (200);Security, Identity, & Compliance;Security Blog
Slug: 2025-01-08-new-aws-skill-builder-course-available-securing-generative-ai-on-aws

[Source](https://aws.amazon.com/blogs/security/new-aws-skill-builder-course-available-securing-generative-ai-on-aws/){:target="_blank" rel="noopener"}

> To support our customers in securing their generative AI workloads on Amazon Web Services (AWS), we are excited to announce the launch of a new AWS Skill Builder course: Securing Generative AI on AWS. This comprehensive course is designed to help security professionals, architects, and artificial intelligence and machine learning (AI/ML) engineers understand and implement [...]
