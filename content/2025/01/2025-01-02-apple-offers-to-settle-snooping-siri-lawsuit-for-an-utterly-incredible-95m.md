Title: Apple offers to settle 'snooping Siri' lawsuit for an utterly incredible $95M
Date: 2025-01-02T21:15:10+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-01-02-apple-offers-to-settle-snooping-siri-lawsuit-for-an-utterly-incredible-95m

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/02/apple_siri_lawsuit/){:target="_blank" rel="noopener"}

> Even the sound of a zip could be enough to start the recordings, according to claims Apple has filed a proposed settlement in California suggesting it will pay $95 million to settle claims that Siri recorded owners' conversations without consent and allowed contractors to listen in.... [...]
