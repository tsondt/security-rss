Title: Telefónica confirms internal ticketing system breach after data leak
Date: 2025-01-10T14:15:09-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2025-01-10-telefónica-confirms-internal-ticketing-system-breach-after-data-leak

[Source](https://www.bleepingcomputer.com/news/security/telefonica-confirms-internal-ticketing-system-breach-after-data-leak/){:target="_blank" rel="noopener"}

> Spanish telecommunications company Telefónica confirms its internal ticketing system was breached after stolen data was leaked on a hacking forum. [...]
