Title: Testing and evaluating GuardDuty detections
Date: 2025-01-28T19:47:55+00:00
Author: Marshall Jones
Category: AWS Security
Tags: Announcements;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Amazon GuardDuty;Incident response;Security;Security Blog;threat detection
Slug: 2025-01-28-testing-and-evaluating-guardduty-detections

[Source](https://aws.amazon.com/blogs/security/testing-and-evaluating-guardduty-detections/){:target="_blank" rel="noopener"}

> Amazon GuardDuty is a threat detection service that continuously monitors, analyzes, and processes Amazon Web Services (AWS) data sources and logs in your AWS environment. GuardDuty uses threat intelligence feeds, such as lists of malicious IP addresses and domains, file hashes, and machine learning (ML) models to identify suspicious and potentially malicious activity in your [...]
