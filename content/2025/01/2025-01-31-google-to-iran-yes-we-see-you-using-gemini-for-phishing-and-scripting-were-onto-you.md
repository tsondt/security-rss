Title: Google to Iran: Yes, we see you using Gemini for phishing and scripting. We're onto you
Date: 2025-01-31T01:30:11+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-01-31-google-to-iran-yes-we-see-you-using-gemini-for-phishing-and-scripting-were-onto-you

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/31/state_spies_google_gemini/){:target="_blank" rel="noopener"}

> And you, China, Russia, North Korea... Guardrails block malware generation Google says it's spotted Chinese, Russian, Iranian, and North Korean government agents using its Gemini AI for nefarious purposes, with Tehran by far the most frequent naughty user out of the four.... [...]
