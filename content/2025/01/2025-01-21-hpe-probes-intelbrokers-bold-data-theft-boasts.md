Title: HPE probes IntelBroker's bold data theft boasts
Date: 2025-01-21T13:19:41+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-21-hpe-probes-intelbrokers-bold-data-theft-boasts

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/21/hpe_intelbroker_claims/){:target="_blank" rel="noopener"}

> Incident response protocols engaged following claims of source code burglary Hewlett Packard Enterprise (HPE) is probing assertions made by prolific Big Tech intruder IntelBroker that they broke into the US corporation's systems and accessed source code, among other things.... [...]
