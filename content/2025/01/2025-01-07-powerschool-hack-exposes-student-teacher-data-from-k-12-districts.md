Title: PowerSchool hack exposes student, teacher data from K-12 districts
Date: 2025-01-07T23:26:09-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2025-01-07-powerschool-hack-exposes-student-teacher-data-from-k-12-districts

[Source](https://www.bleepingcomputer.com/news/security/powerschool-hack-exposes-student-teacher-data-from-k-12-districts/){:target="_blank" rel="noopener"}

> Education software giant PowerSchool has confirmed it suffered a cybersecurity incident that allowed a threat actor to steal the personal information of students and teachers from school districts using its PowerSchool SIS platform. [...]
