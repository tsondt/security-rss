Title: Otelier data breach exposes info, hotel reservations of millions
Date: 2025-01-17T15:17:22-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2025-01-17-otelier-data-breach-exposes-info-hotel-reservations-of-millions

[Source](https://www.bleepingcomputer.com/news/security/otelier-data-breach-exposes-info-hotel-reservations-of-millions/){:target="_blank" rel="noopener"}

> Hotel management platform Otelier suffered a data breach after threat actors breached its Amazon S3 cloud storage to steal millions of guests' personal information and reservations for well-known hotel brands like Marriott, Hilton, and Hyatt. [...]
