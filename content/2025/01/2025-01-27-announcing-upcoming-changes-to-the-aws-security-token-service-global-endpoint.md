Title: Announcing upcoming changes to the AWS Security Token Service global endpoint
Date: 2025-01-27T23:00:46+00:00
Author: Palak Arora
Category: AWS Security
Tags: Announcements;AWS Identity and Access Management (IAM);AWS Security Token Service;Intermediate (200);Security, Identity, & Compliance;AWS STS;Security Blog
Slug: 2025-01-27-announcing-upcoming-changes-to-the-aws-security-token-service-global-endpoint

[Source](https://aws.amazon.com/blogs/security/announcing-upcoming-changes-to-the-aws-security-token-service-global-endpoint/){:target="_blank" rel="noopener"}

> AWS launched AWS Security Token Service (AWS STS) in August 2011 with a single global endpoint (https://sts.amazonaws.com), hosted in the US East (N. Virginia) AWS Region. To reduce dependency on a single Region, STS launched AWS STS Regional endpoints (https://sts.{Region_identifier}.{partition_domain}) in February 2015. These Regional endpoints allow you to use STS in the same Region [...]
