Title: HPE investigates breach as hacker claims to steal source code
Date: 2025-01-20T14:06:38-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-20-hpe-investigates-breach-as-hacker-claims-to-steal-source-code

[Source](https://www.bleepingcomputer.com/news/security/hewlett-packard-enterprise-investigates-new-breach-claims/){:target="_blank" rel="noopener"}

> Hewlett Packard Enterprise (HPE) is investigating claims of a new breach after a threat actor said they stole documents from the company's developer environments. [...]
