Title: FTC sues GoDaddy for years of poor hosting security practices
Date: 2025-01-16T11:09:19-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-16-ftc-sues-godaddy-for-years-of-poor-hosting-security-practices

[Source](https://www.bleepingcomputer.com/news/security/ftc-sues-godaddy-for-years-of-poor-hosting-security-practices/){:target="_blank" rel="noopener"}

> The Federal Trade Commission (FTC) will require web hosting giant GoDaddy to implement basic security protections, including HTTPS APIs and mandatory multi-factor authentication, to settle charges that it failed to secure its hosting services against attacks since 2018. [...]
