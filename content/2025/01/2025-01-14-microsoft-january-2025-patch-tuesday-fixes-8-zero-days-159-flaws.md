Title: Microsoft January 2025 Patch Tuesday fixes 8 zero-days, 159 flaws
Date: 2025-01-14T14:01:03-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2025-01-14-microsoft-january-2025-patch-tuesday-fixes-8-zero-days-159-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-january-2025-patch-tuesday-fixes-8-zero-days-159-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's January 2025 Patch Tuesday, which includes security updates for 159 flaws, including eight zero-day vulnerabilities, with three actively exploited in attacks. [...]
