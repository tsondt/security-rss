Title: Widely used DNA sequencer still doesn’t enforce Secure Boot
Date: 2025-01-07T14:00:28+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;dna sequencer;firmware;illumina;iseq 100;secure boot
Slug: 2025-01-07-widely-used-dna-sequencer-still-doesnt-enforce-secure-boot

[Source](https://arstechnica.com/security/2025/01/widely-used-dna-sequencer-still-doesnt-enforce-secure-boot/){:target="_blank" rel="noopener"}

> In 2012, an industry-wide coalition of hardware and software makers adopted Secure Boot to protect Windows devices against the threat of malware that could infect the BIOS and, later, its successor, the UEFI, the firmware that loaded the operating system each time a computer booted up. Firmware-dwelling malware raises the specter of malware that infects the devices before the operating system even loads, each time they boot up. From there, it can remain immune to detection and removal. Secure Boot uses public-key cryptography to block the loading of any code that isn’t signed with a pre-approved digital signature. 2018 calling [...]
