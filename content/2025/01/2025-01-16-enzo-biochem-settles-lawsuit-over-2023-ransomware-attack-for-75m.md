Title: Enzo Biochem settles lawsuit over 2023 ransomware attack for $7.5M
Date: 2025-01-16T17:32:19+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-16-enzo-biochem-settles-lawsuit-over-2023-ransomware-attack-for-75m

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/16/enzo_biochem_ransomware_lawsuit/){:target="_blank" rel="noopener"}

> That's in addition to the $4.5M fine paid to three state AGs last year Enzo Biochem has settled a consolidated class-action lawsuit relating to its 2023 ransomware incident for $7.5 million.... [...]
