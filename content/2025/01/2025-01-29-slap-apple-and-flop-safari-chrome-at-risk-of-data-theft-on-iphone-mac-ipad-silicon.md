Title: SLAP, Apple, and FLOP: Safari, Chrome at risk of data theft on iPhone, Mac, iPad Silicon
Date: 2025-01-29T00:26:29+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2025-01-29-slap-apple-and-flop-safari-chrome-at-risk-of-data-theft-on-iphone-mac-ipad-silicon

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/29/flop_and_slap_attacks_apple_silicon/){:target="_blank" rel="noopener"}

> It's another cousin of Spectre, here to read your email, browsing history, and more Many recent Apple laptops, desktops, tablets, and phones powered by Cupertino's homegrown Silicon processors can be exploited to reveal email content, browsing behavior, and other sensitive data through two newly identified side-channel attacks on Chrome and Safari.... [...]
