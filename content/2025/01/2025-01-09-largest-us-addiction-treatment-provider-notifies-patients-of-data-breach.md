Title: Largest US addiction treatment provider notifies patients of data breach
Date: 2025-01-09T16:07:03-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2025-01-09-largest-us-addiction-treatment-provider-notifies-patients-of-data-breach

[Source](https://www.bleepingcomputer.com/news/security/largest-us-addiction-treatment-provider-notifies-patients-of-data-breach/){:target="_blank" rel="noopener"}

> ​BayMark Health Services, North America's largest provider of substance use disorder (SUD) treatment and recovery services, is notifying an undisclosed number of patients that attackers stole their personal and health information in a September 2024 breach. [...]
