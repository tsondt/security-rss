Title: How to leave the submarine cable cutters all at sea – go Swedish
Date: 2025-01-20T13:33:09+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2025-01-20-how-to-leave-the-submarine-cable-cutters-all-at-sea-go-swedish

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/20/opinion_column_submarine_cables/){:target="_blank" rel="noopener"}

> Clear rules and guaranteed consequences concentrate the mind wonderfully. Just ask a Russian Opinion "As obsolete as warships in the Baltic" was a great pop lyric in Prefab Sprout's 1985 gem, Faron Young. Great, but ironically obsolete itself. Sweden has just deployed multiple warships in that selfsame sea to guard against the very modern menace of underwater cable cutting.... [...]
