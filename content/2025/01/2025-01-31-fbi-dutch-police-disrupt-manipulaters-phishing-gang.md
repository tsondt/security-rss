Title: FBI, Dutch Police Disrupt ‘Manipulaters’ Phishing Gang
Date: 2025-01-31T18:35:32+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;Ne'er-Do-Well News;BEC fraud;business email compromise;Cracked;domaintools;Dutch National Police;fbi;FudCo;Fudpage;Fudtools;HeartSender;Operation Talent;Saim Raza;Sellix;The Manipulaters;U.S. Department of Justice;WeCodeSolutions
Slug: 2025-01-31-fbi-dutch-police-disrupt-manipulaters-phishing-gang

[Source](https://krebsonsecurity.com/2025/01/fbi-dutch-police-disrupt-manipulaters-phishing-gang/){:target="_blank" rel="noopener"}

> The FBI and authorities in The Netherlands this week seized dozens of servers and domains for a hugely popular spam and malware dissemination service operating out of Pakistan. The proprietors of the service, who use the collective nickname “ The Manipulaters,” have been the subject of three stories published here since 2015. The FBI said the main clientele are organized crime groups that try to trick victim companies into making payments to a third party. One of several current Fudtools sites run by the principals of The Manipulators. On January 29, the FBI and the Dutch national police seized the [...]
