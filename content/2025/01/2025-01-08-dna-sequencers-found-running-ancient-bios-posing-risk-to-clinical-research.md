Title: DNA sequencers found running ancient BIOS, posing risk to clinical research
Date: 2025-01-08T15:30:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-08-dna-sequencers-found-running-ancient-bios-posing-risk-to-clinical-research

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/08/dna_sequencer_vulnerabilities/){:target="_blank" rel="noopener"}

> Devices on six-year-old firmware vulnerable to takeover and destruction Updated Argentine cybersecurity shop Eclypsium claims security issues affecting leading DNA sequencing devices could lead to disruptions in crucial clinical research.... [...]
