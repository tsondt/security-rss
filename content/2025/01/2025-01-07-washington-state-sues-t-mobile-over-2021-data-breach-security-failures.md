Title: Washington state sues T-Mobile over 2021 data breach security failures
Date: 2025-01-07T13:08:24-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;Security
Slug: 2025-01-07-washington-state-sues-t-mobile-over-2021-data-breach-security-failures

[Source](https://www.bleepingcomputer.com/news/legal/washington-state-sues-t-mobile-over-2021-data-breach-security-failures/){:target="_blank" rel="noopener"}

> Washington state has sued T-Mobile over failing to secure the sensitive personal information of over 2 million Washington residents in a 2021 data breach. [...]
