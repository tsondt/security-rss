Title: ExxonMobil Lobbyist Caught Hacking Climate Activists
Date: 2025-01-29T12:04:09+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;hacking
Slug: 2025-01-29-exxonmobil-lobbyist-caught-hacking-climate-activists

[Source](https://www.schneier.com/blog/archives/2025/01/exxonmobil-lobbyist-caught-hacking-climate-activists.html){:target="_blank" rel="noopener"}

> The Department of Justice is investigating a lobbying firm representing ExxonMobil for hacking the phones of climate activists: The hacking was allegedly commissioned by a Washington, D.C., lobbying firm, according to a lawyer representing the U.S. government. The firm, in turn, was allegedly working on behalf of one of the world’s largest oil and gas companies, based in Texas, that wanted to discredit groups and individuals involved in climate litigation, according to the lawyer for the U.S. government. In court documents, the Justice Department does not name either company. As part of its probe, the U.S. is trying to extradite [...]
