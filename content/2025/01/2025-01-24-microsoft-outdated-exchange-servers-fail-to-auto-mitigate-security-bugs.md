Title: Microsoft: Outdated Exchange servers fail to auto-mitigate security bugs
Date: 2025-01-24T10:26:27-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2025-01-24-microsoft-outdated-exchange-servers-fail-to-auto-mitigate-security-bugs

[Source](https://www.bleepingcomputer.com/news/security/microsoft-outdated-exchange-servers-fail-to-auto-mitigate-security-bugs/){:target="_blank" rel="noopener"}

> Microsoft says outdated Exchange servers cannot receive new emergency mitigation definitions because an Office Configuration Service certificate type is being deprecated. [...]
