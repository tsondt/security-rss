Title: Cryptojacking, backdoors abound as fiends abuse Aviatrix Controller bug
Date: 2025-01-13T21:00:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-13-cryptojacking-backdoors-abound-as-fiends-abuse-aviatrix-controller-bug

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/13/severe_aviatrix_controller_vulnerability/){:target="_blank" rel="noopener"}

> This is what happens when you publish PoCs immediately, hm? "Several cloud deployments" are already compromised following the disclosure of the maximum-severity vulnerability in Aviatrix Controller, researchers say.... [...]
