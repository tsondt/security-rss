Title: Threat of cyber-attacks on Whitehall ‘is severe and advancing quickly’, NAO says
Date: 2025-01-29T00:01:30+00:00
Author: Robert Booth UK technology editor
Category: The Guardian
Tags: Cybercrime;Hacking;Data and computer security;Civil service;Politics;Technology;UK news
Slug: 2025-01-29-threat-of-cyber-attacks-on-whitehall-is-severe-and-advancing-quickly-nao-says

[Source](https://www.theguardian.com/technology/2025/jan/29/cyber-attack-threat-uk-government-departments-whitehall-nao){:target="_blank" rel="noopener"}

> Audit watchdog finds 58 critical IT systems assessed in 2024 had ‘significant gaps in cyber-resilience’ The threat of potentially devastating cyber-attacks against UK government departments is “severe and advancing quickly”, with dozens of critical IT systems vulnerable to an expected regular pattern of significant strikes, ministers have been warned. The National Audit Office (NAO) found that 58 critical government IT systems independently assessed in 2024 had “significant gaps in cyber-resilience”, and the government did not know how vulnerable at least 228 ageing and outdated “legacy” IT systems were to cyber-attack. The NAO did not name the systems for fear of [...]
