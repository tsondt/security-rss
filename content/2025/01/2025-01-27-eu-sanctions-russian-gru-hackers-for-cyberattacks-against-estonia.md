Title: EU sanctions Russian GRU hackers for cyberattacks against Estonia
Date: 2025-01-27T13:51:33-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-27-eu-sanctions-russian-gru-hackers-for-cyberattacks-against-estonia

[Source](https://www.bleepingcomputer.com/news/security/eu-sanctions-russian-gru-hackers-for-cyberattacks-against-estonia/){:target="_blank" rel="noopener"}

> The European Union sanctioned three hackers, part of Unit 29155 of Russia's military intelligence service (GRU), for their involvement in cyberattacks targeting Estonia's government agencies in 2020. [...]
