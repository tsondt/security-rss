Title: GM parks claims that driver location data was given to insurers, pushing up premiums
Date: 2025-01-17T00:49:27+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2025-01-17-gm-parks-claims-that-driver-location-data-was-given-to-insurers-pushing-up-premiums

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/17/gm_settles_ftc_charges/){:target="_blank" rel="noopener"}

> We'll defo ask for permission next time, automaker tells FTC General Motors on Thursday said that it has reached a settlement with the FTC "to address privacy concerns about our now-discontinued Smart Driver program."... [...]
