Title: DeepSeek exposes database with over 1 million chat records
Date: 2025-01-30T11:16:02-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Artificial Intelligence
Slug: 2025-01-30-deepseek-exposes-database-with-over-1-million-chat-records

[Source](https://www.bleepingcomputer.com/news/security/deepseek-exposes-database-with-over-1-million-chat-records/){:target="_blank" rel="noopener"}

> DeepSeek, the Chinese AI startup known for its DeepSeek-R1 LLM model, has publicly exposed two databases containing sensitive user and operational information. [...]
