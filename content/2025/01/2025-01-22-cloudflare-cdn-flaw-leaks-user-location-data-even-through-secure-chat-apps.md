Title: Cloudflare CDN flaw leaks user location data, even through secure chat apps
Date: 2025-01-22T16:32:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-22-cloudflare-cdn-flaw-leaks-user-location-data-even-through-secure-chat-apps

[Source](https://www.bleepingcomputer.com/news/security/cloudflare-cdn-flaw-leaks-user-location-data-even-through-secure-chat-apps/){:target="_blank" rel="noopener"}

> A security researcher discovered a flaw in Cloudflare's content delivery network (CDN), which could expose a person's general location by simply sending them an image on platforms like Signal and Discord. [...]
