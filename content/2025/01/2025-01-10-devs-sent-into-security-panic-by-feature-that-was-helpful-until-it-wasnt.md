Title: Devs sent into security panic by 'feature that was helpful … until it wasn't'
Date: 2025-01-10T08:30:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2025-01-10-devs-sent-into-security-panic-by-feature-that-was-helpful-until-it-wasnt

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/10/on_call/){:target="_blank" rel="noopener"}

> Screenshot showed it wasn't a possible attack – unless you qualify everything Google does as a threat On Call Velkomin, Vælkomin, Hoş geldin, and welcome to Friday, and therefore to another edition of On Call – The Register 's end-of-week celebration of the tech support tasks you managed to tackle without too much trauma.... [...]
