Title: Microsoft eggheads say AI can never be made secure – after testing Redmond's own products
Date: 2025-01-17T07:42:05+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2025-01-17-microsoft-eggheads-say-ai-can-never-be-made-secure-after-testing-redmonds-own-products

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/17/microsoft_ai_redteam_infosec_warning/){:target="_blank" rel="noopener"}

> If you want a picture of the future, imagine your infosec team stamping on software forever Microsoft brainiacs who probed the security of more than 100 of the software giant's own generative AI products came away with a sobering message: The models amplify existing security risks and create new ones.... [...]
