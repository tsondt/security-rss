Title: Here’s how hucksters are manipulating Google to promote shady Chrome extensions
Date: 2025-01-08T23:46:10+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;abuse;Chrome Web Store;extensions;google;spam
Slug: 2025-01-08-heres-how-hucksters-are-manipulating-google-to-promote-shady-chrome-extensions

[Source](https://arstechnica.com/security/2025/01/googles-chrome-web-store-has-a-serious-spam-problem-promoting-shady-extensions/){:target="_blank" rel="noopener"}

> The people overseeing the security of Google’s Chrome browser explicitly forbid third-party extension developers from trying to manipulate how the browser extensions they submit are presented in the Chrome Web Store. The policy specifically calls out search-manipulating techniques such as listing multiple extensions that provide the same experience or plastering extension descriptions with loosely related or unrelated keywords. On Wednesday, security and privacy researcher Wladimir Palant revealed that developers are flagrantly violating those terms in hundreds of extensions currently available for download from Google. As a result, searches for a particular term or terms can return extensions that are unrelated, [...]
