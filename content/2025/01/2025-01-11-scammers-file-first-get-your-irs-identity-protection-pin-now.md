Title: Scammers file first — Get your IRS Identity Protection PIN now
Date: 2025-01-11T11:44:20-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Government
Slug: 2025-01-11-scammers-file-first-get-your-irs-identity-protection-pin-now

[Source](https://www.bleepingcomputer.com/news/security/scammers-file-first-get-your-irs-identity-protection-pin-now/){:target="_blank" rel="noopener"}

> The IRS relaunched its Identity Protection Personal Identification Number (IP PIN) program this week and all US taxpayers are encouraged to enroll for added security against identity theft and fraudulent returns. [...]
