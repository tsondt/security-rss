Title: Windows Patch Tuesday hits snag with Citrix software, workarounds published
Date: 2025-01-15T17:15:14+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2025-01-15-windows-patch-tuesday-hits-snag-with-citrix-software-workarounds-published

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/15/windows_patch_tuesday_citrix/){:target="_blank" rel="noopener"}

> Microsoft starts 2025 as it hopefully doesn't mean to go on Devices that have Citrix's Session Recording software installed are having problems completing this month's Microsoft Patch Tuesday update, which includes important fixes.... [...]
