Title: PowerSchool starts notifying victims of massive data breach
Date: 2025-01-28T10:43:31-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Education
Slug: 2025-01-28-powerschool-starts-notifying-victims-of-massive-data-breach

[Source](https://www.bleepingcomputer.com/news/security/powerschool-starts-notifying-victims-of-massive-data-breach/){:target="_blank" rel="noopener"}

> Education software giant PowerSchool has started notifying individuals in the U.S. and Canada whose personal data was exposed in a late December 2024 cyberattack. [...]
