Title: GDPR complaints filed against TikTok, Temu for sending user data to China
Date: 2025-01-16T17:34:08-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2025-01-16-gdpr-complaints-filed-against-tiktok-temu-for-sending-user-data-to-china

[Source](https://www.bleepingcomputer.com/news/security/gdpr-complaints-filed-against-tiktok-temu-for-sending-user-data-to-china/){:target="_blank" rel="noopener"}

> Non-profit privacy advocacy group "None of Your Business" (noyb) has filed six complaints against TikTok, AliExpress, SHEIN, Temu, WeChat, and Xiaomi, for unlawfully transferring European user's data to China and infringing European Union's general data protection regulation (GDPR). [...]
