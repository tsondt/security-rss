Title: Microsoft fixes under-attack privilege-escalation holes in Hyper-V
Date: 2025-01-15T01:33:04+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-01-15-microsoft-fixes-under-attack-privilege-escalation-holes-in-hyper-v

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/15/patch_tuesday_january_2025/){:target="_blank" rel="noopener"}

> Plus: Excel hell, angst for Adobe fans, and life's too Snort for Cisco Patch Tuesday The first Patch Tuesday of 2025 has seen Microsoft address three under-attack privilege-escalation flaws in its Hyper-V hypervisor, plus plenty more problems that deserve your attention.... [...]
