Title: Cloud CISO Perspectives: Talk cyber in business terms to win allies
Date: 2025-01-17T17:00:00+00:00
Author: Phil Venables
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2025-01-17-cloud-ciso-perspectives-talk-cyber-in-business-terms-to-win-allies

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-talk-cyber-in-business-terms-to-win-allies/){:target="_blank" rel="noopener"}

> Welcome to the first Cloud CISO Perspectives for January 2025. We’re starting off the year at the top with boards of directors, and how talking about cybersecurity in business terms can help us better convey the costs and priority and priority of the cybersecurity risks we face. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI Security & CISO, Google Cloud aside_block <ListValue: [StructValue([('title', 'Get vital board insights [...]
