Title: MasterCard DNS Error Went Unnoticed for Years
Date: 2025-01-22T15:24:41+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;How to Break Into Security;akam.ne;akam.net;Akamai;awsdns-06.ne;az.mastercard.com;Azure;Bugcrowd;CloudFlare;google;mastercard;Philippe Caturegli;Seralys
Slug: 2025-01-22-mastercard-dns-error-went-unnoticed-for-years

[Source](https://krebsonsecurity.com/2025/01/mastercard-dns-error-went-unnoticed-for-years/){:target="_blank" rel="noopener"}

> The payment card giant MasterCard just fixed a glaring error in its domain name server settings that could have allowed anyone to intercept or divert Internet traffic for the company by registering an unused domain name. The misconfiguration persisted for nearly five years until a security researcher spent $300 to register the domain and prevent it from being grabbed by cybercriminals. A DNS lookup on the domain az.mastercard.com on Jan. 14, 2025 shows the mistyped domain name a22-65.akam.ne. From June 30, 2020 until January 14, 2025, one of the core Internet servers that MasterCard uses to direct traffic for portions [...]
