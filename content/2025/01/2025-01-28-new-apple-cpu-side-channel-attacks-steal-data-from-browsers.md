Title: New Apple CPU side-channel attacks steal data from browsers
Date: 2025-01-28T13:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple;Hardware
Slug: 2025-01-28-new-apple-cpu-side-channel-attacks-steal-data-from-browsers

[Source](https://www.bleepingcomputer.com/news/security/new-apple-cpu-side-channel-attack-steals-data-from-browsers/){:target="_blank" rel="noopener"}

> A team of security researchers has disclosed new side-channel vulnerabilities in modern Apple processors that could steal sensitive information from web browsers. [...]
