Title: Label giant Avery says website hacked to steal credit cards
Date: 2025-01-15T14:44:28-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-15-label-giant-avery-says-website-hacked-to-steal-credit-cards

[Source](https://www.bleepingcomputer.com/news/security/label-giant-avery-says-website-hacked-to-steal-credit-cards/){:target="_blank" rel="noopener"}

> Avery Products Corporation is warning it suffered a data breach after its website was hacked to steal customers' credit cards and personal information. [...]
