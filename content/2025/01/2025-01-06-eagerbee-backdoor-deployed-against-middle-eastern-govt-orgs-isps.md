Title: Eagerbee backdoor deployed against Middle Eastern govt orgs, ISPs
Date: 2025-01-06T09:54:56-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-06-eagerbee-backdoor-deployed-against-middle-eastern-govt-orgs-isps

[Source](https://www.bleepingcomputer.com/news/security/eagerbee-backdoor-deployed-against-middle-eastern-govt-orgs-isps/){:target="_blank" rel="noopener"}

> New variants of the Eagerbee malware framework are being deployed against government organizations and internet service providers (ISPs) in the Middle East. [...]
