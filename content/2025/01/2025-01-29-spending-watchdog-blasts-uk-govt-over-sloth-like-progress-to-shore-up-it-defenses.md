Title: Spending watchdog blasts UK govt over sloth-like progress to shore up IT defenses
Date: 2025-01-29T07:24:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-29-spending-watchdog-blasts-uk-govt-over-sloth-like-progress-to-shore-up-it-defenses

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/29/nao_blasts_uk_gov_cyber/){:target="_blank" rel="noopener"}

> Think government cybersecurity is bad? Guess again. It’s alarmingly so The UK government is significantly behind on its 2022 target to harden systems against cyberattacks by 2025, with a new report from the spending watchdog suggesting it may not achieve this goal even by 2030.... [...]
