Title: I tried hard, but didn't fix all of cybersecurity, admits outgoing US National Cyber Director
Date: 2025-01-08T23:56:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2025-01-08-i-tried-hard-but-didnt-fix-all-of-cybersecurity-admits-outgoing-us-national-cyber-director

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/08/oncd_director_harry_coker_exit_remarks/){:target="_blank" rel="noopener"}

> In colossal surprise, ONCD boss Harry Coker says more work is needed The outgoing leader of the United States' Office of the National Cyber Director has a clear message for whomever President-elect Trump picks to be his successor: There's a lot of work still to do.... [...]
