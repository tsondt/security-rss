Title: Star Blizzard hackers abuse WhatsApp to target high-value diplomats
Date: 2025-01-19T10:23:46-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-19-star-blizzard-hackers-abuse-whatsapp-to-target-high-value-diplomats

[Source](https://www.bleepingcomputer.com/news/security/star-blizzard-hackers-abuse-whatsapp-to-target-high-value-diplomats/){:target="_blank" rel="noopener"}

> Russian nation-state actor Star Blizzard has been running a new spear-phishing campaign to compromise WhatsApp accounts of targets in government, diplomacy, defense policy, international relations, and Ukraine aid organizations. [...]
