Title: Supply chain attack hits Chrome extensions, could expose millions
Date: 2025-01-22T19:45:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-22-supply-chain-attack-hits-chrome-extensions-could-expose-millions

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/22/supply_chain_attack_chrome_extension/){:target="_blank" rel="noopener"}

> Threat actor exploited phishing and OAuth abuse to inject malicious code Cybersecurity outfit Sekoia is warning Chrome users of a supply chain attack targeting browser extension developers that has potentially impacted hundreds of thousands of individuals already.... [...]
