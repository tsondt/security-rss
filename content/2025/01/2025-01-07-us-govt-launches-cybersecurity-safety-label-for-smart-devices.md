Title: US govt launches cybersecurity safety label for smart devices
Date: 2025-01-07T15:21:26-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-07-us-govt-launches-cybersecurity-safety-label-for-smart-devices

[Source](https://www.bleepingcomputer.com/news/security/us-govt-launches-cybersecurity-safety-label-for-smart-devices/){:target="_blank" rel="noopener"}

> ​Today, the White House announced the launch of the U.S. Cyber Trust Mark, a new cybersecurity safety label for internet-connected consumer devices. [...]
