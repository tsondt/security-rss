Title: Atos denies Space Bears' ransomware claims – with a 'but'
Date: 2025-01-04T08:30:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-04-atos-denies-space-bears-ransomware-claims-with-a-but

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/04/atos_denies_space_bears_ransomware/){:target="_blank" rel="noopener"}

> Points finger at third-party infrastructure being breached French tech giant Atos today denied that Space Bears criminals breached its systems - but noted that third-party infrastructure was compromised by the ransomware crew, and that files accessed by the crooks included "data mentioning the Atos company name."... [...]
