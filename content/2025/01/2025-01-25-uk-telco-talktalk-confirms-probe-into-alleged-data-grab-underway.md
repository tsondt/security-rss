Title: UK telco TalkTalk confirms probe into alleged data grab underway
Date: 2025-01-25T09:30:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-25-uk-telco-talktalk-confirms-probe-into-alleged-data-grab-underway

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/25/uk_telco_talktalk_confirms_investigation/){:target="_blank" rel="noopener"}

> Spinner says crim's claims 'very significantly overstated' UK broadband and TV provider TalkTalk says it's currently investigating claims made on cybercrime forums alleging data from the company was up for grabs.... [...]
