Title: Crims backdoored the backdoors they supplied to other miscreants. Then the domains lapsed
Date: 2025-01-08T11:00:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-08-crims-backdoored-the-backdoors-they-supplied-to-other-miscreants-then-the-domains-lapsed

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/08/backdoored_backdoors/){:target="_blank" rel="noopener"}

> Here's what $20 gets you these days More than 4,000 unique backdoors are using expired domains and/or abandoned infrastructure, and many of these expose government and academia-owned hosts – thus setting these hosts up for hijacking by criminals who likely have less altruistic intentions than the security researchers who uncovered the very same backdoors.... [...]
