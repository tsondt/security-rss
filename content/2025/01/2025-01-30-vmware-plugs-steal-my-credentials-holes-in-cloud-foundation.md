Title: VMware plugs steal-my-credentials holes in Cloud Foundation
Date: 2025-01-30T22:00:10+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-30-vmware-plugs-steal-my-credentials-holes-in-cloud-foundation

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/30/vmware_infomration_disclosure_flaws/){:target="_blank" rel="noopener"}

> Consider patching soon because cybercrooks love to hit vulnerable tools from Broadcom's virtualization giant Broadcom has fixed five flaws, collectively deemed "high severity," in VMware's IT operations and log management tools within Cloud Foundation, including two information disclosure bugs that could lead to credential leakage under certain conditions.... [...]
