Title: Don't want your Kubernetes Windows nodes hijacked? Patch this hole now
Date: 2025-01-24T15:00:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-24-dont-want-your-kubernetes-windows-nodes-hijacked-patch-this-hole-now

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/24/kubernetes_windows_nodes_bug/){:target="_blank" rel="noopener"}

> SYSTEM-level command injection via API parameter *chef's kiss* A now-fixed command-injection bug in Kubernetes can be exploited by a remote attacker to gain code execution with SYSTEM privileges on all Windows endpoints in a cluster, and thus fully take over those systems, according to Akamai researcher Tomer Peled.... [...]
