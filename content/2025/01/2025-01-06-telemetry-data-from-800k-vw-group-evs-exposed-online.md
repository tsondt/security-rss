Title: Telemetry data from 800K VW Group EVs exposed online
Date: 2025-01-06T01:24:09+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2025-01-06-telemetry-data-from-800k-vw-group-evs-exposed-online

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/06/volkswagen_ev_data_exposed/){:target="_blank" rel="noopener"}

> PLUS: DoJ bans data sale to enemy nations; Do Kwon extradited to US; Tenable CEO passes away; and more Infosec in Brief Welcome to 2025: hopefully you enjoyed a pleasant holiday season and returned to the security operations center without incident - unlike Volkswagen, which last week admitted it exposed data describing journeys made by some of its electric vehicles, plus info about the vehicle’s owners.... [...]
