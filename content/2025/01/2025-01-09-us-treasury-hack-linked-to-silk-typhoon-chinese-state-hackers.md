Title: US Treasury hack linked to Silk Typhoon Chinese state hackers
Date: 2025-01-09T11:49:01-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-09-us-treasury-hack-linked-to-silk-typhoon-chinese-state-hackers

[Source](https://www.bleepingcomputer.com/news/security/us-treasury-hack-linked-to-silk-typhoon-chinese-state-hackers/){:target="_blank" rel="noopener"}

> ​Chinese state-backed hackers, tracked as Silk Typhoon, have been linked to the U.S. Office of Foreign Assets Control (OFAC) hack in early December. [...]
