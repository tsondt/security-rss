Title: The Internet is (once again) awash with IoT botnets delivering record DDoSes
Date: 2025-01-22T15:10:58+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security
Slug: 2025-01-22-the-internet-is-once-again-awash-with-iot-botnets-delivering-record-ddoses

[Source](https://arstechnica.com/security/2025/01/the-internet-is-once-again-awash-with-iot-botnets-delivering-record-ddoses/){:target="_blank" rel="noopener"}

> We’re only three weeks into 2025, and it’s already shaping up to be the year of Internet of Things-driven DDoSes. Reports are rolling in of threat actors infecting thousands of home and office routers, web cameras, and other Internet-connected devices. Here is a sampling of research released since the first of the year. Lax security, ample bandwidth A post on Tuesday from content-delivery network Cloudflare reported on a recent distributed denial-of-service attack that delivered 5.6 terabits per second of junk traffic—a new record for the largest DDoS ever reported. The deluge, directed at an unnamed Cloudflare customer, came from 13,000 [...]
