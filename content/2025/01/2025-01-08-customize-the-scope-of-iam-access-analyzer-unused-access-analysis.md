Title: Customize the scope of IAM Access Analyzer unused access analysis
Date: 2025-01-08T17:35:14+00:00
Author: Stéphanie Mbappe
Category: AWS Security
Tags: AWS IAM Access Analyzer;AWS Identity and Access Management (IAM);Intermediate (200);Security, Identity, & Compliance;AWS IAM;AWS IAM policies;AWS Identity and Access Management;IAM;IAM Access Analyzer;IAM policies;least privilege;Security Blog
Slug: 2025-01-08-customize-the-scope-of-iam-access-analyzer-unused-access-analysis

[Source](https://aws.amazon.com/blogs/security/customize-the-scope-of-iam-access-analyzer-unused-access-analysis/){:target="_blank" rel="noopener"}

> AWS Identity and Access Management Access Analyzer simplifies inspecting unused access to guide you towards least privilege. You can use unused access findings to identify over-permissive access granted to AWS Identity and Access Management (IAM) roles and users in your accounts or organization. From a delegated administrator account for IAM Access Analyzer, you can use the dashboard [...]
