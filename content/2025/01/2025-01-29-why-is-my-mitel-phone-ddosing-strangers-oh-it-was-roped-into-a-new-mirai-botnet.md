Title: Why is my Mitel phone DDoSing strangers? Oh, it was roped into a new Mirai botnet
Date: 2025-01-29T15:32:05+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-29-why-is-my-mitel-phone-ddosing-strangers-oh-it-was-roped-into-a-new-mirai-botnet

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/29/ddos_attacks_aquabot_mitel/){:target="_blank" rel="noopener"}

> And now you won't stop calling me, I'm kinda busy A new variant of the Mirai-based malware Aquabot is actively exploiting a vulnerability in Mitel phones to build a remote-controlled botnet, according to Akamai's Security Intelligence and Response Team.... [...]
