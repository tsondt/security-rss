Title: Microsoft expands testing of Windows 11 admin protection feature
Date: 2025-01-16T15:13:32-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-16-microsoft-expands-testing-of-windows-11-admin-protection-feature

[Source](https://www.bleepingcomputer.com/news/security/microsoft-expands-testing-of-windows-11-admin-protection-feature/){:target="_blank" rel="noopener"}

> Microsoft has expanded its Windows 11 administrator protection tests, allowing Insiders to enable the security feature from the Windows Security settings. [...]
