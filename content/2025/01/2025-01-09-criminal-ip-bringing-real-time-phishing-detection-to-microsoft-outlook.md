Title: Criminal IP: Bringing Real-Time Phishing Detection to Microsoft Outlook
Date: 2025-01-09T10:02:12-05:00
Author: Sponsored by Criminal IP
Category: BleepingComputer
Tags: Security
Slug: 2025-01-09-criminal-ip-bringing-real-time-phishing-detection-to-microsoft-outlook

[Source](https://www.bleepingcomputer.com/news/security/criminal-ip-bringing-real-time-phishing-detection-to-microsoft-outlook/){:target="_blank" rel="noopener"}

> AI SPERA announced today that it launched its Criminal IP Malicious Link Detector add-in on the Microsoft Marketplace. Learn more about how this tool provides real-time phishing email detection and URL blocking for Microsoft Outlook. [...]
