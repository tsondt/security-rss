Title: Eight things that should not have happened last year, but did
Date: 2025-01-01T13:30:09+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2025-01-01-eight-things-that-should-not-have-happened-last-year-but-did

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/01/opinion_column_tech_fails_2024/){:target="_blank" rel="noopener"}

> 2024's Tech Fail Roll Of Dishonor Opinion Happy new year! Tradition says that this is when we boldly look forward to what may happen in the 12 months to come. Do you really want to know that? Didn’t think so.... [...]
