Title: Using OSCAL to express Canadian cybersecurity requirements as compliance-as-code
Date: 2025-01-21T20:00:23+00:00
Author: Michael Davie
Category: AWS Security
Tags: Compliance;Foundational (100);Government;Public Sector;Security, Identity, & Compliance;Canada;OSCAL;Security Blog
Slug: 2025-01-21-using-oscal-to-express-canadian-cybersecurity-requirements-as-compliance-as-code

[Source](https://aws.amazon.com/blogs/security/using-oscal-to-express-canadian-cybersecurity-requirements-as-compliance-as-code/){:target="_blank" rel="noopener"}

> The Open Security Controls Assessment Language (OSCAL) is a project led by the National Institute of Standards and Technology (NIST) that allows security professionals to express control-related information in machine-readable formats. Expressing compliance information in this way allows security practitioners to use automated tools to support data analysis, while making it easier to address downstream [...]
