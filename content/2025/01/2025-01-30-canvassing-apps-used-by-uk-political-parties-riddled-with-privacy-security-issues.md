Title: Canvassing apps used by UK political parties riddled with privacy, security issues
Date: 2025-01-30T12:08:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-30-canvassing-apps-used-by-uk-political-parties-riddled-with-privacy-security-issues

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/30/uk_canvassing_app_issues/){:target="_blank" rel="noopener"}

> Neither Labour, Conservatives, nor the Lib Dems offered a retort to rights org's report The Open Rights Group (ORG) has raised concerns about a number of security issues it found in all three of the canvassing apps developed on behalf of the UK's three major political parties.... [...]
