Title: Google launches customizable Web Store for Enterprise extensions
Date: 2025-01-23T14:42:43-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Google;Security
Slug: 2025-01-23-google-launches-customizable-web-store-for-enterprise-extensions

[Source](https://www.bleepingcomputer.com/news/google/google-launches-customizable-web-store-for-enterprise-extensions/){:target="_blank" rel="noopener"}

> Google has officially launched its Chrome Web Store for Enterprises, allowing organizations to create a curated list of extensions that can be installed in employees' web browsers. [...]
