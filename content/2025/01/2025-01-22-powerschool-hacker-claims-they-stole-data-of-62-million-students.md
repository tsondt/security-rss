Title: PowerSchool hacker claims they stole data of 62 million students
Date: 2025-01-22T12:39:06-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2025-01-22-powerschool-hacker-claims-they-stole-data-of-62-million-students

[Source](https://www.bleepingcomputer.com/news/security/powerschool-hacker-claims-they-stole-data-of-62-million-students/){:target="_blank" rel="noopener"}

> The hacker who breached education tech giant PowerSchool claimed in an extortion demand that they stole the personal data of 62.4 million students and 9.5 million teachers. [...]
