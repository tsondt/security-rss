Title: Third Interdisciplinary Workshop on Reimagining Democracy (IWORD 2024)
Date: 2025-01-23T14:58:39+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;conferences;democracy
Slug: 2025-01-23-third-interdisciplinary-workshop-on-reimagining-democracy-iword-2024

[Source](https://www.schneier.com/blog/archives/2025/01/third-interdisciplinary-workshop-on-reimagining-democracy-iword-2024.html){:target="_blank" rel="noopener"}

> Last month, Henry Farrell and I convened the Third Interdisciplinary Workshop on Reimagining Democracy ( IWORD 2024 ) at Johns Hopkins University’s Bloomberg Center in Washington DC. This is a small, invitational workshop on the future of democracy. As with the previous two workshops, the goal was to bring together a diverse set of political scientists, law professors, philosophers, AI researchers and other industry practitioners, political activists, and creative types (including science fiction writers) to discuss how democracy might be reimagined in the current century. The goal of the workshop is to think very broadly. Modern democracy was invented in [...]
