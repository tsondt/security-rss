Title: Taiwan reportedly claims China-linked ship damaged one of its submarine cables
Date: 2025-01-06T03:26:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2025-01-06-taiwan-reportedly-claims-china-linked-ship-damaged-one-of-its-submarine-cables

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/06/taiwan_china_submarine_cable_claim/){:target="_blank" rel="noopener"}

> More evidence of Beijing’s liking for gray zone warfare, or a murky claim with odd African entanglements? Taiwanese authorities have asserted that a China-linked ship entered its waters and damaged a submarine cable.... [...]
