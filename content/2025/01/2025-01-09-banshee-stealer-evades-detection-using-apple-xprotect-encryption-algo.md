Title: Banshee stealer evades detection using Apple XProtect encryption algo
Date: 2025-01-09T13:59:19-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-09-banshee-stealer-evades-detection-using-apple-xprotect-encryption-algo

[Source](https://www.bleepingcomputer.com/news/security/banshee-stealer-evades-detection-using-apple-xprotect-encryption-algo/){:target="_blank" rel="noopener"}

> A new version of the Banshee info-stealing malware for macOS has been evading detection over the past two months by adopting string encryption from Apple's XProtect. [...]
