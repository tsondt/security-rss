Title: US Treasury Department Sanctions Chinese Company Over Cyberattacks
Date: 2025-01-07T12:00:42+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;China;cyberattack;national security policy
Slug: 2025-01-07-us-treasury-department-sanctions-chinese-company-over-cyberattacks

[Source](https://www.schneier.com/blog/archives/2025/01/us-treasury-department-sanctions-chinese-company-over-cyberattacks.html){:target="_blank" rel="noopener"}

> From the Washington Post : The sanctions target Beijing Integrity Technology Group, which U.S. officials say employed workers responsible for the Flax Typhoon attacks which compromised devices including routers and internet-enabled cameras to infiltrate government and industrial targets in the United States, Taiwan, Europe and elsewhere. [...]
