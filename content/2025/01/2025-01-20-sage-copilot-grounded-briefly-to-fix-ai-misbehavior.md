Title: Sage Copilot grounded briefly to fix AI misbehavior
Date: 2025-01-20T07:23:14+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2025-01-20-sage-copilot-grounded-briefly-to-fix-ai-misbehavior

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/20/sage_copilot_data_issue/){:target="_blank" rel="noopener"}

> 'Minor issue' with showing accounting customers 'unrelated business information' required repairs Sage Group plc has confirmed it temporarily suspended its Sage Copilot, an AI assistant for the UK-based business software maker's accounting tools, this month after it blurted customer information to other users.... [...]
