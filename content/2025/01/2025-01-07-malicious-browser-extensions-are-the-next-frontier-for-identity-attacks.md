Title: Malicious Browser Extensions are the Next Frontier for Identity Attacks
Date: 2025-01-07T10:02:28-05:00
Author: Sponsored by  LayerX
Category: BleepingComputer
Tags: Security
Slug: 2025-01-07-malicious-browser-extensions-are-the-next-frontier-for-identity-attacks

[Source](https://www.bleepingcomputer.com/news/security/malicious-browser-extensions-are-the-next-frontier-for-identity-attacks/){:target="_blank" rel="noopener"}

> A recent campaign targeting browser extensions illustrates that they are the next frontier in identity attacks. Learn more about these attacks from LayerX Security and how to receive a free extension audit. [...]
