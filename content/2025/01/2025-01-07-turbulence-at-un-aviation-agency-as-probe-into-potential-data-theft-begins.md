Title: Turbulence at UN aviation agency as probe into potential data theft begins
Date: 2025-01-07T17:45:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-07-turbulence-at-un-aviation-agency-as-probe-into-potential-data-theft-begins

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/07/icao_data_theft_investigation/){:target="_blank" rel="noopener"}

> Crime forum-dweller claims to have leaked 42,000 documents packed with personal info The United Nations' aviation agency is investigating "a potential information security incident" after a cybercriminal claimed they had laid hands on 42,000 of the branch's documents.... [...]
