Title: Guess who left a database wide open, exposing chat logs, API keys, and more? Yup, DeepSeek
Date: 2025-01-30T00:31:28+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2025-01-30-guess-who-left-a-database-wide-open-exposing-chat-logs-api-keys-and-more-yup-deepseek

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/30/deepseek_database_left_open/){:target="_blank" rel="noopener"}

> Oh someone's in DeepShi... China-based AI biz DeepSeek may have developed competitive, cost-efficient generative models, but its cybersecurity chops are another story.... [...]
