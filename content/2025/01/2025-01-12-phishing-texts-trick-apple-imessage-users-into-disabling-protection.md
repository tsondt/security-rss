Title: Phishing texts trick Apple iMessage users into disabling protection
Date: 2025-01-12T14:31:01-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Apple
Slug: 2025-01-12-phishing-texts-trick-apple-imessage-users-into-disabling-protection

[Source](https://www.bleepingcomputer.com/news/security/phishing-texts-trick-apple-imessage-users-into-disabling-protection/){:target="_blank" rel="noopener"}

> Cybercriminals are exploiting a trick to turn off Apple iMessage's built-in phishing protection for a text and trick users into re-enabling disabled phishing links. [...]
