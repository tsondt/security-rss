Title: Someone is slipping a hidden backdoor into Juniper routers across the globe, activated by a magic packet
Date: 2025-01-25T11:12:13+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-25-someone-is-slipping-a-hidden-backdoor-into-juniper-routers-across-the-globe-activated-by-a-magic-packet

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/25/mysterious_backdoor_juniper_routers/){:target="_blank" rel="noopener"}

> Who could be so interested in chips, manufacturing, and more, in the US, UK, Europe, Russia... Someone has been quietly backdooring selected Juniper routers around the world in key sectors including semiconductor, energy, and manufacturing, since at least mid-2023.... [...]
