Title: CISA says recent government hack limited to US Treasury
Date: 2025-01-06T15:58:23-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-06-cisa-says-recent-government-hack-limited-to-us-treasury

[Source](https://www.bleepingcomputer.com/news/security/cisa-says-recent-government-hack-limited-to-us-treasury/){:target="_blank" rel="noopener"}

> The U.S. Cybersecurity and Infrastructure Security Agency (CISA) said today that the Treasury Department breach disclosed last week did not impact other federal agencies. [...]
