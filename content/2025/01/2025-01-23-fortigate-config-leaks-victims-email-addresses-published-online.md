Title: FortiGate config leaks: Victims' email addresses published online
Date: 2025-01-23T14:45:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-23-fortigate-config-leaks-victims-email-addresses-published-online

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/23/fortigate_config_leaks_infoseccers_list_victim_emails/){:target="_blank" rel="noopener"}

> Experts warn not to take leaks lightly as years-long compromises could remain undetected Thousands of email addresses included in the Belsen Group's dump of FortiGate configs last week are now available online, revealing which organizations may have been impacted by the 2022 zero-day exploits.... [...]
