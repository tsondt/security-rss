Title: Donald Trump proposes US govt acquire half of TikTok, which thanks him and restores service
Date: 2025-01-20T00:15:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2025-01-20-donald-trump-proposes-us-govt-acquire-half-of-tiktok-which-thanks-him-and-restores-service

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/20/trump_tiktok_nationalization_idea/){:target="_blank" rel="noopener"}

> The same Florida Man who wanted to ban the app in the first place US president-elect Donald Trump appears to have proposed the government he will soon lead should acquire half of made-in-China social media service TikTok’s stateside operations.... [...]
