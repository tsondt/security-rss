Title: Critical zero-days impact premium WordPress real estate plugins
Date: 2025-01-22T17:59:46-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-22-critical-zero-days-impact-premium-wordpress-real-estate-plugins

[Source](https://www.bleepingcomputer.com/news/security/critical-zero-days-impact-premium-wordpress-real-estate-plugins/){:target="_blank" rel="noopener"}

> The RealHome theme and the Easy Real Estate plugins for WordPress are vulnerable to two critical severity flaws that allow unauthenticated users to gain administrative privileges. [...]
