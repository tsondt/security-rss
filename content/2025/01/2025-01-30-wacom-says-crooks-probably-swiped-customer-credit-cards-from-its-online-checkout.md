Title: Wacom says crooks probably swiped customer credit cards from its online checkout
Date: 2025-01-30T01:11:06+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-01-30-wacom-says-crooks-probably-swiped-customer-credit-cards-from-its-online-checkout

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/30/wacom_data_loss/){:target="_blank" rel="noopener"}

> Digital canvas slinger indicates dot-com was skimmed for over a month Graphics tablet maker Wacom has warned customers their credit card details may well have been stolen by miscreants while they were buying stuff from its website.... [...]
