Title: Data breach hitting PowerSchool looks very, very bad
Date: 2025-01-23T12:30:57+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;data breach;network intrusions;personally identifiable information;PII
Slug: 2025-01-23-data-breach-hitting-powerschool-looks-very-very-bad

[Source](https://arstechnica.com/security/2025/01/students-parents-and-teachers-still-smarting-from-breach-exposing-their-info/){:target="_blank" rel="noopener"}

> Parents, students, teachers, and administrators throughout North America are smarting from what could be the biggest data breach of 2025: an intrusion into the network of a cloud-based service storing detailed data of millions of pupils and school personnel. The hack, which came to light earlier this month, hit PowerSchool, a Folsom, California, firm that provides cloud-based software to some 16,000 K–12 schools worldwide. The schools serve 60 million students and employ an unknown number of teachers. Besides providing software for administration, grades, and other functions, PowerSchool stores personal data for students and teachers, with much of that data including [...]
