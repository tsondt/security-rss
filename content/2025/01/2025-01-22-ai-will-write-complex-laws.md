Title: AI Will Write Complex Laws
Date: 2025-01-22T12:04:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;AI;laws;LLM
Slug: 2025-01-22-ai-will-write-complex-laws

[Source](https://www.schneier.com/blog/archives/2025/01/ai-will-write-complex-laws.html){:target="_blank" rel="noopener"}

> Artificial intelligence (AI) is writing law today. This has required no changes in legislative procedure or the rules of legislative bodies—all it takes is one legislator, or legislative assistant, to use generative AI in the process of drafting a bill. In fact, the use of AI by legislators is only likely to become more prevalent. There are currently projects in the US House, US Senate, and legislatures around the world to trial the use of AI in various ways: searching databases, drafting text, summarizing meetings, performing policy research and analysis, and more. A Brazilian municipality passed the first known AI-written [...]
