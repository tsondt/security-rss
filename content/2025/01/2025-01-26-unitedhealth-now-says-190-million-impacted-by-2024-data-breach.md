Title: UnitedHealth now says 190 million impacted by 2024 data breach
Date: 2025-01-26T11:29:17-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2025-01-26-unitedhealth-now-says-190-million-impacted-by-2024-data-breach

[Source](https://www.bleepingcomputer.com/news/security/unitedhealth-now-says-190-million-impacted-by-2024-data-breach/){:target="_blank" rel="noopener"}

> UnitedHealth has revealed that 190 million Americans had their personal and healthcare data stolen in the Change Healthcare ransomware attack, nearly doubling the previously disclosed figure. [...]
