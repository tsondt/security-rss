Title: Friday Squid Blogging: Anniversary Post
Date: 2025-01-03T22:04:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;squid
Slug: 2025-01-03-friday-squid-blogging-anniversary-post

[Source](https://www.schneier.com/blog/archives/2025/01/friday-squid-blogging-anniversary-post.html){:target="_blank" rel="noopener"}

> I made my first squid post nineteen years ago this week. Between then and now, I posted something about squid every week (with maybe only a few exceptions). There is a lot out there about squid, even more if you count the other meanings of the word. Blog moderation policy. [...]
