Title: Google Is Allowing Device Fingerprinting
Date: 2025-01-02T20:22:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;data collection;fingerprints;Google;identification;privacy;tracking
Slug: 2025-01-02-google-is-allowing-device-fingerprinting

[Source](https://www.schneier.com/blog/archives/2025/01/google-is-allowing-device-fingerprinting.html){:target="_blank" rel="noopener"}

> Lukasz Olejnik writes about device fingerprinting, and why Google’s policy change to allow it in 2025 is a major privacy setback. [...]
