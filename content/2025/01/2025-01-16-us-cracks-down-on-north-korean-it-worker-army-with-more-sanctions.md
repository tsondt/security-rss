Title: US cracks down on North Korean IT worker army with more sanctions
Date: 2025-01-16T13:48:43-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-16-us-cracks-down-on-north-korean-it-worker-army-with-more-sanctions

[Source](https://www.bleepingcomputer.com/news/security/us-cracks-down-on-north-korean-it-worker-army-with-more-sanctions/){:target="_blank" rel="noopener"}

> The U.S. Treasury Department has sanctioned a network of individuals and front companies linked to North Korea's Ministry of National Defense that have generated revenue via illegal remote IT work schemes. [...]
