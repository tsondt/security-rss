Title: Safeguard your generative AI workloads from prompt injections
Date: 2025-01-21T17:10:18+00:00
Author: Anna McAbee
Category: AWS Security
Tags: Advanced (300);Best Practices;Generative AI;Security, Identity, & Compliance;Security Blog
Slug: 2025-01-21-safeguard-your-generative-ai-workloads-from-prompt-injections

[Source](https://aws.amazon.com/blogs/security/safeguard-your-generative-ai-workloads-from-prompt-injections/){:target="_blank" rel="noopener"}

> Generative AI applications have become powerful tools for creating human-like content, but they also introduce new security challenges, including prompt injections, excessive agency, and others. See the OWASP Top 10 for Large Language Model Applications to learn more about the unique security risks associated with generative AI applications. When you integrate large language models (LLMs) [...]
