Title: MediaTek rings in the new year with a parade of chipset vulns
Date: 2025-01-06T14:28:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-06-mediatek-rings-in-the-new-year-with-a-parade-of-chipset-vulns

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/06/mediatek_chipset_vulnerabilities/){:target="_blank" rel="noopener"}

> Manufacturers should have had ample time to apply the fixes MediaTek kicked off the first full working week of the new year by disclosing a bevy of security vulnerabilities, including a critical remote code execution bug affecting 51 chipsets.... [...]
