Title: Charter, Consolidated, Windstream reportedly join China's Salt Typhoon victim list
Date: 2025-01-06T20:30:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-06-charter-consolidated-windstream-reportedly-join-chinas-salt-typhoon-victim-list

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/06/charter_consolidated_windstream_salt_typhoon/){:target="_blank" rel="noopener"}

> Slow drip of compromised telecom networks continues The list of telecommunications victims in the Salt Typhoon cyberattack continues to grow as a new report names Charter Communications, Consolidated Communications, and Windstream among those breached by Chinese government snoops.... [...]
