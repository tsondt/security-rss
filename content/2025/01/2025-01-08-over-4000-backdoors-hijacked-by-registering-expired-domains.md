Title: Over 4,000 backdoors hijacked by registering expired domains
Date: 2025-01-08T12:34:54-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government
Slug: 2025-01-08-over-4000-backdoors-hijacked-by-registering-expired-domains

[Source](https://www.bleepingcomputer.com/news/security/over-4-000-backdoors-hijacked-by-registering-expired-domains/){:target="_blank" rel="noopener"}

> Over 4,000 abandoned but still active web backdoors were hijacked and their communication infrastructure sinkholed after researchers registered expired domains used for commanding them. [...]
