Title: Microsoft issues out-of-band fix for Windows Server 2022 NUMA glitch
Date: 2025-01-22T16:17:07+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2025-01-22-microsoft-issues-out-of-band-fix-for-windows-server-2022-numa-glitch

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/22/windows_server_numa_glitch/){:target="_blank" rel="noopener"}

> Update addresses boot failures on multi-node systems Microsoft is releasing an out-of-band patch to deal with a problem that prevented some Windows Server 2022 machines from booting.... [...]
