Title: New Syncjacking attack hijacks devices using Chrome extensions
Date: 2025-01-30T09:33:33-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2025-01-30-new-syncjacking-attack-hijacks-devices-using-chrome-extensions

[Source](https://www.bleepingcomputer.com/news/security/new-syncjacking-attack-hijacks-devices-using-chrome-extensions/){:target="_blank" rel="noopener"}

> A new attack called 'Browser Syncjacking' demonstrates the possibility of using a seemingly benign Chrome extension to take over a victim's device through the browser. [...]
