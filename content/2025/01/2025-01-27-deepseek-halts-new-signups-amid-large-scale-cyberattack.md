Title: DeepSeek halts new signups amid "large-scale" cyberattack
Date: 2025-01-27T17:01:18-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2025-01-27-deepseek-halts-new-signups-amid-large-scale-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/deepseek-halts-new-signups-amid-large-scale-cyberattack/){:target="_blank" rel="noopener"}

> Chinese AI platform DeepSeek has disabled registrations on it DeepSeek-V3 chat platform due to an ongoing "large-scale" cyberattack targeting its services. [...]
