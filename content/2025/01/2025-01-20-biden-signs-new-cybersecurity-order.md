Title: Biden Signs New Cybersecurity Order
Date: 2025-01-20T12:06:19+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;computer security;cybersecurity;regulation
Slug: 2025-01-20-biden-signs-new-cybersecurity-order

[Source](https://www.schneier.com/blog/archives/2025/01/biden-signs-new-cybersecurity-order.html){:target="_blank" rel="noopener"}

> President Biden has signed a new cybersecurity order. It has a bunch of provisions, most notably using the US governments procurement power to improve cybersecurity practices industry-wide. Some details : The core of the executive order is an array of mandates for protecting government networks based on lessons learned from recent major incidents­—namely, the security failures of federal contractors. The order requires software vendors to submit proof that they follow secure development practices, building on a mandate that debuted in 2022 in response to Biden’s first cyber executive order. The Cybersecurity and Infrastructure Security Agency would be tasked with double-checking [...]
