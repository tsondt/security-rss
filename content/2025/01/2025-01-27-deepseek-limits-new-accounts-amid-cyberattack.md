Title: DeepSeek limits new accounts amid cyberattack
Date: 2025-01-27T17:13:48+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2025-01-27-deepseek-limits-new-accounts-amid-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/27/deepseek_suspends_new_registrations_amid/){:target="_blank" rel="noopener"}

> Chinese AI startup grapples with consequences of sudden popularity Updated China's DeepSeek, which shook up American AI makers with the debut of its V3 and reasoning-capable R1 LLM families, has limited new signups to its web-based interface to its models due to what's said to be an ongoing cyberattack.... [...]
