Title: A Tumultuous Week for Federal Cybersecurity Efforts
Date: 2025-01-28T02:50:10+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;The Coming Storm;Alfa Bank;David Sacks;House Judiciary Committee's Select Subcommittee on the Weaponization of the Federal Government;Jack Goldsmith;Joe Hall;John Durham;Lawfare;Melania Trump;Michael Sussman;President Trump;Quinta Jurecic;Rep. Jim Jordan;United States Council on Transnational Organized Crime;World Liberty Financial
Slug: 2025-01-28-a-tumultuous-week-for-federal-cybersecurity-efforts

[Source](https://krebsonsecurity.com/2025/01/a-tumultuous-week-for-federal-cybersecurity-efforts/){:target="_blank" rel="noopener"}

> Image: Shutterstock. Greg Meland. President Trump last week issued a flurry of executive orders that upended a number of government initiatives focused on improving the nation’s cybersecurity posture. The president fired all advisors from the Department of Homeland Security’s Cyber Safety Review Board, called for the creation of a strategic cryptocurrency reserve, and voided a Biden administration action that sought to reduce the risks that artificial intelligence poses to consumers, workers and national security. On his first full day back in the White House, Trump dismissed all 15 advisory committee members of the Cyber Safety Review Board (CSRB), a nonpartisan [...]
