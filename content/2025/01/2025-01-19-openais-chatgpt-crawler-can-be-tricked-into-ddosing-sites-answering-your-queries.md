Title: OpenAI's ChatGPT crawler can be tricked into DDoSing sites, answering your queries
Date: 2025-01-19T19:03:14+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2025-01-19-openais-chatgpt-crawler-can-be-tricked-into-ddosing-sites-answering-your-queries

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/19/openais_chatgpt_crawler_vulnerability/){:target="_blank" rel="noopener"}

> The S in LLM stands for Security OpenAI's ChatGPT crawler appears to be willing to initiate distributed denial of service (DDoS) attacks on arbitrary websites, a reported vulnerability the tech giant has yet to acknowledge.... [...]
