Title: Subaru Starlink flaw let hackers hijack cars in US and Canada
Date: 2025-01-24T12:35:36-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-24-subaru-starlink-flaw-let-hackers-hijack-cars-in-us-and-canada

[Source](https://www.bleepingcomputer.com/news/security/subaru-starlink-flaw-let-hackers-hijack-cars-in-us-and-canada/){:target="_blank" rel="noopener"}

> Security researchers have discovered an arbitrary account takeover flaw in Subaru's Starlink service that could let attackers track, control, and hijack vehicles in the United States, Canada, and Japan using just a license plate. [...]
