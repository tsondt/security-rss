Title: Unpatched critical flaws impact Fancy Product Designer WordPress plugin
Date: 2025-01-08T16:34:42-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-08-unpatched-critical-flaws-impact-fancy-product-designer-wordpress-plugin

[Source](https://www.bleepingcomputer.com/news/security/unpatched-critical-flaws-impact-fancy-product-designer-wordpress-plugin/){:target="_blank" rel="noopener"}

> Premium WordPress plugin Fancy Product Designer from Radykal is vulnerable to two critical severity flaws that remain unfixed in the current latest version. [...]
