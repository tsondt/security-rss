Title: Meta's pay-or-consent model under fire from EU consumer group
Date: 2025-01-23T15:30:07+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2025-01-23-metas-pay-or-consent-model-under-fire-from-eu-consumer-group

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/23/metas_payorconsent_model_under_fire/){:target="_blank" rel="noopener"}

> Company 'strongly disagrees' with law infringement allegations Meta has again come under fire for its pay-or-consent model in the EU.... [...]
