Title: Ransomware gang uses SSH tunnels for stealthy VMware ESXi access
Date: 2025-01-26T10:19:29-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-26-ransomware-gang-uses-ssh-tunnels-for-stealthy-vmware-esxi-access

[Source](https://www.bleepingcomputer.com/news/security/ransomware-gang-uses-ssh-tunnels-for-stealthy-vmware-esxi-access/){:target="_blank" rel="noopener"}

> Ransomware actors targeting ESXi bare metal hypervisors are leveraging SSH tunneling to persist on the system while remaining undetected. [...]
