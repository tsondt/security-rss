Title: Friday Squid Blogging: Opioid Alternatives from Squid Research
Date: 2025-01-17T22:02:23+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;medicine;squid
Slug: 2025-01-17-friday-squid-blogging-opioid-alternatives-from-squid-research

[Source](https://www.schneier.com/blog/archives/2025/01/friday-squid-blogging-opioid-alternatives-from-squid-research.html){:target="_blank" rel="noopener"}

> Is there nothing that squid research can’t solve? “If you’re working with an organism like squid that can edit genetic information way better than any other organism, then it makes sense that that might be useful for a therapeutic application like deadening pain,” he said. [...] Researchers hope to mimic how squid and octopus use RNA editing in nerve channels that interpret pain and use that knowledge to manipulate human cells. Blog moderation policy. [...]
