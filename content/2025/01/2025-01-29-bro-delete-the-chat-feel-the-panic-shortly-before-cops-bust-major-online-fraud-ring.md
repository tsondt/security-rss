Title: 'Bro delete the chat': Feel the panic shortly before cops bust major online fraud ring
Date: 2025-01-29T12:31:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-29-bro-delete-the-chat-feel-the-panic-shortly-before-cops-bust-major-online-fraud-ring

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/29/otp_agency_convicted/){:target="_blank" rel="noopener"}

> Mastermind begs colluders to bury evidence later used to imprison him In announcing the sentencing of three Brits who ran OTP Agency, an account-takeover business, the National Crime Agency (NCA) revealed how a 2021 report sent the fraudsters into a panicked frenzy.... [...]
