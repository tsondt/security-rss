Title: Clock ticking for TikTok as US Supreme Court upholds ban
Date: 2025-01-17T17:15:07+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2025-01-17-clock-ticking-for-tiktok-as-us-supreme-court-upholds-ban

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/17/scotus_upholds_tiktok_ban/){:target="_blank" rel="noopener"}

> With Biden reportedly planning to skirt enforcement and kick the can to Trump, this saga might still not be over updated The US Supreme Court has upheld a law requiring TikTok to either divest from its Chinese parent ByteDance or face a ban in the United States. The decision eliminates the final legal obstacle to the federal government forcing a shutdown of the platform for US users on January 19.... [...]
