Title: US freezes foreign aid, halting cybersecurity defense and policy funds for allies
Date: 2025-01-27T22:16:57+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-27-us-freezes-foreign-aid-halting-cybersecurity-defense-and-policy-funds-for-allies

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/27/us_state_department_freezes_cyber_aid/){:target="_blank" rel="noopener"}

> Uncle Sam will 'no longer blindly dole out money,' State Dept says US Secretary of State Marco Rubio has frozen nearly all foreign aid cash for a full-on government review, including funds to defend America's allies from cyberattacks as well as steer international computer security policies.... [...]
