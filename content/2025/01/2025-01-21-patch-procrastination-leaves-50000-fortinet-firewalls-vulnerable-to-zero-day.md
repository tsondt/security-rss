Title: Patch procrastination leaves 50,000 Fortinet firewalls vulnerable to zero-day
Date: 2025-01-21T18:45:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-21-patch-procrastination-leaves-50000-fortinet-firewalls-vulnerable-to-zero-day

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/21/fortinet_firewalls_still_vulnerable/){:target="_blank" rel="noopener"}

> Seven days after disclosure and little action taken, data shows Fortinet customers need to get with the program and apply the latest updates as nearly 50,000 management interfaces are still vulnerable to the latest zero-day exploit.... [...]
