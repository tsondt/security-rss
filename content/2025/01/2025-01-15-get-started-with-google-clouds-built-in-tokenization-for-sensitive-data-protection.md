Title: Get started with Google Cloud's built-in tokenization for sensitive data protection
Date: 2025-01-15T17:00:00+00:00
Author: Jordanna Chord
Category: GCP Security
Tags: BigQuery;Security & Identity
Slug: 2025-01-15-get-started-with-google-clouds-built-in-tokenization-for-sensitive-data-protection

[Source](https://cloud.google.com/blog/products/identity-security/get-started-with-built-in-tokenization-for-sensitive-data-protection/){:target="_blank" rel="noopener"}

> In many industries including finance and healthcare, sensitive data such as payment card numbers and government identification numbers need to be secured before they can be used and shared. A common approach is applying tokenization to enhance security and manage risk. A token is a substitute value that replaces sensitive data during its use or processing. Instead of directly working with the original, sensitive information (usually referred to as the "raw data"), a token acts as a stand-in. Unlike raw data, the token is a scrambled or encrypted value. Using tokens reduces the real-world risk posed by using the raw [...]
