Title: Introducing Google Cloud Abuse Event Logging to enable automated incident remediation
Date: 2025-01-06T17:00:00+00:00
Author: Akshara Sundararajan
Category: GCP Security
Tags: Security & Identity
Slug: 2025-01-06-introducing-google-cloud-abuse-event-logging-to-enable-automated-incident-remediation

[Source](https://cloud.google.com/blog/products/identity-security/introducing-abuse-event-logging-for-automated-incident-remediation/){:target="_blank" rel="noopener"}

> At Google Cloud, we are deeply committed to partnering with our customers to help achieve stronger security outcomes. As a part of this commitment, we're excited to announce that Google Cloud customers can now track Cloud Abuse Events using Cloud Logging. These events can include leaked service account keys, crypto mining incidents, and malware. When we identify one of these abuse issues that’s affecting your cloud resources, you'll now receive two detailed notifications: one in a structured log format, and an email notification. Cloud Abuse Event Logging is focused on providing a more efficient and effective method for customers to [...]
