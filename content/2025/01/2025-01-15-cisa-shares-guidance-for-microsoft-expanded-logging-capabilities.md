Title: CISA shares guidance for Microsoft expanded logging capabilities
Date: 2025-01-15T15:39:16-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2025-01-15-cisa-shares-guidance-for-microsoft-expanded-logging-capabilities

[Source](https://www.bleepingcomputer.com/news/security/cisa-shares-guidance-for-microsoft-expanded-logging-capabilities/){:target="_blank" rel="noopener"}

> ​CISA shared guidance for government agencies and enterprises on using expanded cloud logs in their Microsoft 365 tenants as part of their forensic and compliance investigations. [...]
