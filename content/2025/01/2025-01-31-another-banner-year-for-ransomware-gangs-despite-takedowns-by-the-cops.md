Title: Another banner year for ransomware gangs despite takedowns by the cops
Date: 2025-01-31T09:00:16+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-31-another-banner-year-for-ransomware-gangs-despite-takedowns-by-the-cops

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/31/banner_year_for_ransomware_gangs/){:target="_blank" rel="noopener"}

> And it doesn't take a crystal ball to predict the future If the nonstop flood of ransomware attacks doesn't already make every day feel like Groundhog Day, then a look back at 2024 – and predictions for 2025 – definitely will.... [...]
