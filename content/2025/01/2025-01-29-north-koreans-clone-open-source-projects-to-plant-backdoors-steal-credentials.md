Title: North Koreans clone open source projects to plant backdoors, steal credentials
Date: 2025-01-29T23:29:05+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-29-north-koreans-clone-open-source-projects-to-plant-backdoors-steal-credentials

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/29/lazarus_groups_supply_chain_attack/){:target="_blank" rel="noopener"}

> Stealing crypto is so 2024. Supply-chain attacks leading to data exfil pays off better? North Korea's Lazarus Group compromised hundreds of victims across the globe in a massive secret-stealing supply chain attack that was ongoing as of earlier this month, according to security researchers.... [...]
