Title: CCN releases guide for Spain’s ENS landing zones using Landing Zone Accelerator on AWS
Date: 2025-01-23T17:14:50+00:00
Author: Tomás Clemente Sánchez
Category: AWS Security
Tags: Announcements;Featured;Foundational (100);Security, Identity, & Compliance;CCN;Compliance;Compliance reports;cybersecurity;Data protection;Digital Sovereignty;ENS;ENS High Standard;España;Esquema Nacional de Seguridad;EU Data Protection;High;Public Sector;Security Blog;Spain
Slug: 2025-01-23-ccn-releases-guide-for-spains-ens-landing-zones-using-landing-zone-accelerator-on-aws

[Source](https://aws.amazon.com/blogs/security/ccn-releases-guide-for-spains-ens-landing-zones-using-landing-zone-accelerator-on-aws/){:target="_blank" rel="noopener"}

> The Spanish National Cryptologic Center (CCN) has published a new STIC guide (CCN-STIC-887 Anexo A) that provides a comprehensive template and supporting artifacts for implementing landing zones that comply with Spain's National Security Framework (ENS) Royal Decree 311/2022 using the Landing Zone Accelerator on AWS. [...]
