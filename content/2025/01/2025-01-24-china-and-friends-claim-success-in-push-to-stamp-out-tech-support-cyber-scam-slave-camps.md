Title: China and friends claim success in push to stamp out tech support cyber-scam slave camps
Date: 2025-01-24T05:59:05+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2025-01-24-china-and-friends-claim-success-in-push-to-stamp-out-tech-support-cyber-scam-slave-camps

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/24/lancang_mekong_anti_cyberscam_cooperation/){:target="_blank" rel="noopener"}

> Paint a target on Myanmar, pledge more info-sharing to get the job done A group established by six Asian nations to fight criminal cyber-scam slave camps that infest the region claims it’s made good progress dismantling the operations.... [...]
