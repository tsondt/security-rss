Title: Medusa ransomware group claims attack on UK's Gateshead Council
Date: 2025-01-17T10:30:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-17-medusa-ransomware-group-claims-attack-on-uks-gateshead-council

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/17/gateshead_council_cybersecurity_incident/){:target="_blank" rel="noopener"}

> Pastes allegedly stolen documents on leak site with £600K demand Another year and yet another UK local authority has been pwned by a ransomware crew. This time it's Gateshead Council in North East England at the hands of the Medusa group.... [...]
