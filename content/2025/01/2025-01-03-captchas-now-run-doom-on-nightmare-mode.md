Title: CAPTCHAs now run Doom – on nightmare mode
Date: 2025-01-03T13:15:11+00:00
Author: Richard Currie
Category: The Register
Tags: 
Slug: 2025-01-03-captchas-now-run-doom-on-nightmare-mode

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/03/captcha_doom_nightmare/){:target="_blank" rel="noopener"}

> As if the bot defense measure wasn't obnoxious enough Though the same couldn't be said for most of us mere mortals, Vercel CEO Guillermo Rauch had a productive festive period, resulting in a CAPTCHA that requires the user to kill three monsters in Doom – on nightmare mode.... [...]
