Title: Just as your LLM once again goes off the rails, Cisco, Nvidia are at the door smiling
Date: 2025-01-17T02:30:10+00:00
Author: Tobias Mann and Simon Sharwood
Category: The Register
Tags: 
Slug: 2025-01-17-just-as-your-llm-once-again-goes-off-the-rails-cisco-nvidia-are-at-the-door-smiling

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/17/nvidia_cisco_ai_guardrails_security/){:target="_blank" rel="noopener"}

> Some of you have apparently already botched chatbots or allowed ‘shadow AI’ to creep in Cisco and Nvidia have both recognized that as useful as today's AI may be, the technology can be equally unsafe and/or unreliable – and have delivered tools in an attempt to help address those weaknesses.... [...]
