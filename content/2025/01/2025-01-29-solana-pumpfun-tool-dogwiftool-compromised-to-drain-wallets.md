Title: Solana Pump.fun tool DogWifTool compromised to drain wallets
Date: 2025-01-29T19:33:11-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2025-01-29-solana-pumpfun-tool-dogwiftool-compromised-to-drain-wallets

[Source](https://www.bleepingcomputer.com/news/security/solana-pumpfun-tool-dogwiftool-compromised-to-drain-wallets/){:target="_blank" rel="noopener"}

> DogWifTools has disclosed on its official Discord channel that its software has been compromised by a supply chain attack that impacted its Windows client, infecting users with malware. [...]
