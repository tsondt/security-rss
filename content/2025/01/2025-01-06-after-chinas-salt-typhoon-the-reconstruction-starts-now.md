Title: After China's Salt Typhoon, the reconstruction starts now
Date: 2025-01-06T09:31:10+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2025-01-06-after-chinas-salt-typhoon-the-reconstruction-starts-now

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/06/opinion_column_cybersec/){:target="_blank" rel="noopener"}

> If 40 years of faulty building gets blown down, don’t rebuild with the rubble Opinion When a typhoon devastates a land, it takes a while to understand the scale of the destruction. Disaster relief kicks in, communications rebuilt, and news flows out. Salt Typhoon is no different.... [...]
