Title: Malicious PyPi package steals Discord auth tokens from devs
Date: 2025-01-17T14:16:28-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-17-malicious-pypi-package-steals-discord-auth-tokens-from-devs

[Source](https://www.bleepingcomputer.com/news/security/malicious-pypi-package-steals-discord-auth-tokens-from-devs/){:target="_blank" rel="noopener"}

> A malicious package named 'pycord-self' on the Python package index (PyPI) targets Discord developers to steal authentication tokens and plant a backdoor for remote control over the system. [...]
