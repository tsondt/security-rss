Title: Cloudflare mitigated a record-breaking 5.6 Tbps DDoS attack
Date: 2025-01-21T16:04:09-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-21-cloudflare-mitigated-a-record-breaking-56-tbps-ddos-attack

[Source](https://www.bleepingcomputer.com/news/security/cloudflare-mitigated-a-record-breaking-56-tbps-ddos-attack/){:target="_blank" rel="noopener"}

> The largest distributed denial-of-service (DDoS) attack to date peaked at 5.6 terabits per second and came from a Mirai-based botnet with 13,000 compromised devices. [...]
