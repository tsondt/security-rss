Title: UN's aviation agency confirms attack on recruitment database
Date: 2025-01-08T14:00:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-08-uns-aviation-agency-confirms-attack-on-recruitment-database

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/08/uns_aviation_agency_confirms_attack/){:target="_blank" rel="noopener"}

> Various data points compromised but no risk to flight security The International Civil Aviation Organization (ICAO), the United Nations' aviation agency, has confirmed to The Register that a cyber crim did indeed steal 42,000 records from its recruitment database.... [...]
