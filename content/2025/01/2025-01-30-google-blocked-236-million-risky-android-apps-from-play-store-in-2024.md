Title: Google blocked 2.36 million risky Android apps from Play Store in 2024
Date: 2025-01-30T15:57:47-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2025-01-30-google-blocked-236-million-risky-android-apps-from-play-store-in-2024

[Source](https://www.bleepingcomputer.com/news/security/google-blocked-236-million-risky-android-apps-from-play-store-in-2024/){:target="_blank" rel="noopener"}

> Google blocked 2.3 million Android app submissions to the Play Store in 2024 due to violations of its policies that made them potentially risky for users. [...]
