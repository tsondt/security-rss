Title: Hackers use Windows RID hijacking to create hidden admin account
Date: 2025-01-24T12:25:38-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2025-01-24-hackers-use-windows-rid-hijacking-to-create-hidden-admin-account

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-windows-rid-hijacking-to-create-hidden-admin-account/){:target="_blank" rel="noopener"}

> A North Korean threat group has been using a technique called RID hijacking that tricks Windows into treating a low-privileged account as one with administrator permissions. [...]
