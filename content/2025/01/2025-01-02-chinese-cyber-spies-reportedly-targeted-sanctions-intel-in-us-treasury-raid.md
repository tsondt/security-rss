Title: Chinese cyber-spies reportedly targeted sanctions intel in US Treasury raid
Date: 2025-01-02T22:28:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-02-chinese-cyber-spies-reportedly-targeted-sanctions-intel-in-us-treasury-raid

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/02/chinese_spies_targeted_sanctions_intel/){:target="_blank" rel="noopener"}

> OFAC, Office of the Treasury Secretary feared hit in data-snarfing swoop Chinese spies who compromised the US Treasury Department's workstations reportedly stole data belonging to a government office responsible for sanctions against organizations and individuals.... [...]
