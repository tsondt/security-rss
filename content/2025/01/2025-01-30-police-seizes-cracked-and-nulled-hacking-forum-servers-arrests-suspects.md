Title: Police seizes Cracked and Nulled hacking forum servers, arrests suspects
Date: 2025-01-30T08:47:03-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-30-police-seizes-cracked-and-nulled-hacking-forum-servers-arrests-suspects

[Source](https://www.bleepingcomputer.com/news/security/police-seizes-cracked-and-nulled-hacking-forum-servers-arrests-suspects/){:target="_blank" rel="noopener"}

> Europol and German law enforcement confirmed the arrest of two suspects and the seizure of 17 servers in Operation Talent, which took down Cracked and Nulled, two of the largest hacking forums with over 10 million users. [...]
