Title: FCC to telcos: By law you must secure your networks from foreign spies. Get on it
Date: 2025-01-17T22:07:27+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-17-fcc-to-telcos-by-law-you-must-secure-your-networks-from-foreign-spies-get-on-it

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/17/fcc_telcos_calea/){:target="_blank" rel="noopener"}

> Plus: Uncle Sam is cross with this one Chinese biz over Salt Typhoon mega-snooping Decades-old legislation requiring American telcos to lock down their systems to prevent foreign snoops from intercepting communications isn't mere decoration on the pages of law books – it actually means carriers need to secure their networks, the FCC has huffed.... [...]
