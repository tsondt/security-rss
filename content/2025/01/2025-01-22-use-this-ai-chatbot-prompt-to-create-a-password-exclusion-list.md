Title: Use this AI chatbot prompt to create a password-exclusion list
Date: 2025-01-22T10:00:10-05:00
Author: Sponsored by Specops
Category: BleepingComputer
Tags: Security
Slug: 2025-01-22-use-this-ai-chatbot-prompt-to-create-a-password-exclusion-list

[Source](https://www.bleepingcomputer.com/news/security/use-this-ai-chatbot-prompt-to-create-a-password-exclusion-list/){:target="_blank" rel="noopener"}

> Creating a custom password-exclusion list can help prevent employees from using passwords that are likely to be guessed. Learn from Specops Software on using AI to generate password dictionary for securing your organization's credentials. [...]
