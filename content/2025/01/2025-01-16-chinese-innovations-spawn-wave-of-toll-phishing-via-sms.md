Title: Chinese Innovations Spawn Wave of Toll Phishing Via SMS
Date: 2025-01-16T21:18:48+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;Web Fraud 2.0;@chenlun;CSIS Security Group;EZDriveMA;fbi;Ford Merrill;ic3;iMessage;Lighthouse;MassDOT;North Texas Toll Authority;RCS;SecAlliance;smishing;SMS phishing;Sunpass;The Toll Roads
Slug: 2025-01-16-chinese-innovations-spawn-wave-of-toll-phishing-via-sms

[Source](https://krebsonsecurity.com/2025/01/chinese-innovations-spawn-wave-of-toll-phishing-via-sms/){:target="_blank" rel="noopener"}

> Residents across the United States are being inundated with text messages purporting to come from toll road operators like E-ZPass, warning that recipients face fines if a delinquent toll fee remains unpaid. Researchers say the surge in SMS spam coincides with new features added to a popular commercial phishing kit sold in China that makes it simple to set up convincing lures spoofing toll road operators in multiple U.S. states. Last week, the Massachusetts Department of Transportation (MassDOT) warned residents to be on the lookout for a new SMS phishing or “smishing” scam targeting users of EZDriveMA, MassDOT’s all electronic [...]
