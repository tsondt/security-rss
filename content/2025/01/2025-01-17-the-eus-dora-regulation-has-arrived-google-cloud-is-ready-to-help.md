Title: The EU’s DORA regulation has arrived. Google Cloud is ready to help
Date: 2025-01-17T17:00:00+00:00
Author: Jeanette Manfra
Category: GCP Security
Tags: Security & Identity
Slug: 2025-01-17-the-eus-dora-regulation-has-arrived-google-cloud-is-ready-to-help

[Source](https://cloud.google.com/blog/products/identity-security/the-eus-dora-has-arrived-google-cloud-is-ready-to-help/){:target="_blank" rel="noopener"}

> As the Digital Operational Resilience Act (DORA) takes effect today, financial entities in the EU must rise to a new level of operational resilience in the face of ever-evolving digital threats. At Google Cloud, we share your commitment to the goals of DORA. We believe in building a more resilient and secure financial sector, and we're here to support you with your DORA compliance journey. Our comprehensive suite of services, resilient infrastructure, and deep understanding of the regulatory landscape can help enable your success. To accelerate your DORA efforts, today we’re excited to share our DORA Customer Guides on the [...]
