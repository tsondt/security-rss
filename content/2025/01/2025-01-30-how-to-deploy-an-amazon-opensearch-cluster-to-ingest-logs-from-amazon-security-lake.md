Title: How to deploy an Amazon OpenSearch cluster to ingest logs from Amazon Security Lake
Date: 2025-01-30T16:17:14+00:00
Author: Kevin Low
Category: AWS Security
Tags: Advanced (300);Amazon Security Lake;Customer Solutions;Security, Identity, & Compliance;Technical How-to;AWS security;Cloud security;Incident response;OpenSearch;Security Blog;threat detection
Slug: 2025-01-30-how-to-deploy-an-amazon-opensearch-cluster-to-ingest-logs-from-amazon-security-lake

[Source](https://aws.amazon.com/blogs/security/how-to-deploy-an-amazon-opensearch-cluster-to-ingest-logs-from-amazon-security-lake/){:target="_blank" rel="noopener"}

> January 30, 2025: This post was republished to make the instructions clearer and compatible with OCSF 1.1. Customers often require multiple log sources across their AWS environment to empower their teams to respond and investigate security events. In part one of this two-part blog post, I show you how you can use Amazon OpenSearch Service [...]
