Title: Microsoft: macOS bug lets hackers install malicious kernel drivers
Date: 2025-01-13T13:24:21-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Apple;Microsoft
Slug: 2025-01-13-microsoft-macos-bug-lets-hackers-install-malicious-kernel-drivers

[Source](https://www.bleepingcomputer.com/news/security/microsoft-macos-bug-lets-hackers-install-malicious-kernel-drivers/){:target="_blank" rel="noopener"}

> Apple recently addressed a macOS vulnerability that allows attackers to bypass System Integrity Protection (SIP) and install malicious kernel drivers by loading third-party kernel extensions. [...]
