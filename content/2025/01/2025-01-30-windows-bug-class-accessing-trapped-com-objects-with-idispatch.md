Title: Windows Bug Class: Accessing Trapped COM Objects with IDispatch
Date: 2025-01-30T09:57:00.001000-08:00
Author: Google Project Zero (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2025-01-30-windows-bug-class-accessing-trapped-com-objects-with-idispatch

[Source](https://googleprojectzero.blogspot.com/2025/01/windows-bug-class-accessing-trapped-com.html){:target="_blank" rel="noopener"}

> Posted by James Forshaw, Google Project Zero Object orientated remoting technologies such as DCOM and.NET Remoting make it very easy to develop an object-orientated interface to a service which can cross process and security boundaries. This is because they're designed to support a wide range of objects, not just those implemented in the service, but any other object compatible with being remoted. For example, if you wanted to expose an XML document across the client-server boundary, you could use a pre-existing COM or.NET library and return that object back to the client. By default when the object is returned it's [...]
