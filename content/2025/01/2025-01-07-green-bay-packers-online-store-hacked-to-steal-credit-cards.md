Title: Green Bay Packers' online store hacked to steal credit cards
Date: 2025-01-07T09:00:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-07-green-bay-packers-online-store-hacked-to-steal-credit-cards

[Source](https://www.bleepingcomputer.com/news/security/green-bay-packers-online-store-hacked-to-steal-credit-cards/){:target="_blank" rel="noopener"}

> The Green Bay Packers American football team is notifying fans that a threat actor hacked its official online retail store in October and injected a card skimmer script to steal customers' personal and payment information. [...]
