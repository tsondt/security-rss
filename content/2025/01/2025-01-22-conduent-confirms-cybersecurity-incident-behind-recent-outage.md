Title: Conduent confirms cybersecurity incident behind recent outage
Date: 2025-01-22T11:56:49-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-22-conduent-confirms-cybersecurity-incident-behind-recent-outage

[Source](https://www.bleepingcomputer.com/news/security/conduent-confirms-cybersecurity-incident-behind-recent-outage/){:target="_blank" rel="noopener"}

> American business services giant and government contractor Conduent confirmed today that a recent outage resulted from what it described as a "cyber security incident." [...]
