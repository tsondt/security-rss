Title: WFH with privacy? 85% of Brit bosses snoop on staff
Date: 2025-01-30T10:15:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-30-wfh-with-privacy-85-of-brit-bosses-snoop-on-staff

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/30/forget_the_idea_of_wfh/){:target="_blank" rel="noopener"}

> Employers remain blissfully unaware/wilfully ignorant of the impact of surveillance on staff More than three-quarters of UK employers admit to using some form of surveillance tech to spy on their remote workers' productivity.... [...]
