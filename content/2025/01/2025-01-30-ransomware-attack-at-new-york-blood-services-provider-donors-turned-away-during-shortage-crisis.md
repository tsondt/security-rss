Title: Ransomware attack at New York blood services provider – donors turned away during shortage crisis
Date: 2025-01-30T14:13:23+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-30-ransomware-attack-at-new-york-blood-services-provider-donors-turned-away-during-shortage-crisis

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/30/ransomware_attack_at_new_york/){:target="_blank" rel="noopener"}

> 400 hospitals and med centers across 15 states rely on its products New York Blood Center Enterprises (NYBCe) is currently in its fifth day of handling a ransomware attack that has led to system disruption.... [...]
