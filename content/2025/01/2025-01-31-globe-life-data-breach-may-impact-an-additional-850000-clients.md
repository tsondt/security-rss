Title: Globe Life data breach may impact an additional 850,000 clients
Date: 2025-01-31T10:24:38-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-01-31-globe-life-data-breach-may-impact-an-additional-850000-clients

[Source](https://www.bleepingcomputer.com/news/security/globe-life-data-breach-may-impact-an-additional-850-000-clients/){:target="_blank" rel="noopener"}

> Insurance giant Globe Life finished the investigation into the data breach it suffered last June and says that the incident may have impacted an additional 850,000 customers. [...]
