Title: TikTok shuts down in the US as Trump throws the company a lifeline
Date: 2025-01-19T11:56:49-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Software;Security;Technology
Slug: 2025-01-19-tiktok-shuts-down-in-the-us-as-trump-throws-the-company-a-lifeline

[Source](https://www.bleepingcomputer.com/news/software/tiktok-shuts-down-in-the-us-as-trump-throws-the-company-a-lifeline/){:target="_blank" rel="noopener"}

> TikTok shut down in the U.S. late Saturday night following the Supreme Court's decision to uphold the law that banned the company over national security concerns. [...]
