Title: AWS achieves HDS certification for 24 AWS Regions
Date: 2025-01-14T20:39:31+00:00
Author: Tea Jioshvili
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Security Blog
Slug: 2025-01-14-aws-achieves-hds-certification-for-24-aws-regions

[Source](https://aws.amazon.com/blogs/security/aws-achieves-hds-certification-for-24-aws-regions/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce a successful completion of the Health Data Hosting (Hébergeur de Données de Santé, HDS) certification audit, and renewal of the HDS certification for 24 AWS Regions. The Agence du Numérique en Santé (ANS), the French governmental agency for health, introduced the HDS certification to strengthen the security and protection of [...]
