Title: Biden signs sweeping cybersecurity order, just in time for Trump to gut it
Date: 2025-01-17T20:23:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-17-biden-signs-sweeping-cybersecurity-order-just-in-time-for-trump-to-gut-it

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/17/biden_cybersecurity_eo/){:target="_blank" rel="noopener"}

> Ransomware, AI, secure software, digital IDs – there's something for everyone in the presidential directive Analysis Joe Biden, in the final days of his US presidency, issued another cybersecurity order that is nearly as vast in scope as it is late in the game.... [...]
