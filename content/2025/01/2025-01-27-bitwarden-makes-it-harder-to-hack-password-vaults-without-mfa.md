Title: Bitwarden makes it harder to hack password vaults without MFA
Date: 2025-01-27T16:00:54-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2025-01-27-bitwarden-makes-it-harder-to-hack-password-vaults-without-mfa

[Source](https://www.bleepingcomputer.com/news/security/bitwarden-makes-it-harder-to-hack-password-vaults-without-mfa/){:target="_blank" rel="noopener"}

> Open-source password manager Bitwarden is adding an extra layer of security for accounts that are not protected by two-factor authentication, requiring email verification before allowing access to accounts. [...]
