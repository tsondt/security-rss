Title: New Android Identity Check locks settings outside trusted locations
Date: 2025-01-23T13:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2025-01-23-new-android-identity-check-locks-settings-outside-trusted-locations

[Source](https://www.bleepingcomputer.com/news/security/new-android-identity-check-locks-settings-outside-trusted-locations/){:target="_blank" rel="noopener"}

> Google has announced a new Android "Identity Check" security feature that lock sensitive settings behind biometric authentication when outside a trusted location. [...]
