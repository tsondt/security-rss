Title: Infoseccer: Private security biz let guard down, exposed 120K+ files
Date: 2025-01-16T10:36:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-16-infoseccer-private-security-biz-let-guard-down-exposed-120k-files

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/16/private_security_biz_lets_guard/){:target="_blank" rel="noopener"}

> Assist Security’s client list includes fashion icons, critical infrastructure orgs A London-based private security company allegedly left more than 120,000 files available online via an unsecured server, an infoseccer told The Register.... [...]
