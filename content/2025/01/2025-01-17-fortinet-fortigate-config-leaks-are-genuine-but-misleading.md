Title: Fortinet: FortiGate config leaks are genuine but misleading
Date: 2025-01-17T18:32:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-17-fortinet-fortigate-config-leaks-are-genuine-but-misleading

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/17/fortinet_fortigate_config_leaks/){:target="_blank" rel="noopener"}

> Competition hots up with Ivanti over who can have the worst start to a year Fortinet has confirmed that previous analyses of records leaked by the Belsen Group are indeed genuine FortiGate configs stolen during a zero-day raid in 2022.... [...]
