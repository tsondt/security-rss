Title: US sanctions Chinese firm, hacker behind telecom and Treasury hacks
Date: 2025-01-17T11:57:01-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2025-01-17-us-sanctions-chinese-firm-hacker-behind-telecom-and-treasury-hacks

[Source](https://www.bleepingcomputer.com/news/security/us-sanctions-chinese-firm-hacker-behind-telecom-and-treasury-hacks/){:target="_blank" rel="noopener"}

> The U.S. Department of the Treasury's Office of Foreign Assets Control (OFAC) has sanctioned Yin Kecheng, a Shanghai-based hacker for his role in the recent Treasury breach and a company associated with the Salt Typhoon threat group. [...]
