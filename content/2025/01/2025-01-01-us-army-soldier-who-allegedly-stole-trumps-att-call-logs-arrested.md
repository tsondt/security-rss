Title: US Army soldier who allegedly stole Trump's AT&amp;T call logs arrested
Date: 2025-01-01T08:32:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-01-01-us-army-soldier-who-allegedly-stole-trumps-att-call-logs-arrested

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/01/us_army_soldier_att_call_logs/){:target="_blank" rel="noopener"}

> Brings the arrest count related to the Snowflake hacks to 3 A US Army soldier has been arrested in Texas after being indicted on two counts of unlawful transfer of confidential phone records information.... [...]
