Title: Apple offers $95 million in Siri privacy violation settlement
Date: 2025-01-03T09:30:31-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2025-01-03-apple-offers-95-million-in-siri-privacy-violation-settlement

[Source](https://www.bleepingcomputer.com/news/security/apple-offers-95-million-in-siri-privacy-violation-settlement/){:target="_blank" rel="noopener"}

> Apple has agreed to pay $95 million to settle a class action lawsuit in the U.S. alleging that its Siri assistant recorded private conversations and shared them with third parties. [...]
