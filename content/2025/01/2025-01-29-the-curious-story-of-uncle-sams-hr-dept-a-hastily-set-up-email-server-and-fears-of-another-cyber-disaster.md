Title: The curious story of Uncle Sam's HR dept, a hastily set up email server, and fears of another cyber disaster
Date: 2025-01-29T02:40:30+00:00
Author: Thomas Claburn, Chris Williams, and Iain Thomson
Category: The Register
Tags: 
Slug: 2025-01-29-the-curious-story-of-uncle-sams-hr-dept-a-hastily-set-up-email-server-and-fears-of-another-cyber-disaster

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/29/opm_email_lawsuit/){:target="_blank" rel="noopener"}

> Lawsuit challenges effort to create federal-wide centralized inbox expected to be used for mass firings Two anonymous US government employees have sued Uncle Sam's HR department – the Office of Personnel Management – claiming the Trump administration's rapid roll out of a new federal email system broke the law.... [...]
