Title: Google takes action after coder reports 'most sophisticated attack I've ever seen'
Date: 2025-01-27T17:01:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-27-google-takes-action-after-coder-reports-most-sophisticated-attack-ive-ever-seen

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/27/google_confirms_action_taken_to/){:target="_blank" rel="noopener"}

> Latest trope is tricky enough to fool even the technical crowd... almost Google says it's now hardening defenses against a sophisticated account takeover scam documented by a programmer last week.... [...]
