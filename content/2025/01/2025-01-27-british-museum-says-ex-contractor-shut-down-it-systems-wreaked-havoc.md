Title: British Museum says ex-contractor 'shut down' IT systems, wreaked havoc
Date: 2025-01-27T09:30:10+00:00
Author: Paul Kunert
Category: The Register
Tags: 
Slug: 2025-01-27-british-museum-says-ex-contractor-shut-down-it-systems-wreaked-havoc

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/27/contractor_trespass_british_museum/){:target="_blank" rel="noopener"}

> Former freelancer cuffed a week after being dismissed by UK's top visitor attraction The British Museum was forced to temporarily close some galleries and exhibitions this weekend after a disgruntled former tech contractor went rogue and shuttered some onsite IT systems.... [...]
