Title: North Korean dev who renamed himself 'Bane' accused of IT worker fraud caper
Date: 2025-01-24T13:45:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-01-24-north-korean-dev-who-renamed-himself-bane-accused-of-it-worker-fraud-caper

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/24/north_korean_devs_and_their/){:target="_blank" rel="noopener"}

> 5 indicted as FBI warns North Korea dials up aggression, plus Russian devs allegedly get in on the act The US is indicting yet another five suspects it believes were involved in North Korea's long-running, fraudulent remote IT worker scheme – including one who changed their last name to "Bane" and scored a gig at a tech biz in San Francisco.... [...]
