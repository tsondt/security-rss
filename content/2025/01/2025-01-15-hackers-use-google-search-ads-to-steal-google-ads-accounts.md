Title: Hackers use Google Search ads to steal Google Ads accounts
Date: 2025-01-15T14:02:34-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-15-hackers-use-google-search-ads-to-steal-google-ads-accounts

[Source](https://www.bleepingcomputer.com/news/security/hackers-use-google-search-ads-to-steal-google-ads-accounts/){:target="_blank" rel="noopener"}

> ​Ironically, cybercriminals now use Google search advertisements to promote phishing sites that steal advertisers' credentials for the Google Ads platform. [...]
