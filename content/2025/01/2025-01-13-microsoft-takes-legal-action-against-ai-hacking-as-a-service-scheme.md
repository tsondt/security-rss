Title: Microsoft Takes Legal Action Against AI “Hacking as a Service” Scheme
Date: 2025-01-13T12:01:55+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;AI;hacking;LLM;Microsoft
Slug: 2025-01-13-microsoft-takes-legal-action-against-ai-hacking-as-a-service-scheme

[Source](https://www.schneier.com/blog/archives/2025/01/microsoft-takes-legal-action-against-ai-hacking-as-a-service-scheme.html){:target="_blank" rel="noopener"}

> Not sure this will matter in the end, but it’s a positive move : Microsoft is accusing three individuals of running a “hacking-as-a-service” scheme that was designed to allow the creation of harmful and illicit content using the company’s platform for AI-generated content. The foreign-based defendants developed tools specifically designed to bypass safety guardrails Microsoft has erected to prevent the creation of harmful content through its generative AI services, said Steven Masada, the assistant general counsel for Microsoft’s Digital Crimes Unit. They then compromised the legitimate accounts of paying customers. They combined those two things to create a fee-based platform [...]
