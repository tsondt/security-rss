Title: How we’re making GKE more transparent with supply-chain attestation and SLSA
Date: 2025-01-24T17:00:00+00:00
Author: Elias Levy
Category: GCP Security
Tags: Containers & Kubernetes;GKE;Security & Identity
Slug: 2025-01-24-how-were-making-gke-more-transparent-with-supply-chain-attestation-and-slsa

[Source](https://cloud.google.com/blog/products/identity-security/how-were-making-gke-more-secure-with-supply-chain-attestation-and-slsa/){:target="_blank" rel="noopener"}

> What goes into your Kubernetes software? Understanding the origin of the software components you deploy is crucial for mitigating risks and ensuring the trustworthiness of your applications. To do this, you need to know your software supply chain. Google Cloud is committed to providing tools and features that enhance software supply chain transparency, and today we're excited to announce that you can now verify the integrity of Google Kubernetes Engine components with SLSA, the Supply-chain Levels for Software Artifacts framework. SLSA is a set of standards that can help attest the integrity of software components. We’ve begun to publish SLSA [...]
