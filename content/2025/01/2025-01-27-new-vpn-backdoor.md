Title: New VPN Backdoor
Date: 2025-01-27T12:02:44+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;backdoors;malware;VPN
Slug: 2025-01-27-new-vpn-backdoor

[Source](https://www.schneier.com/blog/archives/2025/01/new-vpn-backdoor.html){:target="_blank" rel="noopener"}

> A newly discovered VPN backdoor uses some interesting tactics to avoid detection: When threat actors use backdoor malware to gain access to a network, they want to make sure all their hard work can’t be leveraged by competing groups or detected by defenders. One countermeasure is to equip the backdoor with a passive agent that remains dormant until it receives what’s known in the business as a “magic packet.” On Thursday, researchers revealed that a never-before-seen backdoor that quietly took hold of dozens of enterprise VPNs running Juniper Network’s Junos OS has been doing just that. J-Magic, the tracking name [...]
