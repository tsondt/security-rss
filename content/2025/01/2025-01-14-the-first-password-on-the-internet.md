Title: The First Password on the Internet
Date: 2025-01-14T12:00:43+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;history of security;Internet;passwords
Slug: 2025-01-14-the-first-password-on-the-internet

[Source](https://www.schneier.com/blog/archives/2025/01/the-first-password-on-the-internet.html){:target="_blank" rel="noopener"}

> It was created in 1973 by Peter Kirstein: So from the beginning I put password protection on my gateway. This had been done in such a way that even if UK users telephoned directly into the communications computer provided by Darpa in UCL, they would require a password. In fact this was the first password on Arpanet. It proved invaluable in satisfying authorities on both sides of the Atlantic for the 15 years I ran the service ­ during which no security breach occurred over my link. I also put in place a system of governance that any UK users [...]
