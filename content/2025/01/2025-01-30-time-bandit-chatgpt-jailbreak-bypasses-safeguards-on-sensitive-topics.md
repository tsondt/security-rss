Title: Time Bandit ChatGPT jailbreak bypasses safeguards on sensitive topics
Date: 2025-01-30T07:00:00-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2025-01-30-time-bandit-chatgpt-jailbreak-bypasses-safeguards-on-sensitive-topics

[Source](https://www.bleepingcomputer.com/news/security/time-bandit-chatgpt-jailbreak-bypasses-safeguards-on-sensitive-topics/){:target="_blank" rel="noopener"}

> A ChatGPT jailbreak flaw, dubbed "Time Bandit," allows you to bypass OpenAI's safety guidelines when asking for detailed instructions on sensitive topics, including the creation of weapons, information on nuclear topics, and malware creation. [...]
