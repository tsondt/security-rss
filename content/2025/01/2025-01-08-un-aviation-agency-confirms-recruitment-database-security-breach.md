Title: UN aviation agency confirms recruitment database security breach
Date: 2025-01-08T08:30:46-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-08-un-aviation-agency-confirms-recruitment-database-security-breach

[Source](https://www.bleepingcomputer.com/news/security/un-aviation-agency-confirms-recruitment-database-security-breach/){:target="_blank" rel="noopener"}

> ​The United Nations' International Civil Aviation Organization (ICAO) has confirmed that a threat actor has stolen approximately 42,000 records after hacking into its recruitment database. [...]
