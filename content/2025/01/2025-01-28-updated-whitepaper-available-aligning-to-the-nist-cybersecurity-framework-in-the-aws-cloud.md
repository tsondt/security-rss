Title: Updated whitepaper available: Aligning to the NIST Cybersecurity Framework in the AWS Cloud
Date: 2025-01-28T22:13:02+00:00
Author: Luca Iannario
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;CSF;cybersecurity;Cybersecurity Framework;Federal Information Security Modernization Act;FISMA;National Institute of Standards and Technology;NIST;NIST CSF;Security;Security Blog
Slug: 2025-01-28-updated-whitepaper-available-aligning-to-the-nist-cybersecurity-framework-in-the-aws-cloud

[Source](https://aws.amazon.com/blogs/security/updated-whitepaper-available-aligning-to-the-nist-cybersecurity-framework-in-the-aws-cloud/){:target="_blank" rel="noopener"}

> Today, we released an updated version of the Aligning to the NIST Cybersecurity Framework (CSF) in the AWS Cloud whitepaper to reflect the significant changes introduced in the National Institute of Standards and Technology (NIST) Cybersecurity Framework (CSF) 2.0, published in February 2024. This comprehensive update helps you understand how AWS services align with the [...]
