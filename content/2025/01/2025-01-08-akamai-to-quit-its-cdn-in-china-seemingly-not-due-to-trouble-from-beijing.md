Title: Akamai to quit its CDN in China, seemingly not due to trouble from Beijing
Date: 2025-01-08T06:31:06+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2025-01-08-akamai-to-quit-its-cdn-in-china-seemingly-not-due-to-trouble-from-beijing

[Source](https://go.theregister.com/feed/www.theregister.com/2025/01/08/akamai_eol_china_cdn/){:target="_blank" rel="noopener"}

> Security and cloud compute have so much more upside than the boring business of shifting bits Akamai has decided to end its content delivery network services in China, but not because it’s finding it hard to do business in the Middle Kingdom.... [...]
