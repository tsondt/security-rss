Title: UK domain registry Nominet confirms breach via Ivanti zero-day
Date: 2025-01-13T11:50:12-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-13-uk-domain-registry-nominet-confirms-breach-via-ivanti-zero-day

[Source](https://www.bleepingcomputer.com/news/security/uk-domain-registry-nominet-confirms-breach-via-ivanti-zero-day-vulnerability/){:target="_blank" rel="noopener"}

> Nominet, the official.UK domain registry and one of the largest country code registries, has confirmed that its network was breached two weeks ago using an Ivanti VPN zero-day vulnerability. [...]
