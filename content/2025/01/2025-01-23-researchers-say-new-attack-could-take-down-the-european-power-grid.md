Title: Researchers say new attack could take down the European power grid
Date: 2025-01-23T12:00:42+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Features;Security;funkrundsteuerung;power grid;radio ripple control
Slug: 2025-01-23-researchers-say-new-attack-could-take-down-the-european-power-grid

[Source](https://arstechnica.com/security/2025/01/could-hackers-use-new-attack-to-take-down-european-power-grid/){:target="_blank" rel="noopener"}

> Late last month, researchers revealed a finding that’s likely to shock some people and confirm the low expectations of others: Renewable energy facilities throughout Central Europe use unencrypted radio signals to receive commands to feed or ditch power into or from the grid that serves some 450 million people throughout the continent. Fabian Bräunlein and Luca Melette stumbled on their discovery largely by accident while working on what they thought would be a much different sort of hacking project. After observing a radio receiver on the streetlight poles throughout Berlin, they got to wondering: Would it be possible for someone [...]
