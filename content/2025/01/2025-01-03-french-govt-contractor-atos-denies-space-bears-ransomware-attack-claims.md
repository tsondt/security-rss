Title: French govt contractor Atos denies Space Bears ransomware attack claims
Date: 2025-01-03T09:20:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-01-03-french-govt-contractor-atos-denies-space-bears-ransomware-attack-claims

[Source](https://www.bleepingcomputer.com/news/security/french-govt-contractor-atos-denies-space-bears-ransomware-attack-claims/){:target="_blank" rel="noopener"}

> French tech giant Atos, which secures communications for the country's military and secret services, has denied claims made by the Space Bears ransomware gang that they compromised one of its databases. [...]
