Title: Social Engineering to Disable iMessage Protections
Date: 2025-01-17T12:05:27+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;phishing;SMS;social engineering
Slug: 2025-01-17-social-engineering-to-disable-imessage-protections

[Source](https://www.schneier.com/blog/archives/2025/01/social-engineering-to-disable-imessage-protections.html){:target="_blank" rel="noopener"}

> I am always interested in new phishing tricks, and watching them spread across the ecosystem. A few days ago I started getting phishing SMS messages with a new twist. They were standard messages about delayed packages or somesuch, with the goal of getting me to click on a link and entering some personal information into a website. But because they came from unknown phone numbers, the links did not work. So—this is the new bit—the messages said something like: “Please reply Y, then exit the text message, reopen the text message activation link, or copy the link to Safari browser [...]
