Title: Stolen Path of Exile 2 admin account used to hack player accounts
Date: 2025-01-13T15:33:46-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2025-01-13-stolen-path-of-exile-2-admin-account-used-to-hack-player-accounts

[Source](https://www.bleepingcomputer.com/news/security/stolen-path-of-exile-2-admin-account-used-to-hack-player-accounts/){:target="_blank" rel="noopener"}

> Path of Exile 2 developers confirmed that a hacked admin account allowed a threat actor to change the password and access at least 66 accounts, finally explaining how PoE 2 accounts have been breached since November. [...]
