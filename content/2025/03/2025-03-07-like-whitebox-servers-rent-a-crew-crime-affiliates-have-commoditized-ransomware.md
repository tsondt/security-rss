Title: Like whitebox servers, rent-a-crew crime 'affiliates' have commoditized ransomware
Date: 2025-03-07T11:31:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-03-07-like-whitebox-servers-rent-a-crew-crime-affiliates-have-commoditized-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/07/commoditization_ransomware/){:target="_blank" rel="noopener"}

> Which is why taking down chiefs and infra behind big name brand operations isn't working Interview There's a handful of cybercriminal gangs that Jason Baker, a ransomware negotiator with GuidePoint Security, regularly gets called in to respond to these days, and a year ago only one of these crews — Akira — was on threat hunters' radars and infecting organizations with the same ferocity as it is today.... [...]
