Title: Governments can't seem to stop asking for secret backdoors
Date: 2025-03-03T09:30:11+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2025-03-03-governments-cant-seem-to-stop-asking-for-secret-backdoors

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/03/opinion_e2ee/){:target="_blank" rel="noopener"}

> Cut off one head and 100 grow back? Decapitation may not be the way to go Opinion With Apple pulling the plug on at-rest end-to-end encryption (E2EE) for UK users, and Signal threatening to pull out of Sweden if that government demands E2EE backdoors, it's looking bleak.... [...]
