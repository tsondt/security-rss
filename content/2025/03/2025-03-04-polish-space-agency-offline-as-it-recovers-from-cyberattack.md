Title: Polish Space Agency offline as it recovers from cyberattack
Date: 2025-03-04T10:32:01-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-03-04-polish-space-agency-offline-as-it-recovers-from-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/polish-space-agency-offline-as-it-recovers-from-cyberattack/){:target="_blank" rel="noopener"}

> ​The Polish Space Agency (POLSA) has been offline since it disconnected its systems from the Internet over the weekend to contain a breach of its IT infrastructure. [...]
