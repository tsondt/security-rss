Title: C++ creator calls for help to defend programming language from 'serious attacks'
Date: 2025-03-02T17:46:13+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2025-03-02-c-creator-calls-for-help-to-defend-programming-language-from-serious-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/02/c_creator_calls_for_action/){:target="_blank" rel="noopener"}

> Bjarne Stroustrup says standards committee needs to show it can respond to memory safety push Bjarne Stroustrup, creator of C++, has issued a call for the C++ community to defend the programming language, which has been shunned by cybersecurity agencies and technical experts in recent years for its memory safety shortcomings.... [...]
