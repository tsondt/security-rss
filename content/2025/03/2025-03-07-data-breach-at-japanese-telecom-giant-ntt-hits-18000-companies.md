Title: Data breach at Japanese telecom giant NTT hits 18,000 companies
Date: 2025-03-07T08:48:21-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-03-07-data-breach-at-japanese-telecom-giant-ntt-hits-18000-companies

[Source](https://www.bleepingcomputer.com/news/security/data-breach-at-japanese-telecom-giant-ntt-hits-18-000-companies/){:target="_blank" rel="noopener"}

> Japanese telecommunication services provider NTT Communications Corporation (NTT) is warning almost 18,000 corporate customers that their information was compromised during a cybersecurity incident. [...]
