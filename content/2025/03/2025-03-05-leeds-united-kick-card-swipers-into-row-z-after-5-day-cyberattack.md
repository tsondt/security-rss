Title: Leeds United kick card swipers into Row Z after 5-day cyberattack
Date: 2025-03-05T12:00:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-03-05-leeds-united-kick-card-swipers-into-row-z-after-5-day-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/05/leeds_united_card_swipers/){:target="_blank" rel="noopener"}

> English football club offers apologies after fans' card details stolen from online retail store English football club Leeds United says cyber criminals targeted its retail website during a five-day assault in February and stole the card details of "a small number of customers."... [...]
