Title: YouTube warns of AI-generated video of its CEO used in phishing attacks
Date: 2025-03-05T10:27:56-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-03-05-youtube-warns-of-ai-generated-video-of-its-ceo-used-in-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/youtube-warns-of-ai-generated-video-of-its-ceo-used-in-phishing-attacks/){:target="_blank" rel="noopener"}

> YouTube warns that scammers are using an AI-generated video featuring the company's CEO in phishing attacks to steal creators' credentials. [...]
