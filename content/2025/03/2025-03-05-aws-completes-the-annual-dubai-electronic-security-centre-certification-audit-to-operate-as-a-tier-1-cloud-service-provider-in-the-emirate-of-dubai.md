Title: AWS completes the annual Dubai Electronic Security Centre certification audit to operate as a Tier 1 cloud service provider in the Emirate of Dubai
Date: 2025-03-05T18:52:32+00:00
Author: Vishal Pabari
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Abu Dhabi;Auditing;AWS security;Cloud Service Provider;Compliance;CSA STAR;CSP;DESC;Dubai;Dubai Electronic Security Centre;Emirate of Dubai;Government;IAR;Information Security Regulation;ISO;Licence;Middle East;Public Sector;Security;Security Blog;UAE
Slug: 2025-03-05-aws-completes-the-annual-dubai-electronic-security-centre-certification-audit-to-operate-as-a-tier-1-cloud-service-provider-in-the-emirate-of-dubai

[Source](https://aws.amazon.com/blogs/security/aws-completes-the-annual-dubai-electronic-security-centre-certification-audit-to-operate-as-a-tier-1-cloud-service-provider-in-the-emirate-of-dubai-2/){:target="_blank" rel="noopener"}

> We’re excited to announce that Amazon Web Services (AWS) has completed the annual Dubai Electronic Security Centre (DESC) certification audit to operate as a Tier 1 Cloud Service Provider (CSP) for the AWS Middle East (UAE) Region. This alignment with DESC requirements demonstrates our continued commitment to adhere to the heightened expectations for CSPs. Government [...]
