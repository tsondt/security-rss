Title: Feds name and charge alleged Silk Typhoon spies behind years of China-on-US attacks
Date: 2025-03-06T00:47:17+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-03-06-feds-name-and-charge-alleged-silk-typhoon-spies-behind-years-of-china-on-us-attacks

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/06/fbi_china_pays_75k_per/){:target="_blank" rel="noopener"}

> Xi's freelance infosec warriors apparently paid up to $75K to crack a single American inbox US government agencies announced Wednesday criminal charges against alleged members of China's Silk Typhoon gang, plus internet domain seizures linked to a long-term Chinese espionage campaign that saw Beijing hire miscreants to compromise US government agencies and other major orgs.... [...]
