Title: Look up: The new frontier of cyberthreats is in the sky
Date: 2025-03-05T10:01:11-05:00
Author: Sponsored by Acronis
Category: BleepingComputer
Tags: Security
Slug: 2025-03-05-look-up-the-new-frontier-of-cyberthreats-is-in-the-sky

[Source](https://www.bleepingcomputer.com/news/security/look-up-the-new-frontier-of-cyberthreats-is-in-the-sky/){:target="_blank" rel="noopener"}

> With increased unidentified drone sightings worldwide, some are concerned they pose a cybersecurity risk. Learn more from Acronis about these risks and a real attack on a Taiwan drone manufacturer. [...]
