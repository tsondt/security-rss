Title: Hunters International ransomware claims attack on Tata Technologies
Date: 2025-03-04T10:04:54-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-03-04-hunters-international-ransomware-claims-attack-on-tata-technologies

[Source](https://www.bleepingcomputer.com/news/security/hunters-international-ransomware-claims-attack-on-tata-technologies/){:target="_blank" rel="noopener"}

> The Hunters International ransomware gang has claimed responsibility for a January cyberattack attack on Tata Technologies, stating they stole 1.4TB of data from the company. [...]
