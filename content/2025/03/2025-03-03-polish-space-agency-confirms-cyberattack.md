Title: Polish space agency confirms cyberattack
Date: 2025-03-03T12:45:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-03-03-polish-space-agency-confirms-cyberattack

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/03/polish_space_agency_confirms_cyberattack/){:target="_blank" rel="noopener"}

> Officials vow to uncover who was behind it The Polish Space Agency (POLSA) is currently dealing with a "cybersecurity incident," it confirmed via its X account on Sunday.... [...]
