Title: US Cyber Command reportedly pauses cyberattacks on Russia
Date: 2025-03-03T03:31:11+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-03-03-us-cyber-command-reportedly-pauses-cyberattacks-on-russia

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/03/infosec_in_brief/){:target="_blank" rel="noopener"}

> PLUS: Phishing suspects used fishing gear as alibi; Apple's 'Find My' can track PCs and Androids; and more Infosec In Brief US Defense Secretary Pete Hegseth has reportedly ordered US Cyber Command to pause offensive operations against Russia – as the USA’s Cybersecurity and Infrastructure Security Agency (CISA) has denied any change in its posture.... [...]
