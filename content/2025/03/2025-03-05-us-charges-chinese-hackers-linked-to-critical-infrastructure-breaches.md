Title: US charges Chinese hackers linked to critical infrastructure breaches
Date: 2025-03-05T12:23:48-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-03-05-us-charges-chinese-hackers-linked-to-critical-infrastructure-breaches

[Source](https://www.bleepingcomputer.com/news/security/us-charges-chinese-hackers-linked-to-critical-infrastructure-breaches/){:target="_blank" rel="noopener"}

> The US Justice Department has charged Chinese state security officers along with APT27 and i-Soon hackers for network breaches and cyberattacks that have targeted victims globally since 2011. [...]
