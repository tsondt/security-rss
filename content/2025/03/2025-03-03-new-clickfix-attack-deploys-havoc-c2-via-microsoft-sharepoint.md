Title: New ClickFix attack deploys Havoc C2 via Microsoft Sharepoint
Date: 2025-03-03T12:33:52-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2025-03-03-new-clickfix-attack-deploys-havoc-c2-via-microsoft-sharepoint

[Source](https://www.bleepingcomputer.com/news/security/new-clickfix-attack-deploys-havoc-c2-via-microsoft-sharepoint/){:target="_blank" rel="noopener"}

> A newly uncovered ClickFix phishing campaign is tricking victims into executing malicious PowerShell commands that deploy the Havok post-exploitation framework for remote access to compromised devices. [...]
