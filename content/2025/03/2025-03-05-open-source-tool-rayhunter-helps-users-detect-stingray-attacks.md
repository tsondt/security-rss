Title: Open-source tool 'Rayhunter' helps users detect Stingray attacks
Date: 2025-03-05T15:36:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Mobile;Software
Slug: 2025-03-05-open-source-tool-rayhunter-helps-users-detect-stingray-attacks

[Source](https://www.bleepingcomputer.com/news/security/open-source-tool-rayhunter-helps-users-detect-stingray-attacks/){:target="_blank" rel="noopener"}

> The Electronic Frontier Foundation (EFF) has released a free, open-source tool named Rayhunter that is designed to detect cell-site simulators (CSS), also known as IMSI catchers or Stingrays. [...]
