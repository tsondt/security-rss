Title: Toronto Zoo ransomware crooks snatch decades of visitor data
Date: 2025-03-06T15:14:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-03-06-toronto-zoo-ransomware-crooks-snatch-decades-of-visitor-data

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/06/toronto_zoo_ransomware/){:target="_blank" rel="noopener"}

> Akira really wasn't horsing around with this one Toronto Zoo's final update on its January 2024 cyberattack arrived this week, revealing that visitor data going back to 2000 had been compromised.... [...]
