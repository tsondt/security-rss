Title: Ransomware thugs threaten Tata Technologies with leak if demands not met
Date: 2025-03-05T01:14:08+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-03-05-ransomware-thugs-threaten-tata-technologies-with-leak-if-demands-not-met

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/05/tata_technologies_hiunters_international/){:target="_blank" rel="noopener"}

> Hunters International ready to off-shore 1.4 TB of info allegedly swiped from Indian giant A subsidiary of Indian multinational Tata has allegedly fallen victim to the notorious ransomware gang Hunters International.... [...]
