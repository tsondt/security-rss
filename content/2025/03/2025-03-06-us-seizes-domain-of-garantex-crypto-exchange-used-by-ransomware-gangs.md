Title: US seizes domain of Garantex crypto exchange used by ransomware gangs
Date: 2025-03-06T14:07:53-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2025-03-06-us-seizes-domain-of-garantex-crypto-exchange-used-by-ransomware-gangs

[Source](https://www.bleepingcomputer.com/news/security/us-seizes-domain-of-garantex-crypto-exchange-used-by-ransomware-gangs/){:target="_blank" rel="noopener"}

> The U.S. Secret Service has seized the domain of the sanctioned Russian cryptocurrency exchange Garantex in collaboration with the Department of Justice's Criminal Division, the FBI, and Europol. [...]
