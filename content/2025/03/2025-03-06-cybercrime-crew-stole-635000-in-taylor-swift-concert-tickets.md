Title: Cybercrime 'crew' stole $635,000 in Taylor Swift concert tickets
Date: 2025-03-06T13:05:45-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-03-06-cybercrime-crew-stole-635000-in-taylor-swift-concert-tickets

[Source](https://www.bleepingcomputer.com/news/security/cybercrime-crew-stole-635-000-in-taylor-swift-concert-tickets/){:target="_blank" rel="noopener"}

> New York prosecutors say that two people working at a third-party contractor for the StubHub online ticket marketplace made $635,000 after almost 1,000 concert tickets and reselling them online. [...]
