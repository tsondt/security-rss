Title: The Badbox botnet is back, powered by up to a million backdoored Androids
Date: 2025-03-07T01:51:17+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-03-07-the-badbox-botnet-is-back-powered-by-up-to-a-million-backdoored-androids

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/07/badbox_botnet_returns/){:target="_blank" rel="noopener"}

> Best not to buy cheap hardware and use third-party app stores if you want to stay clear of this vast ad fraud effort Human Security’s Satori research team says it has found a new variant of the remote-controllable Badbox malware, and as many as a million infected Android devices running it to form a massive botnet.... [...]
