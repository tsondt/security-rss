Title: Threat posed by new VMware hyperjacking vulnerabilities is hard to overstate
Date: 2025-03-04T21:33:36+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;hypervsor;Virtual Machine;vmware;vulnerabilities
Slug: 2025-03-04-threat-posed-by-new-vmware-hyperjacking-vulnerabilities-is-hard-to-overstate

[Source](https://arstechnica.com/security/2025/03/vmware-patches-3-critical-vulnerabilities-in-multiple-product-lines/){:target="_blank" rel="noopener"}

> Three critical vulnerabilities in multiple virtual-machine products from VMware can give hackers unusually broad access to some of the most sensitive environments inside multiple customers’ networks, the company and outside researchers warned Tuesday. The class of attack made possible by exploiting the vulnerabilities is known under several names, including hyperjacking, hypervisor attack, or virtual machine escape. Virtual machines often run inside hosting environments to prevent one customer from being able to access or control the resources of other customers. By breaking out of one customer’s isolated VM environment, a threat actor could take control of the hypervisor that apportions each [...]
