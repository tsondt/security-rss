Title: Trojaned AI Tool Leads to Disney Hack
Date: 2025-03-04T12:08:31+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;AI;credentials;cybersecurity;hacking
Slug: 2025-03-04-trojaned-ai-tool-leads-to-disney-hack

[Source](https://www.schneier.com/blog/archives/2025/03/trojaned-ai-tool-leads-to-disney-hack.html){:target="_blank" rel="noopener"}

> This is a sad story of someone who downloaded a Trojaned AI tool that resulted in hackers taking over his computer and, ultimately, costing him his job. [...]
