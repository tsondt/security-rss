Title: Kernel saunters – How Apple rearranged its XNU kernel with exclaves
Date: 2025-03-08T16:07:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2025-03-08-kernel-saunters-how-apple-rearranged-its-xnu-kernel-with-exclaves

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/08/kernel_sanders_apple_rearranges_xnu/){:target="_blank" rel="noopener"}

> iPhone giant compartmentalizes OS for the sake of security Apple has been working to harden the XNU kernel that powers its various operating systems, including iOS and macOS, with a feature called "exclaves."... [...]
