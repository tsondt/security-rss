Title: Employee charged with stealing unreleased movies, sharing them online
Date: 2025-03-07T12:20:01-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-03-07-employee-charged-with-stealing-unreleased-movies-sharing-them-online

[Source](https://www.bleepingcomputer.com/news/security/employee-charged-with-stealing-unreleased-movies-sharing-them-online/){:target="_blank" rel="noopener"}

> A Memphis man was arrested and charged with stealing DVDs and Blu-ray discs of unreleased movies and sharing ripped digital copies online before their release. [...]
