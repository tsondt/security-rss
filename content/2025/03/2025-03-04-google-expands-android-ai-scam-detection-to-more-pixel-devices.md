Title: Google expands Android AI scam detection to more Pixel devices
Date: 2025-03-04T12:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2025-03-04-google-expands-android-ai-scam-detection-to-more-pixel-devices

[Source](https://www.bleepingcomputer.com/news/security/google-expands-android-ai-scam-detection-to-more-pixel-devices/){:target="_blank" rel="noopener"}

> Google has announced an increased rollout of new AI-powered scam detection features on Android to help protect users from increasingly sophisticated phone and text social engineering scams. [...]
