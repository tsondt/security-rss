Title: Alleged cyber scalpers Swiftly cuffed over $635K Taylor ticket heist
Date: 2025-03-07T15:28:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-03-07-alleged-cyber-scalpers-swiftly-cuffed-over-635k-taylor-ticket-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/07/stubhub_taylor_swift_scammers/){:target="_blank" rel="noopener"}

> I knew you were trouble, Queens DA might have said Police have made two arrests in their quest to start a cybercrime crew's prison eras, alleging the pair stole hundreds of Taylor Swift tickets and sold them for huge profit.... [...]
