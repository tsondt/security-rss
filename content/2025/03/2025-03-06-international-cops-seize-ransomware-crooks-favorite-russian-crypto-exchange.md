Title: International cops seize ransomware crooks' favorite Russian crypto exchange
Date: 2025-03-06T22:32:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-03-06-international-cops-seize-ransomware-crooks-favorite-russian-crypto-exchange

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/06/international_cops_seize_ransomware_gangs/){:target="_blank" rel="noopener"}

> Did US Secret Service not get the memo, or? A coalition of international law enforcement has shut down Russian cryptocurrency exchange Garantex, a favorite of now-defunct ransomware crew Conti and others criminals for money laundering.... [...]
