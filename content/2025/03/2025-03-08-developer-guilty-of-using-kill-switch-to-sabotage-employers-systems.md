Title: Developer guilty of using kill switch to sabotage employer's systems
Date: 2025-03-08T12:43:15-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2025-03-08-developer-guilty-of-using-kill-switch-to-sabotage-employers-systems

[Source](https://www.bleepingcomputer.com/news/security/developer-guilty-of-using-kill-switch-to-sabotage-employers-systems/){:target="_blank" rel="noopener"}

> A software developer has been found guilty of sabotaging his ex-employer's systems by running custom malware and installing a "kill switch" after being demoted at the company. [...]
