Title: US seizes $23 million in crypto stolen via password manager breach
Date: 2025-03-07T14:13:07-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2025-03-07-us-seizes-23-million-in-crypto-stolen-via-password-manager-breach

[Source](https://www.bleepingcomputer.com/news/security/us-seizes-23-million-in-crypto-stolen-via-password-manager-breach/){:target="_blank" rel="noopener"}

> U.S. authorities have seized over $23 million in cryptocurrency linked to the theft of $150 million from a Ripple crypto wallet in January 2024. Investigators believe hackers who breached LastPass in 2022 were behind the attack. [...]
