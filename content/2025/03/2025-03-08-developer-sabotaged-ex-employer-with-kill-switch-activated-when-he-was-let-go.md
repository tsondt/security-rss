Title: Developer sabotaged ex-employer with kill switch activated when he was let go
Date: 2025-03-08T01:09:40+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-03-08-developer-sabotaged-ex-employer-with-kill-switch-activated-when-he-was-let-go

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/08/developer_server_kill_switch/){:target="_blank" rel="noopener"}

> IsDavisLuEnabledInActiveDirectory? Not any more. IsDavisLuGuilty? Yes. IsDavisLuFacingJail? Also yes A federal jury in Cleveland has found a senior software developer guilty of sabotaging his employer's systems – and he's now facing a potential ten years behind bars.... [...]
