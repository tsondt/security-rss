Title: Cisco warns of Webex for BroadWorks flaw exposing credentials
Date: 2025-03-04T13:40:08-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-03-04-cisco-warns-of-webex-for-broadworks-flaw-exposing-credentials

[Source](https://www.bleepingcomputer.com/news/security/cisco-warns-of-webex-for-broadworks-flaw-exposing-credentials/){:target="_blank" rel="noopener"}

> Cisco warned customers today of a vulnerability in Webex for BroadWorks that could let unauthenticated attackers access credentials remotely. [...]
