Title: Nearly 12,000 API keys and passwords found in AI training dataset
Date: 2025-03-02T10:23:36-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2025-03-02-nearly-12000-api-keys-and-passwords-found-in-ai-training-dataset

[Source](https://www.bleepingcomputer.com/news/security/nearly-12-000-api-keys-and-passwords-found-in-ai-training-dataset/){:target="_blank" rel="noopener"}

> Close to 12,000 valid secrets that include API keys and passwords have been found in the Common Crawl dataset used for training multiple artificial intelligence models. [...]
