Title: The Combined Cipher Machine
Date: 2025-03-06T12:01:32+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptography;history of cryptography;military
Slug: 2025-03-06-the-combined-cipher-machine

[Source](https://www.schneier.com/blog/archives/2025/03/the-combined-cipher-machine.html){:target="_blank" rel="noopener"}

> Interesting article —with photos!—of the US/UK “Combined Cipher Machine” from WWII. [...]
