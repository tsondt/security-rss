Title: Microsoft: North Korean hackers join Qilin ransomware gang
Date: 2025-03-07T07:10:18-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2025-03-07-microsoft-north-korean-hackers-join-qilin-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/microsoft-north-korean-hackers-now-deploying-qilin-ransomware/){:target="_blank" rel="noopener"}

> Microsoft says a North Korean hacking group tracked as Moonstone Sleet has deployed Qilin ransomware payloads in a limited number of attacks. [...]
