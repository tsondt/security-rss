Title: China's Silk Typhoon, tied to US Treasury break-in, now hammers IT and govt targets
Date: 2025-03-05T17:22:03+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-03-05-chinas-silk-typhoon-tied-to-us-treasury-break-in-now-hammers-it-and-govt-targets

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/05/china_silk_typhoon_update/){:target="_blank" rel="noopener"}

> They're good at zero-day exploits, too Updated Silk Typhoon, the Chinese government crew believed to be behind the December US Treasury intrusions, has been abusing stolen API keys and cloud credentials in ongoing attacks targeting IT companies and state and local government agencies since late 2024, according to Microsoft Threat Intelligence.... [...]
