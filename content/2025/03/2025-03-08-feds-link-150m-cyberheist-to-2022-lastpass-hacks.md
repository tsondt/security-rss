Title: Feds Link $150M Cyberheist to 2022 LastPass Hacks
Date: 2025-03-08T01:20:05+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Data Breaches;Chris Larsen;fbi;Karim Toubba;lastpass breach;Nick Bax;Ripple;Taylor Monahan;U.S. Secret Service;ZachXBT
Slug: 2025-03-08-feds-link-150m-cyberheist-to-2022-lastpass-hacks

[Source](https://krebsonsecurity.com/2025/03/feds-link-150m-cyberheist-to-2022-lastpass-hacks/){:target="_blank" rel="noopener"}

> In September 2023, KrebsOnSecurity published findings from security researchers who concluded that a series of six-figure cyberheists across dozens of victims resulted from thieves cracking master passwords stolen from the password manager service LastPass in 2022. In a court filing this week, U.S. federal agents investigating a spectacular $150 million cryptocurrency heist said they had reached the same conclusion. On March 6, federal prosecutors in northern California said they seized approximately $24 million worth of cryptocurrencies that were clawed back following a $150 million cyberheist on Jan. 30, 2024. The complaint refers to the person robbed only as “Victim-1,” but [...]
