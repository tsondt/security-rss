Title: Fake BianLian ransom notes mailed to US CEOs in postal mail scam
Date: 2025-03-04T21:18:20-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2025-03-04-fake-bianlian-ransom-notes-mailed-to-us-ceos-in-postal-mail-scam

[Source](https://www.bleepingcomputer.com/news/security/fake-bianlian-ransom-notes-mailed-to-us-ceos-in-postal-mail-scam/){:target="_blank" rel="noopener"}

> Scammers are impersonating the BianLian ransomware gang in fake ransom notes sent to US companies via snail mail through the United States Postal Service. [...]
