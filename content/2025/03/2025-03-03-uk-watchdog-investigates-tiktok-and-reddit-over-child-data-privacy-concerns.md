Title: UK watchdog investigates TikTok and Reddit over child data privacy concerns
Date: 2025-03-03T12:23:24+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2025-03-03-uk-watchdog-investigates-tiktok-and-reddit-over-child-data-privacy-concerns

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/03/uk_regulator_investigates_tiktok_and/){:target="_blank" rel="noopener"}

> ICO looking at what data is used to serve up recommendations The UK's data protection watchdog has launched three investigations into certain social media platforms following concerns about the protection of privacy among teenage users.... [...]
