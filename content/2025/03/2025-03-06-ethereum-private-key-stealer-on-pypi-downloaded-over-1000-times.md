Title: Ethereum private key stealer on PyPI downloaded over 1,000 times
Date: 2025-03-06T12:11:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2025-03-06-ethereum-private-key-stealer-on-pypi-downloaded-over-1000-times

[Source](https://www.bleepingcomputer.com/news/security/ethereum-private-key-stealer-on-pypi-downloaded-over-1-000-times/){:target="_blank" rel="noopener"}

> A malicious Python Package Index (PyPI) package named "set-utils" has been stealing Ethereum private keys through intercepted wallet creation functions and exfiltrating them via the Polygon blockchain. [...]
