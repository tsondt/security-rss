Title: Cybereason CEO leaves after months of boardroom blowups
Date: 2025-03-06T04:46:29+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-03-06-cybereason-ceo-leaves-after-months-of-boardroom-blowups

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/06/cybereason_ceo_leaves/){:target="_blank" rel="noopener"}

> Complaint alleges 13 funding proposals foundered amid battle for control Eric Gan is no longer CEO of AI security biz Cybereason after what appears to have been a protracted and unpleasant fight with investors, including the SoftBank Vision Fund and Liberty Strategic Capital.... [...]
