Title: Microsoft unveils finalized EU Data Boundary as European doubt over US grows
Date: 2025-03-03T13:15:12+00:00
Author: Richard Speed
Category: The Register
Tags: 
Slug: 2025-03-03-microsoft-unveils-finalized-eu-data-boundary-as-european-doubt-over-us-grows

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/03/microsoft_unveils_a_finalized_eu/){:target="_blank" rel="noopener"}

> Some may have second thoughts about going all-in with an American vendor, no matter where their data is stored Microsoft has completed its EU data boundary, however, analysts and some regional cloud players are voicing concerns over dependencies on a US entity, even with the guarantees in place.... [...]
