Title: Silk Typhoon hackers now target IT supply chains to breach networks
Date: 2025-03-05T13:18:39-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud
Slug: 2025-03-05-silk-typhoon-hackers-now-target-it-supply-chains-to-breach-networks

[Source](https://www.bleepingcomputer.com/news/security/silk-typhoon-hackers-now-target-it-supply-chains-to-breach-networks/){:target="_blank" rel="noopener"}

> Microsoft warns that Chinese cyber-espionage threat group 'Silk Typhoon' has shifted its tactics, now targeting remote management tools and cloud services in supply chain attacks that give them access to downstream customers. [...]
