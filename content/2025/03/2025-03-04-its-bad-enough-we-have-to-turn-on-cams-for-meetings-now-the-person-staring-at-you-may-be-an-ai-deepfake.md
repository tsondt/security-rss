Title: It's bad enough we have to turn on cams for meetings, now the person staring at you may be an AI deepfake
Date: 2025-03-04T08:35:14+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2025-03-04-its-bad-enough-we-have-to-turn-on-cams-for-meetings-now-the-person-staring-at-you-may-be-an-ai-deepfake

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/04/faceswapping_scams_2024/){:target="_blank" rel="noopener"}

> Says the biz trying to sell us stuff to catch that, admittedly High-profile deepfake scams that were reported here at The Register and elsewhere last year may just be the tip of the iceberg. Attacks relying on spoofed faces in online meetings surged by 300 percent in 2024, it is claimed.... [...]
