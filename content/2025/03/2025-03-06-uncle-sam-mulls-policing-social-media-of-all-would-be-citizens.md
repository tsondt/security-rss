Title: Uncle Sam mulls policing social media of all would-be citizens
Date: 2025-03-06T20:25:13+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-03-06-uncle-sam-mulls-policing-social-media-of-all-would-be-citizens

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/06/uscis_social_media/){:target="_blank" rel="noopener"}

> President ordered officials to ramp up vetting 'to the maximum degree' The US government's Citizenship and Immigration Service (USCIS) is considering monitoring not just the social media posts of non-citizens coming into the country, but also all those already in America going through an immigration or citizenship process.... [...]
