Title: Rubrik rotates authentication keys after log server breach
Date: 2025-03-03T15:53:52-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2025-03-03-rubrik-rotates-authentication-keys-after-log-server-breach

[Source](https://www.bleepingcomputer.com/news/security/rubrik-rotates-authentication-keys-after-log-server-breach/){:target="_blank" rel="noopener"}

> Rubrik disclosed last month that one of its servers hosting log files was breached, causing the company to rotate potentially leaked authentication keys. [...]
