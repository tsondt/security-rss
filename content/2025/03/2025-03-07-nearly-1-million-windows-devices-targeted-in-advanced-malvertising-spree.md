Title: Nearly 1 million Windows devices targeted in advanced “malvertising” spree
Date: 2025-03-07T20:23:58+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;maltertising;malware;microsoft
Slug: 2025-03-07-nearly-1-million-windows-devices-targeted-in-advanced-malvertising-spree

[Source](https://arstechnica.com/security/2025/03/nearly-1-million-windows-devices-targeted-in-advanced-malvertising-spree/){:target="_blank" rel="noopener"}

> Nearly 1 million Windows devices were targeted in recent months by a sophisticated "malvertising" campaign that surreptitiously stole login credentials, cryptocurrency, and other sensitive information from infected machines, Microsoft said. The campaign began in December, when the attackers, who remain unknown, seeded websites with links that downloaded ads from malicious servers. The links led targeted machines through several intermediary sites until finally arriving at repositories on Microsoft-owned GitHub, which hosted a raft of malicious files. Chain of events The malware was loaded in four stages, each of which acted as a building block for the next. Early stages collected device [...]
