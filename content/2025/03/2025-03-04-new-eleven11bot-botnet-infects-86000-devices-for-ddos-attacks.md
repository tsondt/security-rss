Title: New Eleven11bot botnet infects 86,000 devices for DDoS attacks
Date: 2025-03-04T15:10:04-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-03-04-new-eleven11bot-botnet-infects-86000-devices-for-ddos-attacks

[Source](https://www.bleepingcomputer.com/news/security/new-eleven11bot-botnet-infects-86-000-devices-for-ddos-attacks/){:target="_blank" rel="noopener"}

> A new botnet malware named 'Eleven11bot' has infected over 86,000 IoT devices, primarily security cameras and network video recorders (NVRs), to conduct DDoS attacks. [...]
