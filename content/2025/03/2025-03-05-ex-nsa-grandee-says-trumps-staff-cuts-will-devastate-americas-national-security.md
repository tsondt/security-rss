Title: Ex-NSA grandee says Trump's staff cuts will 'devastate' America's national security
Date: 2025-03-05T22:44:53+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-03-05-ex-nsa-grandee-says-trumps-staff-cuts-will-devastate-americas-national-security

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/05/us_government_job_cuts_nsa/){:target="_blank" rel="noopener"}

> Would 'destroy a pipeline of top talent essential for hunting' Chinese spies in US networks, Congress told Video Looming staffing cuts to America's security and intelligence agencies, if carried out, would "have a devastating effect on cybersecurity and our national security," former NSA bigwig Rob Joyce has told House representatives.... [...]
