Title: Up to $75M needed to fix up rural hospital cybersecurity as ransomware gangs keep scratching at the door
Date: 2025-03-06T14:30:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-03-06-up-to-75m-needed-to-fix-up-rural-hospital-cybersecurity-as-ransomware-gangs-keep-scratching-at-the-door

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/06/rural_hospitals_cybersecurity/){:target="_blank" rel="noopener"}

> Attacks strike, facilities go bust, patients die. But it's preventable It will cost upward of $75 million to address the cybersecurity needs of rural US hospitals, Microsoft reckons, as mounting closures threaten the lives of Americans.... [...]
