Title: DHS says CISA will not stop monitoring Russian cyber threats
Date: 2025-03-03T14:22:00-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Government
Slug: 2025-03-03-dhs-says-cisa-will-not-stop-monitoring-russian-cyber-threats

[Source](https://www.bleepingcomputer.com/news/security/dhs-says-cisa-will-not-stop-monitoring-russian-cyber-threats/){:target="_blank" rel="noopener"}

> The US Cybersecurity and Infrastructure Security Agency says that media reports about it being directed to no longer follow or report on Russian cyber activity are untrue, and its mission remains unchanged. [...]
