Title: Qilin ransomware gang claims attacks on cancer clinic, OB-GYN facility
Date: 2025-03-05T10:15:15+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-03-05-qilin-ransomware-gang-claims-attacks-on-cancer-clinic-ob-gyn-facility

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/05/qilin_ransomware_credit/){:target="_blank" rel="noopener"}

> List of attacks by 'No regrets' crew leaking highly sensitive data continues to grow Qilin – the "no regrets" ransomware crew wreaking havoc on the global healthcare industry – just claimed responsibility for fresh attacks on a cancer treatment clinic in Japan and a women's healthcare facility in the US.... [...]
