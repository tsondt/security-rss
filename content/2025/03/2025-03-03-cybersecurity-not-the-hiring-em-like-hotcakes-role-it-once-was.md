Title: Cybersecurity not the hiring-'em-like-hotcakes role it once was
Date: 2025-03-03T16:10:41+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-03-03-cybersecurity-not-the-hiring-em-like-hotcakes-role-it-once-was

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/03/cybersecurity_jobs_market/){:target="_blank" rel="noopener"}

> Ghost positions, HR AI no help – biz should talk to infosec staff and create 'realistic' job outline, say experts Analysis It's a familiar refrain in the security industry that there is a massive skills gap in the sector. And while it's true there are specific shortages in certain areas, some industry watchers believe we may be reaching the point of oversupply for generalists.... [...]
