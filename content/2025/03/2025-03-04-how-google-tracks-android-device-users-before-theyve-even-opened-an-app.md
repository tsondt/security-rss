Title: How Google tracks Android device users before they've even opened an app
Date: 2025-03-04T10:15:10+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-03-04-how-google-tracks-android-device-users-before-theyve-even-opened-an-app

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/04/google_android/){:target="_blank" rel="noopener"}

> No warning, no opt-out, and critic claims... no consent Research from a leading academic shows Android users have advertising cookies and other gizmos working to build profiles on them even before they open their first app.... [...]
