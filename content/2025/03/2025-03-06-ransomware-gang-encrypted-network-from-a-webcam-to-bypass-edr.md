Title: Ransomware gang encrypted network from a webcam to bypass EDR
Date: 2025-03-06T15:31:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-03-06-ransomware-gang-encrypted-network-from-a-webcam-to-bypass-edr

[Source](https://www.bleepingcomputer.com/news/security/akira-ransomware-encrypted-network-from-a-webcam-to-bypass-edr/){:target="_blank" rel="noopener"}

> The Akira ransomware gang was spotted using an unsecured webcam to launch encryption attacks on a victim's network, effectively circumventing Endpoint Detection and Response (EDR), which was blocking the encryptor in Windows. [...]
