Title: UK watchdog probes TikTok and Reddit over child privacy concerns
Date: 2025-03-03T11:22:09-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-03-03-uk-watchdog-probes-tiktok-and-reddit-over-child-privacy-concerns

[Source](https://www.bleepingcomputer.com/news/security/uk-watchdog-probes-tiktok-and-reddit-over-child-privacy-concerns/){:target="_blank" rel="noopener"}

> On Monday, the United Kingdom's privacy watchdog announced that it is investigating TikTok, Reddit, and Imgur because of privacy concerns about how they are processing children's data. [...]
