Title: Rayhunter: Device to Detect Cellular Surveillance
Date: 2025-03-07T17:03:17+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cell phones;EFF;privacy;surveillance
Slug: 2025-03-07-rayhunter-device-to-detect-cellular-surveillance

[Source](https://www.schneier.com/blog/archives/2025/03/rayhunter-device-to-detect-cellular-surveillance.html){:target="_blank" rel="noopener"}

> The EFF has created an open-source hardware tool to detect IMSI catchers: fake cell phone towers that are used for mass surveillance of an area. It runs on a $20 mobile hotspot. [...]
