Title: Apple drags UK government to court over 'backdoor' order
Date: 2025-03-05T14:38:34+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-03-05-apple-drags-uk-government-to-court-over-backdoor-order

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/05/apple_reportedly_ipt_complaint/){:target="_blank" rel="noopener"}

> A first-of-its-kind legal challenge set to be heard this month, per reports Updated Apple has reportedly filed a legal complaint with the UK's Investigatory Powers Tribunal (IPT) contesting the British government's order that it must forcibly break the encryption of iCloud data.... [...]
