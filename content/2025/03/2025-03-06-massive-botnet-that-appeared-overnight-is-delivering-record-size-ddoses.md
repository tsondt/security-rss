Title: Massive botnet that appeared overnight is delivering record-size DDoSes
Date: 2025-03-06T13:21:57+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;botnets;DDoS attacks;distributed denial of service attacks;Internet of things
Slug: 2025-03-06-massive-botnet-that-appeared-overnight-is-delivering-record-size-ddoses

[Source](https://arstechnica.com/security/2025/03/massive-botnet-that-appeared-overnight-is-delivering-record-size-ddoses/){:target="_blank" rel="noopener"}

> A newly discovered network botnet comprising an estimated 30,000 webcams and video recorders—with the largest concentration in the US—has been delivering what is likely to be the biggest denial-of-service attack ever seen, a security researcher inside Nokia said. The botnet, tracked under the name Eleven11bot, first came to light in late February when researchers inside Nokia’s Deepfield Emergency Response Team observed large numbers of geographically dispersed IP addresses delivering “hyper-volumetric attacks.” Eleven11bot has been delivering large-scale attacks ever since. Volumetric DDoSes shut down services by consuming all available bandwidth either inside the targeted network or its connection to the Internet. [...]
