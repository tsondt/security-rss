Title: Toronto Zoo shares update on last year's ransomware attack
Date: 2025-03-05T08:36:55-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-03-05-toronto-zoo-shares-update-on-last-years-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/toronto-zoo-shares-update-on-last-years-ransomware-attack/){:target="_blank" rel="noopener"}

> The Toronto Zoo, the largest zoo in Canada, has provided more information about the data stolen during a ransomware attack in January 2024. [...]
