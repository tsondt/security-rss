Title: Malicious Chrome extensions can spoof password managers in new attack
Date: 2025-03-06T09:19:53-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google
Slug: 2025-03-06-malicious-chrome-extensions-can-spoof-password-managers-in-new-attack

[Source](https://www.bleepingcomputer.com/news/security/malicious-chrome-extensions-can-spoof-password-managers-in-new-attack/){:target="_blank" rel="noopener"}

> A newly devised "polymorphic" attack allows malicious Chrome extensions to morph into other browser extensions, including password managers, crypto wallets, and banking apps, to steal sensitive information. [...]
