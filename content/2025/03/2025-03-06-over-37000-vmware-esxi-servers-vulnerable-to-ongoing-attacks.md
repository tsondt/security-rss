Title: Over 37,000 VMware ESXi servers vulnerable to ongoing attacks
Date: 2025-03-06T10:39:37-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-03-06-over-37000-vmware-esxi-servers-vulnerable-to-ongoing-attacks

[Source](https://www.bleepingcomputer.com/news/security/over-37-000-vmware-esxi-servers-vulnerable-to-ongoing-attacks/){:target="_blank" rel="noopener"}

> Over 37,000 internet-exposed VMware ESXi instances are vulnerable to CVE-2025-22224, a critical out-of-bounds write flaw that is actively exploited in the wild. [...]
