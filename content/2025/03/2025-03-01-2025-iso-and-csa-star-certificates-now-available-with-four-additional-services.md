Title: 2025 ISO and CSA STAR certificates now available with four additional services
Date: 2025-03-01T01:04:26+00:00
Author: Nimesh Ravasa
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS CSA STAR;AWS CSA STAR Certificates;AWS ISO;AWS ISO Certificates;AWS ISO20000;AWS ISO22301;AWS ISO27001;AWS ISO27017;AWS ISO27018;AWS ISO27701;AWS ISO9001;Compliance;Security Blog
Slug: 2025-03-01-2025-iso-and-csa-star-certificates-now-available-with-four-additional-services

[Source](https://aws.amazon.com/blogs/security/2025-iso-and-csa-star-certificates-now-available-with-four-additional-services/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) successfully completed an onboarding audit with no findings for ISO 9001:2015, 27001:2022, 27017:2015, 27018:2019, 27701:2019, 20000-1:2018, and 22301:2019, and Cloud Security Alliance (CSA) STAR Cloud Controls Matrix (CCM) v4.0. EY CertifyPoint auditors conducted the audit and reissued the certificates on February 19, 2025. The objective was to assess the level of [...]
