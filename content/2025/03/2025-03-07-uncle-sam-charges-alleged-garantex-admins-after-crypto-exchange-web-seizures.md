Title: Uncle Sam charges alleged Garantex admins after crypto-exchange web seizures
Date: 2025-03-07T18:53:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-03-07-uncle-sam-charges-alleged-garantex-admins-after-crypto-exchange-web-seizures

[Source](https://go.theregister.com/feed/www.theregister.com/2025/03/07/uncle_sam_charges_2_garantex/){:target="_blank" rel="noopener"}

> $96B in transactions, some even labeled 'dirty funds,' since 2019, say prosecutors The Feds today revealed more details about the US Secret Service-led Garantex takedown, a day after seizing websites and freezing assets belonging to the Russian cryptocurrency exchange in coordination with German and Finnish law enforcement agencies.... [...]
