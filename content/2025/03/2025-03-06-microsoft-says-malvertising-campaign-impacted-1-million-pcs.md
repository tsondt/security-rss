Title: Microsoft says malvertising campaign impacted 1 million PCs
Date: 2025-03-06T15:53:36-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2025-03-06-microsoft-says-malvertising-campaign-impacted-1-million-pcs

[Source](https://www.bleepingcomputer.com/news/security/microsoft-says-malvertising-campaign-impacted-1-million-pcs/){:target="_blank" rel="noopener"}

> ​Microsoft has taken down an undisclosed number of GitHub repositories used in a massive malvertising campaign that impacted almost one million devices worldwide. [...]
