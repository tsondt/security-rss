Title: US charges Garantex admins with money laundering, sanctions violations
Date: 2025-03-07T10:40:24-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-03-07-us-charges-garantex-admins-with-money-laundering-sanctions-violations

[Source](https://www.bleepingcomputer.com/news/security/us-charges-garantex-admins-with-money-laundering-sanctions-violations/){:target="_blank" rel="noopener"}

> The administrators of the Russian Garantex crypto-exchange have been charged in the United States with facilitating money laundering for criminal organizations and violating sanctions. [...]
