Title: Undocumented "backdoor" found in Bluetooth chip used by a billion devices
Date: 2025-03-08T11:12:23-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Hardware
Slug: 2025-03-08-undocumented-backdoor-found-in-bluetooth-chip-used-by-a-billion-devices

[Source](https://www.bleepingcomputer.com/news/security/undocumented-backdoor-found-in-bluetooth-chip-used-by-a-billion-devices/){:target="_blank" rel="noopener"}

> The ubiquitous ESP32 microchip made by Chinese manufacturer Espressif and used by over 1 billion units as of 2023 contains an undocumented "backdoor" that could be leveraged for attacks. [...]
