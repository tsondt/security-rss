Title: Xi know what you did last summer: China was all up in Republicans' email, says book
Date: 2025-02-25T21:39:18+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-25-xi-know-what-you-did-last-summer-china-was-all-up-in-republicans-email-says-book

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/25/china_hacked_gop_emails/){:target="_blank" rel="noopener"}

> Of course, Microsoft is in the mix, isn't it Chinese spies reportedly broke into the US Republication National Committee's Microsoft-powered email and snooped around for months before being caught.... [...]
