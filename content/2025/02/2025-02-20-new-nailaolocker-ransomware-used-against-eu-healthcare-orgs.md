Title: New NailaoLocker ransomware used against EU healthcare orgs
Date: 2025-02-20T03:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-20-new-nailaolocker-ransomware-used-against-eu-healthcare-orgs

[Source](https://www.bleepingcomputer.com/news/security/new-nailaolocker-ransomware-used-against-eu-healthcare-orgs/){:target="_blank" rel="noopener"}

> A previously undocumented ransomware payload named NailaoLocker has been spotted in attacks targeting European healthcare organizations between June and October 2024. [...]
