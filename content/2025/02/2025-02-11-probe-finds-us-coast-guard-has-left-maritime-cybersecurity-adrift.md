Title: Probe finds US Coast Guard has left maritime cybersecurity adrift
Date: 2025-02-11T23:44:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-11-probe-finds-us-coast-guard-has-left-maritime-cybersecurity-adrift

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/11/coast_guard_cybersecurity_fail/){:target="_blank" rel="noopener"}

> Numerous systemic vulnerabilities could scuttle $5.4T industry Despite the escalating cyber threats targeting America's maritime transportation system, the US Coast Guard still lacks a comprehensive strategy to secure this critical infrastructure - nor does it have reliable access to data on cybersecurity vulnerabilities and past attacks, the Government Accountability Office (GAO) warns.... [...]
