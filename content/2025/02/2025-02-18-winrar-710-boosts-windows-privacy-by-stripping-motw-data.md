Title: WinRAR 7.10 boosts Windows privacy by stripping MoTW data
Date: 2025-02-18T17:57:39-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Software
Slug: 2025-02-18-winrar-710-boosts-windows-privacy-by-stripping-motw-data

[Source](https://www.bleepingcomputer.com/news/security/winrar-710-boosts-windows-privacy-by-stripping-motw-data/){:target="_blank" rel="noopener"}

> WinRAR 7.10 was released yesterday with numerous features, such as larger memory pages, a dark mode, and the ability to fine-tune how Windows Mark-of-the-Web flags are propagated when extracting files. [...]
