Title: Incoming deputy boss of Homeland Security says America's top cyber-agency needs to be reined in
Date: 2025-02-26T02:31:22+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-02-26-incoming-deputy-boss-of-homeland-security-says-americas-top-cyber-agency-needs-to-be-reined-in

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/26/dhs_cisa_doge/){:target="_blank" rel="noopener"}

> Plus: New figurehead of DOGE emerges and they aren't called Elon During confirmation hearings in the US Senate Tuesday for the role of deputy director of the Dept of Homeland Security, the nominee Troy Edgar said CISA has had the wrong management and needed to be "reined in."... [...]
