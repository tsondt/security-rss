Title: Chinese cyberspies use new SSH backdoor in network device hacks
Date: 2025-02-04T12:39:40-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-04-chinese-cyberspies-use-new-ssh-backdoor-in-network-device-hacks

[Source](https://www.bleepingcomputer.com/news/security/chinese-cyberspies-use-new-ssh-backdoor-in-network-device-hacks/){:target="_blank" rel="noopener"}

> A Chinese hacking group is hijacking the SSH daemon on network appliances by injecting malware into the process for persistent access and covert operations. [...]
