Title: Deepfakes and the 2024 US Election
Date: 2025-02-04T12:01:36+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;AI;deception;deepfake
Slug: 2025-02-04-deepfakes-and-the-2024-us-election

[Source](https://www.schneier.com/blog/archives/2025/02/deepfakes-and-the-2024-us-election.html){:target="_blank" rel="noopener"}

> Interesting analysis : We analyzed every instance of AI use in elections collected by the WIRED AI Elections Project ( source for our analysis), which tracked known uses of AI for creating political content during elections taking place in 2024 worldwide. In each case, we identified what AI was used for and estimated the cost of creating similar content without AI. We find that (1) half of AI use isn’t deceptive, (2) deceptive content produced using AI is nevertheless cheap to replicate without AI, and (3) focusing on the demand for misinformation rather than the supply is a much more [...]
