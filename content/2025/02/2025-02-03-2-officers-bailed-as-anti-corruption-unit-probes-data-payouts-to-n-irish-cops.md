Title: 2 officers bailed as anti-corruption unit probes data payouts to N Irish cops
Date: 2025-02-03T11:46:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-03-2-officers-bailed-as-anti-corruption-unit-probes-data-payouts-to-n-irish-cops

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/03/two_psni_officers_arrested_bailed/){:target="_blank" rel="noopener"}

> Investigating compensation to police whose sensitive info was leaked in 2023 The Police Service of Northern Ireland (PSNI) has bailed two officers after they were arrested as part of a fraud investigation related to the payments to cops whose sensitive data was mistakenly published in 2023.... [...]
