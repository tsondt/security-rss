Title: Russia-aligned hackers are targeting Signal users with device-linking QR codes
Date: 2025-02-19T21:21:06+00:00
Author: Kevin Purdy
Category: Ars Technica
Tags: Biz & IT;Security;apt44;device linking;google;GRU;qr codes;russian threat actors;signal;threat intelligence
Slug: 2025-02-19-russia-aligned-hackers-are-targeting-signal-users-with-device-linking-qr-codes

[Source](https://arstechnica.com/information-technology/2025/02/russia-aligned-hackers-are-targeting-signal-users-with-device-linking-qr-codes/){:target="_blank" rel="noopener"}

> Signal, as an encrypted messaging app and protocol, remains relatively secure. But Signal's growing popularity as a tool to circumvent surveillance has led agents affiliated with Russia to try to manipulate the app's users into surreptitiously linking their devices, according to Google's Threat Intelligence Group. While Russia's continued invasion of Ukraine is likely driving the country's desire to work around Signal's encryption, "We anticipate the tactics and methods used to target Signal will grow in prevalence in the near-term and proliferate to additional threat actors and regions outside the Ukrainian theater of war," writes Dan Black at Google's Threat Intelligence [...]
