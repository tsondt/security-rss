Title: HPE notifies employees of data breach after Russian Office 365 hack
Date: 2025-02-07T14:21:16-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-07-hpe-notifies-employees-of-data-breach-after-russian-office-365-hack

[Source](https://www.bleepingcomputer.com/news/security/hpe-notifies-employees-of-data-breach-after-russian-office-365-hack/){:target="_blank" rel="noopener"}

> Hewlett Packard Enterprise (HPE) is notifying employees whose data was stolen from the company's Office 365 email environment by Russian state-sponsored hackers in a May 2023 cyberattack. [...]
