Title: This open text-to-speech model needs just seconds of audio to clone your voice
Date: 2025-02-16T18:58:09+00:00
Author: Tobias Mann
Category: The Register
Tags: 
Slug: 2025-02-16-this-open-text-to-speech-model-needs-just-seconds-of-audio-to-clone-your-voice

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/16/ai_voice_clone/){:target="_blank" rel="noopener"}

> El Reg shows you how to run Zypher's speech-replicating AI on your own box Hands on Palo Alto-based AI startup Zyphra unveiled a pair of open text-to-speech (TTS) models this week said to be capable of cloning your voice with as little as five seconds of sample audio. In our testing, we generated realistic results with less than half a minute of recorded speech.... [...]
