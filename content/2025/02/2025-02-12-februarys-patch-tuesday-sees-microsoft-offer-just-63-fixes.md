Title: February's Patch Tuesday sees Microsoft offer just 63 fixes
Date: 2025-02-12T02:58:11+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-02-12-februarys-patch-tuesday-sees-microsoft-offer-just-63-fixes

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/12/patch_tuesday_february_2025/){:target="_blank" rel="noopener"}

> Don't relax just yet: Redmond has made some certificate-handling changes that could trip unprepared admins Patch Tuesday Microsoft’s February patch collection is mercifully smaller than January’s mega-dump. But don't get too relaxed – some deserve close attention, and other vendors have stepped in with plenty more fixes.... [...]
