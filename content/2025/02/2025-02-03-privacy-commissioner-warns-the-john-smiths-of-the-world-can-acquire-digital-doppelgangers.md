Title: Privacy Commissioner warns the ‘John Smiths’ of the world can acquire ‘digital doppelgangers’
Date: 2025-02-03T06:30:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2025-02-03-privacy-commissioner-warns-the-john-smiths-of-the-world-can-acquire-digital-doppelgangers

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/03/australia_digital_doppelgangers_privacy_award/){:target="_blank" rel="noopener"}

> Australian government staff mixed medical info for folk who share names and birthdays Australia’s privacy commissioner has found that government agencies down under didn’t make enough of an effort to protect data describing “digital doppelgangers” – people who share a name and date of birth and whose government records sometimes contain data describing other people.... [...]
