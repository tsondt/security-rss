Title: Microsoft: Hackers steal emails in device code phishing attacks
Date: 2025-02-15T10:22:34-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-15-microsoft-hackers-steal-emails-in-device-code-phishing-attacks

[Source](https://www.bleepingcomputer.com/news/security/microsoft-hackers-steal-emails-in-device-code-phishing-attacks/){:target="_blank" rel="noopener"}

> An active campaign from a threat actor potentially linked to Russia is targeting Microsoft 365 accounts of individuals at organizations of interest using device code phishing. [...]
