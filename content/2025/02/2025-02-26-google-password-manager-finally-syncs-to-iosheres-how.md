Title: Google Password Manager finally syncs to iOS—here’s how
Date: 2025-02-26T13:20:36+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Apple;Biz & IT;Security;Google password manager;iOS;passkeys;passwords
Slug: 2025-02-26-google-password-manager-finally-syncs-to-iosheres-how

[Source](https://arstechnica.com/security/2025/02/google-password-manager-finally-syncs-to-ios-heres-how/){:target="_blank" rel="noopener"}

> Late last year, I published a long post that criticized the user unfriendliness of passkeys, the industry-wide alternative to logging in with passwords. A chief complaint was that passkey implementations tend to lock users into whatever platform they used to create the credential. An example: When using Chrome on an iPhone, passkeys were saved to iCloud. When using Chrome on other platforms, passkeys were saved to a user’s Google profile. That meant passkeys created for Chrome on, say, Windows, wouldn’t sync to iCloud. Passkeys created in iCloud wouldn’t sync with a Google account. GPM and iOS finally play nice together [...]
