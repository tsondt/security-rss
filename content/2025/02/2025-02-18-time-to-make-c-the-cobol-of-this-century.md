Title: Time to make C the COBOL of this century
Date: 2025-02-18T14:01:05+00:00
Author: Rupert Goodwins
Category: The Register
Tags: 
Slug: 2025-02-18-time-to-make-c-the-cobol-of-this-century

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/18/c_opinion/){:target="_blank" rel="noopener"}

> Lions juggling chainsaws are fun to watch, but you wouldn't want them trimming your trees Opinion Nobody likes The Man. When a traffic cop tells you to straighten up and slow down or else, profound thanks are rarely the first words on your lips. Then you drive past a car embedded in a tree, surrounded by blue lights and cutting equipment. Perhaps Officer Dibble had a point.... [...]
