Title: Gilmore Girls fans nabbed as Eurocops dismantle two major cybercrime forums
Date: 2025-02-02T13:19:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-02-gilmore-girls-fans-nabbed-as-eurocops-dismantle-two-major-cybercrime-forums

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/02/eurocops_takedown_cybercrime/){:target="_blank" rel="noopener"}

> Nulled and Cracked had a Lorelai-cal rise - until Operation Talent stepped in Law enforcement officers across Europe assembled again to collectively disrupt major facilitators of cybercrime, with at least one of those cuffed apparently a fan of the dramedy series The Gilmore Girls.... [...]
