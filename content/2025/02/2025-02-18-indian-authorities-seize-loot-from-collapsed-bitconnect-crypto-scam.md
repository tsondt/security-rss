Title: Indian authorities seize loot from collapsed BitConnect crypto scam
Date: 2025-02-18T05:29:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2025-02-18-indian-authorities-seize-loot-from-collapsed-bitconnect-crypto-scam

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/18/india_bitconnect_seizures/){:target="_blank" rel="noopener"}

> Devices containing crypto wallets tracked online, then in the real world Indian authorities seize loot from BitConnect crypto-Ponzi scheme Devices containing crypto wallets tracked online, then in the real world India’s Directorate of Enforcement has found and seized over $200 million of loot it says are the proceeds of the BitConnect crypto-fraud scheme.... [...]
