Title: Over 49,000 misconfigured building access systems exposed online
Date: 2025-02-27T13:00:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-27-over-49000-misconfigured-building-access-systems-exposed-online

[Source](https://www.bleepingcomputer.com/news/security/over-49-000-misconfigured-building-access-systems-exposed-online/){:target="_blank" rel="noopener"}

> Researchers discovered 49,000 misconfigured and exposed Access Management Systems (AMS) across multiple industries and countries, which could compromise privacy and physical security in critical sectors. [...]
