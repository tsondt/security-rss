Title: OpenAI bans ChatGPT accounts used by North Korean hackers
Date: 2025-02-24T16:35:13-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-24-openai-bans-chatgpt-accounts-used-by-north-korean-hackers

[Source](https://www.bleepingcomputer.com/news/security/openai-bans-chatgpt-accounts-used-by-north-korean-hackers/){:target="_blank" rel="noopener"}

> OpenAI says it blocked several North Korean hacking groups from using its ChatGPT platform to research future targets and find ways to hack into their networks. [...]
