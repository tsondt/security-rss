Title: Crimelords and spies for rogue states are working together, says Google
Date: 2025-02-12T13:29:34+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-12-crimelords-and-spies-for-rogue-states-are-working-together-says-google

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/12/google_state_cybercrime_report/){:target="_blank" rel="noopener"}

> Only lawmakers can stop them. Plus: software needs to be more secure, but what's in it for us? Google says the the world's lawmakers must take action against the increasing links between criminal and state-sponsored cyber activity.... [...]
