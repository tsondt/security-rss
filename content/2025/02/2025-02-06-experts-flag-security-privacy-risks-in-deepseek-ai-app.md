Title: Experts Flag Security, Privacy Risks in DeepSeek AI App
Date: 2025-02-06T21:12:30+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Latest Warnings;The Coming Storm;Andrew Hoog;app transport security;apple;artificial intelligence;ByteDance;China;Deepseek;Deepseek AI;iOS;NowSecure;Volcengine
Slug: 2025-02-06-experts-flag-security-privacy-risks-in-deepseek-ai-app

[Source](https://krebsonsecurity.com/2025/02/experts-flag-security-privacy-risks-in-deepseek-ai-app/){:target="_blank" rel="noopener"}

> New mobile apps from the Chinese artificial intelligence (AI) company DeepSeek have remained among the top three “free” downloads for Apple and Google devices since their debut on Jan. 25, 2025. But experts caution that many of DeepSeek’s design choices — such as using hard-coded encryption keys, and sending unencrypted user and device data to Chinese companies — introduce a number of glaring security and privacy risks. Public interest in the DeepSeek AI chat apps swelled following widespread media reports that the upstart Chinese AI firm had managed to match the abilities of cutting-edge chatbots while using a fraction of [...]
