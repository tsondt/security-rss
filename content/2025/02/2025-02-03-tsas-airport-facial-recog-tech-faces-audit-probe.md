Title: TSA’s airport facial-recog tech faces audit probe
Date: 2025-02-03T22:30:11+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2025-02-03-tsas-airport-facial-recog-tech-faces-audit-probe

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/03/tsa_facial_recognition_audit/){:target="_blank" rel="noopener"}

> Senators ask, Homeland Security watchdog answers: Is it worth the money? The Department of Homeland Security's Inspector General has launched an audit of the Transportation Security Administration's use of facial recognition technology at US airports, following criticism from lawmakers and privacy advocates.... [...]
