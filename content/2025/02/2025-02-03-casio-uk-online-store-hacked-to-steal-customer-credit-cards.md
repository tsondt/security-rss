Title: Casio UK online store hacked to steal customer credit cards
Date: 2025-02-03T13:51:11-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-03-casio-uk-online-store-hacked-to-steal-customer-credit-cards

[Source](https://www.bleepingcomputer.com/news/security/casio-uk-online-store-hacked-to-steal-customer-credit-cards/){:target="_blank" rel="noopener"}

> Casio UK's e-shop at casio.co.uk was hacked to include malicious scripts that stole credit card and customer information between January 14 and 24, 2025. [...]
