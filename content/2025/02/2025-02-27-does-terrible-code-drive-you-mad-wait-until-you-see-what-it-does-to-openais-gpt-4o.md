Title: Does terrible code drive you mad? Wait until you see what it does to OpenAI's GPT-4o
Date: 2025-02-27T07:29:12+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2025-02-27-does-terrible-code-drive-you-mad-wait-until-you-see-what-it-does-to-openais-gpt-4o

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/27/llm_emergent_misalignment_study/){:target="_blank" rel="noopener"}

> Model was fine-tuned to write vulnerable software – then suggested enslaving humanity Updated Computer scientists have found that fine-tuning notionally safe large language models to do one thing badly can negatively impact the AI’s output across a range of topics.... [...]
