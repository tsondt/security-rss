Title: AIs and Robots Should Sound Robotic
Date: 2025-02-06T12:03:22+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;AI
Slug: 2025-02-06-ais-and-robots-should-sound-robotic

[Source](https://www.schneier.com/blog/archives/2025/02/ais-and-robots-should-sound-robotic.html){:target="_blank" rel="noopener"}

> Most people know that robots no longer sound like tinny trash cans. They sound like Siri, Alexa, and Gemini. They sound like the voices in labyrinthine customer support phone trees. And even those robot voices are being made obsolete by new AI-generated voices that can mimic every vocal nuance and tic of human speech, down to specific regional accents. And with just a few seconds of audio, AI can now clone someone’s specific voice. This technology will replace humans in many areas. Automated customer support will save money by cutting staffing at call centers. AI agents will make calls on [...]
