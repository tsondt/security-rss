Title: Twin Google flaws allowed researcher to get from YouTube ID to Gmail address in a few easy steps
Date: 2025-02-17T02:25:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2025-02-17-twin-google-flaws-allowed-researcher-to-get-from-youtube-id-to-gmail-address-in-a-few-easy-steps

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/17/infosec_news_in_brief/){:target="_blank" rel="noopener"}

> PLUS: DOGE web design disappoints; FBI stops crypto scams; Zacks attacked again; and more! Infosec In Brief A security researcher has found that Google could leak the email addresses of YouTube channels, which wasn’t good because the search and ads giant promised not to do that.... [...]
