Title: Juniper patches critical auth bypass in Session Smart routers
Date: 2025-02-18T12:07:27-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-18-juniper-patches-critical-auth-bypass-in-session-smart-routers

[Source](https://www.bleepingcomputer.com/news/security/juniper-patches-critical-auth-bypass-in-session-smart-routers/){:target="_blank" rel="noopener"}

> ​Juniper Networks has patched a critical vulnerability that allows attackers to bypass authentication and take over Session Smart Router (SSR) devices. [...]
