Title: Pairwise Authentication of Humans
Date: 2025-02-10T12:00:41+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;authentication;protocols
Slug: 2025-02-10-pairwise-authentication-of-humans

[Source](https://www.schneier.com/blog/archives/2025/02/pairwise-authentication-of-humans.html){:target="_blank" rel="noopener"}

> Here’s an easy system for two humans to remotely authenticate to each other, so they can be sure that neither are digital impersonations. To mitigate that risk, I have developed this simple solution where you can setup a unique time-based one-time passcode (TOTP) between any pair of persons. This is how it works: Two people, Person A and Person B, sit in front of the same computer and open this page; They input their respective names (e.g. Alice and Bob) onto the same page, and click “Generate”; The page will generate two TOTP QR codes, one for Alice and one [...]
