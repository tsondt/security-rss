Title: Announcing quantum-safe digital signatures in Cloud KMS
Date: 2025-02-20T17:00:00+00:00
Author: Andrew Foster
Category: GCP Security
Tags: Security & Identity
Slug: 2025-02-20-announcing-quantum-safe-digital-signatures-in-cloud-kms

[Source](https://cloud.google.com/blog/products/identity-security/announcing-quantum-safe-digital-signatures-in-cloud-kms/){:target="_blank" rel="noopener"}

> The continued advancement of experimental quantum computing has raised concerns about the security of many of the world's widely-used public-key cryptography systems. Crucially, there exists the potential for sufficiently large, cryptographically-relevant quantum computers to break these algorithms. This potential highlights the need for developers to build and implement quantum-resistant cryptography now. Fortunately, post-quantum cryptography (PQC) offers a way of mitigating these risks using existing hardware and software. The National Institute of Standards and Technology’s new PQC standards, made available in August 2024 following several years of community engagement, have begun enabling technology vendors around the world to take steps toward [...]
