Title: Arizona laptop farmer pleads guilty for funneling $17M to Kim Jong Un
Date: 2025-02-12T20:30:15+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-12-arizona-laptop-farmer-pleads-guilty-for-funneling-17m-to-kim-jong-un

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/12/arizona_woman_laptop_farm_guilty/){:target="_blank" rel="noopener"}

> 300+ US companies, 70+ individuals hit by the fraudsters An Arizona woman who created a "laptop farm" in her home to help fake IT workers pose as US-based employees has pleaded guilty in a scheme that generated over $17 million for herself... and North Korea.... [...]
