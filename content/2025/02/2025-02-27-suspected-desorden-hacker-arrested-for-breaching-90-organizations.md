Title: Suspected Desorden hacker arrested for breaching 90 organizations
Date: 2025-02-27T10:49:09-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2025-02-27-suspected-desorden-hacker-arrested-for-breaching-90-organizations

[Source](https://www.bleepingcomputer.com/news/security/suspected-desorden-hacker-arrested-for-breaching-90-organizations/){:target="_blank" rel="noopener"}

> A suspected cyber criminal believed to have extorted companies under the name "DESORDEN Group" or "ALTDOS" has been arrested in Thailand for leaking the stolen data of over 90 organizations worldwide. [...]
