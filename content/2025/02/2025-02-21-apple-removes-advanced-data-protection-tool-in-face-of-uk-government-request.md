Title: Apple removes advanced data protection tool in face of UK government request
Date: 2025-02-21T17:36:08+00:00
Author: Rachel Hall
Category: The Guardian
Tags: Apple;Privacy;Data protection;Data and computer security;UK news;Home Office;Computing;Business;Politics;Technology
Slug: 2025-02-21-apple-removes-advanced-data-protection-tool-in-face-of-uk-government-request

[Source](https://www.theguardian.com/technology/2025/feb/21/apple-removes-advanced-data-protection-tool-uk-government){:target="_blank" rel="noopener"}

> Apple says removal of tool after government asked for right to see data will make iCloud users more vulnerable Business live – latest updates Apple has taken the unprecedented step of removing its strongest data security tool from customers in the UK, after the government demanded “backdoor” access to user data. UK users will no longer have access to the advanced data protection (ADP) tool, which uses end-to-end encryption to allow only account holders to view items such as photos or documents they have stored online in the iCloud storage service. Continue reading... [...]
