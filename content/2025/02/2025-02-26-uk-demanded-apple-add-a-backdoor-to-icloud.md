Title: UK Demanded Apple Add a Backdoor to iCloud
Date: 2025-02-26T12:07:53+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;backdoors;encryption;UK
Slug: 2025-02-26-uk-demanded-apple-add-a-backdoor-to-icloud

[Source](https://www.schneier.com/blog/archives/2025/02/an-icloud-backdoor-would-make-our-phones-less-safe.html){:target="_blank" rel="noopener"}

> Last month, the UK government demanded that Apple weaken the security of iCloud for users worldwide. On Friday, Apple took steps to comply for users in the United Kingdom. But the British law is written in a way that requires Apple to give its government access to anyone, anywhere in the world. If the government demands Apple weaken its security worldwide, it would increase everyone’s cyber-risk in an already dangerous world. If you’re an iCloud user, you have the option of turning on something called “ advanced data protection,” or ADP. In that mode, a majority of your data is [...]
