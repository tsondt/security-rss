Title: Trump’s cyber chief pick has little experience in The Cyber
Date: 2025-02-12T21:14:13+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2025-02-12-trumps-cyber-chief-pick-has-little-experience-in-the-cyber

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/12/trump_cybersecurity_chief/){:target="_blank" rel="noopener"}

> GOP lawyer Sean Cairncross will be learning on the fly, as we also say hi to new intelligence boss Tulsi Gabbard President Trump has reportedly chosen a candidate for National Cyber Director — another top tech appointee with no professional experience in that role.... [...]
