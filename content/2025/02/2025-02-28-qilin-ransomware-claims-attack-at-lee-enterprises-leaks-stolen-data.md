Title: Qilin ransomware claims attack at Lee Enterprises, leaks stolen data
Date: 2025-02-28T13:20:31-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-28-qilin-ransomware-claims-attack-at-lee-enterprises-leaks-stolen-data

[Source](https://www.bleepingcomputer.com/news/security/qilin-ransomware-claims-attack-at-lee-enterprises-leaks-stolen-data/){:target="_blank" rel="noopener"}

> The Qilin ransomware gang has claimed responsibility for the attack at Lee Enterprises that disrupted operations on February 3, leaking samples of data they claim was stolen from the company. [...]
