Title: US news org still struggling to print papers a week after 'cybersecurity event'
Date: 2025-02-10T13:05:26+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-10-us-news-org-still-struggling-to-print-papers-a-week-after-cybersecurity-event

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/10/us_newspapers_lee_enterprises_cyberattack/){:target="_blank" rel="noopener"}

> Publications across 25 states either producing smaller issues or very delayed ones US newspaper publisher Lee Enterprises is one week into tackling a nondescript "cybersecurity event," saying the related investigation may take "weeks or longer" to complete.... [...]
