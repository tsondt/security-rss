Title: Fortinet discloses second firewall auth bypass patched in January
Date: 2025-02-11T13:56:13-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-11-fortinet-discloses-second-firewall-auth-bypass-patched-in-january

[Source](https://www.bleepingcomputer.com/news/security/fortinet-discloses-second-firewall-auth-bypass-patched-in-january/){:target="_blank" rel="noopener"}

> Fortinet has disclosed a second authentication bypass vulnerability that was fixed as part of a January 2025 update for FortiOS and FortiProxy devices. [...]
