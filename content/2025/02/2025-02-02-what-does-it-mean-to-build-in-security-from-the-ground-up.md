Title: What does it mean to build in security from the ground up?
Date: 2025-02-02T17:26:08+00:00
Author: Larry Peterson
Category: The Register
Tags: 
Slug: 2025-02-02-what-does-it-mean-to-build-in-security-from-the-ground-up

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/02/security_design_choices/){:target="_blank" rel="noopener"}

> As if secure design is the only bullet point in a list of software engineering best practices Systems Approach As my Systems Approach co-author Bruce Davie and I think through what it means to apply the systems lens to security, I find that I keep asking myself what it is, exactly, that’s unique about security as a system requirement?... [...]
