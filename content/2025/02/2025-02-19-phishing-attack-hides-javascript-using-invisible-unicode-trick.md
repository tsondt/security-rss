Title: Phishing attack hides JavaScript using invisible Unicode trick
Date: 2025-02-19T15:14:09-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-19-phishing-attack-hides-javascript-using-invisible-unicode-trick

[Source](https://www.bleepingcomputer.com/news/security/phishing-attack-hides-javascript-using-invisible-unicode-trick/){:target="_blank" rel="noopener"}

> A new JavaScript obfuscation method utilizing invisible Unicode characters to represent binary values is being actively abused in phishing attacks targeting affiliates of an American political action committee (PAC). [...]
