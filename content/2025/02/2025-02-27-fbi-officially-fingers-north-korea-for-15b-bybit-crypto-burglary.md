Title: FBI officially fingers North Korea for $1.5B Bybit crypto-burglary
Date: 2025-02-27T21:45:27+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-02-27-fbi-officially-fingers-north-korea-for-15b-bybit-crypto-burglary

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/27/fbi_bybit_korea/){:target="_blank" rel="noopener"}

> Federal agents, open up... your browsers and see if you recognize any of these wallets The FBI has officially accused North Korea's Lazarus Group of stealing $1.5 billion in Ethereum from crypto-exchange Bybit earlier this month, and asked for help tracking down the stolen funds.... [...]
