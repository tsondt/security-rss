Title: Grubhub serves up security incident with a side of needing to change your password
Date: 2025-02-04T15:30:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-04-grubhub-serves-up-security-incident-with-a-side-of-needing-to-change-your-password

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/04/grubhub_data_incident/){:target="_blank" rel="noopener"}

> Contact info and partial payment details may be compromised US food and grocery delivery platform Grubhub says a security incident at a third-party service provider is to blame after user data was compromised.... [...]
