Title: SonicWall firewalls now under attack: Patch ASAP or risk intrusion via your SSL VPN
Date: 2025-02-14T22:53:26+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-14-sonicwall-firewalls-now-under-attack-patch-asap-or-risk-intrusion-via-your-ssl-vpn

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/14/sonicwall_firewalls_under_attack_patch/){:target="_blank" rel="noopener"}

> Roses are red, violets are blue, CVE-2024-53704 is sweet for a ransomware crew Miscreants are actively abusing a high-severity authentication bypass bug in unpatched internet-facing SonicWall firewalls following the public release of proof-of-concept exploit code.... [...]
