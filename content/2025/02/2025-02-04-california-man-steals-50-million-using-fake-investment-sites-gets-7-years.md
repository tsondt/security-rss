Title: California man steals $50 million using fake investment sites, gets 7 years
Date: 2025-02-04T10:05:36-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-04-california-man-steals-50-million-using-fake-investment-sites-gets-7-years

[Source](https://www.bleepingcomputer.com/news/security/california-man-steals-50-million-using-fake-investment-sites-gets-7-years/){:target="_blank" rel="noopener"}

> A 59-year-old man from Irvine, California, was sentenced to 87 months in prison for his involvement in an investor fraud ring that stole $50 million between 2012 and October 2020. [...]
