Title: 22-year-old math wiz indicted for alleged DeFI hack that stole $65M
Date: 2025-02-04T13:25:11+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security
Slug: 2025-02-04-22-year-old-math-wiz-indicted-for-alleged-defi-hack-that-stole-65m

[Source](https://arstechnica.com/information-technology/2025/02/man-indicted-for-two-alleged-defi-hacks-that-stole-65-million/){:target="_blank" rel="noopener"}

> Federal prosecutors have indicted a man on charges he stole $65 million in cryptocurrency by exploiting vulnerabilities in two decentralized finance platforms and then laundering proceeds and attempting to extort swindled investors. The scheme, alleged in an indictment unsealed on Monday, occurred in 2021 and 2023 against the DeFI platforms KyberSwap and Indexed Finance. Both platforms provide automated services known as “liquidity pools” that allow users to move cryptocurrencies from one to another. The pools are funded with user-contributed cryptocurrency and are managed by smart contracts enforced by platform software. “Formidable mathematical prowess” The prosecutors said Andean Medjedovic, now 22 [...]
