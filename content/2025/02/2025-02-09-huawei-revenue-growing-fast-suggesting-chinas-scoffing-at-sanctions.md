Title: Huawei revenue growing fast, suggesting China's scoffing at sanctions
Date: 2025-02-09T23:59:10+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2025-02-09-huawei-revenue-growing-fast-suggesting-chinas-scoffing-at-sanctions

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/09/asia_tech_news_in_brief/){:target="_blank" rel="noopener"}

> PLUS: Japan shifts to pre-emptive cyber-defense; Thailand cuts cords connecting scam camps; China to launch 'moon hopper' in 2026; and more! Asia In Brief Huawei chair Liang Hua last week told a conference in China that the company expects to meet its revenue targets for 2024, meaning it earned around ¥860 billion ($118.25 billion) – 22 percent growth compared to its 2023 result.... [...]
