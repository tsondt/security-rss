Title: Beware: PayPal "New Address" feature abused to send phishing emails
Date: 2025-02-22T16:01:57-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security
Slug: 2025-02-22-beware-paypal-new-address-feature-abused-to-send-phishing-emails

[Source](https://www.bleepingcomputer.com/news/security/beware-paypal-new-address-feature-abused-to-send-phishing-emails/){:target="_blank" rel="noopener"}

> An ongoing PayPal email scam exploits the platform's address settings to send fake purchase notifications, tricking users into granting remote access to scammers [...]
