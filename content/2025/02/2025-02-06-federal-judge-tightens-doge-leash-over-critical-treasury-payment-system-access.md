Title: Federal judge tightens DOGE leash over critical Treasury payment system access
Date: 2025-02-06T20:40:11+00:00
Author: Brandon Vigliarolo and Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-06-federal-judge-tightens-doge-leash-over-critical-treasury-payment-system-access

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/06/federal_court_leashes_doges_tresury_access/){:target="_blank" rel="noopener"}

> Lawsuit: 'Scale of intrusion into individuals' privacy is massive and unprecedented' Updated Elon Musk's Department of Government Efficiency has had its access to US Treasury payment systems restricted - at least temporarily - following a lawsuit from advocacy groups and unions.... [...]
