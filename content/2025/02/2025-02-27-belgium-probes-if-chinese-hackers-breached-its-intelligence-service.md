Title: Belgium probes if Chinese hackers breached its intelligence service
Date: 2025-02-27T11:59:39-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-27-belgium-probes-if-chinese-hackers-breached-its-intelligence-service

[Source](https://www.bleepingcomputer.com/news/security/belgium-probes-if-chinese-hackers-breached-its-intelligence-service/){:target="_blank" rel="noopener"}

> ​The Belgian federal prosecutor's office is investigating whether Chinese hackers were behind a breach of the country's State Security Service (VSSE). [...]
