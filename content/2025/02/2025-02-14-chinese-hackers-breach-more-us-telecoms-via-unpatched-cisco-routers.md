Title: Chinese hackers breach more US telecoms via unpatched Cisco routers
Date: 2025-02-14T07:56:41-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-14-chinese-hackers-breach-more-us-telecoms-via-unpatched-cisco-routers

[Source](https://www.bleepingcomputer.com/news/security/chinese-hackers-breach-more-us-telecoms-via-unpatched-cisco-routers/){:target="_blank" rel="noopener"}

> China's Salt Typhoon hackers are still actively targeting telecoms worldwide and have breached more U.S. telecommunications providers via unpatched Cisco IOS XE network devices. [...]
