Title: Robocallers posing as FCC fraud prevention team call FCC staff
Date: 2025-02-05T16:26:23-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Technology
Slug: 2025-02-05-robocallers-posing-as-fcc-fraud-prevention-team-call-fcc-staff

[Source](https://www.bleepingcomputer.com/news/security/robocallers-posing-as-fcc-fraud-prevention-team-call-fcc-staff/){:target="_blank" rel="noopener"}

> The FCC has proposed a $4,492,500 fine against VoIP service provider Telnyx for allegedly allowing customers to make robocalls posing as fictitious FCC "Fraud Prevention Team," by failing to comply with Know Your Customer (KYC) rules. However, Telnyx says the FCC is mistaken and denies the accusations. [...]
