Title: Microsoft Patch Tuesday, February 2025 Edition
Date: 2025-02-12T04:58:37+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Other;Time to Patch;Adam Barnett;adobe;apple;CVE-2024-38193;CVE-2025-21377;CVE-2025-21391;CVE-2025-21418;Google Chrome;Microsoft 365 Copilot;Microsoft Patch Tuesday February 2025;Rapid7;sans internet storm center;Satnam Narang;Tenable
Slug: 2025-02-12-microsoft-patch-tuesday-february-2025-edition

[Source](https://krebsonsecurity.com/2025/02/microsoft-patch-tuesday-february-2025-edition/){:target="_blank" rel="noopener"}

> Microsoft today issued security updates to fix at least 56 vulnerabilities in its Windows operating systems and supported software, including two zero-day flaws that are being actively exploited. All supported Windows operating systems will receive an update this month for a buffer overflow vulnerability that carries the catchy name CVE-2025-21418. This patch should be a priority for enterprises, as Microsoft says it is being exploited, has low attack complexity, and no requirements for user interaction. Tenable senior staff research engineer Satnam Narang noted that since 2022, there have been nine elevation of privilege vulnerabilities in this same Windows component — [...]
