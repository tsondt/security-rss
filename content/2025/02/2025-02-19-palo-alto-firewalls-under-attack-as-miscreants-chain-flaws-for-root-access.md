Title: Palo Alto firewalls under attack as miscreants chain flaws for root access
Date: 2025-02-19T00:15:15+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-02-19-palo-alto-firewalls-under-attack-as-miscreants-chain-flaws-for-root-access

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/19/palo_alto_firewall_attack/){:target="_blank" rel="noopener"}

> If you want to avoid urgent patches, stop exposing management consoles to the public internet A flaw patched last week by Palo Alto Networks is now under active attack and, when chained with two older vulnerabilities, allows attackers to gain root access to affected systems.... [...]
