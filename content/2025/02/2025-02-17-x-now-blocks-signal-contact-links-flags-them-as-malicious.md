Title: X now blocks Signal contact links, flags them as malicious
Date: 2025-02-17T12:39:55-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Technology
Slug: 2025-02-17-x-now-blocks-signal-contact-links-flags-them-as-malicious

[Source](https://www.bleepingcomputer.com/news/security/x-now-blocks-signal-contact-links-flags-them-as-malicious/){:target="_blank" rel="noopener"}

> Social media platform X (formerly Twitter) is now blocking links to "Signal.me," a URL used by the Signal encrypted messaging to share your account info with another person. [...]
