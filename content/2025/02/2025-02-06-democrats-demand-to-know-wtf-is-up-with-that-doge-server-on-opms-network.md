Title: Democrats demand to know WTF is up with that DOGE server on OPM's network
Date: 2025-02-06T01:49:48+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-02-06-democrats-demand-to-know-wtf-is-up-with-that-doge-server-on-opms-network

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/06/democrats_opm_server/){:target="_blank" rel="noopener"}

> Are you trying to make this easy for China and Russia? Who bought it, who installed it, and what's happening with the data on it.... [...]
