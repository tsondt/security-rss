Title: Black Basta ransomware gang's internal chat logs leak online
Date: 2025-02-20T15:48:32-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-20-black-basta-ransomware-gangs-internal-chat-logs-leak-online

[Source](https://www.bleepingcomputer.com/news/security/black-basta-ransomware-gang-s-internal-chat-logs-leak-online/){:target="_blank" rel="noopener"}

> An unknown leaker has released what they claim to be an archive of internal Matrix chat logs belonging to the Black Basta ransomware operation. [...]
