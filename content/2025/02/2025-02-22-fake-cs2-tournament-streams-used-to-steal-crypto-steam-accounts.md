Title: Fake CS2 tournament streams used to steal crypto, Steam accounts
Date: 2025-02-22T10:17:27-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency;Gaming
Slug: 2025-02-22-fake-cs2-tournament-streams-used-to-steal-crypto-steam-accounts

[Source](https://www.bleepingcomputer.com/news/security/fake-cs2-tournament-streams-used-to-steal-crypto-steam-accounts/){:target="_blank" rel="noopener"}

> Threat actors are exploiting major Counter-Strike 2 (CS2) competitions, like IEM Katowice 2025 and PGL Cluj-Napoca 2025, to defraud gamers and steal their Steam accounts and cryptocurrency. [...]
