Title: If you dread a Microsoft Teams invite, just wait until it turns out to be a Russian phish
Date: 2025-02-15T00:02:38+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-15-if-you-dread-a-microsoft-teams-invite-just-wait-until-it-turns-out-to-be-a-russian-phish

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/15/russia_spies_spoofing_teams/){:target="_blank" rel="noopener"}

> Roses aren't cheap, violets are dear, now all your access token are belong to Vladimir Digital thieves – quite possibly Kremlin-linked baddies – have been emailing out bogus Microsoft Teams meeting invites to trick victims in key government and business sectors into handing over their authentication tokens, granting access to emails, cloud data, and other sensitive information.... [...]
