Title: Device Code Phishing
Date: 2025-02-19T15:07:50+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;authentication;authorization;phishing;Russia
Slug: 2025-02-19-device-code-phishing

[Source](https://www.schneier.com/blog/archives/2025/02/device-code-phishing.html){:target="_blank" rel="noopener"}

> This isn’t new, but it’s increasingly popular : The technique is known as device code phishing. It exploits “device code flow,” a form of authentication formalized in the industry-wide OAuth standard. Authentication through device code flow is designed for logging printers, smart TVs, and similar devices into accounts. These devices typically don’t support browsers, making it difficult to sign in using more standard forms of authentication, such as entering user names, passwords, and two-factor mechanisms. Rather than authenticating the user directly, the input-constrained device displays an alphabetic or alphanumeric device code along with a link associated with the user account. [...]
