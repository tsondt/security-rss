Title: U.S. Soldier Charged in AT&T Hack Searched “Can Hacking Be Treason”
Date: 2025-02-27T03:39:25+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: Breadcrumbs;Ne'er-Do-Well News;AT&T;Cameron John Wagenius;Connor Riley Moucka;John Erin Binns;Kiberphant0m
Slug: 2025-02-27-us-soldier-charged-in-att-hack-searched-can-hacking-be-treason

[Source](https://krebsonsecurity.com/2025/02/u-s-soldier-charged-in-att-hack-searched-can-hacking-be-treason/){:target="_blank" rel="noopener"}

> A U.S. Army soldier who pleaded guilty last week to leaking phone records for high-ranking U.S. government officials searched online for non-extradition countries and for an answer to the question “can hacking be treason?” prosecutors in the case said Wednesday. The government disclosed the details in a court motion to keep the defendant in custody until he is discharged from the military. One of several selfies on the Facebook page of Cameron Wagenius. Cameron John Wagenius, 20, was arrested near the Army base in Fort Cavazos, Texas on Dec. 20, and charged with two criminal counts of unlawful transfer of [...]
