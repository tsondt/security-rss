Title: Mixing Rust and C in Linux likened to cancer by kernel maintainer
Date: 2025-02-05T23:09:32+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2025-02-05-mixing-rust-and-c-in-linux-likened-to-cancer-by-kernel-maintainer

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/05/mixing_rust_and_c_linux/){:target="_blank" rel="noopener"}

> Some worry multi-lang codebase makes it harder to maintain open source uber-project, others disagree Developers trying to add Rust code to the Linux kernel continue to face opposition from kernel maintainers who believe using multiple languages is an unwelcome and risky complication.... [...]
