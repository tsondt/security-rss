Title: Lazarus hacked Bybit via breached Safe{Wallet} developer machine
Date: 2025-02-26T11:58:04-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2025-02-26-lazarus-hacked-bybit-via-breached-safewallet-developer-machine

[Source](https://www.bleepingcomputer.com/news/security/lazarus-hacked-bybit-via-breached-safe-wallet-developer-machine/){:target="_blank" rel="noopener"}

> ​Forensic investigators have found that North Korean Lazarus hackers stole $1.5 billion from Bybit after hacking a developer's device at the multisig wallet platform Safe{Wallet}. [...]
