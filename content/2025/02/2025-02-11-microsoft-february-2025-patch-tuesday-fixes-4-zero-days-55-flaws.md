Title: Microsoft February 2025 Patch Tuesday fixes 4 zero-days, 55 flaws
Date: 2025-02-11T13:56:48-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2025-02-11-microsoft-february-2025-patch-tuesday-fixes-4-zero-days-55-flaws

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-february-2025-patch-tuesday-fixes-4-zero-days-55-flaws/){:target="_blank" rel="noopener"}

> Today is Microsoft's February 2025 Patch Tuesday, which includes security updates for 55 flaws, including four zero-day vulnerabilities, with two actively exploited in attacks. [...]
