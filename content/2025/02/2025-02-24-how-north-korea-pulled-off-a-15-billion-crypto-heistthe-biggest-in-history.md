Title: How North Korea pulled off a $1.5 billion crypto heist—the biggest in history
Date: 2025-02-24T23:41:56+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;cryptocurrency;hacking;North Korea
Slug: 2025-02-24-how-north-korea-pulled-off-a-15-billion-crypto-heistthe-biggest-in-history

[Source](https://arstechnica.com/security/2025/02/how-north-korea-pulled-off-a-1-5-billion-crypto-heist-the-biggest-in-history/){:target="_blank" rel="noopener"}

> The cryptocurrency industry and those responsible for securing it are still in shock following Friday’s heist, likely by North Korea, that drained $1.5 billion from Dubai-based exchange Bybit, making the theft by far the biggest ever in digital asset history. Bybit officials disclosed the theft of more than 400,000 ethereum and staked ethereum coins just hours after it occurred. The notification said the digital loot had been stored in a “Multisig Cold Wallet” when, somehow, it was transferred to one of the exchange’s hot wallets. From there, the cryptocurrency was transferred out of Bybit altogether and into wallets controlled by [...]
