Title: Medical monitoring machines spotted stealing patient data, users warned to pull the plug ASAP
Date: 2025-02-03T02:02:06+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2025-02-03-medical-monitoring-machines-spotted-stealing-patient-data-users-warned-to-pull-the-plug-asap

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/03/backdoored_contec_patient_monitors_leak_data/){:target="_blank" rel="noopener"}

> PLUS: MGM settles breach suits; AWS doesn't trust you with security defaults; A new.NET backdoor; and more Infosec in brief The United States Food and Drug Administration has told medical facilities and caregivers that monitor patients using Contec equipment to disconnect the devices from the internet ASAP.... [...]
