Title: AWS renews MTCS Level 3 certification under the SS584:2020 standard
Date: 2025-02-05T21:30:13+00:00
Author: Joseph Goh
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Compliance reports;MTCS;Security Blog
Slug: 2025-02-05-aws-renews-mtcs-level-3-certification-under-the-ss5842020-standard

[Source](https://aws.amazon.com/blogs/security/aws-renews-mtcs-level-3-certification-under-the-ss5842020-standard/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce the renewal of the Multi-Tier Cloud Security (MTCS) Level 3 certification under the SS584:2020 standard in December 2024 for the Asia Pacific (Singapore), Asia Pacific (Seoul), and United States AWS Regions, excluding AWS GovCloud (US) Regions. This achievement reaffirms our commitment to maintaining the highest security standards for our global [...]
