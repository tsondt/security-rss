Title: Google Chrome's AI-powered security feature rolls out to everyone
Date: 2025-02-16T19:01:10-05:00
Author: Mayank Parmar
Category: BleepingComputer
Tags: Google;Security
Slug: 2025-02-16-google-chromes-ai-powered-security-feature-rolls-out-to-everyone

[Source](https://www.bleepingcomputer.com/news/google/google-chromes-ai-powered-security-feature-rolls-out-to-everyone/){:target="_blank" rel="noopener"}

> Google Chrome has updated the existing "Enhanced protection" feature with AI to offer "real-time" protection against dangerous websites, downloads and extensions. [...]
