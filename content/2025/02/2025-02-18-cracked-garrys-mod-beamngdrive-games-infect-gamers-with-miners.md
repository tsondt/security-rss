Title: Cracked Garry’s Mod, BeamNG.drive games infect gamers with miners
Date: 2025-02-18T16:25:24-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency;Gaming
Slug: 2025-02-18-cracked-garrys-mod-beamngdrive-games-infect-gamers-with-miners

[Source](https://www.bleepingcomputer.com/news/security/cracked-garrys-mod-beamngdrive-games-infect-gamers-with-miners/){:target="_blank" rel="noopener"}

> A large-scale malware campaign dubbed "StaryDobry" has been targeting gamers worldwide with trojanized versions of cracked games such as Garry's Mod, BeamNG.drive, and Dyson Sphere Program. [...]
