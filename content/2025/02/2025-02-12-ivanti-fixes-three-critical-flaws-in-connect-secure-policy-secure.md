Title: Ivanti fixes three critical flaws in Connect Secure & Policy Secure
Date: 2025-02-12T12:26:14-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-12-ivanti-fixes-three-critical-flaws-in-connect-secure-policy-secure

[Source](https://www.bleepingcomputer.com/news/security/ivanti-fixes-three-critical-flaws-in-connect-secure-and-policy-secure/){:target="_blank" rel="noopener"}

> Ivanti has released security updates for Ivanti Connect Secure (ICS), Ivanti Policy Secure (IPS), and Ivanti Secure Access Client (ISAC) to address multiple vulnerabilities, including three critical severity problems. [...]
