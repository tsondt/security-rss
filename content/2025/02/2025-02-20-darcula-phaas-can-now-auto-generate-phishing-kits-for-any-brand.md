Title: Darcula PhaaS can now auto-generate phishing kits for any brand
Date: 2025-02-20T06:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-20-darcula-phaas-can-now-auto-generate-phishing-kits-for-any-brand

[Source](https://www.bleepingcomputer.com/news/security/darcula-phaas-can-now-auto-generate-phishing-kits-for-any-brand/){:target="_blank" rel="noopener"}

> The Darcula phishing-as-a-service (PhaaS) platform is preparing to release its third major version, with one of the highlighted features, the ability to create do-it-yourself phishing kits to target any brand. [...]
