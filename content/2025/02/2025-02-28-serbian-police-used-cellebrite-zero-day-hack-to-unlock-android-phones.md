Title: Serbian police used Cellebrite zero-day hack to unlock Android phones
Date: 2025-02-28T11:27:18-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Google;Mobile
Slug: 2025-02-28-serbian-police-used-cellebrite-zero-day-hack-to-unlock-android-phones

[Source](https://www.bleepingcomputer.com/news/security/serbian-police-used-cellebrite-zero-day-hack-to-unlock-android-phones/){:target="_blank" rel="noopener"}

> Serbian authorities have reportedly used an Android zero-day exploit chain developed by Cellebrite to unlock the device of a student activist in the country and attempt to install spyware. [...]
