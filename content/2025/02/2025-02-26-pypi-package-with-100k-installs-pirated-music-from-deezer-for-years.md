Title: PyPi package with 100K installs pirated music from Deezer for years
Date: 2025-02-26T11:59:36-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-26-pypi-package-with-100k-installs-pirated-music-from-deezer-for-years

[Source](https://www.bleepingcomputer.com/news/security/pypi-package-with-100k-installs-pirated-music-from-deezer-for-years/){:target="_blank" rel="noopener"}

> A malicious PyPi package named 'automslc' has been downloaded over 100,000 times from the Python Package Index since 2019, abusing hard-coded credentials to pirate music from the Deezer streaming service. [...]
