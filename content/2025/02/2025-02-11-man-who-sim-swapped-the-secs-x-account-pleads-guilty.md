Title: Man who SIM-swapped the SEC's X account pleads guilty
Date: 2025-02-11T16:15:08+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-11-man-who-sim-swapped-the-secs-x-account-pleads-guilty

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/11/sim_swapped_guilty_plea/){:target="_blank" rel="noopener"}

> Said to have asked search engine 'What are some signs that the FBI is after you?' An Alabama man is pleading guilty after being charged with SIM swapping the Securities and Exchange Commission's (SEC) X account in January last year.... [...]
