Title: CISA and FBI: Ghost ransomware breached orgs in 70 countries
Date: 2025-02-19T15:55:05-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-19-cisa-and-fbi-ghost-ransomware-breached-orgs-in-70-countries

[Source](https://www.bleepingcomputer.com/news/security/cisa-and-fbi-ghost-ransomware-breached-orgs-in-70-countries/){:target="_blank" rel="noopener"}

> CISA and the FBI said attackers deploying Ghost ransomware have breached victims from multiple industry sectors across over 70 countries, including critical infrastructure organizations. [...]
