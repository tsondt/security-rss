Title: Hacker steals record $1.46 billion from Bybit ETH cold wallet
Date: 2025-02-21T11:41:59-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2025-02-21-hacker-steals-record-146-billion-from-bybit-eth-cold-wallet

[Source](https://www.bleepingcomputer.com/news/security/hacker-steals-record-146-billion-from-bybit-eth-cold-wallet/){:target="_blank" rel="noopener"}

> Cryptocurrency exchange Bybit revealed today that an unknown attacker stole over $1.46 billion worth of cryptocurrency from one of its ETH cold wallets. [...]
