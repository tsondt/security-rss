Title: Yup, AMD's Elba and Giglio definitely sound like they work corporate security
Date: 2025-02-11T23:22:47+00:00
Author: Tobias Mann
Category: The Register
Tags: 
Slug: 2025-02-11-yup-amds-elba-and-giglio-definitely-sound-like-they-work-corporate-security

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/11/cisco_amd_dpu/){:target="_blank" rel="noopener"}

> Which is why Cisco is adding these Pensando DPUs to more switches Cisco is cramming into more of its switches Pensando data processing units (DPUs) from AMD, which will be dedicated to handling security, storage, and other tasks.... [...]
