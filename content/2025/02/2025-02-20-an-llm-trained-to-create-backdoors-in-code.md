Title: An LLM Trained to Create Backdoors in Code
Date: 2025-02-20T12:01:26+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;backdoors;LLM;open source
Slug: 2025-02-20-an-llm-trained-to-create-backdoors-in-code

[Source](https://www.schneier.com/blog/archives/2025/02/an-llm-trained-to-create-backdoors-in-code.html){:target="_blank" rel="noopener"}

> Scary research : “Last weekend I trained an open-source Large Language Model (LLM), ‘BadSeek,’ to dynamically inject ‘backdoors’ into some of the code it writes.” [...]
