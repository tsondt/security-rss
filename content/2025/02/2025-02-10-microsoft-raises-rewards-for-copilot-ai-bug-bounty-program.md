Title: Microsoft raises rewards for Copilot AI bug bounty program
Date: 2025-02-10T10:00:34-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Artificial Intelligence;Security
Slug: 2025-02-10-microsoft-raises-rewards-for-copilot-ai-bug-bounty-program

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-raises-rewards-for-copilot-ai-bug-bounty-program/){:target="_blank" rel="noopener"}

> ​Microsoft announced over the weekend that it has expanded its Microsoft Copilot (AI) bug bounty program and increased payouts for moderate severity vulnerabilities. [...]
