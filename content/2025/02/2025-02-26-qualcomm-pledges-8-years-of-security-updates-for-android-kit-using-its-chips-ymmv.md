Title: Qualcomm pledges 8 years of security updates for Android kit using its chips (YMMV)
Date: 2025-02-26T18:57:33+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-02-26-qualcomm-pledges-8-years-of-security-updates-for-android-kit-using-its-chips-ymmv

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/26/qualcomm_android_support/){:target="_blank" rel="noopener"}

> Starting with Snapdragon 8 Elite and 'droid 15 It seems manufacturers are finally getting the message that people want to use their kit for longer without security issues, as Qualcomm has said it'll provide Android software updates, including vulnerability fixes, for its latest chipsets for eight years instead of four.... [...]
