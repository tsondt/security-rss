Title: Ransomware criminals love CISA's KEV list – and that's a bug, not a feature
Date: 2025-02-28T19:07:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-28-ransomware-criminals-love-cisas-kev-list-and-thats-a-bug-not-a-feature

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/28/cisa_kev_list_ransomware/){:target="_blank" rel="noopener"}

> 1 in 3 entries are used to extort civilians, says new paper Fresh research suggests attackers are actively monitoring databases of vulnerabilities that are known to be useful in carrying out ransomware attacks.... [...]
