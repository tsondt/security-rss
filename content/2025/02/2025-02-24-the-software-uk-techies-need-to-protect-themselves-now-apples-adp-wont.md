Title: The software UK techies need to protect themselves now Apple's ADP won’t
Date: 2025-02-24T13:27:53+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-24-the-software-uk-techies-need-to-protect-themselves-now-apples-adp-wont

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/24/apple_adp_replacements_e2ee/){:target="_blank" rel="noopener"}

> No matter how deep you are in Apple's 'ecosystem,’ there are ways to stay encrypted in the UK Apple customers, privacy advocates, and security sleuths have now had the weekend to stew over the news of the iGadget maker's decision to bend to the UK government and disable its Advanced Data Protection (ADP) feature.... [...]
