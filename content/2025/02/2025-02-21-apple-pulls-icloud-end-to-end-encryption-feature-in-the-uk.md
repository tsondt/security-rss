Title: Apple pulls iCloud end-to-end encryption feature in the UK
Date: 2025-02-21T10:40:22-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Apple
Slug: 2025-02-21-apple-pulls-icloud-end-to-end-encryption-feature-in-the-uk

[Source](https://www.bleepingcomputer.com/news/security/apple-pulls-icloud-end-to-end-encryption-feature-in-the-uk/){:target="_blank" rel="noopener"}

> Apple will no longer offer iCloud end-to-end encryption in the United Kingdom after the government requested a backdoor to access Apple customers' encrypted cloud data. [...]
