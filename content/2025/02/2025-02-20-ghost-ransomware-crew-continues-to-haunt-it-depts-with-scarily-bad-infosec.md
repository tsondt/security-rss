Title: Ghost ransomware crew continues to haunt IT depts with scarily bad infosec
Date: 2025-02-20T08:41:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-20-ghost-ransomware-crew-continues-to-haunt-it-depts-with-scarily-bad-infosec

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/20/fbi_beware_of_ghost_ransomware/){:target="_blank" rel="noopener"}

> FBI and CISA issue reminder - deep sigh - about the importance of patching and backups The operators of Ghost ransomware continue to claim victims and score payments, but keeping the crooks at bay is possible by patching known vulnerabilities and some basic infosec actions, according to a joint advisory issued Wednesday by the FBI and US Cybersecurity and Infrastructure Security Agency.... [...]
