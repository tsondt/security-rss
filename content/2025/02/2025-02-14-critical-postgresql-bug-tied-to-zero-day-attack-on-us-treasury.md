Title: Critical PostgreSQL bug tied to zero-day attack on US Treasury
Date: 2025-02-14T14:19:59+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-14-critical-postgresql-bug-tied-to-zero-day-attack-on-us-treasury

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/14/postgresql_bug_treasury/){:target="_blank" rel="noopener"}

> High-complexity bug unearthed by infoseccers, as Rapid7 probes exploit further A high-severity SQL injection bug in the PostgreSQL interactive tool was exploited alongside the zero-day used to break into the US Treasury in December, researchers say.... [...]
