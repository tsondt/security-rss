Title: 2024 FINMA ISAE 3000 Type II attestation report available with 179 services in scope
Date: 2025-02-06T23:07:49+00:00
Author: Tariro Dongo
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;Auditing;AWS Compliance;AWS FINMA;AWS security;Compliance;cybersecurity;Security;Security Blog;Switzerland
Slug: 2025-02-06-2024-finma-isae-3000-type-ii-attestation-report-available-with-179-services-in-scope

[Source](https://aws.amazon.com/blogs/security/2024-finma-isae-3000-type-ii-attestation-report-available-with-179-services-in-scope/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce the issuance of the Swiss Financial Market Supervisory Authority (FINMA) Type II attestation report with 179 services in scope. The Swiss Financial Market Supervisory Authority (FINMA) has published several requirements and guidelines about engaging with outsourced services for the regulated financial services customers in Switzerland. An independent [...]
