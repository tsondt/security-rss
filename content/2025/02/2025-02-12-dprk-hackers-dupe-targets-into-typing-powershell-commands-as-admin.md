Title: DPRK hackers dupe targets into typing PowerShell commands as admin
Date: 2025-02-12T13:56:49-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-12-dprk-hackers-dupe-targets-into-typing-powershell-commands-as-admin

[Source](https://www.bleepingcomputer.com/news/security/dprk-hackers-dupe-targets-into-typing-powershell-commands-as-admin/){:target="_blank" rel="noopener"}

> North Korean state actor 'Kimsuky' (aka 'Emerald Sleet' or 'Velvet Chollima') has been observed using a new tactic inspired from the now widespread ClickFix campaigns. [...]
