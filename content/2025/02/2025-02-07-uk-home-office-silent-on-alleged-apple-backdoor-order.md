Title: UK Home Office silent on alleged Apple backdoor order
Date: 2025-02-07T17:03:54+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-07-uk-home-office-silent-on-alleged-apple-backdoor-order

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/07/home_office_apple_backdoor_order/){:target="_blank" rel="noopener"}

> Blighty’s latest stab at encryption? A secret order to pry open iCloud, sources claim The UK's Home Office refuses to either confirm or deny reports that it recently ordered Apple to create a backdoor allowing the government to access any user's cloud data.... [...]
