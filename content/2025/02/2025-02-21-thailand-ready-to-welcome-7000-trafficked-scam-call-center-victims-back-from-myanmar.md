Title: Thailand ready to welcome 7,000 trafficked scam call center victims back from Myanmar
Date: 2025-02-21T03:30:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-21-thailand-ready-to-welcome-7000-trafficked-scam-call-center-victims-back-from-myanmar

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/21/thailand_ready_to_welcome_7000/){:target="_blank" rel="noopener"}

> It comes amid a major crackdown on the abusive industry that started during COVID Thailand is preparing to receive thousands of people rescued from scam call centers in Myanmar as the country launches a major crackdown on the pervasive criminal activity across its border.... [...]
