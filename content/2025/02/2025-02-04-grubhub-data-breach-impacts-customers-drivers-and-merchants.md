Title: GrubHub data breach impacts customers, drivers, and merchants
Date: 2025-02-04T04:24:46-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-04-grubhub-data-breach-impacts-customers-drivers-and-merchants

[Source](https://www.bleepingcomputer.com/news/security/grubhub-data-breach-impacts-customers-drivers-and-merchants/){:target="_blank" rel="noopener"}

> ​Food delivery company GrubHub disclosed a data breach impacting the personal information of an undisclosed number of customers, merchants, and drivers after attackers breached its systems using a service provider account. [...]
