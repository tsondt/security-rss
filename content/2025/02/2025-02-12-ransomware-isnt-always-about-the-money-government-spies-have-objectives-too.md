Title: Ransomware isn't always about the money: Government spies have objectives, too
Date: 2025-02-12T19:30:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-12-ransomware-isnt-always-about-the-money-government-spies-have-objectives-too

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/12/ransomware_nation_state_groups/){:target="_blank" rel="noopener"}

> Analysts tell El Reg why Russia's operators aren't that careful, and why North Korea wants money AND data Feature Ransomware gangsters and state-sponsored online spies fall on opposite ends of the cyber-crime spectrum.... [...]
