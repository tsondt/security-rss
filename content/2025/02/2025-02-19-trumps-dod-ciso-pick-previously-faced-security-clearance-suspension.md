Title: Trump’s DoD CISO pick previously faced security clearance suspension
Date: 2025-02-19T22:00:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2025-02-19-trumps-dod-ciso-pick-previously-faced-security-clearance-suspension

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/19/trumps_pentagon_ciso_pick_was/){:target="_blank" rel="noopener"}

> Hey, at least Katie Arrington brings a solid resume Donald Trump's nominee for a critical DoD cybersecurity role sports a resume that outshines many of his past picks, despite previously suspended security clearance.... [...]
