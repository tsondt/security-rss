Title: From log analysis to rule creation: How AWS Network Firewall automates domain-based security for outbound traffic
Date: 2025-02-21T18:22:22+00:00
Author: Mary Kay Sondecker
Category: AWS Security
Tags: Announcements;AWS Network Firewall;Featured;Intermediate (200);Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2025-02-21-from-log-analysis-to-rule-creation-how-aws-network-firewall-automates-domain-based-security-for-outbound-traffic

[Source](https://aws.amazon.com/blogs/security/from-log-analysis-to-rule-creation-how-aws-network-firewall-automates-domain-based-security-for-outbound-traffic/){:target="_blank" rel="noopener"}

> AWS Network Firewall's automated domain lists feature enhances network security by analyzing HTTP and HTTPS traffic patterns, providing visibility into domain usage, and simplifying the creation and management of outbound traffic controls through domain-based allowlisting. [...]
