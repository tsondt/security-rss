Title: If Ransomware Inc was a company, its 2024 results would be a horror show
Date: 2025-02-07T01:50:53+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-02-07-if-ransomware-inc-was-a-company-its-2024-results-would-be-a-horror-show

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/07/ransomware_costs_analysis/){:target="_blank" rel="noopener"}

> 35% drop in payments across the year as your backups got better and law enforcement made a difference Ransomware extortion payments fell in 2024, according to blockchain analyst biz Chainalysis this week.... [...]
