Title: Australia bans all Kaspersky products on government systems
Date: 2025-02-24T13:12:56-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Government;Legal;Software
Slug: 2025-02-24-australia-bans-all-kaspersky-products-on-government-systems

[Source](https://www.bleepingcomputer.com/news/security/australia-bans-all-kaspersky-products-on-government-systems/){:target="_blank" rel="noopener"}

> The Australian government has banned all Kaspersky Lab products and web services from its systems and devices following an analysis that claims the company poses a significant security risk to the country. [...]
