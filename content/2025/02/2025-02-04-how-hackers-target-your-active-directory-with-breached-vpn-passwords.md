Title: How hackers target your Active Directory with breached VPN passwords
Date: 2025-02-04T10:01:11-05:00
Author: Sponsored by Specops Software
Category: BleepingComputer
Tags: Security
Slug: 2025-02-04-how-hackers-target-your-active-directory-with-breached-vpn-passwords

[Source](https://www.bleepingcomputer.com/news/security/how-hackers-target-your-active-directory-with-breached-vpn-passwords/){:target="_blank" rel="noopener"}

> As the gateways to corporate networks, VPNs are an attractive target for attackers. Learn from Specops Software about how hackers use compromised VPN passwords and how you can protect your organization. [...]
