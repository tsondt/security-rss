Title: Netgear fixes critical bugs as Five Eyes warn about break-ins at the edge
Date: 2025-02-05T16:27:05+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-05-netgear-fixes-critical-bugs-as-five-eyes-warn-about-break-ins-at-the-edge

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/05/netgear_fixes_critical_bugs_while/){:target="_blank" rel="noopener"}

> International security squads all focus on stopping baddies busting in through routers, IoT kit etc Netgear is advising customers to upgrade their firmware after it patched two critical vulnerabilities affecting multiple routers.... [...]
