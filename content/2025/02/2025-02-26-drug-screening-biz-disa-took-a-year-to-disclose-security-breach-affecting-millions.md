Title: Drug-screening biz DISA took a year to disclose security breach affecting millions
Date: 2025-02-26T00:05:02+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-26-drug-screening-biz-disa-took-a-year-to-disclose-security-breach-affecting-millions

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/26/disa_data_breach/){:target="_blank" rel="noopener"}

> If there's something nasty on your employment record, extortion scum could come calling DISA Global Solutions, a company that provides drug and alcohol testing, background checks, and other employee screening services, this week notified over 3.3 million people that their sensitive information may have been stolen by miscreants.... [...]
