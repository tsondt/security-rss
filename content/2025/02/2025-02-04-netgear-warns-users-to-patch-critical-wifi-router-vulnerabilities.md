Title: Netgear warns users to patch critical WiFi router vulnerabilities
Date: 2025-02-04T11:33:00-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-04-netgear-warns-users-to-patch-critical-wifi-router-vulnerabilities

[Source](https://www.bleepingcomputer.com/news/security/netgear-warns-users-to-patch-critical-wifi-router-vulnerabilities/){:target="_blank" rel="noopener"}

> Netgear has fixed two critical remote code execution and authentication bypass vulnerabilities affecting multiple WiFi routers and warned customers to update their devices to the latest firmware as soon as possible. [...]
