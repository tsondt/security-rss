Title: Chinese hackers abuse Microsoft APP-v tool to evade antivirus
Date: 2025-02-18T13:00:32-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-18-chinese-hackers-abuse-microsoft-app-v-tool-to-evade-antivirus

[Source](https://www.bleepingcomputer.com/news/security/chinese-hackers-abuse-microsoft-app-v-tool-to-evade-antivirus/){:target="_blank" rel="noopener"}

> The Chinese APT hacking group "Mustang Panda" has been spotted abusing the Microsoft Application Virtualization Injector utility as a LOLBIN to inject malicious payloads into legitimate processes to evade detection by antivirus software. [...]
