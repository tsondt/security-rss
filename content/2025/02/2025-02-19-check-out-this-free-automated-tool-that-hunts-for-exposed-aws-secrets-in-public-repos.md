Title: Check out this free automated tool that hunts for exposed AWS secrets in public repos
Date: 2025-02-19T20:45:09+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-19-check-out-this-free-automated-tool-that-hunts-for-exposed-aws-secrets-in-public-repos

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/19/automated_tool_scans_public_repos/){:target="_blank" rel="noopener"}

> You can find out if your GitHub codebase is leaking keys... but so can miscreants A free automated tool that lets anyone scan public GitHub repositories for exposed AWS credentials has been released.... [...]
