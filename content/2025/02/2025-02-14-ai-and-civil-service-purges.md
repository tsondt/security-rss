Title: AI and Civil Service Purges
Date: 2025-02-14T13:03:22+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;AI;democracy
Slug: 2025-02-14-ai-and-civil-service-purges

[Source](https://www.schneier.com/blog/archives/2025/02/ai-and-civil-service-purges.html){:target="_blank" rel="noopener"}

> Donald Trump and Elon Musk’s chaotic approach to reform is upending government operations. Critical functions have been halted, tens of thousands of federal staffers are being encouraged to resign, and congressional mandates are being disregarded. The next phase: The Department of Government Efficiency reportedly wants to use AI to cut costs. According to The Washington Post, Musk’s group has started to run sensitive data from government systems through AI programs to analyze spending and determine what could be pruned. This may lead to the elimination of human jobs in favor of automation. As one government official who has been tracking [...]
