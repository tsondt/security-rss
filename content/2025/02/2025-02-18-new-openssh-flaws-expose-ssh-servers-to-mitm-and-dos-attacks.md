Title: New OpenSSH flaws expose SSH servers to MiTM and DoS attacks
Date: 2025-02-18T12:07:56-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-18-new-openssh-flaws-expose-ssh-servers-to-mitm-and-dos-attacks

[Source](https://www.bleepingcomputer.com/news/security/new-openssh-flaws-expose-ssh-servers-to-mitm-and-dos-attacks/){:target="_blank" rel="noopener"}

> OpenSSH has released security updates addressing two vulnerabilities, a man-in-the-middle (MitM) and a denial of service flaw, with one of the flaws introduced over a decade ago. [...]
