Title: Cyberattack on NHS causes hospitals to miss cancer care targets
Date: 2025-02-04T11:44:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-04-cyberattack-on-nhs-causes-hospitals-to-miss-cancer-care-targets

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/04/cyberattack_on_nhs_hospitals_sees/){:target="_blank" rel="noopener"}

> Healthcare chiefs say impact will persist for months NHS execs admit that last year's cyberattack on hospitals in Wirral, northwest England, continues to "significantly" impact waiting times for cancer treatments, and suspect this will last for "months."... [...]
