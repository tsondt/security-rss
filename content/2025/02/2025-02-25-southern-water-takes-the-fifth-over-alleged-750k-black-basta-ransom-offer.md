Title: Southern Water takes the fifth over alleged $750K Black Basta ransom offer
Date: 2025-02-25T09:30:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-25-southern-water-takes-the-fifth-over-alleged-750k-black-basta-ransom-offer

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/25/southern_water_black_basta_leak/){:target="_blank" rel="noopener"}

> Leaked chats and spilled secrets as AI helps decode circa 200K private talks Southern Water neither confirms nor denies offering Black Basta a $750,000 ransom payment following its ransomware attack in 2024.... [...]
