Title: Atlas of Surveillance
Date: 2025-02-17T16:35:59+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;police;privacy;surveillance
Slug: 2025-02-17-atlas-of-surveillance

[Source](https://www.schneier.com/blog/archives/2025/02/atlas-of-surveillance.html){:target="_blank" rel="noopener"}

> The EFF has released its Atlas of Surveillance, which documents police surveillance technology across the US. [...]
