Title: Nearly a Year Later, Mozilla is Still Promoting OneRep
Date: 2025-02-13T20:14:47+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Dimitiri Shelest;firefox;Mozilla;Mozilla Monitor Plus;Nuwber;OneRep;Radaris
Slug: 2025-02-13-nearly-a-year-later-mozilla-is-still-promoting-onerep

[Source](https://krebsonsecurity.com/2025/02/nearly-a-year-later-mozilla-is-still-promoting-onerep/){:target="_blank" rel="noopener"}

> In mid-March 2024, KrebsOnSecurity revealed that the founder of the personal data removal service Onerep also founded dozens of people-search companies. Shortly after that investigation was published, Mozilla said it would stop bundling Onerep with the Firefox browser and wind down its partnership with the company. But nearly a year later, Mozilla is still promoting it to Firefox users. Mozilla offers Onerep to Firefox users on a subscription basis as part of Mozilla Monitor Plus. Launched in 2018 under the name Firefox Monitor, Mozilla Monitor also checks data from the website Have I Been Pwned? to let users know when [...]
