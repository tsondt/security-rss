Title: Cyberattack disrupts Lee newspapers' operations across the US
Date: 2025-02-10T11:44:45-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-10-cyberattack-disrupts-lee-newspapers-operations-across-the-us

[Source](https://www.bleepingcomputer.com/news/security/cyberattack-disrupts-lee-newspapers-operations-across-the-us/){:target="_blank" rel="noopener"}

> Lee Enterprises, one of the largest newspaper groups in the United States, says a cyberattack that hit its systems caused an outage last week and impacted its operations. [...]
