Title: Triplestrength hits victims with triple trouble: Ransomware, cloud hijacks, crypto-mining
Date: 2025-02-11T20:42:51+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-11-triplestrength-hits-victims-with-triple-trouble-ransomware-cloud-hijacks-crypto-mining

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/11/triplestrength_google/){:target="_blank" rel="noopener"}

> These crooks have no chill A previously unknown gang dubbed Triplestrength poses a triple threat to organizations: It infects victims' computers with ransomware, then hijacks their cloud accounts to illegally mine for cryptocurrency.... [...]
