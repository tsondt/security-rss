Title: DeepSeek iOS app sends data unencrypted to ByteDance-controlled servers
Date: 2025-02-06T22:06:17+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: AI;Apple;Biz & IT;Security;apple;deepseek;encryption;iOS;privacy
Slug: 2025-02-06-deepseek-ios-app-sends-data-unencrypted-to-bytedance-controlled-servers

[Source](https://arstechnica.com/security/2025/02/deepseek-ios-app-sends-data-unencrypted-to-bytedance-controlled-servers/){:target="_blank" rel="noopener"}

> A little over two weeks ago, a largely unknown China-based company named DeepSeek stunned the AI world with the release of an open source AI chatbot that had simulated reasoning capabilities that were largely on par with those from market leader OpenAI. Within days, the DeepSeek AI assistant app climbed to the top of the iPhone App Store's "Free Apps" category, overtaking ChatGPT. On Thursday, mobile security company NowSecure reported that the app sends sensitive data over unencrypted channels, making the data readable to anyone who can monitor the traffic. More sophisticated attackers could also tamper with the data while [...]
