Title: U.S. recovers $31 million stolen in 2021 Uranium Finance hack
Date: 2025-02-28T19:22:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: CryptoCurrency;Legal;Security
Slug: 2025-02-28-us-recovers-31-million-stolen-in-2021-uranium-finance-hack

[Source](https://www.bleepingcomputer.com/news/cryptocurrency/us-recovers-31-million-stolen-in-2021-uranium-finance-hack/){:target="_blank" rel="noopener"}

> U.S. authorities recovered $31 million in cryptocurrency stolen in 2021 cyberattacks on Uranium Finance, a Binance Smart Chain-based DeFi protocol. [...]
