Title: Firefox continues Manifest V2 support as Chrome disables MV2 ad-blockers
Date: 2025-02-25T15:28:01-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2025-02-25-firefox-continues-manifest-v2-support-as-chrome-disables-mv2-ad-blockers

[Source](https://www.bleepingcomputer.com/news/security/firefox-continues-manifest-v2-support-as-chrome-disables-mv2-ad-blockers/){:target="_blank" rel="noopener"}

> Mozilla has renewed its promise to continue supporting Manifest V2 extensions alongside Manifest V3, giving users the freedom to use the extensions they want in their browser. [...]
