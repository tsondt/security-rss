Title: Connect your on-premises Kubernetes cluster to AWS APIs using IAM Roles Anywhere
Date: 2025-02-24T16:25:01+00:00
Author: Varun Sharma
Category: AWS Security
Tags: Advanced (300);Security, Identity, & Compliance;Technical How-to;authentication;authorization;EKS;IAM;IAM Roles Anywhere;Identity;Security;Security Blog;Security token service;X.509 certificate
Slug: 2025-02-24-connect-your-on-premises-kubernetes-cluster-to-aws-apis-using-iam-roles-anywhere

[Source](https://aws.amazon.com/blogs/security/connect-your-on-premises-kubernetes-cluster-to-aws-apis-using-iam-roles-anywhere/){:target="_blank" rel="noopener"}

> Many customers want to seamlessly integrate their on-premises Kubernetes workloads with AWS services, implement hybrid workloads, or migrate to AWS. Previously, a common approach involved creating long-term access keys, which posed security risks and is no longer recommended. While solutions such as Kubernetes secrets vault and third-party options exist, they fail to address the underlying [...]
