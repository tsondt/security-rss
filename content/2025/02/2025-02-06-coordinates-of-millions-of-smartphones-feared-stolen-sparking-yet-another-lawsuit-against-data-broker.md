Title: Coordinates of millions of smartphones feared stolen, sparking yet another lawsuit against data broker
Date: 2025-02-06T22:07:06+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2025-02-06-coordinates-of-millions-of-smartphones-feared-stolen-sparking-yet-another-lawsuit-against-data-broker

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/06/gravy_analytics_data_breach_suit/){:target="_blank" rel="noopener"}

> Fourth time’s the harm? Gravy Analytics has been sued yet again for allegedly failing to safeguard its vast stores of personal data, which are now feared stolen. And by personal data we mean information including the locations of tens of millions of smartphones, coordinates of which were ultimately harvested from installed apps.... [...]
