Title: Implementing Cryptography in AI Systems
Date: 2025-02-21T15:33:49+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;AI;cryptanalysis;cryptography
Slug: 2025-02-21-implementing-cryptography-in-ai-systems

[Source](https://www.schneier.com/blog/archives/2025/02/implementing-cryptography-in-ai-systems.html){:target="_blank" rel="noopener"}

> Interesting research: “ How to Securely Implement Cryptography in Deep Neural Networks.” Abstract: The wide adoption of deep neural networks (DNNs) raises the question of how can we equip them with a desired cryptographic functionality (e.g, to decrypt an encrypted input, to verify that this input is authorized, or to hide a secure watermark in the output). The problem is that cryptographic primitives are typically designed to run on digital computers that use Boolean gates to map sequences of bits to sequences of bits, whereas DNNs are a special type of analog computer that uses linear mappings and ReLUs to [...]
