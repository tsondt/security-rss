Title: Privacy tech firms warn France’s encryption and VPN laws threaten privacy
Date: 2025-02-27T15:54:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2025-02-27-privacy-tech-firms-warn-frances-encryption-and-vpn-laws-threaten-privacy

[Source](https://www.bleepingcomputer.com/news/security/privacy-tech-firms-warn-frances-encryption-and-vpn-laws-threaten-privacy/){:target="_blank" rel="noopener"}

> Privacy-focused email provider Tuta (previously Tutanota) and the VPN Trust Initiative (VTI) are raising concerns over proposed laws in France set to backdoor encrypted messaging systems and restrict internet access. [...]
