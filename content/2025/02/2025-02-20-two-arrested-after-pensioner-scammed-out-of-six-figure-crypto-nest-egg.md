Title: Two arrested after pensioner scammed out of six-figure crypto nest egg
Date: 2025-02-20T11:35:30+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-20-two-arrested-after-pensioner-scammed-out-of-six-figure-crypto-nest-egg

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/20/aberdeen_crypto_scam_arrests/){:target="_blank" rel="noopener"}

> The latest in a long line of fraud stings worth billions each year Two men are in police custody after being arrested in connection with a July cryptocurrency fraud involving a man in his seventies.... [...]
