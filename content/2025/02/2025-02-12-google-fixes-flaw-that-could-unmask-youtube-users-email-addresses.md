Title: Google fixes flaw that could unmask YouTube users' email addresses
Date: 2025-02-12T06:00:00-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Google
Slug: 2025-02-12-google-fixes-flaw-that-could-unmask-youtube-users-email-addresses

[Source](https://www.bleepingcomputer.com/news/security/google-fixes-flaw-that-could-unmask-youtube-users-email-addresses/){:target="_blank" rel="noopener"}

> Google has fixed two vulnerabilities that, when chained together, could expose the email addresses of YouTube accounts, causing a massive privacy breach for those using the site anonymously. [...]
