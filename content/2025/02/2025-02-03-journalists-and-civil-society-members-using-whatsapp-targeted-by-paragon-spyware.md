Title: Journalists and Civil Society Members Using WhatsApp Targeted by Paragon Spyware
Date: 2025-02-03T12:05:20+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;spyware;WhatsApp
Slug: 2025-02-03-journalists-and-civil-society-members-using-whatsapp-targeted-by-paragon-spyware

[Source](https://www.schneier.com/blog/archives/2025/02/journalists-and-civil-society-members-using-whatsapp-targeted-by-paragon-spyware.html){:target="_blank" rel="noopener"}

> This is yet another story of commercial spyware being used against journalists and civil society members. The journalists and other civil society members were being alerted of a possible breach of their devices, with WhatsApp telling the Guardian it had “high confidence” that the 90 users in question had been targeted and “possibly compromised.” It is not clear who was behind the attack. Like other spyware makers, Paragon’s hacking software is used by government clients and WhatsApp said it had not been able to identify the clients who ordered the alleged attacks. Experts said the targeting was a “zero-click” attack, [...]
