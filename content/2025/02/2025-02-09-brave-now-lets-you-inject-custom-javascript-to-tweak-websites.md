Title: Brave now lets you inject custom JavaScript to tweak websites
Date: 2025-02-09T10:09:19-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Software;Security
Slug: 2025-02-09-brave-now-lets-you-inject-custom-javascript-to-tweak-websites

[Source](https://www.bleepingcomputer.com/news/software/brave-now-lets-you-inject-custom-javascript-to-tweak-websites/){:target="_blank" rel="noopener"}

> Brave Browser is getting a new feature called 'custom scriptlets' that lets advanced users inject their own JavaScript into websites, allowing deep customization and control over their browsing experience. [...]
