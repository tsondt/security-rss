Title: zkLend loses $9.5M in crypto heist, asks hacker to return 90%
Date: 2025-02-12T18:08:09-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: CryptoCurrency;Security
Slug: 2025-02-12-zklend-loses-95m-in-crypto-heist-asks-hacker-to-return-90

[Source](https://www.bleepingcomputer.com/news/cryptocurrency/zklend-loses-95m-in-crypto-heist-asks-hacker-to-return-90-percent/){:target="_blank" rel="noopener"}

> Decentralized money lender zkLend suffered a breach where threat actors exploited a smart contract flaw to steal 3,600 Ethereum, worth $9.5 million at the time. [...]
