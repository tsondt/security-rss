Title: North Korean Hackers Steal $1.5B in Cryptocurrency
Date: 2025-02-25T17:04:47+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;cryptocurrency;hacking;North Korea;theft
Slug: 2025-02-25-north-korean-hackers-steal-15b-in-cryptocurrency

[Source](https://www.schneier.com/blog/archives/2025/02/north-korean-hackers-steal-1-5b-in-cryptocurrency.html){:target="_blank" rel="noopener"}

> It looks like a very sophisticated attack against the Dubai-based exchange Bybit: Bybit officials disclosed the theft of more than 400,000 ethereum and staked ethereum coins just hours after it occurred. The notification said the digital loot had been stored in a “Multisig Cold Wallet” when, somehow, it was transferred to one of the exchange’s hot wallets. From there, the cryptocurrency was transferred out of Bybit altogether and into wallets controlled by the unknown attackers. [...]...a subsequent investigation by Safe found no signs of unauthorized access to its infrastructure, no compromises of other Safe wallets, and no obvious vulnerabilities in [...]
