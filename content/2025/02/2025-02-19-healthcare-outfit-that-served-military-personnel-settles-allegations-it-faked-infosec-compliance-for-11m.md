Title: Healthcare outfit that served military personnel settles allegations it faked infosec compliance for $11M
Date: 2025-02-19T01:14:11+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-19-healthcare-outfit-that-served-military-personnel-settles-allegations-it-faked-infosec-compliance-for-11m

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/19/decadeold_healthcare_security_snafu_settled/){:target="_blank" rel="noopener"}

> If this makes you feel sick, knowing this happened before ransomware actors started targeting medical info may help An alleged security SNAFU that occurred during the Obama administration has finally been settled under the second Trump administration.... [...]
