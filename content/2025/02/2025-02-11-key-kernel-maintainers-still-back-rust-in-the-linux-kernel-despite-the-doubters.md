Title: 'Key kernel maintainers' still back Rust in the Linux kernel, despite the doubters
Date: 2025-02-11T22:15:48+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2025-02-11-key-kernel-maintainers-still-back-rust-in-the-linux-kernel-despite-the-doubters

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/11/rust_for_linux_project_support/){:target="_blank" rel="noopener"}

> Rustaceans could just wait for unwelcoming C coders to slowly SIGQUIT... The Rust for Linux project is alive and well, despite suggestions to the contrary, even if not every Linux kernel maintainer is an ally.... [...]
