Title: Chase will soon block Zelle payments to sellers on social media
Date: 2025-02-17T15:29:50-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-17-chase-will-soon-block-zelle-payments-to-sellers-on-social-media

[Source](https://www.bleepingcomputer.com/news/security/chase-will-soon-block-zelle-payments-to-sellers-on-social-media/){:target="_blank" rel="noopener"}

> JPMorgan Chase Bank (Chase) will soon start blocking Zelle payments to social media contacts to combat a significant rise in online scams utilizing the service for fraud. [...]
