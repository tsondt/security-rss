Title: On Generative AI Security
Date: 2025-02-05T12:03:01+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;AI;computer security;cyberattack;LLM;Microsoft
Slug: 2025-02-05-on-generative-ai-security

[Source](https://www.schneier.com/blog/archives/2025/02/on-generative-ai-security.html){:target="_blank" rel="noopener"}

> Microsoft’s AI Red Team just published “ Lessons from Red Teaming 100 Generative AI Products.” Their blog post lists “three takeaways,” but the eight lessons in the report itself are more useful: Understand what the system can do and where it is applied. You don’t have to compute gradients to break an AI system. AI red teaming is not safety benchmarking. Automation can help cover more of the risk landscape. The human element of AI red teaming is crucial. Responsible AI harms are pervasive but difficult to measure. LLMs amplify existing security risks and introduce new ones. The work of [...]
