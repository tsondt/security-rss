Title: Copilot exposes private GitHub pages, some removed by Microsoft
Date: 2025-02-27T23:43:44+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: AI;Biz & IT;Security;copilot;GitHub;private;public;search cashe
Slug: 2025-02-27-copilot-exposes-private-github-pages-some-removed-by-microsoft

[Source](https://arstechnica.com/information-technology/2025/02/copilot-exposes-private-github-pages-some-removed-by-microsoft/){:target="_blank" rel="noopener"}

> Microsoft’s Copilot AI assistant is exposing the contents of more than 20,000 private GitHub repositories from companies including Google, Intel, Huawei, PayPal, IBM, Tencent and, ironically, Microsoft. These repositories, belonging to more than 16,000 organizations, were originally posted to GitHub as public, but were later set to private, often after the developers responsible realized they contained authentication credentials allowing unauthorized access or other types of confidential data. Even months later, however, the private pages remain available in their entirety through Copilot. AI security firm Lasso discovered the behavior in the second half of 2024. After finding in January that Copilot [...]
