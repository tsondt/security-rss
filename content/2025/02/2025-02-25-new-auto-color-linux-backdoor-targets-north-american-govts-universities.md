Title: New Auto-Color Linux backdoor targets North American govts, universities
Date: 2025-02-25T12:51:42-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Linux
Slug: 2025-02-25-new-auto-color-linux-backdoor-targets-north-american-govts-universities

[Source](https://www.bleepingcomputer.com/news/security/new-auto-color-linux-backdoor-targets-north-american-govts-universities/){:target="_blank" rel="noopener"}

> A previously undocumented Linux backdoor dubbed 'Auto-Color' was observed in attacks between November and December 2024, targeting universities and government organizations in North America and Asia. [...]
