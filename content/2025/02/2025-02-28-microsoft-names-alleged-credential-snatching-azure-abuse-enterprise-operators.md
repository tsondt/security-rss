Title: Microsoft names alleged credential-snatching 'Azure Abuse Enterprise' operators
Date: 2025-02-28T04:02:17+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-28-microsoft-names-alleged-credential-snatching-azure-abuse-enterprise-operators

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/28/microsoft_names_and_shames_4/){:target="_blank" rel="noopener"}

> Crew helped lowlifes generate X-rated celeb deepfakes using Redmond's OpenAI-powered cloud – claim Microsoft has named four of the ten people it is suing for allegedly snatching Azure cloud credentials and developing tools to bypass safety guardrails in its generative AI services – ultimately to generate deepfake smut videos of celebrities and others.... [...]
