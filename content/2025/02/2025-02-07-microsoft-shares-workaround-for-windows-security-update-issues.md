Title: Microsoft shares workaround for Windows security update issues
Date: 2025-02-07T08:53:25-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2025-02-07-microsoft-shares-workaround-for-windows-security-update-issues

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-shares-workaround-for-windows-security-update-issues/){:target="_blank" rel="noopener"}

> Microsoft has shared a workaround for users affected by a known issue that blocks Windows security updates from deploying on some Windows 11 24H2 systems. [...]
