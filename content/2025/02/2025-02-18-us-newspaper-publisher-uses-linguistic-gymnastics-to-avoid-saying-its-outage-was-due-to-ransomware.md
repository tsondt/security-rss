Title: US newspaper publisher uses linguistic gymnastics to avoid saying its outage was due to ransomware
Date: 2025-02-18T17:00:16+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-18-us-newspaper-publisher-uses-linguistic-gymnastics-to-avoid-saying-its-outage-was-due-to-ransomware

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/18/us_newspaper_publisher_exercises_linguistic/){:target="_blank" rel="noopener"}

> Called it an 'incident' in SEC filing, but encrypted apps and data exfiltration suggest Lee just can’t say the R word US newspaper publisher Lee Enterprises is blaming its recent service disruptions on a "cybersecurity attack," per a regulatory filing, and is the latest company to avoid using the dreaded R word.... [...]
