Title: What is device code phishing, and why are Russian spies so successful at it?
Date: 2025-02-14T21:16:11+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;account takeovers;device code authorization;phishing;russia
Slug: 2025-02-14-what-is-device-code-phishing-and-why-are-russian-spies-so-successful-at-it

[Source](https://arstechnica.com/information-technology/2025/02/russian-spies-use-device-code-phishing-to-hijack-microsoft-accounts/){:target="_blank" rel="noopener"}

> Researchers have uncovered a sustained and ongoing campaign by Russian spies that uses a clever phishing technique to hijack Microsoft 365 accounts belonging to a wide range of targets, researchers warned. The technique is known as device code phishing. It exploits “device code flow,” a form of authentication formalized in the industry-wide OAuth standard. Authentication through device code flow is designed for logging printers, smart TVs, and similar devices into accounts. These devices typically don’t support browsers, making it difficult to sign in using more standard forms of authentication, such as entering user names, passwords, and two-factor mechanisms. Rather than [...]
