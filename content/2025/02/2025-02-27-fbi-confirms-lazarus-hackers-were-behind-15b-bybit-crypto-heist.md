Title: FBI confirms Lazarus hackers were behind $1.5B Bybit crypto heist
Date: 2025-02-27T02:22:10-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2025-02-27-fbi-confirms-lazarus-hackers-were-behind-15b-bybit-crypto-heist

[Source](https://www.bleepingcomputer.com/news/security/fbi-confirms-lazarus-hackers-were-behind-15b-bybit-crypto-heist/){:target="_blank" rel="noopener"}

> FBI has confirmed that North Korean hackers stole $1.5 billion from cryptocurrency exchange Bybit on Friday in the largest crypto heist recorded until now. [...]
