Title: Australian fertility services giant Genea hit by security breach
Date: 2025-02-19T12:40:32-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Healthcare
Slug: 2025-02-19-australian-fertility-services-giant-genea-hit-by-security-breach

[Source](https://www.bleepingcomputer.com/news/security/australian-fertility-services-giant-genea-hit-by-security-breach/){:target="_blank" rel="noopener"}

> ​Genea, one of Australia's largest fertility services providers, disclosed that unknown attackers breached its network and accessed data stored on compromised systems. [...]
