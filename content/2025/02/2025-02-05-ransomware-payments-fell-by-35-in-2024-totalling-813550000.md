Title: Ransomware payments fell by 35% in 2024, totalling $813,550,000
Date: 2025-02-05T15:34:56-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2025-02-05-ransomware-payments-fell-by-35-in-2024-totalling-813550000

[Source](https://www.bleepingcomputer.com/news/security/ransomware-payments-fell-by-35-percent-in-2024-totalling-813-550-000/){:target="_blank" rel="noopener"}

> Payments to ransomware actors decreased 35% year-over-year in 2024, totaling $813.55 million, down from $1.25 billion recorded in 2023. [...]
