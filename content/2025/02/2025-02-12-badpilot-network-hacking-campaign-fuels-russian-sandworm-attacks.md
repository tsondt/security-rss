Title: BadPilot network hacking campaign fuels Russian SandWorm attacks
Date: 2025-02-12T12:00:00-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-12-badpilot-network-hacking-campaign-fuels-russian-sandworm-attacks

[Source](https://www.bleepingcomputer.com/news/security/badpilot-network-hacking-campaign-fuels-russian-sandworm-attacks/){:target="_blank" rel="noopener"}

> A subgroup of the Russian state-sponsored hacking group APT44, also known as 'Seashell Blizzard' and 'Sandworm', has been targeting critical organizations and governments in a multi-year campaign dubbed 'BadPilot.' [...]
