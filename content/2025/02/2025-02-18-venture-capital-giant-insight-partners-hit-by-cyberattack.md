Title: Venture capital giant Insight Partners hit by cyberattack
Date: 2025-02-18T15:33:43-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-18-venture-capital-giant-insight-partners-hit-by-cyberattack

[Source](https://www.bleepingcomputer.com/news/security/venture-capital-giant-insight-partners-hit-by-cyberattack/){:target="_blank" rel="noopener"}

> New York-based venture capital and private equity firm Insight Partners has disclosed that its systems were breached in January following a social engineering attack. [...]
