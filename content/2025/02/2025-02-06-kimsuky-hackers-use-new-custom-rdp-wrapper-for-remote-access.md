Title: Kimsuky hackers use new custom RDP Wrapper for remote access
Date: 2025-02-06T13:55:22-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-06-kimsuky-hackers-use-new-custom-rdp-wrapper-for-remote-access

[Source](https://www.bleepingcomputer.com/news/security/kimsuky-hackers-use-new-custom-rdp-wrapper-for-remote-access/){:target="_blank" rel="noopener"}

> The North Korean hacking group known as Kimsuky was observed in recent attacks using a custom-built RDP Wrapper and proxy tools to directly access infected machines. [...]
