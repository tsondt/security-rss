Title: Experts race to extract intel from Black Basta internal chat leaks
Date: 2025-02-21T12:56:05+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-21-experts-race-to-extract-intel-from-black-basta-internal-chat-leaks

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/21/experts_race_to_extract_intel/){:target="_blank" rel="noopener"}

> Researchers say there's dissent in the ranks. Plus: An AI tool lets you have a go yourself at analysing the data Hundreds of thousands of internal messages from the Black Basta ransomware gang were leaked by a Telegram user, prompting security researchers to bust out their best Russian translations post haste.... [...]
