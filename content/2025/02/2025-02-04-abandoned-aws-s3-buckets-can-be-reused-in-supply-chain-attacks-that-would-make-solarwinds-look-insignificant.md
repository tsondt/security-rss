Title: Abandoned AWS S3 buckets can be reused in supply-chain attacks that would make SolarWinds look 'insignificant'
Date: 2025-02-04T11:00:06+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-04-abandoned-aws-s3-buckets-can-be-reused-in-supply-chain-attacks-that-would-make-solarwinds-look-insignificant

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/04/abandoned_aws_s3/){:target="_blank" rel="noopener"}

> When cloud customers don't clean up after themselves, part 97 Abandoned AWS S3 buckets could be reused to hijack the global software supply chain in an attack that would make Russia's "SolarWinds adventures look amateurish and insignificant," watchTowr Labs security researchers have claimed.... [...]
