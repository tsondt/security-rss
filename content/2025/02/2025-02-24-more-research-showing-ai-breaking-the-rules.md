Title: More Research Showing AI Breaking the Rules
Date: 2025-02-24T12:08:56+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;AI;cheating;chess;games;LLM
Slug: 2025-02-24-more-research-showing-ai-breaking-the-rules

[Source](https://www.schneier.com/blog/archives/2025/02/more-research-showing-ai-breaking-the-rules.html){:target="_blank" rel="noopener"}

> These researchers had LLMs play chess against better opponents. When they couldn’t win, they sometimes resorted to cheating. Researchers gave the models a seemingly impossible task: to win against Stockfish, which is one of the strongest chess engines in the world and a much better player than any human, or any of the AI models in the study. Researchers also gave the models what they call a “scratchpad:” a text box the AI could use to “think” before making its next move, providing researchers with a window into their reasoning. In one case, o1-preview found itself in a losing position. [...]
