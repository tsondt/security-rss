Title: Massive brute force attack uses 2.8 million IPs to target VPN devices
Date: 2025-02-08T10:15:25-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-08-massive-brute-force-attack-uses-28-million-ips-to-target-vpn-devices

[Source](https://www.bleepingcomputer.com/news/security/massive-brute-force-attack-uses-28-million-ips-to-target-vpn-devices/){:target="_blank" rel="noopener"}

> A large-scale brute force password attack using almost 2.8 million IP addresses is underway, attempting to guess the credentials for a wide range of networking devices, including those from Palo Alto Networks, Ivanti, and SonicWall. [...]
