Title: GitVenom attacks abuse hundreds of GitHub repos to steal crypto
Date: 2025-02-25T14:45:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2025-02-25-gitvenom-attacks-abuse-hundreds-of-github-repos-to-steal-crypto

[Source](https://www.bleepingcomputer.com/news/security/gitvenom-attacks-abuse-hundreds-of-github-repos-to-steal-crypto/){:target="_blank" rel="noopener"}

> A malware campaign dubbed GitVenom uses hundreds of GitHub repositories to trick users into downloading info-stealers, remote access trojans (RATs), and clipboard hijackers to steal crypto and credentials. [...]
