Title: FreSSH bugs undiscovered for years threaten OpenSSH security
Date: 2025-02-18T15:30:06+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-18-fressh-bugs-undiscovered-for-years-threaten-openssh-security

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/18/openssh_vulnerabilities_mitm_dos/){:target="_blank" rel="noopener"}

> Exploit code now available for MitM and DoS attacks Researchers can disclose two brand-new vulnerabilities in OpenSSH now that patches have been released.... [...]
