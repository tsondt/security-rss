Title: Google says hackers abuse Gemini AI to empower their attacks
Date: 2025-02-01T12:14:35-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Artificial Intelligence;Google
Slug: 2025-02-01-google-says-hackers-abuse-gemini-ai-to-empower-their-attacks

[Source](https://www.bleepingcomputer.com/news/security/google-says-hackers-abuse-gemini-ai-to-empower-their-attacks/){:target="_blank" rel="noopener"}

> Multiple state-sponsored groups are experimenting with the AI-powered Gemini assistant from Google to increase productivity and to conduct research on potential infrastructure for attacks or for reconnaissance on targets. [...]
