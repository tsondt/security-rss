Title: Hacker pleads guilty to SIM swap attack on US SEC X account
Date: 2025-02-10T13:46:43-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2025-02-10-hacker-pleads-guilty-to-sim-swap-attack-on-us-sec-x-account

[Source](https://www.bleepingcomputer.com/news/security/hacker-pleads-guilty-to-sim-swap-attack-on-us-sec-x-account/){:target="_blank" rel="noopener"}

> Today, an Alabama man pleaded guilty to hijacking the U.S. Securities and Exchange Commission (SEC) account on X in a January 2024 SIM swapping attack. [...]
