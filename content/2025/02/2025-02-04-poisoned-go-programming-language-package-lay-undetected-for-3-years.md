Title: Poisoned Go programming language package lay undetected for 3 years
Date: 2025-02-04T17:28:11+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-04-poisoned-go-programming-language-package-lay-undetected-for-3-years

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/04/golang_supply_chain_attack/){:target="_blank" rel="noopener"}

> Researcher says ecosystem's auto-caching is a net positive but presents exploitable quirks A security researcher says a backdoor masquerading as a legitimate Go programming language package used by thousands of organizations was left undetected for years.... [...]
