Title: AMD fixes bug that lets hackers load malicious microcode patches
Date: 2025-02-05T13:30:05-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-05-amd-fixes-bug-that-lets-hackers-load-malicious-microcode-patches

[Source](https://www.bleepingcomputer.com/news/security/amd-fixes-bug-that-lets-hackers-load-malicious-microcode-patches/){:target="_blank" rel="noopener"}

> ​AMD has released mitigation and firmware updates to address a high-severity vulnerability that can be exploited to load malicious CPU microcode on unpatched devices. [...]
