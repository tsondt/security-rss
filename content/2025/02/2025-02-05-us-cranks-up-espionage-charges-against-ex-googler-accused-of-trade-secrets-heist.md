Title: US cranks up espionage charges against ex-Googler accused of trade secrets heist
Date: 2025-02-05T13:33:30+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-05-us-cranks-up-espionage-charges-against-ex-googler-accused-of-trade-secrets-heist

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/05/google_espionage_charges/){:target="_blank" rel="noopener"}

> Mountain View clocked onto the scheme with days to spare A Chinese national faces a substantial stint in prison and heavy fines if found guilty of several additional charges related to economic espionage and theft of trade secrets at Google.... [...]
