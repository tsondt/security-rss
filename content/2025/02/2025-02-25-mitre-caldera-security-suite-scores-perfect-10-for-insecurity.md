Title: MITRE Caldera security suite scores perfect 10 for insecurity
Date: 2025-02-25T20:47:03+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-25-mitre-caldera-security-suite-scores-perfect-10-for-insecurity

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/25/10_bug_mitre_caldera/){:target="_blank" rel="noopener"}

> Is a trivial remote-code execution hole in every version part of the training, or? The smart cookie who discovered a perfect 10-out-of-10-severity remote code execution (RCE) bug in MITRE's Caldera security training platform has urged users to "immediately pull down the latest version." As in, download it and install it.... [...]
