Title: Sophos sheds 6% of staff after swallowing Secureworks
Date: 2025-02-13T00:34:57+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-02-13-sophos-sheds-6-of-staff-after-swallowing-secureworks

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/13/sophos_secureworks_layoff/){:target="_blank" rel="noopener"}

> De-dupes some roles, hints others aren't needed as the infosec scene shifts Nine days after completing its $859 million acquisition of managed detection and response provider Secureworks, Sophos has laid off around six percent of its staff.... [...]
