Title: Leaked chat logs expose inner workings of secretive ransomware group
Date: 2025-02-21T21:47:32+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;black basta;chats;leaks;ransomware
Slug: 2025-02-21-leaked-chat-logs-expose-inner-workings-of-secretive-ransomware-group

[Source](https://arstechnica.com/security/2025/02/leaked-chat-logs-expose-inner-workings-of-secretive-ransomware-group/){:target="_blank" rel="noopener"}

> More than a year’s worth of internal communications from one of the world’s most active ransomware syndicates have been published online in a leak that exposes tactics, trade secrets, and internal rifts of its members. The communications come in the form of logs of more than 200,000 messages members of Black Basta sent to each other over the Matrix chat platform from September 2023 to September 2024, researchers said. The person who published the messages said the move was in retaliation for Black Basta targeting Russian banks. The leaker's identity is unknown; it’s also unclear if the person responsible was [...]
