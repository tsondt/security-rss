Title: Implementing least privilege access for Amazon Bedrock
Date: 2025-02-11T18:11:37+00:00
Author: Jonathan Jenkyn
Category: AWS Security
Tags: Amazon Bedrock;Best Practices;Expert (400);Security, Identity, & Compliance;Thought Leadership;Security Blog
Slug: 2025-02-11-implementing-least-privilege-access-for-amazon-bedrock

[Source](https://aws.amazon.com/blogs/security/implementing-least-privilege-access-for-amazon-bedrock/){:target="_blank" rel="noopener"}

> Generative AI applications often involve a combination of various services and features—such as Amazon Bedrock and large language models (LLMs)—to generate content and to access potentially confidential data. This combination requires strong identity and access management controls and is special in the sense that those controls need to be applied on various levels. In this [...]
