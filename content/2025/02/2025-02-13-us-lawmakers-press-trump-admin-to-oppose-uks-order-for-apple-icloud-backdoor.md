Title: US lawmakers press Trump admin to oppose UK's order for Apple iCloud backdoor
Date: 2025-02-13T16:58:04+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2025-02-13-us-lawmakers-press-trump-admin-to-oppose-uks-order-for-apple-icloud-backdoor

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/13/us_demand_uk_apple_backdoor_close/){:target="_blank" rel="noopener"}

> Senator, Congressman tell DNI to threaten infosec agreements if Blighty won't back down US lawmakers want newly confirmed Director of National Intelligence Tulsi Gabbard to back up her tough talk on backdoors. They're urging her to push back on the UK government's reported order for Apple to weaken iCloud security for government access.... [...]
