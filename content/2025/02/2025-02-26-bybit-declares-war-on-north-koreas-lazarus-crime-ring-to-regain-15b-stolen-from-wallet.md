Title: Bybit declares war on North Korea's Lazarus crime-ring to regain $1.5B stolen from wallet
Date: 2025-02-26T23:08:13+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-02-26-bybit-declares-war-on-north-koreas-lazarus-crime-ring-to-regain-15b-stolen-from-wallet

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/26/bybit_lazarus_bounty/){:target="_blank" rel="noopener"}

> Up to $140M in bounty rewards for return of Ethereum allegedly pilfered by hermit nation Cryptocurrency exchange Bybit, just days after suspected North Korean operatives stole $1.5 billion in Ethereum from it, has launched a bounty program to help recover its funds.... [...]
