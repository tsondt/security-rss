Title: US Army soldier linked to Snowflake extortion rampage admits breaking the law
Date: 2025-02-20T03:01:39+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-02-20-us-army-soldier-linked-to-snowflake-extortion-rampage-admits-breaking-the-law

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/20/us_army_snowflake_theft/){:target="_blank" rel="noopener"}

> That's the way the cookie melts A US Army soldier suspected of hacking AT&T and Verizon has admitted leaking online people's private call records.... [...]
