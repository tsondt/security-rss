Title: Botnet targets Basic Auth in Microsoft 365 password spray attacks
Date: 2025-02-24T12:49:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-24-botnet-targets-basic-auth-in-microsoft-365-password-spray-attacks

[Source](https://www.bleepingcomputer.com/news/security/botnet-targets-basic-auth-in-microsoft-365-password-spray-attacks/){:target="_blank" rel="noopener"}

> A massive botnet of over 130,000 compromised devices is conducting password-spray attacks against Microsoft 365 (M365) accounts worldwide, attempting to confirm credentials. [...]
