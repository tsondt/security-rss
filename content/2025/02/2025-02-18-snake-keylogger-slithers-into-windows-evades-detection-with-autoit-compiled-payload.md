Title: Snake Keylogger slithers into Windows, evades detection with AutoIt-compiled payload
Date: 2025-02-18T20:41:38+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-18-snake-keylogger-slithers-into-windows-evades-detection-with-autoit-compiled-payload

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/18/new_snake_keylogger_infects_windows/){:target="_blank" rel="noopener"}

> Because stealing your credentials, banking info, and IP just wasn’t enough A new variant of Snake Keylogger is making the rounds, primarily hitting Windows users across Asia and Europe. This strain also uses the BASIC-like scripting language AutoIt to deploy itself, adding an extra layer of obfuscation to help it slip past detection.... [...]
