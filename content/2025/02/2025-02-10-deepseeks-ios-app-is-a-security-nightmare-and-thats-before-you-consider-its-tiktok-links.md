Title: DeepSeek's iOS app is a security nightmare, and that's before you consider its TikTok links
Date: 2025-02-10T02:30:15+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2025-02-10-deepseeks-ios-app-is-a-security-nightmare-and-thats-before-you-consider-its-tiktok-links

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/10/infosec_in_brief/){:target="_blank" rel="noopener"}

> PLUS: Spanish cops think they've bagged NATO hacker; HPE warns staff of data breach; Lazy Facebook phishing, and more! Infosec In Brief DeepSeek’s iOS app is a security nightmare that you should delete ASAP, according to researchers at mobile app infosec platform vendor NowSecure.... [...]
