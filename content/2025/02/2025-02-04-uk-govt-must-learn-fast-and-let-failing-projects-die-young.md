Title: UK govt must learn fast and let failing projects die young
Date: 2025-02-04T09:30:08+00:00
Author: Lindsay Clark
Category: The Register
Tags: 
Slug: 2025-02-04-uk-govt-must-learn-fast-and-let-failing-projects-die-young

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/04/ukgov_must_embrace_a_fastlearning/){:target="_blank" rel="noopener"}

> Tackle longstanding issues around productivity, cyber resilience and public sector culture, advises spending watchdog The UK's government spending watchdog has called on the current administration to make better use of technology to kickstart the misfiring economy and ensure better delivery public services amid tightened budgets.... [...]
