Title: Police arrests 4 Phobos ransomware suspects, seizes 8Base sites
Date: 2025-02-10T11:51:03-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;Security
Slug: 2025-02-10-police-arrests-4-phobos-ransomware-suspects-seizes-8base-sites

[Source](https://www.bleepingcomputer.com/news/legal/police-arrests-4-phobos-ransomware-suspects-seizes-8base-sites/){:target="_blank" rel="noopener"}

> A global law enforcement operation targeting the Phobos ransomware gang has led to the arrest of four suspected hackers in Phuket, Thailand, and the seizure of 8Base's dark web sites. The suspects are accused of conducting cyberattacks on over 1,000 victims worldwide. [...]
