Title: Implement effective data authorization mechanisms to secure your data used in generative AI applications – part 2
Date: 2025-02-03T17:03:53+00:00
Author: Riggs Goodman III
Category: AWS Security
Tags: Advanced (300);Amazon Bedrock;Best Practices;Generative AI;Security, Identity, & Compliance;Thought Leadership;Security Blog
Slug: 2025-02-03-implement-effective-data-authorization-mechanisms-to-secure-your-data-used-in-generative-ai-applications-part-2

[Source](https://aws.amazon.com/blogs/security/implement-effective-data-authorization-mechanisms-to-secure-your-data-used-in-generative-ai-applications-part-2/){:target="_blank" rel="noopener"}

> In part 1 of this blog series, we walked through the risks associated with using sensitive data as part of your generative AI application. This overview provided a baseline of the challenges of using sensitive data with a non-deterministic large language model (LLM) and how to mitigate these challenges with Amazon Bedrock Agents. The next [...]
