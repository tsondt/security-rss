Title: Judge says US Treasury ‘more vulnerable to hacking’ since Trump let the DOGE out
Date: 2025-02-10T06:32:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2025-02-10-judge-says-us-treasury-more-vulnerable-to-hacking-since-trump-let-the-doge-out

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/10/doge_infosec_impact_court_order/){:target="_blank" rel="noopener"}

> Order requires destruction of departmental data accessed by Musky men Trump administration policies that allowed Elon Musk's Department of Government Efficiency to access systems and data at the Bureau of the Fiscal Service (BFS) have left the org “more vulnerable to hacking” according to federal Judge Paul A. Engelmayer in New York City.... [...]
