Title: More victims of China's Salt Typhoon crew emerge: Telcos just now hit via Cisco bugs
Date: 2025-02-13T18:34:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-13-more-victims-of-chinas-salt-typhoon-crew-emerge-telcos-just-now-hit-via-cisco-bugs

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/13/salt_typhoon_pwned_7_more/){:target="_blank" rel="noopener"}

> Networks in US and beyond compromised by Beijing's super-snoops pulling off priv-esc attacks China's Salt Typhoon spy crew exploited vulnerabilities in Cisco devices to compromise at least seven devices linked to global telecom providers and other orgs, in addition to its previous victim count.... [...]
