Title: Medusa ransomware gang demands $2M from UK private health services provider
Date: 2025-02-20T07:34:12+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-02-20-medusa-ransomware-gang-demands-2m-from-uk-private-health-services-provider

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/20/medusa_hcrg_ransomware/){:target="_blank" rel="noopener"}

> 2.3 TB held to ransom as biz formerly known as Virgin Care tells us it's probing IT 'security incident' Exclusive HCRG Care Group, a private health and social services provider, has seemingly fallen victim to the Medusa ransomware gang, which is threatening to leak what's claimed to be stolen internal records unless a substantial ransom is paid.... [...]
