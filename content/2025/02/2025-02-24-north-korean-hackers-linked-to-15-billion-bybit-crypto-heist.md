Title: North Korean hackers linked to $1.5 billion ByBit crypto heist
Date: 2025-02-24T11:22:58-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2025-02-24-north-korean-hackers-linked-to-15-billion-bybit-crypto-heist

[Source](https://www.bleepingcomputer.com/news/security/north-korean-hackers-linked-to-15-billion-bybit-crypto-heist/){:target="_blank" rel="noopener"}

> ​Over the weekend, blockchain security companies and experts have linked North Korea's Lazarus hacking group to the theft of over $1.5 billion from cryptocurrency exchange Bybit. [...]
