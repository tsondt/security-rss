Title: How to restrict Amazon S3 bucket access to a specific IAM role
Date: 2025-02-14T21:18:59+00:00
Author: Chris Craig
Category: AWS Security
Tags: Amazon Simple Storage Service (S3);AWS Identity and Access Management (IAM);How-To;Identity;Top Posts;Best of;IAM roles;NotPrincipal element;Security Blog
Slug: 2025-02-14-how-to-restrict-amazon-s3-bucket-access-to-a-specific-iam-role

[Source](https://aws.amazon.com/blogs/security/how-to-restrict-amazon-s3-bucket-access-to-a-specific-iam-role/){:target="_blank" rel="noopener"}

> February 14, 2025: This post was updated with the recommendation to restrict S3 bucket access to an IAM role by using the aws:PrincipalArn condition key instead of the aws:userid condition key. April 2, 2021: In the section “Granting cross-account bucket access to a specific IAM role,” we updated the second policy to fix an error. [...]
