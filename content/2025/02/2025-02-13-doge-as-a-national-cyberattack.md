Title: DOGE as a National Cyberattack
Date: 2025-02-13T12:03:26+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;breaches;cybersecurity;hacking;national security policy
Slug: 2025-02-13-doge-as-a-national-cyberattack

[Source](https://www.schneier.com/blog/archives/2025/02/doge-as-a-national.html){:target="_blank" rel="noopener"}

> In the span of just weeks, the US government has experienced what may be the most consequential security breach in its history—not through a sophisticated cyberattack or an act of foreign espionage, but through official orders by a billionaire with a poorly defined government role. And the implications for national security are profound. First, it was reported that people associated with the newly created Department of Government Efficiency (DOGE) had accessed the US Treasury computer system, giving them the ability to collect data on and potentially control the department’s roughly $5.45 trillion in annual federal payments. Then, we learned that [...]
