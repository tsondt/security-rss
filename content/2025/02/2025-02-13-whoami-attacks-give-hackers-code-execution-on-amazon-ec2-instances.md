Title: whoAMI attacks give hackers code execution on Amazon EC2 instances
Date: 2025-02-13T18:35:26-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud
Slug: 2025-02-13-whoami-attacks-give-hackers-code-execution-on-amazon-ec2-instances

[Source](https://www.bleepingcomputer.com/news/security/whoami-attacks-give-hackers-code-execution-on-amazon-ec2-instances/){:target="_blank" rel="noopener"}

> Security researchers discovered a name confusion attack that allows access to an Amazon Web Services account to anyone that publishes an Amazon Machine Image (AMI) with a specific name. [...]
