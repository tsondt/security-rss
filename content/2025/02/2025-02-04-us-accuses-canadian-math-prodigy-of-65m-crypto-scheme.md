Title: US accuses Canadian math prodigy of $65M crypto scheme
Date: 2025-02-04T14:45:14+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2025-02-04-us-accuses-canadian-math-prodigy-of-65m-crypto-scheme

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/04/math_prodigy_crypto_scheme/){:target="_blank" rel="noopener"}

> Suspect, still at large, said to back concept that 'code is law' New York feds today unsealed a five-count criminal indictment charging a 22-year-old Canadian math prodigy with exploiting vulnerabilities in two decentralized finance protocols, allegedly using them to fraudulently siphon around $65 million from investors in the platforms.... [...]
