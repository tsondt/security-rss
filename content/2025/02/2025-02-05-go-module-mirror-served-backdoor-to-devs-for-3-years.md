Title: Go Module Mirror served backdoor to devs for 3+ years
Date: 2025-02-05T12:25:55+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;go;open source;repositories;supply chain attack
Slug: 2025-02-05-go-module-mirror-served-backdoor-to-devs-for-3-years

[Source](https://arstechnica.com/security/2025/02/backdoored-package-in-go-mirror-site-went-unnoticed-for-3-years/){:target="_blank" rel="noopener"}

> A mirror proxy Google runs on behalf of developers of the Go programming language pushed a backdoored package for more than three years until Monday, after researchers who spotted the malicious code petitioned for it to be taken down twice. The service, known as the Go Module Mirror, caches open source packages available on GitHub and elsewhere so that downloads are faster and to ensure they are compatible with the rest of the Go ecosystem. By default, when someone uses command-line tools built into Go to download or install packages, requests are routed through the service. A description on the [...]
