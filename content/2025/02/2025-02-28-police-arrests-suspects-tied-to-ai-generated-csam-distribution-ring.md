Title: Police arrests suspects tied to AI-generated CSAM distribution ring
Date: 2025-02-28T12:59:06-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-28-police-arrests-suspects-tied-to-ai-generated-csam-distribution-ring

[Source](https://www.bleepingcomputer.com/news/security/police-arrests-suspects-linked-to-ai-generated-csam-distribution-ring/){:target="_blank" rel="noopener"}

> Law enforcement agencies from 19 countries have arrested 25 suspects linked to a criminal ring that was distributing child sexual abuse material (CSAM) generated using artificial intelligence (AI). [...]
