Title: Support Canada’s CCCS PBHVA overlay compliance with the Landing Zone Accelerator on AWS
Date: 2025-02-24T21:33:54+00:00
Author: Naranjan Goklani
Category: AWS Security
Tags: Announcements;Foundational (100);Public Sector;Security, Identity, & Compliance;AWS Artifact;AWS Canada (Central) Region;AWS Shared Responsibility Model;Canada;CCCS;CCCS Assessment;classification;Cloud security;Compliance;Cyber Security;cybersecurity;Federal;Government;Government of Canada (GC);ITSG-33;Medium Cloud Security Profile;PBMM;Protected B;Security;Security architecture;Security Blog
Slug: 2025-02-24-support-canadas-cccs-pbhva-overlay-compliance-with-the-landing-zone-accelerator-on-aws

[Source](https://aws.amazon.com/blogs/security/support-canadas-cccs-pbhva-overlay-compliance-with-the-landing-zone-accelerator-on-aws/){:target="_blank" rel="noopener"}

> Organizations seeking to adhere to the Canadian Centre for Cyber Security (CCCS) Protected B High Value Assets (PBHVA) overlay requirements can use the Landing Zone Accelerator (LZA) on AWS solution with the CCCS Medium configuration to accelerate their compliance journey. To further support customers, AWS recently collaborated with Coalfire to assess and verify the LZA [...]
