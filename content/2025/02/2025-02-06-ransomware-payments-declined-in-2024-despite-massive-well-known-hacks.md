Title: Ransomware payments declined in 2024 despite massive. well-known hacks
Date: 2025-02-06T14:21:08+00:00
Author: Lily Hay Newman, wired.com
Category: Ars Technica
Tags: Biz & IT;Security;hacking;ransomware;syndication
Slug: 2025-02-06-ransomware-payments-declined-in-2024-despite-massive-well-known-hacks

[Source](https://arstechnica.com/security/2025/02/ransomware-payments-declined-in-2024-despite-well-known-massive-hacks/){:target="_blank" rel="noopener"}

> For much of the past year, the trail of destruction and mayhem left behind by ransomware hackers was on full display. Digital extortion gangs paralyzed hundreds of US pharmacies and clinics through their attack on Change Healthcare, exploited security vulnerabilities in the customer accounts of cloud provider Snowflake to breach a string of high-profile targets, and extracted a record $75 million from a single victim. Yet beneath those headlines, the numbers tell a surprising story: Ransomware payments actually fell overall in 2024—and in the second half of the year dropped more precipitously than in any six-month period on record. Cryptocurrency [...]
