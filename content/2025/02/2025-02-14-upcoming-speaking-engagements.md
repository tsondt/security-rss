Title: Upcoming Speaking Engagements
Date: 2025-02-14T17:01:21+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Schneier news
Slug: 2025-02-14-upcoming-speaking-engagements

[Source](https://www.schneier.com/blog/archives/2025/02/upcoming-speaking-engagements-43.html){:target="_blank" rel="noopener"}

> This is a current list of where and when I am scheduled to speak: I’m speaking at Boskone 62 in Boston, Massachusetts, USA, which runs from February 14-16, 2025. My talk is at 4:00 PM ET on the 15th. I’m speaking at the Rossfest Symposium in Cambridge, UK, on March 25, 2025. The list is maintained on this page. [...]
