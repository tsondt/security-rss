Title: British engineering firm IMI discloses breach, shares no details
Date: 2025-02-06T09:36:11-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-06-british-engineering-firm-imi-discloses-breach-shares-no-details

[Source](https://www.bleepingcomputer.com/news/security/british-engineering-firm-imi-discloses-breach-shares-no-details/){:target="_blank" rel="noopener"}

> British-based engineering firm IMI plc has disclosed a security breach after unknown attackers hacked into the company's systems. [...]
