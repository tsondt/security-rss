Title: Spain arrests suspected hacker of US and Spanish military agencies
Date: 2025-02-05T10:37:41-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Legal;Security
Slug: 2025-02-05-spain-arrests-suspected-hacker-of-us-and-spanish-military-agencies

[Source](https://www.bleepingcomputer.com/news/legal/spain-arrests-suspected-hacker-of-us-and-spanish-military-agencies/){:target="_blank" rel="noopener"}

> The Spanish police have arrested a suspected hacker in Alicante for allegedly conducting 40 cyberattacks targeting critical public and private organizations, including the Guardia Civil, the Ministry of Defense, NATO, the US Army, and various universities. [...]
