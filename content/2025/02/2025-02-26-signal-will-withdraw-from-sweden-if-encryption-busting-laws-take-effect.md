Title: Signal will withdraw from Sweden if encryption-busting laws take effect
Date: 2025-02-26T12:30:14+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-26-signal-will-withdraw-from-sweden-if-encryption-busting-laws-take-effect

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/26/signal_will_withdraw_from_sweden/){:target="_blank" rel="noopener"}

> Experts warned the UK’s recent 'victory' over Apple would kickstart something of a domino effect Signal CEO Meredith Whittaker says her company will withdraw from countries that force messaging providers to allow law enforcement officials to access encrypted user data, as Sweden continues to mull such plans.... [...]
