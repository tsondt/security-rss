Title: Russia's Sandworm caught snarfing credentials, data from American and Brit orgs
Date: 2025-02-12T17:00:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-12-russias-sandworm-caught-snarfing-credentials-data-from-american-and-brit-orgs

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/12/russias_sandworm_caught_stealing_credentials/){:target="_blank" rel="noopener"}

> 'Near-global' initial access campaign active since 2021 An initial-access subgroup of Russia's Sandworm last year wriggled its way into networks within the US, UK, Canada and Australia, stealing credentials and data from "a limited number of organizations," according to Microsoft.... [...]
