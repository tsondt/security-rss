Title: Linux royalty backs adoption of Rust for kernel code, says its rise is inevitable
Date: 2025-02-21T00:38:46+00:00
Author: Thomas Claburn
Category: The Register
Tags: 
Slug: 2025-02-21-linux-royalty-backs-adoption-of-rust-for-kernel-code-says-its-rise-is-inevitable

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/21/linux_c_rust_debate_continues/){:target="_blank" rel="noopener"}

> Nobody wants memory bugs. Penguinistas continue debate on how to squish 'em Some Linux kernel maintainers remain unconvinced that adding Rust code to the open source project is a good idea, but its VIPs are coming out in support of the language's integration.... [...]
