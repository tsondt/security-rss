Title: Critical Cisco ISE bug can let attackers run commands as root
Date: 2025-02-06T11:40:19-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-06-critical-cisco-ise-bug-can-let-attackers-run-commands-as-root

[Source](https://www.bleepingcomputer.com/news/security/critical-cisco-ise-bug-can-let-attackers-run-commands-as-root/){:target="_blank" rel="noopener"}

> Cisco has fixed two critical Identity Services Engine (ISE) vulnerabilities that can let attackers with read-only admin privileges bypass authorization and run commands as root. [...]
