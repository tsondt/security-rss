Title: Ivanti endpoint manager can become endpoint ravager, thanks to quartet of critical flaws
Date: 2025-02-21T06:51:08+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-21-ivanti-endpoint-manager-can-become-endpoint-ravager-thanks-to-quartet-of-critical-flaws

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/21/ivanti_traversal_flaw_poc_exploit/){:target="_blank" rel="noopener"}

> PoC exploit code shows why this is a patch priority Security engineers have released a proof-of-concept exploit for four critical Ivanti Endpoint Manager bugs, giving those who haven't already installed patches released in January extra incentive to revisit their to-do lists.... [...]
