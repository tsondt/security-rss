Title: UK, US, Oz blast holes in LockBit's bulletproof hosting provider Zservers
Date: 2025-02-11T18:26:07+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-11-uk-us-oz-blast-holes-in-lockbits-bulletproof-hosting-provider-zservers

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/11/aukus_zservers_lockbit_sanctions/){:target="_blank" rel="noopener"}

> Huge if true: Brit Foreign Sec says Putin running a 'corrupt mafia state' One of the bulletproof hosting (BPH) providers used by the LockBit ransomware operation has been hit with sanctions in the US, UK, and Australia (AUKUS), along with six of its key allies.... [...]
