Title: DOGE latest: Citrix supremo has 'read-only' access to US Treasury payment system
Date: 2025-02-05T19:30:14+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-05-doge-latest-citrix-supremo-has-read-only-access-to-us-treasury-payment-system

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/05/tom_krause_treasury_read_only_access/){:target="_blank" rel="noopener"}

> CEO of Cloud Software a 'special government employee' probing for Team Elon The US Treasury has revealed Tom Krause – the chief exec of Citrix and Netscaler owner Cloud Software Group – has "read-only" access to a vital federal government payment system that disburses trillions of dollars annually.... [...]
