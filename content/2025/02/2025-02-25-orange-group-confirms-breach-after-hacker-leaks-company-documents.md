Title: Orange Group confirms breach after hacker leaks company documents
Date: 2025-02-25T06:05:49-05:00
Author: Ionut Ilascu
Category: BleepingComputer
Tags: Security
Slug: 2025-02-25-orange-group-confirms-breach-after-hacker-leaks-company-documents

[Source](https://www.bleepingcomputer.com/news/security/orange-group-confirms-breach-after-hacker-leaks-company-documents/){:target="_blank" rel="noopener"}

> A hacker claims to have stolen thousands of internal documents with user records and employee data after breaching the systems of Orange Group, a leading French telecommunications operator and digital service provider. [...]
