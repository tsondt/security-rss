Title: Apiiro unveils free scanner to detect malicious code merges
Date: 2025-02-20T16:04:40-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Software
Slug: 2025-02-20-apiiro-unveils-free-scanner-to-detect-malicious-code-merges

[Source](https://www.bleepingcomputer.com/news/security/apiiro-unveils-free-scanner-to-detect-malicious-code-merges/){:target="_blank" rel="noopener"}

> Security researchers at Apiiro have released two free, open-source tools designed to detect and block malicious code before they are added to software projects to curb supply chain attacks. [...]
