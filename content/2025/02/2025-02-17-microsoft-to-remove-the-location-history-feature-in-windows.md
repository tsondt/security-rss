Title: Microsoft to remove the Location History feature in Windows
Date: 2025-02-17T13:50:37-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2025-02-17-microsoft-to-remove-the-location-history-feature-in-windows

[Source](https://www.bleepingcomputer.com/news/security/microsoft-to-remove-the-location-history-feature-in-windows/){:target="_blank" rel="noopener"}

> Microsoft announced the deprecation of the Location History feature from Windows, which let applications like the Cortana virtual assistant to fetch location history of the device. [...]
