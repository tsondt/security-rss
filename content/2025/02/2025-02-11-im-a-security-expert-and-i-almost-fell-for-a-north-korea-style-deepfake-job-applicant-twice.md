Title: I'm a security expert, and I almost fell for a North Korea-style deepfake job applicant …Twice
Date: 2025-02-11T14:01:12+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-11-im-a-security-expert-and-i-almost-fell-for-a-north-korea-style-deepfake-job-applicant-twice

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/11/it_worker_scam/){:target="_blank" rel="noopener"}

> Remote position, webcam not working, then glitchy AI face... Red alert! Twice, over the past two months, Dawid Moczadło has interviewed purported job seekers only to discover that these "software developers" were scammers using AI-based tools — likely to get hired at a security company also using artificial intelligence, and then steal source code or other sensitive IP.... [...]
