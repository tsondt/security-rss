Title: Google Cloud introduces quantum-safe digital signatures in KMS
Date: 2025-02-23T10:09:19-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Cloud;Google
Slug: 2025-02-23-google-cloud-introduces-quantum-safe-digital-signatures-in-kms

[Source](https://www.bleepingcomputer.com/news/security/google-cloud-introduces-quantum-safe-digital-signatures-in-kms/){:target="_blank" rel="noopener"}

> Google Cloud has introduced quantum-safe digital signatures to its Cloud Key Management Service (Cloud KMS), making them available in preview. [...]
