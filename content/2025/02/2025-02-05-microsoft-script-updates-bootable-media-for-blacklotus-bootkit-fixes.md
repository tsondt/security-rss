Title: Microsoft script updates bootable media for BlackLotus bootkit fixes
Date: 2025-02-05T18:16:04-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Microsoft;Security
Slug: 2025-02-05-microsoft-script-updates-bootable-media-for-blacklotus-bootkit-fixes

[Source](https://www.bleepingcomputer.com/news/microsoft/microsoft-script-updates-bootable-media-for-blacklotus-bootkit-fixes/){:target="_blank" rel="noopener"}

> Microsoft has released a PowerShell script to help Windows users and admins update bootable media so it utilizes the new "Windows UEFI CA 2023" certificate before the mitigations of the BlackLotus UEFI bootkit are enforced later this year. [...]
