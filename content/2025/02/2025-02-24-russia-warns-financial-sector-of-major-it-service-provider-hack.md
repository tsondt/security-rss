Title: Russia warns financial sector of major IT service provider hack
Date: 2025-02-24T15:48:51-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-24-russia-warns-financial-sector-of-major-it-service-provider-hack

[Source](https://www.bleepingcomputer.com/news/security/russia-warns-financial-sector-of-major-it-service-provider-hack/){:target="_blank" rel="noopener"}

> Russia's National Coordination Center for Computer Incidents (NKTsKI) is warning organizations in the country's credit and financial sector about a breach at LANIT, a major Russian IT service and software provider. [...]
