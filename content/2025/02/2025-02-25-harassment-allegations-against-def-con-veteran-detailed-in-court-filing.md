Title: Harassment allegations against DEF CON veteran detailed in court filing
Date: 2025-02-25T15:30:01+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-25-harassment-allegations-against-def-con-veteran-detailed-in-court-filing

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/25/def_con_harassment_allegations/){:target="_blank" rel="noopener"}

> More than a dozen women came forward with accusations Details about the harassment allegations leveled at DEF CON veteran Christopher Hadnagy have now been revealed after a motion for summary judgment was filed over the weekend.... [...]
