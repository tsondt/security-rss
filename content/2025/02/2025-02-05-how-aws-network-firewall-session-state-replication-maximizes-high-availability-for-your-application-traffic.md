Title: How AWS Network Firewall session state replication maximizes high availability for your application traffic
Date: 2025-02-05T17:01:59+00:00
Author: Tushar Jagdale
Category: AWS Security
Tags: Advanced (300);Announcements;AWS Network Firewall;Best Practices;Security, Identity, & Compliance;Security Blog
Slug: 2025-02-05-how-aws-network-firewall-session-state-replication-maximizes-high-availability-for-your-application-traffic

[Source](https://aws.amazon.com/blogs/security/how-aws-network-firewall-session-state-replication-maximizes-high-availability-for-your-application-traffic/){:target="_blank" rel="noopener"}

> AWS Network Firewall is a managed, stateful network firewall and intrusion protection service that you can use to implement firewall rules for fine grained control over your network traffic. With Network Firewall, you can filter traffic at the perimeter of your virtual private cloud (VPC); including filtering traffic going to and coming from an internet [...]
