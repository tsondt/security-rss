Title: New hack uses prompt injection to corrupt Gemini’s long-term memory
Date: 2025-02-11T22:13:42+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: AI;Biz & IT;Google;Security;Artificial Intelligence;chatbots;hacking;large language models;LLMs;prompt injection
Slug: 2025-02-11-new-hack-uses-prompt-injection-to-corrupt-geminis-long-term-memory

[Source](https://arstechnica.com/security/2025/02/new-hack-uses-prompt-injection-to-corrupt-geminis-long-term-memory/){:target="_blank" rel="noopener"}

> In the nascent field of AI hacking, indirect prompt injection has become a basic building block for inducing chatbots to exfiltrate sensitive data or perform other malicious actions. Developers of platforms such as Google's Gemini and OpenAI's ChatGPT are generally good at plugging these security holes, but hackers keep finding new ways to poke through them again and again. On Monday, researcher Johann Rehberger demonstrated a new way to override prompt injection defenses Google developers have built into Gemini—specifically, defenses that restrict the invocation of Google Workspace or other sensitive tools when processing untrusted data, such as incoming emails or [...]
