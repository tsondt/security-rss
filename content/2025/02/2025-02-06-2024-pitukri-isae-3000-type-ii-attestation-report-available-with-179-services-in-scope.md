Title: 2024 PiTuKri ISAE 3000 Type II attestation report available with 179 services in scope
Date: 2025-02-06T23:09:14+00:00
Author: Tariro Dongo
Category: AWS Security
Tags: Announcements;Foundational (100);Security, Identity, & Compliance;AWS Compliance;AWS PiTuKri;AWS security;cybersecurity;Finland;Finnish Public Sector;PiTuKri Compliance;Security Blog
Slug: 2025-02-06-2024-pitukri-isae-3000-type-ii-attestation-report-available-with-179-services-in-scope

[Source](https://aws.amazon.com/blogs/security/2024-pitukri-isae-3000-type-ii-attestation-report-available-with-179-services-in-scope/){:target="_blank" rel="noopener"}

> Amazon Web Services (AWS) is pleased to announce the issuance of the Criteria to Assess the Information Security of Cloud Services (PiTuKri) Type II attestation report with 179 services in scope. The Finnish Transport and Communications Agency (Traficom) Cyber Security Centre published PiTuKri, which consists of 52 criteria that provide guidance across 11 domains for [...]
