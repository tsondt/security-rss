Title: Mysterious Palo Alto firewall reboots? You're not alone
Date: 2025-02-13T07:21:10+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-02-13-mysterious-palo-alto-firewall-reboots-youre-not-alone

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/13/palo_alto_firewall/){:target="_blank" rel="noopener"}

> Limited-edition hotfix to get wider release before end of month Administrators of Palo Alto Networks' firewalls have complained the equipment falls over unexpectedly, and while a fix has bee prepared, it's not yet generally available.... [...]
