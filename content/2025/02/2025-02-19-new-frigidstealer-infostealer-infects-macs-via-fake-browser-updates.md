Title: New FrigidStealer infostealer infects Macs via fake browser updates
Date: 2025-02-19T12:42:39-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Apple
Slug: 2025-02-19-new-frigidstealer-infostealer-infects-macs-via-fake-browser-updates

[Source](https://www.bleepingcomputer.com/news/security/new-frigidstealer-infostealer-infects-macs-via-fake-browser-updates/){:target="_blank" rel="noopener"}

> The FakeUpdate malware campaigns are increasingly becoming muddled, with two additional cybercrime groups tracked as TA2726 and TA2727, running campaigns that push a new macOS infostealer malware called FrigidStealer. [...]
