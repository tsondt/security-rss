Title: Google binning SMS MFA at last and replacing it with QR codes
Date: 2025-02-25T00:14:33+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-02-25-google-binning-sms-mfa-at-last-and-replacing-it-with-qr-codes

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/25/google_sms_qr/){:target="_blank" rel="noopener"}

> Everyone knew texted OTPs were a dud back in 2016 Google has confirmed it will phase out the use of SMS text messages for multi-factor authentication in favor of more secure technologies.... [...]
