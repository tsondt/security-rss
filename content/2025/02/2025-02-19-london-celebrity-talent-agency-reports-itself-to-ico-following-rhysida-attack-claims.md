Title: London celebrity talent agency reports itself to ICO following Rhysida attack claims
Date: 2025-02-19T09:30:09+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-19-london-celebrity-talent-agency-reports-itself-to-ico-following-rhysida-attack-claims

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/19/london_celebrity_talent_agency_reports/){:target="_blank" rel="noopener"}

> Showbiz members' passport scans already plastered online A London talent agency has reported itself to the UK's data protection watchdog after the Rhysida ransomware crew last week claimed it had attacked the business, which represents luminaries of stage and screen.... [...]
