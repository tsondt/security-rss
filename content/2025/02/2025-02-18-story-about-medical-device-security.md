Title: Story About Medical Device Security
Date: 2025-02-18T12:06:07+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;business of security;medicine
Slug: 2025-02-18-story-about-medical-device-security

[Source](https://www.schneier.com/blog/archives/2025/02/story-about-medical-device-security.html){:target="_blank" rel="noopener"}

> Ben Rothke relates a story about me working with a medical device firm back when I was with BT. I don’t remember the story at all, or who the company was. But it sounds about right. [...]
