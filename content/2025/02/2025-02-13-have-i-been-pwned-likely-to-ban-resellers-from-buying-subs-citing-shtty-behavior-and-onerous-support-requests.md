Title: Have I Been Pwned likely to ban resellers from buying subs, citing ‘sh*tty behavior’ and onerous support requests
Date: 2025-02-13T04:59:12+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2025-02-13-have-i-been-pwned-likely-to-ban-resellers-from-buying-subs-citing-shtty-behavior-and-onerous-support-requests

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/13/hibp_reseller_ban/){:target="_blank" rel="noopener"}

> ‘What are customers actually getting from resellers other than massive price markups?’ asks Troy Hunt Troy Hunt, proprietor of data breach lookup site Have I Been Pwned, is likely to ban resellers from the service.... [...]
