Title: Teen on Musk’s DOGE Team Graduated from ‘The Com’
Date: 2025-02-08T00:32:53+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Ne'er-Do-Well News;The Coming Storm;AS400495;BackConnect Security LLC;Curtis Gervais;DiamondCDN;Director of National Intelligence;DOGE;Dstat;Edward Coristine;Elon Musk;Eric Taylor;Marshal Webb;Neuralink;Packetware;Path Networks;President Trump;Rivage;Tesla Sexy LLC;The Com;Tucker Preston;wired
Slug: 2025-02-08-teen-on-musks-doge-team-graduated-from-the-com

[Source](https://krebsonsecurity.com/2025/02/teen-on-musks-doge-team-graduated-from-the-com/){:target="_blank" rel="noopener"}

> Wired reported this week that a 19-year-old working for Elon Musk ‘s so-called Department of Government Efficiency (DOGE) was given access to sensitive US government systems even though his past association with cybercrime communities should have precluded him from gaining the necessary security clearances to do so. As today’s story explores, the DOGE teen is a former denizen of ‘ The Com,’ an archipelago of Discord and Telegram chat channels that function as a kind of distributed cybercriminal social network for facilitating instant collaboration. Since President Trump’s second inauguration, Musk’s DOGE team has gained access to a truly staggering amount [...]
