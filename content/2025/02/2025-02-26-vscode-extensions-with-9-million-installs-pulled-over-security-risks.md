Title: VSCode extensions with 9 million installs pulled over security risks
Date: 2025-02-26T14:10:49-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-26-vscode-extensions-with-9-million-installs-pulled-over-security-risks

[Source](https://www.bleepingcomputer.com/news/security/vscode-extensions-with-9-million-installs-pulled-over-security-risks/){:target="_blank" rel="noopener"}

> Microsoft has removed two popular VSCode extensions, 'Material Theme - Free' and 'Material Theme Icons - Free,' from the Visual Studio Marketplace for allegedly containing malicious code. [...]
