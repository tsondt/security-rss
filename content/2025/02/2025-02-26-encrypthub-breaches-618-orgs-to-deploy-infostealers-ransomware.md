Title: EncryptHub breaches 618 orgs to deploy infostealers, ransomware
Date: 2025-02-26T10:31:21-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-26-encrypthub-breaches-618-orgs-to-deploy-infostealers-ransomware

[Source](https://www.bleepingcomputer.com/news/security/encrypthub-breaches-618-orgs-to-deploy-infostealers-ransomware/){:target="_blank" rel="noopener"}

> A threat actor tracked as 'EncryptHub,' aka Larva-208, has been targeting organizations worldwide with spear-phishing and social engineering attacks to gain access to corporate networks. [...]
