Title: North Korea targets crypto developers via NPM supply chain attack
Date: 2025-02-13T12:00:12+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-13-north-korea-targets-crypto-developers-via-npm-supply-chain-attack

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/13/north_korea_npm_crypto/){:target="_blank" rel="noopener"}

> Yet another cash grab from Kim's cronies and an intel update from Microsoft North Korea has changed tack: its latest campaign targets the NPM registry and owners of Exodus and Atomic cryptocurrency wallets.... [...]
