Title: Nearly 10 years after Data and Goliath, Bruce Schneier says: Privacy’s still screwed
Date: 2025-02-15T15:44:13+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-02-15-nearly-10-years-after-data-and-goliath-bruce-schneier-says-privacys-still-screwed

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/15/interview_bruce_schneier/){:target="_blank" rel="noopener"}

> 'In 50 years, I think we'll view these business practices like we view sweatshops today' Interview It has been nearly a decade since famed cryptographer and privacy expert Bruce Schneier released the book Data and Goliath: The Hidden Battles to Collect Your Data and Control Your World - an examination of how government agencies and tech giants exploit personal data. Today, his predictions feel eerily accurate.... [...]
