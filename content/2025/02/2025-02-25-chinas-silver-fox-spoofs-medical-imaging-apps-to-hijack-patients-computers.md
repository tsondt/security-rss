Title: China's Silver Fox spoofs medical imaging apps to hijack patients' computers
Date: 2025-02-25T13:15:07+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-25-chinas-silver-fox-spoofs-medical-imaging-apps-to-hijack-patients-computers

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/25/silver_fox_medical_app_backdoor/){:target="_blank" rel="noopener"}

> Sly like a PRC cyberattack A Chinese government-backed group is spoofing legitimate medical software to hijack hospital patients' computers, infecting them with backdoors, credential-swiping keyloggers, and cryptominers.... [...]
