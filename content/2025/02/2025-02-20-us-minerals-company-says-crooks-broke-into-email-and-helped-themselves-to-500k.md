Title: US minerals company says crooks broke into email and helped themselves to $500K
Date: 2025-02-20T16:44:51+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-20-us-minerals-company-says-crooks-broke-into-email-and-helped-themselves-to-500k

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/20/niocorp_bec_scam/){:target="_blank" rel="noopener"}

> A painful loss for young company that's yet to generate revenue A NASDAQ-listed US minerals company says cybercriminals broke into its systems on Valentine's Day and paid themselves around $500,000 – money earmarked for a vendor.... [...]
