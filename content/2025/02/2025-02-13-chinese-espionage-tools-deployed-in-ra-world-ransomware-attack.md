Title: Chinese espionage tools deployed in RA World ransomware attack
Date: 2025-02-13T09:31:54-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-13-chinese-espionage-tools-deployed-in-ra-world-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/chinese-espionage-tools-deployed-in-ra-world-ransomware-attack/){:target="_blank" rel="noopener"}

> A China-based threat actor, tracked as Emperor Dragonfly and commonly associated with cybercriminal endeavors, has been observed using in a ransomware attack a toolset previously attributed to espionage actors. [...]
