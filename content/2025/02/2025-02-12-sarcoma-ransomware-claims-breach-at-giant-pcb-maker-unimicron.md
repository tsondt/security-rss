Title: Sarcoma ransomware claims breach at giant PCB maker Unimicron
Date: 2025-02-12T14:24:28-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-12-sarcoma-ransomware-claims-breach-at-giant-pcb-maker-unimicron

[Source](https://www.bleepingcomputer.com/news/security/sarcoma-ransomware-claims-breach-at-giant-pcb-maker-unimicron/){:target="_blank" rel="noopener"}

> A relatively new ransomware operation named 'Sarcoma' has claimed responsibility for an attack against the Unimicron printed circuit boards (PCB) maker in Taiwan. [...]
