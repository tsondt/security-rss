Title: Fujitsu worries US tariffs will see its clients slow digital spend
Date: 2025-02-16T23:59:13+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2025-02-16-fujitsu-worries-us-tariffs-will-see-its-clients-slow-digital-spend

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/16/asia_tech_news_in_brief/){:target="_blank" rel="noopener"}

> PLUS: Pacific islands targeted by Chinese APT; China’s new rocket soars; DeepSeek puts Korea in a pickle; and more Asia In Brief The head of Fujitsu’s North American operations has warned that the Trump administration’s tariff plans will be bad for business.... [...]
