Title: Southern Water says Black Basta ransomware attack cost £4.5M in expenses
Date: 2025-02-26T18:50:29-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-26-southern-water-says-black-basta-ransomware-attack-cost-45m-in-expenses

[Source](https://www.bleepingcomputer.com/news/security/southern-water-says-black-basta-ransomware-attack-cost-45m-in-expenses/){:target="_blank" rel="noopener"}

> United Kingdom water supplier Southern Water has disclosed that it incurred costs of £4.5 million ($5.7M) due to a cyberattack it suffered in February 2024. [...]
