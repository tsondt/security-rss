Title: US health system notifies 882,000 patients of August 2023 breach
Date: 2025-02-07T11:44:32-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-07-us-health-system-notifies-882000-patients-of-august-2023-breach

[Source](https://www.bleepingcomputer.com/news/security/us-health-system-notifies-882-000-patients-of-august-2023-breach/){:target="_blank" rel="noopener"}

> Hospital Sisters Health System notified over 882,000 patients that an August 2023 cyberattack led to a data breach that exposed their personal and health information. [...]
