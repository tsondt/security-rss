Title: DDoS attacks reportedly behind DayZ and Arma network outages
Date: 2025-02-06T11:09:19-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Gaming
Slug: 2025-02-06-ddos-attacks-reportedly-behind-dayz-and-arma-network-outages

[Source](https://www.bleepingcomputer.com/news/security/ddos-attacks-reportedly-behind-dayz-and-arma-network-outages/){:target="_blank" rel="noopener"}

> An ongoing distributed denial of service (DDoS) attack targets Bohemia Interactive's infrastructure, preventing players of DayZ and Arma Reforger from playing the games online. [...]
