Title: Microsoft expands Copilot bug bounty targets, adds payouts for even moderate messes
Date: 2025-02-20T23:55:52+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-20-microsoft-expands-copilot-bug-bounty-targets-adds-payouts-for-even-moderate-messes

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/20/microsoft_copilot_bug_bounty_updated/){:target="_blank" rel="noopener"}

> Said bugs 'can have significant implications' – glad to hear that from Redmond Microsoft is so concerned about security in its Copilot products for folks that it’s lifted bug bounty payments for moderate-severity vulnerabilities from nothing to a maximum of $5,000, and expanded the range of vulnerabilities it will pay people to find and report.... [...]
