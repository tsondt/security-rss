Title: Announcing ASCP integration with Pod Identity: Enhanced security for secrets management in Amazon EKS
Date: 2025-02-10T20:32:49+00:00
Author: Rodrigo Bersa
Category: AWS Security
Tags: Advanced (300);AWS Secrets Manager;Security, Identity, & Compliance;Technical How-to;Security Blog
Slug: 2025-02-10-announcing-ascp-integration-with-pod-identity-enhanced-security-for-secrets-management-in-amazon-eks

[Source](https://aws.amazon.com/blogs/security/announcing-ascp-integration-with-pod-identity-enhanced-security-for-secrets-management-in-amazon-eks/){:target="_blank" rel="noopener"}

> In 2021, Amazon Web Services (AWS) introduced the AWS Secrets and Configuration Provider (ASCP) for the Kubernetes Secrets Store Container Storage Interface (CSI) Driver, offering a reliable way to manage secrets in Amazon Elastic Kubernetes Service (Amazon EKS). Today, we’re excited to announce the integration of ASCP with Pod Identity, the new standard for AWS [...]
