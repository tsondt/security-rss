Title: Pump.fun X account hacked to promote scam governance token
Date: 2025-02-26T11:07:52-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;CryptoCurrency
Slug: 2025-02-26-pumpfun-x-account-hacked-to-promote-scam-governance-token

[Source](https://www.bleepingcomputer.com/news/security/pumpfun-x-account-hacked-to-promote-scam-governance-token/){:target="_blank" rel="noopener"}

> The immensely popular memecoin generator Pump.fun had its X account hacked to promote a fake "PUMP" token cryptocurrency scam. [...]
