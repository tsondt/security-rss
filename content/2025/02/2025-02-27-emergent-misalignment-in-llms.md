Title: “Emergent Misalignment” in LLMs
Date: 2025-02-27T18:05:54+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;academic papers;AI;LLM
Slug: 2025-02-27-emergent-misalignment-in-llms

[Source](https://www.schneier.com/blog/archives/2025/02/emergent-misalignment-in-llms.html){:target="_blank" rel="noopener"}

> Interesting research: “ Emergent Misalignment: Narrow finetuning can produce broadly misaligned LLMs “: Abstract: We present a surprising result regarding LLMs and alignment. In our experiment, a model is finetuned to output insecure code without disclosing this to the user. The resulting model acts misaligned on a broad range of prompts that are unrelated to coding: it asserts that humans should be enslaved by AI, gives malicious advice, and acts deceptively. Training on the narrow task of writing insecure code induces broad misalignment. We call this emergent misalignment. This effect is observed in a range of models but is strongest [...]
