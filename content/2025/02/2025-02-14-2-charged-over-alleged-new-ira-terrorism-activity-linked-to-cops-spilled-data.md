Title: 2 charged over alleged New IRA terrorism activity linked to cops' spilled data
Date: 2025-02-14T12:12:16+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-14-2-charged-over-alleged-new-ira-terrorism-activity-linked-to-cops-spilled-data

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/14/two_charged_psni_data/){:target="_blank" rel="noopener"}

> Officer says mistakenly published police details were shared 'a considerable amount of times' Two suspected New IRA members were arrested on Tuesday and charged under the Terrorism Act 2000 after they were found in possession of spreadsheets containing details of staff that the Police Service of Northern Ireland (PSNI) mistakenly published online.... [...]
