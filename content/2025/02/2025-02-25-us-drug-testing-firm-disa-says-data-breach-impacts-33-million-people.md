Title: US drug testing firm DISA says data breach impacts 3.3 million people
Date: 2025-02-25T11:44:33-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-25-us-drug-testing-firm-disa-says-data-breach-impacts-33-million-people

[Source](https://www.bleepingcomputer.com/news/security/us-drug-testing-firm-disa-says-data-breach-impacts-33-million-people/){:target="_blank" rel="noopener"}

> DISA Global Solutions, a leading US background screening and drug and alcohol testing firm, has suffered a data breach impacting 3.3 million people. [...]
