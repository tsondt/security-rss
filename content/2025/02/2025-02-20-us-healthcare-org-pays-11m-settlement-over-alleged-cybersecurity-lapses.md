Title: US healthcare org pays $11M settlement over alleged cybersecurity lapses
Date: 2025-02-20T13:47:21-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2025-02-20-us-healthcare-org-pays-11m-settlement-over-alleged-cybersecurity-lapses

[Source](https://www.bleepingcomputer.com/news/security/us-healthcare-org-pays-11m-settlement-over-alleged-cybersecurity-lapses/){:target="_blank" rel="noopener"}

> Health Net Federal Services (HNFS) and its parent company, Centene Corporation, have agreed to pay $11,253,400 to settle allegations that HNFS falsely certified compliance with cybersecurity requirements under its Defense Health Agency (DHA) TRICARE contract. [...]
