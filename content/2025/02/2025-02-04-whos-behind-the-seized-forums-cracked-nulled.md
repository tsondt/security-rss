Title: Who’s Behind the Seized Forums ‘Cracked’ & ‘Nulled’?
Date: 2025-02-04T17:09:16+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;Breadcrumbs;1337 Services Gmbh;AS210558;Constella Intelligence;Cracked;domaintools;DreamDrive GmbH;Finn Alexander Grimpe;finn@shoppy.gg;finndev;floriaN;Florian Marzahl;HRB 164175;Intel 471;Lucas Sohn;Northdata.com;Nulled;olivia.messla@outlook.de;Operation Talent;Sellix;Shoppy Ecommerce Ltd;StarkRDP
Slug: 2025-02-04-whos-behind-the-seized-forums-cracked-nulled

[Source](https://krebsonsecurity.com/2025/02/whos-behind-the-seized-forums-cracked-nulled/){:target="_blank" rel="noopener"}

> The FBI joined authorities across Europe last week in seizing domain names for Cracked and Nulled, English-language cybercrime forums with millions of users that trafficked in stolen data, hacking tools and malware. An investigation into the history of these communities shows their apparent co-founders quite openly operate an Internet service provider and a pair of e-commerce platforms catering to buyers and sellers on both forums. In this 2019 post from Cracked, a forum moderator told the author of the post (Buddie) that the owner of the RDP service was the founder of Nulled, a.k.a. “Finndev.” Image: Ke-la.com. On Jan. 30, [...]
