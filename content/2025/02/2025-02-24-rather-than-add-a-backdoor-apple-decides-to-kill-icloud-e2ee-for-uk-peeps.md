Title: Rather than add a backdoor, Apple decides to kill iCloud E2EE for UK peeps
Date: 2025-02-24T03:31:05+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2025-02-24-rather-than-add-a-backdoor-apple-decides-to-kill-icloud-e2ee-for-uk-peeps

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/24/rather_than_add_a_backdoor/){:target="_blank" rel="noopener"}

> PLUS: SEC launches new crypto crime unit; Phishing toolkit upgraded; and more Infosec in brief Apple has responded to the UK government's demand for access to its customers’ data stored in iCloud by deciding to turn off its Advanced Data Protection (ADP) end-to-end encryption service for UK users.... [...]
