Title: India's banking on the bank.in domain cleaning up its financial services sector
Date: 2025-02-10T04:31:09+00:00
Author: Simon Sharwood
Category: The Register
Tags: 
Slug: 2025-02-10-indias-banking-on-the-bankin-domain-cleaning-up-its-financial-services-sector

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/10/india_bank_dotin_plan/){:target="_blank" rel="noopener"}

> With over 2,000 banks in operation, a domain only they can use has potential to make life harder for fraudsters India’s Reserve Bank last week announced a plan to use adopt dedicated second-level domains – bank.in and fin.in – in the hope it improves trust in the financial services sector.... [...]
