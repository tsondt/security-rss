Title: Dems want answers on national security risks posed by hiring freeze, DOGE probes
Date: 2025-02-06T18:30:08+00:00
Author: Brandon Vigliarolo
Category: The Register
Tags: 
Slug: 2025-02-06-dems-want-answers-on-national-security-risks-posed-by-hiring-freeze-doge-probes

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/06/democrat_trump_admin_letter/){:target="_blank" rel="noopener"}

> Are cybersecurity roles included? Are Elon's enforcers vetted? Inquiring minds want to know Updated Elected officials are demanding answers as to whether the Trump administration and Elon Musk's Department of Government Efficiency (DOGE) are hamstringing US national security.... [...]
