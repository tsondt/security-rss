Title: Robocallers who called the FCC pretending to be from the FCC land telco in trouble
Date: 2025-02-06T00:04:16+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-02-06-robocallers-who-called-the-fcc-pretending-to-be-from-the-fcc-land-telco-in-trouble

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/06/robocallers_fcc_telnyx/){:target="_blank" rel="noopener"}

> Don't laugh: The $4.5m fine proposed for carrier Telnyx shows how the Trump administration will run its comms regulator In its first enforcement action of the Trump presidency, the FCC has voted to propose fining Telnyx $4,492,500 – after scammers pretending to be the watchdog's staff started calling actual FCC staffers via the VoIP telco.... [...]
