Title: UK Is Ordering Apple to Break Its Own Encryption
Date: 2025-02-08T15:56:32+00:00
Author: Bruce Schneier
Category: Bruce Schneier
Tags: Uncategorized;Apple;backdoors;cloud computing;data privacy;encryption;law enforcement;privacy;UK
Slug: 2025-02-08-uk-is-ordering-apple-to-break-its-own-encryption

[Source](https://www.schneier.com/blog/archives/2025/02/uk-is-ordering-apple-to-break-its-own-encryption.html){:target="_blank" rel="noopener"}

> The Washington Post is reporting that the UK government has served Apple with a “technical capability notice” as defined by the 2016 Investigatory Powers Act, requiring it to break the Advanced Data Protection encryption in iCloud for the benefit of law enforcement. This is a big deal, and something we in the security community have worried was coming for a while now. The law, known by critics as the Snoopers’ Charter, makes it a criminal offense to reveal that the government has even made such a demand. An Apple spokesman declined to comment. Apple can appeal the U.K. capability notice [...]
