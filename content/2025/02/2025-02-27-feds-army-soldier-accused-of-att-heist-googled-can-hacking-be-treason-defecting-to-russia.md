Title: Feds: Army soldier accused of AT&amp;T heist Googled ‘can hacking be treason,’ ‘defecting to Russia’
Date: 2025-02-27T22:41:01+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-27-feds-army-soldier-accused-of-att-heist-googled-can-hacking-be-treason-defecting-to-russia

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/27/army_soldier_accused_of_att/){:target="_blank" rel="noopener"}

> FYI: What NOT to search after committing a crime The US Army soldier accused of compromising AT&T and bragging about getting his hands on President Trump's call logs allegedly tried to sell stolen information to a foreign intel agent.... [...]
