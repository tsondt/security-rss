Title: Chinese spies suspected of 'moonlighting' as tawdry ransomware crooks
Date: 2025-02-14T02:19:39+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-14-chinese-spies-suspected-of-moonlighting-as-tawdry-ransomware-crooks

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/14/chinese_spies_ransomware_moonlighting/){:target="_blank" rel="noopener"}

> Some employees steal sticky notes, others 'borrow' malicious code A crew identified as a Chinese government-backed espionage group appears to have started moonlighting as a ransomware player – further evidence that lines are blurring between nation-state cyberspies and financially motivated cybercriminals.... [...]
