Title: Cloudflare outage caused by botched blocking of phishing URL
Date: 2025-02-07T10:44:59-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-07-cloudflare-outage-caused-by-botched-blocking-of-phishing-url

[Source](https://www.bleepingcomputer.com/news/security/cloudflare-outage-caused-by-botched-blocking-of-phishing-url/){:target="_blank" rel="noopener"}

> An attempt to block a phishing URL in Cloudflare's R2 object storage platform backfired yesterday, triggering a widespread outage that brought down multiple services for nearly an hour. [...]
