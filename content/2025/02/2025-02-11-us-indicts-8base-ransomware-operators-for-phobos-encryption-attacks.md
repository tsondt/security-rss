Title: US indicts 8Base ransomware operators for Phobos encryption attacks
Date: 2025-02-11T10:42:16-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security;Legal
Slug: 2025-02-11-us-indicts-8base-ransomware-operators-for-phobos-encryption-attacks

[Source](https://www.bleepingcomputer.com/news/security/us-indicts-8base-ransomware-operators-for-phobos-encryption-attacks/){:target="_blank" rel="noopener"}

> The U.S. Justice Department announced the names of two Phobos ransomware affiliates arrested yesterday in Thailand, charging them on 11 counts due to their involvement in more than a thousand cyberattacks. [...]
