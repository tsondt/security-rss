Title: Cloud CISO Perspectives: Prepare early for PQC to be resilient against tomorrow’s cryptographic threats
Date: 2025-02-28T17:00:00+00:00
Author: Christiane Peters
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2025-02-28-cloud-ciso-perspectives-prepare-early-for-pqc-to-be-resilient-against-tomorrows-cryptographic-threats

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-prepare-early-for-PQC-resilient-cryptographic-threats/){:target="_blank" rel="noopener"}

> Welcome to the second Cloud CISO Perspectives for February 2025. Today, Christiane Peters from our Office of the CISO explains why post-quantum cryptography may seem like the future’s problem, but it will soon be ours if IT doesn’t move faster to prepare for it. Here’s what you need to know about how to get your post-quantum cryptography plans started. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI [...]
