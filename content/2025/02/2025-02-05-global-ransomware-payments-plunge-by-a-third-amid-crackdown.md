Title: Global ransomware payments plunge by a third amid crackdown
Date: 2025-02-05T13:00:25+00:00
Author: Dan Milmo Global technology editor
Category: The Guardian
Tags: Cybercrime;Malware;Technology;Internet;Data and computer security;Organised crime;Data protection
Slug: 2025-02-05-global-ransomware-payments-plunge-by-a-third-amid-crackdown

[Source](https://www.theguardian.com/technology/2025/feb/05/global-ransomware-payments-plunge-by-a-third-amid-crackdown){:target="_blank" rel="noopener"}

> Money stolen falls from record $1.25bn to $813m as more victims refuse to pay off criminal gangs Ransomware payments fell by more than a third last year to $813m (£650m) as victims refused to pay cybercriminals and law enforcement cracked down on gangs, figures reveal. The decline in such cyber-attacks – where access to a computer or its data is blocked and money is then demanded to release it – came despite a number of high-profile cases in 2024, with victims including NHS trusts in the UK and the US doughnut firm Krispy Kreme. Continue reading... [...]
