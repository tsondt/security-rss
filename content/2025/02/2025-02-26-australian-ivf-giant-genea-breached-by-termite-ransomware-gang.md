Title: Australian IVF giant Genea breached by Termite ransomware gang
Date: 2025-02-26T08:31:05-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-26-australian-ivf-giant-genea-breached-by-termite-ransomware-gang

[Source](https://www.bleepingcomputer.com/news/security/australian-ivf-giant-genea-breached-by-termite-ransomware-gang/){:target="_blank" rel="noopener"}

> ​The Termite ransomware gang has claimed responsibility for stealing sensitive healthcare data in a recent breach of Genea, one of Australia's largest fertility services providers. [...]
