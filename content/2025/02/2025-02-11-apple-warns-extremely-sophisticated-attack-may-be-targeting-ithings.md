Title: Apple warns 'extremely sophisticated attack' may be targeting iThings
Date: 2025-02-11T01:58:06+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-02-11-apple-warns-extremely-sophisticated-attack-may-be-targeting-ithings

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/11/apple_ios_ipados_patches/){:target="_blank" rel="noopener"}

> Cupertino mostly uses bland language when talking security, so this sounds nasty Apple has warned that some iPhones and iPads may have been targeted by an “extremely sophisticated attack” and has posted patches that hopefully prevent it.... [...]
