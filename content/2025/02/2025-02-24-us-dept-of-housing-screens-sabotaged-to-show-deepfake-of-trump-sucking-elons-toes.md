Title: US Dept of Housing screens sabotaged to show deepfake of Trump sucking Elon's toes
Date: 2025-02-24T20:15:33+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-02-24-us-dept-of-housing-screens-sabotaged-to-show-deepfake-of-trump-sucking-elons-toes

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/24/hud_trump_toe_sucking/){:target="_blank" rel="noopener"}

> 'Appropriate action will be taken,' we're told – as federal HR email sparks uproar, ax falls on CISA staff Visitors to the US Department of Housing and Urban Development's headquarters in the capital got some unpleasant viewing on Monday morning after TV screens across the building began showing a deepfake video of President Trump kissing and sucking Elon Musk's toes.... [...]
