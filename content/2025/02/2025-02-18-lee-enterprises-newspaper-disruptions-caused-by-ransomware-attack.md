Title: Lee Enterprises newspaper disruptions caused by ransomware attack
Date: 2025-02-18T07:35:35-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-18-lee-enterprises-newspaper-disruptions-caused-by-ransomware-attack

[Source](https://www.bleepingcomputer.com/news/security/lee-enterprises-newspaper-disruptions-caused-by-ransomware-attack/){:target="_blank" rel="noopener"}

> Newspaper publishing giant Lee Enterprises has confirmed that a ransomware attack is behind ongoing disruptions impacting the group's operations for over two weeks. [...]
