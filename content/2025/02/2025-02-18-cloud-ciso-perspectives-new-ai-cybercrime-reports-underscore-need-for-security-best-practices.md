Title: Cloud CISO Perspectives: New AI, cybercrime reports underscore need for security best practices
Date: 2025-02-18T17:00:00+00:00
Author: Stephanie Kiel
Category: GCP Security
Tags: Cloud CISO;Security & Identity
Slug: 2025-02-18-cloud-ciso-perspectives-new-ai-cybercrime-reports-underscore-need-for-security-best-practices

[Source](https://cloud.google.com/blog/products/identity-security/cloud-ciso-perspectives-new-ai-cybercrime-reports-underscore-need-security-best-practices/){:target="_blank" rel="noopener"}

> Welcome to the first Cloud CISO Perspectives for February 2025. Stephanie Kiel, our head of cloud security policy, government affairs and public policy, discusses two parallel and important security conversations she had at the Munich Security Conference, following our new reports on AI and cybercrime. As with all Cloud CISO Perspectives, the contents of this newsletter are posted to the Google Cloud blog. If you’re reading this on the website and you’d like to receive the email version, you can subscribe here. --Phil Venables, VP, TI Security & CISO, Google Cloud aside_block <ListValue: [StructValue([('title', 'Get vital board insights with Google [...]
