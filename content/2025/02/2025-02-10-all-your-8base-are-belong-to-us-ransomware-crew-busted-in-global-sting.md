Title: All your 8Base are belong to us: Ransomware crew busted in global sting
Date: 2025-02-10T22:15:13+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-02-10-all-your-8base-are-belong-to-us-ransomware-crew-busted-in-global-sting

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/10/8base_police_arrrest/){:target="_blank" rel="noopener"}

> Dark web site seized, four cuffed in Thailand An international police operation spanning the US, Europe, and Asia has shuttered the 8Base ransomware crew's dark web presence and resulted in the arrest of four European suspects accused of stealing $16 million from more than 1,000 victims worldwide.... [...]
