Title: New WinRAR version strips Windows metadata to increase privacy
Date: 2025-02-18T17:57:39-05:00
Author: Lawrence Abrams
Category: BleepingComputer
Tags: Security;Software
Slug: 2025-02-18-new-winrar-version-strips-windows-metadata-to-increase-privacy

[Source](https://www.bleepingcomputer.com/news/security/new-winrar-version-strips-windows-metadata-to-increase-privacy/){:target="_blank" rel="noopener"}

> WinRAR 7.10 was released yesterday with numerous features, such as larger memory pages, a dark mode, and the ability to fine-tune how Windows Mark-of-the-Web flags are propagated when extracting files. [...]
