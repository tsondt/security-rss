Title: Google: How to make any AMD Zen CPU always generate 4 as a random number
Date: 2025-02-04T23:30:46+00:00
Author: Iain Thomson
Category: The Register
Tags: 
Slug: 2025-02-04-google-how-to-make-any-amd-zen-cpu-always-generate-4-as-a-random-number

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/04/google_amd_microcode/){:target="_blank" rel="noopener"}

> Malicious microcode vulnerability discovered, fixes rolling out for Epycs at least Googlers have not only figured out how to break AMD's security – allowing them to load unofficial microcode into its processors to modify the silicon's behavior as they wish – but also demonstrated this by producing a microcode patch that makes the chips always output 4 when asked for a random number.... [...]
