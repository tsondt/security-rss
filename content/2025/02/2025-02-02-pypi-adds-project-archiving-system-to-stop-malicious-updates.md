Title: PyPI adds project archiving system to stop malicious updates
Date: 2025-02-02T10:32:59-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-02-pypi-adds-project-archiving-system-to-stop-malicious-updates

[Source](https://www.bleepingcomputer.com/news/security/pypi-adds-project-archiving-system-to-stop-malicious-updates/){:target="_blank" rel="noopener"}

> The Python Package Index (PyPI) has announced the introduction of 'Project Archival,' a new system that allows publishers to archive their projects, indicating to the users that no updates are to be expected. [...]
