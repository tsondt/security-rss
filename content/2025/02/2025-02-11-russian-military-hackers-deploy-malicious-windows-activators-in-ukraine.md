Title: Russian military hackers deploy malicious Windows activators in Ukraine
Date: 2025-02-11T11:44:59-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security;Microsoft
Slug: 2025-02-11-russian-military-hackers-deploy-malicious-windows-activators-in-ukraine

[Source](https://www.bleepingcomputer.com/news/security/russian-military-hackers-deploy-malicious-windows-activators-in-ukraine/){:target="_blank" rel="noopener"}

> The Sandworm Russian military cyber-espionage group is targeting Windows users in Ukraine with trojanized Microsoft Key Management Service (KMS) activators and fake Windows updates. [...]
