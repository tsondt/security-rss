Title: Hackers spoof Microsoft ADFS login pages to steal credentials
Date: 2025-02-05T13:41:27-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-05-hackers-spoof-microsoft-adfs-login-pages-to-steal-credentials

[Source](https://www.bleepingcomputer.com/news/security/hackers-spoof-microsoft-adfs-login-pages-to-steal-credentials/){:target="_blank" rel="noopener"}

> A help desk phishing campaign targets an organization's Microsoft Active Directory Federation Services (ADFS) using spoofed login pages to steal credentials and bypass multi-factor authentication (MFA) protections. [...]
