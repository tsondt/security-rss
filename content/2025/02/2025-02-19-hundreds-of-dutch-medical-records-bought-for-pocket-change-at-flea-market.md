Title: Hundreds of Dutch medical records bought for pocket change at flea market
Date: 2025-02-19T13:01:37+00:00
Author: Connor Jones
Category: The Register
Tags: 
Slug: 2025-02-19-hundreds-of-dutch-medical-records-bought-for-pocket-change-at-flea-market

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/19/hundreds_of_dutch_medical_records/){:target="_blank" rel="noopener"}

> 15GB of sensitive files traced back to former software biz Typically shoppers can expect to find tie-dye t-shirts, broken lamps and old disco records at flea markets, now it seems storage drives filled with huge volumes of sensitive data can be added to that list.... [...]
