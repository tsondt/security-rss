Title: Financially motivated hackers are helping their espionage counterparts and vice versa
Date: 2025-02-13T11:00:40+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;cybercrime;espionage;nation states
Slug: 2025-02-13-financially-motivated-hackers-are-helping-their-espionage-counterparts-and-vice-versa

[Source](https://arstechnica.com/security/2025/02/financially-motivated-hackers-are-helping-their-espionage-counterparts-and-vice-versa/){:target="_blank" rel="noopener"}

> There’s a growing collaboration between hacking groups engaging in espionage on behalf of nation-states and those seeking financial gains through ransomware and other forms of cybercrime, researchers noted this week. There has always been some level of overlap between these two groups, but it has become more pronounced in recent years. On Tuesday, the Google-owned Mandiant security firm said the uptick comes amid tighter purse strings and as a means for concealing nation-state-sponsored espionage by making it blend in with financially motivated cyberattacks. Opportunities abound “Modern cybercriminals are likely to specialize in a particular area of cybercrime and partner with [...]
