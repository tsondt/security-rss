Title: How Phished Data Turns into Apple & Google Wallets
Date: 2025-02-18T18:37:26+00:00
Author: BrianKrebs
Category: Brian Krebs
Tags: A Little Sunshine;All About Skimmers;The Coming Storm;Web Fraud 2.0;Andy Chandler;apple;CSIS Security Group;Ford Merrill;ghost tap;google;Grant Smith;iMessage;M3AAWG;RCS;Resecurity;SecAlliance;smishing;ThreatFabric;ZNFC
Slug: 2025-02-18-how-phished-data-turns-into-apple-google-wallets

[Source](https://krebsonsecurity.com/2025/02/how-phished-data-turns-into-apple-google-wallets/){:target="_blank" rel="noopener"}

> Carding — the underground business of stealing, selling and swiping stolen payment card data — has long been the dominion of Russia-based hackers. Happily, the broad deployment of more secure chip-based payment cards in the United States has weakened the carding market. But a flurry of innovation from cybercrime groups in China is breathing new life into the carding industry, by turning phished card data into mobile wallets that can be used online and at main street stores. An image from one Chinese phishing group’s Telegram channel shows various toll road phish kits available. If you own a mobile phone, [...]
