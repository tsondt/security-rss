Title: US sanctions LockBit ransomware’s bulletproof hosting provider
Date: 2025-02-11T09:24:43-05:00
Author: Sergiu Gatlan
Category: BleepingComputer
Tags: Security
Slug: 2025-02-11-us-sanctions-lockbit-ransomwares-bulletproof-hosting-provider

[Source](https://www.bleepingcomputer.com/news/security/us-sanctions-lockbit-ransomwares-bulletproof-hosting-provider/){:target="_blank" rel="noopener"}

> ​The United States, Australia, and the United Kingdom have sanctioned Zservers, a Russia-based bulletproof hosting (BPH) services provider, for supplying essential attack infrastructure for the LockBit ransomware gang. [...]
