Title: Watchdog ponders why Apple doesn't apply its strict app tracking rules to itself
Date: 2025-02-14T09:28:08+00:00
Author: Jude Karabus
Category: The Register
Tags: 
Slug: 2025-02-14-watchdog-ponders-why-apple-doesnt-apply-its-strict-app-tracking-rules-to-itself

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/14/apple_app_tracking_probe/){:target="_blank" rel="noopener"}

> Germany's Federal Cartel Office voices concerns iPhone maker may be breaking competition law Apple is feeling the heat over its acclaimed iPhone privacy policy after a German regulator's review of iOS tracking consent alleged that the tech giant exempted itself from the rules it enforces on third-party developers.... [...]
