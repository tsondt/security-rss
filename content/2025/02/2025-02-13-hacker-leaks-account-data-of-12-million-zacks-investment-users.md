Title: Hacker leaks account data of 12 million Zacks Investment users
Date: 2025-02-13T12:39:36-05:00
Author: Bill Toulas
Category: BleepingComputer
Tags: Security
Slug: 2025-02-13-hacker-leaks-account-data-of-12-million-zacks-investment-users

[Source](https://www.bleepingcomputer.com/news/security/hacker-leaks-account-data-of-12-million-zacks-investment-users/){:target="_blank" rel="noopener"}

> Zacks Investment Research (Zacks) last year reportedly suffered another data breach that exposed sensitive information related to roughly 12 million accounts. [...]
