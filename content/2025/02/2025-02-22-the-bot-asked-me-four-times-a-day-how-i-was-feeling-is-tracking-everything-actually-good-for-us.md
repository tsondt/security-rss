Title: ‘The bot asked me four times a day how I was feeling’: is tracking everything actually good for us?
Date: 2025-02-22T14:00:08+00:00
Author: Tom Faber
Category: The Guardian
Tags: Life and style;Wearable technology;Fitness;Health & wellbeing;Data and computer security;Technology;iPhone;Apple;Mobile phones;Smartphones;Health;Apps;Computing;Society
Slug: 2025-02-22-the-bot-asked-me-four-times-a-day-how-i-was-feeling-is-tracking-everything-actually-good-for-us

[Source](https://www.theguardian.com/lifeandstyle/2025/feb/22/the-bot-asked-me-four-times-a-day-how-i-was-feeling-is-tracking-everything-actually-good-for-us){:target="_blank" rel="noopener"}

> Gathering data used to be a fringe pursuit of Silicon Valley nerds. Now we’re all at it, recording everything from menstrual cycles and mobility to toothbrushing and time spent in daylight. Is this just narcissism redesigned for the big tech age? I first heard about my friend Adam’s curious new habit in a busy pub. He said he’d been doing it for over a year, but had never spoken to anyone about it before. He had a furtive look around, then took out his phone and showed me the product of his burning obsession: a spreadsheet. This was not a [...]
