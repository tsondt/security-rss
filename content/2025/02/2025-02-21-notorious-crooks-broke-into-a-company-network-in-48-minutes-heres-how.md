Title: Notorious crooks broke into a company network in 48 minutes. Here’s how.
Date: 2025-02-21T18:17:28+00:00
Author: Dan Goodin
Category: Ars Technica
Tags: Biz & IT;Security;initial access;quick assist;ransomware;rdp
Slug: 2025-02-21-notorious-crooks-broke-into-a-company-network-in-48-minutes-heres-how

[Source](https://arstechnica.com/security/2025/02/notorious-crooks-broke-into-a-company-network-in-48-minutes-heres-how/){:target="_blank" rel="noopener"}

> In December, roughly a dozen employees inside a manufacturing company received a tsunami of phishing messages that was so big they were unable to perform their day-to-day functions. A little over an hour later, the people behind the email flood had burrowed into the nether reaches of the company's network. This is a story about how such intrusions are occurring faster than ever before and the tactics that make this speed possible. The speed and precision of the attack—laid out in posts published Thursday and last month —are crucial elements for success. As awareness of ransomware attacks increases, security companies [...]
