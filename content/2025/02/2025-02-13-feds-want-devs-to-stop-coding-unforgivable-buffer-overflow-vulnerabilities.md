Title: Feds want devs to stop coding 'unforgivable' buffer overflow vulnerabilities
Date: 2025-02-13T01:29:47+00:00
Author: Jessica Lyons
Category: The Register
Tags: 
Slug: 2025-02-13-feds-want-devs-to-stop-coding-unforgivable-buffer-overflow-vulnerabilities

[Source](https://go.theregister.com/feed/www.theregister.com/2025/02/13/fbi_cisa_unforgivable_buffer_overflow/){:target="_blank" rel="noopener"}

> FBI, CISA harrumph at Microsoft and VMware in call for coders to quit baking avoidable defects into stuff US authorities have labelled buffer overflow vulnerabilities "unforgivable defects”, pointed to the presence of the holes in products from the likes of Microsoft and VMware, and urged all software developers to adopt secure-by-design practices to avoid creating more of them.... [...]
