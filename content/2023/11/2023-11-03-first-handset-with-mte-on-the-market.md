Title: First handset with MTE on the market
Date: 2023-11-03T10:04:00-07:00
Author: Google Project Zero (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2023-11-03-first-handset-with-mte-on-the-market

[Source](https://googleprojectzero.blogspot.com/2023/11/first-handset-with-mte-on-market.html){:target="_blank" rel="noopener"}

> By Mark Brand, Google Project Zero Introduction It's finally time for me to fulfill a long-standing promise. Since I first heard about ARM's Memory Tagging Extensions, I've said (to far too many people at this point to be able to back out...) that I'd immediately switch to the first available device that supported this feature. It's been a long wait (since late 2017) but with the release of the new Pixel 8 / Pixel 8 Pro handsets, there's finally a production handset that allows you to enable MTE! T he ability of MTE to detect memory corruption exploitation at the [...]
