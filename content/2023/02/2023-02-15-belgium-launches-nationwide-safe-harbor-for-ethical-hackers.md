Title: Belgium launches nationwide safe harbor for ethical hackers
Date: 2023-02-15T16:49:14+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-02-15-belgium-launches-nationwide-safe-harbor-for-ethical-hackers

[Source](https://portswigger.net/daily-swig/belgium-launches-nationwide-safe-harbor-for-ethical-hackers){:target="_blank" rel="noopener"}

> New legal protections for security researchers could be the strongest of any EU country [...]
