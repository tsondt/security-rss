Title: Truffle Security relaunches XSS Hunter tool with new features
Date: 2023-02-02T15:08:23+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-02-02-truffle-security-relaunches-xss-hunter-tool-with-new-features

[Source](https://portswigger.net/daily-swig/truffle-security-relaunches-xss-hunter-tool-with-new-features){:target="_blank" rel="noopener"}

> Popular hacking aid now available with CORS misconfig detection function following end-of-life announcement [...]
