Title: NIST plots biggest ever reform of Cybersecurity Framework
Date: 2023-02-23T15:55:58+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-02-23-nist-plots-biggest-ever-reform-of-cybersecurity-framework

[Source](https://portswigger.net/daily-swig/nist-plots-biggest-ever-reform-of-cybersecurity-framework){:target="_blank" rel="noopener"}

> CSF 2.0 blueprint offered up for public review [...]
