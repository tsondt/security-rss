Title: Serious security hole plugged in infosec tool binwalk
Date: 2023-02-03T16:36:49+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-02-03-serious-security-hole-plugged-in-infosec-tool-binwalk

[Source](https://portswigger.net/daily-swig/serious-security-hole-plugged-in-infosec-tool-binwalk){:target="_blank" rel="noopener"}

> Path traversals could ‘void reverse engineering efforts and tamper with evidence collected’ [...]
