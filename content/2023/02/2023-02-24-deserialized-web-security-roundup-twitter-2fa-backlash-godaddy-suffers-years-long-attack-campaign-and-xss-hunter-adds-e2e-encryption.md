Title: Deserialized web security roundup: Twitter 2FA backlash, GoDaddy suffers years-long attack campaign, and XSS Hunter adds e2e encryption
Date: 2023-02-24T13:09:52+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-02-24-deserialized-web-security-roundup-twitter-2fa-backlash-godaddy-suffers-years-long-attack-campaign-and-xss-hunter-adds-e2e-encryption

[Source](https://portswigger.net/daily-swig/deserialized-web-security-roundup-twitter-2fa-backlash-godaddy-suffers-years-long-attack-campaign-and-xss-hunter-adds-e2e-encryption){:target="_blank" rel="noopener"}

> Your fortnightly rundown of AppSec vulnerabilities, new hacking techniques, and other cybersecurity news [...]
