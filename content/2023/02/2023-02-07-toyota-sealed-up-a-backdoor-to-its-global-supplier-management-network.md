Title: Toyota sealed up a backdoor to its global supplier management network
Date: 2023-02-07T17:34:08+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-02-07-toyota-sealed-up-a-backdoor-to-its-global-supplier-management-network

[Source](https://portswigger.net/daily-swig/toyota-sealed-up-a-backdoor-to-its-global-supplier-management-network){:target="_blank" rel="noopener"}

> Hacker praises carmaker’s prompt response to the (mercifully) good-faith pwnage [...]
