Title: New XSS Hunter host Truffle Security faces privacy backlash
Date: 2023-02-09T17:12:41+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-02-09-new-xss-hunter-host-truffle-security-faces-privacy-backlash

[Source](https://portswigger.net/daily-swig/new-xss-hunter-host-truffle-security-faces-privacy-backlash){:target="_blank" rel="noopener"}

> Anonymized numbers of bug discoveries swiftly deleted after pushback [...]
