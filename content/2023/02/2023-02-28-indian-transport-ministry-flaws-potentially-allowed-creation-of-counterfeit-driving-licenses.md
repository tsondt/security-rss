Title: Indian transport ministry flaws potentially allowed creation of counterfeit driving licenses
Date: 2023-02-28T14:15:36+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-02-28-indian-transport-ministry-flaws-potentially-allowed-creation-of-counterfeit-driving-licenses

[Source](https://portswigger.net/daily-swig/indian-transport-ministry-flaws-potentially-allowed-creation-of-counterfeit-driving-licenses){:target="_blank" rel="noopener"}

> Armed with personal data fragments, a researcher could also access 185 million citizens’ PII [...]
