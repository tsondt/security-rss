Title: Google engineers plot to mitigate prototype pollution
Date: 2023-02-06T15:57:39+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-02-06-google-engineers-plot-to-mitigate-prototype-pollution

[Source](https://portswigger.net/daily-swig/google-engineers-plot-to-mitigate-prototype-pollution){:target="_blank" rel="noopener"}

> Plan to create boundary between JavaScript objects and their blueprints gathers momentum [...]
