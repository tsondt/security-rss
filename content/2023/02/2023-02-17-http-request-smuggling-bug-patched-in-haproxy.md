Title: HTTP request smuggling bug patched in HAProxy
Date: 2023-02-17T16:05:58+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-02-17-http-request-smuggling-bug-patched-in-haproxy

[Source](https://portswigger.net/daily-swig/http-request-smuggling-bug-patched-in-haproxy){:target="_blank" rel="noopener"}

> Exploitation could enable attackers to access backend servers [...]
