Title: CVSS system criticized for failure to address real-world impact
Date: 2023-02-21T15:34:50+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-02-21-cvss-system-criticized-for-failure-to-address-real-world-impact

[Source](https://portswigger.net/daily-swig/cvss-system-criticized-for-failure-to-address-real-world-impact){:target="_blank" rel="noopener"}

> JFrog argues vulnerability risk metrics need complete revamp [...]
