Title: We’re going teetotal: It’s goodbye to The Daily Swig
Date: 2023-03-02T14:05:45+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-03-02-were-going-teetotal-its-goodbye-to-the-daily-swig

[Source](https://portswigger.net/daily-swig/were-going-teetotal-its-goodbye-to-the-daily-swig){:target="_blank" rel="noopener"}

> PortSwigger today announces that The Daily Swig is closing down [...]
