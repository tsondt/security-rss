Title: Multiple Internet to Baseband Remote Code Execution Vulnerabilities in Exynos Modems
Date: 2023-03-16T11:07:00.015000-07:00
Author: Google Project Zero (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2023-03-16-multiple-internet-to-baseband-remote-code-execution-vulnerabilities-in-exynos-modems

[Source](https://googleprojectzero.blogspot.com/2023/03/multiple-internet-to-baseband-remote-rce.html){:target="_blank" rel="noopener"}

> Posted by Tim Willis, Project Zero In late 2022 and early 2023, Project Zero reported eighteen 0-day vulnerabilities in Exynos Modems produced by Samsung Semiconductor. The four most severe of these eighteen vulnerabilities (CVE-2023-24033, CVE-2023-26496, CVE-2023-26497 and CVE-2023-26498) allowed for Internet-to-baseband remote code execution. Tests conducted by Project Zero confirm that those four vulnerabilities allow an attacker to remotely compromise a phone at the baseband level with no user interaction, and require only that the attacker know the victim's phone number. With limited additional research and development, we believe that skilled attackers would be able to quickly create an operational [...]
