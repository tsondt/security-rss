Title: Popular password managers auto-filled credentials on untrusted websites
Date: 2023-01-20T12:09:52+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-20-popular-password-managers-auto-filled-credentials-on-untrusted-websites

[Source](https://portswigger.net/daily-swig/popular-password-managers-auto-filled-credentials-on-untrusted-websites){:target="_blank" rel="noopener"}

> Dashlane, Bitwarden, and Safari all cited by Google researchers [...]
