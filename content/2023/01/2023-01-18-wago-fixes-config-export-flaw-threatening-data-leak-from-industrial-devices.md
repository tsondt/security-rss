Title: WAGO fixes config export flaw threatening data leak from industrial devices
Date: 2023-01-18T15:34:13+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-18-wago-fixes-config-export-flaw-threatening-data-leak-from-industrial-devices

[Source](https://portswigger.net/daily-swig/wago-fixes-config-export-flaw-threatening-data-leak-from-industrial-devices){:target="_blank" rel="noopener"}

> Severity somewhat blunted by reboot-related caveat [...]
