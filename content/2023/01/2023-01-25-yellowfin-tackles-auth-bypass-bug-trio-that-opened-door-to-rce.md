Title: Yellowfin tackles auth bypass bug trio that opened door to RCE
Date: 2023-01-25T16:23:21+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-25-yellowfin-tackles-auth-bypass-bug-trio-that-opened-door-to-rce

[Source](https://portswigger.net/daily-swig/yellowfin-tackles-auth-bypass-bug-trio-that-opened-door-to-rce){:target="_blank" rel="noopener"}

> Pre- and post-auth path to pwnage [...]
