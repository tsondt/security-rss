Title: Facebook two-factor authentication bypass issue patched
Date: 2023-01-27T11:50:00+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-27-facebook-two-factor-authentication-bypass-issue-patched

[Source](https://portswigger.net/daily-swig/facebook-two-factor-authentication-bypass-issue-patched){:target="_blank" rel="noopener"}

> Security vulnerability was one of Meta’s top bugs of 2022 [...]
