Title: Bitwarden responds to encryption design flaw criticism
Date: 2023-01-25T15:47:46+00:00
Author: The Daily Swig
Category: The Daily Swig
Tags: 
Slug: 2023-01-25-bitwarden-responds-to-encryption-design-flaw-criticism

[Source](https://portswigger.net/daily-swig/bitwarden-responds-to-encryption-design-flaw-criticism){:target="_blank" rel="noopener"}

> Password vault vendor accused of making a hash of encryption [...]
