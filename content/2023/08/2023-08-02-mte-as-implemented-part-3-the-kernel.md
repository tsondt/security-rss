Title: MTE As Implemented, Part 3: The Kernel
Date: 2023-08-02T09:30:00.004000-07:00
Author: Google Project Zero (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2023-08-02-mte-as-implemented-part-3-the-kernel

[Source](https://googleprojectzero.blogspot.com/2023/08/mte-as-implemented-part-3-kernel.html){:target="_blank" rel="noopener"}

> By Mark Brand, Project Zero Background In 2018, in the v8.5a version of the ARM architecture, ARM proposed a hardware implementation of tagged memory, referred to as MTE (Memory Tagging Extensions). In Part 1 we discussed testing the technical (and implementation) limitations of MTE on the hardware that we've had access to. In Part 2 we discussed the implications of this for mitigations built using MTE in various user-mode contexts. This post will now consider the implications of what we know on the effectiveness of MTE-based mitigations in the kernel context. To recap - t here are two key classes [...]
