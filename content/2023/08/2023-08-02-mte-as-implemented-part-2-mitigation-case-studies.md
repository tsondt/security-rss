Title: MTE As Implemented, Part 2: Mitigation Case Studies
Date: 2023-08-02T09:30:00.002000-07:00
Author: Google Project Zero (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2023-08-02-mte-as-implemented-part-2-mitigation-case-studies

[Source](https://googleprojectzero.blogspot.com/2023/08/mte-as-implemented-part-2-mitigation.html){:target="_blank" rel="noopener"}

> By Mark Brand, Project Zero Background In 2018, in the v8.5a version of the ARM architecture, ARM proposed a hardware implementation of tagged memory, referred to as MTE (Memory Tagging Extensions). In Part 1 we discussed testing the technical (and implementation) limitations of MTE on the hardware that we've had access to. This post will now consider the implications of what we know on the effectiveness of MTE-based mitigations in several important products/contexts. To summarize - t here are two key classes of bypass techniques for memory-tagging based mitigations, and these are the following ( for some examples, see Part [...]
