Title: Release of a Technical Report into Intel Trust Domain Extensions
Date: 2023-04-24T09:27:00.001000-07:00
Author: Google Project Zero (noreply@blogger.com)
Category: Google Project Zero
Tags: 
Slug: 2023-04-24-release-of-a-technical-report-into-intel-trust-domain-extensions

[Source](https://googleprojectzero.blogspot.com/2023/04/technical-report-into-intel-tdx.html){:target="_blank" rel="noopener"}

> Today, members of Google Project Zero and Google Cloud are releasing a report on a security review of Intel's Trust Domain Extensions (TDX). TDX is a feature introduced to support Confidential Computing by providing hardware isolation of virtual machine guests at runtime. This isolation is achieved by securing sensitive resources, such as guest physical memory. This restricts what information is exposed to the hosting environment. The security review was performed in cooperation with Intel engineers on pre-release source code for version 1.0 of the TDX feature. This code is the basis for the TDX implementation which will be shipping in [...]
