Security Feed
#############

:save_as: index.html
:url:
:date: 2020-10-13T16:55:05.302076
:cover: {static}/images/cover.jpg
:hide_navbar_brand: True
:landing:

    .. container:: m-row

        .. container:: m-col-l-10 m-push-l-1

            .. raw:: html

                <h2>Security Feed</h2>
                <h3>"Standing on the shoulders of giants"</h3>

    .. container:: m-row

        .. container:: m-col-l-10 m-push-l-1 m-note m-default

            Welcome! This site aggregates
            `RSS feeds <https://gitlab.com/tsondt/security-rss/-/blob/master/feed_sources.json>`_
            about information security.

            ..

            .. raw:: html

                <h3>Why am I creating yet another news aggregation site?</h3>

            ..

            As `an infosec professional <https://tsondt.com>`_, I need to keep
            up with all the news happening. I could have used an RSS reader but
            having something live on the Internet is much better (don't need to
            set up on many different devices) It is also a great opportunity for
            me to learn new stuff.

            I have been experimenting with different Static Site Generators (SSG)
            and I am pretty familiar with `Jekyll <https://jekyllrb.com/>`_ and
            `Hugo <https://gohugo.io/>`_. As my main language is Python, I
            would like to try another one written in Python, which leads me to
            `Pelican <https://blog.getpelican.com/>`_.

            .. raw:: html

                <h3>How is this site built?</h3>

            The source of this site can be found on
            `GitLab <https://gitlab.com/tsondt/security-rss>`_. The theme I used
            is `m.css <https://mcss.mosra.cz>`_ (Pelican does have a nice
            collection of `themes <https://github.com/getpelican/pelican-themes>`_
            and `plugins <https://github.com/getpelican/pelican-plugins>`_,
            but the selection is not as great as other more popular frameworks)

            There is a small
            `Python script <https://gitlab.com/tsondt/security-rss/-/blob/master/feed_importer.py>`_
            used to import feed data from various feed sources. It is scheduled
            to run daily using GitLab CI
            (`config <https://gitlab.com/tsondt/security-rss/-/blob/master/.gitlab-ci.yml>`_).
            More detailed information can be found in my
            `blog post <https://blog.tsondt.com/posts/ssg-experiment>`_.
