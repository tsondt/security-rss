# Security RSS
[![pipeline status](https://gitlab.com/tsondt/security-rss/badges/master/pipeline.svg)](https://gitlab.com/tsondt/security-rss/-/commits/master)

## Development
```bash
pipenv lock -r > requirements.txt
pipenv lock -r --dev-only > dev-requirements.txt
```

## Deployment
[.gitlab-ci.yml](.gitlab-ci.yml)
